<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
	<ui:define name="body">
		<h:form id="apiForm">
			<div id="bottom">
				<h1>
					<h:graphicImage id="imgApiRest" alt="REST API Test"
						value="/images/headers/hdr-rest-api-test-tool.gif" />
				</h1>
			</div>
			<div id="table-four-content">
			 <h:panelGrid columns="2">
			     <h:outputLabel value="API Endpoint" />
			     <h:inputText id="endPoint" size="100" value="#{apiTestBean.endPoint}" />
			     <h:outputLabel value="Username" />
                 <h:inputText id="username" size="50" value="#{apiTestBean.username}" />
                 <h:outputLabel value="Password" />
                 <h:inputText id="password" size="50" value="#{apiTestBean.password}" />
			     <h:outputLabel value="Request" />
			     <h:inputTextarea id="request" cols="50" rows="10" value="#{apiTestBean.request}" />
			     <h:outputLabel value="" />
			     <h:commandButton id="submitButton" value="Submit" action="#{apiTestBean.submitAction}" />
			     <h:outputLabel value="Response" />
                 <h:inputTextarea id="response" cols="50" rows="10" value="#{apiTestBean.response}" />
			 </h:panelGrid>
			</div>
		</h:form>
	</ui:define>
</ui:composition>
</html>