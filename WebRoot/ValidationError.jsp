<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
      
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>400 Bad Request</title>
</head>
<body>


Status:400 Bad Request<br>
Reason: The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.<br> 
Error details:<%=request.getAttribute("javax.servlet.error.message") %>
</body>
</html>