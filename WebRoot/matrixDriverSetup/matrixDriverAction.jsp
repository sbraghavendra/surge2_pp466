<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
xmlns:ui="http://java.sun.com/jsf/facelets"
xmlns:h="http://java.sun.com/jsf/html"
xmlns:f="http://java.sun.com/jsf/core"
xmlns:rich="http://richfaces.org/rich"
xmlns:a4j="http://richfaces.org/a4j">


<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">

<h:form id="matrixDriverAdd">
<h1><h:graphicImage id="imgMatrixDrivers" alt="Matrix Drivers" value="/images/headers/hdr-matrix-drivers.gif"/></h1>
<div id="top">
<div id="table-one">
<div id="table-one-top">
  <a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
</div>
<div id="table-one-content" style="height:300px;">
  
  <table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
    <thead>
      <tr><td colspan="6"><h:outputText value="#{MatrixDriverBean.actionHeading}"/>&#160;</td></tr>
    </thead>
    <tbody>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td>Driver ID:</td>
        <td colspan="1"><h:inputText id="drvId" value="#{MatrixDriverBean.driverId}" disabled="true"/></td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td>Trans. Dtl. Column Name:</td>
        <td colspan="1">
          <h:inputText id="tr_detail_column" style="width:650px;" onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
          	disabled="#{! MatrixDriverBean.changeTransColumn}"
          	binding="#{MatrixDriverBean.transDtlColName}" 
          	valueChangeListener="#{MatrixDriverBean.transDtlColNameChanged}" >&#160;
          	
          	<a4j:support event="onkeyup" ajaxSingle="true" reRender="chkNullDriverAllowed,chkWildCardAllowed" />                    
          </h:inputText>
          <a4j:commandButton value ="select" id="select"
          	rendered="#{MatrixDriverBean.changeTransColumn}"
          	styleClass="image" image="/images/search_small.png" immediate="true"
          	actionListener="#{MatrixDriverBean.getAllByTableName}"
          	oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm" />       
        </td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td style="width:50px;">&#160;</td>
        <td colspan="1">
          <h:inputText id="trColumnDesc" style="width:650px;" immediate="true"
          value="#{MatrixDriverBean.transDtlColDesc}" disabled="true"/>&#160;&#160;&#160;
        </td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td>Matrix Column Name:</td>
        <td colspan="1">
          <h:inputText id="matrixName" style="width:650px;" value="#{MatrixDriverBean.matrixColumnName}" disabled="true"/></td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td><h:outputText id="mand" value="Mandatory?:" />&#160;</td>
        <td><h:selectBooleanCheckbox id="chkMandatory" align="left" styleClass="check" value="#{MatrixDriverBean.mandatoryFlag}" disabled="#{MatrixDriverBean.deleteAction}"  />&#160;</td>
        <td style="width:100%;">&#160;</td>
      </tr>
      
      <h:panelGroup rendered="#{MatrixDriverBean.selectedCategory == 'T'}">
      <tr>
        <td style="width:50px;">&#160;</td>
        <td><h:outputText id="bUnit" value="Business Unit?:" rendered="#{MatrixDriverBean.selectedCategory == 'T'}"/>&#160;</td>
        <td><h:selectBooleanCheckbox id="chkBusinessUnit"  styleClass="check" value="#{MatrixDriverBean.businessUnitFlag}" rendered="#{MatrixDriverBean.selectedCategory == 'T'}" 
            disabled="#{MatrixDriverBean.selectedDriverFlag}" />&#160;</td>
        <td style="width:100%;">&#160;</td>
      </tr>
      </h:panelGroup>
      
      <h:panelGroup rendered="#{MatrixDriverBean.selectedCategory != 'E'}">
      <tr>
        <td style="width:50px;">&#160;</td>
        <td><h:outputText id="nullDrv" value="Null Driver Allowed?:" />&#160;</td>
        <td>
        	<h:selectBooleanCheckbox id="chkNullDriverAllowed" align="left" styleClass="check" value="#{MatrixDriverBean.nullDriverFlag}" disabled="#{MatrixDriverBean.deleteAction or MatrixDriverBean.isStateCodeUsed}" > 
            	<a4j:support id="ajaxiCheck_nullselectedFlag" event="onclick" actionListener="#{MatrixDriverBean.selectTableCheckChange}" >
				</a4j:support>
        	</h:selectBooleanCheckbox>&#160;</td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td><h:outputText id="wildCrd"   value="Wild Card Allowed?:" />&#160;</td>
        <td>
        	<h:selectBooleanCheckbox  id="chkWildCardAllowed" styleClass="check" value="#{MatrixDriverBean.wildcardFlag}" disabled="#{MatrixDriverBean.deleteAction or MatrixDriverBean.isStateCodeUsed}" > 	
        		<a4j:support id="ajaxiCheck_wildselectedFlag" event="onclick" actionListener="#{MatrixDriverBean.selectTableCheckChange}" >
        		</a4j:support>
			</h:selectBooleanCheckbox>&#160;</td>
        <td style="width:100%;">&#160;</td>
      </tr>
      </h:panelGroup>
      
<!--        <h:panelGroup rendered="#{MatrixDriverBean.selectedCategory == 'T'}" >  -->
<!--        <tr>  -->
<!--          <td style="width:50px;">&#160;</td>  -->
<!--          <td><h:outputText id="range" value="Range Allowed?:" />&#160;</td>  -->
<!--          <td style="width:50px;"><h:selectBooleanCheckbox id="chkRangeAllowed" styleClass="check" value="#{MatrixDriverBean.rangeFlag}" />&#160;</td>  -->
<!--          <td><h:outputText id="convert" value="Convert to Number?:" />&#160;</td>  -->
<!--          <td style="width:40px;"><h:selectBooleanCheckbox id="chkConvertToNumber" align="left" styleClass="check" value="#{MatrixDriverBean.toNumberFlag}" />&#160;</td>  -->
<!--          <td style="width:100%;">&#160;</td>  -->
<!--        </tr>  -->
<!--        </h:panelGroup>  -->
      
      <tr>
        <td style="width:50px;">&#160;</td>
        <td><h:outputText id="active" value="Active/Available?:" />&#160;</td>
        <td><h:selectBooleanCheckbox id="chkActive" styleClass="check" value="#{MatrixDriverBean.activeFlag}" disabled="#{MatrixDriverBean.deleteAction}" />&#160;</td>
        <td colspan="1" style="width:100%;">&#160;</td>
      </tr>

      <tr>
        <td style="width:50px;">&#160;</td>
        <td>Last Update User ID:</td>
        <td colspan = "1">
        	<h:inputText id="userId" style="width:650px;" value="#{MatrixDriverBean.userId}" disabled="true"/>&#160;
        </td>
        <td style="width:100%;">&#160;</td>
      </tr>
      <tr>
        <td style="width:50px;">&#160;</td>
        <td>Last Update TimeStamp:</td>
        <td colspan = "1">
        	<h:inputText id="userTime" style="width:650px;" value="#{MatrixDriverBean.userTimestamp}" disabled="true">
               <f:converter converterId="dateTime"/>
			</h:inputText>&#160;
		</td>
        <td style="width:100%;">&#160;</td>
      </tr>
    </tbody>			
  </table>
</div>
<div id="table-one-bottom">
  <ul class="right">		
    <li class="ok"><h:commandLink id="process" action="#{MatrixDriverBean.actionOK}" /></li>
    <li class="cancel"><h:commandLink id="cancelAction" action="#{MatrixDriverBean.cancelAction}" /></li>
  </ul>
</div>

</div>
</div>

</h:form>
<ui:include src="/WEB-INF/view/components/transactionDetailCol_search.xhtml">
<ui:param name="handler" value="#{MatrixDriverBean}"/>
<ui:param name="reRender" value="MatrixDriverAdd:column_input"/>
</ui:include>

</ui:define>
</ui:composition>
</html>
