<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
xmlns:ui="http://java.sun.com/jsf/facelets"
xmlns:h="http://java.sun.com/jsf/html"
xmlns:f="http://java.sun.com/jsf/core"
xmlns:c="http://java.sun.com/jstl/core"
xmlns:rich="http://richfaces.org/rich"
xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() {
selectRowByIndex('matrixDriverView:driverNames', 'driverNamesRowIndex'); }
);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverNamesRowIndex" value="#{MatrixDriverBean.selectedDriverNamesIndex}"/>
<h:form id="matrixDriverView">

<h1><img id="imgMatrixDrivers" alt="Matrix Drivers" src="../images/headers/hdr-matrix-drivers.gif" width="250" height="19" /></h1>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSelectDriver.gif" />
	</span>
</div>
<h:outputText width="300" style="color:red; font-weight: bold; text-align:right; font: 12px Helvetica Neue, Arial, Helvetica, Geneva, sans-serif;" 
value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;** All menu options disabled.  To enable, log out and log back in." rendered="#{loginBean.showInactivedMenuMark}" />


<div id="bottom">
<div id="table-four">
<div id="table-one-top" style="padding-left: 15px">
  <h:outputLabel id="categorySelectionLabel" for="categorySelection" value="Driver Type: " />
  <h:selectOneMenu id="categorySelection" value="#{MatrixDriverBean.selectedCategory}" style="width:250px;"
  		valueChangeListener="#{MatrixDriverBean.onCategoryChange}">
  	<f:selectItems value="#{MatrixDriverBean.categoriesMenuItems}" />
  	<a4j:support ajaxSingle="true" event="onchange" oncomplete="clearRow('matrixDriverView:driverNames');" action="setup_matrix_drivers"/>
  </h:selectOneMenu>
  <a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
</div>
<div id="table-four-content">
<div class="scrollContainer">
<div class="scrollInner">
  <rich:dataTable rowClasses="odd-row,even-row" id="driverNames" 	value="#{MatrixDriverBean.driverNames}" var="nameDTO"
    reRender="driverReferences">
  <a4j:support event="onRowClick"
  onsubmit="selectRow('matrixDriverView:driverNames', this);"
  actionListener="#{MatrixDriverBean.selectedNameRowChanged}"
  reRender="driverReferences,update, delete"/>

  <!-- 				<a4j:support event="onselectionchange" -->
  <!-- 					actionListener="#{MatrixDriverBean.selectedNameRowChanged}" -->
  <!-- 					reRender="driverReferences, update, delete" /> -->

  <rich:column id="driverId" style="text-align: right;" rendered="true" sortBy="#{nameDTO.driverNamesPK.driverId}">
  <f:facet name="header">
  <h:outputText styleClass="headerText" value="Driver ID" />
  </f:facet>
  <h:outputText value="#{nameDTO.driverNamesPK.driverId}" />
  </rich:column>
  
  <rich:column id="description" rendered="true" sortBy="#{nameDTO.description}">
  <f:facet name="header">
  <h:outputText styleClass="headerText" value="Description" />
  </f:facet>
  <h:outputText value="#{nameDTO.description}" />
  </rich:column>
  
  <rich:column id="transDtlColumnName" rendered="true" sortBy="#{nameDTO.transDtlColName}">
  <f:facet name="header">
  <h:outputText styleClass="headerText" value="Trans. Dtl Column Name" />
  </f:facet>
  <h:outputText value="#{nameDTO.transDtlColName}" />
  </rich:column>

  <rich:column id="matrixColumnName" rendered="true" sortBy="#{nameDTO.matrixColName}">
  <f:facet name="header">
  <h:outputText styleClass="headerText" value="Matrix Column Name" />
  </f:facet>
  <h:outputText value="#{nameDTO.matrixColName}" />
  </rich:column>

  <rich:column id="businessUnitFlagBoolean" style="text-align: center;" rendered="#{MatrixDriverBean.selectedCategory == 'T'}" sortBy="#{nameDTO.businessUnitFlagBoolean}">
  <f:facet name="header">
  <h:outputText styleClass="headerText" value="Bus. Unit?" />
  </f:facet>
  <h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.businessUnitFlagBoolean}" disabled="true" />
  </rich:column>

  <rich:column id="mandatoryFlagBoolean" style="text-align: center;" rendered="true" sortBy="#{nameDTO.mandatoryFlagBoolean}">
  <f:facet name="header">
<h:outputText styleClass="headerText" value="Mandatory?" />
</f:facet>
<h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.mandatoryFlagBoolean}" disabled="true" />
</rich:column>

<rich:column id="nullDriverFlagBoolean" style="text-align: center;" rendered="#{MatrixDriverBean.selectedCategory != 'E'}" sortBy="#{nameDTO.nullDriverFlagBoolean}">
<f:facet name="header">
<h:outputText styleClass="headerText" value="Null Driver?" />
</f:facet>
<h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.nullDriverFlagBoolean}" disabled="true" />
</rich:column>

<rich:column id="wildcardFlagBoolean" style="text-align: center;" rendered="#{MatrixDriverBean.selectedCategory != 'E'}" sortBy="#{nameDTO.wildcardFlagBoolean}">
<f:facet name="header">
<h:outputText styleClass="headerText" value="Wildcard Allowed?" />
</f:facet>
<h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.wildcardFlagBoolean}" disabled="true" />
</rich:column>

<!--  <rich:column id="rangeFlagBoolean" style="text-align: center;" rendered="#{MatrixDriverBean.selectedCategory == 'T'}">  -->
<!--  <f:facet name="header">  -->
<!--  <h:outputText styleClass="headerText" value="Range Allowed?" />  -->
<!--  </f:facet>  -->
<!--  <h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.rangeFlagBoolean}" disabled="true" />  -->
<!--  </rich:column>  -->

<!--  <rich:column id="toNumberFlagBoolean" style="text-align: center;" rendered="#{MatrixDriverBean.selectedCategory == 'T'}">  -->
<!--  <f:facet name="header">  -->
<!--  <h:outputText styleClass="headerText" value="Convert to Number?" />  -->
<!--  </f:facet>  -->
<!--  <h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.toNumberFlagBoolean}" disabled="true" />  -->
<!--  </rich:column>  -->

<rich:column id="activeFlagBoolean" style="text-align: center;" rendered="true" sortBy="#{nameDTO.activeFlagBoolean}">
<f:facet name="header">
<h:outputText styleClass="headerText" value="Active?" />
</f:facet>
<h:selectBooleanCheckbox styleClass="check" value="#{nameDTO.activeFlagBoolean}" disabled="true" />
</rich:column>

<rich:column id="driverWeight" style="text-align: right;" rendered="#{MatrixDriverBean.selectedCategory != 'E'}" sortBy="#{nameDTO.driverWeight}">
<f:facet name="header">
<h:outputText styleClass="headerText" value="Driver Weight" />
</f:facet>
<h:outputText value="#{nameDTO.driverWeight}">
<f:convertNumber integerOnly="true" />
</h:outputText>
</rich:column>

</rich:dataTable>
</div>
</div>
</div>
<div id="table-one-bottom">		
  <ul class="right">
    <li class="add2"><h:commandLink id="add" disabled="#{! MatrixDriverBean.canAdd or MatrixDriverBean.currentUser.viewOnlyBooleanFlag}" style="#{(MatrixDriverBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{MatrixDriverBean.addMatrixAction}"/></li>
    <li class="update2"><h:commandLink id="update" disabled="#{! MatrixDriverBean.canUpdate or MatrixDriverBean.currentUser.viewOnlyBooleanFlag}" style="#{(MatrixDriverBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{MatrixDriverBean.updateMatrixAction}"/></li>
    <li class="deletebtm"><h:commandLink id="delete" disabled="#{! MatrixDriverBean.canDelete or MatrixDriverBean.currentUser.viewOnlyBooleanFlag}" style="#{(MatrixDriverBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{MatrixDriverBean.deleteMatrixAction}"/></li>
  </ul>		
</div>
</div>
</div>

<div class="wrapper"><span class="block-right">&#160;</span> <span
  class="block-left tab">
  <img src="../images/containers/STSSecurity-details-open.gif"/>
  </span></div>

  <div id="bottom">
  <div id="table-four">	
  <div id="table-four-top">	</div>	
  <div id="table-four-content">
  <div class="scrollContainer">
  <div class="scrollInner">
    <rich:dataTable rowClasses="odd-row,even-row"  id="driverReferences"
    value="#{MatrixDriverBean.driverReferences}" var="referenceDTO">
    
    <rich:column id="driverValue" sortBy="#{referenceDTO.driverValue}">
    <f:facet name="header">
    <h:outputText styleClass="headerText" value="Driver Value" />
    </f:facet>
    <h:outputText value="#{referenceDTO.driverValue}" />
    </rich:column>

    <rich:column id="driverDesc" sortBy="#{referenceDTO.driverDesc}">
    <f:facet name="header">
    <h:outputText styleClass="headerText" value="Driver Description" />
    </f:facet>
    <h:outputText value="#{referenceDTO.driverDesc}" />
    </rich:column>

    </rich:dataTable>
  </div>
  </div>
  </div>
  </div>
  </div>

</h:form>
</ui:define>
</ui:composition>
</html>
