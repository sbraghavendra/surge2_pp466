<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxrateAddForm:taxrateDataTable', 'selectedTaxRateIndex'); } );
//]]>
</script>	
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedTaxRateIndex" value="#{TaxJurisdictionBean.selectedTaxRateIndex}"/>
<h:form id="taxrateAddForm">
<h1><h:graphicImage id="imgTransactionTaxRate" alt="Transaction Tax Rate" value="/images/headers/hdr-transaction-process.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height: 100px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7">Add a Tax Rate from a Transaction</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>GeoCode:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.geocode}" 
					 	 disabled="true" id="txtGeocode"
				         maxlength="50" />
                </td>
			
				<td style="width:50px;">&#160;</td>
				<td>City:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.city}" 
					 	 disabled="true" id="txtCity"
				         maxlength="50" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>State:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.state}" 
					 	 disabled="true" id="txtState"
				         maxlength="50" />
                </td>
			
				<td style="width:50px;">&#160;</td>
				<td>ZIP Code:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.zip}" 
					 	 disabled="true" id="txtZip"
				         maxlength="50" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>County:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.county}" 
					 	 disabled="true" id="txtCounty"
				         maxlength="50" />						
                </td>
			
				<td style="width:50px;">&#160;</td>
				<td>Country:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.country}" 
					 	 disabled="true" id="txtCountry"
				         maxlength="50" />
				</td>
			</tr>
			<tr>
				
				<td style="width:50px;">&#160;</td>
				<td>In Out:</td>
				<td>
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.inOut}" 
					 	 disabled="true" id="txtInOut"
				         maxlength="50" />
				</td>
				
				<td style="width:50px;">&#160;</td>
				<td style="width:50px;">&#160;</td>
				<td style="width:50px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td colspan="4">
				<h:inputText value="#{transactionDetailBean.currentJurisdiction.description}" 
					 	 disabled="true" id="txtDescription"
				         style="width:650px;" 
				         maxlength="50" />
                </td>
			</tr>			
		</tbody>
	</table>
	</div>

	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSView-Tax Rates.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
		<div id="table-four-top">
		</div>
		<div id="table-four-content" style="height: 250px;">
			<div class="scrollContainer">
				<div class="scrollInner" id="resize">
				<rich:dataTable rowClasses="odd-row,even-row" 
				id="taxrateDataTable" styleClass="GridContent"
				value="#{transactionDetailBean.jurisdictionTaxrateList}" var="taxRateDTO">
				
				<a4j:support event="onRowClick"
					onsubmit="selectRow('taxrateAddForm:taxrateDataTable', this);"
					actionListener="#{transactionDetailBean.taxrateRowChanged}"
					reRender="copy" />
					
		<rich:column id="jurisdictionTaxrateId" sortBy="#{taxRateDTO.jurisdictionTaxrateId}">
			<f:facet name="header"><h:outputText value="Jur. Taxrate ID" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.jurisdictionTaxrateId}" />
		</rich:column>

		<rich:column id="effectiveDate" sortBy="#{taxRateDTO.effectiveDate}" sortOrder="DESCENDING" >
			<f:facet name="header"><h:outputText value="Effective Date" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.effectiveDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
			</h:outputText>
		</rich:column>

		<rich:column id="ratetypeCode" sortBy="#{TaxJurisdictionBean.rateTypeCodeToRateTypeMap[taxRateDTO.ratetypeCode]}" sortOrder="ASCENDING" >
			<f:facet name="header"><h:outputText value="Rate Type" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{TaxJurisdictionBean.rateTypeCodeToRateTypeMap[taxRateDTO.ratetypeCode]}" />
		</rich:column>

		<rich:column id="expirationDate" sortBy="#{taxRateDTO.expirationDate}">
			<f:facet name="header"><h:outputText value="Expiration Date" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.expirationDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
			</h:outputText>
		</rich:column>
		
		<!-- 4061 -->
		<rich:column id="combinedSalesRate" sortBy="#{taxRateDTO.combinedSalesRate}">
			<f:facet name="header"><h:outputText value="Combined Sales Rate" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.combinedSalesRate}" >
				<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
			</h:outputText>
		</rich:column>
		
		<rich:column id="combinedUseRate" sortBy="#{taxRateDTO.combinedUseRate}">
			<f:facet name="header"><h:outputText value="Combined Use Rate" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.combinedUseRate}"  >
				<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
			</h:outputText>
		</rich:column>
		
		<!-- Display Sales Rate -->		
		<c:if test="#{TaxJurisdictionBean.displaySalesRates}">
		
		<rich:column id="countrySalesTier1Rate" >
			<f:facet name="header"><h:outputText value="Country Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countrySalesTier1Rate}" />
		</rich:column>
	
		<rich:column id="stateSalesTier1Rate">
			<f:facet name="header"><h:outputText value="State Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateSalesTier1Rate}" />
		</rich:column>
		
		<rich:column id="countySalesTier1Rate">
			<f:facet name="header"><h:outputText value="County Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countySalesTier1Rate}" />
		</rich:column>

		<rich:column id="citySalesTier1Rate">
			<f:facet name="header"><h:outputText value="City Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.citySalesTier1Rate}" />
		</rich:column>

		<rich:column id="stj1SalesRate">
			<f:facet name="header"><h:outputText value="STJ1 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj1SalesRate}" />
		</rich:column>
		
		<rich:column id="stj2SalesRate">
			<f:facet name="header"><h:outputText value="STJ2 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj2SalesRate}" />
		</rich:column>
		
		<rich:column id="stj3SalesRate">
			<f:facet name="header"><h:outputText value="STJ3 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj3SalesRate}" />
		</rich:column>
		
		<rich:column id="stj4SalesRate">
			<f:facet name="header"><h:outputText value="STJ4 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj4SalesRate}" />
		</rich:column>
		
		<rich:column id="stj5SalesRate">
			<f:facet name="header"><h:outputText value="STJ5 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj5SalesRate}" />
		</rich:column>

		</c:if>
		<!-- End of Sales Rate -->

		<!-- Display Use Rate -->
		<c:if test="#{TaxJurisdictionBean.displayUseRates}">
	
		<rich:column id="countryUseTier1Rate">
			<f:facet name="header"><h:outputText value="Country Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countryUseTier1Rate}" />
		</rich:column>
	
		<rich:column id="stateUseTier1Rate">
			<f:facet name="header"><h:outputText value="State Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateUseTier1Rate}" />
		</rich:column>
		
		<rich:column id="countyUseTier1Rate">
			<f:facet name="header"><h:outputText value="County Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countyUseTier1Rate}" />
		</rich:column>

		<rich:column id="cityUseTier1Rate">
			<f:facet name="header"><h:outputText value="City Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.cityUseTier1Rate}" />
		</rich:column>

		<rich:column id="stj1UseRate">
			<f:facet name="header"><h:outputText value="STJ1 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj1UseRate}" />
		</rich:column>
		
		<rich:column id="stj2UseRate">
			<f:facet name="header"><h:outputText value="STJ2 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj2UseRate}" />
		</rich:column>
		
		<rich:column id="stj3UseRate">
			<f:facet name="header"><h:outputText value="STJ3 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj3UseRate}" />
		</rich:column>
		
		<rich:column id="stj4UseRate">
			<f:facet name="header"><h:outputText value="STJ4 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj4UseRate}" />
		</rich:column>
		
		<rich:column id="stj5UseRate">
			<f:facet name="header"><h:outputText value="STJ5 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj5UseRate}" />
		</rich:column>
	
		</c:if>
		<!-- End of Use Rate -->
<!-- PP-24			
		<rich:column id="stateSplitAmount">
			<f:facet name="header"><h:outputText value="Tier 1 Max Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateSplitAmount}" />
		</rich:column>

		<rich:column id="stateTier2MinAmount">
			<f:facet name="header"><h:outputText value="State Tier2 Min Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateTier2MinAmount}" />
		</rich:column>

		<rich:column id="stateTier2MaxAmount">
			<f:facet name="header"><h:outputText value="State Tier2 Max Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateTier2MaxAmount}" />
		</rich:column>

		<rich:column id="stateMaxtaxAmount">
			<f:facet name="header"><h:outputText value="State Max Tax Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateMaxtaxAmount}" />
		</rich:column>	
		
		<rich:column id="countySplitAmount">
			<f:facet name="header"><h:outputText value="County Split Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.countySplitAmount}" />
		</rich:column>

		<rich:column id="countyMaxtaxAmount">
			<f:facet name="header"><h:outputText value="County Max Tax Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.countyMaxtaxAmount}" />
		</rich:column>
		
		<rich:column id="citySplitAmount">
			<f:facet name="header"><h:outputText value="City Split Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.citySplitAmount}" />
		</rich:column>
-->
		<rich:column id="customFlagBoolean">
			<f:facet name="header"><h:outputText value="Custom?" /></f:facet>
			<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxRateDTO.customFlagBoolean}" />
		</rich:column>

		<rich:column id="modifiedFlagBoolean">
			<f:facet name="header"><h:outputText value="Modified?" /></f:facet>
			<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxRateDTO.modifiedFlagBoolean}" />
		</rich:column>

				</rich:dataTable>

	                
		
				</div>
			</div>
		</div>
	</div>
	<div id="table-four-bottom">
		<ul class="right">
	 		<li class="add"><h:commandLink id="okBtn" action="#{transactionDetailBean.addTaxRateAction}" 
	 							onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
			<li class="copy-add"><h:commandLink id="copy" action="#{transactionDetailBean.copyTaxRateAction}" disabled="#{empty transactionDetailBean.selectedTaxRate}"
								onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')"/></li>
			<li class="cancel"><h:commandLink id="cancel" action="trans_main"/></li>
		</ul>
	</div>
</div>

</h:form>


<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>

</ui:define>

</ui:composition>

</html>