<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() {changePanel(document.getElementById('filterForm:ddlDisplay'));});

var currentPanel = "limitedInformationFilter";
function changePanel(menu) {
	var obj = document.getElementById(currentPanel);
	if (obj) obj.style.display = "none";
	obj = document.getElementById(menu.value);
	if (obj) obj.style.display = "block";
	currentPanel = menu.value;
}

function changeDisplayPanel(objId) {
	var obj = document.getElementById(objId);
	
	if (obj) obj.style.display = "none";
	obj.style.display = "block";
	
	if (obj) obj.style.display = "block";
	obj.style.display = "none";
}

function insertDateTime(objId) {
	var obj = document.getElementById(objId);
	if (obj) {
		var time = new Date();
		obj.value = obj.value +
			(time.getMonth() + 1) + '/' +
			time.getDate() + '/' +
			time.getFullYear() + ' ' +
			time.getHours() + ':' +
			time.getMinutes() + ':' +
			time.getSeconds() + ' - ';  
	}
}

function toggleApplyAll(txtId, chkId) {
	var txt = document.getElementById(txtId);
	var chk = document.getElementById(chkId);
	if (chk) {
		chk.disabled = (!txt || (txt.value == ''));
	}
}

function resetMinimumDrivers(id) {
	var obj = document.getElementById(id).getElementsByTagName('input');
	for (var i = 0; i < obj.length; i++) {
		if (!obj[i].disabled && obj[i].checked) {
			obj[i].checked = false;
		}
	}
}

function resetState()
{ 
    document.getElementById("filterForm:state").value = "";
}
registerEvent(window, "load", adjustHeight);
//]]>
</script>
<style type="text/css">
.ie7 .scrollContainer {
	padding-top: 26px;
}

.ie7 .scrollContainer thead tr {
	height: 18px;
	vertical-align: top;
}
#table-one-content th{
background-color: #ecf4fe;
padding: 3px;
}
</style>
</ui:define>

<ui:define name="body">
	<h:form id="filterForm">
		<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-transaction-sale.gif" width="250" height="19" /></h1>
		
		<a4j:outputPanel id="msg" ><h:messages errorClass="error" /></a4j:outputPanel>
	
		<a4j:outputPanel id="showhidefilter">
		<div id="top" style="width:1060px">
		<div id="table-one" >	
		<div id="table-four-top">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
			<tr>
				<td><h:outputLabel id="selectionlabel" value="View Section: "  />
					<h:selectOneMenu id="ddlDisplay" style="width:130px;font-size:11px;" value="#{transactionDetailSaleViewBean.selectedDisplay}" immediate="true"
							valueChangeListener="#{transactionDetailSaleViewBean.displaySelectionChanged}">
							<f:selectItem itemValue="limitedInformationFilter" itemLabel="Limited Information"/>
							<f:selectItem itemValue="basicinformationFilter" itemLabel="Basic Information"/>
							<f:selectItem itemValue="customerFilter" itemLabel="Customer"/>	
							<f:selectItem itemValue="invoiceFilter" itemLabel="Invoice"/>
							<f:selectItem itemValue="locationsFilter" itemLabel="Locations"/>
							<f:selectItem itemValue="taxamountsFilter" itemLabel="Tax Amounts"/>
							<f:selectItem itemValue="transactionFilter" itemLabel="Transaction"/>
							<f:selectItem itemValue="userfieldsFilter" itemLabel="User Fields"/>
							<f:selectItem itemValue="resultViewFilter" itemLabel="Results"/>
							<a4j:support event="onchange"   />
					
					</h:selectOneMenu>
				</td>
				
				<td style="padding-left: 2px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
					 reRender="showhidefilter" actionListener="#{transactionDetailSaleViewBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
				<td style="padding-left: 2px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
  					 reRender="showhidefilter" actionListener="#{transactionDetailSaleViewBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
				<td style="width:40%">&#160;</td>
				<td class="error" style="padding-right: 10px;"><h:outputText  value="Transaction Error" rendered="#{(transactionDetailSaleViewBean.saleTransactionFilter.transactionInd eq 'E' )}" /></td> 
				<!-- <td style="padding-right: 10px;" ><h:outputText style="color:white;" value="&#160;" /></td> -->
			</tr>
			</table>
		</div>
		
		<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'filterForm:searchBtn')" >
		
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td style="align:left;" colspan="8">View a Sale Transaction</td></tr>
		</thead>
		</table>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'limitedInformationFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_limitedinformation_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'basicinformationFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_basicinformation_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'customerFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_customer_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'invoiceFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_invoice_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'locationsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_locations_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'taxamountsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
			<div id="scrollhidden" style="overflow:hidden;">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_taxamounts_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
				</div>
			</a4j:outputPanel>
		</c:if>
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'transactionFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_transaction_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		
		<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'userfieldsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_userfields_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
			<c:if test="#{transactionDetailSaleViewBean.selectedDisplay == 'resultViewFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true"  layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_result_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleViewBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleViewBean}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		</div>
		 <!-- table-one-content -->
		<div id="table-four-bottom">
			<ul class="right"><li class="ok"><h:commandLink id="okBtn" action="#{transactionDetailSaleViewBean.viewOkAction}"/></li></ul>
		</div>
		
		</div>
		</div>
		</a4j:outputPanel>
	
    
    	
	</h:form>
<!--  
<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="filterForm:jurisInput"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandler1}"/>
	<ui:param name="popupName" value="searchJurisdiction1"/>
	<ui:param name="popupForm" value="searchJurisdictionForm1"/>
	<ui:param name="jurisId" value="jurisId1"/>
	<ui:param name="reRender" value="filterForm:jurisInput1"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandler2}"/>
	<ui:param name="popupName" value="searchJurisdiction2"/>
	<ui:param name="popupForm" value="searchJurisdictionForm2"/>
	<ui:param name="jurisId" value="jurisId2"/>
	<ui:param name="reRender" value="filterForm:jurisInput2"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandler3}"/>
	<ui:param name="popupName" value="searchJurisdiction3"/>
	<ui:param name="popupForm" value="searchJurisdictionForm3"/>
	<ui:param name="jurisId" value="jurisId3"/>
	<ui:param name="reRender" value="filterForm:jurisInput3"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandler4}"/>
	<ui:param name="popupName" value="searchJurisdiction4"/>
	<ui:param name="popupForm" value="searchJurisdictionForm4"/>
	<ui:param name="jurisId" value="jurisId4"/>
	<ui:param name="reRender" value="filterForm:jurisInput4"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandler}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionID"/>
	<ui:param name="popupForm" value="searchJurisdictionFormID"/>
	<ui:param name="jurisId" value="ratesJurisId"/>
	<ui:param name="reRender" value="filterForm:jurisdictionId"/>
</ui:include>


<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdictionShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipto"/>
	<ui:param name="jurisId" value="jurisIdShipto"/>
	<ui:param name="reRender" value="filterForm:jurisInputShipto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerShipfrom}"/>
	<ui:param name="popupName" value="searchJurisdictionShipfrom"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipfrom"/>
	<ui:param name="jurisId" value="jurisIdShipfrom"/>
	<ui:param name="reRender" value="filterForm:jurisInputShipfrom"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerOrdrorgn}"/>
	<ui:param name="popupName" value="searchJurisdictionOrdrorgn"/>
	<ui:param name="popupForm" value="searchJurisdictionFormOrdrorgn"/>
	<ui:param name="jurisId" value="jurisIdOrdrorgn"/>
	<ui:param name="reRender" value="filterForm:jurisInputOrdrorgn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerOrdracpt}"/>
	<ui:param name="popupName" value="searchJurisdictionOrdracpt"/>
	<ui:param name="popupForm" value="searchJurisdictionFormOrdracpt"/>
	<ui:param name="jurisId" value="jurisIdOrdracpt"/>
	<ui:param name="reRender" value="filterForm:jurisInputOrdracpt"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerFirstuse}"/>
	<ui:param name="popupName" value="searchJurisdictionFirstuse"/>
	<ui:param name="popupForm" value="searchJurisdictionFormFirstuse"/>
	<ui:param name="jurisId" value="jurisIdFirstuse"/>
	<ui:param name="reRender" value="filterForm:jurisInputFirstuse"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.filterHandlerBillto}"/>
	<ui:param name="popupName" value="searchJurisdictionBillto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormBillto"/>
	<ui:param name="jurisId" value="jurisIdBillto"/>
	<ui:param name="reRender" value="filterForm:jurisInputBillto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerShipto}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDShipto"/>
	<ui:param name="jurisId" value="ratesJurisIdShipto"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdShipto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerShipfrom}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDShipfrom"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDShipfrom"/>
	<ui:param name="jurisId" value="ratesJurisIdShipfrom"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdShipfrom"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerOrdrorgn}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDOrdrorgn"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDOrdrorgn"/>
	<ui:param name="jurisId" value="ratesJurisIdOrdrorgn"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdOrdrorgn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerOrdracpt}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDOrdracpt"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDOrdracpt"/>
	<ui:param name="jurisId" value="ratesJurisIdOrdracpt"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdOrdracpt"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerFirstuse}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDFirstuse"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDFirstuse"/>
	<ui:param name="jurisId" value="ratesJurisIdFirstuse"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdFirstuse"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleViewBean.jurisdictionHandlerBillto}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDBillto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDBillto"/>
	<ui:param name="jurisId" value="ratesJurisIdBillto"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdBillto"/>
</ui:include>
-->
	
</ui:define>
</ui:composition>
</html>
