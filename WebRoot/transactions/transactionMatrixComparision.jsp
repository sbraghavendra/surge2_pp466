<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">


<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('transactionmatrixCompareForm', 'selectedCompareTaxMatrixIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('transactionmatrixCompareForm', 'selectedCompareLocationMatrixIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="transactionmatrixCompareForm">

<h1><h:graphicImage id="imgtransmatrixanalysis"  alt="Transaction matrix comparision" url="/images/headers/hdr-transaction_matrix_analysis.png"  /></h1>
<div id="top">
   <div id="table-one"> 

   		<div id="table-one-top">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>

		<div id="table-one-content" style="height: auto;" >
			<div id="embedded-table">
				<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">	 
      				<tr>
      					<td class="column-spacer">&#160;</td>		
      					<td class="column-spacer" colspan="2"><h:outputText value="Transaction:" style = "font-weight: bold;" /></td>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
					</tr>
					
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td align="left" class="column-label">&#160;Transaction ID:</td>
						<td style="width:160px;">
							<h:inputText style="text-align:right;float:left;width:160px;" id="transactionid" value = "#{transactionDetailBean.selectedTransaction.purchtransId}" disabled = "true" />
						</td>
						<td>
							<h:commandButton id="viewComparisonTransBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/view-taxcode_small.png" 
								action="#{transactionDetailBean.displayComparisonTransViewAction}">
							</h:commandButton>
						</td>	
					</tr>	
					
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td align="left" class="column-label">&#160;G&amp;S Matrix ID:</td>
						<td style="width:160px;">
							<h:inputText style="text-align:right;float:left;width:160px;" id="enteredDesc" value = "#{transactionDetailBean.selectedTransaction.taxMatrixId}"  disabled = "true"/>
						</td>
						<td>
							<h:commandButton id="viewTaxMatrixBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/view-taxcode_small.png" 
								action="#{transactionDetailBean.displayViewTaxMatrixAction}">
							</h:commandButton>
						</td>			
					</tr>
			 
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td align="left" class="column-label">&#160;Locn Matrix ID:</td>
						<td style="width:160px;">
							<h:inputText style="text-align:right;float:left;width:160px;" id="locnmatrixid" value = "#{transactionDetailBean.selectedTransaction.shiptoLocnMatrixId}" disabled = "true" />
						</td>
						<td>
							<h:commandButton id="viewLocationMatrixBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/view-taxcode_small.png" 
								action="#{transactionDetailBean.displayViewLocationMatrixAction}">
							</h:commandButton>
						</td>			
					</tr>
					
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer" colspan="2"><h:outputText value="Compare&#160;To:" style = "font-weight: bold;" /></td>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
					</tr>
					
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td align="left" class="column-label">&#160;Matrix Type:</td>
						<td style="width:167px;padding-left: 2px;">
							<h:selectOneMenu id="matrixtype" style="width:167px;" value= "#{transactionDetailBean.compareToMatrixType}" 
											valueChangeListener="#{transactionDetailBean.matrixTypeValueChanged}" immediate="true" >
								 <c:if test = "#{not empty transactionDetailBean.selectedTransaction.taxMatrixId}">
									 <f:selectItem itemValue = "T" itemLabel = "&#160;Goods &amp; Services Matrix" />
								</c:if>
								<c:if test = "#{not empty transactionDetailBean.selectedTransaction.shiptoLocnMatrixId}">
								  <f:selectItem itemValue = "L" itemLabel = "&#160;Location Matrix"   /> 
							   </c:if>						   
							   <a4j:support ajaxSingle="true" event="onchange" reRender="transactionmatrixCompareForm" />					   
					        </h:selectOneMenu>
				        </td>
				        <td class="column-spacer">&#160;</td>		
					</tr>
					
					<tr>
						<td class="column-spacer">&#160;</td>
						<td class="column-spacer">&#160;</td>
						<td align="left" class="column-label">&#160;Matrix ID:</td>
						<td style="width:160px;">
							<h:inputText style="text-align:right;float:left;width:160px;" id="matrixid" value= "#{transactionDetailBean.compareToMatrixId}" 
								onkeypress="return onlyIntegerValue(event);" >
								<f:convertNumber integerOnly="true" groupingUsed="false" />
							</h:inputText>								
						</td>
						<td>
							<h:commandButton id="comparedViewMatrixBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/view-taxcode_small.png" 
								action="#{transactionDetailBean.displayComparedViewAction}">
							</h:commandButton>
							
							<h:commandButton id="taxMatrixIdBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								disabled="#{transactionDetailBean.taxMatrixIdDisabled}"
								image="/images/search_small.png" immediate="true" 
								action="#{transactionDetailBean.findComparedMatrixIdAction}">
							</h:commandButton>
						</td>			
					</tr>
				</table>	
			</div>
		</div>
		<!-- end of table-one-content -->       
		      
		<div id="table-one-bottom">
			<ul class="right">
				<li class="search">
					<h:commandLink id="btncompareSearch" action="#{transactionDetailBean.displayTransMatrixAction}" />
				</li>
			</ul>
		</div>
    </div>
 </div>


<!-- Start data table -->
<div id="bottom" >
	<div id="table-four">
	
	<div id="table-four-top" style="padding-left: 15px;">	
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
			<tbody>
			<tr>
				<td align="left" style="font-weight: bold ;font-size : 11px">
					<h:outputText value="Comparison "/>
				</td>
				<td style="width:80%;" >
				</td>				
				<td>
					<h:outputText value="&#160;W&#160;" style="visibility:visible;background-color:#00b300;" />
				</td>
				<td>
					<h:outputText value="&#160;=&#160;Wildcard Allowed&#160;&#160;" />
				</td>
				<td>
					<h:outputText value="&#160;N&#160;" style="visibility:visible;background-color:#ff0080;" />
				</td>
				<td>
					<h:outputText value="&#160;=&#160;Null Allowed&#160;&#160;" />
				</td>
				<td>
					<h:outputText value="&#160;A&#160;" style="visibility:visible;background-color:#3399ff;" />
				</td>
				<td>
					<h:outputText value="&#160;=&#160;Active&#160;&#160;" />
				</td>			
				<td style="width: 20px;">&#160;</td>
			</tr>
			</tbody>
		</table>					
	</div>
	
     <div id="table-four-contentSmall">

			<rich:dataTable rowKeyVar="index" rowClasses="odd-row,even-row"  id="txnMatrixList" 
				value="#{transactionDetailBean.transactionMatrixList}" var = "compare">
				
				<rich:column id="colcomparemodule" >
					<f:facet name="header">
						<h:outputText id="outcomparemodule" value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"/>
					</f:facet>
					<h:outputText value = "#{compare.comparemodule}"  />
				</rich:column>
				
				<rich:column id="matrixid" >
					<f:facet name="header">
						<h:outputText id="outmoduleid" value="Matrix ID"/>
					</f:facet>
					<h:outputText value = "#{compare.matrixId}"  />
				</rich:column>
				
				<rich:column id="locmodule">
					<f:facet name="header">
						<h:outputText id="locoutmodule" value = "Module"  />
					</f:facet>
					<div align="left">
						<h:outputText value = "#{compare.modulecode}" style = "#{(index eq 2 and compare.modulecode ne 'Match' )?  'color : #FF0000' : 'color : #000000'}" /> 
					</div>
					<div align="right">
				        <h:outputText value="&#160;&#160;" rendered="true" />	       
				    </div>
				</rich:column>
							
				<rich:column id="entity">
					<f:facet name="header">
						<h:outputText id="outentity" value="Entity"/>
					</f:facet>
					<div align="left">
						<h:outputText value = "#{compare.entitycode}" style = "#{(index eq 2 and compare.entitycode ne 'Match' )?  'color : #FF0000' : 'color : #000000'}"  />
					</div>
					<div align="right">
				        <h:outputText value="&#160;&#160;" rendered="true" />	       
				    </div>		
				</rich:column>
				
				<rich:column id="effectivedate">
					<f:facet name="header">
						<h:outputText id="outeffectivedate" value="G/L or Effective Date"/>
					</f:facet>
					<div align="left">
						<h:outputText value = "#{compare.effectiveDate}" style = "#{(index eq 2 and compare.effectiveDate ne 'Match' )?  'color : #FF0000' : 'color : #000000'}" />
					</div>
					<div align="right">
				        <h:outputText value="&#160;&#160;" rendered="true" />	       
				    </div>
				</rich:column>
				
				<rich:column id="expirationdate">
					<f:facet name="header">
						<h:outputText id="outexpirationdate" value="Expiration Date"/>
					</f:facet>
					<div align="left">
						<h:outputText value = "#{compare.expirationDate}" style = "#{(index eq 2 and compare.expirationDate ne 'Match' )?  'color : #FF0000' : 'color : #000000'}" />
					</div>
					<div align="right">
				        <h:outputText value="&#160;&#160;" rendered="true" />	       
				    </div>
				</rich:column>
				
				<!-- Tax Driver -->
				<rich:column id="taxcoldriver01" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver01" value="Driver&#160;1&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver01}" rendered="#{!(index eq 2 and compare.driver01 eq 'No Match') and !(index eq 2 and compare.driver01 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver01}" rendered="#{index eq 2 and compare.driver01 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver01}" rendered="#{index eq 2 and compare.driver01 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_01'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_01'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_01'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver02" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver02" value="Driver&#160;2&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver02}" rendered="#{!(index eq 2 and compare.driver02 eq 'No Match') and !(index eq 2 and compare.driver02 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver02}" rendered="#{index eq 2 and compare.driver02 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver02}" rendered="#{index eq 2 and compare.driver02 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_02'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_02'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_02'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver03" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver03" value="Driver&#160;3&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver03}" rendered="#{!(index eq 2 and compare.driver03 eq 'No Match') and !(index eq 2 and compare.driver03 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver03}" rendered="#{index eq 2 and compare.driver03 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver03}" rendered="#{index eq 2 and compare.driver03 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_03'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_03'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_03'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver04" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver04" value="Driver&#160;4&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver04}" rendered="#{!(index eq 2 and compare.driver04 eq 'No Match') and !(index eq 2 and compare.driver04 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver04}" rendered="#{index eq 2 and compare.driver04 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver04}" rendered="#{index eq 2 and compare.driver04 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_04'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_04'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_04'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver05" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver05" value="Driver&#160;5&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver05}" rendered="#{!(index eq 2 and compare.driver05 eq 'No Match') and !(index eq 2 and compare.driver05 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver05}" rendered="#{index eq 2 and compare.driver05 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver05}" rendered="#{index eq 2 and compare.driver05 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_05'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_05'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_05'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver06" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver06" value="Driver&#160;6&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver06}" rendered="#{!(index eq 2 and compare.driver06 eq 'No Match') and !(index eq 2 and compare.driver06 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver06}" rendered="#{index eq 2 and compare.driver06 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver06}" rendered="#{index eq 2 and compare.driver06 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_06'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_06'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_06'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver07" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver07" value="Driver&#160;7&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver07}" rendered="#{!(index eq 2 and compare.driver07 eq 'No Match') and !(index eq 2 and compare.driver07 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver07}" rendered="#{index eq 2 and compare.driver07 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver07}" rendered="#{index eq 2 and compare.driver07 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_07'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_07'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_07'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver08" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver08" value="Driver&#160;8&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver08}" rendered="#{!(index eq 2 and compare.driver08 eq 'No Match') and !(index eq 2 and compare.driver08 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver08}" rendered="#{index eq 2 and compare.driver08 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver08}" rendered="#{index eq 2 and compare.driver08 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_08'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_08'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_08'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver09" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver09" value="Driver&#160;9&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver09}" rendered="#{!(index eq 2 and compare.driver09 eq 'No Match') and !(index eq 2 and compare.driver09 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver09}" rendered="#{index eq 2 and compare.driver09 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver09}" rendered="#{index eq 2 and compare.driver09 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_09'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_09'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_09'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver10" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver10" value="Driver&#160;10&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver10}" rendered="#{!(index eq 2 and compare.driver10 eq 'No Match') and !(index eq 2 and compare.driver10 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver10}" rendered="#{index eq 2 and compare.driver10 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver10}" rendered="#{index eq 2 and compare.driver10 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_10'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_10'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_10'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver11" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver11" value="Driver&#160;11&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver11}" rendered="#{!(index eq 2 and compare.driver11 eq 'No Match') and !(index eq 2 and compare.driver11 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver11}" rendered="#{index eq 2 and compare.driver11 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver11}" rendered="#{index eq 2 and compare.driver11 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_11'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_11'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_11'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver12" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver12" value="Driver&#160;12&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver12}" rendered="#{!(index eq 2 and compare.driver12 eq 'No Match') and !(index eq 2 and compare.driver12 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver12}" rendered="#{index eq 2 and compare.driver12 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver12}" rendered="#{index eq 2 and compare.driver12 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_12'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_12'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_12'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver13" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver13" value="Driver&#160;13&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver13}" rendered="#{!(index eq 2 and compare.driver13 eq 'No Match') and !(index eq 2 and compare.driver13 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver13}" rendered="#{index eq 2 and compare.driver13 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver13}" rendered="#{index eq 2 and compare.driver13 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_13'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_13'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_13'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver14" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver14" value="Driver&#160;14&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver14}" rendered="#{!(index eq 2 and compare.driver14 eq 'No Match') and !(index eq 2 and compare.driver14 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver14}" rendered="#{index eq 2 and compare.driver14 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver14}" rendered="#{index eq 2 and compare.driver14 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_14'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_14'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_14'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver15" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver15" value="Driver&#160;15&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver15}" rendered="#{!(index eq 2 and compare.driver15 eq 'No Match') and !(index eq 2 and compare.driver15 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver15}" rendered="#{index eq 2 and compare.driver15 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver15}" rendered="#{index eq 2 and compare.driver15 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_15'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_15'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_15'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver16" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver16" value="Driver&#160;16&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver16}" rendered="#{!(index eq 2 and compare.driver16 eq 'No Match') and !(index eq 2 and compare.driver16 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver16}" rendered="#{index eq 2 and compare.driver16 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver16}" rendered="#{index eq 2 and compare.driver16 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_16'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_16'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_16'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver17" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver17" value="Driver&#160;17&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver17}" rendered="#{!(index eq 2 and compare.driver17 eq 'No Match') and !(index eq 2 and compare.driver17 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver17}" rendered="#{index eq 2 and compare.driver17 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver17}" rendered="#{index eq 2 and compare.driver17 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_17'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_17'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_17'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver18" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver18" value="Driver&#160;18&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver18}" rendered="#{!(index eq 2 and compare.driver18 eq 'No Match') and !(index eq 2 and compare.driver18 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver18}" rendered="#{index eq 2 and compare.driver18 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver18}" rendered="#{index eq 2 and compare.driver18 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_18'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_18'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_18'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver19" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver19" value="Driver&#160;19&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver19}" rendered="#{!(index eq 2 and compare.driver19 eq 'No Match') and !(index eq 2 and compare.driver19 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver19}" rendered="#{index eq 2 and compare.driver19 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver19}" rendered="#{index eq 2 and compare.driver19 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_19'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_19'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_19'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver20" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver20" value="Driver&#160;20&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver20}" rendered="#{!(index eq 2 and compare.driver20 eq 'No Match') and !(index eq 2 and compare.driver20 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver20}" rendered="#{index eq 2 and compare.driver20 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver20}" rendered="#{index eq 2 and compare.driver20 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_20'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_20'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_20'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver21" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver21" value="Driver&#160;21&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver21}" rendered="#{!(index eq 2 and compare.driver21 eq 'No Match') and !(index eq 2 and compare.driver21 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver21}" rendered="#{index eq 2 and compare.driver21 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver21}" rendered="#{index eq 2 and compare.driver21 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_21'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_21'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_21'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver22" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver22" value="Driver&#160;22&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver22}" rendered="#{!(index eq 2 and compare.driver22 eq 'No Match') and !(index eq 2 and compare.driver22 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver22}" rendered="#{index eq 2 and compare.driver22 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver22}" rendered="#{index eq 2 and compare.driver22 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_22'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_22'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_22'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver23" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver23" value="Driver&#160;23&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver23}" rendered="#{!(index eq 2 and compare.driver23 eq 'No Match') and !(index eq 2 and compare.driver23 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver23}" rendered="#{index eq 2 and compare.driver23 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver23}" rendered="#{index eq 2 and compare.driver23 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_23'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_23'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_23'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver24" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver24" value="Driver&#160;24&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver24}" rendered="#{!(index eq 2 and compare.driver24 eq 'No Match') and !(index eq 2 and compare.driver24 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver24}" rendered="#{index eq 2 and compare.driver24 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver24}" rendered="#{index eq 2 and compare.driver24 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_24'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_24'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_24'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver25" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver25" value="Driver&#160;25&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver25}" rendered="#{!(index eq 2 and compare.driver25 eq 'No Match') and !(index eq 2 and compare.driver25 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver25}" rendered="#{index eq 2 and compare.driver25 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver25}" rendered="#{index eq 2 and compare.driver25 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_25'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_25'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_25'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver26" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver26" value="Driver&#160;26&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver26}" rendered="#{!(index eq 2 and compare.driver26 eq 'No Match') and !(index eq 2 and compare.driver26 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver26}" rendered="#{index eq 2 and compare.driver26 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver26}" rendered="#{index eq 2 and compare.driver26 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_26'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_26'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_26'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver27" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver27" value="Driver&#160;27&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver27}" rendered="#{!(index eq 2 and compare.driver27 eq 'No Match') and !(index eq 2 and compare.driver27 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver27}" rendered="#{index eq 2 and compare.driver27 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver27}" rendered="#{index eq 2 and compare.driver27 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_27'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_27'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_27'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver28" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver28" value="Driver&#160;28&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver28}" rendered="#{!(index eq 2 and compare.driver28 eq 'No Match') and !(index eq 2 and compare.driver28 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver28}" rendered="#{index eq 2 and compare.driver28 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver28}" rendered="#{index eq 2 and compare.driver28 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_28'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_28'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_28'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver29" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver29" value="Driver&#160;29&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver29}" rendered="#{!(index eq 2 and compare.driver29 eq 'No Match') and !(index eq 2 and compare.driver29 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver29}" rendered="#{index eq 2 and compare.driver29 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver29}" rendered="#{index eq 2 and compare.driver29 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_29'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_29'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_29'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="taxcoldriver30" rendered="#{transactionDetailBean.compareToMatrixType eq 'T' and !empty matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}">
				    <f:facet name="header">
				        <h:outputText id="taxoutcompdriver30" value="Driver&#160;30&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver30}" rendered="#{!(index eq 2 and compare.driver30 eq 'No Match') and !(index eq 2 and compare.driver30 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver30}" rendered="#{index eq 2 and compare.driver30 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver30}" rendered="#{index eq 2 and compare.driver30 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_30'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_30'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.taxMatrixDriverMap['DRIVER_30'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>			
				<!-- End of Tax Driver -->
				
				
				<!-- Location Driver -->
				<rich:column id="loccoldriver01" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver01" value="Driver&#160;1&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver01}" rendered="#{!(index eq 2 and compare.driver01 eq 'No Match') and !(index eq 2 and compare.driver01 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver01}" rendered="#{index eq 2 and compare.driver01 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver01}" rendered="#{index eq 2 and compare.driver01 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_01'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_01'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_01'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver02" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver02" value="Driver&#160;2&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver02}" rendered="#{!(index eq 2 and compare.driver02 eq 'No Match') and !(index eq 2 and compare.driver02 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver02}" rendered="#{index eq 2 and compare.driver02 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver02}" rendered="#{index eq 2 and compare.driver02 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_02'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_02'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_02'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver03" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver03" value="Driver&#160;3&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver03}" rendered="#{!(index eq 2 and compare.driver03 eq 'No Match') and !(index eq 2 and compare.driver03 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver03}" rendered="#{index eq 2 and compare.driver03 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver03}" rendered="#{index eq 2 and compare.driver03 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_03'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_03'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_03'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver04" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver04" value="Driver&#160;4&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver04}" rendered="#{!(index eq 2 and compare.driver04 eq 'No Match') and !(index eq 2 and compare.driver04 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver04}" rendered="#{index eq 2 and compare.driver04 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver04}" rendered="#{index eq 2 and compare.driver04 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_04'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_04'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_04'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver05" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver05" value="Driver&#160;5&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver05}" rendered="#{!(index eq 2 and compare.driver05 eq 'No Match') and !(index eq 2 and compare.driver05 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver05}" rendered="#{index eq 2 and compare.driver05 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver05}" rendered="#{index eq 2 and compare.driver05 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_05'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_05'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_05'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver06" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver06" value="Driver&#160;6&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver06}" rendered="#{!(index eq 2 and compare.driver06 eq 'No Match') and !(index eq 2 and compare.driver06 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver06}" rendered="#{index eq 2 and compare.driver06 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver06}" rendered="#{index eq 2 and compare.driver06 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_06'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_06'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_06'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver07" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver07" value="Driver&#160;7&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver07}" rendered="#{!(index eq 2 and compare.driver07 eq 'No Match') and !(index eq 2 and compare.driver07 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver07}" rendered="#{index eq 2 and compare.driver07 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver07}" rendered="#{index eq 2 and compare.driver07 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_07'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_07'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_07'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver08" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver08" value="Driver&#160;8&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver08}" rendered="#{!(index eq 2 and compare.driver08 eq 'No Match') and !(index eq 2 and compare.driver08 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver08}" rendered="#{index eq 2 and compare.driver08 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver08}" rendered="#{index eq 2 and compare.driver08 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_08'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_08'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_08'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver09" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver09" value="Driver&#160;9&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver09}" rendered="#{!(index eq 2 and compare.driver09 eq 'No Match') and !(index eq 2 and compare.driver09 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver09}" rendered="#{index eq 2 and compare.driver09 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver09}" rendered="#{index eq 2 and compare.driver09 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_09'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_09'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_09'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				
				<rich:column id="loccoldriver10" rendered="#{transactionDetailBean.compareToMatrixType eq 'L' and !empty matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}">
				    <f:facet name="header">
				        <h:outputText id="locoutcompdriver10" value="Driver&#160;10&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}"/>
				    </f:facet>
				    <div align="left">
				        <h:outputText value="#{compare.driver10}" rendered="#{!(index eq 2 and compare.driver10 eq 'No Match') and !(index eq 2 and compare.driver10 eq 'Inactive')}" style ="color:#000000;"/>
				        <h:outputText value="#{compare.driver10}" rendered="#{index eq 2 and compare.driver10 eq 'No Match'}" style ="color:#FF0000;"/>
				        <h:outputText value="#{compare.driver10}" rendered="#{index eq 2 and compare.driver10 eq 'Inactive'}" style ="color:#696969;"/>
				    </div>
				    <div align="right">
				        <h:outputText value="&#160;W&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_10'].wildcardFlag eq '1'}" style="background-color:#00b300;" />
				        <h:outputText value="&#160;N&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_10'].nullDriverFlag eq '1'}" style="background-color:#ff0080;" />
				        <h:outputText value="&#160;A&#160;" rendered="#{empty compare.matrixId and matrixCommonBean.locationMatrixDriverMap['DRIVER_10'].activeFlag eq '1'}" style="background-color:#3399ff;" />
				    </div>
				</rich:column>
				<!-- End of Location Driver -->	
				
				<rich:column id="binaryWt">
					<f:facet name="header">
						<h:outputText id="outbinweight" value="Binary Weight"/>
					</f:facet>
					<h:outputText value = "#{compare.binaryWeight}" style = "#{(index eq 2 and compare.binaryWeight ne 'Match' )?  'color : #FF0000' : 'color : #000000'}"/>
				</rich:column>
				
				<rich:column id="bestMatrix">
					<f:facet name="header">
						<h:outputText id="outbestMatrix" value="Best Matrix?"/>
					</f:facet>
					<h:outputText value = "#{compare.bestMatrix}" style = "#{((index eq 0 and compare.bestMatrix ne 'Best Match') or (index eq 1 and compare.bestMatrix ne 'Best Match'))?  'color : #FF0000' : 'color : #000000'}"/>
				</rich:column>
				
				<rich:column id="active">
					<f:facet name="header">
						<h:outputText id="outactive" value="Active"/>
					</f:facet>
					<div align="left">
						<h:outputText  value = "#{compare.activeFlag}" style = "#{(index eq 2 and compare.activeFlag ne 'Match' )?  'color : #FF0000' : 'color : #000000'}" />
					</div>
					<div align="right">
				        <h:outputText value="&#160;&#160;" rendered="true" />	       
				    </div>
				</rich:column>
			</rich:dataTable>
     </div>
     
     <div id="table-four-bottom" />  
           
     <div class="wrapper"></div>
   
     <div id="table-four-top" style="padding-left: 15px;font-weight: bold ;font-size : 11px"><h:outputText value="Matching Matrix Lines "/></div>
     
     <div id="table-four-content">
	 <div class="scrollContainer">
     <c:if test="#{transactionDetailBean.isLocationMatrixSelected}">
     	<div class="scrollInner"  id="resize1" >
         	<rich:dataTable rowClasses="odd-row,even-row" id="locationMatrixTable"
                    value="#{transactionDetailBean.matchingLocationMatrixList}" var="matrix" >
			
				<a4j:support event="onRowClick" onsubmit="selectRow('transactionmatrixCompareForm:locationMatrixTable', this);"
					actionListener="#{transactionDetailBean.selectedLocationMatrixChanged}" immediate="true"
					reRender="viewBtn,compareBtn"/>
					
				<rich:column width="30px" >
					<f:facet name="header"><h:outputText value="Matrix ID"/></f:facet>
					<h:outputText id="lx_taxMatrixId" value="#{matrix.locationMatrixId}" />
				</rich:column>
				
				<rich:column width="30px" >
					<f:facet name="header"><h:outputText value="Entity"/></f:facet>
					<h:outputText id="lx_entityId" value="#{transactionDetailBean.entityMap[matrix.entityId]}" />
				</rich:column>
				
				<rich:column width="70px" >
					<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
					<h:outputText id="lx_effectiveDate" value="#{matrix.effectiveDate}" >
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
				</rich:column>
				
				<rich:column width="70px" >
					<f:facet name="header"><h:outputText value="Expiration Date"/></f:facet>
					<h:outputText id="lx_expirationDate" value="#{matrix.expirationDate}" >
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
				</rich:column>	
			
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;1&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}"/></f:facet>
				    <h:outputText id="lx_driver01" value="#{matrix.driver01}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;2&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}"/></f:facet>
				    <h:outputText id="lx_driver02" value="#{matrix.driver02}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;3&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}"/></f:facet>
				    <h:outputText id="lx_driver03" value="#{matrix.driver03}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;4&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}"/></f:facet>
				    <h:outputText id="lx_driver04" value="#{matrix.driver04}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;5&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}"/></f:facet>
				    <h:outputText id="lx_driver05" value="#{matrix.driver05}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;6&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}"/></f:facet>
				    <h:outputText id="lx_driver06" value="#{matrix.driver06}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;7&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}"/></f:facet>
				    <h:outputText id="lx_driver07" value="#{matrix.driver07}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;8&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}"/></f:facet>
				    <h:outputText id="lx_driver08" value="#{matrix.driver08}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;9&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}"/></f:facet>
				    <h:outputText id="lx_driver09" value="#{matrix.driver09}" />
				</rich:column>
				<rich:column width="30px" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}" >
				    <f:facet name="header"><h:outputText value="Driver&#160;10&#160;-&#160;#{matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}"/></f:facet>
				    <h:outputText id="lx_driver10" value="#{matrix.driver10}" />
				</rich:column>	
			
				<rich:column width="70px" >
					<f:facet name="header"><h:outputText value="Binary Weight"/></f:facet>
					<h:outputText id="lx_binaryWeight" value="#{matrix.binaryWt}" />
				</rich:column>
				
			</rich:dataTable>
		</div>
	</c:if>
     
    <c:if test="#{transactionDetailBean.isTaxMatrixSelected}">
     	<div class="scrollInner"  id="resize1" >
         	<rich:dataTable rowClasses="odd-row,even-row" id="taxMatrixTable"
                    value="#{transactionDetailBean.matchingTaxMatrixList}" var="matrix" >
			
			<a4j:support event="onRowClick" onsubmit="selectRow('transactionmatrixCompareForm:taxMatrixTable', this);"
					actionListener="#{transactionDetailBean.selectedTaxMatrixChanged}" immediate="true"
					reRender="viewBtn,compareBtn"/>
					
			<rich:column width="30px" >
				<f:facet name="header"><h:outputText value="Matrix ID"/></f:facet>
				<h:outputText id="tx_taxMatrixId" value="#{matrix.taxMatrixId}" />
			</rich:column>
			
			<rich:column width="30px" >
				<f:facet name="header"><h:outputText value="Module"/></f:facet>
				<h:outputText id="tx_moduleCode" value="#{transactionDetailBean.moduleMap[matrix.moduleCode]}" />
			</rich:column>
			
			<rich:column width="120px" >
				<f:facet name="header"><h:outputText value="Entity"/></f:facet>
				<h:outputText id="tx_entityId" value="#{transactionDetailBean.entityMap[matrix.entityId]}" />
			</rich:column>
			
			<rich:column width="70px" >
				<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
				<h:outputText id="tx_effectiveDate" value="#{matrix.effectiveDate}" >
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>
			
			<rich:column width="70px" >
				<f:facet name="header"><h:outputText value="Expiration Date"/></f:facet>
				<h:outputText id="tx_expirationDate" value="#{matrix.expirationDate}" >
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>
			
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;1&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}"/></f:facet>
			    <h:outputText id="tx_driver01" value="#{matrix.driver01}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;2&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}"/></f:facet>
			    <h:outputText id="tx_driver02" value="#{matrix.driver02}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;3&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}"/></f:facet>
			    <h:outputText id="tx_driver03" value="#{matrix.driver03}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;4&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}"/></f:facet>
			    <h:outputText id="tx_driver04" value="#{matrix.driver04}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;5&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}"/></f:facet>
			    <h:outputText id="tx_driver05" value="#{matrix.driver05}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;6&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}"/></f:facet>
			    <h:outputText id="tx_driver06" value="#{matrix.driver06}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;7&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}"/></f:facet>
			    <h:outputText id="tx_driver07" value="#{matrix.driver07}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;8&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}"/></f:facet>
			    <h:outputText id="tx_driver08" value="#{matrix.driver08}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;9&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}"/></f:facet>
			    <h:outputText id="tx_driver09" value="#{matrix.driver09}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;10&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}"/></f:facet>
			    <h:outputText id="tx_driver10" value="#{matrix.driver10}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;11&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}"/></f:facet>
			    <h:outputText id="tx_driver11" value="#{matrix.driver11}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;12&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}"/></f:facet>
			    <h:outputText id="tx_driver12" value="#{matrix.driver12}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;13&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}"/></f:facet>
			    <h:outputText id="tx_driver13" value="#{matrix.driver13}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;14&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}"/></f:facet>
			    <h:outputText id="tx_driver14" value="#{matrix.driver14}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;15&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}"/></f:facet>
			    <h:outputText id="tx_driver15" value="#{matrix.driver15}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;16&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}"/></f:facet>
			    <h:outputText id="tx_driver16" value="#{matrix.driver16}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;17&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}"/></f:facet>
			    <h:outputText id="tx_driver17" value="#{matrix.driver17}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;18&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}"/></f:facet>
			    <h:outputText id="tx_driver18" value="#{matrix.driver18}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;19&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}"/></f:facet>
			    <h:outputText id="tx_driver19" value="#{matrix.driver19}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;20&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}"/></f:facet>
			    <h:outputText id="tx_driver20" value="#{matrix.driver20}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;21&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}"/></f:facet>
			    <h:outputText id="tx_driver21" value="#{matrix.driver21}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;22&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}"/></f:facet>
			    <h:outputText id="tx_driver22" value="#{matrix.driver22}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;23&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}"/></f:facet>
			    <h:outputText id="tx_driver23" value="#{matrix.driver23}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;24&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}"/></f:facet>
			    <h:outputText id="tx_driver24" value="#{matrix.driver24}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;25&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}"/></f:facet>
			    <h:outputText id="tx_driver25" value="#{matrix.driver25}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;26&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}"/></f:facet>
			    <h:outputText id="tx_driver26" value="#{matrix.driver26}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;27&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}"/></f:facet>
			    <h:outputText id="tx_driver27" value="#{matrix.driver27}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;28&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}"/></f:facet>
			    <h:outputText id="tx_driver28" value="#{matrix.driver28}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;29&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}"/></f:facet>
			    <h:outputText id="tx_driver29" value="#{matrix.driver29}" />
			</rich:column>
			<rich:column width="30px" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}" >
			    <f:facet name="header"><h:outputText value="Driver&#160;30&#160;-&#160;#{matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}"/></f:facet>
			    <h:outputText id="tx_driver30" value="#{matrix.driver30}" />
			</rich:column>		
			
			<rich:column width="70px" >
				<f:facet name="header"><h:outputText value="Binary Weight"/></f:facet>
				<h:outputText id="tx_binaryWeight" value="#{matrix.binaryWeight}" />
			</rich:column>
			
			<rich:column width="30px" >
				<f:facet name="header"><h:outputText value="Active?"/></f:facet>
				<h:selectBooleanCheckbox id="tx_activeFlag" value="#{matrix.activeBooleanFlag}" disabled="true" styleClass="check"  />
			</rich:column>
		</rich:dataTable>
	</div>
	</c:if>
	</div>
	</div>
		
    <div id="table-four-bottom">
		<ul class="right">
			<li class="compare"><h:commandLink id="compareBtn" disabled="#{!transactionDetailBean.validComparedMatrixSelection}" action="#{transactionDetailBean.displayComparedMatrixCompareAction}" /></li>
			<li class="view"><h:commandLink id="viewBtn" disabled="#{!transactionDetailBean.validComparedMatrixSelection}" action="#{transactionDetailBean.displayComparedMatrixViewAction}" /></li>
			<li class="close"><h:commandLink id="btnClose" immediate="true" action="#{transactionDetailBean.cancelMatrixAnalysisAction}" /></li>
		</ul>
	</div>
	
    </div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>