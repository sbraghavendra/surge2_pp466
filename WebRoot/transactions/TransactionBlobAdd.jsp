<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="transactionBlobAddForm">
<h1><h:graphicImage id="imgTransactionBlobAdd" value="/images/headers/hdr-transaction-download.gif" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="4"><h:outputText id="actionIdText" value="#{transactionBlobBean.actionText}"/> a Transaction Download</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>	
				<td>&#160;</td>				       
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Name:</td>
				<td>
					<h:inputText id="fileNameText" value="#{transactionBlobBean.workingBlob.fileName}" 
				         label="File Name"
				         disabled="#{!transactionBlobBean.isStartAction}" 
				         style="width:650px;" />
                </td>
                <td>							
					<h:outputText id="fileNameRequiredId" rendered="#{transactionBlobBean.isProcessError}" 
						value=" *File Name is required." style="#{transactionBlobBean.isProcessError ? 'color:red' : 'color:red;visibility:hidden;display:block'}" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>
					<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       					<a4j:status id="pageInfo"  
						startText="You have initiated a file creation job for download. Please wait..." 
						stopText=""
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     					onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 			
              	</td> 	
				<td>&#160;</td>	
			</tr>

			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>		
					<h:commandLink id="downloadlink1" rendered="#{(transactionBlobBean.isFinishedAction and loginBean.showTransDetailList)}"  action="#{transactionBlobBean.goToDownloadAction}" >
						<h:outputText value="Your file is ready for download. Click here to go to the Data Utility/Trans Detail List."/></h:commandLink>
						
					<h:outputText id="downloadlink2" rendered="#{(transactionBlobBean.isFinishedAction and !loginBean.showTransDetailList)}" 
					           	value="Your file is ready for download. However, you don't have permission to access the Data Utility/Trans Detail List."/>
                </td>
                <td>&#160;</td>	
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
    						
    	<li class="ok"><a4j:commandLink id="addBtn"  action="#{transactionBlobBean.addBlobAction}" 
    				style="#{!transactionBlobBean.isStartAction? 'display:none;':'display:block;'}" ></a4j:commandLink>
    	</li>
    			
    	<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" 
    						disabled="#{!transactionBlobBean.isStartAction}"
    						action="#{transactionBlobBean.cancelBlobAction}" />
    	</li>
    	<li class="ok"><h:commandLink id="closeBtn" immediate="true" 
    						disabled="#{!transactionBlobBean.isFinishedAction}"
    						action="#{transactionBlobBean.closeBlobAction}" />
    	</li>

	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>