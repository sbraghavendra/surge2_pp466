<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() {changePanel(document.getElementById('purchaseTransForm:ddlDisplay'));});
registerEvent(window, "unload", function() { document.getElementById('purchaseTransForm:hiddenSaveLink').onclick(); });
var currentPanel = "taxDriver";
function changePanel(menu) {
	var obj = document.getElementById(currentPanel);
	if (obj) obj.style.display = "none";
	obj = document.getElementById(menu.value);
	if (obj) obj.style.display = "block";
	currentPanel = menu.value;
}
//]]>
function resetState()
{ 
  
	document.getElementById("purchaseTransForm:txnPanel:stateEdit").value = "";
}
</script>

</ui:define>

<ui:define name="body">	
		<h:form id="purchaseTransForm">
		<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
			<a4j:support event="onclick"  actionListener="#{transactionDetailBean.filterSaveAction}" />
		</a4j:commandLink>

		<h1><img id="imgTransactionProcess" alt="View Transaction" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
		<a4j:outputPanel id="msg" ><h:messages errorClass="error" /></a4j:outputPanel>
		<a4j:queue/> 
   		
   	<a4j:outputPanel id="showhidefilter">
		<c:if test="#{!transactionViewBean.togglePanelController.isHideSearchPanel}">

		<div id="top" style = "width:1170px">
		<div id="table-one">
		
		<div id="table-four-bottom" style="padding-left: 10px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;height: 30px">
			<tr>
				<td style="width: 86%"><h:outputLabel id="selectionlabel" value="#{transactionDetailBean.actionText}"/> Section:
					<h:selectOneMenu id="ddlDisplay" style="width:130px;font-size:11px;" value="#{transactionViewBean.selectedDisplay}" 
							valueChangeListener="#{transactionViewBean.displaySelectionChanged}">
							<f:selectItem itemValue="taxDriver" itemLabel="Drivers"/>
							<f:selectItem itemValue="transaction" itemLabel="Transaction"/>
							<f:selectItem itemValue="accounting" itemLabel="Accounting"/>
							<f:selectItem itemValue="vendor" itemLabel="Vendor"/>
							<f:selectItem itemValue="documentation" itemLabel="Documentation"/>
							<f:selectItem itemValue="locations" itemLabel="Locations"/>
							<f:selectItem itemValue="userFields" itemLabel="User Fields"/>
							<c:if test="#{transactionDetailBean.isViewScreen}">
							   <f:selectItem itemValue="processing" itemLabel="Processing"/>
							</c:if>
							<c:if test="#{transactionDetailBean.isViewScreen}">
							    <f:selectItem itemValue="results" itemLabel="Results"/>
							</c:if>
							<a4j:support event="onchange" reRender="showhidefilter,msg"  />
					
					</h:selectOneMenu>
				</td>
				
				<td style="width: 7%" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
					 reRender="showhidefilter" actionListener="#{transactionViewBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
				<td style="width: 7%" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
  					 reRender="showhidefilter" actionListener="#{transactionViewBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
			</tr>
			</table>
			
		</div>
		<div id="table-one-content" style="height:418px;">
		<table cellpadding="0" cellspacing="0" width="100%" id="transactionsHeader" class="ruler">
			<thead>
				<tr>
					<td colspan="5"><h:outputText id="lbltransaction" value="#{transactionDetailBean.actionText}"/> a Purchase Transaction</td>
					<td colspan="4" style="text-align:right;">Transaction ID:&#160;<h:outputText value="#{transactionViewBean.selectedTransaction.purchtransId}"/></td>
				</tr>
			</thead>
		  </table>
		 </div> 	
		<div id="table-one-content" style="height:auto;">
		<c:if test="#{transactionViewBean.selectedDisplay == 'taxDriver'}">
			<a4j:outputPanel id="selTaxDrPanel" layout="block">
				<a4j:include id = "driIncludeid" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_drivers_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'accounting'}">
			<a4j:outputPanel id="selAccPanel" ajaxRendered="true" layout="block">
				<a4j:include id = "acctIncludeid" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_account_info.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'transaction'}">
			<a4j:outputPanel id="selTxnPanel" ajaxRendered="true" layout="block">
				<a4j:include id = "transIncludeId" ajaxRendered="true"  viewId="/WEB-INF/view/components/transactiondetail_txn_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'vendor'}">
			<a4j:outputPanel id="selVendorPanel" ajaxRendered="true" layout="block">
				<a4j:include id = "vendorIncludeId" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_vendor_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'documentation'}">
			<a4j:outputPanel id="seleDocPanel" ajaxRendered="true" layout="block">
				<a4j:include id ="docIncludeid" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_doc_info.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'locations'}">
			<a4j:outputPanel id="selLocPanel" ajaxRendered="true" layout="block">
				<a4j:include id = "locIncludeid" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_ship_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'userFields'}">
			<a4j:outputPanel id="selUserPanel"  ajaxRendered="true" layout="block">
				<a4j:include id = "userFieldsIncludeId" ajaxRendered="true" viewId="/WEB-INF/view/components/transactiondetail_user_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'processing'}">
			<a4j:outputPanel id="selProcPanel" layout="block">
				<a4j:include id = "prcIncludeId" viewId="/WEB-INF/view/components/transactiondetail_tax_processing_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailBean.updatedTransaction}" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'results'}">
			<a4j:outputPanel id="selResPanel" ajaxRendered="true" layout="block">
				<a4j:include id = "resultsIncludeId"  viewId="/WEB-INF/view/components/purchase_result_panel.xhtml">
					<ui:param name="trDetail" value= "#{transactionDetailBean.updatedTransaction}" />
					<ui:param name="jurisdiction" value="#{transactionViewBean.currentJurisdiction}"/>
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		</div> <!-- table-one-content -->
		<div id="table-four-bottom">
		<c:if test =  "#{!transactionDetailBean.isViewScreen}">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk"   action="#{transactionViewBean.updateAction}" /></li>
			<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{transactionDetailBean.cancelAction}" /></li>
		</ul>
		</c:if>
		<c:if test =  "#{transactionDetailBean.isViewScreen}">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk"   rendered="#{transactionDetailBean.isViewScreen}"  action="#{transactionDetailBean.cancelAction}" /></li>
		</ul>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay != 'taxDriver'}">
		  <p style = "color:red">Goods &amp; Services Matrix Drivers are Red</p>
		</c:if>
		<c:if test="#{transactionViewBean.selectedDisplay == 'taxDriver'}">
		  <p style = "color:red">Mandatory Drivers are Red</p>
		</c:if>
		</div>
		</div> <!-- table-one -->
		</div> <!-- top -->
		
	</c:if>
	</a4j:outputPanel>
	</h:form>
</ui:define>
</ui:composition>
</html>
