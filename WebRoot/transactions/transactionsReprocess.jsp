<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { displayWarning(); } );
registerEvent(window, "load", function() { displayIncomplete(); } );

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function displayIncomplete() {
 var incomplete = document.getElementById('displayIncomplete');
 var val = parseInt(incomplete.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('incomplete');
 }
}

//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="displayWarning" value="#{transactionDetailBean.displayWarning}"/>
<h:inputHidden id="displayIncomplete" value="#{transactionDetailBean.displayIncomplete}"/>

<h:form id="transModifyForm">

<h1><img id="imgTransactionProcess" alt="Modify Transaction" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
<div id="top" style = "width:970px">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				<td colspan="4">
					<h:outputText rendered="#{transactionDetailBean.isHoldAction}" value="Hold Transaction"/>
					<h:outputText rendered="#{!transactionDetailBean.isHoldAction and !transactionDetailBean.heldTransaction}" value="Reprocess Transaction&#160;-&#160;#{transactionDetailBean.currentTransaction.processStatus}"/>
					<h:outputText rendered="#{!transactionDetailBean.isHoldAction and transactionDetailBean.heldTransaction}" value="Accept/Override Held Transaction"/>
				</td>
				<td colspan="4" style="text-align:right;">Transaction ID:&#160;<h:outputText value="#{transactionDetailBean.currentTransaction.purchtransId}"/></td>
			</tr>
		</thead>
		<tbody>
			<tr><th colspan="8">Drivers</th></tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm" style="width:138px;"> Country: </td> 
				<td class="column-input">
					<h:selectOneMenu id="country"
						value="#{transactionDetailBean.currentTransaction.transactionCountryCode}"
						disabled="true">
						<f:selectItems value="#{transactionDetailBean.countryMenuItems}"/>
					</h:selectOneMenu>
				</td>
				<td class="column-description-wild-null">&#160;</td> 
				<td class="column-label-gsm">&#160;</td>
				<td class="column-input-small">&#160;</td> 
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm" style="width:138px;"> State: </td> 
				<td class="column-input">
					<h:selectOneMenu id="state"
						value="#{transactionDetailBean.currentTransaction.transactionStateCode}"
						disabled="true">
						<f:selectItems value="#{transactionDetailBean.stateMenuItemsModifyOriginal}"/>
					</h:selectOneMenu>
				</td>
				<td class="column-description-wild-null">&#160;</td> 
				<td class="column-label-gsm">&#160;</td>
				<td class="column-input-small">&#160;</td> 
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	
	<h:panelGrid binding="#{transactionDetailBean.taxDriverViewPanel}" width="100%" class="panel-ruler"/>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr><th colspan="8">Details</th></tr>
			
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm"><h:outputText id="lblTaxcodeCode" value="#{matrixCommonBean.transactionPropertyMap['taxcodeCode'].description}:"/></td>
				<td class="column-input-small"><h:inputText id="txtTaxcodeCode" value="#{transactionDetailBean.currentTransaction.taxcodeCode}" disabled="true"/></td>
				<td class="column-description-wild-null" ><h:inputText id="txtTaxcodeCodeDesc" value="#{cacheManager.taxCodeMap[transactionDetailBean.currentTransaction.taxcodeCode].description}" disabled="true"/></td>
				<td class="column-label-gsm">&#160;</td>
				<td class="column-input-small">
					&#160;
				</td> 
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			
		  	
			 <tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm"><h:outputText id="lblTaxability" value="Taxability:"/></td>
				<c:if test="#{empty cacheManager.listCodeMap['TCTYPE'][transactionDetailBean.selectedTransaction.stateTaxcodeTypeCode]}">
					<td class="column-input-small"><h:inputText id="txtTaxability" value="#{transactionDetailBean.selectedTransaction.stateTaxcodeTypeCode}" disabled="true"/></td>
				</c:if>
				
				<c:if test="#{not empty cacheManager.listCodeMap['TCTYPE'][transactionDetailBean.selectedTransaction.stateTaxcodeTypeCode]}">
					<td class="column-input-small"><h:inputText id="txtTaxability" value="#{cacheManager.listCodeMap['TCTYPE'][transactionDetailBean.selectedTransaction.stateTaxcodeTypeCode].description}" disabled="true"/></td>
				</c:if>
				<td class="column-description-wild-null">&#160;</td>
				
				<td class="column-label-gsm"><h:outputText id="lblGlLineItemDistAmt" value="#{matrixCommonBean.transactionPropertyMap['glLineItmDistAmt'].description}:"/></td>
				<td class="column-input-small">
					<h:inputText id="txtGlLineItemDistAmt" value="#{transactionDetailBean.currentTransaction.glLineItmDistAmt}" disabled="true">
						<f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" pattern="#0.00"/>
					</h:inputText>
				</td>
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm"><h:outputText id="lblTaxType" value="Override TaxType:"/></td>
				<c:if test="#{not empty cacheManager.listCodeMap['TAXTYPE'][transactionDetailBean.selectedTransaction.stateTaxtypeCode]}">
					<td class="column-input-small"><h:inputText id="txtTaxType" value="#{cacheManager.listCodeMap['TAXTYPE'][transactionDetailBean.selectedTransaction.stateTaxtypeCode].description}" disabled="true"/></td>
				</c:if>
				<c:if test="#{empty cacheManager.listCodeMap['TAXTYPE'][transactionDetailBean.selectedTransaction.stateTaxtypeCode]}">
					<td class="column-input-small"><h:inputText id="txtTaxType" value="#{transactionDetailBean.selectedTransaction.stateTaxtypeCode}" disabled="true"/></td>
				</c:if>
				<td >
				<div id="embedded-table">
				<h:panelGrid id="ratetypePanel" columns="2" cellpadding="2" cellspacing="0">	
					<h:outputText id="lblRateType" value="RateType:"/>
					<c:if test="#{not empty cacheManager.listCodeMap['RATETYPE'][transactionDetailBean.selectedTransaction.stateRatetypeCode]}">
						<h:inputText id="txtRateType" value="#{cacheManager.listCodeMap['RATETYPE'][transactionDetailBean.selectedTransaction.stateRatetypeCode].description}" disabled="true"/>
					</c:if>
					<c:if test="#{empty cacheManager.listCodeMap['RATETYPE'][transactionDetailBean.selectedTransaction.stateRatetypeCode]}">
						<h:inputText id="txtRateType" value="#{transactionDetailBean.selectedTransaction.stateRatetypeCode}" disabled="true"/>
					</c:if>
				</h:panelGrid>
				</div>

				</td>
				
				<td class="column-label-gsm"><h:outputText id="lblInvLineAmt" value="#{matrixCommonBean.transactionPropertyMap['invoiceLineAmt'].description}:"/></td>
				<td class="column-input-small">
					<h:inputText id="txtInvLineAmt" value="#{transactionDetailBean.currentTransaction.invoiceLineAmt}" disabled="true">
						<f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"  pattern="#0.00"/>
					</h:inputText>
				</td>
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			
			
			 <tr>
 				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm"><h:outputText id="lblSpecialRate" value="Special Rate:"/></td>
				<td class="column-input-small"><h:inputText id="txtSpecialRate" value="#{transactionDetailBean.existedTaxCodeDetail.specialRate}" disabled="true"/></td>
				<td class="column-description-wild-null">&#160;</td>
		
				
				<td class="column-label-gsm"><h:outputText id="lblInvLineTax" value="#{matrixCommonBean.transactionPropertyMap['invoiceLineTax'].description}:"/></td>
				<td class="column-input-small">
					<h:inputText id="txtInvLineTax" value="#{transactionDetailBean.currentTransaction.invoiceLineTax}" disabled="true">
						<f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" pattern="#0.00"/>
					</h:inputText>
				</td>
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		
			<tr>
				<td class="column-spacer">&#160;</td>
		
				<td class="column-label-gsm"><h:outputText id="lblTbCalcTaxAmt" value="#{matrixCommonBean.transactionPropertyMap['tbCalcTaxAmt'].description}:"/></td>
				<td class="column-input-small">
					<h:inputText id="txtTbCalcTaxAmt" value="#{transactionDetailBean.currentTransaction.tbCalcTaxAmt}" disabled="true">
						<f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" pattern="#0.00"/>
					</h:inputText>
				</td>
		
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-label-gsm">&#160;</td>
				<td class="column-input-small"> &#160; </td>
				
				
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm"><h:outputText id="lblComments" value="#{matrixCommonBean.transactionPropertyMap['comments'].description}:"/></td>
				<td colspan="2" style = "width:286px" ><h:inputText id="txtComments" disabled="true" value="#{transactionDetailBean.currentTransaction.comments}" style="width:300px;"/></td>
				<td class="column-label-gsm"><h:outputText id="lblGlExtractFlag" value="#{matrixCommonBean.transactionPropertyMap['glExtractFlag'].description}:"/></td>
				<td class="column-input-small"><h:inputText id="txtGlExtractFlag" value="#{transactionDetailBean.currentTransaction.glExtractFlag}" disabled="true"/></td>
				<td class="column-description-wild-null">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>  
			
		</tbody>
	</table>
	<h:panelGroup rendered="#{!transactionDetailBean.heldTransaction}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-input-small">Jurisdiction:</td>
				<td colspan="5">
				<div id="embedded-table">
				<h:panelGrid id="juris" columns="7" cellpadding="2" cellspacing="0">	
					<h:outputText value="GeoCode:"/>
					<h:outputText value="City:"/>
					<h:outputText value="County:"/>
					<h:outputText value="State:"/>
					<h:outputText value="Country:"/>
					<h:outputText value="Zip Code:"/>
					<h:outputText value=" "/>					
					<h:inputText id="geocode" disabled="true" label="GeoCode" value="#{transactionDetailBean.currentJurisdiction.geocode}"/>
					<h:inputText id="city" disabled="true" label="City" value="#{transactionDetailBean.currentJurisdiction.city}"/>
					<h:inputText id="county" disabled="true" label="County" value="#{transactionDetailBean.currentJurisdiction.county}"/>
					<h:panelGroup>
						<h:selectOneMenu id="state_menu" disabled="true" label="State" value="#{transactionDetailBean.currentJurisdiction.state}">
							<f:selectItems value="#{transactionDetailBean.stateJurisMenuItemsModifyOriginal}"/>
						</h:selectOneMenu>
					</h:panelGroup>
					<h:panelGroup>
						<h:selectOneMenu id="country_menu" disabled="true" label="Country" value="#{transactionDetailBean.currentJurisdiction.country}">
							<f:selectItems value="#{transactionDetailBean.countryMenuItems}"/>
						</h:selectOneMenu>
					</h:panelGroup>
					<h:inputText id="zip" disabled="true" label="Zip Code" value="#{transactionDetailBean.currentJurisdiction.zip}"/>					
				</h:panelGrid>
				</div>
				</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	</h:panelGroup>
	 
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td colspan="3">
					<h:outputText value="#{transactionDetailBean.currentTransactionIndex} of #{transactionDetailBean.totalSelected} Transactions Selected"/>
				</td>
				<td colspan="3">
					<h:selectBooleanCheckbox id="applyToAll" styleClass="check"
						title="Apply to All Remaining Transactions"
						value="#{transactionDetailBean.applyToAll}">
						<a4j:support event="onchange" action="#{transactionDetailBean.applyToAllChanged}"/>
					</h:selectBooleanCheckbox>
					<h:outputText value="Apply to All Remaining Transactions"/>
				</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
	 <h:panelGroup rendered = "#{transactionDetailBean.isHoldAction}" >
	   	<li class="ok"><h:commandLink id="btnOkhold" action="#{transactionDetailBean.updateHoldAction}" 
								onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
	  </h:panelGroup>
	  <h:panelGroup rendered = "#{!transactionDetailBean.isHoldAction}" >
		<li class="ok"><h:commandLink id="btnOkreprocess" action="#{transactionDetailBean.updateFromReprocessAction}" 
								onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
	  </h:panelGroup>
		<li class="view115"><a4j:commandLink id="btnView" immediate="true" action="#{transactionDetailBean.displayViewFromUpdateAction}"/></li>
		<li class="cancel-all"><a4j:commandLink id="btnCancelAll" immediate="true" action="#{transactionDetailBean.cancelAll}"/></li> 
		<li class="cancel"><a4j:commandLink id="btnCancel" immediate="true" action="#{transactionDetailBean.nextTransactionActionForPage}"/></li>
	</ul>
	</div>
	</div>
</div>

</h:form>

<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>


<rich:modalPanel id="warning" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Warning" />
	</f:facet>
	<h:form id="warningForm">
		
		<h:panelGrid columns="1" style="margin:10px;">
		
			<h:outputText value="Do you want to override the TaxCode with the value specified?"/>

			<rich:spacer height="10px"/>

			<h:panelGroup style="text-align:center;">
				<a4j:commandButton value="Yes" immediate="true"
						action="#{transactionDetailBean.updateAction}"
						oncomplete="Richfaces.hideModalPanel('warning'); return false;" />
				<rich:spacer width="20px"/>
				<a4j:commandButton value="No" immediate="true"
						action="#{transactionDetailBean.updateNoOverrideAction}"
						oncomplete="Richfaces.hideModalPanel('warning'); return false;" />
				<rich:spacer width="20px"/>
				<a4j:commandButton immediate="true" value="Cancel" 
						action="#{transactionDetailBean.cancelWarning}" 
						oncomplete="Richfaces.hideModalPanel('warning'); return false;" />
			</h:panelGroup>
		
		</h:panelGrid>
		
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="incomplete" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Incomplete Information" />
	</f:facet>
	<h:form id="incompleteForm">
		
		<h:panelGrid columns="1" style="margin:10px;">
		
			<h:outputText value="TaxCode or Jurisdiction is invalid."/>

			<rich:spacer height="10px"/>

			<h:panelGroup style="text-align:center;">
				<a4j:commandButton immediate="true" value="Ok" 
						action="#{transactionDetailBean.cancelIncomplete}" 
						oncomplete="Richfaces.hideModalPanel('incomplete'); return false;" />
			</h:panelGroup>
		
		</h:panelGrid>
		
	</h:form>
</rich:modalPanel>

</ui:define>

</ui:composition>

</html>