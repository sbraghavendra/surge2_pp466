<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('advancedSortForm:columnItemListId', 'selectedColumnRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('advancedSortForm:sortItemListId', 'selectedSortRowIndex'); } ); 
//]]>
</script>
</ui:define>

<ui:define name="body" >
<h:inputHidden id="selectedColumnRowIndex" value="#{advancedSortBean.selectedColumnRowIndex}" />
<h:inputHidden id="selectedSortRowIndex" value="#{advancedSortBean.selectedSortRowIndex}" />

<h:form id="advancedSortForm">
<h1><h:graphicImage id="imgAdvancedSort" alt="Advanced Sort" value="/images/headers/hdr-advanced-sort-selection.png" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content">
	
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td colspan="6">&#160;</td>
			</tr>	
			<tr>
				<td width="50%">
					<div class="scrollContainer" >
					<div class="scrollInner" style="height: 600px">
            		<rich:dataTable rowClasses="odd-row,even-row"  id="columnItemListId" styleClass="GridContent" 
						value="#{advancedSortBean.columnItemList}" var="columnItem"  rows="300">
						
             
            		<a4j:support id="clk" event="onRowClick" 
            			onsubmit="selectRow('advancedSortForm:columnItemListId',this);" 
            			actionListener="#{advancedSortBean.selectedColumnChanged}" 
            			/>                    
             
            			<rich:column id="columnItemlabel" width="250px" sortBy="#{columnItem.label}" sortOrder="ASCENDING" >
							<f:facet name="header"><h:outputText styleClass="headerText" value="Columns Available for Sorting&#160;&#160;&#160;"/></f:facet>
							<h:outputText id="txtColumnItemLabel" value="#{columnItem.label}" />
						</rich:column>
						
						<rich:column id="columnItemascending" width="114px" >
							<f:facet name="header"><h:outputText styleClass="headerText" value="Ascending&#160;"/></f:facet>
							<h:selectBooleanCheckbox id="chkColumnItemAscending" styleClass="check" value="#{columnItem.ascending}" />
						</rich:column>	
			
					</rich:dataTable>	
					</div>
					</div>
				</td>
				<td>&#160;&#160;&#160;&#160;</td>
				<td>
					<h:commandButton id="btnAssign" image="/images/actionbuttons/NCSTSAssign.JPG" type="button" style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
						action="#{advancedSortBean.assignSortColumn}" reRender="columnItemListId,sortItemListId" /><br/>&#160;<br/>
					<h:commandButton id="btnRemove" image="/images/actionbuttons/NCSTSRemove.JPG" type="button" 
						style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
						action="#{advancedSortBean.removeSortColumn}" reRender="columnItemListId,sortItemListId" /><br/>&#160;<br/>
					<h:commandButton id="btnRemoveAll" image="/images/actionbuttons/NCSTSRemoveAll.JPG" type="button" style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
						action="#{advancedSortBean.removeAllSortColumn}" reRender="columnItemListId,sortItemListId"
						onclick="return confirm('Are you sure you want to Remove All?')"/>
				</td>
				<td>&#160;&#160;&#160;&#160;</td>
				<td width="50%">
					<div class="scrollContainer" >
					<div class="scrollInner" style="height: 600px">
            		<rich:dataTable rowClasses="odd-row,even-row" id="sortItemListId" styleClass="GridContent"
						value="#{advancedSortBean.sortItemList}" var="sortItem" rows="300">
      
            		<a4j:support id="clk1" event="onRowClick" 
            			onsubmit="selectRow('advancedSortForm:sortItemListId',this);" 
            			actionListener="#{advancedSortBean.selectedSortChanged}" 
            		 	/>
						 																
						<rich:column id="sortItemlabel" width="250px" >
							<f:facet name="header"><h:outputText styleClass="headerText" value="Sort Columns&#160;&#160;&#160;"/></f:facet>
							<h:outputText id="txtSortItemLabel" value="#{sortItem.label}" />
						</rich:column>
						
						<rich:column id="sortItemascending" width="114px" >
							<f:facet name="header"><h:outputText styleClass="headerText" value="Ascending&#160;"/></f:facet>
							<h:selectBooleanCheckbox id="chkAscending" styleClass="check" value="#{sortItem.ascending}" />
						</rich:column>
		
					</rich:dataTable>	
					</div>
					</div>
				</td>
				<td >&#160;</td>
				
			</tr>
		</tbody>
		</table>
			
	</div>	
	<div id="table-one-bottom">
		<ul class="right">
     		<li class="ok"><h:commandLink id="okAction" action="#{advancedSortBean.okAdvancedSortAction}" ></h:commandLink></li>
     		<li class="cancel"><h:commandLink id="cancelAction" action="#{advancedSortBean.cancelAdvancedSortAction}" /></li>
    	</ul>
	</div>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>