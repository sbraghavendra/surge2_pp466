<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetailList', 'aMDetailListRowIndex'); } );
registerEvent(window, "unload", function() { document.getElementById('filterForm:hiddenSaveLink').onclick(); });

var currentPanel = "basicinformationFilter";
function changePanel(menu) {
	var obj = document.getElementById(currentPanel);
	if (obj) obj.style.display = "none";
	obj = document.getElementById(menu.value);
	if (obj) obj.style.display = "block";
	currentPanel = menu.value;
}

function changeDisplayPanel(objId) {
	var obj = document.getElementById(objId);
	
	if (obj) obj.style.display = "none";
	obj.style.display = "block";
	
	if (obj) obj.style.display = "block";
	obj.style.display = "none";
}

function insertDateTime(objId) {
	var obj = document.getElementById(objId);
	if (obj) {
		var time = new Date();
		obj.value = obj.value +
			(time.getMonth() + 1) + '/' +
			time.getDate() + '/' +
			time.getFullYear() + ' ' +
			time.getHours() + ':' +
			time.getMinutes() + ':' +
			time.getSeconds() + ' - ';  
	}
}

function toggleApplyAll(txtId, chkId) {
	var txt = document.getElementById(txtId);
	var chk = document.getElementById(chkId);
	if (chk) {
		chk.disabled = (!txt || (txt.value == ''));
	}
}

function resetMinimumDrivers(id) {
	var obj = document.getElementById(id).getElementsByTagName('input');
	for (var i = 0; i < obj.length; i++) {
		if (!obj[i].disabled && obj[i].checked) {
			obj[i].checked = false;
		}
	}
}

function resetState()
{ 
    document.getElementById("filterForm:state").value = "";
}

function resetEntity2()
{ 
	var obj = document.getElementById("filterForm:entityMenuMain");
	document.getElementById("filterForm:transentityMenuMain").value = obj.value;
}

function resetEntity1()
{ 
	var obj = document.getElementById("filterForm:transentityMenuMain");
	document.getElementById("filterForm:entityMenuMain").value = obj.value;
}

function resetFromGlDate()
{ 
	var obj = document.getElementById("filterForm:fromGlTranDate");
	document.getElementById("filterForm:fromGlDate").value = obj.value;
}


//]]>
</script>

</ui:define>

<ui:define name="body">	
	<h:inputHidden id="aMDetailListRowIndex" value="#{transactionDetailSaleSearchBean.selectedRowIndex}"/>
	<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" ><a4j:support event="onclick" immediate="true" actionListener="#{transactionDetailSaleSearchBean.filterSaveAction}" /></a4j:commandLink>

	<h:form id="filterForm">	

		<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-view-sale-transactions.png" width="250" height="19" /></h1>
		<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
			<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
	  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{transactionDetailSaleSearchBean.togglePanelController.toggleHideSearchPanel}" >
	  			<h:outputText value="#{(transactionDetailSaleSearchBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
	  		</a4j:commandLink>					
		</div> 
		
		<a4j:outputPanel id="msg" ><h:messages errorClass="error" /></a4j:outputPanel>
	
   		<a4j:queue/> 
   		
   	<a4j:outputPanel id="showhidefilter">
		<c:if test="#{!transactionDetailSaleSearchBean.togglePanelController.isHideSearchPanel}">

		<div id="top" style="width:1060px">
		<div id="table-one">
		<div id="table-one-top" style="padding-left: 10px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
			<tr>
				<td><h:outputLabel id="selectionlabel" value="Selection Filter Section: "  />
					<h:selectOneMenu id="ddlDisplay" style="width:130px;font-size:11px;" value="#{transactionDetailSaleSearchBean.selectedDisplay}" 
							valueChangeListener="#{transactionDetailSaleSearchBean.displaySelectionChanged}">
							<f:selectItem itemValue="basicinformationFilter" itemLabel="Basic Information"/>	
							<f:selectItem itemValue="customerFilter" itemLabel="Customer"/>	
							<f:selectItem itemValue="invoiceFilter" itemLabel="Invoice"/>
							<f:selectItem itemValue="locationsFilter" itemLabel="Locations"/>
							<f:selectItem itemValue="taxamountsFilter" itemLabel="Tax Amounts"/>
							<f:selectItem itemValue="transactionFilter" itemLabel="Transaction"/>
							<f:selectItem itemValue="userfieldsFilter" itemLabel="User Fields"/>
							
							<a4j:support event="onchange" reRender="showhidefilter"  />
					
					</h:selectOneMenu>
				</td>
				
				<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
					 reRender="showhidefilter" actionListener="#{transactionDetailSaleSearchBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
				<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
  					 reRender="showhidefilter" actionListener="#{transactionDetailSaleSearchBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
				<td style="width: 50%"></td>
				<td style="padding-right: 10px;" ><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>
			</tr>
			</table>
		</div>
		

		<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'filterForm:searchBtn')" >
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'basicinformationFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<ui:include ajaxRendered="true" src="/WEB-INF/view/components/sale_basicinformation_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</ui:include>
			</a4j:outputPanel>
		</c:if>
		
		<!--  
		<div id="basicinformationFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_basicinformation_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>			
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'customerFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_customer_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<!--
		<div id="customerFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_customer_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>	
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'invoiceFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_invoice_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<!--
		<div id="invoiceFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_invoice_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'locationsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_locations_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		
		<!--
		<div id="locationsFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_locations_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>				
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'taxamountsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_taxamounts_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<!--
		<div id="taxamountsFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_taxamounts_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>	
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'transactionFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_transaction_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<!--
		<div id="transactionFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_transaction_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>	
			</ui:include>
		</div>
		-->
		
		<c:if test="#{transactionDetailSaleSearchBean.selectedDisplay == 'userfieldsFilter'}">
			<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/sale_userfields_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}" />
					<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}" />
					<ui:param name="readonly" value="false" />
				</a4j:include>
			</a4j:outputPanel>
		</c:if>
		<!--
		<div id="userfieldsFilter" style="display:none;">
			<ui:include src="/WEB-INF/view/components/sale_userfields_panel.xhtml">
				<ui:param name="trDetail" value="#{transactionDetailSaleSearchBean.saleTransactionFilter}"/>
				<ui:param name="beanDetail" value="#{transactionDetailSaleSearchBean}"/>
				<ui:param name="readonly" value="false"/>
				
			</ui:include>
		</div>
		-->

		</div> <!-- table-one-content -->
		
		<div id="table-one-bottom">
		<ul class="right">
			<li class="clear">
				<a4j:commandLink id="btnClear" action="#{transactionDetailSaleSearchBean.resetSearchFilter}" reRender="showhidefilter" >
				</a4j:commandLink>
			</li>
			<li class="search">
				<a4j:commandLink id="searchBtn" action="#{transactionDetailSaleSearchBean.listTransactions}" 
								reRender="showhidefilter,aMDetailList,trScroll,pageInfo,viewBtn,taxMessageId" oncomplete="initScrollingTables();"  >
				</a4j:commandLink>
			</li>

		</ul>
		</div> <!-- table-one-bottom -->
		
		</div> <!-- table-one -->
		</div> <!-- top -->
		
	</c:if>
	</a4j:outputPanel>
<!-- 			
		<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" /> --> 
    	
	</h:form>

	<div class="wrapper">
	<span class="block-right">&#160;</span> 
	<span class="block-left tab">
	<img src="../images/containers/STSView-transactions-open.gif"/>
	</span>
	</div>

	<h:form id="transForm">
		<!-- default queue for the form is created -->
   		<a4j:queue/>
   		
		<div id="bottom">
		<div id="table-four" style="vertical-align: bottom;">
		<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>
					<td width="200">
						<h:selectBooleanCheckbox id="chkSelectAll" value="#{transactionDetailSaleSearchBean.allSelected}" styleClass="check" 
							disabled="#{!transactionDetailSaleSearchBean.selectAllEnabled}">
								<a4j:support event="onclick" reRender="aMDetailList,pageInfo,selectAllWarning"
									actionListener="#{transactionDetailSaleSearchBean.selectAllChange}"
								oncomplete="initScrollingTables();"  />
								
						</h:selectBooleanCheckbox>		
						<h:outputText value="#{transactionDetailSaleSearchBean.selectAllCaption}"/>				
					</td>
					<td align="center" >
						<a4j:status id="pageInfo"
							startText="Request being processed..." 
							stopText="#{transactionDetailSaleSearchBean.transactionBillDataModel.pageDescription}"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     						
							<h:outputText id="selectAllWarning" style="color:red" value="#{transactionDetailSaleSearchBean.selectAllWarning}" />
							
					</td>
					<td align="center" width="270">
						<h:outputLabel id="taxMessageId" value="#{transactionDetailSaleSearchBean.validTaxMessage}" />
					</td>
					<td align="right" width="150">
						<table cellpadding="0" cellspacing="0" style="height: 22px;">
							<tr>
								<td>
									<rich:spacer width="20px"/>
									<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{transactionDetailSaleSearchBean.displayAllFields}" styleClass="check">
									<a4j:support event="onclick" reRender="aMDetailList,pageInfo"
										actionListener="#{transactionDetailSaleSearchBean.displayChange}"
										oncomplete="initScrollingTables();"/>
									</h:selectBooleanCheckbox>
									<h:outputText value="Display All Fields"/>
								</td>
							</tr>
						</table>
					</td>
					<td style="width:20px;">&#160;</td>
					</tr>
				</tbody>
			</table>
		</div>


		<ui:include src="/WEB-INF/view/components/transactiondetailsale_table.xhtml">
			<ui:param name="formName" value="transForm"/>
			<ui:param name="bean" value="#{transactionDetailSaleSearchBean}"/>
			<ui:param name="dataModel" value="#{transactionDetailSaleSearchBean.transactionBillDataModel}"/>
			<ui:param name="singleSelect" value="true"/>
			<ui:param name="multiSelect" value="#{transactionDetailSaleSearchBean.multiSelect}"/>
			<ui:param name="doubleClick" value="true"/>
			<ui:param name="selectReRender" value="viewBtn,taxMessageId,reprocessBtn"/>
			<ui:param name="scrollReRender" value="pageInfo"/>
		</ui:include>
	
		
		<div id="table-four-bottom">
		<ul class="right">
<!-- PP-394 
			<li class="reprocess"><h:commandLink id="reprocessBtn" 
			    style="#{(!transactionDetailSaleSearchBean.currentUser.viewOnlyBooleanFlag and transactionDetailSaleSearchBean.validSelection) ? 'display:block;':'display:none;'}"
			    disabled="#{!transactionDetailSaleSearchBean.validSelection}" action="#{transactionDetailSaleSearchBean.reprocess}" /></li>
-->
			<li class="view"><h:commandLink id="viewBtn" disabled="#{!transactionDetailSaleSearchBean.validSelection}" action="#{transactionDetailSaleSearchBean.displayViewAction}" /></li>
		</ul>
		</div>
		</div>
		</div>
	</h:form>
	
<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="filterForm:jurisInput,filterForm:jurisInputShipfrom"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandler1}"/>
	<ui:param name="popupName" value="searchJurisdiction1"/>
	<ui:param name="popupForm" value="searchJurisdictionForm1"/>
	<ui:param name="jurisId" value="jurisId1"/>
	<ui:param name="reRender" value="filterForm:jurisInput1,filterForm:jurisInputShipto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandler2}"/>
	<ui:param name="popupName" value="searchJurisdiction2"/>
	<ui:param name="popupForm" value="searchJurisdictionForm2"/>
	<ui:param name="jurisId" value="jurisId2"/>
	<ui:param name="reRender" value="filterForm:jurisInput2,filterForm:jurisdictionId"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandler}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionID"/>
	<ui:param name="popupForm" value="searchJurisdictionFormID"/>
	<ui:param name="jurisId" value="ratesJurisId"/>
	<ui:param name="reRender" value="filterForm:jurisdictionId"/>
</ui:include>


<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdictionShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipto"/>
	<ui:param name="jurisId" value="jurisIdShipto"/>
	<ui:param name="reRender" value="filterForm:jurisInputShipto,filterForm:jurisInput1,filterForm:jurisdictionIdShipto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerTtlxfr}"/>
	<ui:param name="popupName" value="searchJurisdictionTtlxfr"/>
	<ui:param name="popupForm" value="searchJurisdictionFormTtlxfr"/>
	<ui:param name="jurisId" value="jurisIdTtlxfr"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerShipfrom}"/>
	<ui:param name="popupName" value="searchJurisdictionShipfrom"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipfrom"/>
	<ui:param name="jurisId" value="jurisIdShipfrom"/>
	<ui:param name="reRender" value="filterForm:jurisInputShipfrom,filterForm:jurisInput,filterForm:jurisdictionIdShipfrom"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerOrdrorgn}"/>
	<ui:param name="popupName" value="searchJurisdictionOrdrorgn"/>
	<ui:param name="popupForm" value="searchJurisdictionFormOrdrorgn"/>
	<ui:param name="jurisId" value="jurisIdOrdrorgn"/>
	<ui:param name="reRender" value="filterForm:jurisInputOrdrorgn,filterForm:jurisdictionIdOrdrorgn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerOrdracpt}"/>
	<ui:param name="popupName" value="searchJurisdictionOrdracpt"/>
	<ui:param name="popupForm" value="searchJurisdictionFormOrdracpt"/>
	<ui:param name="jurisId" value="jurisIdOrdracpt"/>
	<ui:param name="reRender" value="filterForm:jurisInputOrdracpt,filterForm:jurisdictionIdOrdracpt"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerFirstuse}"/>
	<ui:param name="popupName" value="searchJurisdictionFirstuse"/>
	<ui:param name="popupForm" value="searchJurisdictionFormFirstuse"/>
	<ui:param name="jurisId" value="jurisIdFirstuse"/>
	<ui:param name="reRender" value="filterForm:jurisInputFirstuse,filterForm:jurisdictionIdFirstuse"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.filterHandlerBillto}"/>
	<ui:param name="popupName" value="searchJurisdictionBillto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormBillto"/>
	<ui:param name="jurisId" value="jurisIdBillto"/>
	<ui:param name="reRender" value="filterForm:jurisInputBillto,filterForm:jurisdictionIdBillto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerShipto}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDShipto"/>
	<ui:param name="jurisId" value="ratesJurisIdShipto"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdShipto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerShipfrom}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDShipfrom"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDShipfrom"/>
	<ui:param name="jurisId" value="ratesJurisIdShipfrom"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdShipfrom"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerOrdrorgn}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDOrdrorgn"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDOrdrorgn"/>
	<ui:param name="jurisId" value="ratesJurisIdOrdrorgn"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdOrdrorgn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerOrdracpt}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDOrdracpt"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDOrdracpt"/>
	<ui:param name="jurisId" value="ratesJurisIdOrdracpt"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdOrdracpt"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerFirstuse}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDFirstuse"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDFirstuse"/>
	<ui:param name="jurisId" value="ratesJurisIdFirstuse"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdFirstuse"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.jurisdictionHandlerBillto}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionIDBillto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormIDBillto"/>
	<ui:param name="jurisId" value="ratesJurisIdBillto"/>
	<ui:param name="reRender" value="filterForm:jurisdictionIdBillto"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/batchid_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailSaleSearchBean.batchIdHandler}"/>
	<ui:param name="bean" value="#{transactionDetailSaleSearchBean}"/>
	<ui:param name="reRenderGL" value="filterForm:batchId"/>
</ui:include>
	
</ui:define>
</ui:composition>
</html>
