<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">	
		<h:form id="purchaseTransLogForm">

		<h1><img id="imgTransactionProcess" alt="View Transaction" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
		<a4j:outputPanel id="msg" ><h:messages errorClass="error" /></a4j:outputPanel>
		<a4j:queue/> 
   		
   	<a4j:outputPanel id="showhidefilter">

		<div id="top" style = "width:1190px;">
		<div id="table-one">
		
		<div id="table-four-bottom" style="padding-left: 10px;">
		<table cellpadding="0" cellspacing="0" style="width: 100%;height: 30px">
			<tr>
				<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
					 reRender="showhidefilter" actionListener="#{transactionViewBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
				<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
  					 reRender="showhidefilter" actionListener="#{transactionViewBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
				<td style="width: 50%"></td>
				<td style="width: 50%"></td>
			</tr>
			</table>
		</div>
		<div id="table-one-content" style="height:418px;">
		
		<table cellpadding="0" cellspacing="0" width="100%" id="transactionsHeader" class="ruler">
			<thead>
				<tr>
					<td colspan="5">View  Purchase Transaction Log Entry</td>
				</tr>
			</thead>
		</table>
		 </div> 	
			<div id="table-one-content" style="height:auto;">
			<a4j:outputPanel id="contentToggle" layout="block">
					<ui:include src="/WEB-INF/view/components/transactionlog_view_panel.xhtml">
					<ui:param name="trDetail" value="#{transactionViewBean.selectedTransactionLog}" />
					<ui:param name="readonly" value="false" />
				</ui:include>
			</a4j:outputPanel>
		    </div> <!-- table-one-content -->
		<div id="table-four-bottom">
			<ul class="right">
				<li class="ok"><h:commandLink id="btnOk" immediate="true" action="#{transactionDetailBean.okAction}" /></li>
			</ul>
		</div>
		</div> <!-- table-one -->
		</div> <!-- top -->
		
	</a4j:outputPanel>
	</h:form>
</ui:define>
</ui:composition>
</html>
