<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('advancedFilterForm:columnTable', 'selectedRowIndex'); } );
 
//]]>
</script>
</ui:define>

<ui:define name="body" >
<h:inputHidden id="selectedRowIndex" value="#{advancedFilterBean.selectedAdvancedFilterRowIndex}" />

<h:form id="advancedFilterForm">
<h1><h:graphicImage id="imgAdvancedFilter" alt="Advanced Filter" value="/images/headers/hdr-advanced-filter-selection.gif" /></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
		<table cellpadding="0" cellspacing="0" width="99%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td><h:outputText value="Filter Expression:    "/></td>
				<td><h:outputText id="displaymessageId" value="#{advancedFilterBean.advancedFilterMessage}" style="color:red; font-weight: bold;" /></td>
			</tr>	
			<tr>
				<td colspan="2"><h:inputTextarea id="advancedFilterExpression" style="width:100%;" rows="6" value="#{advancedFilterBean.advancedFilterTemp}"/></td>
			</tr>
			<tr>
				<td colspan="2">&#160;</td>
			</tr>
		</tbody>
		</table>

		<table cellpadding="0" cellspacing="0" width="99%" id="rollover" class="ruler">
		<tbody>
			<!--tr>
				<td><h:outputText value="Columns:"/></td>
				<td>&#160;</td>
				<td><h:outputText value="Operators:"/></td>
				<td>&#160;</td>
			</tr-->	
			<tr>
				<td width="60%">
					<div class="scrollContainer">
					<div class="scrollInner">
            		<rich:dataTable rowClasses="odd-row,even-row" styleClass="GridContent" id="columnTable"
						value="#{advancedFilterBean.dataDefinitionColList}" var="el" sortMode="single" rows="250">
             
            		<a4j:support id="clk" event="onRowClick" 
            			onsubmit="selectRow('advancedFilterForm:columnTable',this);" />
                       
            		<a4j:support id="dblClk" event="onRowDblClick"
            			onsubmit="selectRow('advancedFilterForm:columnTable', this);" 
						actionListener="#{advancedFilterBean.selectedColumnChanged}"

						reRender="advancedFilterForm:advancedFilterExpression" />
             
            		<rich:column id="description" width="300px" sortBy="#{el.description}" >
					<f:facet name="header">
					<h:outputText styleClass="headerText" value="Description"/>
					</f:facet>
					<h:outputText value="#{el.description}" />
					</rich:column>		
			   																
					<rich:column id="detailColumnName" width="300px" sortBy="#{el.dataDefinitionColumnPK.columnName}">
					<f:facet name="header">
					<h:outputText styleClass="headerText" value="Transaction Detail Column Name" />
					</f:facet>
					<h:outputText value="#{el.dataDefinitionColumnPK.columnName}" style="width:150px;"></h:outputText>
					</rich:column>
			
					</rich:dataTable>	
					</div>
					</div>
				</td>
				<td>&#160;&#160;&#160;</td>
				<td width="40%">
					<div class="scrollContainer">
					<div class="scrollInner">
            		<rich:dataTable rowClasses="odd-row,even-row" styleClass="GridContent" id="operatorTable"
						value="#{advancedFilterBean.operatorItems}" var="el" sortMode="single" rows="50">
             
            		<a4j:support id="clk1" event="onRowClick" 
            			onsubmit="selectRow('advancedFilterForm:operatorTable',this);" />
                       
            		<a4j:support id="dblClk1" event="onRowDblClick"
            			onsubmit="selectRow('advancedFilterForm:operatorTable', this);" 
						actionListener="#{advancedFilterBean.selectedOperatorChanged}"

						reRender="advancedFilterForm:advancedFilterExpression" />
			   																
					<rich:column id="operatorid" width="100px" >
					<f:facet name="header">
					<h:outputText styleClass="headerText" value="&#160;&#160;Operator&#160;&#160;" />
					</f:facet>
					<h:outputText value="#{el.value}" ></h:outputText>
					</rich:column>
			
					</rich:dataTable>	
					</div>
					</div>
				</td>
				<td>&#160;</td>
			</tr>
		</tbody>
		</table>
			
	</div>	
	<div id="table-four-bottom">
		<ul class="right">
     		<li class="ok2"><h:commandLink id="okAction" action="#{advancedFilterBean.okAdvancedFilterAction}" ></h:commandLink></li>
     		<li class="clear2"><h:commandLink id="clearAction" action="#{advancedFilterBean.clearAdvancedFilterAction}"></h:commandLink></li>
     		<li class="cancel2"><h:commandLink id="cancelAction" action="#{advancedFilterBean.cancelAdvancedFilterAction}" /></li>
    	</ul>
	</div>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>