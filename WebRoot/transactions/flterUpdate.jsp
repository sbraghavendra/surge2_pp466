<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<f:view>
<h:form id="filterUpdateForm">
	<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="8">Selection Filter Maintenance</td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="8">View Definition</th>
		</tr>
		<tr>
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">Screen Name:</td>
          <td><h:inputText style="width: 233px;" id="selectedFilterId" disabled="true" value="#{selectionFilterBean.screenNameDescription}" /></td>
          <td>&#160;</td>
          
          <td style="width: 10px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">User Name:</td>
          <td class="column-input2-search">
				<h:selectOneMenu id="UserNameId" immediate="true" disabled="#{!selectionFilterBean.isSelectUserNameEnabled}" 
						value="#{selectionFilterBean.selectedUserName}">
					<f:selectItems value="#{selectionFilterBean.userNameList}"/>	
					<a4j:support event="onchange" status="rowstatus" actionListener="#{selectionFilterBean.userNameChangeListener}"
						reRender="filterUpdateForm"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 10px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
       </tr>
       <tr>   
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">Filter Name:</td>
          <td class="column-input2-search">
				<h:selectOneMenu id="FilterNameId" immediate="true" disabled="false" 
						value="#{selectionFilterBean.selectedFilterName}">
					<f:selectItems value="#{selectionFilterBean.filterNameList}"/>
					<a4j:support event="onchange" actionListener="#{selectionFilterBean.filterNameChangeListener}"
						reRender="filterUpdateForm"/>
				</h:selectOneMenu>
          </td>
               
          <td class="column-input2-search">
          	<c:if test="#{selectionFilterBean.displayAddButton}" >
           		<h:inputText  id="newFilterNameId" maxlength = "100" value="#{selectionFilterBean.createdFilterName}"></h:inputText>
            </c:if>
			&#160;
          </td>
          
          <td>&#160;</td>
          <td>&#160;</td>
          <td style="width:10%;">&#160;</td>
        </tr>
        
        <tr>
			<td colspan="8" style="text-align: center;" >&#160;</td>
		</tr>

		<tr>
			<th colspan="8">Column Selection:</th>
		</tr>
	
		
		<tr>
			<td colspan="8" style="text-align: center;" >Filter Columns &#38; Values</td>
		</tr>
		<!--  
		<tr>
			<td colspan="8" >
			<div class="scrollContainer">
			<div class="scrollInner">
	
				<rich:dataTable rowClasses="odd-row,even-row" 
					id="columnsDataTable"
					value="#{selectionFilterBean.dwFilterList}" var="dwFilter">
						
					<rich:column id="dwFilterColumn" style="width:250px">
						<f:facet name="header"><h:outputText value="Column Name" /></f:facet>
						<h:outputText value="#{dwFilter.columnName}" />
					</rich:column>
			
					<rich:column id="dwFilterValue" style="width:680px">
						<f:facet name="header"><h:outputText value="Filter Value" /></f:facet>
							<h:outputText value="#{dwFilter.filterValue}" />
					</rich:column>

				</rich:dataTable>

			</div>
			</div>
			</td>
		</tr>
		-->
		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner" id="resize1">
	
				<rich:dataTable rowClasses="odd-row,even-row" 
					id="columnsDataTable"
					value="#{selectionFilterBean.dwFilterList}" var="dwFilter">
						
					<rich:column id="dwFilterColumn" style="width:250px" sortBy="#{selectionFilterBean.columnDescriptionMap[dwFilter.columnName]}" >
						<f:facet name="header"><h:outputText styleClass="sort-local"  value="Filter Column" /></f:facet>
						<h:outputText value="#{selectionFilterBean.columnDescriptionMap[dwFilter.columnName]}" />
					</rich:column>
			
					<rich:column id="dwFilterValue" style="width:680px;" sortBy="#{dwFilter.filterValue}" >
						<f:facet name="header" ><h:outputText styleClass="sort-local" value="Filter Value" /></f:facet>
							<h:outputText value="#{dwFilter.filterValue}" />
					</rich:column>

				</rich:dataTable>

			</div>
			</div>
	
	</div><!-- table-four-content -->
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="update"><h:commandLink id="okUpdateBtn" disabled="#{!selectionFilterBean.displayUpdateButton}" action="#{selectionFilterBean.okUpdateAction}"/></li>
		<li class="add"><h:commandLink id="okAddBtn" disabled="#{!selectionFilterBean.displayAddButton}" action="#{selectionFilterBean.okAddAction}"/></li>
<!--  	<li class="delete115"><h:commandLink id="okDeleteBtn" disabled="#{!selectionFilterBean.displayDeleteButton}" action="#{selectionFilterBean.deleteAction}"/></li> -->
		<li class="delete115">
				<a4j:commandLink id="okDeleteBtn"
 					style="#{!selectionFilterBean.displayDeleteButton? 'display:none;':'display:block;'}" 
					oncomplete="javascript:Richfaces.showModalPanel('executeDelete');" 
					reRender="executeDeleteForm"/>
			</li> 
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{selectionFilterBean.cancelAction}" /></li>	
		
		 
						  
	</ul>
	</div>

	</div>
	</div>
	
	<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
    	</rich:modalPanel>

</h:form>
</f:view>


<rich:modalPanel id="executeDelete" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Delete" />
	</f:facet>
	<h:form id="executeDeleteForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('executeDelete');" action="#{selectionFilterBean.deleteAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('executeDelete'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>

	
</ui:define>
</ui:composition>
</html>
