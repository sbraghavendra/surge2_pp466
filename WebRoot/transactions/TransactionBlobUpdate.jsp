<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form>
<h1><h:graphicImage id="imgTransactionBlobUpdate" value="/images/headers/hdr-transaction-download.gif" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="actionIdText" value="#{transactionBlobBean.actionText}"/> a Transaction Download</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File ID:</td>
				<td style="width:700px;">
				<h:inputText id="fileIDText" value="#{transactionBlobBean.workingBlob.fileId}" 
				         label="File ID"
				         disabled="true"
				         style="width:650px;" />
				</td>				       
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Name:</td>
				<td>
				<h:inputText id="fileNameText" value="#{transactionBlobBean.workingBlob.fileName}" 
				         label="File Name"
				         disabled="#{transactionBlobBean.isDeleteAction}" 
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Name:</td>
				<td>
				<h:inputText id="userNameText" value="#{transactionBlobBean.workingBlob.userName}" 
				         label="User Name"
				         disabled="true"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Create Date:</td>
				<td>
				<h:inputText id="createDateText" value="#{transactionBlobBean.workingBlob.createDate}" 
				         label="Create Date"
				         disabled="true"
				         style="width:650px;" >
				         <f:converter converterId="dateTime"/>
				</h:inputText>         
                </td>
			</tr>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Row Count:</td>
				<td>
				<h:inputText id="rowCountText" value="#{transactionBlobBean.workingBlob.rowCount}" 
				         label="Row Count"
				         disabled="true"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Size in MB:</td>
				<td>
				<h:inputText id="fileSizeText" value="#{transactionBlobBean.workingBlob.fileSize}" 
				         label="File Size in MB"
				         disabled="true"
				         style="width:650px;" />
                </td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Start Time:</td>
				<td>
				<h:inputText id="startTimeText" value="#{transactionBlobBean.workingBlob.startTime}" 
				         label="Start Time"
				         disabled="true"
				         style="width:650px;" >
				         <f:converter converterId="dateTime"/>
				</h:inputText>         
                </td>
			</tr>	
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>End Time:</td>
				<td>
				<h:inputText id="endTimeText" value="#{transactionBlobBean.workingBlob.endTime}" 
				         label="End Time"
				         disabled="true"
				         style="width:650px;" >
				         <f:converter converterId="dateTime"/>
				</h:inputText>         
                </td>
			</tr>		
			
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="deleteBtn" action="#{transactionBlobBean.deleteBlobAction}" 
        					disabled="#{!transactionBlobBean.isDeleteAction}"/>
    	</li>
    						
    	<li class="ok"><h:commandLink id="addBtn" 
    						disabled="#{!transactionBlobBean.isAddAction}" action="#{transactionBlobBean.addBlobAction}"/>
    	</li>
    			
    	<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" 
    						action="#{transactionBlobBean.cancelBlobAction}" />
    	</li>

	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>