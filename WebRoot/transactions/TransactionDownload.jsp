<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="transactionBlobAddForm">
<h1><h:graphicImage id="imgTransactionBlobAdd" value="/images/headers/hdr-transaction-download.gif" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="4"><h:outputText id="actionIdText" value="Create a Transaction Download Batch" /></td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>	
				<td>&#160;</td>				       
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Created File Name:</td>
				<td>
					<h:inputText id="fileNameText" value="#{downloadAddBean.downloadBatchMaintenance.vc01}" 
				         disabled="true" 
				         style="width:650px;" />
                </td>
                <td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Total Rows to Download:</td>
				<td>
					<h:inputText id="totalCount" value="#{downloadAddBean.downloadBatchMaintenance.totalRows}" 
				         disabled="true" 
				         style="width:650px;" />
                </td>
                <td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
                <td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td>
					<h:outputText rendered="#{downloadAddBean.stopDownload}" value="#{downloadAddBean.message}"  style="width:650px; color:red"  />	         
                </td>
                <td>&#160;</td>
			</tr>
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
    						
    	<li class="ok"><h:commandLink id="addBtn" immediate="true" disabled="#{downloadAddBean.stopDownload}" action="#{downloadAddBean.addDownloadAction}" />
    	</li>
    			
    	<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{downloadAddBean.cancelDownloadAction}" />
    	</li>
    	

	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>