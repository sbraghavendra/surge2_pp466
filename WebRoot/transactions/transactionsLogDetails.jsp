<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() {selectRowByIndex('transLogForm:logTable', 'selectedTransactionLogIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedTransactionLogIndex" value="#{transactionDetailBean.selectedTransactionLogIndex}"/>
<h:form id="transLogForm">
	<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
	<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				 <td style = "font-size: 11px"><h:outputText id="headerId" value="View Purchase Transaction History"/></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th style = "font-weight: bold;font-size: 11px;padding-left: 10px">Transaction</th>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" >
		<tbody>
		<tr>
		  	<td class="column-spacer">&#160;</td>
      		<td class="column-label2"><h:outputText value="Transaction ID:" /></td>
			<td class="column-input-search"><h:inputText style="float:left;" value="#{transactionDetailBean.currentTransaction.purchtransId}" disabled="true"/>
			 <c:if test="#{transactionDetailBean.selectedTransaction.purchtransId != 0 and (not empty transactionDetailBean.selectedTransaction.purchtransId)}">
				 <a4j:commandButton id="btnRulesId117" styleClass="image" style="float:right;width:16px;height:16px;" 
				                   image="/images/view-small.png" immediate="true"
					               action="#{transactionDetailBean.viewTransactionFromHistorty}">
	             </a4j:commandButton>
	         </c:if>
			</td>
			<td class="column-spacer">&#160;</td>
			<td class="column-label2"><h:outputText value="Process Batch No.:" /></td>
			<td class="column-input-search"><h:inputText style="float:left;" value="#{transactionDetailBean.selectedTransaction.processBatchNo}" disabled="true" />
				<c:if test="#{transactionDetailBean.selectedTransaction.processBatchNo != 0 and (not empty transactionDetailBean.selectedTransaction.processBatchNo)}">
				<a4j:commandButton id="btnLogId8" styleClass="image" style="float:right;width:16px;height:16px;" 
                   image="/images/view-small.png" immediate="true"
	              action="#{transactionDetailBean.displayBatchView}">
				  <f:param name="fromview" value="viewFromLogHistory" />
    			  <f:param name="viewBatchNo" value="#{transactionDetailBean.selectedTransaction.processBatchNo}" />
			    </a4j:commandButton>
                   </c:if>
			</td>
		   <td class="column-spacer">&#160;</td>
	       <td class="column-label2">&#160;</td>
		   <td class="column-input">&#160;</td>
		</tr>
		<!-- SECOND ROW -->
	<tr>
		<td class="column-spacer">&#160;</td>
		<td class="column-label2" ><h:outputText value="GL Date:"/></td>
		<td class="column-input" >
			<h:inputText value="#{transactionDetailBean.selectedTransaction.glDate}" disabled="true">
			<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
			</h:inputText>
	     </td>
	     <td class="column-spacer">&#160;</td>
		
		<td class="column-label2" ><h:outputText value="Entered Timestamp:"/></td>
		<td class="column-input" >
		<h:inputText value="#{transactionDetailBean.selectedTransaction.enteredDate}" disabled="true">
	    <f:convertDateTime type="date" pattern="MM/dd/yyyy hh:mm:ss a"/>
	    </h:inputText>
	     </td>
	       <td class="column-spacer">&#160;</td>
	       <td class="column-label2">&#160;</td>
		   <td class="column-input">&#160;</td>
	     
	</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th style = "font-weight: bold; font-size: 11px;padding-left: 10px">History</th>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
		<div id="bottom">
		<div id="table-four" style="vertical-align: bottom;">
		<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>
					
				 	<td align="center" >
					  	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
						<a4j:status id="pageInfo"
							startText="Request being processed..." 
							stopText="#{transactionDetailBean.pageDescription}"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     						
							
					</td>  
				
				
					<td style="width:20px;">&#160;</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div id="table-four-content">
			<div class="scrollContainer">
				<div class="scrollInner" id="resize" >				
					<rich:dataTable rowClasses="odd-row,even-row" id="logTable" style = "width:1800px;"
						value="#{transactionDetailBean.transactiondetailLogList}"  var="item"
						rows="#{transactionDetailBean.transLogHistoryTablePageSize}">
						<a4j:support event="onRowClick" 
								onsubmit="selectRow('transLogForm:logTable', this);"
								immediate="true"
								actionListener="#{transactionDetailBean.selectedTransactionLogChanged}" 
								oncomplete="initScrollingTables();"
								reRender = "viewBtn"
								/>
					
						<rich:column sortBy="#{item.updateTimestamp}">
							<f:facet name="header"><h:outputText value="Date/Time" style = "color:#336690"/></f:facet>
							<h:outputText id="updateTimestamp" value="#{item.updateTimestamp}">
							<f:converter converterId="dateTime" />
							</h:outputText>
						</rich:column>
						
						<rich:column sortBy="#{item.logSource}">
							<f:facet name="header"><h:outputText value="Process" style = "color:#336690"/></f:facet>
							<h:outputText  value="#{item.logSourceDesc}"/>
						</rich:column>
						
						<rich:column sortBy="#{item.transactionInd}">
							<f:facet name="header"><h:outputText value="Status" style = "color:#336690"/></f:facet>
							<h:outputText  value="#{transactionDetailBean.getStatus(item.transIndDesc,item.suspendDesc)}"/>
							
						</rich:column>
						
						
						<rich:column sortBy="#{item.transactionCountryCode}">
							<f:facet name="header"><h:outputText value="Trans. Country" style = "color:#336690"/></f:facet>
							<h:panelGroup rendered="#{not empty cacheManager.listCodeMap['COUNTRY'][item.transactionCountryCode]}">
								<h:outputText id="alogTransactionCountryCode1" value="#{cacheManager.listCodeMap['COUNTRY'][item.transactionCountryCode].description} (#{item.transactionCountryCode})" />
							</h:panelGroup>
							<h:panelGroup rendered="#{empty cacheManager.listCodeMap['COUNTRY'][item.transactionCountryCode]}">
								<h:outputText id="alogTransactionCountryCode2" value="#{item.transactionCountryCode}" />
							</h:panelGroup>
						</rich:column>
						
					    <rich:column sortBy="#{item.transactionStateCode}">
						<f:facet name="header"><h:outputText value="Trans. State" style = "color:#336690"/></f:facet>
						<h:panelGroup rendered="#{not empty cacheManager.taxCodeStateMap[item.transactionStateCode]}">
							<h:outputText id="alogTransactionStateCode1" value="#{cacheManager.taxCodeStateMap[item.transactionStateCode].name} (#{item.transactionStateCode})"  rendered="#{not empty item.transactionStateCode}" />
						</h:panelGroup>
						<h:panelGroup rendered="#{empty cacheManager.taxCodeStateMap[item.transactionStateCode]}">
							<h:outputText id="alogTransactionStateCode2" value="#{item.transactionStateCode}"  />
						</h:panelGroup>
					    </rich:column>
						
						
						<rich:column   sortBy="#{item.shiptoJurisdictionId}" >
							<f:facet name="header"><h:outputText value="Situs Jurisdiction" style = "color:#336690"/></f:facet>
							 <h:outputText id="alogJurisdictionId" style="float:left;" value="#{transactionDetailBean.jurisdictionHistory(item.cityName,item.countyName,item.stateCode,item.countryCode)}"/>
						</rich:column>
					
					    <rich:column styleClass="column-right" sortBy="#{item.taxMatrixId}">
							<f:facet name="header"><h:outputText value="G&amp;S Matrix ID" style = "color:#336690"/></f:facet>
							 <a4j:commandButton id="btnLogId2" styleClass="image" style="float:left;width:16px;height:16px;" 
				                   image="/images/view-small.png" immediate="true"
					               action="#{transactionViewBean.viewGSMatrix}"
					                rendered="#{item.taxMatrixId != 0 and (not empty item.taxMatrixId)}">
                                    <f:param name="fromview" value="viewFromLogHistory" />
                                    <f:param name = "gsTaxMatrixId" value="#{item.taxMatrixId}"/>
	                          </a4j:commandButton>
							<h:outputText value="#{item.taxMatrixId}" rendered="#{item.taxMatrixId != 0}" />
							<h:outputText value=""  rendered="#{item.taxMatrixId == 0}"/>
						</rich:column>
						
							<rich:column sortBy="#{item.taxcodeCode}">
							<f:facet name="header"><h:outputText value="TaxCode" style = "color:#336690"/></f:facet>
							<h:panelGroup rendered="#{not empty cacheManager.taxCodeMap[item.taxcodeCode]}">
								<h:outputText id="alogTaxcodeCode1" style="float:left;" value="#{item.taxcodeCode} - #{cacheManager.taxCodeMap[item.taxcodeCode].description}"/>
								<a4j:commandButton id="btnLogId3" styleClass="image" style="float:right;width:16px;height:16px;" 
				                   image="/images/view-small.png" immediate="true"
					               action="#{transactionViewBean.viewTaxCode}"
					                rendered="#{not empty item.taxcodeCode}">
					                <f:param name="fromview" value="viewFromLogHistory" />
					                <f:param name="taxcode" value="#{item.taxcodeCode}" />
	                            </a4j:commandButton>
							</h:panelGroup>
							<h:panelGroup rendered="#{empty cacheManager.taxCodeMap[item.taxcodeCode]}">
								<h:outputText id="alogTaxcodeCode2" value="#{item.taxcodeCode}"/>
							</h:panelGroup>
						</rich:column>
						
						<rich:column sortBy="#{item.manualTaxcodeInd}">
							<f:facet name="header"><h:outputText value="Manual TaxCode" style = "color:#336690"/></f:facet>
							<h:panelGroup rendered="#{not empty cacheManager.listCodeMap['MANUAL'][item.manualTaxcodeInd]}">
							   <h:outputText id="alogManualTaxcodeInd1" value="#{cacheManager.listCodeMap['MANUAL'][item.manualTaxcodeInd].description}"/>
							</h:panelGroup>
							<h:panelGroup rendered="#{empty cacheManager.listCodeMap['MANUAL'][item.manualTaxcodeInd]}">
							   <h:outputText id="alogManualTaxcodeInd2" value="#{item.manualTaxcodeInd}"/>
							</h:panelGroup>
						</rich:column>
						
						<rich:column styleClass="column-right" sortBy="#{item.aTbCalcTaxAmt}">
							<f:facet name="header"><h:outputText value="Tax Amount" style = "color:#336690"/></f:facet>
							<h:outputText value="0.00" rendered="#{item.aTbCalcTaxAmt eq null}"/>
							<h:outputText id="alogTbCalcTaxAmt" value="#{item.aTbCalcTaxAmt}" rendered="#{not empty item.aTbCalcTaxAmt}">
							  <f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
							</h:outputText>  
						</rich:column>
						
						<rich:column styleClass="column-right" sortBy="#{item.glExtractBatchNo}">
							<f:facet name="header"><h:outputText value="Extract Batch" style = "color:#336690"/></f:facet>
							 <a4j:commandButton id="btnLogId4" styleClass="image" style="float:left;width:16px;height:16px;" 
				                   image="/images/view-small.png" immediate="true"
					               action="#{transactionDetailBean.displayBatchView}"
					                rendered="#{item.glExtractBatchNo != 0 and (not empty item.glExtractBatchNo)}">
					                 <f:param name="fromview" value="viewFromLogHistory" />
					                 <f:param name="viewBatchNo" value="#{item.glExtractBatchNo}" />
	                          </a4j:commandButton>
							<h:outputText value="#{item.glExtractBatchNo}" rendered="#{item.glExtractBatchNo != 0}" />
							<h:outputText value=""  rendered="#{item.glExtractBatchNo == 0}"/>
						</rich:column>
						
						<rich:column sortBy="#{item.glExtractTimestamp}">
							<f:facet name="header"><h:outputText value="Extract Date/Time" style = "color:#336690"/></f:facet>
							<h:outputText id="glExtractTimestamp" value="#{item.glExtractTimestamp}">
							 <f:converter converterId="dateTime" />
						    </h:outputText>
						</rich:column>
						
					</rich:dataTable>
				</div>
			
			<rich:datascroller id="trScroll" for="logTable" maxPages="10" oncomplete="initScrollingTables();"
			 style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  
			 page="#{transactionDetailBean.transLogHistoryTablePageNumber}" reRender="pageInfo" /> 
		</div>
		</div>
		<div id="table-four-bottom">
		<ul class="right">
			<li class="view"><h:commandLink id="viewBtn" disabled="#{!transactionDetailBean.validLogSelection}" action="#{transactionDetailBean.displayTransactionLogViewAction}" /></li>
			<li class="close"><h:commandLink id="btnClose" action="#{transactionDetailBean.transProcessCloseAction}" /></li>
		</ul>
		</div>
		</div>
		</div>
	</h:form>
	
<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>	

</ui:define>
</ui:composition>
</html>
