<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetailList', 'aMDetailListRowIndex');checkAllView(); } );
registerEvent(window, "unload", function() { document.getElementById('filterForm:hiddenSaveLink').onclick(); });

function insertDateTime(objId) {
	var obj = document.getElementById(objId);
	if (obj) {
		var time = new Date();
		obj.value = obj.value +
			(time.getMonth() + 1) + '/' +
			time.getDate() + '/' +
			time.getFullYear() + ' ' +
			time.getHours() + ':' +
			time.getMinutes() + ':' +
			time.getSeconds() + ' - ';  
	}
}

function toggleApplyAll(txtId, chkId) {
	var txt = document.getElementById(txtId);
	var chk = document.getElementById(chkId);
	if (chk) {
		chk.disabled = (!txt || (txt.value == ''));
	}
}

function resetMinimumDrivers(id) {
	var obj = document.getElementById(id).getElementsByTagName('input');
	for (var i = 0; i < obj.length; i++) {
		if (!obj[i].disabled && obj[i].checked) {
			obj[i].checked = false;
		}
	}
}

function resetState()
{ 
    document.getElementById("filterForm:state").value = "";
}

function checkAllView()
{ 
	var statusMenu = document.getElementById("filterForm:statusMenu").value;	
	if(statusMenu =='US' || statusMenu =='U' || statusMenu =='UH'){
		document.getElementById("transForm:chkSelectAll").style.display='none'
		document.getElementById("transForm:selectAllCaption").style.display='none'
	}
	else{
		document.getElementById("transForm:chkSelectAll").style.display='block'
		document.getElementById("transForm:selectAllCaption").style.display='block'
	}
}

var filterFormHidden1 = false;
var filterFormHidden2 = false;
//Chenged functionality for Show and Hide Buttions for issue no 4627
jQuery(document).ready(function(){
	jQuery("#showId").hide().css("cursor", "pointer");
	jQuery('#showId').click(function(){
                        jQuery('#filterForm').show();
                        jQuery('#hideId').show();     
                        jQuery('#showId').hide();     
                        jQuery('[id="filterFormDummy:filterFormState"]').val("open");
                        jQuery('[id="filterFormDummy:filterFormState"]').change();
                           
      });
	  
	  jQuery('#hideId').click(function(){

                      jQuery('#filterForm').hide();
                      jQuery('#hideId').hide();       
                      jQuery('#showId').show();
                      jQuery('[id="filterFormDummy:filterFormState"]').val("close");
                      jQuery('[id="filterFormDummy:filterFormState"]').change();
                            	});
    
 	if(jQuery('[id="filterFormDummy:filterFormState"]').val() == "open") {
		jQuery('#filterForm').show();
	    jQuery('#hideId').show();
	}
 	if(jQuery('[id="filterFormDummy:filterFormState"]').val() == "close") {
		jQuery('#filterForm').hide();
	    jQuery('#hideId').hide();
	    jQuery('#showId').show();
	}
 	
	}); 

function confirm_message(formName, selectedStatus) { 
	if (typeof selectedStatus !== "undefined" && selectedStatus == '') {
		Richfaces.showModalPanel('requestProcessed');
		return true;
	}
	
   	var messageId = formName + 'Form:messageId';
   	var element = document.getElementById(messageId);
   	element.innerHTML = 'Only ' + selectedStatus + ' Transactions';

	Richfaces.showModalPanel(formName);
	return false;
}

//]]>
</script>
<style type="text/css">
.ie7 .scrollContainer {
	padding-top: 40px;
}

.ie7 .scrollContainer thead tr {
	height: 32px;
	vertical-align: top;
}
</style>
</ui:define>

<ui:define name="body">
	<h:inputHidden id="aMDetailListRowIndex" value="#{transactionDetailBean.selectedRowIndex}"/>

	<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
	
	<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
		
			<a  class = "srcshowhide" id="showId" href="#" > show </a>
			
			<a class = "srcshowhide" id="hideId" href="#"> hide </a></div>
		
	<h:form id="filterFormDummy">
		<!-- JJ - 4/11/2012 h:inputHidden doesn't fire valueChangeListener so had to use h:inputText with visibility hidden -->
		<h:inputText style="display: none; visibility: hidden;" id="filterFormState" value="#{transactionDetailBean.filterFormState}" immediate="true" valueChangeListener="#{transactionDetailBean.filterFormStateChanged}">
		<a4j:support event="onchange" />
   		</h:inputText>
	</h:form>
	
	<h:form id="filterForm">
		<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
			<a4j:support event="onclick" immediate="true" actionListener="#{transactionDetailBean.filterSaveAction}" />
		</a4j:commandLink>
		
		<!-- default queue for the form is created -->
   		<a4j:queue/>
   		<a4j:outputPanel id="showhidefilter">
		<c:if test="#{!transactionDetailBean.togglePanelController.isHideSearchPanel}">
		<div id="top">
		<div id="table-one">
		
		<div id="table-one-top" style="padding-left: 10px;">
			 <table cellpadding="0" cellspacing="0" style="width: 100%;">
              <tr>
              <td style="padding-left: 10px;"><a4j:commandLink id="toggleTransactionProcessCollapse" style="text-decoration: underline;color:#0033FF;"
		          reRender="showhidefilter" actionListener="#{transactionDetailBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
	          <td style="padding-left: 10px;" ><a4j:commandLink id="toggleTransactionProcessExpand" style="text-decoration: underline;color:#0033FF;"  
				          reRender="showhidefilter" actionListener="#{transactionDetailBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
	          <td style="width: 50%; padding-left: 100px; "><a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel></td>        
	 		  <td style="padding-right: 5px; padding-left: 10px; padding-top:3px; width: 50px"><h:outputText value="Filter:" /></td>
	 		  <td style="padding-right: 3px;" class="column-input2-search">
				<h:selectOneMenu id="filterMenu" style="width:225px;" value="#{transactionDetailBean.selectedFilter}">
					<f:selectItems value="#{transactionDetailBean.filterItems}"/>
					<a4j:support event="onchange" status="rowstatus" actionListener="#{transactionDetailBean.filterChangeListener}"
						reRender="filterForm"/>
				</h:selectOneMenu>
			  </td>
			  
			  <!--  
			  <td style="padding-right: 6px; padding-top: 2px;">
				<a4j:commandLink id="filterMenuEdit" styleClass="button" action="#{transactionDetailBean.filterEditAction}">
					<h:graphicImage value="/images/editButton.png" />
            	</a4j:commandLink>
	          </td> 
	          -->
	          <td style="padding-right: 6px; padding-top: 2px;">
				<h:commandLink id="filterMenuEdit" styleClass="button"   action="#{transactionDetailBean.filterEditAction}">
					<h:graphicImage value="/images/editButton.png" />
            	</h:commandLink>
	          </td> 
	          
	                   
	          <td style="padding-right: 10px; padding-left: 20px; padding-top:3px; width: 50px"><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>  			  
             </tr>
			 </table>
		  </div>
		<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'filterForm:searchBtn')" >
		
	 	<rich:simpleTogglePanel id="taxDriversInformation"   switchType="ajax" label="Goods &amp; Services Matrix Drivers" 
		actionListener="#{transactionDetailBean.togglePanelController.togglePanelChanged}" 
		opened="#{transactionDetailBean.togglePanelController.togglePanels['taxDriversInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		<h:panelGrid id="driverPanel1" binding="#{transactionDetailBean.filterTaxDriverSelectionPanel}" styleClass="panel-ruler" />
		</rich:simpleTogglePanel>
		
		<rich:simpleTogglePanel id="locationDriversInformation"   switchType="ajax" label="Location Matrix Drivers" 
		actionListener="#{transactionDetailBean.togglePanelController.togglePanelChanged}" 
		opened="#{transactionDetailBean.togglePanelController.togglePanels['locationDriversInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		<h:panelGrid id="driverPanel" binding="#{transactionDetailBean.filterLocDriverSelectionPanel}" styleClass="panel-ruler" />
		</rich:simpleTogglePanel>     
		
		
		<rich:simpleTogglePanel id="miscInformation"   switchType="ajax" label="Misc. Information" 
		actionListener="#{transactionDetailBean.togglePanelController.togglePanelChanged}" 
		opened="#{transactionDetailBean.togglePanelController.togglePanels['miscInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		
		<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
			<tbody>
			    <tr>
					<td class="column-spacer">&#160;</td>
					
					<td class="column-label">Transaction Country Code: </td> 
					<td class="column-input">
						<h:selectOneMenu id="countryMenuMain" 
							disabled="#{transactionDetailBean.locationMatrixIdDisabled}"
							value="#{transactionDetailBean.selectedCountry}"
							immediate="true"
							valueChangeListener="#{transactionDetailBean.searchCountryChanged}" 
							onchange="resetState();submit();"
					
							>
							<f:selectItems value="#{transactionDetailBean.countryMenuItems}"/>
						</h:selectOneMenu>
					</td>
					
					<td class="column-label"> Transaction State Code: </td> 
					<td class="column-input">
						<h:selectOneMenu id="state"
							disabled="#{transactionDetailBean.locationMatrixIdDisabled}"
							value="#{transactionDetailBean.selectedState}" immediate="true"
							valueChangeListener="#{transactionDetailBean.searchStateChanged}"
							>
							<f:selectItems value="#{transactionDetailBean.stateMenuItems}"/>
						</h:selectOneMenu>
					</td>
					<td class="column-label">&#160;</td> 
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> Trans Status: </td> 
					<td class="column-input">
						<h:selectOneMenu id="statusMenu"
							value="#{transactionDetailBean.transactionStatus}">
							<f:selectItems value="#{transactionDetailBean.statusItems}"/>
							<a4j:support event="onchange" status="rowstatus"
								actionListener="#{transactionDetailBean.statusChangeListener}"
								reRender="filterTaxCode,taxabilityCode,taxMatrixId,locnMatrixId,taxMatrixIdBtn,locnMatrixIdBtn,countryMenuMain,state"/>
						</h:selectOneMenu>
					</td>
					<td class="column-label"> Transaction ID: </td> 
					<td class="column-input">
						<h:inputText id="transactionId" style="text-align:right;" 
							value="#{transactionDetailBean.purchtransId}"
							onkeypress="return onlyIntegerValue(event);" 
							label="Transaction ID">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
					</td>
					<td class="column-label"> Tax Matrix ID: </td> 
					<td class="column-input-search">
						<h:inputText id="taxMatrixId" style="text-align:right;float:left;"
							disabled="#{transactionDetailBean.taxMatrixIdDisabled}"
							value="#{transactionDetailBean.taxMatrixId}"
							onkeypress="return onlyIntegerValue(event);"
							label="Tax Matrix ID">
							<f:convertNumber integerOnly="true" groupingUsed="false"/>
						</h:inputText>
						<h:commandButton id="taxMatrixIdBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								disabled="#{transactionDetailBean.taxMatrixIdDisabled}"
								image="/images/search_small.png"
								action="#{transactionDetailBean.findTaxMatrixIdAction}">
						</h:commandButton>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> From G/L Date: </td> 
					<td>
						<rich:calendar zindex="1000"
							id="fromGlDate" enableManualInput="true" label="From G/L Date"
							oninputkeypress="return onlyDateValue(event);"
							value="#{transactionDetailBean.fromGLDate}" popup="true"
							converter="date"
							datePattern="M/d/yyyy"
							showApplyButton="false" inputClass="textbox" />
					</td>
		
					<td class="column-label"> Ship To Jur. ID: </td> 
					<td class="column-input-search">
						<h:inputText id="jurisdictionId" style="text-align:right;float:left;"
							value="#{transactionDetailBean.jurisdictionId}"
							onkeypress="return onlyIntegerValue(event);" 
							label="Jurisdiction ID">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
						<!--
						<a4j:commandButton id="btnJurisdictionId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								actionListener="#{transactionDetailBean.popupSearchAction}"
								oncomplete="javascript:Richfaces.showModalPanel('searchJurisdictionID');"
								reRender="searchJurisdictionFormID" >
						</a4j:commandButton>
						-->
						<h:commandButton id="btnJurisdictionId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								action="#{transactionDetailBean.findJurisdictionIdAction}">
						</h:commandButton>
					</td>
					
					<td class="column-label"> Ship To Locn Matrix ID: </td> 
					<td class="column-input-search">
						<h:inputText id="locnMatrixId" style="text-align:right;float:left;"
							disabled="#{transactionDetailBean.locationMatrixIdDisabled}"
							value="#{transactionDetailBean.locationMatrixId}"
							onkeypress="return onlyIntegerValue(event);" 
							label="Locn Matrix ID">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
						<h:commandButton id="locnMatrixIdBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								disabled="#{transactionDetailBean.locationMatrixIdDisabled}"
								image="/images/search_small.png" immediate="true" 
								action="#{transactionDetailBean.findLocationMatrixIdAction}">
						</h:commandButton>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> To G/L Date: </td> 
					<td>
						<rich:calendar zindex="1000"
							id="toGlDate" enableManualInput="true" label="To G/L Date"
							oninputkeypress="return onlyDateValue(event);"
							value="#{transactionDetailBean.toGLDate}" popup="true"
							converter="date"
							datePattern="M/d/yyyy"
							showApplyButton="false" inputClass="textbox" />
					</td>
					<td class="column-label"> G/L Ext No: </td> 
					<td class="column-input-search">
						<h:inputText id="glExtractId" style="text-align:right;float:left;" 
							value="#{transactionDetailBean.glExtractId}"
							onkeypress="return onlyIntegerValue(event);"
							label="G/L Ext No">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
						<!--  
						<a4j:commandButton id="btnGlExtractId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								actionListener="#{transactionDetailBean.defaultGLExtractListener}"
								oncomplete="javascript:Richfaces.showModalPanel('batchIdSearch');"
								reRender="searchForm" >
						</a4j:commandButton>
						-->
						<h:commandButton id="btnGlExtractId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								action="#{transactionDetailBean.findGlExtractBatchIdAction}">
						</h:commandButton>
					</td>
					
					<td class="column-label"> Process Batch No: </td> 
					<td class="column-input-search">
						<h:inputText id="batchId" style="text-align:right;float:left;" 
							value="#{transactionDetailBean.processBatchId}"
							onkeypress="return onlyIntegerValue(event);"
							label="Process Batch No">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
						<!--
						<a4j:commandButton id="btnProcessBatchId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								actionListener="#{transactionDetailBean.defaultImportMapProcessListener}"
								oncomplete="javascript:Richfaces.showModalPanel('batchIdSearch');"
								reRender="searchForm" >
						</a4j:commandButton>
						 -->
						<h:commandButton id="btnProcessBatchId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" 
								action="#{transactionDetailBean.findProcessBatchIdAction}">
						</h:commandButton>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> Multi-Trans: </td> 
					<td class="column-input">
						<h:selectOneMenu id="multiTrans"
							value="#{transactionDetailBean.multiTransCode}">
							<f:selectItems value="#{matrixCommonBean.multiTransCodeItems}"/>
						</h:selectOneMenu>
					</td>
					<td class="column-label"> Sub-Trans. ID: </td> 
					<td class="column-input">
						<h:inputText id="subTransId" style="text-align:right;" 
							value="#{transactionDetailBean.subTransId}"
							onkeypress="return onlyIntegerValue(event);" 
							label="Sub-Trans. ID">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
							
						</h:inputText>
					</td>
					<td class="column-spacer" colspan="3">&#160;</td>
				</tr>
				
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> Taxability: </td> 
					<td class="column-input">
						<h:selectOneMenu id="taxabilityCode"
						  disabled="#{transactionDetailBean.taxableTypeDisabled}" value="#{transactionDetailBean.selectedTaxabilityCode}">
							<f:selectItems value="#{transactionDetailBean.taxabilityCodeMenuItems}"/>
						</h:selectOneMenu>
					</td>
					<td class="column-label"> TaxCode: </td> 
					<td colspan="3" style="padding-left: 1px">
						<ui:include src="/WEB-INF/view/components/taxcode_code_search.xhtml">
							<ui:param name="handler" value="#{transactionDetailBean.filterTaxCodeHandler}"/>
							<ui:param name="bean" value="#{transactionDetailBean}"/>															
							<ui:param name="id" value="filterTaxCode" />
							<ui:param name="readonly" value="#{transactionDetailBean.taxCodeDisabled}"/>
							<ui:param name="required" value="false" />
							<ui:param name="allornothing" value="false" />
							<ui:param name="showheaders" value="false" />
							<ui:param name="forId" value="false" />
						</ui:include>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
		</table>
		</rich:simpleTogglePanel>
		</div>
		<div id="table-one-bottom">
		<ul class="right">
			<li class="minimum">
				<a4j:commandLink id="minDriversBtn"
					oncomplete="javascript:Richfaces.showModalPanel('minDrivers');" 
					reRender="minDriversForm"/>
			</li>  
			<li class="clear"><a4j:commandLink id="btnClear" action="#{transactionDetailBean.resetFilter}" 
								reRender="countryMenuMain,taxabilityCode,state,driverPanel,driverPanel1,transactionId,taxMatrixId,fromGlDate,jurisdictionId,locnMatrixId,toGlDate,glExtractId,batchId,filterTaxCode,subTransId,multiTrans"
								/></li>
			<li class="search">
				<a4j:commandLink id="searchBtn"
				action="#{transactionDetailBean.listTransactions}" 
				reRender="chkSelectAll,aMDetailList,trScroll,pageInfo,processBtn,reprocessBtn,applyTaxMatrixBtn,applyLocationMatrixBtn,updateBtn,editBtn,viewBtn,suspendBtn,suspendDisabledBtn,commentsBtn,commentsDisabledBtn,copyAddBtn,saveAS,historyBtn,matrixanalysisBtn,multiTypeSelected,splitBtn,viewSplitBtn,holdBtn" oncomplete="initScrollingTables();;checkAllView();"   >

	<!--  			 	<rich:componentControl event="onclick" for="loader" attachTo="searchBtn" operation="show"/>      -->          
				</a4j:commandLink>
			</li>
			<li class="advancedfilter"><a4j:commandLink id="advFilterBtn" rendered="#{!transactionDetailBean.isAdvancedFilterEnabled}" action="#{transactionDetailBean.advancedFilterAction}" /></li>
			<li class="advancedfilterenabled"><a4j:commandLink id="advFilterEnabledBtn" rendered="#{transactionDetailBean.isAdvancedFilterEnabled}" action="#{transactionDetailBean.advancedFilterAction}" /></li>
		</ul>
		</div> <!-- table-one-bottom -->
		
		</div>
		</div>
		</c:if>
		</a4j:outputPanel>
		<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
    	
	</h:form>

	<div class="wrapper">
	<span class="block-right">&#160;</span> 
	<span class="block-left tab">
	<img src="../images/containers/STSView-transactions-open.gif"/>
	</span>
	</div>

	<h:form id="transForm">
		<!-- default queue for the form is created -->
   		<a4j:queue/>
   		
		<div id="bottom">
		<div id="table-four" style="vertical-align: bottom;">
		<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>
					<td width="10">
						<h:selectBooleanCheckbox id="chkSelectAll" value="#{transactionDetailBean.allSelected}" styleClass="check" 
							disabled="#{!transactionDetailBean.selectAllEnabled}">
								<a4j:support event="onclick" reRender="aMDetailList,pageInfo,selectAllWarning,multiTypeSelected"
									actionListener="#{transactionDetailBean.selectAllChange}"
								oncomplete="initScrollingTables();"  />
								
						</h:selectBooleanCheckbox>	</td>		
					<td><h:outputText id = "selectAllCaption" value="#{transactionDetailBean.selectAllCaption}"/>	</td>	 	
					
					<td align="center" style="width: 200px;" >
						<h:outputText id="multiTypeSelected" style="color:red;width: 200px;" 
						value="&#160;#{(not empty transactionDetailBean.multiTypeSelected) ? 'Only' :'&#160;'}&#160;#{(not empty transactionDetailBean.multiTypeSelected) ? transactionDetailBean.multiTypeSelected :'&#160;'}&#160;#{(not empty transactionDetailBean.multiTypeSelected) ? 'Transactions&#160;will&#160;be&#160;modified.' :'&#160;'}" />		
					</td>

					<td style="width:20px;">&#160;	
					</td>			
					<td align="left" >
						<a4j:status id="pageInfo"
							startStyle="color:red;"
							stopText="#{transactionDetailBean.transactionDataModel.pageDescription}" >
							<f:facet name="start">
									<h:panelGroup style="text-align:center;">
									<h:outputText value="Request being processed...&#160;&#160;" style="vertical-align:text-top;" />
									<h:graphicImage value="/images/headers/ajax-loader.gif" alt="ai" />
									</h:panelGroup>   
						    </f:facet>				
						</a4j:status>    
     						
							<h:outputText id="selectAllWarning" style="color:red" value="&#160;&#160;#{transactionDetailBean.selectAllWarning}" />
							
					</td>
					<td align="right" width="300">
						<table cellpadding="0" cellspacing="0" style="height: 22px;">
							<tr>
								<td style="width:20%;">&#160;</td>
								<!--  
								<td style="vertical-align: bottom;">
									<div class="reorder-columns" style="display: inline; vertical-align: bottom;">
						        		<a4j:commandLink id="forReorder" action="#{transactionDetailBean.prepareReorderAction}" reRender="forColomnRearrangeForm, withObject" onclick="javascript:Richfaces.showModalPanel('forColomnRearrangeLookUp');"/>
						      		</div>
								</td>
								<td>
									<rich:spacer width="20px"/>
									<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{transactionDetailBean.displayAllFields}" styleClass="check">
									<a4j:support event="onclick" reRender="aMDetailList,pageInfo,withObject"
										actionListener="#{transactionDetailBean.displayChange}"
										oncomplete="initScrollingTables();"/>
									</h:selectBooleanCheckbox>
									<h:outputText value="Display All Fields"/>
								</td>
								-->
								
								<td style="padding-right: 5px; padding-left: 10px; padding-top:3px; width: 50px"><h:outputText value="View:" /></td>
					 		  	<td style="padding-right: 3px;" class="column-input2-search">
									<h:selectOneMenu id="viewMenu" style="width:225px;" value="#{transactionDetailBean.selectedColumn}" immediate="true" >
										<f:selectItems value="#{transactionDetailBean.viewItems}"/>
										<a4j:support event="onchange" status="rowstatus" actionListener="#{transactionDetailBean.viewChangeListener}"
										reRender="aMDetailList,pageInfo,withObject" oncomplete="initScrollingTables();"/>
									</h:selectOneMenu>
							  	</td>
							  	
							  	<td style="padding-right: 6px; padding-top: 2px;">
									<h:commandLink id="viewMenuEdit" styleClass="button"   action="#{transactionDetailBean.viewEditAction}">
										<h:graphicImage value="/images/editButton.png" />
					            	</h:commandLink>
						        </td>
							  	
							</tr>
						</table>
					</td>
					<td style="width:20px;">&#160;</td>
					</tr>
				</tbody>
			</table>
		</div>

		<ui:include src="/WEB-INF/view/components/transactiondetail_table.xhtml">
			<ui:param name="formName" value="transForm"/>
			<ui:param name="bean" value="#{transactionDetailBean}"/>
			<ui:param name="dataModel" value="#{transactionDetailBean.transactionDataModel}"/>
			<ui:param name="singleSelect" value="true"/>
			<ui:param name="multiSelect" value="#{transactionDetailBean.multiSelect}"/>
			<ui:param name="doubleClick" value="true"/>
			<ui:param name="selectReRender" value="splitBtn,viewSplitBtn,processBtn,reprocessBtn,applyTaxMatrixBtn,applyLocationMatrixBtn,applyDisabledBtn,updateBtn,editBtn,viewBtn,suspendBtn,suspendDisabledBtn,multiTypeSelected,commentsBtn,commentsDisabledBtn,copyAddBtn,historyBtn,multiTypeSelected,commentsBtn,commentsDisabledBtn, matrixanalysisBtn, holdBtn"/>
			<ui:param name="scrollReRender" value="pageInfo"/>
		</ui:include>
		
		<div id="table-four-bottom">
		<ul class="right">
      		<li class="viewsplit">
        		<h:commandLink id="viewSplitBtn" onclick="Richfaces.showModalPanel('requestProcessed');" 
        			style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
        			disabled="#{!transactionDetailBean.validSelection or !transactionDetailBean.displayViewSplit or transactionDetailBean.transactionIndError or transactionDetailBean.currentUser.viewOnlyBooleanFlag or transactionDetailBean.currentUser.viewOnlyBooleanFlag}" action="#{transactionDetailBean.viewSplitAction}" />
      		</li>
      		
      		<li class="split">
        		<h:commandLink id="splitBtn" onclick="Richfaces.showModalPanel('requestProcessed');"  
        			style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
					disabled="#{!transactionDetailBean.validSelection or !transactionDetailBean.displaySplit or transactionDetailBean.transactionIndError or transactionDetailBean.currentUser.viewOnlyBooleanFlag }" action="#{transactionDetailBean.splitAction}" />
      		</li>
			
      		<li class="processBtn">
	        <h:commandLink id="processBtn" 
	        			   onclick="return confirm_message('processTransMessage','#{(transactionDetailBean.multiTypeSelected)}');"
	                       style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
	                       disabled="#{!transactionDetailBean.validSelection or transactionDetailBean.originalSplit or transactionDetailBean.nullTransInd or transactionDetailBean.transactionIndError or transactionDetailBean.transactionProcessed or transactionDetailBean.currentUser.viewOnlyBooleanFlag}"
	                       action="#{transactionDetailBean.displayProcessAction}" /> 
      		</li>
      		<!--  0005238  -->
      		<li class="reprocessBtn">
      			<h:commandLink id="reprocessBtn"
      				onclick="return confirm_message('reprocessTransMessage','#{(transactionDetailBean.multiTypeSelected)}');" 
					style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
      				disabled="#{transactionDetailBean.reprocessDisabled or transactionDetailBean.originalSplit or transactionDetailBean.nullTransInd or transactionDetailBean.currentUser.viewOnlyBooleanFlag}"
      				action="#{transactionDetailBean.displayReprocessAction}"  />
	        </li>
			<li class="applyBtn">
				<!-- 4211
				<a4j:commandLink id="applyTaxMatrixBtn"
					rendered="#{!transactionDetailBean.originalSplit}" 
					style="#{transactionDetailBean.applyTaxMatrixDisabled? 'display:none;':'display:block;'}" 
					action="#{transactionDetailBean.prepareDisplayAction}" 
					onclick="javascript:Richfaces.showModalPanel('applyTaxMatrix');" 			
					reRender="applyTaxMatrixForm"/>
				-->	
				<h:commandLink id="applyTaxMatrixBtn" 
					onclick="return confirm_message('applyTaxCodeTransMessage','#{(transactionDetailBean.multiTypeSelected)}');"
					style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
					disabled="#{transactionDetailBean.applyTaxMatrixDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag}" 
					action="#{transactionDetailBean.displayApplyTaxCodeAction}" />
			</li>
			<li class="applyBtn">
				<!-- 4211 
				<a4j:commandLink id="applyLocationMatrixBtn"
					rendered="#{!transactionDetailBean.originalSplit}" 
					style="#{transactionDetailBean.applyLocationMatrixDisabled? 'display:none;':'display:block;'}" 
					action="#{transactionDetailBean.prepareDisplayAction}" 
					oncomplete="javascript:Richfaces.showModalPanel('applyLocationMatrix');" 
					reRender="applyLocationMatrixForm"/>
				-->	
				<h:commandLink id="applyLocationMatrixBtn" 
					onclick="return confirm_message('applyJurisdictionTransMessage','#{(transactionDetailBean.multiTypeSelected)}');"
					style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
					disabled="#{transactionDetailBean.applyLocationMatrixDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag}" 
					action="#{transactionDetailBean.displayApplyJurisdictionAction}" />
			</li>
			<li class="applyDisabledBtn">
				<h:commandLink id="applyDisabledBtn" 
					disabled="#{!(transactionDetailBean.applyMatrixDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag)}"
				 />
			</li>
			
			<li class="updateBtn"><h:commandLink id="updateBtn" 
			 	onclick="return confirm_message('modifyTransMessage','#{(transactionDetailBean.multiTypeSelected)}');"
				style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
				disabled="#{transactionDetailBean.updateDisabled or transactionDetailBean.originalSplit or transactionDetailBean.nullTransInd or transactionDetailBean.currentUser.viewOnlyBooleanFlag}" 
				action="#{transactionDetailBean.displayUpdateAction}" />
			</li>
			<li class="edit"><h:commandLink id="editBtn" 
				style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
				disabled="#{transactionDetailBean.updateDisabled or transactionDetailBean.originalSplit or transactionDetailBean.nullTransInd or transactionDetailBean.currentUser.viewOnlyBooleanFlag}" 
				action="#{transactionDetailBean.displayEditAction}" />
			</li>
			
			<li class="viewBtn"><h:commandLink id="viewBtn" onclick="Richfaces.showModalPanel('requestProcessed');" disabled="#{!transactionDetailBean.validSelection}" action="#{transactionDetailBean.displayViewAction}" /></li>
			<li class="suspendBtn">
				<a4j:commandLink id="suspendBtn"
					disabled="#{!transactionDetailBean.validSelection or transactionDetailBean.currentUser.viewOnlyBooleanFlag or transactionDetailBean.originalSplit}"
					style="#{(transactionDetailBean.suspendDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag or transactionDetailBean.originalSplit)? 'display:none;':'display:block;'}"
					action="#{transactionDetailBean.prepareDisplayAction}" 
					oncomplete="javascript:Richfaces.showModalPanel('suspend');" 
					reRender="suspendForm"/>
			</li>

			<li class="suspendDisabledBtn">
				<h:commandLink id="suspendDisabledBtn" 
					style="#{(transactionDetailBean.suspendDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag or transactionDetailBean.originalSplit)? 'display:block;':'display:none;'}"
					disabled="#{!(transactionDetailBean.suspendDisabled or transactionDetailBean.currentUser.viewOnlyBooleanFlag or transactionDetailBean.originalSplit)}"
				 />
			</li>
			
		 <li class="holdBtn">
        	  <h:commandLink id="holdBtn" style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
	                       disabled="#{!transactionDetailBean.validSelection  or transactionDetailBean.transactionProcessedOpen }"
	                       action="#{transactionDetailBean.displayHoldAction}" /> 
                         
         </li>
			
			<!--
			<li class="comments">
				<a4j:commandLink id="commentsBtn" 
					disabled="#{transactionDetailBean.currentUser.viewOnlyBooleanFlag or !transactionDetailBean.validForComment}" 		  
					  >
					
						<a4j:support event="onclick"
						immediate="true"
							action="#{transactionDetailBean.prepareDisplayActionForComments}"
							oncomplete="javascript:Richfaces.showModalPanel('comments');"/>
				</a4j:commandLink>
			</li>
			
			<li class="comments">
				<h:commandLink id="commentsBtn"
					disabled="#{transactionDetailBean.currentUser.viewOnlyBooleanFlag or !transactionDetailBean.validForComment}" 
					
				
					 >
					<a4j:support event="onclick"
					 
						action="#{transactionDetailBean.prepareDisplayActionForComments}"
						reRender="commentsForm"
							oncomplete="javascript:Richfaces.showModalPanel('comments'); return false;"/>
							
				</h:commandLink>
			</li>
			-->
			<li class="commentsBtn">
				<a4j:commandLink id="commentsBtn" immediate="true"
					style="#{(transactionDetailBean.validForComment and !transactionDetailBean.currentUser.viewOnlyBooleanFlag) ? 'display:block;':'display:none;'}"
					disabled = "#{transactionDetailBean.currentUser.viewOnlyBooleanFlag }"
					action="#{transactionDetailBean.prepareDisplayActionForComments}"
					reRender="commentsForm" 
					oncomplete="javascript:Richfaces.showModalPanel('comments');" 
				/>
			</li>
			<li class="commentsDisabledBtn">
				<h:commandLink id="commentsDisabledBtn" 
					style="#{(transactionDetailBean.validForComment and !transactionDetailBean.currentUser.viewOnlyBooleanFlag) ? 'display:none;':'display:block;'}"
				 />
			</li>
			
<!-- 07/27/2010 SaveAs temporarily comment out	-->	
			<li class="saveasBtn">
				 <h:commandLink id="saveAS" immediate="true"
					style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
				 	onclick="Richfaces.showModalPanel('requestProcessed');" 
				 	disabled="#{!transactionDetailBean.dispSaveAs or transactionDetailBean.currentUser.viewOnlyBooleanFlag}"
					action="#{transactionDetailBean.saveAction}" />
			</li>
				
			
			<li class="copyaddBtn">
				<h:commandLink id="copyAddBtn"
							   onclick="Richfaces.showModalPanel('requestProcessed');"
							   disabled="#{transactionDetailBean.copyAddDisabled or transactionDetailBean.originalSplit or transactionDetailBean.currentUser.viewOnlyBooleanFlag}"
							   style="#{(transactionDetailBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
							   action="#{transactionDetailBean.displayCopyAddAction}" />
			</li>
			<li class="advancedsortBtn"><h:commandLink id="advSortBtn" onclick="Richfaces.showModalPanel('requestProcessed');" action="#{transactionDetailBean.advancedSortAction}" /></li>
			<li class="historyBtn">
	      	        <h:commandLink id="historyBtn" onclick="Richfaces.showModalPanel('requestProcessed');" disabled="#{!transactionDetailBean.validSelection}" action="#{transactionDetailBean.displayHistoryAction}" /> 
      		</li>
      		<li class="matrixanalysisBtn">
	      	        <h:commandLink id="matrixanalysisBtn" disabled="#{!transactionDetailBean.validMatrixAnalysisSelection}" action="#{transactionDetailBean.displayMatrixAnalysisAction}" /> 
      		</li>
      	
		</ul>
		</div>
		</div>
		</div>
	</h:form>
	
<!-- 5208 remove pop-up, Apply a tax matrix -->
	
<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>	
	
	
  <rich:modalPanel id="suspend" zindex="2000" autosized="true" width="400">
	 <f:facet name="header">
		<h:outputText value="Confirm Suspend Transaction(s)" />
	</f:facet>
  <h:form id="suspendForm">
	<div id="table-one-content" style="height:418px;">
		<table cellpadding="0" cellspacing="0" width="100%" id="headbodyseparate">
			<thead>
				<tr><td><div align="left"><h:outputText id="suspendCheckboxes" value="Suspend"/></div></td></tr>
			</thead>
		</table>
	</div>
	<table class="suspendSelect" width="100%">
		<tr><td><div align="left"><h:outputText id="confirmText" value="Suspend Selected Transaction(s)?"/></div></td></tr>
		<tr>
            <td align="left">
				<h:selectOneRadio id="suspendTypeRadio" disabledClass ="disabledradiotext" style="padding-left: 10px;vertical-align:top;" layout="pageDirection" valueChangeListener="#{transactionDetailBean.checkSelectedVal}" value= "#{transactionDetailBean.suspendType}">
			 	 <f:selectItem itemValue="SUSPEND_LOCN" itemLabel="Suspend for Location Matrix"/> 
			 	 <f:selectItem itemValue="SUSPEND_GS" itemLabel="Suspend for Goods &amp; Services Matrix"/>
			 	 <f:selectItem itemValue="SUSPEND_TAXCODE_DTL" itemLabel="Suspend for Taxcode Rule"/>
			 	 <f:selectItem itemValue="SUSPEND_RATE" itemLabel="Suspend for TaxRate"/>
			 	 
			 		<a4j:support event="onclick" ajaxSingle="true" reRender="btnOk" status="none" />       
				</h:selectOneRadio>	
				</td>
   	 		<td>&#160;</td>
            
        </tr>
        <tr><td></td></tr>
        
        <tr><td align="left"><h:outputText id="count" value="#{transactionDetailBean.totalSelected} Processed Transaction(s) selected"/></td></tr>
   
    </table>
	<div id="table-one-bottom">	
		<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" disabled="#{!transactionDetailBean.suspendFlag}"
		                             onclick="Richfaces.hideModalPanel('suspend');javascript:Richfaces.showModalPanel('panelsuspend');javascript:Richfaces.showModalPanel('loadersuspend');" 
                                     action="#{transactionDetailBean.suspendAction}" />
		 </li>
		
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" onclick="Richfaces.hideModalPanel('suspend'); return false;" /></li>		
		</ul>
	</div>
  </h:form>
 </rich:modalPanel>

<rich:modalPanel id="comments" zindex="2000" autosized="true" >
	<f:facet name="header">
		<h:outputText value="Comments" />
	</f:facet>
	<h:form id="commentsForm">
	<a4j:region id="a4j-comment-region">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText id="transId" value="Transaction ID: #{transactionDetailBean.currentTransaction.purchtransId}"/>
			</f:facet>
			
			<h:outputText value="Modify Comments"/>
			<h:inputTextarea id="comment" style="width: 500px;" rows="8"
				value="#{transactionDetailBean.currentTransaction.comments}"/>
			
			<a4j:commandButton id="btnInsertDateTime"
         style="cursor:pointer"
         value="Insert Date/Time"
         action="#{transactionDetailBean.insertDateTimeAction}" reRender="comment" />
			
			<h:outputText value="Append Comments"/>
			<h:inputText id="comments" style="width: 500px;" value="#{transactionDetailBean.comments}" maxlength="4000"/>

			<h:outputText id="oneOf"
                    value="#{transactionDetailBean.currentTransactionIndex} of #{transactionDetailBean.totalSelected} Transactions Selected"/>

			<h:panelGrid columns="2" columnClasses="column-left,column-right" width="100%" cellpadding="0" cellspacing="0">
				<h:panelGroup style="text-align:left;">
					<a4j:commandButton id="okBtn"
                             style="cursor:pointer"
							               value="Ok"
                             action="#{transactionDetailBean.addCommentsAction}" 
                             reRender="commentsForm"
							               onclick="if (#{transactionDetailBean.processedAllTransactions}) {Richfaces.hideModalPanel('comments');}"
                             />
					<rich:spacer width="20px"/>
					<h:selectBooleanCheckbox id="applyToAll" styleClass="check"
						title="Apply to All Remaining Transactions"
						value="#{transactionDetailBean.applyToAll}"/>
					<h:outputText value="Apply to All Remaining Transactions"/>
				</h:panelGroup>
				<h:panelGroup style="text-align:right;">						             
					<a4j:commandButton id="cancelAllBtn" action="#{transactionDetailBean.cancelAll}"
                           style="cursor:pointer"
							             value="Cancel ALL" onclick="Richfaces.hideModalPanel('comments'); " />
							             
					<rich:spacer width="20px"/>
					<a4j:commandButton id="cancelBtn" 
                             style="cursor:pointer"
							               value="Cancel"
                             action="#{transactionDetailBean.nextTransactionAction}" 
                             reRender="commentsForm"
							               onclick="if (#{transactionDetailBean.processedAllTransactions}) {Richfaces.hideModalPanel('comments');}"
                             />
				</h:panelGroup>
			</h:panelGrid>
		</h:panelGrid>
    </a4j:region>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="loadersuspend" zindex="2000" autosized="true">
	<h:outputText value="Transactions are now suspending..."/>
</rich:modalPanel>	
    	
<rich:modalPanel id="panelsuspend" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
						
<rich:modalPanel id="minDrivers" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Select Minimum Drivers" />
	</f:facet>
	<h:form id="minDriversForm">
		<h:panelGrid columns="1" width="430px" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Minimum Drivers"/>
			</f:facet>
				 <c:if test="#{loginBean.isPurchasingSelected}">   
			<h:panelGrid id="minDriversPanel"
                   binding="#{matrixCommonBean.minimumTaxDriversPanel}"/>
                   </c:if>
				 <c:if test="#{!loginBean.isPurchasingSelected}">   
			<h:panelGrid id="minDriversPanel"
                   binding="#{matrixCommonBean.minimumTaxDriversPanelgsb}"/>
                   </c:if>
			<h:panelGroup id="minDriversPG"
                    style="text-align:center;">
				<a4j:commandButton id="okBtn" value="Ok" actionListener="#{transactionDetailBean.minimumTaxDriversActionListener}"
                             style="cursor:pointer"
					oncomplete="Richfaces.hideModalPanel('minDrivers'); return false;"
					reRender="driverPanel,driverPanel1"/>
				<rich:spacer width="20px"/>		
				<a4j:commandButton id="resetBtn" value="Reset"  
					onclick="resetMinimumDrivers('minDriversForm'); return true;"/>	
									
				<rich:spacer width="20px"/>
				<a4j:commandButton id="cancelBtn"
                           style="cursor:pointer"
						value="Cancel" onclick="Richfaces.hideModalPanel('minDrivers'); return false;"/>
			</h:panelGroup>
		</h:panelGrid>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="forColomnRearrangeLookUp" zindex="2000" autosized="true">
			<f:facet name="header">
		<h:outputText value="Process Transaction Headers" />
	</f:facet>
	<h:form id="forColomnRearrangeForm">
		
	<rich:orderingList value="#{matrixCommonBean.forHeaderRearrangeForModal}" var="item"
						id="withObject" 
						listHeight="280"
						listWidth="200"
						immediate="true"
						style="text-align: left;"
						converter="headerConvertor"
								controlsType="button">
							<rich:column>
								<f:facet name="header" >
									<h:outputText value="Column Headers" />
								</f:facet>
								<h:outputText value="#{item.columnLabel}" />
							</rich:column>
							<a4j:support event="onorderchanged" action="#{transactionDetailBean.showOk}" 
							reRender="rearrangeBtn, rearrangeBtnPrompt"/>
						</rich:orderingList>
						<h:commandButton value="Ok" id="rearrangeBtn" style="cursor:pointer"
						disabled="#{!transactionDetailBean.showOkButton }"
						 rendered="#{!transactionDetailBean.renderOptionPanel }"
						action="#{transactionDetailBean.reArrangeColumns}" />
						
						<a4j:commandButton value="Ok" id="rearrangeBtnPrompt" style="cursor:pointer"
						disabled="#{!transactionDetailBean.showOkButton }"
						 rendered="#{transactionDetailBean.renderOptionPanel}"
						onclick="Richfaces.showModalPanel('gridSaveOption');"
						 />
						<rich:spacer width="20px"/>
							<h:commandButton id="cancelReorder"
                           style="cursor:pointer"
						value="Cancel" onclick="Richfaces.hideModalPanel('forColomnRearrangeLookUp');" action="#{transactionDetailBean.resetForHeaderRearrangeTemp}"/>
								<rich:spacer width="20px"/>
								<a4j:commandButton value="Default" action="#{transactionDetailBean.refreshColumns}" reRender="withObject,rearrangeBtn,rearrangeBtnPrompt"/>
								
						</h:form>
</rich:modalPanel>

<rich:modalPanel id="gridSaveOption" zindex="2000" autosized="true">
			<f:facet name="header">
		<h:outputText value="Save ?" />
	</f:facet>
	<h:form id="saveGridOptionForm">
		
	<h:outputText value="Save Changes To Grid layout ?" />
	<rich:spacer width="20px"/>
						<h:commandButton value="Yes" id="rearrangeBtn" style="cursor:pointer" action ="#{transactionDetailBean.saveGridAction }"
						 />
						<rich:spacer width="20px"/>
							<h:commandButton id="cancelReorder"
                           style="cursor:pointer"
						value="No"  action="#{transactionDetailBean.doNotSave }" onclick="Richfaces.hideModalPanel('gridSaveOption'); Richfaces.hideModalPanel('forColomnRearrangeLookUp');"/>
				
						</h:form>
</rich:modalPanel>

<rich:modalPanel id="modifyTransMessage" zindex="2000" autosized="true" width="280">
	<f:facet name="header"><h:outputText value="Information" /></f:facet>
	<h:form id="modifyTransMessageForm">
	<a4j:region id="a4j-modifyTransMessageForm-region">
		<h:panelGrid columns="1" headerClass="column-center" cellpadding="2" cellspacing="2">
			<f:facet name="header">		
				<h:panelGroup style="text-align:center;">		  
					<h:outputText id="messageId" style="text-align:center;color:red" value="default"/><BR/>
					<h:outputText style="text-align:center;color:red" value="will&#160;be&#160;Updated."/>
				</h:panelGroup>
			</f:facet>		
			<rich:spacer width="200px"/>
			<h:panelGroup style="text-align:center;">			  
				<h:commandButton id="okBtn" value="Ok" style="cursor:pointer" onclick="Richfaces.hideModalPanel('modifyTransMessage');Richfaces.showModalPanel('requestProcessed');" 
					action="#{transactionDetailBean.displayUpdateAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('modifyTransMessage'); return false;" />
			</h:panelGroup>		
		</h:panelGrid>
	</a4j:region>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="processTransMessage" zindex="2000" autosized="true" width="280">
	<f:facet name="header"><h:outputText value="Information" /></f:facet>
	<h:form id="processTransMessageForm">
	<a4j:region id="a4j-processTransMessageForm-region">
		<h:panelGrid columns="1" headerClass="column-center" cellpadding="2" cellspacing="2">
			<h:panelGroup style="text-align:center;">
				<h:outputText id="messageId" style="text-align:center;color:red" value="default"/><BR/>
				<h:outputText style="text-align:center;color:red" value="will&#160;be&#160;Processed."/>
			</h:panelGroup>		
			<rich:spacer width="200px"/>
			<h:panelGroup style="text-align:center;">			  
				<h:commandButton id="okBtn" value="Ok" style="cursor:pointer" onclick="Richfaces.hideModalPanel('processTransMessage');Richfaces.showModalPanel('requestProcessed');" 
					action="#{transactionDetailBean.displayProcessAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('processTransMessage'); return false;" />
			</h:panelGroup>		
		</h:panelGrid>
	</a4j:region>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="reprocessTransMessage" zindex="2000" autosized="true" width="280">
	<f:facet name="header"><h:outputText value="Information" /></f:facet>
	<h:form id="reprocessTransMessageForm">
	<a4j:region id="a4j-reprocessTransMessageForm-region">
		<h:panelGrid columns="1" headerClass="column-center" cellpadding="2" cellspacing="2">
			<h:panelGroup style="text-align:center;">
				<h:outputText id="messageId" style="text-align:center;color:red" value="default"/><BR/>
				<h:outputText style="text-align:center;color:red" value="will&#160;be&#160;Reprocessed."/>
			</h:panelGroup>		
			<rich:spacer width="200px"/>
			<h:panelGroup style="text-align:center;">			  
				<h:commandButton id="okBtn" value="Ok" style="cursor:pointer" onclick="Richfaces.hideModalPanel('reprocessTransMessage');Richfaces.showModalPanel('requestProcessed');" 
					action="#{transactionDetailBean.displayReprocessAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('reprocessTransMessage'); return false;" />
			</h:panelGroup>		
		</h:panelGrid>
	</a4j:region>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="applyTaxCodeTransMessage" zindex="2000" autosized="true" width="280">
	<f:facet name="header"><h:outputText value="Information" /></f:facet>
	<h:form id="applyTaxCodeTransMessageForm">
	<a4j:region id="a4j-applyTaxCodeTransMessageForm-region">
		<h:panelGrid columns="1" headerClass="column-center" cellpadding="2" cellspacing="2">
			<h:panelGroup style="text-align:center;">
				<h:outputText id="messageId" style="text-align:center;color:red" value="default"/><BR/>
				<h:outputText style="text-align:center;color:red" value="will&#160;be&#160;Applied."/>
			</h:panelGroup>		
			<rich:spacer width="200px"/>
			<h:panelGroup style="text-align:center;">			  
				<h:commandButton id="okBtn" value="Ok" style="cursor:pointer" onclick="Richfaces.hideModalPanel('applyTaxCodeTransMessage');Richfaces.showModalPanel('requestProcessed');" 
					action="#{transactionDetailBean.displayApplyTaxCodeAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('applyTaxCodeTransMessage'); return false;" />
			</h:panelGroup>		
		</h:panelGrid>
	</a4j:region>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="applyJurisdictionTransMessage" zindex="2000" autosized="true" width="280">
	<f:facet name="header"><h:outputText value="Information" /></f:facet>
	<h:form id="applyJurisdictionTransMessageForm">
	<a4j:region id="a4j-applyJurisdictionTransMessageForm-region">
		<h:panelGrid columns="1" headerClass="column-center" cellpadding="2" cellspacing="2">
			<h:panelGroup style="text-align:center;">
				<h:outputText id="messageId" style="text-align:center;color:red" value="default"/><BR/>
				<h:outputText style="text-align:center;color:red" value="will&#160;be&#160;Applied."/>
			</h:panelGroup>		
			<rich:spacer width="200px"/>
			<h:panelGroup style="text-align:center;">			  
				<h:commandButton id="okBtn" value="Ok" style="cursor:pointer" onclick="Richfaces.hideModalPanel('applyJurisdictionTransMessage');Richfaces.showModalPanel('requestProcessed');" 
					action="#{transactionDetailBean.displayApplyJurisdictionAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('applyJurisdictionTransMessage'); return false;" />
			</h:panelGroup>		
		</h:panelGrid>
	</a4j:region>
	</h:form>
</rich:modalPanel>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{transactionDetailBean.jurisdictionHandler}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionID"/>
	<ui:param name="popupForm" value="searchJurisdictionFormID"/>
	<ui:param name="jurisId" value="ratesJurisId"/>
	<ui:param name="reRender" value="filterForm:jurisdictionId"/>
</ui:include>


</ui:define>

</ui:composition>

</html>
