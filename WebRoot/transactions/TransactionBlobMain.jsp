<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('transactionBlobForm:transactionBlobTable', 'selectedRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedRowIndex" value="#{transactionBlobBean.selectedRowIndex}" />
<h:form id="transactionBlobForm">
	<div id="bottom">
    <h1><h:graphicImage id="imgTransactionBlobMain" value="/images/headers/hdr-transaction-download.gif" /></h1>
    <div id="table-four">
    <div id="table-four-top">
       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{transactionBlobBean.transactionBlobDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 	
       <h:messages errorClass="error" />
     </div>
     
     <div id="table-four-content">
       <ui:include src="/WEB-INF/view/components/transaction_blob.xhtml">
        <ui:param name="bean" value="#{transactionBlobBean}" />
		<ui:param name="showSelected" value="false" />
        <ui:param name="onsubmit" value="selectRow('transactionBlobForm:transactionBlobTable', this);" />
        <ui:param name="reRender"  value="deleteBtn,saveASBtn"/>
       </ui:include>
     </div>     <!-- table-four-content -->

     <div id="table-four-bottom">
       <ul class="right">  	
        	<li class="download"><h:commandLink id="saveASBtn" disabled="#{transactionBlobBean.disableDeleteButton}"
				action="#{transactionBlobBean.saveAsBlobAction}" />
			</li>	
        	
        	<li class="delete"><h:commandLink id="deleteBtn" action="#{transactionBlobBean.displayDeleteBlobAction}" 
        		disabled="#{transactionBlobBean.disableDeleteButton}"/></li>
        		  	
        	<li class="refresh"><h:commandLink id="refreshBtn" value=""
                action="#{transactionBlobBean.refreshAction}" ></h:commandLink>
        	</li>
       </ul>
      </div>      <!-- table-four-bottom -->
      
     </div>
    <!-- t4 -->
   </div>
   <!-- bottom -->
  </h:form>
</ui:define>
</ui:composition>
</html>
