<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
var currentPanel = "taxDriver";
function changePanel(menu) {
	var obj = document.getElementById(currentPanel);
	if (obj) obj.style.display = "none";
	obj = document.getElementById(menu.value);
	if (obj) obj.style.display = "block";
	currentPanel = menu.value;
}
//]]>
</script>
</ui:define>

<ui:define name="body">
	<h:form id="transForm">

		<h1><img id="imgTransactionProcess" alt="View Transaction" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
		<div id="top">
		<div id="table-one">
		<div id="table-one-top"></div>
		<div id="table-one-content">

		<table cellpadding="0" cellspacing="0" width="100%" id="transactionsHeader" class="ruler">
			<thead>
				<tr>
					<td colspan="7">View Transaction</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2"><h:outputText id="lblTransactionDetailId" value="#{matrixCommonBean.transactionPropertyMap['transactionDetailId'].description}:"/></td>
					<td class="column-input2"><h:inputText id="txtTransactionDetailId" value="#{bcpTransactionViewBean.selectedTransaction.transactionDetailId}" disabled="true"/></td>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2"><h:outputText id="lblSourceTransactionId" value="#{matrixCommonBean.transactionPropertyMap['sourceTransactionId'].description}:"/></td>
					<td class="column-input2"><h:inputText id="txtSourceTransactionId" value="#{bcpTransactionViewBean.selectedTransaction.sourceTransactionId}" disabled="true"/></td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2"><h:outputText id="lblEnteredDate" value="#{matrixCommonBean.transactionPropertyMap['enteredDate'].description}:"/></td>
					<td class="column-input2">
						<h:inputText id="txtEnteredDate" value="#{bcpTransactionViewBean.selectedTransaction.enteredDate}" disabled="true">
							<f:convertDateTime type="both" pattern="MM/dd/yyyy h:mm:ss a"/>
						</h:inputText>
					</td>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2">Transaction Status:</td>
					<td class="column-input2"><h:inputText id="txtTransactionStatus" value="#{bcpTransactionViewBean.selectedTransaction.transactionStatus}" disabled="true"/></td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2">Display:</td>
					<td class="column-input2">
						<h:selectOneMenu id="ddlDisplay" value="taxDriver" onchange="changePanel(this); return false;">
							<f:selectItem itemValue="taxDriver" itemLabel="Tax Driver"/>
							<f:selectItem itemValue="locDriver" itemLabel="Location Driver"/>
							<f:selectItem itemValue="gl" itemLabel="G/L"/>
							<f:selectItem itemValue="vendor" itemLabel="Vendor/Ship To"/>
							<f:selectItem itemValue="invoice" itemLabel="Invoice"/>
							<f:selectItem itemValue="afe" itemLabel="AFE/Inventory/Voucher"/>
							<f:selectItem itemValue="wopo" itemLabel="WO/PO"/>
							<f:selectItem itemValue="tax" itemLabel="Tax"/>
							<f:selectItem itemValue="user" itemLabel="User"/>
							<f:selectItem itemValue="batch" itemLabel="Batch"/>
						</h:selectOneMenu>
					</td>		
					<td class="column-spacer">&#160;</td>
					<td class="column-label2">&#160;</td>
					<td class="column-input2">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
		</table>
		
		<div id="taxDriver" style="display:block;">
			<h:panelGrid binding="#{bcpTransactionViewBean.taxDriverViewPanel}" width="100%"/>
		</div>
		<div id="locDriver" style="display:none;">
			<h:panelGrid binding="#{bcpTransactionViewBean.locationDriverViewPanel}" width="100%"/>
		</div>
		<div id="gl" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_gl_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="vendor" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_vendor_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="invoice" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_invoice_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="afe" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_afe_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="wopo" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_wopo_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="tax" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_tax_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
				<ui:param name="jurisdiction" value="#{transactionDetailBean.currentJurisdiction}"/>
			</ui:include>
		</div>
		<div id="user" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_user_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		<div id="batch" style="display:none;">
			<ui:include src="/WEB-INF/view/components/transactiondetail_batch_panel.xhtml">
				<ui:param name="trDetail" value="#{bcpTransactionViewBean.selectedTransaction}"/>
			</ui:include>
		</div>
		
		<table cellpadding="0" cellspacing="0" width="100%" id="transactionFooter" class="ruler">
			<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2"><h:outputText id="lblUpdateUserId" value="#{matrixCommonBean.transactionPropertyMap['updateUserId'].description}:"/></td>
					<td class="column-input2"><h:inputText id="txtUpdateUserId" value="#{bcpTransactionViewBean.selectedTransaction.updateUserId}" disabled="true"/></td>
					<td class="column-spacer">&#160;</td>
					<td class="column-label2"><h:outputText id="lblUpdateTimestamp" value="#{matrixCommonBean.transactionPropertyMap['updateTimestamp'].description}:"/></td>
					<td class="column-input2">
						<h:inputText id="txtUpdateTimestamp" value="#{bcpTransactionViewBean.selectedTransaction.updateTimestamp}" disabled="true">
							<f:converter converterId="dateTime"/>
						</h:inputText>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
		</table>
		</div>
		<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk" immediate="true" action="#{bcpTransactionViewBean.okAction}" /></li>
		</ul>
		</div>
		</div>
		</div>
		
	</h:form>
	
</ui:define>

</ui:composition>

</html>
