<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetailList', 'aMDetailListRowIndex'); } );

//]]>
</script>

</ui:define>

<ui:define name="body">
	<h:inputHidden id="aMDetailListRowIndex" value="#{transactionDetailBean.selectedSplitRowIndex}"/>
	<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
	
	<div class="wrapper">
	<span class="block-right">&#160;</span> 
	<span class="block-left tab">
	<img src="../images/containers/STSView-multi-transaction-group.gif"/>
	</span>
	</div>
	
	<h:form id="transForm">
		<!-- default queue for the form is created -->
   		<a4j:queue/>
   		
		<div id="bottom">
		<div id="table-four">
		<div id="table-four-top" style="padding-left: 23px;">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
					<td width="200">
						&nbsp;
					</td>
					<td align="center" >
						<a4j:status id="pageInfo"
							startText="Request being processed..." 
							stopText="#{transactionDetailBean.splitGroupDataModel.pageDescription}"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     						
							<h:outputText id="selectAllWarning" style="color:red" value="#{transactionDetailBean.selectAllWarning}" />
							
					</td>
					<td align="right" width="300">
					<rich:spacer width="20px"/>
						<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{transactionDetailBean.displayAllViewSplitFields}" styleClass="check">
						<a4j:support event="onclick" reRender="aMDetailList,pageInfo"
							actionListener="#{transactionDetailBean.displayViewSplitChange}"
							oncomplete="initScrollingTables();"/>
						</h:selectBooleanCheckbox>
						<h:outputText value="Display All Fields"/>
					</td>
					<td style="width:20px;">&#160;</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div id="table-four-content">
			<ui:include src="/WEB-INF/view/components/splitgroup_table.xhtml">
				<ui:param name="formName" value="transForm"/>
				<ui:param name="bean" value="#{transactionDetailBean}"/>
				<ui:param name="dataModel" value="#{transactionDetailBean.splitGroupDataModel}"/>
				<ui:param name="singleSelect" value="true"/>
				<ui:param name="multiSelect" value="false"/>
				<ui:param name="doubleClick" value="true"/>
				<ui:param name="selectReRender" value="viewBtn,viewSplitBtn,viewAllocBtn,closeBtn"/>
				<ui:param name="scrollReRender" value="pageInfo"/>
			</ui:include>
		</div>
		
		<div id="table-four-bottom">
		<ul class="right">
			<li class="viewtrans">
				<h:commandLink id="viewBtn" disabled="#{!transactionDetailBean.validSplitSelection}" action="#{transactionDetailBean.displaySplitViewAction}" />
			</li>
			<li class="viewsplit">
				<h:commandLink id="viewSplitBtn" disabled="#{!transactionDetailBean.displaySplitGroupViewSplit}" action="#{transactionDetailBean.splitGroupViewSplitAction}" />
			</li>
			<li class="viewalloc">
				<h:commandLink id="viewAllocBtn" disabled="#{!transactionDetailBean.displaySplitGroupViewAlloc}" action="#{transactionDetailBean.splitGroupViewAllocAction}" />
			</li>
      		<li class="close">
        		<h:commandLink id="closeBtn" action="#{transactionDetailBean.closeSplitSubTransIdAction}" />
      		</li>
		</ul>
		</div>
		</div>
		</div>
	</h:form>
	
	
</ui:define>

</ui:composition>
</html>