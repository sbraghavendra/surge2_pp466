<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="applyTaxMatrixFormScreen">

<h1><img id="imgTransactionProcess" alt="Apply a TaxCode to a Suspended Transaction" src="../images/headers/hdr-transaction-process.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	
	<div id="embedded-table">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				<td colspan="4"><h:outputText value="Apply a TaxCode to a Suspended Transaction"/></td>
				<td colspan="4" style="text-align:right;"><h:outputText value="Transaction ID: #{transactionDetailBean.currentTransaction.purchtransId}"/></td>
			</tr>
		</thead>	
	</table>
	
	<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">	
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td colspan="5">			
					<h:panelGroup>
					<ui:include src="/WEB-INF/view/components/taxcode_code.xhtml">
						<ui:param name="handler" value="#{transactionDetailBean.newTaxCodeHandler}"/>
						<ui:param name="id" value="taxCode"/>
						<ui:param name="readonly" value="false"/>
						<ui:param name="label" value="TaxCode State"/>
						<ui:param name="type" value="TaxCode Type"/>
						<ui:param name="TaxCode" value="TaxCode"/>
						<ui:param name="required" value="false"/>
						<ui:param name="allornothing" value="false"/>
						<ui:param name="cchrequired" value="false"/>
						<ui:param name="cchreadonly" value="false"/>
						<ui:param name="showheaders" value="true"/>
						
						<ui:param name="forId" value="false"/>
						<ui:param name="cchRead" value="true"/>
						<ui:param name="popupName" value="searchTaxCodeNew"/>
						<ui:param name="popupForm" value="searchTaxCodeFormNew"/>
					</ui:include>
					</h:panelGroup>				
				</td>
					
				<td>
					<h:commandButton id="taxCodeApplyBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
						image="/images/search_small.png" immediate="true" 
						action="#{transactionDetailBean.findTaxCodeApplyAction}">
					</h:commandButton>
				</td>
				<td class="column-spacer" width="20%">&#160;</td>		
			</tr>
		</tbody>
	</table>
		
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td colspan="6"><h:outputText value="Comments:"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>			
			<tr>
				<td class="column-spacer">&#160;</td>
				<td colspan="6"><h:inputTextarea id="comment" style="width: 100%;padding: 0px;" rows="4" value="#{transactionDetailBean.comments}"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>		
		</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td colspan="3">
					<h:outputText value="#{transactionDetailBean.currentTransactionIndex} of #{transactionDetailBean.totalSelected} Transactions Selected"/>
				</td>
				<td colspan="3">
					<h:selectBooleanCheckbox id="applyToAll" styleClass="check" title="Apply to All Remaining Transactions" value="#{transactionDetailBean.applyToAll}">
						<a4j:support event="onchange" action="#{transactionDetailBean.applyToAllChanged}"/>
					</h:selectBooleanCheckbox>
					<h:outputText value="Apply to All Remaining Transactions"/>
				</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	</div>
	
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{transactionDetailBean.applyTaxMatrixAction}" 
			onclick="Richfaces.showModalPanel('loader');"  /></li>
		<li class="cancel-all"><a4j:commandLink id="btnCancelAll" immediate="true" action="#{transactionDetailBean.cancelAll}"/></li> 
		<li class="cancel"><a4j:commandLink id="btnCancel" immediate="true" action="#{transactionDetailBean.nextTransactionApplyTaxCodeActionForPage}"/></li>
	</ul>
	<rich:modalPanel id="loader" zindex="2000" autosized="true">
        <h:outputText value="Processing..."/>
    </rich:modalPanel>
	</div>
	</div>
</div>
</h:form>

</ui:define>
</ui:composition>
</html>