<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<f:view>
<h:form id="filterUpdateForm">
	<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="8">Datagrid View Maintenance</td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="8">View Definition</th>
		</tr>
		<tr>
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">Screen Name:</td>
          <td><h:inputText style="width: 233px;" id="selectedFilterId" disabled="true" value="#{selectionColumnBean.screenNameDescription}" /></td>
          <td>&#160;</td>
          
          <td style="width: 10px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">User Name:</td>
          <td class="column-input2-search">
				<h:selectOneMenu id="UserNameId" immediate="true" disabled="#{!selectionColumnBean.isSelectUserNameEnabled}" 
						value="#{selectionColumnBean.selectedUserName}">
					<f:selectItems value="#{selectionColumnBean.userNameList}"/>	
					<a4j:support event="onchange" status="rowstatus" actionListener="#{selectionColumnBean.userNameChangeListener}"
						reRender="filterUpdateForm"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 10px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
       </tr>
       <tr>   
          <td style="width: 20px;">&#160;</td>
          <td style="width: 70px;">View Name:</td>
          <td class="column-input2-search">
				<h:selectOneMenu id="FilterNameId" immediate="true" disabled="false" 
						value="#{selectionColumnBean.selectedColumnName}">
					<f:selectItems value="#{selectionColumnBean.columnNameList}"/>
					<a4j:support event="onchange" actionListener="#{selectionColumnBean.columnNameChangeListener}"
						reRender="filterUpdateForm"/>
				</h:selectOneMenu>
          </td>
               
          <td class="column-input2-search">
          	<c:if test="#{selectionColumnBean.displayAddButton}" >
           		<h:inputText  id="newFilterNameId" maxlength = "100" value="#{selectionColumnBean.createdColumnName}"></h:inputText>
            </c:if>
			&#160;
          </td>
          
          <td>&#160;</td>
          <td>&#160;</td>
          <td style="width:10%;">&#160;</td>
        </tr>
        
        <tr>
			<td colspan="8" style="text-align: center;" >&#160;</td>
		</tr>

		<tr>
			<th colspan="8">Column Selection:</th>
		</tr>
	
		<!--  
		<tr>
			<td colspan="8" >
			<div class="scrollContainer">
			<div class="scrollInner">
	
				<rich:dataTable rowClasses="odd-row,even-row" 
					id="columnsDataTable"
					value="#{selectionColumnBean.dwFilterList}" var="dwFilter">
						
					<rich:column id="dwFilterColumn" style="width:250px">
						<f:facet name="header"><h:outputText value="Column Name" /></f:facet>
						<h:outputText value="#{dwFilter.columnName}" />
					</rich:column>
			
					<rich:column id="dwFilterValue" style="width:680px">
						<f:facet name="header"><h:outputText value="Filter Value" /></f:facet>
							<h:outputText value="#{dwFilter.filterValue}" />
					</rich:column>

				</rich:dataTable>

			</div>
			</div>
			</td>
		</tr>
		-->
		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	<a4j:outputPanel id="columnMappings">
	<!--<div id="table-four-content">-->
		<!--<div class="scrollContainer">-->
			<!--<div class="scrollInner" id="resize1">-->
			
				<table style="width: 100%; text-align: left">
              		<tr>
                		<td style="text-align: center;"><h:outputLabel id="hiddenColumnCount" value="#{selectionColumnBean.hiddenColumnCount}" />&#160;Hidden Column(s)</td>
                		<td style="width:10%;">&#160;</td>
                		<td style="text-align: center;"><h:outputLabel id="displayedColumnCount" value="#{selectionColumnBean.displayedColumnCount}" />&#160;Displayed Column(s)</td>
                		<td style="width:10%;">&#160;</td>
              		</tr>
              		<tr>
                		<td style="width:40%;">
                			<div class="scrollContainer">
                    		<div id="selectImportColScroller" class="scrollInner" style="height: 540px" >
                			<rich:dataTable rowClasses="odd-row,even-row" id="hiddenColumnsDataTable"
								value="#{selectionColumnBean.dwHiddenList}" var="dwFilter">
								
								<a4j:support id="a4j-selectMapping"
                                     actionListener="#{selectionColumnBean.importColumnChanged}"
                                     reRender="assignAction"
                                     onsubmit="selectRow('form:hiddenColumnsDataTable', this);"
                                     event="onRowClick" />
								
									
								<rich:column id="dwHiddenColumn" style="width:250px" sortBy="#{dwFilter.columnName}" >
									<f:facet name="header"><h:outputText styleClass="sort-local"  value="Column Name" /></f:facet>
									<h:outputText value="#{dwFilter.columnName}" />
								</rich:column>
						
								<rich:column id="dwHiddenValue" style="width:250px;" sortBy="#{dwFilter.filterValue}" >
									<f:facet name="header" ><h:outputText styleClass="sort-local" value="Description" /></f:facet>
										<h:outputText value="#{dwFilter.filterValue}" />
								</rich:column>			
							</rich:dataTable>
							</div>
                  			</div>
							
                		</td>
                		<td style="width:10%;">
                			<a4j:outputPanel id="assignOutputPanel" ajaxRendered="true">
                    		<ul class="block-list" style="margin-top: 10em; margin: auto;">
                      			<li>
                        			<a4j:commandButton id="assignAction" disabled="#{!selectionColumnBean.displayEditButton}"
                                           image="/images/actionbuttons/NCSTSAssign.JPG"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="hiddenColumnsDataTable, displayedColumnsDataTable, hiddenColumnCount, displayedColumnCount"
                                           action="#{selectionColumnBean.assignAction}" /> 
                      				</li>
                      			<li>
                        			<a4j:commandButton id="assignAllAction" disabled="#{!selectionColumnBean.displayEditButton}"
                                           image="/images/actionbuttons/NCSTSAssignAll.jpg"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="hiddenColumnsDataTable, displayedColumnsDataTable, hiddenColumnCount, displayedColumnCount"
                                           action="#{selectionColumnBean.assignAllAction}" />
                      			</li>
                      			<li>
                        			<a4j:commandButton id="removeAction" disabled="#{!selectionColumnBean.displayEditButton}"
                                           image="/images/actionbuttons/NCSTSRemove.JPG"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="hiddenColumnsDataTable, displayedColumnsDataTable, hiddenColumnCount, displayedColumnCount"
                                           action="#{selectionColumnBean.removeAction}" />
                      			</li>
                      			<li>
                        			<a4j:commandButton id="removeAllAction" disabled="#{!selectionColumnBean.displayEditButton}"
                                           image="/images/actionbuttons/NCSTSRemoveAll.JPG"
                                           type="button"
                                           onclick="if (! confirm('Are you sure you want to Remove All?')) {return false}"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="hiddenColumnsDataTable, displayedColumnsDataTable, hiddenColumnCount, displayedColumnCount"
                                           action="#{selectionColumnBean.removeAllAction}" />
                      			</li>
                    		</ul>
                  			</a4j:outputPanel>
                		
                		</td>
                		<td  style="width:40%;">
                			<div class="scrollContainer">
                    		<div id="mappingScroller" class="scrollInner" style="height: 540px" >
                			<rich:dataTable rowClasses="odd-row,even-row" id="displayedColumnsDataTable"
								value="#{selectionColumnBean.dwFilterList}" var="dwFilter">
								
								<a4j:support id="a4j-selectMapping"
                                     actionListener="#{selectionColumnBean.mappingColumnChanged}"
                                     reRender="removeAction"
                                     onsubmit="selectRow('form:displayedColumnsDataTable', this);"
                                     event="onRowClick" />
									
								<rich:column id="dwDisplayedColumn" style="width:250px">
									<f:facet name="header"><h:outputText styleClass="sort-local"  value="Column Name" /></f:facet>
									<h:outputText value="#{dwFilter.columnName}" />
								</rich:column>
						
								<rich:column id="dwDisplayedValue" style="width:250px;">
									<f:facet name="header" ><h:outputText styleClass="sort-local" value="Description" /></f:facet>
										<h:outputText value="#{dwFilter.filterValue}" />
								</rich:column>		
							</rich:dataTable>
							</div>
                  			</div>
                		</td>
                		<td>
                			<table style="width: 30px; text-align: left">
                				<tr>	
						        	<td width="15px">
						        		<a4j:commandLink  id="upupArrow" styleClass="button" disabled="#{!selectionColumnBean.displayEditButton}"
						        			reRender="displayedColumnsDataTable" actionListener="#{selectionColumnBean.processUpUpReorderCommand}" >
						        			<h:graphicImage value="/images/icon_arrow-top.png" />	
						        		</a4j:commandLink >		  
						        	</td>
                				 </tr>
                				<tr>
						        	<td width="15px">
						        		<a4j:commandLink  id="upArrow" styleClass="button" disabled="#{!selectionColumnBean.displayEditButton}"
						        			reRender="displayedColumnsDataTable" actionListener="#{selectionColumnBean.processUpReorderCommand}" >
						        			<h:graphicImage value="/images/icon_arrow-up.jpg" />	
						        		</a4j:commandLink >  		  
						        	</td>
						         </tr>
                				<tr>	
									<td width="15px">
						            	<a4j:commandLink id="downArrow" styleClass="button" disabled="#{!selectionColumnBean.displayEditButton}"
						            		reRender="displayedColumnsDataTable" actionListener="#{selectionColumnBean.processDownReorderCommand}" >
						            		<h:graphicImage value="/images/icon_arrow-down.jpg" />
						            	</a4j:commandLink>
						            </td>
								 </tr>	
              					<tr>        		
			                		<td width="15px">
						            	<a4j:commandLink id="downdownArrow" styleClass="button" disabled="#{!selectionColumnBean.displayEditButton}"
						            		reRender="displayedColumnsDataTable" actionListener="#{selectionColumnBean.processDownDownReorderCommand}" >
						            		<h:graphicImage value="/images/icon_arrow-bottom.png" />
						            	</a4j:commandLink>
						            </td>
								</tr>   
                			</table>
                		</td>
              		</tr>
				</table>
	
				

			<!--</div>-->
		<!--	</div>-->
	
	<!--</div> table-four-content -->
	</a4j:outputPanel>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="update"><h:commandLink id="okUpdateBtn" disabled="#{!selectionColumnBean.displayUpdateButton}" action="#{selectionColumnBean.okUpdateAction}"/></li>
		<li class="add"><h:commandLink id="okAddBtn" disabled="#{!selectionColumnBean.displayAddButton}" action="#{selectionColumnBean.okAddAction}"/></li>
<!--  	<li class="delete115"><h:commandLink id="okDeleteBtn" disabled="#{!selectionColumnBean.displayDeleteButton}" action="#{selectionColumnBean.deleteAction}"/></li> -->
		<li class="delete115">
				<a4j:commandLink id="okDeleteBtn"
 					style="#{!selectionColumnBean.displayDeleteButton? 'display:none;':'display:block;'}" 
					oncomplete="javascript:Richfaces.showModalPanel('executeDelete');" 
					reRender="executeDeleteForm"/>
			</li> 
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{selectionColumnBean.cancelAction}" /></li>	
		
		 
						  
	</ul>
	</div>

	</div>
	</div>
	
	<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
    	</rich:modalPanel>

</h:form>
</f:view>


<rich:modalPanel id="executeDelete" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Delete" />
	</f:facet>
	<h:form id="executeDeleteForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('executeDelete');" action="#{selectionColumnBean.deleteAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('executeDelete'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>

	
</ui:define>
</ui:composition>
</html>
