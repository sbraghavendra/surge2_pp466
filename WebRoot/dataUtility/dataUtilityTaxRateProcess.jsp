<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgTaxRateUpdates" alt="Tax Rate Updates" src="../images/headers/hdr-tax-rate-updates.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">Confirm Submit Tax Rate Update Batch(s)</li>
                  <li class="sub-heading-dark">Batch Settings</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">

			            <h:outputLabel value="Batch Type Desc:" />
                  <h:inputText size="40" id="txtBatchTypeDesc"
                               value="#{cacheManager.listCodeMap['BATCHTYPE']['TR'].description}" 
                               disabled="#{true}" />

					        <h:outputLabel value="Batch Status Desc:" />
                  <h:inputText size="40" id="txtBatchStatusDesc"
                               value="#{cacheManager.listCodeMap['BATCHSTAT']['FP'].description}" 
                               disabled="#{true}" />

					        <h:outputLabel value="Start Date/Time: "/>
                  <rich:calendar id="cDate"
                                 popup="true"
                                 rendered="true"
                                 binding="#{dataUtilityTaxRateUpdatesBean.batchStartTime}"
        						             enableManualInput="true"
                                 converter="dateTime"
                                 datePattern="MM/dd/yyyy hh:mm a"
                                 disabled="#{! dataUtilityTaxRateUpdatesBean.filePreferenceBean.batchPreferenceDTO.canChangeBatchStartTime}"/>
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-dark">Selected batch(s)</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner" id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable" 
                                    style="margin: 10px"
                                    value="#{dataUtilityTaxRateUpdatesBean.selectedBatches}" 
                                    var="row">
                      
                      <rich:column id="batchId"> 
                        <f:facet name="header">
                          <h:outputText value="Batch Id"/>
                        </f:facet>
                        <h:outputText value="#{row.batchId}" id="rowBatchId"
                                      style="#{empty row.error ? '' : 'color: red'}"/>
                      </rich:column>

                      <rich:column styleClass="column-right">
                        <f:facet name="header">
                          <h:outputText value="Import Bytes" />
                        </f:facet>
                        <h:outputText value="#{row.totalBytes}" id="rowTotalBytes"
                                      style="#{empty row.error ? '' : 'color: red'}">
                          <f:convertNumber />
                        </h:outputText>
                      </rich:column>

                      <rich:column styleClass="column-right">
                        <f:facet name="header">
                          <h:outputText value="Import Rows" />
                        </f:facet>
                        <h:outputText value="#{row.totalRows}" id="rowTotalRows"
                                      style="#{empty row.error ? '' : 'color: red'}">
                          <f:convertNumber/>
                        </h:outputText>
                      </rich:column>

                      <rich:column>
                        <f:facet name="header">
                          <h:outputText value="Release date" />
                        </f:facet>
                        <h:outputText value="#{row.releaseDate}" id="rowReleaseDate"
                                      style="#{empty row.error ? '' : 'color: red'}">
                          <f:convertDateTime pattern="yyyy-MM" />
                        </h:outputText>
                      </rich:column>

                      <rich:column>
                        <f:facet name="header">
                          <h:outputText value="Release version" />
                        </f:facet>
                        <h:outputText value="#{row.releaseVersion}" id="rowReleaseVersion"
                                      style="#{empty row.error ? '' : 'color: red'}"/>
                      </rich:column>
                      
                      <rich:column>
                        <f:facet name="header">
                          <h:outputText value="Error" />
                        </f:facet>
                        <h:outputText value="#{row.error}" id="rowError"
                                      style="#{empty row.error ? '' : 'color: red'}"/>
                      </rich:column>
      
                    </rich:dataTable>

                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="ok2">
                      <h:commandLink id="ok2"
                                     action="#{dataUtilityTaxRateUpdatesBean.submitProcessAction}" />
                    </li>
                    <li class="cancel">
                      <h:commandLink id="cancelAction" immediate="true"
                                     action="#{dataUtilityTaxRateUpdatesBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
              </div>
              </div>
              </div>
              <!--  content -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
