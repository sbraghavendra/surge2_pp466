<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   //]]>
   </script>
  </ui:define>
  <ui:define name="body">
 <h:form id="batchMaintStatusStatistics">
    <h1><img id="imgBatchStatistics" alt="Batch Statistics" src="../images/headers/hdr-batch-statistics.gif" width="250" height="19" /></h1>
    <div id="bottom">
      <div id="table-four">
        <div id="table-four-top">
          <a4j:outputPanel id="msg">
            <h:messages errorClass="error" />
          </a4j:outputPanel>
        </div>
        <div id="table-four-content">
          <ui:include src="/WEB-INF/view/components/batch_statistics.xhtml">
            <ui:param name="batchStatistics"
                      value="#{dataUtilitySitusRulesBean.batchStatistics}" />
          </ui:include>
        </div>
        <div id="table-four-bottom">
          <ul class="right">
		        <li class="cancel"><h:commandLink id="cancelAction" action="#{dataUtilitySitusRulesBean.cancelAction}" /></li>
		        <li class="refresh">
              <h:commandLink id="btnRefresh"
                 rendered="#{! dataUtilitySitusRulesBean.batchStatistics.batchPreferenceDTO.hasRefreshInterval}"
                 action="#{dataUtilitySitusRulesBean.refreshStatistics}"/>
            </li>
          </ul>
        </div>
      </div>
    </div>
 </h:form>
  </ui:define>
 </ui:composition>
</html>
