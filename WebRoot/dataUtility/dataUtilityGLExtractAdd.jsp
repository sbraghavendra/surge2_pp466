<?xml version="1.0" encoding="iso-8859-1"?>
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="body">
   <h:form id="extractform">
    <h1>
     <h:graphicImage id="imgCustomGlExtract" alt="Custom G/L Extract" value="/images/headers/hdr-custom-gl-extract.gif" />
    </h1>
    
    <div id="top">
     <div id="table-one">
      <div id="table-one-top">
        <a4j:outputPanel id="msg">
          <h:messages errorClass="error" />
        </a4j:outputPanel>
      </div>
      <div id="table-one-content">
        <h:panelGrid id="header"
                        columns="2"
                        styleClass="basic-form"
                        cellspacing="0"
                        cellpadding="0"
                        style="width: 100%;">
          <h:outputLabel value="&#160;&#160;G/L Through Date"/>
          <rich:calendar id="cDate"
                         popup="true"
                         oninputkeypress="return onlyDateValue(event);"
                         rendered="true"
                         binding="#{dataUtilityGLExtractBean.myCalendarDate}"
                         converter="date" datePattern="M/d/yyyy" style="width:0px;" />
          <h:outputLabel value="&#160;&#160;Extract Filename"/>
          <h:inputText id="fileName" value="#{dataUtilityGLExtractBean.fileName}" style="width:150px;"  />
          <h:outputLabel value="&#160;&#160;Return Message"/>
          <h:inputText id="extractedActionMessageId" value="#{dataUtilityGLExtractBean.extractedActionMessage}" disabled="true" style="width:795px; color:red"  />
        </h:panelGrid>
      </div>
      <div id="table-one-bottom">
       <ul class="right">
        <li class="extract"><h:commandLink id="btnExtract" rendered="#{!dataUtilityGLExtractBean.extractedAction}" action="#{dataUtilityGLExtractBean.extractAction}"
        									onclick="javascript:Richfaces.showModalPanel('extractloader');" disabled="false" reRender = "extractform"/></li>
     	 <rich:modalPanel id="extractloader" zindex="2000" autosized="true">
              <h:outputText value="Extracting...."/>
    	 </rich:modalPanel>
        <li class="cancel"><h:commandLink id="btnCancel" rendered="#{!dataUtilityGLExtractBean.extractedAction}" immediate="true" action="#{dataUtilityGLExtractBean.closeAction}" /></li>
        <li class="close2"><h:commandLink id="btnClose" rendered="#{dataUtilityGLExtractBean.extractedAction}" immediate="true"   action="#{dataUtilityGLExtractBean.closeAction}" /></li>
       </ul>
      </div>
     </div>
    </div>
   </h:form>
  </ui:define>
 </ui:composition>
</html>