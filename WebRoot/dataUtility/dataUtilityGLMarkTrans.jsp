<?xml version="1.0" encoding="iso-8859-1"?>
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('glStatusForm:batchTable', 'selectedRowIndex'); } );
registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body"><h:inputHidden id="selectedRowIndex" value="#{dataUtilityGLMarkExtractBean.selectedBatchIndex}" />
<h1><h:graphicImage id="imgGlMarkTrans" alt="G/L Mark Trans" value="/images/headers/hdr-glmark-transactions.png" /></h1>
<h:form id="glStatusForm">
<div id="bottom">
 <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
    <ui:param name="bean" value="#{dataUtilityGLMarkExtractBean}"/>
    <ui:param name="batchTypeCodeParam" value="GM"/>
 </a4j:include>
   <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
     	<div id="table-four">
      		<div id="table-four-top">
	      		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	        
	      		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	      		<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilityGLMarkExtractBean.batchMaintenanceDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />      
	      	</div>
	      	
	      	<div id="table-four-content">
	       		<ui:include src="/WEB-INF/view/components/batch_status.xhtml">
	        	<ui:param name="bean" value="#{dataUtilityGLMarkExtractBean}" />
		  		<ui:param name="isGE" value="true" />
	        	<ui:param name="isTR" value="false" />
	        	<ui:param name="batchTypeCodeParam" value="GM"/>
	        	<ui:param name="onsubmit"  value="selectRow('glStatusForm:batchTable', this);" />
	        	<ui:param name="reRender" value="error" />
	       		</ui:include>
	      	</div>
      	
	       	<div id="table-four-bottom">
	        	<ul class="right">
	         		<li class="error3"><h:commandLink id="error" disabled="#{! dataUtilityGLMarkExtractBean.displayError}" action="#{dataUtilityGLMarkExtractBean.errorsMarkTransAction}" /></li>
	         		<li class="add"><h:commandLink id="btnNewAdd" action="#{dataUtilityGLMarkExtractBean.addMarkTransAction}" disabled = "#{dataUtilityGLMarkExtractBean.currentUser.viewOnlyBooleanFlag}"
	         										style="#{(dataUtilityGLMarkExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" /></li>
	        	</ul>
	       	</div>
     	</div>
	</div>
</h:form>
</ui:define>
</ui:composition>
</html>
