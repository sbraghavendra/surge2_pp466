<?xml version="1.0" encoding="iso-8859-1"?>
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('glStatusForm:batchTable', 'selectedRowIndex'); } );
registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body"><h:inputHidden id="selectedRowIndex" value="#{dataUtilityGLExtractBean.selectedBatchIndex}" />
<h1><h:graphicImage id="imgGlExtract" alt="G/L Extract" value="/images/headers/hdr-gl-extract.gif" /></h1>
<h:form id="glStatusForm">
	 <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
    	 <ui:param name="bean" value="#{dataUtilityGLExtractBean}"/>
     </a4j:include>
     <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
	<div id="bottom">
     	<div id="table-four">
      		<div id="table-four-top">
	      		<a4j:outputPanel id="msg"><h:message for = "glStatusForm" errorClass="error" /></a4j:outputPanel>
	        
	      		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	      		<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilityGLExtractBean.batchMaintenanceDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />      
	      	</div>
	      	
	      	<div id="table-four-content">
	       		<ui:include src="/WEB-INF/view/components/batch_status.xhtml">
	        	<ui:param name="bean" value="#{dataUtilityGLExtractBean}" />
		  		<ui:param name="isGE" value="true" />
	        	<ui:param name="isTR" value="false" />
	        	<ui:param name="onsubmit"  value="selectRow('glStatusForm:batchTable', this);" />
	        	<ui:param name="reRender" value="error,updateBatch" />
	       		</ui:include>
	      	</div>
      	
	       	<div id="table-four-bottom">
	        	<ul class="right">
					<li class="update">
						<h:commandLink id="updateBatch"
									   disabled="#{(dataUtilityGLExtractBean.selectedBatchIndex == -1 or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)}"
									   style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
									   immediate="true"
									   action="#{dataUtilityGLExtractBean.updateUserFieldsAction}" />
					</li>
	         		<li class="error3"><h:commandLink id="error" disabled="#{! dataUtilityGLExtractBean.displayError}" action="#{dataUtilityGLExtractBean.errorsAction}" /></li>
	         		<li class="add"><h:commandLink id="btnAdd" action="#{dataUtilityGLExtractBean.addAction}" 
	         		style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
	         		disabled = "#{dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}" /></li>
	         		<li class="refresh"><h:commandLink id="btnRefresh" action="#{dataUtilityGLExtractBean.refreshAction}" /></li>
	        	</ul>
	       	</div>
     	</div>
	</div>
</h:form>
</ui:define>
</ui:composition>
</html>
