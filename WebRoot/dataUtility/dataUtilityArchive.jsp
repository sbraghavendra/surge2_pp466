<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="duArchiveForm">
<h1><img src="../images/headers/hdr-batch-maintenance-status.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<rich:scrollableDataTable rowClasses="odd-row,even-row" styleClass="Tabruler"

																width="913px"

																frozenColCount="0"

																id="taxRateBatchTable" rows="20"

																headerClass="header"

																value="#{dataUtilityBackingBean.arBatchList}"

																var="batchMaintenance" 

																sortMode="single" 

																>

															
																<rich:column id="batchId">

																	<f:facet name="header" >

																		<h:outputText id="id2x" styleClass="headerText" value="Batch Id" />

																	</f:facet>

																	<h:outputText style="width:20px;"

																		value="#{batchMaintenance.batchId}">

																	</h:outputText>

																</rich:column>



																<rich:column id="batchTypeDesc">

																	<f:facet name="header">

																		<h:outputText id="id3x" styleClass="headerText" value="Batch Type Desc" />

																	</f:facet>

																	<h:outputText style="width:87px;"

																		value="#{batchMaintenance.batchTypeDesc}" />

																</rich:column>

																<rich:column id="entryTimeStamp" style="width:130px;">

																	<f:facet name="header">

																		<h:outputText id="id4x" styleClass="headerText" value="Entry Timestamp" />

																	</f:facet>

																	<h:outputText style="width:130px;"

																		value="#{batchMaintenance.entryTimestamp}" />

																</rich:column>

																<rich:column id="schedStart" style="width:130px;">

																	<f:facet name="header">

																		<h:outputText id="id5x" styleClass="headerText" value="Sched Start" />

																	</f:facet>

																	<h:outputText style="width:130px;" styleClass="dateFiledColTxt"

																		value="#{batchMaintenance.schedStartTimestamp}" />

																</rich:column>

																

																<rich:column id="actualStart" style="width:130px;">

																	<f:facet name="header">

																		<h:outputText id="id7x" styleClass="headerText" value="Actual Start" />

																	</f:facet>

																	<h:outputText style="width:130px;"

																		value="#{batchMaintenance.actualStartTimestamp}" />

																</rich:column>

																<rich:column id="batchStatusDesc">

																	<f:facet name="header">

																		<h:outputText id="id8x" styleClass="headerText" value="Batch Status Desc" />

																	</f:facet>

																	<h:outputText style="width:100px;"

																		value="#{batchMaintenance.batchStatusDesc}" />

																</rich:column>

																<rich:column id="held">

																	<f:facet name="header">

																	<h:outputText id="tbl2" styleClass="headerText" value="Held"/>

																	</f:facet>

																		<h:outputText style="width:108px;" 

																		value="#{batchMaintenance.held}"/>

																</rich:column>

																<rich:column id="somethingelse">

																	<f:facet name="header">

																	<h:outputText id="tbl3" styleClass="headerText" value="Something Else"/>

																	</f:facet>

																		<h:outputText style="width:108px;" 

																		value="#{batchMaintenance.someThingElse}"/>

																</rich:column>	
	</rich:scrollableDataTable>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="add2"><h:commandLink action="archive_add"/></li>
		<li class="select-all"><a href="#"></a></li>
		<li class="process2"><a href="#"></a></li>
		<li class="statistics2"><a href="#"></a></li>
		<li class="error2"><a href="#"></a></li>
		<li class="view115"><a href="#"></a></li>
		<li class="refresh"><a href="#"></a></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>