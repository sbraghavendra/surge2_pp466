<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgTaxRateUpdates" alt="TaxCode Rules Updates" src="../images/headers/hdr-tax-code-rules-updates.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
                
                <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       			<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilityTaxCodeRulesBean.bcpTaxCodeRuleDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />  
                
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">View a TaxCode Rules Updates Batch - Formatted</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="8"
                             styleClass="basic-form"
                             columnClasses="left-indent,right,right,right,right,right,right,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="20" id="txtBatchId"
                               value="#{dataUtilityTaxCodeRulesBean.selectedBatch.batchId}"
                               disabled="#{true}" />
                  <h:outputLabel value="Update Date:" />
                  <h:inputText size="20" id="txtUpdateDate"
                               value="#{dataUtilityTaxCodeRulesBean.selectedBatch.ts01}"
                               disabled="#{true}" />
                  <h:outputLabel value="Release:" />
                  <h:inputText size="20" id="txtRelease"
                               value="#{dataUtilityTaxCodeRulesBean.selectedBatch.nu01}"
                               disabled="#{true}" >
                               <f:convertNumber integerOnly="true" />
                  </h:inputText>             
                  <h:outputLabel value="File Type:" />
                  <h:inputText size="20" id="FileType"
                               value="#{dataUtilityTaxCodeRulesBean.selectedBatch.vc02}"
                               disabled="#{true}" />
                </h:panelGrid>
                <h:panelGrid id="batchFile"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="File Name:" />
                  <h:inputText size="92" id="txtFileName"
                               value="#{dataUtilityTaxCodeRulesBean.selectedBatch.vc01}"
                               disabled="#{true}" />
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-light">TaxCode Rules Updates</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner"
                       id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                                    border="1"
                                    sortmode="single"
                                    rows="#{dataUtilityTaxCodeRulesBean.bcpTaxCodeRuleDataModel.pageSize }"
                                    value="#{dataUtilityTaxCodeRulesBean.bcpTaxCodeRuleDataModel}"
                                    var="row">
                                    
                        <rich:column>
                        	<f:facet name="header">Country Code</f:facet>
                        	<h:outputText id="rowtaxcodeCountryCode" value="#{row.taxcodeCountryCode}" />
			            </rich:column>
			            
			            <rich:column>
			            	<f:facet name="header">List Code Description</f:facet>
                        	<h:outputText id="rowlcDescription" value="#{row.lcDescription}" />
			            </rich:column>  
                                    
	                    <rich:column>
                        	<f:facet name="header">State Code</f:facet>
                        	<h:outputText id="rowtaxcodeStateCode" value="#{row.taxcodeStateCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Geocode</f:facet>
                        	<h:outputText id="rowtsGeocode" value="#{row.tsGeocode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Name</f:facet>
                        	<h:outputText id="rowtsName" value="#{row.tsName}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Local Taxability Code</f:facet>
                        	<h:outputText id="rowtsLocalTaxabilityCode" value="#{row.tsLocalTaxabilityCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode State Active Flag</f:facet>
                        	<h:outputText id="rowtsActiveFlag" value="#{row.tsActiveFlag}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode Code</f:facet>
                        	<h:outputText id="rowtaxcodeCode" value="#{row.taxcodeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode Description</f:facet>
                        	<h:outputText id="rowtcDescription" value="#{row.tcDescription}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode Comments</f:facet>
                        	<h:outputText id="rowtcComments" value="#{row.tcComments}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode Active Flag</f:facet>
                        	<h:outputText id="rowtcActiveFlag" value="#{row.tcActiveFlag}" />
			            </rich:column>
	
			            <rich:column>
                        	<f:facet name="header">Sort No</f:facet>
                        	<h:outputText id="rowTdSortNo" value="#{row.tdSortNo}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Jur Level</f:facet>
                        	<h:outputText id="rowTdJurLevel" value="#{row.tdJurLevel}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">County</f:facet>
                        	<h:outputText id="rowtdTaxcodeCounty" value="#{row.tdTaxcodeCounty}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">City</f:facet>
                        	<h:outputText id="rowtdTaxcodeCity" value="#{row.tdTaxcodeCity}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">STJ</f:facet>
                        	<h:outputText id="rowtdTaxcodeStj" value="#{row.tdTaxcodeStj}" />
			            </rich:column>
	
			            <rich:column>
                        	<f:facet name="header">Effective Date</f:facet>
                        	<h:outputText id="rowtdEffectiveDate" value="#{row.tdEffectiveDate}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Expiration Date</f:facet>
                        	<h:outputText id="rowtdExpirationDate" value="#{row.tdExpirationDate}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxcode Type Code</f:facet>
                        	<h:outputText id="rowtdTaxcodeTypeCode" value="#{row.tdTaxcodeTypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Override Taxtype Code</f:facet>
                        	<h:outputText id="rowtdOverrideTaxtypeCode" value="#{row.tdOverrideTaxtypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Rate Type Code</f:facet>
                        	<h:outputText id="rowtdRatetypeCode" value="#{row.tdRatetypeCode}" />
			           	</rich:column>
			            
	                    <rich:column>
                        	<f:facet name="header">Taxable Threshold Amt</f:facet>
                        	<h:outputText id="rowtdTaxableThresholdAmt" value="#{row.tdTaxableThresholdAmt}"/>
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Taxable Limitation Amt</f:facet>
                        	<h:outputText id="rowtdTaxableLimitationAmt" value="#{row.tdTaxableLimitationAmt}" />
			            </rich:column>
	                    
	                    <rich:column>
                        	<f:facet name="header">Tax Limitation Amt</f:facet>
                        	<h:outputText id="rowtdTaxLimitationAmt" value="#{row.tdTaxLimitationAmt}" />
			            </rich:column>
	                    
	                    <rich:column>
                        	<f:facet name="header">Cap Amt</f:facet>
                        	<h:outputText id="rowtdCapAmt" value="#{row.tdCapAmt}" />
			            </rich:column>
	                    
	                    <rich:column>
                        	<f:facet name="header">Base Change Pct</f:facet>
                        	<h:outputText id="rowtdBaseChangePct" value="#{row.tdBaseChangePct}" />
			            </rich:column>
	                    
	                    <rich:column>
                        	<f:facet name="header">Special Rate</f:facet>
                        	<h:outputText id="rowtdSpecialRate" value="#{row.tdSpecialRate}" />
			            </rich:column>
	                    
	                    <rich:column>
                        	<f:facet name="header">Update Code</f:facet>
                        	<h:outputText id="rowupdateCode" value="#{row.updateCode}" />
			            </rich:column>
                    </rich:dataTable>
                  </div>
		              <rich:datascroller id="scroll" for="batchTable" maxPages="10" oncomplete="initScrollingTables();"
			                               align="center" stepControls="auto" ajaxSingle="false" 
                                     reRender="pageInfo"
			                               page="#{dataUtilityTaxCodeRulesBean.bcpTaxCodeRuleDataModel.curPage}" />
                </div>
              </div>
              <!--  content -->
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="unformatted">
                      <h:commandLink id="viewufr"
                                     action="#{dataUtilityTaxCodeRulesBean.viewUnformattedAction}" />
                    </li>
                    <li class="ok2">
                      <h:commandLink id="cancelAction"
                                     action="#{dataUtilityTaxCodeRulesBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
