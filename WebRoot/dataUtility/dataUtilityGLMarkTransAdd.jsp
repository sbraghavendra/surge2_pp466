<?xml version="1.0" encoding="iso-8859-1"?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
	<ui:define name="body">
		<h:form id="glmarkform">
			<h1>
				<h:graphicImage id="imgCustomGlExtract" alt="Custom G/L Extract"
					value="/images/headers/hdr-glmark-transactions.png" />
			</h1>
			<div id="top" style = "width:913px">
				<div id="table-one">
					<div id="table-four-top" style="padding: 1px 10px 1px 2px;">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;"><tr>
			<td ><h:messages errorClass="error" /></td>
			<td style="width:100%;">&#160;</td>
			<td width="70"><h:outputText style="color:red;" value="*Mandatory&#160;&#160;&#160;&#160;"/></td>
			
		</tr></table>
	</div>
				</div>
				<div id="table-one-content">
					<table cellpadding="0" cellspacing="0" width="913" id="rollover"
						class="ruler">
						<thead>
							<tr>
								<td colspan="7"><h:outputText
										value="#{DataUtilityGLExtractBackingBean.actionText}" /> Add a G/L Mark Transactions Batch</td>
							</tr>
						</thead>
						<div>
							<tr>
								<th colspan="1" style="font-weight: bold;width:80px;">Date Options</th>
								<th colspan="5">&#160;</th>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-label"><h:outputText style="color:red;" value="*&#160;"/>G/L Through Date:</td>
								<td><rich:calendar id="cDate" popup="true" disabled="#{dataUtilityGLMarkExtractBean.searched}"
										oninputkeypress="return onlyDateValue(event);" rendered="true" value="#{dataUtilityGLMarkExtractBean.myCalendar}"
										binding="#{dataUtilityGLMarkExtractBean.myCalendarDate}"
										converter="date" datePattern="M/d/yyyy" style="width:0px;" /></td>
							</tr>

							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-label"><h:outputText style="color:red;" value="*&#160;"/>Fiscal Period:</td>
								<td><h:selectOneMenu id="yearPeriodMain" 
								value="#{dataUtilityGLMarkExtractBean.filterYear}" disabled="#{dataUtilityGLMarkExtractBean.searched}"
								immediate="true">
							  
										<f:selectItems
											value="#{dataUtilityGLMarkExtractBean.filterYearPeriodMenuItems}" />
									</h:selectOneMenu></td>
							</tr>
							<tr>
								<td colspan="7">
									<div id="table-four-bottom">
										<ul class="right">
											<!--li class="extract"><h:commandLink id="btnExtract" rendered="#{!dataUtilityGLMarkExtractBean.extractedAction}" action="#{dataUtilityGLMarkExtractBean.extractAction}" disabled="false" /></li>
        <li class="cancel"><h:commandLink id="btnCancel" rendered="#{!dataUtilityGLMarkExtractBean.extractedAction}" immediate="true" action="#{dataUtilityGLMarkExtractBean.closeAction}" /></li>
        <li class="close2"><h:commandLink id="btnClose" rendered="#{dataUtilityGLMarkExtractBean.extractedAction}" immediate="true"   action="#{dataUtilityGLMarkExtractBean.closeAction}" /></li>-->
        
        									<li class="clear2"><h:commandLink id="btnClear" action="#{dataUtilityGLMarkExtractBean.resetFilterGLExtract}" /></li>
											<li class="search"><h:commandLink id="searchBtn" action="#{dataUtilityGLMarkExtractBean.retrieveFilterGLExtract}" /></li>
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<th colspan="1" style="font-weight: bold;width:30px;">Results&#160;</th>
								<th colspan="5">&#160;</th>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-label">&#160;&#160;Transaction ID Count:</td>
								<td><h:inputText id="trasactionId"
										style="text-align:right;"
										value="#{dataUtilityGLMarkExtractBean.transactionCount}" disabled="true"
										onkeypress="return onlyIntegerValue(event);" /></td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-label">&#160;&#160;Record Count:</td>
								<td><h:inputText id="recordCount" style="text-align:right;" disabled="true"
										value="#{dataUtilityGLMarkExtractBean.recordCount}"
										onkeypress="return onlyIntegerValue(event);" /></td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-label">&#160;&#160;Tax Amount:</td>
								<td><h:inputText id="taxAmount" style="text-align:right;" disabled="true"
										value="#{dataUtilityGLMarkExtractBean.taxAmount}"
										onkeypress="return onlyIntegerValue(event);" >
									<f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" /></h:inputText>	
								</td>
							</tr>
							<tr>
								<th colspan="1" style="font-weight: bold;">Transactions</th>
								<th colspan="5">&#160;</th>
							</tr>
						</div>
					</table>
				</div>
			</div>
			<div id="bottom">
				<div id="table-four">
					<div id="table-four-content">					
						 <table cellpadding="0" cellspacing="0" width="100%" id="rollover"
							class="ruler">
							<tbody>  
								<tr>
									<th align="left" width="350"><a4j:status id="pageInfo"
											startText="Request being processed..."
											stopText="#{dataUtilityGLMarkExtractBean.glExtractDataModel.pageDescription}"
											onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
											onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
									</th>		
								</tr>								
							</tbody>
						</table> 

						 <ui:include
							src="/WEB-INF/view/components/transactiondetaillog_table.xhtml">
							<ui:param name="formName" value="glmarkform" />
							<ui:param name="bean" value="#{dataUtilityGLMarkExtractBean}" />
							<ui:param name="dataModel" value="#{dataUtilityGLMarkExtractBean.glExtractDataModel}" />
							<ui:param name="singleSelect" value="true" />
							<ui:param name="multiSelect" value="false" />
							<ui:param name="doubleClick" value="false" />
							<ui:param name="selectReRender" value="btnMark,btnCancel" />
							<ui:param name="scrollReRender" value="pageInfo" />
						</ui:include>  
					
					</div>	
				   	<div id="table-four-bottom">
			        	<ul class="right">
							<li class="mark"><h:commandLink id="btnMark"  disabled="#{!dataUtilityGLMarkExtractBean.displayMarkAction}"  action="#{dataUtilityGLMarkExtractBean.markAction}"/></li>	
			        		<li class="cancel"><h:commandLink id="btnCancel" immediate="true"  action="#{dataUtilityGLMarkExtractBean.cancelMarkTransAction}" /></li>
						</ul>
					</div>			
				</div>
			</div>
		</h:form>
	</ui:define>
</ui:composition>
</html>