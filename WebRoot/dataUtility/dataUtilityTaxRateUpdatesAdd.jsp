<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
 <ui:define name="script">
 <script type="text/javascript">
 //<![CDATA[
function copyMe(from,to){
	var fromValue = document.getElementById(from).value;
	var lastIndex = fromValue.lastIndexOf("\\");
  	if(lastIndex > -1){	
    	document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    }
    else{
    	lastIndex = fromValue.lastIndexOf("//");
    	if(lastIndex > -1){	
    		document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    	}
    	else{
    		document.getElementById(to).value = fromValue;
    	}
    }
}
//]]>
 </script>
 </ui:define>
    <ui:define name="body">
      <h:form enctype="multipart/form-data"
              id="form">
        <h1>
          <img id="imgTaxRateUpdates" alt="Tax Rate Updates" src="../images/headers/hdr-tax-rate-updates.gif"
               width="250"
               height="19" />
        </h1>
        <div id="bottom">
          <div id="table-four">
            <div id="table-four-top">
              <a4j:outputPanel id="msg">
                <h:messages errorClass="error" />
              </a4j:outputPanel>
            </div>
            <div id="table-one-content">
              <table cellpadding="0"
                     cellspacing="0"
                     width="100%"
                     id="rollover"
                     class="ruler">
                <thead>
                  	<tr>
                    	<td colspan="5">Import Jurisdictions &#38; Tax Rates Updates File</td>
                  	</tr>
                </thead>
                <tbody>
                  	<tr>
                    	<th colspan="5">Select File to Import</th>
                  	</tr>
                  	<tr>
                    	<td style="width: 50px;">&#160;</td>
                    	<td>Last Year_Month/Release Processed:</td>
                    	<td>
                      		<h:inputText size="20" id="txtLastTaxRateRelUpdate"
                                   value="#{dataUtilityTaxRateUpdatesBean.lastTaxRateDateUpdate}&#160;/&#160;#{dataUtilityTaxRateUpdatesBean.lastTaxRateRelUpdate}"
                                   disabled="true" />
                    	</td>
                    	<td>&#160;</td>
                    	<td style="width: 30%;">&#160;</td>
                  	</tr>
                  	<tr>
                    	<td>&#160;</td>
                    	<td>File Name:</td>
                      	<td colspan="2">
                      		<h:inputText id="fileName"
				                label="File Name"
				                value="#{dataUtilityTaxRateUpdatesBean.fileName}"
				                disabled="#{dataUtilityTaxRateUpdatesBean.displayOk || dataUtilityTaxRateUpdatesBean.displayClose}"
				                required="true"
				                size="70" />
                      	
                        	<t:inputFileUpload id="uploadFile"
                                           binding="#{dataUtilityTaxRateUpdatesBean.uploadFile}"
                                           value="#{dataUtilityTaxRateUpdatesBean.uploadedFile}"
                                           disabled="#{dataUtilityTaxRateUpdatesBean.displayOk || dataUtilityTaxRateUpdatesBean.displayClose}"
                                           
                                           immediate="true"
                                           storage="file"
                                           style="margin: 0px;font-size: 12px;border: 1px;width: 70px;" >
                            
                          	<a4j:support event="onchange" 
                          		actionListener="#{dataUtilityTaxRateUpdatesBean.fileSelectedActionListener}"
			                	ajaxSingle="true"                           
			         			immediate="true" 
			         			oncomplete="copyMe('form:uploadFile','form:fileName')" reRender="btnSelect,btnOk,btnCancel,closeBtn" />
                        	</t:inputFileUpload>
                      	</td>
                    	<td style="width: 30%;">&#160;</td>
                      
                       <!--
                      <td colspan="1">
                        <h:commandButton id="fileSel"
                                         value="Select File"
                                         type="button"
                                         immediate="true"
                                         disabled="#{dataUtilityTaxRateUpdatesBean.displayOk || dataUtilityTaxRateUpdatesBean.displayClose}"
                                         action="#{dataUtilityTaxRateUpdatesBean.fileUploadAction}" />
                      </td>
               
                    <c:if test="#{dataUtilityTaxRateUpdatesBean.displayChangeFile}">
                      <td colspan="2">
                        <h:inputText size="70" value="#{dataUtilityTaxRateUpdatesBean.uploadFileName}"
                                     disabled="true"/>
                      </td>
                      <td colspan="1">
                        <h:commandButton id="fileSel"
                                         value="Change File"
                                         type="button"
                                         immediate="true"
                                         rendered="#{dataUtilityTaxRateUpdatesBean.displayUploadAdd}" 
                                         action="#{dataUtilityTaxRateUpdatesBean.processAddUpdateAction}" />
                      </td>
                    </c:if>
                    -->
                  </tr>
               	</tbody>
              	</table>
              	
 				<table cellpadding="0"
                     cellspacing="0"
                     width="100%"
                     id="rollover"
                     class="ruler">
 				<tbody>
              		<tr>
                      	<th colspan="6">File Information</th>
                    </tr>
                    <tr>
                      	<td>&#160;</td>
                      	<td>Year_Month/Release:</td>
                      	<td>
                        	<h:inputText id="rDate"
                                     value="#{dataUtilityTaxRateUpdatesBean.releaseDateAndVersion}"
                                     disabled="#{true}">
                        	</h:inputText>
                      	</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
                      	<td style="width:10%;">&#160;</td>
                    </tr>
                    <tr>
                      	<td>&#160;</td>
                      	<td>File Type:</td>
                      	<td>
                        	<h:inputText id="fType"
                                     value="#{dataUtilityTaxRateUpdatesBean.fileType}"
                                     disabled="#{true}" />
                      	</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
                    </tr>
                    
					<tr>
						<td style="width:50px;">&#160;</td>
						<td class="column-label">Bytes in File:</td>
						<td>
							<h:inputText value="#{dataUtilityTaxRateUpdatesBean.noOfBytesInFile}" 
								disabled="true" label="Bytes in File" id="noOfBytes"
							     style="width:200px;" 
						         maxlength="120"/>
		                </td>
		                <td>&#160;</td>
                        <td>&#160;</td>
                        <td>&#160;</td>
					</tr>
					<tr>
						<td style="width:50px;">&#160;</td>
						<td class="column-label">Estimated&#160;Rows&#160;in&#160;File:</td>
						<td>
							<h:inputText value="#{dataUtilityTaxRateUpdatesBean.noOfRows}" 
								disabled="true" label="Estimated Rows in File" id="rows"
							     style="width:200px;" 
						         maxlength="120"/>
		                </td>
		                <td>&#160;</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
					</tr>
					<tr>
						<td style="width:50px;">&#160;</td>
						<td class="column-label">Batch Number:</td>
						<td>
							<h:inputText value="#{dataUtilityTaxRateUpdatesBean.batchNumber}" 
								disabled="true" label="Batch Number" id="batchNo"
							     style="width:200px;" 
						         maxlength="120"/>
		                </td>
		                <td>&#160;</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
					</tr>
					<tr>
						<td style="width:50px;">&#160;</td>
						<td class="column-label">Import Status:</td>
		 				<td style="font-weight: bold;">				
		          			<h:outputText id="impStatus" escape="false" style="#{dataUtilityTaxRateUpdatesBean.error eq true ? 'color:red;':'color:black;'}font-weight: bold;" value="#{dataUtilityTaxRateUpdatesBean.importStatus}" />          		
		                </td>
		                <td>&#160;</td>
                      	<td>&#160;</td>
                      	<td>&#160;</td>
		        	</tr>   
		        	
		        	<tr>
						<td style="width:50px;">&#160;</td>
						<td>&#160;</td>
		 				<td colspan="4" style="font-weight: bold;" align="left">				
		          			<c:if test="#{dataUtilityTaxRateUpdatesBean.displayClose || dataUtilityTaxRateUpdatesBean.displayOk}">
				            	<a4j:outputPanel id="progressPanel" style="width: 100%">
				              		<rich:progressBar id="progressBar"
				                                value="#{dataUtilityTaxRateUpdatesBean.progress}"
				                                styleClass="sts-progress-bar"
				                                interval="2000"
				                                mode="ajax"
				                                label="Upload progress: #{dataUtilityTaxRateUpdatesBean.progress} %"			                                 
				                                minValue="0"
				                                maxValue="99"
				                                reRender="impStatus" >					                
					              	</rich:progressBar>
				            	</a4j:outputPanel>
				            </c:if>&#160;          		
		                </td>
		        	</tr>           
          
                </tbody>
              	</table>
            </div>
            
            
            
            <div id="table-four-bottom">
            	<rich:modalPanel id="loader" zindex="2000" autosized="true">
         			<h:outputText value="Processing..."/>
    		  	</rich:modalPanel>
     				
            	<ul class="right">
					<li class="select"><h:commandLink id="btnSelect" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displaySelect}" action="#{dataUtilityTaxRateUpdatesBean.validateImportedFile}" >							   							   	
								   		</h:commandLink>
					</li>
					<!--
					<li class="ok"><h:commandLink id="btnOk" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displayOk}" action="#{dataUtilityTaxRateUpdatesBean.importOkSubmit}"
											onclick="Richfaces.showModalPanel('loader');" 
								   			oncomplete="Richfaces.hideModalPanel('loader');" >							   	
								   			<rich:componentControl event="onclick" for="loader" attachTo="btnOk" operation="show"/>							   	
								   		</h:commandLink>
					</li>
					-->
					<li class="ok"><h:commandLink id="btnOk" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displayOk}" action="#{dataUtilityTaxRateUpdatesBean.importOkSubmit}">							   							   	
								   		</h:commandLink>
					</li>
					<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displayCancel}"  action="#{dataUtilityTaxRateUpdatesBean.cancelAction}" /></li>
					<li class="close"><h:commandLink id="closeBtn" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displayClose}"  action="#{dataUtilityTaxRateUpdatesBean.closeAction}" /></li>
					
				</ul>
            
            <!-- 	
              <ul class="right">
              	  <li class="select">
                  	<h:commandLink id="btnSelect" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displaySelect}" 
                  			action="#{dataUtilityTaxRateUpdatesBean.fileUploadAction}" />
                  </li>
                  <li class="ok2">
				  	<h:commandLink id="addAction" immediate="true" disabled="#{!dataUtilityTaxRateUpdatesBean.displayOk}" 
				  		action="#{dataUtilityTaxRateUpdatesBean.addAction}" />
                  </li>

                  <li class="close">
                  	<a4j:commandLink id="completeAction"
                                   style="display: #{dataUtilityTaxRateUpdatesBean.displayComplete ? '' : 'none'}"
                                   action="#{dataUtilityTaxRateUpdatesBean.backgroundCompleteAction}"/>
                  </li>
                  <li class="cancel">
                  	<a4j:commandLink id="cancelAction" immediate="true"
                                   style="display: #{dataUtilityTaxRateUpdatesBean.displayComplete ? 'none': ''}"
                                   action="#{dataUtilityTaxRateUpdatesBean.cancelAction}" />
                  </li>
              </ul>
               -->
              
            </div> <!-- t4 bottom -->
<!--               <a4j:log popup="false" level="ALL" style="width: 800px; height: 300px;"/> -->
          </div> <!-- t4 -->
        </div> <!-- top -->
      </h:form>
    </ui:define>
  </ui:composition>
</html>
