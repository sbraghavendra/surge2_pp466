<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="duArchieveAdd">
<h1><img src="../images/headers/hdr-batch-maintenance-status.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">Parameters</td></tr>
		</thead>
		<tbody>
			<tr>
				<td>&#160;</td>
				<td>Business Unit:</td>
				<td style="width:730px;">

				<h:selectOneMenu id="buName" styleClass="NCSTSSelector" 

				value="#{dataUtilityBackingBean.baAddDTO.vc03}" style="width: 130px;"> 

				<f:selectItems id="buNameItms" value="#{dataUtilityBackingBean.buItemList}"/>

				</h:selectOneMenu>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>From G/L Date:</td>

				
				<td>

				<rich:calendar popup="true" 
						oninputkeypress="return onlyDateValue(event);"
             	rendered="true" converter="date" datePattern="M/d/yyyy" value="#{dataUtilityBackingBean.baAddDTO.ts01}" 

             	id="fGLDate"/>
				</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>To G/L Date:</td>

				<td>

				<rich:calendar popup="true" 
						oninputkeypress="return onlyDateValue(event);"
             	rendered="true" converter="date" datePattern="M/d/yyyy" value="#{dataUtilityBackingBean.baAddDTO.ts02}" 

             	id="tGLDate"/>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink action="#{dataUtilityBackingBean.baAddAction}"/></li>
		<li class="cancel"><a href="#"></a></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>