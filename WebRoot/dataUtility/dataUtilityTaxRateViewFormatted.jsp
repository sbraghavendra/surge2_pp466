<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgTaxRateUpdates" alt="Tax Rate Updates" src="../images/headers/hdr-tax-rate-updates.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
                
                <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       			<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilityTaxRateUpdatesBean.bcpJurisdictionTaxRateDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />  
                
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">View a Tax Rate Updates Batch - Formatted</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="8"
                             styleClass="basic-form"
                             columnClasses="left-indent,right,right,right,right,right,right,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="20" id="txtBatchId"
                               value="#{dataUtilityTaxRateUpdatesBean.selectedBatch.batchId}"
                               disabled="#{true}" />
                  <h:outputLabel value="Update Date:" />
                  <h:inputText size="20" id="txtUpdateDate"
                               value="#{dataUtilityTaxRateUpdatesBean.selectedBatch.ts01}"
                               disabled="#{true}" />
                  <h:outputLabel value="Release:" />
                  <h:inputText size="20" id="txtRelease"
                               value="#{dataUtilityTaxRateUpdatesBean.selectedBatch.nu01}"
                               disabled="#{true}" >
                               <f:convertNumber integerOnly="true" />
                  </h:inputText>             
                  <h:outputLabel value="File Type:" />
                  <h:inputText size="20" id="FileType"
                               value="#{dataUtilityTaxRateUpdatesBean.selectedBatch.vc02}"
                               disabled="#{true}" />
                </h:panelGrid>
                <h:panelGrid id="batchFile"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="File Name:" />
                  <h:inputText size="92" id="txtFileName"
                               value="#{dataUtilityTaxRateUpdatesBean.selectedBatch.vc01}"
                               disabled="#{true}" />
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-light">Tax Rate Updates</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner"
                       id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                                    border="1"
                                    sortmode="single"
                                    rows="#{dataUtilityTaxRateUpdatesBean.bcpJurisdictionTaxRateDataModel.pageSize }"
                                    value="#{dataUtilityTaxRateUpdatesBean.bcpJurisdictionTaxRateDataModel}"
									var="row">
									                                    
									<rich:column>
										<f:facet name="header">Country</f:facet>
										<h:outputText id="rowCountry" value="#{row.country}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Geocode</f:facet>
										<h:outputText id="rowGeocode" value="#{row.geocode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">State</f:facet>
										<h:outputText id="rowState" value="#{row.state}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">County</f:facet>
										<h:outputText id="rowCounty" value="#{row.county}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">City</f:facet>
										<h:outputText id="rowCity" value="#{row.city}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">ReportingCity</f:facet>
										<h:outputText id="rowReportingCity" value="#{row.reportingCity}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">ReportingCityFips</f:facet>
										<h:outputText id="rowReportingCityFips" value="#{row.reportingCityFips}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1Name</f:facet>
										<h:outputText id="rowStj1Name" value="#{row.stj1Name}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2Name</f:facet>
										<h:outputText id="rowStj2Name" value="#{row.stj2Name}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3Name</f:facet>
										<h:outputText id="rowStj3Name" value="#{row.stj3Name}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4Name</f:facet>
										<h:outputText id="rowStj4Name" value="#{row.stj4Name}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5Name</f:facet>
										<h:outputText id="rowStj5Name" value="#{row.stj5Name}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Zip</f:facet>
										<h:outputText id="rowZip" value="#{row.zip}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Zipplus4</f:facet>
										<h:outputText id="rowZipplus4" value="#{row.zipplus4}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">InOut</f:facet>
										<h:outputText id="rowInOut" value="#{row.inOut}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">JurEffectiveDate</f:facet>
										<h:outputText id="rowJurEffectiveDate" value="#{row.jurEffectiveDate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">JurExpirationDate</f:facet>
										<h:outputText id="rowJurExpirationDate" value="#{row.jurExpirationDate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Description</f:facet>
										<h:outputText id="rowDescription" value="#{row.description}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryNexusindCode</f:facet>
										<h:outputText id="rowCountryNexusindCode" value="#{row.countryNexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateNexusindCode</f:facet>
										<h:outputText id="rowStateNexusindCode" value="#{row.stateNexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyNexusindCode</f:facet>
										<h:outputText id="rowCountyNexusindCode" value="#{row.countyNexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityNexusindCode</f:facet>
										<h:outputText id="rowCityNexusindCode" value="#{row.cityNexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1NexusindCode</f:facet>
										<h:outputText id="rowStj1NexusindCode" value="#{row.stj1NexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2NexusindCode</f:facet>
										<h:outputText id="rowStj2NexusindCode" value="#{row.stj2NexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3NexusindCode</f:facet>
										<h:outputText id="rowStj3NexusindCode" value="#{row.stj3NexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4NexusindCode</f:facet>
										<h:outputText id="rowStj4NexusindCode" value="#{row.stj4NexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5NexusindCode</f:facet>
										<h:outputText id="rowStj5NexusindCode" value="#{row.stj5NexusindCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1LocalCode</f:facet>
										<h:outputText id="rowStj1LocalCode" value="#{row.stj1LocalCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2LocalCode</f:facet>
										<h:outputText id="rowStj2LocalCode" value="#{row.stj2LocalCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3LocalCode</f:facet>
										<h:outputText id="rowStj3LocalCode" value="#{row.stj3LocalCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4LocalCode</f:facet>
										<h:outputText id="rowStj4LocalCode" value="#{row.stj4LocalCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5LocalCode</f:facet>
										<h:outputText id="rowStj5LocalCode" value="#{row.stj5LocalCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">RatetypeCode</f:facet>
										<h:outputText id="rowRatetypeCode" value="#{row.ratetypeCode}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">RateEffectiveDate</f:facet>
										<h:outputText id="rowRateEffectiveDate" value="#{row.rateEffectiveDate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">RateExpirationDate</f:facet>
										<h:outputText id="rowRateExpirationDate" value="#{row.rateExpirationDate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier1Rate</f:facet>
										<h:outputText id="rowCountrySalesTier1Rate" value="#{row.countrySalesTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier2Rate</f:facet>
										<h:outputText id="rowCountrySalesTier2Rate" value="#{row.countrySalesTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier3Rate</f:facet>
										<h:outputText id="rowCountrySalesTier3Rate" value="#{row.countrySalesTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier1Setamt</f:facet>
										<h:outputText id="rowCountrySalesTier1Setamt" value="#{row.countrySalesTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier2Setamt</f:facet>
										<h:outputText id="rowCountrySalesTier2Setamt" value="#{row.countrySalesTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier3Setamt</f:facet>
										<h:outputText id="rowCountrySalesTier3Setamt" value="#{row.countrySalesTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier1MaxAmt</f:facet>
										<h:outputText id="rowCountrySalesTier1MaxAmt" value="#{row.countrySalesTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier2MinAmt</f:facet>
										<h:outputText id="rowCountrySalesTier2MinAmt" value="#{row.countrySalesTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier2MaxAmt</f:facet>
										<h:outputText id="rowCountrySalesTier2MaxAmt" value="#{row.countrySalesTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier2EntamFlag</f:facet>
										<h:outputText id="rowCountrySalesTier2EntamFlag" value="#{row.countrySalesTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesTier3EntamFlag</f:facet>
										<h:outputText id="rowCountrySalesTier3EntamFlag" value="#{row.countrySalesTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountrySalesMaxtaxAmt</f:facet>
										<h:outputText id="rowCountrySalesMaxtaxAmt" value="#{row.countrySalesMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier1Rate</f:facet>
										<h:outputText id="rowCountryUseTier1Rate" value="#{row.countryUseTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier2Rate</f:facet>
										<h:outputText id="rowCountryUseTier2Rate" value="#{row.countryUseTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier3Rate</f:facet>
										<h:outputText id="rowCountryUseTier3Rate" value="#{row.countryUseTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier1Setamt</f:facet>
										<h:outputText id="rowCountryUseTier1Setamt" value="#{row.countryUseTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier2Setamt</f:facet>
										<h:outputText id="rowCountryUseTier2Setamt" value="#{row.countryUseTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier3Setamt</f:facet>
										<h:outputText id="rowCountryUseTier3Setamt" value="#{row.countryUseTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier1MaxAmt</f:facet>
										<h:outputText id="rowCountryUseTier1MaxAmt" value="#{row.countryUseTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier2MinAmt</f:facet>
										<h:outputText id="rowCountryUseTier2MinAmt" value="#{row.countryUseTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier2MaxAmt</f:facet>
										<h:outputText id="rowCountryUseTier2MaxAmt" value="#{row.countryUseTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier2EntamFlag</f:facet>
										<h:outputText id="rowCountryUseTier2EntamFlag" value="#{row.countryUseTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseTier3EntamFlag</f:facet>
										<h:outputText id="rowCountryUseTier3EntamFlag" value="#{row.countryUseTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseMaxtaxAmt</f:facet>
										<h:outputText id="rowCountryUseMaxtaxAmt" value="#{row.countryUseMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountryUseSameSales</f:facet>
										<h:outputText id="rowCountryUseSameSales" value="#{row.countryUseSameSales}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier1Rate</f:facet>
										<h:outputText id="rowStateSalesTier1Rate" value="#{row.stateSalesTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier2Rate</f:facet>
										<h:outputText id="rowStateSalesTier2Rate" value="#{row.stateSalesTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier3Rate</f:facet>
										<h:outputText id="rowStateSalesTier3Rate" value="#{row.stateSalesTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier1Setamt</f:facet>
										<h:outputText id="rowStateSalesTier1Setamt" value="#{row.stateSalesTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier2Setamt</f:facet>
										<h:outputText id="rowStateSalesTier2Setamt" value="#{row.stateSalesTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier3Setamt</f:facet>
										<h:outputText id="rowStateSalesTier3Setamt" value="#{row.stateSalesTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier1MaxAmt</f:facet>
										<h:outputText id="rowStateSalesTier1MaxAmt" value="#{row.stateSalesTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier2MinAmt</f:facet>
										<h:outputText id="rowStateSalesTier2MinAmt" value="#{row.stateSalesTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier2MaxAmt</f:facet>
										<h:outputText id="rowStateSalesTier2MaxAmt" value="#{row.stateSalesTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier2EntamFlag</f:facet>
										<h:outputText id="rowStateSalesTier2EntamFlag" value="#{row.stateSalesTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesTier3EntamFlag</f:facet>
										<h:outputText id="rowStateSalesTier3EntamFlag" value="#{row.stateSalesTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateSalesMaxtaxAmt</f:facet>
										<h:outputText id="rowStateSalesMaxtaxAmt" value="#{row.stateSalesMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier1Rate</f:facet>
										<h:outputText id="rowStateUseTier1Rate" value="#{row.stateUseTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier2Rate</f:facet>
										<h:outputText id="rowStateUseTier2Rate" value="#{row.stateUseTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier3Rate</f:facet>
										<h:outputText id="rowStateUseTier3Rate" value="#{row.stateUseTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier1Setamt</f:facet>
										<h:outputText id="rowStateUseTier1Setamt" value="#{row.stateUseTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier2Setamt</f:facet>
										<h:outputText id="rowStateUseTier2Setamt" value="#{row.stateUseTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier3Setamt</f:facet>
										<h:outputText id="rowStateUseTier3Setamt" value="#{row.stateUseTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier1MaxAmt</f:facet>
										<h:outputText id="rowStateUseTier1MaxAmt" value="#{row.stateUseTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier2MinAmt</f:facet>
										<h:outputText id="rowStateUseTier2MinAmt" value="#{row.stateUseTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier2MaxAmt</f:facet>
										<h:outputText id="rowStateUseTier2MaxAmt" value="#{row.stateUseTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier2EntamFlag</f:facet>
										<h:outputText id="rowStateUseTier2EntamFlag" value="#{row.stateUseTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseTier3EntamFlag</f:facet>
										<h:outputText id="rowStateUseTier3EntamFlag" value="#{row.stateUseTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseMaxtaxAmt</f:facet>
										<h:outputText id="rowStateUseMaxtaxAmt" value="#{row.stateUseMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StateUseSameSales</f:facet>
										<h:outputText id="rowStateUseSameSales" value="#{row.stateUseSameSales}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier1Rate</f:facet>
										<h:outputText id="rowCountySalesTier1Rate" value="#{row.countySalesTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier2Rate</f:facet>
										<h:outputText id="rowCountySalesTier2Rate" value="#{row.countySalesTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier3Rate</f:facet>
										<h:outputText id="rowCountySalesTier3Rate" value="#{row.countySalesTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier1Setamt</f:facet>
										<h:outputText id="rowCountySalesTier1Setamt" value="#{row.countySalesTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier2Setamt</f:facet>
										<h:outputText id="rowCountySalesTier2Setamt" value="#{row.countySalesTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier3Setamt</f:facet>
										<h:outputText id="rowCountySalesTier3Setamt" value="#{row.countySalesTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier1MaxAmt</f:facet>
										<h:outputText id="rowCountySalesTier1MaxAmt" value="#{row.countySalesTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier2MinAmt</f:facet>
										<h:outputText id="rowCountySalesTier2MinAmt" value="#{row.countySalesTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier2MaxAmt</f:facet>
										<h:outputText id="rowCountySalesTier2MaxAmt" value="#{row.countySalesTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier2EntamFlag</f:facet>
										<h:outputText id="rowCountySalesTier2EntamFlag" value="#{row.countySalesTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesTier3EntamFlag</f:facet>
										<h:outputText id="rowCountySalesTier3EntamFlag" value="#{row.countySalesTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountySalesMaxtaxAmt</f:facet>
										<h:outputText id="rowCountySalesMaxtaxAmt" value="#{row.countySalesMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier1Rate</f:facet>
										<h:outputText id="rowCountyUseTier1Rate" value="#{row.countyUseTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier2Rate</f:facet>
										<h:outputText id="rowCountyUseTier2Rate" value="#{row.countyUseTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier3Rate</f:facet>
										<h:outputText id="rowCountyUseTier3Rate" value="#{row.countyUseTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier1Setamt</f:facet>
										<h:outputText id="rowCountyUseTier1Setamt" value="#{row.countyUseTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier2Setamt</f:facet>
										<h:outputText id="rowCountyUseTier2Setamt" value="#{row.countyUseTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier3Setamt</f:facet>
										<h:outputText id="rowCountyUseTier3Setamt" value="#{row.countyUseTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier1MaxAmt</f:facet>
										<h:outputText id="rowCountyUseTier1MaxAmt" value="#{row.countyUseTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier2MinAmt</f:facet>
										<h:outputText id="rowCountyUseTier2MinAmt" value="#{row.countyUseTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier2MaxAmt</f:facet>
										<h:outputText id="rowCountyUseTier2MaxAmt" value="#{row.countyUseTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier2EntamFlag</f:facet>
										<h:outputText id="rowCountyUseTier2EntamFlag" value="#{row.countyUseTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseTier3EntamFlag</f:facet>
										<h:outputText id="rowCountyUseTier3EntamFlag" value="#{row.countyUseTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseMaxtaxAmt</f:facet>
										<h:outputText id="rowCountyUseMaxtaxAmt" value="#{row.countyUseMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CountyUseSameSales</f:facet>
										<h:outputText id="rowCountyUseSameSales" value="#{row.countyUseSameSales}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier1Rate</f:facet>
										<h:outputText id="rowCitySalesTier1Rate" value="#{row.citySalesTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier2Rate</f:facet>
										<h:outputText id="rowCitySalesTier2Rate" value="#{row.citySalesTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier3Rate</f:facet>
										<h:outputText id="rowCitySalesTier3Rate" value="#{row.citySalesTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier1Setamt</f:facet>
										<h:outputText id="rowCitySalesTier1Setamt" value="#{row.citySalesTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier2Setamt</f:facet>
										<h:outputText id="rowCitySalesTier2Setamt" value="#{row.citySalesTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier3Setamt</f:facet>
										<h:outputText id="rowCitySalesTier3Setamt" value="#{row.citySalesTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier1MaxAmt</f:facet>
										<h:outputText id="rowCitySalesTier1MaxAmt" value="#{row.citySalesTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier2MinAmt</f:facet>
										<h:outputText id="rowCitySalesTier2MinAmt" value="#{row.citySalesTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier2MaxAmt</f:facet>
										<h:outputText id="rowCitySalesTier2MaxAmt" value="#{row.citySalesTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier2EntamFlag</f:facet>
										<h:outputText id="rowCitySalesTier2EntamFlag" value="#{row.citySalesTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesTier3EntamFlag</f:facet>
										<h:outputText id="rowCitySalesTier3EntamFlag" value="#{row.citySalesTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CitySalesMaxtaxAmt</f:facet>
										<h:outputText id="rowCitySalesMaxtaxAmt" value="#{row.citySalesMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier1Rate</f:facet>
										<h:outputText id="rowCityUseTier1Rate" value="#{row.cityUseTier1Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier2Rate</f:facet>
										<h:outputText id="rowCityUseTier2Rate" value="#{row.cityUseTier2Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier3Rate</f:facet>
										<h:outputText id="rowCityUseTier3Rate" value="#{row.cityUseTier3Rate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier1Setamt</f:facet>
										<h:outputText id="rowCityUseTier1Setamt" value="#{row.cityUseTier1Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier2Setamt</f:facet>
										<h:outputText id="rowCityUseTier2Setamt" value="#{row.cityUseTier2Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier3Setamt</f:facet>
										<h:outputText id="rowCityUseTier3Setamt" value="#{row.cityUseTier3Setamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier1MaxAmt</f:facet>
										<h:outputText id="rowCityUseTier1MaxAmt" value="#{row.cityUseTier1MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier2MinAmt</f:facet>
										<h:outputText id="rowCityUseTier2MinAmt" value="#{row.cityUseTier2MinAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier2MaxAmt</f:facet>
										<h:outputText id="rowCityUseTier2MaxAmt" value="#{row.cityUseTier2MaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier2EntamFlag</f:facet>
										<h:outputText id="rowCityUseTier2EntamFlag" value="#{row.cityUseTier2EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseTier3EntamFlag</f:facet>
										<h:outputText id="rowCityUseTier3EntamFlag" value="#{row.cityUseTier3EntamFlag}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseMaxtaxAmt</f:facet>
										<h:outputText id="rowCityUseMaxtaxAmt" value="#{row.cityUseMaxtaxAmt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CityUseSameSales</f:facet>
										<h:outputText id="rowCityUseSameSales" value="#{row.cityUseSameSales}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1SalesRate</f:facet>
										<h:outputText id="rowStj1SalesRate" value="#{row.stj1SalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1SalesSetamt</f:facet>
										<h:outputText id="rowStj1SalesSetamt" value="#{row.stj1SalesSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2SalesRate</f:facet>
										<h:outputText id="rowStj2SalesRate" value="#{row.stj2SalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2SalesSetamt</f:facet>
										<h:outputText id="rowStj2SalesSetamt" value="#{row.stj2SalesSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3SalesRate</f:facet>
										<h:outputText id="rowStj3SalesRate" value="#{row.stj3SalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3SalesSetamt</f:facet>
										<h:outputText id="rowStj3SalesSetamt" value="#{row.stj3SalesSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4SalesRate</f:facet>
										<h:outputText id="rowStj4SalesRate" value="#{row.stj4SalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4SalesSetamt</f:facet>
										<h:outputText id="rowStj4SalesSetamt" value="#{row.stj4SalesSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5SalesRate</f:facet>
										<h:outputText id="rowStj5SalesRate" value="#{row.stj5SalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5SalesSetamt</f:facet>
										<h:outputText id="rowStj5SalesSetamt" value="#{row.stj5SalesSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1UseRate</f:facet>
										<h:outputText id="rowStj1UseRate" value="#{row.stj1UseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj1UseSetamt</f:facet>
										<h:outputText id="rowStj1UseSetamt" value="#{row.stj1UseSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2UseRate</f:facet>
										<h:outputText id="rowStj2UseRate" value="#{row.stj2UseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj2UseSetamt</f:facet>
										<h:outputText id="rowStj2UseSetamt" value="#{row.stj2UseSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3UseRate</f:facet>
										<h:outputText id="rowStj3UseRate" value="#{row.stj3UseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj3UseSetamt</f:facet>
										<h:outputText id="rowStj3UseSetamt" value="#{row.stj3UseSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4UseRate</f:facet>
										<h:outputText id="rowStj4UseRate" value="#{row.stj4UseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj4UseSetamt</f:facet>
										<h:outputText id="rowStj4UseSetamt" value="#{row.stj4UseSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5UseRate</f:facet>
										<h:outputText id="rowStj5UseRate" value="#{row.stj5UseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">Stj5UseSetamt</f:facet>
										<h:outputText id="rowStj5UseSetamt" value="#{row.stj5UseSetamt}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">StjUseSameSales</f:facet>
										<h:outputText id="rowStjUseSameSales" value="#{row.stjUseSameSales}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CombinedUseRate</f:facet>
										<h:outputText id="rowCombinedUseRate" value="#{row.combinedUseRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">CombinedSalesRate</f:facet>
										<h:outputText id="rowCombinedSalesRate" value="#{row.combinedSalesRate}" />
									</rich:column>
									
									<rich:column>
										<f:facet name="header">UpdateFlag</f:facet>
										<h:outputText id="rowUpdateFlag" value="#{row.updateFlag}" />
									</rich:column>          
                        
                    </rich:dataTable>
                  </div>
		              <rich:datascroller id="scroll" for="batchTable" maxPages="10" oncomplete="initScrollingTables();"
			                               align="center" stepControls="auto" ajaxSingle="false" 
                                     reRender="pageInfo"
			                               page="#{dataUtilityTaxRateUpdatesBean.bcpJurisdictionTaxRateDataModel.curPage}" />
                </div>
              </div>
              <!--  content -->
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="unformatted">
                      <h:commandLink id="viewufr"
                                     action="#{dataUtilityTaxRateUpdatesBean.viewUnformattedAction}" />
                    </li>
                    <li class="ok2">
                      <h:commandLink id="cancelAction"
                                     action="#{dataUtilityTaxRateUpdatesBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
