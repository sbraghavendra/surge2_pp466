<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgTaxRateUpdates" alt="Situs Rules Updates" src="../images/headers/hdr-custlocn-updates.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
                
                <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       			<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilityCustLocnBean.bcpCustLocnDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />  
                
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">View a Customers&#32;&#38;&#32;Locations Updates Batch - Formatted</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="8"
                             styleClass="basic-form"
                             columnClasses="left-indent,right,right,right,right,right,right,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="20" id="txtBatchId"
                               value="#{dataUtilityCustLocnBean.selectedBatch.batchId}"
                               disabled="#{true}" />
            
                  <h:outputLabel value="File Type:" />
                  <h:inputText size="20" id="FileType"
                               value="#{dataUtilityCustLocnBean.selectedBatch.vc02}"
                               disabled="#{true}" />
                </h:panelGrid>
                <h:panelGrid id="batchFile"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="File Name:" />
                  <h:inputText size="108" id="txtFileName"
                               value="#{dataUtilityCustLocnBean.selectedBatch.vc01}"
                               disabled="#{true}" />
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-light">Customers&#32;&#38;&#32;Locations Updates</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner"
                       id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                                    border="1"
                                    sortmode="single"
                                    rows="#{dataUtilityCustLocnBean.bcpCustLocnDataModel.pageSize }"
                                    value="#{dataUtilityCustLocnBean.bcpCustLocnDataModel}"
                                    var="row">
                                    
                        <rich:column>
                        	<f:facet name="header">Record Type</f:facet>
                        	<h:outputText id="rowrecordType" value="#{row.recordType}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Customer Number</f:facet>
                        	<h:outputText id="rowcustNbr" value="#{row.custNbr}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Customer name</f:facet>
                        	<h:outputText id="rowcustName" value="#{row.custName}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Location Code</f:facet>
                        	<h:outputText id="rowcustLocnCode" value="#{row.custLocnCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Location name</f:facet>
                        	<h:outputText id="rowlocationName" value="#{row.locationName}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Geocode</f:facet>
                        	<h:outputText id="rowgeocode" value="#{row.geocode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">AddressLine 1</f:facet>
                        	<h:outputText id="rowaddressLine1" value="#{row.addressLine1}" />
			            </rich:column>
                                    
                        <rich:column>
                        	<f:facet name="header">AddressLine 2</f:facet>
                        	<h:outputText id="rowaddressLine2" value="#{row.addressLine2}" />
			            </rich:column>
                                    
	                    <rich:column>
                        	<f:facet name="header">City</f:facet>
                        	<h:outputText id="rowcity" value="#{row.city}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">County</f:facet>
                        	<h:outputText id="rowcounty" value="#{row.county}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">State</f:facet>
                        	<h:outputText id="rowstate" value="#{row.state}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Zip</f:facet>
                        	<h:outputText id="rowzip" value="#{row.zip}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Zipplus4</f:facet>
                        	<h:outputText id="rowzipplus4" value="#{row.zipplus4}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Country</f:facet>
                        	<h:outputText id="rowcountry" value="#{row.country}" />
			            </rich:column>		        
			            
			            <rich:column>
                        	<f:facet name="header">Active Flag</f:facet>
                        	<h:outputText id="rowactiveFlag" value="#{row.activeFlag}" />
			            </rich:column>
			           
	                    <rich:column>
                        	<f:facet name="header">Entity Code</f:facet>
                        	<h:outputText id="rowentityCode" value="#{row.entityCode}" />
			            </rich:column>
			            			            			            	          	        
	                    <rich:column>
                        	<f:facet name="header">Update Code</f:facet>
                        	<h:outputText id="rowupdateCode" value="#{row.updateCode}" />
			            </rich:column>			            
			            
                    </rich:dataTable>
                  </div>
		              <rich:datascroller id="scroll" for="batchTable" maxPages="10" oncomplete="initScrollingTables();"
			                               align="center" stepControls="auto" ajaxSingle="false" 
                                     reRender="pageInfo"
			                               page="#{dataUtilityCustLocnBean.bcpCustLocnDataModel.curPage}" />
                </div>
              </div>
              <!--  content -->
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="unformatted">
                      <h:commandLink id="viewufr"
                                     action="#{dataUtilityCustLocnBean.viewUnformattedAction}" />
                    </li>
                    <li class="ok2">
                      <h:commandLink id="cancelAction"
                                     action="#{dataUtilityCustLocnBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
