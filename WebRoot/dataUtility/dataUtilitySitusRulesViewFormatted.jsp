<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgTaxRateUpdates" alt="Situs Rules Updates" src="../images/headers/hdr-situs-rules-updates.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
                
                <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       			<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{dataUtilitySitusRulesBean.bcpSitusRuleDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />  
                
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">View a Situs Rules Updates Batch - Formatted</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="8"
                             styleClass="basic-form"
                             columnClasses="left-indent,right,right,right,right,right,right,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="20" id="txtBatchId"
                               value="#{dataUtilitySitusRulesBean.selectedBatch.batchId}"
                               disabled="#{true}" />
                  <h:outputLabel value="Update Date:" />
                  <h:inputText size="20" id="txtUpdateDate"
                               value="#{dataUtilitySitusRulesBean.selectedBatch.ts01}"
                               disabled="#{true}" />
                  <h:outputLabel value="Release:" />
                  <h:inputText size="20" id="txtRelease"
                               value="#{dataUtilitySitusRulesBean.selectedBatch.nu01}"
                               disabled="#{true}" >
                               <f:convertNumber integerOnly="true" />
                  </h:inputText>             
                  <h:outputLabel value="File Type:" />
                  <h:inputText size="20" id="FileType"
                               value="#{dataUtilitySitusRulesBean.selectedBatch.vc02}"
                               disabled="#{true}" />
                </h:panelGrid>
                <h:panelGrid id="batchFile"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="File Name:" />
                  <h:inputText size="92" id="txtFileName"
                               value="#{dataUtilitySitusRulesBean.selectedBatch.vc01}"
                               disabled="#{true}" />
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-light">Situs Rules Updates</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner"
                       id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                                    border="1"
                                    sortmode="single"
                                    rows="#{dataUtilitySitusRulesBean.bcpSitusRuleDataModel.pageSize }"
                                    value="#{dataUtilitySitusRulesBean.bcpSitusRuleDataModel}"
                                    var="row">
                                    
                        <rich:column>
                        	<f:facet name="header">Record Type</f:facet>
                        	<h:outputText id="rowrecordType" value="#{row.recordType}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">TransactionType Code</f:facet>
                        	<h:outputText id="rowtransactionTypeCode" value="#{row.transactionTypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Ratetype Code</f:facet>
                        	<h:outputText id="rowratetypeCode" value="#{row.ratetypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">MethodDeliveryCode</f:facet>
                        	<h:outputText id="rowmethodDeliveryCode" value="#{row.methodDeliveryCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">LocationType Code</f:facet>
                        	<h:outputText id="rowsddLocationTypeCode" value="#{row.sddLocationTypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver Id</f:facet>
                        	<h:outputText id="rowsddDriverId" value="#{row.sddDriverId}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Mandatory Flag</f:facet>
                        	<h:outputText id="rowsddMandatoryFlag" value="#{row.sddMandatoryFlag}" />
			            </rich:column>
                                    
                        <rich:column>
                        	<f:facet name="header">Country Code</f:facet>
                        	<h:outputText id="rowSitusCountryCode" value="#{row.smSitusCountryCode}" />
			            </rich:column>
                                    
	                    <rich:column>
                        	<f:facet name="header">State Code</f:facet>
                        	<h:outputText id="rowSitusStateCode" value="#{row.smSitusStateCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Jur Level</f:facet>
                        	<h:outputText id="rowsmJurLevel" value="#{row.smJurLevel}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver01</f:facet>
                        	<h:outputText id="rowsmDriver01" value="#{row.smDriver01}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver02</f:facet>
                        	<h:outputText id="rowsmDriver02" value="#{row.smDriver02}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver03</f:facet>
                        	<h:outputText id="rowsmDriver03" value="#{row.smDriver03}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver04</f:facet>
                        	<h:outputText id="rowsmDriver04" value="#{row.smDriver04}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver05</f:facet>
                        	<h:outputText id="rowsmDriver05" value="#{row.smDriver05}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver06</f:facet>
                        	<h:outputText id="rowsmDriver06" value="#{row.smDriver06}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver07</f:facet>
                        	<h:outputText id="rowsmDriver07" value="#{row.smDriver07}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver08</f:facet>
                        	<h:outputText id="rowsmDriver08" value="#{row.smDriver08}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver09</f:facet>
                        	<h:outputText id="rowsmDriver09" value="#{row.smDriver09}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver10</f:facet>
                        	<h:outputText id="rowsmDriver10" value="#{row.smDriver10}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver11</f:facet>
                        	<h:outputText id="rowsmDriver11" value="#{row.smDriver11}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver12</f:facet>
                        	<h:outputText id="rowsmDriver12" value="#{row.smDriver12}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver13</f:facet>
                        	<h:outputText id="rowsmDriver13" value="#{row.smDriver13}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver14</f:facet>
                        	<h:outputText id="rowsmDriver14" value="#{row.smDriver14}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver15</f:facet>
                        	<h:outputText id="rowsmDriver15" value="#{row.smDriver15}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver16</f:facet>
                        	<h:outputText id="rowsmDriver16" value="#{row.smDriver16}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver17</f:facet>
                        	<h:outputText id="rowsmDriver17" value="#{row.smDriver17}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver18</f:facet>
                        	<h:outputText id="rowsmDriver18" value="#{row.smDriver18}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver19</f:facet>
                        	<h:outputText id="rowsmDriver19" value="#{row.smDriver19}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver20</f:facet>
                        	<h:outputText id="rowsmDriver20" value="#{row.smDriver20}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver21</f:facet>
                        	<h:outputText id="rowsmDriver21" value="#{row.smDriver21}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver22</f:facet>
                        	<h:outputText id="rowsmDriver22" value="#{row.smDriver22}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver23</f:facet>
                        	<h:outputText id="rowsmDriver23" value="#{row.smDriver23}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver24</f:facet>
                        	<h:outputText id="rowsmDriver24" value="#{row.smDriver24}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver25</f:facet>
                        	<h:outputText id="rowsmDriver25" value="#{row.smDriver25}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver26</f:facet>
                        	<h:outputText id="rowsmDriver26" value="#{row.smDriver26}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver27</f:facet>
                        	<h:outputText id="rowsmDriver27" value="#{row.smDriver27}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver28</f:facet>
                        	<h:outputText id="rowsmDriver28" value="#{row.smDriver28}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver29</f:facet>
                        	<h:outputText id="rowsmDriver29" value="#{row.smDriver29}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Driver30</f:facet>
                        	<h:outputText id="rowsmDriver30" value="#{row.smDriver30}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Binary Weight</f:facet>
                        	<h:outputText id="rowsmBinaryWeight" value="#{row.smBinaryWeight}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Effective Date</f:facet>
                        	<h:outputText id="rowsmEffectiveDate" value="#{row.smEffectiveDate}" >
								<f:convertDateTime type="date" pattern="yyyy/MM/dd"/>
							</h:outputText>	
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Expiration Date</f:facet>
                        	<h:outputText id="rowsmExpirationDate" value="#{row.smExpirationDate}" >
								<f:convertDateTime type="date" pattern="yyyy/MM/dd"/>
							</h:outputText>	
			            </rich:column>
  
			            <rich:column>
                        	<f:facet name="header">Primary Situs Code</f:facet>
                        	<h:outputText id="rowsmPrimarySitusCode" value="#{row.smPrimarySitusCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Primary Taxtype Code</f:facet>
                        	<h:outputText id="rowsmPrimaryTaxtypeCode" value="#{row.smPrimaryTaxtypeCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Secondary Situs Code</f:facet>
                        	<h:outputText id="rowsmSecondarySitusCode" value="#{row.smSecondarySitusCode}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Secondary Taxtype Code</f:facet>
                        	<h:outputText id="rowsmSecondaryTaxtypeCode" value="#{row.smSecondaryTaxtypeCode}" />
			            </rich:column>
			           
	                    <rich:column>
                        	<f:facet name="header">Comments</f:facet>
                        	<h:outputText id="rowsmComments" value="#{row.smComments}" />
			            </rich:column>
			            			            			            	          	        
	                    <rich:column>
                        	<f:facet name="header">All Levels Flag</f:facet>
                        	<h:outputText id="rowsmActiveFlag" value="#{row.smActiveFlag}" />
			            </rich:column>
			            
			            <rich:column>
                        	<f:facet name="header">Active Flag</f:facet>
                        	<h:outputText id="rowtdSpecialRate" value="#{row.smAllLevelsFlag}" />
			            </rich:column>	               
			            
                    </rich:dataTable>
                  </div>
		              <rich:datascroller id="scroll" for="batchTable" maxPages="10" oncomplete="initScrollingTables();"
			                               align="center" stepControls="auto" ajaxSingle="false" 
                                     reRender="pageInfo"
			                               page="#{dataUtilitySitusRulesBean.bcpSitusRuleDataModel.curPage}" />
                </div>
              </div>
              <!--  content -->
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="unformatted">
                      <h:commandLink id="viewufr"
                                     action="#{dataUtilitySitusRulesBean.viewUnformattedAction}" />
                    </li>
                    <li class="ok2">
                      <h:commandLink id="cancelAction"
                                     action="#{dataUtilitySitusRulesBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
