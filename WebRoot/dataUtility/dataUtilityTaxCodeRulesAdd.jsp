<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript"></script>
    </ui:define>
    <ui:define name="body">
      <h:form enctype="multipart/form-data"
              id="form">
        <h1>
          <img id="imgTaxRateUpdates" alt="Taxcode Rules Updates" src="../images/headers/hdr-tax-code-rules-updates.gif"
               width="250"
               height="19" />
        </h1>
        <div id="bottom">
          <div id="table-four">
            <div id="table-four-top">
              <a4j:outputPanel id="msg">
                <h:messages errorClass="error" />
              </a4j:outputPanel>
            </div>
            <div id="table-one-content">
              <table cellpadding="0"
                     cellspacing="0"
                     width="100%"
                     id="rollover"
                     class="ruler">
                <thead>
                  <tr>
                    <td colspan="5">Import Batch Data</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th colspan="5">Batch</th>
                  </tr>
                  <tr>
                    <td style="width: 50px;">&#160;</td>
                    <td>Last Year/Month Processed:</td>
                    <td>
                      <h:inputText size="20" id="txtLastTaxCodeRulesRelUpdate"
                                   value="#{dataUtilityTaxCodeRulesBean.lastTaxCodeRulesRelUpdate}"
                                   disabled="true" />
                    </td>
                    <td>&#160;</td>
                    <td style="width: 500px;">&#160;</td>
                  </tr>
                  <tr>
                    <td>&#160;</td>
                    <td>TaxCode Rules/Ref Docs Data File:</td>
                    <c:if test="#{dataUtilityTaxCodeRulesBean.displayFileSelect}">
                      <td colspan="2">
                        <t:inputFileUpload id="uploadFile"
                                           binding="#{dataUtilityTaxCodeRulesBean.uploadFile}"
                                           size="70"
                                           label="TaxCode Rules data file"
                                           required="true"
                                           immediate="true">
                          <f:validator validatorId="TaxCodeRulesFileUpload" />
                        </t:inputFileUpload>
                      </td>
                      <td colspan="1">
                        <h:commandButton id="fileSel"
                                         value="Select File"
                                         type="button"
                                         immediate="true"
                                         action="#{dataUtilityTaxCodeRulesBean.fileUploadAction}" />
                      </td>
                    </c:if>
                    <c:if test="#{dataUtilityTaxCodeRulesBean.displayChangeFile}">
                      <td colspan="2">
                        <h:inputText size="70" value="#{dataUtilityTaxCodeRulesBean.uploadFileName}"
                                     disabled="true"/>
                      </td>
                      <td colspan="1">
                        <h:commandButton id="fileSel"
                                         value="Change File"
                                         type="button"
                                         immediate="true"
                                         rendered="#{dataUtilityTaxCodeRulesBean.displayUploadAdd}" 
                                         action="#{dataUtilityTaxCodeRulesBean.processAddUpdateAction}" />
                      </td>
                    </c:if>
                  </tr>
                  <c:if test="#{dataUtilityTaxCodeRulesBean.displayFileHeader}">
                    <tr>
                      <th colspan="5">File Header Information</th>
                    </tr>
                    <tr>
                      <td>&#160;</td>
                      <td>File Type ( Full or Update):</td>
                      <td>
                        <h:inputText id="fType"
                                     value="#{dataUtilityTaxCodeRulesBean.fileType}"
                                     disabled="#{true}" />
                      </td>
                      <td>&#160;</td>
                      <td>&#160;</td>
                    </tr>
                    <tr>
                      <td>&#160;</td>
                      <td>Year/Month of Data:</td>
                      <td>
                        <h:inputText id="rDate"
                                     value="#{dataUtilityTaxCodeRulesBean.releaseDate}"
                                     disabled="#{true}">
					                <f:convertDateTime pattern="yyyy_MM" />
                        </h:inputText>
                      </td>
                      <td>Release of Data:</td>
                      <td>
                        <h:inputText id="rVer"
                                     value="#{dataUtilityTaxCodeRulesBean.releaseVersion}"
                                     disabled="#{true}" >
                                     <f:convertNumber integerOnly="true" groupingUsed="false"/>
                  		</h:inputText>
                      </td>
                    </tr>
                  </c:if>
                </tbody>
              </table>
            </div>
            <a4j:outputPanel id="progressPanel" style="width: 100%">
              <rich:progressBar id="progressBar"
                                value="#{dataUtilityTaxCodeRulesBean.progress}"
                                styleClass="sts-progress-bar"
                                interval="2000"
                                mode="ajax"
                                label="Upload progress: #{dataUtilityTaxCodeRulesBean.progress} %"
                                enabled="#{dataUtilityTaxCodeRulesBean.displayProgress}"
                                minValue="0"
                                maxValue="99"
                                reRenderAfterComplete="completeAction,addAction,cancelAction, msg">
                <f:facet name="complete">
                  <h:outputText style="text-align: right; width: 50%; margin:auto" value="Upload Complete"/>
                </f:facet>
              </rich:progressBar>
            </a4j:outputPanel>
            <div id="table-four-bottom">
              <ul class="right">
                  <li class="ok2">
                  <a4j:commandLink id="addAction"
                                   rendered="#{dataUtilityTaxCodeRulesBean.displayUploadAdd}"
                                   reRender="progressPanel"
                                   oncomplete="turnOff('form:addAction', true); turnOff('form:fileSel', true)"
                                   action="#{dataUtilityTaxCodeRulesBean.addAction}"/>
                  </li>
                  <li class="close">
                  <a4j:commandLink id="completeAction"
                                   style="display: #{dataUtilityTaxCodeRulesBean.displayComplete ? '' : 'none'}"
                                   action="#{dataUtilityTaxCodeRulesBean.backgroundCompleteAction}"/>
                  </li>
                  <li class="cancel">
                  <a4j:commandLink id="cancelAction" immediate="true"
                                   style="display: #{dataUtilityTaxCodeRulesBean.displayComplete ? 'none': ''}"
                                   action="#{dataUtilityTaxCodeRulesBean.cancelAction}" />
                  </li>
              </ul>
            </div>
          </div> <!-- t4 -->
        </div> <!-- top -->
      </h:form>
    </ui:define>
  </ui:composition>
</html>
