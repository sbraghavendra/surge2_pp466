<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   registerEvent(window, "load", function() { selectRowByIndex('taxRateUpdateForm:batchTable', 'batchListRowIndex'); } );
   //]]>
   </script>
  </ui:define>
  <ui:define name="body">
   <h:inputHidden id="batchListRowIndex"
                  value="#{dataUtilityTaxRateUpdatesBean.selectedBatchIndex}" />
   <h1>
    <img id="imgTaxRateUpdates" alt="Tax Rate Updates" src="../images/headers/hdr-tax-rate-updates.gif"
         width="250"
         height="19" />
   </h1>
   <h:form id="taxRateUpdateForm">
   <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
    <ui:param name="bean" value="#{dataUtilityTaxRateUpdatesBean}"/>
   </a4j:include>
   <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
    <div id="bottom">
     <div id="table-four">
      <div id="table-four-top">
       <a4j:outputPanel id="msg">
        <h:message for = "taxRateUpdateForm" errorClass="error" />
       </a4j:outputPanel>
       
       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{dataUtilityTaxRateUpdatesBean.batchMaintenanceDataModel.pageDescription}"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />    
       
      </div>
      <div id="table-four-content">
       <ui:include src="/WEB-INF/view/components/batch_status.xhtml">
        <ui:param name="bean"
                  value="#{dataUtilityTaxRateUpdatesBean}" />
        <ui:param name="isTR"
                  value="true" />
        <ui:param name="showSelected"
                  value="true" />
        <ui:param name="onsubmit"
                  value="selectRow('taxRateUpdateForm:batchTable', this);" />
        <ui:param name="reRender"
                  value="process,statistics,error,view,updateBatch" />
       </ui:include>
      </div>
       <div id="table-four-bottom">
        <ul class="right">
         <li class="add">
          <h:commandLink id="add"
                         action="#{dataUtilityTaxRateUpdatesBean.processAddUpdateAction}" 
                         disabled = "#{dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" />
         </li>
            <li class="update">
                <h:commandLink id="updateBatch"
                               disabled="#{(dataUtilityTaxRateUpdatesBean.selectedBatchIndex == -1 or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)}"
                               style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
                               immediate="true"
                               action="#{dataUtilityTaxRateUpdatesBean.updateUserFieldsAction}" />
            </li>
         <li class="select-all">
          <h:commandLink id="selectAll"
                         action="#{dataUtilityTaxRateUpdatesBean.selectAllAction}" />
         </li>
         <li class="process">
          <h:commandLink id="process"
                         disabled="#{! dataUtilityTaxRateUpdatesBean.displayProcess or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
                         action="#{dataUtilityTaxRateUpdatesBean.processAction}" />
         </li>
         <li class="statistics">
          <h:commandLink id="statistics"
                         disabled="#{(dataUtilityTaxRateUpdatesBean.selectedBatchIndex == -1)}"
                         action="#{dataUtilityTaxRateUpdatesBean.statisticsAction}" />
         </li>
         <li class="error3">
          <h:commandLink id="error"
                         disabled="#{! dataUtilityTaxRateUpdatesBean.displayError}"
                         action="#{dataUtilityTaxRateUpdatesBean.errorsAction}" />
         </li>
         <li class="view">
          <h:commandLink id="view"
                         disabled="#{(dataUtilityTaxRateUpdatesBean.selectedBatchIndex == -1)}"
                         action="#{dataUtilityTaxRateUpdatesBean.viewAction}" />
         </li>
         <li class="refresh">
          <h:commandLink id="btnRefresh" action="#{dataUtilityTaxRateUpdatesBean.refreshAction}" />
         </li>
        </ul>
       </div>
     </div>
    </div>
   </h:form>
  </ui:define>
 </ui:composition>
</html>
