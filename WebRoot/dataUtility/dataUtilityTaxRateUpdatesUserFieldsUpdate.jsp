<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
        <script type="text/javascript">
            //<![CDATA[
            //]]>
        </script>
    </ui:define>
    <ui:define name="body">
        <f:view>
            <h:form id="batchMaintStatusUpdate">
                <!-- <h1>
                <img id="imgBatchMaintenanceStatus" alt="Batch Maintenance" src="../images/headers/hdr-batch-maintenance-status.gif"
                width="250"
                height="19" />
                </h1>-->
                <div id="table-four-content">
                    <ui:include src="/WEB-INF/view/components/batch_user_fields.xhtml">
                        <ui:param name="bean"
                                  value="#{dataUtilityTaxRateUpdatesBean}" />

                    </ui:include>
                </div>

            </h:form>
        </f:view>
    </ui:define>
</ui:composition>
</html>
