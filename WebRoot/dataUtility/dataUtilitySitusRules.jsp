<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">
	<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
		<ui:define name="script">
			<script type="text/javascript">
				//         
				registerEvent(window, "load", function() {
					selectRowByIndex('situsCodeRulesForm:batchTable', 'batchListRowIndex');
				});
				//
			</script>
		</ui:define>
		<ui:define name="body">
   			<h:inputHidden id="batchListRowIndex"
                  value="#{dataUtilitySitusRulesBean.selectedBatchIndex}" />
            <h1>
		    	<img id="imgTaxRateUpdates" alt="Situs Rules Updates" src="../images/headers/hdr-situs-rules-updates.gif"
			         width="250"
			         height="19" />
		   </h1>
		   
		   <h:form id="situsCodeRulesForm">
		    <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
   			  <ui:param name="bean" value="#{dataUtilitySitusRulesBean}"/>
  			</a4j:include>
			<div class="wrapper">
		      <span class="block-right">&#160;</span>
		      <span class="block-left tab">
		       <img src="../images/containers/STSView-batches.gif"
		            width="192"
		            height="17" />
		      </span>
		     </div>
		   		<div id="bottom">
			     <div id="table-four">
			      <div id="table-four-top">
			       <a4j:outputPanel id="msg">
			        <h:message for = "situsCodeRulesForm" errorClass="error" />
			       </a4j:outputPanel>
			       
			       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
			       <a4j:status id="pageInfo"  
							startText="Request being processed..." 
							stopText="#{dataUtilitySitusRulesBean.batchMaintenanceDataModel.pageDescription}"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
			     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />    
			       
			      </div>
			      
			      <div id="table-four-content">
			       <ui:include src="/WEB-INF/view/components/batch_status.xhtml">
			        <ui:param name="bean"
			                  value="#{dataUtilitySitusRulesBean}" />
			        <ui:param name="isTR"
			                  value="false" />
			        <ui:param name="showSelected"
			                  value="true" />
			        <ui:param name="onsubmit"
			                  value="selectRow('situsCodeRulesForm:batchTable', this);" />
			        <ui:param name="reRender"
			                  value="process,statistics,error,view,updateBatch" />
			       </ui:include>
			      </div>
			      
			      <div id="table-four-bottom">
			        <ul class="right">
			         <li class="add">
			          <h:commandLink id="add"
			                         action="#{dataUtilitySitusRulesBean.processAddUpdateAction}" 
			                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
			                         disabled = "#{dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"/>
			         </li>
			<!--			<li class="update">
							<h:commandLink id="updateBatch"
										   disabled="#{(dataUtilitySitusRulesBean.selectedBatchIndex == -1)}"
										   immediate="true"
										   action="#{dataUtilitySitusRulesBean.updateUserFieldsAction}" />
						</li>-->
			         <li class="select-all">
			          <h:commandLink id="selectAll"
			                         action="#{dataUtilitySitusRulesBean.selectAllAction}" />
			         </li>
			         <li class="process">
			          <h:commandLink id="process"
			                         disabled="#{! dataUtilitySitusRulesBean.displayProcess or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
			                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
			                         action="#{dataUtilitySitusRulesBean.processAction}" />
			         </li>
			         <li class="statistics">
			          <h:commandLink id="statistics"
			                         disabled="#{(dataUtilitySitusRulesBean.selectedBatchIndex == -1 or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)}"
			                         action="#{dataUtilitySitusRulesBean.statisticsAction}" />
			         </li>
			         <li class="error3">
			          <h:commandLink id="error"
			                         disabled="#{! dataUtilitySitusRulesBean.displayError}"
			                         action="#{dataUtilitySitusRulesBean.errorsAction}" />
			         </li>
			         <li class="view">
			          <h:commandLink id="view"
			                         disabled="#{(dataUtilitySitusRulesBean.selectedBatchIndex == -1)}"
			                         action="#{dataUtilitySitusRulesBean.viewAction}" />
			         </li>
			         <li class="refresh">
			          <h:commandLink id="btnRefresh" action="#{dataUtilitySitusRulesBean.refreshAction}" />
			         </li>
			        </ul>
			       </div>
			     </div>
			     </div>
		   </h:form>
		</ui:define>
	</ui:composition>
</html>