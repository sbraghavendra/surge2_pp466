<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { displayWarning(); } );

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function addAllocation()
{
	var percent = document.getElementById("matrixDetailForm:jurisdictionControl").value*100;
	if(percent>100 || percent=="") {
		alert("Please enter a decimal value less than or equal to 1" );
		return false;
	} else if (isNaN(percent)) {
		alert("Allocation percent should be a decimal number");
		return false;
	}
	return true;
}
function numToString(num, decimalNum) {
	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// Get sign of number
	
	// Adjust number so only the specified number of numbers after
	// the decimal point are shown.
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Readjust for sign
	return new String(tmpNum);
}

function updateTotal() {
  var total = 0.0;
  for (var i = 0; i < 1000; i++) {
    var pct = document.getElementById("matrixDetailForm:aMDetail:"+i+":pct");
    if (!pct) break;
    total += parseFloat(pct.value);
  } 

  var out = formatNumber(numToString(total,10), 2);
  replaceInnerHTML("matrixDetailForm:total",out);

  out = formatNumber(numToString(1.0 - total,10), 2); 
  replaceInnerHTML("matrixDetailForm:balance",out);
  replaceInnerHTML("matrixDetailForm:balance2",out);
}

function replaceInnerHTML(id,val){
	var obj = document.getElementById(id);
	if(obj) {
		obj.innerHTML = val;
	}
}


//]]>
</script>
</ui:define>

<ui:define name="body">
	<h:inputHidden id="displayWarning" value="#{allocationMatrixBean.displayWarning}"/>

	<h:form id="matrixDetailForm" >
		<h1><img id="imgAllocationMatrix" alt="Allocation Matrix" src="../images/headers/hdr-allocation-matrix.png" width="250" height="19" /></h1>
		<div id="top">
		<div id="table-one">
		<div id="table-one-top">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>
		<div id="table-one-content">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<thead>
				<tr>
					<td colspan="4"><h:outputText value="#{allocationMatrixBean.actionText}"/> an Allocation Matrix Line <h:outputText value="#{locationMatrixBean.actionTextSuffix}"/></td>
					<td colspan="4" style="text-align:right;">Allocation Matrix ID:&#160;<h:outputText value="#{allocationMatrixBean.matrixId}"/></td>
				</tr>
			</thead>
			<tr>
				<th colspan="9">Driver Selection</th>
			</tr>
		</table>

		<h:panelGrid id="driverPanel"
			rendered="#{!allocationMatrixBean.updateAction and !allocationMatrixBean.readOnlyAction}" 
			binding="#{allocationMatrixBean.copyAddPanel}"
			styleClass="panel-ruler"/>
		<h:panelGrid 
			rendered="#{allocationMatrixBean.updateAction or allocationMatrixBean.readOnlyAction}" 
			binding="#{allocationMatrixBean.viewPanel}"
			styleClass="panel-ruler"/>

		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tr>
				<th colspan="7">Details</th>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td>
					<rich:calendar binding="#{allocationMatrixBean.effectiveDateCalendar}"
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{allocationMatrixBean.readOnlyAction}"
						value="#{allocationMatrixBean.selectedMatrix.effectiveDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" id="effDate" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Expiration Date:</td>
				<td>
					<rich:calendar popup="true" enableManualInput="true"
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{allocationMatrixBean.readOnlyAction}"
						value="#{allocationMatrixBean.selectedMatrix.expirationDate}"
						validator="#{allocationMatrixBean.validateExpirationDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" id="expDate" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Comments:</td>
				<td><h:inputText id="comments"  
					value="#{allocationMatrixBean.selectedMatrix.comments}" style="width:650px;" 
					disabled="#{allocationMatrixBean.readOnlyAction}"
					maxlength="255"/>
				</td>
			</tr>
		</table>

		</div> <!-- table-one-content -->
		</div>
		</div>
		
		<div id="bottom">
		<div id="table-four">
		<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
		<div id="table-four-content">
		
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<th colspan="5">Allocation</th>
				</tr>
			</tbody>
		</table>

		<div class="scrollContainer">
		<div class="scrollInner">
		<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail" styleClass="GridContent"
			value="#{allocationMatrixBean.selectedMatrixAllocDetails}" var="allocMatrixDetail">
			
            <rich:column id="geocode">
                <f:facet name="header"><h:outputText value="GeoCode&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.geocode}"/>
            </rich:column>
            <rich:column id="state">
                <f:facet name="header"><h:outputText value="State&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.state}"/>
            </rich:column>
            <rich:column id="county">
                <f:facet name="header"><h:outputText value="County&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.county}"/>
            </rich:column>
            <rich:column id="city">
                <f:facet name="header"><h:outputText value="City&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.city}"/>
            </rich:column>
            
            <rich:column id="country">
                <f:facet name="header"><h:outputText value="Country&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.country}"/>
            </rich:column>
            <rich:column id="zip">
                <f:facet name="header"><h:outputText value="Zip Code&#160;&#160;" /></f:facet>
                <h:outputText value="#{allocMatrixDetail.jurisdiction.zip}"/>
            </rich:column>
            <rich:column id="allocationPercent" style="text-align: right;">
				<f:facet name="header"><h:outputText value="Allocation Percent" /></f:facet>
				<h:panelGroup>
					<h:inputText id="pct" rendered="#{!allocationMatrixBean.readOnlyAction}" label="Allocation Percent"
						value="#{allocMatrixDetail.allocationPercent}" style="text-align: right;"
						onkeypress="return onlyNumerics(event);">
						<a4j:support event="onblur" oncomplete="updateTotal();" />
						<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						<f:validateDoubleRange minimum="0" maximum="1"/>
					</h:inputText>
					<h:outputText rendered="#{allocationMatrixBean.readOnlyAction}" value="#{allocMatrixDetail.allocationPercent}" style="text-align: right;">
						<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
					</h:outputText>
				</h:panelGroup>
			</rich:column>
            <rich:column id="delete" rendered="#{!allocationMatrixBean.readOnlyAction}">
				<f:facet name="header"><h:outputText id="deleteHdr" escape="false" value="&#160;&#160;&#160;&#160;&#160;" /></f:facet>
                <a4j:commandButton id="deleteDetailBtn" value="Delete"
                	
                	oncomplete="initScrollingTables();"
                	reRender="aMDetail,total,balance,balance2">
					<a4j:actionparam name="instanceCreatedId"
						assignTo="#{allocationMatrixBean.instanceCreatedId}" actionListener="#{allocationMatrixBean.deleteMatrixDetail}"
						value="#{allocMatrixDetail.instanceCreatedId}"/>
                </a4j:commandButton>
            </rich:column>
		
		</rich:dataTable>
		</div>
		</div>

		<h:panelGroup rendered="#{!allocationMatrixBean.readOnlyAction}">
		<table cellpadding="2" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px">Total:</td>
					<td class="column-input">
						<h:outputText id="total" value="#{allocationMatrixBean.selectedMatrix.totalAllocationPercent}" >
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px" >Remaining:</td>
					<td class="column-input">
						<h:outputText id="balance" value="#{1 - allocationMatrixBean.selectedMatrix.totalAllocationPercent}">
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px" >Jurisdiction:</td>
					<td colspan="5">
					<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
						<ui:param name="handler" value="#{allocationMatrixBean.jurisdictionHandler}"/>
						<ui:param name="id" value="jurisInput1"/>
						<ui:param name="readonly" value="false"/>
						<ui:param name="required" value="false"/>
						<ui:param name="showheaders" value="true"/>
						<ui:param name="popupName" value="searchJurisdiction"/>
						<ui:param name="popupForm" value="searchJurisdictionForm"/>
					</ui:include>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px" >Allocation Percent:</td>
					<td>
						<h:inputText id="jurisdictionControl" label="Allocation Percent" immediate="true"
							binding="#{allocationMatrixBean.allocationPercent}" style="text-align:right;"
							onkeypress="return onlyNumerics(event);">
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
							<f:validateDoubleRange minimum="0" maximum="1"/>
						</h:inputText>
					</td>
					<td colspan="4">
					
						<h:outputText value="Please enter a percentage between 0.00 and "/>
						<h:outputText id="balance2" value="#{1 - allocationMatrixBean.selectedMatrix.totalAllocationPercent}">
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
						<h:outputText value="."/>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">&#160;</td>
					<td>
						<h:commandButton id="Add" value="Add" immediate="true" action="#{allocationMatrixBean.addMatrixDetail}" reRender="aMDetail"/>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
		</table>
		</h:panelGroup>
		
		<h:panelGroup rendered="#{allocationMatrixBean.readOnlyAction}">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Total:</td>
					<td class="column-input">
						<h:outputText id="txtTotalAllocationPercent" value="#{allocationMatrixBean.selectedMatrix.totalAllocationPercent}" >
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Remaining:</td>
					<td class="column-input">
						<h:outputText id="txtRemainingPercent" value="#{1 - allocationMatrixBean.selectedMatrix.totalAllocationPercent}">
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
		</table>
		</h:panelGroup>
	
		</div>
		<div id="table-one-bottom">
		
			<ul class="right">
				<li class="ok"><h:commandLink id="okBtn" rendered="#{!allocationMatrixBean.readOnlyAction}" action="#{allocationMatrixBean.saveAction}"/></li>
				<li class="ok"><h:commandLink id="btnDelete" rendered="#{allocationMatrixBean.readOnlyAction and !allocationMatrixBean.viewAction}" immediate="#{allocationMatrixBean.viewAction}" action="#{allocationMatrixBean.deleteAction}"/></li>
				<li class="ok"><h:commandLink id="btnView" rendered="#{allocationMatrixBean.readOnlyAction and allocationMatrixBean.viewAction}" immediate="#{allocationMatrixBean.viewAction}" action="#{allocationMatrixBean.viewAction}"/></li>
				<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!allocationMatrixBean.viewAction}" immediate="true" action="#{allocationMatrixBean.cancelAction}" /></li>
			</ul>
		</div>
		</div>

		</div>
	</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{allocationMatrixBean.jurisdictionHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="matrixDetailForm:jurisInput1"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{allocationMatrixBean}"/>
	<ui:param name="id" value="warning"/>
	<ui:param name="warning" value="Warning - not all driver values were found.  Save anyway?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBtn"/>
</ui:include>

</ui:define>

</ui:composition>
		
		
</html>
