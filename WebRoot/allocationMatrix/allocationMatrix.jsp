<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMList', 'aMListRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMDetail', 'aMDetailListRowIndex'); } );
registerEvent(window, "unload", function() { document.getElementById('matrixDetailForm:hiddenSaveLink').onclick(); });
//]]>
</script>
</ui:define>

<ui:define name="body">
	<h:inputHidden id="aMListRowIndex" value="#{allocationMatrixBean.selectedMasterRowIndex}"/>
	<h:inputHidden id="aMDetailListRowIndex" value="#{allocationMatrixBean.selectedRowIndex}"/>
	
	<h:form id="matrixDetailForm">
		<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
			<a4j:support event="onclick" actionListener="#{allocationMatrixBean.filterSaveAction}" />
		</a4j:commandLink>
		
		<h1><img id="imgAllocationMatrix" alt="Allocation Matrix" src="../images/headers/hdr-allocation-matrix.png" width="250" height="19" /></h1>
		<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif"/></div>
		<div id="top">
		<div id="table-one">
		<div id="table-one-top">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>
		<div id="table-one-content" onkeyup="return submitEnter(event,'matrixDetailForm:searchBtn')" >

		<h:panelGrid id="filterPanel" binding="#{allocationMatrixBean.filterSelectionPanel}" styleClass="panel-ruler"/>

		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Jurisdiction:</td>
				<td colspan="5">
					<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
						<ui:param name="handler" value="#{allocationMatrixBean.filterHandler}"/>
						<ui:param name="id" value="jurisInput"/>
						<ui:param name="readonly" value="false"/>
						<ui:param name="showheaders" value="true"/>
						<ui:param name="popupName" value="searchJurisdiction"/>
						<ui:param name="popupForm" value="searchJurisdictionForm"/>
					</ui:include>
				</td>
				<td>&#160;</td>
			</tr>
			</tbody>
		</table>
		
		<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
			<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Allocation Matrix ID: </td> 
				<td class="column-input">
					<h:inputText id="allocMatrixId" style="text-align:right;" 
						value="#{allocationMatrixBean.filterSelection.allocationMatrixId}" 
						onkeypress="return onlyIntegerValue(event);"
						label="Allocation Matrix ID">
						<f:convertNumber integerOnly="true" />
					</h:inputText>
				</td>
				<td class="column-label">&#160;</td> 
				<td class="column-input">&#160;</td>
				<td class="column-label">&#160;</td> 
				<td class="column-input">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			</tbody>
		</table>
		</div>

		<div id="table-one-bottom">
		<ul class="right">
			<li class="clear"><a4j:commandLink id="btnClear" action="#{allocationMatrixBean.resetFilter}"/></li>
			<li class="search"><a4j:commandLink id="searchBtn" action="#{allocationMatrixBean.updateMatrixList}" >
<!--			 	<rich:componentControl event="onclick" for="loader" attachTo="searchBtn" operation="show"/>    -->            
			</a4j:commandLink></li>
		</ul>
		</div>
		</div>
		</div>
		
		<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />

		<div class="wrapper"><span class="block-right">&#160;</span> 
		<span class="block-left tab">
		<img src="../images/containers/STSAllocation-Matrix-Lines-and-Detials.gif" width="192" height="17" />
		</span></div>

		<div id="bottom">
		<div id="table-four">
		<div id="table-four-top" style="padding-left: 15px;">
			<a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{allocationMatrixBean.matrixDataModel.pageDescription}"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		</div>
		<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner" id="resize">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMList"
				value="#{allocationMatrixBean.matrixDataModel}" var="matrix"
				rows="#{allocationMatrixBean.matrixDataModel.pageSize}">
				
				<a4j:support event="onRowClick"
						onsubmit="selectRow('matrixDetailForm:aMList', this);"
						actionListener="#{allocationMatrixBean.selectedMasterRowChanged}"
						reRender="aMDetail,aMAlloc,copyAddBtn,deleteBtn,updateBtn,okBtn,viewBtn"/>
                
				<rich:column id="driver01" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_01']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver01" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver01']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_01']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver01}" />
				</rich:column>
				
				<rich:column id="driver02" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_02']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver02" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver02']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_02']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver02}" />
				</rich:column>
				
				<rich:column id="driver03" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_03']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver03" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver03']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_03']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver03}" />
				</rich:column>
				
				<rich:column id="driver04" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_04']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver04" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver04']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_04']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver04}" />
				</rich:column>
				
				<rich:column id="driver05" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_05']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver05" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver05']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_05']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver05}" />
				</rich:column>
				
				<rich:column id="driver06" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_06']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver06" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver06']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_06']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver06}" />
				</rich:column>
				
				<rich:column id="driver07" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_07']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver07" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver07']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_07']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver07}" />
				</rich:column>
				
				<rich:column id="driver08" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_08']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver08" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver08']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_08']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver08}" />
				</rich:column>
				
				<rich:column id="driver09" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_09']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver09" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver09']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_09']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver09}" />
				</rich:column>
				
				<rich:column id="driver10" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_10']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver10" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver10']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_10']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver10}" />
				</rich:column>
				
				<rich:column id="driver11" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_11']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver11" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver11']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_11']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver11}" />
				</rich:column>
				
				<rich:column id="driver12" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_12']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver12" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver12']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_12']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver12}" />
				</rich:column>
				
				<rich:column id="driver13" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_13']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver13" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver13']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_13']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver13}" />
				</rich:column>
				
				<rich:column id="driver14" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_14']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver14" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver14']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_14']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver14}" />
				</rich:column>
				
				<rich:column id="driver15" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_15']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver15" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver15']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_15']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver15}" />
				</rich:column>
				
				<rich:column id="driver16" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_16']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver16" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver16']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_16']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver16}" />
				</rich:column>
				
				<rich:column id="driver17" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_17']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver17" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver17']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_17']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver17}" />
				</rich:column>
				
				<rich:column id="driver18" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_18']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver18" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver18']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_18']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver18}" />
				</rich:column>
				
				<rich:column id="driver19" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_19']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver19" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver19']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_19']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver19}" />
				</rich:column>
				
				<rich:column id="driver20" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_20']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver20" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver20']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_20']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver20}" />
				</rich:column>
				
				<rich:column id="driver21" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_21']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver21" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver21']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_21']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver21}" />
				</rich:column>
				
				<rich:column id="driver22" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_22']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver22" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver22']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_22']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver22}" />
				</rich:column>
				
				<rich:column id="driver23" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_23']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver23" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver23']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_23']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver23}" />
				</rich:column>
				
				<rich:column id="driver24" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_24']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver24" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver24']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_24']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver24}" />
				</rich:column>
				
				<rich:column id="driver25" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_25']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver25" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver25']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_25']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver25}" />
				</rich:column>
				
				<rich:column id="driver26" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_26']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver26" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver26']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_26']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver26}" />
				</rich:column>
				
				<rich:column id="driver27" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_27']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver27" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver27']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_27']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver27}" />
				</rich:column>
				
				<rich:column id="driver28" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_28']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver28" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver28']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_28']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver28}" />
				</rich:column>
				
				<rich:column id="driver29" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_29']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver29" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver29']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_29']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver29}" />
				</rich:column>
				
				<rich:column id="driver30" rendered="#{!empty matrixCommonBean.allocationMatrixColumnMap['DRIVER_30']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver30" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['driver30']}"
							value="#{matrixCommonBean.allocationMatrixColumnMap['DRIVER_30']}"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
					</f:facet>
					<h:outputText value="#{matrix.driver30}" />
				</rich:column>

                <rich:column id="binaryWeight">
                    <f:facet name="header">
						<a4j:commandLink id="s_binaryWeight" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortOrder['binaryWeight']}"
							value="Binary Weight"
							actionListener="#{allocationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList,aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{matrix.binaryWeight}"/>
                </rich:column>  

            </rich:dataTable>
			</div>
			</div>
	
			<rich:datascroller id="trScroll" for="aMList" maxPages="10" oncomplete="initScrollingTables();"
				style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"
				page="#{allocationMatrixBean.matrixDataModel.curPage}" 
				reRender="pageInfo"/>
            
		<div class="scrollContainer" >
		<div width="100%" >
			<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail" width="894px"   
                value="#{allocationMatrixBean.selectedMatrixDetails}"
                var="allocationMatrixDetail" >
				
				<a4j:support event="onRowClick"
						onsubmit="selectRow('matrixDetailForm:aMDetail', this);"
						actionListener="#{allocationMatrixBean.selectedRowChanged}"
						reRender="aMAlloc,copyAddBtn,deleteBtn,updateBtn,okBtn,viewBtn"/>

                <rich:column id="allocationMatrixId">
                    <f:facet name="header">
						<a4j:commandLink id="s_allocationMatrixId" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['allocationMatrixId']}"
							value="Matrix Id"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText value="#{allocationMatrixDetail.allocationMatrixId}"/>
                </rich:column>

                <rich:column id="effectiveDate">
                    <f:facet name="header">
						<a4j:commandLink id="s_effectiveDate" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['effectiveDate']}"
							value="Effective Date"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{allocationMatrixDetail.effectiveDate}">

						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
                </rich:column>      
                
                <rich:column id="expirationDate">
                    <f:facet name="header">
						<a4j:commandLink id="s_expirationDate" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['expirationDate']}"
							value="Expiration Date"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{allocationMatrixDetail.expirationDate}">
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
                </rich:column> 

                <rich:column id="comments">
                    <f:facet name="header">
						<a4j:commandLink id="s_comments" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['comments']}"
							value="Comments"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{allocationMatrixDetail.comments}"/>
                </rich:column>         

                <rich:column id="updateUserId">
                    <f:facet name="header">
						<a4j:commandLink id="s_updateUserId" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['updateUserId']}"
							value="Last Update UserId"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{allocationMatrixDetail.updateUserId}"/>
                </rich:column> 

                <rich:column id="updateTimestamp">
                    <f:facet name="header">
						<a4j:commandLink id="s_updateTimestamp" styleClass="sort-#{allocationMatrixBean.matrixDataModel.sortDetailOrder['updateTimestamp']}"
							value="Time Stamp"
							actionListener="#{allocationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText  value="#{allocationMatrixDetail.updateTimestamp}">
						<f:converter converterId="dateTime"/>
					</h:outputText>
                </rich:column>                   

            </rich:dataTable>
		</div>
		</div>
		<br/>
		
		<div class="scrollContainer">
		<div class="scrollInner">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMAlloc" width="894px"    
                value="#{allocationMatrixBean.selectedMatrixAllocDetails}"
                var="allocMatrixDetail" >

                <rich:column id="geocode" sortBy="#{allocMatrixDetail.jurisdiction.geocode}">
                    <f:facet name="header"><h:outputText value="GeoCode" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.geocode}"/>
                </rich:column>
                <rich:column id="state" sortBy="#{allocMatrixDetail.jurisdiction.state}">
                    <f:facet name="header"><h:outputText value="State" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.state}"/>
                </rich:column>
                <rich:column id="county" sortBy="#{allocMatrixDetail.jurisdiction.county}">
                    <f:facet name="header"><h:outputText value="County" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.county}"/>
                </rich:column>
                <rich:column id="city" sortBy="#{allocMatrixDetail.jurisdiction.city}">
                    <f:facet name="header"><h:outputText value="City" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.city}"/>
                </rich:column>
                <rich:column id="zip" sortBy="#{allocMatrixDetail.jurisdiction.zip}">
                    <f:facet name="header"><h:outputText value="Zip Code" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.zip}"/>
                </rich:column>
                
                <rich:column id="country" sortBy="#{allocMatrixDetail.jurisdiction.country}">
                    <f:facet name="header"><h:outputText value="Country" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.jurisdiction.country}"/>
                </rich:column>
                
                <rich:column id="allocationPercent" sortBy="#{allocMatrixDetail.allocationPercent}" style="text-align: right;">
                    <f:facet name="header"><h:outputText value="Allocation Percent" /></f:facet>
                    <h:outputText value="#{allocMatrixDetail.allocationPercent}">
						<f:convertNumber type="percent" minFractionDigits="2" maxFractionDigits="10"/>
					</h:outputText>
                </rich:column>  
            </rich:dataTable>
		</div>
		</div>
		
		</div>
		<div id="table-four-bottom">
		<ul class="right">
			<li class="add"><h:commandLink id="addBtn" disabled="#{allocationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{allocationMatrixBean.displayAddAction}" 
											style="#{(allocationMatrixBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>
			<li class="copy-add"><h:commandLink id="copyAddBtn" style="#{(allocationMatrixBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!allocationMatrixBean.validSelection or allocationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{allocationMatrixBean.displayCopyAddAction}"/></li>
			<li class="update"><h:commandLink id="updateBtn" style="#{(allocationMatrixBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!allocationMatrixBean.validSelection or allocationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{allocationMatrixBean.displayUpdateAction}"/></li>
			<li class="delete115"><h:commandLink id="deleteBtn" style="#{(allocationMatrixBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!allocationMatrixBean.validSelection or allocationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{allocationMatrixBean.displayDeleteAction}"/></li>
			<li class="view"><h:commandLink id="viewBtn"  disabled="#{!allocationMatrixBean.validSelection}" action="#{allocationMatrixBean.displayViewAction}"><f:param name="command" value="view" /></h:commandLink></li>
		</ul>
		</div>
		</div>

		</div>
	</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{allocationMatrixBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="matrixDetailForm:jurisInput"/>
</ui:include>

</ui:define>
		
</ui:composition>
		
</html>
