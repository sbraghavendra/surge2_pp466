<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
        function confirmDelete()
		{
		var entityLevelId = document.getElementById("entityLevelDelete:entityLevelId").value;
		var transDetailColumnName = document.getElementById("entityLevelDelete:transDetailColumnName").value;
		var description = document.getElementById("entityLevelDelete:description").value;
		var updateUserId = document.getElementById("entityLevelDelete:updateUserId").value;
		var updateTimestamp = document.getElementById("entityLevelDelete:updateTimestamp").value;
		return confirm("Confirm Delete Entity " + '\n'+ "StructureLevel: "+ entityLevelId+'\n' + "Trans. Dtl. Column Name:"+transDetailColumnName+'\n'+ "Description: "+description+'\n'+ "Update User Id: "+updateUserId+'\n'+"Update Timestamp:"+updateTimestamp)
		}  
//]]>
</script>
</ui:define>
    <ui:define name="body" >
	<f:view>
	<h:form id="entityLevelDelete">
<h1><img id="imgEntityStructure" alt="Entity Structure" src="../images/headers/hdr-entity-structure.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:230px;" >
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Delete An Entity Structure Level</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;"></td>
				<td>Structure Level:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.entityLevelId}" 
				         disabled="true" id="entityLevelId"
                         size="4"				
				         style="width:300px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;"></td>
				<td>Trans. Dtl. Column Name:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.transDetailColumnName}" 
                         size="4" disabled="true" 
                         id="transDetailColumnName"				
				         style="width:300px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;"></td>
				<td>Description:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.description}" 
                         size="4" disabled="true" 
                         id="description"				
				         style="width:300px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;"></td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.updateUserId}" 
				         disabled="true" id="updateUserId"
                         size="4"				
				         style="width:300px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;"></td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.updateTimestamp}" 
				         disabled="true" id="updateTimestamp"
				         style="width:300px;" >
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>

	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{securityModuleBackingBean.removeEntityLevelAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{securityModuleBackingBean.cancelAddEntityLevelAction}" /></li>
	</ul>
	</div>

	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>