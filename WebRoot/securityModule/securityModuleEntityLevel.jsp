<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('entityLevel:entityLevelsTable', 'selectedEntityLevelRowIndexId'); } );
//]]>
</script>
</ui:define>




<ui:define name="body" >
<h:inputHidden id="selectedEntityLevelRowIndexId" value="#{securityModuleBackingBean.selectedEntityLevelRowIndex}"/>
<f:view>
<h:form id="entityLevel">
<h1><h:graphicImage id="imgEntityStructure" alt="Entity Structure" value="/images/headers/hdr-entity-structure.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner" id="resize" >
            <rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			width="100%" 
			frozenColCount="1"
			id="entityLevelsTable"
			
			value="#{securityModuleBackingBean.entityLevelsList}"
			var="el" sortMode="single" rows="50">
            <a4j:support id="clk" event="onRowClick" 
            	onsubmit="selectRow('entityLevel:entityLevelsTable', this);"
                actionListener="#{securityModuleBackingBean.selectedEntityLevelRowChanged}" reRender="updateEntityStructure"/>
																
			<rich:column id="entityLevelId" style="width:120px;" sortBy="#{el.entityLevelId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Structure Id" />
				</f:facet>
				<h:outputText id="rowEntityLevelId" value="#{el.entityLevelId}" style="width:100px;text-align:center"/>
			</rich:column>
			<rich:column id="TransactionDetailColumn" style="width:180px;" sortBy="#{el.transDetailColumnName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Trans. Detail Column Name"/>
				</f:facet>
				<h:outputText id="rowTransDetailColumnName" value="#{el.transDetailColumnName}" style="width:200px;text-align:center;"/>
			</rich:column>
			<rich:column id="description" style="width:200px;" sortBy="#{el.description}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Description"/>
				</f:facet>
				<h:outputText id="rowDescription" value="#{el.description}" style="width:200px;text-align:center;"/>
			</rich:column>		
			<rich:column id="updateUserId" style="width:150px;" sortBy="#{el.updateUserId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update User Id"/>
				</f:facet>
				<h:outputText id="rowUpdateUserId" value="#{el.updateUserId}" style="width:130px;text-align:center;" />
			</rich:column>			
		    <rich:column id="updateTimestamp" style="width:130px;" sortBy="#{el.updateTimestamp}">
				<f:facet name="header">
					<h:outputText id="rowUpdateTimestamp" styleClass="headerText" value="Update Time Stamp" style="width:130px;"/>
				</f:facet>
				<h:outputText value="#{el.updateTimestamp}" style="width:130px;text-align:center">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>
			<!--
					    <rich:column id="businessUnitFlag">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Business Unit Flag"/>
				</f:facet>
				<h:outputText value="#{el.businessUnitFlag}">
				</h:outputText>
			</rich:column>
		-->
		</rich:dataTable>	
	</div>
	</div>
	</div>	
	<div id="table-four-bottom">
		
	<ul class="right">
	<!-- <li class="add"><h:commandLink id="addEntityStructure" action="#{securityModuleBackingBean.setUpForAddEntity}" /></li> -->
		<li class="update"><h:commandLink id="updateEntityStructure" style="#{(securityModuleBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!securityModuleBackingBean.showUpdateButton or securityModuleBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{securityModuleBackingBean.displayUpdateEntityStructureLevelAction}" /></li>
	<!-- <li class="delete"><h:commandLink id="deleteEntityStructure" action="#{securityModuleBackingBean.setUpForDelete}" disabled="#{!securityModuleBackingBean.disableDeleteButton}"/></li> -->
	</ul>
	</div>
	</div>
</div>

</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>