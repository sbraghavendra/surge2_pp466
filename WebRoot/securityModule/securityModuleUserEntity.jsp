<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form>
<h1><h:graphicImage id="imgUserEntity" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="898" id="rollover" class="ruler">
			<tr><th colspan="5">Entity Structure Level Maintenance</th></tr>
	</table>
                                                            <rich:scrollableDataTable
																styleClass="GridContent"
																height="225px"
																width="915px"
																frozenColCount="1"
																id="userEntityTable"
																value="#{securityModuleBackingBean.userEntityList}"
																rendered="#{securityModuleBackingBean.viewUserEntitys}"
																var="ue" sortMode="single">		
																<a4j:support event="onselectionchange" 
                                                                    actionListener="#{securityModuleBackingBean.selectedUserEntityRowChanged}"/>
																<rich:column id="roleCode">
																	<f:facet name="header">
																		<h:outputText styleClass="headerText" value="RoleCode" />
																	</f:facet>
																	<h:outputText style="width:20px;"
																		value="#{ue.roleCode}"></h:outputText>
																</rich:column>
																<rich:column id="userCode">
																	<f:facet name="header">
																		<h:outputText styleClass="headerText" value="UserCode"/>
																	</f:facet>
																	<h:outputText style="width:87px;"
																		value="#{ue.userCode}" />
																</rich:column>
																<rich:column id="entityId">
																	<f:facet name="header">
																		<h:outputText styleClass="headerText" value="EntityId"/>
																	</f:facet>
																	<h:outputText style="width:87px;"
																		value="#{ue.entityId}" />
																</rich:column>																
															</rich:scrollableDataTable>	  	
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="add2"><a href="#"></a></li>
		<li class="update2"><h:commandLink action="#{securityModuleBackingBean.displayUpdateEntityLevelAction}" /></li>
		<li class="deletebtm"><a href="#"></a></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>