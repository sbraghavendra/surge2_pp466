<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleUserMap">
<h1><h:graphicImage id="imgUserMap" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one" style = "width:1005px">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">User</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:10px;">&#160;</td>
				<td class = "column-label">User Code:</td>
				<td class = "column-input2">
				<h:inputText id="txtUserCode" value="#{securityModuleBackingBean.selectedUser.userCode}" label="Code"
				             style="width:200px;" disabled="true" />
				</td>
				<td colspan="2">&#160;</td>
				
			</tr>	
			<tr>
				<td style="width:10px;">&#160;</td>
				<td class = "column-label">User Name:</td>
				<td class = "column-input2">
				<h:inputText id="txtUserName" value="#{securityModuleBackingBean.selectedUser.userName}"	label="Name"
				             style="width:200px;" disabled="true" />
				</td>
				<td colspan="2">&#160;</td>
			</tr>	
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">	
		<thead>
			<tr><td colspan="5">Map to Entities/Roles</td></tr>
		</thead>
		<tbody>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td style="vertical-align: top">
				  <h:panelGrid columns="2" width="100%" >
				    <h:outputText id="EntitiesTitle" style="vertical-align:bottom;color:black;font-weight:bold;font-size:8pt " value="Entities :"/><BR/>
            		<rich:tree id="entityTreeId" style="overflow-y:auto;overflow-x:auto;width:310px;height:312px;background:white;padding:5px;margin-bottom:8px" nodeSelectListener="#{securityModuleBackingBean.processSelection}" 
                		 	ajaxSubmitSelection="true"  switchType="client" nodeFace="#{item.type}" reRender="entityTreeId"
                			value="#{securityModuleBackingBean.treeNode}" var="item" ajaxKeys="#{null}">
                		
                		<rich:treeNode type="mapped" iconLeaf="/images/icons/entity.gif" icon="/images/icons/entity.gif">
                			<h:outputText value="#{item.entityCode} - #{item.entityName}" style="color:gray;" />
            			</rich:treeNode>
            			<rich:treeNode type="selected" iconLeaf="/images/icons/entity.gif" icon="/images/icons/entity.gif">
                			<h:outputText value="#{item.entityCode} - #{item.entityName}"  style="color:black;Font;background:lightblue" />
            			</rich:treeNode>
            			<rich:treeNode type="" iconLeaf="/images/icons/entity.gif" icon="/images/icons/entity.gif">
                			<h:outputText value="#{item.entityCode} - #{item.entityName}" style="color:black;" />
            			</rich:treeNode>
            		</rich:tree>
        		  </h:panelGrid>
				</td>
	<!--  			
				<td>
				 <rich:orderingList id="lstEntity" value="#{securityModuleBackingBean.entityItemsList}" var="ei" listHeight="300"
				listWidth="50" controlsType="none" captionLabel="Entities :" 
				converter="entityItemConveter" selection="#{securityModuleBackingBean.selectedEntityList}">
					<rich:column id="entityCode">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Entity Code"/>
						</f:facet>
						<h:outputText id="rowEntityCode" value="#{ei.entityCode}" />
					</rich:column>
					<rich:column id="entityName">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Entity Name"/>
						</f:facet>
						<h:outputText id="rowEntityName" value="#{ei.entityName}" />
					</rich:column>
					<rich:column id="entityId">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Entity Id" />
						</f:facet>
						<h:outputText id="rowEntityId" value="#{ei.entityId}"/>
					</rich:column>
				</rich:orderingList>
				</td>	
	-->			
				<td>
				 <rich:orderingList id="lstRole" value="#{securityModuleBackingBean.rolesList}" var="role" listHeight="300"
				listWidth="200" controlsType="none" captionLabel="Roles :" 
				converter="roleConverter" selection="#{securityModuleBackingBean.selectedRoleList}">	
				<rich:column id="roleCode">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role Code" />
					</f:facet>
					<h:outputText id="rowRoleCode" value="#{role.roleCode}"/>
				</rich:column>
				<rich:column id="roleName">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role Name"/>
					</f:facet>
					<h:outputText id="rowRoleName" value="#{role.roleName}" />
				</rich:column>
				</rich:orderingList>
				</td>			
				<td><h:commandButton id="btnAssign" image="/images/actionbuttons/NCSTSAssign.JPG" type="button" style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
				action="#{securityModuleBackingBean.assignEntity}" reRender="entityTreeId" /><br/>&#160;<br/>
				<h:commandButton id="btnRemove" image="/images/actionbuttons/NCSTSRemove.JPG" type="button" 
				style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
				action="#{securityModuleBackingBean.removeEntity}" reRender="entityTreeId" /><br/>&#160;<br/>
				<h:commandButton id="btnRemoveAll" image="/images/actionbuttons/NCSTSRemoveAll.JPG" type="button" style="border-style:solid; border-width: 0px;border-color: SlateGrey" 
				action="#{securityModuleBackingBean.removeAllEntity}" reRender="entityTreeId"
				onclick="return confirm('Are you sure you want to UnMap All?')"/></td>
				<td>
				 <rich:orderingList id="lstAssigned" value="#{securityModuleBackingBean.userEntityRoleList}" var="userEntityRole" listHeight="300"
				listWidth="300" controlsType="none" captionLabel="Assigned Entity/Roles :" converter="userEntityRoleConverter"
				selection="#{securityModuleBackingBean.selectedMappingList}">
				<rich:column id="entityName">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Entity Name" />
					</f:facet>
					<h:outputText id="rowAssignedEntityName" value="#{userEntityRole.entityItem.entityName}"/>
				</rich:column>
				<rich:column id="roleName">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role Name" />
					</f:facet>
					<h:outputText id="rowAssignedRoleName" value="#{userEntityRole.role.roleName}"/>
				</rich:column>
				</rich:orderingList>
				</td>				
			</tr>	

		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{securityModuleBackingBean.mapAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{securityModuleBackingBean.cancelAction}" /></li>
		<li class="map"><h:commandLink id="mapBtn" action="#{securityModuleBackingBean.displayUserEntityMapAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>