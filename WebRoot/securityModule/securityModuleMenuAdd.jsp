<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleMenuAdd">
<h1><h:graphicImage id="imgMenuAdd" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{securityModuleBackingBean.actionText}"/> a Menu Option</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Menu Code:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.selectedMenu.menuCode}" 
							 validator="#{securityModuleBackingBean.validateMenuCode}"
						 	 disabled="#{securityModuleBackingBean.viewAction or securityModuleMenuAddsecurityModuleBackingBean.deleteAction or securityModuleBackingBean.updateAction}"
	                         required="true" label="Menu Code" id="txtMenuCode"
				             style="width:650px;" />
				</td>
			</tr>	
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Main Menu Code:</td>
				<td style="width:700px;">
				<h:selectOneMenu id="ddlMainMenuCode" value="#{securityModuleBackingBean.selectedMenu.mainMenuCode}" 
							 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
							 required="true" label="Main Menu Code">
					<f:selectItems value="#{securityModuleBackingBean.mainMenuItems}" />
				</h:selectOneMenu>
				</td>
			</tr>			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Menu Sequence:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedMenu.menuSequence}" 
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         onkeypress="return onlyIntegerValue(event);"
				         style="width:650px;" id="txtMenuSequence"
						label="Menu Sequence">
					<f:convertNumber integerOnly="true" />
				</h:inputText>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Option Name:</td>
				<td>
				<h:inputText id="txtOptionName" value="#{securityModuleBackingBean.selectedMenu.optionName}" 
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         style="width:650px;" />
				</td>
			</tr>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Command Type:</td>
				<td>
				<h:selectOneMenu id="ddlCommandType" value="#{securityModuleBackingBean.selectedMenu.commandType}"
					disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}">
					<f:selectItems value="#{securityModuleBackingBean.commandTypeItems}" />
				</h:selectOneMenu>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Command Line:</td>
				<td>
				<h:inputText id="txtCommandLine" value="#{securityModuleBackingBean.selectedMenu.commandLine}" 
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         style="width:650px;" />
				</td>
			</tr>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Key Values:</td>
				<td>
				<h:inputText id="txtKeyValues" value="#{securityModuleBackingBean.selectedMenu.keyValues}" 
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Panel Class Name:</td>
				<td>
				<h:inputText id="txtPanelClassName" value="#{securityModuleBackingBean.selectedMenu.panelClassName}" 
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText id="txtUpdateUserId" value="#{securityModuleBackingBean.selectedMenu.updateUserId}" disabled="true" style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText id="txtUpdateTimestamp" value="#{securityModuleBackingBean.selectedMenu.updateTimestamp}" disabled="true" style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{securityModuleBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{securityModuleBackingBean.viewAction}" immediate="true" action="#{securityModuleBackingBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>