<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('stsSecurityModuleData:rolesTable', 'rowTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('stsSecurityModuleData:usersTable', 'userTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('stsSecurityModuleData:menusTable', 'menuTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('stsSecurityModuleData:entityItemsTable', 'entityTableRowIndex'); } );
//]]>

function resetCombo(){
if(document.getElementById("stsSecurityModule:SelView")!=null){
document.getElementById("stsSecurityModule:SelView").value="0";
}
stsSecurityModule.submit();

}
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="rowTableRowIndex" value="#{securityModuleBackingBean.selectedRowIndex}"/>
<h:inputHidden id="userTableRowIndex" value="#{securityModuleBackingBean.selectedRowIndex}"/>
<h:inputHidden id="menuTableRowIndex" value="#{securityModuleBackingBean.selectedRowIndex}"/>
<h:inputHidden id="entityTableRowIndex" value="#{securityModuleBackingBean.selectedRowIndex}"/>

<h1><h:graphicImage id="imgSecurityModules" alt="Security Modules" value="/images/headers/hdr-roles-users.gif" width="250" height="19" /></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>

<h:form id="stsSecurityModule">
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
        <tr>
            <td style="width: 10px;">&#160;</td>
			<td>
				<h:selectOneMenu id="SelModule" styleClass="NCSTSSelector" title="Select View Type"
						style="width: 288px;" value="#{securityModuleBackingBean.selectedView}" 
						valueChangeListener="#{securityModuleBackingBean.viewChanged}"
						onchange="return resetCombo();">
<!--  					<f:selectItem id="selViewitem" itemLabel="Select a Module" itemValue="0"/> -->
						<f:selectItems id="selViewItems" value="#{securityModuleBackingBean.viewItems}" />
				</h:selectOneMenu>
			</td>
        </tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 20px;">
			<h:selectOneMenu id="SelView" styleClass="NCSTSSelector" title="Select View Type"
					style="width: 288px;" value="#{securityModuleBackingBean.selectedUserRole}" 
					rendered="#{securityModuleBackingBean.viewRoles or securityModuleBackingBean.viewUsers}"
					valueChangeListener="#{securityModuleBackingBean.selectedRoleorUserChanged}"
					onchange="submit();">
					<f:selectItems id="selViewUserRole" value="#{securityModuleBackingBean.userRoleComboItems}" />
			</h:selectOneMenu>
			</td>
		</tr>
		<a4j:region id = "entityCombo" rendered="#{securityModuleBackingBean.viewEntityItems}">
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 20px;">						
			<h:panelGrid id="selectedLevelPanel" columns="1" cellpadding="0" cellspacing="0">
			<h:selectOneMenu id="id1" styleClass="NCSTSSelector" 
					style="width: 288px;" value="#{securityModuleBackingBean.selectedLevelOneCombo}" 
					binding="#{securityModuleBackingBean.levelOneCombo}" immediate="true">  
					<a4j:support event="onchange" reRender="id2,id3,entityItemsTable,entInfo" actionListener="#{securityModuleBackingBean.entityItemChanged}"/>
			</h:selectOneMenu>
			<h:selectOneMenu id="id2" styleClass="NCSTSSelector" 
					style="width: 288px;" value="#{securityModuleBackingBean.selectedLevelTwoCombo}" 
					binding="#{securityModuleBackingBean.levelTwoCombo}" immediate="true">
					<a4j:support event="onchange" reRender="id3,entityItemsTable,entInfo" actionListener="#{securityModuleBackingBean.entityItemChanged}"/>
			</h:selectOneMenu>
			<h:selectOneMenu id="id3" styleClass="NCSTSSelector"
					style="width: 288px;" value="#{securityModuleBackingBean.selectedLevelThreeCombo}" 
					binding="#{securityModuleBackingBean.levelThreeCombo}" immediate="true">
					<a4j:support event="onchange" reRender="entityItemsTable,entInfo" actionListener="#{securityModuleBackingBean.entityItemChanged}"/>
			</h:selectOneMenu>				
			</h:panelGrid>
			</td>
		</tr>
		<tr>
			<td colspan="2">				
			<rich:dataTable rowClasses="odd-row,even-row" width="100%" id="entInfo" rendered="#{securityModuleBackingBean.viewEntityItems}">
			<f:facet name="header">
			<rich:columnGroup>
			<rich:column id="entityLevelId">
					<h:outputText styleClass="headerText" value="Entity Level Id"/>
			</rich:column>			
		    <rich:column id="parentEntityId">
					<h:outputText styleClass="headerText" value="Parent Entity Id"/>
			</rich:column>
			<rich:column id="entityLevelDescription">				
					<h:outputText styleClass="headerText" value="Description"/>				
			</rich:column>
			 <rich:column id="entityColumn">
					<h:outputText styleClass="headerText" value="Driver References"/>
			</rich:column>
			 <rich:column id="parentCode">
					<h:outputText styleClass="headerText" value="Parent Entity Code"/>
			</rich:column>
			 <rich:column id="parentName">
					<h:outputText styleClass="headerText" value="Parent Entity Name"/>
			</rich:column>
			 <rich:column id="parentEntityLevel">
					<h:outputText styleClass="headerText" value="Parent Entity Level Id"/>
			</rich:column>
			<rich:column id="parentLevelDescription">
					<h:outputText styleClass="headerText" value="Parent Description"/>
			</rich:column>
			<rich:column id="entityLevelIdD" breakBefore="true">
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.entityLevelId}" />
			</rich:column>			
		    <rich:column id="parentEntityIdD">
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.entityId}" />
			</rich:column>
			<rich:column id="entityLevelDescriptionD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.entityLevelDescription}"/>
			</rich:column>			 
			<rich:column id="entityColumnD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.entityColumn}"/>
			</rich:column>
			 <rich:column id="parentCodeD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.parentCode}"/>
			</rich:column>
			 <rich:column id="parentNameD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.parentName}"/>
			</rich:column>			 
			<rich:column id="parentEntityLevelD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.parentEntityLevel}"/>
			</rich:column>
			<rich:column id="parentLevelDescriptionD">				
				<h:outputText value="#{securityModuleBackingBean.parentEntityAndLevelInfo.parentLevelDescription}"/>
			</rich:column>
			
			</rich:columnGroup>
			</f:facet>
			 </rich:dataTable>
			</td>
		</tr>
		</a4j:region>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
	</ul>
	</div>
	</div>
</div>
</h:form>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>

<h:form id="stsSecurityModuleData">
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top">	</div>
	
	<div id="table-four-content" >
		<div class="scrollContainer">
		<div class="scrollInner" id="resize" >
	
	
		<rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			id="rolesDescTable"
			value="#{securityModuleBackingBean.userEntityMapList}"
			rendered="#{securityModuleBackingBean.viewRoles and securityModuleBackingBean.viewSublist}"
			var="role" >																
			<rich:column id="userCode1" width="200px" sortBy="#{role.userEntityPK.userCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="User Code" />
				</f:facet>
				<h:outputText  value="#{role.userEntityPK.userCode}"/>
			</rich:column>
			<rich:column id="userName1" width="200px" sortBy="#{role.userName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="User Name"/>
				</f:facet>
				<h:outputText value="#{role.userName}" />
			</rich:column>
			<rich:column id="entityCode1" width="200px" sortBy="#{role.entity.entityCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Code"/>
				</f:facet>
				<h:outputText value="#{role.entity.entityCode}" />
			</rich:column>
			<rich:column id="entityName1" width="200px" sortBy="#{role.entity.entityName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Name"/>
				</f:facet>
				<h:outputText value="#{role.entity.entityName}" />
			</rich:column>
		</rich:dataTable>

			<rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			id="userDescTable"
			value="#{securityModuleBackingBean.userEntityMapList}"
			rendered="#{securityModuleBackingBean.viewUsers and securityModuleBackingBean.viewSublist}"
			var="user">																
			
			<rich:column id="entityCode2" width="200px" sortBy="#{user.entity.entityCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Code"/>
				</f:facet>
				<h:outputText value="#{user.entity.entityCode}" />
			</rich:column>
			<rich:column id="entityName2" width="200px" sortBy="#{user.entity.entityName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Name"/>
				</f:facet>
				<h:outputText value="#{user.entity.entityName}" />
			</rich:column>
			<rich:column id="roleCode2" width="200px" sortBy="#{user.roleCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Role Code" />
				</f:facet>
				<h:outputText value="#{user.roleCode}"/>
			</rich:column>
			<rich:column id="roleName2" width="200px" sortBy="#{user.roleName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Role Name"/>
				</f:facet>
				<h:outputText value="#{user.roleName}" />
			</rich:column>
		</rich:dataTable>

		<rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			width="915px"
			id="rolesTable"
			value="#{securityModuleBackingBean.rolesList}"
			rendered="#{!securityModuleBackingBean.viewUsers and !securityModuleBackingBean.viewMenus and !securityModuleBackingBean.viewEntityItems and !securityModuleBackingBean.viewSublist}"
			var="role">
			<a4j:support event="onRowClick" 
				onsubmit="selectRow('stsSecurityModuleData:rolesTable', this);"
				actionListener="#{securityModuleBackingBean.selectedRoleRowChanged}"
				reRender="updateBtn,deleteBtn,mapBtn,viewBtn"/>	
																																			
			<rich:column id="roleCode" sortBy="#{role.roleCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Role Code" />
				</f:facet>
				<h:outputText value="#{role.roleCode}"/>
			</rich:column>
			<rich:column id="roleName" sortBy="#{role.roleName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Role Name"/>
				</f:facet>
				<h:outputText value="#{role.roleName}" />
			</rich:column>
			<rich:column id="globalBuFlag" sortBy="#{role.globalBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Global Matrix?"/>
				</f:facet>
        <h:selectBooleanCheckbox styleClass="check" value="#{role.globalBooleanFlag}" disabled="true" />
			</rich:column>																
			<rich:column id="activeFlag" sortBy="#{role.activeBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Active?"/>
				</f:facet>
        <h:selectBooleanCheckbox styleClass="check" value="#{role.activeBooleanFlag}" disabled="true" />
			</rich:column>
			<rich:column id="updateUserId" sortBy="#{role.updateUserId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update User Id"/>
				</f:facet>
				<h:outputText value="#{role.updateUserId}" />
			</rich:column>			
		    <rich:column id="updateTimestamp" sortBy="#{role.updateTimestamp}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update Timestamp"/>
				</f:facet>
				<h:outputText value="#{role.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>		
		</rich:dataTable>	 
		
		<rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			width="915px"
			id="usersTable"
			value="#{securityModuleBackingBean.usersList}"
			rendered="#{securityModuleBackingBean.viewUsers and !securityModuleBackingBean.viewSublist}"
			var="user">
			<a4j:support event="onRowClick" 
				onsubmit="selectRow('stsSecurityModuleData:usersTable', this);"
				actionListener="#{securityModuleBackingBean.selectedUserRowChanged}"
				reRender="updateBtn,deleteBtn,mapBtn,viewBtn"/>																
																
			<rich:column id="userCode" sortBy="#{user.userCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="User Code"  />
				</f:facet>
				<h:outputText  value="#{user.userCode}" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
			<rich:column id="userName" sortBy="#{user.userName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="User Name"/>
				</f:facet>
				<h:outputText value="#{user.userName}" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } " />
			</rich:column>
			<rich:column id="lastUsedEntityNameCode" sortBy="#{user.lastUsedEntityString}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Last Used Entity"/>
				</f:facet>
				<h:outputText value="#{user.lastUsedEntityString}" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>			
		    <rich:column id="globalBuFlag" sortBy="#{user.globalBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Global Matrix?"/>
				</f:facet>
        		<h:selectBooleanCheckbox styleClass="check" value="#{user.globalBooleanFlag}" disabled="true"  style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
		    <rich:column id="defaultMatrixLineFlag" sortBy="#{user.defaultMatrixLineBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Default Matrix?"/>
				</f:facet>
        		<h:selectBooleanCheckbox styleClass="check" value="#{user.defaultMatrixLineBooleanFlag}" disabled="true" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
			
			<rich:column id="adminFlag" sortBy="#{user.adminFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Administrative?"/>
				</f:facet>
				<h:outputText value="#{securityModuleBackingBean.administrativeMap[user.adminFlag]}" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>	
			</rich:column>	
			
			<rich:column id="addNexusFlag" sortBy="#{user.addNexusBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Add Nexus?"/>
				</f:facet>
        		<h:selectBooleanCheckbox styleClass="check" value="#{user.addNexusBooleanFlag}" disabled="true" />
			</rich:column>
			
			<rich:column id="createViewsFilterFlag" sortBy="#{user.globalViewBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Create *GLOBAL Views/Filters?"/>
				</f:facet>
        		<h:selectBooleanCheckbox styleClass="check" value="#{user.globalViewBooleanFlag}" disabled="true" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
			
			<rich:column id="mappedFlag" sortBy="#{user.userMappedBooleanFag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Mapped?" />
				</f:facet>
        			<h:selectBooleanCheckbox styleClass="check" value="#{user.userMappedBooleanFag}" disabled="true" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
			
			<rich:column id="viewOnlyFlag" sortBy="#{user.viewOnlyBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="View Only?"/>
				</f:facet>
				<h:selectBooleanCheckbox styleClass="check" value="#{user.viewOnlyBooleanFlag}" disabled="true" />
			</rich:column>
				
			<rich:column id="activeFlag" sortBy="#{user.activeBooleanFlag}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Active?" />
				</f:facet>
        			<h:outputText value="#{user.userActiveDescription}"  style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } "/>
			</rich:column>
			<rich:column id="updateUserId" sortBy="#{user.updateUserId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update User Id"/>
				</f:facet>
				<h:outputText value="#{user.updateUserId}" style = "#{user.userActiveDescription  eq 'New User' ?  'color : #C0C0C0' : 'color : #000000' } " />
			</rich:column>			
		    <rich:column id="updateTimestamp" sortBy="#{user.updateTimestamp}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update Timestamp" />
				</f:facet>
				<h:outputText value="#{user.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>
		</rich:dataTable>

		<rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			width="915px"
			frozenColCount="1"
			id="menusTable"
			value="#{securityModuleBackingBean.menusList}"
			rendered="#{securityModuleBackingBean.viewMenus}"
			var="menu" sortMode="single">
			<a4j:support event="onRowClick" 
				onsubmit="selectRow('stsSecurityModuleData:menusTable', this);"
				actionListener="#{securityModuleBackingBean.selectedMenuRowChanged}"
				reRender="updateBtn,deleteBtn,viewBtn"/>																
																
			<rich:column id="menuCode" sortBy="#{menu.menuCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Menu Code" />
				</f:facet>
				<h:outputText value="#{menu.menuCode}"/>
			</rich:column>
			<rich:column id="mainMenuCode" sortBy="#{menu.mainMenuCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Main Menu Code"/>
				</f:facet>
				<h:outputText value="#{menu.mainMenuCode}" />
			</rich:column>
			<rich:column id="menuSequence" sortBy="#{menu.menuSequence}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Menu Sequence"/>
				</f:facet>
				<h:outputText value="#{menu.menuSequence}" />
			</rich:column>
			<rich:column id="optionName" sortBy="#{menu.optionName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Option Name"/>
				</f:facet>
				<h:outputText value="#{menu.optionName}" />
			</rich:column>			
		    <rich:column id="commandType" sortBy="#{menu.commandType}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Command Type"/>
				</f:facet>
				<h:outputText value="#{menu.commandType}" />
			</rich:column>		
			<rich:column id="commandLine" sortBy="#{menu.commandLine}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Command Line"/>
				</f:facet>
				<h:outputText value="#{menu.commandLine}" />
			</rich:column>
			<rich:column id="keyValues" sortBy="#{menu.keyValues}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Key Values"/>
				</f:facet>
				<h:outputText value="#{menu.keyValues}" />
			</rich:column>		
			<rich:column id="panelClassName" sortBy="#{menu.panelClassName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Panel Class Name"/>
				</f:facet>
				<h:outputText value="#{menu.panelClassName}" />
			</rich:column>	
			<rich:column id="updateUserId" sortBy="#{menu.updateUserId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update User Id"/>
				</f:facet>
				<h:outputText value="#{menu.updateUserId}" />
			</rich:column>			
		    <rich:column id="updateTimestamp" sortBy="#{menu.updateTimestamp}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update Timestamp"/>
				</f:facet>
				<h:outputText value="#{menu.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>																																															
		</rich:dataTable>	
		
        <rich:dataTable rowClasses="odd-row,even-row" 
			styleClass="GridContent"
			width="915px"
			frozenColCount="1"
			id="entityItemsTable"
			value="#{securityModuleBackingBean.entityItemsList}"
			rendered="#{securityModuleBackingBean.viewEntityItems}"
			var="ei" sortMode="single">
			<a4j:support event="onRowClick" 
				onsubmit="selectRow('stsSecurityModuleData:entityItemsTable', this);"
				actionListener="#{securityModuleBackingBean.selectedEntityRowChanged}"
				reRender="updateBtn,deleteBtn"/>

			<rich:column id="entityId" sortBy="#{ei.entityId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Id" />
				</f:facet>
				<h:outputText value="#{ei.entityId}"/>
			</rich:column>
			<rich:column id="entityCode" sortBy="#{ei.entityCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Code"/>
				</f:facet>
				<h:outputText value="#{ei.entityCode}" />
			</rich:column>
			<rich:column id="entityName" sortBy="#{ei.entityName}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Name"/>
				</f:facet>
				<h:outputText value="#{ei.entityName}" />
			</rich:column>
			<rich:column id="updateUserId" sortBy="#{ei.updateUserId}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update User Id"/>
				</f:facet>
				<h:outputText value="#{ei.updateUserId}" />
			</rich:column>			
		    <rich:column id="updateTimestamp" sortBy="#{ei.updateTimestamp}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Update Timestamp"/>
				</f:facet>
				<h:outputText value="#{ei.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>
		</rich:dataTable>	
		
		</div>
		</div>
	
	</div>
		

	<div id="table-four-bottom">
	<ul class="right">
		<li class="add">
			<h:commandLink id="addBtn" disabled="#{securityModuleBackingBean.disableAdd or securityModuleBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{securityModuleBackingBean.displayAddAction}" 
										style="#{(securityModuleBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/>  
        </li>
		<li class="update">
			<h:commandLink id="updateBtn" disabled="#{!securityModuleBackingBean.validSelection or securityModuleBackingBean.viewSublist or securityModuleBackingBean.currentUser.viewOnlyBooleanFlag
			                                            or securityModuleBackingBean.disableUpdateUserSecurityBtn}" 
						   style="#{(securityModuleBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{securityModuleBackingBean.displayUpdateAction}" />		
		</li>
		<li class="delete">
			<h:commandLink id="deleteBtn" disabled="#{!securityModuleBackingBean.validSelection or securityModuleBackingBean.viewSublist or securityModuleBackingBean.currentUser.viewOnlyBooleanFlag
			                                            or securityModuleBackingBean.disableDeleteUserSecurityBtn}" 
						   style="#{(securityModuleBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'} " action="#{securityModuleBackingBean.displayDeleteAction}" />
        </li>
        
        <li class="view">
			<h:commandLink id="viewBtn" disabled="#{!securityModuleBackingBean.validSelection or securityModuleBackingBean.viewSublist }" 
						    action="#{securityModuleBackingBean.displayViewAction}" />		
		</li>
		<li class="map">
			<h:commandLink id="mapBtn" disabled="#{!securityModuleBackingBean.validSelection or securityModuleBackingBean.viewSublist or securityModuleBackingBean.currentUser.viewOnlyBooleanFlag
			                                        or securityModuleBackingBean.disableUpdateUserSecurityBtn}"
			 style="#{(securityModuleBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
			 rendered="#{!(securityModuleBackingBean.viewMenus or securityModuleBackingBean.viewEntityItems)}" 
			 action="#{securityModuleBackingBean.displayMapAction}" />
	    </li>
		<li class="refresh">
			<h:commandLink id="refreshBtn" action="#{securityModuleBackingBean.refreshAction}" />		
		</li>
	</ul>
	</div>
	
	
	
	</div>
</div>

</h:form>
</ui:define>

</ui:composition>
</html>