<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleUserAdd">
<h1><h:graphicImage id="imgUserAdd" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{securityModuleBackingBean.actionText}"/> a User</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Code:</td>
				<td style="width:700px;">
				<h:inputText id="userCodeTxt" value="#{securityModuleBackingBean.selectedUser.userCode}" 
                         required="true" label="User Code"
						 validator="#{securityModuleBackingBean.validateUserCode}"
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction or securityModuleBackingBean.updateAction}"
                         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" >
				</h:inputText>
				</td>				
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Name:</td>
				<td>
				<h:inputText id="userNameTxt" value="#{securityModuleBackingBean.selectedUser.userName}"
                         required="true" label="User Name"
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
                         style="width:650px;"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Global Matrix Lines?:</td>
				<td>
				    <h:selectBooleanCheckbox id="globalBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.globalBooleanFlag}"	
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Matrix Lines?:</td>
				<td>
				    <h:selectBooleanCheckbox id="defaultMatrixLineBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.defaultMatrixLineBooleanFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Administrative?:</td>
				<td>
				    <h:selectOneMenu  id="adminBooleanFlagChk" styleClass="NCSTSSelector" value="#{securityModuleBackingBean.selectedUser.adminFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}">
						<f:selectItems id="selViewItems" value="#{securityModuleBackingBean.administrativeItems}" />
					</h:selectOneMenu>
					
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Add Nexus?:</td>
				<td>
				    <h:selectBooleanCheckbox id="addNexusBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.addNexusBooleanFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Create *GLOBAL Views/Filters?:</td>
				<td>
				    <h:selectBooleanCheckbox id="defaultGlobalViewFilterBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.globalViewBooleanFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>

			<tr>
				<td style="width:50px;">&#160;</td>
				<td>View Only?:</td>
				<td>
					<h:selectBooleanCheckbox id="defaultViewOnlyBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.viewOnlyBooleanFlag}"
											 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>


			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Active?:</td>
				<td>
				    <h:selectBooleanCheckbox id="activeBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedUser.activeBooleanFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText id="updateUserIdTxt" value="#{securityModuleBackingBean.selectedUser.updateUserId}" disabled="true" style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText id="updateTimestampTxt" value="#{securityModuleBackingBean.selectedUser.updateTimestamp}" disabled="true" style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" disabled = "#{securityModuleBackingBean.showUserSecurityOkMapButton and !securityModuleBackingBean.deleteAction and !securityModuleBackingBean.viewAction}" action="#{securityModuleBackingBean.okAction}" /></li>
		<li class="okandmap"><h:commandLink id="btnOkandmap"  disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}" rendered = "#{securityModuleBackingBean.showUserSecurityOkMapButton}" value="" action="#{securityModuleBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{securityModuleBackingBean.viewAction}" action="#{securityModuleBackingBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>