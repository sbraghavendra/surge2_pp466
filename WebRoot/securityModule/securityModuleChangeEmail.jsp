<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form>
<h1><h:graphicImage id="imgChangeEmail" value="/images/headers/hdr-email.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Submit/Change Email Address</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Current Password</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Current Password:</td>
				<td style="width:700px;">
				<h:inputSecret 
				         id="pwdEmailControl"  
				         redisplay="true"
                         size="4"
                         binding="#{securityModuleBackingBean.pwdEmailControl}"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr><th colspan="3">New Email Address</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>New Email Address:</td>
				<td>
				<h:inputText
				         id="newEmailControl"
				         redisplay="true"
                         size="4"
                         binding="#{securityModuleBackingBean.newEmailControl}"				
				         style="width:650px;" />
                </td>
			</tr>
		</tbody>
	</table>
    <h:outputLabel id="message" style="color:red" value="#{securityModuleBackingBean.emailChangeMessage}"/>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink action="#{securityModuleBackingBean.changeEmailAction}" /></li>
		<li class="cancel"><h:commandLink action="#{securityModuleBackingBean.cancelChangeEmailAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>