<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleUserEntityMap">
<h1><h:graphicImage id="imgUserEntityMap" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="9">User</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Code:</td>
				<td style="width:150px;">
				<h:inputText id="txtUserCode" value="#{securityModuleBackingBean.selectedMapping.user.userCode}" label="Role Code"
				             style="width:150px;" disabled="true" />
				</td>
				<td>Role Code:</td>
				<td style="width:150px;">
				<h:inputText id="txtRoleCode" value="#{securityModuleBackingBean.selectedMapping.role.roleCode}" label="Role Code"
				             style="width:150px;" disabled="true" />
				</td>
				<td>Entity Code:</td>
				<td style="width:150px;">
				<h:inputText id="txtEntityCode" value="#{securityModuleBackingBean.selectedMapping.entityItem.entityCode}" label="Role Code"
				             style="width:150px;" disabled="true" />
				</td>
			</tr>	
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td style="width:150px;">
				<h:inputText id="txtUserName" value="#{securityModuleBackingBean.selectedMapping.user.userName}"	label="Name"
				             style="width:150px;" disabled="true" />
				</td>
				<td>Name:</td>
				<td style="width:150px;">
				<h:inputText id="txtRoleName" value="#{securityModuleBackingBean.selectedMapping.role.roleName}"	label="Name"
				             style="width:150px;" disabled="true" />
				</td>
				<td>Name:</td>
				<td style="width:150px;">
				<h:inputText id="txtEntityName" value="#{securityModuleBackingBean.selectedMapping.entityItem.entityName}"	label="Name"
				             style="width:150px;" disabled="true" />
				</td>
			</tr>	
		</tbody>
			<thead>
			<tr><td colspan="9">Map to Menu Option</td></tr>
		</thead>
		<tbody>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="8">
				<rich:listShuttle id="menuList" sourceValue="#{securityModuleBackingBean.defaultMenu}" targetValue="#{securityModuleBackingBean.deniedMenu}" var="menu" listsHeight="300"
				targetListWidth="375" sourceListWidth="375" sourceCaptionLabel="Granted:" targetCaptionLabel="Denied:"
				copyControlLabel="Deny" removeControlLabel="Grant"
                copyAllControlLabel="Deny all" removeAllControlLabel="Grant all"
				fastOrderControlsVisible="false" orderControlsVisible="false" converter="menuDTOConverter" style="height:200px;">
					<rich:column id="menuCode" width="150px">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Menu Code" />
						</f:facet>
						<h:outputText id="rowMenuCode" value="#{menu.menuCode}"/>
					</rich:column>
					<rich:column id="optionName" width="150px" >
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Option Name"/>
						</f:facet>
						<h:outputText id="rowOptionName" value="#{menu.optionName}" />
					</rich:column>	
					<rich:column id="exceptionType" width="20px">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Excp"/>
						</f:facet>
						<h:outputText id="txtExceptionType" value="#{menu.exceptionType}" />
					</rich:column>	
					<a4j:support event="onlistchanged"  reRender="menuList" actionListener="#{securityModuleBackingBean.onlistchangedAction}" />
				</rich:listShuttle>
				</td>								
			</tr>	

		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{securityModuleBackingBean.mapUserEntityAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="security_user_map" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>