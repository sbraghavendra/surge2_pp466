<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleRoleAdd">
<h1><h:graphicImage id="imgRoleAdd" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{securityModuleBackingBean.actionText}"/> a Role</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Role Code:</td>
				<td style="width:700px;">
				<h:inputText id="roleCodeTxt" value="#{securityModuleBackingBean.selectedRole.roleCode}"
                         required="true" label="Role Code"
						 validator="#{securityModuleBackingBean.validateRoleCode}"
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction or securityModuleBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" >
				</h:inputText>
				</td>				
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Role Name:</td>
				<td>
				<h:inputText id="roleNameTxt" value="#{securityModuleBackingBean.selectedRole.roleName}"
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"
				         style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Global Matrix Lines?:</td>
				<td>
				    <h:selectBooleanCheckbox id="globalBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedRole.globalBooleanFlag}"				
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Active?:</td>
				<td>
				    <h:selectBooleanCheckbox id="activeBooleanFlagChk" styleClass="check" value="#{securityModuleBackingBean.selectedRole.activeBooleanFlag}"									
						 disabled="#{securityModuleBackingBean.viewAction or securityModuleBackingBean.deleteAction}"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText id="updateUserIdTxt" value="#{securityModuleBackingBean.selectedRole.updateUserId}" disabled="true" style="width:650px;" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText id="updateTimestampTxt" value="#{securityModuleBackingBean.selectedRole.updateTimestamp}" disabled="true" style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" value="" action="#{securityModuleBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="cancelBtn" disabled="#{securityModuleBackingBean.viewAction}"  immediate="true" action="#{securityModuleBackingBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>