<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
function resetInputs()
{
document.getElementById("entityLevelAdd:columnSearch").value="";
document.getElementById("entityLevelAdd:descSearch").value="";
document.getElementById("entityLevelAdd:dataTypeSearch").value="";
}

//]]>
</script>
</ui:define>
    <ui:define name="body" >
	<f:view>
	<h:form id="entityLevelAdd">
	<h1><img src="../images/headers/hdr-entity-structure.gif" width="250" height="19" /></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	
	<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div><br/>
<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
<tbody>
<tr>
<td style="width:400px;">&#160;</td>


<td style="width:120px;">Trans. Dtl Column Name:</td><td>
<h:inputText id="columnSearch" binding="#{securityModuleBackingBean.columnSearch}" value ="" style="width:300px;"></h:inputText></td>
</tr>
<tr>
<td style="width:400px;">&#160;</td>


<td style="width:120px;">Description:</td><td>
<h:inputText id="descSearch" binding="#{securityModuleBackingBean.descriptionSearch}" value ="" style="width:300px;"></h:inputText></td>
</tr>
<tr>
<td style="width:400px;">&#160;</td>


<td style="width:120px;">Data Type:</td><td>
<h:inputText id="dataTypeSearch" binding="#{securityModuleBackingBean.dataTypeSearch}" value ="" style="width:300px;"></h:inputText></td>

</tr>
</tbody>
</table>
</div>

	<div id="table-one-bottom">
	<ul class="right">

		<li class="search"><h:commandLink action="#{securityModuleBackingBean.getByColumnName}"/></li>
						<li class="clear"><h:commandLink onclick="return resetInputs();"/></li>
	</ul>
	</div>	
	<div id="scrollContainer">

<rich:scrollableDataTable rowClasses="odd-row,even-row" styleClass="GridContent" height="240px"
							width="915px" id="ColNameTable" rows="40"
							value="#{securityModuleBackingBean.dataDefinitionColList}"
							var="security">
							<a4j:support event="onselectionchange"  
                actionListener="#{securityModuleBackingBean.selectedEntityLevelRowChangedToAdd}"/>
				<rich:column id="columnNames" style="width:180px;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Trans. Dtl Column Name:" style="width:180px;"/>
				</f:facet>
				<h:outputText value="#{security.dataDefinitionColumnPK.columnName}" />
			</rich:column>.
							<rich:column id="dataType">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Data Type"/>
				</f:facet>
				<h:outputText value="#{security.dataType}" />
			</rich:column>
										<rich:column id="dataLength">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Data Length"/>
				</f:facet>
				<h:outputText value="#{security.datalength}" />
			</rich:column>
													<rich:column id="description" style="width:180px;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Description" style="width:180px;"/>
				</f:facet>
				<h:outputText value="#{security.description}" />
			</rich:column>
							<rich:column id="abbrDesc" style="width:150px;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Abbr Desc" style="width:150px;"/>
				</f:facet>
				<h:outputText value="#{security.abbrDesc}" />
			</rich:column>
								<rich:column id="descColumnName" style="width:200px;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Desc Column Name" style="width:200px;"/>
				</f:facet>
				<h:outputText value="#{security.descColumnName}" />
			</rich:column>
								</rich:scrollableDataTable>
						


	</div>
			<div id="table-one-bottom">
			<div id="table-one-top" align="center">
	<a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<ul class="right">
	<li class="ok"><h:commandLink action="#{securityModuleBackingBean.setUpdateAction}" rendered="#{securityModuleBackingBean.displayUpdateButton}"/></li>
<li class="ok"><h:commandLink action="#{securityModuleBackingBean.setAddAction}" rendered="#{securityModuleBackingBean.displayAddButton}"/></li>
<li class="cancel"><h:commandLink immediate="true" action="#{securityModuleBackingBean.cancelEntityLevelAction}" /></li>
	</ul>
	</div>
	</div>
	
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>