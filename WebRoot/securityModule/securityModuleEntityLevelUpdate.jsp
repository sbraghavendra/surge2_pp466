<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
function validateForm()
{
var description = document.getElementById("entityLevelUpdate:description").value; 
if(description==null || description=="")
{
alert("Description field may not be blank");
return false;
}
else {
return true;
}
}
//]]>
</script>
</ui:define>
    <ui:define name="body" >
	<f:view>
	<h:form id="entityLevelUpdate">
<h1><img id="imgEntityStructure" alt="Entity Structure" src="../images/headers/hdr-entity-structure.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:230px;" >
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Update an Entity Structure Level</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Structure Level:</td>
				<td style="width:700px;">
				<h:inputText id="txtStructureLevel" value="#{securityModuleBackingBean.selectedEntityLevel.entityLevelId}" 
				         disabled="true" size="4" immediate="true"  style="width:300px;" />
				</td>
			</tr>
		<tr>
				<td style="width:50px;">&#160;</td>
				<td>Trans. Dtl. Column Name:</td>
				<td>
				<h:inputText id="txtTransDtlColumnName" disabled="#{(securityModuleBackingBean.selectedEntityLevel.entityLevelId>0) ? 'false':'true'}" binding="#{securityModuleBackingBean.transDetailColumnName}"
						 style="width:300px;"/>&#160;&#160;&#160;
						  <a4j:commandButton rendered="#{(securityModuleBackingBean.selectedEntityLevel.entityLevelId>0) ? 'true':'false'}" value ="select" id="select" 
						  styleClass="image" image="/images/search_small.png" immediate="true"
						  actionListener="#{securityModuleBackingBean.getAllByTableName}"
						  oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText binding="#{securityModuleBackingBean.description}" id ="description" size="4" style="width:300px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
                         size="4"	
                         immediate="true"			
				         style="width:300px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{securityModuleBackingBean.selectedEntityLevel.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         immediate="true"
				         style="width:300px;" >
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>

	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{securityModuleBackingBean.updateEntityStructureAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{securityModuleBackingBean.cancelAddEntityLevelAction}" /></li>
	</ul>
	</div>
	
	</div>
</div>
</h:form>
					<ui:include src="/WEB-INF/view/components/transactionDetailCol_search.xhtml">
					<ui:param name="handler" value="#{securityModuleBackingBean}"/>
					</ui:include>
</f:view>
</ui:define>
</ui:composition>
</html>