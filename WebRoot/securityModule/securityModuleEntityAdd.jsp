<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('stsEntityAdd:driverReferences', 'driverIndex'); } );
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverIndex" value="#{securityModuleBackingBean.selectedFilterIndex}"/>

<h1><h:graphicImage id="imgEntityAdd" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>

<h:form id="stsEntityAdd">
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{securityModuleBackingBean.actionText}"/> an Entity</td></tr>
		</thead>
		<tbody>			
			<tr><th colspan="3">Parent Entity</th></tr>
			<tr>				
				<td style="width:50px;">&#160;</td>
				<td>Level:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.parentLevelDescription}" 							 
						 	 disabled="true" id="txtParentLevelDesc"
				             style="width:650px;" /></td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Code:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.parentCode}" 							 
						 	 disabled="true" id="txtParentCode"
				             style="width:650px;" /></td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.parentName}" 							 
						 	 disabled="true" id="txtParentName"
				             style="width:650px;" /></td>
			</tr>
			<tr><th colspan="3">This Entity</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Level:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.entityLevel}" 							 
						 	 disabled="true" id="txtEntityLevel"
				             style="width:650px;" /></td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Column:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.entityColumn}" 							 
						 	 disabled="true" id="txtEntityColumn"
				             style="width:650px;" />				
							 </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Code:</td>
				<td style="width:700px;">
				<h:inputText id="entCode" value="#{securityModuleBackingBean.entityItemDTO.entityCode}" 
							 disabled="#{!securityModuleBackingBean.addAction}"	
				             style="width:600px;" onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />&#160;
				<h:commandButton id="search" value="Search" type="button" action="#{securityModuleBackingBean.searchDriverReference}"
							rendered="#{securityModuleBackingBean.addAction}"/>	</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td style="width:700px;">
				<h:inputText id="entName" value="#{securityModuleBackingBean.entityItemDTO.entityName}" 
							 disabled="#{securityModuleBackingBean.deleteAction}"						 
				             style="width:650px;" /></td>
			</tr>
			<tr><th colspan="3">&#160;</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.updateUserId}" 							 
						 	 disabled="true" id="txtUpdateUserId"
				             style="width:650px;" /></td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.entityItemDTO.updateTimestamp}" 							 
						 	 disabled="true" id="txtUpdateTimestamp"
				             style="width:650px;">
				<f:converter converterId="dateTime"/>
				</h:inputText></td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<h:graphicImage id="imgEntityAddModule" value="/images/containers/STSView-transaction-detail-column-open.GIF" rendered="#{securityModuleBackingBean.addAction}"/>
	</span>
</div>
	<div id="bottom">	
	
	</div>
	<div id="table-four-content">
		<rich:dataTable rowClasses="odd-row,even-row" 
			id="driverReferences"
			width="100%"
			rows="10"
			value="#{securityModuleBackingBean.transctionDetailColumn}" var="driverRef" rendered="#{securityModuleBackingBean.addAction}">
			
			<a4j:support event="onRowClick" onsubmit="selectRow('stsEntityAdd:driverReferences', this);"
					actionListener="#{securityModuleBackingBean.selectionFilterChanged}"
					reRender="entCode,entName" />
					
			<rich:column id="driverValue" style="text-align: center;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Driver Value" />
				</f:facet>
				<h:outputText id="rowDriverValue" value="#{driverRef.driverValue}" />
			</rich:column>

			<rich:column id="driverDesc" style="text-align: center;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Driver Description" />
				</f:facet>
				<h:outputText id="rowDriverDesc" value="#{driverRef.driverDesc}" />
			</rich:column>
			
			<rich:column id="userValue" style="text-align: center;">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="User Value" />
				</f:facet>
				<h:outputText id="rowUserValue" value="#{driverRef.userValue}" />
			</rich:column>		
		</rich:dataTable>
	</div>
	<rich:datascroller id="drScroll" for="driverReferences" maxPages="10" oncomplete="initScrollingTables();"
	style="clear:both;" align="center" stepControls="auto" ajaxSingle="false" />
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{securityModuleBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{securityModuleBackingBean.cancelAction}" /></li>
	</ul>
	</div>	
	
	
</div>	


</h:form>
</ui:define>
</ui:composition>
</html>