<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
 <script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('openEntity:userEntityTable', 'entityselectedRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">

<h:inputHidden id="entityselectedRowIndex" value="#{openEntityBean.selectedRowIndex}"/>


<h:form id="openEntity">
<h1><h:graphicImage id="imgOpenEntity" alt="Open Entity" value="../images/headers/hdr-open-entity.gif" width="250" height="19" /></h1>

<c:if test ="#{openEntityBean.userEntityMapListSize eq '0'}">
<a4j:outputPanel id="mypanelempty" rendered = "#{openEntityBean.openEntityAction or openEntityBean.userEntityMapListSize eq '0'}">
  
   <div id="bottom">
	<div id="table-four">
	<div id="table-four-bottom">
		<table cellpadding="0" cellspacing="0" width="898" id="rollover" class="ruler">
			<tr><th style="width:100%;align :left"><h:outputText style = "color:red" value="You are not mapped to any Entities. Contact your System Administrator." /></th></tr>
		</table>
	</div>
	</div>
	</div>
  </a4j:outputPanel> 
  </c:if>

<c:if test ="#{openEntityBean.userEntityMapListSize ne '0'}">
 <a4j:outputPanel id="mypanelnotempty" rendered = "#{openEntityBean.openEntityAction or openEntityBean.userEntityMapListSize ge '1'}">
   <div id="bottom">
	<div id="table-four">
	<div id="table-four-bottom">
		<table cellpadding="0" cellspacing="0" width="898" id="rollover" class="ruler">
			<tr><th  height="16"><h:outputText style = "font-size: 11px" value="&#160;&#160;&#160;#{openEntityBean.tableHeader}"/></th></tr>
			<tr><th  height="16"><h:outputText style = "font-size: 11px;color:red" value="&#160;&#160;&#160;The selected Entity will be opened automatically on your next login. To change Entities in the future, select File > Open Entity." rendered ="#{openEntityBean.userEntityMapListSize gt '1' or openEntityBean.openEntityAction}"/></th></tr>
		</table>
	</div>
	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner" id="resize" >
    <rich:dataTable rowClasses="odd-row,even-row" 
			id="userEntityTable"
			value="#{openEntityBean.userEntityMapList}"
			var="ue">		

			<a4j:support id="clk" event="onRowClick" onsubmit="selectRow('openEntity:userEntityTable', this);"
								 actionListener="#{openEntityBean.selectedUserEntityRowChanged}" reRender="okBtn" />

			<rich:column id="entityCode"  width="100px" sortBy="#{ue.entity.entityCode}" >
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Code"/>
				</f:facet>
				<h:outputText style="width:87px;"
					value="#{ue.entity.entityCode}" />
			</rich:column>	
			<rich:column id="entityName"  width="150px" sortBy="#{ue.entity.entityName}" >
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Entity Name"/>
				</f:facet>
				<h:outputText style="width:87px;"
					value="#{ue.entity.entityName}" />
			</rich:column>
			<rich:column id="roleName"  width="300px" sortBy="#{ue.roleName}" >
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Role Name" />
				</f:facet>
				<h:outputText style="width:20px;"
					value="#{ue.roleName}"></h:outputText>
			</rich:column>
<!--			<rich:column id="description">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="BU Driver Name"/>
				</f:facet>
				<h:outputText style="width:87px;"
					value="#{ue.entityLevel.description}" />
			</rich:column>  -->
<!--			<rich:column id="roleCode">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="RoleCode" />
				</f:facet>
				<h:outputText style="width:20px;"
					value="#{ue.roleCode}"></h:outputText>
			</rich:column>  -->
<!--			<rich:column id="userCode">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="UserCode"/>
				</f:facet>
				<h:outputText style="width:87px;"
					value="#{ue.userCode}" />
			</rich:column>  -->
	
<!--			<rich:column id="entityId">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="EntityId"/>
				</f:facet>
				<h:outputText style="width:87px;"
					value="#{ue.entity.entityId}" />
			</rich:column>  -->	
		</rich:dataTable>	  	
	</div>
	</div>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok2"><h:commandLink id="okBtn" disabled="#{!openEntityBean.showButtons}" action="#{openEntityBean.setDynamicMenus}" /></li>
	</ul>
	</div>
	</div>
  </div>
 </a4j:outputPanel>
</c:if>


</h:form>
</ui:define>

</ui:composition>
</html>