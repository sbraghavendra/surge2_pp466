<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityModuleRoleMap">
<h1><h:graphicImage id="imgRoleMap" alt="Security Modules" value="/images/headers/hdr-security-modules.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Role</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Role Code:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.selectedRole.roleCode}" label="Role Code"
				             style="width:200px;" disabled="true" id="txtRoleCode" />
				</td>
			</tr>	
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td style="width:700px;">
				<h:inputText value="#{securityModuleBackingBean.selectedRole.roleName}"	label="Name"
				             style="width:200px;" disabled="true" id="txtRoleName" />
				</td>
			</tr>	
			

		</tbody>
			<thead>
			<tr><td colspan="3">Map to Menu Option</td></tr>
		</thead>
		<tbody>		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="2" style="width:700px;">
				<rich:listShuttle sourceValue="#{securityModuleBackingBean.grantedList}" targetValue="#{securityModuleBackingBean.deniedList}" var="menu" listsHeight="300"
				targetListWidth="300" sourceListWidth="300" sourceCaptionLabel="Granted:" targetCaptionLabel="Denied:"
				copyControlLabel="Deny" removeControlLabel="Grant"
                copyAllControlLabel="Deny all" removeAllControlLabel="Grant all"
				fastOrderControlsVisible="false" orderControlsVisible="false" converter="listShuttleConverter">
					<rich:column id="menuCode">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Menu Code" />
						</f:facet>
						<h:outputText id="rowMenuCode" value="#{menu.menuCode}"/>
					</rich:column>
					<rich:column id="optionName">
						<f:facet name="header">
							<h:outputText styleClass="headerText" value="Option Name"/>
						</f:facet>
						<h:outputText id="txtOptionName" value="#{menu.optionName}" />
					</rich:column>	
				</rich:listShuttle>
				</td>								
			</tr>	

		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{securityModuleBackingBean.mapAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{securityModuleBackingBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>