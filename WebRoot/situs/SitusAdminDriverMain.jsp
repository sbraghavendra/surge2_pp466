<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('situsDriverMaintenanceForm:driverTable', 'selectedRowIndex'); } );
               

//]]>
</script>
</ui:define>
<ui:define name="body">
   <f:view>
    <h:inputHidden id="selectedRowIndex" value="#{situsAdminDriverBackingBean.selectedRowIndex}" />
    <h:form id="situsDriverMaintenanceForm">
     <h1><h:graphicImage id="imgTaxholidayMaintenance" alt="Admin Situs Drivers" url="/images/headers/hdr-admin-situs-maintenance.gif" width="250" height="19" /></h1>
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" /></div>
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'situsDriverMaintenanceForm:searchBtn')">
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
               
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="transactionTypeItem" value="#{situsAdminDriverBackingBean.filterSitusDriver.transactionTypeCode}">
							<f:selectItems value="#{situsAdminDriverBackingBean.filterTransactionTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width: 50px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="rateTypeItem" value="#{situsAdminDriverBackingBean.filterSitusDriver.ratetypeCode}">
							<f:selectItems value="#{situsAdminDriverBackingBean.filterRateTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width: 50px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="methodOfDeliveryItem" value="#{situsAdminDriverBackingBean.filterSitusDriver.methodDeliveryCode}">
							<f:selectItems value="#{situsAdminDriverBackingBean.filterMethodOfDeliveryItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width: 50px;">&#160;</td>
         </tr>
         
        </table>
       </div>
       <!-- table-one-content -->
       <div id="table-one-bottom">
        <ul class="right">
         <li class="clear">
          <h:commandLink id="btnClear"
                         action="#{situsAdminDriverBackingBean.resetFilterSearchAction}" />
         </li>
         <li class="search">
          <h:commandLink id="searchBtn"
                         action="#{situsAdminDriverBackingBean.retrieveFilterSitusDriver}" />
         </li>
        </ul>
       </div>
       <!-- table-one-bottom -->
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
     <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-situsdriver.gif" width="192"  height="17" />
      </span>
     </div>
     <div id="bottom">
      <div id="table-four">
       <div id="table-four-top">
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{situsAdminDriverBackingBean.situsDriverDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
       </div>
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner">
          <rich:dataTable rowClasses="odd-row,even-row" id="driverTable"
                          rows="#{situsAdminDriverBackingBean.situsDriverDataModel.pageSize }"
                          value="#{situsAdminDriverBackingBean.situsDriverDataModel}"
                          var="situsDriverMaintenance">
           	<a4j:support id="ref2tbl"
                        event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsDriverMaintenanceForm:driverTable', this);"
                        actionListener="#{situsAdminDriverBackingBean.selectedRowChanged}"
                        reRender="situsDriverMaintenanceForm:addSitusDriver,situsDriverMaintenanceForm:updateSitusDriver,situsDriverMaintenanceForm:deleteSitusDriver,situsDriverMaintenanceForm:detailTable" />
                        
           	<rich:column id="transactionTypeCode" styleClass="column-left" >
            	<f:facet name="header">
             	<a4j:commandLink id="transactionType-a4j"
                              styleClass="sort-#{situsAdminDriverBackingBean.situsDriverDataModel.sortOrder['transactionTypeCode']}"
                              value="Transaction Type"
                              actionListener="#{situsAdminDriverBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="driverTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminDriverBackingBean.transactionTypeMap[situsDriverMaintenance.transactionTypeCode]}"  />
           	</rich:column>
           	
           <rich:column id="ratetypeCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="ratetypeCode-a4j"
                              styleClass="sort-#{situsAdminDriverBackingBean.situsDriverDataModel.sortOrder['ratetypeCode']}"
                              value="Rate Type"
                              actionListener="#{situsAdminDriverBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="driverTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminDriverBackingBean.ratetypeMap[situsDriverMaintenance.ratetypeCode]}"  />
           	</rich:column>

           	<rich:column id="methodDeliveryCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="methodDeliveryCode-a4j"
                              styleClass="sort-#{situsAdminDriverBackingBean.situsDriverDataModel.sortOrder['methodDeliveryCode']}"
                              value="Method of Delivery"
                              actionListener="#{situsAdminDriverBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="driverTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminDriverBackingBean.methodDeliveryMap[situsDriverMaintenance.methodDeliveryCode]}"  />
           	</rich:column>
           	
           	<rich:column id="customFlag" style="text-align:center;" >
            	<f:facet name="header">
             	<a4j:commandLink id="customFlag-a4j" 
                              styleClass="sort-#{situsAdminDriverBackingBean.situsDriverDataModel.sortOrder['customFlag']}"
                              value="Custom"
                              actionListener="#{situsAdminDriverBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="driverTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox id="chkCustom" value="#{situsDriverMaintenance.customBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>
           	
           	<rich:column id="blank" styleClass="column-left" style="width:100%" >
            	<f:facet name="header" >
             	<a4j:commandLink id="blank-a4j" value="" reRender="driverTable" />
            	</f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="driverTable"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{situsAdminDriverBackingBean.situsDriverDataModel.curPage}" />
                          
       
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable"
                          value="#{situsAdminDriverBackingBean.situsDriverDetailList}"
                          var="situsDriverDetail">
           
           	<rich:column id="driverId" styleClass="column-left" sortBy="#{situsDriverDetail.driverId}" >
            	<f:facet name="header"><h:outputText id="driverId-a4j" styleClass="headerText" value="Driver ID"/></f:facet>
            	<h:outputText value="#{situsDriverDetail.driverId}" />
           	</rich:column>
           	
           	<rich:column id="driverName" styleClass="column-left" sortBy="#{situsDriverDetail.locationTypeCode}" width="300px" >
            	<f:facet name="header"><h:outputText id="driverName-a4j" styleClass="headerText" value="Driver Name"/></f:facet>
            	<h:outputText value="#{situsAdminDriverBackingBean.driverNameMap[situsDriverDetail.locationTypeCode]}"  />
           	</rich:column>
           	
           	<rich:column id="mandatory"  sortBy="#{situsDriverDetail.mandatoryFlag}" style="text-align:center;" >
            	<f:facet name="header"><h:outputText id="mandatory-a4j" styleClass="headerText" value="Mandatory"/></f:facet>
            	<h:selectBooleanCheckbox id="chkMandatory" value="#{situsDriverDetail.mandatoryBooleanFlag}" disabled="true" styleClass="check" />
           	</rich:column>
           	
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
                          
                          
       <div id="table-four-bottom">
        <ul class="right">
         	<li class="add">
          		<h:commandLink id="addSitusDriver" action="#{situsAdminDriverBackingBean.displayAddDriverAction}" />
         	</li>

         	<li class="update">
          		<h:commandLink id="updateSitusDriver" disabled="#{!situsAdminDriverBackingBean.displayButtons}" immediate="true" action="#{situsAdminDriverBackingBean.displayUpdateDriverAction}" />
         	</li>
         	<li class="delete115">
          		<h:commandLink id="deleteSitusDriver" disabled="#{!situsAdminDriverBackingBean.displayButtons}" immediate="true"  action="#{situsAdminDriverBackingBean.displayDeleteDriverAction}" />
         	</li>
			
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
   </f:view>
   
  </ui:define>
 </ui:composition>
</html>
