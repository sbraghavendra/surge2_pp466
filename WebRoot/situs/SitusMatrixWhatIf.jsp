<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixRulesForm:aMList', 'selectedRulesRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixRulesForm:detailTable', 'selectedRulesDetailRowIndex'); } );
               

//]]>
</script>
</ui:define>
<ui:define name="body">
   <f:view>
    <h:inputHidden id="selectedRulesRowIndex" value="#{situsAdminMatrixBackingBean.selectedRulesRowIndex}" />
    <h:inputHidden id="selectedRulesDetailRowIndex" value="#{situsAdminMatrixBackingBean.selectedRulesDetailRowIndex}" />
    
    <h:form id="situsMatrixWhatIfForm">
     <h1><img id="imgSitusMatrixMaintenance" alt="Situs Matrix" src="#{(situsAdminMatrixBackingBean.isAdminActionRole) ? '../images/headers/hdr-admin-matrix-maintenance.gif' : '../images/headers/hdr-user-matrix-maintenance.gif'}"  width="250" height="19" /></h1>
     
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'situsMatrixRulesForm:searchBtn')">
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
         <thead>
			<tr><td colspan="10"><h:outputText value="Mini What If Analysis" /></td></tr>
		 </thead>
         <tr>
			<th colspan="10">Situs Information</th>
		 </tr>     
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="transactionTypeItem" value="#{situsAdminMatrixBackingBean.filterWhatIfSitusMatrix.transactionTypeCode}" >
						<f:selectItems value="#{situsAdminMatrixBackingBean.headerTransactionTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>  
          
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="rateTypeItem" value="#{situsAdminMatrixBackingBean.filterWhatIfSitusMatrix.ratetypeCode}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.headerRateTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="methodOfDeliveryItem" value="#{situsAdminMatrixBackingBean.filterWhatIfSitusMatrix.methodDeliveryCode}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.headerMethodOfDeliveryItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td> 
         </tr>
         
         <tr>
			<th colspan="10">Transaction Information</th>
		 </tr>
		 
		 
		 
		 <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Transaction Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="transactionDate"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy" 
                          value="#{situsAdminMatrixBackingBean.filterWhatIfSitusMatrix.effectiveDate}" />
          </td>
          <td colspan="7">&#160;</td>
        </tr>
		      
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td style="vertical-align:bottom;" >
				<div id="embedded-table">
				<table cellpadding="0" cellspacing="0" width="100%" id="rollover" >
					<tr><td class="column-label">Ship To/Service</td></tr>
					<tr><td class="column-label">Performance: </td></tr>
				</table>
				</div>
		  </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler1}"/>
					<ui:param name="id" value="jurisInput1"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="true"/>
					<ui:param name="popupName" value="searchJurisdiction1"/>
					<ui:param name="popupForm" value="searchJurisdictionForm1"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td class="column-label">Ship From: </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler2}"/>
					<ui:param name="id" value="jurisInput2"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="false"/>
					<ui:param name="popupName" value="searchJurisdiction2"/>
					<ui:param name="popupForm" value="searchJurisdictionForm2"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td class="column-label">Order Origin: </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler3}"/>
					<ui:param name="id" value="jurisInput3"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="false"/>
					<ui:param name="popupName" value="searchJurisdiction3"/>
					<ui:param name="popupForm" value="searchJurisdictionForm3"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td class="column-label">Order Acceptance: </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler4}"/>
					<ui:param name="id" value="jurisInput4"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="false"/>
					<ui:param name="popupName" value="searchJurisdiction4"/>
					<ui:param name="popupForm" value="searchJurisdictionForm4"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td class="column-label">First Use: </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler5}"/>
					<ui:param name="id" value="jurisInput5"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="false"/>
					<ui:param name="popupName" value="searchJurisdiction5"/>
					<ui:param name="popupForm" value="searchJurisdictionForm5"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>  
          <td class="column-label">Bill To: </td>
          <td colspan="7">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler6}"/>
					<ui:param name="id" value="jurisInput6"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="false"/>
					<ui:param name="popupName" value="searchJurisdiction6"/>
					<ui:param name="popupForm" value="searchJurisdictionForm6"/>
				</ui:include>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
			<th colspan="10">Situs Rules</th>
		 </tr>
         

        </table>
       </div>
       <!-- table-one-content -->
       
       <!--  
       <div id="table-one-bottom">
         <ul class="right"></ul>
       </div>-->
       <!-- table-one-bottom -->
       
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
    
     <div id="bottom">
      <div id="table-four">
       
       <div id="table-four-content" >
        <div class="scrollContainer" >
         <div class="scrollInner" >
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable"
                          value="#{situsAdminMatrixBackingBean.situsMatrixWhatIfList}"
                          var="situsMatrix">
               
            <rich:column id="jurisdictionLevel" styleClass="column-left"  >
            	<f:facet name="header"><h:outputText id="jurisdictionLevel-a4j" styleClass="headerText" value="Jurisdiction Level"/></f:facet>
            	<h:outputText value="" />
           	</rich:column>           
          
           	<rich:column id="matrixId" styleClass="column-left" sortBy="#{situsMatrix.situsMatrixId}" >
            	<f:facet name="header"><h:outputText id="matrixId-a4j" styleClass="headerText" value="Matrix ID"/></f:facet>
            	<h:outputText value="#{situsMatrix.situsMatrixId}" />
           	</rich:column>
           	

           	<rich:column id="effectiveDate" styleClass="column-left" sortBy="#{situsMatrix.effectiveDate}" >
            	<f:facet name="header"><h:outputText id="effectiveDate-a4j" styleClass="headerText" value="Effective Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.effectiveDate}" />
           	</rich:column>
           	
           	<rich:column id="expirationDate" styleClass="column-left" sortBy="#{situsMatrix.expirationDate}" >
            	<f:facet name="header"><h:outputText id="expirationDate-a4j" styleClass="headerText" value="Expiration Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.expirationDate}" />
           	</rich:column>
           	
           	<rich:column id="customFlag" styleClass="column-left" sortBy="#{situsMatrix.customFlag}" >
            	<f:facet name="header"><h:outputText id="customFlag-a4j" styleClass="headerText" value="Custom"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.customBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>
           	
           	<rich:column id="primarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.primarySitusCode}" >
            	<f:facet name="header"><h:outputText id="primarySitusCode-a4j" styleClass="headerText" value="Primary Situs Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.primarySitusCode}" />
           	</rich:column>
           	
           	<rich:column id="primaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.primaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="primaryTaxtypeCode-a4j" styleClass="headerText" value="Primary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.primaryTaxtypeCode}" />
           	</rich:column>
           	
           	<rich:column id="secondarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.secondarySitusCode}" >
            	<f:facet name="header"><h:outputText id="secondarySitusCode-a4j" styleClass="headerText" value="Secondary Situs Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.secondarySitusCode}" />
           	</rich:column>
           	
           	<rich:column id="secondaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.secondaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="secondaryTaxtypeCode-a4j" styleClass="headerText" value="Secondary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.secondaryTaxtypeCode}" />
           	</rich:column>
           	
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
                          
       <div id="table-one-bottom">
        <ul class="right">
			<li class="ok"><h:commandLink id="okBtn" action="#{situsAdminMatrixBackingBean.processWhatIfCommand}" /></li>			
			<li class="cancel2"><h:commandLink id="closeBtn" action="situs_admin_matrix_main" ></h:commandLink></li>
			
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
    
    
<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler1}"/>
	<ui:param name="popupName" value="searchJurisdiction1"/>
	<ui:param name="popupForm" value="searchJurisdictionForm1"/>
	<ui:param name="jurisId" value="jurisId1"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput1"/>
</ui:include>   
    
<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler2}"/>
	<ui:param name="popupName" value="searchJurisdiction2"/>
	<ui:param name="popupForm" value="searchJurisdictionForm2"/>
	<ui:param name="jurisId" value="jurisId2"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput2"/>
</ui:include>   

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler3}"/>
	<ui:param name="popupName" value="searchJurisdiction3"/>
	<ui:param name="popupForm" value="searchJurisdictionForm3"/>
	<ui:param name="jurisId" value="jurisId3"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput3"/>
</ui:include> 

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler4}"/>
	<ui:param name="popupName" value="searchJurisdiction4"/>
	<ui:param name="popupForm" value="searchJurisdictionForm4"/>
	<ui:param name="jurisId" value="jurisId4"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput4"/>
</ui:include> 

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler5}"/>
	<ui:param name="popupName" value="searchJurisdiction5"/>
	<ui:param name="popupForm" value="searchJurisdictionForm5"/>
	<ui:param name="jurisId" value="jurisId5"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput5"/>
</ui:include> 

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{situsAdminMatrixBackingBean.filterHandler6}"/>
	<ui:param name="popupName" value="searchJurisdiction6"/>
	<ui:param name="popupForm" value="searchJurisdictionForm6"/>
	<ui:param name="jurisId" value="jurisId6"/>
	<ui:param name="reRender" value="situsMatrixWhatIfForm:jurisInput6"/>
</ui:include> 
    
   </f:view>
   
  </ui:define>
 </ui:composition>
</html>
