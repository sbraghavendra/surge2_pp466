<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixMaintenanceForm:matrixTable', 'selectedRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixMaintenanceForm:detailTable', 'selectedDetailRowIndex'); } );     
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixMaintenanceForm:aMList', 'selectedRulesRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixMaintenanceForm:aDetailTable', 'selectedRulesDetailRowIndex'); } );      
//]]>
</script>
</ui:define>
<ui:define name="body">

    <h:inputHidden id="selectedRowIndex" value="#{situsAdminMatrixBackingBean.selectedRowIndex}" />
    <h:inputHidden id="selectedDetailRowIndex" value="#{situsAdminMatrixBackingBean.selectedDetailRowIndex}" />
    
    <h:form id="situsMatrixMaintenanceForm">
     <h1><h:graphicImage id="imgSitusMatrixMaintenance" alt="#{(situsAdminMatrixBackingBean.isAdminActionRole) ? 'Admin Situs Matrix' : 'Situs Matrix'}" url="#{(situsAdminMatrixBackingBean.isAdminActionRole) ? '/images/headers/hdr-admin-matrix-maintenance.gif' : '/images/headers/hdr-user-matrix-maintenance.gif'}"  width="250" height="19" /></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" /></div>
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'situsMatrixMaintenanceForm:searchBtn')">
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
               
         <tr>
          <td style="width: 50px;">&#160;</td>
       
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 150px;" class="column-input">           	
				<h:selectOneMenu id="transactionTypeItem" binding="#{situsAdminMatrixBackingBean.transactionTypeCodeMenu}" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.transactionTypeCode}"  >
							<f:selectItems id="transactionTypeItemItems" value="#{situsAdminMatrixBackingBean.filterTransactionTypeCodeItems}"/>
							<a4j:support id="transactionTypeItemSupport" event="onchange" reRender="methodOfDeliveryItem,rateTypeItem" immediate="true" actionListener="#{situsAdminMatrixBackingBean.filterTransactionTypeCodeChanged}"/>
				</h:selectOneMenu>
          </td>
          
          <td>&#160;</td>      
          
          <td style="width: 70px;">Country:</td>
          <td style="width: 120px;" class="column-input">
					<h:selectOneMenu id="countryMenuMain" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.situsCountryCode}" immediate="true" 
							valueChangeListener="#{situsAdminMatrixBackingBean.searchCountryChanged}" onchange="submit();">
						<f:selectItems value="#{situsAdminMatrixBackingBean.countryMenuItems}"/>
					</h:selectOneMenu>
		  </td>
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 150px;" class="column-input">           	
				<h:selectOneMenu id="rateTypeItem" binding="#{situsAdminMatrixBackingBean.rateTypeCodeMenu}" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.ratetypeCode}"  >
							<f:selectItems id="rateTypeItemItems" value="#{situsAdminMatrixBackingBean.filterRateTypeCodeItems}"/>
							<a4j:support id="rateTypeItemSupport" event="onchange" reRender="methodOfDeliveryItem,transactionTypeItem" immediate="true" actionListener="#{situsAdminMatrixBackingBean.filterRatetypeCodeChanged}"/>
				</h:selectOneMenu>
          </td>
          
          
          <td>&#160;</td>  
          
          <td style="width: 70px;">State:</td>
          <td style="width: 120px;" class="column-input">
				<h:selectOneMenu id="stateMenuMain" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.situsStateCode}" immediate="true" >
					<f:selectItems id="stateMenuMainItems" value="#{situsAdminMatrixBackingBean.stateMenuItems}"/>
				</h:selectOneMenu>
		  </td>
              
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 150px;" class="column-input">           	
				<h:selectOneMenu id="methodOfDeliveryItem" binding="#{situsAdminMatrixBackingBean.methodOfDeliveryMenu}" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.methodDeliveryCode}"  >
							<f:selectItems id="methodOfDeliveryItemItems" value="#{situsAdminMatrixBackingBean.filterMethodOfDeliveryItems}"/>
							<a4j:support id="methodOfDeliveryItemSupport" event="onchange" reRender="rateTypeItem,transactionTypeItem" immediate="true" actionListener="#{situsAdminMatrixBackingBean.filterMethodDeliveryCodeChanged}"/>
				</h:selectOneMenu>
          </td>
          
          
          <td>&#160;</td> 
          <td style="width: 70px;">Jur. Level:</td>
          <td style="width: 120px;">
				<h:selectOneMenu id="displayMenuItems" value="#{situsAdminMatrixBackingBean.filterSitusMatrix.jurLevel}">
							<f:selectItems value="#{situsAdminMatrixBackingBean.displayMenuItems}"/>
				</h:selectOneMenu>
		  </td>      
          <td style="width: 70px;">&#160;</td>
         </tr>
         
        </table>
       </div>
       <!-- table-one-content -->
       <div id="table-one-bottom">
        <ul class="right">
         <li class="clear">
          <h:commandLink id="btnClear"
                         action="#{situsAdminMatrixBackingBean.resetFilterSearchAction}" />
         </li>
         <li class="search">
          <h:commandLink id="searchBtn" reRender="matrixTable,detailTable,aMList,aDetailTable,addBtn,dupSystemBtn,copyAddBtn,updateBtn,deleteBtn,viewBtn,okDeleteBtn"
                         action="#{situsAdminMatrixBackingBean.retrieveFilterSitusMatrix}" />
         </li>
        </ul>
       </div>
       <!-- table-one-bottom -->
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
     <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-situsmatrix.gif" width="192"  height="17" />
      </span>
     </div>
     <div id="bottom">
      <div id="table-four">
       <div id="table-four-top">
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{situsAdminMatrixBackingBean.situsMatrixDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
       </div>
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner">
          <rich:dataTable rowClasses="odd-row,even-row" id="matrixTable" rows="#{situsAdminMatrixBackingBean.situsMatrixDataModel.pageSize }"
                          value="#{situsAdminMatrixBackingBean.situsMatrixDataModel}" var="situsMatrixMaintenance">
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixMaintenanceForm:matrixTable', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedRowChanged}"
                        reRender="detailTable,aMList,aDetailTable,addBtn,dupSystemBtn,copyAddBtn,updateBtn,deleteBtn,viewBtn,okDeleteBtn" />
                        
           	<rich:column id="transactionTypeCode" styleClass="column-left" >
            	<f:facet name="header">
             	<a4j:commandLink id="transactionTypeCode-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['transactionTypeCode']}"
                              value="Transaction Type"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="matrixTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.transactionTypeMap[situsMatrixMaintenance.transactionTypeCode]}"  />
           	</rich:column>
           	
           <rich:column id="ratetypeCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="ratetypeCode-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['ratetypeCode']}"
                              value="Rate Type"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="matrixTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.ratetypeMap[situsMatrixMaintenance.ratetypeCode]}"  />
           	</rich:column>

           	<rich:column id="methodDeliveryCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="methodDeliveryCode-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['methodDeliveryCode']}"
                              value="Method of Delivery"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="matrixTable" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.methodDeliveryMap[situsMatrixMaintenance.methodDeliveryCode]}"  />
           	</rich:column>
           	
           	<rich:column id="blank" styleClass="column-left" style="width:100%" >
            	<f:facet name="header" >
             	<a4j:commandLink id="blank-a4j" value="" reRender="matrixTable" />
            	</f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="matrixTable"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{situsAdminMatrixBackingBean.situsMatrixDataModel.curPage}" />
                          
       
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable" value="#{situsAdminMatrixBackingBean.situsMatrixList}" var="situsMatrix">
            <a4j:support id="detailtbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixMaintenanceForm:detailTable', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedMatrixDetailRowChanged}"
                        reRender="aMList,aDetailTable,addBtn,dupSystemBtn,copyAddBtn,updateBtn,deleteBtn,viewBtn,okDeleteBtn" />
           	 
           	
           	<rich:column id="situsCountryCode" styleClass="column-left" sortBy="#{situsMatrix.situsCountryCode}" width="300px" >
            	<f:facet name="header"><h:outputText id="situsCountryCode-a4j" styleClass="headerText" value="Country"/></f:facet>
            	<h:outputText value="#{cacheManager.countryMap[situsMatrix.situsCountryCode]} (#{situsMatrix.situsCountryCode})"  />
           	</rich:column>
           	
           	<rich:column id="situsStateCode" styleClass="column-left" sortBy="#{situsMatrix.situsStateCode}" width="300px" >
            	<f:facet name="header"><h:outputText id="situsStateCode-a4j" styleClass="headerText" value="State"/></f:facet>
            	<h:outputText value="#{cacheManager.taxCodeStateMap[situsMatrix.situsStateCode].name}    	
            		#{(not empty cacheManager.taxCodeStateMap[situsMatrix.situsStateCode].name) ? '(' : ''}#{situsMatrix.situsStateCode}#{(not empty cacheManager.taxCodeStateMap[situsMatrix.situsStateCode].name) ? ')' : ''}         	
            		" />
           	</rich:column>
           	
           	<rich:column id="jurLevel" styleClass="column-left" sortBy="#{situsMatrix.jurLevel}" width="300px" >
            	<f:facet name="header"><h:outputText id="jurLevel-a4j" styleClass="headerText" value="Jurisdiction Level"/></f:facet>
            	<h:outputText value="#{situsMatrix.jurLevel}"  />
           	</rich:column>
           	
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner">
          <rich:dataTable rowClasses="odd-row,even-row" id="aMList"
                          rows="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.pageSize }"
                          value="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel}"
                          var="matrix">
                          
           		<a4j:support id="ref2tbl" event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixMaintenanceForm:aMList', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedMasterRowChanged}"
                        reRender="aDetailTable,addBtn,dupSystemBtn,copyAddBtn,updateBtn,deleteBtn,viewBtn,okDeleteBtn" />
                        
          			<rich:column id="driver01" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_01']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver01" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver01']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_01']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver01]}"  />
					</rich:column>
					
					<rich:column id="driver02" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_02']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver02" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver02']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_02']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver02]}"  />
					</rich:column>
					
					<rich:column id="driver03" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_03']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver03" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver03']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_03']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver03]}"  />
					</rich:column>
					
					<rich:column id="driver04" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_04']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver04" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver04']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_04']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver04]}"  />
					</rich:column>
					
					<rich:column id="driver05" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_05']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver05" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver05']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_05']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver05]}"  />
					</rich:column>
					
					<rich:column id="driver06" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_06']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver06" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver06']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_06']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver06]}"  />
					</rich:column>
					
					<rich:column id="driver07" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_07']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver07" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver07']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_07']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver07]}"  />
					</rich:column>
					
					<rich:column id="driver08" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_08']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver08" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver08']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_08']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver08]}"  />
					</rich:column>
					
					<rich:column id="driver09" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_09']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver09" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver09']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_09']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver09]}"  />
					</rich:column>
					
					<rich:column id="driver10" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_10']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver10" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver10']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_10']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver10]}"  />
					</rich:column>
					
					<rich:column id="driver11" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_11']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver11" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver11']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_11']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver11]}"  />
					</rich:column>
					
					<rich:column id="driver12" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_12']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver12" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver12']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_12']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver12]}"  />
					</rich:column>
					
					<rich:column id="driver13" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_13']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver13" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver13']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_13']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver13]}"  />
					</rich:column>
					
					<rich:column id="driver14" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_14']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver14" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver14']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_14']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver14]}"  />
					</rich:column>
					
					<rich:column id="driver15" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_15']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver15" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver15']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_15']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver15]}"  />
					</rich:column>
					
					<rich:column id="driver16" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_16']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver16" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver16']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_16']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver16]}"  />
					</rich:column>
					
					<rich:column id="driver17" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_17']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver17" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver17']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_17']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver17]}"  />
					</rich:column>
					
					<rich:column id="driver18" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_18']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver18" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver18']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_18']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver18]}"  />
					</rich:column>
					
					<rich:column id="driver19" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_19']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver19" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver19']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_19']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver19]}"  />
					</rich:column>
					
					<rich:column id="driver20" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_20']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver20" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver20']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_20']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver20]}"  />
					</rich:column>
					
					<rich:column id="driver21" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_21']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver21" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver21']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_21']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver21]}"  />
					</rich:column>
					
					<rich:column id="driver22" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_22']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver22" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver22']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_22']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver22]}"  />
					</rich:column>
					
					<rich:column id="driver23" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_23']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver23" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver23']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_23']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver23]}"  />
					</rich:column>
					
					<rich:column id="driver24" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_24']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver24" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver24']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_24']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver24]}"  />
					</rich:column>
					
					<rich:column id="driver25" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_25']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver25" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver25']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_25']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver25]}"  />
					</rich:column>
					
					<rich:column id="driver26" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_26']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver26" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver26']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_26']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver26]}"  />
					</rich:column>
					
					<rich:column id="driver27" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_27']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver27" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver27']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_27']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver27]}"  />
					</rich:column>
					
					<rich:column id="driver28" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_28']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver28" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver28']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_28']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver28]}"  />
					</rich:column>
					
					<rich:column id="driver29" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_29']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver29" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver29']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_29']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver29]}"  />
					</rich:column>
					
					<rich:column id="driver30" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_30']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver30" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver30']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_30']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver30]}"  />
					</rich:column>
					
					<rich:column id="binaryWeight" >
						<f:facet name="header">
							<a4j:commandLink id="s_binaryWeight" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['binaryWeight']}"
								value="Binary Weight"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.binaryWeight}"  />
					</rich:column>	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll-1"
                          for="aMList"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.curPage}" />
                          
                          
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="aDetailTable"
                          value="#{situsAdminMatrixBackingBean.situsMatrixRulersDetailList}"
                          var="situsMatrix">
                          
            <a4j:support id="dtltbl"
                        event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixMaintenanceForm:aDetailTable', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedDetailRowChanged}"
                        reRender="addBtn,dupSystemBtn,copyAddBtn,updateBtn,deleteBtn,viewBtn,okDeleteBtn" />
           
           	<rich:column id="matrixId" styleClass="column-left" sortBy="#{situsMatrix.situsMatrixId}" >
            	<f:facet name="header"><h:outputText id="matrixId-a4j" styleClass="headerText" value="Matrix ID"/></f:facet>
            	<h:outputText value="#{situsMatrix.situsMatrixId}" />
           	</rich:column>
           	
           	<rich:column id="customFlag" styleClass="column-left" sortBy="#{situsMatrix.customFlag}" >
            	<f:facet name="header"><h:outputText id="customFlag-a4j" styleClass="headerText" value="Custom?"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.customBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>

           	<rich:column id="effectiveDate" styleClass="column-left" sortBy="#{situsMatrix.effectiveDate}" >
            	<f:facet name="header"><h:outputText id="effectiveDate-a4j" styleClass="headerText" value="Effective Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.effectiveDate}" />
           	</rich:column>         
           	
           	<rich:column id="primarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.primarySitusCode}" >
            	<f:facet name="header"><h:outputText id="primarySitusCode-a4j" styleClass="headerText" value="Primary Situs Code"/></f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.matrixNameMap[situsMatrix.primarySitusCode]}" />
           	</rich:column>
           	
           	<rich:column id="primaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.primaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="primaryTaxtypeCode-a4j" styleClass="headerText" value="Primary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.taxTypeMapMap[situsMatrix.primaryTaxtypeCode]}" />
           	</rich:column>
           	
           	<rich:column id="secondarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.secondarySitusCode}" >
            	<f:facet name="header"><h:outputText id="secondarySitusCode-a4j" styleClass="headerText" value="Secondary Situs Code"/></f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.matrixNameMap[situsMatrix.secondarySitusCode]}" />
           	</rich:column>
           	
           	<rich:column id="secondaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.secondaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="secondaryTaxtypeCode-a4j" styleClass="headerText" value="Secondary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.taxTypeMapMap[situsMatrix.secondaryTaxtypeCode]}" />
           	</rich:column>
           	
           	<rich:column id="expirationDate" styleClass="column-left" sortBy="#{situsMatrix.expirationDate}" >
            	<f:facet name="header"><h:outputText id="expirationDate-a4j" styleClass="headerText" value="Expiration Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.expirationDate}" />
           	</rich:column>
           	
           	<rich:column id="allLevelsFlag" styleClass="column-left" sortBy="#{situsMatrix.allLevelsFlag}" >
            	<f:facet name="header"><h:outputText id="allLevelsFlag-a4j" styleClass="headerText" value="All Levels?"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.allLevelsBooleanFlag}" disabled="true" styleClass="check" />
           	</rich:column>
           	
           	<rich:column id="activeFlag" styleClass="column-left" sortBy="#{situsMatrix.activeFlag}" >
            	<f:facet name="header"><h:outputText id="activeFlag-a4j" styleClass="headerText" value="Active?"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.activeBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>
           	
      		<rich:column id="comments" styleClass="column-left" sortBy="#{situsMatrix.comments}" >
            	<f:facet name="header"><h:outputText id="comments-a4j" styleClass="headerText" value="Comments"/></f:facet>
            	<h:outputText value="#{situsMatrix.comments}" />
           	</rich:column>        
           
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
                          
       <!--                    
       <div id="table-four-bottom">
        <ul class="right">
         	<li class="add"><h:commandLink id="addSitusMatrix" immediate="true" action="#{situsAdminMatrixBackingBean.displayAddMatrixAction}" /></li>
			<li class="copy-add"><h:commandLink id="copyAddSitusMatrix" disabled="#{situsAdminMatrixBackingBean.disableMatrixButton}" immediate="true" action="#{situsAdminMatrixBackingBean.displayCopyAddMatrixAction}" /></li>
         	<li class="update"><h:commandLink id="updateSitusMatrix" disabled="#{situsAdminMatrixBackingBean.disableMatrixButton}" immediate="true" action="#{situsAdminMatrixBackingBean.displayUpdateMatrixAction}" /></li>       	
         	<li class="delete115"><h:commandLink id="deleteSitusMatrix" disabled="#{situsAdminMatrixBackingBean.disableMatrixButton}" immediate="true"  action="#{situsAdminMatrixBackingBean.displayDeleteMatrixAction}" /></li>
			<li class="whatif"><a4j:commandLink id="whatIfSitusMatrix"  immediate="true" action="#{situsAdminMatrixBackingBean.processDisplayWhatIf}"  /></li>
        </ul>
       </div> table-four-bottom -->
       
       
       <div id="table-four-bottom">
        <ul class="right">
			<li class="add"><h:commandLink id="addBtn" disabled="false" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="addMatrixRule" /></h:commandLink></li>
			
			<li class="dupsystem"><h:commandLink id="dupSystemBtn" disabled="#{situsAdminMatrixBackingBean.disableDupSystemButton}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="dupSystemMatrixRule" /></h:commandLink></li>

			<li class="copy-add"><h:commandLink id="copyAddBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="copyaddMatrixRule" /></h:commandLink></li>  
			<li class="update"><h:commandLink id="updateBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix or situsAdminMatrixBackingBean.isUserDisable}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="updateMatrixRule" /></h:commandLink></li>
	<!--  		<li class="copy-update"><h:commandLink id="copyUpdateBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="copyupdateMatrixRule" /></h:commandLink></li> -->
			<li class="delete115"><h:commandLink id="deleteBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix or situsAdminMatrixBackingBean.isUserDisable}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="deleteMatrixRule" /></h:commandLink></li>
			<li class="view"><h:commandLink id="viewBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="viewMatrixRule" /></h:commandLink></li>
			<li class="ok"><h:commandLink id="okDeleteBtn" disabled="#{!situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processUpdateCommand}" /></li>					
			<li class="addsitusdriver"><a4j:commandLink id="addSitusDriverBtn" action="#{situsAdminMatrixBackingBean.processDisplayAddSitus}" /></li>
			<li class="whatif"><a4j:commandLink id="whatIfSitusMatrix"  immediate="true" action="#{situsAdminMatrixBackingBean.processDisplayWhatIf}"  /></li>
			
        </ul>
       </div> <!-- table-four-bottom -->
       
       
       
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>

   
  </ui:define>
 </ui:composition>
</html>
