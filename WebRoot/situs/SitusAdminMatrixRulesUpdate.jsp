<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">

	<h:form id="situsMatrixRuleUpdateForm">

	<h1><img id="imgSitusMatrixMaintenance" alt="Admin Situs Matrix" src="#{(situsAdminMatrixBackingBean.isAdminActionRole) ? '../images/headers/hdr-admin-matrix-maintenance.gif' : '../images/headers/hdr-user-matrix-maintenance.gif'}"  width="250" height="19" /></h1>
	
	<div id="top" style="width:983px">
	<div id="table-one"   >
	<div id="table-one-top" >
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="width:100%">
	<table cellpadding="0" cellspacing="0" width="100%"  id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="10"><h:outputText value="#{situsAdminMatrixBackingBean.ruleActionText}"/>Situs Matrix Rule</td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="10">Situs Header</th>
		</tr>
		  
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 150px;">      
				<h:selectOneMenu id="transactionTypeItem" binding="#{situsAdminMatrixBackingBean.transactionTypeCodeMenuHeader}" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.transactionTypeCode}" 
									disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}" >
							<f:selectItems id="transactionTypeItemItems" value="#{situsAdminMatrixBackingBean.headerTransactionTypeCodeItems}"/>
							<a4j:support id="transactionTypeItemSupport" event="onchange" action="situs_admin_matrix_rules_update" immediate="true" actionListener="#{situsAdminMatrixBackingBean.updateTransactionTypeChanged}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>  
          
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 220px;">  
				<h:selectOneMenu id="rateTypeItem" binding="#{situsAdminMatrixBackingBean.rateTypeCodeMenuHeader}" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.ratetypeCode}" 
									disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}" >
							<f:selectItems id="rateTypeItemItems" value="#{situsAdminMatrixBackingBean.headerRateTypeCodeItems}"/>
							<a4j:support id="rateTypeItemSupport" event="onchange" action="situs_admin_matrix_rules_update" immediate="true" actionListener="#{situsAdminMatrixBackingBean.updateRateTypeChanged}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 150px;">   
				<h:selectOneMenu id="methodOfDeliveryItem" binding="#{situsAdminMatrixBackingBean.methodOfDeliveryMenuHeader}" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.methodDeliveryCode}" 
									disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}" >
							<f:selectItems id="methodOfDeliveryItemItems" value="#{situsAdminMatrixBackingBean.headerMethodOfDeliveryItems}"/>
							<a4j:support id="methodOfDeliveryItemSupport"  event="onchange" action="situs_admin_matrix_rules_update" immediate="true" actionListener="#{situsAdminMatrixBackingBean.updateMethodofDeliveryChanged}"/>
				</h:selectOneMenu>
          </td>
          <td style="width:50%">&#160;</td> 
        </tr>
         
        <tr>
		  <th colspan="10">Jurisdiction</th>
		</tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          
          <td style="width: 70px;">Country:</td>
          <td style="width: 120px;" class="column-input">
					<h:selectOneMenu id="countryMenuMain" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.situsCountryCode}" immediate="true" 
					
							valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderCountryChanged}" onchange="submit();"
							disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}" 
							
							>
						<f:selectItems value="#{situsAdminMatrixBackingBean.headerCountryMenuItems}"/>
					</h:selectOneMenu>
		  </td>
          <td>&#160;</td>  
          
          <td style="width: 70px;">State:</td>
          <td style="width: 120px;" class="column-input">
				<h:selectOneMenu id="stateMenuMain" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.situsStateCode}" immediate="true" 
							disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}"
						>
					<f:selectItems id="stateMenuMainItems" value="#{situsAdminMatrixBackingBean.headerStateMenuItems}"/>
				</h:selectOneMenu>
		  </td>
		  <td>&#160;</td>
          <td style="width: 70px;">Jurisdiction Level:</td>
          <td style="width: 120px;" class="column-input">
				<h:selectOneMenu id="jurisdictionLevelSelect" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.jurLevel}" immediate="true" 
							disabled="#{!situsAdminMatrixBackingBean.isAddOrCopyAdd}"
						>
					<f:selectItems value="#{situsAdminMatrixBackingBean.displayJurisdictionLevelItems}"/>
				</h:selectOneMenu>
		  </td> 
          <td>&#160;</td>
        </tr>
        
        <tr>
		  <th colspan="10">Driver Selection</th>
		</tr>
		
		<c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_01'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_02'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_03']}" >
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td><h:outputText  id="driver01Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_01'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_01']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_01']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver01" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_01']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver01}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver02Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_02'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_02']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_02']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver02" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_02']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver02}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver03Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_03'] ? 'color:red;' : '' }"           			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_03']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_03']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver03" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_03']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver03}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_04'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_05'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_06']}" >
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td><h:outputText  id="driver04Label"  style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_04'] ? 'color:red;' : '' }"   			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_04']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_04']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver04" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_04']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver04}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver05Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_05'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_05']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_05']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver05" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_05']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver05}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver06Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_06'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_06']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_06']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver06" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_06']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver06}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
  			
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_07'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_08'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_09']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver07Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_07'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_07']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_07']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver07" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_07']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver07}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver08Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_08'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_08']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_08']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver08" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_08']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver08}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver09Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_09'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_09']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_09']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver09" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_09']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver09}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_10'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_11'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_12']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver10Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_10'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_10']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_10']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver10" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_10']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver10}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver11Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_11'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_11']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_11']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver11" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_11']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver11}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver12Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_12'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_12']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_12']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver12" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_12']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver12}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_13'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_14'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_15']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver13Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_13'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_13']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_13']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver13" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_13']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver13}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver14Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_14'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_14']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_14']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver14" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_14']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver14}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver15Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_15'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_15']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_15']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver15" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_15']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver15}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>  
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_16'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_17'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_18']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver16Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_16'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_16']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_16']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver16" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_16']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver16}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver17Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_17'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_17']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_17']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver17" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_17']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver17}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver18Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_18'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_18']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_18']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver18" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_18']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver18}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
          
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_19'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_20'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_21']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver19Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_19'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_19']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_19']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver19" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_19']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver19}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver20Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_20'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_20']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_20']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver20" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_20']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver20}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver21Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_21'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_21']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_21']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver21" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_21']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver21}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_22'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_23'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_24']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver22Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_22'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_22']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_22']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver22" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_22']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver22}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver23Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_23'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_23']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_23']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver23" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_23']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver23}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver24Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_24'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_24']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_24']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver24" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_24']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver24}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_25'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_26'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_27']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver25Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_25'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_25']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_25']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver25" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_25']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver25}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver26Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_26'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_26']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_26']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver26" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_26']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver26}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver27Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_27'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_27']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_27']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver27" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_27']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver27}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <c:if test="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_28'] or !empty  situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_29'] or !empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_30']}" >
		<tr>
          <td style="width: 80px;">&#160;</td>
          <td><h:outputText  id="driver28Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_28'] ? 'color:red;' : '' }"    			
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_28']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_28']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver28" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_28']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" 	
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver28}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver29Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_29'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_29']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_29']}" />:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver29" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_29']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver29}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td><h:outputText  id="driver30Label" style="#{situsAdminMatrixBackingBean.situsMatrixNameMandatoryMap['DRIVER_30'] ? 'color:red;' : '' }" 
          			value="#{situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_30']}" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_30']}" />:</td> 
          <td style="width: 150px;">           	
				<h:selectOneMenu id="driver30" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameUpdateMap['DRIVER_30']}"
					disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isDupSystemEnabled or situsAdminMatrixBackingBean.isRuleCopyUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}"
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.driver30}" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.situsDriverTypeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
        </tr>
        </c:if>
        
        <tr>
		  <th colspan="10">Details</th>
		</tr>
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Effective Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="effectiveDate" disabled="#{situsAdminMatrixBackingBean.isRuleViewOnly or situsAdminMatrixBackingBean.isRuleUpdate}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy" validator="#{situsAdminMatrixBackingBean.validateEffectiveDate}"
                          value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.effectiveDate}" />
          </td>
          <td>&#160;</td>
          <td>Expiration Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="expirationDate" disabled="#{situsAdminMatrixBackingBean.isRuleViewOnly}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue(event);"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy" validator="#{situsAdminMatrixBackingBean.validateExpirationDate}"
                          value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.expirationDate}" popup="true"                     
                          showApplyButton="false" inputClass="textbox" />
          </td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Primary:</td>
          <td colspan="7">
          		<div id="embedded-table">
           		<table>
				<tr>
					<td>Situs Location:</td>
					<td style="width: 150px;">           	
						<h:selectOneMenu id="primarySitusCode" rendered="true" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.primarySitusCode}" 
											disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" >
							<f:selectItems value="#{situsAdminMatrixBackingBean.primaryLocationTypeItems}"/>
						</h:selectOneMenu>
          			</td>

					<td>&#160;</td>
					<td>Tax Type:</td>
					<td style="width: 150px;">           	
						<h:selectOneMenu id="primaryTaxtypeCode" rendered="true" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.primaryTaxtypeCode}" 
											disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" >
							<f:selectItems value="#{situsAdminMatrixBackingBean.primaryTaxTypeItems}"/>
						</h:selectOneMenu>
          			</td>	
					
					<td>&#160;</td>
				</tr>
				</table>
				</div>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Secondary:</td>
          <td colspan="7">
          		<div id="embedded-table">
           		<table>
				<tr>
					<td>Situs Location:</td>
					<td style="width: 150px;">           	
						<h:selectOneMenu id="secondarySitusCode" rendered="true" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.secondarySitusCode}" 
											disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" >
							<f:selectItems value="#{situsAdminMatrixBackingBean.secondaryLocationTypeItems}"/>
						</h:selectOneMenu>
          			</td>			
							
					<td>&#160;</td>
					<td>Tax Type:</td>
					<td style="width: 150px;">           	
						<h:selectOneMenu id="secondaryTaxtypeCode" rendered="true" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.secondaryTaxtypeCode}" 
											disabled="#{situsAdminMatrixBackingBean.isRuleUpdate or situsAdminMatrixBackingBean.isRuleViewOnly}" >
							<f:selectItems value="#{situsAdminMatrixBackingBean.secondaryTaxTypeItems}"/>
						</h:selectOneMenu>
          			</td>
					
					<td>&#160;</td>
				</tr>
				</table>
				</div>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Comments:</td>
          <td colspan="7">
           	<h:inputText style="width: 740px;" id="comments" rendered="true" value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.comments}" 
           					disabled="#{situsAdminMatrixBackingBean.isRuleViewOnly}" />	
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>All Levels?</td>
          <td colspan="7">
           	<h:selectBooleanCheckbox id="allLevels" 
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.allLevelsBooleanFlag}" disabled="#{situsAdminMatrixBackingBean.isRuleViewOnly or situsAdminMatrixBackingBean.isRuleUpdate}" />
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Active?</td>
          <td colspan="7">
           	<h:selectBooleanCheckbox id="active" 
					value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.activeBooleanFlag}" disabled="#{situsAdminMatrixBackingBean.isRuleViewOnly}" />
          </td>
          <td>&#160;</td>
        </tr>
        

        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Last Update User ID:</td>
          <td colspan="7">
           	<h:inputText style="width: 250px;"  id="updateUserId"  disabled="true" rendered="true"
                          value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.updateUserId}" />
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td>Last Update Timestamp:</td>
          <td colspan="7">
           	<h:inputText style="width: 250px;"  id="updateTimestamp"  disabled="true" rendered="true"
                          value="#{situsAdminMatrixBackingBean.updateSitusMatrixRule.updateTimestamp}" >
                          <f:converter converterId="dateTime"/>
			</h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	<div id="table-one-bottom">
	<ul class="right">
		<li class="copy-update"><h:commandLink id="copyUpdateBtn" disabled="#{!situsAdminMatrixBackingBean.isRuleUpdate}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="copyupdateMatrixRule" /></h:commandLink></li>
		<li class="ok"><h:commandLink id="okBtn" rendered="#{situsAdminMatrixBackingBean.okEnanled}" action="#{situsAdminMatrixBackingBean.processUpdateCommand}"/></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" rendered="#{!situsAdminMatrixBackingBean.isRuleView}" action="situs_admin_matrix_main" ></h:commandLink></li>
	</ul>
	</div>
	
	
	</div>
	</div>

</h:form>
</ui:define>
</ui:composition>
</html>
