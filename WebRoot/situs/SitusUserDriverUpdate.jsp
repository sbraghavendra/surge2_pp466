<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
   

//]]>
</script>
</ui:define>
<ui:define name="body">
   <f:view>

    <h:form id="situsDriverUpdateForm">
     <h1><h:graphicImage id="imgUserMap" alt="Situs Drivers" value="/images/headers/hdr-user-situs-maintenance.gif" /></h1>
     
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'situsDriverUpdateForm:searchBtn')">
       
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
        <thead>
			<tr><td colspan="5"><h:outputText value="#{situsUserDriverBackingBean.actionText}"/><h:outputText value="#{(!situsUserDriverBackingBean.isCustomSitus and situsUserDriverBackingBean.updateAction) ? ' System ' : ' Custom '}"/>Situs Drivers</td></tr>
		 </thead>
		 <tbody>
		 
         <tr><th colspan="5" >Situs Information</th></tr>	
               
         <tr>
          <td>&#160;</td>
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="transactionTypeItem" value="#{situsUserDriverBackingBean.selectedSitusDriverDTO.transactionTypeCode}" disabled="#{situsUserDriverBackingBean.deleteAction or situsUserDriverBackingBean.updateAction}" >
							<f:selectItems value="#{situsUserDriverBackingBean.updateTransactionTypeCodeItems}"  />
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width:100%;">&#160;</td>
         </tr>
         
         <tr>
          <td>&#160;</td>
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="rateTypeItem" value="#{situsUserDriverBackingBean.selectedSitusDriverDTO.ratetypeCode}" disabled="#{situsUserDriverBackingBean.deleteAction or situsUserDriverBackingBean.updateAction}" >
							<f:selectItems value="#{situsUserDriverBackingBean.updateRateTypeCodeItems}" />
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width:100%;">&#160;</td>
         </tr>
         
         <tr>
          <td>&#160;</td>
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="methodOfDeliveryItem" value="#{situsUserDriverBackingBean.selectedSitusDriverDTO.methodDeliveryCode}" disabled="#{situsUserDriverBackingBean.deleteAction or situsUserDriverBackingBean.updateAction}" >
							<f:selectItems value="#{situsUserDriverBackingBean.updateMethodOfDeliveryItems}"  />
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          <td style="width:100%;">&#160;</td>
         </tr>
         
         <tr><th colspan="5" >Drivers</th></tr>
         </tbody>
        </table>
       </div>
       <!-- table-one-content -->
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
     
     
     <div id="bottom">
      <div id="table-four">
      <div id="table-four-top"></div>
      
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  >
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable" width="100%"
                          value="#{situsUserDriverBackingBean.situsDriverDetailDTOList}"
                          var="situsDriverDetail">
           
            <rich:column id="driverName" styleClass="column-left" sortBy="#{situsDriverDetail.locationTypeCode}" width="300px" >
            	<f:facet name="header"><h:outputText id="driverName-a4j" styleClass="headerText" value="Driver Name"/></f:facet>
            	<h:outputText value="#{situsUserDriverBackingBean.driverNameMap[situsDriverDetail.locationTypeCode]}"  />
           	</rich:column>
           
           <rich:column id="driverId" styleClass="column-left" sortBy="#{situsDriverDetail.driverId}" width="50px">
            	<f:facet name="header"><h:outputText id="driverId-a4j" styleClass="headerText" value="Driver ID" /></f:facet>
            	<h:selectOneMenu id="driverIdItem" value="#{situsDriverDetail.driverId}" style="width:70px;" disabled="#{situsUserDriverBackingBean.deleteAction or situsUserDriverBackingBean.updateAction}" >
							<f:selectItems value="#{situsUserDriverBackingBean.driverSelectItems}" />
				</h:selectOneMenu>
            	
           	</rich:column> 
           	
           	<rich:column id="mandatory"  sortBy="#{situsDriverDetail.mandatoryFlag}" style="text-align:center;" width="80px" rendered="#{!situsUserDriverBackingBean.isCustomSitus and situsUserDriverBackingBean.updateAction}" >
            	<f:facet name="header"><h:outputText id="mandatory-a4j" styleClass="headerText" value="Mandatory"/></f:facet>
            	<h:selectBooleanCheckbox id="chkMandatory" value="#{situsDriverDetail.mandatoryBooleanFlag}" styleClass="check" disabled="true" />
           	</rich:column>
           	
           	<rich:column id="customMandatoryFlag"  sortBy="#{situsDriverDetail.customMandatoryFlag}" style="text-align:center;" width="80px" >
            	<f:facet name="header"><h:outputText id="customMandatoryFlag-a4j" styleClass="headerText" value="Custom Mandatory"/></f:facet>
            	<h:selectBooleanCheckbox id="chkCustomMandatoryFlag" value="#{situsDriverDetail.customMandatoryBooleanFlag}" styleClass="check" disabled="#{situsUserDriverBackingBean.deleteAction or situsDriverDetail.mandatoryBooleanFlag}" />
           	</rich:column>
           	
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
                                           
       <div id="table-four-bottom">
        <ul class="right">
         	<li class="ok"><h:commandLink id="okAdminDriverBtn" action="#{situsUserDriverBackingBean.okDriverAction}" /></li>
         	<li class="cancel"><h:commandLink id="cancelAdminDriverBtn" immediate="true" action="situs_user_drivers_main" ></h:commandLink></li> 
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
   </f:view>
   
  </ui:define>
 </ui:composition>
</html>
