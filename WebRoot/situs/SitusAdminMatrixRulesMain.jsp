<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixRulesForm:aMList', 'selectedRulesRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('situsMatrixRulesForm:detailTable', 'selectedRulesDetailRowIndex'); } );
    registerEvent(window, "load", adjustHeight);           

//]]>
</script>
</ui:define>
<ui:define name="body">
   <f:view>
    <h:inputHidden id="selectedRulesRowIndex" value="#{situsAdminMatrixBackingBean.selectedRulesRowIndex}" />
    <h:inputHidden id="selectedRulesDetailRowIndex" value="#{situsAdminMatrixBackingBean.selectedRulesDetailRowIndex}" />
    
    <h:form id="situsMatrixRulesForm">
     <h1><img id="imgSitusMatrixMaintenance" alt="Situs Matrix" src="#{(situsAdminMatrixBackingBean.isAdminActionRole) ? '../images/headers/hdr-admin-matrix-maintenance.gif' : '../images/headers/hdr-user-matrix-maintenance.gif'}"  width="250" height="19" /></h1>
     
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'situsMatrixRulesForm:searchBtn')">
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
         <thead>
			<tr><td colspan="10"><h:outputText value="#{situsAdminMatrixBackingBean.matrixActionText}" /></td></tr>
		 </thead>
         <tr>
			<th colspan="10">Situs Header</th>
		 </tr>     
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Transaction Type:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="transactionTypeItem" value="#{situsAdminMatrixBackingBean.headerSitusMatrix.transactionTypeCode}"
							disabled="#{situsAdminMatrixBackingBean.isUpdateSitusMatrix or situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
							valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderChanged}" onchange="submit();" 
							reRender="itemLevelTable,itemCountyTable,itemCityTable,itemStjTable,aMList,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn">
						<f:selectItems value="#{situsAdminMatrixBackingBean.headerTransactionTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>  
          
          <td style="width: 70px;">Rate Type:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="rateTypeItem" value="#{situsAdminMatrixBackingBean.headerSitusMatrix.ratetypeCode}"
						disabled="#{situsAdminMatrixBackingBean.isUpdateSitusMatrix  or situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
						valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderChanged}" onchange="submit();" 
						reRender="itemLevelTable,itemCountyTable,itemCityTable,itemStjTable,aMList,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn">
					<f:selectItems value="#{situsAdminMatrixBackingBean.headerRateTypeCodeItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>      
          
          <td style="width: 70px;">Method of Delivery:</td>
          <td style="width: 150px;">           	
				<h:selectOneMenu id="methodOfDeliveryItem" value="#{situsAdminMatrixBackingBean.headerSitusMatrix.methodDeliveryCode}"
						disabled="#{situsAdminMatrixBackingBean.isUpdateSitusMatrix  or situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
						valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderChanged}" onchange="submit();" 
						reRender="itemLevelTable,itemCountyTable,itemCityTable,itemStjTable,aMList,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn">
					<f:selectItems value="#{situsAdminMatrixBackingBean.headerMethodOfDeliveryItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td> 
         </tr>
         
         <tr>
			<th colspan="10">State</th>
		 </tr>     
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          
          <td style="width: 70px;">Country:</td>
          <td style="width: 120px;" class="column-input">
					<h:selectOneMenu id="countryMenuMain" value="#{situsAdminMatrixBackingBean.headerSitusMatrix.situsCountryCode}" immediate="true" 
							disabled="#{situsAdminMatrixBackingBean.isUpdateSitusMatrix or situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
							valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderCountryChanged}" onchange="submit();"
							reRender="itemLevelTable,itemCountyTable,itemCityTable,itemStjTable,aMList,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" >
						<f:selectItems value="#{situsAdminMatrixBackingBean.headerCountryMenuItems}"/>
					</h:selectOneMenu>
		  </td>
          <td>&#160;</td>  
          
          <td style="width: 70px;">State:</td>
          <td style="width: 120px;" class="column-input">
				<h:selectOneMenu id="stateMenuMain" value="#{situsAdminMatrixBackingBean.headerSitusMatrix.situsStateCode}" immediate="true" 
						disabled="#{situsAdminMatrixBackingBean.isUpdateSitusMatrix  or situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
						valueChangeListener="#{situsAdminMatrixBackingBean.searchHeaderStateChanged}" onchange="submit();" 
						reRender="itemLevelTable,itemCountyTable,itemCityTable,itemStjTable,aMList,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" >
					<f:selectItems value="#{situsAdminMatrixBackingBean.headerStateMenuItems}"/>
				</h:selectOneMenu>
		  </td>
          <td style="width: 70px;">&#160;</td>
          
          <td>&#160;</td>
          <td>&#160;</td>  
          <td style="width: 70px;">&#160;</td>
         </tr>
         
         <tr>
			<th colspan="10">Select Jurisdictions</th>
		 </tr>
         <tr>
			<td colspan="10">
				<table class="embedded-table" width="100%">
					<tr>
						<td style="width: 10px;border-bottom: none;">&#160;</td>
						<td style="border-bottom: none;"><b>Levels</b></td>
						<td style="border-bottom: none;"><b>Counties</b></td>
						<td style="border-bottom: none;"><b>Cities</b></td>
						<td style="border-bottom: none;"><b>STJs</b></td>
					</tr>
					<tr>
						<td style="width: 10px;border-bottom: none;">&#160;</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background-color: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemLevelTable" style="width: 100%;" styleClass="GridContent"
									disabled="#{situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
									value="#{situsAdminMatrixBackingBean.levelList}" var="itemLevel" ajaxKeys="#{situsAdminMatrixBackingBean.levelRowsToUpdate}">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemLevelTable', this);"
										actionListener="#{situsAdminMatrixBackingBean.selectedLevelChanged}"
										immediate="true"
										reRender="itemCountyTable,itemCityTable,itemStjTable,aMList,pageInfo,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
								
									<rich:column width="22px;" id="levelSelectColumn">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="levelSelect" value="${itemLevel.selected}"  disabled="${!itemLevel.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{situsAdminMatrixBackingBean.levelCheckboxSelected}"
												reRender="levelSelect,itemCountyTable,itemCityTable,itemStjTable"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Rules"/></f:facet>
										<h:selectBooleanCheckbox id="levelNexus" disabled="true" value="#{itemLevel.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="Level"/></f:facet>
										<h:outputText id="level" value="#{itemLevel.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background-color: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemCountyTable" style="width: 100%;" styleClass="GridContent"
									disabled="#{situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
									value="#{situsAdminMatrixBackingBean.countyList}" var="itemCounty">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemCountyTable', this);"
										actionListener="#{situsAdminMatrixBackingBean.selectedCountyChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCityTable,itemStjTable,aMList,pageInfo,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
								
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="countySelect" value="${itemCounty.selected}" disabled="${!itemCounty.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{situsAdminMatrixBackingBean.countyCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Rules"/></f:facet>
										<h:selectBooleanCheckbox id="countyNexus" disabled="true" value="#{itemCounty.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="County"/></f:facet>
										<h:outputText id="county" value="#{itemCounty.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemCityTable" style="width: 100%;" styleClass="GridContent"
									disabled="#{situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
									value="#{situsAdminMatrixBackingBean.cityList}" var="itemCity">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemCityTable', this);"
										actionListener="#{situsAdminMatrixBackingBean.selectedCityChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCountyTable,itemStjTable,aMList,pageInfo,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
										
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="citySelect" value="${itemCity.selected}" disabled="${!itemCity.enabled}">
											<a4j:support event="onclick"
												actionListener="#{situsAdminMatrixBackingBean.cityCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Rules"/></f:facet>
										<h:selectBooleanCheckbox id="cityNexus" disabled="true" value="#{itemCity.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="City"/></f:facet>
										<h:outputText id="city" value="#{itemCity.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemStjTable" style="width: 100%;" styleClass="GridContent"
									disabled="#{situsAdminMatrixBackingBean.isDeleteSitusMatrix}"
									value="#{situsAdminMatrixBackingBean.stjList}" var="itemSTJ">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemStjTable', this);"
										actionListener="#{situsAdminMatrixBackingBean.selectedStjChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCountyTable,itemCityTable,aMList,pageInfo,detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
										
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="stjSelect" value="${itemSTJ.selected}" disabled="${!itemSTJ.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{situsAdminMatrixBackingBean.stjCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Rules"/></f:facet>
										<h:selectBooleanCheckbox id="stjNexus" disabled="true" value="#{itemSTJ.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="STJ"/></f:facet>
										<h:outputText id="stj" value="#{itemSTJ.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<th colspan="10">Matrix Rules &#38; Details</th>
		</tr>
         
        </table>
       </div>
       <!-- table-one-content -->
       
       <!--  
       <div id="table-one-bottom">
         <ul class="right"></ul>
       </div>-->
       <!-- table-one-bottom -->
       
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
    
     <div id="bottom">
      <div id="table-four">
       <div id="table-four-top">
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
       </div>
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner">
          <rich:dataTable rowClasses="odd-row,even-row" id="aMList"
                          rows="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.pageSize }"
                          value="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel}"
                          var="matrix">
                          
           		<a4j:support id="ref2tbl" event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixRulesForm:aMList', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedMasterRowChanged}"
                        reRender="detailTable,addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
                        
          			<rich:column id="driver01" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_01']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver01" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver01']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_01']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver01]}"  />
					</rich:column>
					
					<rich:column id="driver02" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_02']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver02" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver02']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_02']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver02]}"  />
					</rich:column>
					
					<rich:column id="driver03" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_03']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver03" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver03']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_03']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver03]}"  />
					</rich:column>
					
					<rich:column id="driver04" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_04']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver04" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver04']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_04']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver04]}"  />
					</rich:column>
					
					<rich:column id="driver05" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_05']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver05" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver05']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_05']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver04]}"  />
					</rich:column>
					
					<rich:column id="driver06" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_06']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver06" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver06']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_06']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver04]}"  />
					</rich:column>
					
					<rich:column id="driver07" rendered="#{!empty situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_07']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver07" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['driver07']}"
								value="#{situsAdminMatrixBackingBean.situsMatrixNameMainMap['DRIVER_07']}"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList"/>
						</f:facet>
						<h:outputText value="#{situsAdminMatrixBackingBean.situsDriverTypeMap[matrix.driver04]}"  />
					</rich:column>
					
					<rich:column id="binaryWeight" >
						<f:facet name="header">
							<a4j:commandLink id="s_binaryWeight" 
								styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.sortOrder['binaryWeight']}"
								value="Binary Weight"
								actionListener="#{situsAdminMatrixBackingBean.sortRulersAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.binaryWeight}"  />
					</rich:column>
                        
            
         <!--      
            <rich:column id="driver01" rendered="#{!empty situsAdminMatrixBackingBean.matrixNameMap['DRIVER_01']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver01" 
						value="#{situsAdminMatrixBackingBean.matrixNameMap['DRIVER_01']}"
						actionListener="#{situsAdminMatrixBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver01}" />
			</rich:column>
			
			<rich:column id="driver02" rendered="#{!empty situsAdminMatrixBackingBean.matrixNameMap['DRIVER_02']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver02" 
						value="#{situsAdminMatrixBackingBean.matrixNameMap['DRIVER_02']}"
						actionListener="#{situsAdminMatrixBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver02}" />
			</rich:column>
           --> 
              
         <!--              
           	<rich:column id="transactionType" styleClass="column-left" >
            	<f:facet name="header">
             	<a4j:commandLink id="transactionType-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['transactionTypeCode']}"
                              value="Transaction Type"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="aMList" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.transactionTypeMap[situsMatrixMaintenance.transactionTypeCode]}"  />
           	</rich:column>
           	
           <rich:column id="ratetypeCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="ratetypeCode-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['ratetypeCode']}"
                              value="Rate Type"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="aMList" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.ratetypeMap[situsMatrixMaintenance.ratetypeCode]}"  />
           	</rich:column>

           	<rich:column id="methodDeliveryCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="methodDeliveryCode-a4j"
                              styleClass="sort-#{situsAdminMatrixBackingBean.situsMatrixDataModel.sortOrder['methodDeliveryCode']}"
                              value="Method of Delivery"
                              actionListener="#{situsAdminMatrixBackingBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="aMList" />
            	</f:facet>
            	<h:outputText value="#{situsAdminMatrixBackingBean.methodDeliveryMap[situsMatrixMaintenance.methodDeliveryCode]}"  />
           	</rich:column>
       -->       	
           	
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="aMList"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{situsAdminMatrixBackingBean.situsMatrixRulesDataModel.curPage}" />
                          
       
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable"
                          value="#{situsAdminMatrixBackingBean.situsMatrixRulersDetailList}"
                          var="situsMatrix">
                          
            <a4j:support id="dtltbl"
                        event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('situsMatrixRulesForm:detailTable', this);"
                        actionListener="#{situsAdminMatrixBackingBean.selectedDetailRowChanged}"
                        reRender="addBtn,copyAddBtn,updateBtn,copyUpdateBtn,deleteBtn,viewBtn,closeBtn" />
           
           	<rich:column id="matrixId" styleClass="column-left" sortBy="#{situsMatrix.situsMatrixId}" >
            	<f:facet name="header"><h:outputText id="matrixId-a4j" styleClass="headerText" value="Matrix ID"/></f:facet>
            	<h:outputText value="#{situsMatrix.situsMatrixId}" />
           	</rich:column>
           	

           	<rich:column id="effectiveDate" styleClass="column-left" sortBy="#{situsMatrix.effectiveDate}" >
            	<f:facet name="header"><h:outputText id="effectiveDate-a4j" styleClass="headerText" value="Effective Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.effectiveDate}" />
           	</rich:column>
           	
           	<rich:column id="expirationDate" styleClass="column-left" sortBy="#{situsMatrix.expirationDate}" >
            	<f:facet name="header"><h:outputText id="expirationDate-a4j" styleClass="headerText" value="Expiration Date"/></f:facet>
            	<h:outputText value="#{situsMatrix.expirationDate}" />
           	</rich:column>
           	
           	<rich:column id="primarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.primarySitusCode}" >
            	<f:facet name="header"><h:outputText id="primarySitusCode-a4j" styleClass="headerText" value="Primary Situs Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.primarySitusCode}" />
           	</rich:column>
           	
           	<rich:column id="primaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.primaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="primaryTaxtypeCode-a4j" styleClass="headerText" value="Primary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.primaryTaxtypeCode}" />
           	</rich:column>
           	
           	<rich:column id="secondarySitusCode" styleClass="column-left" sortBy="#{situsMatrix.secondarySitusCode}" >
            	<f:facet name="header"><h:outputText id="secondarySitusCode-a4j" styleClass="headerText" value="Secondary Situs Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.secondarySitusCode}" />
           	</rich:column>
           	
           	<rich:column id="secondaryTaxtypeCode" styleClass="column-left" sortBy="#{situsMatrix.secondaryTaxtypeCode}" >
            	<f:facet name="header"><h:outputText id="secondaryTaxtypeCode-a4j" styleClass="headerText" value="Secondary Taxtype Code"/></f:facet>
            	<h:outputText value="#{situsMatrix.secondaryTaxtypeCode}" />
           	</rich:column>
           	
           	
           	
           	<rich:column id="allLevelsFlag" styleClass="column-left" sortBy="#{situsMatrix.allLevelsFlag}" >
            	<f:facet name="header"><h:outputText id="allLevelsFlag-a4j" styleClass="headerText" value="All Levels"/></f:facet>
            	<h:outputText value="#{situsMatrix.allLevelsFlag}" />
           	</rich:column>
           	
      		<rich:column id="comments" styleClass="column-left" sortBy="#{situsMatrix.comments}" >
            	<f:facet name="header"><h:outputText id="comments-a4j" styleClass="headerText" value="Comments"/></f:facet>
            	<h:outputText value="#{situsMatrix.comments}" />
           	</rich:column>
           	
           	<rich:column id="activeFlag" styleClass="column-left" sortBy="#{situsMatrix.activeFlag}" >
            	<f:facet name="header"><h:outputText id="activeFlag-a4j" styleClass="headerText" value="Active"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.activeBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>
           	
           	<rich:column id="customFlag" styleClass="column-left" sortBy="#{situsMatrix.customFlag}" >
            	<f:facet name="header"><h:outputText id="customFlag-a4j" styleClass="headerText" value="Custom"/></f:facet>
            	<h:selectBooleanCheckbox value="#{situsMatrix.customBooleanFlag}" disabled="true" styleClass="check"  />
           	</rich:column>
           
           	<rich:column id="blankdetail" styleClass="column-left" style="width:50%" >
            	<f:facet name="header" ><h:outputText id="blankdetail-a4j" styleClass="headerText" value=""/></f:facet>
            	<h:outputText value="" />
           	</rich:column>
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
                          
                          
       <div id="table-four-bottom">
        <ul class="right">
			<li class="add"><h:commandLink id="addBtn" disabled="#{situsAdminMatrixBackingBean.disableAddButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="addMatrixRule" /></h:commandLink></li>
			<li class="copy-add"><h:commandLink id="copyAddBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="copyaddMatrixRule" /></h:commandLink></li>  
			<li class="update"><h:commandLink id="updateBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix or situsAdminMatrixBackingBean.isUserDisable}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="updateMatrixRule" /></h:commandLink></li>
			<li class="copy-update"><h:commandLink id="copyUpdateBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="copyupdateMatrixRule" /></h:commandLink></li>
			<li class="delete115"><h:commandLink id="deleteBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix or situsAdminMatrixBackingBean.isUserDisable}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="deleteMatrixRule" /></h:commandLink></li>
			<li class="view"><h:commandLink id="viewBtn" disabled="#{situsAdminMatrixBackingBean.disableButton or situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processDisplayCommand}" ><f:param name="command" value="viewMatrixRule" /></h:commandLink></li>
			<li class="ok"><h:commandLink id="okDeleteBtn" disabled="#{!situsAdminMatrixBackingBean.isDeleteSitusMatrix}" action="#{situsAdminMatrixBackingBean.processUpdateCommand}" /></li>			
			<li class="cancel2"><h:commandLink id="closeBtn" action="situs_admin_matrix_main" ></h:commandLink></li>
			
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
   </f:view>
   
  </ui:define>
 </ui:composition>
</html>
