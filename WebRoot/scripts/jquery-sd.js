jQuery.noConflict();
 (function($) { 
	 
	 $(function() {
		 $( "#searchCustAccordion" ).accordion({
			 heightStyle: "content",
			 collapsible: true
		 });
	 }); 
	 
	 $(function() {
		 $( "#searchLocnAccordion" ).accordion({
			 heightStyle: "content",
			 collapsible: true
		 });
	 });
 
	 $(function() {    
		 $( "#toggleCustLocnCollapse" ).button().click(function() {
			 $("#searchCustAccordion").accordion( "option", "active", false);
			 $("#searchLocnAccordion").accordion( "option", "active", false);
		 });
	 	   
		 $( "#toggleCustLocnExpand" ).button().click(function() {    
			 $("#searchCustAccordion").accordion( "option", "active", 0);
			 $("#searchLocnAccordion").accordion( "option", "active", 0);
		 });
	 });
	 
	 $(function() {
		 $( "#limitedinformationaccordion" ).accordion({
			 heightStyle: "content",
			 collapsible: true
		 });
	 }); 	    	 
 
	 $(function() {
		 $( "#basicinformationaccordion" ).accordion({
			 heightStyle: "content",
			 collapsible: true   	 
		 });
    
	 });
 	
 	$(function() {
 	    $( "#customeraccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});

 	$(function() {
 	    $( "#invoiceaccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});

 	$(function() {
 	    $( "#locationsaccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});

 	$(function() {
 	    $( "#taxamountsaccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});

 	$(function() {
 	    $( "#transactionaccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});

 	$(function() {
 	    $( "#userfieldsaccordion" ).accordion({
 	        heightStyle: "content",
 	        collapsible: true
 	    });
 	});
 	
 })(jQuery);