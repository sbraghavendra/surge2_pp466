// NewCo STS v2.0 Action Button Scripts 
//  Created: Friday January 18th 2008 //  Author: Rob Rosen//  Copyright (c) 2007/2008 NewCo 
//variables
var COLOR1 = {border:"#666666", shadow:"#DBD8D1", bgON:"white", bgOVER:"#DADADA", imagebg:"#DBD8D1", oimagebg:"#B6BDD2"};
var CSS1 = {ON:'clsCMOn', OVER:'clsCMOver'};
var STYLE1 = {border:1, shadow:2, color:COLOR1, css:CSS1};
//menu items
var MENU_ITEMS = [
  {pos:[20, 20], size:[25, 100], itemoff:[0, 99], leveloff:[0, 0], delay:600, imgsize:[16, 16], arrsize:[16, 16], style:STYLE1}, 
    {code:"", 
      sub:[
      {size:[22, 139], itemoff:[21, 0], leveloff:[24, 0], delay:600, imgsize:[16, 16], arrsize:[16, 16], style:{border:1, shadow:2, color:{border:"#666666", shadow:"#DBD8D1", bgON:"white", bgOVER:"#B6BDD2", imagebg:"#DBD8D1", oimagebg:"#B6BDD2"}, css:{ON:'clsCMOn', OVER:'clsCMOver'}}}, 
        {code:"Taxability Matrix"},
        {code:"Location Matrix"},
        {code:"Allocation Matrix"},
        {code:"Taxability Codes"},
        {code:"Tax Rates"}
      ]}
];

function findMenuItem(n, d) {     var p, i, x;      if(!d) d = document;     if((p = n.indexOf("?")) > 0 && parent.frames.length)     {d = parent.frames[n.substring(p + 1)].document;     n=n.substring(0,p);}    if(!(x = d[n]) && d.all) x = d.all[n];     for (i=0;!x && i < d.forms.length; i++) x = d.forms[i][n];    for(i = 0;!x && d.layers && i < d.layers.length; i++)     x = findMenuItem(n,d.layers[i].document);    if(!x && d.getElementById) x = d.getElementById(n);     return x;}function procMenu(event) {
    var o = document.getElementById("maintenancedropdown");
     
    if (event == "over") {
        o.style.display="block";
    } else  {
        o.style.display="none";
    } 
}function procMenuChoice(event, id) {
    var o = document.getElementById(id);
     
    if (event == "over") {
        o.style.backgroundColor="#d1dcee";
    } else if (event == "out") {
        o.style.backgroundColor="#ffffff";
    } 
}function procMenuItem(event, grpName) {      var i, img,nbArr,args=procMenuItem.arguments;          if (event == "init" && args.length > 2) {        if ((img = findMenuItem(args[2])) != null && !img.MM_init) {            img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;                        if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();                nbArr[nbArr.length] = img;                            for (i = 4; i < args.length - 1; i += 2) if ((img = findMenuItem(args[i])) != null) {                if (!img.MM_up) img.MM_up = img.src;                    img.src = img.MM_dn = args[i + 1];                    nbArr[nbArr.length] = img;            }         }      } else if (event == "over") {        document.MM_nbOver = nbArr = new Array();                for (i = 1; i < args.length - 1; i += 3) if ((img = findMenuItem(args[i])) != null) {            if (!img.MM_up) img.MM_up = img.src;            img.src = (img.MM_dn && args[i + 2]) ? args[i + 2] : ((args[i + 1])? args[i + 1] : img.MM_up);            nbArr[nbArr.length] = img;        }      } else if (event == "out" ) {        for (i = 0; i < document.MM_nbOver.length; i++)             { img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }      } else if (event == "down") {        nbArr = document[grpName];        if (nbArr)             for (i = 0; i < nbArr.length; i++) { img = nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }                document[grpName] = nbArr = new Array();        for (i = 2; i < args.length-1; i += 2)             if ((img = findMenuItem(args[i])) != null) {                if (!img.MM_up) img.MM_up = img.src;                img.src = img.MM_dn = (args[i +1 ]) ? args[i +1 ] : img.MM_up;                nbArr[nbArr.length] = img;            }               }}




