function formatNumber(num, minFractionDigits) {
	num += '';
	x = num.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? x[1] : '';
	x2 = rightPad(x2, '0', 2)
	
	return x1 + "." + x2;
}

function rightPad(s, c, n) {
    if (s == null || ! c || s.length >= n) {
        return s;
    }

    var max = (n - s.length)/c.length;
    for (var i = 0; i < max; i++) {
        s += c;
    }

    return s;
}

function clickObject(objId) {
	var obj = document.getElementById(objId);
	if (obj != null) { 
		if (obj.onclick) {
			obj.onclick();
		} else if (obj.click) {
			obj.click();
		}
	}
}

function submitEnter(e, objId) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	if (keycode == 13) {
		clickObject(objId);
		return false;
	}
	return true;
}

function turnOff(id, turnOff) { 
	var elem = document.getElementById(id);
	if (elem != null) {
		if (turnOff == true) { 
			elem.style.visibility = "hidden";
		} else { 
			elem.style.visibility = "visible";
		}
	} else {
// 		alert("ID not found: " + id);
	}
}

function toggleDisabled(id, disabledFlag) { 
	var elem = document.getElementById(id);
	if (elem != null) {
		elem.disabled = disabledFlag;
	} else {
		alert("ID not found: " + id);
	}
}


function getRightTop(ref) {
	var position = new Object();
	position.top = 0; //ref.offsetTop;
	position.left =0; // ref.offsetLeft+ref.clientWidth+6;
	return position;
}

function onlyDateValue(evt)
{
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if( charCode == 47) {	  
	  return true;
	}
	
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}

function onlyPercentage(evt)
{
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode == 37) {	  
	  // add code not have mutiple percentage 
	  return true;
	}
	
	return onlyNumerics(evt);
}

function onlyNumerics(evt)
{
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	//Allow . -
	if( (charCode == 46) || (charCode == 45)) {	  
	  // add code not have mutiple decimal points 
	  return true;
	}
	
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}

function onlyIntegerValue(evt)
{
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;

}

// Useful functions for converting text input box text to upper/lower case.
// Use as an onkeyup event handler.
function upperCaseInputText(x) {
	x.value = x.value.toUpperCase();
}

function upperCaseInputTextKey(e, x) {
	var keycode;
	if (window.event) {
		keycode = window.event.keyCode;
		if ((keycode >= 97 && keycode <= 122) || (keycode >= 65 && keycode <= 90)){
			event.keyCode = String.fromCharCode(keycode).toUpperCase().charCodeAt(); 
		}			
	}
	else if (e) {
		keycode = e.which;
		if ((keycode >= 97 && keycode <= 122) || (keycode >= 65 && keycode <= 90)){
			event.keyCode = String.fromCharCode(keycode).toUpperCase().charCodeAt(); 
		} 	
	}
}

function lowerCaseInputText(x) {
	x.value = x.value.toLowerCase();
}

// Row highlight functionality, used in onclick handler
// tableId is name of table to enable, for example myForm:myTable
var oldRow = new Array();
var oldColor = new Array();
function selectRow(tableId, row) {
	if (oldRow[tableId] != undefined) {
		//alert(row.idx.value);
		
		//oldRow[tableId].style.backgroundColor = '#ffffff';
		
		oldRow[tableId].style.backgroundColor = oldColor[tableId];
	}
	
	oldColor[tableId] = row.style.backgroundColor;
	
	row.style.backgroundColor = '#91B4D2';
	oldRow[tableId] = row;
}

function selectRowByIndex(tableId, rowIndex) {
	var tbl = document.getElementById(tableId);
	var idx = document.getElementById(rowIndex);
	if (tbl && idx && (idx.value >= 0)) {
    	var row = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[idx.value];
    	if (row) {
    		selectRow(tableId, row);
    	}
	}
}

var _extendSelectionAnchor = -1;
function extendSelection(evt) {
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var targ = (e.target)? e.target:e.srcElement;

	var row = targ.parentNode.parentNode.rowIndex;
	//alert("now"+targ.checked);
	if(!targ.checked){ //aargh trying to handle 3988 weirdness - basically if thing has just been unchecked (i.e. is now blank) that resets things
		_extendSelectionAnchor = -1;
		return;
	}
		
	if ((_extendSelectionAnchor != -1) && e.shiftKey) {
		//alert("ok checking from row "+row+" to anchors "+_extendSelectionAnchor);
		var start = Math.min(row,_extendSelectionAnchor);
	    var end = Math.max(row,_extendSelectionAnchor);
		
	    
	    var rows = targ.parentNode.parentNode.parentNode.getElementsByTagName('tr');
		
		//go for all elements between this thing being clicked and what was last clicked...
		for (var i = start; i <= end; i++) {
		  var checkbox = rows[i-1].getElementsByTagName('input')[0];
		  if(i != _extendSelectionAnchor) {  //3988 - leave the anchor alone
			  if (checkbox){ 
				  if(checkbox.checked == false){
					  checkbox.click(); //need to click to get onClick, not just change value!
				  } 
			  }
		  }
		}
	}
	_extendSelectionAnchor = row;
}

function resetExtendSelection(){
	_extendSelectionAnchor = -1; //3988 - reset on page change (called in transactiondetail_table.xhtml)
	//ideally if we could figure out how to read the current page, we could make _extendSelectionAnchor
	//an array or something, and keep track of the click for each page. Which isn't entirely great UI,
	//but might be more consistent with what we're basing this on
}

function registerEvent(obj, evt, func) {
	if (obj) {
		if (obj.attachEvent) obj.attachEvent("on"+evt, func);
		else if (obj.addEventListener) obj.addEventListener(evt, func, false);
	}
}

function unregisterEvent(obj, evt, func) {
	if (obj) {
		if (obj.detachEvent) obj.detachEvent("on"+evt, func);
		else if (obj.removeEventListener) obj.removeEventListener(evt, func, false);
	}
}

// OLD - should use selectRow now!
function highlightAndSelectRow(tableId, event) {
	
    var obj = document.getElementById(tableId);
    if (obj != null) {
		event = event || window.event; // IE doesn't pass event as argument.
		var tr = event.target || event.srcElement; // IE doesn't use .target
    	if ((tr != null) && (tr.tagName.toUpperCase() == 'TD')) {
    		tr = tr.parentNode;
    	} 
    	var trs = obj.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
	    for (var i = 0; i < trs.length; i++) {
	        if (trs[i] == tr) {
	        	// if (trs[i].className.indexOf(" selected") == -1)
	            // trs[i].className = trs[i].className + " selected";
	            trs[i].bgColor = '#ECF4FE';
	        } else {
	            // trs[i].className = trs[i].className.replace(" selected", "");
	            trs[i].bgColor = '#ffffff';
	        }
	    }
    }
}

function getClientHeight() {
	if (typeof window.innerHeight != 'undefined') 
		return window.innerHeight; 
 
	// IE6 in standards compliant mode
 	if (typeof document.documentElement != 'undefined' && 
     	typeof document.documentElement.clientHeight != 'undefined' && 
     	document.documentElement.clientHeight != 0)
     	return document.documentElement.clientHeight;
 
 	// older versions of IE
    return document.getElementsByTagName('body')[0].clientHeight;
}

function getOffsetTop(elm) {
	y = elm.offsetTop;
    elm = elm.offsetParent;
    while(elm != null) {
        y += elm.offsetTop;
        elm = elm.offsetParent;
	}

    return y;
}

function setHeight(id, h, offset) {
    var e = document.getElementById(id);
    if (e != undefined) { 
       var clientHeight = getClientHeight();
       var offsetTop = getOffsetTop(e);
       var targetHeight = clientHeight - offsetTop - offset;
        if ((h > 0) && (targetHeight < h)) {
            targetHeight = h;
       }
       e.style.height = targetHeight + "px";
    }
}

function adjustHeight() { 
    setHeight("resize", 230, 80);
    setHeight("resize1", 230, 80);
} 

registerEvent(window, "resize", adjustHeight)
registerEvent(window, "resize1", adjustHeight)
registerEvent(window, "resize1", adjustWidth)
registerEvent(window, "load", adjustHeight)  


function getClientWidth() {
	if (typeof window.innerWidth != 'undefined') 
		return window.innerWidth; 
 
	// IE6 in standards compliant mode
 	if (typeof document.documentElement != 'undefined' && 
     	typeof document.documentElement.clientWidth != 'undefined' && 
     	document.documentElement.clientWidth != 0)
     	return document.documentElement.clientWidth;
 
 	// older versions of IE
    return document.getElementsByTagName('body')[0].clientWidth;
}

function getOffsetLeft(elm) {
	y = elm.offsetLeft;
    elm = elm.offsetParent;
    while(elm != null) {
        y += elm.offsetLeft;
        elm = elm.offsetParent;
	}

    return y;
}

function setWidth(id, h, offset) {
    var e = document.getElementById(id);
    if (e != undefined) { 
       var clientWidth = getClientWidth();
       var offsetLeft = getOffsetLeft(e);
       var targetWidth = clientWidth - offsetLeft - offset;
        if ((h > 0) && (targetWidth < h)) {
            targetWidth = h;
       }
       e.style.Width = targetWidth + "px";
    }
}

function adjustWidth() { 
    setWidth("resize1", 230, 80);
} 
