function tableruler() {
  if (document.getElementById && document.createTextNode) {
    var tables=document.getElementsByTagName('table');
    for (var i=0;i<tables.length;i++)
    {
      if(tables[i].className=='ruler') {
        var trs=tables[i].getElementsByTagName('tr');
        for(var j=0;j<trs.length;j++)
        {
          if(trs[j].parentNode.nodeName=='TBODY') {
            trs[j].onmouseover=function(){this.className='ruled';return false}
            trs[j].onmouseout=function(){this.className='';return false}
          }
        }
      }
      if(tables[i].className=='panel-ruler') {
        var trs=tables[i].getElementsByTagName('tr');
        for(var j=0;j<trs.length;j++)
        {
            trs[j].onmouseover=function(){this.className='ruled';return false}
            trs[j].onmouseout=function(){this.className='';return false}
        }
      }
    }
  }
}
//window.onload=function(){tableruler();}


sfHover = function() {
    var nav = document.getElementById("nav");
    if (nav) {
		var sfEls = nav.getElementsByTagName("LI");
		for (var i=0; i<sfEls.length; i++) {
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
				this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

/*
CSS Browser Selector v0.2.7
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors
*/
var css_browser_selector = function() {var ua=navigator.userAgent.toLowerCase(),is=function(t){return ua.indexOf(t) != -1;},h=document.getElementsByTagName('html')[0],b=(!(/opera|webtv/i.test(ua))&&/msie (\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?'gecko ff2':is('firefox/3')?'gecko ff3':is('gecko/')?'gecko':is('opera/9')?'opera opera9':/opera (\d)/.test(ua)?'opera opera'+RegExp.$1:is('konqueror')?'konqueror':is('applewebkit/')?'webkit safari':is('mozilla/')?'gecko':'',os=(is('x11')||is('linux'))?' linux':is('mac')?' mac':is('win')?' win':'';var c=b+os+' js'; h.className += h.className?' '+c:c;}();

/* 
Suppoer for scrolling tables with fixed headers 
IE ONLY!
*/
function addScrollSync(fromElement, toElement) {
   removeScrollSync(fromElement);

   fromElement._syncScroll = getOnScrollFunction(fromElement);
   fromElement._syncTo = toElement;
   toElement.attachEvent("onscroll", fromElement._syncScroll);
}

function getOnScrollFunction(oElement) {
   return function () {
      	oElement.style.left = -event.srcElement.scrollLeft;
   };
}

function removeScrollSync(fromElement) {
   if (fromElement._syncTo != null)
      fromElement._syncTo.detachEvent("onscroll", fromElement._syncScroll);

   fromElement._syncTo = null;;
   fromElement._syncScroll = null;
}

function initScrollingTables() {
	/* IE7 ONLY! */
	if (navigator.appVersion.indexOf("MSIE 7.") != -1) { 
	  if (document.getElementById && document.createTextNode) {
	    var divs=document.getElementsByTagName('div');
	    for (var i=0;i<divs.length;i++) {
	      if(divs[i].className == 'scrollInner') {
	      	var th = divs[i].getElementsByTagName('thead')[0];
	      	if(th) {
				var tr = th.getElementsByTagName('tr')[0];
				if(tr) {
					addScrollSync(tr, divs[i]);
				}
			}
	      }
	    }
	  }
	}
}

if (window.attachEvent) window.attachEvent("onload", initScrollingTables);
