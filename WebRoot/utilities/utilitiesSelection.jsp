<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/oneTable.xhtml">

<ui:define name="header">
	<h:graphicImage id="imgUtilitySelection" value="/images/headers/hdr-utilities.gif"/>
</ui:define>

<ui:define name="table-one">
<h:form>
	<div id="table-one-top"></div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler" border="0">
		<thead>
			<tr>
				<td colspan="3">Utilities</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Run Master Control Program"
						onclick="javascript:Richfaces.showModalPanel('runMasterControlProgram',{width:450, top:200})"
						id="runMasterControlProgramLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Reset DataWindow Syntax"
						onclick="javascript:Richfaces.showModalPanel('resetDataWindowSyntax',{width:450, top:200})"
						id="resetDataWindowSyntaxLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Build Indices"
						onclick="javascript:Richfaces.showModalPanel('buildIndices',{width:450, top:200})"
						id="buildIndicesLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Reweight Matrices"
						onclick="javascript:Richfaces.showModalPanel('reweightMatrices',{width:450, top:200})"
						id="reweightMatricesLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Reset All Sequences"
						onclick="javascript:Richfaces.showModalPanel('resetAllSequences',{width:450, top:200})"
						id="resetAllSequencesLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Rebuild Driver References"
						onclick="javascript:Richfaces.showModalPanel('rebuildDriverReferences',{width:450, top:200})"
						id="rebuildDriverReferencesLabel"/>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>
					<h:outputLabel value="Truncate BCP Tables"
						onclick="javascript:Richfaces.showModalPanel('truncateBCPTables',{width:450, top:200})"
						id="truncateBCPTablesLabel"/>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<div id="table-one-bottom">
		<ul class="right">
			<!-- Button goes here -->
		</ul>
	</div>
</h:form>
</ui:define>

<ui:define name="extra">

<rich:modalPanel id="runMasterControlProgram" minHeight="200" minWidth="450" height="250" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="runMasterControlProgramForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Run Master Control Program?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td colspan="3" height="30" width="350" style="text-align:left;"><h:outputText styleClass="headerText" style="text-align:left;" value="Note: This procedure will process the next batch real-time. This computer will be locked. Check the batch status from another computer."/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('runMasterControlProgramForm:cancelButton').disabled=true" 
							action="#{utilitiesMenuBean.runMasterControlProgramAction}" reRender="runMasterControlProgramForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('runMasterControlProgram')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="runMasterControlProgramForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('runMasterControlProgram')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="runMasterControlProgramForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="resetDataWindowSyntax" minHeight="200" minWidth="450" height="200" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="resetDataWindowSyntaxForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Reset DataWindow Syntax?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('resetDataWindowSyntaxForm:cancelButton').disabled=true"  
							action="#{utilitiesMenuBean.resetDataWindowSyntaxAction}" reRender="resetDataWindowSyntaxForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('resetDataWindowSyntax')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="resetDataWindowSyntaxForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('resetDataWindowSyntax')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="resetDataWindowSyntaxForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="buildIndices" minHeight="200" minWidth="450" height="270" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="buildIndicesForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Build/Rebuild Transaction Indices?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td colspan="3" width="350" style="text-align:center;">
					<table id="checkTable" cellpadding="0" cellspacing="0" title="" width="350" style="text-align:center;">
						<tbody>
							<tr><td width="50">&#160;</td><td colspan="5" style="text-align:left;">Tables:</td></tr>
							<tr><td colspan="6">&#160;</td></tr>
							<tr>
								<td width="50">&#160;</td>
								<td style="text-align:left;"><h:selectBooleanCheckbox id="transactionDetailCheck"  value="#{utilitiesMenuBean.transactionDetailCheck}"></h:selectBooleanCheckbox>&#160;&#160;</td>
								<td style="text-align:left;">Transaction Detail&#160;&#160;&#160;&#160;&#160;&#160;&#160;</td>
								<td style="text-align:right;" width="100"><h:selectBooleanCheckbox id="taxMatrixCheck"  value="#{utilitiesMenuBean.taxMatrixCheck}"></h:selectBooleanCheckbox>&#160;&#160;</td> 
								<td style="text-align:left;">Tax Matrix</td>
								<td width="50">&#160;</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('buildIndicesForm:cancelButton').disabled=true" 
							action="#{utilitiesMenuBean.buildIndicesAction}" reRender="buildIndicesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('buildIndices')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="buildIndicesForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('buildIndices')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="buildIndicesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="reweightMatrices" minHeight="200" minWidth="450" height="200" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="reweightMatricesForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Reweight Tax and Location Matrices?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('reweightMatricesForm:cancelButton').disabled=true"  
							action="#{utilitiesMenuBean.reweightMatricesAction}" reRender="reweightMatricesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('reweightMatrices')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="reweightMatricesForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('reweightMatrices')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="reweightMatricesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="resetAllSequences" minHeight="200" minWidth="450" height="200" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="resetAllSequencesForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Reset All Sequences?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('resetAllSequencesForm:cancelButton').disabled=true" 
							action="#{utilitiesMenuBean.resetAllSequencesAction}" reRender="resetAllSequencesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('resetAllSequences')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="resetAllSequencesForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('resetAllSequences')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="resetAllSequencesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="rebuildDriverReferences" minHeight="200" minWidth="450" height="200" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="rebuildDriverReferencesForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Clear and Rebuild Driver References?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('rebuildDriverReferencesForm:cancelButton').disabled=true" 
							action="#{utilitiesMenuBean.rebuildDriverReferencesAction}" reRender="rebuildDriverReferencesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('rebuildDriverReferences')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="rebuildDriverReferencesForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('rebuildDriverReferences')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="rebuildDriverReferencesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="truncateBCPTables" minHeight="200" minWidth="450" height="200" width="500" zindex="2000">
	<f:facet name="header">
		<h:outputText value="" />
	</f:facet>
	<h:form id="truncateBCPTablesForm">
		<table id="buttonTable" cellpadding="0" cellspacing="0" title="">
			<tbody>
				<tr>
					<td colspan="3"><h:outputText styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Truncate BCP Tables?"/></td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<tr>
					<td style="text-align:right;"><a4j:commandButton alt="" id="okButton" style="cursor:pointer" value="&#160;&#160;&#160;OK&#160;&#160;&#160;" height="10" width="25" 
							onclick="this.disabled=true; document.getElementById('truncateBCPTablesForm:cancelButton').disabled=true" 
							action="#{utilitiesMenuBean.truncateBCPTablesAction}" reRender="truncateBCPTablesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:center;"><a4j:commandButton alt="" id="closeButton" style="cursor:pointer" value="&#160;&#160;Close&#160;&#160;" onclick="Richfaces.hideModalPanel('truncateBCPTables')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="truncateBCPTablesForm" rendered="#{utilitiesMenuBean.isStart}"/>
					</td>
					<td style="text-align:left;">
						<a4j:commandButton alt="" id="cancelButton" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('truncateBCPTables')" 
							action="#{utilitiesMenuBean.cancelDisplayAction}" reRender="truncateBCPTablesForm" rendered="#{!utilitiesMenuBean.isStart}"/>
					</td>
				</tr>
				<tr><td colspan="3">&#160;</td></tr><tr><td colspan="3">&#160;</td></tr>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3"><h:outputText id="isDoneId" styleClass="headerText" style="text-align:center; color:red;font-size:large;" value="Done" /></td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{utilitiesMenuBean.isStart and !empty utilitiesMenuBean.returnMessage}">
				<tr>
					<td colspan="3" height="50" width="350" style="text-align:left;"><h:outputText id="returnMessageId" styleClass="headerText" style="text-align:left; color:red;font-size:small;" value="#{utilitiesMenuBean.returnMessage}" /></td>
				</tr>
				</h:panelGroup>
			</tbody>
		</table>
	</h:form>
</rich:modalPanel>

</ui:define>
</ui:composition>
</html>