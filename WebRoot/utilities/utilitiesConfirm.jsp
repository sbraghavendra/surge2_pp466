<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", adjustHeight);
//]]>
function openloading() {
    document.getElementById('loading').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
}

function closeloading() {
document.getElementById('loading').style.display = 'none';
document.getElementById('fade').style.display = 'none';
}
    
function loadAjax() {
	
openloading();
var xhr = false;
if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
}
else {
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
if (xhr) {
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 &amp;&amp; xhr.status == 200) {
            closeloading();
        }
    }
    xhr.open("GET", "/utilities/utilitiesConfirm.jsp", true);
    xhr.send(null);
}
}
</script>
<style type="text/css">
 #table-one-content td {
	padding: 2px;
	border-top: none;
	border-bottom: 0px solid #fff;
	border-left: none;
	border-right: none;
	}
.disableradiobuildindixestext {
    color : #C0C0C0;
}
</style>
</ui:define>

<ui:define name="body">
<f:view>
<h:form  id="utilitiesConfirm">
<a4j:queue/>

<div id="container">
<div id="fade"></div>
<div id="loading">
       <h:graphicImage id="loadingimg" alt="Loading.." value="/images/loading-img.gif"/>
</div>
<h1><h:graphicImage id="imgUtilities" alt="Utilities" value="/images/headers/hdr-utilities.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
			<tr>
	
			<td style="width:20px;">&#160;	</td>			
			<td align="left">
	  			<a4j:status id="pageInfo" startStyle="color:red;" stopText="" 
	  				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" >
     				
					<f:facet name="start">
							<h:panelGroup style="text-align:center;">
								<h:outputText value="Request being processed...&#160;&#160;" style="vertical-align:text-top;" />
								<h:graphicImage value="/images/headers/ajax-loader.gif" alt="ai" />
							</h:panelGroup>   
				    </f:facet>				
				</a4j:status>   
				
				<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" /> 						
			</td>
			</tr>
		</table>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" >
		<thead>
			<tr>
				<td colspan="10">
					<h:outputText id="RunMasterControlProgram" value="Confirm Run Master Control Program" rendered="#{utilitiesMenuBean.isRunMasterControlProgram}" />&#160;
					<h:outputText id="FixBrokenJob" value="Is the Bus running?" rendered="#{utilitiesMenuBean.isFixBrokenJob}" />&#160;
					<h:outputText id="ResetDataWindowSyntax" value="Confirm Reset Data Table Grid Columns" rendered="#{utilitiesMenuBean.isResetDataWindowSyntax}" />&#160;
					<h:outputText id="BuildIndices" value="Confirm Build/Rebuild Indices" rendered="#{utilitiesMenuBean.isBuildIndices}" />&#160;
					<h:outputText id="ReweightMatrices" value="Confirm Reweight All Matrices" rendered="#{utilitiesMenuBean.isReweightMatrices}" />&#160;
					<h:outputText id="ResetAllSequences" value="Confirm Reset All Sequences" rendered="#{utilitiesMenuBean.isResetAllSequences}" />&#160;
					<h:outputText id="RebuildDriverReferences" value="Confirm Clear and Rebuild Driver References" rendered="#{utilitiesMenuBean.isRebuildDriverReferences}" />&#160;
					<h:outputText id="TruncateBCPTables" value="Confirm Truncate Staging Tables" rendered="#{utilitiesMenuBean.isTruncateBCPTables}" />&#160;
					<h:outputText id="RunBatchProcess" value="Confirm Run Batch Process" rendered="#{utilitiesMenuBean.isRunBatchProcess}" />&#160;
				</td>
			</tr>
		</thead>
		<tbody>
		  <h:panelGroup rendered="#{utilitiesMenuBean.isBuildIndices}">
				<td style="width:50px;">&#160;</td>
				<td align="center">
					<h:outputText id="lengthyMessageLine"  value="Depending on the number of drivers and volume of Transactions, this can be a lengthy process" style = "font-size:11px;color:red" />&#160;
				</td>
				<td>&#160;</td>
			</h:panelGroup>
			<c:if test="#{!utilitiesMenuBean.isFixBrokenJob}">	
			 <h:panelGroup rendered="#{utilitiesMenuBean.isRunMasterControlProgram}"  >
				<td style="width:50px;">&#160;</td>
				<td colspan="8">
					<h:outputText id="RunMasterControlProgramMessageLine1" value="Note: This procedure will process the next batch real-time.  This computer will be locked." rendered="#{utilitiesMenuBean.isRunMasterControlProgram}" />&#160;
				</td>
				<td>&#160;</td>
			 </h:panelGroup>	
			  <tr>
			   <td colspan = "2">
				 <h:selectOneRadio id="buildindexesradio" rendered="#{utilitiesMenuBean.isBuildIndices}" disabledClass ="disableradiobuildindixestext" layout="pageDirection" value= "#{utilitiesMenuBean.buildindexType}"
			                    valueChangeListener="#{utilitiesMenuBean.updateBuildSpecificIndexes}">
			 	   <f:selectItem itemValue="buildallindexes" itemLabel= "Build All Indexes "   itemDisabled = "#{!utilitiesMenuBean.isBuildspecificIndexType}" /> 
			 	   <f:selectItem itemValue="buildspecificindexes" itemLabel="Build Specific Indexes:"   />
			 		<a4j:support id = "buildindsupportId" event="onclick" ajaxSingle="true" reRender="buildspecificindexradios,btnOk" status="none" />       
				 </h:selectOneRadio>	
    		   </td>
    		  </tr>
    		
    		  <tr>
    		  <td style="width:32px;">&#160;</td>
			   <td>
				<h:selectOneRadio id="buildspecificindexradios" rendered="#{utilitiesMenuBean.isBuildIndices}" disabledClass ="disableradiobuildindixestext"  layout="pageDirection" value= "#{utilitiesMenuBean.buildSpecIndexType}"
			                   disabled = "#{utilitiesMenuBean.isBuildspecificIndexType}"  valueChangeListener="#{utilitiesMenuBean.updateBuildSpecificIndexes}">
			 	 <f:selectItem itemValue="buildmatrixdriverindexall" itemLabel= "Build Matrix Drvier Indexes (All)" /> 
			 	 <f:selectItem itemValue="buildmatrixdriverindexesloconly" itemLabel= "Build Matrix Drvier Indexes (Location Only)"/> 
			 	 <f:selectItem itemValue="buildmatrixdriverindexesgands" itemLabel= "Build Matrix Drvier Indexes  (Goods and Svc Only)"/> 
			 	 <f:selectItem itemValue="buildtransactiondetailsindexall" itemLabel= "Build Transaction Detail Indexes (All)"/> 
			 	 <f:selectItem itemValue="buildtransactiondetailsindexdriversonly" itemLabel= "Build Transaction Detail Indexes (Drivers Only)"/> 
			 		<a4j:support id = "buildspeindsupportId" event="onclick" ajaxSingle="true" reRender="buildspecificindexradios,btnOk" status="none" />       
				</h:selectOneRadio>	

    		   </td>
    		  </tr>
    		 <h:panelGroup rendered = "#{!utilitiesMenuBean.isBuildIndices}" >
				<td style="width:50px;">&#160;</td>
				<td colspan="4" align="left">Click Ok to confirm.</td>
				<td colspan="5">&#160;</td>
			</h:panelGroup>
			</c:if>
			
			<c:if test="#{utilitiesMenuBean.isFixBrokenJob}">		
			<tr>
				<td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="DBMS Job:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="dbmsjobid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busPropertyMap['JOB']:'undefined'}" /></td>
		        <td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="Call:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="whatid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busPropertyMap['WHAT']:'&#160;'}" /></td>
		        <td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="Status:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="brokenid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busStatus:'&#160;'}" /></td>
		        <td>&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="Next Date:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="nextdateid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busPropertyMap['NEXT_DATE']:'&#160;'}" /></td>
		        <td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="Next Second:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="nextsecondid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busPropertyMap['NEXT_SEC']:'&#160;'}" /></td>
		        <td style="width:50px;">&#160;</td>
		        <td style="width:100px;"><h:outputText value="Interval:" style="font-weight:bold;" />&#160;</td>
		        <td style="width:100px;"><h:outputText id="intervalid" value="#{(!utilitiesMenuBean.isAddBus) ? utilitiesMenuBean.busPropertyMap['INTERVAL']:'&#160;'}" /></td>
		        <td>&#160;</td>
			</tr>
			</c:if>
		</tbody>
	</table>
    <h:outputLabel id="message" style="color:red" value="#{utilitiesMenuBean.returnMessage}"/>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><a4j:commandLink id="btnOk" action="#{utilitiesMenuBean.runUtilityAction}"   rendered ="#{!utilitiesMenuBean.isStart and !utilitiesMenuBean.isFixBrokenJob}" 
									  disabled="#{utilitiesMenuBean.currentUser.viewOnlyBooleanFlag}" reRender="utilitiesConfirm"
									  style="#{(utilitiesMenuBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>	
		<li class="refresh"><h:commandLink id="btnRefresh" action="#{utilitiesMenuBean.refreshBusAction}" rendered="#{utilitiesMenuBean.isFixBrokenJob}" /></li>
		<li class="resumeservice"><h:commandLink id="resumeserviceButton" action="#{utilitiesMenuBean.resumeBusAction}" rendered="#{utilitiesMenuBean.isBusSuspended}"
												 disabled="#{utilitiesMenuBean.currentUser.viewOnlyBooleanFlag}"
												 style="#{(utilitiesMenuBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
												  /></li>
		<li class="suspendservice"><h:commandLink id="suspendserviceButton" action="#{utilitiesMenuBean.suspendBusAction}" rendered="#{utilitiesMenuBean.isBusRunning}" 
												  disabled="#{utilitiesMenuBean.currentUser.viewOnlyBooleanFlag}"
												  style="#{(utilitiesMenuBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>			
		<li class="add2"><h:commandLink id="addserviceButton" action="#{utilitiesMenuBean.createBusAction}" rendered="#{utilitiesMenuBean.isAddBus}" 
										disabled="#{utilitiesMenuBean.currentUser.viewOnlyBooleanFlag}"
										style="#{(utilitiesMenuBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" action="#{utilitiesMenuBean.cancelUtilityAction}" rendered="#{!utilitiesMenuBean.isStart}" /></li>
		<li class="close2"><h:commandLink id="btnClose" action="#{utilitiesMenuBean.cancelUtilityAction}" rendered="#{utilitiesMenuBean.isStart}" /></li>
	</ul>
	</div>
	</div>
</div>

<div class="scrollInner" id="resize"></div>

</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>