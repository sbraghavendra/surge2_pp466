<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('frmEntityDefault:entityDefaultListTable', 'entityDefaultTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="entityDefaultTableRowIndex" value="#{entityBackingBean.selectedEntityDefaultRowIndex}"/>
<f:view>
<h:form id="frmEntityDefault">
<h1><h:graphicImage id="imgUserMap" alt="Entities" value="/images/headers/hdr-entity.gif" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top"><a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel></div>
	
	<div id="table-one-content" >
		<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7"><h:outputText value="#{entityBackingBean.actionText}"/> a Location Set</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="7" >Entity</th></tr>	
			<tr>
				<td>&#160;</td>
				<td>Code:</td>
				<td style="width:300px;">
					<h:inputText id="entityCode" value="#{entityBackingBean.entityItemDTO.entityCode}" disabled="true" style="width:300px;" /></td>        
				<td style="width:100%;">&#160;</td>
				<td>Name:</td>
				<td style="width:300px;">
					<h:inputText id="feinId" value="#{entityBackingBean.entityItemDTO.entityName}" disabled="true"  style="width:300px;" /></td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Description:</td>
				<td style="width:300px;">
					<h:inputText id="description" value="#{entityBackingBean.entityItemDTO.description}" disabled="true" style="width:300px;" /></td>   
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr><th colspan="7" >Location Set</th></tr>	
			
			<tr>
				<td>&#160;</td>
				<td>Set Code:</td>
				<td style="width:300px;"> 
					<h:inputText id="locnSetCodeId" value="#{entityBackingBean.entityLocnSetDTO.locnSetCode}" maxlength="40" immediate="true" style="width:300px;" 
							disabled="#{entityBackingBean.deleteAction or entityBackingBean.updateAction}"  >
  					</h:inputText>
			    </td>    
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td>&#160;</td>
				<td>Name:</td>
				<td style="width:300px;"> 
					<h:inputText id="nameId" value="#{entityBackingBean.entityLocnSetDTO.name}" maxlength="50" immediate="true" style="width:300px;" 
							disabled="#{entityBackingBean.deleteAction}" >
  					</h:inputText>
			    </td>    
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td>&#160;</td>
				<td>Active:</td>
			
			    
			    <td style="width:200px;">
					
					 
					 <h:selectBooleanCheckbox id="activeFlagId" 
					  disabled="#{entityBackingBean.deleteAction}"
						value="#{entityBackingBean.entityLocnSetDTO.activeBooleanFlag}"/>
					 
					 
					 </td>
			         
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td style="width:300px;">
					<h:inputText value="#{entityBackingBean.entityLocnSetDTO.updateUserId}" disabled="true" id="txtUpdateUserId" style="width:300px;" /></td>        
				<td style="width:100%;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td style="width:300px;">
					<h:inputText value="#{entityBackingBean.entityLocnSetDTO.updateTimestamp}" disabled="true" id="txtUpdateTimestamp" style="width:300px;">
						<f:converter converterId="dateTime"/>
					</h:inputText></td>
				<td>&#160;</td>
			</tr>
		</tbody>
		</table>
	</div><!-- end table-one-content -->
	
	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okLocationBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="okLocationSet" /></h:commandLink></li>
			<li class="cancel"><h:commandLink id="cancelLocationBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="cancelLocationSet" /></h:commandLink></li> 
	 	</ul>
	</div><!-- end table-one-bottom -->
	</div><!-- end table-one -->
</div>

</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>