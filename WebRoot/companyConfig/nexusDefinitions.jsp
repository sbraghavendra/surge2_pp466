<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemTable', 'selectedEntityIndex'); } );
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="selectedEntityIndex" value="#{nexusDefinitionBackingBean.selectedEntityIndex}"/>
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="../images/headers/hdr-nexus-definitions.gif" width="250" height="19" /></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<td style="width: 100px;">&#160;Entity Code:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="filterEntityCode" value="#{nexusDefinitionBackingBean.filterEntityCode}" />
			</td>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;Entity Name:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 300px;" id="filterEntityName" value="#{nexusDefinitionBackingBean.filterEntityName}" />
			</td>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;Address:</td>
			<td style="padding-left: 15px;">
				<table class="embedded-table" style="width: 100%">
					<tr>
						<td colspan="5" style="border-bottom: none;">
							<ui:include src="/WEB-INF/view/components/address.xhtml">
								<ui:param name="handler" value="#{nexusDefinitionBackingBean.filterHandler}"/>
								<ui:param name="id" value="addressInput"/>
								<ui:param name="readonly" value="false"/>
								<ui:param name="showheaders" value="true"/>
								<ui:param name="popupName" value="searchJurisdiction"/>
								<ui:param name="popupForm" value="searchJurisdictionForm"/>
							</ui:include>
						</td>
						<td style="border-bottom: none;">&#160;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;Active?:</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="selectedActiveInactive" value="#{nexusDefinitionBackingBean.filterActive}">
					<f:selectItem id="allItm" itemValue="-1" itemLabel="All Entities" />
					<f:selectItem id="activeItm" itemValue="1" itemLabel="Active Only" />
					<f:selectItem id="inactiveItm" itemValue="0" itemLabel="Inactive Only" />
				</h:selectOneMenu>
			</td>
		</tr>
	</table>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="clear">
				<a4j:commandLink id="btnClear" action="#{nexusDefinitionBackingBean.nexusDefResetFilter}" reRender="filterEntityCode,filterEntityName,selectedActiveInactive,addressInput" />
			</li>
			<li class="search">
				<a4j:commandLink id="btnSearch" action="#{nexusDefinitionBackingBean.nexusDefSearchFilter}" reRender="itemTable,updBtn"/>
			</li>
		</ul>
	</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSEntities-view-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
		<div id="table-four-top">
			<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
			<a4j:status id="pageInfo" 
						startText="Request being processed..." 
						stopText=""
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		</div>	
		<div id="table-four-content">
			<div class="scrollContainer">
				<div class="scrollInner">				
					<rich:dataTable rowClasses="odd-row,even-row" id="itemTable" styleClass="GridContent"
						value="#{nexusDefinitionBackingBean.entityItemList}" var="item">
					
						<a4j:support event="onRowClick"
								onsubmit="selectRow('nexusDefForm:itemTable', this);"
								actionListener="#{nexusDefinitionBackingBean.selectedEntityChanged}" immediate="true"
								reRender="updBtn"/>
					
						<rich:column sortBy="#{item.entityCode}">
							<f:facet name="header"><h:outputText value="Code"/></f:facet>
							<h:outputText id="rowentityCode" value="#{item.entityCode}"/>
						</rich:column>
						<rich:column sortBy="#{item.entityName}">
							<f:facet name="header"><h:outputText value="Name"/></f:facet>
							<h:outputText id="rowentityName" value="#{item.entityName}"/>
						</rich:column>
						<rich:column sortBy="#{item.geocode}">
							<f:facet name="header"><h:outputText value="Geocode"/></f:facet>
							<h:outputText id="rowgeocode" value="#{item.geocode}"/>
						</rich:column>
						<rich:column sortBy="#{item.country}">
							<f:facet name="header"><h:outputText value="Country"/></f:facet>
							<h:outputText id="rowcountry" value="#{cacheManager.countryMap[item.country]} #{(not empty item.country) ? '(' : ''}#{item.country}#{(not empty item.country) ? ')' : ''}"/>
						</rich:column>
						<rich:column sortBy="#{item.state}">
							<f:facet name="header"><h:outputText value="State"/></f:facet>
							<h:outputText id="rowstate" value="#{cacheManager.taxCodeStateMap[item.state].name} #{(not empty item.state) ? '(' : ''}#{item.state}#{(not empty item.state) ? ')' : ''}"/>
						</rich:column>
						<rich:column sortBy="#{item.county}">
							<f:facet name="header"><h:outputText value="County"/></f:facet>
							<h:outputText id="rowcounty" value="#{item.county}"/>
						</rich:column>
						<rich:column sortBy="#{item.city}">
							<f:facet name="header"><h:outputText value="City"/></f:facet>
							<h:outputText id="rowcity" value="#{item.city}"/>
						</rich:column>
						<rich:column sortBy="#{item.zip}">
							<f:facet name="header"><h:outputText value="Zip Code"/></f:facet>
							<h:outputText id="rowzip" value="#{item.zip} #{(not empty item.zipPlus4) ? '-' : ''}#{item.zipPlus4}"/>
						</rich:column>
						<rich:column sortBy="#{item.activeBooleanFlag}" style="text-align:center;">
							<f:facet name="header"><h:outputText value="Active?"/></f:facet>
							<h:selectBooleanCheckbox id="chkActive" value="#{item.activeBooleanFlag}" disabled="true" styleClass="check"/>
						</rich:column>
					</rich:dataTable>
				</div>
			</div>
		</div>
		<div id="table-four-bottom">
			<ul class="right">
				<li class="update"><h:commandLink id="updBtn" disabled="#{!nexusDefinitionBackingBean.validSelection}" action="#{nexusDefinitionBackingBean.displayUpdateAction}" /></li>	
			</ul>
		</div>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>