<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<f:view>
<h:form id="frmEntityDefault">
<h1><h:graphicImage id="imgUserMap" alt="Entities" value="/images/headers/hdr-entity.gif" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top"><a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel></div>
	
	<div id="table-one-content" >
		<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7"><h:outputText value="#{entityBackingBean.actionText}"/> Location Set Details</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="7" >Entity</th></tr>	
			<tr>
				<td>&#160;</td>
				<td>Code:</td>
				<td style="width:300px;">
					<h:inputText id="entityCode" value="#{entityBackingBean.entityItemDTO.entityCode}" disabled="true" style="width:300px;" /></td>        
				<td style="width:100%;">&#160;</td>
				<td>Name:</td>
				<td style="width:300px;">
					<h:inputText id="feinId" value="#{entityBackingBean.entityItemDTO.entityName}" disabled="true"  style="width:300px;" /></td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Description:</td>
				<td style="width:300px;">
					<h:inputText id="description" value="#{entityBackingBean.entityItemDTO.description}" disabled="true" style="width:300px;" /></td>   
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr><th colspan="7" >Location Set</th></tr>	
			
			<tr>
				<td>&#160;</td>
				<td>Set Code:</td>
				<td style="width:300px;"> 
					<h:inputText id="locnSetCodeId" value="#{entityBackingBean.entityLocnSetDTO.locnSetCode}" immediate="true" style="width:300px;" 
							disabled="#{true}"  >
  					</h:inputText>
			    </td>    
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td>&#160;</td>
				<td>Name:</td>
				<td style="width:300px;"> 
					<h:inputText id="nameId" value="#{entityBackingBean.entityLocnSetDTO.name}" immediate="true" style="width:300px;" 
							disabled="#{true}" >
  					</h:inputText>
			    </td>    
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td>&#160;</td>
				<td>Active:</td>
			    <td style="width:200px;"> 
					 <h:selectBooleanCheckbox id="activeFlagId" disabled="#{true}" value="#{entityBackingBean.entityLocnSetDTO.activeBooleanFlag}"/>
					 </td> 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>

			<tr><th colspan="7" >Set Details</th></tr>	
			
			<tr>
				<td>&#160;</td>
				<td>Description:</td>
			    <td style="width:300px;"> 
					 <h:inputText id="descriptionDtlId"  disabled="#{entityBackingBean.deleteAction}" maxlength="100" value="#{entityBackingBean.entityLocnSetDtlDTO.description}" style="width:300px;" />
					 </td> 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Ship From:</td>
			    <td style="width:200px;"> 
					 <h:selectOneMenu id="sfEntityId" disabled="#{entityBackingBean.deleteAction}" value="#{entityBackingBean.entityLocnSetDtlDTO.sfEntityId}" immediate="true"  
						style="width:300px;" >
  						<f:selectItems value="#{entityBackingBean.pickShipFromEntityListOptions}" /> 
  					 </h:selectOneMenu>

					</td> 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>P.O. Origin:</td>
			    <td style="width:200px;"> 
					 <h:selectOneMenu id="pooEntityId" disabled="#{entityBackingBean.deleteAction}" value="#{entityBackingBean.entityLocnSetDtlDTO.pooEntityId}" immediate="true"  
						style="width:300px;" >
  						<f:selectItems value="#{entityBackingBean.pickPOOriginEntityListOptions}" /> 
  					 </h:selectOneMenu>
					 
					 </td> 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>P.O. Acceptance:</td>
			    <td style="width:200px;"> 
					 <h:selectOneMenu id="poaEntityId" disabled="#{entityBackingBean.deleteAction}" value="#{entityBackingBean.entityLocnSetDtlDTO.poaEntityId}" immediate="true"  
						style="width:300px;" >
  						<f:selectItems value="#{entityBackingBean.pickPOAcceptanceEntityListOptions}" /> 
  					 </h:selectOneMenu>
  					 
					 </td> 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Effective Date:</td>
				<td><rich:calendar popup="true" id="effectiveDate" disabled="#{entityBackingBean.deleteAction}"
						oninputkeypress="return onlyDateValue(event);"
             			rendered="true" converter="date" datePattern="M/d/yyyy" value="#{entityBackingBean.entityLocnSetDtlDTO.effectiveDate}" />
				</td>	 	 
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td style="width:300px;">
					<h:inputText value="#{entityBackingBean.entityLocnSetDtlDTO.updateUserId}" disabled="true" id="txtUpdateUserId" style="width:300px;" /></td>        
				<td style="width:100%;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td style="width:300px;">
					<h:inputText value="#{entityBackingBean.entityLocnSetDtlDTO.updateTimestamp}" disabled="true" id="txtUpdateTimestamp" style="width:300px;">
						<f:converter converterId="dateTime"/>
					</h:inputText></td>
				<td>&#160;</td>
			</tr>
		</tbody>
		</table>
	</div><!-- end table-one-content -->
	
	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okLocationBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="okLocationSetDetail" /></h:commandLink></li>
			<li class="cancel"><h:commandLink id="cancelLocationBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="cancelLocationSetDetail" /></h:commandLink></li> 
	 	</ul>
	</div><!-- end table-one-bottom -->
	</div><!-- end table-one -->
</div>

</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>