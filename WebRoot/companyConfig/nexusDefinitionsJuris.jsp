<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemTable', 'selectedDefinitionDetailIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:registrationTable', 'selectedRegistrationDetailIndex'); } );	
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemLevelTable', 'selectedLevelIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemCountyTable', 'selectedCountyIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemCityTable', 'selectedCityIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemStjTable', 'selectedStjIndex'); } );
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="selectedDefinitionDetailIndex" value="#{nexusDefinitionBackingBean.selectedDefinitionDetailIndex}"/>
<h:inputHidden id="selectedRegistrationDetailIndex" value="#{nexusDefinitionBackingBean.selectedRegistrationDetailIndex}"/>
<h:inputHidden id="selectedLevelIndex" value="#{nexusDefinitionBackingBean.selectedLevelIndex}"/>
<h:inputHidden id="selectedCountyIndex" value="#{nexusDefinitionBackingBean.selectedCountyIndex}"/>
<h:inputHidden id="selectedCityIndex" value="#{nexusDefinitionBackingBean.selectedCityIndex}"/>
<h:inputHidden id="selectedStjIndex" value="#{nexusDefinitionBackingBean.selectedStjIndex}"/>
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="#{(nexusDefinitionBackingBean.registrationTask) ? '../images/headers/hdr-registrations.gif' : '../images/headers/hdr-nexus-definitions.gif'}" width="250" height="19" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="4"><h:outputText id="lblHeading" value=""/>Define Jurisdictions &amp; #{(nexusDefinitionBackingBean.registrationTask) ? 'Registrations' : 'Definitions'}</td></tr>
		</thead>
		<tbody>
		<tr>
			<th colspan="4">&#160;Entity</th>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;Entity Code:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="filterEntityCode" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityCode}" disabled="true" />
			</td>
			<td style="width: 100px;">&#160;Entity Name:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 300px;" id="filterEntityName" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityName}" disabled="true" />
			</td>
		</tr>
		<tr>
			<th colspan="4">&#160;State</th>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;Country:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="selectedCountry" value="#{cacheManager.countryMap[nexusDefinitionBackingBean.selectedState.country]} (#{nexusDefinitionBackingBean.selectedState.country})" disabled="true" />
			</td>
			<td style="width: 100px;">&#160;State:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 300px;" id="selectedState" value="#{nexusDefinitionBackingBean.selectedState.name} (#{nexusDefinitionBackingBean.selectedState.taxCodeState})" disabled="true" />
			</td>
		</tr>
		<tr>
			<th colspan="4">&#160;Selected Jurisdictions</th>
		</tr>
		<tr>
			<td colspan="4">
				<table class="embedded-table" width="100%">
					<tr>
						<td style="width: 10px;border-bottom: none;">&#160;</td>
						<td style="border-bottom: none;"><b>Jurisdiction Levels</b></td>
						<td style="border-bottom: none;"><b>Counties</b></td>
						<td style="border-bottom: none;"><b>Cities</b></td>
						<td style="border-bottom: none;"><b>STJs</b></td>
					</tr>
					<tr>
						<td style="width: 10px;border-bottom: none;">&#160;</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background-color: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemLevelTable" style="width: 100%;" styleClass="GridContent"
									value="#{nexusDefinitionBackingBean.levelList}" var="itemLevel" ajaxKeys="#{nexusDefinitionBackingBean.levelRowsToUpdate}">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemLevelTable', this);"
										actionListener="#{nexusDefinitionBackingBean.selectedLevelChanged}"
										immediate="true"
										reRender="itemCountyTable,itemCityTable,itemStjTable,addBtn,delBtn,itemTable,registrationTable"/>
								
									<rich:column width="22px;" id="levelSelectColumn">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="levelSelect" value="${itemLevel.selected}"  disabled="${!itemLevel.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{nexusDefinitionBackingBean.levelCheckboxSelected}"
												reRender="levelSelect,itemCountyTable,itemCityTable,itemStjTable"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="#{(nexusDefinitionBackingBean.registrationTask) ? 'Reg' : 'Nexus'}"/></f:facet>
										<h:selectBooleanCheckbox id="levelNexus" disabled="true" value="#{itemLevel.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="Level"/></f:facet>
										<h:outputText id="level" value="#{itemLevel.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background-color: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemCountyTable" style="width: 100%;" styleClass="GridContent"
									value="#{nexusDefinitionBackingBean.countyList}" var="itemCounty">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemCountyTable', this);"
										actionListener="#{nexusDefinitionBackingBean.selectedCountyChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCityTable,itemStjTable,addBtn,delBtn,itemTable,registrationTable"/>
								
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="countySelect" value="${itemCounty.selected}" disabled="${!itemCounty.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{nexusDefinitionBackingBean.countyCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="#{(nexusDefinitionBackingBean.registrationTask) ? 'Reg' : 'Nexus'}"/></f:facet>
										<h:selectBooleanCheckbox id="countyNexus" disabled="true" value="#{itemCounty.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="County"/></f:facet>
										<h:outputText id="county" value="#{itemCounty.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemCityTable" style="width: 100%;" styleClass="GridContent"
									value="#{nexusDefinitionBackingBean.cityList}" var="itemCity">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemCityTable', this);"
										actionListener="#{nexusDefinitionBackingBean.selectedCityChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCountyTable,itemStjTable,addBtn,delBtn,itemTable,registrationTable"/>
										
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="citySelect" value="${itemCity.selected}" disabled="${!itemCity.enabled}">
											<a4j:support event="onclick"
												actionListener="#{nexusDefinitionBackingBean.cityCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="#{(nexusDefinitionBackingBean.registrationTask) ? 'Reg' : 'Nexus'}"/></f:facet>
										<h:selectBooleanCheckbox id="cityNexus" disabled="true" value="#{itemCity.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="City"/></f:facet>
										<h:outputText id="city" value="#{itemCity.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
						<td style="border-bottom: none;" id="override">
							<div class="scrollContainer" style="border: 1px solid black; background: white;">
							<div class="scrollInner" style="overflow-x: hidden; -ms-overflow-x: hidden">
								<rich:dataTable rowClasses="odd-row,even-row" id="itemStjTable" style="width: 100%;" styleClass="GridContent"
									value="#{nexusDefinitionBackingBean.stjList}" var="itemSTJ">
									
									<a4j:support event="onRowClick"
										onsubmit="selectRow('nexusDefForm:itemStjTable', this);"
										actionListener="#{nexusDefinitionBackingBean.selectedStjChanged}"
										immediate="true"
										reRender="itemLevelTable,itemCountyTable,itemCityTable,addBtn,delBtn,itemTable,registrationTable"/>
										
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="Select"/></f:facet>
										<h:selectBooleanCheckbox id="stjSelect" value="${itemSTJ.selected}" disabled="${!itemSTJ.enabled}" >
											<a4j:support event="onclick"
												actionListener="#{nexusDefinitionBackingBean.stjCheckboxSelected}"
												reRender="levelSelect"/>
										</h:selectBooleanCheckbox>
									</rich:column>
									<rich:column width="22px;">
										<f:facet name="header"><h:outputText value="#{(nexusDefinitionBackingBean.registrationTask) ? 'Reg' : 'Nexus'}"/></f:facet>
										<h:selectBooleanCheckbox id="stjNexus" disabled="true" value="#{itemSTJ.hasNexus}" />
									</rich:column>
									<rich:column>
										<f:facet name="header"><h:outputText value="STJ"/></f:facet>
										<h:outputText id="stj" value="#{itemSTJ.name}"/>
									</rich:column>
								</rich:dataTable>
							</div>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	
	<div id="table-one-content" style="min-height: 0px;">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<th colspan="2">&#160;#{(nexusDefinitionBackingBean.registrationTask) ? 'Registrations' : 'Definitions'}</th>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="table-one-top">
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText=""
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	</div>
	
	<h:panelGroup rendered="#{nexusDefinitionBackingBean.registrationTask}">
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner">
				<rich:dataTable rowClasses="odd-row,even-row" id="registrationTable" styleClass="GridContent"
					value="#{nexusDefinitionBackingBean.registrationDetailList}" var="item">
					<a4j:support event="onRowClick"
							onsubmit="selectRow('nexusDefForm:registrationTable', this);"
							actionListener="#{nexusDefinitionBackingBean.selectedRegistrationDetailChanged}" immediate="true"
							reRender="addBtn,delBtn"/>
				
					<rich:column sortBy="#{item.effectiveDate}">
						<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
						<h:outputText id="regeffectiveDate" value="#{item.effectiveDate}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
					
					<rich:column style="width:250px;" sortBy="#{item.regTypeCode}">
						<f:facet name="header"><h:outputText value="Nexus Type"/></f:facet>
						<h:outputText id="regTypeCode" value="#{cacheManager.listCodeMap['REGTYPE'][item.regTypeCode].description}"/>
					</rich:column>
					
					<rich:column style="width:200px;" sortBy="#{item.regNo}">
						<f:facet name="header"><h:outputText value="Registration No."/></f:facet>
						<h:outputText id="regNo"  value="#{item.regNo}" />
					</rich:column>
					
					
					<rich:column sortBy="#{item.expirationDate}">
						<f:facet name="header"><h:outputText value="Expiration Date"/></f:facet>
						<h:outputText id="regexpirationDate" value="#{item.expirationDate}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
				</rich:dataTable>
			</div>
		</div>
	</div>
	</h:panelGroup>
	
	
	<h:panelGroup rendered="#{!nexusDefinitionBackingBean.registrationTask}">
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner">
				<rich:dataTable rowClasses="odd-row,even-row" id="itemTable" styleClass="GridContent"
					value="#{nexusDefinitionBackingBean.nexusDefDetailList}" var="item">
					<a4j:support event="onRowClick"
							onsubmit="selectRow('nexusDefForm:itemTable', this);"
							actionListener="#{nexusDefinitionBackingBean.selectedNexusDefDetailChanged}" immediate="true"
							reRender="addBtn,delBtn"/>
				
					<rich:column sortBy="#{item.effectiveDate}">
						<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
						<h:outputText id="roweffectiveDate" value="#{item.effectiveDate}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.nexusTypeCode}">
						<f:facet name="header"><h:outputText value="Nexus Type"/></f:facet>
						<h:outputText id="rownexusTypeCode" value="#{cacheManager.listCodeMap['NEXUSTYPE'][item.nexusTypeCode].description}"/>
					</rich:column>
					<rich:column sortBy="#{item.expirationDate}">
						<f:facet name="header"><h:outputText value="Expiration Date"/></f:facet>
						<h:outputText id="rowexpirationDate" value="#{item.expirationDate}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
				</rich:dataTable>
			</div>
		</div>
	</div>
	</h:panelGroup>
	
	<div id="table-one-bottom">
		<ul class="right">
			<li class="add"><h:commandLink id="addBtn" disabled="#{!nexusDefinitionBackingBean.displayAdd}" action="#{nexusDefinitionBackingBean.displayAddAction}" /></li>
			<li class="delete2"><h:commandLink id="delBtn" disabled="#{!nexusDefinitionBackingBean.displayDelete}" action="#{nexusDefinitionBackingBean.displayDeleteAction}" /></li>
			<li class="close2"><h:commandLink id="closeBtn" action="#{nexusDefinitionBackingBean.jurisCloseAction}" /></li>	
		</ul>
	</div>
	
	
</div>


</h:form>
</ui:define>
</ui:composition>
</html>