<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
</ui:define>
<ui:define name="body">
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="#{(nexusDefinitionBackingBean.registrationTask) ? '../images/headers/hdr-registrations.gif' : '../images/headers/hdr-nexus-definitions.gif'}" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7"><h:outputText id="lblHeading" value=""/>#{nexusDefinitionBackingBean.currentAction.actionText} #{(nexusDefinitionBackingBean.bulkExpire) ? 'All' : 'a'} #{(nexusDefinitionBackingBean.registrationTask) ? 'Registration' : 'Nexus Definition'}#{(nexusDefinitionBackingBean.bulkExpire) ? 's' : ''}</td></tr>
		</thead>
		<tbody>
		<tr>
			<th colspan="7">&#160;Entity</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Code:</td>
			<td class="column-input">
				<h:inputText id="filterEntityCode" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityCode}" disabled="true" />
			</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width: 100px;">&#160;Name:</td>
			<td class="column-description" colspan="5">
				<h:inputText style="width: 300px;" id="filterEntityName" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityName}" disabled="true" />
			</td>
		</tr>
		<c:if test="#{!nexusDefinitionBackingBean.jurisdictionChecked}">
		<tr>
			<th colspan="7">&#160;Jurisdiction</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Country:</td>
			<td class="column-input">
				<h:inputText id="selectedCountry" value="#{cacheManager.countryMap[nexusDefinitionBackingBean.selectedJurisdiction.countryCode]} (#{nexusDefinitionBackingBean.selectedJurisdiction.countryCode})" disabled="true" />
			</td>
			<td class="column-label">&#160;State:</td>
			<td class="column-input"><h:inputText id="selectedState" value="#{nexusDefinitionBackingBean.selectedJurisdiction.stateName} (#{nexusDefinitionBackingBean.selectedJurisdiction.stateCode})" disabled="true" /></td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<c:if test="#{!nexusDefinitionBackingBean.bulkExpire}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;County:</td>
			<td class="column-input"><h:inputText id="selectedCounty" value="#{nexusDefinitionBackingBean.selectedJurisdiction.county}" disabled="true" /></td>
			<td class="column-label">&#160;City:</td>
			<td class="column-input"><h:inputText id="selectedCity" value="#{nexusDefinitionBackingBean.selectedJurisdiction.city}" disabled="true" /></td>
			<td class="column-label">&#160;STJ:</td>
			<td class="column-input"><h:inputText id="selectedStj" value="#{nexusDefinitionBackingBean.selectedJurisdiction.stj}" disabled="true" /></td>
		</tr>
		</c:if>
		</c:if>
		
		<h:panelGroup rendered="#{nexusDefinitionBackingBean.registrationTask}">
		<tr>
			<th colspan="7">&#160;Registration</th>
		</tr>
		
		<c:if test="#{!nexusDefinitionBackingBean.bulkExpire}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Effective Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{nexusDefinitionBackingBean.readOnlyAction}"
						value="#{nexusDefinitionBackingBean.selectedRegistrationDetail.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefinitionBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="regeffDate" />
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. Type:</td>
			<td class="column-input" colspan="5">
				<h:selectOneMenu id="regType" disabled="#{nexusDefinitionBackingBean.readOnlyAction}" value="#{nexusDefinitionBackingBean.selectedRegistrationDetail.regTypeCode}">
					<f:selectItems id="regTypeItms" value="#{nexusDefinitionBackingBean.regTypeItems}" />
				</h:selectOneMenu>
			</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. No.:</td>
			<td class="column-input" colspan="5">
				<h:inputText disabled="#{nexusDefinitionBackingBean.readOnlyAction}" value="#{nexusDefinitionBackingBean.selectedRegistrationDetail.regNo}" id="regNo" style="width:300px;" maxlength="100" />
			</td>
		</tr>
		</c:if>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Expiration Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{nexusDefinitionBackingBean.readOnlyAction and !nexusDefinitionBackingBean.bulkExpire}"
						value="#{nexusDefinitionBackingBean.selectedRegistrationDetail.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefinitionBackingBean.validateExpirationDate}"
						showApplyButton="false" id="regexpiryDate" />
			</td>
		</tr>
		</h:panelGroup>
		
		<h:panelGroup rendered="#{!nexusDefinitionBackingBean.registrationTask}">
		<tr>
			<th colspan="7">&#160;Definition</th>
		</tr>
		<c:if test="#{!nexusDefinitionBackingBean.bulkExpire}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Effective Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{nexusDefinitionBackingBean.readOnlyAction}"
						value="#{nexusDefinitionBackingBean.selectedNexusDefDetail.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefinitionBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="effDate" />
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Nexus Type:</td>
			<td class="column-input" colspan="5">
				<h:selectOneMenu id="nexusType" disabled="#{nexusDefinitionBackingBean.readOnlyAction}" value="#{nexusDefinitionBackingBean.selectedNexusDefDetail.nexusTypeCode}">
					<f:selectItems id="nexusTypeItms" value="#{nexusDefinitionBackingBean.nexusTypeItems}" />
				</h:selectOneMenu>
			</td>
		</tr>
		</c:if>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Expiration Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{nexusDefinitionBackingBean.readOnlyAction and !nexusDefinitionBackingBean.bulkExpire}"
						value="#{nexusDefinitionBackingBean.selectedNexusDefDetail.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefinitionBackingBean.validateExpirationDate}"
						showApplyButton="false" id="expiryDate" />
			</td>
		</tr>
		</h:panelGroup>
		
		
		</tbody>
	</table>
	<c:if test="#{!nexusDefinitionBackingBean.bulkExpire}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{nexusDefinitionBackingBean.selectedNexusDefDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{nexusDefinitionBackingBean.selectedNexusDefDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</c:if>
	</div>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" action="#{nexusDefinitionBackingBean.okAddAction}" /></li>
			<c:if test="#{!nexusDefinitionBackingBean.bulkExpire}">
				<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{nexusDefinitionBackingBean.cancelAction}" /></li>
			</c:if>
			<c:if test="#{nexusDefinitionBackingBean.bulkExpire}">
				<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{nexusDefinitionBackingBean.bulkExpireCancelAction}" /></li>
			</c:if>	
		</ul>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>