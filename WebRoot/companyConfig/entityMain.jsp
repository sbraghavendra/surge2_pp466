<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('frmEntityMain:entityItemsTable', 'entityTableRowIndex'); } );
registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="entityTableRowIndex" value="#{entityBackingBean.selectedRowIndex}"/>

<h:form id="frmEntityMain">


<h:panelGroup rendered="#{entityBackingBean.findMode}">
<h1><h:graphicImage id="image1"  alt="Lookup Entity"  url="/images/headers/hdr-Lookup_Entity.png"></h:graphicImage>  </h1>
</h:panelGroup>
		
<h:panelGroup rendered="#{!entityBackingBean.findMode}">	
<h1><h:graphicImage id="imgUserMap" alt="Entities" value="/images/headers/hdr-entity.gif" /></h1>
</h:panelGroup>

<div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{entityBackingBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(entityBackingBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	</a4j:commandLink>					
</div> 

<a4j:outputPanel id="showhidefilter">
    	<c:if test="#{!entityBackingBean.togglePanelController.isHideSearchPanel}">
     	<div id="top">
      	<div id="table-one" style = "width:957px">
			<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/entities_panel.xhtml">
					<ui:param name="handlerBean" value="#{entityBackingBean}"/>
					<ui:param name="readonly" value="false"/>					
				</a4j:include>
			</a4j:outputPanel >
      	</div>
     	</div>
	</c:if>
</a4j:outputPanel>
    
<div class="wrapper">
  <span class="block-right">&#160;</span>
  <span class="block-left tab">
   <h:graphicImage url="/images/containers/STSView-entities.png" width="192"  height="17" />
  </span>
</div>


<div id="bottom" >
<div id="table-four">
<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
		<tbody>
			<tr>
			<td style="width:60px;">&#160;</td>
			<td align="left" >
				<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
			        <a4j:status id="pageInfo"  
							startText="Request being processed..." 
							stopText="#{entityBackingBean.entityDataModel.pageDescription }"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
			     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			</td>
			<td align="right" width="300">
				<a4j:commandLink id="reserSortOrder" style="text-decoration: underline;color:#0033FF;" action="#{entityBackingBean.resetSortAction}" 
					reRender="pageInfo,entityItemsTable,trScroll,addBtn,copyaddBtn,updateBtn,viewBtn,deleteBtn,entityutilitiesId,NexusRegisId,DefaultsId,ProductGandSId,DefaultsId1,ProductGandSId1,customersId,CopyToId,messageTest" oncomplete="initScrollingTables();" ><h:outputText value="default order"/></a4j:commandLink>
			</td>
			<td style="width:20px;">&#160;</td>
			</tr>
		</tbody>
	</table>
</div>	

	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
		<!--  
	  	<rich:dataTable rowClasses="odd-row,even-row" id="entityItemsTable" value="#{entityBackingBean.entityItemsList}" 
	    	var="ei" sortMode="single" >
	    -->
	    <rich:dataTable rowClasses="odd-row,even-row" id="entityItemsTable" rows="#{entityBackingBean.entityDataModel.pageSize }"  
	    		value="#{entityBackingBean.entityDataModel}" var="ei">

	    
		<a4j:support id="clk" event="onRowClick" status="rowstatus"
			onsubmit="selectRow('frmEntityMain:entityItemsTable', this);"
			actionListener="#{entityBackingBean.selectedEntityRowChanged}" 
			reRender="okBtn,addBtn,copyaddBtn,updateBtn,viewBtn,deleteBtn,entityutilitiesId,NexusRegisId,DefaultsId,ProductGandSId,DefaultsId1,ProductGandSId1,customersId,CopyToId" />
<!--  
		<rich:column id="lockFlagId" sortBy="#{ei.lockFlag}" >
			<f:facet name="header" >
				<h:outputText styleClass="headerText" value="Inherit?" />
			</f:facet>
			<h:selectBooleanCheckbox value="#{ei.lockBooleanFlag}" disabled="true" styleClass="check"/>
		</rich:column>
-->		
	
		<rich:column id="lockFlag" styleClass="column-left" style="text-align:center;">
            <f:facet name="header">
            	<a4j:commandLink id="lockFlag-a4j" value="Inherit?" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['lockFlag']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:selectBooleanCheckbox value="#{ei.lockBooleanFlag}" disabled="true" styleClass="check"/>
        </rich:column>
		
		<rich:column id="entityLevelId" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="entityLevelId-a4j" value="Level" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['entityLevelId']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{entityBackingBean.entityLevelMap[ei.entityLevelId]}" />
        </rich:column>

		<!-- 
		<rich:column id="compCode" sortBy="#{ei.compCode}">
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="#{entityBackingBean.entityLevelMap[1]}"/>
			</f:facet>
			<h:outputText value="#{ei.compCode}#{(not empty ei.compName) ? '&#160;-&#160;' : ''}#{ei.compName}" />
		</rich:column>
		-->
		<rich:column id="compCode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="compCode-a4j" value="#{entityBackingBean.entityLevelMap[1]}" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['compCode']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.compCode}#{(not empty ei.compName) ? '&#160;-&#160;' : ''}#{ei.compName}" />
        </rich:column>
		
		<rich:column id="divnCode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="divnCode-a4j" value="#{entityBackingBean.entityLevelMap[2]}" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['divnCode']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.divnCode}#{(not empty ei.divnName) ? '&#160;-&#160;' : ''}#{ei.divnName}" />
        </rich:column>
		
		<rich:column id="locnCode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="locnCode-a4j" value="#{entityBackingBean.entityLevelMap[3]}" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['locnCode']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.locnCode}#{(not empty ei.locnName) ? '&#160;-&#160;' : ''}#{ei.locnName}" />
        </rich:column>
		
		<!--  -->
		
		<rich:column id="fein" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="fein-a4j" value="FEIN" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['fein']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.fein}" />
        </rich:column>
		
		<rich:column id="geocode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="geocode-a4j" value="GeoCode" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['geocode']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.geocode}" />
        </rich:column>
		
		<rich:column id="country" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="country-a4j" value="Country" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['country']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{cacheManager.countryMap[ei.country]} #{(not empty ei.country) ? '(' : ''}#{ei.country}#{(not empty ei.country) ? ')' : ''}"  /> 	         
        </rich:column>

		<rich:column id="state" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="state-a4j" value="State" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['state']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{cacheManager.taxCodeStateMap[ei.state].name} #{(not empty cacheManager.taxCodeStateMap[ei.state].name) ? 
						'(' : ''}#{ei.state}#{(not empty cacheManager.taxCodeStateMap[ei.state].name) ? ')' : ''}" />
 	
        </rich:column>
		
		<rich:column id="county" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="county-a4j" value="County" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['county']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.county}" />
        </rich:column>
		
		<rich:column id="city" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="city-a4j" value="City" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['city']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.city}" />
        </rich:column>
		
		<rich:column id="zip" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="zip-a4j" value="Zip" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['zip']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.zip}" />
        </rich:column>
		
		<rich:column id="zipPlus4" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="zipPlus4-a4j" value="Plus4" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['zipPlus4']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.zipPlus4}" />
        </rich:column>
		
		<rich:column id="activeFlag" styleClass="column-left" style="text-align:center;">
            <f:facet name="header">
            	<a4j:commandLink id="activeFlag-a4j" value="Active?" styleClass="sort-#{entityBackingBean.entityDataModel.sortOrder['activeFlag']}"                            
                	actionListener="#{entityBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:selectBooleanCheckbox value="#{ei.activeBooleanFlag}" disabled="true" styleClass="check"/>
        </rich:column>
		
	  	</rich:dataTable>	
	  	
	  	<a4j:outputPanel id="messageTest">
          <c:if test="#{entityBackingBean.entityDataModel.rowCount == 0}">
				<div class="nodatadisplay1"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
          
	</div>
 	</div>
	</div> <!-- end of table-four-content -->
	
	<rich:datascroller id="trScroll"  for="entityItemsTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{entityBackingBean.entityDataModel.curPage}" />
		
	<div id="table-four-bottom">

	<h:panelGroup rendered="#{!entityBackingBean.findMode}">	
		<ul id="nav-up" class="right" >
			<li style=" margin-left: -20px;" ><span class="configure" ><h:commandLink value="" id="entityutilitiesId" immediate="true" action="#" disabled="#{entityBackingBean.selectedEntityLocked}"  /></span>
			     <ul style="margin-top: -68px;  margin-left: 0px; width:115px;">		    
			     	<li class="defaults-rounded" ><h:commandLink immediate="true" id="DefaultsId" action="#{entityBackingBean.displayDefaultAction}" disabled="#{loginBean.isPurchasingSelected}"/></li>   	  
			        <li class="nexus-square" ><h:commandLink immediate="true" id="NexusRegisId" action="#{entityBackingBean.displayNexusDefinitionAction}" disabled="#{loginBean.isPurchasingSelected}"/></li>
			        <li class="customers" ><h:commandLink immediate="true" id="customersId" action="#{entityBackingBean.displayCustomersAction}" disabled="#{loginBean.isPurchasingSelected}"/></li>	     	   
			        <li class="gandS_square" ><h:commandLink immediate="true" id="ProductGandSId" action="#{entityBackingBean.displayGandSgsbAction}" disabled="#{loginBean.isPurchasingSelected}"/></li>
			      </ul>
			      <ul style="margin-top: -34px;  margin-left: 0px; width:115px;">
			      	<li class="defaults-rounded" ><h:commandLink immediate="true" id="DefaultsId1" action="#{entityBackingBean.displayDefaultAction}" disabled="#{loginBean.isBillingSelected}"/></li> 
			      	<li class="gandS_square" ><h:commandLink immediate="true" id="ProductGandSId1" action="#{entityBackingBean.displayGandSAction}" disabled="#{loginBean.isBillingSelected}"/></li>			      
			      </ul>
			</li>			
		</ul>
		</h:panelGroup>
	  	<ul class="right">
		  	<h:panelGroup rendered="#{!entityBackingBean.findMode}">
				<li class="add"><h:commandLink id="addBtn"  disabled = "#{entityBackingBean.currentUser.viewOnlyBooleanFlag}" 
												style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
												action="#{entityBackingBean.displayAddAction}" /></li>
	        	<li class="copy-add"><h:commandLink id="copyaddBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableCopyAdd or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.displayCopyAddAction}" /></li>       
				<li class="update"><h:commandLink id="updateBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.displayUpdateAction}" /></li>
				<li class="delete115"><h:commandLink id="deleteBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableDelete or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.displayDeleteAction}" /></li>
				<li class="view"><h:commandLink id="viewBtn"  disabled="#{entityBackingBean.disableUpdate}" action="#{entityBackingBean.displayViewAction}" /></li>
				<li class="copyto"><h:commandLink id="CopyToId" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.selectedEntityLocked or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.displayCopyToAction}"   /></li>
			</h:panelGroup>
			<h:panelGroup rendered="#{entityBackingBean.findMode}">
				<li class="ok2"><h:commandLink id="okBtn" disabled="#{entityBackingBean.disableUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag}"
				    style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
				    immediate="true" action="#{entityBackingBean.findAction}"/></li>
				<li class="cancel2"><h:commandLink id="btnCancel" 
				    style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
				    disabled = "#{entityBackingBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{entityBackingBean.cancelFindAction}" /></li>
			</h:panelGroup>
			
	   	</ul>
	</div>	
	
</div>	
</div>
</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{entityBackingBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdictionShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipto"/>
	<ui:param name="jurisId" value="jurisIdShipto"/>
	<ui:param name="reRender" value="jurisInputShipto"/>
</ui:include>


</ui:define>
</ui:composition>
</html>