<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('driverHandlerMain:searchResults', 'driverHandlerRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverHandlerRowIndex" value="#{driverHandlerBean.selectedItemKey}"/>

<h:form id="driverHandlerMain">
<h1><h:graphicImage id="image1"  alt="Lookup Driver Reference"  url="/images/headers/hdr-Lookup-DriverReference.png"></h:graphicImage>  </h1>
		
<div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{driverHandlerBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(driverHandlerBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	</a4j:commandLink>					
</div> 

<a4j:outputPanel id="showhidefilter">
    	<c:if test="#{!driverHandlerBean.togglePanelController.isHideSearchPanel}">
     	<div id="top">
      	<div id="table-one">
			<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/driver_handler_panel.xhtml">
					<ui:param name="handlerBean" value="#{driverHandlerBean}"/>				
				</a4j:include>
			</a4j:outputPanel >
      	</div>
     	</div>
	</c:if>
</a4j:outputPanel>
    
<div class="wrapper">
  <span class="block-right">&#160;</span>
  <span class="block-left tab">
   <h:graphicImage url="/images/containers/STSSecurity-details-open.gif" width="192"  height="17" />
  </span>
</div>


<div id="bottom" >
<div id="table-four">

	<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
			<tbody>
				<tr>
				<td style="width:60px;">&#160;</td>
				<td align="left" >
					<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
				        <a4j:status id="pageInfo"  
								startText="Request being processed..." 
								stopText="#{driverHandlerBean.pageDescription}"
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
				     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
				</td>
				<td style="width:20px;">&#160;</td>
				</tr>
			</tbody>
		</table>
	</div>	
	
	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
	    <rich:dataTable rowClasses="odd-row,even-row" id="searchResults" width="680px" value="#{driverHandlerBean.searchList}" var="row">
				
			<a4j:support id="clickHandler" event="onRowClick" status="rowstatus"
				onsubmit="selectRow('driverHandlerMain:searchResults', this);"
				actionListener="#{driverHandlerBean.selectItem}"
				reRender="okBtn"/>

			<rich:column id="driverValue" sortBy="#{row.driverValue}">
				<f:facet name="header"><h:outputText value="Driver Value"/></f:facet>
				<h:outputText value="#{row.driverValue}"/>
			</rich:column>
			
			<rich:column id="driverDesc" sortBy="#{row.driverDesc}">
				<f:facet name="header"><h:outputText value="Driver Desc"/></f:facet>
				<h:outputText value="#{row.driverDesc}"/>
			</rich:column>
			
			<rich:column id="userValue" sortBy="#{row.userValue}">
				<f:facet name="header"><h:outputText value="User Value"/></f:facet>
				<h:outputText value="#{row.userValue}"/>
			</rich:column>
	  	</rich:dataTable>	     
	</div>
 	</div>
	</div> <!-- end of table-four-content -->
	
	  
	<rich:datascroller id="trScroll"  for="searchResults"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="1" />
	
	<div id="table-four-bottom">
	  	<ul class="right">
		<!--	<li class="ok2"><h:commandLink id="okBtn" disabled="#{entityBackingBean.disableUpdate}" immediate="true" action="#{entityBackingBean.findAction}"/></li> -->
			<li class="ok2"><h:commandLink id="okBtn"

						disabled="#{!driverHandlerBean.validSearchResult}" 
						actionListener="#{driverHandlerBean.updateListener}" 
						action="#{driverHandlerBean.callBackAction}" /></li>
				
			<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{driverHandlerBean.callBackAction}" /></li>
			
	   	</ul>
	</div>	
	
</div>	
</div>
</h:form>

</ui:define>
</ui:composition>
</html>