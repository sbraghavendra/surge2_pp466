<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
</ui:define>
<ui:define name="body">
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="../images/headers/hdr-nexus-definitions.gif" width="250" height="19" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7">Automatically Create Nexus Definition</td></tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" action="#{nexusDefinitionAutoTestBean.autoAction}" /></li>
		</ul>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>