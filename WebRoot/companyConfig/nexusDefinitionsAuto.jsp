<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
</ui:define>
<ui:define name="body">
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="../images/headers/hdr-nexus-definitions.gif" width="250" height="19" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7">Automatically Create Nexus Definition</td></tr>
		</thead>
		<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Source:</td>
			<td class="column-input">
				<h:inputText id="sourceCode" value="#{cacheManager.listCodeMap['AUTONEXSRC'][nexusDefinitionAutoBackingBean.sourceCode].description}" disabled="true" />
			</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<th colspan="7">&#160;Entity</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Code:</td>
			<td class="column-input">
				<h:inputText id="entityCode" value="#{nexusDefinitionAutoBackingBean.selectedEntity.entityCode}" disabled="true" />
			</td>
			<td class="column-label">&#160;Name:</td>
			<td class="column-description" colspan="3">
				<h:inputText id="entityName" value="#{nexusDefinitionAutoBackingBean.selectedEntity.entityName}" disabled="true" />
			</td>
		</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<th colspan="5">&#160;Select Jurisdictions</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Level</td>
			<td class="column-label">&#160;Select</td>
			<td class="column-label">&#160;Jurisdictions</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Country:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStateNexus}" value="#{nexusDefinitionAutoBackingBean.countryChecked}" id="countrySelect">
					<a4j:support event="onclick" actionListener="#{nexusDefinitionAutoBackingBean.countryOrStateCheckboxChecked}"
						reRender="stateSelect" />
				</h:selectBooleanCheckbox>
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="country" value="#{nexusDefinitionAutoBackingBean.jurisdiction.country}" />
			</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;State:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStateNexus}" value="#{nexusDefinitionAutoBackingBean.stateChecked}" id="stateSelect">
					<a4j:support event="onclick" actionListener="#{nexusDefinitionAutoBackingBean.countryOrStateCheckboxChecked}"
						reRender="countrySelect" />
				</h:selectBooleanCheckbox>
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="state" value="#{nexusDefinitionAutoBackingBean.jurisdiction.state}" />
			</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;County:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasCountyNexus}" value="#{nexusDefinitionAutoBackingBean.countyChecked}" id="countySelect"  />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="county" value="#{nexusDefinitionAutoBackingBean.jurisdiction.county}" />
			</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;City:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasCityNexus}" value="#{nexusDefinitionAutoBackingBean.cityChecked}" id="citySelect" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="city" value="#{nexusDefinitionAutoBackingBean.jurisdiction.city}" />
			</td>
			<td>&#160;</td>
		</tr>
		<c:if test="#{not empty nexusDefinitionAutoBackingBean.jurisdiction.stj1Name}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;STJ1:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStj1Nexus}" value="#{nexusDefinitionAutoBackingBean.stj1Checked}" id="stj1Select" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="stj1" value="#{nexusDefinitionAutoBackingBean.jurisdiction.stj1Name}" />
			</td>
			<td>&#160;</td>
		</tr>
		</c:if>
		<c:if test="#{not empty nexusDefinitionAutoBackingBean.jurisdiction.stj2Name}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;STJ2:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStj2Nexus}" value="#{nexusDefinitionAutoBackingBean.stj2Checked}" id="stj2Select" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="stj2" value="#{nexusDefinitionAutoBackingBean.jurisdiction.stj2Name}" />
			</td>
			<td>&#160;</td>
		</tr>
		</c:if>
		<c:if test="#{not empty nexusDefinitionAutoBackingBean.jurisdiction.stj3Name}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;STJ3:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStj3Nexus}" value="#{nexusDefinitionAutoBackingBean.stj3Checked}" id="stj3Select" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="stj3" value="#{nexusDefinitionAutoBackingBean.jurisdiction.stj3Name}" />
			</td>
			<td>&#160;</td>
		</tr>
		</c:if>
		<c:if test="#{not empty nexusDefinitionAutoBackingBean.jurisdiction.stj4Name}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;STJ4:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStj4Nexus}" value="#{nexusDefinitionAutoBackingBean.stj4Checked}" id="stj4Select" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="stj4" value="#{nexusDefinitionAutoBackingBean.jurisdiction.stj4Name}" />
			</td>
			<td>&#160;</td>
		</tr>
		</c:if>
		<c:if test="#{not empty nexusDefinitionAutoBackingBean.jurisdiction.stj5Name}">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;STJ5:</td>
			<td class="column-label">
				<h:selectBooleanCheckbox disabled="#{nexusDefinitionAutoBackingBean.hasStj5Nexus}" value="#{nexusDefinitionAutoBackingBean.stj5Checked}" id="stj5Select" />
			</td>
			<td class="column-label">
				<h:inputText disabled="true" id="stj5" value="#{nexusDefinitionAutoBackingBean.jurisdiction.stj5Name}" />
			</td>
			<td>&#160;</td>
		</tr>
		</c:if>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<th colspan="6">&#160;Select None</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reason:</td>
			<td class="column-label" colspan="2">
				<h:inputText id="reason" value="#{nexusDefinitionAutoBackingBean.reason}" style="width:650px;" />
			</td>
			<td class="column-label">
				&#160;
			</td>
			<td>&#160;</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText  
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText  
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" action="#{nexusDefinitionAutoBackingBean.okAction}" /></li>
		</ul>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>