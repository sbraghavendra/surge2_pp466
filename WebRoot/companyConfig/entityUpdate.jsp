<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverIndex" value="#{entityBackingBean.selectedFilterIndex}"/>

<h1><h:graphicImage id="imgEntityAdd" alt="Security Modules" value="/images/headers/hdr-entity.gif"  /></h1>

<h:form id="stsEntityAdd">
<div id="top">
	<div id="table-one">
	<div id="table-four-top" style="padding: 1px 10px 1px 2px;">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;"><tr>
			<td ><h:messages errorClass="error" /></td>
			<td style="width:100%;">&#160;</td>
			<td width="70"><h:outputText style="color:red;" value="*Mandatory&#160;&#160;&#160;&#160;"/></td>
			
		</tr></table>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{entityBackingBean.actionText}"/> an Entity</td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="7">Parent Level:&#160;</th></tr>  
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;">Code - Name:</td>
					
			<c:if test="#{!entityBackingBean.addAction}">
				<td style="width:300px;">
					<h:inputText value="#{entityBackingBean.entityItemDTO.parentCode}&#160;-&#160;#{entityBackingBean.entityItemDTO.parentName}" 
						disabled="true" id="txtParentCode" style="width:300px;" /></td>
			</c:if>			
			
			<c:if test="#{entityBackingBean.addAction}">			
            	<td><h:selectOneMenu id="parentListId" value="#{entityBackingBean.entityCopyFromSelected}" style="width:300px;" immediate="true"
            				valueChangeListener="#{entityBackingBean.onParentEntityChange}">
 						<f:selectItems value="#{entityBackingBean.entityParentList}" /> 
 						
 						<a4j:support ajaxSingle="true" event="onchange" 
							reRender="txtParentDescription,txtParentLevelDescription,txtParentActive,entityLevelId,btnOkAddChild,activeFlagId,exemptGraceDays" />

 					</h:selectOneMenu></td>			
			</c:if>				
									
			<td>&#160;</td>
			<td style="width:80px;">Description:</td>
			<td style="width:300px;">
				<h:inputText value="#{entityBackingBean.entityItemDTO.parentDescription}" 
						disabled="true" id="txtParentDescription"  style="width:300px;" /></td>
			<td >&#160;</td>
		</tr>

		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;" >Level:</td>
			<td style="width:300px;">
				<h:inputText value="#{entityBackingBean.entityItemDTO.parentLevelDescription}" 
						disabled="true" id="txtParentLevelDescription" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:200px;padding-left:0px">
				<h:selectBooleanCheckbox id="txtParentActive" styleClass="check"  value="#{entityBackingBean.entityItemDTO.parentActiveFlag=='1' ? true:false}"  
						disabled="true" /></td>
			<td >&#160;</td>
		</tr>
		

		<tr><th colspan="7">This Level:</th></tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;" >Level:</td>
			<td style="width:154px;">		
				<h:selectOneMenu id="entityLevelId" value="#{entityBackingBean.entityItemDTO.entityLevelId}"
						disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction or entityBackingBean.updateAction}" style="width:154px;" >
  					<f:selectItems value="#{entityBackingBean.entityLevelList}" /> 
  					<a4j:support event="onchange" reRender="btnOkAddChild"/>
  				</h:selectOneMenu></td>            
			<td >&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td >&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="color:red;width:120px;" >*Code:</td>
			<td style="width:300px;">
				<h:inputText id="entityCode" value="#{entityBackingBean.entityItemDTO.entityCode}" maxlength="40" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction or entityBackingBean.updateAction}" style="width:300px;" /></td>          
			<td >&#160;</td>
			<td>FEIN:</td>
			<td style="width:300px;">
				<h:inputText id="feinId" value="#{entityBackingBean.entityItemDTO.fein}" maxlength="100" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}"  style="width:300px;" /></td>
			<td >&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="color:red;width:120px;" >*Name:</td>
			<td style="width:300px;">
				<h:inputText id="entityName" value="#{entityBackingBean.entityItemDTO.entityName}" maxlength="50" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>Description:</td>
			<td style="width:300px;" >
				<h:inputText id="descriptionId" value="#{entityBackingBean.entityItemDTO.description}" maxlength="255" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" style="width:300px;" /></td>
			<td >&#160;</td>
		</tr>
		
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;" >Exemption Grace Days:</td>	 
			<td style="width:150px;">
				<h:inputText id="exemptGraceDays" style="text-align:right;width:150px;" value="#{entityBackingBean.entityItemDTO.exemptGraceDays}" maxlength="10" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction or entityBackingBean.disableActiveFlag}"
						 onkeypress="return onlyIntegerValue(event);"   >
						 <f:convertNumber integerOnly="true" groupingUsed="false" />
				</h:inputText>
			</td>
			             
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:20px;padding-left:0px">	 
				<h:selectBooleanCheckbox id="activeFlagId" styleClass="check"
					disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction or entityBackingBean.disableActiveFlag}"
					value="#{entityBackingBean.entityItemDTO.activeBooleanFlag}"/>
							 
						 </td>
			<td >&#160;</td>
		</tr>

		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;" >Address:&#160;&#160;&#160;&#160;Primary:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine1Id" value="#{entityBackingBean.entityItemDTO.addressLine1}" maxlength="50" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" style="width:300px;" /></td>
			<td>&#160;</td>
			<td >Secondary:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine2Id" value="#{entityBackingBean.entityItemDTO.addressLine2}" maxlength="50" 
						 disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" style="width:300px;" /></td>
			<td >&#160;</td>
		</tr>
	
	</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0"  width="913" id="rollover" class="ruler">
	<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="align:left;padding-left:0px">
			<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
				<ui:param name="handler" value="#{entityBackingBean.filterHandler}"/>
				<ui:param name="id" value="jurisInput"/>
				<ui:param name="readonly" value="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}"/>
				<ui:param name="showheaders" value="true"/>
				<ui:param name="popupName" value="searchJurisdiction"/>
				<ui:param name="popupForm" value="searchJurisdictionForm"/>
			</ui:include>
			</td>
		</tr>
	</tbody>
	</table>
	

	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<tbody>
		<tr><th colspan="7">Configuration:</th></tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="vertical-align:top;width:120px;font-weight:bold" >Inherit Parent Configs?</td>
			<td style="vertical-align:top;padding-left:0px">
					<h:selectBooleanCheckbox id="entityLockFlagId" disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" styleClass="check" value="#{entityBackingBean.entityLocked}" >
						<a4j:support event="onclick" reRender="copyFromId,copyFromListId" />
					</h:selectBooleanCheckbox>
			</td>

			<td >&#160;</td>
			<td >&#160;</td>
			<td >&#160;</td>
			<td >&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;font-weight:bold" >Copy Configs From:</td>
	
			<td style="width:305px;padding-left:3px"><h:selectOneMenu id="copyFromId" value="#{entityBackingBean.entityCopyFromSelected}"
				disabled="#{entityBackingBean.viewAction || entityBackingBean.deleteAction || entityBackingBean.entityLocked}" style="width:305px;" >
						<f:selectItems value="#{entityBackingBean.entityCopyFromList}" /> 
						</h:selectOneMenu> 
					</td>
		
			<td >&#160;</td>
			<td >&#160;</td>  
			<td >&#160;</td>
			<td >&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="vertical-align:top;color:grey;width:120px;" >(Ctrl-Click for multi-select)</td>
		
			<td style="width:306px;"><h:selectManyListbox id="copyFromListId" value="#{entityBackingBean.pickComponentListUpdateResult}" 
				disabled="#{entityBackingBean.viewAction || entityBackingBean.deleteAction || entityBackingBean.entityLocked}" style="width:306px;" >
							<f:selectItems value="#{entityBackingBean.pickComponentListUpdateOptions}"/>
         			</h:selectManyListbox></td>

			<td >&#160;</td>
			<td >&#160;</td>
			<td >&#160;</td>
			<td >&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width:120px;" >Last UserID:</td>
			<td style="width:300px;" >
				<h:inputText value="#{entityBackingBean.entityItemDTO.updateUserId}" 							 
					 	 disabled="true" id="txtUpdateUserId" style="width:300px;" /></td>    
			 	 				 	 			 	     
			<td>&#160;</td>
			<td style="width:80px;" >Last Timestamp:</td>
			<td style="width:300px;">
				<h:inputText value="#{entityBackingBean.entityItemDTO.updateTimestamp}" 							 
					 	 disabled="true" id="txtUpdateTimestamp" style="width:300px;">
				<f:converter converterId="dateTime"/>
				</h:inputText></td>
			<td >&#160;</td>
		</tr>
	</tbody>
	</table>

	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok-add-child"><h:commandLink id="btnOkAddChild" disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction or entityBackingBean.isEntitlLevel3}" value="" action="#{entityBackingBean.okAddChildAction}" /></li>
		<li class="ok-add-peer"><h:commandLink id="btnOkAddPeer" disabled="#{entityBackingBean.viewAction or entityBackingBean.deleteAction}" value="" action="#{entityBackingBean.okAddPeerAction}" /></li>
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{entityBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{entityBackingBean.viewAction}" action="#{entityBackingBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{entityBackingBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="stsEntityAdd:jurisInput"/>
</ui:include>

</ui:define>
</ui:composition>
</html>