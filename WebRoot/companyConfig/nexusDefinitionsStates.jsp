<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('nexusDefForm:itemTable', 'selectedStateIndex'); } );
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="selectedStateIndex" value="#{nexusDefinitionBackingBean.selectedStateIndex}"/>
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus Definitions" src="#{(nexusDefinitionBackingBean.registrationTask) ? '../images/headers/hdr-registrations.gif' : '../images/headers/hdr-nexus-definitions.gif'}" width="250" height="19" /></h1>
<div class="tab"><img src="../images/containers/STSEntities-entity-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<td>&#160;</td>
			<td>Entity Code:</td>
			<td style="width:300px;">
				<h:inputText style="width:300px;" id="filterEntityCode" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityCode}" disabled="true" />
			</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Entity Name:</td>
			<td style="width:300px;">
				<h:inputText style="width:300px;" id="filterEntityName" value="#{nexusDefinitionBackingBean.selectedEntityItem.entityName}" disabled="true" />
			</td>
		</tr>
		<tr>
			<td>&#160;</td>			
			<td>Address:</td>
			<td>
				<table class="embedded-table" style="width: 100%">
					<tr>
						<td colspan="5" style="border-bottom: none;">
							<ui:include src="/WEB-INF/view/components/address.xhtml">
								<ui:param name="handler" value="#{nexusDefinitionBackingBean.viewFilterHandler}"/>
								<ui:param name="id" value="addressInput"/>
								<ui:param name="readonly" value="true"/>
								<ui:param name="showheaders" value="true"/>
								<ui:param name="popupName" value="searchJurisdiction"/>
								<ui:param name="popupForm" value="searchJurisdictionForm"/>
							</ui:include>
						</td>
						<td style="border-bottom: none;">&#160;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSelection-filter-open.gif" />
	</span>
</div>

<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<h:panelGroup id="selection_filter">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<td>&#160;</td>
			<td>Country:&#160;&#160;</td>
			<td style="width:200px;" >
				<h:selectOneMenu style="width:200px;" id="sf_country_menu" binding="#{nexusDefinitionBackingBean.statesFilterHandler.countryInput}" 
					rendered="true" required="false" label="country" value="#{nexusDefinitionBackingBean.statesFilterHandler.country}" >
					
			     	<a4j:support id="sf_country_menu_country_handler" event="onchange" reRender="sf_country_menu_state_items,selection_filter"
						immediate="true" actionListener="#{nexusDefinitionBackingBean.statesFilterHandler.countrySelected}"/>
			    </h:selectOneMenu>
			</td>
			<td style="width:100%;">&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>State:</td>
			<td style="width:200px;">
				<h:selectOneMenu style="width:200px;" id="sf_state_menu" binding="#{nexusDefinitionBackingBean.statesFilterHandler.stateInput}" 
					rendered="true" required="false" label="State" value="#{nexusDefinitionBackingBean.statesFilterHandler.state}" >
					
					<f:selectItems id="sf_country_menu_state_items" value="#{nexusDefinitionBackingBean.statesFilterHandler.stateItems}" />
					
				</h:selectOneMenu>
			</td>
			<td style="width:100%;">&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:200px;">
				<h:selectOneMenu style="width:200px;" id="selectedActiveInactive" value="#{nexusDefinitionBackingBean.filterStatesActive}">
					<f:selectItem id="allItm" itemValue="-1" itemLabel="All States" />
					<f:selectItem id="activeItm" itemValue="1" itemLabel="Active Only" />
					<f:selectItem id="inactiveItm" itemValue="0" itemLabel="Inactive Only" />
				</h:selectOneMenu>
			</td>
			<td style="width:100%;">&#160;</td>
		</tr>
	</table>
	</h:panelGroup>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="clear">
				<a4j:commandLink id="btnClear" action="#{nexusDefinitionBackingBean.nexusDefStatesResetFilter}" reRender="sf_country_menu,sf_state_menu,selectedActiveInactive" />
			</li>
			<li class="search">
				<a4j:commandLink id="btnSearch" action="#{nexusDefinitionBackingBean.nexusDefStatesSearchFilter}" reRender="itemTable,updBtn,delBtn"/>
			</li>
		</ul>
	</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSEntities-view-states-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
		<div id="table-four-top">
			<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
						startText="Request being processed..." 
						stopText=""
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		</div>	
		<div id="table-four-content">
			<div class="scrollContainer">
				<div class="scrollInner">				
					<rich:dataTable rowClasses="odd-row,even-row" id="itemTable" styleClass="GridContent"
						value="#{nexusDefinitionBackingBean.stateList}" var="item">
					
						<a4j:support event="onRowClick"
								onsubmit="selectRow('nexusDefForm:itemTable', this);"
								actionListener="#{nexusDefinitionBackingBean.selectedStateChanged}" immediate="true"
								reRender="updBtn,delBtn"/>
					
						<rich:column style="width:150px;" sortBy="#{item.country}">
							<f:facet name="header"><h:outputText value="Country"/></f:facet>
							<h:outputText id="rowcountry" value="#{cacheManager.countryMap[item.country]} (#{item.country})"/>
						</rich:column>
						<rich:column style="width:250px;" sortBy="#{item.state}">
							<f:facet name="header"><h:outputText value="State"/></f:facet>
							<h:outputText id="rowstate" value="#{item.name} (#{item.taxCodeState})"/>
						</rich:column>
						<rich:column style="width:80px; text-align:center;" sortBy="#{item.activeBooleanFlag}" >
							<f:facet name="header"><h:outputText value="Active?"/></f:facet>
							<h:selectBooleanCheckbox id="chkActive" value="#{item.activeBooleanFlag}" disabled="true" styleClass="check"/>
						</rich:column>						
					</rich:dataTable>
				</div>
			</div>
		</div>
		<div id="table-four-bottom">
			<ul class="right">
				<li class="update"><h:commandLink id="updBtn" disabled="#{!nexusDefinitionBackingBean.validStateSelection}" action="#{nexusDefinitionBackingBean.statesUpdateAction}" /></li>
				<li class="delete"><h:commandLink id="delBtn" disabled="#{!nexusDefinitionBackingBean.validStateSelection}" action="#{nexusDefinitionBackingBean.statesDeleteAction}" /></li>
				<li class="close"><h:commandLink id="closeBtn" disabled="false" action="#{nexusDefinitionBackingBean.statesCloseAction}" /></li>	
			</ul>
		</div>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>