<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('frmEntityDefault:entityDefaultListTable', 'entityDefaultListTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('frmEntityDefault:locationListTable', 'locationListTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('frmEntityDefault:locationListDetailTable', 'locationListDetailTableRowIndex'); } );
//]]>
</script>
</ui:define>



<ui:define name="body">
<h:inputHidden id="entityDefaultListTableRowIndex" value="#{entityBackingBean.selectedEntityDefaultRowIndex}"/>
<h:inputHidden id="locationListTableRowIndex" value="#{entityBackingBean.selectedLocationListRowIndex}"/>
<h:inputHidden id="locationListDetailTableRowIndex" value="#{entityBackingBean.selectedLocationListDetailRowIndex}"/>

<f:view>
<h:form id="frmEntityDefault">
<h1><h:graphicImage id="imgUserMap" alt="Entities" value="/images/headers/hdr-entity.gif" /></h1>
<div id="top">
	<div id="table-one">
	
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<!-- end table-one-top -->
	
	<div id="table-one-content" >
		<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7">&#160;Set Entity Defaults</td></tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="7" >Entity</th>
			</tr>	
			<tr>
				<td>&#160;</td>
				<td>Code:</td>
				<td style="width:300px;">
					<h:inputText id="entityCode" value="#{entityBackingBean.entityItemDTO.entityCode}" 
						 disabled="true" style="width:300px;" /></td>
			             
				<td style="width:100%;">&#160;</td>
				<td>Name:</td>
				<td style="width:300px;">
					<h:inputText id="feinId" value="#{entityBackingBean.entityItemDTO.entityName}" 
						 disabled="true"  style="width:300px;" /></td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Description:</td>
				<td style="width:300px;">
					<h:inputText id="description" value="#{entityBackingBean.entityItemDTO.description}" 
						 disabled="true" style="width:300px;" /></td>
			             
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<th colspan="7" >&#160;</th>
			</tr>	
			<tr>
				<td>&#160;</td>
				<td>Default Type:</td>
				<td style="width:300px;"> 
					<h:selectOneMenu id="entityLevelId" onchange="submit();" value="#{entityBackingBean.selectedDefaultTypeList}" immediate="true"  
						style="width:300px;" valueChangeListener="#{entityBackingBean.defaultTypeSelectionChanged}" 
						reRender="frmEntityDefault:addlocationBtn,frmEntityDefault:updatelocationBtn,frmEntityDefault:deleteDefault1Btn,
						frmEntityDefault:addlocationDetailBtn,frmEntityDefault:updatelocationDetailBtn,frmEntityDefault:deletelocationDetailBtn" >
  						<f:selectItems value="#{entityBackingBean.defaultTypeList}" /> 
  					</h:selectOneMenu>
			    </td>
			             
				<td style="width:100%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
		</tbody>
		</table>
	</div><!-- end table-one-content -->
	</div><!-- end table-one -->
	
</div>

<div id="bottom">	
	<div id="table-four">
	<c:if id="entityDefaultTableMainPanel" test="#{entityBackingBean.isEntityDefaultTableMainPanel}">
	
	<div id="table-four-top"></div>
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner"  >
         	<rich:dataTable rowClasses="odd-row,even-row" id="entityDefaultTableMain" styleClass="GridContent" >	
			</rich:dataTable>
			
			<a4j:outputPanel id="defaultTypeTest">
					<div class="nodatadisplay1"><h:outputText value="Select a default type to retrieve." /></div>
           </a4j:outputPanel>
           
		</div>	
		</div>
	</div><!-- end table-four-content -->
	<div id="table-four-bottom">
		<ul class="right">
			<li class="close"><h:commandLink id="closeDefaultMainBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="closeEntityDefaultMain" /></h:commandLink></li>
	 	</ul>
	</div><!-- end table-four-bottom -->

	</c:if>
	</div><!-- end table-four -->


	<div id="table-four">
	<c:if id="entityDefaultListTablePanel" test="#{entityBackingBean.isEntityDefaultListTablePanel}">
	<div id="table-four-top"></div>
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner"  >
         	<rich:dataTable rowClasses="odd-row,even-row" id="entityDefaultListTable" styleClass="GridContent"   																
				value="#{entityBackingBean.entityDefaultList}" var="def" >
	             
	          	<a4j:support id="entityDefaultListClk" event="onRowClick" onsubmit="selectRow('frmEntityDefault:entityDefaultListTable', this);"
			       	actionListener="#{entityBackingBean.selectedEntityDefaultRowChanged}" reRender="frmEntityDefault:updateDefaultBtn,frmEntityDefault:deleteDefault1Btn"/>	
	             														
				<rich:column id="defaultCodeId" sortBy="#{def.defaultCode}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Default Code" />
					</f:facet>
					<h:outputText value="#{cacheManager.listCodeMap['DEFCODES'][def.defaultCode].description}" />  
				</rich:column>
				
				<rich:column id="defaultValueId" sortBy="#{def.defaultValue}" style="width:100%" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Default Value" />
					</f:facet>
					<h:outputText value="#{cacheManager.listCodeMap[def.defaultCode][def.defaultValue].description}" />  
				</rich:column>
			</rich:dataTable>
		</div>	
		</div>
	</div><!-- end table-four-content -->

	<div id="table-four-bottom">
	<ul class="right">
			<li class="add"><h:commandLink id="addDefaultBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableDefaultAdd or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayAddSingle" /></h:commandLink></li>
			<li class="update"><h:commandLink id="updateDefaultBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableDefaultUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayUpdateSingle" /></h:commandLink></li>
			<li class="delete"><h:commandLink id="deleteDefault1Btn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableDefaultDelete or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayDeleteSingle" /></h:commandLink></li>
			<li class="close"><h:commandLink id="closeDefaultBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="closeSingleMain" /></h:commandLink></li>
	 	</ul>
	</div><!-- end table-four-bottom -->
	</c:if>
	
	</div><!-- end table-four -->

	<div id="table-four">
	<c:if id="locationListTablePanel" test="#{entityBackingBean.isLocationListTablePanel}">
  	<div id="table-four-top"></div>
  	
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner"  >
         	<rich:dataTable rowClasses="odd-row,even-row" id="locationListTable" styleClass="GridContent"   																
				value="#{entityBackingBean.entityLocnSetList}" var="def" >
	             
	          	<a4j:support id="locationListClk" event="onRowClick" onsubmit="selectRow('frmEntityDefault:locationListTable', this);"
			       	actionListener="#{entityBackingBean.selectedLocationListRowChanged}" reRender="addlocationBtn,updatelocationBtn,deletelocationBtn,locationListDetailTable,addlocationDetailBtn,updatelocationDetailBtn,deletelocationDetailBtn"/>	
	             														
				<rich:column id="locationSetCodeId" sortBy="#{def.locnSetCode}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Location Set Code" />
					</f:facet>
					<h:outputText value="#{def.locnSetCode}"/>
				</rich:column>
				
				<rich:column id="nameId" sortBy="#{def.name}"  style="width:100%" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Name" />
					</f:facet>
					<h:outputText value="#{def.name}"/>
				</rich:column>
			</rich:dataTable>
		</div>	
		</div>
	</div><!-- end table-four-content -->

	<div id="table-four-bottom">
		<ul class="right">
			<li class="add"><h:commandLink id="addlocationBtn" disabled = "#{entityBackingBean.currentUser.viewOnlyBooleanFlag }" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayAddLocationSet" /></h:commandLink></li>
			<li class="update"><h:commandLink id="updatelocationBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{entityBackingBean.disableLocationUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag }" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayUpdateLocationSet" /></h:commandLink></li>
			<li class="delete"><h:commandLink id="deletelocationBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{entityBackingBean.disableLocationUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayDeleteLocationSet" /></h:commandLink></li>
	 	</ul>
	</div><!-- end table-four-bottom -->
</c:if>
	</div><!-- end table-four -->


	
	<div id="table-four">
	<c:if id="locationListDetailTablePanel" test="#{entityBackingBean.isLocationListTablePanel}">
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner"  >
         	<rich:dataTable rowClasses="odd-row,even-row" id="locationListDetailTable" styleClass="GridContent" 																
				value="#{entityBackingBean.entityLocnSetDtlList}" var="def" >
	             
	          	<a4j:support id="locationListDetailClk" event="onRowClick" onsubmit="selectRow('frmEntityDefault:locationListDetailTable', this);"
			       	actionListener="#{entityBackingBean.selectedLocationListDetailRowChanged}" reRender="addlocationDetailBtn,updatelocationDetailBtn,deletelocationDetailBtn"/>	
	             														
				<rich:column id="sfEntityId" sortBy="#{def.sfEntityId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Ship From Entity" />
					</f:facet>
					<h:outputText value="#{entityBackingBean.entityItemMap[def.sfEntityId]}"/>
				</rich:column>
				
				<rich:column id="pooEntityId" sortBy="#{def.pooEntityId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Order Origin Entity" />
					</f:facet>
					<h:outputText value="#{entityBackingBean.entityItemMap[def.pooEntityId]}"/>
				</rich:column>
				
				<rich:column id="poaEntityId" sortBy="#{def.poaEntityId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Order Acceptance Entity" />
					</f:facet>
					<h:outputText value="#{entityBackingBean.entityItemMap[def.poaEntityId]}"/>
				</rich:column>
				
				<rich:column id="description" sortBy="#{def.description}"  style="width:100%" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Description" />
					</f:facet>
					<h:outputText value="#{def.description}"/>
				</rich:column>
				
				<rich:column id="effectiveDate" sortBy="#{def.effectiveDate}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Effective Date" />
					</f:facet>
					<h:outputText value="#{def.effectiveDate}"/>
				</rich:column>
			</rich:dataTable>
		</div>	
		</div>
	</div><!-- end table-four-content -->

	<div id="table-four-bottom">
	<ul class="right">
			<li class="add"><h:commandLink id="addlocationDetailBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableLocationDtlAdd or entityBackingBean.currentUser.viewOnlyBooleanFla }" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayAddLocationSetDetail" /></h:commandLink></li> 
			<li class="update"><h:commandLink id="updatelocationDetailBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableLocationDtlUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayUpdateLocationSetDetail" /></h:commandLink></li>
			<li class="delete"><h:commandLink id="deletelocationDetailBtn" style="#{(entityBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{entityBackingBean.disableLocationDtlUpdate or entityBackingBean.currentUser.viewOnlyBooleanFlag }" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="displayDeleteLocationSetDetail" /></h:commandLink></li>
			<li class="close"><h:commandLink id="closelocationDetailBtn" action="#{entityBackingBean.processEntityCommand}" ><f:param name="command" value="closeLocationSetDetailMain" /></h:commandLink></li>
	 	</ul>
	</div><!-- end table-four-bottom -->

</c:if>
	</div><!-- end table-four -->
</div>

</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>