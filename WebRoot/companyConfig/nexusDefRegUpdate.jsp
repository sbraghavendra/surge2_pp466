<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
</ui:define>
<ui:define name="body">
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus" src="#{(nexusDefRegBackingBean.registrationTask) ? '../images/headers/hdr-registrations.gif' : '../images/headers/hdr-nexus-definitions.gif'}" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="7"><h:outputText id="lblHeading" value=""/>#{nexusDefRegBackingBean.taskHeaderLabel}</td></tr>
		</thead>
		<tbody>
		<tr>
			<th colspan="7">&#160;Entity</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Code:</td>
			<td class="column-input">
				<h:inputText id="filterEntityCode" value="#{nexusDefRegBackingBean.updateEntityItem.entityCode}" disabled="true" />
			</td>
	 
			<td style="width: 100px;">&#160;Name:</td>
			<td class="column-description" >
				<h:inputText style="width: 300px;" id="filterEntityName" value="#{nexusDefRegBackingBean.updateEntityItem.entityName}" disabled="true" />
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="width: 100px;">&#160;Description:</td>
			<td class="column-description" colspan="4">
				<h:inputText style="width: 550px;" id="filterEntityDescription" value="#{nexusDefRegBackingBean.updateEntityItem.description}" disabled="true" />
			</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		<h:panelGroup rendered="#{!nexusDefRegBackingBean.isAddNexusTask and !nexusDefRegBackingBean.isRemoveNexusTask}">
		<tr>
			<th colspan="7">&#160;Jurisdiction</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Country:</td>
			<td class="column-input">
				<h:inputText id="selectedCountry" value="#{nexusDefRegBackingBean.selectedJurisdiction.countryName} (#{nexusDefRegBackingBean.selectedJurisdiction.countryCode})" disabled="true" />
			</td>
			<td class="column-label">&#160;State:</td>
			<td class="column-input"><h:inputText id="selectedState" value="#{cacheManager.taxCodeStateMap[nexusDefRegBackingBean.selectedJurisdiction.stateCode].name} (#{nexusDefRegBackingBean.selectedJurisdiction.stateCode})" disabled="true" /></td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;County:</td>
			<td class="column-input"><h:inputText id="selectedCounty" value="#{nexusDefRegBackingBean.selectedJurisdiction.county}" disabled="true" /></td>
			<td class="column-label">&#160;City:</td>
			<td class="column-input"><h:inputText id="selectedCity" value="#{nexusDefRegBackingBean.selectedJurisdiction.city}" disabled="true" /></td>
			<td class="column-label">&#160;STJ:</td>
			<td class="column-input"><h:inputText id="selectedStj" value="#{nexusDefRegBackingBean.selectedJurisdiction.stj}" disabled="true" /></td>
		</tr>
		</h:panelGroup>
		 
		<h:panelGroup rendered="#{nexusDefRegBackingBean.registrationTask}">
		<tr>
			<th colspan="7">&#160;Registration</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Effective Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusDefRegBackingBean.updateOnlyAction}"
						value="#{nexusDefRegBackingBean.updateRegistrationDetail.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefRegBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="regeffDate" />
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. Type:</td>
			<td class="column-input" colspan="5">
				<h:selectOneMenu id="regType" disabled="#{!nexusDefRegBackingBean.updateOnlyAction}" value="#{nexusDefRegBackingBean.updateRegistrationDetail.regTypeCode}">
					<f:selectItems id="regTypeItms" value="#{nexusDefRegBackingBean.regTypeItems}" />
				</h:selectOneMenu>
			</td>
		</tr>	
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. No.:</td>
			<td class="column-input" colspan="5">
				<h:inputText disabled="#{!nexusDefRegBackingBean.updateOnlyAction}" value="#{nexusDefRegBackingBean.updateRegistrationDetail.regNo}" id="regNo" style="width:300px;" maxlength="100" />
			</td>
		</tr>	
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Expiration Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusDefRegBackingBean.updateOnlyAction}"
						value="#{nexusDefRegBackingBean.updateRegistrationDetail.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefRegBackingBean.validateExpirationDate}"
						showApplyButton="false" id="regexpiryDate" />
			</td>
		</tr>
		</h:panelGroup>
		
		<h:panelGroup rendered="#{!nexusDefRegBackingBean.registrationTask and !nexusDefRegBackingBean.isRemoveNexusTask}">
		<tr>
			<th colspan="7">&#160;Definition</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Effective Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusDefRegBackingBean.updateOnlyAction}"
						value="#{nexusDefRegBackingBean.updateNexusDefinitionDetail.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefRegBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="effDate" >
					
						<a4j:support event="onchanged" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusDefRegBackingBean.effectiveDateChanged}"  />
						<a4j:support event="oninputblur" ajaxSingle="true" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusDefRegBackingBean.effectiveDateChanged}"  />
						
				</rich:calendar>
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Nexus Type:</td>
			<td class="column-input" colspan="5">
				<h:selectOneMenu id="nexusType" disabled="#{!nexusDefRegBackingBean.updateOnlyAction}" value="#{nexusDefRegBackingBean.updateNexusDefinitionDetail.nexusTypeCode}">
					<f:selectItems id="nexusTypeItms" value="#{nexusDefRegBackingBean.nexusTypeItems}" />
					
					<a4j:support id="nexusType_handler" event="onchange" immediate="true" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusDefRegBackingBean.nexusTypeChanged}"/>
					
				</h:selectOneMenu>
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Expiration Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusDefRegBackingBean.updateOnlyAction}"
						value="#{nexusDefRegBackingBean.updateNexusDefinitionDetail.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusDefRegBackingBean.validateExpirationDate}"
						showApplyButton="false" id="expiryDate" >
						
						<a4j:support event="onchanged" reRender="detailTable,okBtn" actionListener="#{nexusDefRegBackingBean.expirationDateChanged}"  />
						<a4j:support event="oninputblur" ajaxSingle="true" reRender="detailTable,okBtn" actionListener="#{nexusDefRegBackingBean.expirationDateChanged}"  />
						
				</rich:calendar>
			</td>
		</tr>
		</h:panelGroup>
		
		
		</tbody>
	</table>
	
	<h:panelGroup rendered="#{nexusDefRegBackingBean.registrationTask}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{nexusDefRegBackingBean.updateRegistrationDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{nexusDefRegBackingBean.updateRegistrationDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</h:panelGroup>
	
	<h:panelGroup rendered="#{!nexusDefRegBackingBean.registrationTask and !nexusDefRegBackingBean.isAddNexusTask and !nexusDefRegBackingBean.isRemoveNexusTask}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{nexusDefRegBackingBean.updateNexusDefinitionDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId1"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{nexusDefRegBackingBean.updateNexusDefinitionDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp1"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</h:panelGroup>
	
	</div>
	</div>
	
	<h:panelGroup rendered="#{!nexusDefRegBackingBean.isAddNexusTask and !nexusDefRegBackingBean.isRemoveNexusTask}">
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn1" action="#{nexusDefRegBackingBean.okProcessAction}" /></li>
			<li class="cancel"><h:commandLink id="cancelBtn1" immediate="true" action="#{nexusDefRegBackingBean.cancelAction}" /></li>
		</ul>
	</div>
	</h:panelGroup>
	
</div>
	
<!-- datagrid for selected jurisdictions -->
<h:panelGroup rendered="#{nexusDefRegBackingBean.isAddNexusTask or nexusDefRegBackingBean.isRemoveNexusTask}">
<div id="bottom">
	<div id="table-four">		
		
 
		
		<div id="table-four-top" style="padding: 0 0 0 5px;" >
			
       		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;"><tr>
				<td align="left" width="10"><img style="#{(nexusDefRegBackingBean.isDisplayAuto) ? '' : 'visibility:hidden'}" src="../images/icons/iconinfo.png" /></td>
				<td align="left" width="300"><h:outputText rendered="#{nexusDefRegBackingBean.isDisplayAuto}" value="&#160;&#160;&#160;&#160;#{nexusDefRegBackingBean.displayAutoMessage}" /></td>
				<td align="left" width="300">
					<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        		<a4j:status id="pageInfo"  
					startText="Request being processed..."  
					stopText="#{nexusDefRegBackingBean.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
				</td>
				<td align="right" width="200"><a4j:commandLink  ajaxSingle="true" style="text-decoration: underline;color:#0033FF" immediate="true" reRender="detailTable" action="#{nexusDefRegBackingBean.resetTableSorting}">
												<h:outputText value="default order" /></a4j:commandLink></td>
				<td style="width:20px;">&#160;</td>		
			</tr></table>	
	    </div>
		
		
		<div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner" id="resize" >
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable" headerClass="header" value="#{nexusDefRegBackingBean.selectedJurisdictionList}" var="selectedJurisdiction">     	 
           	
           	<rich:column id="imageIcn" >
				<f:facet name="header"><h:outputText value=""/></f:facet>
				<h:graphicImage rendered="#{selectedJurisdiction.newItemFlag}" url="/images/icons/iconinfo.png" />
				<h:outputText rendered="#{!selectedJurisdiction.newItemFlag}" value="&#160;&#160;&#160;&#160;&#160;"  />

			</rich:column>
           	
           	<rich:column id="country" styleClass="column-left" sortBy="#{selectedJurisdiction.country}" width="150px" >
            	<f:facet name="header"><h:outputText id="country-a4j" styleClass="headerText" value="Country"/></f:facet>
            	<h:outputText value="#{cacheManager.countryMap[selectedJurisdiction.country]} (#{selectedJurisdiction.country})"  />
           	</rich:column>
           	
           	<rich:column id="state" styleClass="column-left" sortBy="#{selectedJurisdiction.state}" width="150px" >
            	<f:facet name="header"><h:outputText id="state-a4j" styleClass="headerText" value="State"/></f:facet>
            	<h:outputText value="#{cacheManager.taxCodeStateMap[selectedJurisdiction.state].name}    	
            		#{(not empty cacheManager.taxCodeStateMap[selectedJurisdiction.state].name) ? '(' : ''}#{selectedJurisdiction.state}#{(not empty cacheManager.taxCodeStateMap[selectedJurisdiction.state].name) ? ')' : ''}         	
            		" />
           	</rich:column>
           	
           	<rich:column id="county" styleClass="column-left" sortBy="#{selectedJurisdiction.county}" width="250px" >
            	<f:facet name="header"><h:outputText id="county-a4j" styleClass="headerText" value="County"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.county}"  />
           	</rich:column>
           	
           	<rich:column id="city" styleClass="column-left" sortBy="#{selectedJurisdiction.city}" width="250px" >
            	<f:facet name="header"><h:outputText id="city-a4j" styleClass="headerText" value="City"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.city}"  />
           	</rich:column>
           	
           	<rich:column id="stj" styleClass="column-left" sortBy="#{selectedJurisdiction.stj}" width="40%" >
            	<f:facet name="header"><h:outputText id="stj-a4j" styleClass="headerText" value="STJ"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.stj}"  />
           	</rich:column>
           	
          
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
	
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" immediate="true" disabled="#{!nexusDefRegBackingBean.addRemoveNexus}" action="#{nexusDefRegBackingBean.okProcessAction}" /></li>
			<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{nexusDefRegBackingBean.cancelAction}" /></li>
		</ul>
	</div>
	
	</div>
</div>
</h:panelGroup>

</h:form>
</ui:define>
</ui:composition>
</html>