<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	registerEvent(window, "unload", function() { document.getElementById('stsVendorAdd:hiddenSaveLink').onclick(); });
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverIndex" value="#{vendorBackingBean.selectedFilterIndex}"/>

<h1><h:graphicImage id="imgEntityAdd" alt="Security Modules" value="/images/headers/hdr-vendor.png"  /></h1>

<h:form id="stsVendorAdd">
<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
	<a4j:support event="onclick" actionListener="#{vendorBackingBean.filterSaveAction}" />
</a4j:commandLink>

<div id="top">
	<div id="table-one">
	<div id="table-four-top" style="padding: 1px 10px 1px 2px;">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;"><tr>
			<td ><h:messages errorClass="error" /></td>
			<td style="width:100%;">&#160;</td>
			<td width="70"><h:outputText style="color:red;" value="*Mandatory&#160;&#160;&#160;&#160;"/></td>
			
		</tr></table>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{vendorBackingBean.actionText}"/> a Vendor</td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="7">Vendor:</th></tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" style="color:red;" >*Code:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<h:inputText id="vendorCode" value="#{vendorBackingBean.vendorItemDTO.vendorCode}" maxlength="100" onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
						 disabled="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction or vendorBackingBean.updateAction}" style="width:300px;" /></td>     
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" style="color:red;">*Name:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<h:inputText id="vendorName" value="#{vendorBackingBean.vendorItemDTO.vendorName}" maxlength="100" 
						 disabled="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction}" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>

		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Address:&#160;&#160;&#160;&#160;</td>
			<td>Line&#160;1:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine1Id" value="#{vendorBackingBean.vendorItemDTO.addressLine1}" maxlength="100" 
						 disabled="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction}" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;</td>
			<td style="width:50px;" >Line&#160;2:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine2Id" value="#{vendorBackingBean.vendorItemDTO.addressLine2}" maxlength="100" 
						 disabled="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction}" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
	</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>		
		 <tr>
			<td class="column-spacer">&#160;</td>
			<td style="align:left;" colspan="5">
				<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
					<ui:param name="handler" value="#{vendorBackingBean.filterHandler}"/>
					<ui:param name="id" value="jurisInput"/>
					<ui:param name="readonly" value="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction}"/>
					<ui:param name="required" value="false"/> 
					<ui:param name="showheaders" value="true"/>
					<ui:param name="showzipplus4" value="true"/>
					<ui:param name="popupName" value="searchJurisdiction"/>
					<ui:param name="popupForm" value="searchJurisdictionForm"/>
				</ui:include>
			</td>
			<td style="width:50%;">&#160;</td>
		</tr>
	</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>	
		<tr>
			<td class="column-spacer">&#160;</td> 
			<td>Active?:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:20px;padding-left:0px">	 
				<h:selectBooleanCheckbox id="activeFlagId" styleClass="check"
					disabled="#{vendorBackingBean.viewAction or vendorBackingBean.deleteAction}"
					value="#{vendorBackingBean.vendorItemDTO.activeBooleanFlag}"/>					 
			</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Last&#160;Update&#160;User&#160;ID:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;" colspan="3" >
				<h:inputText value="#{vendorBackingBean.vendorItemDTO.updateUserId}" 							 
					 	 disabled="true" id="txtUpdateUserId" style="width:690px;" /></td>    	 	 				 	 			 	     
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Last&#160;Update&#160;Timestamp:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;" colspan="3" >
				<h:inputText value="#{vendorBackingBean.vendorItemDTO.updateTimestamp}" 							 
					 	 disabled="true" id="txtUpdateTimestamp" style="width:690px;">
				<f:converter converterId="dateTime"/>
				</h:inputText></td>
			<td>&#160;</td>
		</tr>
	</tbody>
	</table>
<!--  	
	<table cellpadding="0" cellspacing="0"  width="913" id="rollover" class="ruler">
	<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td style="align:left;padding-left:0px">
			<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
				<ui:param name="handler" value="#{vendorBackingBean.filterHandler}"/>
				<ui:param name="id" value="jurisInput"/>
				<ui:param name="readonly" value="#{vendorBackingBean.deleteAction}"/>
				<ui:param name="showheaders" value="true"/>
				<ui:param name="popupName" value="searchJurisdiction"/>
				<ui:param name="popupForm" value="searchJurisdictionForm"/>
			</ui:include>
			</td>
		</tr>
	</tbody>
	</table>
-->	

	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{vendorBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{vendorBackingBean.viewAction}" immediate="true" action="#{vendorBackingBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{vendorBackingBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="stsEntityAdd:jurisInput"/>
</ui:include>

</ui:define>
</ui:composition>
</html>