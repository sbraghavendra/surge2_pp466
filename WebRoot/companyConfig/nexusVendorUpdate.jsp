<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
</ui:define>
<ui:define name="body">
<h:form id="nexusDefForm">
<h1><img id="imgTaxabilityCodes" alt="Nexus" src="#{(nexusVendorBackingBean.registrationTask) ? '../images/headers/hdr-registrations.gif' : '../images/headers/hdr-vendornexus.png'}" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="min-height: 0px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6"><h:outputText id="lblHeading" value=""/>#{nexusVendorBackingBean.taskHeaderLabel}</td>
				<td colspan="1" style="text-align:right;"><h:outputText id="ruleId1" value="#{nexusVendorBackingBean.nexusUsed ? '(Used on Trxn)':''}"/>&#160;</td>
				
			</tr>
		</thead>
		<tbody>
		<tr>
			<th colspan="7">Vendor</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Code:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<h:inputText style="width: 300px;" id="filterEntityCode" value="#{nexusVendorBackingBean.updateVendorItem.vendorCode}" disabled="true" />
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Name:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;" >
				<h:inputText style="width: 300px;" id="filterEntityName" value="#{nexusVendorBackingBean.updateVendorItem.vendorName}" disabled="true" />
			</td>
	 
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Address:&#160;&#160;&#160;&#160;</td>
			<td style="width:50px;" >Line&#160;1:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine1Id" value="#{nexusVendorBackingBean.updateVendorItem.addressLine1}" maxlength="100" 
						 disabled="true" style="width:300px;" /></td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;</td>
			<td style="width:50px;" >Line&#160;2:</td>
			<td style="width:300px;">
				<h:inputText id="addressLine2Id" value="#{nexusVendorBackingBean.updateVendorItem.addressLine2}" maxlength="100" 
						 disabled="true" style="width:300px;" /></td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
<!-- 		<h:panelGroup rendered="#{nexusVendorBackingBean.isAddNexusTask or nexusVendorBackingBean.isRemoveNexusTask}">-->
			 <tr>
				<td class="column-spacer">&#160;</td>
				<td style="align:left;" colspan="5">
					<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
						<ui:param name="handler" value="#{nexusVendorBackingBean.filterHandler}"/>
						<ui:param name="id" value="jurisInput"/>
						<ui:param name="readonly" value="true"/>
						<ui:param name="required" value="false"/> 
						<ui:param name="showheaders" value="true"/>
						<ui:param name="showzipplus4" value="true"/>
						<ui:param name="popupName" value="searchJurisdiction"/>
						<ui:param name="popupForm" value="searchJurisdictionForm"/>
					</ui:include>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>

<!--		</h:panelGroup> -->
		
		<h:panelGroup rendered="#{!nexusVendorBackingBean.isAddNexusTask and !nexusVendorBackingBean.isRemoveNexusTask}">
		<tr>
			<th colspan="7">Jurisdiction</th>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td colspan="6">
				<div id="embedded-table">
				<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
					<tr>
						<td class="column-label">&#160;Country:</td>
						<td class="column-label">&#160;State:</td>
						<td class="column-label">&#160;County:</td>
						<td class="column-label">&#160;City:</td>
						<td style="width:50%;">&#160;</td>
					</tr>
					<tr>
						<td class="column-input">
							<h:inputText id="selectedCountry" value="#{cacheManager.countryMap[nexusVendorBackingBean.selectedJurisdiction.countryCode]} (#{nexusVendorBackingBean.selectedJurisdiction.countryCode})" disabled="true" style="width:100px;" />
						</td>	
						<td class="column-input"><h:inputText id="selectedState" value="#{cacheManager.taxCodeStateMap[nexusVendorBackingBean.selectedJurisdiction.stateCode].name} (#{nexusVendorBackingBean.selectedJurisdiction.stateCode})" disabled="true" /></td>
						<td class="column-input"><h:inputText id="selectedCounty" value="#{nexusVendorBackingBean.selectedJurisdiction.county}" disabled="true" /></td>
						<td class="column-input"><h:inputText id="selectedCity" value="#{nexusVendorBackingBean.selectedJurisdiction.city}" disabled="true" /></td>
						<td style="width:50%;">&#160;</td>
					</tr>
					<tr>
						<td colspan="4" class="column-label">&#160;STJ:</td>
						<td style="width:50%;">&#160;</td>
					</tr>
					<tr>
						<td colspan="4" class="column-input"><h:inputText id="selectedStj" value="#{nexusVendorBackingBean.selectedJurisdiction.stj}" disabled="true" style="width:586px;" /></td>
						<td style="width:50%;">&#160;</td>
					</tr>
				</table>
				</div>
			</td>
	
		</tr> 
		
		<!--
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Country:</td>
			<td class="column-label">&#160;State:</td>
			<td class="column-label">&#160;County:</td>
			<td class="column-label">&#160;City:</td>
			<td class="column-label">&#160;STJ:</td>
			<td class="column-label">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-spacer">&#160;</td>
			<td class="column-input">
				<h:inputText id="selectedCountry" value="#{nexusVendorBackingBean.selectedJurisdiction.countryName} (#{nexusVendorBackingBean.selectedJurisdiction.countryCode})" disabled="true" />
			</td>	
			<td class="column-input"><h:inputText id="selectedState" value="#{cacheManager.taxCodeStateMap[nexusVendorBackingBean.selectedJurisdiction.stateCode].name} (#{nexusVendorBackingBean.selectedJurisdiction.stateCode})" disabled="true" /></td>
			<td class="column-input"><h:inputText id="selectedCounty" value="#{nexusVendorBackingBean.selectedJurisdiction.county}" disabled="true" /></td>
			<td class="column-input"><h:inputText id="selectedCity" value="#{nexusVendorBackingBean.selectedJurisdiction.city}" disabled="true" /></td>
			<td class="column-input"><h:inputText id="selectedStj" value="#{nexusVendorBackingBean.selectedJurisdiction.stj}" disabled="true" /></td>
			<td class="column-input">&#160;</td>
		</tr>
		 -->
		</h:panelGroup>
		 
		<h:panelGroup rendered="#{nexusVendorBackingBean.registrationTask}">
		<tr>
			<th colspan="7">&#160;Registration</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Effective Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusVendorBackingBean.updateOnlyAction}"
						value="#{nexusVendorBackingBean.updateRegistrationDetail.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusVendorBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="regeffDate" />
			</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. Type:</td>
			<td class="column-input" colspan="5">
				<h:selectOneMenu id="regType" disabled="#{!nexusVendorBackingBean.updateOnlyAction}" value="#{nexusVendorBackingBean.updateRegistrationDetail.regTypeCode}">
					<f:selectItems id="regTypeItms" value="#{nexusVendorBackingBean.regTypeItems}" />
				</h:selectOneMenu>
			</td>
		</tr>	
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Reg. No.:</td>
			<td class="column-input" colspan="5">
				<h:inputText disabled="#{!nexusVendorBackingBean.updateOnlyAction}" value="#{nexusVendorBackingBean.updateRegistrationDetail.regNo}" id="regNo" style="width:300px;" maxlength="100" />
			</td>
		</tr>	
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;Expiration Date:</td>
			<td colspan="5">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusVendorBackingBean.updateOnlyAction}"
						value="#{nexusVendorBackingBean.updateRegistrationDetail.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusVendorBackingBean.validateExpirationDate}"
						showApplyButton="false" id="regexpiryDate" />
			</td>
		</tr>
		</h:panelGroup>
		
		<h:panelGroup rendered="#{!nexusVendorBackingBean.registrationTask and !nexusVendorBackingBean.isRemoveNexusTask}">
		<tr>
			<th colspan="7">Definition</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Effective Date:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusVendorBackingBean.updateOnlyAction or nexusVendorBackingBean.nexusUsed}"
						value="#{nexusVendorBackingBean.updateVendorNexusDtl.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusVendorBackingBean.validateEffectiveDate}"
						showApplyButton="false" id="effDate" >
					
						<a4j:support event="onchanged" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusVendorBackingBean.effectiveDateChanged}"  />
						<a4j:support event="oninputblur" ajaxSingle="true" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusVendorBackingBean.effectiveDateChanged}"  />
						
				</rich:calendar>
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Nexus Type:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<h:selectOneMenu id="nexusType" disabled="#{!nexusVendorBackingBean.updateOnlyAction or nexusVendorBackingBean.nexusUsed}" value="#{nexusVendorBackingBean.updateVendorNexusDtl.nexusTypeCode}">
					<f:selectItems id="nexusTypeItms" value="#{nexusVendorBackingBean.nexusTypeItems}" />
					
					<a4j:support id="nexusType_handler" event="onchange" immediate="true" reRender="detailTable,pageInfo,okBtn" actionListener="#{nexusVendorBackingBean.nexusTypeChanged}"/>
					
				</h:selectOneMenu>
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Expiration Date:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">
				<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!nexusVendorBackingBean.updateOnlyAction}"
						value="#{nexusVendorBackingBean.updateVendorNexusDtl.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy" validator="#{nexusVendorBackingBean.validateExpirationDate}"
						showApplyButton="false" id="expiryDate" >
						
						<a4j:support event="onchanged" reRender="detailTable,okBtn" actionListener="#{nexusVendorBackingBean.expirationDateChanged}"  />
						<a4j:support event="oninputblur" ajaxSingle="true" reRender="detailTable,okBtn" actionListener="#{nexusVendorBackingBean.expirationDateChanged}"  />
						
				</rich:calendar>
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Active?:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" disabled="#{!nexusVendorBackingBean.updateOnlyAction}"
					value="#{nexusVendorBackingBean.updateVendorNexusDtl.activeBooleanFlag}" styleClass="check" >
					<a4j:support id="ajaxiCheck_activeFlagId" event="onclick" actionListener="#{nexusVendorBackingBean.selectActiveCheckChange}" >
					</a4j:support>				
				</h:selectBooleanCheckbox>
			</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-input">&#160;</td>
		</tr>
		
		</h:panelGroup>
		
		<h:panelGroup rendered="#{!nexusVendorBackingBean.registrationTask and !nexusVendorBackingBean.isAddNexusTask and !nexusVendorBackingBean.isRemoveNexusTask}">			
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Last&#160;Update&#160;User&#160;ID:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;" colspan="3" >
				<h:inputText value="#{nexusVendorBackingBean.updateVendorNexusDtl.updateUserId}" 
				         disabled="true" id="txtUpdateUserId1"
				         style="width:690px;" />	
			</td>    	 	 				 	 			 	     
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label" >Last&#160;Update&#160;Timestamp:</td>
			<td style="width:50px;" >&#160;</td>
			<td style="width:300px;" colspan="3" >
				<h:inputText value="#{nexusVendorBackingBean.updateVendorNexusDtl.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp1"
				         style="width:690px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>			
			</td>
			<td>&#160;</td>
		</tr>
		</h:panelGroup>
		
		
		</tbody>
	</table>
	
	<h:panelGroup rendered="#{nexusVendorBackingBean.registrationTask}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Last Update User ID:</td>
				<td>
				<h:inputText value="#{nexusVendorBackingBean.updateRegistrationDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{nexusVendorBackingBean.updateRegistrationDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</h:panelGroup>
	
	
	
	</div>
	</div>
	
	<h:panelGroup rendered="#{!nexusVendorBackingBean.isAddNexusTask and !nexusVendorBackingBean.isRemoveNexusTask}">
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn1" action="#{nexusVendorBackingBean.okProcessAction}" /></li>
			<li class="cancel"><h:commandLink id="cancelBtn1" immediate="true" action="#{nexusVendorBackingBean.cancelAction}" /></li>
		</ul>
	</div>
	</h:panelGroup>
	
</div>
	
<!-- datagrid for selected jurisdictions -->
<h:panelGroup rendered="#{nexusVendorBackingBean.isAddNexusTask or nexusVendorBackingBean.isRemoveNexusTask}">
<div id="bottom">
	<div id="table-four">		
		
 
		
		<div id="table-four-top" style="padding: 0 0 0 5px;" >
			
       		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;"><tr>
				<td align="left" width="10"><img style="#{(nexusVendorBackingBean.isDisplayAuto) ? '' : 'visibility:hidden'}" src="../images/icons/iconinfo.png" /></td>
				<td align="left" width="300"><h:outputText rendered="#{nexusVendorBackingBean.isDisplayAuto}" value="&#160;&#160;&#160;&#160;#{nexusVendorBackingBean.displayAutoMessage}" /></td>
				<td align="left" width="300">
					<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        		<a4j:status id="pageInfo"  
					startText="Request being processed..."  
					stopText="#{nexusVendorBackingBean.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
				</td>
				<td align="right" width="200"><a4j:commandLink  ajaxSingle="true" style="text-decoration: underline;color:#0033FF" immediate="true" reRender="detailTable" action="#{nexusVendorBackingBean.resetTableSorting}">
												<h:outputText value="default order" /></a4j:commandLink></td>
				<td style="width:20px;">&#160;</td>		
			</tr></table>	
	    </div>
		
		
		<div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner" id="resize" >
          <rich:dataTable rowClasses="odd-row,even-row" id="detailTable" headerClass="header" value="#{nexusVendorBackingBean.selectedJurisdictionList}" var="selectedJurisdiction">     	 
           	
           	<rich:column id="imageIcn" >
				<f:facet name="header"><h:outputText value=""/></f:facet>
				<h:graphicImage rendered="#{selectedJurisdiction.newItemFlag}" url="/images/icons/iconinfo.png" />
				<h:outputText rendered="#{!selectedJurisdiction.newItemFlag}" value="&#160;&#160;&#160;&#160;&#160;"  />

			</rich:column>
           	
           	<rich:column id="country" styleClass="column-left" sortBy="#{selectedJurisdiction.country}" width="150px" >
            	<f:facet name="header"><h:outputText id="country-a4j" styleClass="sort-local" value="Country"/></f:facet>
            	<h:outputText value="#{cacheManager.countryMap[selectedJurisdiction.country]} (#{selectedJurisdiction.country})"  />
           	</rich:column>
           	
           	<rich:column id="state" styleClass="column-left" sortBy="#{selectedJurisdiction.state}" width="150px" >
            	<f:facet name="header"><h:outputText id="state-a4j" styleClass="sort-local" value="State"/></f:facet>
            	<h:outputText value="#{cacheManager.taxCodeStateMap[selectedJurisdiction.state].name}    	
            		#{(not empty cacheManager.taxCodeStateMap[selectedJurisdiction.state].name) ? '(' : ''}#{selectedJurisdiction.state}#{(not empty cacheManager.taxCodeStateMap[selectedJurisdiction.state].name) ? ')' : ''}         	
            		" />
           	</rich:column>
           	
           	<rich:column id="county" styleClass="column-left" sortBy="#{selectedJurisdiction.county}" width="250px" >
            	<f:facet name="header"><h:outputText id="county-a4j" styleClass="sort-local" value="County"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.county}"  />
           	</rich:column>
           	
           	<rich:column id="city" styleClass="column-left" sortBy="#{selectedJurisdiction.city}" width="250px" >
            	<f:facet name="header"><h:outputText id="city-a4j" styleClass="sort-local" value="City"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.city}"  />
           	</rich:column>
           	
           	<rich:column id="stj" styleClass="column-left" sortBy="#{selectedJurisdiction.stj}" width="40%" >
            	<f:facet name="header"><h:outputText id="stj-a4j" styleClass="sort-local" value="STJ"/></f:facet>
            	<h:outputText value="#{selectedJurisdiction.stj}"  />
           	</rich:column>
           	
          
           	
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
	
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" immediate="true" disabled="#{!nexusVendorBackingBean.addRemoveNexus}" action="#{nexusVendorBackingBean.okProcessAction}" /></li>
			<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{nexusVendorBackingBean.cancelAction}" /></li>
		</ul>
	</div>
	
	</div>
</div>
</h:panelGroup>

</h:form>
</ui:define>
</ui:composition>
</html>