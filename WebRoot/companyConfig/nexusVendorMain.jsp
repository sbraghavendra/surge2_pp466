<ui:component xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">

	<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
		<ui:define name="script">
			<script type="text/javascript">
				//<![CDATA[
				registerEvent(window, "load", function() {
					selectRowByIndex('frmEntityMain:definitionTable',
							'definitionTableRowIndex');
				});
				registerEvent(window, "load", adjustHeight);
				//]]>
			</script>

			<style>
.rich-stglpanel-marker {
	float: left
}

.rich-stglpanel-header {
	text-align: left
}

.rich-tree-node-icon,.rich-tree-node-icon-leaf {
	display: none
}

.tree-node-selected {
	background-color: #99CCFF;
}

.selectBtn {
	cursor: pointer;
	cursor: hand;
	border: none;
	background-color: transparent;
	width: 55px;
	height: 16px;
	text-decoration: underline;
	color: #0033FF
}
</style>

</ui:define>
<ui:define name="body">
<f:view>
	<h:inputHidden id="definitionTableRowIndex"
		value="#{nexusVendorBackingBean.selectedDefinitionRowIndex}" />

	<h:form id="frmEntityMain">
		<h1>
			<h:graphicImage id="imgUserMap" alt="Nexus Definitions"
				value="/images/headers/hdr-vendornexus.png" />
		</h1>

		<div class="tab">

			<h:graphicImage
				url="/images/containers/STSSelection-filter-open.gif" />
			&#160;&#160;&#160;
			<a4j:commandLink id="toggleHideSearchPanel"
				style="height:22px;text-decoration: underline;color:#0033FF"
				reRender="showhidefilter,toggleHideSearchPanel"
				actionListener="#{nexusVendorBackingBean.togglePanelController.toggleHideSearchPanel}">
				<h:outputText
					value="#{(nexusVendorBackingBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
			</a4j:commandLink>
			<h:outputText
				value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;" />
			<h:outputText id="labelEntity1"
				value="#{nexusVendorBackingBean.updateVendorItemLabel}"
				style="width:300px;font-weight:bold;align:right; vertical-align:middle;" />

		</div>

		<a4j:outputPanel id="showhidefilter">
			<c:if
				test="#{!nexusVendorBackingBean.togglePanelController.isHideSearchPanel}">
				<div id="top">
					<div id="table-one">
						<a4j:outputPanel id="contentToggle" ajaxRendered="true"
							layout="block">
							<a4j:include ajaxRendered="true"
								viewId="/WEB-INF/view/components/nexus_panel.xhtml">
								<ui:param name="handlerBean"
									value="#{nexusVendorBackingBean}" />
								<ui:param name="readonly" value="false" />
							</a4j:include>
						</a4j:outputPanel>
					</div>
				</div>
			</c:if>
		</a4j:outputPanel>

		<div class="wrapper">
			<span class="block-right">&#160;</span> <span
				class="block-left tab"> <h:graphicImage
					url="/images/containers/STSView-definition-details.png"
					width="192" height="17" />
			</span>
		</div>

		<!-- table-four-contentNew  -->
		<div id="table-four-contentNew">

			<table cellpadding="0" cellspacing="0"
				style="width: auto; height: 100%">
				<tbody>
					<tr>
						<td style="height: auto; vertical-align: top;">

							<div id="bottom" style="padding: 0 0 3px 0;">
								<div id="table-four">
									<div id="table-four-top" style="padding: 3px 0 0 10px;">
										<table cellpadding="0" cellspacing="0"
											style="width: 100%; height: 22px;">
											<tr>
												<td align="left" width="150"><h:outputText
														style="font-weight: bolder; color:#336699;font-size: 11px;"
														value="Jurisdictions&#160;&#160;&#160;&#160;&#160;" />
												</td>
												
												<td align="center" width="200"><a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
												</td>

												<td align="right" width="20%"><a4j:commandLink
														id="clearall"
														style="height:22px; width:100px; align:right; text-decoration: underline;color:#0033FF"
														immediate="true" reRender="entityTreeId"
														actionListener="#{nexusVendorBackingBean.clearAllSelected}">
														<h:outputText value="clear all" />
													</a4j:commandLink> <h:outputText value="&#160;&#160;" />
												</td>

											</tr>
										</table>

									</div>

									<div id="table-four-top"
										style="padding: 3px 0 0 10px; background-color: #F0F8FE;">
										<table cellpadding="0" cellspacing="0"
											style="width: 100%; height: 22px;">
											<tr>
												<td align="left" width="300">
													<h:outputText style="font-size: 11px;"
														value="Click&#160;" />
													<h:outputText style="font-size: 11px;font-weight: bold"
														value="bold" />
													<h:outputText style="font-size: 11px;"
														value="&#160;Jurisdictions to display Nexus Definitions." />
													<br/>
													<h:outputText style="font-size: 11px;"
														value="Counties in gray inherit Nexus from the State." />
												</td>
											</tr>
										</table>

									</div>

									<div class="scrollInnerNew" id="resize"
										style="overflow-x: hidden; -ms-overflow-x: hidden; width: 445px;">
										<rich:tree id="entityTreeId"
											nodeSelectListener="#{nexusVendorBackingBean.processSelection}"
											changeExpandListener="#{nexusVendorBackingBean.processExpansion}"
											ajaxSubmitSelection="true" switchType="ajax"
											nodeFace="#{item.type}"
											reRender="entityTreeId,definitionTable,addDefBtn,removeDefBtn,updateBtn,deleteBtn"
											value="#{nexusVendorBackingBean.treeNode}" var="item"
											ajaxKeys="#{null}"
											binding="#{nexusVendorBackingBean.treeBinding}">

											<!--  	stateAdvisor="#{nexusTreeStateAdvisor}" > -->

											<rich:treeNode type="mapped"
												iconLeaf="/images/icons/entity.gif"
												icon="/images/icons/entity.gif">
												<h:selectBooleanCheckbox styleClass="check" id="mappedId"
													style="padding-right:0px;margin-right:5px;margin-left:0px; vertical-align:middle; border: none;"
													disabled="#{item.isCheckDisabled}"
													value="#{item.selectedFlag}"
													rendered="#{item.isAllowCheck}">
												</h:selectBooleanCheckbox>

												<h:outputText value="#{item.description}"
													style="#{(item.isCheckDisabled) ? 'color:grey' : 'color:black'};font-weight:bold;" />

												<a4j:commandButton id="mappedSelectAll" type="button"
													styleClass="selectBtn" disabled="#{item.disabledFlag}"
													value="select all" rendered="#{item.isAllowSelectAll}">
													<a4j:support event="onclick" reRender="entityTreeId"
														actionListener="#{nexusVendorBackingBean.selectAllClicked}">
														<f:param name="command" value="#{item.entityId}" />
													</a4j:support>
												</a4j:commandButton>

											</rich:treeNode>

											<rich:treeNode type="mappedselected"
												iconLeaf="/images/icons/entity.gif"
												icon="/images/icons/entity.gif">
												<h:selectBooleanCheckbox styleClass="check"
													id="mappedselectedId"
													style="padding-right:0px;margin-right:5px;margin-left:0px; vertical-align:middle; border: none;"
													disabled="#{item.isCheckDisabled}"
													value="#{item.selectedFlag}"
													rendered="#{item.isAllowCheck}">
												</h:selectBooleanCheckbox>

												<h:outputText value="#{item.description}"
													style="#{(item.isCheckDisabled) ? 'color:grey' : 'color:black'};font-weight:bold;background:lightblue" />

												<a4j:commandButton id="mappedselectedSelectAll"
													type="button" styleClass="selectBtn"
													disabled="#{item.disabledFlag}" value="select all"
													rendered="#{item.isAllowSelectAll}">
													<a4j:support event="onclick" reRender="entityTreeId"
														actionListener="#{nexusVendorBackingBean.selectAllClicked}">
														<f:param name="command" value="#{item.entityId}" />
													</a4j:support>
												</a4j:commandButton>

											</rich:treeNode>

											<rich:treeNode type="selected"
												iconLeaf="/images/icons/entity.gif"
												icon="/images/icons/entity.gif">
												<h:selectBooleanCheckbox styleClass="check"
													id="selectedId"
													style="padding-right:0px;margin-right:5px;margin-left:0px; vertical-align:middle; border: none;"
													disabled="#{item.isCheckDisabled}"
													value="#{item.selectedFlag}"
													rendered="#{item.isAllowCheck}">
												</h:selectBooleanCheckbox>

												<h:outputText value="#{item.description}"
													style="#{(item.isCheckDisabled) ? 'color:grey' : 'color:black'};Font;background:lightblue" />

												<a4j:commandButton id="mappedIdSelectAll" type="button"
													styleClass="selectBtn" disabled="#{item.disabledFlag}"
													value="select all" rendered="#{item.isAllowSelectAll}">
													<a4j:support event="onclick" reRender="entityTreeId"
														actionListener="#{nexusVendorBackingBean.selectAllClicked}">
														<f:param name="command" value="#{item.entityId}" />
													</a4j:support>
												</a4j:commandButton>

											</rich:treeNode>

											<rich:treeNode type="" iconLeaf="/images/icons/entity.gif"
												icon="/images/icons/entity.gif">
												<h:selectBooleanCheckbox styleClass="check" id="noneId"
													style="padding-right:0px;margin-right:5px;margin-left:0px; vertical-align:middle; border: none;"
													disabled="#{item.isCheckDisabled}"
													value="#{item.selectedFlag}"
													rendered="#{item.isAllowCheck}">
												</h:selectBooleanCheckbox>

												<h:outputText value="#{item.description}"
													style="#{(item.isCheckDisabled) ? 'color:grey' : 'color:black'}; vertical-align:middle;" />

												<a4j:commandButton id="noneIdSelectAll" type="button"
													styleClass="selectBtn" disabled="#{item.disabledFlag}"
													value="select all" rendered="#{item.isAllowSelectAll}">
													<a4j:support event="onclick" reRender="entityTreeId"
														actionListener="#{nexusVendorBackingBean.selectAllClicked}">
														<f:param name="command" value="#{item.entityId}" />
													</a4j:support>
												</a4j:commandButton>

											</rich:treeNode>
										</rich:tree>

									</div>
									<!-- scroll-inner -->


									<div id="table-four-bottom">
										<ul class="right">

											<li class="addnexusFour"><h:commandLink
													id="addDefBtn" disabled="false"
													action="#{nexusVendorBackingBean.displayAddNexusAction}" /></li>
											<li class="removenexusFour"><h:commandLink
													id="removeDefBtn" disabled="false"
													action="#{nexusVendorBackingBean.displayRemoveNexusAction}" /></li>
											<!--  <li class="addregistration"><h:commandLink id="addRegBtn" disabled="false" action="#{nexusVendorBackingBean.displayAddRegAction}" /></li> -->

										</ul>
									</div>
									<!-- table-four-bottom -->
									<!-- end custGrid -->

								</div>
							</div>
						</td>

						<td style="height: auto; vertical-align: top;">
							<div id="bottom" style="padding: 0 0 3px 0;">
								<div id="table-four" style="margin: 0;">
									<div id="table-four-top" style="padding: 3px 0 0 10px;">
										<table cellpadding="0" cellspacing="0"
											style="width: 100%; height: 22px;">
											<tr>
												<td align="left" width="150"><h:outputText
														style="font-weight: bolder; color:#336699;font-size: 11px;"
														value="Nexus&#160;Definitions&#160;&#160;&#160;&#160;&#160;" /></td>
												<td align="right" width="100%"><rich:modalPanel
														id="requestProcessed" zindex="2000" moveable="false"
														width="0" top="-30" height="0" left="-30"
														resizeable="false" /> <a4j:status id="pageInfo"
														startText="" stopText=""
														onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
														onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
												</td>

											</tr>
										</table>

									</div>

									<div class="scrollInner" id="resize1"
										style="width: 445px; min-height: 261px;">
										<rich:dataTable rowClasses="odd-row,even-row"
											id="definitionTable"
											value="#{nexusVendorBackingBean.vendorNexusDtlList}"
											var="item">
											<a4j:support event="onRowClick"
												onsubmit="selectRow('frmEntityMain:definitionTable', this);"
												actionListener="#{nexusVendorBackingBean.selectedDefinitionRowChanged}"
												immediate="true" reRender="updateBtn,deleteBtn" />

											<rich:column sortBy="#{item.effectiveDate}">
												<f:facet name="header">
													<h:outputText value="Effective Date"
														styleClass="sort-local" />
												</f:facet>
												<h:outputText id="roweffectiveDate" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888;'}"
													value="#{item.effectiveDate}">
													<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
												</h:outputText>
											</rich:column>

											<rich:column style="width:180px;"
												sortBy="#{item.nexusTypeCode}">
												<f:facet name="header">
													<h:outputText value="Nexus Type" styleClass="sort-local" />
												</f:facet>
												<h:outputText id="rownexusTypeCode" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888;'}"
													value="#{cacheManager.listCodeMap['NEXUSTYPE'][item.nexusTypeCode].description}" />
											</rich:column>

											<rich:column sortBy="#{item.expirationDate}">
												<f:facet name="header">
													<h:outputText value="Expiration Date"
														styleClass="sort-local" />
												</f:facet>
												<h:outputText id="rowexpirationDate"
													value="#{item.expirationDate}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888;'}" >
													<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
												</h:outputText>
											</rich:column>
											<rich:column id="activeFlag" sortBy="#{item.activeFlag}" >
					          					<f:facet name="header">
					          						<h:outputText id="activeFlag-a4j" styleClass="sort-local" value="Active?"/>
					          					</f:facet>
					          					<h:selectBooleanCheckbox value="#{item.activeBooleanFlag}" disabled="true" styleClass="check"  />
					         				</rich:column>
										</rich:dataTable>

									</div>
									<!-- scroll-inner -->

									<div id="table-four-bottom">
										<ul class="right">

											<li class="update"><h:commandLink id="updateBtn"
													disabled="#{nexusVendorBackingBean.disableUpdate}"
													action="#{nexusVendorBackingBean.displayUpdateAction}" /></li>
											<li class="delete115"><h:commandLink id="deleteBtn"
													disabled="#{nexusVendorBackingBean.disableDelete}"
													action="#{nexusVendorBackingBean.displayDeleteAction}" /></li>
											<li class="close"><h:commandLink id="closelBtn"
													action="vendor_main"></h:commandLink></li>


										</ul>
									</div>
									<!-- table-four-bottom -->
									<!-- end custLocnGrid -->

								</div>
								<!-- table-four -->
							</div> <!-- bottom -->

						</td>
					</tr>
				</tbody>
			</table>

		</div>
		<!-- end of table-four-contentNew  -->

	</h:form>
</f:view>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler"
		value="#{nexusVendorBackingBean.filterHandlerShipto}" />
	<ui:param name="popupName" value="searchJurisdiction" />
	<ui:param name="popupForm" value="searchJurisdictionForm" />
	<ui:param name="jurisId" value="jurisId" />
	<ui:param name="reRender" value="jurisInput" />
</ui:include>

</ui:define>
</ui:composition>
</ui:component>
