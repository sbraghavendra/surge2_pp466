<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="driverIndex" value="#{entityBackingBean.selectedFilterIndex}"/>

<h1><h:graphicImage id="imgEntityAdd" alt="Security Modules" value="/images/headers/hdr-entity.gif"  /></h1>

<h:form id="stsEntityAdd">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7">Copy Components to Entities</td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="3" style="width:300px;">Parent Level:&#160;<h:outputText value="#{entityBackingBean.entityItemDTO.parentLevelDescription}"/>
		</th>
		<th colspan="4">
							<h:outputText style="color:red;" value="Warning: All existing Components will be cleared and re-copied."/>
		</th>
		</tr>  
		<tr>
			<td>&#160;</td>
			<td>Code:</td>
			<td style="width:300px;">
				<h:inputText value="#{entityBackingBean.entityItemDTO.parentCode}" 
						disabled="true" id="txtParentCode" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Name:</td>
			<td style="width:100%;" colspan="4">
				<h:inputText value="#{entityBackingBean.entityItemDTO.parentName}" 
						disabled="true" id="txtParentName" style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Description:</td>
			<td style="width:100%;" colspan="4">
				<h:inputText value="#{entityBackingBean.entityItemDTO.parentLevelDescription}" 
						disabled="true" id="txtParentDescription" style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:200px;">
				<h:selectBooleanCheckbox id="txtParentActive" styleClass="check"  value="#{entityBackingBean.entityItemDTO.parentActiveFlag=='1' ? true:false}"  
						disabled="true" /></td>
            <td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>

		<tr><th colspan="7">This Level:</th></tr>
		
		<tr>
			<td>&#160;</td>
			<td>Level:</td>
			<td style="width:300px;">		
				<h:selectOneMenu id="entityLevelId" value="#{entityBackingBean.entityItemDTO.entityLevelId}" disabled="true"  style="width:300px;" >
  					<f:selectItems value="#{entityBackingBean.entityLevelList}" /> 
  					<a4j:support event="onchange" action="#{entityBackingBean.entityLevelChange}" reRender="btnOkAddChild"/>
  				</h:selectOneMenu></td>
			             
			<td style="width:100%;">&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Code:</td>
			<td style="width:300px;">
				<h:inputText id="entityCode" value="#{entityBackingBean.entityItemDTO.entityCode}" 
						 disabled="true"  style="width:300px;" /></td>
			             
			<td style="width:100%;">&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Name:</td>
			<td style="width:100%;" colspan="4">
				<h:inputText id="entityName" value="#{entityBackingBean.entityItemDTO.entityName}" 
						 disabled="true"  style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Description:</td>
			<td style="width:100%;" colspan="4">
				<h:inputText id="descriptionId" value="#{entityBackingBean.entityItemDTO.description}" 
						 disabled="true"  style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
			
	</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover"  >
	<tbody>
		<tr>
			<td>&#160;</td>
			<td>Configs:</td>
			<td>&#160;</td>
			<td>Components:</td>
			<td>&#160;</td>
			<td>Copy To:</td>
			<td style="width:100px;" >&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td><h:selectManyListbox id="pickComponentListOptionsId" value="#{entityBackingBean.pickComponentListResult}" style="width:200px;" >
  				<f:selectItems value="#{entityBackingBean.pickComponentListOptions}"/>
            	</h:selectManyListbox></td>
			<td>&#160;</td>
			<td><h:selectManyListbox id="pickEntityListOptionsId" size="5" value="#{entityBackingBean.pickEntityListResult}" style="width:200px;" >
  				<f:selectItems value="#{entityBackingBean.pickEntityListOptions}"/>
				</h:selectManyListbox></td>
			<td style="width:100px;" >&#160;</td>
		</tr>
	</tbody>
	</table>

	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{entityBackingBean.okCopyToAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{entityBackingBean.cancelAction}" /></li>
	</ul>
	</div>	
</div>	

</h:form>

</ui:define>
</ui:composition>
</html>