<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('frmVendorMain:entityItemsTable', 'entityTableRowIndex'); } );
registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="entityTableRowIndex" value="#{vendorBackingBean.selectedRowIndex}"/>

<h:form id="frmVendorMain">


<h:panelGroup rendered="#{vendorBackingBean.findMode}">
<h1><h:graphicImage id="image1"  alt="Lookup Vendor"  url="/images/headers/hdr-Lookup_vendor.png"></h:graphicImage>  </h1>
</h:panelGroup>
		
<h:panelGroup rendered="#{!vendorBackingBean.findMode}">	
<h1><h:graphicImage id="imgUserMap" alt="Vendors" value="/images/headers/hdr-vendor.png" /></h1>
</h:panelGroup>

<div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{vendorBackingBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(vendorBackingBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	</a4j:commandLink>					
</div> 

<a4j:outputPanel id="showhidefilter">
    	<c:if test="#{!vendorBackingBean.togglePanelController.isHideSearchPanel}">
     	<div id="top">
      	<div id="table-one">
      		
			<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/vendors_panel.xhtml">
					<ui:param name="handlerBean" value="#{vendorBackingBean}"/>
					<ui:param name="readonly" value="false"/>					
				</a4j:include>
			</a4j:outputPanel >
			<!--
			<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
				<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Jurisdiction:</td>
					<td colspan="5">
						<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
							<ui:param name="handler" value="#{vendorBackingBean.filterHandlerShipto}"/>
							<ui:param name="id" value="jurisInput"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="showheaders" value="true"/>
							<ui:param name="popupName" value="searchJurisdiction"/>
							<ui:param name="popupForm" value="searchJurisdictionForm"/>
						</ui:include>
					</td>
					<td>&#160;</td>
				</tr>
				</tbody>
			</table>
			-->
      	</div>
     	</div>
	</c:if>
</a4j:outputPanel>
    
<div class="wrapper">
  <span class="block-right">&#160;</span>
  <span class="block-left tab">
   <h:graphicImage url="/images/containers/STSView-vendors.png" width="192"  height="17" />
  </span>
</div>


<div id="bottom" >
<div id="table-four">
<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
		<tbody>
			<tr>
			<td style="width:60px;">&#160;</td>
			<td align="left" >
				<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
			        <a4j:status id="pageInfo"  
							startText="Request being processed..." 
							stopText="#{vendorBackingBean.vendorDataModel.pageDescription }"
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
			     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			</td>
			<td style="width:20px;">&#160;</td>
			</tr>
		</tbody>
	</table>
</div>	

	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
		<!--  
	  	<rich:dataTable rowClasses="odd-row,even-row" id="entityItemsTable" value="#{entityBackingBean.entityItemsList}" 
	    	var="ei" sortMode="single" >
	    -->
	    <rich:dataTable rowClasses="odd-row,even-row" id="entityItemsTable" rows="#{vendorBackingBean.vendorDataModel.pageSize }"  
	    		value="#{vendorBackingBean.vendorDataModel}" var="ei">

	    
		<a4j:support id="clk" event="onRowClick" status="rowstatus"
			onsubmit="selectRow('frmEntityMain:entityItemsTable', this);"
			actionListener="#{vendorBackingBean.selectedEntityRowChanged}" 
			reRender="okBtn,addBtn,copyaddBtn,updateBtn,deleteBtn,viewBtn,vendorutilitiesId,NexusRegisId,DefaultsId,ProductGandSId,ProductGandSId1,customersId,CopyToId" />

		<rich:column id="vendorCode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="vendorCode-a4j" value="Code" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['vendorCode']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.vendorCode}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>
		
		<rich:column id="vendorName" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="vendorName-a4j" value="Name" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['vendorName']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.vendorName}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>

		<rich:column id="geocode" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="geocode-a4j" value="GeoCode" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['geocode']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.geocode}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>
		
		<rich:column id="country" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="country-a4j" value="Country" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['country']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{cacheManager.countryMap[ei.country]} #{(not empty ei.country) ? '(' : ''}#{ei.country}#{(not empty ei.country) ? ')' : ''}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}"  /> 	         
        </rich:column>

		<rich:column id="state" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="state-a4j" value="State" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['state']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{cacheManager.taxCodeStateMap[ei.state].name} #{(not empty cacheManager.taxCodeStateMap[ei.state].name) ? 
						'(' : ''}#{ei.state}#{(not empty cacheManager.taxCodeStateMap[ei.state].name) ? ')' : ''}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
 	
        </rich:column>
		
		<rich:column id="county" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="county-a4j" value="County" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['county']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.county}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>
		
		<rich:column id="city" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="city-a4j" value="City" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['city']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.city}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>
		
		<rich:column id="zip" styleClass="column-left">
            <f:facet name="header">
            	<a4j:commandLink id="zip-a4j" value="Zip Code" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['zip']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:outputText value="#{ei.zip}#{(not empty ei.zipplus4) ? '-' : ''}#{ei.zipplus4}" style="#{(ei.activeFlag) == 1 ? '' : 'color:#888888;'}" />
        </rich:column>

		<rich:column id="activeFlag" styleClass="column-left" style="text-align:center;">
            <f:facet name="header">
            	<a4j:commandLink id="activeFlag-a4j" value="Active?" styleClass="sort-#{vendorBackingBean.vendorDataModel.sortOrder['activeFlag']}"                            
                	actionListener="#{vendorBackingBean.sortAction}" oncomplete="initScrollingTables();" immediate="true" reRender="entityItemsTable" />
           	</f:facet>
           	<h:selectBooleanCheckbox value="#{ei.activeBooleanFlag}" disabled="true" styleClass="check"/>
        </rich:column>
		
	  	</rich:dataTable>	
	  	
	  	<a4j:outputPanel id="messageTest">
          <c:if test="#{vendorBackingBean.vendorDataModel.rowCount == 0}">
				<div class="nodatadisplay1"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
          
	</div>
 	</div>
	</div> <!-- end of table-four-content -->
	
	<rich:datascroller id="trScroll"  for="entityItemsTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{vendorBackingBean.vendorDataModel.curPage}" />
		
	<div id="table-four-bottom">

		<ul id="nav-up" class="right" >
			<h:panelGroup rendered="#{!vendorBackingBean.findMode}">
				<li style=" margin-left: -20px;" ><span class="configure" ><h:commandLink value="" id="vendorutilitiesId" disabled="#{vendorBackingBean.disableUpdate or vendorBackingBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#" /></span>
					<ul style="margin-top: -15px;  margin-left: 0px; width:115px;">
	  
				        <li class="nexus-rounded" ><h:commandLink immediate="true" id="NexusRegisId" action="#{vendorBackingBean.displayNexusDefinitionAction}"/></li>
				    
					</ul>	      
				</li>
			</h:panelGroup>	
		</ul>
	  	<ul class="right">
		  	<h:panelGroup rendered="#{!vendorBackingBean.findMode}">
				<li class="add"><h:commandLink id="addBtn" style="#{(vendorBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled = "#{vendorBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{vendorBackingBean.displayAddAction}" /></li>
	        	<li class="copy-add"><h:commandLink id="copyaddBtn" style="#{(vendorBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{vendorBackingBean.disableCopyAdd or vendorBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{vendorBackingBean.displayCopyAddAction}" /></li>       
				<li class="update"><h:commandLink id="updateBtn" style="#{(vendorBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{vendorBackingBean.disableUpdate or vendorBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{vendorBackingBean.displayUpdateAction}" /></li>
				<li class="delete115"><h:commandLink id="deleteBtn" style="#{(vendorBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{vendorBackingBean.disableDelete or vendorBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{vendorBackingBean.displayDeleteAction}" /></li>
				<li class="view"><h:commandLink id="viewBtn" disabled="#{vendorBackingBean.disableView}" action="#{vendorBackingBean.displayViewAction}" /></li>
			</h:panelGroup>
			<h:panelGroup rendered="#{vendorBackingBean.findMode}">
				<li class="ok2"><h:commandLink id="okBtn" style="#{(vendorBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{vendorBackingBean.disableUpdate or vendorBackingBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{vendorBackingBean.findAction}"/></li>
				<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{vendorBackingBean.cancelFindAction}" /></li>
			</h:panelGroup>
			
	   	</ul>
	</div>	
	
</div>	
</div>
</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{vendorBackingBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="frmVendorMain:jurisInput"/>
</ui:include>

<!--  
<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{entityBackingBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdictionShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipto"/>
	<ui:param name="jurisId" value="jurisIdShipto"/>
	<ui:param name="reRender" value="jurisInputShipto"/>
</ui:include>
-->

</ui:define>
</ui:composition>
</html>