<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

	<ui:define name="script">
		<script type="text/javascript">
	//<![CDATA[
	           var currentPanel = "n/a";
function changePanel(menu) {
	var obj = document.getElementById(currentPanel);
	if (obj) obj.style.display = "none";
	obj = document.getElementById(menu.value);
	if (obj) obj.style.display = "block";
	currentPanel = menu.value;
	//]]>
	</script>
	</ui:define>

	<ui:define name="body">
		<h:form id="fisYearUpdateForm">
			<div id="top">
				<div id="table-one">
					<div id="table-four-top" style="padding: 1px 10px 1px 2px;">
						<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;"><tr>
							<td ><h:messages errorClass="error" /></td>
							<td style="width:100%;">&#160;</td>
							<td width="70"><h:outputText style="color:red;" value="#{(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction) ? '*Mandatory&#160;&#160;&#160;&#160;' : '' }"/></td>							
						</tr></table>
					</div>

					<div id="table-one-content">
						<table cellpadding="0" cellspacing="0" width="913" id="rollover"
							class="ruler">
							<thead>
								<tr>
									<td colspan="7"><h:outputText
											value="#{fiscalYearBean.actionText}" /> a Fiscal Year</td>
								</tr>
							</thead>
							<tbody>
								<h:panelGroup id="fisOpenvalue" rendered="#{fiscalYearBean.actionText=='Open'}">
									<tr>
										<th colspan="1"
											style="width: 200px; color: red; font-weight: bold;">&#160;Open&#160;Fiscal&#160;Period:&#160;&#160;<h:outputText
												value="#{fiscalYearBean.actionOpenText}" /> ?
										</th>
										<th colspan="6">&#160;</th>
									</tr>
								</h:panelGroup>
								<h:panelGroup id="fisDeleteValue"
									rendered="#{fiscalYearBean.displayDeleteAction or fiscalYearBean.displayCloseAction}">
									<tr>
										<th colspan="4"
											style="width: 300px; color: red; font-weight: bold;">&#160;
											<h:outputText value="#{fiscalYearBean.actionText}" />&#160;Fiscal&#160;Period(s)?
										</th>
										<th colspan="3">&#160;</th>
									</tr>
								</h:panelGroup>
								<h:panelGroup id="fisOpen"
									rendered="#{!(fiscalYearBean.displayOpenAction or fiscalYearBean.displayDeleteAction or fiscalYearBean.displayCloseAction)}">
									<tr>
										<th>&#160;</th>
										<th colspan="2" style="width: 80px;">&#160;Year&#160;Options&#160;</th>
										<th colspan="4" style="width: 90%;">&#160;</th>
									</tr>

									<tr>
										<td>&#160;</td>
										<td style="text-align: right; color: red;">*</td>
										<td>Fiscal Year:</td>
										<td><h:inputText id="fisYear" style="width:100px;"
												onkeypress="return onlyIntegerValue(event);"
												value="#{fiscalYearBean.updateFisYear.fiscalYear}"
												disabled="#{fiscalYearBean.displayUpdateAction}">
												<f:convertNumber integerOnly="true" pattern="####" />
											</h:inputText></td>
										<td style="width: 20%;">&#160;</td>
										<td style="width: 220px;">PD = Period Number</td>
										<td style="width: 300px;">YY = Two digit Fiscal Year
											Number</td>
									</tr>

									<tr>
										<td>&#160;</td>
										<td>&#160;</td>
										<td>Description:</td>
										<td style="width: 300px;"><h:inputText id="description"
												style="width:300px;"
												value="#{fiscalYearBean.updateFisYear.description}" /></td>
										<td style="width: 20%;">&#160;</td>
										<td style="width: 220px;">MM = Month Number</td>
										<td style="width: 300px;">YYYY = Full Fiscal Year Number</td>
									</tr>

									<tr>
										<td>&#160;</td>
										<td>&#160;</td>
										<td>First Month:</td>
										<td style="width: 150px;"><h:selectOneMenu
												id="firstMonth"
												value="#{fiscalYearBean.updateFisYear.firstMonth}">
												<f:selectItems value="#{fiscalYearBean.firstMonthItems}" />
											</h:selectOneMenu></td>
										<td style="width: 20%;">&#160;</td>
										<td style="width: 220px;">MON = Short Month Name</td>
										<td style="width: 300px;">YR = Two digit Calendar Year
											Number</td>
									</tr>

									<tr>
										<td>&#160;</td>
										<td>&#160;</td>
										<td>First Year:</td>
										<td style="width: 150px;"><h:selectOneMenu id="firstYear"
												value="#{fiscalYearBean.updateFisYear.firstYear}">
												<f:selectItems value="#{fiscalYearBean.firstYearItems}" />
											</h:selectOneMenu></td>
										<td style="width: 20%;">&#160;</td>
										<td style="width: 220px;">MONTH = Full Month Name</td>
										<td style="width: 300px;">YEAR = Full Calendar Year
											Number</td>
									</tr>

									<tr>
										<td colspan="7">&#160;</td>
									</tr>
								</h:panelGroup>

								<h:panelGroup id="fisDelete"
									rendered="#{fiscalYearBean.actionText!='Open' and !fiscalYearBean.displayDeleteAction and !fiscalYearBean.displayCloseAction}">
									<tr>
										<th>&#160;</th>
										<th colspan="3" style="width: 70px;">&#160;Results</th>
										<th width="100%" colspan="3">&#160;</th>
									</tr>
								</h:panelGroup>
							</tbody>
						</table>
						
						<h:panelGroup rendered="#{fiscalYearBean.actionText!='Open'}" >

						<a4j:outputPanel id="forEachPanelYear" >
							<table cellpadding="0" cellspacing="0" width="100%" id="rollover"
								class="ruler">
								<tbody>
									<tr>
										<th>&#160;</th>
										<th>&#160;</th>
										<th width="190px">Year_Period</th>
										<th style="text-align: right;"><h:outputText style="color:red;" value="#{(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction) ? '*' : '&#160;' }"/></th>										
										<th width="300px"><h:outputText style="width: 300px;"
												value="Description" /></th>
										<th width="30%">&#160;</th>
										<th width="15px"><a4j:commandLink id="addYearResult"
												styleClass="button"
												rendered="#{fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction}"
												reRender="forEachPanelYear"
												actionListener="#{fiscalYearBean.processAddYearResultCommand}">
												<h:graphicImage value="/images/addButton.png" />
											</a4j:commandLink> <h:outputText style="width: 15px;" value="&#160;"
												rendered="#{!(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction)}" />
										</th>
										<th width="50px">&#160;</th>
									</tr>

									<c:forEach var="fisYearResult"
										items="#{fiscalYearBean.retrievePeriodList}"
										varStatus="loopCounter">
										<tr>
											<td>&#160;</td>
											<td>&#160;</td>
											<td width="150px"><h:inputText
													id="fieldYearPeriod_#{loopCounter.index}"
													style="width: 150px;" value="#{fisYearResult.yearPeriod}"
													disabled="#{true}" /></td>
											<td width="10px">&#160;</td>
											<td width="300px"><h:inputText
													id="fieldDescription_#{loopCounter.index}"
													style="width: 300px;" value="#{fisYearResult.description}"
													disabled="#{!(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction)}" />
											</td>

											<td width="30%">&#160;</td>
											<td width="15px"><a4j:commandLink
													id="removeYearResult_#{loopCounter.index}"
													styleClass="button"
													rendered="#{(loopCounter.index eq fiscalYearBean.lastYearResultObject) and (fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction)}"
													reRender="forEachPanelYear"
													actionListener="#{fiscalYearBean.processRemoveYearResultCommand}">
													<h:graphicImage value="/images/deleteButton.png" />
													<f:param name="command" value="#{loopCounter.index}" />
												</a4j:commandLink> <h:outputText style="width: 15px;" value="&#160;"
													rendered="#{!(loopCounter.index eq fiscalYearBean.lastYearResultObject) or !(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction)}" />
											</td>
											<td width="50px">&#160;</td>
										</tr>
									</c:forEach>

									<tr>
										<td colspan="8">&#160;</td>
									</tr>
								</tbody>
							</table>
						</a4j:outputPanel>
						</h:panelGroup>
						
					</div>
				</div>

				<div id="table-four-bottom">
					<ul class="right">
						<li class="populate"><h:commandLink id="populatedata"
								disabled="#{!(fiscalYearBean.displayAddAction or fiscalYearBean.displayUpdateAction)}"
								action="#{fiscalYearBean.okFisAndAddAction}" /></li>
						<li class="ok"><h:commandLink id="btnOk" value=""
								action="#{fiscalYearBean.okProcessTableAction}" /></li>
						<li class="cancel"><h:commandLink id="btnCancel"
								immediate="true" action="#{fiscalYearBean.cancelAction}" /></li>
					</ul>
				</div>
			</div>
		</h:form>
	</ui:define>
</ui:composition>
</html>