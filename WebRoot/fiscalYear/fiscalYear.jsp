<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('fiscalForm:fiscalYearTable', 'selectedfisYearRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('fiscalForm:periodYearTable', 'selectedyearPeriodsRowIndex'); } );
registerEvent(window, "load", adjustHeight);
function resetState()
{ 
    document.getElementById("fiscalForm:stateMenuMain").value = "";
}

//]]>
</script>
<style>
.rich-stglpanel-marker {float: left}
.rich-stglpanel-header {text-align: left}
</style>
</ui:define>
<ui:define name="body">
<f:view>
<h:inputHidden id="selectedfisYearRowIndex" value="#{fiscalYearBean.selectedfisYearRowIndex}" />
<h:inputHidden id="selectedyearPeriodsRowIndex" value="#{fiscalYearBean.selectedyearPeriodsRowIndex}" />
     <h:form id="fiscalForm">
     <h1><h:graphicImage id="imageFiscalYear" url="/images/headers/hdr-FiscalYears.png"  width="250" height="19"></h:graphicImage></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160; 
     	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{fiscalYearBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(fiscalYearBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       	</a4j:commandLink>					
     </div> 
		<a4j:outputPanel id="showhidefilter">
     	<c:if test="#{!fiscalYearBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/fiscal_panel.xhtml">
						<ui:param name="fiscalYearBean" value="#{fiscalYearBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel >
	      	</div>
	     	</div>
		</c:if>
	</a4j:outputPanel>			
	  <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/subhdr-YearsAndPeriods.png" width="192"  height="17" />
      </span>
    </div>
	<!-- table-four-contentNew  -->     
	<div id="table-four-contentNew">
        
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
    <tbody>
	<tr>
	<td style="height:auto;vertical-align:top;">
 
    <div id="bottom" style="padding: 0 0 3px 0;">
    	<div id="table-four"  >
	    <div id="table-four-top" style="padding: 3px 0 0 10px;" >
         <table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>					 
					<td align="left" >
						<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Fiscal Years&#160;&#160;&#160;&#160;&#160;" />
					</td>
					<td align="left" width="300" >
						<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
				        	<a4j:status id="pageInfo"  
								startText="Request being processed..." 
								stopText="#{fiscalYearBean.fisYearDataModel.pageDescription }"
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
				     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
					</td>
					<td>&#160;</td>
					</tr>
				</tbody>
			</table>	
	    </div>
       
        <div class="scrollInnerNew" id="resize" style="width:400px;">
       	<rich:dataTable rowClasses="odd-row,even-row" id="fiscalYearTable" value="#{fiscalYearBean.fisYearDataModel}" rows="#{procruleBean.procruleDataModel.pageSize }" 
       	  var="fisYearMaintenance">
       	 
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus" 
                        onsubmit="selectRow('fiscalForm:fiscalYearTable', this);"  actionListener="#{fiscalYearBean.selectedfisYearRowChanged}"
                        reRender="openYear,closeYear,periodYearTable,addprocrule,updateprocrule,deleteprocrule,procruleDetailDataTest" />
            <rich:column id="selectYear" styleClass="column-center">
            	<f:facet name="header">
             	<h:outputText value="Select" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" id="chkYearSelected" value="#{fisYearMaintenance.selectedYear}">	
            	<a4j:support event="onclick" actionListener="#{fiscalYearBean.displayChangeFirst}" />
            	</h:selectBooleanCheckbox>
           	</rich:column>
          	<rich:column id="fiscalYear" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="fiscalYearId-a4j"
                              styleClass="sort-#{fiscalYearBean.fisYearDataModel.sortOrder['fiscalYear']}"
                              value="Year"
                              actionListener="#{fiscalYearBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="fiscalYearTable" />
            	</f:facet>
           	  	<h:outputText value="#{fisYearMaintenance.fiscalYear}" style="#{(fisYearMaintenance.checkCount)? 'color:#888888;' :''}"/>
           	</rich:column>
           	
           	
         </rich:dataTable>
          
          <a4j:outputPanel id="custDataTest">
          <c:if test="#{fiscalYearBean.fisYearDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
         
         </div>
         <!-- scroll-inner -->

       <rich:datascroller id="trScroll"  for="fiscalYearTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false" reRender="pageInfo" page="#{fiscalYearBean.fisYearDataModel.curPage}" />
                                  
       <div id="table-four-bottom">
        	<ul class="right">
           	<li class="add"><h:commandLink id="addprocrule" style="#{(fiscalYearBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{fiscalYearBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{fiscalYearBean.displayAddFisYearAction}" ></h:commandLink></li>          
         	<li class="update"><h:commandLink id="updateprocrule" style="#{(fiscalYearBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!fiscalYearBean.displayProcruleButtons or fiscalYearBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{fiscalYearBean.displayUpdateFisYearAction}" ></h:commandLink></li>
         	<li class="delete115"><h:commandLink id="deleteprocrule" style="#{(fiscalYearBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!fiscalYearBean.displayProcruleButtons or fiscalYearBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{fiscalYearBean.displayDeleteFisYearAction}" ></h:commandLink></li>
         	
        	</ul>
       </div> <!-- table-four-bottom -->   
       <!-- end custGrid -->
       
       </div>
    </div>
	</td>
	<td style="height:auto;vertical-align:top;" >
  	<div id="bottom" style="padding: 0 0 3px 0;">
       <div id="table-four"  style="margin:0;" >
       <div id="table-four-top" style="padding: 3px 0 0 10px;">
      
       		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				
			<tbody>
					<tr>					 
					<td align="left" >
						<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Periods&#160;&#160;&#160;" />
					</td>
					<td align="right" width="100" valign="baseline">
						<!--a4j:commandLink id="showTextSqlPanel" style="height:22px;color:#0033FF; text-decoration: underline; "  oncomplete="initScrollingTables();"
       							reRender="periodYearTable">
       						
       					</a4j:commandLink>-->
					</td>
					<td style="width:25px;">&#160;</td>
					</tr>
				</tbody>
			</table>	
       </div>
        <div class="scrollInner"  id="resize1" style="width:520px;">
         	<rich:dataTable rowClasses="odd-row,even-row" id="periodYearTable" value="#{fiscalYearBean.selectedFisYearList}" var="periodsYearMaintenance" >
         	<a4j:support id="ref2tbl2"  event="onRowClick" status="rowstatus" 
                        onsubmit="selectRow('fiscalForm:periodYearTable', this);"  actionListener="#{fiscalYearBean.selectedyearPeriodsRowIndex}"
                        reRender="openYear,closeYear"/>
                <rich:column id="selectPeriod" styleClass="column-center">
            	<f:facet name="header">
             	<h:outputText value="Select" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" id="chkPeriodSelected" value="#{periodsYearMaintenance.selectedPeriod}">
            	<a4j:support event="onclick" actionListener="#{fiscalYearBean.displayChange}" />
            	
            		
				</h:selectBooleanCheckbox>
            </rich:column>        
           	<rich:column id="yearPeriod" styleClass="column-left">
            	<f:facet name="header">
            	<h:outputText value="Year_Period" />
             	</f:facet>
            	<h:outputText value="#{periodsYearMaintenance.yearPeriod}" style="#{((periodsYearMaintenance.closeFlag) == 0 || (periodsYearMaintenance.closeFlag) == '')? '' : 'color:#888888;'}"/>
           	</rich:column>
           	<rich:column id="description" styleClass="column-left">
            	<f:facet name="header">
            	<h:outputText value="Description" />
             	  	</f:facet>
            	<h:outputText value="#{periodsYearMaintenance.description}" style="#{((periodsYearMaintenance.closeFlag) == 0 || (periodsYearMaintenance.closeFlag) == '')? '' : 'color:#888888;'}"/>
           	</rich:column>
           	<rich:column id="closeFlagfiscalPeriod" styleClass="column-center">
            	<f:facet name="header">
             	<h:outputText value="Closed" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" value="#{periodsYearMaintenance.closeFlag == 1}" disabled="#{true}" id="closeFlagPeriodId" />
           		</rich:column>
           	
         </rich:dataTable>

          <a4j:outputPanel id="procruleDetailDataTest">
           		<c:if test="#{fiscalYearBean.fisYearDataModel.rowCount == 0}">
					<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          		</c:if>
          		<c:if test="#{fiscalYearBean.selectedfisYearRowIndex > 0 and fiscalYearBean.fisYearDataModel.rowCount == 0 and fiscalYearBean.fisYearDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Fiscal Year selected" /></div>
          		</c:if>
          		<c:if test="#{fiscalYearBean.selectedfisYearRowIndex == -1 and fiscalYearBean.fisYearDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Fiscal Year selected" /></div>
          		</c:if>
           </a4j:outputPanel>
       </div>
	
        <!-- scroll-inner -->
                          
       	<div id="table-four-bottom">
        	<ul class="right">
            <li class="openPeriod">
           	<h:commandLink id="openYear"  style="#{(fiscalYearBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!(fiscalYearBean.disableOnCheckedOpen or fiscalYearBean.currentUser.viewOnlyBooleanFlag)}"  immediate="true" action="#{fiscalYearBean.displayOpenAction}"/></li>
           	<li class="closePeriod">
         	<h:commandLink id="closeYear" style="#{(fiscalYearBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!(fiscalYearBean.disableOnCheckedClosed or fiscalYearBean.currentUser.viewOnlyBooleanFlag)}" immediate="true" action="#{fiscalYearBean.displayCloseAction}"/></li>
             </ul>
       	</div> <!-- table-four-bottom -->  
       	<!-- end custLocnGrid --> 

      	</div>  <!-- table-four -->   
    </div>   <!-- bottom -->
	
	</td>
	</tr>
	</tbody>
	</table>

	</div>
    <!-- end of table-four-contentNew  -->  
	 </h:form>
	 </f:view>
</ui:define>

</ui:composition>

</html>
