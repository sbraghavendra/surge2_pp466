<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpRPForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">RatePoint</td></tr>
		</thead>
		<tbody>
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Lat/Long Lookup?:</td>
				<td>

				<h:selectBooleanCheckbox styleClass="check" id="ratepointLatlongLookup" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.ratepointLatlongLookup}" > 
				  	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
                </h:selectBooleanCheckbox>

				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Generic Lookup?:</td>
				<td>
				<h:selectOneMenu id="genericLookup" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
						value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.genericJurApi}" style="width: 200px;">
        		<f:selectItems id="genericLookupItms" value="#{filePreferenceBackingBean.genericLookupList}" />
				  	<a4j:support event="onchange"  ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" /></h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>		
		
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Default Jurisdiction: </td> 
				<td class="column-input-search">  
					<h:inputText id="jurisdictionId" style="text-align:left;float:left;width:120px" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
						value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.ratepointDefaultJurisdiction}"
						onkeypress="return onlyIntegerValue(event);" label="Jurisdiction ID">
						<f:convertNumber integerOnly="true" groupingUsed="false" />
						<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
		
						 
					</h:inputText>
					<h:commandButton id="btnJurisdictionId" styleClass="image" style="float:left;width:16px;height:16px;" 
						image="/images/search_small.png" immediate="true" 
						action="#{filePreferenceBackingBean.findJurisdictionIdAction}">
					</h:commandButton>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			
			<tr style="width: 700px;">
					<td style="width:50px;">&#160;</td>
					<td class="column-label">Default TaxCode: </td> 
					<td colspan="2" class="column-spacer">
						<ui:include src="/WEB-INF/view/components/taxcode_code.xhtml">
							<ui:param name="handler" value="#{filePreferenceBackingBean.filterTaxCodeHandlerDefault}"/>
							<ui:param name="id" value="filterTaxCodeDefault"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCodeDefault"/>
							<ui:param name="popupForm" value="searchTaxCodeFormDefault"/>
							<ui:param name="reRender" value="updateButton,cancelButton,errorMessage"/>
						</ui:include>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			
			<!--  
			<tr>
				<td class="column-label">Default TaxCode: </td> 
				<td colspan="2" class="column-spacer">
					<ui:include src="/WEB-INF/view/components/taxcode_preference_lookup.xhtml">
						<ui:param name="handler" value="#{filePreferenceBackingBean.filterTaxCodeHandlerDefault}"/>
						<ui:param name="id" value="filterTaxCodeDefault"/>
						<ui:param name="readonly" value="false"/>
						<ui:param name="required" value="false"/>
						<ui:param name="allornothing" value="false"/>
						<ui:param name="showheaders" value="false"/>
						<ui:param name="forId" value="true"/>
						<ui:param name="popupName" value="searchTaxCodeDefault"/>
						<ui:param name="popupForm" value="searchTaxCodeFormDefault"/>	
						<ui:param name="disabledSearchButton" value="false"/>
					</ui:include>
				</td>
				<td class="column-spacer">&#160;
			
                   </td>
			</tr>
			-->
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
	   <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>      
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemRatePointAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemRatePointAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>

<!--  
<ui:include src="/WEB-INF/view/components/taxcode_search.xhtml">
	<ui:param name="handler" value="#{filePreferenceBackingBean.filterTaxCodeHandlerDefault}"/>
	<ui:param name="popupName" value="searchTaxCodeDefault"/>
	<ui:param name="popupForm" value="searchTaxCodeFormDefault"/>
	 <ui:param name="forPreference" value="true"/>
	<ui:param name="reRender" value="fpRPForm:filterTaxCodeDefault"/>
</ui:include>
-->

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{filePreferenceBackingBean.jurisdictionHandler}"/>
	<ui:param name="showTaxRates" value="false"/>
	<ui:param name="popupName" value="searchJurisdictionID"/>
	<ui:param name="popupForm" value="searchJurisdictionFormID"/>
	<ui:param name="jurisId" value="ratesJurisId"/>
	<ui:param name="reRender" value="fpRPForm:jurisdictionId"/>
</ui:include>


</f:view>
</ui:define>
</ui:composition>
</html>