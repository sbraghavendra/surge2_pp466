<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
function onlyIntegerValue(evt)
{
	var e = (typeof event!=='undefined')? event:evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;

}
//]]>
</script>
</ui:define>



<ui:define name="body">
<f:view>
<h:form id="fpSMForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Matrix</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Drivers</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Minimum G&amp;S Drivers Selected:</td>
				<td style="width:700px;">
				<h:inputText 
				    id="minTaxDrvs" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				    onkeypress="return onlyIntegerValue(event);"
				    value="#{filePreferenceBackingBean.matrixDTO.sysMinDriver}" 
				    size="25"
					label="Minimum Drivers" >				
					<f:convertNumber integerOnly="true" />
					<a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>
			</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Minimum Location Drivers Selected:</td>
				<td style="width:700px;">
				<h:inputText 
				    id="minLocationDrvs" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				    onkeypress="return onlyIntegerValue(event);"
				    value="#{filePreferenceBackingBean.matrixDTO.sysMinLocationDriver}" 
				    size="25"
				    immediate="true"
					label="Minimum Drivers"
					> 
                    <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     					
					<f:convertNumber integerOnly="true" />
				</h:inputText>
			</td>
			</tr>			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Minimum Allocation Drivers Selected:</td>
				<td style="width:700px;">
				<h:inputText 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				    id="minAllocationDrvs" 
				    onkeypress="return onlyIntegerValue(event);"
				    value="#{filePreferenceBackingBean.matrixDTO.sysMinAllocationDriver}" 
				    size="25"
					label="Minimum Drivers"
					>
					<f:convertNumber integerOnly="true" />
					<a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
				</h:inputText>
			</td>
			</tr>				
			<tr><th colspan="3">Driver Reference Restriction</th></tr>
			<tr>
				<td>&#160;</td>
				<td colspan="2">
				<h:selectOneRadio id="gridLayout"  styleClass ="sysDrivRefRadio"  value="#{filePreferenceBackingBean.matrixDTO.sysDriverRef}" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				> 
                    <f:selectItem itemValue="-1" itemLabel="Full"/>
                    <f:selectItem itemValue="0" itemLabel="Partial (Warning)"/>  
                    <f:selectItem itemValue="1" itemLabel="None"/>
                    <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
				</h:selectOneRadio>				
				</td>
			</tr>
			
			<tr><th colspan="3">Data Fetch Threshold</th></tr>
			<tr>
				<td>&#160;</td>
				<td>Full Threshold:</td>
				<td>

				<h:inputText id="fullthreshold" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.matrixDTO.sysFullThreshold}" size="25" 
				onkeypress="return onlyIntegerValue(event);" maxlength="5">
								  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage"  />    
                        <f:convertNumber integerOnly="true" /> 
				</h:inputText>

				</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Limited Threshold:</td>
				<td>

				<h:inputText id="limitedthreshold" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.matrixDTO.sysLimitedThreshold}" size="25" 
				onkeypress="return onlyIntegerValue(event);" maxlength="5">
								  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage"  />    
                        <f:convertNumber integerOnly="true" /> 
				</h:inputText>

				</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Full Threshold Pages:</td>
				<td>

				<h:inputText id="fullthresholdpages" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.matrixDTO.sysFullThresholdPages}" size="25" 
				onkeypress="return onlyIntegerValue(event);" maxlength="5">
								  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        <f:convertNumber integerOnly="true" />   
				</h:inputText>

				</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Limited Threshold Pages:</td>
				<td>

				<h:inputText id="limitedthresholdpages" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.matrixDTO.sysLimitedThresholdPages}" size="25"
				onkeypress="return onlyIntegerValue(event);" maxlength="5">
								  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />   
                        <f:convertNumber integerOnly="true" />  
				</h:inputText>

				</td>
			</tr>	
			
			
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
                <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton  or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemMatrixAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemMatrixAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>