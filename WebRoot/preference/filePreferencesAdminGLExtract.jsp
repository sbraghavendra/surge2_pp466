<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpAGLForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">G/L Extract</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>G/L Extract Enabled?</td>
				<td style="width:770px;">
				<h:selectBooleanCheckbox styleClass="check" id="glExtractEnb"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			                value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractEnabled}">
			                				  <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        </h:selectBooleanCheckbox>
				</td>
			</tr>
			<!--  
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Window Name:</td>
				<td><h:inputText id="winName" size="40"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			                value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractWindowName}">
			                				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        </h:inputText>
				</td>
			</tr>
			-->
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Set Extract Mark:</td>
				<td><h:selectBooleanCheckbox styleClass="check" id="setMark"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			                value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractSetMark}">
			                				  <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        </h:selectBooleanCheckbox>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
		<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateAdminGLExtractAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelAdminGLExtractAction}"/></li>
	</ul>
	</div>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>