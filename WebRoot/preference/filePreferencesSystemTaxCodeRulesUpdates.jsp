<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpSTUForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">TaxCode Rules</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Last Valid TaxCode Rules Import</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Year/Month for Update TaxCode Rules Import:</td>
				<td style="width:600px;">
			    <h:inputText id="lastTaxCodeRulesDateUpdate"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}" 	
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.lastTaxCodeRulesDateUpdate}" size="20">	
			          <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>			       
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Release Number for Update TaxCode Rules Import:</td>
				<td style="width:600px;">
			    <h:inputText id="lastTaxCodeRulesRelUpdate" 
			     valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.lastTaxCodeRulesRelUpdate}" size="20">		
			          <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>		       
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	<div id="table-one-bottom">
		<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.updateSystemTaxCodeRulesUpdatesAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemTaxCodeRulesUpdatesAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>