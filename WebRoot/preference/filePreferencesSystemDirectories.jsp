<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpSDForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support id="onchange1" event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support  id="onchange2" event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Directories</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Reference Documents</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Directory Path:</td>
				<td>
			    <h:inputText id="refDocFilePath" style="float:left;"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}" 
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.refDocFilePath}" size="60" >
			      <a4j:support event="onkeyup" id="onchange5"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                </h:inputText>	
               
             				  <a4j:commandButton value ="search" id="search"  style="float:left;width:16px;height:16px;"
						  styleClass="image" image="/images/search_small.png" immediate="true"
						  action="#{filePreferenceBackingBean.updateButtonActionLookUp}"
						  actionListener="#{directoryBrowserBackingBean.searchActionPreferences}"
						  oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm">
						   <a4j:support event="onclick" reRender="updateButton, cancelButton, errorMessage"/>         
						  </a4j:commandButton>
                     
				</td>
				
			</tr>		
			<tr><th colspan="3">Default Directory for Crystal Reports</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Directory Path:</td>
				<td>
			    <h:inputText id="defaultDirCrystal" style="float:left;"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.defaultDirCrystal}" size="60" >
			      <a4j:support event="onkeyup" id="onchange6"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>		
                          		  <a4j:commandButton value ="crystalReport" id="defCrystalRepDir"  style="float:left;width:16px;height:16px;"
						  styleClass="image" image="/images/search_small.png" immediate="true"
						  action="#{filePreferenceBackingBean.updateButtonActionLookUp}"
						  actionListener="#{directoryBrowserBackingBean.searchActionCrystalReport}"
						  oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm, updateButton, cancelButton, errorMessage">
						  </a4j:commandButton>			       
				</td>
			</tr>
			
		<!-- 	<tr><th colspan="3">Download Files</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Directory Path:</td>
				<td>
			    <h:inputText id="downloadFilePath"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}" 
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.downloadFilePath}" size="60" >
			      <a4j:support event="onkeyup" id="onchange7"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                </h:inputText>	
               
             				  <a4j:commandButton value ="downloadsearch" id="downloadsearch" 
						  styleClass="image" image="/images/search_small.png" immediate="true"
						  action="#{filePreferenceBackingBean.updateButtonActionLookUp}"
						  actionListener="#{directoryBrowserBackingBean.searchActionDownloadFile}"
						  oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm, updateButton, cancelButton, errorMessage">
						  </a4j:commandButton>	   
				</td>
				
			</tr>		
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User ID:</td>
				<td>
			    <h:inputText id="downloadUserId" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.downloadUserId}" size="20" >
			      <a4j:support event="onkeyup" id="onchange71"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>					       
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Password:</td>
				<td>
			    <h:inputText id="downloadPassword" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.downloadPassword}" size="20" >
			      <a4j:support event="onkeyup" id="onchange72"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>					       
				</td>
			</tr> -->
			
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
		<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemDirectoriesAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemDirectoriesAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
<ui:include src="/WEB-INF/view/components/networkDirectoryBrowser.xhtml">
					<ui:param name="handler" value="#{directoryBrowserBackingBean}"/>
					<ui:param name="reRenderItems" value="dir, refDocFilePath, back, defaultDirCrystal,downloadFilePath"/>
					</ui:include>
</f:view>
</ui:define>
</ui:composition>
</html>