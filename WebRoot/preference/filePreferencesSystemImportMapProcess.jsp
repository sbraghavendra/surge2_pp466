<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpSIForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:450px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">Import/Map/Process</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="5">Import/Map</th></tr>
			<!-- tr>
				<td style="width:50px;">&#160;</td>
				<td>File Size Duplicate Restriction:</td>
				<td>
				<h:selectOneRadio id="gridLayout1" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.fileSizeDuplicateRestriction}" 
				> 
                    <f:selectItem itemValue="0" itemLabel="Full"/>
                    <f:selectItem itemValue="1" itemLabel="Partial (Warning)"/>  
                    <f:selectItem itemValue="2" itemLabel="None"/>
                             <a4j:support id="a4j-gridLayout1" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
				</h:selectOneRadio>				
				</td>
				<td style="width:300px;">&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>File Name Duplicate Restriction:</td>
				<td>
				<h:selectOneRadio id="gridLayout2"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.fileNameDuplicateRestriction}" 
				> 
                    <f:selectItem itemValue="0" itemLabel="Full"/>
                    <f:selectItem itemValue="1" itemLabel="Partial (Warning)"/>  
                    <f:selectItem itemValue="2" itemLabel="None"/>
                    <a4j:support id="a4j-gridLayout2" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:selectOneRadio>				
				</td>
				<td style="width:300px;">&#160;</td>
				<td>&#160;</td>
			</tr>-->
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Number of terminal errors before abort import:</td>
				<td colspan="2">
			    <h:inputText id="terminalErrorsBeforeAbortImport" onkeypress="return onlyNumerics(event);"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.terminalErrorsBeforeAbortImport}" size="25" >
			     <a4j:support id="a4j-TEBAI" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>							
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
			<!-- 
			<tr><th colspan="5">Process</th></tr>
			<tr>				
				<td>&#160;</td>
				<td>Kill Processing Procedure/Count?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="killProcessingProcedureCountFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.vclProcessingProcedure}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCountFlag}">
                               <a4j:support id="a4j-killPPA" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage, killProcessingProcedureCount" />  
                        </h:selectBooleanCheckbox>			                
			    <h:inputText id="killProcessingProcedureCount" onkeypress="return onlyNumerics(event);"
			     valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCount}" 
			    disabled="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCountFlag==false}" size="25" >
			     <a4j:support id="a4j-killPPC" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>			                
				</td>
				<td>&#160;</td>
			</tr>
			-->
			<!-- tr>
				<td>&#160;</td>
				<td>Default Import Date Map:</td>
				<td colspan="2">
			    <h:inputText id="defaultImportDateMap" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.defaultImportDateMap}" size="25" >
			     <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>							
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Number of Rows to Import/Map Before Save:</td>
				<td colspan="2">
			    <h:inputText id="noRowsBeforeSave" onkeypress="return onlyNumerics(event);"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.rowsToImportMapBeforeSave}" size="25" >
			     <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>							
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Do Not Import/Replace With Characters:</td>
				<td colspan="2">
			    	<h:inputText id="doNotImportMarkers"
				            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.doNotImportMarkers}" 
                       		size="25" style="background-color: #F5F5F5;" disabled="true" tabindex="-1" > 
          			</h:inputText>&#160;&#160;
          			
					<a4j:commandLink id="doNotImportBtn" immediate="true" value="Click to update"
							action="#{filePreferenceBackingBean.parseValuesToEditControls}"
							oncomplete="javascript:Richfaces.showModalPanel('doNotImportReplaceWithCharacters');" 
							reRender="doNotImportReplaceWithForm,msg"/>		
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>&#160;</td>
				<td colspan="2">
			    	<h:inputText id="replaceWithMarkers"
	                     	style="background-color: #F5F5F5;" disabled="true" tabindex="-1" 
				            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.replaceWithMarkers}"  size="25" >    
          			</h:inputText>&#160;&#160;<h:message id="replaceWithErrorMessages" for="replaceWithMarkers" errorClass="error" />
				</td>
				<td>&#160;</td>
			</tr>
				<tr><th colspan="5">Batch End-of-File</th></tr>
			<tr>
				<td>&#160;</td>
				<td>End-of-File Marker:</td>
				<td colspan="2">
			    <h:inputText id="endOfFileMarker" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.endOfFileMarker}" size="25" >
			     <a4j:support id="a4j-eofMarker" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>							
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Batch Count Marker:</td>
				<td colspan="2">
			    <h:inputText id="batchCountMarker" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.batchCountMarker}" size="25" >
			     <a4j:support id="a4j-batchCount" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>						    
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Batch Total $ Marker:</td>
				<td colspan="2">
			    <h:inputText id="batchTotalMarker" 
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.batchTotalMarker}" size="25" >	
			    			    <a4j:support id="a4j-batchTotal" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>			    
				</td>
				<td>&#160;</td>
			</tr>
			-->
			<tr><th colspan="5">Process</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Email Process Complete?:</td>
				<td colspan="3">
                <h:selectBooleanCheckbox styleClass="check" id="emailProcessStatusFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.emailProcessStatusFlag}">	
                  <a4j:support id="a4j-emailProcessStatus" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        </h:selectBooleanCheckbox>				                
			      (Uses email addresses from System:AID Service page.)            
				</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Delete Transactions with Amount=0?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="deleteTransactionWithZeroAmountFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.deleteTransactionWithZeroAmountFlag}">	
                  <a4j:support id="a4j-deleteWithZero" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                        </h:selectBooleanCheckbox>				                
			                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
			<tr>	
				<td style="width:50px;">&#160;</td>
				<td>Hold Transactions with Amount &gt; ?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="holdTransactionAmountFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.vclTransactionAmount}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.holdTransactionAmountFlag}">
                            <a4j:support id="a4j-holdTrWA" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage, holdTransactionAmount" />  
                        </h:selectBooleanCheckbox>						                
			    <h:inputText id="holdTransactionAmount" onkeypress="return onlyNumerics(event);"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.holdTransactionAmount}" 
			    disabled="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.holdTransactionAmountFlag==false}" size="25" >
			    <a4j:support id="a4j-holdAmount" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>			                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
			<tr>				
			    <td style="width:50px;">&#160;</td>
				<td>Process Transaction with Amount &lt; ?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="processTransactionAmountFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.vclApplyTaxCode}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.processTransactionAmountFlag}">	
                         <a4j:support event="onclick" 
                         reRender="updateButton, cancelButton, errorMessage, processTransactionAmount, filterTaxCode_detailId, filterTaxCode_search, filterTaxCode_code" />  
                        </h:selectBooleanCheckbox>			                
			    <h:inputText id="processTransactionAmount" onkeypress="return onlyNumerics(event);"
			    binding="#{filePreferenceBackingBean.processTransactionAmountInput}"
			      valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.processTransactionAmount}" size="25" 
			    disabled="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.processTransactionAmountFlag==false}"> 
			   <a4j:support id="a4j-processAmount" event="onkeyup"
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>					                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>

			<tr style="width: 700px;">
					<td class="column-spacer">&#160;</td>
					<td class="column-label" style = "padding-left: 50px;"> Apply TaxCode : </td> 
					<td colspan="2" class="column-spacer">
					
						<ui:include src="/WEB-INF/view/components/taxcode_code_noview.xhtml">
							<ui:param name="handler" value="#{filePreferenceBackingBean.filterTaxCodeHandler}"/>
							<ui:param name="id" value="filterTaxCode"/>
							<ui:param name="readonly" value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.processTransactionAmountFlag==false}"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
							<ui:param name="reRender" value="updateButton,cancelButton,errorMessage"/>
						</ui:include>
					</td>
					<td class="column-spacer">&#160;</td>					
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Do custom TaxCode Rules override Standard TaxCode Rules?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="overrideTaxionaryRuleidFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.customTaxCodeOverrideTaxionaryFlag}">	
                  <a4j:support id="overrideTaxionaryRuleid" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                 </h:selectBooleanCheckbox>				                
			                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Run POST Process Rules for HELD Trans?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="runpostrulesheldtransFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.runPostProcessrulesForheldTransFlag}">	
                  <a4j:support id="heldtransruleid" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                 </h:selectBooleanCheckbox>				                
			                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
			
			<tr>				
				<td style="width:50px;">&#160;</td>
				<td>Kill Processing Procedure/Count?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="killProcessingProcedureCountFlag" 
                			valueChangeListener="#{filePreferenceBackingBean.vclProcessingProcedure}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCountFlag}">
                               <a4j:support id="a4j-killPPA" event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage, killProcessingProcedureCount" />  
                        </h:selectBooleanCheckbox>			                
			    <h:inputText id="killProcessingProcedureCount" onkeypress="return onlyNumerics(event);"
			     valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCount}" 
			    disabled="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.killProcessingProcedureCountFlag==false}" size="25" >
			     <a4j:support id="a4j-killPPC" event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>			                
				</td>
				<td style="width:220px;">&#160;</td>
			</tr>
				<tr><th colspan="5">Allocations</th></tr>
		  <tr>
			 <td style="width:50px;">&#160;</td>
			 <td>Post Jurisdiction to Sales Address </td>
			 <td colspan= "3">
				<h:selectOneMenu id="postJurItemList"  value = "#{filePreferenceBackingBean.importMapProcessPreferenceDTO.postJurisdictiontoSalesAddress}"  disabled = "#{!filePreferenceBackingBean.importMapProcessPreferenceDTO.isPostJurisdtoSalesAddressEnabled}"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}" style="width: 225px;" > 
							
					<f:selectItems id="postJurItems" value="#{filePreferenceBackingBean.jurisdictiontoSalesAddressItems}" />
				  		<a4j:support event="onchange" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
          
				</h:selectOneMenu>
			</td>
		  </tr>
				<!--
				<tr>				
					<td>&#160;</td>
					<td>Lookup Vendor Nexus? (otherwise assume Vendor Nexus)</td>
					<td colspan="2">
	                <h:selectBooleanCheckbox styleClass="check" id="lookupVendorNexusFlag" 
	                			valueChangeListener="#{filePreferenceBackingBean.vclProcessingProcedure}"
	                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.lookupVendorNexusFlag}">
	                               <a4j:support id="a4j-lookupVendorNexus" event="onclick" 
	                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
	                        </h:selectBooleanCheckbox>		                
					</td>
					<td>&#160;</td>
				</tr>
				-->
		</tbody>
	</table>
	</div>
    </div>
	<div id="table-one-bottom">
		<a4j:outputPanel id="errorMessage">
      <h:messages  id="jsfErrorMessages" globalOnly="true" errorClass="error" />
	    <h:outputLabel 
         style="color:red;font-weight:bold;font-size:10px" 
         id="errorMessageLabel"
         disabled="#{!filePreferenceBackingBean.displayButton}"  
         value="#{filePreferenceBackingBean.errorMessage}"/>
    </a4j:outputPanel>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemImportMapProcessAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemImportMapProcessAction}"/></li>
	</ul>
	</div>

	</div>
</div>
</h:form>
<!--  
<ui:include src="/WEB-INF/view/components/taxcode_search.xhtml">
	<ui:param name="handler" value="#{filePreferenceBackingBean.filterTaxCodeHandler}"/>
	<ui:param name="popupName" value="searchTaxCode"/>
	<ui:param name="popupForm" value="searchTaxCodeForm"/>
	 <ui:param name="forPreference" value="true"/>
	<ui:param name="reRender" value="fpSIForm:filterTaxCode"/>
</ui:include>
-->

<rich:modalPanel id="doNotImportReplaceWithCharacters" zindex="2000" autosized="true" >
	<f:facet name="header">
		<h:outputText value="Do Not Import/Replace With Characters" />
	</f:facet>
	
	<h:form id="doNotImportReplaceWithForm">
		<div align="left" style="padding-left: 10px;">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>
		<table>
			<tr>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td>DNI = Do Not Import</td>
			</tr>
			<tr>
				<td>1-</td>
				<td><h:inputText id="dni1" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni1}" maxlength="1" tabindex="1"/></td>
				<td>=</td>
				<td><h:inputText id="rw1" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw1}" maxlength="2" tabindex="2"/></td>
				<td></td>
				<td>11-</td>
				<td><h:inputText id="dni11" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni11}" maxlength="1" tabindex="21"/></td>
				<td>=</td>
				<td><h:inputText id="rw11" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw11}" maxlength="2" tabindex="22"/></td>
				<td></td>
				<td>21-</td>
				<td><h:inputText id="dni21" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni21}" maxlength="1" tabindex="41"/></td>
				<td>=</td>
				<td><h:inputText id="rw21" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw21}" maxlength="2" tabindex="42"/></td>
				<td></td>
				<td>31-</td>
				<td><h:inputText id="dni31" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni31}" maxlength="1" tabindex="61"/></td>
				<td>=</td>
				<td><h:inputText id="rw31" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw31}" maxlength="2" tabindex="62"/></td>
				<td></td>
				<td>41-</td>
				<td><h:inputText id="dni41" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni41}" maxlength="1" tabindex="81"/></td>
				<td>=</td>
				<td><h:inputText id="rw41" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw41}" maxlength="2" tabindex="82"/></td>
				<td></td>
				<td>RW = Replace With</td>
			</tr>
			<tr>
				<td>2-</td>
				<td><h:inputText id="dni2" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni2}" maxlength="1" tabindex="3"/></td>
				<td>=</td>
				<td><h:inputText id="rw2" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw2}" maxlength="2" tabindex="4"/></td>
				<td></td>
				<td>12-</td>
				<td><h:inputText id="dni12" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni12}" maxlength="1" tabindex="23"/></td>
				<td>=</td>
				<td><h:inputText id="rw12" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw12}" maxlength="2" tabindex="24"/></td>
				<td></td>
				<td>22-</td>
				<td><h:inputText id="dni22" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni22}" maxlength="1" tabindex="43"/></td>
				<td>=</td>
				<td><h:inputText id="rw22" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw22}" maxlength="2" tabindex="44"/></td>
				<td></td>
				<td>32-</td>
				<td><h:inputText id="dni32" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni32}" maxlength="1" tabindex="63"/></td>
				<td>=</td>
				<td><h:inputText id="rw32" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw32}" maxlength="2" tabindex="64"/></td>
				<td></td>
				<td>42-</td>
				<td><h:inputText id="dni42" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni42}" maxlength="1" tabindex="83"/></td>
				<td>=</td>
				<td><h:inputText id="rw42" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw42}" maxlength="2" tabindex="84"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>3-</td>
				<td><h:inputText id="dni3" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni3}" maxlength="1" tabindex="5"/></td>
				<td>=</td>
				<td><h:inputText id="rw3" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw3}" maxlength="2" tabindex="6"/></td>
				<td></td>
				<td>13-</td>
				<td><h:inputText id="dni13" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni13}" maxlength="1" tabindex="25"/></td>
				<td>=</td>
				<td><h:inputText id="rw13" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw13}" maxlength="2" tabindex="26"/></td>
				<td></td>
				<td>23-</td>
				<td><h:inputText id="dni23" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni23}" maxlength="1" tabindex="45"/></td>
				<td>=</td>
				<td><h:inputText id="rw23" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw23}" maxlength="2" tabindex="46"/></td>
				<td></td>
				<td>33-</td>
				<td><h:inputText id="dni33" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni33}" maxlength="1" tabindex="65"/></td>
				<td>=</td>
				<td><h:inputText id="rw33" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw33}" maxlength="2" tabindex="66"/></td>
				<td></td>
				<td>43-</td>
				<td><h:inputText id="dni43" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni43}" maxlength="1" tabindex="85"/></td>
				<td>=</td>
				<td><h:inputText id="rw43" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw43}" maxlength="2" tabindex="86"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>4-</td>
				<td><h:inputText id="dni4" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni4}" maxlength="1" tabindex="7"/></td>
				<td>=</td>
				<td><h:inputText id="rw4" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw4}" maxlength="2" tabindex="8"/></td>
				<td></td>
				<td>14-</td>
				<td><h:inputText id="dni14" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni14}" maxlength="1" tabindex="27"/></td>
				<td>=</td>
				<td><h:inputText id="rw14" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw14}" maxlength="2" tabindex="28"/></td>
				<td></td>
				<td>24-</td>
				<td><h:inputText id="dni24" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni24}" maxlength="1" tabindex="47"/></td>
				<td>=</td>
				<td><h:inputText id="rw24" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw24}" maxlength="2" tabindex="48"/></td>
				<td></td>
				<td>34-</td>
				<td><h:inputText id="dni34" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni34}" maxlength="1" tabindex="67"/></td>
				<td>=</td>
				<td><h:inputText id="rw34" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw34}" maxlength="2" tabindex="68"/></td>
				<td></td>
				<td>44-</td>
				<td><h:inputText id="dni44" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni44}" maxlength="1" tabindex="87"/></td>
				<td>=</td>
				<td><h:inputText id="rw44" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw44}" maxlength="2" tabindex="88"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>5-</td>
				<td><h:inputText id="dni5" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni5}" maxlength="1" tabindex="9"/></td>
				<td>=</td>
				<td><h:inputText id="rw5" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw5}" maxlength="2" tabindex="10"/></td>
				<td></td>
				<td>15-</td>
				<td><h:inputText id="dni15" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni15}" maxlength="1" tabindex="29"/></td>
				<td>=</td>
				<td><h:inputText id="rw15" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw15}" maxlength="2" tabindex="30"/></td>
				<td></td>
				<td>25-</td>
				<td><h:inputText id="dni25" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni25}" maxlength="1" tabindex="49"/></td>
				<td>=</td>
				<td><h:inputText id="rw25" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw25}" maxlength="2" tabindex="50"/></td>
				<td></td>
				<td>35-</td>
				<td><h:inputText id="dni35" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni35}" maxlength="1" tabindex="69"/></td>
				<td>=</td>
				<td><h:inputText id="rw35" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw35}" maxlength="2" tabindex="70"/></td>
				<td></td>
				<td>45-</td>
				<td><h:inputText id="dni45" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni45}" maxlength="1" tabindex="89"/></td>
				<td>=</td>
				<td><h:inputText id="rw45" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw45}" maxlength="2" tabindex="90"/></td>
				<td></td>
				<td>Legend</td>
			</tr>
			<tr>
				<td>6-</td>
				<td><h:inputText id="dni6" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni6}" maxlength="1" tabindex="11"/></td>
				<td>=</td>
				<td><h:inputText id="rw6" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw6}" maxlength="2" tabindex="12"/></td>
				<td></td>
				<td>16-</td>
				<td><h:inputText id="dni16" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni16}" maxlength="1" tabindex="31"/></td>
				<td>=</td>
				<td><h:inputText id="rw16" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw16}" maxlength="2" tabindex="32"/></td>
				<td></td>
				<td>26-</td>
				<td><h:inputText id="dni26" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni26}" maxlength="1" tabindex="51"/></td>
				<td>=</td>
				<td><h:inputText id="rw26" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw26}" maxlength="2" tabindex="52"/></td>
				<td></td>
				<td>36-</td>
				<td><h:inputText id="dni36" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni36}" maxlength="1" tabindex="71"/></td>
				<td>=</td>
				<td><h:inputText id="rw36" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw36}" maxlength="2" tabindex="72"/></td>
				<td></td>
				<td>46-</td>
				<td><h:inputText id="dni46" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni46}" maxlength="1" tabindex="91"/></td>
				<td>=</td>
				<td><h:inputText id="rw46" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw46}" maxlength="2" tabindex="92"/></td>
				<td></td>
				<td>~s = space</td>
			</tr>
			<tr>
				<td>7-</td>
				<td><h:inputText id="dni7" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni7}" maxlength="1" tabindex="13"/></td>
				<td>=</td>
				<td><h:inputText id="rw7" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw7}" maxlength="2" tabindex="14"/></td>
				<td></td>
				<td>17-</td>
				<td><h:inputText id="dni17" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni17}" maxlength="1" tabindex="33"/></td>
				<td>=</td>
				<td><h:inputText id="rw17" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw17}" maxlength="2" tabindex="34"/></td>
				<td></td>
				<td>27-</td>
				<td><h:inputText id="dni27" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni27}" maxlength="1" tabindex="53"/></td>
				<td>=</td>
				<td><h:inputText id="rw27" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw27}" maxlength="2" tabindex="54"/></td>
				<td></td>
				<td>37-</td>
				<td><h:inputText id="dni37" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni37}" maxlength="1" tabindex="73"/></td>
				<td>=</td>
				<td><h:inputText id="rw37" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw37}" maxlength="2" tabindex="74"/></td>
				<td></td>
				<td>47-</td>
				<td><h:inputText id="dni47" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni47}" maxlength="1" tabindex="93"/></td>
				<td>=</td>
				<td><h:inputText id="rw47" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw47}" maxlength="2" tabindex="94"/></td>
				<td></td>
				<td>~a = <img src="../images/preferences/currency_sign.GIF" /></td>
			</tr>
			<tr>
				<td>8-</td>
				<td><h:inputText id="dni8" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni8}" maxlength="1" tabindex="15"/></td>
				<td>=</td>
				<td><h:inputText id="rw8" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw8}" maxlength="2" tabindex="16"/></td>
				<td></td>
				<td>18-</td>
				<td><h:inputText id="dni18" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni18}" maxlength="1" tabindex="35"/></td>
				<td>=</td>
				<td><h:inputText id="rw18" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw18}" maxlength="2" tabindex="36"/></td>
				<td></td>
				<td>28-</td>
				<td><h:inputText id="dni28" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni28}" maxlength="1" tabindex="55"/></td>
				<td>=</td>
				<td><h:inputText id="rw28" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw28}" maxlength="2" tabindex="56"/></td>
				<td></td>
				<td>38-</td>
				<td><h:inputText id="dni38" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni38}" maxlength="1" tabindex="75"/></td>
				<td>=</td>
				<td><h:inputText id="rw38" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw38}" maxlength="2" tabindex="76"/></td>
				<td></td>
				<td>48-</td>
				<td><h:inputText id="dni48" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni48}" maxlength="1" tabindex="95"/></td>
				<td>=</td>
				<td><h:inputText id="rw48" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw48}" maxlength="2" tabindex="96"/></td>
				<td></td>
				<td>~b = <img src="../images/preferences/section_sign.GIF"/></td>
			</tr>
			<tr>
				<td>9-</td>
				<td><h:inputText id="dni9" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni9}" maxlength="1" tabindex="17"/></td>
				<td>=</td>
				<td><h:inputText id="rw9" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw9}" maxlength="2" tabindex="18"/></td>
				<td></td>
				<td>19-</td>
				<td><h:inputText id="dni19" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni19}" maxlength="1" tabindex="37"/></td>
				<td>=</td>
				<td><h:inputText id="rw19" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw19}" maxlength="2" tabindex="38"/></td>
				<td></td>
				<td>29-</td>
				<td><h:inputText id="dni29" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni29}" maxlength="1" tabindex="57"/></td>
				<td>=</td>
				<td><h:inputText id="rw29" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw29}" maxlength="2" tabindex="58"/></td>
				<td></td>
				<td>39-</td>
				<td><h:inputText id="dni39" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni39}" maxlength="1" tabindex="77"/></td>
				<td>=</td>
				<td><h:inputText id="rw39" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw39}" maxlength="2" tabindex="78"/></td>
				<td></td>
				<td>49-</td>
				<td><h:inputText id="dni49" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni49}" maxlength="1" tabindex="97"/></td>
				<td>=</td>
				<td><h:inputText id="rw49" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw49}" maxlength="2" tabindex="98"/></td>
				<td></td>
				<td>~c = <img src="../images/preferences/diamond_sign.GIF"/></td>
			</tr>
			<tr>
				<td>10-</td>
				<td><h:inputText id="dni10" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni10}" maxlength="1" tabindex="19"/></td>
				<td>=</td>
				<td><h:inputText id="rw10" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw10}" maxlength="2" tabindex="20"/></td>
				<td></td>
				<td>20-</td>
				<td><h:inputText id="dni20" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni20}" maxlength="1" tabindex="39"/></td>
				<td>=</td>
				<td><h:inputText id="rw20" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw20}" maxlength="2" tabindex="40"/></td>
				<td></td>
				<td>30-</td>
				<td><h:inputText id="dni30" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni30}" maxlength="1" tabindex="59"/></td>
				<td>=</td>
				<td><h:inputText id="rw30" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw30}" maxlength="2" tabindex="60"/></td>
				<td></td>
				<td>40-</td>
				<td><h:inputText id="dni40" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni40}" maxlength="1" tabindex="79"/></td>
				<td>=</td>
				<td><h:inputText id="rw40" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw40}" maxlength="2" tabindex="80"/></td>
				<td></td>
				<td>50-</td>
				<td><h:inputText id="dni50" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.dni50}" maxlength="1" tabindex="99"/></td>
				<td>=</td>
				<td><h:inputText id="rw50" size="2" value="#{filePreferenceBackingBean.doNotImportCharBean.rw50}" maxlength="2" tabindex="100"/></td>
				<td></td>
				<td>~d = <img src="../images/preferences/middle_point.GIF"/></td>
			</tr>
		</table>
		                    
		<a4j:commandButton id="btnOk" value="Ok" 
        	actionListener="#{filePreferenceBackingBean.moveEditControlToValuesListener}"          
           	oncomplete="if (#{filePreferenceBackingBean.controlToValuesSucceed}) {Richfaces.hideModalPanel('doNotImportReplaceWithCharacters');}"            
                       reRender="updateButton, cancelButton, errorMessage, msg,doNotImportReplaceWithForm,doNotImportMarkers,replaceWithMarkers" >
		</a4j:commandButton>
  		<rich:spacer width="10px"/>
  		<a4j:commandButton id="btnCancel" immediate="true" type="reset"
			onclick="Richfaces.hideModalPanel('doNotImportReplaceWithCharacters');" value="Cancel">
		</a4j:commandButton>
		
	</h:form>
</rich:modalPanel>


</f:view>
</ui:define>
</ui:composition>
</html>