<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpAGForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">RatePoint</td></tr>
		</thead>
		<tbody>
		    <tr><th colspan="5">RatePoint API Options</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Enabled?:</td>
				<td>

				<h:selectBooleanCheckbox styleClass="check" id="ratepointEnabled" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.companyDTO.ratepointEnabled}" > 
				  	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
               </h:selectBooleanCheckbox>

				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Server:</td>
				<td>

				<h:inputText id="ratePointServer" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.companyDTO.ratepointServer}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>User ID:</td>
				<td>

				<h:inputText id="ratepointUserId" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.companyDTO.ratepointUserId}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Password:</td>
				<td>

				<h:inputText id="ratepointPassword" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.companyDTO.ratepointPassword}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>	
		 <tr><th colspan="5">RatePoint API Endpoints</th></tr>
		 	<tr>
		 		<td style="width:50px;">&#160;</td>
		 		<td>Lat/Long Lookup: </td>
		 		<td>
		 			<h:inputText id="latLongLookup" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.lonLatLookup}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>
		 		</td>
		 		<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
		 	</tr>
		 	<tr>
		 		<td style="width:50px;">&#160;</td>
		 		<td>Generic Jurisdiction Lookup: </td>
		 		<td>
		 			<h:inputText id="genJuriLookup" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.genJurLookup}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>
		 		</td>
		 		<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
		 	</tr>
		 	<tr>
		 		<td style="width:50px;">&#160;</td>
		 		<td>Street Level Lookup </td>
		 		<td>
		 			<h:inputText id="streetLevelLookup" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.streetLookup}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>
		 		</td>
		 		<td>&#160;</td>
				<td style="width:100px;">&#160;</td>
		 	</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
	   <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>      
	<ul class="right">
		<li class="verify"><h:commandLink id="verifyButton" 
			actionListener="#{filePreferenceBackingBean.verifyRatePointAPIActionListener}"
			reRender="updateButton, cancelButton, errorMessage"  /></li>
			
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateAdminRatepointAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelAdminRatepointAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>