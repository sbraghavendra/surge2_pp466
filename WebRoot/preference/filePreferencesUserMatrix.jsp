<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpUMForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6">Matrix</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Transaction Indicator:</td>
				<td>

				<h:selectOneMenu id="selTransInd" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.userPreferenceDTO.userTransInd}" style="width: 200px;">

        		<f:selectItems id="selTransIndItms" value="#{filePreferenceBackingBean.matrixUserTransIndList}" />
									  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
        		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default TaxCode Type:</td>
				<td>

				<h:selectOneMenu id="selTaxCode" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.userPreferenceDTO.userTaxCode}" style="width: 200px;">
				
        		<f:selectItems id="selTaxCodeItms" value="#{filePreferenceBackingBean.matrixUserTaxCodeList}" />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Tax Type:</td>
				<td>

				<h:selectOneMenu id="selTaxType" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.userPreferenceDTO.userTaxType}" style="width: 200px;">
				
        		<f:selectItems id="selTaxTypeItms" value="#{filePreferenceBackingBean.matrixUserTaxTypeList}" />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Rate Type:</td>
				<td>

				<h:selectOneMenu id="selRateType" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.userPreferenceDTO.userRateType}" style="width: 200px;">
				
        		<f:selectItems id="selRateTypeItms" value="#{filePreferenceBackingBean.matrixUserRateTypeList}" />
				  <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Entity:</td>
				<td>
					<h:selectOneMenu id="selEntity" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonDefaultEntityAction}"	immediate="true"
						value="#{filePreferenceBackingBean.userPreferenceDTO.userEntity}" style="width: 200px;">
					
	        			<f:selectItems id="selEntityItms" value="#{filePreferenceBackingBean.matrixEntityList}" />
					  		<a4j:support event="onchange" 
	                        ajaxSingle="true" reRender="checkEntityLabelId,checkEntityDefaultFlag,updateButton, cancelButton, errorMessage" />     
	                </h:selectOneMenu>
				</td>		
				<td>
					<h:selectBooleanCheckbox styleClass="check" id="checkEntityDefaultFlag" 
						disabled="#{empty filePreferenceBackingBean.userPreferenceDTO.userEntity}"
                		valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                        value="#{filePreferenceBackingBean.userPreferenceDTO.userEntityFilter}">	
                  			<a4j:support id="checkEntityDefaultFlagId" event="onclick" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                 	</h:selectBooleanCheckbox>
				</td>			
				<td>
					<h:outputLabel id="checkEntityLabelId"
                    	style="#{(empty filePreferenceBackingBean.userPreferenceDTO.userEntity) ? 'color:gray;' : ''}" 
                    	value="Use Default in Selection Filters?"/>
                </td>
                <td style="width:20%;" >&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Country:</td>
				<td>
					<h:selectOneMenu id="selCountry" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonDefaultCountryAction}"	immediate="true"
						value="#{filePreferenceBackingBean.userPreferenceDTO.userCountry}" style="width: 200px;">
					
	        			<f:selectItems id="selCountryItms" value="#{filePreferenceBackingBean.matrixCountryList}" />
					  		<a4j:support event="onchange" 
	                        ajaxSingle="true" reRender="checkCountryLabelId,checkCountryDefaultFlag,checkStateDefaultFlag,checkStateLabelId,updateButton,cancelButton,errorMessage,selState,selStateItms" />     
	                </h:selectOneMenu>
				</td>		
				<td>
					<h:selectBooleanCheckbox styleClass="check" id="checkCountryDefaultFlag" 
						disabled="#{empty filePreferenceBackingBean.userPreferenceDTO.userCountry}"
                		valueChangeListener="#{filePreferenceBackingBean.updateButtonFilterCountryAction}"
                        value="#{filePreferenceBackingBean.userPreferenceDTO.userCountryFilter}">	
                  			<a4j:support id="checkCountryDefaultFlagId" event="onclick" 
                        	ajaxSingle="true" reRender="checkStateDefaultFlag,checkStateLabelId,updateButton, cancelButton, errorMessage" />  
                 	</h:selectBooleanCheckbox>
				</td>			
				<td>
					<h:outputLabel id="checkCountryLabelId"
                    	style="#{(empty filePreferenceBackingBean.userPreferenceDTO.userCountry) ? 'color:gray;' : ''}" 
                    	value="Use Default in Selection Filters?"/>
                </td>
                <td style="width:20%;" >&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default State:</td>
				<td>
					<h:selectOneMenu id="selState" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonDefaultStateAction}"	immediate="true"
						disabled="#{empty filePreferenceBackingBean.userPreferenceDTO.userCountry}"
						value="#{filePreferenceBackingBean.userPreferenceDTO.userState}" style="width: 200px;">
					
	        			<f:selectItems id="selStateItms" value="#{filePreferenceBackingBean.matrixStateList}" />
					  		<a4j:support event="onchange" 
	                        ajaxSingle="true" reRender="checkStateLabelId,checkStateDefaultFlag,updateButton, cancelButton, errorMessage" />     
	                </h:selectOneMenu>
				</td>		
				<td>
					<h:selectBooleanCheckbox styleClass="check" id="checkStateDefaultFlag" 
						disabled="#{empty filePreferenceBackingBean.userPreferenceDTO.userState or !filePreferenceBackingBean.userPreferenceDTO.userCountryFilter}"
                		valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                        value="#{filePreferenceBackingBean.userPreferenceDTO.userStateFilter}">	
                  			<a4j:support id="checkStateDefaultFlagId" event="onclick" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
                 	</h:selectBooleanCheckbox>
				</td>			
				<td>
					<h:outputLabel id="checkStateLabelId"
                    	style="#{(empty filePreferenceBackingBean.userPreferenceDTO.userState) ? 'color:gray;' : ''}" 
                    	value="Use Default in Selection Filters?"/>
                </td>
                <td style="width:20%;" >&#160;</td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
	<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton"
		disabled="#{!filePreferenceBackingBean.displayButton}" 
		action="#{filePreferenceBackingBean.updateUserMatrixAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelUserMatrixAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>