<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpSGForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
    <div id="table-one">
    <div id="table-one-top">

        Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu> 
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
    </div>
    <div id="table-one-content" style="height:418px;">
    <div class="scrollInner" id="resize" >
    <table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
        <thead>
            <tr><td colspan="5">General</td></tr>
        </thead>
        <tbody>
        	<tr>
				<th colspan="5">&#160;Company Information</th>
			</tr>
            <tr>
                <td style="width:50px;">&#160;</td>
                <td>Company Name:</td>
                <td>
                <h:inputText id="companyName"  
                    value="#{filePreferenceBackingBean.companyDTO.name}" 
                    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    style="width: 300px;">
                    <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                </h:inputText>
                </td>
                <td>&#160;</td>
               <td>&#160;</td>
            </tr>
            
            <tr>
                <td style="width:50px;">&#160;</td>
                <td>Auto Create Nexus Definition?:</td>
                <td>
                <h:selectOneMenu id="autoCreateNexusDef" 
                	value="#{filePreferenceBackingBean.companyDTO.autoAddNexus}" 
                    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    style="width: 200px;">
                	<f:selectItem itemValue="Always" itemLabel="Always" id="acnda" />
                	<f:selectItem itemValue="Never" itemLabel="Never" id="acndn" />
                	<f:selectItem itemValue="Prompt" itemLabel="Prompt" id="acndp" />
                	<a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
                </h:selectOneMenu>
                </td>
                <td>&#160;</td>
                <td style="width:400px;">&#160;</td>
            </tr>
            
            <tr>
                <td style="width:50px;">&#160;</td>
                <td>Users without Add Nexus rights?:</td>
                <td>
                <h:selectOneMenu id="usersWithoutAddNexusRights" 
                	value="#{filePreferenceBackingBean.companyDTO.autoAddNexusNoRights}" 
                    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    style="width: 200px;">
                	<f:selectItem itemValue="Always" itemLabel="Always" id="uwanra" />
                	<f:selectItem itemValue="Never" itemLabel="Never" id="uwanrn" />
                	<a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
                </h:selectOneMenu>
                </td>
                <td>&#160;</td>
                <td style="width:400px;">&#160;</td>
            </tr> 
            
            <tr>
                <td style="width:50px;">&#160;</td>
                <td>Default Nexus Type:</td>
                <td>
                <h:selectOneMenu id="defaultNexusType" 
                	value="#{filePreferenceBackingBean.companyDTO.defaultNexusType}" 
                    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    style="width: 200px;">
                	<f:selectItems id="dntItms" value="#{filePreferenceBackingBean.defaultNexusTypes}" />
                	<a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
                </h:selectOneMenu>
                </td>
                <td>&#160;</td>
                <td style="width:400px;">&#160;</td>
            </tr>
             
            <tr>
				<th colspan="5">&#160;Time Zone</th>
			</tr>
            
            <tr>
                <td style="width:50px;">&#160;</td>
				<td>Default User Time Zone:</td>
				<td>
				<h:selectOneMenu id="defaultUserTimeZone" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.companyDTO.defaultTimeZone}" style="width: 200px;">
        		<f:selectItems id="defaultUserTimeZoneItms" value="#{filePreferenceBackingBean.timeZoneList}" />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
                <td style="width:400px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>System Time Zone:</td>
				<td>
				<h:selectOneMenu id="stsTimeZone" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.companyDTO.stsTimeZone}" style="width: 200px;" disabled="true">
        		<f:selectItems id="stsTimeZoneItms" value="#{filePreferenceBackingBean.timeZoneList}" />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
                <td style="width:400px;">&#160;</td>
			</tr>
			
			<tr>
                <th colspan="5">&#160;License</th>
            </tr>
            
            <tr>
                <td style="width:50px;border: 0;">&#160;</td>
                <td style= "border: 0;">Key:</td>
                <td style= "border: 0;">
                <h:inputTextarea id="licenseKey"  
                    value="#{filePreferenceBackingBean.licenseKey}" 
                    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    style="width: 300px;" rows="3">
                    <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                </h:inputTextarea>
                </td>
                <td style= "border: 0;">&#160;</td>
                <td style="width:400px;border: 0;">&#160;</td>
            </tr> </tbody>
           </table>
            <table cellpadding="0" cellspacing="0" width="911" > 
             <tr>
                <td style="width:152px;" colspan = "2">&#160;</td>
                <td>By entering a license key, you accept that you have obtained a legitimate product license from Ryan, LLC.</td>
            </tr>
            </table>
       
    </div>
    </div>
    <div id="table-one-bottom">
                <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.showLicenseWarning}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>        
    <ul class="right">
		<li class="update2"><h:commandLink 
		                       id="updateButton" 
		                       action="#{filePreferenceBackingBean.updateSystemGeneralAction}"
		                       disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"/>
		</li>
		<li class="cancel"><h:commandLink 
		                       id="cancelButton" 
                               disabled="#{!filePreferenceBackingBean.displayButton}" 		
		                       action="#{filePreferenceBackingBean.cancelSystemGeneralAction}"/></li>
    </ul>
    </div>
    </div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>
