<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
//<![CDATA[

function callClick(){
	var val = document.fpSGLForm.uploadFile.value;
	if(val!=""){
	var ind = val.lastIndexOf('\\');
	document.getElementById('fpSGLForm:glExtractFilePrefix').value = val.substring(ind+1,val.lastIndexOf('.'));
	document.getElementById('fpSGLForm:glExtractFilePath').value = val.substring(0,ind);
	document.getElementById('fpSGLForm:glExtractFileExtension').value = val.substring(val.lastIndexOf('.'), val.size);
	document.getElementById('fpSGLForm:hiddenButton').click();
	
	}
}

//]]>
</ui:define>
<ui:define name="body">
<f:view>
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<h:form id="fpSGLForm">
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">G/L Extract</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">G/L Extract File Name</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Path:</td>
				<td style="width:770px;">
			    <h:inputText id="glExtractFilePath" style="float:left;"
			    valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractFilePath}" size="60">
			     <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>	 	
                          		  <a4j:commandButton value ="gLExtractFolderPath" id="gLExtractDirPath" style="float:left;width:16px;height:16px;"
						  styleClass="image" image="/images/search_small.png" immediate="true"
						  action="#{filePreferenceBackingBean.updateButtonActionLookUp}"
						  actionListener="#{directoryBrowserBackingBean.searchActionGlExtract}"
						  oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm, updateButton, cancelButton, errorMessage">
						  </a4j:commandButton>	
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Prefix:</td>
				<td>
			    <h:inputText id="glExtractFilePrefix" 
			    	valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractFilePrefix}" size="20"> 
			     <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>				                
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Extension:</td>
				<td>
			    <h:inputText id="glExtractFileExtension"
			     valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.glExtractFileExtension}" size="20">
			    <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>		
						 
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
		<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemGLExtractAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemGLExtractAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
<ui:include src="/WEB-INF/view/components/networkDirectoryBrowser.xhtml">
					<ui:param name="handler" value="#{directoryBrowserBackingBean}"/>
					</ui:include>
</f:view>
</ui:define>
</ui:composition>
</html>