<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpAIForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Import/Map/Process</td></tr>
		</thead>
		<tbody>
		<tr><th colspan="3">Processing Options</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Jurisdiction Allocations Enabled?:</td>
				<td style="width:600px;">
                <h:selectBooleanCheckbox styleClass="check" id="allocationsEnabled" 
                	valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.allocationsEnabled}">
                            	     <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
                        </h:selectBooleanCheckbox>			                
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Process Rules Enabled?:</td>
				<td style="width:600px;">
                <h:selectBooleanCheckbox styleClass="check" id="processrulesEnabled" 
                	valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.processrulesEnabled}">
                            	     <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
                        </h:selectBooleanCheckbox>			                
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>TaxCode Allocations Enabled?:</td>
				<td style="width:600px;">
                <h:selectBooleanCheckbox styleClass="check" id="taxCodeAllocationsEnabled" 
                	valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                            value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.taxCodeAllocationsEnabled}">
                            	     <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
                        </h:selectBooleanCheckbox>			                
				</td>
			</tr>
		</tbody>
	</table>
	<h:panelGroup rendered="#{filePreferenceBackingBean.companyDTO.pcoCustomer eq true}">
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			
		</thead>
		<tbody>
		  <tr><th colspan="4">Sales Engine API Options</th></tr>
		   <tr>
		        <td style="width:50px;">&#160;</td>
				<td>Database:</td>
				<td>

				<h:inputText id="saleDatabase" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.saleApiDatabase}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Server:</td>
				<td>

				<h:inputText id="saleServer" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.saleApiServer}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>User ID:</td>
				<td>

				<h:inputText id="saleUserId" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.saleApiUserId}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Password:</td>
				<td>

				<h:inputText id="salePassword" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.saleApiPassword}" size="60" >
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td style="width:100px;">&#160;</td>
			</tr>	

		</tbody>
	</table>
	</h:panelGroup>
	</div>
	</div>
	<div id="table-one-bottom">
	<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="verify"><h:commandLink id="verifyButton" 
			actionListener="#{filePreferenceBackingBean.verifyPinPointAPIActionListener}"
			reRender="updateButton, cancelButton, errorMessage"  /></li>
			
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateAdminImportMapProcessAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelAdminImportMapProcessAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>