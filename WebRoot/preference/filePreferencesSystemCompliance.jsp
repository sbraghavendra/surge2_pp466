<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk">      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
//<![CDATA[
function copyMe(from,to){
  document.getElementById(to).value = document.getElementById(from).value;
}
//]]>
</ui:define>
<ui:define name="body">
<f:view>
<h:form id="fpSCForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Compliance</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Default CCH Tax Codes</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>CCH Tax Category:</td>
				<td style="width:660px;">
                <h:selectOneMenu id="defCCHTaxCatCode" 
                	valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.defCCHTaxCatCode}" 
                    style="width: 300px;">
		            <f:selectItems id="cchTaxCatItems" value="#{filePreferenceBackingBean.cchTaxCatItems}" />  
		              <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                   
                </h:selectOneMenu>			      
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>CCH Group:</td>
			    <td style="width:660px;">
			    <h:selectOneMenu id="defCCHGroupCode" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
                    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.defCCHGroupCode}" 
                    style="width: 300px;">
		            <f:selectItems id="cchTaxGrpItems" value="#{filePreferenceBackingBean.cchTaxGrpItems}" />  
		             	  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                     
                </h:selectOneMenu>
				</td>
			</tr>
			<tr><th colspan="3">Tax Partners Extract File Name</th></tr>
			<tr>
				<td style="width:100px;">&#160;</td>
				<td>File Path:</td>
		        <td style="width:660px;">	
             <h:inputText id="fileName" style="border: none; background: transparent; color: black;"
                          size="60"
                          disabled="false"
			                    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.tpExtractFilePath}">
             </h:inputText>					       
             <br />
				    <t:inputFileUpload id="fileSrc" 
				                       value="#{filePreferenceBackingBean.fileSrc}"  				    
				                       size="70">
		          <a4j:support event="onchange" 
                           actionListener="#{filePreferenceBackingBean.updateButtonActionListener}"
                           ajaxSingle="true"
                           oncomplete="copyMe('fpSCForm:fileSrc','fpSCForm:fileName')"
                           reRender="updateButton, cancelButton, errorMessage" />
            </t:inputFileUpload> 		
				</td>
			</tr>
			<tr>
				<td style="width:100px;">&#160;</td>
				<td>File Prefix:</td>
				<td style="width:660px;">
		        <h:inputText id="tpExtractFilePrefix" 
		        valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.tpExtractFilePrefix}" size="20">
			    	  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>					       
				</td>
			</tr>
			<tr>
				<td style="width:100px;">&#160;</td>
				<td>File Extension:</td>
				<td style="width:660px;">
		        <h:inputText id="tpExtractExt" 
		        valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
			    value="#{filePreferenceBackingBean.importMapProcessPreferenceDTO.tpExtractExt}" size="20">
			    	  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                        </h:inputText>					       
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.updateSystemComplianceAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemComplianceAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>