<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpUGForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>

	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">General</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Purchase View:</td>
				<td>
				<h:selectOneMenu id="viewItemsList"  value="#{filePreferenceBackingBean.userPreferenceDTO.gridLayout}" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}" style="width: 200px;" > 
							
					<f:selectItems id="viewItems" value="#{filePreferenceBackingBean.viewItemsList}" />
				  		<a4j:support event="onchange" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />
          
				</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>	
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>User Time Zone:</td>
				<td>
				<h:selectOneMenu id="userTimeZone" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				value="#{filePreferenceBackingBean.userPreferenceDTO.userTimeZoneID}" style="width: 200px;">
        		<f:selectItems id="userTimeZoneItms" value="#{filePreferenceBackingBean.timeZoneList}" />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>	
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Default Module:</td>
				<td>
				<!-- value="#{filePreferenceBackingBean.userPreferenceDTO.module}" -->
				<h:selectOneMenu id="userModule" value="#{filePreferenceBackingBean.selectedModule}" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"	
				 style="width: 200px;">
        		<f:selectItems id="moduleItms" value="#{filePreferenceBackingBean.moduleList}"  />
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>	
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Max Decimal Places:</td>
				<td>
				<h:selectOneMenu id="maxdecimalplaces" value="#{filePreferenceBackingBean.maxdecimalplace}"  immediate="true"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonMaxDecAction}" style="width: 200px;">
        		 <f:selectItems value="#{filePreferenceBackingBean.maxDecimalListItems}"/>
				  <a4j:support event="onchange" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />     
                       		</h:selectOneMenu>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Use Lookup For TaxCode Selection:</td>
				<td>
				<h:selectBooleanCheckbox styleClass="check" id="alertOnLogon" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.userPreferenceDTO.useTaxCodeLookup}">
				 <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
				</h:selectBooleanCheckbox>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>			
		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
	<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateUserGeneralAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelUserGeneralAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>