<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpABForm">

<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">Batches</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="5">Batch Process</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Interval Seconds:</td>
				<td>
				<h:inputText id="intervalsec" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.batchPreferenceDTO.adminIntervalTime}" style="width: 200px;">
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:inputText>
				</td>
				<td>(0 = off, 30 = default)</td>
				<td>&#160;</td>
			</tr>
			
			<tr><th colspan="5">Scheduled Start Time</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Default Time:</td>
				<td>
				<h:inputText id="defTime" 
           validator="TimeValidator"
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.batchPreferenceDTO.adminDefTime}" size="12">
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:inputText>
				</td>
				<td>&#160;</td>
				<td style="width:500px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Allow Users to Change?:</td>
				<td>

				<h:selectBooleanCheckbox styleClass="check" id="chkUserChange"
				 valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.batchPreferenceDTO.adminUserFlag}">
				  <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:selectBooleanCheckbox>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr><th colspan="5">Statistics Recalc Seconds</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Seconds:</td>
				<td>
				<h:inputText id="recalsec" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.batchPreferenceDTO.adminRecalTime}" style="width: 200px;">
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:inputText>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Allow Users to Change?:</td>
				<td>
				<h:selectBooleanCheckbox styleClass="check" id="recalflag" 
				 valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.batchPreferenceDTO.adminRecalUserFlag}" >
							  <a4j:support event="onclick" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />  
				</h:selectBooleanCheckbox>
				</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom" style="padding: 2px">
    <a4j:outputPanel id="msg">
      <h:messages errorClass="error" />
    </a4j:outputPanel>
	<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateAdminBatchesAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelAdminBatchesAction}"/></li>
	</ul>
	</div>
	</div>
	</div>
</div>
</h:form>

</f:view>
</ui:define>
</ui:composition>
</html>