<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpAGForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6">AID&#160;Service</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="6">AID&#160;Service</th></tr>
			
			<tr>
				<td>&#160;</td>
				<td>&#160;</td>
				<td>Main:</td>		
				<td>Load Balanced:</td>
				<td style="width:200px;">&#160;</td>
				<td style="width:200px;">&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Host Name:</td>
				<td>
					<h:inputText id="aidHostname" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.aidHostname}" style="width: 80px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td>
					<h:inputText id="aidHostname2" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.aidHostname2}" style="width: 80px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			 <tr>
				<td style="width:50px;">&#160;</td>
				<td>Port:</td>
				<td>
					<h:inputText id="port" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.aidPort}" style="width: 80px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td>
					<h:inputText id="port2" valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.aidPort2}" style="width: 80px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
                <td>Status:</td>
                <td>
                    <h:outputText id="status" value="#{filePreferenceBackingBean.aidStatus}" />&#160;
                </td>
                <td>
                    <h:outputText id="status2" value="#{filePreferenceBackingBean.aidStatus2}" />&#160;
                </td>
                <td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
            </tr>	
            
            <tr><th colspan="6">SMTP&#160;Settings</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Host Name:</td>
				<td>

				<h:inputText id="emailHost" 
				valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
				value="#{filePreferenceBackingBean.emailHost}" style="width: 80px;">
				  <a4j:support event="onkeyup" 
                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
				</h:inputText>

				</td>
				<td>&#160;</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Port:</td>
				<td>
					<h:inputText id="emailPort" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
						value="#{filePreferenceBackingBean.emailPort}" style="width: 80px;">
					  	<a4j:support event="onkeyup" 
	                        ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td>&#160;</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>

			<tr><th colspan="6">Email&#160;Notifications&#160;</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Send email from:&#160;&#160;&#160;&#160;&#160;&#160;</td>
				<td colspan="2">
					<h:inputText id="emailFrom" 
							valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.emailFrom}" style="width: 400px;">
				  		<a4j:support event="onkeyup" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr>
			
            <tr><th colspan="6">AID&#160;Setting</th></tr>         
           <tr>
			    <td style="width:50px;">&#160;</td>
				<td>Installation&#160;Folder&#160;Path:</td>
				<td colspan="2">
					<h:inputText id="installationFolderPath" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
						value="#{filePreferenceBackingBean.aidInstallation}" style="width: 400px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr> 
			
			<tr>
			    <td style="width:50px;">&#160;</td>
				<td>Days&#160;to&#160;keep&#160;AID&#160;files:</td>
				<td colspan="2">
					<h:inputText id="numberofdaystokeep" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
						value="#{filePreferenceBackingBean.daysToKeep}" style="width: 400px;">
				  		<a4j:support event="onkeyup" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				<td style="width:20%;">&#160;</td>
				<td style="width:100px;">&#160;</td>
			</tr> 		

		</tbody>
	</table>
	</div>
	</div>
	<div id="table-one-bottom">
	   <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>      
	<ul class="right">
		<li class="resumeservice"><h:commandLink id="resumeserviceButton" 
			disabled="#{!filePreferenceBackingBean.resumeButtonEnabled or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
			actionListener="#{filePreferenceBackingBean.aidResumeActionListener}"
			reRender="resumeserviceButton,suspendserviceButton,status, updateButton, cancelButton, errorMessage"  /></li>
			
		<li class="suspendservice"><h:commandLink id="suspendserviceButton" 
			disabled="#{!filePreferenceBackingBean.suspendButtonEnabled or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
			actionListener="#{filePreferenceBackingBean.aidSuspendActionListener}"
			reRender="resumeserviceButton,suspendserviceButton,status, updateButton, cancelButton, errorMessage"  /></li>
		
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateAdminAIDAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelAdminAIDAction}"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>