<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="fpAGForm">
<h1><img id="imgFilePreferences" alt="File Preferences" src="../images/headers/hdr-file-preferences.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		Role: 
		<h:selectOneMenu id="selRole" value="#{filePreferenceBackingBean.selectedRole}">
        <f:selectItems id="selRoleItms" value="#{filePreferenceBackingBean.roleItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.roleSubmit}" />
        </h:selectOneMenu>
		&#160;&#160;Function: 
		<h:selectOneMenu id="selPreference" value="#{filePreferenceBackingBean.selectedPreference}">
        <f:selectItems id="selPreferenceItms" value="#{filePreferenceBackingBean.preferencesItems}" />
		<a4j:support event="onchange" action="#{filePreferenceBackingBean.preferenceSubmit}" />
        </h:selectOneMenu>
	</div>
	<div id="table-one-content" style="height:418px;">
	<div class="scrollInner" id="resize" >
	<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="11">AID&#160;Service</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="11">Email&#160;Notifications&#160;</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="2">When a file import begins:</td>
				<td colspan="7">
					<h:selectBooleanCheckbox styleClass="check" id="emailStart" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
						value="#{filePreferenceBackingBean.emailStart}">
					 	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
					</h:selectBooleanCheckbox>
				</td>
				
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="2">After a file import is complete:</td>
				<td colspan="7">
					<h:selectOneRadio id="emailStop"  value="#{filePreferenceBackingBean.emailStop}" 
						valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"> 
                    		<f:selectItem itemValue="0" itemLabel="Never"/>
                    		<f:selectItem itemValue="99" itemLabel="Always"/>  
                    		<f:selectItem itemValue="20" itemLabel="On Error Only"/>
                    	<a4j:support event="onclick" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />   
					</h:selectOneRadio>
				</td>
				
			</tr>	
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="2">Send email to:&#160;&#160;&#160;&#160;&#160;&#160;</td>
				<td style="width:25px;">To:</td>
				<td colspan="7">
					<h:inputText id="emailTo" 
							valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.emailTo}" style="width: 450px;">
				  		<a4j:support event="onkeyup" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="2">&#160;&#160;&#160;&#160;&#160;&#160;</td>
				<td style="width:25px;">Cc:</td>
				<td colspan="7">
					<h:inputText id="emailCc" 
							valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
							value="#{filePreferenceBackingBean.emailCc}" style="width: 450px;">
				  		<a4j:support event="onkeyup" 
                        	ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />                    
					</h:inputText>
				</td>
				
			</tr>
			</tbody>
	      </table>
		<table cellpadding="0" cellspacing="0" width="911" id="rollover" class="ruler">
			<tr><th colspan="11">User&#160;File&#160;Access</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td  style="width:156px;">Add Import Files from Local PC?:</td>
				<td style="width:50px;">
				<h:selectBooleanCheckbox styleClass="check" id="allowAddFile" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.allowAidAddFile}">
				 	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
				</h:selectBooleanCheckbox>
				</td>	
				<td>&#160;</td>
				<td style="width:120px;">Re-Import Files in AID?:</td>
				<td style="width:50px;">
				<h:selectBooleanCheckbox styleClass="check" id="allowReimportFile" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.allowAidReimportFile}">
				 	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
				</h:selectBooleanCheckbox>
				</td>	
				<td>&#160;</td>
				<td style="width:120px;">Delete Files from AID?:</td>
				<td colspan="3">
				<h:selectBooleanCheckbox styleClass="check" id="allowDeleteFile" 
					valueChangeListener="#{filePreferenceBackingBean.updateButtonAction}"
					value="#{filePreferenceBackingBean.allowAidDeleteFile}">
				 	<a4j:support event="onclick" ajaxSingle="true" reRender="updateButton, cancelButton, errorMessage" />    
				</h:selectBooleanCheckbox>
				</td>
				
			</tr>	 
	</table>
	</div>
    </div>
    <div id="table-one-bottom">
	   <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="errorMessage"
                    disabled="#{!filePreferenceBackingBean.displayButton}"  
                    value="#{filePreferenceBackingBean.errorMessage}"/>      
	<ul class="right">
		<li class="update2"><h:commandLink id="updateButton" 
		disabled="#{!filePreferenceBackingBean.displayButton or filePreferenceBackingBean.currentUser.viewOnlyBooleanFlag}"
		action="#{filePreferenceBackingBean.updateSystemAIDAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelButton" immediate="true" 
		disabled="#{!filePreferenceBackingBean.displayButton}"
		action="#{filePreferenceBackingBean.cancelSystemAIDAction}"/></li>
	</ul>
    </div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>