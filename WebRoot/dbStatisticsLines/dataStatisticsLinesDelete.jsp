<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmDbStatLinesDelete">
<h1><h:graphicImage id="imgDbStatLines" alt="DB Statistics Lines" value="/images/headers/hdr-db-statistics-lines.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Delete a DB Statistics Line</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>DB Statistics ID:</td>
				<td style="width:700px;">
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.dataStatisticsId}" 
				         disabled="true" id="txtDbStatisticsId"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Row Title:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.rowTitle}" 
                         disabled="true" id="txtRowTitle"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Where Clause:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.rowWhere}" 
                         disabled="true" id="txtWhereClause"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Group by on Drilldown:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.groupByOnDrilldown}" 
                         disabled="true" id="txtGroupByOnDrilldown"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.selectedDataStatistics.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
                         size="4"				
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{dataStatisticsBackingBean.removeAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{dataStatisticsBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>