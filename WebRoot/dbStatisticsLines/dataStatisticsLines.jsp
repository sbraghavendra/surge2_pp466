<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('dataStatisticsForm:dataStatisticsTable', 'dataStatisticsTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="dataStatisticsTableRowIndex" value="#{dataStatisticsBackingBean.selectedRowIndex}"/>
<h:form id="dataStatisticsForm">

<h1><h:graphicImage id="imgDbStatLines" alt="DB Statistics Lines" value="/images/headers/hdr-db-statistics-lines.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	
	<div class="scrollContainer">
		<div class="scrollInner" id="resize" >
         <rich:dataTable rowClasses="odd-row,even-row" 
				id="dataStatisticsTable" styleClass="Tabruler" headerClass="header"																
				value="#{dataStatisticsBackingBean.dataStatisticsList}" var="dbsl" >
	             
	         <a4j:support id="clk" event="onRowClick" status="rowstatus"
					onsubmit="selectRow('dataStatisticsForm:dataStatisticsTable', this);"
			       	actionListener="#{dataStatisticsBackingBean.selectedRowChanged}" reRender="updateBtn,deleteBtn"/>	
	             														
				<rich:column id="dataStatisticsId" sortBy="#{dbsl.dataStatisticsId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="DB Statistics ID" />
					</f:facet>
					<h:outputText value="#{dbsl.dataStatisticsId}"/>
				</rich:column>
				<rich:column id="rowTitleHTML" sortBy="#{dbsl.rowTitleHTML}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Row Title"/>
					</f:facet>
					<h:outputText escape="false" value="#{dbsl.rowTitleHTML}" />
				</rich:column>
				<rich:column id="rowWhere" width="280px" sortBy="#{dbsl.rowWhere}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Row Where"/>
					</f:facet>
					<h:outputText value="#{dbsl.rowWhere}" />
				</rich:column>		
				<rich:column id="groupByOnDrilldown" width="300px" sortBy="#{dbsl.groupByOnDrilldown}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Group By On Drilldown"/>
					</f:facet>
					<h:outputText value="#{dbsl.groupByOnDrilldown}" />
				</rich:column>
			</rich:dataTable>
		</div>	
	</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="btnAdd" disabled="#{dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag}" style="#{(dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{dataStatisticsBackingBean.displayAddAction}" /></li>
		<li class="update"><h:commandLink id="updateBtn" style="#{(dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!dataStatisticsBackingBean.showButtons or dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{dataStatisticsBackingBean.displayUpdateAction}" /></li>		
		<li class="delete"><h:commandLink id="deleteBtn" style="#{(dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!dataStatisticsBackingBean.showButtons or dataStatisticsBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{dataStatisticsBackingBean.displayDeleteAction}" /></li>		
	</ul>
	</div>
	</div>
</div>

</h:form>


</ui:define>
</ui:composition>
</html>