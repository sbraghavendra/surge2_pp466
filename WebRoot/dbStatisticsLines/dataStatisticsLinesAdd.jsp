<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmDbStatLinesAdd">
<h1><h:graphicImage id="imgDbStatLines" alt="DB Statistics Lines" value="/images/headers/hdr-db-statistics-lines.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Add a DB Statistics Line</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>DB Statistics ID:</td>
				<td style="width:700px;">
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.dataStatisticsId}" 
				         required="true" label="DB Statistics ID" id="txtDbStatisticsId"
				         validator="#{dataStatisticsBackingBean.validateDBStatisticsID}"
				         onkeypress="return onlyIntegerValue(event);"
				         style="width:650px;">
					<f:convertNumber integerOnly="true" />
				</h:inputText>
				</td>				       
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Row Title:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.rowTitle}" 
				         required="true" label="Row Title" id="txtRowTitle"
				         style="width:650px;" 
				         validator="#{dataStatisticsBackingBean.validateRowTitle}" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Where Clause:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.rowWhere}" 
				         style="width:650px;" id="txtWhereClause"
				         validator="#{dataStatisticsBackingBean.validateWhereClause}" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Group by on Drilldown:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.groupByOnDrilldown}" 
				         style="width:650px;" id="txtGroupByOnDrilldown"
				         validator="#{dataStatisticsBackingBean.validateGroupByOnDrilldown}" />
                </td>
			</tr>			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{dataStatisticsBackingBean.dataStatisticsDTONew.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{dataStatisticsBackingBean.addAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{dataStatisticsBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>