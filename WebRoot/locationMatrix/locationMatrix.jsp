<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('locationMatrixForm:aMList', 'aMListRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('locationMatrixForm:aMDetail', 'aMDetailListRowIndex'); } );
registerEvent(window, "unload", function() { document.getElementById('locationMatrixForm:hiddenSaveLink').onclick(); });

//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="aMListRowIndex" value="#{locationMatrixBean.selectedMasterRowIndex}"/>
<h:inputHidden id="aMDetailListRowIndex" value="#{locationMatrixBean.selectedRowIndex}"/>
<h:form id="locationMatrixForm">
<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
	<a4j:support event="onclick" actionListener="#{locationMatrixBean.filterSaveAction}" />
</a4j:commandLink>

<h:panelGroup rendered="#{!locationMatrixBean.findMode}">
	<h1><h:graphicImage id="image2"
   alt="Location Matrix"
   url="/images/headers/hdr-location-matrix.gif">
</h:graphicImage>  </h1>
</h:panelGroup>
<h:panelGroup rendered="#{locationMatrixBean.findMode}">
	<h1>
	<h:graphicImage id="image3"
   alt="Location Matrix"
   url="/images/headers/hdr-lookup-location-matrix-id.gif">
</h:graphicImage>  
	</h1>
</h:panelGroup>

<div class="tab">
<h:graphicImage id="image4"
   alt="Selection Filter"
   url="/images/containers/STSSelection-filter-open.gif">
</h:graphicImage>  </div>
<div id="top" style = "width:945px">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" onkeyup="return submitEnter(event,'locationMatrixForm:searchBtn')" >
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label-gsm">Entity:</td>
			<td>		
                <h:selectOneMenu id="entityVal" style="float:left;width:140px;" value="#{locationMatrixBean.entityCodeFilter}" immediate="true">
					<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
                </h:selectOneMenu>      
				<h:commandButton id="entityIdBtn" styleClass="image" 
						disabled="#{locationMatrixBean.isEntityCheck}" style="float:left;width:16px;height:16px;" 
						image="/images/search_small.png" immediate="true" 
						action="#{commonCallBack.findLocationMatrixIdAction}">
				</h:commandButton>							
			</td>
			<td class="column-label">&#160;</td>
			
			<td>&#160;</td>
		</tr>
		</tbody>
	</table>

	<h:panelGrid id="filterPanel" width="100%" binding="#{locationMatrixBean.filterSelectionPanel}" styleClass="panel-ruler"/>

	<h:panelGroup rendered="#{matrixCommonBean.userDefaultMatrixLineFlag}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label-gsm">Default Line:</td>
			<td class="column-input">
				<h:selectOneMenu id="defVal"
					value="#{locationMatrixBean.defaultLineFilter}">
					<f:selectItems value="#{matrixCommonBean.defaultLineItems}" />
                </h:selectOneMenu>
			</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td>&#160;</td>
		</tr>
		</tbody>
	</table>
  </h:panelGroup>

	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Jurisdiction:</td>
			<td colspan="5">
				<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
					<ui:param name="handler" value="#{locationMatrixBean.filterHandler}"/>
					<ui:param name="id" value="jurisInput"/>
					<ui:param name="readonly" value="false"/>
					<ui:param name="showheaders" value="true"/>
					<ui:param name="popupName" value="searchJurisdiction"/>
					<ui:param name="popupForm" value="searchJurisdictionForm"/>
				</ui:include>
			</td>
			<td>&#160;</td>
		</tr>
		</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Location Matrix ID:</td>
			<td class="column-input">
				<h:inputText id="lmId" style="text-align:right;"
					value="#{locationMatrixBean.filterSelection.locationMatrixId}"
					onkeypress="return onlyIntegerValue(event);"
					label="Matrix ID">
					<f:convertNumber integerOnly="true" groupingUsed="false"/>
				</h:inputText>
			</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td class="column-label">&#160;</td>
			<td class="column-input">&#160;</td>
			<td>&#160;</td>
		</tr>		
		</tbody>
	</table>
	</div>

	<div id="table-one-bottom">
	<ul class="right">
		<li class="clear"><a4j:commandLink id="btnClear" action="#{locationMatrixBean.resetFilter}" /></li>
		<li class="search"><a4j:commandLink id="searchBtn" action="#{locationMatrixBean.updateMatrixList}" >
<!--			 	<rich:componentControl event="onclick" for="loader" attachTo="searchBtn" operation="show"/>   -->             
			</a4j:commandLink></li>
	</ul>
	</div>
	</div>
</div>

<rich:modalPanel id="loader" zindex="2000" autosized="true">
              <h:outputText value="Processing Search ..."/>
</rich:modalPanel>

<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
    	
<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<h:graphicImage id="image5"
   alt="Location-Matrix-Lines-and-Detials"
   url="/images/containers/STSLocation-Matrix-Lines-and-Detials.gif">
</h:graphicImage>
	
	</span>
</div>

<!-- Start data table -->
<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top" style="padding-left: 15px;">
		<a4j:status id="pageInfo"  
			startText="Request being processed..." 
			stopText="#{locationMatrixBean.matrixDataModel.pageDescription}"
			onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     		onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	</div>
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner" id="resize">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMList"
				value="#{locationMatrixBean.matrixDataModel}" var="matrix"
				rows="#{locationMatrixBean.matrixDataModel.pageSize}">
				
				<a4j:support event="onRowClick"
						onsubmit="selectRow('locationMatrixForm:aMList', this);"
						actionListener="#{locationMatrixBean.selectedMasterRowChanged}"
						reRender="aMDetail,viewBtn,copyAddBtn,deleteBtn,suspendBtn,effectivedateBtn,updateBtn,okBtn"
						oncomplete="initScrollingTables();" />
				
				<rich:column id="defaultFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_defaultFlag" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['defaultFlag']}"
							value="Default Flag"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:selectBooleanCheckbox value="#{matrix.defaultBooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="entityId" style="text-align:left;">
					<f:facet name="header">
						<a4j:commandLink id="s_entityId" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['entityId']}"
							value="Entity"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{locationMatrixBean.entityMap[matrix.entityId]}" />
				</rich:column>

				<rich:column id="driver01" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver01" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver01']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_01']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver01}" />
				</rich:column>
				
				<rich:column id="driver02" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver02" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver02']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_02']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver02}" />
				</rich:column>
				
				<rich:column id="driver03" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver03" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver03']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_03']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver03}" />
				</rich:column>
				
				<rich:column id="driver04" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver04" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver04']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_04']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver04}" />
				</rich:column>
				
				<rich:column id="driver05" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver05" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver05']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_05']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver05}" />
				</rich:column>
				
				<rich:column id="driver06" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver06" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver06']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_06']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver06}" />
				</rich:column>
				
				<rich:column id="driver07" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver07" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver07']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_07']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver07}" />
				</rich:column>
				
				<rich:column id="driver08" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver08" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver08']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_08']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver08}" />
				</rich:column>
				
				<rich:column id="driver09" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver09" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver09']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_09']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver09}" />
				</rich:column>
				
				<rich:column id="driver10" rendered="#{!empty matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver10" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['driver10']}"
							value="#{matrixCommonBean.locationMatrixColumnMap['DRIVER_10']}"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.driver10}" />
				</rich:column>

				<rich:column id="binaryWt">
					<f:facet name="header">
						<a4j:commandLink id="s_binaryWt" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['binaryWt']}"
							value="Binary Weight"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.binaryWt}" />
				</rich:column>
		
				<rich:column id="defltBinaryWt">
					<f:facet name="header">
						<a4j:commandLink id="s_defltBinaryWt" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortOrder['defltBinaryWt']}"
							value="Default Weight"
							actionListener="#{locationMatrixBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					<h:outputText value="#{matrix.defltBinaryWt}" />
				</rich:column>
		
			</rich:dataTable>
		</div> 
		</div>

		<rich:datascroller id="trScroll" for="aMList" maxPages="10" oncomplete="initScrollingTables();"
			style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"
			page="#{locationMatrixBean.matrixDataModel.curPage}" 
			reRender="pageInfo"/>
			
			
		<div class="scrollContainer">
		<div class="scrollInner" id="resize">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail"
				value="#{locationMatrixBean.selectedMatrixDetails}" var="locationMatrixDetail" >
				
				<a4j:support event="onRowClick"
						onsubmit="selectRow('locationMatrixForm:aMDetail', this);"
						actionListener="#{locationMatrixBean.selectedRowChanged}"
						reRender="viewBtn,copyAddBtn,deleteBtn,suspendBtn,effectivedateBtn,updateBtn,okBtn"/>

				<rich:column id="locationMatrixId">
				<f:facet name="header">
					<a4j:commandLink id="s_locationMatrixId" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['locationMatrixId']}"
						value="Matrix ID" 
						actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();"
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{locationMatrixDetail.locationMatrixId}" />
				</rich:column>			

				<rich:column id="effectiveDate">
					<f:facet name="header">
						<a4j:commandLink id="s_effectiveDate" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['effectiveDate']}"
							value="Effective Date"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.effectiveDate}" >
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
				</rich:column>
				
				<rich:column id="geoCodeId">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="GeoCode" />
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.geocode}" />
				</rich:column>
				
				<rich:column id="cityId">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="City" />
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.city}" />
				</rich:column>
				
				<rich:column id="countyId">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="County" />
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.county}" />
				</rich:column>

				<rich:column id="state">
					<f:facet name="header">
						<a4j:commandLink id="s_state" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['state']}"
							value="State"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.state}" />
				</rich:column>
				
				<rich:column id="country">
					<f:facet name="header">
						<a4j:commandLink id="s_country" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['country']}"
							value="Country"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.country}" />
				</rich:column>
	
				<rich:column id="zipId">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="zipCode" />
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.jurisdiction.zip}" />
				</rich:column>

				<rich:column id="expirationDate">
					<f:facet name="header">
						<a4j:commandLink id="s_expirationDate" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['expirationDate']}"
							value="Expiration Date"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.expirationDate}" >
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:outputText>
				</rich:column>
			
				<rich:column id="overrideTaxtypeCode">
					<f:facet name="header">
						<a4j:commandLink id="s_overrideTaxtypeCode" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['overrideTaxtypeCode']}"
							value="Override Tax Type Code"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.overrideTaxtypeCode}" />
				</rich:column>

				<rich:column id="countryFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_countryFlag" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['countryFlag']}"
							value="Country Flag"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.countryBooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stateFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stateFlag" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['stateFlag']}"
							value="State Flag"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stateBooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>

				<rich:column id="countyFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_countyFlag" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['countyFlag']}"
							value="County Flag"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.countyBooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
	
				<rich:column id="cityFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_cityFlag" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['cityFlag']}"
							value="City Flag"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.cityBooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stj1BooleanFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stj1BooleanFlag"  value="STJ1 Flag" />
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stj1BooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stj2BooleanFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stj2BooleanFlag"  value="STJ2 Flag" />
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stj2BooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stj3BooleanFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stj3BooleanFlag"  value="STJ3 Flag" />
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stj3BooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stj4BooleanFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stj4BooleanFlag"  value="STJ4 Flag" />
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stj4BooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
				<rich:column id="stj5BooleanFlag" style="text-align:center;">
					<f:facet name="header">
						<a4j:commandLink id="s_stj5BooleanFlag"  value="STJ5 Flag" />
					</f:facet>
					<h:selectBooleanCheckbox value="#{locationMatrixDetail.stj5BooleanFlag}" disabled="true" styleClass="check"/>
				</rich:column>
				
                <rich:column id="comments">
                    <f:facet name="header">
						<a4j:commandLink id="s_comments" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['comments']}"
							value="Comments"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
                    </f:facet>
                    <h:outputText value="#{locationMatrixDetail.comments}"/>
                </rich:column>                 

				<rich:column id="updateUserId">
					<f:facet name="header">
						<a4j:commandLink id="s_updateUserId" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['updateUserId']}"
							value="Last Update UserId"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.updateUserId}" />
				</rich:column>

				<rich:column id="updateTimestamp">
					<f:facet name="header">
						<a4j:commandLink id="s_updateTimestamp" styleClass="sort-#{locationMatrixBean.matrixDataModel.sortDetailOrder['updateTimestamp']}"
							value="Time Stamp"
							actionListener="#{locationMatrixBean.sortDetailAction}" oncomplete="initScrollingTables();"
							immediate="true" reRender="aMDetail"/>
					</f:facet>
					<h:outputText value="#{locationMatrixDetail.updateTimestamp}">
						<f:converter converterId="dateTime"/>
					</h:outputText>
				</rich:column>
		
			</rich:dataTable>
		</div> 
		</div>		
		    
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
	<h:panelGroup rendered="#{!locationMatrixBean.findMode}">
		<li class="add"><h:commandLink id="addBtn" disabled = "#{locationMatrixBean.currentUser.viewOnlyBooleanFlag}"  style="#{locationMatrixBean.validSelection? 'display:block;':'display:none;' or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{locationMatrixBean.displayAddAction}" /></li>
		<li class="copy-add"><h:commandLink id="copyAddBtn" style="#{locationMatrixBean.validSelection? 'display:block;':'display:none;' or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" disabled="#{!locationMatrixBean.validSelection or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{locationMatrixBean.displayCopyAddAction}" /></li>
		<li class="update"><h:commandLink id="updateBtn" style="#{locationMatrixBean.validSelection? 'display:block;':'display:none;' or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" disabled="#{!locationMatrixBean.validSelection or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{locationMatrixBean.displayUpdateAction}" /></li>
		<li class="delete115"><h:commandLink id="deleteBtn" disabled="#{!locationMatrixBean.validSelection or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{locationMatrixBean.displayDeleteAction}" /></li>
		<li class="suspend">
			<a4j:commandLink id="suspendBtn" disabled = "#{locationMatrixBean.currentUser.viewOnlyBooleanFlag}" 
							style="#{(!locationMatrixBean.validSelection or locationMatrixBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
							immediate="true" actionListener="#{locationMatrixBean.displaySuspendAction}"
							oncomplete="javascript:Richfaces.showModalPanel('suspend'); return false;" 
							reRender="suspendForm"/>
		</li>
		<li class="effectivedate"><h:commandLink id="effectivedateBtn" style="#{locationMatrixBean.validSelection? 'display:block;':'display:none;' or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" disabled="#{!locationMatrixBean.effectivedateSelection or locationMatrixBean.currentUser.viewOnlyBooleanFlag}" action="#{locationMatrixBean.displayBackDateAction}" /></li>
		<li class="view"><h:commandLink id="viewBtn" disabled="#{!locationMatrixBean.validSelection}" action="#{locationMatrixBean.displayViewAction}" /></li>
	</h:panelGroup>
	<h:panelGroup rendered="#{locationMatrixBean.findMode}">
		<li class="ok2"><h:commandLink id="okBtn" disabled="#{!locationMatrixBean.validSelection}" immediate="true" action="#{locationMatrixBean.findAction}"/></li>
		<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{locationMatrixBean.cancelFindAction}" /></li>
	</h:panelGroup>
	</ul>
	</div>

	</div>
</div>
</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{locationMatrixBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="locationMatrixForm:jurisInput"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/suspend_matrix_dialog.xhtml">
	<ui:param name="bean" value="#{locationMatrixBean}"/>
	<ui:param name="detail" value="#{locationMatrixBean.prossdTrDatailBean}"/>
	<ui:param name="type" value="Location"/>
	<ui:param name="isTax" value="false"/>
</ui:include>

</ui:define>

</ui:composition>

</html>
