<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMDetailList', 'aMDetailListRowIndex'); } );
registerEvent(window, "load", function() { displayWarning(); } );
registerEvent(window, "load", function() { displayBackdateWarning(); } );
registerEvent(window, "load", function() { displayBackdateExistWarning(); } );

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function displayBackdateExistWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 1) {
	 	javascript:Richfaces.showModalPanel('backdateexistwarning');
	 }
}

function displayBackdateWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 2) {
	 	javascript:Richfaces.showModalPanel('backdatewarning');
	 }
}

//]]>
</script>
</ui:define>


<ui:define name="body">
	<h:inputHidden id="displayBackdateWarning" immediate="true" value="#{locationMatrixBean.displayBackdateWarning}"/>
	<h:inputHidden id="displayWarning" value="#{locationMatrixBean.displayWarning}"/>
	<h:inputHidden id="aMDetailListRowIndex" value="#{locationMatrixBean.selectedTransactionIndex}"/>

<h:form id="matrixDetailForm">

<h1><img id="imgLocationMatrix" alt="Location Matrix" src="../images/headers/hdr-location-matrix.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">

	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				<td colspan="4"><h:outputText value="#{locationMatrixBean.actionText}"/> a Location Matrix Line <h:outputText value="#{locationMatrixBean.actionTextSuffix}"/></td>
				<td colspan="4" style="text-align:right;">Location Matrix ID:&#160;<h:outputText value="#{locationMatrixBean.matrixId}"/></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="7">Driver Selection</th>
				<th style="text-align:right;">
					<h:outputText id="txtDefaultMatrixLine" value="#{locationMatrixBean.selectedMatrix.defaultBooleanFlag ? 'Default Matrix Line':'&#160;'}"
						style="color:red; font-weight: bold;" />
				</th>
			</tr>
      <c:if test="#{locationMatrixBean.showDefaultLine}">
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm" style = "width:129px">Default Line: </td>
				<td class="column-input">
					<h:selectBooleanCheckbox id="chkDefaultLine" styleClass="check" 
						value="#{locationMatrixBean.selectedMatrix.defaultBooleanFlag}" immediate="true"	
						disabled="#{locationMatrixBean.readOnlyDrivers}" >
						<a4j:support id="ajaxchkDefaultLine" event="onclick" reRender="txtDefaultMatrixLine" limitToList="true" ajaxSingle="true" />
					</h:selectBooleanCheckbox>
				</td>
				<td colspan="4">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
      </c:if>
      		<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label-gsm" style = "width:129px"><h:outputText value="Entity:" ></h:outputText></td>
				<td>
					<c:if test="#{!(locationMatrixBean.isEntityCheck or !locationMatrixBean.isAddAction)}">					
						<h:selectOneMenu id="entityId" style="float:left;width:140px;" disabled="#{locationMatrixBean.isEntityCheck or !locationMatrixBean.isAddAction}"
										value="#{locationMatrixBean.entityCode}" immediate="true">
						<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
                		</h:selectOneMenu> 
                	</c:if>
                	<c:if test="#{locationMatrixBean.isEntityCheck or !locationMatrixBean.isAddAction}">
          				<h:inputText id="entityCodeCode" style="width: 140px;" disabled="true"
							value="#{locationMatrixBean.entityCodeDesc}" >
						</h:inputText>		
	            	</c:if>
                	
                	<c:if test="#{!(locationMatrixBean.isEntityCheck or !locationMatrixBean.isAddAction)}">	     							
						<h:commandButton id="entityIdBtn" styleClass="image" 
							disabled="#{locationMatrixBean.isEntityCheck or !locationMatrixBean.isAddAction}" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png" immediate="true" 
							action="#{commonCallBack.findLocationMatrixIdUpdateAction}">
						</h:commandButton>
					</c:if>					
  				</td>
				<td colspan="4">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	<h:panelGrid id="driverPanel"
		rendered="#{!locationMatrixBean.readOnlyDrivers}" 
		binding="#{locationMatrixBean.copyAddPanel}"
		styleClass="panel-ruler"/>
	<h:panelGrid id="driverViewPanel" width="100%" 
		rendered="#{locationMatrixBean.readOnlyDrivers}" 
		binding="#{locationMatrixBean.viewPanel}"
		styleClass="panel-ruler"/>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="8">Details</th>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Dates:</td>
			<td colspan="5">
			<table cellpadding="0" cellspacing="0" id="embedded-table">
				<tr>
				<!--#################Changes made at Effective,@ binding attribute##############
				    ################# to fix the bug 0003951  ##################################
				-->
					<td>
					Effective&#160; <rich:calendar binding="#{locationMatrixBean.effectiveDateLocationCalendar}" 
						id="effDate" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{locationMatrixBean.selectedMatrix.effectiveDate}" popup="true"
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction  or locationMatrixBean.addFromTransactionAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox"/>
					</td>
					<td style="width:23px;">&#160;</td>
					<td>
					Expires&#160; <rich:calendar id="expDate" enableManualInput="true"
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{locationMatrixBean.selectedMatrix.expirationDate}" popup="true"
						validator="#{locationMatrixBean.validateExpirationDateForLocation}"
						disabled="#{locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox" /></td>
				</tr>
			</table>
			</td>
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Taxing Jurisdiction:</td>
			<td colspan="5">
				<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
					<ui:param name="handler" value="#{locationMatrixBean.selectedHandler}"/>
					<ui:param name="id" value="jurisInput"/>
					<ui:param name="readonly" value="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction  or locationMatrixBean.backdateAction}"/>
	  				<ui:param name="required" value="false"/> <!-- Will validate in bean to prevent message in queue -->
					<ui:param name="showheaders" value="true"/>
					<ui:param name="popupName" value="searchJurisdiction"/>
					<ui:param name="popupForm" value="searchJurisdictionForm"/>
				</ui:include>
			</td>
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Override Tax Type:</td>
			<td colspan="5">
				<h:selectOneMenu id="defOT" style="width: 125px;"
					required="true" label="Override Tax Type Code"
					disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}"
					value="#{locationMatrixBean.selectedMatrix.overrideTaxtypeCode}">
			   		<f:selectItem id="it1" itemLabel="Override Tax Type Code" itemValue=""/>
			   		<f:selectItem id="it2" itemLabel="No Override" itemValue="*NO"/>
					<f:selectItem id="it3" itemLabel="Sales" itemValue="S"/>
					<f:selectItem id="it4" itemLabel="Use" itemValue="U"/>
				</h:selectOneMenu>
			</td>
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Calculate Taxes:</td>
			<td colspan="5">
				<h:selectBooleanCheckbox id="chkCountryBooleanFlag" value="#{locationMatrixBean.selectedMatrix.countryBooleanFlag}" 
					disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}" styleClass="check"/>
				<h:outputText style="width:150px;" id="countryLabelId" value="#{!empty locationMatrixBean.selectedHandler.country ? locationMatrixBean.selectedHandler.country : 'Country'}" />
				<rich:spacer width="10"/>
				
				<h:selectBooleanCheckbox id="chkStateBooleanFlag" value="#{locationMatrixBean.selectedMatrix.stateBooleanFlag}" 
					disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}" styleClass="check"/>
				<h:outputText style="width:150px;" id="stateLabelId" value="#{!empty locationMatrixBean.selectedHandler.state ? locationMatrixBean.selectedHandler.state : 'State'}" />
				<rich:spacer width="10"/>

				<h:selectBooleanCheckbox id="chkCountyBooleanFlag" value="#{locationMatrixBean.selectedMatrix.countyBooleanFlag}" 
					disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}" styleClass="check"/>
				<h:outputText style="width:150px;" id="countyLabelId" value="#{!empty locationMatrixBean.selectedHandler.county ? locationMatrixBean.selectedHandler.county : 'County'}" />
				<rich:spacer width="10"/>	
				
				<h:selectBooleanCheckbox id="chkCityBooleanFlag" value="#{locationMatrixBean.selectedMatrix.cityBooleanFlag}" 
					disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}" styleClass="check"/>
				<h:outputText style="width:150px;" id="cityLabelId" value="#{!empty locationMatrixBean.selectedHandler.city ? locationMatrixBean.selectedHandler.city : 'City'}" />
				<rich:spacer width="10"/>				
			</td>
			
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr id="stjNameRow" style="#{(!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj1Name or !empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj2Name or !empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj3Name or !empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj4Name or !empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj5Name) ? 'display:table-row;':'display:none;'}">
			<td class="column-spacer">&#160;</td>
			<td class="column-label">&#160;</td>

			<td colspan="5">
				<h:panelGroup id="stj1NamePanel" style="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj1Name ? 'visibility:visible;':'visibility:hidden;'}" >
					<h:selectBooleanCheckbox id="chkStj1BooleanFlag" value="#{locationMatrixBean.selectedMatrix.stj1BooleanFlag}" 
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj1Name}" styleClass="check"/>
					<h:outputText style="width:150px;" id="stj1LabelId" value="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj1Name ? locationMatrixBean.selectedHandler.selectedJurisdiction.stj1Name : 'STJ1'}" />
					<rich:spacer width="10"/>
				</h:panelGroup>
				
				<h:panelGroup id="stj2NamePanel" style="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj2Name ? 'visibility:visible;':'visibility:hidden;'}" >
					<h:selectBooleanCheckbox id="chkStj2BooleanFlag" value="#{locationMatrixBean.selectedMatrix.stj2BooleanFlag}" 
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj2Name}" styleClass="check"/>
					<h:outputText style="width:150px;" id="stj2LabelId" value="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj2Name ? locationMatrixBean.selectedHandler.selectedJurisdiction.stj2Name : 'STJ2'}" />
					<rich:spacer width="10"/>
				</h:panelGroup>
				
				<h:panelGroup id="stj3NamePanel" style="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj3Name ? 'visibility:visible;':'visibility:hidden;'}" >
					<h:selectBooleanCheckbox id="chkStj3BooleanFlag" value="#{locationMatrixBean.selectedMatrix.stj3BooleanFlag}" 
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj3Name}" styleClass="check"/>
					<h:outputText style="width:150px;" id="stj3LabelId" value="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj3Name ? locationMatrixBean.selectedHandler.selectedJurisdiction.stj3Name : 'STJ3'}" />
					<rich:spacer width="10"/>
				</h:panelGroup>
				
				<h:panelGroup id="stj4NamePanel" style="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj4Name ? 'visibility:visible;':'visibility:hidden;'}" >
					<h:selectBooleanCheckbox id="chkStj4BooleanFlag" value="#{locationMatrixBean.selectedMatrix.stj4BooleanFlag}" 
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj4Name}" styleClass="check"/>
					<h:outputText style="width:150px;" id="stj4LabelId" value="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj4Name ? locationMatrixBean.selectedHandler.selectedJurisdiction.stj4Name : 'STJ4'}" />
					<rich:spacer width="10"/>
				</h:panelGroup>
				
				<h:panelGroup id="stj5NamePanel" style="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj5Name ? 'visibility:visible;':'visibility:hidden;'}" >
					<h:selectBooleanCheckbox id="chkStj5BooleanFlag" value="#{locationMatrixBean.selectedMatrix.stj5BooleanFlag}" 
						disabled="#{locationMatrixBean.updateAction or locationMatrixBean.readOnlyAction or empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj5Name}" styleClass="check"/>
					<h:outputText style="width:150px;" id="stj5LabelId" value="#{!empty locationMatrixBean.selectedHandler.selectedJurisdiction.stj5Name ? locationMatrixBean.selectedHandler.selectedJurisdiction.stj5Name : 'STJ5'}" />
					<rich:spacer width="10"/>
				</h:panelGroup>
		
			</td>
			
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Active?:</td>
			<td colspan="5">
				<h:selectBooleanCheckbox id="chkActiveFlag" value="#{locationMatrixBean.selectedMatrix.activeBooleanFlag}" 
					disabled="#{locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}" styleClass="check"/>
			</td>
			<td class="column-spacer">&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-label">Comments:</td>
			<td colspan="5">
				<h:inputText id="commentAdd" style="width:780px;"
					disabled="#{locationMatrixBean.readOnlyAction or locationMatrixBean.backdateAction}"
					value="#{locationMatrixBean.selectedMatrix.comments}"
					maxlength="255" />
			</td>
			<td class="column-spacer">&#160;</td>
		</tr>
		</tbody>
	</table>
	
	</div> <!-- table-one-content -->
	<div id="table-four-bottom">
	<ul class="right">
		<li class="copy-update">
			<a4j:commandLink id="btnCopyUpdate" rendered="#{locationMatrixBean.updateAction}" immediate="true" 
				actionListener="#{locationMatrixBean.copyUpdateListener}" reRender="matrixDetailForm"/>
		</li>
		<li class="view115">
      <h:commandLink id="viewTrans"
                     immediate="true" rendered="#{locationMatrixBean.addFromTransactionAction}"
                     action="#{locationMatrixBean.viewSourceTransactionAction}"/>
    </li>
		<li class="viewWhatIf">
      <h:commandLink id="viewWhatIfTrans" 
                     disabled="#{!locationMatrixBean.validTransactionSelection}" 
                     action="#{locationMatrixBean.viewTransactionAction}"/>
    </li>
		<li class="whatif"><a4j:commandLink id="btnWhatIf" rendered="#{!locationMatrixBean.readOnlyAction and !locationMatrixBean.backdateAction}" actionListener="#{locationMatrixBean.displayTransactionsAction}" reRender="msg,aMDetailList,trScroll,pageInfo" oncomplete="initScrollingTables();" /></li>
		<li class="ok"><h:commandLink id="okBtn" rendered="#{!locationMatrixBean.readOnlyAction and !locationMatrixBean.backdateAction and !locationMatrixBean.entityCodeError}" action="#{locationMatrixBean.saveAction}"
						onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
		<li class="ok"><h:commandLink id="okBtndel" rendered="#{locationMatrixBean.readOnlyAction and locationMatrixBean.deleteAction and !locationMatrixBean.backdateAction}" action="#{locationMatrixBean.deleteAction}"/></li>
		<li class="ok"><h:commandLink id="okBackdate" rendered="#{locationMatrixBean.backdateAction}" action="#{locationMatrixBean.backdateAction}"/></li>
		<li class="ok"><h:commandLink id="okBtnview" rendered="#{locationMatrixBean.readOnlyAction and locationMatrixBean.viewAction and !locationMatrixBean.backdateAction}" immediate="true" action="#{locationMatrixBean.cancelAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!locationMatrixBean.viewAction}" immediate="true" action="#{locationMatrixBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
	</div>
	<h:panelGroup rendered="#{!locationMatrixBean.readOnlyAction}">
	<div id="bottom">
	<div id="table-four">
<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
	<div id="table-four-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="2">Visual Verification (note no transactions are processed):</th>
		</tr>
		<tr>
			<th align="left" width="350">
				<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText="#{locationMatrixBean.transactionDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			</th>
			<th align="right">
				<!-- <h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{locationMatrixBean.displayAllFields}" styleClass="check">
					<a4j:support event="onclick" reRender="aMDetailList,trScroll,pageInfo" immediate="true"
						actionListener="#{locationMatrixBean.displayChange}"
						oncomplete="initScrollingTables();"/>
				</h:selectBooleanCheckbox>  
				<h:outputText value="Display All Fields"/>  -->
				<h:outputText  value="View:&#160;" />
				<h:outputText  value="#{locationMatrixBean.selectedUserNameLocal}&#47;" />
				<h:outputText  value="#{locationMatrixBean.searchColumnNameLocal}&#160;&#160;" />
			</th>	
		</tr>
		</tbody>
	</table>
	<ui:include src="/WEB-INF/view/components/transactiondetail_table.xhtml">
		<ui:param name="formName" value="matrixDetailForm"/>
		<ui:param name="bean" value="#{locationMatrixBean}"/>
		<ui:param name="dataModel" value="#{locationMatrixBean.transactionDataModel}"/>
		<ui:param name="singleSelect" value="true"/>
		<ui:param name="multiSelect" value="false"/>
		<ui:param name="doubleClick" value="false"/>
		<ui:param name="selectReRender" value="viewWhatIfTrans"/>
		<ui:param name="scrollReRender" value="pageInfo"/>
	</ui:include>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td colspan="8">
			Processed:&#160;<h:selectBooleanCheckbox id="chkProcessedTransactions" value="#{locationMatrixBean.processedTransactions}" styleClass="check"/>
			Suspended:&#160;<h:selectBooleanCheckbox id="chkSuspendedTransactions" value="#{locationMatrixBean.suspendedTransactions}" styleClass="check"/>
			</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
</h:panelGroup>
<h:inputHidden id="validateDriversId" rendered="#{!locationMatrixBean.readOnlyAction}" value=" " validator="#{locationMatrixBean.validateDrivers}"/>

<rich:modalPanel id="loader" zindex="2000" autosized="true">
       <h:outputText value="Processing Search ..."/>
</rich:modalPanel>

<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />

</h:form>

<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{locationMatrixBean.selectedHandler}"/>
	<ui:param name="showTaxRates" value="true"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="matrixDetailForm:jurisInput,matrixDetailForm:countryLabelId,matrixDetailForm:stateLabelId,matrixDetailForm:countyLabelId,matrixDetailForm:cityLabelId,matrixDetailForm:chkStj1BooleanFlag,matrixDetailForm:stj1LabelId,matrixDetailForm:chkStj2BooleanFlag,matrixDetailForm:stj2LabelId,matrixDetailForm:chkStj3BooleanFlag,matrixDetailForm:stj3LabelId,matrixDetailForm:chkStj4BooleanFlag,matrixDetailForm:stj4LabelId,matrixDetailForm:chkStj5BooleanFlag,matrixDetailForm:stj5LabelId,matrixDetailForm:stj1NamePanel,matrixDetailForm:stj2NamePanel,matrixDetailForm:stj3NamePanel,matrixDetailForm:stj4NamePanel,matrixDetailForm:stj5NamePanel, matrixDetailForm:stjNameRow"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{locationMatrixBean}"/>
	<ui:param name="id" value="warning"/>
	<ui:param name="warning" value="Warning - not all driver values were found.  Save anyway?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBtn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{locationMatrixBean}"/>
	<ui:param name="id" value="backdatewarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than today. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{locationMatrixBean}"/>
	<ui:param name="id" value="backdateexistwarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than an existing Rule. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

</ui:define>

</ui:composition>

</html>
