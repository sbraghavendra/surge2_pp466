<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
    <head>
		<ui:include src="/WEB-INF/view/components/header.xhtml" />
        <link href="../NCSTS.css" type="text/css" rel="stylesheet" />    
        <script type='text/javascript' src='../scripts/NCSTSActionButton.js'> </script>  
    </head>
    <body >
    <f:view>
	<h:form  id="jurisdictionViewForm">
        <div class="NCSTSContainerPage">
            <div class="NCSTSContainerMargin">
                <!-- This is the header -->
                <table>
                    <tr>
                        <!-- Branding area -->
                        <td class="NCSTSContainerBranding">&#160;</td>
                        <!-- Info area -->
                        <td class="NCSTSContainerInfoBar">&#160;</td> 
                    </tr>
                    
                    <tr></tr>
                </table>
                
            <!-- MENU STARTS HERE -->
            <%@ include file="../menu.jsp" %>        
            <!-- MENU ENDS HERE -->
                              
                <!--This is the main panel object -->
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                       <td class="NCSTSContainerWorkAreaMargin">&#160;</td>
                       <td class="NCSTSContainerWorkAreaLeftEdge">&#160;</td>                    
                       
                       <td class="NCSTSContainerWorkAreaMiddle"> 
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <!-- Action containers go here -->
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <!-- Info action container -->
                                        <tr><td class="NCSTSContainerWorkAreaHeader">TAX RATE MATRIX</td></tr>
                                        
                                         <!-- Add matrix Line action container -->
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0" >
                                                    <tr>
                                                        <td class="NCSTSContainerActionUpperLeft">&#160;</td>
                                                        <td class="NCSTSContainerActionUpperMiddle" > 
                                                             
                                                             <!-- Action container head has a name -->
                                                             <table border="0" cellpadding="0" cellspacing="0" width="915px">
                                                                <tr ><td class="NCSTSContainerActionName" >View A Jurisdiction &amp; Rate</td></tr>
                                                             </table>
                                                        </td>
                                                        <td class="NCSTSContainerActionUpperRight">&#160;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NCSTSContainerActionMiddleLeft" style="height: 443px;">&#160;</td>
                                                        <td class="NCSTSContainerActionMiddleMiddle" style="width: 915px;">
                                                            <!-- Bounding table for all add items -->
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <!-- Jurisdiction Header -->
                                                                <tr><td class="NCSTSActionSectionHeader">Jurisdiction</td></tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="NCSTSDivScrollable" style="height: 61px;">
                                                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px;">
                                                                                <tr class="NCSTSGridRowAlternate">
                                                                                    <td style="width: 10px;">&#160;</td>
                                                                                    <td class="NCSTSText">
                                                                                        Jurisdiction ID<span style="width:12px;"> </span>
                                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdiction.jurisdictionId}"></h:outputText>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="NCSTSGridRow">
                                                                                    <td style="width: 10px;">&#160;</td>
                                                                                    <td class="NCSTSText">
                                                                                        GeoCode:<span style="width:35px;"> </span>
                                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdiction.geocode}"></h:outputText>
                                                                                        <span style="width:10px;"> </span>
                                                                                        Client GeoCode<span style="width:10px;"> </span>
                                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdiction.clientGeocode}"></h:outputText>
                                                                                        <span style="width:10px;"> </span>
                                                                                         Comp. GeoCode<span style="width:10px;"> </span>
                                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdiction.compGeocode}"></h:outputText>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="NCSTSGridRowAlternate">
                                                                                    <td style="width: 10px;">&#160;</td>
                                                                                    <td class="NCSTSText">
                                                                                        City:<span style="width:62px;"> </span>
                                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdiction.city}"></h:outputText>
                                                                                        <span style="width:114px;"> </span>
                                                                                        State:<span style="width:62px;"> </span>
                                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdiction.state}"></h:outputText>
                                                                                         <span style="width:102px;"> </span> 
                                                                                         ZIP Code<span style="width:58px;"> </span>
                                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdiction.zip}"></h:outputText>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="NCSTSGridRow">
                                                                                    <td style="width: 10px;">&#160;</td>
                                                                                    <td class="NCSTSText">
                                                                                        County:<span style="width:44px;"> </span>
                                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdiction.county}"></h:outputText>
                                                                                        <span style="width:114px;"> </span>
                                                                                        In/Out:<span style="width:44px;"> </span>
                                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdiction.inOut}"></h:outputText>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <!-- Tax Rates Header -->
                                                                <tr><td class="NCSTSActionSectionHeader">Tax Rates</td></tr>
                                                                <tr class="NCSTSActionRow">
                                                                    <td class="NCSTSText">
                                                                        <span style="width: 10px;"></span>Tax Rate ID<span style="width: 23px;"></span>
                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.jurisdictionTaxrateId}"></h:outputText>
                                                                        <span style="width: 10px;"> </span>Effective Date<span style="width: 23px;"> </span>
                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.effectiveDate}">
                                                                         </h:outputText>
                                                                    </td>
                                                                </tr>
                                                                <tr class="NCSTSActionRow">
                                                                    <td class="NCSTSText">
                                                                        <span style="width: 103px;"></span>
                                                                        <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.measureTypeCode}">
                                                                         </h:outputText>
                                                                          <span style="width: 10px;"> </span>Expiration Date<span style="width: 14px;"> </span>
                                                                         <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.expirationDate}">
                                                                         </h:outputText>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <!-- Two column tabel with all the tax rates -->
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                   <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <!-- State Tax Rates Header -->
                                                                                        <tr><td class="NCSTSActionSectionHeader">State Tax Rates:</td></tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="NCSTSDivScrollable" style="height: 94px;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 1 Minimum Amount</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                           		 <h:outputText value="">
                                                                        										 </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 1 Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                               <h:outputText value="">
                                                                        										 </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 1 Max Amount</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            	<h:outputText value="">
                                                                        										 </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 1 Max</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText"><input type="checkbox"/></td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 2 Minimum</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.stateTier2MinAmount}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 2 Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.stateUseTier2Rate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 2 Max</td>
                                                                                                             <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.stateTier2MaxAmount}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 2 Max</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText"><input type="checkbox"/></td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 3 Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.stateUseTier3Rate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Tier 3 Max</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                           <td class="NCSTSText">
                                                                                                           <h:outputText value="">
                                                                                                            </h:outputText>
                                                                                                           </td>
                                                                                                           <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table> 
                                                                                </td>
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td>
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <!-- City Tax Rates Header -->
                                                                                        <tr><td class="NCSTSActionSectionHeader">City Tax Rates:</td></tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="NCSTSDivScrollable" style="height: 94px;">
                                                                                                     <table border="0" cellpadding="0" cellspacing="0">
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.cityUseRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Sales Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.citySalesRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Split Amount</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.citySplitAmount}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Split Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.citySplitUseRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Split Sales Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.citySplitSalesRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Single Item</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:selectBooleanCheckbox styleClass="check" >
                                                                                                            </h:selectBooleanCheckbox>
                                                                                                            
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">County Default</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:selectBooleanCheckbox styleClass="check" >
                                                                                                            </h:selectBooleanCheckbox>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td>
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <!-- County Tax Rates Header -->
                                                                                        <tr><td class="NCSTSActionSectionHeader">County Tax Rates:</td></tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="NCSTSDivScrollable" style="height: 94px;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Use Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.countyUseRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Sales Rate</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.countySalesRate}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>                                                                         
                                                         
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Split Amount</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.countySplitAmount}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Max Tax Amount</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">
                                                                                                            <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.countyMaxtaxAmount}">
                                                                                                            </h:outputText>
                                                                                                            </td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRowAlternate"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">Single Item</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText"><input type="checkbox"/></td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                         <tr class="NCSTSGridRow"> 
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText">County Default</td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                            <td class="NCSTSText"><input type="checkbox"/></td>
                                                                                                            <td style="width: 10px;">&#160;</td>
                                                                                                         </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- Combined Rates Header -->
                                                                <tr><td class="NCSTSActionSectionHeader">Combined Tax Rates:</td></tr>
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                             <tr class="NCSTSActionRow"> 
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td class="NCSTSText">Use Rate</td>
                                                                                 <td style="width: 23px;">&#160;</td>
                                                                                <td class="NCSTSText">
                                                                                <h:outputText value="">
                                                                                </h:outputText>
                                                                                </td>
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td class="NCSTSText">Sales Rate</td>
                                                                                 <td style="width: 30px;">&#160;</td>
                                                                                <td class="NCSTSText">
                                                                                <h:outputText value="">
                                                                                </h:outputText>
                                                                                </td>
                                                                             </tr>
                                                                             <tr class="NCSTSActionRow"> 
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td class="NCSTSText">Update User</td>
                                                                                <td style="width: 23px;">&#160;</td>
                                                                                <td class="NCSTSText">
                                                                                <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.updateUserId}">
                                                                                </h:outputText>
                                                                                </td>
                                                                                <td style="width: 10px;">&#160;</td>
                                                                                <td class="NCSTSText">Update Time</td>
                                                                                <td style="width: 30px;">&#160;</td>
                                                                                <td class="NCSTSText">
                                                                                <h:outputText value="#{JurisdictionBean.selectedJurisdictionTaxrate.updateTimestamp}">
																					<f:converter converterId="dateTime"/>
                                                                                </h:outputText>
                                                                                </td>
                                                                             </tr>
                                                                        </table>
                                                                    </td>                       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="NCSTSContainerActionMiddleRight">&#160;</td>
                                                    </tr>
                                                    
                                                    <tr >
                                                        <td class="NCSTSContainerActionLowerLeft" >&#160;</td>
                                                        
                                                        <!-- Action container footer has all the action buttons -->
                                                        <td class="NCSTSContainerActionLowerMiddle" style="width: 915px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" >
                                                                <tr>
                                                                    <td style="width: 815px;">&#160;</td>
                                                                    <td >
                                                                    <h:commandButton id="ok" image="../images/actionbuttons/NCSTSOK.jpg" action="OK"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="NCSTSContainerActionLowerRight">&#160;</td>
                                                    </tr>
                                                 </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                         
                        </table>
                       </td>
                       <td class="NCSTSContainerWorkAreaRightEdge">&#160;</td>
                    </tr>
                </table>
            </div>      
        </div> 
        </h:form>
        </f:view>  
    </body>
</html>
