<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
    <head>
		<ui:include src="/WEB-INF/view/components/header.xhtml" />
        <link href="../NCSTS.css" type="text/css" rel="stylesheet" />    
        <script type='text/javascript' src='../scripts/NCSTSActionButton.js'> </script>  
    </head>
    <body >
<f:view>
<h:form  id="jurisdictionform">
<div class="NCSTSContainerPage">
            <div class="NCSTSContainerMargin">
                <!-- This is the header -->
                <table>
                    <tr>
                        <!-- Branding area -->
                        <td class="NCSTSContainerBranding">&#160;</td>
                        <!-- Info area -->
                        <td class="NCSTSContainerInfoBar">&#160;</td> 
                    </tr>
                    
                    <tr></tr>
                </table>
            <!-- MENU STARTS HERE -->
            <%@ include file="../menu.jsp" %>        
            <!-- MENU ENDS HERE -->
                              
                <!--This is the main panel object -->
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                       <td class="NCSTSContainerWorkAreaMargin">&#160;</td>
                       <td class="NCSTSContainerWorkAreaLeftEdge">&#160;</td>                    
                       
                       <td class="NCSTSContainerWorkAreaMiddle"> 
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <!-- Action containers go here -->
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <!-- Info action container -->
                                        <tr><td class="NCSTSContainerWorkAreaHeader">  TAX RATE MATRIX</td></tr>
                                        
                                         <!-- Selection action container -->
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="NCSTSContainerActionUpperLeft">&#160;</td>
                                                        <td class="NCSTSContainerActionUpperMiddle" > 
                                                        
                                                        <!-- Action container head has a name, menu, info button, and expand/contract -->
                                                             <table border="0" cellpadding="0" cellspacing="0" width="915px">
                                                                <tr >
                                                                    <td class="NCSTSContainerActionName" >Selection Filter</td>
                                                                    <td class="TMPMaintenanceTaxabilityMatrixSelectionFilter" >&#160;</td>
                                                                </tr>
                                                             </table>
                                                        </td>
                                                        <td class="NCSTSContainerActionUpperRight">&#160;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NCSTSContainerActionMiddleLeft" style="height: 98px;">&#160;</td>
                                                        <td class="NCSTSContainerActionMiddleMiddle" style="width: 915px;">
                                                            
                                                            <!-- Selection filter action object -->
                                                            <div class="NCSTSDivScrollable" style="height: 98px;" >
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tr class="NCSTSGridRow" style="width: 700px;">
                                                                        <td style="width: 10px;">&#160;</td>
                                                                        <td>&#160;</td>
                                                                        <td style="width: 100px;" class="NCSTSText"> GeoCode: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="geoCode" value="#{JurisdictionBean.geoCode}"/>
                                                                        </td>
                                                                        <td style="width: 100px;" class="NCSTSText"> JurisdictionID: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="jurisdictionId" value="#{JurisdictionBean.jurisdictionId}"/>
                                                                        </td>
                                                                        
                                                                        <td style="width: 100px;" class="NCSTSText"> Effective Date: </td>
                                                                        <td style="width: 193px;" class="NCSTSText">
                                                                        	<rich:calendar id="effectiveDate" value="#{JurisdictionBean.effectiveDate}" 
									                        								popup="true"
									                        								converter="date"
									                        								datePattern="dd/M/yy"
									                        								showApplyButton="false" inputClass="NCSTSText"/>
                                                   	
                                                   									 
                                                                        </td>
                                                                        <td style="width: 10px;">&#160;</td>
                                                                    </tr>
                                                                    
                                                                    <tr class="NCSTSGridRow" style="width: 700px;">
                                                                        <td style="width: 10px;">&#160;</td>
                                                                        <td>&#160;</td>
                                                                        <td style="width: 100px;" class="NCSTSText"> City: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="city" value="#{JurisdictionBean.city}"/>
                                                                        </td>
                                                                        <td style="width: 100px;" class="NCSTSText"> Jur.Tax Rate ID: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="jurisdictionTaxRateId" value="#{JurisdictionBean.jurisdictionTaxRateId}"/>
                                                                        </td>
                                                                        
                                                                        <td style="width: 100px;" class="NCSTSText" > Expiration Date: </td>
                                                                        <td style="width: 193px;" class="NCSTSText">
                                                                        	<rich:calendar id="expireDate" value="#{JurisdictionBean.expireDate}" 
									                        								popup="true"
									                        								converter="date"
									                        								datePattern="dd/M/yy"
									                        								showApplyButton="false" inputClass="NCSTSText"/>
                                                   	
                                                   									 
                                                                        </td>
                                                                        <td style="width: 10px;">&#160;</td>
                                                                    </tr>
                                                                    
                                                                    
                                                                    <tr class="NCSTSGridRow" style="width: 700px;">
                                                                        <td style="width: 10px;">&#160;</td>
                                                                        <td>&#160;</td>
                                                                        <td style="width: 100px;" class="NCSTSText"> County: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="country" value="#{JurisdictionBean.county}"/>
                                                                        </td>
                                                                        <td colspan="5">&#160;</td>
                                                                        
                                                                    </tr>
                                                                    
                                                                    <tr class="NCSTSGridRow" style="width: 700px;">
                                                                        <td style="width: 10px;">&#160;</td>
                                                                        <td>&#160;</td>
                                                                        <td style="width: 100px;" class="NCSTSText"> State: </td>
                                                                        <td style="width: 193px;">
                                                                        	 <h:selectOneMenu styleClass="NCSTSSelector" 
                                                                           					style="width: 150px;" 
                                                                           					id="state" 
                                                                           					binding="#{JurisdictionBean.stateMenu}" >
																		     	<f:selectItem itemValue="" itemLabel="Select State"/>
																		   </h:selectOneMenu>
                                                                        </td>
                                                                        <td style="width: 100px;" class="NCSTSText"> Client GeoCode: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="clientGeoCode" value="#{JurisdictionBean.clientGeoCode}"/>
                                                                        </td>
                                                                        
                                                                        <td style="width: 100px;" class="NCSTSText"> Custom? </td>
                                                                        <td style="width: 193px;">
                                                                        	 <h:selectBooleanCheckbox styleClass="check" id="custom"  value="#{JurisdictionBean.custom}" > 
                                                                             </h:selectBooleanCheckbox>
                                                                        </td>
                                                                        <td style="width: 10px;">&#160;</td>
                                                                    </tr>
                                                                    
                                                                    <tr class="NCSTSGridRow" style="width: 700px;">
                                                                        <td style="width: 10px;">&#160;</td>
                                                                        <td>&#160;</td>
                                                                        <td style="width: 100px;" class="NCSTSText"> ZIP Code: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="zipCode" value="#{JurisdictionBean.zipCode}"/> 
                                                                        </td>
                                                                        <td style="width: 100px;" class="NCSTSText"> Comp GeoCode: </td>
                                                                        <td style="width: 193px;">
                                                                        	<h:inputText styleClass="NCSTSText" id="compGeoCode" value="#{JurisdictionBean.compGeoCode}"/>
                                                                        </td>
                                                                        
                                                                        <td style="width: 100px;" class="NCSTSText"> Modified? </td>
                                                                        <td style="width: 193px;">
                                                                        	 <h:selectBooleanCheckbox id="modified"  value="#{JurisdictionBean.modified}" > 
                                                                             </h:selectBooleanCheckbox>
                                                                        </td>
                                                                        <td style="width: 10px;">&#160;</td>
                                                                    </tr>
                                                                  </table>
                                                            </div>
                                                        </td>
                                                        <td class="NCSTSContainerActionMiddleRight">&#160;</td>
                                                    </tr>
                                                    <tr >
                                                        <td class="NCSTSContainerActionLowerLeft" >&#160;</td>
                                                        
                                                        <!-- Action container footer has all the action buttons -->
                                                        <td class="NCSTSContainerActionLowerMiddle" style="width: 915px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" >
                                                                <tr>
                                                                    <td style="width: 815px;">&#160;</td>
                                                                    <td >
                                                                    <h:commandButton image="../images/actionbuttons/NCSTSReset.jpg"
                                                                                 action="">
                                                                       </h:commandButton>
                                                                    
													                </td>
                                                                    <td>
                                                                         <h:commandButton image="../images/actionbuttons/NCSTSSearch.jpg"
                                                                                 action="#{JurisdictionBean.fetchJurisdictionListList}">
                                                                         </h:commandButton>                                                                      
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="NCSTSContainerActionLowerRight">&#160;</td>
                                                    </tr>
                                                 </table>
                                            </td>
                                        </tr>
                                        <!-- Spacer -->
                                       <tr><td style="height: 5px;">&#160;</td></tr>
                                       
                                       <!-- Location Matrix Lines and selected line details -->
                                       <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="NCSTSContainerActionUpperLeft">&#160;</td>
                                                                    <td class="NCSTSContainerActionUpperMiddle" > 
                                                                        <!-- Action container head has a name, menu, info button, and expand/contract -->
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="600px">
                                                                            <tr>
                                                                                <td class="NCSTSContainerActionName" style="width: 255px;">Tax Rate Matrix Lines</td>
                                                                                <td class="TMPMaintenanceTaxabilityMatrixViewLines" >&#160;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="NCSTSContainerActionUpperRight">&#160;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NCSTSContainerActionMiddleLeft" style="height: 265px;">&#160;</td>
                                                                    
                                                                    <!-- Transactions action object 
                                                                    
                                                                        NOTE: I have hard coded the alternating rows fro now. I am assuming there is some type of server side object we can use
                                                                        to do this. If not I can write a javascript routine to do alternating row color at the onload event -->
                                                                    <td class="NCSTSContainerActionMiddleMiddle" style="width: 640px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr><td style="height: 2px;">&#160;</td></tr>
                                                                            <tr><td class="NCSTSTextLight">Displaying <h:outputText value="#{JurisdictionBean.rowCount}"></h:outputText> matching records. 0 Selected. Columns can be sorted, rearranged, and resized</td></tr>
                                                                            <tr><td style="height: 2px;">&#160;</td></tr>
                                                                         
                                                                            <tr>
                                                                                <td>
                                                                                    <!-- div class="NCSTSDivScrollable" style="height: 225px;" -->
                <rich:scrollableDataTable rowClasses="odd-row,even-row" 
                styleClass="GridContent"
                height="240px"
                width="650px"
                frozenColCount="0" 
                id="jMList" rows="40"                
                columnClasses="one, two, three, four, five, six, one"
                value="#{JurisdictionBean.jurisdictionList}"
                var="jurisdictionDTO"
                selection="#{JurisdictionBean.selection}" 
                sortMode="multi">
                <a4j:support event="onselectionchange" 
                             actionListener="#{JurisdictionBean.selectedRowChanged}" 
                             reRender="jMDetail"/>

                <rich:column id="jurisdictionId">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Jurisdiction ID" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.jurisdictionId}"/>
                </rich:column>
                
                <rich:column id="geocode">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="GeoCode" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.geocode}"/>
                </rich:column>
                
                
                
                 <rich:column id="state">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.state}"/>
                </rich:column>
                
                
                 <rich:column id="county">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Country" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.county}"/>
                </rich:column>
                
                 <rich:column id="city">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.city}"/>
                </rich:column>
                
                 <rich:column id="zip">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Zip" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.zip}"/>
                </rich:column>
                
                 <rich:column id="inOut">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="In Out" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.inOut}"/>
                </rich:column>
                
                 <rich:column id="clientGeocode">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Client Code" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.clientGeocode}"/>
                </rich:column>
                
                 <rich:column id="compGeocode">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Compliance Code" /></f:facet>
                    <h:outputText value="#{jurisdictionDTO.compGeocode}"/>
                </rich:column>
                
                 <rich:column id="C">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Custom?" /></f:facet>
                    <h:selectBooleanCheckbox styleClass="check" ></h:selectBooleanCheckbox>
                </rich:column>
                
                 </rich:scrollableDataTable>                                                                           
                                                                                 
                                                                                </td>
                                                                            </tr>                                                               
                                                                        </table>
                                                                    </td>
                                                                    
                                                                    <td class="NCSTSContainerActionMiddleRight">&#160;</td>
                                                                </tr><tr>
                                                                    <td class="NCSTSContainerActionLowerLeft">&#160;</td>
                                                                    
                                                                    <!-- Action container footer has all the action buttons -->
                                                                    <td class="NCSTSContainerActionLowerMiddle">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 300px;">
                                                                            <tr  align="right">
                                                                                <td valign="middle">
                                                                                <table border="0" cellpadding="0" cellspacing="0" >
                                                                                        <tr>
                                                                                            <td>
					                                                                                <h:selectOneMenu styleClass="NCSTSSelector" 
					                                                                           					style="width: 150px;" 
					                                                                           					id="taxRateTypeMenu" 
					                                                                           					binding="#{JurisdictionBean.taxRateTypeMenu}" >
																							     	<f:selectItem itemValue="" itemLabel="Select Tax Rate Type"/>
																							   		</h:selectOneMenu>
                                                                                    		</td>
                                                                                        </tr>
                                                                                        <tr><td style="height: 10px;">&#160;</td></tr>
                                                                                    </table>
                                                                                
                                                                                </td>
                                                                                <td> &#160;</td>
                                                                                <td >
                                                                                   <a4j:commandLink action="Add"  
                                                                                                        			 onmouseover="procActionButton('over','Add','../images/actionbuttons/NCSTSAdd-Over.jpg','../images/actionbuttons/NCSTSAdd.jpg',1);"
                                                                                                        			onmouseout="procActionButton('out','Add','../images/actionbuttons/NCSTSAdd-Over.jpg','../images/actionbuttons/NCSTSAdd.jpg',1);"
                                                                                                        			 >
															        											<h:graphicImage id="imgAddTaxRate" url="../images/actionbuttons/NCSTSAdd.jpg" alt="Add">
															        											</h:graphicImage>
															      					</a4j:commandLink>
                                                                                
                                                                                
                                                                                </td>
                                                                                <td >
                                                                                	<a4j:commandLink action="CopyAdd"  
                                                                                                   	 onmouseover="procActionButton('over','Copy/Add','../images/actionbuttons/NCSTSCopyAdd-Over.jpg','../images/actionbuttons/NCSTSCopyAdd.jpg',1);"
                                                                                                   	 onmouseout="procActionButton('out','Copy/Add','../images/actionbuttons/NCSTSCopyAdd-Over.jpg','../images/actionbuttons/NCSTSCopyAdd.jpg',1);"
                                                                                                        			 >
															        											<h:graphicImage id="imgCopyAddTaxRate" url="../images/actionbuttons/NCSTSCopyAdd.jpg" alt="Copy/Add">
															        											</h:graphicImage>
															      					</a4j:commandLink>
                                                                                
                                                                                </td>
                                                                                <td >
                                                                                   <a4j:commandLink action="Delete"  
                                                                                                   	 onmouseover="procActionButton('over','Delete','../images/actionbuttons/NCSTSDelete-Over.jpg','../images/actionbuttons/NCSTSDelete.jpg',1);"
                                                                                                   	 onmouseout="procActionButton('out','Delete','../images/actionbuttons/NCSTSDelete-Over.jpg','../images/actionbuttons/NCSTSDelete.jpg',1);"
                                                                                                        			 >
															        											<h:graphicImage id="imgDeleteTaxRate" url="../images/actionbuttons/NCSTSDelete.jpg" alt="Delete">
															        											</h:graphicImage>
															      				   </a4j:commandLink>
                                                                                </td >
                                                                                <td >
                                                                                   <a4j:commandLink action="View"  
                                                                                                   	 onmouseover="procActionButton('over','View','../images/actionbuttons/NCSTSView-Over.jpg','../images/actionbuttons/NCSTSView.jpg',1);"
                                                                                                   	 onmouseout="procActionButton('out','View','../images/actionbuttons/NCSTSView-Over.jpg','../images/actionbuttons/NCSTSView.jpg',1);"
                                                                                                        			 >
															        											<h:graphicImage id="imgViewTaxRate" url="../images/actionbuttons/NCSTSView.jpg" alt="View">
															        											</h:graphicImage>
															      				   </a4j:commandLink>
                                                                                </td >
                                                                         
                                                                                                                                                                  
                                                                                
                                                                                
                                                                                <td >
                                                                                   <a4j:commandLink action="Update"  
                                                                                                   	 onmouseover="procActionButton('over','Update','../images/actionbuttons/NCSTSUpdate-Over.jpg','../images/actionbuttons/NCSTSUpdate.jpg',1);"
                                                                                                   	 onmouseout="procActionButton('out','Update','../images/actionbuttons/NCSTSUpdate-Over.jpg','../images/actionbuttons/NCSTSUpdate.jpg',1);"
                                                                                                        			 >
															        											<h:graphicImage id="imgUpdateTaxRate" url="../images/actionbuttons/NCSTSUpdate.jpg" alt="Update">
															        											</h:graphicImage>
															      				   </a4j:commandLink>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="NCSTSContainerActionLowerRight">&#160;</td>
                                                                </tr>
                                                             </table>
                                                            </td>
                                                        <td style="width: 5px;">&#160;</td>
                                                        <td>
                                                             <!-- Tax Matrix details action container -->
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="NCSTSContainerActionUpperLeft">&#160;</td>
                                                                    <td class="NCSTSContainerActionUpperMiddle" > 
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="255px">
                                                                            <tr>
                                                                                <td class="NCSTSContainerActionName" style="width: 175px;">Selected Line Details</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="NCSTSContainerActionUpperRight">&#160;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NCSTSContainerActionMiddleLeft" style="height: 265px;">&#160;</td>
                                                                    
                                                                    <!-- Transactions action object 
                                                                    
                                                                        NOTE: I have hard coded the alternating rows fro now. I am assuming there is some type of server side object we can use
                                                                        to do this. If not I can write a javascript routine to do alternating row color at the onload event -->
                                                                    <td class="NCSTSContainerActionMiddleMiddle" style="width: 255px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr><td style="height: 2px;">&#160;</td></tr>
                                                                            <tr><!-- td class="TMPMaintenanceGridListTitles"  </td>--></tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="NCSTSDivScrollable" style="height: 240px;">
<rich:scrollableDataTable rowClasses="odd-row,even-row" 
                height="240px"
                width="240px"
                frozenColCount="1" 
                id="jMDetail" rows="4"                
                value="#{JurisdictionBean.jurisdictionTaxrateList}"
                var="jurisdictionTaxrateDTO" >

                <rich:column id="jurisdictionTaxrateId">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Jurisdiction Taxrate ID" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.jurisdictionTaxrateId}"/>
                </rich:column>
                
                
                <rich:column id="measureTypeCode">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Measure Type" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.measureTypeCode}"/>
                </rich:column>
                
                <rich:column id="effectiveDate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Effective Date" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.effectiveDate}"/>
                </rich:column>
                
                <rich:column id="expirationDate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Expiration Date" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.expirationDate}"/>
                </rich:column>
                
                <rich:column id="combinedUseRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Combined Use Rate" /></f:facet>
                    
                </rich:column>
                
                <rich:column id="stateUseRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Tier1 Use Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateUseRate}"/>
                </rich:column>
                
                <rich:column id="stateMaxtaxAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Tier1 Max Amount" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateMaxtaxAmount}"/>
                </rich:column>
                
                <rich:column id="stateTier2MinAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Tier2 Min Amount" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateTier2MinAmount}"/>
                </rich:column>
                
                <rich:column id="stateTier2MaxAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Tier2 Max Amount" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateTier2MaxAmount}"/>
                </rich:column>
                
                <rich:column id="stateUseTier3Rate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Tier3 Use Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateUseTier3Rate}"/>
                </rich:column>
                
                <rich:column id="stateMaxtaxAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Max Tax Amt" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateMaxtaxAmount}"/>
                </rich:column>
                
                <rich:column id="countyUseRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Country Use Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.countyUseRate}"/>
                </rich:column>
                
                <rich:column id="countyMaxtaxAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Country Max Tax Amt" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.countyMaxtaxAmount}"/>
                </rich:column>
                
                <rich:column id="cityUseRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City Use rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.cityUseRate}"/>
                </rich:column>        
                
                <rich:column id="combinedSlaesRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="Combined Slaes Rate" /></f:facet>
                    
                </rich:column>
                
                <rich:column id="stateSalesRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="State Sales Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.stateSalesRate}"/>
                </rich:column>
                
                <rich:column id="countySalesRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="County Sales Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.countySalesRate}"/>
                </rich:column>
                
                <rich:column id="citySalesRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City Sales Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.citySalesRate}"/>
                </rich:column>
                
                <rich:column id="countySingleFlag">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="County Single" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.countySingleFlag}"/>
                </rich:column>
                
                <rich:column id="countyDefaultFlag">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="County Default" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.countyDefaultFlag}"/>
                </rich:column>
                
                <rich:column id="citySplitAmount">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City Split Amt" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.citySplitAmount}"/>
                </rich:column>
                
                <rich:column id="citySplitSalesRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City Split Sales Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.citySplitSalesRate}"/>
                </rich:column>
                
                <rich:column id="citySplitUseRate">
                    <f:facet name="header"><h:outputText styleClass="headerText" value="City Split Use Rate" /></f:facet>
                    <h:outputText value="#{jurisdictionTaxrateDTO.citySplitUseRate}"/>
                </rich:column>
                
               </rich:scrollableDataTable>                                                                            
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    
                                                                    <td class="NCSTSContainerActionMiddleRight">&#160;</td>
                                                                </tr>   
                                                                <tr>
                                                                    <td class="NCSTSContainerActionLowerLeft">&#160;</td>
                                                                    
                                                                    <!-- Action container footer has all the action buttons -->
                                                                    <td class="NCSTSContainerActionLowerMiddle" style="width: 200px;">&#160;</td>
                                                                    <td class="NCSTSContainerActionLowerRight">&#160;</td>
                                                                </tr>
                                                                
                                                             </table> 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                       </tr>
                                    </table>
                           </td>
                            </tr>                         
                        </table>
                        </td>
                        </tr>
                        </table>
                        
                     </div></div></h:form></f:view> </body></html>
       