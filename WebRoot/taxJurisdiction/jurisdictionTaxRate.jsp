<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('jurisdictionform:jurisdictionDataTable', 'selectedJurisdictionIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('jurisdictionform:taxrateDataTable', 'selectedTaxRateIndex'); } );

function resetInputs()
{
    document.getElementById("jurisdictionform:searchgeocode").value = "";
    document.getElementById("jurisdictionform:searchjurisdictionId").value = "";
    document.getElementById("jurisdictionform:searcheffectiveDateInputDate").value = "";
    document.getElementById("jurisdictionform:searcheffectiveDate").component.resetSelectedDate(); 
    document.getElementById("jurisdictionform:searchcity").value = "";
    document.getElementById("jurisdictionform:searchexpirationDateInputDate").value = "";
    document.getElementById("jurisdictionform:searchexpirationDate").component.resetSelectedDate();  
    document.getElementById("jurisdictionform:stateMenuMain").value = "";
    document.getElementById("jurisdictionform:countryMenuMain").value = "";
    document.getElementById("jurisdictionform:searchclientGeocode").value = ""; 
    document.getElementById("jurisdictionform:searchcustomFlag").value = "All";
    document.getElementById("jurisdictionform:searchzip").value = ""; 
    document.getElementById("jurisdictionform:searchcompGeocode").value = "";
    document.getElementById("jurisdictionform:searchmodifiedFlag").value = "All"; 
    document.getElementById("jurisdictionform:searchcounty").value = ""; 
    document.getElementById("jurisdictionform:searchjurisdictionTaxrateId").value = "";
     

}

function resetState()
{ 
    document.getElementById("jurisdictionform:stateMenuMain").value = "";
}

//]]>
</script>	
</ui:define>

<ui:define name="body">
<f:view>

<h:inputHidden id="selectedJurisdictionIndex" value="#{TaxJurisdictionBean.selectedJurisdictionIndex}"/>
<h:inputHidden id="selectedTaxRateIndex" value="#{TaxJurisdictionBean.selectedTaxRateIndex}"/>
<h:form id="jurisdictionform">

<h:panelGroup rendered="#{!TaxJurisdictionBean.findMode}">
    <h1><h:graphicImage id="imgTaxRatesMaintenance" alt="Tax Rates Maintenance" url="/images/headers/hdr-tax-rates-maintenance.png" width="250" height="19" ></h:graphicImage></h1>
</h:panelGroup>

<h:panelGroup rendered="#{TaxJurisdictionBean.findMode}">
	<h1><h:graphicImage id="imgTaxRatesMaintenance1" alt="Lookup Jurisdiction id" url="/images/headers/hdr-lookup-jurisdiction-id.png" width="250" height="19" ></h:graphicImage></h1>
</h:panelGroup>

<div class="tab">
	<h:graphicImage id="image2" alt="Selection Filter" url="/images/containers/STSSelection-filter-open.gif"></h:graphicImage>
	&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel"
		style="height:22px;text-decoration: underline;color:#0033FF"
		reRender="showhidefilter,toggleHideSearchPanel"
		actionListener="#{TaxJurisdictionBean.togglePanelController.toggleHideSearchPanel}">
		<h:outputText value="#{(TaxJurisdictionBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
	</a4j:commandLink>
</div>

<a4j:outputPanel id="showhidefilter">
<c:if test="#{!TaxJurisdictionBean.togglePanelController.isHideSearchPanel}">
<div id="top">
	<div id="table-one">
	<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
	<div id="table-one-top" style="padding-left: 10px;">
		<table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
            	<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
          			reRender="showhidefilter" actionListener="#{TaxJurisdictionBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
         		<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
		        	reRender="showhidefilter" actionListener="#{TaxJurisdictionBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
         		<td style="width: 100%"></td>
         		<td style="padding-right: 10px; padding-left: 250px; padding-top:3px; width: 200px"><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>
           	</tr>
         </table>
	</div>
	
	<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'jurisdictionform:Bt2Search')" >
	<div id="embedded-table">
   		<h:panelGrid id="togglePanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
   			<rich:simpleTogglePanel id="TaxRatesPanel" switchType="ajax"
					label="Jurisdiction/Tax Rate Information"
					actionListener="#{TaxJurisdictionBean.togglePanelController.togglePanelChanged}"
					opened="#{TaxJurisdictionBean.togglePanelController.togglePanels['TaxRatesPanel']}"
					bodyClass="togglepanel-body" headerClass="togglepanel-header"
					styleClass="togglepanel" >
   				<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
   				<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
			  	
				<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
					<tr>
						<td>&#160;</td>
						<td class="column-label">Jurisdiction ID:</td>
						<td>
							<h:inputText styleClass="column-input" id="searchjurisdictionId"
								value="#{TaxJurisdictionBean.searchjurisdictionId}" 
							 	onkeypress="return onlyIntegerValue(event);" 
								label="Jurisdiction ID">
									<f:convertNumber integerOnly="true" groupingUsed="false"/>
							</h:inputText>
					 	</td>
						<td>&#160;</td>
						<td class="column-label" >Tax&#160;Rate&#160;ID:</td>
						<td>
							<h:inputText styleClass="column-input" id="searchjurisdictionTaxrateId"
								value="#{TaxJurisdictionBean.searchjurisdictionTaxrateId}"
								maxlength="#{TaxJurisdictionBean.fieldSizeMap['JURISDICTIONTAXRATEID']}" 
								onkeypress="return onlyIntegerValue(event);"
								label="Jur. Tax Rate ID">
									<f:convertNumber integerOnly="true" groupingUsed="false"/>
							</h:inputText>
						</td>
						<td class="column-label" >&#160;</td>
						<td class="column-label" >&#160;</td>			
						<td style="width: 30%;">&#160;</td>
					</tr>
					
					 <tr>
						<td>&#160;</td>
						<td colspan="7">
						<div id="embedded-table">
						<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">	
							<tbody>
								<tr>
									<td style="width: 100px;" class="NCSTSText">GeoCode:</td>
									<td style="width: 100px;" class="NCSTSText">Country:</td>
									<td style="width: 100px;" class="NCSTSText">State:</td>
									<td style="width: 100px;" class="NCSTSText">County:</td>
									<td style="width: 100px;" class="NCSTSText">City:</td>
									<td style="width: 70px;" class="NCSTSText">ZIP Code:</td>
									<td style="width: 10px;" class="NCSTSText"></td>
									<td style="width: 40px;" class="NCSTSText">Plus4:</td>
									<td style="width: 70%;">&#160;</td>						
								</tr>							
								<tr>
									<td style="width: 100px;">
										<h:inputText styleClass="NCSTSText" id="searchgeocode"
											value="#{TaxJurisdictionBean.searchgeocode}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['GEOCODE']}"
											onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
									</td>
									<td style="width: 100px;">
										<h:selectOneMenu styleClass="NCSTSSelector" style="width: 120px;"
											id="countryMenuMain"
											value="#{TaxJurisdictionBean.searchCountry}"
											immediate="true"
											valueChangeListener="#{TaxJurisdictionBean.searchCountryChanged}" 
											onchange="resetState();submit();"
											>
									
											<f:selectItems value="#{TaxJurisdictionBean.countryMenuItems}" />
										</h:selectOneMenu>
									</td>
									<td style="width: 193px;">
										<h:selectOneMenu styleClass="NCSTSSelector" style="width: 150px;"
											id="stateMenuMain"
											value="#{TaxJurisdictionBean.searchstate}">
									
											<f:selectItems value="#{TaxJurisdictionBean.stateMenuItems}" />
										</h:selectOneMenu>
									</td>					
									<td style="width: 193px;">
										<h:inputText styleClass="NCSTSText" id="searchcounty"
											value="#{TaxJurisdictionBean.searchcounty}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTY']}"
											onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
									</td>
									<td style="width: 193px;">
										<h:inputText styleClass="NCSTSText" id="searchcity"
											value="#{TaxJurisdictionBean.searchcity}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITY']}"
											onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
									</td>
									<td style="width: 70px;">
										<h:inputText id="searchzip" style="width: 70px;"
											value="#{TaxJurisdictionBean.searchzip}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['ZIP']}" />
									</td>
									<td>-</td>
									<td style="width: 40px;">
										<h:inputText id="searchzipplus4" style="width: 40px;"
											value="#{TaxJurisdictionBean.searchzipplus4}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['ZIPPLUS4']}" />
									</td>
									
								</tr>
							</tbody>
							</table>
							</div>
						</td>
						<td style="width: 20%;">&#160;</td>
					</tr>

					<tr>
						<td>&#160;</td>
						<td class="column-label">STJ:</td>
						<td colspan="6">
							<h:inputText id="searchstj" style="width: 400px;"
								value="#{TaxJurisdictionBean.searchstj}" 
								label="STJ" onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" >
							</h:inputText>
					 	</td>					
						<td style="width: 20%;">&#160;</td>
					</tr>
			
					<tr>
						<td style="width: 10px;">&#160;</td>
						<td class="column-label">Rate Type:</td>
						<td colspan="6">
							<h:selectOneMenu styleClass="column-input"
								id="searchratetype"
								value="#{TaxJurisdictionBean.searchratetype}" >
									<f:selectItems value="#{TaxJurisdictionBean.rateTypeCodeItems}"/>
							</h:selectOneMenu>
					 	</td>					
						<td style="width: 20%;">&#160;</td>
					</tr>
				</table>
			</rich:simpleTogglePanel>
		
			<rich:simpleTogglePanel id="MiscInformationPanel" switchType="ajax"
					label="Misc. Information"
					actionListener="#{TaxJurisdictionBean.togglePanelController.togglePanelChanged}"
					opened="#{TaxJurisdictionBean.togglePanelController.togglePanels['MiscInformationPanel']}"
					bodyClass="togglepanel-body" headerClass="togglepanel-header"
					styleClass="togglepanel" >
   				<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
   				<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
			  	
				<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
					
					<tr>
						<td>&#160;</td>
						<td class="column-label">Client&#160;GeoCode:</td>
						<td>
							<h:inputText id="searchclientGeocode" styleClass="column-input"
								value="#{TaxJurisdictionBean.searchclientGeocode}"
								maxlength="#{TaxJurisdictionBean.fieldSizeMap['CLIENTGEOCODE']}"
								onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
						</td>
						<td>&#160;</td>
						<td class="column-label">Compliance&#160;GeoCode:</td>
						<td >
							<h:inputText id="searchcompGeocode" styleClass="column-input"
								value="#{TaxJurisdictionBean.searchcompGeocode}"
								maxlength="#{TaxJurisdictionBean.fieldSizeMap['COMPGEOCODE']}"
								onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
						</td>
						<td>&#160;</td>
						<td>&#160;</td>
						<td style="width: 30%;">&#160;</td>
					</tr>  
					
					<tr>
						<td>&#160;</td>
						<td class="column-label">Effective From:</td>
						<td>
							<rich:calendar id="searcheffectiveDate" styleClass="column-input"
								value="#{TaxJurisdictionBean.searcheffectiveDate}"
								popup="true" locale="#{TaxJurisdictionBean.locale}"
								converter="date"
								datePattern="M/d/yyyy" showApplyButton="false" 
								oninputkeypress="return onlyDateValue();" label="Effective Date: "
								inputClass="NCSTSText" enableManualInput="true"
								toolTipMode="single" />
						</td>
						<td>&#160;</td>
						<td class="column-label">Effective Through:</td>
						<td>
							<rich:calendar id="searchexpirationDate" styleClass="column-input"
								value="#{TaxJurisdictionBean.searchexpirationDate}"
								popup="true" locale="#{TaxJurisdictionBean.locale}"
								converter="date"
								datePattern="M/d/yyyy" showApplyButton="false"
								oninputkeypress="return onlyDateValue();" label="Expiration Date: "
								inputClass="NCSTSText" enableManualInput="true"
								toolTipMode="single" />
						</td>		
						<td>&#160;</td>
						<td>&#160;</td>					
						<td style="width: 30px;">&#160;</td>
					</tr>
			
					<tr>
						<td>&#160;</td>
						<td class="column-label">Custom?:</td>
						<td>
							<h:selectOneMenu 
								id="searchcustomFlag"
								value="#{TaxJurisdictionBean.customFlagFilter}">
								<f:selectItems value="#{TaxJurisdictionBean.customFlagMenuItems}" />
							</h:selectOneMenu>
						</td>
						<td>&#160;</td>
						<td class="column-label">Modified?:</td>
						<td>
							<h:selectOneMenu 
								id="searchmodifiedFlag"
								value="#{TaxJurisdictionBean.modifiedFlagFilter}">
								<f:selectItems value="#{TaxJurisdictionBean.modifiedFlagMenuItems}" />
			       			</h:selectOneMenu>
			      		</td>
						<td>&#160;</td>
						<td>&#160;</td>
						<td style="width: 30px;">&#160;</td>
					</tr>
			       
				</table>
			</rich:simpleTogglePanel>
		</h:panelGrid>
	</div> <!-- "embedded-table" -->	
	</div> <!-- table-one-content -->
	
	<div id="table-one-bottom">
		<ul class="right">
			<li class="clear"><a4j:commandLink id="At1Reset" action="#{TaxJurisdictionBean.resetSearch}"/></li>
			<li class="search"><a4j:commandLink id="Bt2Search" action="#{TaxJurisdictionBean.searchJurisdiction}" /></li>	
		</ul>
	</div>
	</a4j:outputPanel>
	</div>	<!-- "table-one" -->
</div> <!-- "top" -->
</c:if>
</a4j:outputPanel> <!-- "showhidefilter" -->

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSView-Jurisdictions.gif" width="192" height="17" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four" >
	<div id="table-four-top" style="padding-left: 15px;">
	
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText="#{TaxJurisdictionBean.jurisdictionDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	</div>

	<div id="table-four-content">

		<div class="scrollContainer">
		<div class="scrollInner"  id="resize" >

		<rich:dataTable rowClasses="odd-row,even-row" 
			id="jurisdictionDataTable" 
			value="#{TaxJurisdictionBean.jurisdictionDataModel}" var="taxJurisdictionDTO"
			rows="#{TaxJurisdictionBean.jurisdictionDataModel.pageSize}">

			<a4j:support event="onRowClick" status="rowstatus"
				onsubmit="selectRow('jurisdictionform:jurisdictionDataTable', this);"
				actionListener="#{TaxJurisdictionBean.jurisdictionRowChanged}"
				reRender="taxrateDataTable,copyAddJurisdiction,updateJurisdiction,deleteJurisdiction,viewJurisdiction,okBtn,custLocnDataTest,
				addJurisdictionTaxrate,copyAddJurisdictionTaxrate,updateJurisdictionTaxrate,viewJurisdictionTaxrate,deleteJurisdictionTaxrate,pageBottomInfo,taxrateDataTable"
				oncomplete="initScrollingTables();" />

			<rich:column id="jurisdictionId">
				<f:facet name="header">
				<a4j:commandLink id="jurisdictionId-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['jurisdictionId']}"
							value="Jurisdiction ID"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.jurisdictionId}" />
			</rich:column>
			
			<rich:column id="geocode" >
			<f:facet name="header">
						<a4j:commandLink id="geocode-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['geocode']}"
							value="GeoCode"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.geocode}" />
			</rich:column>
			
			<rich:column id="country">
			
				<f:facet name="header">
								<a4j:commandLink id="country-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['country']}"
							value="Country"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{cacheManager.countryMap[taxJurisdictionDTO.country]} (#{taxJurisdictionDTO.country})"  />
			</rich:column>		
			
			<rich:column id="state">
			
				<f:facet name="header">
								<a4j:commandLink id="state-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['state']}"
							value="State"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{cacheManager.taxCodeStateMap[taxJurisdictionDTO.state].name} #{(not empty cacheManager.taxCodeStateMap[taxJurisdictionDTO.state].name) ? 
						'(' : ''}#{taxJurisdictionDTO.state}#{(not empty cacheManager.taxCodeStateMap[taxJurisdictionDTO.state].name) ? ')' : ''}" />
	
			</rich:column>

			<rich:column id="county">
				<f:facet name="header">
								<a4j:commandLink id="county-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['county']}"
							value="County"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.county}" />
			</rich:column>

			<rich:column id="city">
				<f:facet name="header">
								<a4j:commandLink id="city-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['city']}"
							value="City"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.city}" />
			</rich:column>
			
			<rich:column id="zip">
				<f:facet name="header">
								<a4j:commandLink id="zip-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['zip']}"
							value="ZIP Code"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.zip}#{(not empty taxJurisdictionDTO.zipplus4) ? '-' : ''}#{taxJurisdictionDTO.zipplus4}" />
			</rich:column>
			
			<rich:column id="stj1Name">
				<f:facet name="header">
					<a4j:commandLink id="stj1Name-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['stj1Name']}"
							value="STJ1"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.stj1Name}" />
			</rich:column>
			
			<rich:column id="stj2Name">
				<f:facet name="header">
					<a4j:commandLink id="stj2Name-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['stj2Name']}"
							value="STJ2"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.stj2Name}" />
			</rich:column>
			
			<rich:column id="stj3Name">
				<f:facet name="header">
					<a4j:commandLink id="stj3Name-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['stj3Name']}"
							value="STJ3"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.stj3Name}" />
			</rich:column>

			<rich:column id="stj4Name">
				<f:facet name="header">
					<a4j:commandLink id="stj4Name-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['stj4Name']}"
							value="STJ4"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.stj4Name}" />
			</rich:column>
			
			<rich:column id="stj5Name">
				<f:facet name="header">
					<a4j:commandLink id="stj5Name-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['stj5Name']}"
							value="STJ5"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.stj5Name}" />
			</rich:column>
			
			<rich:column id="inOut">
				<f:facet name="header">
								<a4j:commandLink id="inOut-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['inOut']}"
							value="In Out"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.inOut}" />
			</rich:column>

			<rich:column id="description">
				<f:facet name="header">
								<a4j:commandLink id="description-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['description']}"
							value="Description"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.description}" />
			</rich:column>

			<rich:column id="clientGeocode">
				<f:facet name="header">
								<a4j:commandLink id="clientGeocode-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['clientGeocode']}"
							value="Client Code"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.clientGeocode}" />
			</rich:column>

			<rich:column id="compGeocode">
				<f:facet name="header">
								<a4j:commandLink id="compGeocode-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['compGeocode']}"
							value="Compliance Code"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:outputText value="#{taxJurisdictionDTO.compGeocode}" />
			</rich:column>

			<rich:column id="customFlag">
				<f:facet name="header">
								<a4j:commandLink id="customFlagBoolean-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['customFlagBoolean']}"
							value="Custom?"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxJurisdictionDTO.customFlag==1}" />
			</rich:column>
			
			<rich:column id="modifiedFlag">
				<f:facet name="header">
								<a4j:commandLink id="modifiedFlagBoolean-a4j" styleClass="sort-#{TaxJurisdictionBean.jurisdictionDataModel.sortOrder['modifiedFlagBoolean']}"
							value="Modified?"
							actionListener="#{TaxJurisdictionBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="jurisdictionDataTable"/>
				</f:facet>
				<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxJurisdictionDTO.modifiedFlag.toString()=='1'}" />
			</rich:column>
			
			
			<!-- 

			 -->
						
		</rich:dataTable>
		
		<a4j:outputPanel id="jurisdictionDataTest">
          	<c:if test="#{TaxJurisdictionBean.jurisdictionDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          	</c:if>
        </a4j:outputPanel>

		</div>
		</div>

		<rich:datascroller id="trScroll" for="jurisdictionDataTable" maxPages="10" oncomplete="initScrollingTables();"
			style="clear:both;" align="center" stepControls="auto" ajaxSingle="false" 
			page="#{TaxJurisdictionBean.jurisdictionDataModel.curPage}" 
			reRender="pageInfo,pageBottomInfo"/>

	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<h:panelGroup rendered="#{!TaxJurisdictionBean.findMode}">
		<li class="add"><h:commandLink id="addJurisdiction"  style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="addJurisdiction" /></h:commandLink></li>
		<li class="copy-add"><h:commandLink id="copyAddJurisdiction" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.validJurisdictionSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="copyAddJurisdiction" /></h:commandLink></li>		
		<li class="update"><h:commandLink id="updateJurisdiction" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.canDeleteJurisdictionSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="updateJurisdiction" /></h:commandLink></li>
		<li class="delete115"><h:commandLink id="deleteJurisdiction" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.canDeleteJurisdictionSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="deleteJurisdiction" /></h:commandLink></li>
		<li class="view"><h:commandLink id="viewJurisdiction" disabled="#{!TaxJurisdictionBean.validJurisdictionSelection}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="viewJurisdiction" /></h:commandLink></li>
		</h:panelGroup>	
		
		<h:panelGroup rendered="#{TaxJurisdictionBean.findMode}">
			<li class="ok"><h:commandLink id="okBtn" disabled="#{!TaxJurisdictionBean.validJurisdictionSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{TaxJurisdictionBean.findAction}"/></li>
			<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{TaxJurisdictionBean.cancelFindAction}" /></li>
		</h:panelGroup>
	</ul>
	</div>	
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSView-Tax Rates.gif" />
	</span>
</div>

<div id="bottom">
	<div id="table-four">	
	<div id="table-four-top" style="padding-left: 15px;">	
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
			<tbody>
			<tr>
				<td align="left ">
					<a4j:status id="pageBottomInfo" forceId="true"
						startText="Request being processed..." 
						stopText="#{TaxJurisdictionBean.pageTaxRateDescription}"/>	
				</td>
				<td align="right" width="300">
						<h:panelGrid columns="3" >
							<h:selectOneRadio style="padding-left: 10px;" layout="lineDirection" id="rateOptionControl" 
								valueChangeListener="#{TaxJurisdictionBean.optionChangeAction}"
								value="#{TaxJurisdictionBean.rateOptionFlag}">
				  				<f:selectItem id="item1" itemLabel="&#160;Sales&#160;&#160;" itemValue="0" />
				  				<f:selectItem id="item2" itemLabel="&#160;Use&#160;&#160;" itemValue="1" />
				  				<f:selectItem id="item3" itemLabel="&#160;Both&#160;&#160;" itemValue="2" />
				  				
								<a4j:support event="onclick" ajaxSingle="true" reRender="taxrateDataTable" oncomplete="initScrollingTables();" />   
				  							  				
							</h:selectOneRadio>	
						</h:panelGrid>


				</td>
				<td style="width: 20px;">&#160;</td>
			</tr>
			</tbody>
		</table>					
	</div>
	


	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
	
	<rich:dataTable rowClasses="odd-row,even-row" 
		id="taxrateDataTable" styleClass="GridContent"
		value="#{TaxJurisdictionBean.taxRateList}" var="taxRateDTO">
			
		<a4j:support event="onRowClick" status="rowstatus"
			onsubmit="selectRow('jurisdictionform:taxrateDataTable', this);"
			actionListener="#{TaxJurisdictionBean.taxrateRowChanged}"
			reRender="copyAddJurisdiction,updateJurisdiction,deleteJurisdiction,viewJurisdiction,viewJurisdictionTaxrate,addJurisdictionTaxrate,copyAddJurisdictionTaxrate,updateJurisdictionTaxrate,deleteJurisdictionTaxrate" />
 			
		<rich:column id="jurisdictionTaxrateId" sortBy="#{taxRateDTO.jurisdictionTaxrateId}">
			<f:facet name="header"><h:outputText value="Jur. Taxrate ID" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.jurisdictionTaxrateId}" />
		</rich:column>

		<rich:column id="effectiveDate" sortBy="#{taxRateDTO.effectiveDate}" sortOrder="DESCENDING" >
			<f:facet name="header"><h:outputText value="Effective Date" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.effectiveDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
			</h:outputText>
		</rich:column>

		<rich:column id="ratetypeCode" sortBy="#{TaxJurisdictionBean.rateTypeCodeToRateTypeMap[taxRateDTO.ratetypeCode]}" sortOrder="ASCENDING" >
			<f:facet name="header"><h:outputText value="Rate Type" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{TaxJurisdictionBean.rateTypeCodeToRateTypeMap[taxRateDTO.ratetypeCode]}" />
		</rich:column>

		<rich:column id="expirationDate" sortBy="#{taxRateDTO.expirationDate}">
			<f:facet name="header"><h:outputText value="Expiration Date" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.expirationDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy" />
			</h:outputText>
		</rich:column>
		
		<!-- 4061 -->
		<rich:column id="combinedSalesRate" sortBy="#{taxRateDTO.combinedSalesRate}">
			<f:facet name="header"><h:outputText value="Combined Sales Rate" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.combinedSalesRate}" >
				<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
			</h:outputText>
		</rich:column>
		
		<rich:column id="combinedUseRate" sortBy="#{taxRateDTO.combinedUseRate}">
			<f:facet name="header"><h:outputText value="Combined Use Rate" styleClass="sort-local" /></f:facet>
			<h:outputText value="#{taxRateDTO.combinedUseRate}"  >
				<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
			</h:outputText>
		</rich:column>
		
		<!-- Display Sales Rate -->		
		<c:if test="#{TaxJurisdictionBean.displaySalesRates}">
		
		<rich:column id="countrySalesTier1Rate" >
			<f:facet name="header"><h:outputText value="Country Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countrySalesTier1Rate}" />
		</rich:column>
	
		<rich:column id="stateSalesTier1Rate">
			<f:facet name="header"><h:outputText value="State Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateSalesTier1Rate}" />
		</rich:column>
		
		<rich:column id="countySalesTier1Rate">
			<f:facet name="header"><h:outputText value="County Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countySalesTier1Rate}" />
		</rich:column>

		<rich:column id="citySalesTier1Rate">
			<f:facet name="header"><h:outputText value="City Sales Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.citySalesTier1Rate}" />
		</rich:column>

		<rich:column id="stj1SalesRate">
			<f:facet name="header"><h:outputText value="STJ1 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj1SalesRate}" />
		</rich:column>
		
		<rich:column id="stj2SalesRate">
			<f:facet name="header"><h:outputText value="STJ2 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj2SalesRate}" />
		</rich:column>
		
		<rich:column id="stj3SalesRate">
			<f:facet name="header"><h:outputText value="STJ3 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj3SalesRate}" />
		</rich:column>
		
		<rich:column id="stj4SalesRate">
			<f:facet name="header"><h:outputText value="STJ4 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj4SalesRate}" />
		</rich:column>
		
		<rich:column id="stj5SalesRate">
			<f:facet name="header"><h:outputText value="STJ5 Sales Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj5SalesRate}" />
		</rich:column>

		</c:if>
		<!-- End of Sales Rate -->

		<!-- Display Use Rate -->
		<c:if test="#{TaxJurisdictionBean.displayUseRates}">
	
		<rich:column id="countryUseTier1Rate">
			<f:facet name="header"><h:outputText value="Country Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countryUseTier1Rate}" />
		</rich:column>
	
		<rich:column id="stateUseTier1Rate">
			<f:facet name="header"><h:outputText value="State Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateUseTier1Rate}" />
		</rich:column>
		
		<rich:column id="countyUseTier1Rate">
			<f:facet name="header"><h:outputText value="County Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.countyUseTier1Rate}" />
		</rich:column>

		<rich:column id="cityUseTier1Rate">
			<f:facet name="header"><h:outputText value="City Use Tier1 Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.cityUseTier1Rate}" />
		</rich:column>

		<rich:column id="stj1UseRate">
			<f:facet name="header"><h:outputText value="STJ1 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj1UseRate}" />
		</rich:column>
		
		<rich:column id="stj2UseRate">
			<f:facet name="header"><h:outputText value="STJ2 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj2UseRate}" />
		</rich:column>
		
		<rich:column id="stj3UseRate">
			<f:facet name="header"><h:outputText value="STJ3 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj3UseRate}" />
		</rich:column>
		
		<rich:column id="stj4UseRate">
			<f:facet name="header"><h:outputText value="STJ4 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj4UseRate}" />
		</rich:column>
		
		<rich:column id="stj5UseRate">
			<f:facet name="header"><h:outputText value="STJ5 Use Rate" /></f:facet>
			<h:outputText value="#{taxRateDTO.stj5UseRate}" />
		</rich:column>
	
		</c:if>
		<!-- End of Use Rate -->
<!-- PP-24			
		<rich:column id="stateSplitAmount">
			<f:facet name="header"><h:outputText value="Tier 1 Max Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateSplitAmount}" />
		</rich:column>

		<rich:column id="stateTier2MinAmount">
			<f:facet name="header"><h:outputText value="State Tier2 Min Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateTier2MinAmount}" />
		</rich:column>

		<rich:column id="stateTier2MaxAmount">
			<f:facet name="header"><h:outputText value="State Tier2 Max Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateTier2MaxAmount}" />
		</rich:column>

		<rich:column id="stateMaxtaxAmount">
			<f:facet name="header"><h:outputText value="State Max Tax Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.stateMaxtaxAmount}" />
		</rich:column>	
		
		<rich:column id="countySplitAmount">
			<f:facet name="header"><h:outputText value="County Split Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.countySplitAmount}" />
		</rich:column>

		<rich:column id="countyMaxtaxAmount">
			<f:facet name="header"><h:outputText value="County Max Tax Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.countyMaxtaxAmount}" />
		</rich:column>
		
		<rich:column id="citySplitAmount">
			<f:facet name="header"><h:outputText value="City Split Amount" /></f:facet>
			<h:outputText value="#{taxRateDTO.citySplitAmount}" />
		</rich:column>
-->
		<rich:column id="customFlagBoolean">
			<f:facet name="header"><h:outputText value="Custom?" /></f:facet>
			<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxRateDTO.customFlagBoolean}" />
		</rich:column>

		<rich:column id="modifiedFlagBoolean">
			<f:facet name="header"><h:outputText value="Modified?" /></f:facet>
			<h:selectBooleanCheckbox styleClass="check" disabled="true" value="#{taxRateDTO.modifiedFlagBoolean}" />
		</rich:column>

		</rich:dataTable>
		
		<a4j:outputPanel id="custLocnDataTest">
        	<c:if test="#{TaxJurisdictionBean.jurisdictionDataModel.rowCount == 0 and TaxJurisdictionBean.jurisdictionDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
        	</c:if>
        	
        	<c:if test="#{TaxJurisdictionBean.selectedJurisdictionIndex == -1 and TaxJurisdictionBean.jurisdictionDataModel.rowCount > 0}">
				<div class="nodatadisplay"><h:outputText value="No Jurisdiction selected." /></div>
        	</c:if>
       	</a4j:outputPanel>

		</div>
		</div>

	<div id="table-four-bottom">
	<ul class="right">
		<h:panelGroup rendered="#{!TaxJurisdictionBean.findMode}">
		<li class="add"><h:commandLink id="addJurisdictionTaxrate" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.validJurisdictionSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="addJurisdictionTaxrate" /></h:commandLink></li>
		<li class="copy-add"><h:commandLink id="copyAddJurisdictionTaxrate" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.validJurisdictionTaxrateSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="copyAddJurisdictionTaxrate" /></h:commandLink></li>		
		<li class="update"><h:commandLink id="updateJurisdictionTaxrate" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.validJurisdictionTaxrateSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="updateJurisdictionTaxrate" /></h:commandLink></li>
		<li class="delete"><h:commandLink id="deleteJurisdictionTaxrate" style="#{(TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxJurisdictionBean.canDeleteJurisdictionTaxrateSelection or TaxJurisdictionBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="deleteJurisdictionTaxrate" /></h:commandLink></li>
		<li class="view"><h:commandLink id="viewJurisdictionTaxrate" disabled="#{!TaxJurisdictionBean.validJurisdictionTaxrateSelection}" action="#{TaxJurisdictionBean.processTaxJurisdictionCommand}"><f:param name="command" value="viewJurisdictionTaxrate" /></h:commandLink></li>
		</h:panelGroup>
	</ul>
	</div>
	</div>
</div>
</h:form>

<rich:modalPanel id="messagesModalPanel" zindex="2000" minWidth="300"
	minHeight="300" autosized="true" moveable="true">
	<h:form id="messageForm">
		<f:facet name="header">
			<h:outputText value="Input Errors" />
		</f:facet>
		<f:facet name="controls">
			<h:graphicImage value="/images/icons/close.png"
				style="cursor:pointer"
				onclick="Richfaces.hideModalPanel('messagesModalPanel')" />
		</f:facet>
		<h:panelGrid columns="1" id="errorMsgBody">
			<h:outputText styleClass="errorMsg" id="errorMsg" value="Must enter at least one search parameter." />
		</h:panelGrid>
		<h:commandButton id="CloseMP"
			onclick="javascript:Richfaces.hideModalPanel('messagesModalPanel'); return false;"
			styleClass="closebutton" value="Close" />
			
	</h:form>
</rich:modalPanel>

</f:view>
</ui:define>

</ui:composition>

</html>
