<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<script type='text/javascript' src='../scripts/NCSTSActionButton.js'> </script>
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
function setValue() 
{
var tier1UseRate = document.getElementById("jurisdictionViewForm:stateUseRate").value;
var tier1SalesRate = document.getElementById("jurisdictionViewForm:stateSalesRate").value;
var tempValue = document.getElementById("jurisdictionViewForm:stateSplitAmountString").value;
var tier2Max  = document.getElementById("jurisdictionViewForm:stateTier2MaxAmountString").value;
var tier3UseRate = document.getElementById("jurisdictionViewForm:stateUseTier3Rate").value;
var tier2UseRate = document.getElementById("jurisdictionViewForm:stateUseTier2Rate").value;
var tier3SalesRate = document.getElementById("jurisdictionViewForm:stateSalesTier3Rate").value;
var tier2SalesRate = document.getElementById("jurisdictionViewForm:stateSalesTier2Rate").value;
var countyUseRate = document.getElementById("jurisdictionViewForm:countyUseRate").value;
var countySalesRate = document.getElementById("jurisdictionViewForm:countySalesRate").value;
var  citySalesRate= document.getElementById("jurisdictionViewForm:citySalesRate").value;
var  cityUseRate= document.getElementById("jurisdictionViewForm:cityUseRate").value;

if(tier2Max=="" || tier2Max==null){
document.getElementById("jurisdictionViewForm:stateTier2MaxAmountString").value=0;
}
if(tempValue=="" || tempValue==null){
document.getElementById("jurisdictionViewForm:stateSplitAmountString").value=0;
}
if(tier3UseRate=="" || tier3UseRate==null){
document.getElementById("jurisdictionViewForm:stateUseTier3Rate").value="0.0";
}
if(tier2UseRate=="" || tier2UseRate==null){
document.getElementById("jurisdictionViewForm:stateUseTier2Rate").value="0.0";
}
if(tier3SalesRate=="" || tier3SalesRate==null){
document.getElementById("jurisdictionViewForm:stateSalesTier3Rate").value="0.0";
}
if(tier2SalesRate=="" || tier2SalesRate==null){
document.getElementById("jurisdictionViewForm:stateSalesTier2Rate").value="0.0";
}
if(tier1UseRate=="" || tier1UseRate==null){
document.getElementById("jurisdictionViewForm:stateUseRate").value="0.0";
}
if(tier1SalesRate=="" || tier1SalesRate==null){
document.getElementById("jurisdictionViewForm:stateSalesRate").value="0.0";
}
if(countyUseRate=="" || countyUseRate==null){
document.getElementById("jurisdictionViewForm:countyUseRate").value="0.0";
}
if(countySalesRate=="" || countySalesRate==null){
document.getElementById("jurisdictionViewForm:countySalesRate").value="0.0";
}
if(cityUseRate=="" || cityUseRate==null){
document.getElementById("jurisdictionViewForm:cityUseRate").value="0.0";
}
if(citySalesRate=="" || citySalesRate==null){
document.getElementById("jurisdictionViewForm:citySalesRate").value="0.0";
}
}

function resetStateDetail()
{ 
    document.getElementById("jurisdictionViewForm:stateMenuMainDetail").value = "";
}

function enableDisableNexus(source, target) {
	if(source && source.value.length > 0) {
		document.getElementById(target).disabled = false;
	}
	else {
		document.getElementById(target).disabled = true;
	}
}

//]]>
</script>	
</ui:define>

<ui:define name="body">
<h:form id="jurisdictionViewForm">
<h1><h:graphicImage id="imgTaxRatesMaintenance" alt="Tax Rates Maintenance" value="/images/headers/hdr-tax-rates-maintenance.png"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages id="error97" errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
		
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td colspan="9"><h:outputText id="headerText" styleClass="headerText" value="#{TaxJurisdictionBean.currentAction.actionText}" /></td>
					<td class="column-spacer">&#160;</td>
					<td colspan="1" style="text-align:right;">
						<h:outputText id="rateId1" value="#{TaxJurisdictionBean.rateUsed ? '(Used on Trxn)':''}"/>&#160;
					</td>
				</tr>
			</thead>
			<!-- Jurisdiction Header -->
			<tr>
				<th colspan="11">&#160;Jurisdiction</th>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Jurisdiction ID:</td>
				<td>
					<h:inputText 
						disabled="true"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['JURISDICTIONID']}"
						id="jurisdictionId"
						value="#{TaxJurisdictionBean.editJurisdiction.jurisdictionIdString}" 
						onkeypress="return onlyIntegerValue(event);" 
						label="Jurisdiction ID">
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Description:</td>
				<td colspan="4">
					<h:inputText id="description"
						value="#{TaxJurisdictionBean.editJurisdiction.description}"
						disabled="true"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['DESCRIPTION']}"
						size="#{TaxJurisdictionBean.fieldSizeMap['DESCRIPTION']}" />
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td style="padding-left: 15px;" colspan="9">
					<table class="embedded-table" style="width: 100%">
						<tr>
							<td colspan="5" style="border-bottom: none;">
								<ui:include src="/WEB-INF/view/components/address.xhtml">
									<ui:param name="handler" value="#{TaxJurisdictionBean.addressHandler}"/>
									<ui:param name="id" value="addressInput"/>
									<ui:param name="readonly" value="true"/>
									<ui:param name="showheaders" value="true"/>
									<ui:param name="popupName" value="searchJurisdiction"/>
									<ui:param name="popupForm" value="searchJurisdictionForm"/>
								</ui:include>
							</td>
							<td style="border-bottom: none;">&#160;</td>
						</tr>
					</table>
				</td>			
			</tr>
			<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name or 
				not empty TaxJurisdictionBean.editJurisdiction.stj2Name or 
				not empty TaxJurisdictionBean.editJurisdiction.stj3Name or
				not empty TaxJurisdictionBean.editJurisdiction.stj4Name or
				not empty TaxJurisdictionBean.editJurisdiction.stj5Name}">
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td style="padding-left: 15px;" colspan="9">
					<table class="embedded-table" style="width: 100%">
						<tr>
							<td colspan="5" style="border-bottom: none;">
								
								<div id="embedded-table">
									<h:panelGrid id="stjTable" columns="7" cellpadding="2" cellspacing="0">
										<h:outputText id="hstj1" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name}" value="STJ1:"/>
										<h:outputText id="hstj2" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj2Name}" value="STJ2:"/>
										<h:outputText id="hstj3" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj3Name}" value="STJ3:"/>
										<h:outputText id="hstj4" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj4Name}" value="STJ4:"/>
										<h:outputText id="hstj5" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj5Name}" value="STJ5:"/>
										<h:outputText id="hstj11" rendered="#{empty TaxJurisdictionBean.editJurisdiction.stj1Name}" value=" "/>
										<h:outputText id="hstj21" rendered="#{empty TaxJurisdictionBean.editJurisdiction.stj2Name}" value=" "/>
										<h:outputText id="hstj31" rendered="#{empty TaxJurisdictionBean.editJurisdiction.stj3Name}" value=" "/>
										<h:outputText id="hstj41" rendered="#{empty TaxJurisdictionBean.editJurisdiction.stj4Name}" value=" "/>
										<h:outputText id="hstj51" rendered="#{empty TaxJurisdictionBean.editJurisdiction.stj5Name}" value=" "/>
										<h:outputText id="hstj6" value=" "/>
										<h:outputText id="hstj7" value=" "/>
										
										<h:inputText id="astj1" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name}" disabled="true" label="STJ1" value="#{TaxJurisdictionBean.editJurisdiction.stj1Name}"/>
										<h:inputText id="astj2" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj2Name}" disabled="true" label="STJ2" value="#{TaxJurisdictionBean.editJurisdiction.stj2Name}"/>
										<h:inputText id="astj3" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj3Name}" disabled="true" label="STJ3" value="#{TaxJurisdictionBean.editJurisdiction.stj3Name}"/>
										<h:inputText id="astj4" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj4Name}" disabled="true" label="STJ4" value="#{TaxJurisdictionBean.editJurisdiction.stj4Name}"/>
										<h:inputText id="astj5" rendered="#{not empty TaxJurisdictionBean.editJurisdiction.stj5Name}" disabled="true" label="STJ5" value="#{TaxJurisdictionBean.editJurisdiction.stj5Name}"/>
										<h:panelGroup id="pg191" />
										<h:panelGroup id="pg192" />
									</h:panelGrid>
								</div>								
							</td>
							<td style="border-bottom: none;">&#160;</td>
						</tr>
					</table>
				</td>			
			</tr>
			</c:if>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Last Update User ID:</td>
				<td colspan="6" class="column-description">
					<h:inputText style="width:650px;" disabled="true" id="tjUpdateUserId"
									value="#{TaxJurisdictionBean.editJurisdiction.updateUserId}" />
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td colspan="6" class="column-description">
					<h:inputText style="width:650px;" disabled="true" id="tjDpdateTimestamp"
									value="#{TaxJurisdictionBean.editJurisdiction.updateTimestamp}">
									<f:converter converterId="dateTime"/>
								</h:inputText>
				</td>
				<td>&#160;</td>
			</tr>
		</table>

		<h:panelGroup id="pg599" rendered="#{!TaxJurisdictionBean.isShortDetailAction}">

		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<!-- Tax Rates Header -->
			<tr>
				<th colspan="10">&#160;Tax Rates</th>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Tax Rate ID:</td>
				<td><h:inputText
					id="jurisdictionTaxrateId"
					value="#{TaxJurisdictionBean.editTaxRate.jurisdictionTaxrateIdString}"
					disabled="true"
					maxlength="#{TaxJurisdictionBean.fieldSizeMap['JURISDICTIONTAXRATEID']}" 
					 onkeypress="return onlyIntegerValue(event);"
					label="Tax Rate ID">
				</h:inputText>
			    </td>
				<td class="column-spacer">&#160;</td>
				<td>Rate Type:</td>
				<td>
					<h:selectOneMenu
						id="rateTypeCodeMenu"
						binding="#{TaxJurisdictionBean.rateTypeCodeMenu}"
						disabled="#{TaxJurisdictionBean.currentActionMap['RATETYPECODE'].disabled}"
						value="#{TaxJurisdictionBean.editTaxRate.ratetypeCode}" >
							<f:selectItems value="#{TaxJurisdictionBean.rateTypeCodeItems}"/>
					</h:selectOneMenu>
				</td>

				<td>&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Effective Date:</td>
				<td><rich:calendar
					binding="#{TaxJurisdictionBean.effectiveDate}"
					id="effectiveDate"
					value="#{TaxJurisdictionBean.editTaxRate.effectiveDate}"
					popup="true" locale="#{TaxJurisdictionBean.locale}"
					converter="date"
					datePattern="M/dd/yyyy" showApplyButton="false"
					enableManualInput="true"
					oninputkeypress="return onlyDateValue();" label="Effective Date: "
					toolTipMode="single"
					 disabled="#{TaxJurisdictionBean.currentActionMap['EFFECTIVEDATE'].disabled}"/></td>
				<td class="column-spacer">&#160;</td>
				<td>Expiration Date:</td>
				<td><rich:calendar
					id="expirationDate"
					binding="#{TaxJurisdictionBean.expirationDate}"
					value="#{TaxJurisdictionBean.editTaxRate.expirationDate}"
					popup="true" locale="#{TaxJurisdictionBean.locale}"
					converter="date"
					datePattern="M/dd/yyyy" showApplyButton="false"
					enableManualInput="true"
					oninputkeypress="return onlyDateValue();" label="Expiration Date: "
					toolTipMode="single"
					disabled="#{TaxJurisdictionBean.currentActionMap['EXPIRATIONDATE'].disabled}" />
				</td>
				<td style="width:30%;">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
			</tr>
			
			<tr>
				<td colspan="10">&#160;</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0"  width="100%" >						
			<tr>
				<td valign="bottom" style="width:450px;height:20px">
					<a4j:outputPanel id="commandpanel">
					<table cellpadding="0" cellspacing="0"  width="100%">
						<tr style="height:20px;">
							<td style="width:30px;">&#160;</td>		
							<td>&#160;</td>
							
							<td valign="bottom">		
								<a4j:commandLink id="countrylink" value="Country"
									style="#{(TaxJurisdictionBean.tabSelected)=='countrytab' ? 'color:#C92424' : 'color:#1A3A7C;text-decoration:underline;'};font-size:11px;background: url(../images/backgrounds/whitebackground.png) no-repeat 0 0; display: block; width: 60px;text-align: center;height:16px;" 
									action="#{TaxJurisdictionBean.processTabCommand}" reRender="commandpanel,sameaspanel,countrytabpanel,statetabpanel,countytabpanel,citytabpanel,stjtabpanel" >
									<f:param name="command" value="countrytab" />
								</a4j:commandLink>
							</td>
							
							<td  valign="bottom" >				
								<a4j:commandLink id="statelink" value="State"
									style="#{(TaxJurisdictionBean.tabSelected) == 'statetab' ? 'color:#C92424' : 'color:#1A3A7C;text-decoration:underline;'};font-size:11px;background:url(../images/backgrounds/whitebackground.png) no-repeat 0 0; display: block; width: 60px;text-align: center;height:16px;" 
									action="#{TaxJurisdictionBean.processTabCommand}" reRender="commandpanel,sameaspanel,countrytabpanel,statetabpanel,countytabpanel,citytabpanel,stjtabpanel">
									<f:param name="command" value="statetab" />
								</a4j:commandLink>
							</td>
							
							<td valign="bottom">		
								<a4j:commandLink id="countylink" value="County"
									style="#{(TaxJurisdictionBean.tabSelected)=='countytab' ? 'color:#C92424' : 'color:#1A3A7C;text-decoration:underline;'};font-size:11px;background: url(../images/backgrounds/whitebackground.png) no-repeat 0 0; display: block; width: 60px;text-align: center;height:16px;" 
									action="#{TaxJurisdictionBean.processTabCommand}"  reRender="commandpanel,sameaspanel,countrytabpanel,statetabpanel,countytabpanel,citytabpanel,stjtabpanel">
									<f:param name="command" value="countytab" />
								</a4j:commandLink>
							</td>
							
							<td  valign="bottom" >				
								<a4j:commandLink id="citylink" value="City"
									style="#{(TaxJurisdictionBean.tabSelected) == 'citytab' ? 'color:#C92424' : 'color:#1A3A7C;text-decoration:underline;'};font-size:11px;background:url(../images/backgrounds/whitebackground.png) no-repeat 0 0; display: block; width: 60px;text-align: center;height:16px;" 
									action="#{TaxJurisdictionBean.processTabCommand}" reRender="commandpanel,sameaspanel,countrytabpanel,statetabpanel,countytabpanel,citytabpanel,stjtabpanel">
									<f:param name="command" value="citytab" />
								</a4j:commandLink>
							</td>					
							<td  valign="bottom" >				
								<a4j:commandLink id="stjslink" value="STJs"
									style="#{(TaxJurisdictionBean.tabSelected) == 'stjstab' ? 'color:#C92424' : 'color:#1A3A7C;text-decoration:underline;'};font-size:11px;background:url(../images/backgrounds/whitebackground.png) no-repeat 0 0; display: block; width: 60px;text-align: center;height:16px;" 
									action="#{TaxJurisdictionBean.processTabCommand}" reRender="commandpanel,sameaspanel,countrytabpanel,statetabpanel,countytabpanel,citytabpanel,stjtabpanel">
									<f:param name="command" value="stjstab" />
								</a4j:commandLink>
							</td>	
												
							<td style="width:100px; height:20px;">&#160;</td>
							<td style="width:30%;">&#160;</td>
						</tr>	
					</table>
					</a4j:outputPanel>
				</td>
				
				<td valign="bottom" style="width:450px;height:20px" >			
					<table cellpadding="0" cellspacing="0"  width="100%">
						<tr style="height:20px;">
							<td class="column-spacer">&#160;&#160;&#160;</td>
							<td valign="bottom" style="color:#C92424;height:20px;" >
								<a4j:outputPanel id="sameaspanel">
									<h:outputLabel id="displaySameasMessage" style="#{(TaxJurisdictionBean.displaySameasMessage) ? 'visibility:visible;':'visibility:hidden;'}"
									value="All Sales Rates will be copied to Use Rates on save."/>	                       
		                       </a4j:outputPanel>
		                    </td>
							<td style="width:40%;">&#160;</td>
						</tr>		
					</table>		
				</td>
			</tr>		
		</table>	

		<a4j:outputPanel id="countrytabpanel">
		<c:if test="#{(TaxJurisdictionBean.tabSelected) == 'countrytab'}">
		<a4j:outputPanel id="countryRatesPanel">
			<table cellpadding="0" cellspacing="0"  width="100%" >						
				<tr>
					<td style="width:450px;height:305px" valign="top" >
						<!-- Country Sales Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="7" style="height:22px">Country Sales Rates:</td>
							</tr>
				
							<!-- Country Sales Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="countryTier1FromAmountSales" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="countryTier1RateSales"
										binding="#{TaxJurisdictionBean.countrySalesTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.countrySalesTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcountrySalesTier1Rate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="countryTier1SetAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="countryTier1ThroughAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.countrySalesTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcountryTier1ThroughAmountSales" event="onblur" 
											reRender="countryTier2FromAmountSales" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="countryTier1SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countrySalesTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countrySalesTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountryTier1SetToMaxSales" event="onclick"  
											reRender="countryTier1ThroughAmountSales,countryTier2FromAmountSales" actionListener="#{TaxJurisdictionBean.countrySalesTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of Country Sales Rates Tier 1 -->	
			
							<!-- Country Sales Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="countryTier2FromAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.countrySalesTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="countryTier2RateSales"
										binding="#{TaxJurisdictionBean.countrySalesTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="countryTier2SetAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countryTier2ThroughAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="countryTier2SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countrySalesTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countrySalesTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountryTier2SetToMaxSales" event="onclick"  
											reRender="countryTier2ThroughAmountSales,countryTier3RateSales,countryTier3SetAmountSales,countryTier3TaxEntireAmountSales" actionListener="#{TaxJurisdictionBean.countrySalesTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="countryTier2TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.countrySalesTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countrySalesTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of Country Sales Rates Tier 2 -->
							
							<!-- Country Sales Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="countryTier3RateSales"
										binding="#{TaxJurisdictionBean.countrySalesTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="countryTier3SetAmountSales"
										binding="#{TaxJurisdictionBean.countrySalesTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESTIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countryTier3ThroughAmountSales" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="countryTier3TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.countrySalesTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countrySalesTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESTIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="countrySalesMaxtaxAmt"
										binding="#{TaxJurisdictionBean.countrySalesMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYSALESMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYSALESMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of Country Sales Rates Tier 3 -->	
						</table>
			   		</td>
			   		
			   		<td style="width:450px;height:305px" valign="top" >
						<!-- Country Use Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="4">Country Use Rates:</td>					    
							    <td style="width:10px;height:22px">
							     	<h:selectBooleanCheckbox
										id="countrySameAsUseRates" styleClass="check"
										value="#{TaxJurisdictionBean.countrySameAsSalesRatesCheck}"
										binding="#{TaxJurisdictionBean.countrySameAsSalesRatesCheckBind}">
										<a4j:support id="ajaxcountrySameAsUseRates" event="onclick"
											reRender="combinedUseRate,combinedUseRate,countryRatesPanel,sameaspanel" actionListener="#{TaxJurisdictionBean.countrySameAsSalesRatesChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
					       		<td>Same as Sales Rates</td>
					       		<td class="column-spacer">&#160;</td>
					       	</tr>

							<!-- Country Use Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="countryTier1FromAmountUse" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="countryTier1RateUse"
										binding="#{TaxJurisdictionBean.countryUseTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.countryUseTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcountryUseTier1Rate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedUseRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="countryTier1SetAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="countryTier1ThroughAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.countryUseTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcountryTier1ThroughAmountUse" event="onblur" 
											reRender="countryTier2FromAmountUse" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="countryTier1SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countryUseTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countryUseTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountryTier1SetToMaxUse" event="onclick"  
											reRender="countryTier1ThroughAmountUse,countryTier2FromAmountUse" actionListener="#{TaxJurisdictionBean.countryUseTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of Country Use Rates Tier 1 -->	
			
							<!-- Country Use Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="countryTier2FromAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.countryUseTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="countryTier2RateUse"
										binding="#{TaxJurisdictionBean.countryUseTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="countryTier2SetAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countryTier2ThroughAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="countryTier2SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countryUseTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countryUseTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountryTier2SetToMaxUse" event="onclick"  
											reRender="countryTier2ThroughAmountUse,countryTier3RateUse,countryTier3SetAmountUse,countryTier3TaxEntireAmountUse" actionListener="#{TaxJurisdictionBean.countryUseTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="countryTier2TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.countryUseTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countryUseTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of Country Use Rates Tier 2 -->
							
							<!-- Country Use Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="countryTier3RateUse"
										binding="#{TaxJurisdictionBean.countryUseTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="countryTier3SetAmountUse"
										binding="#{TaxJurisdictionBean.countryUseTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSETIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countryTier3ThroughAmountUse" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="countryTier3TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.countryUseTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countryUseTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSETIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="countryUseMaxtaxAmt"
										binding="#{TaxJurisdictionBean.countryUseMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYUSEMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTRYUSEMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of Country Sales Rates Tier 3 -->	
						</table>
			   		</td>
			    </tr>
			</table>
		</a4j:outputPanel>
		</c:if>
		</a4j:outputPanel>
	 	
		<a4j:outputPanel id="statetabpanel">
		<c:if test="#{(TaxJurisdictionBean.tabSelected) == 'statetab'}">
		<a4j:outputPanel id="stateRatesPanel">
			<table cellpadding="0" cellspacing="0"  width="100%" >						
				<tr>
					<td style="width:450px;height:305px" valign="top" >
						<!-- State Sales Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="7" style="height:22px">State Sales Rates:</td>
							</tr>
				
							<!-- State Sales Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="stateTier1FromAmountSales" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="stateTier1RateSales"
										binding="#{TaxJurisdictionBean.stateSalesTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.stateSalesTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxstateSalesTier1Rate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="stateTier1SetAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="stateTier1ThroughAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stateSalesTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxstateTier1ThroughAmountSales" event="onblur" 
											reRender="stateTier2FromAmountSales" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="stateTier1SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.stateSalesTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.stateSalesTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxstateTier1SetToMaxSales" event="onclick"  
											reRender="stateTier1ThroughAmountSales,stateTier2FromAmountSales" actionListener="#{TaxJurisdictionBean.stateSalesTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of State Sales Rates Tier 1 -->	
			
							<!-- State Sales Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="stateTier2FromAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.stateSalesTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="stateTier2RateSales"
										binding="#{TaxJurisdictionBean.stateSalesTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="stateTier2SetAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="stateTier2ThroughAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="stateTier2SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.stateSalesTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.stateSalesTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxstateTier2SetToMaxSales" event="onclick"  
											reRender="stateTier2ThroughAmountSales,stateTier3RateSales,stateTier3SetAmountSales,stateTier3TaxEntireAmountSales" actionListener="#{TaxJurisdictionBean.stateSalesTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="stateTier2TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.stateSalesTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.stateSalesTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of State Sales Rates Tier 2 -->
							
							<!-- State Sales Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="stateTier3RateSales"
										binding="#{TaxJurisdictionBean.stateSalesTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="stateTier3SetAmountSales"
										binding="#{TaxJurisdictionBean.stateSalesTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESTIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="stateTier3ThroughAmountSales" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="stateTier3TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.stateSalesTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.stateSalesTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESTIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="stateSalesMaxtaxAmt"
										binding="#{TaxJurisdictionBean.stateSalesMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATESALESMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATESALESMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of State Sales Rates Tier 3 -->	
						</table>
			   		</td>
			   		
			   		<td style="width:450px;height:305px" valign="top" >
						<!-- State Use Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
					       	<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="4">State Use Rates:</td>					    
							    <td style="width:10px;height:22px">
							     	<h:selectBooleanCheckbox
										id="stateSameAsUseRates" styleClass="check"
										value="#{TaxJurisdictionBean.stateSameAsSalesRatesCheck}"
										binding="#{TaxJurisdictionBean.stateSameAsSalesRatesCheckBind}">
										<a4j:support id="ajaxstateSameAsUseRates" event="onclick"
											reRender="combinedUseRate,combinedUseRate,stateRatesPanel,sameaspanel" actionListener="#{TaxJurisdictionBean.stateSameAsSalesRatesChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
					       		<td>Same as Sales Rates</td>
					       		<td class="column-spacer">&#160;</td>
					       	</tr>
				
							<!-- State Use Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="stateTier1FromAmountUse" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="stateTier1RateUse"
										binding="#{TaxJurisdictionBean.stateUseTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.stateUseTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxstateUseTier1Rate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedUseRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="stateTier1SetAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="stateTier1ThroughAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stateUseTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxstateTier1ThroughAmountUse" event="onblur" 
											reRender="stateTier2FromAmountUse" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="stateTier1SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.stateUseTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.stateUseTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxstateTier1SetToMaxUse" event="onclick"  
											reRender="stateTier1ThroughAmountUse,stateTier2FromAmountUse" actionListener="#{TaxJurisdictionBean.stateUseTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of State Use Rates Tier 1 -->	
			
							<!-- State Use Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="stateTier2FromAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.stateUseTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="stateTier2RateUse"
										binding="#{TaxJurisdictionBean.stateUseTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="stateTier2SetAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="stateTier2ThroughAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="stateTier2SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.stateUseTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.stateUseTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxstateTier2SetToMaxUse" event="onclick"  
											reRender="stateTier2ThroughAmountUse,stateTier3RateUse,stateTier3SetAmountUse,stateTier3TaxEntireAmountUse" actionListener="#{TaxJurisdictionBean.stateUseTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="stateTier2TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.stateUseTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.stateUseTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of State Use Rates Tier 2 -->
							
							<!-- State Use Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="stateTier3RateUse"
										binding="#{TaxJurisdictionBean.stateUseTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="stateTier3SetAmountUse"
										binding="#{TaxJurisdictionBean.stateUseTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSETIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="stateTier3ThroughAmountUse" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="stateTier3TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.stateUseTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.stateUseTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSETIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="stateUseMaxtaxAmt"
										binding="#{TaxJurisdictionBean.stateUseMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STATEUSEMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STATEUSEMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of State Sales Rates Tier 3 -->	
						</table>
			   		</td>
			    </tr>
			</table>
		</a4j:outputPanel>
		</c:if>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="countytabpanel">
		<c:if test="#{(TaxJurisdictionBean.tabSelected) == 'countytab'}">
		<a4j:outputPanel id="countyRatesPanel">
			<table cellpadding="0" cellspacing="0"  width="100%" >						
				<tr>
					<td style="width:450px;height:305px" valign="top" >
						<!-- County Sales Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="7" style="height:22px">County Sales Rates:</td>
							</tr>
				
							<!-- County Sales Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="countyTier1FromAmountSales" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="countyTier1RateSales"
										binding="#{TaxJurisdictionBean.countySalesTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.countySalesTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcountySalesTier1Rate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="countyTier1SetAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="countyTier1ThroughAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.countySalesTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcountyTier1ThroughAmountSales" event="onblur" 
											reRender="countyTier2FromAmountSales" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="countyTier1SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countySalesTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countySalesTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountyTier1SetToMaxSales" event="onclick"  
											reRender="countyTier1ThroughAmountSales,countyTier2FromAmountSales" actionListener="#{TaxJurisdictionBean.countySalesTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of County Sales Rates Tier 1 -->	
			
							<!-- County Sales Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="countyTier2FromAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.countySalesTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="countyTier2RateSales"
										binding="#{TaxJurisdictionBean.countySalesTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="countyTier2SetAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countyTier2ThroughAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="countyTier2SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countySalesTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countySalesTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountyTier2SetToMaxSales" event="onclick"  
											reRender="countyTier2ThroughAmountSales,countyTier3RateSales,countyTier3SetAmountSales,countyTier3TaxEntireAmountSales" actionListener="#{TaxJurisdictionBean.countySalesTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="countyTier2TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.countySalesTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countySalesTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of County Sales Rates Tier 2 -->
							
							<!-- County Sales Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="countyTier3RateSales"
										binding="#{TaxJurisdictionBean.countySalesTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="countyTier3SetAmountSales"
										binding="#{TaxJurisdictionBean.countySalesTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESTIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countyTier3ThroughAmountSales" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="countyTier3TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.countySalesTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countySalesTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESTIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="countySalesMaxtaxAmt"
										binding="#{TaxJurisdictionBean.countySalesMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYSALESMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYSALESMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of County Sales Rates Tier 3 -->	
						</table>
			   		</td>
			   		
			   		<td style="width:450px;height:305px" valign="top" >
						<!-- County Use Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
					       	<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="4">County Use Rates:</td>					    
							    <td style="width:10px;height:22px">
							     	<h:selectBooleanCheckbox
										id="countySameAsUseRates" styleClass="check"
										value="#{TaxJurisdictionBean.countySameAsSalesRatesCheck}"
										binding="#{TaxJurisdictionBean.countySameAsSalesRatesCheckBind}">
										<a4j:support id="ajaxcountySameAsUseRates" event="onclick"
											reRender="combinedUseRate,combinedUseRate,countyRatesPanel,sameaspanel" actionListener="#{TaxJurisdictionBean.countySameAsSalesRatesChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
					       		<td>Same as Sales Rates</td>
					       		<td class="column-spacer">&#160;</td>
					       	</tr>
				
							<!-- County Use Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="countyTier1FromAmountUse" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="countyTier1RateUse"
										binding="#{TaxJurisdictionBean.countyUseTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.countyUseTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcountyUseTier1Rate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedUseRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="countyTier1SetAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="countyTier1ThroughAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.countyUseTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcountyTier1ThroughAmountUse" event="onblur" 
											reRender="countyTier2FromAmountUse" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="countyTier1SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countyUseTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countyUseTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountyTier1SetToMaxUse" event="onclick"  
											reRender="countyTier1ThroughAmountUse,countyTier2FromAmountUse" actionListener="#{TaxJurisdictionBean.countyUseTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of County Use Rates Tier 1 -->	
			
							<!-- County Use Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="countyTier2FromAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.countyUseTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="countyTier2RateUse"
										binding="#{TaxJurisdictionBean.countyUseTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="countyTier2SetAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countyTier2ThroughAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="countyTier2SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.countyUseTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.countyUseTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcountyTier2SetToMaxUse" event="onclick"  
											reRender="countyTier2ThroughAmountUse,countyTier3RateUse,countyTier3SetAmountUse,countyTier3TaxEntireAmountUse" actionListener="#{TaxJurisdictionBean.countyUseTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="countyTier2TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.countyUseTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countyUseTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of County Use Rates Tier 2 -->
							
							<!-- County Use Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="countyTier3RateUse"
										binding="#{TaxJurisdictionBean.countyUseTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="countyTier3SetAmountUse"
										binding="#{TaxJurisdictionBean.countyUseTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSETIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="countyTier3ThroughAmountUse" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="countyTier3TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.countyUseTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.countyUseTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSETIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="countyUseMaxtaxAmt"
										binding="#{TaxJurisdictionBean.countyUseMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYUSEMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTYUSEMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of County Sales Rates Tier 3 -->	
						</table>
			   		</td>
			    </tr>
			</table>
		</a4j:outputPanel>
		</c:if>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="citytabpanel">
		<c:if test="#{(TaxJurisdictionBean.tabSelected) == 'citytab'}">
		<a4j:outputPanel id="cityRatesPanel">
			<table cellpadding="0" cellspacing="0"  width="100%" >						
				<tr>
					<td style="width:450px;height:305px" valign="top" >
						<!-- City Sales Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="7" style="height:22px">City Sales Rates:</td>
							</tr>
				
							<!-- City Sales Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="cityTier1FromAmountSales" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="cityTier1RateSales"
										binding="#{TaxJurisdictionBean.citySalesTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.citySalesTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcitySalesTier1Rate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="cityTier1SetAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="cityTier1ThroughAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.citySalesTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcityTier1ThroughAmountSales" event="onblur" 
											reRender="cityTier2FromAmountSales" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="cityTier1SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.citySalesTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.citySalesTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcityTier1SetToMaxSales" event="onclick"  
											reRender="cityTier1ThroughAmountSales,cityTier2FromAmountSales" actionListener="#{TaxJurisdictionBean.citySalesTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of City Sales Rates Tier 1 -->	
			
							<!-- City Sales Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="cityTier2FromAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.citySalesTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="cityTier2RateSales"
										binding="#{TaxJurisdictionBean.citySalesTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="cityTier2SetAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="cityTier2ThroughAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="cityTier2SetToMaxSales" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.citySalesTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.citySalesTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcityTier2SetToMaxSales" event="onclick"  
											reRender="cityTier2ThroughAmountSales,cityTier3RateSales,cityTier3SetAmountSales,cityTier3TaxEntireAmountSales" actionListener="#{TaxJurisdictionBean.citySalesTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="cityTier2TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.citySalesTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.citySalesTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of City Sales Rates Tier 2 -->
							
							<!-- City Sales Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="cityTier3RateSales"
										binding="#{TaxJurisdictionBean.citySalesTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="cityTier3SetAmountSales"
										binding="#{TaxJurisdictionBean.citySalesTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESTIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="cityTier3ThroughAmountSales" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="cityTier3TaxEntireAmountSales" styleClass="check"
										value="#{TaxJurisdictionBean.citySalesTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.citySalesTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESTIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="citySalesMaxtaxAmt"
										binding="#{TaxJurisdictionBean.citySalesMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYSALESMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYSALESMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of City Sales Rates Tier 3 -->	
						</table>
			   		</td>
			   		
			   		<td style="width:450px;height:305px" valign="top" >
						<!-- City Use Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
					       	<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="4">City Use Rates:</td>					    
							    <td style="width:10px;height:22px">
							     	<h:selectBooleanCheckbox
										id="citySameAsUseRates" styleClass="check"
										value="#{TaxJurisdictionBean.citySameAsSalesRatesCheck}"
										binding="#{TaxJurisdictionBean.citySameAsSalesRatesCheckBind}">
										<a4j:support id="ajaxcitySameAsUseRates" event="onclick"
											reRender="combinedUseRate,combinedUseRate,cityRatesPanel,sameaspanel" actionListener="#{TaxJurisdictionBean.citySameAsSalesRatesChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
					       		<td>Same as Sales Rates</td>
					       		<td class="column-spacer">&#160;</td>
					       	</tr>
				
							<!-- City Use Rates Tier 1 -->
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 1:</td>
			    				<td>From Amount:</td>
			    				<td>
			    					<h:inputText id="cityTier1FromAmountUse" value="0" disabled="true"></h:inputText>
			    				</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
			    				<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">Rate or Set Amount:</td>
			        			<td>
			        				<h:inputText
										id="cityTier1RateUse"
										binding="#{TaxJurisdictionBean.cityUseTier1RateInputText}"			
										valueChangeListener="#{TaxJurisdictionBean.cityUseTier1RateInputTextChangeListener}"								
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER1RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER1RATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >										
										<a4j:support id="ajaxcityUseTier1Rate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedUseRateWarning" immediate="true"/>												
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
			       				<td colspan="2">
			       					<h:inputText
										id="cityTier1SetAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier1SetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER1SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER1SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
			
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td>Through Amount:</td>
			        			<td>
							        <h:inputText id="cityTier1ThroughAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier1MaxAmtInputText}"
										valueChangeListener="#{TaxJurisdictionBean.cityUseTier1MaxAmtChangeListener}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER1MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER1MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);" onchange="return setValue();"
										label="Tier 1 Maximum Amount" >
										<a4j:support id="ajaxcityTier1ThroughAmountUse" event="onblur" 
											reRender="cityTier2FromAmountUse" />
									</h:inputText>
								</td>
					       		<td>&#160;</td>
					       		<td style="width:10px;">
					        		<h:selectBooleanCheckbox id="cityTier1SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.cityUseTier1SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.cityUseTier1SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER1SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcityTier1SetToMaxUse" event="onclick"  
											reRender="cityTier1ThroughAmountUse,cityTier2FromAmountUse" actionListener="#{TaxJurisdictionBean.cityUseTier1SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
				        		<td>Set to Maximum</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of City Use Rates Tier 1 -->	
			
							<!-- City Use Rates Tier 2 -->
				       		<tr>
				       			<td class="column-spacer">&#160;</td>
								<td>Tier 2:</td>
				       			<td>From Amount:</td>
					       		<td>
								    <h:inputText
										id="cityTier2FromAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier2MinAmtInputText}"
										value="#{TaxJurisdictionBean.editTaxRate.cityUseTier2MinAmt}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2MINAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER2MINAMT']}" 
										onkeypress="return onlyIntegerValue(event);" label="">
									</h:inputText>
								</td>	
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Rate or Set Amount:</td>
							       <td><h:inputText
										id="cityTier2RateUse"
										binding="#{TaxJurisdictionBean.cityUseTier2RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER2RATE']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
					       		<td class="column-spacer">or</td>
					       		<td colspan="2">
					       			<h:inputText
										id="cityTier2SetAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier2SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER2SETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
				
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="cityTier2ThroughAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier2MaxAmtInputText}"
										immediate="true"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2MAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER2MAXAMT']}" 
										onkeypress="return onlyIntegerValue(event);"
										onchange="return setValue();"
										label="Tier 2 Maximum Amount">
									</h:inputText>
								</td>
							    <td>&#160;</td>
							    <td style="width:10px;">
									<h:selectBooleanCheckbox id="cityTier2SetToMaxUse" styleClass="check" immediate="true"
										value="#{TaxJurisdictionBean.cityUseTier2SetToMaximumCheck}"
										binding="#{TaxJurisdictionBean.cityUseTier2SetToMaximumCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2SETTOMAXIMUMCHECK'].disabled}">
										<a4j:support id="ajaxcityTier2SetToMaxUse" event="onclick"  
											reRender="cityTier2ThroughAmountUse,cityTier3RateUse,cityTier3SetAmountUse,cityTier3TaxEntireAmountUse" actionListener="#{TaxJurisdictionBean.cityUseTier2SetToMaximumFlagChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
							    <td>Set to Maximum</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Tax Entire Amount?:</td>		
								<td>
							        <h:selectBooleanCheckbox
										id="cityTier2TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.cityUseTier2TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.cityUseTier2TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER2ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							<!-- End of City Use Rates Tier 2 -->
							
							<!-- City Use Rates Tier 3 -->			      
							<tr>
								<td class="column-spacer">&#160;</td>
								<td>Tier 3:</td>
							    <td>Rate or Set Amount:</td>
							        <td><h:inputText
										id="cityTier3RateUse"
										binding="#{TaxJurisdictionBean.cityUseTier3RateInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER3RATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER3RATE']}"
										immediate="true"									 
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">or</td>
							    <td colspan="2">
							    	<h:inputText
										id="cityTier3SetAmountUse"
										binding="#{TaxJurisdictionBean.cityUseTier3SetamtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER3SETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSETIER3SETAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
								</td>
							    <td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
							    <td>Through Amount:</td>		
								<td>
									<h:inputText
										id="cityTier3ThroughAmountUse" value="*MAX" disabled="true" >
									</h:inputText>							
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
				        		<td>Tax Entire Amount?:</td>		
								<td>
				        			<h:selectBooleanCheckbox
										id="cityTier3TaxEntireAmountUse" styleClass="check"
										value="#{TaxJurisdictionBean.cityUseTier3TaxEntireAmountCheck}"
										binding="#{TaxJurisdictionBean.cityUseTier3TaxEntireAmountCheckBind}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSETIER3ENTAMFLAG'].disabled}">
									</h:selectBooleanCheckbox>
								</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
				        		<td class="column-spacer">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="2">MaxTax Amount:</td>
			    				<td>
			    					<h:inputText
										id="cityUseMaxtaxAmt"
										binding="#{TaxJurisdictionBean.cityUseMaxtaxAmtInputText}"
										disabled="#{TaxJurisdictionBean.currentActionMap['CITYUSEMAXTAXAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITYUSEMAXTAXAMT']}" 
										immediate="true"									
										onkeypress="return onlyIntegerValue(event);">
									</h:inputText>
			    				</td>
			    				<td class="column-spacer" colspan="4">&#160;</td>
							</tr>
							<!-- End of City Sales Rates Tier 3 -->	
						</table>
			   		</td>
			    </tr>
			</table>
		</a4j:outputPanel>
		</c:if>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="stjtabpanel">
		<c:if test="#{(TaxJurisdictionBean.tabSelected) == 'stjstab'}">
		<a4j:outputPanel id="stjRatesPanel">
			<table cellpadding="0" cellspacing="0"  width="100%" >						
				<tr>
					<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
					
					<td style="width:450px;height:305px" valign="top" >
						<!-- STJ Sales Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
							<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="7" style="height:22px">STJs Sales Rates:</td>
							</tr>
				
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ 1 Rate or Set Amount:</td>
								<td>
									<h:inputText
										id="stj1SalesRate"
										binding="#{TaxJurisdictionBean.stj1SalesRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"										
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ1SALESRATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ1SALESRATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >
										<a4j:support id="ajaxstj1SalesRate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																					
									</h:inputText>
								</td>
								
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj1SalesSetamt"
										binding="#{TaxJurisdictionBean.stj1SalesSetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ1SALESSETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ1SALESSETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj2Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ 2 Rate or Set Amount:</td>
								<td>
									<h:inputText
										id="stj2SalesRate"
										binding="#{TaxJurisdictionBean.stj2SalesRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ2SALESRATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ2SALESRATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >
										<a4j:support id="ajaxstj2SalesRate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																					
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj2SalesSetamt"
										binding="#{TaxJurisdictionBean.stj2SalesSetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ2SALESSETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ2SALESSETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj3Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ 3 Rate or Set Amount:</td>
								<td>
									<h:inputText
										id="stj3SalesRate"
										binding="#{TaxJurisdictionBean.stj3SalesRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ3SALESRATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ3SALESRATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >	
										<a4j:support id="ajaxstj3SalesRate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj3SalesSetamt"
										binding="#{TaxJurisdictionBean.stj3SalesSetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ3SALESSETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ3SALESSETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj4Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ 4 Rate or Set Amount:</td>
								<td>
									<h:inputText
										id="stj4SalesRate"
										binding="#{TaxJurisdictionBean.stj4SalesRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ4SALESRATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ4SALESRATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >
										<a4j:support id="ajaxstj4SalesRate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj4SalesSetamt"
										binding="#{TaxJurisdictionBean.stj4SalesSetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ4SALESSETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ4SALESSETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj5Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ 5 Rate or Set Amount:</td>
								<td>
									<h:inputText
										id="stj5SalesRate"
										binding="#{TaxJurisdictionBean.stj5SalesRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ5SALESRATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ5SALESRATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >	
										<a4j:support id="ajaxstj5SalesRate" event="onblur"
											reRender="combinedSalesRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj5SalesSetamt"
										binding="#{TaxJurisdictionBean.stj5SalesSetamtInputText}"	
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ5SALESSETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ5SALESSETAMT']}" 
										immediate="true"
										onkeypress="return onlyIntegerValue(event);">	
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
						</table>
			   		</td>
			   		
			   		<td style="width:450px;height:305px" valign="top" >
						<!-- Country Use Rates-->
						<table cellpadding="0" cellspacing="0"  width="100%" style="border: 1px solid Silver;">						
					       	<tr>
								<td class="column-spacer">&#160;</td>
								<td colspan="4">STJs Use Rates:</td>					    
							    <td style="width:10px;height:22px">
							     	<h:selectBooleanCheckbox
										id="stjSameAsUseRates" styleClass="check"
										value="#{TaxJurisdictionBean.stjSameAsSalesRatesCheck}"
										binding="#{TaxJurisdictionBean.stjSameAsSalesRatesCheckBind}">
										<a4j:support id="ajaxstjSameAsUseRates" event="onclick"
											reRender="combinedUseRate,combinedUseRate,stjRatesPanel,sameaspanel" actionListener="#{TaxJurisdictionBean.stjSameAsSalesRatesChangeListener}" />
									</h:selectBooleanCheckbox>
								</td>
					       		<td>Same as Sales Rates</td>
					       		<td class="column-spacer">&#160;</td>
					       	</tr>
				
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ&#160;1&#160;Rate&#160;or&#160;Set&#160;Amount:</td>
								<td>
									<h:inputText
										id="stj1UseRate"
										binding="#{TaxJurisdictionBean.stj1UseRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ1USERATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ1USERATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >
										<a4j:support id="ajaxstj1UseRate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																					
									</h:inputText>
								</td>						
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj1UseSetamt"
										binding="#{TaxJurisdictionBean.stj1UseSetamtInputText}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ1USESETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ1USESETAMT']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj2Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ&#160;2&#160;Rate&#160;or&#160;Set&#160;Amount:</td>
								<td>
									<h:inputText
										id="stj2UseRate"
										binding="#{TaxJurisdictionBean.stj2UseRateInputText}"
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ2USERATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ2USERATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >	
										<a4j:support id="ajaxstj2UseRate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>						
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj2UseSetamt"
										binding="#{TaxJurisdictionBean.stj2UseSetamtInputText}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ2USESETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ2USESETAMT']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj3Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ&#160;3&#160;Rate&#160;or&#160;Set&#160;Amount:</td>
								<td>
									<h:inputText
										id="stj3UseRate"
										binding="#{TaxJurisdictionBean.stj3UseRateInputText}"	
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"		
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ3USERATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ3USERATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >
										<a4j:support id="ajaxstj3UseRate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																					
									</h:inputText>
								</td>						
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj3UseSetamt"
										binding="#{TaxJurisdictionBean.stj3UseSetamtInputText}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ3USESETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ3USESETAMT']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj4Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ&#160;4&#160;Rate&#160;or&#160;Set&#160;Amount:</td>
								<td>
									<h:inputText
										id="stj4UseRate"
										binding="#{TaxJurisdictionBean.stj4UseRateInputText}"	
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"		
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ4USERATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ4USERATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >	
										<a4j:support id="ajaxstj4UseRate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>						
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj4UseSetamt"
										binding="#{TaxJurisdictionBean.stj4UseSetamtInputText}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ4USESETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ4USESETAMT']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
							
							<c:if test="#{not empty TaxJurisdictionBean.editJurisdiction.stj5Name}">
							<tr>
								<td class="column-spacer">&#160;</td>
								<td class="column-spacer">&#160;</td>
			        			<td class="column-label3">STJ&#160;5&#160;Rate&#160;or&#160;Set&#160;Amount:</td>
								<td>
									<h:inputText
										id="stj5UseRate"
										binding="#{TaxJurisdictionBean.stj5UseRateInputText}"	
										valueChangeListener="#{TaxJurisdictionBean.stjRatesInputTextChangeListener}"		
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ5USERATE'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ5USERATE']}" 
										onkeypress="return onlyNumerics(event);"
										immediate="true" >	
										<a4j:support id="ajaxstj5UseRate" event="onblur"
											reRender="combinedUseRate,combinedUseRateWarning,combinedSalesRateWarning" immediate="true"/>																				
									</h:inputText>
								</td>						
			       				<td class="column-spacer">or</td>
								<td colspan="2">
									<h:inputText
										id="stj5UseSetamt"
										binding="#{TaxJurisdictionBean.stj5UseSetamtInputText}"			
										disabled="#{TaxJurisdictionBean.currentActionMap['STJ5USESETAMT'].disabled}"
										maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ5USESETAMT']}" 
										immediate="true"
										onkeypress="return onlyNumerics(event);">
									</h:inputText>
								</td>
			       				<td class="column-spacer" style="width:10%;">&#160;</td>
							</tr>
							</c:if>
						</table>
			   		</td>
			    
			    	</c:if>
			    	
			    	<c:if test="#{empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
			    	
			    		<td style="width:450px;height:305px;text-align: center" valign="middle" >Jurisdiction contains no STJs.</td>
			    	</c:if>
			    </tr>
			</table>
		</a4j:outputPanel>
		</c:if>
		</a4j:outputPanel>
		
		<table cellpadding="0" cellspacing="0" style="width: 100%;">
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Combined Tax Rates:</td>
				<td>Sales Rates:</td>
				<td>Use Rates:</td>							
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Rates:</td>							
				<td>
					<h:inputText  binding="#{TaxJurisdictionBean.combinedSalesRate}"
						disabled="true"
					id="combinedSalesRate">
				
					</h:inputText>
					<br/>
					<em class="red">
	                  <h:outputText id="combinedSalesRateWarning" binding="#{TaxJurisdictionBean.salesWarningOutputText}"  >     
	                  </h:outputText>              
	                </em>
				</td>
				<td>
					<h:inputText binding="#{TaxJurisdictionBean.combinedUseRate}"
					disabled="true"
					id="combinedUseRate" >
					</h:inputText>
					<br/>
					<em class="red">
	                  <h:outputText id="combinedUseRateWarning" binding="#{TaxJurisdictionBean.useWarningOutputText}"  >     
	                  </h:outputText>
	                </em>
				</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0" style="width: 100%;">
			<tr>
				<td class="column-spacer" style="border:0px;">&#160;</td>
				<td valign="top" style="border:0px;">
					
				</td>
				<td class="column-spacer" style="width: 30%;border:0px;">&#160;</td>
				<td valign="top" style="border:0px;">
	
				</td>
				<td class="column-spacer" style="border:0px;">&#160;</td>
			</tr>
    </table>
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-spacer">&#160;</td>
			<td>Last Update User ID:</td>
			<td colspan="6" class="column-description">
				<h:inputText style="width:650px;" disabled="true" id="updateUserId"
								value="#{TaxJurisdictionBean.editTaxRate.updateUserId}" />
			</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td class="column-spacer">&#160;</td>
			<td class="column-spacer">&#160;</td>
			<td>Last Update Timestamp:</td>
			<td colspan="6" class="column-description">
				<h:inputText style="width:650px;" disabled="true" id="updateTimestamp"
								value="#{TaxJurisdictionBean.editTaxRate.updateTimestamp}">
								<f:converter converterId="dateTime"/>
							</h:inputText>
			</td>
			<td>&#160;</td>
		</tr>
	</table>

		</h:panelGroup>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="copy-update">
			<a4j:commandLink id="btnCopyUpdate" rendered="#{TaxJurisdictionBean.updateTaxrateAction}" immediate="true" 
				actionListener="#{TaxJurisdictionBean.copyUpdateListener}" reRender="jurisdictionViewForm"/>
		</li>
		<li class="ok"><h:commandLink id="t1ok" action="#{TaxJurisdictionBean.okActionButton}" rendered="#{TaxJurisdictionBean.showCancelButton}" /></li>
		<li class="ok"><h:commandLink id="btnOk" immediate="true" action="#{TaxJurisdictionBean.cancelActionButton}" rendered="#{!TaxJurisdictionBean.showCancelButton}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{TaxJurisdictionBean.cancelActionButton}" rendered="#{TaxJurisdictionBean.showCancelButton}" /></li>
	</ul>
	</div>
	</div>
</div>

		<div>
			<!-- Required to create the additional room needed for the footer  -->
			<rich:spacer height="25px" style="layout:block;"/>
		</div>
</h:form>
	<h:form id="messageForm2">
<!--  minHeight="200" minWidth="200"  -->
		<rich:modalPanel id="messagesModalPanel"  zindex="2000"
			autosized="true" moveable="true" binding="#{TaxJurisdictionBean.messagesModalPanel}">
			<f:facet name="header">
				<h:outputText id="error1384" value="Input Errors" />
			</f:facet>
			<f:facet name="controls">
				<h:graphicImage id="img1387" value="/images/icons/close.png"
					style="cursor:pointer" onclick="Richfaces.hideModalPanel('messagesModalPanel')" />
			</f:facet>
			<p><h:dataTable id="formMessages1018"
				binding="#{TaxJurisdictionBean.formMessagesDataTable}"
				var="formMessage">
				<h:column id="Message1392">
					<f:facet name="header">
						<h:outputText id="header1395" styleClass="headerText" value="Input Errors" />
					</f:facet>
					<h:outputText id="error1397" styleClass="errorMsg" value="#{formMessage}" />
				</h:column>
			</h:dataTable></p>
			<p>&#160;</p>
			<h:commandButton id="CloseMP1029"
				onclick="javascript:Richfaces.hideModalPanel('messagesModalPanel'); return false;"
				styleClass="closebutton" value="Close" />
		</rich:modalPanel>
	</h:form>
</ui:define>
</ui:composition>
</html>

