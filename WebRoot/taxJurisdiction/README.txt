Tax Jurisdiction and Tax Rate TODO List:

The form validation needs to be a little cleaner in respect to the Coverters.

Change so "disabled" looks more obvious.

Calendar popup on first page is partially hidden because of small DIV.

Add a "" to the list of Measure Type (Not sure if I still need this -- JFJ 3/14/2008)

Think about converting Use Rate Sum and Sales Rate Sum to Backing Bean from Javascript.

Set sort order in Data Tables

Make Tax Jurisdiction Info fit better

Better Visual indicator that JurisdictionId and JurisdictionTaxRateId are not editable when Adding a new TJ or TR.

Multi-line column headers for Data Tables.




KNOWN BUGS / ISSUES:
Calendar Javascript error when double-clicking on Data Tables
http://lists.jboss.org/pipermail/richfaces-issues/2007-November/003163.html

Only allow one row in each table on Main page to be selected.  There is no way to currently make ScrollableDataTable
only select one row.  It is a feature request for the next version.

When trying to sort a column (in rich:scrollableDataTable) with in a null in it, javascript error occurs.
http://jira.jboss.org/jira/browse/RF-1636

DATA & UI ISSUES:
STS uses alphabetic characters in the zip field to show records from Canada

STS Application requires a city field for Tax Jurisdiction, but the Canadian records don't have a city data, so Canadian records will not work in the maintenance interface.

There are records in the TaxRate database tables that have a Measure Type code of 16.  There is no reference to a MTC of 16 in the JAVA Client.

There are records in the TaxRate database tables that have a county_single_flag with a "G" as a value.  Because this is a "boolean", I am unsure how to handle this.


BUSINESS RULES NOTES:

StateTier1Max (Checkbox)
	CHECKED:
	State_split_amount  field will then be read-only and will display *MAX.
	Set State_split_amount to 999999999
	if (tier1MaxVal == MAX_AMOUNT) { Set Checkbox }

	UNCHECKED:
	State_split_amount field is set to ""
	Enable State_split_amount field
	

StateTier2Max
	CHECKED:
	stateTier2MaxAmount set to *MAX / Set stateTier2MaxAmount to 999999999
	Disable stateTier2MaxAmount
	stateUseTier3Rate set to ""
	stateUseTier3Rate disabled
	stateTier3MaxAmount set to *MAX / Set stateTier3MaxAmount to 999999999
	Disable stateTier3MaxAmount 
	
	UNCHECKED
	stateTier2MaxAmount = 0.0
	Enable stateTier2MaxAmount 
	Set focus to stateTier2MaxAmount
	stateTier3MaxAmount set to *MAX
	stateUseTier3Rate 0.0
	Enable stateUseTier3Rate 


StateSplitAmount
	Enter a value, 
		Value is copied to to stateTier2MinAmount
		Disable stateTier2MinAmount


State_SPLIT_AMOUNT (tier1MaxVal)
	*MAX == 999999999
	If 999999999 {
		Set State_split_amount = *MAX
		disable state_split_amount
		StateTier1Max checkbox checked
		enable Tier2minTextField
		stateTier3UseRate value to NOT_APPLICABLE/""
		stateTier3Max value to "*MAX"
	} 0 {
		stateTier1MaxTextField = 0
		enable stateTier2MinTextFIeld 
	} else {
		stateTier1Max = whatever it needs to be
		if (tier2Maxval != MAX_AMOUNT) {
			stateTier3UseRateTextField = state_use_tier3_rate
			stateTier3MaxTextField = *MAX
	}
		

State_Tier2_max_amount
	*MAX == 999999999
	if (state_tier2_max_amount == MAX_AMOUNT) {
		stateTier2maxamount = *MAX
		disable stateTier2Max
		stateTier2Max checked
		stateTier3UseRate = to NOT_APPLICABLE/""
		disabled stateTier3UseRate
	}

Combined Use Rate
m_stateTier1UseRateTextField
m_countyUseRateTextField
m_countyLocalUseRateTextField
m_cityUseRateTextField
m_cityLocalUseRateTextField

Combined Sales Rate
m_stateSalesRateTextField
m_countySalesRateTextField
m_countyLocalSalesRateTextField
m_citySalesRateTextField
m_cityLocalSalesRateTextField





