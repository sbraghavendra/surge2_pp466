<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<script type='text/javascript' src='../scripts/NCSTSActionButton.js'> </script>
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[

function resetStateDetail()
{ 
    document.getElementById("jurisdictionViewForm:stateMenuMainDetail").value = "";
}

function enableDisableNexus(source, target) {
	if(source && source.value.length > 0) {
		document.getElementById(target).disabled = false;
	}
	else {
		document.getElementById(target).disabled = true;
	}
}

function setFocusToStj1Nexus() { document.getElementById("jurisdictionViewForm:stj1Nexus").focus();} 
function setFocusToStj2Nexus() { document.getElementById("jurisdictionViewForm:stj2Nexus").focus();} 
function setFocusToStj3Nexus() { document.getElementById("jurisdictionViewForm:stj3Nexus").focus();} 
function setFocusToStj4Nexus() { document.getElementById("jurisdictionViewForm:stj4Nexus").focus();} 
function setFocusToStj5Nexus() { document.getElementById("jurisdictionViewForm:stj5Nexus").focus();} 

//]]>
</script>	
</ui:define>

<ui:define name="body">
<h:form id="jurisdictionViewForm">
<h1><h:graphicImage id="imgTaxRatesMaintenance" alt="Tax Rates Maintenance" value="/images/headers/hdr-tax-rates-maintenance.png"/></h1>
<h:inputHidden  value="#{TaxJurisdictionBean.jurisdictionInUseMessage}"/>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="25%" align="right"><a4j:outputPanel id="msg"><h:messages id="error41" errorClass="error" /></a4j:outputPanel></td>
			<td align="left" style="padding-left:3px;"><h:commandLink id="displaymorebtn" style="color:blue;font-size:9pt;font-weight:bold;text-decoration:underline;" action="#{TaxJurisdictionBean.okActionButton}" value="Click here for more information." rendered="#{!TaxJurisdictionBean.displayOkButton and TaxJurisdictionBean.isDeleteAction}" /></td>
		</tr>
		</table>
	</div>
	<div id="table-one-content">
		
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td colspan="10"><h:outputText id="headerText" styleClass="headerText" value="#{TaxJurisdictionBean.currentAction.actionText}" /></td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</thead>
			
			<tr>
			<th colspan="12" style="color:red;text-align:right;padding-right: 30px;"><h:outputText value="Copy Rates?:"  rendered="#{TaxJurisdictionBean.currentAction.actionText =='Copy/Add a Jurisdiction'}"></h:outputText>
			<h:selectBooleanCheckbox id="chkCopyRules" value="#{TaxJurisdictionBean.selectedJurisdiction.copyRatesFlag}"
				 rendered="#{TaxJurisdictionBean.currentAction.actionText =='Copy/Add a Jurisdiction'}"  styleClass="check"  >
				   <a4j:support event="onclick"   ajaxSingle="true"  reRender="okAddTaxrate" />
		    </h:selectBooleanCheckbox>
               </th>
			</tr>
			
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Jurisdiction ID:</td>
				<td>
					<h:inputText 
						disabled="#{TaxJurisdictionBean.currentActionMap['JURISDICTIONID'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['JURISDICTIONID']}"
						id="jurisdictionId"
						value="#{TaxJurisdictionBean.editJurisdiction.jurisdictionIdString}" 
						onkeypress="return onlyIntegerValue(event);" 
						label="Jurisdiction ID">
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td colspan="4">&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Description:</td>
				<td colspan="7">
					<h:inputText id="description"
						value="#{TaxJurisdictionBean.editJurisdiction.description}"
						disabled="#{TaxJurisdictionBean.currentActionMap['DESCRIPTION'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['DESCRIPTION']}"
						size="#{TaxJurisdictionBean.fieldSizeMap['DESCRIPTION']}" />
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Effective Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{TaxJurisdictionBean.currentActionMap['EFFECTIVEDATE'].disabled}"
						value="#{TaxJurisdictionBean.editJurisdiction.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="effDate" />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Expiration Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="false" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{TaxJurisdictionBean.currentActionMap['EXPIRATIONDATE'].disabled}"
						value="#{TaxJurisdictionBean.editJurisdiction.expirationDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="expiryDate" />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>GeoCode:</td>
				<td>
				
				<h:inputText id="geocode" label="Geocode"
											value="#{TaxJurisdictionBean.editJurisdiction.geocode}"
											disabled="#{TaxJurisdictionBean.currentActionMap['GEOCODE'].disabled}"
											maxlength="#{TaxJurisdictionBean.fieldSizeMap['GEOCODE']}"
											onkeypress="upperCaseInputTextKey(event,this)"
											onblur="upperCaseInputText(this)">
										</h:inputText>
									 
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Client GeoCode:</td>
				<td>
					<h:inputText id="clientGeocode"
						value="#{TaxJurisdictionBean.editJurisdiction.clientGeocode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['CLIENTGEOCODE'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['CLIENTGEOCODE']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Comp. GeoCode:</td>
				<td>
					<h:inputText id="compGeocode"
						value="#{TaxJurisdictionBean.editJurisdiction.compGeocode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['COMPGEOCODE'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['COMPGEOCODE']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
					</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Country:</td>
				<td>
					<h:selectOneMenu id="countryMenuDetail" label="State"
						value="#{TaxJurisdictionBean.searchCountryDetail}"
						immediate="true"
						valueChangeListener="#{TaxJurisdictionBean.searchCountryDetailChanged}"
						disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRY'].disabled}"
						onchange="resetStateDetail();submit();"
						
						 >
				
						<f:selectItems value="#{TaxJurisdictionBean.countryMenuItemsDetail}" />

					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="countryNexus" value="#{TaxJurisdictionBean.editJurisdiction.countryNexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['COUNTRYNEXUSINDCODE'].disabled}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>State:</td>
				<td>
					<h:selectOneMenu id="stateMenuMainDetail" label="State"
						value="#{TaxJurisdictionBean.searchstateDetail}"
						valueChangeListener="#{TaxJurisdictionBean.vclStateMenu}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STATE'].disabled}" >
				
						<f:selectItems value="#{TaxJurisdictionBean.stateMenuItemsDetail}" />
						<a4j:support id="stateMenuId" event="onchange" reRender="measureTypeCodeMenu" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stateNexus" value="#{TaxJurisdictionBean.editJurisdiction.stateNexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STATENEXUSINDCODE'].disabled}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>County:</td>
				<td>
					<h:inputText id="county" label="County"
						value="#{TaxJurisdictionBean.editJurisdiction.county}"
						disabled="#{TaxJurisdictionBean.currentActionMap['COUNTY'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['COUNTY']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
						 />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="countyNexus" value="#{TaxJurisdictionBean.editJurisdiction.countyNexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['COUNTYNEXUSINDCODE'].disabled}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>City:</td>
				<td>
					<h:inputText id="city" label="City"
						value="#{TaxJurisdictionBean.editJurisdiction.city}"
						disabled="#{TaxJurisdictionBean.currentActionMap['CITY'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['CITY']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
						 />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="cityNexus" value="#{TaxJurisdictionBean.editJurisdiction.cityNexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['CITYNEXUSINDCODE'].disabled}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Zip:</td>
				<td>
					<h:inputText id="zip" label="Zip"
						value="#{TaxJurisdictionBean.editJurisdiction.zip}"
						disabled="#{TaxJurisdictionBean.currentActionMap['ZIP'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['ZIP']}"
						size="#{TaxJurisdictionBean.fieldSizeMap['ZIP']}"
						 />
					<h:outputText id="dash" value=" - " />
					<h:inputText id="zipplus4"
						value="#{TaxJurisdictionBean.editJurisdiction.zipplus4}"
						disabled="#{TaxJurisdictionBean.currentActionMap['ZIPPLUS4'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['ZIPPLUS4']}"
						size="#{TaxJurisdictionBean.fieldSizeMap['ZIPPLUS4']}" />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>In/Out:</td>
				<td>
					<h:selectOneMenu id="inOutMenu"
						binding="#{TaxJurisdictionBean.inOutMenu}"
						value="#{TaxJurisdictionBean.editJurisdiction.inOut}"
						disabled="#{TaxJurisdictionBean.currentActionMap['INOUT'].disabled}">
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>STJ 1:</td>
				<td>
					<h:inputText id="stj1" label="STJ1"
						immediate="true"
						value="#{TaxJurisdictionBean.editJurisdiction.stj1Name}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ1NAME'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ1NAME']}"
						onkeypress="upperCaseInputTextKey(event,this)" 		
						valueChangeListener="#{TaxJurisdictionBean.stjNameChange}"
					>
					<a4j:support event="onchange" oncomplete="upperCaseInputText(this);setFocusToStj1Nexus();" reRender="stj1Nexus,stj1LocalType" />
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stj1Nexus" value="#{TaxJurisdictionBean.editJurisdiction.stj1NexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ1NEXUSINDCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Local Type:</td>
				<td>
					<h:selectOneMenu id="stj1LocalType" value="#{TaxJurisdictionBean.editJurisdiction.stj1LocalCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ1LOCALCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj1Name}">
						<f:selectItems value="#{TaxJurisdictionBean.countyLocalItems}" />
					</h:selectOneMenu>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>STJ 2:</td>
				<td>
					<h:inputText id="stj2" label="STJ2"
						value="#{TaxJurisdictionBean.editJurisdiction.stj2Name}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ2NAME'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ2NAME']}"
						onkeypress="upperCaseInputTextKey(event,this)" 
						valueChangeListener="#{TaxJurisdictionBean.stjNameChange}"
					>
					<a4j:support event="onchange" oncomplete="upperCaseInputText(this);setFocusToStj2Nexus();" reRender="stj2Nexus,stj2LocalType" />
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stj2Nexus" value="#{TaxJurisdictionBean.editJurisdiction.stj2NexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ2NEXUSINDCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj2Name}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Local Type:</td>
				<td>
					<h:selectOneMenu id="stj2LocalType" value="#{TaxJurisdictionBean.editJurisdiction.stj2LocalCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ2LOCALCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj2Name}">
						<f:selectItems value="#{TaxJurisdictionBean.countyLocalItems}" />
					</h:selectOneMenu>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>STJ 3:</td>
				<td>
					<h:inputText id="stj3" label="STJ3"
						value="#{TaxJurisdictionBean.editJurisdiction.stj3Name}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ3NAME'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ3NAME']}"
						onkeypress="upperCaseInputTextKey(event,this)"
						valueChangeListener="#{TaxJurisdictionBean.stjNameChange}"
					>
					<a4j:support event="onchange" oncomplete="upperCaseInputText(this);setFocusToStj3Nexus();" reRender="stj3Nexus,stj3LocalType" />
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stj3Nexus" value="#{TaxJurisdictionBean.editJurisdiction.stj3NexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ3NEXUSINDCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj3Name}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Local Type:</td>
				<td>
					<h:selectOneMenu id="stj3LocalType" value="#{TaxJurisdictionBean.editJurisdiction.stj3LocalCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ3LOCALCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj3Name}">
						<f:selectItems value="#{TaxJurisdictionBean.countyLocalItems}" />
					</h:selectOneMenu>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>STJ 4:</td>
				<td>
					<h:inputText id="stj4" label="STJ4"
						value="#{TaxJurisdictionBean.editJurisdiction.stj4Name}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ4NAME'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ4NAME']}"
						onkeypress="upperCaseInputTextKey(event,this)" 
						valueChangeListener="#{TaxJurisdictionBean.stjNameChange}"
					>
					<a4j:support event="onchange" oncomplete="upperCaseInputText(this);setFocusToStj4Nexus();" reRender="stj4Nexus,stj4LocalType" />
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stj4Nexus" value="#{TaxJurisdictionBean.editJurisdiction.stj4NexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ4NEXUSINDCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj4Name}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Local Type:</td>
				<td>
					<h:selectOneMenu id="stj4LocalType" value="#{TaxJurisdictionBean.editJurisdiction.stj4LocalCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ4LOCALCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj4Name}">
						<f:selectItems value="#{TaxJurisdictionBean.countyLocalItems}" />
					</h:selectOneMenu>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>STJ 5:</td>
				<td>
					<h:inputText id="stj5" label="STJ5"
						value="#{TaxJurisdictionBean.editJurisdiction.stj5Name}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ5NAME'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['STJ5NAME']}"
						onkeypress="upperCaseInputTextKey(event,this)"
						valueChangeListener="#{TaxJurisdictionBean.stjNameChange}"
					 >
					<a4j:support event="onchange" oncomplete="upperCaseInputText(this);setFocusToStj5Nexus();" reRender="stj5Nexus,stj5LocalType" />
					</h:inputText>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Nexus:</td>
				<td>
					<h:selectOneMenu id="stj5Nexus" value="#{TaxJurisdictionBean.editJurisdiction.stj5NexusindCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ5NEXUSINDCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj5Name}">
						<f:selectItems value="#{TaxJurisdictionBean.nexusIndItems}" />
					</h:selectOneMenu>
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Local Type:</td>
				<td>
					<h:selectOneMenu id="stj5LocalType" value="#{TaxJurisdictionBean.editJurisdiction.stj5LocalCode}"
						disabled="#{TaxJurisdictionBean.currentActionMap['STJ5LOCALCODE'].disabled or empty TaxJurisdictionBean.editJurisdiction.stj5Name}">
						<f:selectItems value="#{TaxJurisdictionBean.countyLocalItems}" />
					</h:selectOneMenu>
				</td>
				<td style="width:50%;">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Reporting City:</td>
				<td>
					<h:inputText id="reportingCity" label="Reporting City"
						value="#{TaxJurisdictionBean.editJurisdiction.reportingCity}"
						disabled="#{TaxJurisdictionBean.currentActionMap['REPORTINGCITY'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['REPORTINGCITY']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
						 />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>Reporting City FIPS:</td>
				<td>
					<h:inputText id="reportingCityFips" label="Reporting City FIPS"
						value="#{TaxJurisdictionBean.editJurisdiction.reportingCityFips}"
						disabled="#{TaxJurisdictionBean.currentActionMap['REPORTINGCITYFIPS'].disabled}"
						maxlength="#{TaxJurisdictionBean.fieldSizeMap['REPORTINGCITYFIPS']}"
						onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
						 />
				</td>
				<td class="column-spacer">&#160;</td>
				<td>&#160;</td>
				<td>&#160;</td>
				<td style="width:50%;">&#160;</td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Last Update User ID:</td>
				<td colspan="6" class="column-description">
					<h:inputText style="width:650px;" disabled="true" id="tjUpdateUserId"
									value="#{TaxJurisdictionBean.editJurisdiction.updateUserId}" />
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td colspan="6" class="column-description">
					<h:inputText style="width:650px;" disabled="true" id="tjDpdateTimestamp"
									value="#{TaxJurisdictionBean.editJurisdiction.updateTimestamp}">
									<f:converter converterId="dateTime"/>
								</h:inputText>
				</td>
				<td>&#160;</td>
			</tr>
		</table>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok-add-taxrate"><h:commandLink id="okAddTaxrate" action="#{TaxJurisdictionBean.okAndAddTaxRate}" disabled="#{TaxJurisdictionBean.displayOkAndAddTaxRate}" /></li>
		<li class="ok"><h:commandLink id="t1ok" action="#{TaxJurisdictionBean.okActionButton}" rendered="#{TaxJurisdictionBean.displayOkButton}" /></li>
		<li class="ok"><h:commandLink id="btnOk" immediate="true" action="#{TaxJurisdictionBean.cancelActionButton}" rendered="#{!TaxJurisdictionBean.showCancelButton}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{TaxJurisdictionBean.cancelActionButton}" rendered="#{TaxJurisdictionBean.showCancelButton}" /></li>
	</ul>
	</div>
	</div>
</div>

		<div>
			<!-- Required to create the additional room needed for the footer  -->
			<rich:spacer height="25px" style="layout:block;"/>
		</div>
</h:form>
	<h:form id="messageForm">
<!--  minHeight="200" minWidth="200"  -->
		<rich:modalPanel id="messagesModalPanel"  zindex="2000"
			autosized="true" moveable="true" binding="#{TaxJurisdictionBean.messagesModalPanel}">
			<f:facet name="header">
				<h:outputText id="error495" value="Errors" />
			</f:facet>
			<f:facet name="controls">
				<h:graphicImage id="img498" value="/images/icons/close.png"
					style="cursor:pointer" onclick="Richfaces.hideModalPanel('messagesModalPanel')" />
			</f:facet>
			<p><h:dataTable id="formMessages"
				binding="#{TaxJurisdictionBean.formMessagesDataTable}"
				var="formMessage">
				<h:column id="Message503">
					<h:outputText id="errorMsg508" styleClass="errorMsg" value="#{formMessage}" />
				</h:column>
			</h:dataTable></p>
			<p>&#160;</p>
			<h:commandButton id="CloseMP"
				onclick="javascript:Richfaces.hideModalPanel('messagesModalPanel'); return false;"
				styleClass="closebutton" value="Close" />
		</rich:modalPanel>
	</h:form>
</ui:define>
</ui:composition>
</html>

