<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/management.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('dbPropertiesForm:dbPropertiesTable', 'dbPropertiesTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="dbPropertiesTableRowIndex" value="#{dbPropertiesBean.selectedDbPropertiesRowIndex}"/>
<h:form id="dbPropertiesForm">

<h1><h:graphicImage id="securitydatabaseMainImage" alt="Security Database" value="/images/headers/hdr-security-database.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	
	<div class="scrollContainer">
		<div class="scrollInner">
         <rich:dataTable rowClasses="odd-row,even-row" 
				id="dbPropertiesTable" styleClass="Tabruler" headerClass="header"																
				value="#{dbPropertiesBean.dbPropertiesList}" var="dataRow" >
	             
	         	<a4j:support id="clk" event="onRowClick" 
					onsubmit="selectRow('dbPropertiesForm:dbPropertiesTable', this);"
			       	actionListener="#{dbPropertiesBean.selectedRowChanged}" reRender="addBtn,copyaddBtn,updateBtn,deleteBtn,mapBtn"/>	
	             														
				<rich:column id="dbIdColumn" sortBy="#{dataRow.dbId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Database ID" />
					</f:facet>
					<h:outputText value="#{dataRow.dbId}"/>
				</rich:column>
				
				<rich:column id="userNameColumn" width="100px" sortBy="#{dataRow.userName}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="User Name"/>
					</f:facet>
					<h:outputText value="#{dataRow.userName}" />
				</rich:column>		
				
				<rich:column id="passwordColumn" width="100px" sortBy="#{dataRow.password}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Password" />
					</f:facet>
					<h:outputText value="#{dataRow.password}"/>
				</rich:column>
				
				<rich:column id="databaseNameColumn" width="200px" sortBy="#{dataRow.databaseName}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Database Name" />
					</f:facet>
					<h:outputText value="#{dataRow.databaseName}"/>
				</rich:column>	
				
				<rich:column id="driverClassNameColumn" width="200px" sortBy="#{dataRow.driverClassName}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Driver Class Name" />
					</f:facet>
					<h:outputText value="#{dataRow.driverClassName}"/>
				</rich:column>	
				
				<rich:column id="urlColumn" width="250px" sortBy="#{dataRow.url}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Url" />
					</f:facet>
					<h:outputText value="#{dataRow.url}"/>
				</rich:column>
				
			</rich:dataTable>
		</div>	
	</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="addBtn" action="#{dbPropertiesBean.displayAddDbPropertiesAction}" /></li>
		<li class="copy-add"><h:commandLink id="copyaddBtn" disabled="#{!dbPropertiesBean.showDbPropertiesButtons}" action="#{dbPropertiesBean.displayCopyAddDbPropertiesAction}" /></li>
		<li class="update"><h:commandLink id="updateBtn" disabled="#{!dbPropertiesBean.showDbPropertiesButtons}" action="#{dbPropertiesBean.displayUpdateDbPropertiesAction}" /></li>
		<li class="delete"><h:commandLink id="deleteBtn" disabled="#{!dbPropertiesBean.showDbPropertiesButtons}" action="#{dbPropertiesBean.displayDeleteDbPropertiesAction}" /></li>
		<li class="map"><h:commandLink id="mapBtn" disabled="#{!dbPropertiesBean.showDbPropertiesButtons}" action="#{dbPropertiesBean.displayMapDbPropertiesAction}" /></li>
	</ul>
	</div>
	</div>
</div>

</h:form>


</ui:define>
</ui:composition>
</html>