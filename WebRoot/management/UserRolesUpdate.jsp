<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/management.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmUserRolesUpdate">
<h1><h:graphicImage id="securityRolesUpdateImage" alt="Security Roles Update" value="/images/headers/hdr-security-role.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{userRolesBean.actionUserRolesText}"/> a Role</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Role ID:</td>
				<td style="width:700px;">        
				<h:inputText id="roleIdText" value="#{userRolesBean.isUserRolesAddAction ? '*New' : userRolesBean.userRolesDTOWork.roleId}" 
				         disabled="true"			
				         style="width:650px;" />               
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Role Name:</td>
				<td style="width:700px;">
				<h:inputText id="roleNameText" value="#{userRolesBean.userRolesDTOWork.roleName}" 	
						disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 
				         style="width:650px;" />
                </td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Admin:</td>
				<td style="width:700px;">     
				<h:selectBooleanCheckbox id="adminFlag"
                                  styleClass="check"
                                  disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 
                                  value="#{userRolesBean.userRolesDTOWork.adminDisplay}"  />         
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td># of Password History:</td>
				<td style="width:700px;">
				<h:inputText id="passwordHistoryText" value="#{userRolesBean.userRolesDTOWork.passwordHistory}" 		
						disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td># of Invalid Attempts:</td>
				<td style="width:700px;">
				<h:inputText id="invalidAttemptsText" value="#{userRolesBean.userRolesDTOWork.invalidAttempts}" 	
						disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 	
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td># of Notification Days:</td>
				<td style="width:700px;">
				<h:inputText id="notificationDaysText" value="#{userRolesBean.userRolesDTOWork.notificationDays}" 	
						disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 	
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td># of Expiration Days:</td>
				<td style="width:700px;">
				<h:inputText id="expirationDaysText" value="#{userRolesBean.userRolesDTOWork.expirationDays}" 		
						disabled="#{userRolesBean.isUserRolesViewAction or userRolesBean.isUserRolesDeleteAction}" 
				         style="width:650px;" />
                </td>
			</tr>
			
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" action="#{userRolesBean.okUserRolesAction}" /></li>
		<li class="cancel"><h:commandLink id="cancelBtn" disabled="#{userRolesBean.isUserRolesViewAction}" immediate="true" action="#{userRolesBean.cancelUserRolesAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>