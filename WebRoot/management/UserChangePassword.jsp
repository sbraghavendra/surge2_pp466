<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/changepassword.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmChangePassword">
<div id="container">
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Change Password</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">Current Password</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Current Password:</td>
				<td style="width:700px;">
				<h:inputSecret 
				         id="pwdControl"  
				         redisplay="true"
                         size="4"
                         binding="#{userSecurityBean.pwdControl}"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr><td style="width:50px;" colspan="3">&#160;</td></tr>	
			<tr><th colspan="3">Change Password</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>New Password:</td>
				<td>
				<h:inputSecret
				         id="newPwdControl"
				         redisplay="true"
                         size="4"
                         binding="#{userSecurityBean.newPwdControl}"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>ReEnter New Password:</td>
				<td>
				<h:inputSecret
				         id="confirmPwdControl" 
				         redisplay="true"
                         size="4"
                         binding="#{userSecurityBean.confirmPwdControl}"				
				         style="width:650px;" />				
                </td>
			</tr>
			<tr><td style="width:50px;" colspan="3">&#160;</td></tr>	
		</tbody>
	</table>
    <h:outputLabel id="message" style="color:red" value="#{userSecurityBean.pwdChangeMessage}"/>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnChangePassword" action="#{userSecurityBean.changePasswordAction}" rendered="#{!userSecurityBean.donePasswordFlag}" /></li>
		<li class="close2"><h:commandLink id="btnDonePassword" action="#{loginBean.donePasswordAction}" rendered="#{userSecurityBean.donePasswordFlag}" /></li>
		<li class="cancel"><h:commandLink id="btnCancelPassword" action="#{loginBean.cancelchangePasswordAction}" rendered="#{!userSecurityBean.donePasswordFlag}" /></li>
	</ul>
	</div>
	</div>
</div>



</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>