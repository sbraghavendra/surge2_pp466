<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/management.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmUserSendPassword">
<h1><h:graphicImage id="sendpasswordImage" value="/images/headers/hdr-security-user.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{userSecurityBean.actionUserSecurityText}"/> User's Current Password</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User ID:</td>
				<td style="width:700px;">
				<h:inputText id="userIdText" value="#{userSecurityBean.userSecurityDTOWork.userId}" 
				         disabled="true"			
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Name:</td>
				<td style="width:700px;">
				<h:inputText id="userNameText" value="#{userSecurityBean.userSecurityDTOWork.userName}" 
				         disabled="true"	
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td><h:outputText value="#{userSecurityBean.isUserLockPasswordAction ? 'Email' : 'An Email Notification Will Be Sent To'}"/></td>
				<td style="width:700px;">
				<h:inputText id="emailText" value="#{userSecurityBean.userSecurityDTOWork.email}" 
				         disabled="true"			
				         style="width:650px;" />
                </td>
			</tr>
			
			<h:panelGroup id="passwordPanelGroup" >
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User's Current Encrypted Password:</td>
				<td style="width:700px;">
				<h:inputText id="passwordText" value="#{userSecurityBean.userSecurityDTOWork.password}" 
				         disabled="true"			
				         style="width:650px;" />
                </td>
			</tr>
			</h:panelGroup>
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" action="#{userSecurityBean.okSendPasswordAction}" /></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{userSecurityBean.cancelUserSecurityActionAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>