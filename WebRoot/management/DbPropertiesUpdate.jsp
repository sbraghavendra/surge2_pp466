<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/management.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecurityDatabaseUpdate">
<h1><h:graphicImage id="securitydatabaseUpdateImage" alt="SecurityDatabaseUpdate" value="/images/headers/hdr-security-database.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{dbPropertiesBean.actionDbPropertiesText}"/> a Database Property</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Database ID:</td>
				<td style="width:700px;">
				<h:inputText id="dbIdText" 
						value="#{dbPropertiesBean.isAddAction ? '*New' : dbPropertiesBean.dbPropertiesDTOWork.dbId}" 
				         disabled="true"			
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Database Name:</td>
				<td style="width:700px;">
				<h:inputText id="databaseNameText" value="#{dbPropertiesBean.dbPropertiesDTOWork.databaseName}" 
						disabled="#{dbPropertiesBean.isDeleteAction}" 		
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>User Name:</td>
				<td style="width:700px;">
				<h:inputText id="userNameText" value="#{dbPropertiesBean.dbPropertiesDTOWork.userName}" 
				         disabled="#{dbPropertiesBean.isDeleteAction}" 		
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Password:</td>
				<td style="width:700px;">
				<h:inputText id="passwordText" value="#{dbPropertiesBean.dbPropertiesDTOWork.password}"
						disabled="#{dbPropertiesBean.isDeleteAction}"  		
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Driver Class Name:</td>
				<td style="width:700px;">
				<h:inputText id="driverClassNameText" value="#{dbPropertiesBean.dbPropertiesDTOWork.driverClassName}" 	
						disabled="#{dbPropertiesBean.isDeleteAction}" 	
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Url:</td>
				<td style="width:700px;">
				<h:inputText id="urlText" value="#{dbPropertiesBean.dbPropertiesDTOWork.url}" 
						 disabled="#{dbPropertiesBean.isDeleteAction}" 			
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Verification:</td>
				<td style="width:700px;">
				<h:inputText id="verificationText" value="#{dbPropertiesBean.verificationStatus}" 
						 disabled="true" 			
				         style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>&#160;</td>
				<td style="width:700px;">
				
				<!--
				<h:outputText value="Warning: Any modifications to these database properties require a Manual Restart of the PinPoint Server." 
				id="warningId" style="color: red"/>
				-->
                </td>
			</tr>

		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="verify"><h:commandLink id="applyBtn" action="#{dbPropertiesBean.verifyDbPropertiesAction}" /></li>
		<li class="ok"><h:commandLink id="okBtn" action="#{dbPropertiesBean.okDbPropertiesAction}" /></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{dbPropertiesBean.cancelDbPropertiesAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>