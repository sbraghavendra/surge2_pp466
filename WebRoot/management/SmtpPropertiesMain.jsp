<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/management.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('smtpPropertiesForm:smtpPropertiesTable', 'smtpPropertiesTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="smtpPropertiesTableRowIndex" value="#{smtpPropertiesBean.selectedSmtpPropertiesRowIndex}"/>
<h:form id="smtpPropertiesForm">

<h1><h:graphicImage id="securitysmtpMainImage" alt="Security SMTP" value="/images/headers/hdr-security-smtp.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	
	<div class="scrollContainer">
		<div class="scrollInner">
         <rich:dataTable rowClasses="odd-row,even-row" 
				id="smtpPropertiesTable" styleClass="Tabruler" headerClass="header"																
				value="#{smtpPropertiesBean.smtpPropertiesList}" var="dataRow" >
	             
	         	<a4j:support id="clk" event="onRowClick" 
					onsubmit="selectRow('smtpPropertiesForm:smtpPropertiesTable', this);"
			       	actionListener="#{smtpPropertiesBean.selectedRowChanged}" reRender="updateBtn"/>	
	             														
				<rich:column id="idColumn" sortBy="#{dataRow.id}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="ID" />
					</f:facet>
					<h:outputText value="#{dataRow.id}"/>
				</rich:column>
				
				<rich:column id="actionColumn" width="140px" sortBy="#{dataRow.action}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Action"/>
					</f:facet>
					<h:outputText value="#{dataRow.action}" />
				</rich:column>		
				
				<rich:column id="smtpSubjectColumn" sortBy="#{dataRow.smtpSubject}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Smtp Subject" />
					</f:facet>
					<h:outputText value="#{dataRow.smtpSubject}"/>
				</rich:column>
				
				<rich:column id="smtpFromColumn" sortBy="#{dataRow.smtpFrom}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Smtp From" />
					</f:facet>
					<h:outputText value="#{dataRow.smtpFrom}"/>
				</rich:column>	
				
				<rich:column id="smtpContentColumn" sortBy="#{dataRow.smtpContent}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Smtp Content" />
					</f:facet>
					<h:outputText value="#{dataRow.smtpContent}"/>
				</rich:column>	
				
				<rich:column id="smtpHostNameColumn" sortBy="#{dataRow.smtpHostName}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Smtp Host Name" />
					</f:facet>
					<h:outputText value="#{dataRow.smtpHostName}"/>
				</rich:column>
				
				<rich:column id="smtpPortColumn" sortBy="#{dataRow.smtpPort}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Smtp Port" />
					</f:facet>
					<h:outputText value="#{dataRow.smtpPort}"/>
				</rich:column>		
				
			</rich:dataTable>
		</div>	
	</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
<!--	<li class="add"><h:commandLink id="addBtn" action="#{smtpPropertiesBean.displayAddSmtpPropertiesAction}" /></li>  -->
		<li class="update"><h:commandLink id="updateBtn" disabled="#{!smtpPropertiesBean.showSmtpPropertiesButtons}" action="#{smtpPropertiesBean.displayUpdateSmtpPropertiesAction}" /></li>		
<!--	<li class="delete"><h:commandLink id="deleteBtn" disabled="#{!smtpPropertiesBean.showSmtpPropertiesButtons}" action="#{smtpPropertiesBean.displayDeleteSmtpPropertiesAction}" /></li>  -->
						
	</ul>
	</div>
	</div>
</div>

</h:form>


</ui:define>
</ui:composition>
</html>