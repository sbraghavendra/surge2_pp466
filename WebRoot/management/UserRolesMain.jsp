<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/management.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('userRoleForm:userRolesTable', 'userRolesTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="userRolesTableRowIndex" value="#{userRolesBean.selectedUserRolesRowIndex}"/>
<h:form id="userRoleForm">

<h1><h:graphicImage id="securityRolesImage" alt="Security Roles" value="/images/headers/hdr-security-role.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	
	<div class="scrollContainer">
		<div class="scrollInner">
         <rich:dataTable rowClasses="odd-row,even-row" 
				id="userRolesTable" styleClass="Tabruler" headerClass="header"																
				value="#{userRolesBean.userRolesList}" var="dataRow" >
	             
	         	<a4j:support id="clk" event="onRowClick" 
					onsubmit="selectRow('userRoleForm:userRolesTable', this);"
			       	actionListener="#{userRolesBean.selectedRowChanged}" reRender="addBtn,updateBtn,viewBtn,deleteBtn"/>	
	             														
				<rich:column id="roleIdColumn" sortBy="#{dataRow.roleId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role ID" />
					</f:facet>
					<h:outputText value="#{dataRow.roleId}"/>
				</rich:column>
				
				<rich:column id="roleNameColumn" width="200px" sortBy="#{dataRow.roleName}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role Name"/>
					</f:facet>
					<h:outputText value="#{dataRow.roleName}" />
				</rich:column>		
				
				<rich:column id="adminColumn" sortBy="#{dataRow.adminDisplay}" style="text-align: center;" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Admin?"/>
					</f:facet>
					<h:selectBooleanCheckbox id="adminFlag"
                                  styleClass="check"
                                  value="#{dataRow.adminDisplay}" disabled="#{true}"  />
                </rich:column>
				
				<rich:column id="passwordHistoryColumn" sortBy="#{dataRow.passwordHistory}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Password History" />
					</f:facet>
					<h:outputText value="#{dataRow.passwordHistory}"/>
				</rich:column>
				
				<rich:column id="invalidAttemptsColumn" sortBy="#{dataRow.invalidAttempts}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Invalid Attempts" />
					</f:facet>
					<h:outputText value="#{dataRow.invalidAttempts}"/>
				</rich:column>	
				
				<rich:column id="notificationDaysColumn" sortBy="#{dataRow.notificationDays}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Notification Days" />
					</f:facet>
					<h:outputText value="#{dataRow.notificationDays}"/>
				</rich:column>	
				
				<rich:column id="expirationDaysColumn" sortBy="#{dataRow.expirationDays}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Expiration Days" />
					</f:facet>
					<h:outputText value="#{dataRow.expirationDays}"/>
				</rich:column>		
				
			</rich:dataTable>
		</div>	
	</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">		
		<li class="add"><h:commandLink id="addBtn" action="#{userRolesBean.displayAddUserRolesAction}" /></li>
		<li class="update"><h:commandLink id="updateBtn" disabled="#{!userRolesBean.showUserRolesButtons}" action="#{userRolesBean.displayUpdateUserRolesAction}" /></li>	
		<li class="view"><h:commandLink id="viewBtn" disabled="#{!userRolesBean.showUserRolesButtons}" action="#{userRolesBean.displayViewUserRolesAction}" /></li>		
		<li class="delete"><h:commandLink id="deleteBtn" disabled="#{!userRolesBean.showUserRolesButtons}" action="#{userRolesBean.displayDeleteUserRolesAction}" /></li>
						
	</ul>
	</div>
	</div>
</div>

</h:form>


</ui:define>
</ui:composition>
</html>