<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/management.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('userSecurityForm:userSecurityTable', 'userSecurityTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="userSecurityTableRowIndex" value="#{userSecurityBean.selectedUserSecurityRowIndex}"/>
<h:form id="userSecurityForm">

<h1><h:graphicImage id="securityUsersMainImage" alt="User Security" value="/images/headers/hdr-security-user.gif"/></h1>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	
	<div class="scrollContainer">
		<div class="scrollInner">
         <rich:dataTable rowClasses="odd-row,even-row" 
				id="userSecurityTable" styleClass="Tabruler" headerClass="header"																
				value="#{userSecurityBean.userSecurityList}" var="dataRow" >
	             
	         	<a4j:support id="clk" event="onRowClick" 
					onsubmit="selectRow('userSecurityForm:userSecurityTable', this);"
			       	actionListener="#{userSecurityBean.selectedRowChanged}" reRender="updateBtn,viewBtn,deleteBtn,unlockBtn,lockBtn,resetBtn,sendBtn,mapBtn"/>	
	             														
				<rich:column id="userIdColumn" sortBy="#{dataRow.userId}">
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="ID" />
					</f:facet>
					<h:outputText value="#{dataRow.userId}"/>
				</rich:column>
				
				<rich:column id="userNameColumn" sortBy="#{dataRow.userName}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="User Name"/>
					</f:facet>
					<h:outputText value="#{dataRow.userName}" />
				</rich:column>	
				
				
				<rich:column id="emailColumn" sortBy="#{dataRow.email}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Email"/>
					</f:facet>
					<h:outputText value="#{dataRow.email}" />
				</rich:column>	
				
				<rich:column id="passwordColumn" sortBy="#{dataRow.password}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Password"/>
					</f:facet>
					<h:outputText value="#{dataRow.password}" />
				</rich:column>		
				
				<rich:column id="currentAttemptsColumn" sortBy="#{dataRow.currentAttempts}" style="text-align: center;" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Attempts"/>
					</f:facet>
					<h:outputText value="#{dataRow.currentAttempts}" />
				</rich:column>	
				
				<rich:column id="lockColumn" sortBy="#{dataRow.lock}" style="text-align: center;" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Locked?"/>
					</f:facet>
					<h:selectBooleanCheckbox id="lockFlag"
                                  styleClass="check"
                                  value="#{dataRow.lock == 1}" disabled="#{true}"  />
				</rich:column>	
				
				<rich:column id="changeColumn" sortBy="#{dataRow.change}" style="text-align: center;" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Change Needed?"/>
					</f:facet>
					<h:selectBooleanCheckbox id="changeFlag"
                                  styleClass="check"
                                  value="#{dataRow.change == 1}" disabled="#{true}"  />
				</rich:column>	
				
				<rich:column id="roleNameColumn" sortBy="#{dataRow.roleName}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Role"/>
					</f:facet>
					<h:outputText value="#{dataRow.roleName}" />
				</rich:column>	
				
				<rich:column id="lastUpdateDateColumn" sortBy="#{dataRow.lastUpdateDate}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Password Update Date"/>
					</f:facet>
					<h:outputText value="#{dataRow.lastUpdateDate}" />
				</rich:column>
				
				<rich:column id="createDateColumn" sortBy="#{dataRow.createDate}" >
					<f:facet name="header">
						<h:outputText styleClass="headerText" value="Create Date"/>
					</f:facet>
					<h:outputText value="#{dataRow.createDate}" />
				</rich:column>	
				
			</rich:dataTable>
		</div>	
	</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="addBtn" action="#{userSecurityBean.displayAddUserSecurityAction}" /></li>
		<li class="update"><h:commandLink id="updateBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayUpdateUserSecurityAction}" /></li>
		<li class="view"><h:commandLink id="viewBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayViewUserSecurityAction}" /></li>		
		<li class="delete"><h:commandLink id="deleteBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayDeleteUserSecurityAction}" /></li>
		
		<li class="map"><h:commandLink id="mapBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayMapUsersAction}" /></li>
		<li class="lock"><h:commandLink id="lockBtn" disabled="#{!userSecurityBean.showUserLockButtons}" action="#{userSecurityBean.displayLockUserSecurityAction}" /></li>
		<li class="unlock"><h:commandLink id="unlockBtn" disabled="#{!userSecurityBean.showUserUnLockButtons}" action="#{userSecurityBean.displayUnlockUserSecurityAction}" /></li>
		<li class="reset"><h:commandLink id="resetBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayUserResetPasswordAction}" /></li>
		<li class="send"><h:commandLink id="sendBtn" disabled="#{!userSecurityBean.showUserSecurityButtons}" action="#{userSecurityBean.displayUserSendPasswordAction}" /></li>
		<li class="refresh"><h:commandLink id="refreshBtn" action="#{userSecurityBean.refreshUserAction}" /></li>		
	</ul>
	</div>
	</div>
</div>

</h:form>


</ui:define>
</ui:composition>
</html>