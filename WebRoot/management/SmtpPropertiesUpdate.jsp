<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/management.xhtml">
<ui:define name="body">
<f:view>
<h:form id="frmSecuritySmtpUpdate">
<h1><h:graphicImage id="securitysmtpUpdateImage" alt="Security SMTP Update" value="/images/headers/hdr-security-smtp.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{smtpPropertiesBean.actionSmtpPropertiesText}"/> an SMTP Property</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>ID:</td>
				<td style="width:700px;">
				<h:inputText id="idText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.id}" 
				         disabled="true"			
				         style="width:650px;" />
                </td>
			</tr>
<!--			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Action:</td>
				<td style="width:700px;">
				<h:inputText id="actionText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.action}" 		
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 
				        style="width:650px;" />
                </td>
			</tr>
-->			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Action:</td>
				<td style="width:700px;">
				<h:inputText id="actionText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.action}" 		
						disabled="true" 
				        style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>SMTP Subject:</td>
				<td style="width:700px;">
				<h:inputText id="smtpSubjectText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.smtpSubject}" 	
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 	
				        style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>SMTP From:</td>
				<td style="width:700px;">
				<h:inputText id="smtpFromText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.smtpFrom}" 	
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 	
				        style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>SMTP Content:</td>
				<td style="width:700px;">
				<h:inputText id="smtpContentText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.smtpContent}" 		
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 
				        style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>SMTP Host Name:</td>
				<td style="width:700px;">
				<h:inputText id="smtpHostNameText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.smtpHostName}" 	
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 	
				        style="width:650px;" />
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>SMTP Port:</td>
				<td style="width:700px;">
				<h:inputText id="smtpPortText" value="#{smtpPropertiesBean.smtpPropertiesDTOWork.smtpPort}" 		
						disabled="#{smtpPropertiesBean.isSmtpDeleteAction}" 
				        style="width:650px;" />
                </td>
			</tr>
			
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" action="#{smtpPropertiesBean.okSmtpPropertiesAction}" /></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{smtpPropertiesBean.cancelSmtpPropertiesAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>