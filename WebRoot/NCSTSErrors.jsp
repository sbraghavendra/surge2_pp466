<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/landing.xhtml">

<ui:define name="body">
	<h:form id="LoginForm">
		<a4j:queue/>
		
		<div id="header">
			<div id="logo"><h:outputText binding="#{errorBean.appVersion}" value="#{errorBean.appVersionString}"><f:attribute name="appVersionString" value="PinPoint v${build.appversion} ${build.build_label}.${build.build_number} / ${build.build_timestamp}" /></h:outputText></div>
		</div>
		
		<div id="top-index" >
			<div id="table-one-index" style="height: 443px;">
				<div id="table-one-top" style="padding-left: 5px;">
					<a4j:status id="pageInfo" />     						
					<div class="error">
						ERROR
                    </div>
                    <a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
				</div>
				<div id="table-one-content" style="font-size: 14px; padding: 5px;">
					An internal error has occurred in the application.
                    Click 
                    
                    <a4j:commandLink id="contactsBtn"
					onclick="javascript:Richfaces.showModalPanel('contactInfo'); return false;"
					reRender="contactsForm,msg">
					here
					</a4j:commandLink>
                    
                    to email Error Report to support team at <a href="mailto:#{errorBean.supportEmail}?subject=#{errorBean.appVersionString} Error Report">#{errorBean.supportEmail}</a>.
                    
                    <br/><br/>
                    <h:inputTextarea cols="60" rows="20" value="#{errorBean.error}" />
                    <br/><br/>
                    <h:commandLink action="#{errorBean.backAction}">Back</h:commandLink>
				</div>
			</div>
		</div>
		
		
	</h:form>
	
	<rich:modalPanel id="contactInfo" zindex="2000" autosized="true">
		<f:facet name="header">
			<h:outputText value="Contact Details" />
		</f:facet>
		<h:form id="contactsForm">
			<h:panelGrid columns="2" width="430px" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
				<h:outputText styleClass="label" value="Your Name:" />
				<h:inputText styleClass="column-input" id="name" value="#{errorBean.name}" />
				<h:outputText styleClass="label" value="Company Name:" />
				<h:inputText styleClass="column-input" id="companyName" value="#{errorBean.companyName}" immediate="true">
					<a4j:support event="onchange" reRender="send" />
				</h:inputText>
				<h:outputText styleClass="label" value="Phone Number:" />
				<h:inputText styleClass="column-input" id="phone" value="#{errorBean.phone}" />
			</h:panelGrid>
			<h:commandButton id="cancel"
                       style="cursor:pointer"
					value="Cancel" onclick="Richfaces.hideModalPanel('contactInfo'); return false;" />
			<rich:spacer width="20px"/>
			<h:commandButton id="send"
                style="cursor:pointer"
				value="Send Error Report" action="#{errorBean.sendError}" disabled="#{errorBean.disableSend}" onclick="Richfaces.hideModalPanel('contactInfo');">
				<a4j:support event="onclick" reRender="msg"></a4j:support>
			</h:commandButton>
		</h:form>
	</rich:modalPanel>			
</ui:define>

</ui:composition>
</html>