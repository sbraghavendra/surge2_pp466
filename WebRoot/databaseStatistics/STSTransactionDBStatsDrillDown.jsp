<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('STSTrDBStatusDrillDownForm:ddDetailList', 'drillDownRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="drillDownRowIndex" value="#{databaseStatisticsBean.selectedRowDrillDownIndex}"/>
	
 <h:form id="STSTrDBStatusDrillDownForm">

<h1><h:graphicImage id="imgDatabaseStatistics" alt="DrillDown Database Statistics" value="/images/headers/hdr-database-statistics.gif" width="250" height="19" /></h1>
<div ><img src="../images/containers/STSView-transactions-open.gif" /></div><br/>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"><h:outputText styleClass="headerText" value="View Transactions for: #{databaseStatisticsBean.selectedDatabaseTransactionStatisticsDTO.title} , By: #{databaseStatisticsBean.selectedDatabaseTransactionStatisticsDTO.groupByOnDrilldown} "/></div>
	<div id="table-four-content">	

			<div class="scrollContainer">
			<div class="scrollInner" id="resize">
			
			<rich:dataTable rowClasses="odd-row,even-row" id="ddDetailList"
					width="100%"
					binding="#{databaseStatisticsBean.trDrillDownTable}"
					value="#{databaseStatisticsBean.drillDownList}" var="trStsdetail">
					
			<a4j:support event="onRowClick" onsubmit="selectRow('STSTrDBStatusDrillDownForm:ddDetailList', this);"
								 actionListener="#{databaseStatisticsBean.selectedDrillDownRowChanged}" reRender="viewTr" />
		
			<rich:column id="distCount" sortBy="#{trStsdetail.distCount}">
				<f:facet name="header">
					<h:outputText value="Count" />
				</f:facet>
				<h:outputText id="id1" value="#{trStsdetail.distCount}" >
				<f:convertNumber pattern = "#,###" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="distAmt" sortBy="#{trStsdetail.distAmt}" >
				<f:facet name="header">
					<h:outputText value="Dist.Amt" />
				</f:facet>
				<h:outputText id="id2" value="#{trStsdetail.distAmt}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="taxAmt" sortBy="#{trStsdetail.taxAmt}">
				<f:facet name="header">
					<h:outputText value="TaxAmt." />
				</f:facet>
				<h:outputText id="id3" value="#{trStsdetail.taxAmt}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="effTaxRate" sortBy="#{trStsdetail.effTaxRate}">
				<f:facet name="header">
					<h:outputText value="Eff.TaxRate" />
				</f:facet>
				<h:outputText id="id4" value="#{trStsdetail.effTaxRate}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="maxDistAmt" sortBy="#{trStsdetail.maxDistAmt}">
				<f:facet name="header">
					<h:outputText value="Max.Dist.Amt." />
				</f:facet>
				<h:outputText id="id5" value="#{trStsdetail.maxDistAmt}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="avgDistAmt" sortBy="#{trStsdetail.avgDistAmt}">
				<f:facet name="header">
					<h:outputText value="Avg.Dist.Amt." />
				</f:facet>
				<h:outputText id="id6" value="#{trStsdetail.avgDistAmt}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			</rich:dataTable>
			
			</div>
			</div>
			</div>
			<div id="table-four-bottom">
			<ul class="right">
				<li class="view"><h:commandLink id="viewTr" disabled="#{empty databaseStatisticsBean.selectedDrillDownStatistics}" action="#{databaseStatisticsBean.viewFromDrillDown}"/></li>
				<li class="saveas"><h:commandLink id="saveAs" action="#{databaseStatisticsBean.drillDownSaveAs}"/></li>
				<li class="cancel2"><h:commandLink id="cancelAction" action="transactionStatus_main" /></li>
			</ul>
			</div>

			</div>
		</div>
		
		
  </h:form>
</ui:define>

</ui:composition>
</html>