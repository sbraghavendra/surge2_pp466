<%@ page contentType="text/html;charset=windows-1251"  %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

 <table cellspacing="0" cellpadding="0" width="100%"  id="rollover2" >
		<tbody>
		<tr>
			<td align="left"  style="TextNormal" width="20%">
				<font class="TextNormal">Total Location Matrix Lines:</font>
			</td>
			<td width="20%" class="TdData">
				<h:outputText styleClass="inputClass" style="width: 150px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.total_matrix_lines}"/>
		    </td>
			<td width="20%" align="left" style="TextNormal">
				<font class="TextNormal">Active Location Matrix Lines:</font>
			</td>
			<td width="20%" class="TdData"><h:outputText styleClass="inputClass" style="width: 150px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.active_matrix_lines}"/></td>
			<td width="20%" align="left" style="TextNormal">
				<font class="TextNormal">Distinct Location Matrix Lines:</font>
			</td>
			<td width="20%" class="TdData"><h:outputText styleClass="inputClass" style="width: 150px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.distinct_matrix_lines}"/></td>
		</tr>
		<tr >
			
			<td colspan=6>
				<table width="100%" cellpadding="0" cellspacing="0" border=1 id="rollover" class="ruler">
				
				  <tr class="TabGridRow">
				  	<td width="20%" align="left" > <font class="TextNormal">Location Matrix Drivers: </font> </td>
				    <td width="13%" align="right"  style="TextNormal">
				    		<font class="TextNormal">Plant Number</font>
				    </td>
				    <td width="20%">
				    <h:outputText styleClass="inputClass" style="width: 200px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.driver_01}"/></td>
					<td width="15%" align="right" style="TextNormal">
						<font class="TextNormal">Vendor Zip</font>
					</td>
					<td width="20%">
						<h:outputText styleClass="inputClass" style="width: 200px;"  value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.driver_08}"/>
					</td>
				  </tr>
				  
				  <tr style="background-color:#ffffff;">
				    <td >&#160;</td>
				    <td  align="right"  >
				    	<h:outputText styleClass="TextNormal" value="Cost Center Number" /> 
				    </td>
				    <td ><h:outputText styleClass="inputClass" style="width: 200px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.driver_02}"/></td>
					<td  align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Vendor Number" /></td>
					<td ><h:outputText styleClass="inputClass" style="width: 200px;" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.driver_09}"/></td>
					
				  </tr>
				  
				  
				</table>
				
			
	
		
	</tbody>
	</table>	
			
		
	
		
		<div id="table-four-bottom">
		<ul class="right">
			<li class="view"><a href="#"></a></li>
			<li class="drilldown"><a href="#"></a></li>
			<li class="saveas"><a href="#"></a></li>
		</ul>
		</div>