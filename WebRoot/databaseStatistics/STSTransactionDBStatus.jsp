<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//Midtier project code modified - january 2009
	//<![CDATA[
	var currentPanel = "tableTransactions";
	function toggle(menu) {
		var obj = document.getElementById(currentPanel);
		if (obj) {
			obj.style.display = "none";
			obj.style.visibility = 'hidden';
		}
		obj = document.getElementById(menu.value);
		if (obj) {
			obj.style.display = "block";
			obj.style.visibility = 'visible';
		}
		currentPanel = menu.value;
		
		if(currentPanel == 'tableTransactions')
			document.getElementById("buttonGroup").style.display = "block";
		else
			document.getElementById("buttonGroup").style.display = "none";
	  }  

	registerEvent(window, "load", function() { selectRowByIndex('STSTrDBStatus:trDetailTable', 'transactionsRowIndex'); } );
	registerEvent(window, "unload", function() { document.getElementById('STSTrDBStatusFilter:hiddenSaveLink').onclick(); });
	
	function resetState()
	{ 
    	document.getElementById("STSTrDBStatusFilter:state").value = "";
	}
	
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="transactionsRowIndex" value="#{databaseStatisticsBean.selectedRowIndex}"/>

<h1><img id="imgDatabaseStatistics"  alt="Database Statistics" src="../images/headers/hdr-database-statistics.gif" width="250" height="19" /></h1>
<h:form id="STSTrDBStatusFilter">
 <div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	 <a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{databaseStatisticsBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(databaseStatisticsBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	  </a4j:commandLink>
 </div>

<a4j:outputPanel id="showhidefilter">
<c:if test="#{!databaseStatisticsBean.togglePanelController.isHideSearchPanel}">
<div id="top" style = "width:990px">
	<div id="table-one">
	<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
	<div id="table-one-top" style="padding-left: 10px;">
			 <table cellpadding="0" cellspacing="0" style="width: 100%;">
              <tr>
              <td style="padding-left: 10px;"><a4j:commandLink id="toggleTransactionProcessCollapse" style="text-decoration: underline;color:#0033FF;"
		          reRender="showhidefilter" actionListener="#{databaseStatisticsBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
	          <td style="padding-left: 10px;" ><a4j:commandLink id="toggleTransactionProcessExpand" style="text-decoration: underline;color:#0033FF;"  
				          reRender="showhidefilter" actionListener="#{databaseStatisticsBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
	          <td style="width: 50%; padding-left: 100px; "><a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel></td>        
	 		  <td style="padding-right: 5px; padding-left: 10px; padding-top:3px; width: 50px"><h:outputText value="Filter:" /></td>
	 		  <td style="padding-right: 3px;" class="column-input2-search">
				<h:selectOneMenu id="filterMenu" style="width:225px;" value="#{databaseStatisticsBean.selectedFilter}">
					<f:selectItems value="#{databaseStatisticsBean.filterItems}"/>
					<a4j:support event="onchange" status="rowstatus" actionListener="#{databaseStatisticsBean.filterChangeListener}"
						reRender="filterForm"/>
				</h:selectOneMenu>
			  </td>
			  
	          <td style="padding-right: 6px; padding-top: 2px;">
				<h:commandLink id="filterMenuEdit" styleClass="button"   action="#{databaseStatisticsBean.filterEditAction}">
					<h:graphicImage value="/images/editButton.png" />
            	</h:commandLink>
	          </td> 
	          
	                   
	          <td style="padding-right: 10px; padding-left: 20px; padding-top:3px; width: 50px"><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>  			  
             </tr>
			 </table>
		  </div>
		<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'STSTrDBStatusFilter:searchBtn')" >
		
		<rich:simpleTogglePanel id="dbStatstaxDriversInformation"   switchType="ajax" label="Goods &amp; Services Matrix Drivers" 
		actionListener="#{databaseStatisticsBean.togglePanelController.togglePanelChanged}" 
		opened="#{databaseStatisticsBean.togglePanelController.togglePanels['dbStatstaxDriversInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		<h:panelGrid id="driverPanel1" binding="#{databaseStatisticsBean.filterTaxDriverSelectionPanel}" styleClass="panel-ruler" />
		</rich:simpleTogglePanel>
		
		<rich:simpleTogglePanel id="dbStatslocationDriversInformation"   switchType="ajax" label="Location Matrix Drivers" 
		actionListener="#{databaseStatisticsBean.togglePanelController.togglePanelChanged}" 
		opened="#{databaseStatisticsBean.togglePanelController.togglePanels['dbStatslocationDriversInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		<h:panelGrid id="driverPanel" binding="#{databaseStatisticsBean.filterLocDriverSelectionPanel}" styleClass="panel-ruler" />
		</rich:simpleTogglePanel>
		
		
		<rich:simpleTogglePanel id="dbStatsmiscInformation"   switchType="ajax" label="Misc. Information" 
		actionListener="#{databaseStatisticsBean.togglePanelController.togglePanelChanged}" 
		opened="#{databaseStatisticsBean.togglePanelController.togglePanels['dbStatsmiscInformation']}"  
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
		
		<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
			<tbody>
			 <tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> From Date: </td> 
				<td class="column-input-search" >
					<rich:calendar zindex="1000"
						id="fromGlDate" enableManualInput="true" label="From G/L Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{databaseStatisticsBean.fromGLDate}" popup="true"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox" />
				</td>
				
				<td class="column-label"> &#160; </td> 
				<td class="column-input-search">&#160;</td>
				
				<td class="column-label"> Goods and Services Matrix ID: </td> 
				<td class="column-input-search">
						<h:inputText id="taxMatrixId" style="text-align:right;float:left;"
							value="#{databaseStatisticsBean.taxMatrixId}"
							onkeypress="return onlyIntegerValue(event);"
							label="Tax Matrix ID">
							<f:convertNumber integerOnly="true" groupingUsed="false"/>
						</h:inputText>
						<h:commandButton id="taxMatrixIdBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								action="#{databaseStatisticsBean.findTaxMatrixIdAction}">
						</h:commandButton>
					</td>
				<td class="column-spacer">&#160;</td>
				</tr> 
			   
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> To Date: </td> 
					<td class="column-input-search" >
						<rich:calendar zindex="1000"
							id="toGlDate" enableManualInput="true" label="To G/L Date"
							oninputkeypress="return onlyDateValue(event);"
							value="#{databaseStatisticsBean.toGLDate}" popup="true"
							converter="date"
							datePattern="M/d/yyyy"
							showApplyButton="false" inputClass="textbox" />
					</td>
					<td class="column-label"> Process Batch No: </td> 
					<td class="column-input-search" >
						<h:inputText id="batchId" style="text-align:right;float:left;width:130px" 
							value="#{databaseStatisticsBean.processBatchId}"
							onkeypress="return onlyIntegerValue(event);"
							label="Process Batch No">
							<f:convertNumber integerOnly="true" groupingUsed="false" />
						</h:inputText>
						
						<h:commandButton id="btnProcessBatchId" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								action="#{databaseStatisticsBean.findProcessBatchIdAction}">
						</h:commandButton>
					</td>
					
					<td class="column-label"> Locn Matrix ID: </td> 
					<td class="column-input-search">
					<h:inputText id="locnMatrixId" style="text-align:right;float:left;"
						value="#{databaseStatisticsBean.locationMatrixId}"
						onkeypress="return onlyIntegerValue(event);" 
						label="Locn Matrix ID">
						<f:convertNumber integerOnly="true" groupingUsed="false" />
					</h:inputText>
					<h:commandButton id="locnMatrixIdBtn" styleClass="image" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png" immediate="true" 
							action="#{databaseStatisticsBean.findLocationMatrixIdAction}">
					</h:commandButton>
				</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label"> TaxCode: </td> 
					<td colspan="5">
					<span style="display: inline-block;">
						<ui:include src="/WEB-INF/view/components/taxcode_code.xhtml">
							<ui:param name="handler" value="#{databaseStatisticsBean.filterTaxCodeHandler}"/>
							<ui:param name="id" value="filterTaxCode"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
						</ui:include>

					</span>					

					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Jurisdiction:</td>
					<td colspan="5">
						<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
							<ui:param name="handler" value="#{databaseStatisticsBean.filterHandler}"/>
							<ui:param name="id" value="jurisInput"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="showheaders" value="true"/>
							<ui:param name="popupName" value="searchJurisdiction"/>
							<ui:param name="popupForm" value="searchJurisdictionForm"/>
						</ui:include>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr> 
				
			</tbody>
		</table>
		
		
		</rich:simpleTogglePanel>
 </div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="clear"><a4j:commandLink id="btnClear" action="#{databaseStatisticsBean.resetFilter}" reRender="STSTrDBStatusFilter" /></li>
		<li class="search"><a4j:commandLink id="searchBtn" value="" actionListener="#{databaseStatisticsBean.retriveAll}" 
							reRender="showhidefilter,trDetailTable,taxHeader,taxDriver,locationHeader,locationDriver,view,dDown,saveAs" oncomplete="initScrollingTables();"/></li>
	
		<li class="advancedfilter"><a4j:commandLink id="advFilterBtn" rendered="#{!databaseStatisticsBean.isAdvancedFilterEnabled}" action="#{databaseStatisticsBean.advancedFilterAction}" /></li>
		<li class="advancedfilterenabled"><a4j:commandLink id="advFilterEnabledBtn" rendered="#{databaseStatisticsBean.isAdvancedFilterEnabled}" action="#{databaseStatisticsBean.advancedFilterAction}" /></li>
	</ul>
	</div>
	</a4j:outputPanel>
	</div>
</div>
</c:if>
</a4j:outputPanel>
</h:form>
<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSView-Statistics-open.gif" />
	</span>
</div>

<h:form id="STSTrDBStatus">
<div id="bottom" >
	<div id="table-four">
		
	<div id="table-four-top">
		Select Statistics:&#160;<h:selectOneMenu id="selectid" value="#{databaseStatisticsBean.selectedDisplay}" valueChangeListener="#{databaseStatisticsBean.displaySelectionChanged}" >
			<f:selectItem itemValue="tableTransactions" itemLabel="Transactions"/>
			<f:selectItem itemValue="tableTaxMatrix" itemLabel="Goods/Svc Matrix"/>
			<f:selectItem itemValue="locationMatrix" itemLabel="Location Matrix"/>
			
			<a4j:support event="onchange" reRender="showhidefilter1,saveAs"  />
			
		</h:selectOneMenu>
		
		&#160;&#160;&#160;&#160;&#160;
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
				startText="Request being processed..." 
				stopText="" 
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
    			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');"
    			/>
	</div>
	
	<a4j:outputPanel id="showhidefilter1">
	
	<c:if test="#{databaseStatisticsBean.selectedDisplay == 'tableTransactions'}">
	
	<div id="tableTransactions" style="height: auto;">
	<div id="table-four-content-ori" style="height: 250px;">
		<rich:dataTable rowClasses="odd-row,even-row" 
			width="99%"
			id="trDetailTable"             
			value="#{databaseStatisticsBean.tempStatisticsData}"
			var="dbTrStDTO"> 
			
			<a4j:support event="onRowClick" onsubmit="selectRow('STSTrDBStatus:trDetailTable', this);" status="rowstatus"
								 actionListener="#{databaseStatisticsBean.selectedRowChanged}" reRender="view,dDown" />
			<rich:column id="title">
				<f:facet name="header"><h:outputText styleClass="headerText" value=" " /></f:facet>
				<pre style="font:Arial"><h:outputText value="#{dbTrStDTO.title}" /></pre>
			</rich:column>
			
			<rich:column id="count" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="Count" /></f:facet>
				<h:outputText value="#{dbTrStDTO.count}">
				<f:convertNumber pattern = "#,###" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="countPct" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="% Total" /></f:facet>
				<h:outputText value="#{dbTrStDTO.countPct}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
				<rich:column id="sum" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="$ Amount" /></f:facet>
				<h:outputText value="#{dbTrStDTO.sum}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="sumPct" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="% Sum" /></f:facet>
				<h:outputText value="#{dbTrStDTO.sumPct}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="taxAmt" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="Tax Amount" /></f:facet>
				<h:outputText value="#{dbTrStDTO.taxAmt}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="max" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="Eff. Tax Rate" /></f:facet>
				<h:outputText value="#{dbTrStDTO.effTaxRate}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="10" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="max1" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="$ Highest Amount" /></f:facet>
				<h:outputText value="#{dbTrStDTO.max}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="avg" style="text-align:right;">
				<f:facet name="header"><h:outputText styleClass="headerText" value="$ Average Amount" /></f:facet>
				<h:outputText value="#{dbTrStDTO.avg}" >
				<f:convertNumber  minFractionDigits="2" maxFractionDigits="2" />
				</h:outputText>
			</rich:column>
			
			<rich:column id="grpBy">
				<f:facet name="header"><h:outputText styleClass="headerText" value="Group By On Drilldown" /></f:facet>
				<h:outputText value="#{dbTrStDTO.groupByOnDrilldown}"/>
			</rich:column>
		</rich:dataTable>
	
	</div>
	</div>
	</c:if>
	
	<c:if test="#{databaseStatisticsBean.selectedDisplay == 'tableTaxMatrix'}">
	<div id="tableTaxMatrix" style="height: auto; visibility:visible;display:block;">	
		<div id="table-four-content-ori" style="height: 250px;">
			<rich:dataTable rowClasses="odd-row,even-row" width="99%" id="taxHeader">
				<f:facet name="header">
						<rich:columnGroup>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Total G&amp;S Matrix Lines : " />					
								<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.total_matrix_lines}"/>
							</rich:column>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Active G&amp;S Matrix Lines : " />					
								<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.active_matrix_lines}"/>
							</rich:column>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Distinct G&amp;S Matrix Lines : " />					
								<h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.distinct_matrix_lines}"/>
							</rich:column>
						</rich:columnGroup>
				</f:facet>
			</rich:dataTable>
		
			<rich:panel>
				<f:facet name="header">
						<h:outputText styleClass="label" value="Goods &amp; Services Matrix Drivers :" />
				</f:facet>
				<rich:dataGrid columns="3" width="99%" rowClasses="odd-row,even-row"
					id="taxDriver"             
					value="#{databaseStatisticsBean.taxmatrixDrivers}"
					var="taxDriver">
								<h:outputText styleClass="label" value="#{taxDriver.description}" />
								<h:outputText value="#{taxDriver.driverCount}"/>
				</rich:dataGrid>	
			</rich:panel>	
		</div>
		
	</div>
	</c:if>
	
	<c:if test="#{databaseStatisticsBean.selectedDisplay == 'locationMatrix'}">
	<div id="locationMatrix" style="height: auto; visibility:visible;display:block;">
		 <div id="table-four-content-ori" style="height: 250px;"> 
			  <rich:dataTable rowClasses="odd-row,even-row" width="99%" id="locationHeader">
				<f:facet name="header">
						<rich:columnGroup>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Total Location Matrix Lines : " />					
								<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.total_location_matrix_lines}"/>
							</rich:column>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Active Location Matrix Lines : " />					
								<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.active_location_matrix_lines}"/>
							</rich:column>
							<rich:column>
								<h:outputText styleClass="TextNormal" value="Distinct Location Matrix Lines : " />					
								<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.locationMatrixTransactionStatiticsDTO.distinct_location_matrix_lines}"/>
							</rich:column>
						</rich:columnGroup>
				</f:facet>
			 </rich:dataTable>
			
			 <rich:panel>
				<f:facet name="header">
						<h:outputText styleClass="label" value="Location Matrix Drivers :" />
				</f:facet>
				<rich:dataGrid columns="3" width="80%" rowClasses="odd-row,even-row"
					id="locationDriver"             
					value="#{databaseStatisticsBean.locationmatrixDrivers}"
					var="locationDriver">
								<h:outputText styleClass="label" value="#{locationDriver.description}" />
								<h:outputText value="#{locationDriver.driverCount}"/>
				</rich:dataGrid>	
			</rich:panel>	
		</div>
	</div>
	</c:if>
	</a4j:outputPanel> <!-- end of showhidefilter1 -->

	<div id="buttonGroup" style="height: auto; visibility:visible;display:block;">
  		<div id="table-four-bottom">
				<ul class="right">
					<li class="view"><h:commandLink id="view" disabled="#{!databaseStatisticsBean.isValid}" action="#{databaseStatisticsBean.viewDbTrStatisticsAction}"/></li>
					<li class="drilldown"><h:commandLink id="dDown" disabled="#{!(databaseStatisticsBean.isValid and databaseStatisticsBean.viewDrillDown)}" action="#{databaseStatisticsBean.drillDownStatisticsAction}" /></li>
					<li class="saveas"><h:commandLink id="saveAs" disabled="#{databaseStatisticsBean.isTempNull}" action="#{databaseStatisticsBean.statisticsSaveAs}"/></li>
				</ul>
		</div>
	</div>
	
</div>   
</div>

</h:form>

<ui:include src="/WEB-INF/view/components/batchid_search.xhtml">
	<ui:param name="handler" value="#{databaseStatisticsBean.batchIdHandler}"/>
	<ui:param name="bean" value="#{databaseStatisticsBean}"/>
	<ui:param name="reRender" value="STSTrDBStatusFilter:batchId"/>
</ui:include>

</ui:define>

</ui:composition>

</html>