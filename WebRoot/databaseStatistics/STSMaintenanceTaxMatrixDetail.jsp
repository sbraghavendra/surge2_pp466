
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>


<table cellspacing="0" cellpadding="0" width="100%"  id="rollover2" >
				<tr>
					<td align="right"  width="20%">
						<h:outputText styleClass="TextNormal" value="Total TaxMatrix Lines:" />
					</td>
					<td width="20%">
						<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.total_matrix_lines}"/>
					</td>
					<td width="20%" align="right">
						<h:outputText styleClass="TextNormal" value="Active TaxMatrixLines" />
					</td>
					<td width="20%" >
						<h:outputText styleClass="inputClass" value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.active_matrix_lines}"/>
					</td>
					<td width="20%" align="right" >
						<h:outputText styleClass="TextNormal" value="Distinct TaxMatrixLines" />
					</td>
					<td width="20%">
						<h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.distinct_matrix_lines}"/>
					</td>
				</tr>
				<tr>
				 <td width="100%" colspan="6">
					<table width="100%" cellpadding="0" cellspacing="0" border=1 id="rollover" class="ruler">
						
					  <tr class="TabGridRow">
					  	<td width="20%" align="left" > 
					  		<h:outputText styleClass="TextNormal" value="TaxMatrix Drivers:" />
					  		
					  	</td>
					    
					    <td width="13%" align="right"  style="TextNormal">
					       <h:outputText styleClass="TextNormal" value="Vendor Type:" />
					    		
					    </td>
					    <td width="20%">
					    	<h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_01}"/>
					    </td>
						<td width="15%" align="right" style="TextNormal">
						   <h:outputText styleClass="TextNormal" value="Material Type:" />
							
						</td>
						<td width="20%">
							<h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_08}"/>
						</td>
						
						<td width="20%" align="right" style="TextNormal">
							<h:outputText styleClass="TextNormal" value="W.O.Number:" />
						</td>
						<td width="15%">
							<h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_15}"/>
						</td>
						
					  </tr>
					  <tr class="TabGridRowAlternate">
					    <td >&#160;</td>
					    <td  align="right"  ><h:outputText styleClass="TextNormal" value="Company Number" /> </td>
					    <td ><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_02}"/></td>
						<td  align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Internal Order Type" /></td>
						<td ><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_09}"/></td>
						<td  align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="P.O.Number" /></td>
						<td ><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_16}"/></td>
					  </tr>
					  
					  <tr >
					  <td >&#160;</td>
					    <td align="right"  style="TextNormal"><h:outputText styleClass="TextNormal" value="Vendor Number" /> </td>
					    <td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_03}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Inven Class" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_10}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Priority Type" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_17}"/></td>
					  </tr>
					  
					   <tr style="background-color:#ffffff;">
					   <td >&#160;</td>
					    <td align="right"  style="TextNormal"><h:outputText styleClass="TextNormal" value="Account Number" /> </td>
					    <td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_04}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Network" /> </td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_11}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Maint.Activity Type" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_18}"/></td>
					  </tr>
					  
					  <tr class="TabGridRow">
					  <td >&#160;</td>
					    <td align="right"  style="TextNormal"><h:outputText styleClass="TextNormal" value="Cost Center Number" /> </td>
					    <td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_05}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Inven. Line Type" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_12}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Plant Number" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_19}"/></td>
					  </tr>
					  
					  <tr class="TabGridRow">
					  	<td >&#160;</td>
					    <td align="right"  style="TextNormal"><h:outputText styleClass="TextNormal" value="Resp Cost Ctr." /> </td>
					    <td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_06}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Ship to Site" /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_13}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Breakdown Ind." /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_20}"/></td>
					  </tr>
					  
					  <tr class="TabGridRow">
					  	<td >&#160;</td>
					    <td align="right"  style="TextNormal"><h:outputText styleClass="TextNormal" value="Project No." /> </td>
					    <td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_07}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Inven No." /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_14}"/></td>
						<td align="right" style="TextNormal"><h:outputText styleClass="TextNormal" value="Project Ind." /></td>
						<td><h:outputText styleClass="inputClass"  value="#{databaseStatisticsBean.taxMatrixTransactionStatiticsDTO.driver_21}"/></td>
					  </tr>
					  
					  
					  
					 </table>
					 </td>
					</tr>
		</table>	
		<div id="table-four-bottom">
			<ul class="right">
				<li class="view"><a href="#"></a></li>
				<li class="drilldown"><a href="#"></a></li>
				<li class="saveas"><a href="#"></a></li>
			</ul>
		</div>