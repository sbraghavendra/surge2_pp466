<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('STSTrDBStatusViewForm:aMDetailList', 'transactionsRowIndex'); } );

//specify this function in MatrixCommonBean
function disableButtons(){
	document.getElementById('STSTrDBStatusViewForm:cancelAction').disabled = true;
	document.getElementById('STSTrDBStatusViewForm:saveAs').disabled = true;
	document.getElementById('STSTrDBStatusViewForm:viewTr').disabled = true;
	return true;
}
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="transactionsRowIndex" value="#{databaseStatisticsBean.selectedRowTrIndex}"/>
	
 <h:form id="STSTrDBStatusViewForm">

<h1><h:graphicImage id="imgViewDatabaseStatistics" alt="View Database Statistics"  value="/images/headers/hdr-database-statistics.gif" width="250" height="19" /></h1>
<div ><img src="../images/containers/STSView-transactions-open.gif" /></div><br/>
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"><h:outputText styleClass="headerText" value="View Transactions for: #{databaseStatisticsBean.selectedDatabaseTransactionStatisticsDTO.title}"/></div>

	<div id="table-four-content">	
			<div class="scrollContainer">
			<div class="scrollInner" id="resize">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMDetailList"
					binding="#{databaseStatisticsBean.trDetailTable}"
					value="#{databaseStatisticsBean.statisticsDataModel}" var="trdetail" rows="#{databaseStatisticsBean.statisticsDataModel.pageSize}">

				<a4j:support rendered="true" event="onRowClick" status="rowstatus"
						onsubmit="selectRow('STSTrDBStatusViewForm:aMDetailList', this);"
						actionListener="#{databaseStatisticsBean.selectedTransactionChanged}"
						reRender="viewTr" immediate="true"/>


			</rich:dataTable>
			</div>
			</div>
			
			<rich:datascroller id="trScroll" for="aMDetailList" maxPages="10" oncomplete="initScrollingTables();"
				style="clear:both;" align="center" stepControls="auto" ajaxSingle="false" 
				page="#{databaseStatisticsBean.statisticsDataModel.curPage}" reRender="pageInfo"/>
			
			</div>
			
	<div id="table-four-bottom">
			<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        	<a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{databaseStatisticsBean.statisticsDataModel.pageDescription}"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		
			<ul class="right">
				<li class="view"><h:commandLink id="viewTr" disabled="#{!databaseStatisticsBean.isValid}" action="#{databaseStatisticsBean.displayViewAction}"/></li>

<!-- 07/27/2010 SaveAs temporarily comment out				
				<li class="saveas"><h:commandLink id="saveAs" action="#{databaseStatisticsBean.transactionsSaveAs}"/></li>
-->				
				<li class="cancel2"><h:commandLink id="cancelAction" action="#{databaseStatisticsBean.cancelAction}" /></li>
			</ul>
	</div>

	</div>
</div>
		
		
  </h:form>
</ui:define>

</ui:composition>
</html>