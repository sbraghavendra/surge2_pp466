<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
	<ui:define name="script">
		<script type="text/javascript">
			//         
			registerEvent(window, "load", function() {
				selectRowByIndex('transForm:aMDetailList',
						'aMDetailListRowIndex');
			});
			//
		</script>
	</ui:define>
	<ui:define name="body">
		<h:inputHidden id="aMDetailListRowIndex"
			value="#{purchaseTestingToolBean.selectedRowIndex}" />

		<h1>
			<h:graphicImage id="imgTransactionProcess" alt="Transaction Process"
				value="/images/headers/hdr-PurchaseTestingTool.png" width="250"
				height="19" />
		</h1>


		<div id="top" style="border: none; margin: 0px 0px 0px 20px;">
			<a4j:outputPanel id="msg">
				<h:messages errorClass="error" />
			</a4j:outputPanel>
		</div>

		<div class="tab" style="display: block; margin: 0px 0px 0px 0px;">
			<table cellpadding="0" cellspacing="0" style="width: 925px;">
			<tr><td>				
					<h:graphicImage value="/images/containers/STSUtilities-enter-transaction-data.png" />
				</td>
				<td valign="bottom" style="align:right; color:#F80000;" >
					<h:panelGroup id="scenarioStatusPanel">
					<div align="right">	
						<h:outputText rendered="#{purchaseTestingToolBean.displayNewScenarioName}" id="enterNewNameId" value="Enter new Scenario name and click Save"></h:outputText>
						<h:outputText rendered="#{purchaseTestingToolBean.displayScenarioExists}" id="scenarioExistsId" value="Scenario name exists. Overwrite?"></h:outputText>
						<h:outputText rendered="#{purchaseTestingToolBean.displaySaveFirstWarning}" id="saveFirstWarningId" value="Save current Scenario first?"></h:outputText>
					</div>
					</h:panelGroup>
				</td>
			</tr>
			</table>
		</div>

		<h:form id="billingTestToolForm">
			<!-- default queue for the form is created -->
			<a4j:queue />

			<a4j:outputPanel id="showhidefilter">
			<div id="top" style="display: block; margin-top:3px;">
				<div id="table-one">
					<div id="table-one-top" style="padding-left: 10px;">
						<table cellpadding="0" cellspacing="0" style="width: 100%;">
							<tr>
								<td valign="top"><h:outputLabel id="pageLabel" value="Page: " /> <h:selectOneMenu
										id="ddlPage" style="width: 120px;"
										value="#{purchaseTestingToolBean.selectedPage}"
										immediate="true"
										valueChangeListener="#{purchaseTestingToolBean.pageSelectionChanged}">
										<f:selectItem itemValue="basicinformationFilter"
											itemLabel="Basic Information" />
										<f:selectItem itemValue="vendorFilter" itemLabel="Vendor" />
										<f:selectItem itemValue="invoiceFilter" itemLabel="Invoice" />
										<f:selectItem itemValue="locationsFilter" itemLabel="Locations" />
										<f:selectItem itemValue="userfieldsFilter" itemLabel="User Fields" />

										<a4j:support event="onchange" />

									</h:selectOneMenu>
								</td>
								<td valign="top" style="padding-left: 10px; padding-top:3px;">
									<a4j:commandLink id="toggleBillingTestToolCollapse" style="text-decoration: underline;color:#0033FF;"
					 					reRender="showhidefilter" actionListener="#{purchaseTestingToolBean.togglePanelController.collapseTogglePanels}" >
					 					<h:outputText value="collapse all"/>
					 				</a4j:commandLink>
									
								</td>
								<td valign="top" style="padding-left: 10px; padding-top:3px;">
									<a4j:commandLink id="toggleBillingTestToolExpand" style="text-decoration: underline;color:#0033FF;"
										reRender="showhidefilter" actionListener="#{purchaseTestingToolBean.togglePanelController.expandTogglePanels}">
										<h:outputText value="expand all" />
									</a4j:commandLink>
								</td>
								<td style="width: 200px">&#160;</td>
								<td valign="top" style="padding-right: 10px; padding-left: 100px; padding-top:3px; width: 200px"></td>
								<td style="width: 100%">&#160;</td>
								<td width="70"><h:outputText style="color:red;" value="*Mandatory&#160;&#160;&#160;&#160;"/></td>
								<td ><h:panelGroup id="scenarioForm">							
		
										<div id="table-one" >
											<div style="padding: 5px; width:400px;" align="right">
												<!--  
												<h:outputText rendered="#{purchaseTestingToolBean.displayNewScenarioName}" id="enterNewNameId" styleClass="error"
													value="Enter new Scenario name and click Save"></h:outputText>
												<h:outputText rendered="#{purchaseTestingToolBean.displayScenarioExists}" id="scenarioExistsId" styleClass="error"
													value="Scenario name exists. Overwrite?"></h:outputText>
												<h:outputText rendered="#{purchaseTestingToolBean.displaySaveFirstWarning}" id="saveFirstWarningId" styleClass="error"
													value="Save current Scenario first?"></h:outputText>
												<br/>
												-->
												<h:outputLabel id="scenarioNameLabel" value="Scenario Name: " />
												<h:inputText id="saveScenarioName" rendered="#{purchaseTestingToolBean.displayNewScenarioName || purchaseTestingToolBean.displayScenarioExists}"
													value="#{purchaseTestingToolBean.saveScenarioName}"></h:inputText>
												<h:selectOneMenu id="ddlScenarioName" rendered="#{!purchaseTestingToolBean.displayNewScenarioName &amp;&amp; !purchaseTestingToolBean.displayScenarioExists}"
													style="width: 120px;" value="#{purchaseTestingToolBean.selectedScenarioName}" immediate="true" valueChangeListener="#{purchaseTestingToolBean.scenarioSelectionChanged}">
													<f:selectItems value="#{purchaseTestingToolBean.scenarioNames}" />
													<a4j:support event="onchange" action="#{purchaseTestingToolBean.openScenario}" reRender="scenarioForm,transForm,scenarioStatusPanel" oncomplete="initScrollingTables();" />
												</h:selectOneMenu>
											</div>
											<c:if test="#{purchaseTestingToolBean.showSaveScenario || purchaseTestingToolBean.showDeleteScenario}">
												<div id="table-one-bottom">
													<ul class="right" style="margin-right: 0px;">
														<li class="save"><a4j:commandLink id="btnSaveScenario" 
																rendered="#{purchaseTestingToolBean.showSaveScenario &amp;&amp; !purchaseTestingToolBean.displayScenarioExists}"
																action="#{purchaseTestingToolBean.saveScenario}" reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
														</li>
														<li class="ok"><a4j:commandLink id="btnSaveScenarioOverwrite"
																rendered="#{purchaseTestingToolBean.displayScenarioExists}"
																action="#{purchaseTestingToolBean.saveScenarioOverwrite}" reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
														</li>
														<li class="delete2">
															<a4j:commandLink id="btnDelete"
																rendered="#{purchaseTestingToolBean.showDeleteScenario  &amp;&amp; !purchaseTestingToolBean.displaySaveFirstWarning}"
																onclick="Richfaces.showModalPanel('warning'); return false;"
																action="#{purchaseTestingToolBean.deleteScenario}" reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
										
															<a4j:commandLink id="btnDeleteOk" style="display: none;" action="#{purchaseTestingToolBean.deleteScenario}"
																reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
														</li>
														<li class="cancel"><a4j:commandLink id="btnCancel"
																rendered="#{purchaseTestingToolBean.displayNewScenarioName  || purchaseTestingToolBean.displayScenarioExists}"
																action="#{purchaseTestingToolBean.cancelNewScenarioName}" reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
														</li>
														<li class="cancel"><a4j:commandLink id="btnCancelSave"
																rendered="#{purchaseTestingToolBean.displaySaveFirstWarning}" action="#{purchaseTestingToolBean.cancelModified}"
																reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
														</li>
													</ul>
												</div>
											</c:if>
										</div>
									</h:panelGroup>
								</td>
							</tr>
							
							<!-- Scenario -->
						
								<!-- 
								<td><h:outputLabel id="processMethodLabel" value="Process Method: " />
									<h:selectOneMenu id="ddlProcessMethod" style="width: 120px;" value="#{purchaseTestingToolBean.selectedProcessMethod}" immediate="true">
										<f:selectItems value="#{purchaseTestingToolBean.processMethods}" />
									</h:selectOneMenu></td>
							 -->
				
							<!-- End of Scenaro -->
						</table>
					</div>
					<div id="table-one">
						<c:if test="#{purchaseTestingToolBean.selectedPage == 'basicinformationFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true" layout="block">
								<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/purch_testtool_basicinfo_panel.xhtml">
									<ui:param name="trDetail" value="#{purchaseTestingToolBean.currentInvoice}" />
									<ui:param name="beanDetail" value="#{purchaseTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if test="#{purchaseTestingToolBean.selectedPage == 'vendorFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/purch_testtool_vendor_panel.xhtml">
									<ui:param name="trDetail"
										value="#{purchaseTestingToolBean.currentInvoice}" />
									<ui:param name="beanDetail" value="#{purchaseTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if
							test="#{purchaseTestingToolBean.selectedPage == 'invoiceFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/purch_testtool_invoice_panel.xhtml">
									<ui:param name="trDetail"
										value="#{purchaseTestingToolBean.currentInvoice}" />
									<ui:param name="beanDetail" value="#{purchaseTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if
							test="#{purchaseTestingToolBean.selectedPage == 'locationsFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/purch_testtool_locations_panel.xhtml">
									<ui:param name="trDetail"
										value="#{purchaseTestingToolBean.currentInvoice}" />
									<ui:param name="beanDetail" value="#{purchaseTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if
							test="#{purchaseTestingToolBean.selectedPage == 'userfieldsFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/purch_testtool_userfields_panel.xhtml">
									<ui:param name="trDetail"
										value="#{purchaseTestingToolBean.currentInvoice}" />
									<ui:param name="beanDetail" value="#{purchaseTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
					</div>
					<div id="table-one-bottom">
						<ul class="right">
							<li class="new-invoice"><a4j:commandLink id="btnNewInvoice"
									action="#{purchaseTestingToolBean.newInvoice}"
									reRender="billingTestToolForm,transForm,scenarioForm,scenarioStatusPanel" />
							</li>
							<li class="add-line-item"><h:commandLink id="addBtn"
									action="#{purchaseTestingToolBean.addLineItem}" /></li>
						</ul>
					</div>
				</div>
			</div>
			
			</a4j:outputPanel><!-- end of showhidefilter -->
		</h:form>

		<div class="wrapper">
			<span class="block-right">&#160;</span> <span class="block-left tab">
				<h:graphicImage
					value="/images/containers/STSView-transactions-open.gif" /> </span>
		</div>

		<h:form id="transForm">
			<!-- default queue for the form is created -->
			<a4j:queue />

			<div id="bottom">
				<div id="table-four" style="vertical-align: bottom;">
					<div id="table-four-top"
						style="padding-left: 23px; padding-bottom: 0;">
						<table cellpadding="0" cellspacing="0"
							style="width: 100%; height: 22px; vertical-align: bottom;">
							<tbody>
								<tr>
									<td width="200"><h:selectBooleanCheckbox id="chkSelectAll"
											value="#{purchaseTestingToolBean.allSelected}"
											styleClass="check"
											disabled="#{!purchaseTestingToolBean.selectAllEnabled}">
											<a4j:support event="onclick"
												reRender="aMDetailList,pageInfo,selectAllWarning,btnProcess"
												actionListener="#{purchaseTestingToolBean.selectAllChange}"
												oncomplete="initScrollingTables();" />

										</h:selectBooleanCheckbox> <h:outputText
											value="#{purchaseTestingToolBean.selectAllCaption}" /></td>
									<td align="center"><a4j:status id="pageInfo"
											startText="Request being processed..."
											stopText="#{purchaseTestingToolBean.transactionSaleDataModel.pageDescription}"
											onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
											onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />

										<h:outputText id="selectAllWarning" style="color:red"
											value="#{purchaseTestingToolBean.selectAllWarning}" /></td>
											
									<td align="center" width="270">
										<h:outputLabel id="taxMessageId" value="#{purchaseTestingToolBean.validTaxMessage}" />
									</td>
									
									<td align="right" width="150">
										<table cellpadding="0" cellspacing="0" style="height: 22px;">
											<tr>
												<td><rich:spacer width="20px" /> <h:selectBooleanCheckbox
														id="chkDisplayAllFields"
														value="#{purchaseTestingToolBean.displayAllFields}"
														styleClass="check">
														<a4j:support event="onclick"
															reRender="aMDetailList,pageInfo"
															actionListener="#{purchaseTestingToolBean.displayChange}"
															oncomplete="initScrollingTables();" />
													</h:selectBooleanCheckbox> <h:outputText value="Display All Fields" />
												</td>
											</tr>
										</table>
									</td>
									<td style="width: 20px;">&#160;</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="table-four-content">
						<ui:include
							src="/WEB-INF/view/components/transactiondetailsale_table.xhtml">
							<ui:param name="formName" value="transForm" />
							<ui:param name="bean" value="#{purchaseTestingToolBean}" />
							<ui:param name="dataModel"
								value="#{purchaseTestingToolBean.transactionSaleDataModel}" />
							<ui:param name="singleSelect" value="true" />
							<ui:param name="multiSelect" value="true" />
							<ui:param name="doubleClick" value="false" />
						<!--  <ui:param name="selectReRender" value="taxMessageId,btnProcess,btnView,btnUpdateTrans,btnDeleteTrans,billingTestToolForm" /> -->	
						<ui:param name="selectReRender" value="transForm:taxMessageId,btnProcess,btnSave,btnView,btnUpdateTrans,btnDeleteTrans,billingTestToolForm" />
							<ui:param name="scrollReRender" value="pageInfo" />
						</ui:include>
					</div>

					<div id="table-four-bottom">
						<ul class="right">
							<li class="process"><h:commandLink id="btnProcess"
									disabled="#{!purchaseTestingToolBean.displayProcessTransactions}"
									action="#{purchaseTestingToolBean.process}">
									   <a4j:support event="click" reRender="transForm" oncomplete="initScrollingTables();"></a4j:support>
									</h:commandLink>
							</li>
							<li class="clear-grid"><a4j:commandLink id="btnClear"
									action="#{purchaseTestingToolBean.clearAll}"
									reRender="billingTestToolForm,transForm" />
							</li>
							<li class="save-trans"><h:commandLink id="btnSave"
									disabled="#{!purchaseTestingToolBean.displaySaveTransactions}"
									action="#{purchaseTestingToolBean.saveTransactions}"
									reRender="transForm" oncomplete="initScrollingTables();" />
							</li>
							<li class="view115"><h:commandLink id="btnView"
									disabled="#{!purchaseTestingToolBean.displayViewTransaction}"
									action="#{purchaseTestingToolBean.viewTransaction}" />
							</li>
							<li class="update"><h:commandLink id="btnUpdateTrans"
									disabled="#{!purchaseTestingToolBean.displayUpdateTransaction}"
									action="#{purchaseTestingToolBean.updateLineItem}" />
							</li>
							<li class="delete"><h:commandLink id="btnDeleteTrans"
									disabled="#{!purchaseTestingToolBean.displayDeleteTransaction}"
									action="#{purchaseTestingToolBean.deleteTransaction}" />
							</li>
							<!--
                            <li class="saveas">
                                <a4j:commandLink id="btnSaveAs" rendered="#{purchaseTestingToolBean.hasInvoiceLines}" action="#{purchaseTestingToolBean.saveTransactions}" reRender="transForm" />
                            </li>
                            -->
						</ul>
					</div>
				</div>
			</div>
		</h:form>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.shipToJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionShipto" />
			<ui:param name="popupForm" value="searchJurisdictionFormShipto" />
			<ui:param name="jurisId" value="jurisIdShipto" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputShipto" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.shipFromJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionShipfrom" />
			<ui:param name="popupForm" value="searchJurisdictionFormShipfrom" />
			<ui:param name="jurisId" value="jurisIdShipfrom" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputShipfrom" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.ordrorgnJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionOrdrorgn" />
			<ui:param name="popupForm" value="searchJurisdictionFormOrdrorgn" />
			<ui:param name="jurisId" value="jurisIdOrdrorgn" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputOrdrorgn" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.ordracptJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionOrdracpt" />
			<ui:param name="popupForm" value="searchJurisdictionFormOrdracpt" />
			<ui:param name="jurisId" value="jurisIdOrdracpt" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputOrdracpt" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.firstuseJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionFirstuse" />
			<ui:param name="popupForm" value="searchJurisdictionFormFirstuse" />
			<ui:param name="jurisId" value="jurisIdFirstuse" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputFirstuse" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.billtoJurisdictionHandler}" />
			<ui:param name="popupName" value="searchJurisdictionBillto" />
			<ui:param name="popupForm" value="searchJurisdictionFormBillto" />
			<ui:param name="jurisId" value="jurisIdBillto" />
			<ui:param name="reRender"
				value="billingTestToolForm:jurisInputBillto" />
		</ui:include>

		<!-- Replace With Vendor <ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{purchaseTestingToolBean.customerJurisdictionHandler}" />
			<ui:param name="popupName" value="searchCustomerJurisdiction" />
			<ui:param name="popupForm" value="searchCustomerJurisdictionForm" />
			<ui:param name="jurisId" value="customerJurisId" />
			<ui:param name="reRender"
				value="billingTestToolForm:customerJurisInput" />
		</ui:include>-->

		<ui:include src="/WEB-INF/view/components/warning.xhtml">
			<ui:param name="bean" value="#{purchaseTestingToolBean}" />
			<ui:param name="id" value="warning" />
			<ui:param name="warning" value="Are you sure?" />
			<ui:param name="okBtn" value="billingTestToolForm:btnDeleteOk" />
		</ui:include>
	</ui:define>
</ui:composition>
</html>