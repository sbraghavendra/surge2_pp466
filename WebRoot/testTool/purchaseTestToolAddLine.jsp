<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
	<ui:define name="body">
		<h1>
			<h:graphicImage id="imgTransactionProcess" alt="Transaction Process"
				value="/images/headers/hdr-billing-testing-tool.png" width="250"
				height="19" />
		</h1>

		<h:form id="billingTestToolFormLine">
			<!-- default queue for the form is created -->
			<a4j:queue />
			
			<a4j:outputPanel id="showhidefilter">
			<div id="top">
				<div id="table-one">
					<div id="table-four-top">
						<a4j:outputPanel id="msg">
							<h:messages errorClass="error" />
						</a4j:outputPanel>
					</div>
					<div id="table-one-content" style="min-height: 0px;">
						<table cellpadding="0" cellspacing="0" width="100%" id="rollover"
							class="ruler">
							<thead>
								<tr>
									<td>Add an Invoice Line Item</td>
								</tr>
							</thead>
						</table>
					</div>

					<div id="table-one-top" style="padding-left: 10px;">
						<table cellpadding="0" cellspacing="0" style="width: 100%;">
							<tr>
								<td><h:outputLabel id="pageLabel" value="Page: " /> <h:selectOneMenu
										id="ddlPage" style="width: 120px;"
										value="#{billingTestingToolBean.invoiceLineSelectedPage}"
										immediate="true"
										valueChangeListener="#{billingTestingToolBean.invoiceLinePageSelectionChanged}">
										<f:selectItem itemValue="basicinformationFilter" itemLabel="Basic Information" />
										<f:selectItem itemValue="invoiceFilter" itemLabel="Invoice" />
										<f:selectItem itemValue="locationfieldsFilter" itemLabel="Locations" />
										<f:selectItem itemValue="userfieldsFilter" itemLabel="User Fields" />

										<a4j:support event="onchange" />

									</h:selectOneMenu></td>
								<td style="padding-left: 10px;">
									<a4j:commandLink id="toggleBillingTestToolCollapse" style="text-decoration: underline;color:#0033FF;"
										reRender="showhidefilter" actionListener="#{billingTestingToolBean.lineTogglePanelController.collapseTogglePanels}">
										<h:outputText value="collapse all" />
									</a4j:commandLink>
								</td>
								<td style="padding-left: 10px;">
									<a4j:commandLink id="toggleBillingTestToolExpand" style="text-decoration: underline;color:#0033FF;"
										reRender="showhidefilter" actionListener="#{billingTestingToolBean.lineTogglePanelController.expandTogglePanels}">
										<h:outputText value="expand all" />
									</a4j:commandLink>
								</td>
	
								<td style="width: 100%"></td>
								 
							</tr>
						</table>

					</div>
					<div id="table-one">
						<c:if
							test="#{billingTestingToolBean.invoiceLineSelectedPage == 'basicinformationFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/testtool_invoice_line_basicinfo_panel.xhtml">
									<ui:param name="trDetail"
										value="#{billingTestingToolBean.currentInvoiceLine}" />
									<ui:param name="beanDetail" value="#{billingTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if
							test="#{billingTestingToolBean.invoiceLineSelectedPage == 'invoiceFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/testtool_invoice_line_invoice_panel.xhtml">
									<ui:param name="trDetail"
										value="#{billingTestingToolBean.currentInvoiceLine}" />
									<ui:param name="beanDetail" value="#{billingTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						<c:if
							test="#{billingTestingToolBean.invoiceLineSelectedPage == 'locationfieldsFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/testtool_invoice_line_location_panel.xhtml">
									<ui:param name="trDetail"
										value="#{billingTestingToolBean.currentInvoiceLine}" />
									<ui:param name="beanDetail" value="#{billingTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
						
						<c:if
							test="#{billingTestingToolBean.invoiceLineSelectedPage == 'userfieldsFilter'}">
							<a4j:outputPanel id="contentToggle" ajaxRendered="true"
								layout="block">
								<a4j:include ajaxRendered="true"
									viewId="/WEB-INF/view/components/testtool_invoice_line_userfields_panel.xhtml">
									<ui:param name="trDetail"
										value="#{billingTestingToolBean.currentInvoiceLine}" />
									<ui:param name="beanDetail" value="#{billingTestingToolBean}" />
									<ui:param name="readonly" value="false" />
								</a4j:include>
							</a4j:outputPanel>
						</c:if>
					</div>
					<div id="table-four-bottom">
						<ul class="right">
							<li class="ok"><h:commandLink id="btnOk"
									action="#{billingTestingToolBean.saveInvoiceLine}"
									reRender="billingTestToolForm,transForm,transForm:taxMessageId,transForm:btnProcess,transForm:btnView,transForm:btnUpdateTrans,transForm:btnDeleteTrans" /></li>
							<li class="cancel"><h:commandLink id="btnCancel"
									action="#{billingTestingToolBean.cancelInvoiceLine}" />
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			</a4j:outputPanel><!-- end of showhidefilter -->
		</h:form>
		
		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.shipToJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionShipto" />
			<ui:param name="popupForm" value="searchJurisdictionFormShipto" />
			<ui:param name="jurisId" value="jurisIdShipto" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputShipto" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.shipFromJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionShipfrom" />
			<ui:param name="popupForm" value="searchJurisdictionFormShipfrom" />
			<ui:param name="jurisId" value="jurisIdShipfrom" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputShipfrom" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.ordrorgnJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionOrdrorgn" />
			<ui:param name="popupForm" value="searchJurisdictionFormOrdrorgn" />
			<ui:param name="jurisId" value="jurisIdOrdrorgn" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputOrdrorgn" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.ordracptJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionOrdracpt" />
			<ui:param name="popupForm" value="searchJurisdictionFormOrdracpt" />
			<ui:param name="jurisId" value="jurisIdOrdracpt" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputOrdracpt" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.firstuseJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionFirstuse" />
			<ui:param name="popupForm" value="searchJurisdictionFormFirstuse" />
			<ui:param name="jurisId" value="jurisIdFirstuse" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputFirstuse" />
		</ui:include>

		<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
			<ui:param name="handler"
				value="#{billingTestingToolBean.billtoJurisdictionHandlerLine}" />
			<ui:param name="popupName" value="searchJurisdictionBillto" />
			<ui:param name="popupForm" value="searchJurisdictionFormBillto" />
			<ui:param name="jurisId" value="jurisIdBillto" />
			<ui:param name="reRender"
				value="billingTestToolFormLine:jurisInputBillto" />
		</ui:include>
		
		
	</ui:define>
</ui:composition>
</html>