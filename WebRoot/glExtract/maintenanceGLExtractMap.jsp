<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('glExtractForm:glTable', 'glTableRowIndex'); } );
function resetCaseSelection() 
{
	var form = document.forms["glExtractForm"];
	var element = form.elements["glExtractForm:selTaxJur"];

 	if(element.options[0] != null){
    		element.options[0].selected = true
    }
}
  function clearForm() {
   var f = document.getElementById('glExtractForm');
   f.reset();
}
//]]>
</script>
</ui:define>

<ui:define name="body">

<h:inputHidden id="glTableRowIndex" value="#{glExtractmapBackingBean.selectedRowIndex}"/>

<h:form id="glExtractForm">
<h1><h:graphicImage id="imgGlExtractMap" alt="GL Extract Map" value="/images/headers/hdr-gl-extract-map.gif"/></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>
<!--  Filter start -->
<div id="top" style = "width:964px">
	<div id="table-one" >
	<div id="table-one-top">
	<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" onkeyup="return submitEnter(event,'glExtractForm:searchBtn')" >
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr style="width: 700px;">
			<td style="width: 10px;">&#160;</td>
			<td class="column-label">TaxCode:</td>
			<td align="left" style="width: 350px;">
				<ui:include src="/WEB-INF/view/components/taxcode_lookup.xhtml">
							<ui:param name="handler" value="#{glExtractmapBackingBean.filterTaxCodeHandler}"/>
							<ui:param name="id" value="filterTaxCode"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
						</ui:include>
			</td>
			<td class="column-label" style="text-align:right;">Tax Juris. Type:&#160;&#160;</td>
			<td>
			 	<h:selectOneMenu id="selTaxJur" title="Tax Jurisdiction Type"
                                   style="width: 180px;" 
                                   binding="#{glExtractmapBackingBean.taxJurisTypeCodeInput}">
       			</h:selectOneMenu>
			</td>
		</tr>
		</table>
			<h:panelGrid id="filterPanel" binding="#{glExtractmapBackingBean.filterSelectionPanel}" 
                   styleClass="panel-ruler"/>


	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="clear">
      <a4j:commandLink id="btnClear"
    reRender="filterTaxCode,filterPanel,selTaxJur,glTable"
                       onclick="clearForm();"
                       action="#{glExtractmapBackingBean.resetFilter}"
                     />
<!--                        action="mtns_glextract_main" -->
    </li>
		<li class="search"><h:commandLink id="searchBtn" action="#{glExtractmapBackingBean.searchGlExtract}"/></li>
	</ul>
	</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSExtract-account-mapping.gif" width="192" height="17" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top">
	
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       	<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{glExtractmapBackingBean.glExtractMapDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 
	
	</div>
	<div id="table-four-content">

		<div class="scrollContainer">
		<div class="scrollInner">

		<rich:dataTable rowClasses="odd-row,even-row" id="glTable"
			value="#{glExtractmapBackingBean.glExtractMapDataModel}" 
			rows="#{glExtractmapBackingBean.glExtractMapDataModel.pageSize}"
			var="glextract">

			<a4j:support event="onRowClick"
					onsubmit="selectRow('glExtractForm:glTable', this);"
					actionListener="#{glExtractmapBackingBean.selectedRowChanged}"
					reRender="deleteBtn,updateBtn"/>

			<rich:column id="taxCodeCountryCode">
				<f:facet name="header" >
				<a4j:commandLink id="s_taxCodeCountry"
      styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['taxCodeCountryCode']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="glTable" value="Country"/>
						</f:facet>
					<h:outputText value="#{cacheManager.countryMap[glextract.taxcodeCountryCode]}"/>
			</rich:column>

			<rich:column id="taxCodeStateCode">
				<f:facet name="header" >
				<a4j:commandLink id="s_taxCodeState"
      styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['taxCodeStateCode']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="glTable" value="State"/>
						</f:facet>
					<h:outputText value="#{cacheManager.taxCodeStateMap[glextract.taxCodeStateCode].name}"/>
			</rich:column>
			<rich:column id="taxCodeTypeCode">
				<f:facet name="header" >
				<a4j:commandLink id="s_taxCodeType" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['taxCodeTypeCode']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="glTable" value="Type"/>
						</f:facet>
					<h:outputText value="#{cacheManager.listCodeMap['TCTYPE'][glextract.taxCodeTypeCode].description}"/>
			</rich:column>
			<rich:column id="taxCodeCode">
				<f:facet name="header" >
				<a4j:commandLink id="s_taxCode" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['taxCodeCode']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="glTable" value="TaxCode"/>
						</f:facet>
					<h:outputText value="#{glextract.taxCodeCode}"/>
			</rich:column>
			<rich:column id="taxJurisTypeCode">
				<f:facet name="header">
				<a4j:commandLink id="s_taxJurisTypeCode" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['taxJurisTypeCode']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
				value="Tax Juris Type Desc" /></f:facet>
				<h:outputText value="#{glExtractmapBackingBean.juristypeCodeCodeToDescriptionMap[glextract.taxJurisTypeCode]}" />
			</rich:column>

				<rich:column id="driver01" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_01']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver01" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver01']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_01']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver01}" />
				</rich:column>
							<rich:column id="driver02" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_02']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver02" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver02']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_02']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver02}" />
				</rich:column>
							<rich:column id="driver03" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_03']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver03" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver03']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_03']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver03}" />
				</rich:column>
							<rich:column id="driver04" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_04']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver04" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver04']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_04']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver04}" />
				</rich:column>
							<rich:column id="driver05" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_05']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver05" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver05']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_05']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver05}" />
				</rich:column>
							<rich:column id="driver06" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_06']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver06" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver06']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_06']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver06}" />
				</rich:column>
							<rich:column id="driver07" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_07']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver07" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver07']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_07']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver07}" />
				</rich:column>
							<rich:column id="driver08" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_08']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver08" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver08']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_08']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver08}" />
				</rich:column>
							<rich:column id="driver09" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_09']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver09" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver09']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_09']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver09}" />
				</rich:column>
						<rich:column id="driver10" rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_10']}">
					<f:facet name="header">
						<a4j:commandLink id="s_driver10" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['driver10']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="glTable"
							value="#{matrixCommonBean.glExtractMap['DRIVER_10']}"/>
					</f:facet>
					<h:outputText value="#{glextract.driver10}" />
				</rich:column>

			<rich:column id="comments">
				<f:facet name="header">
				<a4j:commandLink id="s_comments" styleClass="sort-#{glExtractmapBackingBean.glExtractMapDataModel.sortOrder['comments']}"
						actionListener="#{glExtractmapBackingBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="glTable" 
				value="Comments" /></f:facet>
				<h:outputText value="#{glextract.comments}" />
			</rich:column>
		</rich:dataTable>
		
		</div>
		</div>
				<rich:datascroller id="trScroll" for="glTable" maxPages="10" oncomplete="initScrollingTables();"
			style="clear:both;" align="center" stepControls="auto" ajaxSingle="false" 
			page="#{glExtractmapBackingBean.glExtractMapDataModel.curPage}" 
			reRender="pageInfo"/>

	</div>

	<div id="table-four-bottom">
	<ul class="right">
		<li class="add">
      <h:commandLink id="btnAdd" action="#{glExtractmapBackingBean.gotoAddAction}" 
      				 style="#{(glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
      				 disabled="#{glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag}"/>
    </li>
		<li class="update">
      <h:commandLink id="updateBtn"
                     style="#{(glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                     disabled="#{!glExtractmapBackingBean.showButtons or glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag}"
                     action="#{glExtractmapBackingBean.gotoUpdateAction}"/>
    </li>
		<li class="delete">
      <h:commandLink id="deleteBtn" 
                     style="#{(glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                     disabled="#{!glExtractmapBackingBean.showButtons or glExtractmapBackingBean.currentUser.viewOnlyBooleanFlag}"
                     action="#{glExtractmapBackingBean.gotoDeleteAction}"/>
    </li>
	</ul>
	</div>
	</div>
	
</div>

</h:form>
<ui:include src="/WEB-INF/view/components/taxcode_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.filterTaxCodeHandler}"/>
	<ui:param name="popupName" value="searchTaxCode"/>
	<ui:param name="popupForm" value="searchTaxCodeForm"/>
	<ui:param name="reRender" value="glExtractForm:filterTaxCode"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.driverHandler}"/>
</ui:include>
</ui:define>

</ui:composition>

</html>
