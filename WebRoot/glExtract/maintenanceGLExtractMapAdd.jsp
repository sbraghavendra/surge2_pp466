<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="glExtractAddForm">
<h1><h:graphicImage id="imgGlExtractMap" alt="GL Extract Map" value="/images/headers/hdr-gl-extract-map.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">#{glExtractmapBackingBean.currentAction} a G/L Extract Map</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="3">TaxCode</th></tr>
			<tr>
			  <td style="width: 10px;">&#160;</td>
			  <td class="column-label">TaxCode State</td>
			  <td>
				  <ui:include src="/WEB-INF/view/components/taxcode.xhtml">
							<ui:param name="handler" value="#{glExtractmapBackingBean.newTaxCodeHandler}"/>
							<ui:param name="id" value="filterTaxCode"/>
							<ui:param name="readonly" value="false"/>
							<ui:param name="label" value="TaxCode State"/>
							<ui:param name="type" value="TaxCode Type"/>
							<ui:param name="TaxCode" value="TaxCode"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
						</ui:include>
			  </td>
			  <td style="width: 10px;">&#160;</td>
			  <td class="column-label">Jurisdiction Type Code</td>
        <td>
			    <h:selectOneMenu id="selTaxJur"
                           title="Tax Jurisdiction Type Code"
                           style="width: 215px;" 
                           binding="#{glExtractmapBackingBean.taxJurisTypeCodeInput}"/>
			</td>
      </tr>
    </tbody>
    </table>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<tbody>
		<tr><th colspan="6">Drivers</th></tr>
		<tr>
			<td colspan="6">
			<h:panelGrid id="filterPanel" binding="#{glExtractmapBackingBean.filterSelectionPanel}" 
                   styleClass="panel-ruler"/>
			</td>
		</tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Comments:</td>
			<td colspan="4">
			  <h:inputText id="comments" 
			               binding="#{glExtractmapBackingBean.comments}"
			               size="80"
			               maxlength="255">
        </h:inputText>
			</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Last Update User ID:</td>
			<td>
			  <h:inputText id="lastUpdateID" 
                     disabled="true"
			               value="#{glExtractmapBackingBean.selectedGlExtract.updateUserId}" size="30">
        </h:inputText>
			</td>
			<td>Last Update Timestamp:</td>
			<td>
			  <h:inputText id="lastUpdateTimestamp" 
                     disabled="true"
                     converter="dateTime"
			               value="#{glExtractmapBackingBean.selectedGlExtract.updateTimestamp}" size="30">
        </h:inputText>
			</td>
		</tr>
    </tbody>
	</table>
	
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok">
      <h:commandLink id="btnAdd"
         disabled="#{glExtractmapBackingBean.isAddAction}"/>
         action="#{glExtractmapBackingBean.updateGlExtract}"/>
    </li>
		<li class="add">
      <h:commandLink id="btnUpdate"
         disabled="#{glExtractmapBackingBean.isUpdateAction}"/>
         action="#{glExtractmapBackingBean.addGlExtractAction}"/>
    </li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="mtns_glextract_main"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
<ui:include src="/WEB-INF/view/components/taxcode_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.newTaxCodeHandler}"/>
	<ui:param name="popupName" value="searchTaxCode"/>
	<ui:param name="popupForm" value="searchTaxCodeForm"/>
	<ui:param name="reRender" value="glExtractAddForm:filterTaxCode"/>
</ui:include>
</f:view>
</ui:define>
</ui:composition>
</html>