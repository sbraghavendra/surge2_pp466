<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">

<h:form id="glExtractForm">

<h1><h:graphicImage id="imgGlExtractMap" alt="GL Extract Map" value="/images/headers/hdr-gl-extract-map.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="4">#{glExtractmapBackingBean.currentAction} a G/L Extract Map</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="4">TaxCode</th></tr>
			<tr>
			  <td style="width: 10px;">&#160;</td>
			  <td class="column-label">TaxCode:</td>
			  <td align="left" style="width: 350px;">
				  <ui:include src="/WEB-INF/view/components/taxcode.xhtml">
							<ui:param name="handler" value="#{glExtractmapBackingBean.taxCodeHandler}"/>
							<ui:param name="id" value="showTaxCode"/>
							<ui:param name="readonly" value="#{glExtractmapBackingBean.isDeleteAction || glExtractmapBackingBean.isUpdateAction}"/>
							<ui:param name="required" value="false"/>
							<ui:param name="label" value="TaxCode State"/>
							<ui:param name="type" value="TaxCode Type"/>
							<ui:param name="TaxCode" value="TaxCode"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
						</ui:include>
			  </td>
			  <td style="width:100%px;">&#160;</td>
			 </tr> 		 
			 <tr> 
			  <td style="width: 10px;">&#160;</td>
			  <td class="column-label">Tax Juris. Type:</td>
        	  <td align="left">
			    <h:selectOneMenu id="selTaxJur"
							             disabled="#{glExtractmapBackingBean.isDeleteAction  || glExtractmapBackingBean.isUpdateAction}"
                           title="Tax Jurisdiction Type"
                           style="width: 180px;"
                           binding="#{glExtractmapBackingBean.showTaxJurisTypeCodeInput}"/>
			 </td>
			 <td style="width:100%px;">&#160;</td>
      </tr>
      <tr><th colspan="4">Drivers</th></tr>
    </tbody>
    <h:panelGrid id="showPanel" binding="#{glExtractmapBackingBean.showMatrixPanel}" 
                   styleClass="panel-ruler"/>    
    </table>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<tbody>
		<tr><th colspan="5">Info</th></tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td style="width: 127px;">Comments:</td>
			<td colspan="3" align="left">
			  <h:inputText id="comments" 
							       disabled="#{glExtractmapBackingBean.isDeleteAction}"
			               binding="#{glExtractmapBackingBean.comments}"
			               size="130"
			               maxlength="255"/>
			</td>
		</tr>
		<tr>
			<td align="left" >&#160;</td>
			<td style="width: 127px;">Last Update User ID:</td>
			<td>
			  <h:inputText id="lastUpdateID" 
                     disabled="true"
			               value="#{glExtractmapBackingBean.selectedMatrix.updateUserId}" size="30">
        </h:inputText>
			</td>
			<td>Last Update Timestamp:</td>
			<td>
			  <h:inputText id="lastUpdateTimestamp" 
                     disabled="true"
                     converter="dateTime"
			               value="#{glExtractmapBackingBean.selectedMatrix.updateTimestamp}" size="30">
        </h:inputText>
			</td>
		</tr>
    </tbody>
	</table>
	
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok">
      <h:commandLink id="updateAction"
                       rendered="#{glExtractmapBackingBean.isUpdateAction}"
                       reRender="msg"
                       action="#{glExtractmapBackingBean.updateAction}"/>
    </li>
		<li class="ok">
      <h:commandLink id="deleteAction"
         rendered="#{glExtractmapBackingBean.isDeleteAction}"
         action="#{glExtractmapBackingBean.deleteAction}"/>
    </li>
		<li class="ok">
      <h:commandLink id="addAction"
                       rendered="#{glExtractmapBackingBean.isAddAction}"
                       reRender="msg"
                       action="#{glExtractmapBackingBean.addAction}"/>
    </li>
		<li class="cancel">
      <h:commandLink immediate="true" id="cancelAction"
         action="#{glExtractmapBackingBean.cancelAction}"/>
    </li>
	</ul>
	</div>
	</div>
</div>

</h:form>
<ui:include src="/WEB-INF/view/components/taxcode_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.taxCodeHandler}"/>
	<ui:param name="popupName" value="searchTaxCode"/>
	<ui:param name="popupForm" value="searchTaxCodeForm"/>
	<ui:param name="reRender" value="glExtractAddForm:showTaxCode"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.driverHandler}"/>
</ui:include>
</ui:define>
</ui:composition>

</html>