<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="glExtractDeleteForm">
<h1><h:graphicImage id="imgGlExtractMap" alt="GL Extract Map" value="/images/headers/hdr-gl-extract-map.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6">Delete a G/L Extract Map</td></tr>
		</thead>
		<tbody>
			<tr><th colspan="6">TaxCode</th></tr>
			<tr style="width: 700px;">
			<td style="width: 10px;">&#160;</td>
			<td>&#160;</td>
			<td style="width: 220px;">
				<h:selectOneMenu id="selState" styleClass="NCSTSSelector" title="State"
                                   style="width: 215px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeStateCode}" disabled="true">
								   <f:selectItem id="selStateItm" itemLabel="Select a State" itemValue="-1"/>
									<f:selectItems id="selStateItms" value="#{glExtractmapBackingBean.stateComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 100px;">
				 <h:selectOneMenu id="selTaxCodeType" styleClass="NCSTSSelector" title="TaxCode Type"
                                   style="width: 95px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeTypeCode}" disabled="true">
								   <f:selectItem id="selTaxCodeTypeItm" itemLabel="Select a TaxCode Type" itemValue="-1"/>
								   <f:selectItems id="selTaxCodeTypeItms" value="#{glExtractmapBackingBean.listCodeComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 220px;">
				 <h:selectOneMenu id="selTaxCodeCode" styleClass="NCSTSSelector" title="TaxCode Code"
                                   style="width: 215px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeCode}" disabled="true">
								   <f:selectItem id="selTaxCodeCodeItm" itemLabel="Select a TaxCode Code" itemValue="-1"/>
								   <f:selectItems id="selTaxCodeCodeItms" value="#{glExtractmapBackingBean.taxCodeComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 220px;">
				 <h:selectOneMenu id="selTaxJur" styleClass="NCSTSSelector" title="Tax Jurisdiction Type Code"
                                   style="width: 215px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxJurisTypeCode}" disabled="true">
                </h:selectOneMenu>
			</td>
		</tr>
		</tbody>
		</table>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<tr >
			<td style="width: 10px;">&#160;</td>
			<td>&#160;</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_01']}" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_01']}"/></td>
			<td >
			
			<h:inputText id="driver01" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_01']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver01}" 
			size="20"></h:inputText>
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_02']}" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_02']}"/></td>
			<td >
			
			<h:inputText id="driver02" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_02']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver02}" 
			size="20"></h:inputText>
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_03']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_03']}"/></td>
			<td >
			
			<h:inputText id="driver03" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_03']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver03}" 
			size="20"></h:inputText>
			</td>
			
		</tr>
				<tr>
			<td style="width: 10px;">&#160;</td>
			<td>&#160;</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_04']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_04']}"/></td>
			<td >
			
			<h:inputText id="driver04" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_04']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver04}" 
			size="20"></h:inputText>
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_05']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_05']}"/></td>
			<td >
			
			<h:inputText id="driver05" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_05']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver05}" 
			size="20"></h:inputText>
				
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_06']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_06']}"/></td>
			<td >
			
			<h:inputText id="driver06" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_06']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver06}" 
			size="20"></h:inputText>
			</td>
		
		</tr>
				<tr>
			<td style="width: 10px;">&#160;</td>
			<td>&#160;</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_07']}" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_07']}"/></td>
				<td >
			
			<h:inputText id="driver07" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_07']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver07}" 
			size="20"></h:inputText>
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_08']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_08']}"/></td>
			<td >
			
			<h:inputText id="driver08" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_08']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver08}" 
			size="20"></h:inputText>
			</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_09']}"
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_09']}"/></td>
			<td >
			
			<h:inputText id="driver09" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_09']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver09}" 
			size="20"></h:inputText>	
				
			</td>
		</tr>
		<tr >
			<td style="width: 10px;">&#160;</td>
			<td><h:outputText value="#{matrixCommonBean.glExtractMap['DRIVER_10']}" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_10']}"/></td>
			<td >
			
			<h:inputText id="driver10" 
			rendered="#{!empty matrixCommonBean.glExtractMap['DRIVER_10']}"
			disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.driver10}" 
			size="20"></h:inputText>	
			</td>
		</tr>
</table>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<tr>
			<td>&#160;</td>
			<td>Comments:</td>
			<td>
			<h:inputText id="comments" disabled="true" value="#{glExtractmapBackingBean.selectedGlExtract.comments}" size="80"></h:inputText>
			</td>
		</tr>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{glExtractmapBackingBean.removeAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="mtns_glextract_main"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>