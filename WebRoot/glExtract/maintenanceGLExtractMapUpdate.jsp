<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<f:view>
<h:form id="glExtractUpdateForm">
<h1><h:graphicImage id="imgGlExtractMap" alt="GL Extract Map" value="/images/headers/hdr-gl-extract-map.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
	  <a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
  </div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6">Update a G/L Extract Map</td></tr>
		</thead>
		<tbody>
		<tr><th colspan="6">TaxCode</th></tr>
			<tr style="width: 700px;">
			<td style="width: 10px;">&#160;</td>
			<td>&#160;</td>
			<td style="width: 220px;">
				<h:selectOneMenu id="selState"  title="State"
                         style="width: 215px;" 
                         value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeStateCode}">
								   <f:selectItem id="selStateItm" itemLabel="Select a State" itemValue="-1"/>
									<f:selectItems id="selStateItms" value="#{glExtractmapBackingBean.stateComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 100px;">
				 <h:selectOneMenu id="selTaxCodeType" title="TaxCode Type"
                          style="width: 95px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeTypeCode}">
								   <f:selectItem id="selTaxCodeTypeItm" itemLabel="Select a TaxCode Type" itemValue="-1"/>
								   <f:selectItems id="selTaxCodeTypeItms" value="#{glExtractmapBackingBean.listCodeComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 220px;">
				 <h:selectOneMenu id="selTaxCodeCode" title="TaxCode Code"
                          style="width: 215px;" 
                          value="#{glExtractmapBackingBean.selectedGlExtract.taxCodeCode}">
								   <f:selectItem id="selTaxCodeCodeItm" itemLabel="Select a TaxCode Code" itemValue="-1"/>
								   <f:selectItems id="selTaxCodeCodeItms" value="#{glExtractmapBackingBean.taxCodeComboItems}" />
                </h:selectOneMenu>
			</td>
			<td style="width: 220px;">
				 <h:selectOneMenu id="selTaxJur" styleClass="NCSTSSelector" title="Tax Jurisdiction Type Code"
                                   style="width: 215px;" value="#{glExtractmapBackingBean.selectedGlExtract.taxJurisTypeCode}" disabled="true">
                </h:selectOneMenu>
			</td>
		</tr>
		<tr><th colspan="6">Drivers</th></tr>
		<tr>
			<td colspan="6">
			<h:panelGrid id="filterPanel" binding="#{glExtractmapBackingBean.filterSelectionPanel}" styleClass="panel-ruler"/>
			</td>
		</tr>
	</tbody>
</table>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<tr>
			<td>&#160;</td>
			<td>Comments:</td>
			<td colspan="7">
			<h:inputText id="comments" 
			value="#{glExtractmapBackingBean.selectedGlExtract.comments}" size="80"></h:inputText>
			</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Last Update User ID:</td>
			<td>
			  <h:inputText id="lastUpdateID" 
                     disabled="true"
			               value="#{glExtractmapBackingBean.selectedGlExtract.updateUserId}" size="30">
        </h:inputText>
			</td>
			<td>Last Update Timestamp:</td>
			<td>
			  <h:inputText id="lastUpdateTimestamp" 
                     disabled="true"
                     converter="dateTime"
			               value="#{glExtractmapBackingBean.selectedGlExtract.updateTimestamp}" size="30">
        </h:inputText>
			</td>
		</tr>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{glExtractmapBackingBean.updateGlExtract}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="mtns_glextract_main"/></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{glExtractmapBackingBean.driverHandler}"/>
</ui:include>
</f:view>
</ui:define>
</ui:composition>
</html>