<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/landing.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
function setFocus() {
    var el = document.getElementById("LoginForm:dbSelect");
    if (el && el.focus && el.disabled==false) {
        el.focus();
    }
}

function beginLogin() {
	var uName = document.getElementById("LoginForm:userControl"); 
	if (uName && uName.value == 'Enter User Name') {
		uName.value = '';
	}
}

function submitenter(e, objId) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;
	if (keycode == 13) {
		var obj = document.getElementById(objId);
		if (obj != null) { 
			if (obj.onclick) {
				obj.onclick();
			} else if (obj.click) {
				obj.click();
			}
			/*
			} else {
				var evt = obj.ownerDocument.createEvent('MouseEvents');
				evt.initMouseEvent('click', true, true, obj.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
				obj.dispatchEvent(evt);
			*/
		}
		return false;
	}
	return true;
}

registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body">

<h:form id="LoginForm">

	<div id="header">
		<div id="logo">PinPoint ${build.appversion}</div>
	</div>

	<div id="top-index" style="height: 320px;" >
		<div id="table-one-index" style="height: auto !important;" >
			<div id="table-one-top">
				<h:outputLabel 
                    style="color:red;font-weight:bold;font-size:10px" 
                    id="messageNoUser"
                    disabled="#{!empty loginBean.noUserMessage}"  
                    value="#{loginBean.noUserMessage}"/>
                <h:outputLabel 
                    style="color:red;font-weight:bold;font-size:15px" 
                    id="messageAppStartupError"
                    disabled="#{!empty loginBean.appStartupError}"  
                    value="#{loginBean.appStartupError}"/>
			</div>
				<div id="table-one-content-index" style="margin-left:auto;margin-right:auto; height: auto;">
					<table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-left:auto;margin-right:auto;text-align:center;">
						<tr>
							<td height="8px" colspan="3" >&nbsp;</td>
							
						</tr>
						<tr>
							<td align="center" colspan="3" >
								<h:graphicImage id="imgLogoPinPoint" alt="PinPoint_Logo" value="/images/login_logopinpoint.gif" />
					        </td>
					        
						</tr>
						<tr>
							<td height="8px" colspan="3">&nbsp;</td>
							
						</tr>
						<tr>
							<td align="center" colspan="3">
								<p>Enter your User Name and Password to launch this application.<br/>Need help with a <h:commandLink id="btnForgottenPassword" 
									style="text-decoration:underline;" value="forgotten password" 
                                    actionListener="#{loginBean.forgottenPasswordActionListener}" action="logout"/>?</p><br/>
								<h:outputLabel id="message" style="color:red" value="#{loginBean.message}"/>
							</td>
						</tr>
						<tr>
							<td align="right">
								<p>User ID:</p>
							</td>
							<td align="left" colspan="2" >
								<h:inputText id="userControl" styleClass="LoginControl" style="width: 150px;"
									value="#{loginBean.username}" 
									disabled="#{loginBean.validLogin}" />
					        </td>
						</tr>
						<tr>
							<td align="right">
								<p>Password:</p>
							</td>
							<td align="left" colspan="2" >
								 <h:inputSecret id="pwdControl" styleClass="LoginControl" style="width: 150px;" 
								 	value="#{loginBean.password}" onkeypress="return submitenter(event, 'LoginForm:btnLogin')"
                                    redisplay="true" disabled="#{loginBean.validLogin}"  />
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" colspan="3" >
							    <p>
							        <h:graphicImage id="imgLogoPinPointArrow" value="/images/STSarrow.gif" rendered="#{!loginBean.validLogin}" width="14" height="11"/>
                                    <h:commandLink id="btnLogin" value="#{!loginBean.validLogin ? 'Login' : ''}" 
                                    actionListener="#{loginBean.loginActionListener}" action="#{loginBean.loginAction}" disabled="#{loginBean.validLogin and not loginBean.validSelection}" /> 
                                    
                                    <h:outputLabel value="&#160;&#160;&#160;&#160;&#160;&#160;" />
                                    
                                <!--
                                    <h:outputLabel value="&#160;&#160;&#160;&#160;&#160;&#160;" />
                                                                    
                                    <h:commandLink id="btnLoginAdmin" value="Admin" 
                                    actionListener="#{loginBean.loginActionAdminListener}" action="#{loginBean.loginAction}"/> 
                                 -->   
                                    
                                    <!--  
                                    <h:outputLabel value="&#160;&#160;&#160;Admin?" />
                                    
                                    <h:selectBooleanCheckbox styleClass="check" id="chkAdmin"
										value="#{loginBean.adminUserFlag}">
									</h:selectBooleanCheckbox>-->
							    </p>
						    </td>
						</tr>
						<tr>
							<td width="209px" align="right">
								<p>Database:</p>
							</td>
							<td align="center" >
								<h:selectOneMenu id="dbSelect" style="width: 155px;"
									value="#{loginBean.selectedDb}" disabled="#{!loginBean.validLogin or loginBean.expiredLogin}">
									<f:selectItem id="selViewitm" itemLabel="Select a Database" itemValue="0"/>
									<f:selectItems id="selViewItms" value="#{loginBean.dbItems}" />
									<a4j:support event="onchange" reRender="messageNoUser,btnLogin,btnConnect"/>
								</h:selectOneMenu>
	
                                    
                                   
							</td>
							
							<td align="left" width="209px" >&nbsp;<h:commandLink id="btnConnect" value="#{loginBean.validSelection ? 'Connect' : ''}" 
                                    actionListener="#{loginBean.loginActionListener}" action="#{loginBean.loginAction}"  /> 
                            </td>
						</tr>
						<tr>
							<td height="7px" colspan="3" >&nbsp;</td>
						</tr>
						<tr>
							<td align="center" valign="middle" colspan="3" >
							    <p>
                                    <h:commandLink id="btnAdmin" value="Admin" rendered="#{loginBean.adminUserFlag}"
                                       action="#{loginBean.loginAdminAction}"/> 
                                      
                                    <h:outputLabel value="&#160;&#160;&#160;" rendered="#{loginBean.adminUserFlag}" />  
                                    
                                    <h:commandLink id="btnChangePassword" value="Change Password" rendered="#{loginBean.enableChangeFlag }"
                                       action="#{loginBean.loginChangePasswordAction}"/> 
                                    <h:outputLabel value="&#160;&#160;&#160;" rendered="#{loginBean.enableChangeFlag }" /> 
                                    
                                    <h:commandLink id="btnLogout" value="Logout" rendered="#{loginBean.enableCancelFlag }"
                                      actionListener="#{loginBean.logoutActionListener}" action="#{loginBean.logoutAction}" /> 
                 
							    </p>
						    </td>
						</tr>
						<tr>
							<td height="3px" colspan="3" >&nbsp;</td>
						</tr>
					</table>
					
				</div>
			<div id="table-one-bottom">
			</div>
		</div>
		
		
	</div>
	
	<div class="scrollInner" id="resize" >
		<table style="height:100%;">
			<tr >
				<td width="100%" ></td>
				<td align="right" style="vertical-align:bottom;padding-right:50px;">
					<h:graphicImage id="imgSubPinPoint" alt="Ryan" width="120" value="/images/ryan_logo.png" />
				</td>
			</tr>
		</table>
		
	</div>
		
</h:form>

</ui:define>

</ui:composition>
  
</html>
