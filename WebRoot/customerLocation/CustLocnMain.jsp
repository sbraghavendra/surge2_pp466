<ui:component xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('custlocnForm:custTable', 'selectedCustRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('custlocnForm:custLocnTable', 'selectedCustLocnRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
               
    function resetState()
	{ 
    	document.getElementById("custlocnForm:stateMenuMain").value = "";
	}
//]]>
</script>

<style>
.rich-stglpanel-marker {float: left}
.rich-stglpanel-header {text-align: left}
</style>


</ui:define>
<ui:define name="body">
<f:view>
<h:inputHidden id="selectedCustRowIndex" value="#{handlerBean.selectedCustRowIndex}" />
<h:inputHidden id="selectedCustLocnRowIndex" value="#{handlerBean.selectedCustLocnRowIndex}" />
    
<h:form id="custlocnForm">
     <h1><h:graphicImage id="imgCustomerLocationMaintenance" alt="Customers&#32;&#38;&#32;Locations" rendered="#{showSearchScreen and !showLookupScreen}" url="/images/headers/hdr-customerLocation-maintenance.gif" width="250" height="19" /></h1>
     <h1><h:graphicImage id="imgCustomerLocationlookup" alt="Customers&#32;&#38;&#32;Locations" rendered="#{!showSearchScreen and showLookupScreen}" url="/images/headers/hdr-customerLocation-lookup.gif" width="250" height="19" /></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
     	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{handlerBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(handlerBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       	</a4j:commandLink>					
     </div> 
     
     <a4j:outputPanel id="showhidefilter">
     	<c:if test="#{!handlerBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/cuslocn_panel.xhtml">
						<ui:param name="handlerBean" value="#{handlerBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel >
	      	</div>
	     	</div>
		</c:if>
	</a4j:outputPanel>
     
    <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-customerLocations.gif" width="192"  height="17" />
      </span>
    </div>
   
	<!-- table-four-contentNew  -->     
	<div id="table-four-contentNew">
        
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
    <tbody>
	<tr>
	<td style="height:auto;vertical-align:top;">
 
    <div id="bottom" style="padding: 0 0 3px 0;">
    	<div id="table-four"  >
	    <div id="table-four-top" style="padding: 3px 0 0 10px;" ><h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Customers&#160;&#160;&#160;&#160;&#160;" />
	    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{handlerBean.custDataModel.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	    </div>
       
        <div class="scrollInnerNew" id="resize" >
       	<rich:dataTable rowClasses="odd-row,even-row" id="custTable" rows="#{handlerBean.custDataModel.pageSize }"  value="#{handlerBean.custDataModel}" var="custMaintenance"  >
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('custlocnForm:custTable', this);"  actionListener="#{handlerBean.selectedCustRowChanged}"
                        reRender="addExempCert,custLocnTable,addCust,updateCust,deleteCust,viewCust,addCustLocn,updateCustLocn,deleteCustLocn,viewCustLocn,okLookup,custLocnDataTest,custLocnDisplayLabel" />
                                  	
           	<rich:column id="custId" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="custId-a4j"
                              styleClass="sort-#{handlerBean.custDataModel.sortOrder['custId']}"
                              value="Cust Id"
                              actionListener="#{handlerBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custTable" />
            	</f:facet>
            	<h:outputText value="#{custMaintenance.custId}" style="#{(custMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	          	
           	<rich:column id="custNbr" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="custNbr-a4j"
                              styleClass="sort-#{handlerBean.custDataModel.sortOrder['custNbr']}"
                              value="Customer Number"
                              actionListener="#{handlerBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custTable" />
            	</f:facet>
            	<h:outputText value="#{custMaintenance.custNbr}" style="#{(custMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="custName" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="custName-a4j"
                              styleClass="sort-#{handlerBean.custDataModel.sortOrder['custName']}"
                              value="Customer Name"
                              actionListener="#{handlerBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custTable" />
            	</f:facet>
            	<h:outputText value="#{custMaintenance.custName}" style="#{(custMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>  
           	
           	<rich:column id="activeFlag" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="activeFlag-a4j"
                              styleClass="sort-#{handlerBean.custDataModel.sortOrder['activeFlag']}"
                              value="Active?"
                              actionListener="#{handlerBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.activeFlag == 1}" disabled="#{true}" id="activeFlagId" />
           	</rich:column>
          </rich:dataTable>
          
          <a4j:outputPanel id="custDataTest">
          <c:if test="#{handlerBean.custDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
         
         </div>
         <!-- scroll-inner -->

       <rich:datascroller id="trScroll"  for="custTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{handlerBean.custDataModel.curPage}" />
                                  
       <div id="table-four-bottom">
        	<ul class="right">
         		<li class="add"><h:commandLink id="addCust" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="#{handlerBean.displayAddCustAction}" 
         									disabled="#{handlerBean.currentUser.viewOnlyBooleanFlag}" ></h:commandLink></li>      
           	<li class="update"><h:commandLink id="updateCust" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" rendered="#{showSearchScreen and !showLookupScreen}" disabled="#{!handlerBean.displayCustButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{handlerBean.displayUpdateCustAction}" ></h:commandLink></li>
         	<li class="delete115"><h:commandLink id="deleteCust" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" rendered="#{showSearchScreen and !showLookupScreen}" disabled="#{!handlerBean.displayCustDeleteButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{handlerBean.displayDeleteCustAction}" ></h:commandLink></li>
         	<li class="view"><h:commandLink id="viewCust" rendered="#{showSearchScreen and !showLookupScreen}" disabled="#{!handlerBean.displayCustButtons}" immediate="true"  action="#{handlerBean.displayViewCustAction}" ></h:commandLink></li>
	        <li class="close"><h:commandLink id="btnClose"  action="#{TaxMatrixViewBean.closeAction}" disabled="#{!entityBackingBean.isEntitySelected }"/></li>   
        	</ul>
       </div> <!-- table-four-bottom -->   
       <!-- end custGrid -->
       
       </div>
    </div>
	</td>
	
	<td style="height:auto;vertical-align:top;" >
  	<div id="bottom" style="padding: 0 0 3px 0;">
       <div id="table-four"  style="margin:0;" >
       <div id="table-four-top" style="padding: 3px 0 0 10px;"><h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Locations&#160;&#160;&#160;" />
       		<h:outputText id="custLocnDisplayLabel" value="#{handlerBean.custLocnDataModel.pageDescription}"/>
       </div>
       
        <div class="scrollInner"  id="resize1" >
         	<rich:dataTable rowClasses="odd-row,even-row" id="custLocnTable" rows="#{handlerBean.custLocnDataModel.pageSize }"  value="#{handlerBean.custLocnDataModel}" var="custLocnMaintenance" >
           	<a4j:support id="ref2tblLocn"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('custlocnForm:custLocnTable', this);"  actionListener="#{handlerBean.selectedCustLocnRowChanged}"
                        reRender="addExempCert,addCustLocn,updateCustLocn,deleteCustLocn,viewCustLocn,okLookup" />
                        
           	<rich:column id="custLocnCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="custLocnCode-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['custLocnCode']}"
                              value="Location Code"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.custLocnCode}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="locationName" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="locationName-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['locationName']}"
                              value="Location name"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.locationName}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="geocode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="geocode-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['geocode']}"
                              value="Geocode"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.geocode}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="city" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="city-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['city']}"
                              value="City"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.city}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="county" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="county-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['county']}"
                              value="County"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.county}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="state" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="state-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['state']}"
                              value="State"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{cacheManager.taxCodeStateMap[custLocnMaintenance.state].name} #{(not empty cacheManager.taxCodeStateMap[custLocnMaintenance.state].name) ? 
						'(' : ''}#{custLocnMaintenance.state}#{(not empty cacheManager.taxCodeStateMap[custLocnMaintenance.state].name) ? ')' : ''}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
	
           	</rich:column>
           	
           	<rich:column id="zip" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="zip-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['zip']}"
                              value="Zip Code"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{custLocnMaintenance.zip}" style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="country" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="country-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['country']}"
                              value="Country"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:outputText value="#{cacheManager.countryMap[custLocnMaintenance.country]} (#{custLocnMaintenance.country})"  style="#{(custLocnMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="activeFlagLocn" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="activeFlagLocn-a4j"
                              styleClass="sort-#{handlerBean.custLocnDataModel.sortOrder['activeFlag']}"
                              value="Active?"
                              actionListener="#{handlerBean.sortLocnAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="custLocnTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" value="#{custLocnMaintenance.activeFlag == 1}" disabled="#{true}" id="activeFlagLocnId" />
           		</rich:column>
          </rich:dataTable>

          <a4j:outputPanel id="custLocnDataTest">
           		<c:if test="#{handlerBean.custLocnDataModel.rowCount == 0 and handlerBean.custDataModel.rowCount == 0}">
					<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          		</c:if>
          		<c:if test="#{handlerBean.selectedCustRowIndex > 0 and handlerBean.custLocnDataModel.rowCount == 0 and handlerBean.custDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Locations exist for the Selected Customer." /></div>
          		</c:if>
          		<c:if test="#{handlerBean.selectedCustRowIndex == -1 and handlerBean.custLocnDataModel.rowCount == 0 and handlerBean.custDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Customer Selected." /></div>
          		</c:if>
           </a4j:outputPanel>

		</div>
        <!-- scroll-inner -->
        
       	<rich:datascroller id="trScrollLocn"  for="custLocnTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="custLocnDisplayLabel" page="#{handlerBean.custLocnDataModel.curPage}" />
                                  
       	<div id="table-four-bottom">
        	<ul class="right">
        	<li class="importCustLocn"><h:commandLink id="importCustLocn" disabled="#{handlerBean.currentUser.viewOnlyBooleanFlag}" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="custlocn" /></li>
         	<li class="add"><h:commandLink id="addCustLocn" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!handlerBean.displayCustButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="#{handlerBean.displayAddCustLocnAction}" /></li>          
         	<li class="update"><h:commandLink id="updateCustLocn" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!handlerBean.displayCustLocnButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="#{handlerBean.displayUpdateCustLocnAction}" /></li>
         	<li class="delete115"><h:commandLink id="deleteCustLocn" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!handlerBean.displayCustLocnDeleteButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true"  action="#{handlerBean.displayDeleteCustLocnAction}" /></li>
        	<li class="view"><h:commandLink id="viewCustLocn" disabled="#{!handlerBean.displayCustLocnButtons}" rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="#{handlerBean.displayViewCustLocnAction}" /></li>
        	<li class="addExempCert"><h:commandLink id="addExempCert"  rendered="#{showSearchScreen and !showLookupScreen}" immediate="true" action="#{handlerBean.displayExempCertAction}" /></li>
        	
        	<li class="ok2"><h:commandLink id="okLookup" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!handlerBean.displayCustLocnButtons or handlerBean.currentUser.viewOnlyBooleanFlag}" rendered="#{!showSearchScreen and showLookupScreen}"  action="#{handlerBean.selectLookupAction}" /></li>
            <li class="cancel"><h:commandLink id="cancelookup" style="#{(handlerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{handlerBean.currentUser.viewOnlyBooleanFlag}" rendered="#{!showSearchScreen and showLookupScreen}" immediate="true" action="#{handlerBean.canceLookupAction}" /></li>	
        	</ul>
       	</div> <!-- table-four-bottom -->  
       	<!-- end custLocnGrid --> 

      	</div>  <!-- table-four -->   
    </div>   <!-- bottom -->
	
	</td>
	</tr>
	</tbody>
	</table>

	</div>
    <!-- end of table-four-contentNew  -->  
     
</h:form>
</f:view>
   
<rich:modalPanel id="execute" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Execute" />
	</f:facet>
	<h:form id="executeForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('execute');" action="#{taxHolidayBean.executeAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('execute'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>
   
</ui:define>
</ui:composition>
</ui:component>
