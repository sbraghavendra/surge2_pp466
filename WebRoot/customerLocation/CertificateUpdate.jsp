<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk" >

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Customers&#32;&#38;&#32;Locations" value="/images/headers/hdr-exemptioncertificate-maintenance.gif"  /></h1>

<h:form id="custLocnUpdateForm" enctype="multipart/form-data" >
<div id="top">
	<div id="table-one">
	<div id="table-four-top"><h:messages errorClass="error" /></div>
	<div id="table-one-content" style="height:900px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{exemptionCertificateBean.actionExempText}"/></td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="7">Definition</th></tr>  
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Customer:</td>
			<td style="width: 200px;" >
				<h:inputText id="customerId" value="#{exemptionCertificateBean.updateCustCert.custNbr} - #{exemptionCertificateBean.updateCustCert.custName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Location:</td>
          	<td style="width: 200px;" >
				<h:inputText id="locationId" value="#{exemptionCertificateBean.updateCustCert.custLocnCode} - #{exemptionCertificateBean.updateCustCert.custLocnName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Entity:</td>
          	<td style="width: 200px;">
				<h:inputText id="entityId" value="#{exemptionCertificateBean.updateCustCert.entityCode} - #{exemptionCertificateBean.updateCustCert.entityName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Exemption Type:</td>	
          	<td style="width: 200px;">
				<h:inputText id="exceptionTypeId" value="#{exemptionCertificateBean.exemptionTypeMap[exemptionCertificateBean.updateCustCert.exemptionTypeCode]}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
	
		<tr><th colspan="1">Certificate</th><th colspan="2"><h:outputLabel style="width: 200px; color:red; font-weight:bold; align:left;" value="#{(exemptionCertificateBean.isAddFromValidate) ? 'Validates Temporary Exemption' : '&#160;&#160;'}" /></th><th colspan="4" style="width: 50%;">&#160;</th></tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Number:</td>
          	<td style="width: 200px;" >
				<h:inputText id="certNbrId" value="#{exemptionCertificateBean.updateCustCert.certNbr}" disabled="#{exemptionCertificateBean.isViewOnly}"
						 style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Exemption Reason:</td>
         	<td style="width: 200px;">
				<h:selectOneMenu id="exceptionReasonId" value="#{exemptionCertificateBean.updateCustCert.exemptReasonCode}" disabled="#{exemptionCertificateBean.isViewOnly}"
								  style="width:300px;" >
							<f:selectItems value="#{exemptionCertificateBean.updateExemptReasonMenuItems}"/>			
				</h:selectOneMenu>
          	</td>
          	<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Invoice Number:</td>
          	<td style="width: 200px;" >
				<h:inputText id="invoiceNbrId" value="#{exemptionCertificateBean.updateCustCert.invoiceNbr}" disabled="#{exemptionCertificateBean.isViewOnly}"
						 style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Effective Date:</td>
          <td style="width: 200px;">
           	<rich:calendar
                          id="updateEffectiveDate"
                          disabled="#{exemptionCertificateBean.isViewOnly}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                  		  converter="date"
                          datePattern="M/d/yyyy"
                          value="#{exemptionCertificateBean.updateCustCert.effectiveDate}" />
          </td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Expiration Date:</td>
          <td style="width: 200px;">
           	<rich:calendar
                          id="updateExpirationDate"
                          disabled="#{exemptionCertificateBean.isViewOnly}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{exemptionCertificateBean.updateCustCert.expirationDate}" />
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Name on Cert.:</td>
          	<td colspan="4">
				<h:inputText id="certCustNameId" value="#{exemptionCertificateBean.updateCustCert.certCustName}" disabled="#{exemptionCertificateBean.isViewOnly}"
						 style="width:100%;" /></td>

          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;" >Address Line 1:</td>
          	<td colspan="4" >
				<h:inputText id="addressLine1Id" value="#{exemptionCertificateBean.updateCustCert.addressLine1}" disabled="#{exemptionCertificateBean.isViewOnly}"
						 style="width:100%;" /></td>

          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Address Line 2:</td>
          	<td colspan="4" >
				<h:inputText id="addressLine2Id" value="#{exemptionCertificateBean.updateCustCert.addressLine2}" disabled="#{exemptionCertificateBean.isViewOnly}"
						 style="width:100%;" /></td>

          	<td>&#160;</td>
		</tr>
		</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>
		<tr>
			<td width="50px" >&#160;</td>
			<td style="align:left;">
			<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
				<ui:param name="handler" value="#{exemptionCertificateBean.filterHandler}"/>
				<ui:param name="id" value="jurisInput"/>
				<ui:param name="readonly" value="#{exemptionCertificateBean.isViewOnly}"/>
				<ui:param name="showheaders" value="true"/>
				<ui:param name="popupName" value="searchJurisdiction"/>
				<ui:param name="popupForm" value="searchJurisdictionForm"/>
				<ui:param name="reRender" value="custLocnUpdateForm:certStateId"/>
			</ui:include>
			</td>
		</tr>
	</tbody>
	</table>
	
	<table  cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">	
		<tbody>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Certificate State:</td>
         	<td style="width: 200px;">
				<h:selectOneMenu id="certStateId" value="#{exemptionCertificateBean.updateCustCert.certState}" disabled="#{exemptionCertificateBean.isViewOnly}"
								  style="width:300px;" >
							<f:selectItems value="#{exemptionCertificateBean.updateCertStateMenuItems}"/>			
				</h:selectOneMenu>
          	</td>
          	<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Certificate Type:</td>
         	<td style="width: 200px;">
				<h:selectOneMenu id="certTypeCodeId" value="#{exemptionCertificateBean.updateCustCert.certTypeCode}" disabled="#{exemptionCertificateBean.isViewOnly}"
								  style="width:300px;" >
							<f:selectItems value="#{exemptionCertificateBean.updateCertTypeMenuItems}"/>			
				</h:selectOneMenu>
          	</td>
          	<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Certificate Image:</td>
          	<td colspan="3" style="width: 400px;" >
				<t:inputFileUpload id="uploadFile"  binding="#{exemptionCertificateBean.uploadFile}" size="70" label="Certificate data file"
                     disabled="#{exemptionCertificateBean.isViewOnly}" immediate="true">                 		 
                </t:inputFileUpload>
			</td>
			
			<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">&#160;</td>
			<td style="width:400px;" colspan="2">File uploaded:<h:outputLabel style="width: 400px; color:red; font-weight:bold; align:left;" 
				value="#{(exemptionCertificateBean.updateCustCert.certImageUrl)}" /></td>	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Apply to State?:</td>
          	<td style="width: 200px;" >
				<h:selectBooleanCheckbox id="applyToStateBooleanFlagId"  disabled="#{exemptionCertificateBean.isViewOnly}"
						value="#{exemptionCertificateBean.updateCustCert.applyToStateBooleanFlag}" styleClass="check" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Blanket Cert.?:</td>
          	<td style="width: 200px;" >
				<h:selectBooleanCheckbox id="blanketBooleanFlagId"  disabled="#{exemptionCertificateBean.isViewOnly}"
						value="#{exemptionCertificateBean.updateCustCert.blanketBooleanFlag}" styleClass="check" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>


		<tr><th colspan="7">Partial Exemption</th></tr>
		
		 <tr>
          <td style="width: 50px;">&#160;</td>
          <td colspan="6">
          	  <div id="embedded-table">
           	  <table cellpadding="0" cellspacing="0" width="500" id="rollover" >
				<tbody>	
				<tr>
					<td style="width: 50px;">&#160;</td>
					<td style="width: 100px;">Exempt In:</td>
					<td style="width: 50px;">&#160;</td>
					
					<!--  
					<td style="width: 100px;">Percent of Base</td>
					<td style="width: 50px;">&#160;</td>
					<td style="width: 100px;">Special Rate:</td>	
					-->
				</tr>
				
				<tr>
					<td style="width: 50px;">Country?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="countryId" value="#{exemptionCertificateBean.updateCustCert.countryBooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
					<!-- 		<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.countryCheckboxSelected}" reRender="countryBaseChgPct,countrySpecialRate"/> -->
						</h:selectBooleanCheckbox>		
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!--  
					<td style="width: 100px;" >
						<h:inputText id="countryBaseChgPct" disabled="#{exemptionCertificateBean.isCountryCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.countryBaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="countrySpecialRate" disabled="#{exemptionCertificateBean.isCountryCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.countrySpecialRate}" style="width:100px;" /></td>
						-->		
				</tr>
						
		        <tr>  	
		          	<td style="width: 50px;">State?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stateId" value="#{exemptionCertificateBean.updateCustCert.stateBooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
					<!-- 		<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stateCheckboxSelected}" reRender="stateBaseChgPct,stateSpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stateBaseChgPct" disabled="#{exemptionCertificateBean.isStateCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stateBaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stateSpecialRate" disabled="#{exemptionCertificateBean.isStateCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stateSpecialRate}" style="width:100px;" /></td>
						 -->
				</tr>
				
				<tr>
		          	<td style="width: 50px;">County?:</td>
					<td style="width: 50px;">
							<h:selectBooleanCheckbox id="countyId"  value="#{exemptionCertificateBean.updateCustCert.countyBooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
				<!-- 			<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.countyCheckboxSelected}" reRender="countyBaseChgPct,countySpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="countyBaseChgPct" disabled="#{exemptionCertificateBean.isCountyCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.countyBaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="countySpecialRate" disabled="#{exemptionCertificateBean.isCountyCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.countySpecialRate}" style="width:100px;" /></td>
						-->
				</tr>
				
				<tr>
		          	<td style="width: 50px;">City?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="cityId"  value="#{exemptionCertificateBean.updateCustCert.cityBooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
					<!-- 		<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.cityCheckboxSelected}" reRender="cityBaseChgPct,citySpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="cityBaseChgPct" disabled="#{exemptionCertificateBean.isCityCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.cityBaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="citySpecialRate" disabled="#{exemptionCertificateBean.isCityCheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.citySpecialRate}" style="width:100px;" /></td>
						-->
				</tr>
						
				<tr>
					<td style="width: 50px;">STJ1?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stj1Id"  value="#{exemptionCertificateBean.updateCustCert.stj1BooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
						<!-- 	<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stj1CheckboxSelected}" reRender="stj1BaseChgPct,stj1SpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stj1BaseChgPct" disabled="#{exemptionCertificateBean.isStj1CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj1BaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stj1SpecialRate" disabled="#{exemptionCertificateBean.isStj1CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj1SpecialRate}" style="width:100px;" /></td>
						-->
				</tr>			

				<tr>
		          	<td style="width: 50px;">STJ2?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stj2Id"  value="#{exemptionCertificateBean.updateCustCert.stj2BooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
						<!-- 	<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stj2CheckboxSelected}" reRender="stj2BaseChgPct,stj2SpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stj2BaseChgPct" disabled="#{exemptionCertificateBean.isStj2CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj2BaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stj2SpecialRate" disabled="#{exemptionCertificateBean.isStj2CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj2SpecialRate}" style="width:100px;" /></td>
						-->
				</tr>
				
				<tr>
		          	<td style="width: 50px;">STJ3?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stj3Id"  value="#{exemptionCertificateBean.updateCustCert.stj3BooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
						<!-- 	<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stj3CheckboxSelected}" reRender="stj3BaseChgPct,stj3SpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stj3BaseChgPct" disabled="#{exemptionCertificateBean.isStj3CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj3BaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stj3SpecialRate" disabled="#{exemptionCertificateBean.isStj3CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj3SpecialRate}" style="width:100px;" /></td>
						-->
				</tr>
				
				<tr>
		          	<td style="width: 50px;">STJ4?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stj4Id"  value="#{exemptionCertificateBean.updateCustCert.stj4BooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
					<!-- 		<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stj4CheckboxSelected}" reRender="stj4BaseChgPct,stj4SpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stj4BaseChgPct" disabled="#{exemptionCertificateBean.isStj4CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj4BaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stj4SpecialRate" disabled="#{exemptionCertificateBean.isStj4CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj4SpecialRate}" style="width:100px;" /></td>
						-->
				</tr>
				
				<tr>
		          	<td style="width: 50px;">STJ5?:</td>
					<td style="width: 50px;">
						<h:selectBooleanCheckbox id="stj5Id"  value="#{exemptionCertificateBean.updateCustCert.stj5BooleanFlag}" disabled="#{exemptionCertificateBean.isViewOnly}" styleClass="check" >
					<!-- 		<a4j:support event="onclick" actionListener="#{exemptionCertificateBean.stj5CheckboxSelected}" reRender="stj5BaseChgPct,stj5SpecialRate"/> -->
						</h:selectBooleanCheckbox>
					</td>
					
					<td style="width: 50px;">&#160;</td>
					
					<!-- 
					<td style="width: 100px;" >
						<h:inputText id="stj5BaseChgPct" disabled="#{exemptionCertificateBean.isStj5CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj5BaseChgPct}" style="width:100px;" /></td>
					<td style="width: 50px;">&#160;</td>	
					<td style="width: 100px;" >
						<h:inputText id="stj5SpecialRate" disabled="#{exemptionCertificateBean.isStj5CheckboxSelected or exemptionCertificateBean.isViewOnly}" onkeypress="return onlyNumerics(event);" value="#{exemptionCertificateBean.updateCustCert.stj5SpecialRate}" style="width:100px;" /></td>
						-->
				</tr>	

				</tbody>
			</table>
			</div>
          </td>
          <td>&#160;</td>
        </tr>     
     
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Last Update User ID:</td>
          <td>
           	<h:inputText style="width: 200px;"  id="updateUserId"  disabled="true" rendered="true"
                          value="#{exemptionCertificateBean.updateCustCert.updateUserId}" />
          </td>
          <td style="width: 50px;">&#160;</td>
          	
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 120px;">Last Update Timestamp:</td>
          <td>
           	<h:inputText style="width: 200px;"  id="updateTimestamp"  disabled="true" rendered="true"
                          value="#{exemptionCertificateBean.updateCustCert.updateTimestamp}" >
                          <f:converter converterId="dateTime"/>
			</h:inputText>
          </td>
          
          <td style="width: 50px;">&#160;</td>
          	
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>

	</tbody>
	</table>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="upload"><h:commandLink id="btnUpload" disabled="#{exemptionCertificateBean.isViewOnly}" action="#{exemptionCertificateBean.fileUploadAction}" /></li>
		<li class="view115"><h:commandLink id="btnView" action="#{exemptionCertificateBean.viewFileAction}" /></li>
		<li class="ok-add-4"><h:commandLink id="btnOkAdd" disabled="#{!exemptionCertificateBean.isOkAddEnabled}" action="#{exemptionCertificateBean.okCertificateAddAction}" /></li>
		<li class="ok"><h:commandLink id="btnOk" action="#{exemptionCertificateBean.okCertificateAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{exemptionCertificateBean.displayViewAction}" action="#{exemptionCertificateBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	
</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{exemptionCertificateBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="custLocnUpdateForm:jurisInput"/>
</ui:include>

</ui:define>
</ui:composition>
</html>