<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Customers&#32;&#38;&#32;Locations" value="/images/headers/hdr-customerLocation-maintenance.gif"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:500px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{customerLocationSearchBean.actionText}"/> a Customer</td></tr>
	</thead>
	<tbody>			
		<tr>
			<th colspan="1"> Customer&#160;</th>
		    <th  colspan="6">
	      		
					<h:outputText style="color:red; font-weight:bold;" 
						value="#{(!customerLocationSearchBean.isAllowDeleteCustomer and customerLocationSearchBean.displayDeleteAction) ? 'Customer exists on Sale Transaction and may not be deleted.&#160;' : '&#160;&#160;'}"/>
					<h:outputText style="color:red; font-weight:bold;" 
						value="#{(customerLocationSearchBean.isAllowDeleteCustomer and customerLocationSearchBean.displayDeleteAction) ? 'All associated Locations, Exemptions, and Certificates will be deleted.&#160;' : '&#160;&#160;&#160;'}" />
			
			</th>
		</tr> 
		<tr>
			<td>&#160;</td>
			<td>Number:</td>
			<td>
				<h:inputText value="#{customerLocationSearchBean.updateCust.custNbr}" 
						disabled="#{!customerLocationSearchBean.displayAddAction}" id="txtcustNbr" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Name:</td>
			<td>
				<h:inputText value="#{customerLocationSearchBean.updateCust.custName}" 
						disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}" id="txtcustName" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
			<td >	 
				<h:selectBooleanCheckbox id="activeFlagId" 
					disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}"
					value="#{customerLocationSearchBean.updateCust.activeBooleanFlag}" styleClass="check" />
							 
			 </td>			         
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
						
		</tr>
		</tbody>
		</table>
		</div>
		</div>	
			
		<div id="table-four-bottom">
        <ul class="right">
     
        </ul>
    
	</div>
	</div>
	
	
   <div id="top" style="margin-top:10px;">
   <div id="table-one">
   <div id="table-four-top"></div>
   <div id="table-one-content" style="height:500px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7">Select Entities applicable for this Customer</td></tr>
	</thead>
	<tbody>	
      <tr>
      <th colspan="1">Selection&#160;Filter</th>
      <th colspan="6">
      	
      		<h:outputText value="&#160;&#160;"/>
     
      		
      </th>
		</tr>  
		
		<tr>
			<td>&#160;</td>
			<td>Level:</td>
			<td><h:selectOneMenu id="entityLevel"  value="#{customerLocationSearchBean.filterEntityItem.entityLevelId}"
					 style="width:300px;" >
  					<f:selectItems value="#{customerLocationSearchBean.entityLevelList}" /> 
  				</h:selectOneMenu></td>
			<td>&#160;</td>
			<td>&#160;</td>		
			<td>&#160;</td>	       
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Code:</td>
			<td >
				<h:inputText id="entityCode" value="#{customerLocationSearchBean.filterEntityItem.entityCode}" maxlength="50" 
						 style="width:300px;" /></td>          
			
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td >&#160;</td>
			<td>Name:</td>
			<td >
				<h:inputText id="entityName" value="#{customerLocationSearchBean.filterEntityItem.entityName}" maxlength="50" 
						  style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		
	</tbody>
	</table>
	</div>
	</div>
	
	<div id="table-four-bottom">
        <ul class="right">
        
        
         <li class="clear2">
				<h:commandLink  id="btnClear"  action="#{customerLocationSearchBean.resetFilterEntitySearchAction}"/>
			</li>
        <li class="search"><h:commandLink id="searchBtn" action="#{customerLocationSearchBean.retrieveFilterEntity}" /></li>
        </ul>
    </div>
</div>   

<div id="top" style="margin-top:10px;">  
	<div id="table-four" style="margin: 0 0px 0 0;">
	<div id="table-four-top"  style="padding-left:35px; padding-bottom: 0;">
	 <table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; padding-bottom: 0;">
				<tbody>
					<tr>					 
					<td width="100">
						<h:selectBooleanCheckbox id="selectallId" 
					disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}"
					value="#{customerLocationSearchBean.selectAll}" styleClass="check" >
					<a4j:support event="onclick" actionListener="#{customerLocationSearchBean.selectAllCheckboxChecked}"
						reRender="detailTable,pageInfo" 
						oncomplete="initScrollingTables();"  />
		               </h:selectBooleanCheckbox>&#160;	<h:outputText value="#{customerLocationSearchBean.selectAllCaption}"/>			
					</td>
						<td align="center">
						<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
				        	<a4j:status id="pageInfo"  
								startText="Request being processed..." 
								stopText="#{customerLocationSearchBean.entityDataModel.pageDescription }"
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
				     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
					</td>
					<td style="width:20%">&#160;</td>
					</tr>
				</tbody>
			</table>	
	
		
	   </div>
	<div id="table-four-content" >
    <div class="scrollContainer">
     <div class="scrollInner" id="resize">
     <table cellpadding="0" cellspacing="0" width="850px;" id="rollover" class="ruler">
	
	<tbody>	
     <tr><td>
		
      <rich:dataTable rowClasses="odd-row,even-row" id="detailTable" value="#{customerLocationSearchBean.retrieveEntityItemList}" rows="#{customerLocationSearchBean.entityDataModel.pageSize}" var="entityItem" >
       
        <rich:column id="select"  sortBy="#{entityItem.selectFlag}" style="text-align:center;" width="80px" >
        	<f:facet name="header"><a4j:commandLink id="aj4-select" value="Select"/></f:facet>
        	<h:selectBooleanCheckbox id="chkselect" value="#{entityItem.selectBooleanFlag}" styleClass="check" disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}">
          	<a4j:support event="onclick" actionListener="#{customerLocationSearchBean.displayChangeChecked}"></a4j:support></h:selectBooleanCheckbox>
       	</rich:column>
       
        <rich:column id="level" styleClass="column-left" sortBy="#{entityItem.entityLevelId}" >
        	<f:facet name="header"><a4j:commandLink id="aj4-level" value="Level"/></f:facet>
        	<h:outputText value="#{customerLocationSearchBean.entityLevelMap[entityItem.entityLevelId]}"  />
       	</rich:column>
       	
       	<rich:column id="entityCode" styleClass="column-left" sortBy="#{entityItem.entityCode}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-code" value="Code"/></f:facet>
        	<h:outputText value="#{entityItem.entityCode}"  />
       	</rich:column>
       	
       	<rich:column id="entityName" sortBy="#{entityItem.entityName}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-name" value="Name"/></f:facet>
        	<h:outputText value="#{entityItem.entityName}"  />
       	</rich:column>
       	
       	<rich:column id="geocode" styleClass="column-left" sortBy="#{entityItem.geocode}" >
        	<f:facet name="header"><a4j:commandLink id="aj4-geocode" value="Geocode"/></f:facet>
        	<h:outputText value="#{entityItem.geocode}"  />
       	</rich:column>
       	
       	<rich:column id="country" styleClass="column-left" sortBy="#{entityItem.country}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-country" value="Country"/></f:facet>
        	<h:outputText value="#{entityItem.country}"  />
       	</rich:column>
       	
       	<rich:column id="state" styleClass="column-left" sortBy="#{entityItem.state}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-state" value="State"/></f:facet>
        	<h:outputText value="#{entityItem.state}"  />
       	</rich:column>
       	
       	<rich:column id="county" styleClass="column-left" sortBy="#{entityItem.county}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-county" value="County"/></f:facet>
        	<h:outputText value="#{entityItem.county}"  />
       	</rich:column>
       	
       	<rich:column id="city" styleClass="column-left" sortBy="#{entityItem.city}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-city" value="City"/></f:facet>
        	<h:outputText value="#{entityItem.city}"  />
       	</rich:column>
       	
       	<rich:column id="zip" styleClass="column-left" sortBy="#{entityItem.zip}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-zipcode" value="Zip Code"/></f:facet>
        	<h:outputText value="#{entityItem.zip}"  />
       	</rich:column>
       	
       	<rich:column id="activeFlag" style="text-align:center; width=80px" sortBy="#{entityItem.activeFlag}"  >
        	<f:facet name="header"><a4j:commandLink id="aj4-active" value="Active?"/></f:facet>
        	<h:selectBooleanCheckbox value="#{entityItem.activeFlag=='1'? true:false}" styleClass="check" disabled="#{true}" />
       	</rich:column>
       
       
       	
      </rich:dataTable>
      </td>
      </tr>
      </tbody>
	</table>
     </div>
     <!-- scroll-inner -->
    </div>
    <!-- scroll-container -->
    </div>
		<rich:datascroller id="trScroll"  for="detailTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{customerLocationSearchBean.entityDataModel.curPage}" />
	<div id="bottom" >
	<div id="table-one">
	<div id="table-one-content" style="height:500px;overflow: hidden;">			
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<tbody>	
		<tr><th colspan="1"></th><th colspan="6"></th></tr>		
		<tr>
		<td>&#160;</td>
			<td>Last Update User ID:</td>
			<td style="width:300px;" colspan="2">
				<h:inputText value="#{customerLocationSearchBean.updateCust.updateUserId}" 							 
					 	 disabled="true" id="txtUpdateUserId" style="width:500px;" /></td>    </tr>    
			<tr><td>&#160;</td>
			<td>Last Update Timestamp:</td>
			<td style="width:300px;" colspan="2">
				<h:inputText value="#{customerLocationSearchBean.updateCust.updateTimestamp}" 							 
					 	 disabled="true" id="txtUpdateTimestamp" style="width:500px;">
			<f:converter converterId="dateTime"/>
			</h:inputText></td>
			<td>&#160;</td>
		</tr>
	</tbody>
	</table>
	
	</div>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok-add-locn1"><h:commandLink id="btnOkAddChild" disabled="#{!customerLocationSearchBean.displayAddAction}" action="#{customerLocationSearchBean.okCustAndAddAction}"  /></li>
		<li class="ok"><h:commandLink id="btnOk" value="" disabled="#{!customerLocationSearchBean.isAllowDeleteCustomer and customerLocationSearchBean.displayDeleteAction and !(customerLocationSearchBean.updateCust.activeFlag==1)}" action="#{customerLocationSearchBean.okCustAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{customerLocationSearchBean.displayViewAction}" action="#{customerLocationSearchBean.cancelAction}" /></li>
	</ul>
	</div>	
	
	</div>
	
</div>	
</div>

</h:form>


</ui:define>
</ui:composition>
</html>