<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Exemptions&#32;&#38;&#32;Certificates" value="/images/headers/hdr-exemptioncertificate-maintenance.gif"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top"><h:messages errorClass="error" /></div>
	<div id="table-one-content" style="height:900px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{exemptionCertificateBean.actionExempText}"/></td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="7">Definition</th></tr>  
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Customer:</td>
			<td style="width: 200px;" >
				<h:inputText id="customerId" value="#{exemptionCertificateBean.updateCustCert.custNbr} - #{exemptionCertificateBean.updateCustCert.custName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Location:</td>
          	<td style="width: 200px;" >
				<h:inputText id="locationId" value="#{exemptionCertificateBean.updateCustCert.custLocnCode} - #{exemptionCertificateBean.updateCustCert.custLocnName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Entity:</td>
          	<td style="width: 200px;">
				<h:inputText id="entityId" value="#{exemptionCertificateBean.updateCustCert.entityCode} - #{exemptionCertificateBean.updateCustCert.entityName}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Exemption Type:</td>	
          	<td style="width: 200px;">
				<h:inputText id="exceptionTypeId" value="#{exemptionCertificateBean.exemptionTypeMap[exemptionCertificateBean.updateCustCert.exemptionTypeCode]}" 
						disabled="true" style="width:100%;" /></td>
			<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr><th colspan="7">Exemption</th></tr>
		
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Effective Date:</td>
          <td style="width: 200px;">
           	<rich:calendar
                          id="updateEffectiveDate"
                          disabled="#{exemptionCertificateBean.displayViewAction}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{exemptionCertificateBean.updateCustCert.effectiveDate}" />
          </td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Expiration Date:</td>
          <td style="width: 200px;">
           	<rich:calendar
                          id="updateExpirationDate"
                          disabled="#{exemptionCertificateBean.displayViewAction}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{exemptionCertificateBean.updateCustCert.expirationDate}" />
          </td>
          <td>&#160;</td>
         </tr>
         
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 100px;">Exemption Reason:</td>
         	<td style="width: 200px;">
				<h:selectOneMenu id="exceptionReasonId" value="#{exemptionCertificateBean.updateCustCert.exemptReasonCode}" 
								  disabled="#{exemptionCertificateBean.displayViewAction}" style="width:300px;" >
							<f:selectItems value="#{exemptionCertificateBean.updateExemptReasonMenuItems}"/>			
				</h:selectOneMenu>
          	</td>
          	<td style="width: 50px;">&#160;</td>
          	
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		 <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Exempt In:</td>
          <td colspan="4">
           	  <div id="embedded-table">
           	  <table cellpadding="0" cellspacing="0" width="500" id="rollover" >
				<tbody>	
				<tr>
					<td style="width: 50px;">Country?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="countryId"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.countryBooleanFlag}" styleClass="check" />
					</td>			
		          	
		          	<td style="width: 50px;">State?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stateId" disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stateBooleanFlag}" styleClass="check" />
					</td>

		          	<td style="width: 50px;">County?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="countyId"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.countyBooleanFlag}" styleClass="check"  />
					</td>

		          	<td style="width: 50px;">City?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="cityId"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.cityBooleanFlag}" styleClass="check" />
					</td>
	
		          	<td>&#160;</td>
					<td>&#160;</td>
		          	<td>&#160;</td>
				</tr>
						
				<tr>
					<td style="width: 50px;">STJ1?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stj1Id"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stj1BooleanFlag}" styleClass="check" />
					</td>			

		          	<td style="width: 50px;">STJ2?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stj2Id"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stj2BooleanFlag}" styleClass="check" />
					</td>

		          	<td style="width: 50px;">STJ3?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stj3Id"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stj3BooleanFlag}" styleClass="check" />
					</td>

		          	<td style="width: 50px;">STJ4?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stj4Id"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stj4BooleanFlag}" styleClass="check" />
					</td>

		          	<td style="width: 50px;">STJ5?:</td>
					<td style="width: 50px;"><h:selectBooleanCheckbox id="stj5Id"  disabled="#{exemptionCertificateBean.displayViewAction}"
						value="#{exemptionCertificateBean.updateCustCert.stj5BooleanFlag}" styleClass="check" />
					</td>
					
					
		          	<td>&#160;</td>
				</tr>
				</tbody>
			</table>
 			</div>
          </td>
   
       
          <td>&#160;</td>
        </tr>
         
         
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 120px;">Last Update User ID:</td>
          <td>
           	<h:inputText style="width: 200px;"  id="updateUserId"  disabled="true" rendered="true"
                          value="#{exemptionCertificateBean.updateCustCert.updateUserId}" />
          </td>
          <td style="width: 50px;">&#160;</td>
          	
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 120px;">Last Update Timestamp:</td>
          <td>
           	<h:inputText style="width: 200px;"  id="updateTimestamp"  disabled="true" rendered="true"
                          value="#{exemptionCertificateBean.updateCustCert.updateTimestamp}" >
                          <f:converter converterId="dateTime"/>
			</h:inputText>
          </td>
          
          <td style="width: 50px;">&#160;</td>
          	
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>

	</tbody>
	</table>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{exemptionCertificateBean.okExemptionAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{exemptionCertificateBean.displayViewAction}" action="#{exemptionCertificateBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	
</h:form>
</ui:define>
</ui:composition>
</html>