<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Customers&#32;&#38;&#32;Locations" value="/images/headers/hdr-customerLocation-maintenance.gif"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{customerLocationSearchBean.actionText}"/> a Customer Location</td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="7">Customer</th></tr>  
		<tr>
			<td>&#160;</td>
			<td>Number:</td>
			<td style="width:300px;">
				<h:inputText value="#{customerLocationSearchBean.updateCust.custNbr}" 
						disabled="true" id="txtcustNbr" style="width:300px;" /></td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		<tr>
			<td>&#160;</td>
			<td>Name:</td>
			<td style="width:500px;" colspan="4">
				<h:inputText value="#{customerLocationSearchBean.updateCust.custName}" 
						disabled="true" id="txtcustName" style="width:100%;" /></td>
			<td style="width:40%">&#160;</td>
		</tr>

		<tr><th colspan="7">Location:</th></tr>
		<tr>
			<td>&#160;</td>
			<td>Code:</td>
			<td style="width:300px;" colspan="4">
				<h:inputText id="custLocnCode" value="#{customerLocationSearchBean.updateCustLocn.custLocnCode}" maxlength="40" 
						 disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayUpdateAction or customerLocationSearchBean.displayViewAction}" style="width:300px;" /></td>          
			
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Name:</td>
			<td style="width:500px;" colspan="4">
				<h:inputText id="locationName" value="#{customerLocationSearchBean.updateCustLocn.locationName}" maxlength="50" 
						 disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}" style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Address Line 1:</td>
			<td style="width:500px;" colspan="4">
				<h:inputText id="addressLine1" value="#{customerLocationSearchBean.updateCustLocn.addressLine1}" maxlength="50" 
						 disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}" style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td align="right">Line 2:</td>
			<td style="width:500px;" colspan="4">
				<h:inputText id="addressLine2" value="#{customerLocationSearchBean.updateCustLocn.addressLine2}" maxlength="50" 
						 disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}" style="width:100%;" /></td>
			<td>&#160;</td>
		</tr>
	
	</tbody>
	</table>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>
		<tr>
			<td width="50px" >&#160;</td>
			<td style="align:left;">
			<ui:include src="/WEB-INF/view/components/jurisdiction_screen.xhtml">
				<ui:param name="handler" value="#{customerLocationSearchBean.filterHandler}"/>
				<ui:param name="id" value="jurisInput"/>
				<ui:param name="readonly" value="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}"/>
				<ui:param name="showheaders" value="true"/>
				<ui:param name="popupName" value="searchJurisdiction"/>
				<ui:param name="popupForm" value="searchJurisdictionForm"/>
			</ui:include>
			</td>
		</tr>
	</tbody>
	</table>
				
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>			
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:300px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" 
					disabled="#{customerLocationSearchBean.displayDeleteAction or customerLocationSearchBean.displayViewAction}"
					value="#{customerLocationSearchBean.updateCustLocn.activeBooleanFlag}" styleClass="check"  />
							 
						 </td>
			             
			<td style="width:100%;">&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Last Update User ID:</td>
			<td style="width:300px;">
				<h:inputText value="#{customerLocationSearchBean.updateCustLocn.updateUserId}" 							 
					 	 disabled="true" id="txtUpdateUserId" style="width:200px;" /></td>        
			<td style="width:100%;">&#160;</td>
			<td>Last Update Timestamp:</td>
			<td style="width:300px;">
				<h:inputText value="#{customerLocationSearchBean.updateCustLocn.updateTimestamp}" 							 
					 	 disabled="true" id="txtUpdateTimestamp" style="width:200px;">
			<f:converter converterId="dateTime"/>
			</h:inputText></td>
			<td>&#160;</td>
		</tr>
	</tbody>
	</table>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok-add-exemp1"><h:commandLink id="btnOkAddChild" disabled="#{!customerLocationSearchBean.displayAddAction}" value="" action="#{customerLocationSearchBean.addCustLocnExAction}" /></li>
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{customerLocationSearchBean.okCustLocnAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{customerLocationSearchBean.displayViewAction}" action="#{customerLocationSearchBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{customerLocationSearchBean.filterHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="custLocnUpdateForm:jurisInput"/>
</ui:include>

</ui:define>
</ui:composition>
</html>