<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('custlocnForm:custTable', 'selectedCustRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedCustRowIndex" value="#{exemptionCertificateBean.selectedCustRowIndex}" />

<h:form id="custlocnForm">
<h1><h:graphicImage id="imgCustomerLocationMaintenance" alt="Exemptions&#32;&#38;&#32;Certificates" url="/images/headers/hdr-exemptioncertificate-maintenance.gif" width="250" height="19" /></h1>

<div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{exemptionCertificateBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(exemptionCertificateBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	</a4j:commandLink>					
</div> 

<a4j:outputPanel id="showhidefilter">
    	<c:if test="#{!exemptionCertificateBean.togglePanelController.isHideSearchPanel}">
     	<div id="top">
      	<div id="table-one">
			<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
				<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/certificates_panel.xhtml">
					<ui:param name="handlerBean" value="#{exemptionCertificateBean}"/>
					<ui:param name="readonly" value="false"/>					
				</a4j:include>
			</a4j:outputPanel >
      	</div>
     	</div>
	</c:if>
</a4j:outputPanel>
    
<div class="wrapper">
  <span class="block-right">&#160;</span>
  <span class="block-left tab">
   <h:graphicImage url="/images/containers/STSView-exemptionCertificates.gif" width="192"  height="17" />
  </span>
</div>

<div id="bottom">
    <!-- start custGrid -->
    <div id="table-four">
     <div id="table-four-top">
      <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
      <a4j:status id="pageInfo"  
		startText="Request being processed..." 
		stopText="#{exemptionCertificateBean.custCertDataModel.pageDescription }"
		onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
   			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     </div>
     
     <div id="table-four-content">
      <div class="scrollContainer">
       <div class="scrollInner"  id="resize">
        <rich:dataTable rowClasses="odd-row,even-row" id="custTable" rows="#{exemptionCertificateBean.custCertDataModel.pageSize }"  value="#{exemptionCertificateBean.custCertDataModel}" var="custMaintenance">
         	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                      onsubmit="selectRow('custlocnForm:custTable', this);"  actionListener="#{exemptionCertificateBean.selectedCustRowChanged}"
                      reRender="validateCust,addCust,updateCust,copyaddCust,deleteCust,viewCust,messageTest" />
                    
         	
         	<rich:column id="custNbr" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custNbr-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custNbr']}"
                            value="Customer No."
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.custNbr}" />
         	</rich:column>
         	
         	<rich:column id="custName" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custName-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custName']}"
                            value="Customer Name"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.custName}" />
         	</rich:column> 
         	
         	<rich:column id="custLocnCode" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnCode-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnCode']}"
                            value="Location Code"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.custLocnCode}" />
         	</rich:column> 
         	
         	<rich:column id="custLocnName" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnName-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnName']}"
                            value="Location Name"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.custLocnName}" />
         	</rich:column>   
         	
         	<!--  -->
         	<rich:column id="custLocnCountry" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnCountry-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnCountry']}"
                            value="Location Country"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{cacheManager.countryMap[custMaintenance.custLocnCountry]} (#{custMaintenance.custLocnCountry})"  />
         	</rich:column>
         	
         	<rich:column id="custLocnState" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnState-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnState']}"
                            value="Location State"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{cacheManager.taxCodeStateMap[custMaintenance.custLocnState].name} #{(not empty cacheManager.taxCodeStateMap[custMaintenance.custLocnState].name) ? 
	'(' : ''}#{custMaintenance.custLocnState}#{(not empty cacheManager.taxCodeStateMap[custMaintenance.custLocnState].name) ? ')' : ''}" />
         	</rich:column>
         	
         	<rich:column id="custLocnCity" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnCity-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnCity']}"
                            value="Location City"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.custLocnCity}" />
         	</rich:column>
         	
         	<rich:column id="custLocnActiveFlag" styleClass="column-center">
          	<f:facet name="header">
           	<a4j:commandLink id="custLocnActiveFlag-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['custLocnActiveFlag']}"
                            value="Location Active?"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.custLocnActiveFlag == 1}" disabled="#{true}" id="custLocnActiveFlagId" />       	
         	</rich:column>
         	<!--  -->
       	
         	<rich:column id="entityCode" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="entityCode-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['entityCode']}"
                            value="Entity Code"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.entityCode}" />
         	</rich:column> 
         	
         	<rich:column id="entityName" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="entityName-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['entityName']}"
                            value="Entity Name"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{custMaintenance.entityName}" />
         	</rich:column>
         	
         	<rich:column id="exemptionTypeCode" styleClass="column-left">
          	<f:facet name="header">
           	<a4j:commandLink id="exemptionTypeCode-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['exemptionTypeCode']}"
                            value="Exemption Type"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
          	</f:facet>
          	<h:outputText value="#{exemptionCertificateBean.exemptionTypeMap[custMaintenance.exemptionTypeCode]}" />
         	</rich:column>               
         	
         	
         	<rich:column id="effectiveDate" styleClass="column-left">
		<f:facet name="header">
		<a4j:commandLink id="effectiveDate-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['effectiveDate']}"
                            value="Effective Date"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
		</f:facet>
		<h:outputText id="roweffectiveDate" value="#{custMaintenance.effectiveDate}">
			<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
		</h:outputText>
	</rich:column>
	
	<rich:column id="expirationDate" styleClass="column-left">
		<f:facet name="header">
		<a4j:commandLink id="expirationDate-a4j"
                            styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['expirationDate']}"
                            value="Expiration Date"
                            actionListener="#{exemptionCertificateBean.sortAction}"
                            oncomplete="initScrollingTables();"
                            immediate="true"
                            reRender="custTable" />
		</f:facet>
		<h:outputText id="rowexpirationDate" value="#{custMaintenance.expirationDate}">
			<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
		</h:outputText>
	</rich:column>
				
	<rich:column id="certNbr" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="certNbr-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['certNbr']}"
                         value="Cert. No."
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{custMaintenance.certNbr}" />
      	</rich:column>
      	
      	<rich:column id="exemptReasonCode" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="exemptReasonCode-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['exemptReasonCode']}"
                         value="Reason"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{exemptionCertificateBean.exemptReasonMap[custMaintenance.exemptReasonCode]}" />
      	</rich:column>
      	
      	<rich:column id="invoiceNbr" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="invoiceNbr-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['invoiceNbr']}"
                         value="Invoice No."
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{custMaintenance.invoiceNbr}" />
      	</rich:column>
      	  
      	<rich:column id="certCustName" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="certCustName-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['certCustName']}"
                         value="Cert. Cust. Name"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{custMaintenance.certCustName}" />
      	</rich:column>
      	
      	<rich:column id="country" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="country-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['country']}"
                         value="Cert. Country"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{cacheManager.countryMap[custMaintenance.country]} (#{custMaintenance.country})"  />
      	</rich:column>
      	
      	<rich:column id="certState" styleClass="column-left">
       	<f:facet name="header">
        	<a4j:commandLink id="certState-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['certState']}"
                         value="Cert. State"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:outputText value="#{cacheManager.taxCodeStateMap[custMaintenance.certState].name} #{(not empty cacheManager.taxCodeStateMap[custMaintenance.certState].name) ? 
	'(' : ''}#{custMaintenance.certState}#{(not empty cacheManager.taxCodeStateMap[custMaintenance.certState].name) ? ')' : ''}" />

      	</rich:column>
      	
      	<rich:column id="blanketFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="blanketFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['blanketFlag']}"
                         value="Blanket?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.blanketFlag == 1}" disabled="#{true}" id="blanketFlagFlagId" />
      	</rich:column>
      	
      	<rich:column id="countryFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="countryFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['countryFlag']}"
                         value="Country?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.countryFlag == 1}" disabled="#{true}" id="countryFlagId" />
      	</rich:column>
      	
      	<rich:column id="stateFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stateFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stateFlag']}"
                         value="State?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stateFlag == 1}" disabled="#{true}" id="stateFlagId" />
      	</rich:column>
      	
      	<rich:column id="countyFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="countyFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['countyFlag']}"
                         value="County?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.countyFlag == 1}" disabled="#{true}" id="countyFlagId" />
      	</rich:column>
      	
      	<rich:column id="cityFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="cityFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['cityFlag']}"
                         value="City?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.cityFlag == 1}" disabled="#{true}" id="cityFlagId" />
      	</rich:column>
      	
      	<rich:column id="stj1Flag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stj1Flag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stj1Flag']}"
                         value="STJ1?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stj1Flag == 1}" disabled="#{true}" id="stj1FlagId" />
      	</rich:column>
      	
      	<rich:column id="stj2Flag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stj2Flag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stj2Flag']}"
                         value="STJ2?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stj2Flag == 1}" disabled="#{true}" id="stj2FlagId" />
      	</rich:column>
      	
      	<rich:column id="stj3Flag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stj3Flag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stj3Flag']}"
                         value="STJ3?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stj3Flag == 1}" disabled="#{true}" id="stj3FlagId" />
      	</rich:column>
      	
      	<rich:column id="stj4Flag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stj4Flag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stj4Flag']}"
                         value="STJ4?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stj4Flag == 1}" disabled="#{true}" id="stj4FlagId" />
      	</rich:column>
      	
      	<rich:column id="stj5Flag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="stj5Flag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['stj5Flag']}"
                         value="STJ5?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.stj5Flag == 1}" disabled="#{true}" id="stj5FlagId" />
      	</rich:column>
      	
      	<rich:column id="activeFlag" styleClass="column-center">
       	<f:facet name="header">
        	<a4j:commandLink id="activeFlag-a4j"
                         styleClass="sort-#{exemptionCertificateBean.custCertDataModel.sortOrder['activeFlag']}"
                         value="Customer Active?"
                         actionListener="#{exemptionCertificateBean.sortAction}"
                         oncomplete="initScrollingTables();"
                         immediate="true"
                         reRender="custTable" />
       	</f:facet>
       	<h:selectBooleanCheckbox styleClass="check" value="#{custMaintenance.activeFlag == 1}" disabled="#{true}" id="activeFlagId" />
      	</rich:column>
      	
	
     </rich:dataTable>
     
     <a4j:outputPanel id="messageTest">
       	<c:if test="#{exemptionCertificateBean.custCertDataModel.rowCount == 0 and exemptionCertificateBean.custCertDataModel.rowCount == 0}">
			<div class="nodatadisplay1"><h:outputText value="Click Search to retrieve." /></div>
		</c:if>
	 </a4j:outputPanel>
     
    </div>
    <!-- scroll-inner -->
   </div>
   <!-- scroll-container -->
  </div>
  <!-- table-four-content -->
  <rich:datascroller id="trScroll"  for="custTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                     stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{exemptionCertificateBean.custCertDataModel.curPage}" />
                             
  <div id="table-four-bottom">
   	<ul class="right">
   	<li class="validate"><h:commandLink id="validateCust" disabled="#{!exemptionCertificateBean.displayValidateButtons or exemptionCertificateBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{exemptionCertificateBean.displayValidateExempCertAction}" /></li>
    	<li class="add"><h:commandLink id="addCust" disabled = "#{exemptionCertificateBean.currentUser.viewOnlyBooleanFlag}" actionListener="#{exemptionCertificateBean.prepareAddValues}" action="#{exemptionCertificateBean.displayAddExempAction}" /></li> 
    	<li class="copy-add"><h:commandLink id="copyaddCust" disabled="#{!exemptionCertificateBean.displayCopyAddButtons or exemptionCertificateBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{exemptionCertificateBean.displayCopyAddExempCertAction}" /></li>   	         
    	<li class="update"><h:commandLink id="updateCust" disabled="#{!exemptionCertificateBean.displayCustButtons or exemptionCertificateBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{exemptionCertificateBean.displayUpdateExempCertAction}" /></li>
    	<li class="delete115"><h:commandLink id="deleteCust" disabled="#{!exemptionCertificateBean.displayCustDeleteButtons or exemptionCertificateBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{exemptionCertificateBean.displayDeleteExempCertAction}" /></li>
    	<li class="view"><h:commandLink id="viewCust" disabled="#{!exemptionCertificateBean.displayCustButtons}" immediate="true"  action="#{exemptionCertificateBean.displayViewExempCertAction}" /></li>
   	    <li class="close"><h:commandLink id="btnClose" action="#{exemptionCertificateBean.closeAction}" disabled="#{!exemptionCertificateBean.isCertificateSelected}"/></li>
   	</ul>
  </div> <!-- table-four-bottom -->   
  <!-- end custGrid -->

 </div>  <!-- table-four -->
 
</div>   <!-- bottom -->

</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{entityBackingBean.filterHandlerShipto}"/>
	<ui:param name="popupName" value="searchJurisdictionShipto"/>
	<ui:param name="popupForm" value="searchJurisdictionFormShipto"/>
	<ui:param name="jurisId" value="jurisIdShipto"/>
	<ui:param name="reRender" value="jurisInputShipto"/>
</ui:include>


</ui:define>
</ui:composition>
</html>