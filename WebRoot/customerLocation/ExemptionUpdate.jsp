<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Exemptions&#32;&#38;&#32;Certificates" value="/images/headers/hdr-exemptioncertificate-maintenance.gif"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top"><h:messages errorClass="error" /></div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="8"><h:outputText value="#{exemptionCertificateBean.actionExempText}"/></td></tr>
	</thead>
	<tbody>			
		<tr><th colspan="8">Definition</th></tr>  
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 150px;">Customer:</td>
         	<td style="width: 220px;">
         		<h:inputText id="customerId" value="#{exemptionCertificateBean.updateCustCert.custNbr}"  style="width:220px;" 
					 	disabled="#{exemptionCertificateBean.displayAddFromTransactionAction}"
					 	valueChangeListener="#{exemptionCertificateBean.recalCustomerNumber}" immediate="true" >
					 
					<a4j:support id="ajaxcustomerId" event="onblur" immediate="true"
							reRender="custNameId,entityId" />
	 
				</h:inputText>
          	</td>
          	
          	<td>&#160;</td>
          	<td style="width: 220px;">
         		<h:inputText id="custNameId" disabled="true" value="#{exemptionCertificateBean.updateCustCert.custName}" 
					 style="width:220px;" />
          	</td>
          	<td>&#160;</td>
          	<td>	
				<h:commandLink id="btnLookup" value="" styleClass="image"
						action="#{exemptionCertificateBean.searchCustomerLocationAction}" 
						rendered="#{!exemptionCertificateBean.displayAddFromTransactionAction}"
						>
						
						<h:graphicImage value="../images/search_small.png" />
          		</h:commandLink>
          	</td>
          	<td style="width: 30%;">&#160;</td>

		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 150px;" >Location:</td>
         	<td style="width: 220px;">
				<h:inputText id="locationId" value="#{exemptionCertificateBean.updateCustCert.custLocnCode}"  style="width:220px;" 
					 	disabled="#{exemptionCertificateBean.displayAddFromTransactionAction}"
					 	valueChangeListener="#{exemptionCertificateBean.recalLocationCode}" immediate="true" >
					 
					<a4j:support id="ajaxlocationId" event="onblur" immediate="true"
							reRender="custLocnNameId" />
				</h:inputText>		 

          	</td>
          	<td>&#160;</td>
          	<td style="width: 220px;">
         		<h:inputText id="custLocnNameId" disabled="true" value="#{exemptionCertificateBean.updateCustCert.custLocnName}" 
					 style="width:220px;" />
          	</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 150px;" >Entity:</td>
         	<td style="width: 220px;" colspan="3">
				<h:selectOneMenu id="entityId" value="#{exemptionCertificateBean.updateCustCert.entityId}" style="width:220px;" >
							<f:selectItems value="#{exemptionCertificateBean.selEntityMenuItems}"/>	
								
				</h:selectOneMenu>
          	</td>
    
          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>
		
		<tr>
			<td style="width: 50px;">&#160;</td>
          	<td style="width: 150px;" >Exemption Type:</td>    	
         	<td style="width: 220px;" colspan="3" >
				<h:selectOneMenu id="exceptionTypeId" value="#{exemptionCertificateBean.updateCustCert.exemptionTypeCode}" style="width:220px;" >
							<f:selectItems value="#{exemptionCertificateBean.selExemptTypeMenuItems}"/>			
				</h:selectOneMenu>
          	</td>

          	<td>&#160;</td>
          	<td>&#160;</td>
          	<td>&#160;</td>
		</tr>

	</tbody>
	</table>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{exemptionCertificateBean.displayAddExempCertAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{exemptionCertificateBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	
</h:form>

</ui:define>
</ui:composition>
</html>