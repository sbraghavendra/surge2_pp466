<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

  <ui:define name="script">

    <style type="text/css">
      .ie7 .scrollContainer {
        padding-top: 26px;
      }

      .ie7 .scrollContainer thead tr {
        height: 18px;
        vertical-align: top;
      }
      #table-one-content th{
        background-color: #ecf4fe;
        padding: 3px;
      }
    </style>
  </ui:define>

  <ui:define name="body">
    <h:form id="filterForm">


      <a4j:outputPanel id="msg" ><h:messages errorClass="error" /></a4j:outputPanel>

      <a4j:outputPanel id="showhidefilter">
        <div id="top" style="width:983px">
          <div id="table-one" >
            <div id="table-four-top">

            </div>

            <div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'filterForm:searchBtn')" >

              <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
                <thead>
                <tr><td style="align:left;" colspan="8">Update Batch User Field Descriptions</td></tr>
                </thead>
              </table>


              <table cellpadding="0" cellspacing="0" border="3" width="100%" id="rollover" class="ruler">
                <tbody>

                <tr>
                  <td colspan="9">
                    <table cellpadding="0" cellspacing="0" border="3" width="100%" id="rollover" class="ruler">
                      <tr>
                        <td colspan="3" class="column-label-bold">Batch type:</td>
                        <td colspan="1">
                          <h:selectOneMenu id="userFieldsBatchType"
                                           style="width:195px;"
                                           immediate="true"
                                           value="#{batchMaintenanceBean.selectedUserFieldsBmType}"
                                           binding="#{batchMaintenanceBean.batchTypeMenuForDefUserFields}"
                                           valueChangeListener="#{batchMaintenanceBean.userFieldsTypeChanged}">

                              <a4j:support event="onchange" action="#{batchMaintenanceBean.loadCurrentDTO}"
                                           reRender="textFieldFlag1,textField1,textFieldFlag2,textField2,textFieldFlag3,textField3,textFieldFlag4,textField4,textFieldFlag5,textField5,
                                      textFieldFlag6,textField6,textFieldFlag7,textField7,textFieldFlag8,textField8,textFieldFlag9,textField9,textFieldFlag10,textField10,
                                      numberFieldFlag1,numberField1,numberFieldFlag2,numberField2,numberFieldFlag3,numberField3,numberFieldFlag4,numberField4,numberFieldFlag5,numberField5,
                                      numberFieldFlag6,numberField6,numberFieldFlag7,numberField7,numberFieldFlag8,numberField8,numberFieldFlag9,numberField9,numberFieldFlag10,numberField10,
                                      dateFieldFlag1,dateField1,dateFieldFlag2,dateField2,dateFieldFlag3,dateField3,dateFieldFlag4,dateField4,dateFieldFlag5,dateField5,
                                      dateFieldFlag6,dateField6,dateFieldFlag7,dateField7,dateFieldFlag8,dateField8,dateFieldFlag9,dateField9,dateFieldFlag10,dateField10,
                                      txtUpdateUserId,txtUpdateTimestamp"
                                      />

                            </h:selectOneMenu>






                        </td>
                        <td colspan="5" style="width:700px;">&#160;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="9">
                    <ul class="basic-form">
                      <li class="sub-heading-dark">Text Fields</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">1:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          id="textFieldFlag1"
                          immediate="true"

                          value="#{batchMaintenanceBean.textFieldFlag1}"
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc01Desc}" reRender="textField1"/>
                    </h:selectBooleanCheckbox>
                  </td>

                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc01Desc}"

                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag1}"
                                 id="textField1" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:50px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">6:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.textFieldFlag6}"
                          immediate="true"
                          id="textFieldFlag6">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc06Desc}" reRender="textField6"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc06Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag6}"
                                 id="textField6" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">2:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          id="textFieldFlag2"
                          immediate="true"
                          value="#{batchMaintenanceBean.textFieldFlag2}"
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc02Desc}" reRender="textField2"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc02Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag2}"
                                 id="textField2" style="width:300px;"/>
                  </td>

                  <td colspan="1" style="width:50px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">7:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          immediate="true"
                          value="#{batchMaintenanceBean.textFieldFlag7}"
                          id="textFieldFlag7">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc07Desc}" reRender="textField7"/>
                    </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc07Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag7}"
                                 id="textField7" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">3:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.textFieldFlag3}"
                          immediate="true"
                          id="textFieldFlag3"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc03Desc}" reRender="textField3"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc03Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag3}"
                                 id="textField3" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:50px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">8:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.textFieldFlag8}"
                          immediate="true"
                          id="textFieldFlag8">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc08Desc}" reRender="textField8"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc08Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag8}"
                                 id="textField8" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">4:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="textFieldFlag4"
                          value="#{batchMaintenanceBean.textFieldFlag4}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc04Desc}" reRender="textField4"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc04Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag4}"
                                 id="textField4" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:50px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">9:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          value="#{batchMaintenanceBean.textFieldFlag9}"
                          immediate="true"
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="textFieldFlag9">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc09Desc}" reRender="textField9"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc09Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag9}"
                                 id="textField9" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">5:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          value="#{batchMaintenanceBean.textFieldFlag5}"
                          immediate="true"
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="textFieldFlag5"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc05Desc}" reRender="textField5"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc05Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag5}"
                                 id="textField5" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:50px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">10:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          value="#{batchMaintenanceBean.textFieldFlag10}"
                          immediate="true"
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="textFieldFlag10">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUvc10Desc}" reRender="textField10"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uvc10Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.textFieldFlag10}"
                                 id="textField10" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="9">
                    <ul class="basic-form">
                      <li class="sub-heading-dark">Number Fields</li>
                    </ul>
                  </td>
                </tr>


                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">1:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.numberFieldFlag1}"
                          immediate="true"
                          id="numberFieldFlag1"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu01Desc}" reRender="numberField1"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu01Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag1}"
                                 id="numberField1" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">6:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="numberFieldFlag6"
                          value="#{batchMaintenanceBean.numberFieldFlag6}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu06Desc}" reRender="numberField6"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu06Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag6}"
                                 id="numberField6" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">2:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="numberFieldFlag2"
                          value="#{batchMaintenanceBean.numberFieldFlag2}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu02Desc}" reRender="numberField2"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu02Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag2}"
                                 id="numberField2" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">7:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.numberFieldFlag7}"
                          immediate="true"
                          id="numberFieldFlag7">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu07Desc}" reRender="numberField7"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu07Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag7}"
                                 id="numberField7" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">3:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="numberFieldFlag3"
                          value="#{batchMaintenanceBean.numberFieldFlag3}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu03Desc}" reRender="numberField3"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu03Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag3}"
                                 id="numberField3" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">8:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.numberFieldFlag8}"
                          immediate="true"
                          id="numberFieldFlag8">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu08Desc}" reRender="numberField8"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu08Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag8}"
                                 id="numberField8" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">4:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="numberFieldFlag4"
                          value="#{batchMaintenanceBean.numberFieldFlag4}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu04Desc}" reRender="numberField4"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu04Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag4}"
                                 id="numberField4" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">9:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.numberFieldFlag9}"
                          immediate="true"
                          id="numberFieldFlag9">
                      <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu09Desc}" reRender="numberField9"/>
                    </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu09Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag9}"
                                 id="numberField9" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">5:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="numberFieldFlag5"
                          value="#{batchMaintenanceBean.numberFieldFlag5}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu05Desc}" reRender="numberField5"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu05Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag5}"
                                 id="numberField5" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">10:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.numberFieldFlag10}"
                          immediate="true"
                          id="numberFieldFlag10">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetNu10Desc}" reRender="numberField10"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.unu10Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.numberFieldFlag10}"
                                 id="numberField10" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>


                <tr>
                  <td colspan="9">
                    <ul class="basic-form">
                      <li class="sub-heading-dark">Date Fields</li>
                    </ul>
                  </td>
                </tr>


                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">1:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="dateFieldFlag1"
                          value="#{batchMaintenanceBean.dateFieldFlag1}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts01Desc}" reRender="dateField1"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts01Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag1}"
                                 id="dateField1" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">6:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.dateFieldFlag6}"
                          immediate="true"
                          id="dateFieldFlag6">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts06Desc}" reRender="dateField6"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts06Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag6}"
                                 id="dateField6" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">2:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="dateFieldFlag2"
                          value="#{batchMaintenanceBean.dateFieldFlag2}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts02Desc}" reRender="dateField2"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts02Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag2}"
                                 id="dateField2" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">7:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.dateFieldFlag7}"
                          immediate="true"
                          id="dateFieldFlag7">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts07Desc}" reRender="dateField7"/>
                    </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts07Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag7}"
                                 id="dateField7" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">3:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="dateFieldFlag3"
                          value="#{batchMaintenanceBean.dateFieldFlag3}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts03Desc}" reRender="dateField3"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts03Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag3}"
                                 id="dateField3" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">8:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.dateFieldFlag8}"
                          immediate="true"
                          id="dateFieldFlag8">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts08Desc}" reRender="dateField8"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts08Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag8}"
                                 id="dateField8" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">4:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="dateFieldFlag4"
                          value="#{batchMaintenanceBean.dateFieldFlag4}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts04Desc}" reRender="dateField4"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts04Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag4}"
                                 id="dateField4" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">9:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.dateFieldFlag9}"
                          immediate="true"
                          id="dateFieldFlag9">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts09Desc}" reRender="dateField9"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts09Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag9}"
                                 id="dateField9" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>

                <tr>
                  <td colspan="1" style="width:30px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">5:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          id="dateFieldFlag5"
                          value="#{batchMaintenanceBean.dateFieldFlag5}"
                          immediate="true"
                          >
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts05Desc}" reRender="dateField5"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts05Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag5}"
                                 id="dateField5" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:80px;">&#160;</td>
                  <td colspan="1" class="column-label-bold">10:</td>
                  <td colspan="1"><h:selectBooleanCheckbox
                          disabled="#{batchMaintenanceBean.disableComponents}"
                          value="#{batchMaintenanceBean.dateFieldFlag10}"
                          immediate="true"
                          id="dateFieldFlag10">
                    <a4j:support event="onchange" action="#{batchMaintenanceBean.resetUts10Desc}" reRender="dateField10"/>
                  </h:selectBooleanCheckbox>
                  </td>
                  <td colspan="1" style="width:300px;">
                    <h:inputText value="#{batchMaintenanceBean.currentBMDTO.uts10Desc}"
                                 disabled="#{batchMaintenanceBean.disableComponents or !batchMaintenanceBean.dateFieldFlag10}"
                                 id="dateField10" style="width:300px;"/>
                  </td>
                  <td colspan="1" style="width:10%;">&#160;</td>
                </tr>
                <tr>
                  <td colspan="9">

                    <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
                      <tbody>
                      <tr>
                        <td style="width:50px;">&#160;</td>
                        <td>Last Update User ID:</td>
                        <td>
                          <h:inputText value="#{batchMaintenanceBean.currentBMDTO.updateUserId}"
                                       disabled="true" id="txtUpdateUserId"
                                       style="width:650px;" />
                        </td>
                      </tr>
                      <tr>
                        <td style="width:50px;">&#160;</td>
                        <td>Last Update Timestamp:</td>
                        <td>
                          <h:inputText value="#{batchMaintenanceBean.currentBMDTO.updateTimestamp}"
                                       disabled="true" id="txtUpdateTimestamp"
                                       style="width:650px;">
                            <f:converter converterId="dateTime"/>
                          </h:inputText>
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </td>
                </tr>




                </tbody>
              </table>





            </div>
            <!-- table-one-content -->
            <div id="table-four-bottom">
              <ul class="right">
                <li class="ok"><h:commandLink id="okBtn" action="#{batchMaintenanceBean.userFieldsOkAction}"/></li>
                <li class="cancel">
                  <h:commandLink id="btnCancel" disabled="false"
                                 immediate="true"
                                 action="batch_maintenance_main" /></li>

              </ul>
            </div>

          </div>
        </div>
      </a4j:outputPanel>



    </h:form>


  </ui:define>
</ui:composition>
</html>
