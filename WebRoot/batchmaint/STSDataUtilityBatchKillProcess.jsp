<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>
<ui:define name="body">

<h1><img id="imgBatchMaintenanceStatus" alt="Batch Maintenance" src="../images/headers/hdr-batch-maintenance-status.gif" width="250" height="19" /></h1>
<h:form id="batchMaintStatusKillprocess">
<div id="top">
<div id="table-one">
	<div id="table-four-top" >
	</div>
	<div id="table-one-content" >
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead><tr style="height: 15px;"><td><h:outputText value="Confirm Batch Kill Processing"/></td></tr></thead>
    </table>
	</div>
	<div id="table-bare">
        <ul class="basic-form">
         <li class="sub-heading-light">Batch</li>
        </ul>
        <h:panelGrid id="batchPanel"
                     columns="2"
                     styleClass="basic-form"
                     columnClasses="column-form-label, column-form-data"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <h:outputText rendered="true"
                       value="Batch ID:" />
         <h:inputText id="batchIdTxt" size="40"
                      value="#{batchMaintenanceBean.selBatchMaintenance.batchId}"
                      immediate="true"
                      disabled="true" />
         <h:outputText rendered="true"
                       value="Batch Type Desc:" />
         <h:inputText id="batchTypeTxt" size="40"
                      value="#{batchMaintenanceBean.selBatchMaintenance.batchTypeDesc}"
                      immediate="true"
                      disabled="true" />
                      
         <h:outputText rendered="true"
                       value="Batch Status Desc:" />
         <h:inputText id="batchStatusDescTxt" size="40"
                      value="Kill Processing"
                      immediate="true"
                      disabled="true" />
                      
                        
         <h:outputText rendered="true"
                       value="Start Date/Time:" />
         
         <rich:calendar  id="startdatetime" 
						 value="#{batchMaintenanceBean.killBatchScheduleTime}"
						 disabled="true"
						 converter="date" datePattern="M/d/yyyy hh:mm a" 
						timeZone="#{loginBean.timeZone}"
						 inputClass="textbox"/>
        </h:panelGrid>
       </div>

	<div id="table-four-bottom">
    <ul class="right">
       <li class="ok"><h:commandLink id="btnOk"  action = "#{batchMaintenanceBean.killBatchProcessAction}" /></li>
       <li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="batch_maintenance_main" /></li>
    </ul>
	</div>
	
</div>
</div>
</h:form>

</ui:define>
</ui:composition>
</html>
