<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="taxRateViewForm">
          <h1>
            <img id="imgBatchMaintenanceStatus" src="../images/headers/hdr-batch-maintenance-status.gif"
                 width="250"
                 height="19" />
          </h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">Confirm Submit Batch for Delete</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             >
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="40" id="txtBatchId"
                               value="#{batchMaintenanceBean.deleteBatchIds}"
                               disabled="#{true}" />

			            <h:outputLabel value="Batch Type Desc:" />
                  <h:inputText size="40" id="txtBatchtypeDesc"
                               value="#{batchMaintenanceBean.deleteBatchTypeDesc}" 
                               disabled="#{true}" />

					        <h:outputLabel value="Batch Status Desc:" />
                  <h:inputText size="40" id="txtBatchStatusDesc"
                               value="#{cacheManager.listCodeMap['BATCHSTAT']['FD'].description}"
                               disabled="#{true}" />

                  <h:outputLabel value="Start Date/Time: "/>
                  <rich:calendar id="cDate"
                                 popup="true"
                                 binding="#{batchMaintenanceBean.batchStartTime}"
        						             enableManualInput="true"
                                 converter="dateTime"
                                 datePattern="MM/dd/yyyy hh:mm a"
                                 disabled="#{! batchMaintenanceBean.filePreferenceBean.batchPreferenceDTO.canChangeBatchStartTime}"/>
                </h:panelGrid>
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="ok2">
                      <h:commandLink id="ok2"
                                     action="#{batchMaintenanceBean.deleteAction}" />
                    </li>
                    <li class="cancel">
                      <h:commandLink id="cancelAction" immediate="true"
                                     action="#{batchMaintenanceBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
              </div>
              <!--  content -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
