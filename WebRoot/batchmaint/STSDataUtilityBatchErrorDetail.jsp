<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
     //<![CDATA[
     //]]>
   </script>
  </ui:define>
  <ui:define name="body">
   <f:view>
    <h:form id="batchMaintErrors">
     <h1>
      <img id="imgBatchErrors" src="../images/headers/hdr-batch-errors.gif"
           width="250"
           height="19" />
     </h1>
     <div id="table-four">
      <div id="table-four-top"></div>
      <div id="table-four-content">
       <ul class="basic-form">
        <li class="heading">View Batch Errors</li>
        <li class="sub-heading-dark">Error Detail</li>
       </ul>
       <h:panelGrid id="utilityPanelGrid1" columns="4"
                    styleClass="basic-form"
                    columnClasses="left-indent,right,right,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Process Type:" />
        <h:inputText size="20" id="txtProcessType"
                     value="#{batchErrorsBean.selectedError.processType}"
                     disabled="true" />
        <h:outputLabel styleClass="sub-label"
                       value="Process Date/Time:" />
        <h:inputText size="20" id="txtProcessTimestamp"
                     value="#{batchErrorsBean.selectedError.processTimestamp}"
                     disabled="true">
         <f:converter converterId="dateTime" />
        </h:inputText>
        <h:outputLabel value="Batch ID:" />
        <h:inputText size="20" id="txtBatchId"
                     value="#{batchErrorsBean.selectedError.batchId}"
                     disabled="true" />
        <h:outputLabel style="min-width: 20em !important"
                       value="Sequence ID:" />
        <h:inputText size="20" id="txtBatchErrorLogid"
                     value="#{batchErrorsBean.selectedError.batchErrorLogId}"
                     disabled="true" />
        <h:outputLabel value="Row No:" />
        <h:inputText size="20" id="txtRowNumber"
                     value="#{batchErrorsBean.selectedError.rowNumber}"
                     disabled="true" />
        <h:outputLabel value="Column No:" />
        <h:inputText size="20" id="txtColumnNumber"
                     value="#{batchErrorsBean.selectedError.columnNumber}"
                     disabled="true" />
       </h:panelGrid>
       <h:panelGrid id="utilityPanelGrid2" columns="2"
                    styleClass="basic-form"
                    columnClasses="left-indent,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Error Code:" />
        <h:panelGroup>
         <h:inputText size="20" id="txtErrorDefCode"
                      value="#{batchErrorsBean.selectedError.errorDefCode}"
                      disabled="true" />
         <rich:spacer width="4px" />
         <h:inputText size="73" id="txtErrorDefDesc"
                      value="#{batchErrorsBean.selectedError.errorDefDesc}"
                      disabled="true" />
        </h:panelGroup>
        <h:outputLabel value="Explanation:" />
        <h:inputTextarea rows="4"
                         cols="97" id="txtErrorDefExplanation"
                         value="#{batchErrorsBean.selectedError.errorDefExplanation}"
                         disabled="true" />
       </h:panelGrid>
       <h:panelGrid id="utilityPanelGrid3" columns="2"
                    styleClass="basic-form"
                    columnClasses="left-indent,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Severity Level:" />
        <h:panelGroup>
         <h:inputText size="20" id="txtSevLevel"
                      value="#{batchErrorsBean.selectedError.sevLevel}"
                      disabled="true" />
         <rich:spacer width="4px" />
         <h:inputText size="73" id="txtSevlevelDesc"
                      value="#{batchErrorsBean.selectedError.sevLevelDesc}"
                      disabled="true" />
        </h:panelGroup>
       </h:panelGrid>
       <h:panelGrid id="utilityPanelGrid4" columns="2"
                    styleClass="basic-form"
                    columnClasses="left-indent,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Import Header Column:" />
        <h:inputText size="97" id="txtImportHeaderColumn"
                     value="#{batchErrorsBean.selectedError.importHeaderColumn}"
                     disabled="true" />
        <h:outputLabel value="Import Column Value:" />
        <h:inputTextarea rows="4"
                         cols="97" id="txtImportColumnValue"
                         value="#{batchErrorsBean.selectedError.importColumnValue}"
                         disabled="true" />
       </h:panelGrid>
       <h:panelGrid id="utilityPanelGrid5" columns="2"
                    styleClass="basic-form"
                    columnClasses="left-indent,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Trans. Dtl. Column Name:" />
        <h:panelGroup>
         <h:inputText size="30" id="txtTransDtlColumnName"
                      value="#{batchErrorsBean.selectedError.transDtlColumnName}"
                      disabled="true" />
         <rich:spacer width="4px" />
         <h:inputText size="63" id="txtTransDtlColumnDesc"
                      value="#{batchErrorsBean.selectedError.transDtlColumnDesc}"
                      disabled="true" />
        </h:panelGroup>
       </h:panelGrid>
       <h:panelGrid id="utilityPanelGrid6" columns="2"
                    styleClass="basic-form"
                    columnClasses="left-indent,right"
                    cellspacing="0"
                    cellpadding="0">
        <h:outputLabel value="Trans. Dtl. Datatype:" />
        <h:inputText size="20" id="txtTransDtlDatatype"
                     value="#{batchErrorsBean.selectedError.transDtlDatatype}"
                     disabled="true" />
        <h:outputLabel value="Import Row:" />
        <h:inputTextarea rows="4"
                         cols="97" id="txtImportRow"
                         value="#{batchErrorsBean.selectedError.importRow}"
                         disabled="true" />
       </h:panelGrid>
      </div>
      <div id="table-four-bottom">
       <ul class="right">
        <li class="ok2">
         <h:commandLink id="cancelAction"
                        immediate="true"
                        action="batch_maintenance_errors" />
        </li>
       </ul>
      </div>
     </div>
    </h:form>
   </f:view>
  </ui:define>
 </ui:composition>
</html>
