<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
               registerEvent(window, "load", function() { selectRowByIndex('batchMaintStatusForm:batchTable', 'selectedRowIndex'); } );
               registerEvent(window, "load", adjustHeight);
    //]]>
   </script>
  </ui:define>
  <ui:define name="body">
   <f:view>
    <h:inputHidden id="selectedRowIndex"
                   value="#{batchMaintenanceBean.selectedRowIndex}" />
    <h:form id="batchMaintStatusForm">

    <h:panelGroup rendered="#{!batchMaintenanceBean.findMode}">
     	<h1><h:graphicImage  id="imgBatchMaintenanceStatus" alt="Batch Maintenance Status" url="/images/headers/hdr-batch-maintenance-status.gif" width="250" height="19" ></h:graphicImage></h1>
    </h:panelGroup>
	<h:panelGroup rendered="#{batchMaintenanceBean.findMode}">
		<h1><h:graphicImage  id="imgBatchMaintenanceStatus1" alt="Lookup Batch Id" url="/images/headers/hdr-lookup-batch-id.png" width="250" height="19" ></h:graphicImage></h1>
    </h:panelGroup>
     
     
 <a4j:outputPanel id="showhidefilter">
    <c:if test="#{!batchMaintenanceBean.togglePanelController.isHideSearchPanel}">
      <a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
        <h:inputHidden id="batchIdSelected"
                        value="#{batchMaintenanceBean.selBatchMaintenance.batchId}"
                        immediate="true" />
         <h:inputHidden id="batchTypeTxt"
                        value="#{batchMaintenanceBean.selBatchMaintenance.batchTypeDesc}"
                        immediate="true" />
         <h:inputHidden id="batchStatusDescription"
                        value="#{batchMaintenanceBean.selBatchMaintenance.batchStatusDesc}"
                        immediate="true" />
       </a4j:outputPanel >
     </c:if>
     </a4j:outputPanel>
     
     <div id="bottom">
     <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
  	 		<ui:param name="bean" value="#{batchMaintenanceBean}"/>
  	 		<ui:param name = "configBatches" value="false"/>
  	 		<ui:param name = "batchMaintenance" value="true"/>		
     </a4j:include>
      <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
      <div id="table-four">
       <div id="table-four-top">
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{batchMaintenanceBean.batchMaintenanceDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     			
       </div>
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"
              id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                          rows="#{batchMaintenanceBean.batchMaintenanceDataModel.pageSize }"
                          value="#{batchMaintenanceBean.batchMaintenanceDataModel}"
                          var="batchMaintenance">
           <a4j:support id="ref2tbl"
                        event="onRowClick"
                        onsubmit="selectRow('batchMaintStatusForm:batchTable', this);"
                        actionListener="#{batchMaintenanceBean.selectedRowChanged}"
                        reRender="batchMaintStatusForm:batchIdSelected, batchMaintStatusForm:batchTypeTxt, batchMaintStatusForm:batchStatusDescription, batchMaintStatusForm:defnUserFields, batchMaintStatusForm:updateBatch, batchMaintStatusForm:deleteBatch, batchMaintStatusForm:viewBatch, batchMaintStatusForm:statisticsBatch, batchMaintStatusForm:errorBatch, batchMaintStatusForm:holdBatch, batchMaintStatusForm:downloadFile,batchMaintStatusForm:okBtn,batchMaintStatusForm:killprocessBatch" />
           
           <rich:column style="text-align: center">
				<f:facet name="header">
					<h:outputText value="Selected?" />
				</f:facet>
				
				<h:selectBooleanCheckbox styleClass="check" id="chkSelected_#{batchMaintenance.batchId}" value="#{batchMaintenanceBean.batchMaintenanceDataModel.selectionMap[batchMaintenance.batchId]}" >
				</h:selectBooleanCheckbox>		
			</rich:column>
           
           <rich:column id="batchId" styleClass="column-right">
            <f:facet name="header">
             <a4j:commandLink id="batchId-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['batchId']}"
                              value="Batch Id"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
          <h:outputText value="#{batchMaintenance.batchId}" />
           </rich:column>
           
           <rich:column id="batchTypeDesc">
            <f:facet name="header">
             <h:outputText id="batchTypeoutpur" value="Batch Type" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.batchTypeDesc}" />
           </rich:column>
           <rich:column id="batchStatusDesc">
            <f:facet name="header">
             <h:outputText id="batchDescoutpur" value="Batch Status" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.batchStatusDesc}" />
           </rich:column>
           <rich:column id="errorSevCode">
            <f:facet name="header">
            <h:outputText id="errorSevOutPut" value="Error Severity" />
            </f:facet>
            <h:outputText value="#{cacheManager.listCodeMap['ERRORSEV'][batchMaintenance.errorSevCode].description}" />
           </rich:column>

           <rich:column id="vc01">
            <f:facet name="header">
                <h:outputText value="Variable Field 1" />
            </f:facet>
           <c:if test = "#{batchMaintenance.batchTypeCode == 'CI' and (empty batchMaintenance.vc01Desc) }" >
            	<h:outputText value="#{batchMaintenance.vc01}" />
            </c:if>
            <c:if test = "#{batchMaintenance.batchTypeCode == 'CI' and (not empty batchMaintenance.vc01Desc) }" >
            	<h:outputText value="#{batchMaintenance.vc01Desc}" />
            </c:if>
            <c:if test = "#{batchMaintenance.batchTypeCode != 'CI' }" >
            	<h:outputText value="#{batchMaintenance.vc01}" />
            </c:if>
           </rich:column>
           
           <rich:column id="vc02">
            <f:facet name="header">
                <h:outputText value="Variable Field 2" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.vc02}" />
           </rich:column>

			<rich:column rendered="#{!(empty batchMetadata.uts10Desc)}" id="uts10">
                  <f:facet name="header">
                      <a4j:commandLink id="uts10-a4j_#{loopCounter.index}"
                                       styleClass="sort-#{batchMaintenanceDataModel.sortOrder['uts10']}"
                                       value="#{batchMetadata.uts10Desc}"
                                       actionListener="#{batchMaintenance.sortAction}"
                                       oncomplete="initScrollingTables();"
                                       immediate="true"
                                       reRender="batchTable" />
                  </f:facet>
                  <h:outputText id="txtUts10_#{loopCounter.index}" value="#{ (batchMaintenance.batchTypeCode == batchMetadata.batchTypeCode)? batchMaintenance.uts10: null }">
                      <f:converter converterId="dateTime" />
                  </h:outputText>
              </rich:column>


           <rich:column id="totalBytes" styleClass="column-right">
            <f:facet name="header">
             <a4j:commandLink id="totalBytes-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['totalBytes']}"
                              value="Total Bytes"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.totalBytes}">
              <f:convertNumber />
            </h:outputText>
           </rich:column>           
           <rich:column id="totalRows" styleClass="column-right">
            <f:facet name="header">
             <a4j:commandLink id="totalRows-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['totalRows']}"
                              value="Total Rows"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.totalRows}">
             <f:convertNumber />
            </h:outputText>
           </rich:column>
           <rich:column id="schedStartTimestamp">
            <f:facet name="header">
             <a4j:commandLink id="schedStartTimestamp-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['schedStartTimestamp']}"
                              value="Sched Start"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText styleClass="dateFiledColTxt"
                          value="#{batchMaintenance.schedStartTimestamp}">
             <f:converter converterId="dateTime"/>
            </h:outputText>
           </rich:column>
           <rich:column id="actualStartTimestamp">
            <f:facet name="header">
             <a4j:commandLink id="ActualStart-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['actualStartTimestamp']}"
                              value="Actual Start"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.actualStartTimestamp}">
             <f:converter converterId="dateTime"/>
            </h:outputText>
           </rich:column>
           <rich:column id="actualEndTimestamp">
            <f:facet name="header">
             <a4j:commandLink id="Actualend-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['actualEndTimestamp']}"
                              value="Actual End"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.actualEndTimestamp}">
             <f:converter converterId="dateTime"/>
            </h:outputText>
           </rich:column>
           <rich:column id="entryTimestamp">
            <f:facet name="header">
             <a4j:commandLink id="entry-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['entryTimestamp']}"
                              value="Entry Timestamp"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:outputText value="#{batchMaintenance.entryTimestamp}">
             <f:converter converterId="dateTime"/>
            </h:outputText>
           </rich:column>
           <rich:column id="heldFlag">
            <f:facet name="header">
             <a4j:commandLink id="held-a4j"
                              styleClass="sort-#{batchMaintenanceBean.batchMaintenanceDataModel.sortOrder['heldFlag']}"
                              value="Held?"
                              actionListener="#{batchMaintenanceBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            </f:facet>
            <h:selectBooleanCheckbox styleClass="check"
                                     value="#{batchMaintenance.heldFlag == 1}"
                                     disabled="#{true}"
                                     id="hold" />
           </rich:column>
          </rich:dataTable>
          
           <a4j:outputPanel id="batchDataTest">
          <c:if test="#{batchMaintenanceBean.batchMaintenanceDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve."  /> </div>
          </c:if>
         </a4j:outputPanel>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="batchTable"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{batchMaintenanceBean.batchMaintenanceDataModel.curPage}" />
       <div id="table-four-bottom">
        <ul class="right">
        <h:panelGroup rendered="#{!batchMaintenanceBean.findMode}">
        <li class="defnuserfieldsbtn">
            <h:commandLink id="defnUserFields"
                disabled="#{batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                immediate="true"
                action="#{batchMaintenanceBean.displayDefnUserFieldsAction}" />
        </li>
         <li class="update">
          <h:commandLink id="updateBatch"
                         disabled="#{!batchMaintenanceBean.displayButtons or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         immediate="true"
                         action="#{batchMaintenanceBean.displayUpdateAction}" />
         </li>

         <li class="delete115">
          <h:commandLink id="deleteBatch"
                         disabled="#{!batchMaintenanceBean.displayDelete or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         immediate="true"
                         action="#{batchMaintenanceBean.deleteSelectedAction}" />
         </li>

         <li class="view">
          <h:commandLink id="viewBatch"
                         disabled="#{!batchMaintenanceBean.displayButtons}"
                         immediate="true"
                         action="#{batchMaintenanceBean.viewAction}" />
         </li>
         <li class="statistics">
          <h:commandLink id="statisticsBatch"
                         disabled="#{!batchMaintenanceBean.displayButtons or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         action="#{batchMaintenanceBean.statisticsAction}" />
         </li>
         <li class="error3">
          <h:commandLink id="errorBatch"
                         disabled="#{!batchMaintenanceBean.displayError or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         action="#{batchMaintenanceBean.errorsAction}" />
         </li>
         <li class="hold">
          <h:commandLink id="holdBatch"
                         disabled="#{!batchMaintenanceBean.displayHoldButtons or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         action="#{batchMaintenanceBean.holdAction}" />
         </li>
         
         <li class="download">	         
	         <h:commandLink id="downloadFile" disabled="#{!batchMaintenanceBean.displayDownload or batchMaintenanceBean.currentUser.viewOnlyBooleanFlag}" 
	         				style="#{(batchMaintenanceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" immediate="true"
	              action="#{batchMaintenanceBean.downloadActionRedirect}" />
         </li>
         
          <li class="killprocess">
          <h:commandLink id="killprocessBatch"
                         disabled="#{!batchMaintenanceBean.displayButtons or batchMaintenanceBean.displayKillProcessButton}"
                         immediate="true"
                         action="#{batchMaintenanceBean.displayKillProcessAction}" />
         </li>

        </h:panelGroup>
		<h:panelGroup rendered="#{batchMaintenanceBean.findMode}">
			<li class="ok2"><h:commandLink id="okBtn" disabled="#{!batchMaintenanceBean.displayButtons}" immediate="true" action="#{batchMaintenanceBean.findAction}"/></li>
			<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{batchMaintenanceBean.cancelFindAction}" /></li>
		</h:panelGroup> 
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
   </f:view>
  </ui:define>
 </ui:composition>
</html>
