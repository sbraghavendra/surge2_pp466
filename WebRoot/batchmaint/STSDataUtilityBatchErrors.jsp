<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
      registerEvent(window, "load", function() { selectRowByIndex('form:processList', 'selectedProcessRowIndex'); } );
      registerEvent(window, "load", function() { selectRowByIndex('form:errorList', 'selectedErrorRowIndex'); } );
   //]]>
   </script>
  </ui:define>
  <ui:define name="body">
   <h:inputHidden id="selectedProcessRowIndex"
                  value="#{batchErrorsBean.selectedProcessRowIndex}" />
   <h:inputHidden id="selectedErrorRowIndex"
                  value="#{batchErrorsBean.selectedErrorRowIndex}" />
   <f:view>
    <h:form id="form">
     <h1>
      <img id="imgBatchErrors" alt="Batch Errors" src="../images/headers/hdr-batch-errors.gif" />
     </h1>
     <div id="top"
          style="margin-top:6px">
      <div id="table-one">
       <div id="table-four-top"></div>
       <div id="table-one-content">
        <ul class="basic-form">
         <li class="heading">View Batch Errors</li>
         <li class="sub-heading-dark">Batch</li>
        </ul>
        <h:panelGrid id="batchErrors"
                     columns="7"
                     styleClass="basic-form"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <rich:spacer width=".5em" />
         <h:outputLabel for="batchId"
                        value="Batch Number:" />
         <h:inputText id="batchId"
                      size="20"
                      value="#{batchErrorsBean.batchMaintenance.batchId}"
                      disabled="true" />
         <h:outputLabel for="batchType"
                        value="Batch Type:" />
         <h:inputText id="batchType"
                      size="20"
                      value="#{batchErrorsBean.batchMaintenance.batchTypeDesc}"
                      disabled="true" />
         <h:outputLabel for="status"
                        value="Status:" />
         <h:inputText id="status"
                      size="20"
                      value="#{batchErrorsBean.batchMaintenance.batchStatusDesc}"
                      disabled="true" />
         <rich:spacer width=".5em" />
         <h:outputLabel for="timestamp"
                        value="Entry Date/time:" />
         <h:inputText id="timestamp"
                      size="20"
                      value="#{batchErrorsBean.batchMaintenance.entryTimestamp}"
                      disabled="true">
          <f:converter converterId="dateTime"/>
         </h:inputText>
         <h:outputLabel for="errorSevDesc"
                        value="Error Sev:" />
         <h:inputText id="errorSevDesc"
                      size="20"
                      value="#{cacheManager.listCodeMap['ERRORSEV'][batchErrorsBean.batchMaintenance.errorSevCode].description}"
                      disabled="true" />
         <h:outputLabel for="heldFlag"
                        value="Held?:" />
         <h:selectBooleanCheckbox id="heldFlag"
                                  styleClass="check"
                                  value="#{batchErrorsBean.batchMaintenance.heldFlag == 1}"
                                  disabled="#{true}" />
        </h:panelGrid>
       </div>
       <!-- content -->
      </div>
      <!-- table-one -->
     </div>
     <!-- top -->
     <div id="bottom">
      <div id="table-four">
       <div id="table-four-content">
        <ul class="basic-form">
         <li class="sub-heading-light">Process</li>
        </ul>
        <div class="scrollContainer">
         <div class="scrollInner"
              style="height:80px">
          <rich:dataTable rowClasses="odd-row,even-row" id="processList"
                          width="700"
                          sortMode="single"
                          value="#{batchErrorsBean.processList}"
                          var="processList">
           <a4j:support event="onRowClick"
                        onsubmit="selectRow('form:processList', this);"
                        actionListener="#{batchErrorsBean.selectedProcessRowChanged}"
                        reRender="form:errorList,form:pageInfo, form:saveAction" />
           <rich:column id="processTypeDesc"
                        style="text-align: left;"
                        width="400">
            <f:facet name="header">
             <h:outputText value="Process Type" />
            </f:facet>
            <h:outputText value="#{cacheManager.listCodeMap['PROCTYPE'][processList.processType].description}" />
           </rich:column>
           <rich:column id="processTimestamp"
                        style="text-align: left;"
                        width="200">
            <f:facet name="header">
             <h:outputText value="Process Timestamp" />
            </f:facet>
            <h:outputText value="#{processList.processTimestamp}">
             <f:converter converterId="dateTime"/>
            </h:outputText>
           </rich:column>
          </rich:dataTable>
         </div>
        </div>
        <ul class="basic-form">
         <li class="sub-heading-light">Errors</li>
        </ul>
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        	<a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{batchErrorsBean.batchErrorDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     			
        <div class="scrollContainer">
         <div class="scrollInner" id="resize">
         
          <rich:dataTable rowClasses="odd-row,even-row" id="errorList"
                          sortMode="single"
                          rows="#{batchErrorsBean.batchErrorDataModel.pageSize }"                       
                          value="#{batchErrorsBean.batchErrorDataModel}"
                          var="errorDTO">
           <a4j:support event="onRowClick"
                        onsubmit="selectRow('form:errorList', this);"
                        actionListener="#{batchErrorsBean.selectedErrorRowChanged}"
                        reRender="form:viewAction" />
           <rich:column id="rowNumber"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Row No" />
            </f:facet>
            <h:outputText value="#{errorDTO.rowNumber}" />
           </rich:column>
           <rich:column id="columnNumber"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Column No" />
            </f:facet>
            <h:outputText value="#{errorDTO.columnNumber}" />
           </rich:column>
           <rich:column id="errorDefDesc"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Message" />
            </f:facet>
            <h:outputText value="#{errorDTO.errorDefDesc}" />
           </rich:column>
           <rich:column id="importHeaderColumn"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Import header column" />
            </f:facet>
            <h:outputText value="#{errorDTO.importHeaderColumn}" />
           </rich:column>
           <rich:column id="importColumnValue"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Import Column Value" />
            </f:facet>
            <h:outputText value="#{errorDTO.importColumnValue}" />
           </rich:column>
           <rich:column id="transDtlColumnName"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Trans Dtl Column Name" />
            </f:facet>
            <h:outputText value="#{errorDTO.transDtlColumnName}" />
           </rich:column>
           <rich:column id="transDtlColumnDesc"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Trans Dtl Column Desc" />
            </f:facet>
            <h:outputText value="#{errorDTO.transDtlColumnDesc}" />
           </rich:column>
           <rich:column id="transDtlDatatype"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Trans Dtl DataType" />
            </f:facet>
            <h:outputText value="#{errorDTO.transDtlDatatype}" />
           </rich:column>
           <rich:column id="importRow"
                        style="text-align: left;">
            <f:facet name="header">
             <h:outputText value="Import Row" />
            </f:facet>
            <h:outputText value="#{errorDTO.importRowAbbreviated}" />
           </rich:column>
          </rich:dataTable>
         </div>
         <!-- scroll inner -->
        </div>
        <!-- scroll container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="errorList"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{batchErrorsBean.batchErrorDataModel.curPage}" />
       <div id="table-four-bottom">
        <ul class="right">
         <li class="view">
          <h:commandLink id="viewAction"
                         disabled="#{! batchErrorsBean.viewEnabled}"
                         action="#{batchErrorsBean.viewAction}" />
         </li>
<!-- 0001701: Import errors make the system crash       
         <li class="saveas">
          <h:commandLink id="saveAction"
                         disabled="#{! batchErrorsBean.saveAsEnabled}"
                         action="#{batchErrorsBean.saveAsAction}" />
         </li>
 -->
         <li class="ok2">
          <h:commandLink id="cancelAction"
                         action="#{batchErrorsBean.cancelAction}" />
         </li>
        </ul>
       </div>
       <!-- t4-bottom -->
      </div>
      <!-- t4 -->
     </div>
     <!-- bottom -->
     <!--  <a4j:log popup="false" level="ALL" style="width:800px; height:300px;"></a4j:log> -->
    </h:form>
   </f:view>
  </ui:define>
 </ui:composition>
</html>
