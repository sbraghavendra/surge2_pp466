<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   //]]>
   </script>
  </ui:define>
  <ui:define name="body">
   <f:view>
    <h:form id="batchMaintStatusUpdate">
     <h1>
      <img id="imgBatchMaintenanceStatus" alt="Batch Maintenance" src="../images/headers/hdr-batch-maintenance-status.gif"
           width="250"
           height="19" />
     </h1>
     <div id="top">
      <div id="table-one">
       <div id="table-one-top">
        <a4j:outputPanel id="msg">
         <h:messages errorClass="error" />
        </a4j:outputPanel>
       </div>
       <div id="table-bare">
        <ul class="basic-form">
         <li class="sub-heading-dark">#{batchMaintenanceBean.currentAction} a Batch</li>
         <li class="sub-heading-light">Batch Details</li>
        </ul>
        <h:panelGrid id="batchPanel"
                     columns="4"
                     styleClass="basic-form"
                     columnClasses="column-form-label, column-form-data, column-form-label, column-form-data"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <h:outputText rendered="true"
                       value="Batch ID:" />
         <h:inputText id="batchIdTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.batchId}"
                      immediate="true"
                      disabled="true" />
         <h:outputText rendered="true"
                       value="Batch Type:" />
         <h:inputText id="batchTypeTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.batchTypeDesc}"
                      immediate="true"
                      disabled="true" />
         <h:outputText rendered="true"
                       value="Entry Date/Time:" />
         <h:inputText id="batchEntryDtTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.entryTimestamp}"
                      disabled="true">
          <f:converter converterId="dateTime" />
         </h:inputText>
         <h:outputText rendered="true"
                       value="Highest Error Severity:" />
         <h:inputText id="batchErrSrvCdTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.errorSevCode}"
                      immediate="true"
                      disabled="true" />
         <h:outputText rendered="true"
                       value="Batch Status:" />
         <!-- 3313 -->                 
         <h:selectOneMenu id="selPlantNumberAdd"
                          disabled="#{batchMaintenanceBean.isViewAction or batchMaintenanceBean.disableUpdateAction }"
                          binding="#{batchMaintenanceBean.batchStatusMenuForUpdate}"
                          value="#{batchMaintenanceBean.selBatchMaintenance.batchStatusCode}" >
                        <a4j:support event="onchange" actionListener="#{batchMaintenanceBean.batchStatusChanged}" 
							reRender="msg" immediate="true"/>  
         </h:selectOneMenu>                  
                          
                          
         <h:outputText rendered="true"
                       value="Schedule Start Date/Time:" />
         <rich:calendar popup="true"
                        disabled="#{batchMaintenanceBean.isViewAction or batchMaintenanceBean.disableUpdateAction}"
                        oninputkeypress="return onlyDateValue();"
                        enableManualInput="true"
                        converter="dateTime"
                        timeZone="#{loginBean.timeZone}"
                        datePattern="MM/dd/yyyy hh:mm a"
                        label="Scheduled Start Date/Time"
                        value="#{batchMaintenanceBean.selBatchMaintenance.schedStartTimestamp}"
                        id="schedStartTimestamp" />

         <h:outputText rendered="true"
                       value="Last Updated User ID:" />
         <h:inputText id="batchupdatedUserTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.updateUserId}"
                      disabled="true" />
         <h:outputText rendered="true"
                       value="Last Updated Time Stamp:" />
         <h:inputText id="batchLastUpdatedDtTxt"
                      value="#{batchMaintenanceBean.selBatchMaintenance.updateTimestamp}"
                      disabled="true">
          <f:converter converterId="dateTime" />
         </h:inputText>
        </h:panelGrid>
        <ul class="basic-form">
         <li class="sub-heading-light">Batch Attributes</li>
        </ul>
        <rich:dataList var="row" id="StringBatchAttributes"
                       styleClass="list-table"
                        value="#{batchMaintenanceBean.batchMetadataList.strings}">
        <h:panelGrid id="utilityUpdatePanelGrid1" columns="2"
                     styleClass="basic-form"
                     columnClasses="column-form-label, column-form-data"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <h:outputLabel value="#{row.name}: " />
         <h:inputText size="40"
                      disabled="#{batchMaintenanceBean.isViewAction or batchMaintenanceBean.disableUpdateAction}"
                      value="#{row.value}" id="txtRowValue" />
        </h:panelGrid>
        </rich:dataList>
        <rich:dataList var="row" id="NumericBatchAttributes"
                       value="#{batchMaintenanceBean.batchMetadataList.numerics}">
        <h:panelGrid id="utilityUpdatePanelGrid2" columns="2"
                     styleClass="basic-form"
                     columnClasses="column-form-label, column-form-data"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <h:outputLabel value="#{row.name}: " />
         <h:inputText size="40"
                      style="text-align: right"
                      disabled="#{batchMaintenanceBean.isViewAction or batchMaintenanceBean.disableUpdateAction}"
                      onkeypress="return onlyNumerics(event);"
                      value="#{row.value}" id="numRowValue" >
          <f:convertNumber pattern="###,###,###,##0.##" />
         </h:inputText>
        </h:panelGrid>
        </rich:dataList>
        <rich:dataList var="row" id="DateBatchAttributes"
                       value="#{batchMaintenanceBean.batchMetadataList.dates}">
        <h:panelGrid id="utilityUpdatePanelGrid3" columns="2"
                     styleClass="basic-form"
                     columnClasses="column-form-label, column-form-data"
                     cellspacing="0"
                     cellpadding="0"
                     style="width: 100%;">
         <h:outputLabel value="#{row.name}: " />
         <rich:calendar popup="true"
                        disabled="#{batchMaintenanceBean.isViewAction or batchMaintenanceBean.disableUpdateAction}"
                        oninputkeypress="return onlyDateValue();"
                        enableManualInput="true"
                        converter="dateTime"
                        timeZone="#{loginBean.timeZone}"
                        datePattern="MM/dd/yyyy h:mm a"
                        value="#{row.value}"
                        style="text-align: right"
                        label="#{row.name}"  id="datRowValue">
         </rich:calendar>
        </h:panelGrid>
        </rich:dataList>
           <ul class="basic-form">
               <li class="sub-heading-light">User Fields</li>
           </ul>
           <rich:dataList var="row" id="userStringBatchAttributes"
                          styleClass="list-table"
                          value="#{batchMaintenanceBean.batchMetadataList.userStrings}">
               <h:panelGrid id="userUtilityUpdatePanelGrid1" columns="2"
                            styleClass="basic-form"
                            columnClasses="column-form-label, column-form-data"
                            cellspacing="0"
                            cellpadding="0"
                            style="width: 100%;">
                   <h:outputLabel value="#{row.name}: " />
                   <h:inputText size="40"
                                disabled="#{batchMaintenanceBean.isViewAction}"
                                value="#{row.value}" id="userTxtRowValue" />
               </h:panelGrid>
           </rich:dataList>
           <rich:dataList var="row" id="userNumericBatchAttributes"
                          value="#{batchMaintenanceBean.batchMetadataList.userNumbers}">
               <h:panelGrid id="userUtilityUpdatePanelGrid2" columns="2"
                            styleClass="basic-form"
                            columnClasses="column-form-label, column-form-data"
                            cellspacing="0"
                            cellpadding="0"
                            style="width: 100%;">
                   <h:outputLabel value="#{row.name}: " />
                   <h:inputText size="40"
                                style="text-align: right"
                                disabled="#{batchMaintenanceBean.isViewAction}"
                                onkeypress="return onlyNumerics(event);"
                                value="#{row.value}" id="userNumRowValue" >
                       <f:convertNumber pattern="###,###,###,##0.##" />
                   </h:inputText>
               </h:panelGrid>
           </rich:dataList>
               <rich:dataList var="row" id="userDateBatchAttributes"
                              value="#{batchMaintenanceBean.batchMetadataList.userDates}">
                   <h:panelGrid id="userUtilityUpdatePanelGrid3" columns="2"
                                styleClass="basic-form"
                                columnClasses="column-form-label, column-form-data"
                                cellspacing="0"
                                cellpadding="0"
                                style="width: 100%;">
                       <h:outputLabel value="#{row.name}: " />
                       <rich:calendar popup="true"
                                      disabled="#{batchMaintenanceBean.isViewAction}"
                                      oninputkeypress="return onlyDateValue();"
                                      enableManualInput="true"
                                      converter="dateTime"
                                      timeZone="#{loginBean.timeZone}"
                                      datePattern="MM/dd/yyyy h:mm a"
                                      value="#{row.value}"
                                      style="text-align: right"
                                      label="#{row.name}"  id="uaserDatRowValue">
                       </rich:calendar>
                   </h:panelGrid>
               </rich:dataList>

       </div>
       <div id="table-one-bottom">
        <ul class="right">
         <li class="ok">
          <h:commandLink id="btnUpdate" disabled="#{! batchMaintenanceBean.isUpdateAction}"
                         action="#{batchMaintenanceBean.updateAction}" />
         </li>
         <li class="ok">
          <h:commandLink id="btnOk" disabled="#{! batchMaintenanceBean.viewOKAction}"
                         action="#{batchMaintenanceBean.okAction}" />
         </li>
         <li class="cancel">
          <h:commandLink id="btnCancel" disabled="#{!batchMaintenanceBean.viewAction}"
                         immediate="true"
                         action="batch_maintenance_main" />
         </li>
        </ul>
       </div>
      </div>
     </div>
    </h:form>
   </f:view>
  </ui:define>
 </ui:composition>
</html>
