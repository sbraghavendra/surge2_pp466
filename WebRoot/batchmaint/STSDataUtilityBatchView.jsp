<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>
    <ui:define name="body" >
	<f:view>
	<h:form id="batchMaintStatusView">
<h1><img src="../images/headers/hdr-batch-maintenance-status.gif" width="250" height="19" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top" >
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="5">View a Batch</td></tr>
		</thead>
		<tbody>
			<tr>
					<td><h:outputText rendered="true" value="Batch ID:" /></td>
				<td>
				<h:inputText disabled="true" id="batchIdTxt"
								value="#{batchMaintenanceBean.selBatchMaintenance.batchId}"
								style="width: 140px;" immediate="true" >
							</h:inputText>
				</td>
				<td><h:outputText rendered="true" value="Batch Type:" /></td>
				<td style="width:400px;">
				<h:inputText disabled="true" id="batchTypeTxt"
								value="#{batchMaintenanceBean.selBatchMaintenance.batchTypeDesc}"
								style="width: 140px;" immediate="true" >
							</h:inputText>
				</td>
			</tr>
			<tr>
				
				<td><h:outputText rendered="true" value="Entry Date/Time:" />
															</td>
				<td>
				<h:inputText disabled="true" id="batchEntryDtTxt"
								value="#{batchMaintenanceBean.selBatchMaintenance.entryTimestamp}"
								style="width: 140px;" >
								<f:converter converterId="dateTime"/>
							</h:inputText>
				</td>
				<td><h:outputText rendered="true" value="Highest Error Severity:" /></td>
				<td>
				<h:inputText disabled="true" id="batchErrSrvCdTxt"
								value="#{batchMaintenanceBean.selBatchMaintenance.errorSevCode}"
								style="width: 140px;" immediate="true" >
							</h:inputText></td>
			</tr>
			<tr>
				
				<td><h:outputText rendered="true" value="Batch Status:" /></td>
				<td>
				<h:selectOneMenu id="selPlantNumberAdd" disabled="true"
								binding="#{batchMaintenanceBean.batchStatusMenu}"
								value="#{batchMaintenanceBean.selBatchMaintenance.batchStatusCode}">
							</h:selectOneMenu>
							</td>
							<td><h:outputText rendered="true" value="Schedule Start Date/Time:" /></td>
							<td>
							<rich:calendar popup="true" 
								             disabled="true"
                             converter="dateTime"
                             timeZone="#{loginBean.timeZone}"
                             datePattern="MM/dd/yyyy hh:mm a"
                             label="Scheduled Start Date/Time"
								             value="#{batchMaintenanceBean.selBatchMaintenance.schedStartTimestamp}"
								             id="schedStartTimestamp" />
              </td>
						</tr>
			<tr>
				
				<td>
								<h:outputText id="vc01Desc"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc01Desc}"
								value="#{batchMaintenanceBean.batchMetadata.vc01Desc}:" /> 
								</td>
				<td>
								<h:inputText disabled="true" id="vc01Input"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc01Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc01}"
								style="width: 140px;"></h:inputText>
								</td>
							<td>
							<h:outputText id="vc02Desc"
							value="#{batchMaintenanceBean.batchMetadata.vc02Desc}:" 
							rendered="#{!empty batchMaintenanceBean.batchMetadata.vc02Desc}"/> 
								</td>
							<td>
							<h:inputText disabled="true" id="vc02Input"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc02}"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc02Desc}"
								style="width: 140px;">
							</h:inputText></td>
						</tr>
									<tr>
				
				<td>
								<h:outputText id="vc03Desc"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc03Desc}"
								value="#{batchMaintenanceBean.batchMetadata.vc03Desc}:" /> 
								</td>
				<td>
								<h:inputText disabled="true" id="vc03Input"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc03Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc03}"
								style="width: 140px;"></h:inputText>
								</td>
							<td>
							<h:outputText id="vc04Desc"
							value="#{batchMaintenanceBean.batchMetadata.vc04Desc}:" 
							rendered="#{!empty batchMaintenanceBean.batchMetadata.vc04Desc}"
							/> 
								</td>
							<td>
							<h:inputText disabled="true" id="vc04Input"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc04}"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc04Desc}"
								style="width: 140px;">
							</h:inputText></td>
						</tr>
									<tr>
				
				<td>
								<h:outputText id="vc05Desc"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc05Desc}"
								value="#{batchMaintenanceBean.batchMetadata.vc05Desc}:" /> 
								</td>
				<td>
								<h:inputText disabled="true" id="vc05Input"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc05Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc05}"
								style="width: 140px;"></h:inputText>
								</td>
							<td>
							<h:outputText id="vc06Desc"
							value="#{batchMaintenanceBean.batchMetadata.vc06Desc}:" 
							rendered="#{!empty batchMaintenanceBean.batchMetadata.vc06Desc}"
							/> 
								</td>
							<td>
							<h:inputText disabled="true" id="vc06Input"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc06}"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc06Desc}"
								style="width: 140px;">
							</h:inputText></td>
						</tr>
									<tr>
				
				<td>
								<h:outputText id="vc07Desc"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc07Desc}"
								value="#{batchMaintenanceBean.batchMetadata.vc07Desc}:" /> 
								</td>
				<td>
								<h:inputText disabled="true" id="vc07Input"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc07Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc07}"
								style="width: 140px;"></h:inputText>
								</td>
							<td>
							<h:outputText id="vc08Desc"
							value="#{batchMaintenanceBean.batchMetadata.vc08Desc}:" 
							rendered="#{!empty batchMaintenanceBean.batchMetadata.vc08Desc}"
							/> 
								</td>
							<td>
							<h:inputText disabled="true" id="vc08Input"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc08}"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc08Desc}"
								style="width: 140px;">
							</h:inputText></td>
						</tr>
									<tr>
				
									<td>
							<h:outputText id="vc09Desc"
							value="#{batchMaintenanceBean.batchMetadata.vc09Desc}:" 
							rendered="#{!empty batchMaintenanceBean.batchMetadata.vc09Desc}"
							/> 
								</td>
							<td>
							<h:inputText disabled="true" id="vc09Input"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc09}"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc09Desc}"
								style="width: 140px;">
							</h:inputText></td>
				<td>
								<h:outputText id="vc10Desc"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc10Desc}"
								value="#{batchMaintenanceBean.batchMetadata.vc10Desc}:" /> 
								</td>
				<td>
								<h:inputText disabled="true" id="vc10Input"
								rendered="#{!empty batchMaintenanceBean.batchMetadata.vc10Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.vc10}"
								style="width: 140px;"></h:inputText>
								</td>
							</tr>
			<tr>
				
							<td>
							<h:outputText id="nu01Desc"
							rendered="#{!empty batchMaintenanceBean.batchMetadata.nu01Desc}"  
							value="#{batchMaintenanceBean.batchMetadata.nu01Desc}:"> </h:outputText>
								</td>
							<td><h:inputText disabled="true" id="nu01Input" 
								rendered="#{!empty batchMaintenanceBean.batchMetadata.nu01Desc}" 
								value="#{batchMaintenanceBean.selBatchMaintenance.nu01}"
								style="width: 140px;"
								label="#{batchMaintenanceBean.batchMetadata.nu01Desc}" >
                               	<f:convertNumber integerOnly="true" />           
							</h:inputText></td>
<td>
							<h:outputText id="nu02Desc"
							rendered="#{!empty batchMaintenanceBean.batchMetadata.nu02Desc}" 
							value="#{batchMaintenanceBean.batchMetadata.nu02Desc}:"> </h:outputText>
							</td><td>
							<h:inputText disabled="true" id="nu02Input" 
								rendered="#{!empty batchMaintenanceBean.batchMetadata.nu02Desc}" 
								value="#{batchMaintenanceBean.selBatchMaintenance.nu02}"
								style="width: 140px;"
								label="#{batchMaintenanceBean.batchMetadata.nu02Desc}">

							</h:inputText> 
						</td>
			</tr>
			<tr>
				<td>
				<h:outputText id="nu03Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu03Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.nu03Desc}:" />
															</td>
				<td>
				<h:inputText disabled="true" id="nu03Input" 
				value="#{batchMaintenanceBean.selBatchMaintenance.nu03}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu03Desc}" 
				style="width: 140px;"
				label="#{batchMaintenanceBean.batchMetadata.nu03Desc}">

				</h:inputText> 
			</td>
			<td>
				<h:outputText id="nu04Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu04Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.nu04Desc}:" />
			</td>
			<td>
				<h:inputText disabled="true" id="nu04Input" value="#{batchMaintenanceBean.selBatchMaintenance.nu04}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu04Desc}" 
				style="width: 140px;" 
				label="#{batchMaintenanceBean.batchMetadata.nu04Desc}">

				</h:inputText>
				</td>
			</tr>
			
			
						<tr>
				<td>
				<h:outputText id="nu05Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu05Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.nu05Desc}:" />
															</td>
				<td>
				<h:inputText disabled="true" id="nu05Input" 
				value="#{batchMaintenanceBean.selBatchMaintenance.nu05}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu05Desc}" 
				style="width: 140px;" label="#{batchMaintenanceBean.batchMetadata.nu05Desc}">

				</h:inputText> 
			</td>
			<td>
				<h:outputText id="nu06Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu06Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.nu06Desc}:" />
			</td>
			<td>
				<h:inputText disabled="true" id="nu06Input" value="#{batchMaintenanceBean.selBatchMaintenance.nu06}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu06Desc}" 
				style="width: 140px;" 
				label="#{batchMaintenanceBean.batchMetadata.nu06Desc}">

				</h:inputText>
				</td>
			</tr>
						<tr>
				<td>
				<h:outputText id="nu07Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu07Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.nu07Desc}:" />
															</td>
				<td>
				<h:inputText disabled="true" id="nu07Input" 
				value="#{batchMaintenanceBean.selBatchMaintenance.nu07}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu07Desc}" 
				style="width: 140px;" label="#{batchMaintenanceBean.batchMetadata.nu07Desc}">

				</h:inputText> 
			</td>
			<td>
				<h:outputText id="nu08Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu08Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.nu08Desc}:" />
			</td>
			<td>
				<h:inputText disabled="true" id="nu08Input" value="#{batchMaintenanceBean.selBatchMaintenance.nu08}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu08Desc}" 
				style="width: 140px;" 
				label="#{batchMaintenanceBean.batchMetadata.nu08Desc}">

				</h:inputText>
				</td>
			</tr>
						<tr>
				<td>
				<h:outputText id="nu09Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu09Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.nu09Desc}:" />
															</td>
				<td>
				<h:inputText disabled="true" id="nu09Input" 
				value="#{batchMaintenanceBean.selBatchMaintenance.nu09}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu09Desc}" 
				style="width: 140px;"
				label="#{batchMaintenanceBean.batchMetadata.nu09Desc}">

				</h:inputText> 
			</td>
			<td>
				<h:outputText id="nu10Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu10Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.nu10Desc}:" />
			</td>
			<td>
				<h:inputText disabled="true" id="nu10Input" value="#{batchMaintenanceBean.selBatchMaintenance.nu10}"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.nu10Desc}" 
				style="width: 140px;" 
				label="#{batchMaintenanceBean.batchMetadata.nu10Desc}">

				</h:inputText>
				</td>
			</tr>
			
									<tr>
				<td>
				<h:outputText id="ts01Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts01Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.ts01Desc}:" />
															</td>
				<td>
				<rich:calendar popup="true" 
								       disabled="true"
						       	   rendered="#{!empty batchMaintenanceBean.batchMetadata.ts01Desc}" 
                       converter="dateTime"
        						   label="#{batchMaintenanceBean.batchMetadata.ts01Desc}"
								       value="#{batchMaintenanceBean.selBatchMaintenance.ts01}"
								       id="ts01" />
			</td>
			<td>
				<h:outputText id="ts02Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts02Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.ts02Desc}:" />
			</td>
			<td>
				<rich:calendar popup="true" 
								       disabled="true"
						       	   rendered="#{!empty batchMaintenanceBean.batchMetadata.ts02Desc}"
                       converter="dateTime"
        						   label="#{batchMaintenanceBean.batchMetadata.ts02Desc}"
								       value="#{batchMaintenanceBean.selBatchMaintenance.ts02}"
								id="ts02" />
				</td>
			</tr>
											<tr>
				<td>
				<h:outputText id="ts03Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts03Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.ts03Desc}:" />
															</td>
				<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts03Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts03Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts03}"
								id="ts03" />
			</td>
			<td>
				<h:outputText id="ts04Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts04Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.ts04Desc}:" />
			</td>
			<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts04Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts04Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts04}"
								id="ts04" />
				</td>
			</tr>
											<tr>
				<td>
				<h:outputText id="ts05Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts05Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.ts05Desc}:" />
															</td>
				<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts05Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts05Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts05}"
								id="ts05" />
			</td>
			<td>
				<h:outputText id="ts06Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts06Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.ts06Desc}:" />
			</td>
			<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts06Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts06Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts06}"
								id="ts06" />
				</td>
			</tr>
											<tr>
				<td>
				<h:outputText id="ts07Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts07Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.ts07Desc}:" />
															</td>
				<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts07Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts07Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts07}"
								id="ts07" />
			</td>
			<td>
				<h:outputText id="ts08Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts08Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.ts08Desc}:" />
			</td>
			<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts08Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts08Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts08}"
								id="ts08" />
				</td>
			</tr>
											<tr>
				<td>
				<h:outputText id="ts09Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts09Desc}"  
				value="#{batchMaintenanceBean.batchMetadata.ts09Desc}:" />
															</td>
				<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts09Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts09Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts09}"
								id="ts09" />
			</td>
			<td>
				<h:outputText id="ts10Desc"
				rendered="#{!empty batchMaintenanceBean.batchMetadata.ts10Desc}" 
				value="#{batchMaintenanceBean.batchMetadata.ts10Desc}:" />
			</td>
			<td>
				<rich:calendar popup="true" 
								disabled="true"
						       	rendered="#{!empty batchMaintenanceBean.batchMetadata.ts10Desc}" 
        						   converter="dateTime"
                       label="#{batchMaintenanceBean.batchMetadata.ts10Desc}"
								value="#{batchMaintenanceBean.selBatchMaintenance.ts10}"
								id="ts10" />
				</td>
			</tr>
						<tr>
							<td>
				<h:outputText rendered="true" value="Last Updated User ID:" /></td>
							<td>
				<h:inputText disabled="true" id="batchupdatedUserTxt"
								value="#{batchMaintenanceBean.selBatchMaintenance.updateUserId}"
								style="width: 140px;" >
							</h:inputText>
														</td>
														<td>
				<h:outputText rendered="true"
								value="Last Updated Time Stamp:" /></td>
							<td>
				<h:inputText disabled="true" id="batchLastUpdatedDtTxt" 
								value="#{batchMaintenanceBean.selBatchMaintenance.updateTimestamp}"
								style="width: 140px;" immediate="true" >
								<f:converter converterId="dateTime"/>
							</h:inputText></td>
			</tr>

		</tbody>
	</table>




	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink action="#{batchMaintenanceBean.viewAction}"/></li>
	
	</ul>
	</div>
	</div>
</div>
</h:form>
</f:view>
</ui:define>
</ui:composition>
</html>