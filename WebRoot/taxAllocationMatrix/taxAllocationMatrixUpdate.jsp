<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMDetailList', 'aMDetailListRowIndex'); } );
registerEvent(window, "load", function() { displayWarnings(); } );
registerEvent(window, "load", function() { initBUFlag(); } );

function displayWarnings() {
	displayWarning();
	displayGlobalJurisWarning();
	displayIndividualJurisWarning();
}

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function displayGlobalJurisWarning() {
 var warning = document.getElementById('displayGlobalJurisWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('globalJurisWarningId');
 }
}

function displayIndividualJurisWarning() {
 var warning = document.getElementById('displayIndividualJurisWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('individualJurisWarningId');
 }
}

function updateTotal() {
  var total = 0.0;
  for (var i = 0; i < 1000; i++) {
    var pct = document.getElementById("matrixDetailForm:aMDetail:"+i+":pct");
    if (!pct) break;
    total += parseFloat(pct.value);
  }
  
  var out = formatNumber(numToString(total, 10), 2);
  replaceInnerHTML("matrixDetailForm:total",out);

  out = formatNumber(numToString(1.0 - total, 10), 2); 
  replaceInnerHTML("matrixDetailForm:balance",out);
  replaceInnerHTML("matrixDetailForm:balance2",out);
}

function numToString(num, decimalNum) {
	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// Get sign of number
	
	// Adjust number so only the specified number of numbers after
	// the decimal point are shown.
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Readjust for sign
	return new String(tmpNum);
}

function replaceInnerHTML(id,val){
	var obj = document.getElementById(id);
	if(obj) {
		obj.innerHTML = val;
	}
}

function initBUFlag() {
	var ctls = null;
	var panel = null;
	
	panel = document.getElementById('matrixDetailForm:driverPanelCopyAdd');
	if(panel!=null) {
		ctls = panel.getElementsByTagName('input');
	}
	else{
		panel = document.getElementById('matrixDetailForm:driverPanelView');
		ctls = panel.getElementsByTagName('input');
	}	
}

//]]>
</script>
</ui:define>

<ui:define name="body">
	<h:inputHidden id="displayWarning" value="#{TaxAllocationMatrixViewBean.displayWarning}"/>
	<h:inputHidden id="displayGlobalJurisWarning" value="#{TaxAllocationMatrixViewBean.displayGlobalJurisWarning}"/>
	<h:inputHidden id="displayIndividualJurisWarning" value="#{TaxAllocationMatrixViewBean.displayIndividualJurisWarning}"/>
	<h:inputHidden id="aMDetailListRowIndex" value="#{TaxAllocationMatrixViewBean.selectedTransactionIndex}"/>

	<h:form id="matrixDetailForm">

	<h1><img id="imgTaxabilityMatrix" alt="TaxCode Allocations" src="../images/headers/hdr-GoodsAndServicesAllocations.png" width="250" height="19" /></h1>
	<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				<td colspan="5"><h:outputText value="#{TaxAllocationMatrixViewBean.actionText}"/> TaxCode Allocations <h:outputText value="#{TaxAllocationMatrixViewBean.actionTextSuffix}"/></td>
				<td colspan="4" style="text-align:right;">Tax Allocation Matrix ID:&#160;<h:outputText value="#{TaxAllocationMatrixViewBean.matrixId}"/></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="50">Driver Selection</th>				
			</tr>
		</tbody>
	</table>
	
	<h:panelGrid id="driverPanelCopyAdd"
		rendered="#{!TaxAllocationMatrixViewBean.readOnlyDrivers}" 
		binding="#{TaxAllocationMatrixViewBean.copyAddPanel}"
		styleClass="panel-ruler"/>
		
	<h:panelGrid id="driverPanelView"
		rendered="#{TaxAllocationMatrixViewBean.readOnlyDrivers}" 
		binding="#{TaxAllocationMatrixViewBean.viewPanel}"
		styleClass="panel-ruler"/>
		
		
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="9">Details</th>
		</tr>
		<tr>
			<td style="width:20px;">&#160;</td>
			<td style="width:200px;">Effective Date:</td>
			<td style="width: 150px;">
				<rich:calendar binding="#{TaxAllocationMatrixViewBean.effectiveDateCalendar}" 
						id="effDate" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{TaxAllocationMatrixViewBean.selectedMatrix.effectiveDate}" popup="true"
		
						disabled="#{!TaxAllocationMatrixViewBean.addAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox"/>
			</td>
			<td style="width:23px;">&#160;</td>
			<td style="width:150px;">
				<h:outputText value="Future Split?:"/>
				<h:selectBooleanCheckbox id="futureSplitControl" styleClass="check" 
					disabled="#{TaxAllocationMatrixViewBean.readOnlyAction or TaxAllocationMatrixViewBean.displayUpdateAction}"
					value="#{TaxAllocationMatrixViewBean.futureSplit}" immediate="true">
					<a4j:support event="onclick" action="tax_alloc_matrix_add" />
				</h:selectBooleanCheckbox>
			</td>
			<td style="width:600px;">&#160;</td>
		</tr>
		<tr>
			<td style="width:20px;">&#160;</td>
			<td style="width:200px;">Expiration Date:</td>
			<td>
				<rich:calendar id="expDate" enableManualInput="true"
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{TaxAllocationMatrixViewBean.selectedMatrix.expirationDate}" popup="true"
						disabled="#{TaxAllocationMatrixViewBean.readOnlyAction}"
						validator="#{TaxAllocationMatrixViewBean.validateExpirationDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox" />
			</td>
			<td style="width:23px;">&#160;</td>
			<td style="width:150px;">
				<h:outputText value="Active?:"/>
				<h:selectBooleanCheckbox id="activeControl" styleClass="check" 
					disabled="#{TaxAllocationMatrixViewBean.readOnlyAction}"
					value="#{TaxAllocationMatrixViewBean.selectedMatrix.activeBooleanFlag}" immediate="true">
					<a4j:support event="onclick"  />
				</h:selectBooleanCheckbox>
			</td>
			<td style="width:600px;">&#160;</td>
		</tr>
		
		<tr>
			<td style="width:20px;">&#160;</td>
			<td style="width:200px;">Comments:</td>
			<td colspan="7">
				<h:inputText id="comments" style="width:678px;" 
					disabled="#{TaxAllocationMatrixViewBean.readOnlyAction}"
					value="#{TaxAllocationMatrixViewBean.selectedMatrix.comments}"
					maxlength="255"/>
			</td>
		</tr>
		
		<tr>
        	<td colspan="9">&#160;</td>
		</tr>
		
		</tbody>
	</table>
	
	<a4j:outputPanel id="forEachPanelWhen">
	<span>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>		
		<tr>
			<th colspan="9">Allocations</th>
		</tr>
			
		<tr>
        	<td style="width:20px;">&#160;</td>    
	        <td><h:outputText style="width: 180px;" value="TaxCode" /></td>
	        <td><h:outputText style="width: 470px;" value="Description" /></td>
	        <td><h:outputText style="width: 100px;" value="Allocation Percent" /></td>
        	
			<td width="50px">&#160;</td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px">
				<a4j:commandLink id="addWhenTop" styleClass="button" rendered="#{!(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.viewAction)}" 
					reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processAddWhenCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" />
			</td> 
			<td width="1%">&#160;</td> 
		</tr>
		  
		<c:forEach var="taxAllocMatrixDetail" items="#{TaxAllocationMatrixViewBean.selectedMatrixTaxAllocDetails}" varStatus="loopCounter" >
        <tr>
        	<td style="width:20px;">&#160;</td>
        	
	        <td>  
	        	<a4j:outputPanel id="fieldpanelWhen_#{loopCounter.index}">   
	        		<h:selectOneMenu id="allWhen_#{loopCounter.index}"  style="width: 180px;" value="#{taxAllocMatrixDetail.taxcodeCode}" disabled="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" >
						<f:selectItems value="#{TaxAllocationMatrixViewBean.taxCodeItems}"/>
						
						<a4j:support id="fieldTaxCode_#{loopCounter.index}" event="onchange" immediate="true" reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.taxcodeCodeSelected}" >
							<f:param name="command" value="#{loopCounter.index}" />
						</a4j:support>
					</h:selectOneMenu>	
				</a4j:outputPanel>
	        </td>
	        <!--  
	        <td>           	
				<a4j:outputPanel id="fieldDescription_#{loopCounter.index}">   
					<h:inputText id="fieldvalueDescription_#{loopCounter.index}" style="width: 470px;" value="#{taxAllocMatrixDetail.description}" disabled="true" />				 	
	        	</a4j:outputPanel>
	        </td>
	        -->
	        
	        
	        <td>           	
				<a4j:outputPanel id="fieldDescription_#{loopCounter.index}">   
					<h:inputText id="fieldvalueDescription_#{loopCounter.index}" style="width: 470px;" value="#{not empty taxAllocMatrixDetail.description ? taxAllocMatrixDetail.description : cacheManager.taxCodeMap[taxAllocMatrixDetail.taxcodeCode].description}" disabled="true" />				 	
	        	</a4j:outputPanel>
	        </td>
	        <!--
	        <td>           	
				<a4j:outputPanel id="fieldDescription_#{loopCounter.index}">   
					<h:inputText id="fieldvalueDescription_#{loopCounter.index}" style="width: 470px;" value="#{cacheManager.taxCodeMap[taxAllocMatrixDetail.taxcodeCode].description}" disabled="true" />				 	
	        	</a4j:outputPanel>
	        </td>
	        -->

	        <td> 
	        	<a4j:outputPanel id="fieldvaluepaneWhen_#{loopCounter.index}">   
		
					<h:inputText id="fieldvalueWhen_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.allocationPercent}" onkeypress="return onlyNumerics(event);"
						disabled="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" >
						<a4j:support id="ajaxslistener_#{loopCounter.index}" event="onblur" reRender="balance" ajaxSingle="true" />
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						<f:validateDoubleRange minimum="0" maximum="1"/>	
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>      

        
        
			<td width="50px">&#160;</td>
	 	
			<td width="50px">
				<!--
        		<h:commandLink  id="split_#{loopCounter.index}" value="Juris.&#160;Split"  
        			oncomplete="initScrollingTables();" reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processSplitCommand}" >
        			
        			<f:param name="command" value="#{loopCounter.index}" />
        				<a4j:actionparam name="jurisSplitInstanceIdParam" assignTo="#{TaxAllocationMatrixViewBean.jurisSplitInstanceId}" value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
        		</h:commandLink >
        	
        		<a4j:commandButton id="split_#{loopCounter.index}" type="button" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/splitButton.png" immediate="true" >
																
						<a4j:support event="onclick" actionListener="#{TaxAllocationMatrixViewBean.individualJurisSplitListener}" action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction}" >
									<f:param name="command" value="#{loopCounter.index}" />
						</a4j:support>
								
								
				</a4j:commandButton>
				-->	
					
				<a4j:commandButton id="splitDetailBtn1_#{loopCounter.index}" 
			                	rendered="#{TaxAllocationMatrixViewBean.jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId] and !TaxAllocationMatrixViewBean.displayDeleteAction and !TaxAllocationMatrixViewBean.displayViewAction}"
			                	oncomplete="initScrollingTables();"
			                	type="button" styleClass="image" style="float:left;width:16px;height:16px;" 
			                	action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction_jeesmon}"
			                	reRender="aMDetail,total,balance,balance2" image="/images/splitEnabledButton.png" immediate="true">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{TaxAllocationMatrixViewBean.jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
				
			                
			               <a4j:commandButton id="splitDetailBtn2_#{loopCounter.index}" 
			                	rendered="#{!TaxAllocationMatrixViewBean.jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId] and !TaxAllocationMatrixViewBean.displayDeleteAction and !TaxAllocationMatrixViewBean.displayViewAction}"
			                	oncomplete="initScrollingTables();"
			                	type="button" styleClass="image" style="float:left;width:16px;height:16px;" 
			                	action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction_jeesmon}"
			                	reRender="aMDetail,total,balance,balance2" image="/images/splitButton.png" immediate="true">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{TaxAllocationMatrixViewBean.jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
        		
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction  or TaxAllocationMatrixViewBean.displayUpdateAction)}" />   		  
        	</td>
			
			<!-- Test
			<td width="15px">
        		<a4j:commandLink  id="split_#{loopCounter.index}" styleClass="button" rendered="#{!(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction  or TaxAllocationMatrixViewBean.displayUpdateAction)}" 
        			reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processSplitCommand}" >
        			<h:graphicImage value="/images/splitButton.png" />	
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink >
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction  or TaxAllocationMatrixViewBean.displayUpdateAction)}" />   		  
        	</td>
			 
        	<td width="15px">
        		<a4j:commandLink  id="upWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq TaxAllocationMatrixViewBean.firstWhenObject) and !(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction  or TaxAllocationMatrixViewBean.displayUpdateAction)}" 
        			reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processUpWhenCommand}" >
        			<h:graphicImage value="/images/icon_arrow-up.jpg" />	
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink >
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq TaxAllocationMatrixViewBean.firstWhenObject)  or (TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" />   		  
        	</td>
        	-->
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
        	<!--  
            <td width="15px">
            	<a4j:commandLink id="downWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq TaxAllocationMatrixViewBean.lastWhenObject) and !(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction  or TaxAllocationMatrixViewBean.displayUpdateAction)}" 
            		reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processDownWhenCommand}" >
            		<h:graphicImage value="/images/icon_arrow-down.jpg" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq TaxAllocationMatrixViewBean.lastWhenObject)  or (TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" /> 
            </td>
            -->
			<td width="15px">
				<h:outputText style="width: 15px;" value="&#160;"  />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="removeWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" 
            		reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processRemoveWhenCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(TaxAllocationMatrixViewBean.displayDeleteAction or TaxAllocationMatrixViewBean.displayViewAction)}" />
            </td>
            
 			<td width="1%">&#160;</td> 
        </tr>
    	</c:forEach>
    	
    	<tr>
        	<td style="width:20px;">&#160;</td>   
	        <td><h:outputText style="width: 180px;" value="&#160;" /></td>
	        
	        <c:if test="#{TaxAllocationMatrixViewBean.displayDeleteAction}">
		     	<td align="center"><font color="red"><h:outputText style="width:470px; color:ref" 
		     		value="#{(TaxAllocationMatrixViewBean.allowDeleteTaxAllocationMatrix) ? '&#160;' : 'The Matrix line has been used on a Transaction and cannot be deleted. Click Ok to set to inactive.'}" /></font></td>
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			</c:if>  
			
			<c:if test="#{!TaxAllocationMatrixViewBean.displayDeleteAction}">
		     	<td align="right"><h:outputText style="width: 470px;" value="Remaining to Allocate:" /></td>
	        	<td class="column-input" style="width: 100px;" >&#160;
					<h:outputText id="balance" value="#{TaxAllocationMatrixViewBean.remainingAllocationPercent}" >
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
					</h:outputText>
				</td>
			</c:if>
        	
			<td width="50px">&#160;</td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="1%">&#160;</td> 
		</tr>

		<tr>
        	<td colspan="15">&#160;</td>
		</tr>
	</tbody>
	</table>
	</span>
	</a4j:outputPanel>
		
	</div>
</div>


<!--  
<div id="bottom">
	<div id="table-four">
		<div id="table-four-content">
			<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
				<tbody>
					<tr>
						<th colspan="5">Allocation</th>
					</tr>
				</tbody>
			</table>
			
			<div class="scrollContainer">
				<div class="scrollInner">
					<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail" styleClass="GridContent"
						value="#{TaxAllocationMatrixViewBean.selectedMatrixTaxAllocDetails}" var="taxAllocMatrixDetail">
						
						<rich:column id="driver01d" sortBy="#{taxAllocMatrixDetail.driver01}"
							rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}">
							<f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}" /></f:facet>
							<h:outputText value="#{taxAllocMatrixDetail.driver01}" />
						</rich:column>
						
						<rich:column id="driver02d" sortBy="#{taxAllocMatrixDetail.driver02}"
							rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}">
							<f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}" /></f:facet>
							<h:outputText value="#{taxAllocMatrixDetail.driver02}" />
						</rich:column>
						
						<rich:column id="driver03d" sortBy="#{taxAllocMatrixDetail.driver03}"
							rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}">
							<f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}" /></f:facet>
							<h:outputText value="#{taxAllocMatrixDetail.driver03}" />
						</rich:column>
						
						<rich:column id="driver04d" sortBy="#{taxAllocMatrixDetail.driver04}"
							rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}">
							<f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}" /></f:facet>
							<h:outputText value="#{taxAllocMatrixDetail.driver04}" />
						</rich:column>
						
						<rich:column id="driver05d" sortBy="#{taxAllocMatrixDetail.driver05}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver05}" />
						</rich:column>
						
						<rich:column id="driver06d" sortBy="#{taxAllocMatrixDetail.driver06}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver06}" />
						</rich:column>
						
						<rich:column id="driver07d" sortBy="#{taxAllocMatrixDetail.driver07}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver07}" />
						</rich:column>
						
						<rich:column id="driver08d" sortBy="#{taxAllocMatrixDetail.driver08}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver08}" />
						</rich:column>
						
						<rich:column id="driver09d" sortBy="#{taxAllocMatrixDetail.driver09}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver09}" />
						</rich:column>
						
						<rich:column id="driver10d" sortBy="#{taxAllocMatrixDetail.driver10}"
					        rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}">
					        <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}" /></f:facet>
					        <h:outputText value="#{taxAllocMatrixDetail.driver10}" />
						</rich:column>

						<rich:column id="driver11d" sortBy="#{taxAllocMatrixDetail.driver11}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver11}" />
						</rich:column>
						
						<rich:column id="driver12d" sortBy="#{taxAllocMatrixDetail.driver12}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver12}" />
						</rich:column>
						
						<rich:column id="driver13d" sortBy="#{taxAllocMatrixDetail.driver13}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver13}" />
						</rich:column>
						
						<rich:column id="driver14d" sortBy="#{taxAllocMatrixDetail.driver14}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver14}" />
						</rich:column>
						
						<rich:column id="driver15d" sortBy="#{taxAllocMatrixDetail.driver15}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver15}" />
						</rich:column>
						
						<rich:column id="driver16d" sortBy="#{taxAllocMatrixDetail.driver16}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver16}" />
						</rich:column>
						
						<rich:column id="driver17d" sortBy="#{taxAllocMatrixDetail.driver17}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver17}" />
						</rich:column>
						
						<rich:column id="driver18d" sortBy="#{taxAllocMatrixDetail.driver18}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver18}" />
						</rich:column>
						
						<rich:column id="driver19d" sortBy="#{taxAllocMatrixDetail.driver19}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver19}" />
						</rich:column>
						
						<rich:column id="driver20d" sortBy="#{taxAllocMatrixDetail.driver20}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver20}" />
						</rich:column>
						
						<rich:column id="driver21d" sortBy="#{taxAllocMatrixDetail.driver21}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver21}" />
						</rich:column>
						
						<rich:column id="driver22d" sortBy="#{taxAllocMatrixDetail.driver22}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver22}" />
						</rich:column>
						
						<rich:column id="driver23d" sortBy="#{taxAllocMatrixDetail.driver23}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver23}" />
						</rich:column>
						
						<rich:column id="driver24d" sortBy="#{taxAllocMatrixDetail.driver24}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver24}" />
						</rich:column>
						
						<rich:column id="driver25d" sortBy="#{taxAllocMatrixDetail.driver25}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver25}" />
						</rich:column>
						
						<rich:column id="driver26d" sortBy="#{taxAllocMatrixDetail.driver26}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver26}" />
						</rich:column>
						
						<rich:column id="driver27d" sortBy="#{taxAllocMatrixDetail.driver27}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver27}" />
						</rich:column>
						
						<rich:column id="driver28d" sortBy="#{taxAllocMatrixDetail.driver28}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver28}" />
						</rich:column>
						
						<rich:column id="driver29d" sortBy="#{taxAllocMatrixDetail.driver29}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver29}" />
						</rich:column>
						
						<rich:column id="driver30d" sortBy="#{taxAllocMatrixDetail.driver30}"
						    rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}">
						    <f:facet name="header"><h:outputText value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}" /></f:facet>
						    <h:outputText value="#{taxAllocMatrixDetail.driver30}" />
						</rich:column>
						
						<rich:column id="allocationPercent" sortBy="#{taxAllocMatrixDetail.allocationPercent}" style="text-align: right;">
		                    <f:facet name="header"><h:outputText value="Allocation Percent" /></f:facet>
							<h:panelGroup>
								<h:inputText id="pct" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}" label="Allocation Percent"
									value="#{taxAllocMatrixDetail.allocationPercent}" style="text-align: right;"
									onkeypress="return onlyNumerics(event);">
									<a4j:support event="onblur" oncomplete="updateTotal();" />
									<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>
									<f:validateDoubleRange minimum="0" maximum="1"/>
								</h:inputText>
								<h:outputText rendered="#{TaxAllocationMatrixViewBean.readOnlyAction}" value="#{taxAllocMatrixDetail.allocationPercent}" style="text-align: right;">
									<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
								</h:outputText>
							</h:panelGroup>
		                </rich:column>
		                
		                <rich:column id="detailComments" sortBy="#{taxAllocMatrixDetail.comments}" style="text-align: left;">
		                    <f:facet name="header"><h:outputText value="Comments" /></f:facet>
		                    <h:outputText value="#{taxAllocMatrixDetail.comments}" />
		                </rich:column>
		                
		                <rich:column id="split" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}">
							<f:facet name="header">
								<h:outputText id="splitHdr" escape="false" value="&#160;&#160;&#160;&#160;&#160;" />
							</f:facet>
			                <a4j:commandButton id="splitDetailBtn" value="Split"
			                	rendered="#{TaxAllocationMatrixViewBean.jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId]}"
			                	oncomplete="initScrollingTables();"
			                	style="color:red; font-weight: bold;"
			                	action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction}"
			                	reRender="aMDetail,total,balance,balance2">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{TaxAllocationMatrixViewBean.jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
			                
			                <a4j:commandButton id="splitDetailBtn2" value="Split"
			                	oncomplete="initScrollingTables();"
			                	rendered="#{!TaxAllocationMatrixViewBean.jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId]}"
			                	action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction}"
			                	reRender="aMDetail,total,balance,balance2">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{TaxAllocationMatrixViewBean.jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
			            </rich:column>
			
						<rich:column id="delete" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}">
							<f:facet name="header">
								<h:outputText id="deleteHdr" escape="false" value="&#160;&#160;&#160;&#160;&#160;" />
							</f:facet>
			                <a4j:commandButton id="deleteDetailBtn" value="Delete"
			                	oncomplete="initScrollingTables();"
			                	reRender="aMDetail,total,balance,balance2">
								<a4j:actionparam name="instanceCreatedId"
									assignTo="#{TaxAllocationMatrixViewBean.instanceCreatedId}" actionListener="#{TaxAllocationMatrixViewBean.deleteMatrixDetail}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
			            </rich:column>
					</rich:dataTable>
				</div>
			</div>
			
			<h:panelGroup rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}">
			<table cellpadding="2" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px">Total:</td>
					<td class="column-input">
						<h:outputText id="total" value="#{TaxAllocationMatrixViewBean.selectedMatrix.totalAllocationPercent}" >
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label" width="150px" >Remaining:</td>
					<td class="column-input">
						<h:outputText id="balance" value="#{TaxAllocationMatrixViewBean.remainingAllocationPercent}">
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
						</h:outputText>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</tbody>
			</table>
			</h:panelGroup>
		</div>
	</div>
</div>

<h:panelGroup id="allocDriverPanelGroup" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction and !TaxAllocationMatrixViewBean.futureSplit}">
<div id="top">
	<div id="table-one">
		<div id="table-one-content">
			<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
				<tbody>
					<tr>
						<th>Allocation Drivers</th>
					</tr>
				</tbody>
			</table>
			
			<h:panelGrid id="allocDriverPanelControl"
				binding="#{TaxAllocationMatrixViewBean.allocDriverPanel}"
				styleClass="panel-ruler"/>
				
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Allocation Percent:</td>
					<td class="column-input">
						<h:inputText id="taxAllocPercentControl" label="Allocation Percent" immediate="true"
							binding="#{TaxAllocationMatrixViewBean.allocationPercent}" style="text-align:right;"
							onkeypress="return onlyNumerics(event);">
							<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>
							<f:validateDoubleRange minimum="0" maximum="1"/>
						</h:inputText>
					</td>
					<td colspan="4">
						<h:outputText value="Please enter a percentage between 0.00 and "/>
						<h:outputText id="balance2" value="#{1 - TaxAllocationMatrixViewBean.selectedMatrix.totalAllocationPercent}">
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
						</h:outputText>
						<h:outputText value="."/>
					</td>
					<td class="column-spacer">&#160;</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">Comments:</td>
					<td colspan="7">
						<h:inputText id="taxAllocCommentsControl" label="Comments" immediate="true"
							binding="#{TaxAllocationMatrixViewBean.allocationComment}" style="width: 678px;" />
					</td>
				</tr>
				<tr>
					<td class="column-spacer">&#160;</td>
					<td class="column-label">&#160;</td>
					<td>
						<h:commandButton id="Add" value="Add" immediate="true" action="#{TaxAllocationMatrixViewBean.addMatrixDetail}" reRender="aMDetail"/>
					</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-label">&#160;</td>
					<td class="column-input">&#160;</td>
					<td class="column-spacer">&#160;</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</h:panelGroup>
-->

<div id="table-four-bottom">
	<ul class="right">
		<li><h:commandLink id="individualJurisSplitHiddenBtn" style="display: none;" rendered="true" action="#{TaxAllocationMatrixViewBean.individualJurisSplitAction_jeesmon}"/></li>
		<li class="jurissplit"><h:commandLink id="jurisSplitBtn" rendered="#{!TaxAllocationMatrixViewBean.globalSplit and !TaxAllocationMatrixViewBean.displayDeleteAction and !TaxAllocationMatrixViewBean.viewAction}" action="#{TaxAllocationMatrixViewBean.globalJurisSplitAction}"/></li>
  		<li class="jurissplitenabled"><h:commandLink id="jurisSplitEnabledBtn" rendered="#{TaxAllocationMatrixViewBean.globalSplit and !TaxAllocationMatrixViewBean.viewAction}" action="#{TaxAllocationMatrixViewBean.globalJurisSplitAction}"/></li>
		<li class="copy-update">
			<h:commandLink id="btnCopyUpdate" rendered="#{TaxAllocationMatrixViewBean.updateAction}" 
				action="#{TaxAllocationMatrixViewBean.displayCopyUpdateAction}" />
		</li>
		<li class="ok"><h:commandLink id="okBtn" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}" action="#{TaxAllocationMatrixViewBean.saveAction}"/></li>
		<li class="ok"><h:commandLink id="okBtndel" rendered="#{TaxAllocationMatrixViewBean.readOnlyAction and TaxAllocationMatrixViewBean.deleteAction}" action="#{TaxAllocationMatrixViewBean.deleteAction}"/></li>
		<li class="ok"><h:commandLink id="okBtnview" rendered="#{TaxAllocationMatrixViewBean.readOnlyAction and TaxAllocationMatrixViewBean.viewAction}" immediate="true" action="#{TaxAllocationMatrixViewBean.cancelAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!TaxAllocationMatrixViewBean.viewAction}" immediate="true" action="#{TaxAllocationMatrixViewBean.cancelAction}" /></li>	
	</ul>
</div>

</div>
</h:form>

<ui:include src="/WEB-INF/view/components/generic_warning.xhtml">
	<ui:param name="id" value="globalJurisWarningId"/>
	<ui:param name="formId" value="globalJurisWarningFormId"/>
	<ui:param name="panelGridId" value="globalJurisWarningPanelGridId"/>
	<ui:param name="okBtnId" value="globalJurisWarningOkBtnId"/>
	<ui:param name="title" value="Warning"/>
	<ui:param name="warning" value="Any individual Allocations will be deleted. Continue?"/>
	<ui:param name="okBtn" value="matrixDetailForm:jurisSplitBtn"/>
	<ui:param name="bean" value="#{TaxAllocationMatrixViewBean}"/>
	<ui:param name="cancelAction" value="globalJurisWarningCancel"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/generic_warning.xhtml">
	<ui:param name="id" value="individualJurisWarningId"/>
	<ui:param name="formId" value="individualJurisWarningFormId"/>
	<ui:param name="panelGridId" value="individualJurisWarningPanelGridId"/>
	<ui:param name="okBtnId" value="individualJurisWarningOkBtnId"/>
	<ui:param name="title" value="Warning"/>
	<ui:param name="warning" value="Global Allocation will be deleted. Continue?"/>
	<ui:param name="okBtn" value="matrixDetailForm:individualJurisSplitHiddenBtn"/>
	<ui:param name="bean" value="#{TaxAllocationMatrixViewBean}"/>
	<ui:param name="cancelAction" value="individualJurisWarningCancel"/>
</ui:include>

</ui:define>
</ui:composition>
</html>