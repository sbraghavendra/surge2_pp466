<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('transForm:aMList', 'aMListRowIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetail', 'aMDetailListRowIndex'); } );
	registerEvent(window, "unload", function() { document.getElementById('transForm:hiddenSaveLink').onclick(); });
	
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="aMListRowIndex" value="#{TaxAllocationMatrixViewBean.selectedMasterRowIndex}"/>
<h:inputHidden id="aMDetailListRowIndex" value="#{TaxAllocationMatrixViewBean.selectedRowIndex}"/>
<h:form id="transForm">
	<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
		<a4j:support event="onclick" immediate="true" actionListener="#{TaxAllocationMatrixViewBean.filterSaveAction}" />
	</a4j:commandLink>
	
	<h:panelGroup rendered="#{!TaxAllocationMatrixViewBean.findMode}">
		<h1>	<h:graphicImage id="imageTax"
 				 alt="TaxCode Allocations"
 				 url="/images/headers/hdr-GoodsAndServicesAllocations.png">
			</h:graphicImage></h1>
	</h:panelGroup>
	
	<h:panelGroup rendered="#{TaxAllocationMatrixViewBean.findMode}">
		<h1><h:graphicImage id="image1"
 				 alt="Taxability Matrix"
 				 url="/images/headers/hdr-lookup-taxability-matrix-id.gif">
			</h:graphicImage>  </h1>
	</h:panelGroup>
	
	<div class="tab">
		<h:graphicImage id="image2"
 				 alt="Selection Filter" 
 				 url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
    		<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
      				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{TaxAllocationMatrixViewBean.togglePanelController.toggleHideSearchPanel}" >
      			<h:outputText value="#{(TaxAllocationMatrixViewBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
      		</a4j:commandLink>					
    	</div> 
  	
    	<a4j:outputPanel id="showhidefilter">
       	<c:if test="#{!TaxAllocationMatrixViewBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/taxallocation_panel.xhtml">
						<ui:param name="handlerBean" value="#{TaxAllocationMatrixViewBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel >
	      	</div>
	     	</div>
		</c:if>  
	</a4j:outputPanel>
 	

	<rich:modalPanel id="loader" zindex="2000" autosized="true">
		<h:outputText value="Processing Search ..." />
	</rich:modalPanel>

	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false"
			width="0" top="-30" height="0" left="-30" resizeable="false" />
			
	<div class="wrapper">
		<span class="block-right">&#160;</span> 
		<span
			class="block-left tab"> 
			<h:graphicImage id="image3"
				alt="Tax Matrix Lines And Details"
				url="/images/containers/STSTaxAllocMatrixLinesAndDetails.gif">
			</h:graphicImage> 
		</span>
	</div>
	
	<!-- start of displaying 3 datagrids -->
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
	<tbody>
	<tr>
	<td style="height:auto;vertical-align:top;" colspan="2">
	 
		<div id="bottom" style="padding: 0 0 3px 0; width:952px;">
	    	<div id="table-four"  >
		    <div id="table-four-top" style="padding-left: 15px;"><h:outputText value="Drivers&#160;&#160;&#160;&#160;&#160;&#160;" /><a4j:status
						id="pageInfo" startText="Request being processed..."
						stopText="#{TaxAllocationMatrixViewBean.matrixDataModel.pageDescription}"
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		   	</div>
		   
			<!-- start first grid -->
	       	<div class="scrollInnerNew" id="resize" >
				<rich:dataTable rowClasses="odd-row,even-row" 
					id="aMList" value="#{TaxAllocationMatrixViewBean.matrixDataModel}"
					var="matrix" rows="#{TaxAllocationMatrixViewBean.matrixDataModel.pageSize}">
					
					<a4j:support event="onRowClick"
						onsubmit="selectRow('transForm:aMList', this);"
						actionListener="#{TaxAllocationMatrixViewBean.selectedMasterRowChanged}"
						reRender="aMDetail,aMTaxAlloc,copyAddBtn,deleteBtn,viewBtn,suspendBtn,updateBtn,okBtn,suspendForm:matrixId"
						oncomplete="initScrollingTables();" />
					
					<rich:column id="driver01"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver01"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver01']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver01}" />
					</rich:column>
					
					<rich:column id="driver02"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver02"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver02']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver02}" />
					</rich:column>
	
					<rich:column id="driver03"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver03"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver03']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver03}" />
					</rich:column>
	
					<rich:column id="driver04"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver04"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver04']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver04}" />
					</rich:column>
	
					<rich:column id="driver05"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver05"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver05']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver05}" />
					</rich:column>
	
					<rich:column id="driver06"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver06"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver06']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver06}" />
					</rich:column>
	
					<rich:column id="driver07"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver07"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver07']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver07}" />
					</rich:column>
	
					<rich:column id="driver08"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver08"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver08']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver08}" />
					</rich:column>
	
					<rich:column id="driver09"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver09"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver09']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver09}" />
					</rich:column>
	
					<rich:column id="driver10"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver10"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver10']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver10}" />
					</rich:column>
	
					<rich:column id="driver11"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver11"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver11']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver11}" />
					</rich:column>
	
					<rich:column id="driver12"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver12"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver12']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver12}" />
					</rich:column>
	
					<rich:column id="driver13"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver13"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver13']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver13}" />
					</rich:column>
	
					<rich:column id="driver14"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver14"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver14']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver14}" />
					</rich:column>
	
					<rich:column id="driver15"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver15"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver15']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver15}" />
					</rich:column>
	
					<rich:column id="driver16"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver16"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver16']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver16}" />
					</rich:column>
	
					<rich:column id="driver17"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver17"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver17']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver17}" />
					</rich:column>
	
					<rich:column id="driver18"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver18"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver18']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver18}" />
					</rich:column>
	
					<rich:column id="driver19"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver19"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver19']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver19}" />
					</rich:column>
	
					<rich:column id="driver20"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver20"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver20']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver20}" />
					</rich:column>
	
					<rich:column id="driver21"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver21"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver21']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver21}" />
					</rich:column>
	
					<rich:column id="driver22"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver22"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver22']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver22}" />
					</rich:column>
	
					<rich:column id="driver23"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver23"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver23']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver23}" />
					</rich:column>
	
					<rich:column id="driver24"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver24"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver24']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver24}" />
					</rich:column>
	
					<rich:column id="driver25"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver25"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver25']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver25}" />
					</rich:column>
	
					<rich:column id="driver26"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver26"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver26']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver26}" />
					</rich:column>
	
					<rich:column id="driver27"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver27"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver27']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver27}" />
					</rich:column>
	
					<rich:column id="driver28"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver28"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver28']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver28}" />
					</rich:column>
	
					<rich:column id="driver29"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver29"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver29']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver29}" />
					</rich:column>
	
					<rich:column id="driver30"
						rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}">
						<f:facet name="header">
							<a4j:commandLink id="s_driver30"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['driver30']}"
								value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.driver30}" />
					</rich:column>
					
					<rich:column id="binaryWeight">
						<f:facet name="header">
							<a4j:commandLink id="s_binaryWeight"
								styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortOrder['binaryWeight']}"
								value="Binary Weight"
								actionListener="#{TaxAllocationMatrixViewBean.sortAction}"
								oncomplete="initScrollingTables();" immediate="true"
								reRender="aMList" />
						</f:facet>
						<h:outputText value="#{matrix.binaryWeight}" />
					</rich:column>
				</rich:dataTable>
			</div><!-- end first grid --> 
		    
		    <rich:datascroller id="trScroll" for="aMList" maxPages="10"
							oncomplete="initScrollingTables();" style="clear:both;"
							align="center" stepControls="auto" ajaxSingle="false"
							page="#{TaxAllocationMatrixViewBean.matrixDataModel.curPage}"
							reRender="pageInfo" />
							
	       	<div id="table-four-bottom">
				<ul class="right">
					<li class="add"><h:commandLink id="addBtn" style="#{(TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxAllocationMatrixViewBean.displayAddAction}"/></li>
				</ul> 
			</div>           
		                  
	       	</div><!-- end of table-four -->
	    </div>
	
	</td>
	</tr>
	</tbody>
	</table>

   
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
	<tbody>
	<tr>
	<td style="height:auto;vertical-align:top;">
	 
	    <div id="bottom" style="padding: 0 0 3px 0; width:572px;">
	    	<div id="table-four"  >
		    <div id="table-four-top" style="padding: 3px 0 0 10px;"><h:outputText value="Matrix Lines" />
		    </div>
			
			<!-- start middle grid -->				
			<div class="scrollInnerNew" id="resize" style="width:530px;">
			
				<rich:dataTable rowClasses="odd-row,even-row" 
					id="aMDetail" value="#{TaxAllocationMatrixViewBean.selectedMatrixDetails}"
					var="taxAllocationMatrixDetail">
					
					<a4j:support event="onRowClick"
						onsubmit="selectRow('transForm:aMDetail', this);"
						actionListener="#{TaxAllocationMatrixViewBean.selectedRowChanged}"
						reRender="aMTaxAlloc,copyAddBtn,deleteBtn,viewBtn,suspendBtn,updateBtn,okBtn,suspendForm:matrixId"
						oncomplete="initScrollingTables();" />
					
	                <rich:column id="taxAllocationMatrixId">
	                    <f:facet name="header">
							<a4j:commandLink id="s_taxAllocationMatrixId" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['taxAllocationMatrixId']}"
								value="Matrix Id"
								actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMDetail"/>
	                    </f:facet>
	                    <h:outputText value="#{taxAllocationMatrixDetail.taxAllocationMatrixId}"/>
	                </rich:column>
	
	                <rich:column id="effectiveDate">
	                    <f:facet name="header">
							<a4j:commandLink id="s_effectiveDate" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['effectiveDate']}"
								value="Effective Date"
								actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMDetail"/>
	                    </f:facet>
	                    <h:outputText  value="#{taxAllocationMatrixDetail.effectiveDate}">	
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
	                </rich:column>      
	                
	                <rich:column id="expirationDate">
	                    <f:facet name="header">
							<a4j:commandLink id="s_expirationDate" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['expirationDate']}"
								value="Expiration Date"
								actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMDetail"/>
	                    </f:facet>
	                    <h:outputText  value="#{taxAllocationMatrixDetail.expirationDate}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
	                </rich:column> 
	
					<rich:column id="futureSplit">
	                    <f:facet name="header">
							<a4j:commandLink id="s_futureSplit" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['futureSplit']}"
								value="Future Split?"
								actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMDetail"/>
	                    </f:facet>
	                    <h:selectBooleanCheckbox styleClass="check"
	                                 	value="#{taxAllocationMatrixDetail.futureSplit == 1}"
	                                 	disabled="#{true}" />
	                </rich:column>
	                
	                <rich:column id="activeFlag">
	                    <f:facet name="header">
							<a4j:commandLink id="s_activeflag" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['activeFlag']}"
								value="Active?"
								actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
								immediate="true" reRender="aMDetail"/>
	                    </f:facet>
	                    <h:selectBooleanCheckbox styleClass="check"
	                                 	value="#{taxAllocationMatrixDetail.activeFlag == 1}"
	                                 	disabled="#{true}" />
	                </rich:column>
	 
	                <rich:column id="globalheader">
	                    <f:facet name="header">
							<a4j:commandLink id="s_global" value="Global?" />
	                    </f:facet>
	                    <h:selectBooleanCheckbox styleClass="check"
	                                 	value="#{taxAllocationMatrixDetail.globalSplit}"
	                                 	disabled="#{true}"/>
	                </rich:column>			
	            			                
				</rich:dataTable>
		
			</div><!-- end middle grid -->	
	
			<div id="table-four-bottom">
				<ul class="right">
					<li class="copy-add"><h:commandLink id="copyAddBtn" style="#{(TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxAllocationMatrixViewBean.validSelection or TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxAllocationMatrixViewBean.displayCopyAddAction}"/></li>
					<li class="update"><h:commandLink id="updateBtn" style="#{(TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxAllocationMatrixViewBean.validSelection or TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxAllocationMatrixViewBean.displayUpdateAction}"/></li>
					<li class="delete115"><h:commandLink id="deleteBtn" style="#{(TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!TaxAllocationMatrixViewBean.validSelection or TaxAllocationMatrixViewBean.currentUser.viewOnlyBooleanFlag }" action="#{TaxAllocationMatrixViewBean.displayDeleteAction}"/></li>
				    <li class="view"><h:commandLink id="viewBtn" disabled="#{!TaxAllocationMatrixViewBean.validSelection}" action="#{TaxAllocationMatrixViewBean.displayViewAction}"/></li>
				</ul>
			</div>
			
	                                 
	       </div>
	    </div>
	
	</td>
	<td style="height:auto;vertical-align:top;" >
	
	    <div id="bottom" style="padding: 0 0 3px 0; width:360px;">
	       	<div id="table-four"  style="margin:0;" >
	       	   	<div id="table-four-top" style="padding: 3px 0 0 10px;"><h:outputText value="Entities&#160;&#160;&#160;" /></div>       	   	
	       	   
       	   		<!-- start third grid -->
				<div class="scrollInnerNew" id="resize" style="width:359px;">			
					<rich:dataTable rowClasses="odd-row,even-row" id="aMTaxAlloc"     
		                value="#{TaxAllocationMatrixViewBean.selectedMatrixTaxAllocDetails}"
		                var="taxAllocMatrixDetail" >
		                
		                <rich:column id="detailtaxCode">
		                    <f:facet name="header">
		                    	<a4j:commandLink id="s_detailtaxCode" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['taxcodeCode']}"
									value="TaxCode"
									actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
									immediate="true" reRender="aMDetail"/>
		                    </f:facet>
		                    <h:outputText value="#{taxAllocMatrixDetail.taxcodeCode}" />
		                </rich:column>	
		                
		                <rich:column id="detaildescription">
		                    <f:facet name="header"><a4j:commandLink id="s_detaildescription" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['taxcodeCode']}"
									value="Description"
									actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
									immediate="true" reRender="aMDetail"/></f:facet>
		                    <h:outputText value="#{cacheManager.taxCodeMap[taxAllocMatrixDetail.taxcodeCode].description}" />
		                </rich:column>	
		                						
						<rich:column id="allocationPercent">
		                    <f:facet name="header"><a4j:commandLink id="s_allocationPercent" styleClass="sort-#{TaxAllocationMatrixViewBean.matrixDataModel.sortDetailOrder['allocationPercent']}"
									value="Allocation Percent"
									actionListener="#{TaxAllocationMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
									immediate="true" reRender="aMDetail"/></f:facet>
		                    <h:outputText value="#{taxAllocMatrixDetail.allocationPercent}">
									<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
							</h:outputText>
		                </rich:column>
		                
		                <rich:column id="individualheader">
		                    <f:facet name="header">
								<a4j:commandLink id="s_individual"
									value="Individual?"/></f:facet>
		                    <h:selectBooleanCheckbox styleClass="check" 
                                   	value="#{TaxAllocationMatrixViewBean.jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId]}"
                                   	disabled="#{true}"
                                  />
		                </rich:column>									
		            </rich:dataTable>			
				</div><!-- end third grid -->
                               
	       	<div id="table-four-bottom">
	       	</div><!-- table-four-bottom -->  
	      	</div><!-- table-four -->   
	    </div><!-- bottom -->
		
	</td>
	</tr>
	</tbody>
	</table>
	<!-- end of displaying 3 datagrids -->
 
</h:form>

</ui:define>
</ui:composition>
</html>