<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { displayWarning(); } );
registerEvent(window, "load", function() { displayDeleteWarning(); } );

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function displayDeleteWarning() {
	 var warning = document.getElementById('displayDeleteWarning');
	 var val = parseInt(warning.value);
	 if (val != 0) {
	 	javascript:Richfaces.showModalPanel('deletewarning');
	 }
}

function addAllocation()
{
	var percent = document.getElementById("matrixDetailForm:jurisdictionControl").value*100;
	if(percent>100 || percent=="") {
		alert("Please enter a decimal value less than or equal to 1" );
		return false;
	} else if (isNaN(percent)) {
		alert("Allocation percent should be a decimal number");
		return false;
	}
	return true;
}
function numToString(num, decimalNum) {
	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// Get sign of number
	
	// Adjust number so only the specified number of numbers after
	// the decimal point are shown.
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Readjust for sign
	return new String(tmpNum);
}

function updateDummy() {
	
}

function updateTotal() {
  var total = 0.0;
  for (var i = 0; i < 1000; i++) {
    var pct = document.getElementById("matrixDetailForm:aMDetail:"+i+":pct");
    if (!pct) break;
    total += parseFloat(pct.value);
  } 

  var out = formatNumber(numToString(total,10), 2);
  replaceInnerHTML("matrixDetailForm:total",out);

  out = formatNumber(numToString(1.0 - total,10), 2); 
  replaceInnerHTML("matrixDetailForm:balance",out);
  replaceInnerHTML("matrixDetailForm:balance2",out);
}

function replaceInnerHTML(id,val){
	var obj = document.getElementById(id);
	if(obj) {
		obj.innerHTML = val;
	}
}


//]]>
</script>
</ui:define>

<ui:define name="body">
	<h:inputHidden id="displayWarning" value="#{TaxAllocationMatrixViewBean.displayWarning}"/>
	<h:inputHidden id="displayDeleteWarning" immediate="true" value="#{TaxAllocationMatrixViewBean.displayDeleteWarning}"/>

	<h:form id="matrixDetailForm" >
		<h1><img id="imgAllocationMatrix" alt="Allocation Matrix" src="../images/headers/hdr-allocation-matrix.png" width="250" height="19" /></h1>
		<div id="top">
		<div id="table-one">
		<div id="table-four-top">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>
		<div id="table-one-content">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<thead>
				<tr>
					<td colspan="4">Add Jurisdiction Allocations</td>
					<td colspan="4" style="text-align:right;">Taxability Allocation Matrix ID:&#160;<h:outputText value="#{TaxAllocationMatrixViewBean.matrixId}"/></td>
				</tr>
			</thead>
			<tr>
				<th colspan="9">Drivers</th>
			</tr>
		</table>

		<h:panelGrid 
			binding="#{TaxAllocationMatrixViewBean.jurisdictionViewPanel}"
			styleClass="panel-ruler"/>
			
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
			<tr>
				<th colspan="9">Details</th>
			</tr>
			<tr>
				<td style="width:20px;">&#160;</td>
				<td style="width:200px;">Effective Date:</td>

				<td style="width:100px;"><h:inputText id="effDate" style="width:100px;" disabled="true" value="#{TaxAllocationMatrixViewBean.selectedMatrix.effectiveDate}"
						converter="date" datePattern="M/d/yyyy" />
				</td>
				<td style="width:23px;">&#160;</td>
				<td style="width:150px;">
					<h:outputText value="Future Split?:"/>
					<h:selectBooleanCheckbox id="futureSplitControl" styleClass="check" 
						disabled="true"
						value="#{TaxAllocationMatrixViewBean.futureSplit}" immediate="true">
						<a4j:support event="onclick" action="tax_alloc_matrix_add" />
					</h:selectBooleanCheckbox>
				</td>
				<td style="width:600px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:20px;">&#160;</td>
				<td style="width:200px;">Expiration Date:</td>
				
				<td style="width:100px;"><h:inputText id="expDate" style="width:100px;" disabled="true" value="#{TaxAllocationMatrixViewBean.selectedMatrix.expirationDate}"
						converter="date" datePattern="M/d/yyyy" />
				</td>
				<td style="width:23px;">&#160;</td>
				<td style="width:150px;">
					<h:outputText value="Active?:"/>
					<h:selectBooleanCheckbox id="activeControl" styleClass="check" 
						disabled="true"
						value="#{TaxAllocationMatrixViewBean.selectedMatrix.activeBooleanFlag}" immediate="true">
						<a4j:support event="onclick"  />
					</h:selectBooleanCheckbox>
				</td>
				<td style="width:600px;">&#160;</td>
			</tr>
			
			<tr>
				<td style="width:20px;">&#160;</td>
				<td style="width:200px;">Comments:</td>
				<td colspan="7">
					<h:inputText id="comments" style="width:678px;" 
						disabled="true"
						value="#{TaxAllocationMatrixViewBean.selectedMatrix.comments}"
						maxlength="255"/>
				</td>
			</tr>
			
			<tr>
	        	<td colspan="9">&#160;</td>
			</tr>
			
			</tbody>
		</table>
		
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
			<tr>
				<th colspan="7">TaxCode</th>
			</tr>
			
			
			
			<tr>
				<td style="width:20px;">&#160;</td>
				<td style="width:200px;">TaxCode:</td>
				<td style="width:200px;">
					<h:inputText id="taxcodefield" style="width:200px;" 
						disabled="true" value="#{TaxAllocationMatrixViewBean.splitTaxCode}"/>
				</td>
				
				<td style="width:23px;">&#160;</td>
				
				<td >Description:</td>
				<td style="width:300px;">
					<h:inputText id="descriptionfield" style="width:500px;" 
						disabled="true" value="#{TaxAllocationMatrixViewBean.splitTaxCodeDescription}"/>
				</td>
				
				<td style="width:50px;">&#160;</td>
			</tr>
			
			<tr>
	        	<td colspan="7">&#160;</td>
			</tr>
			
			</tbody>
		</table>
		
		<a4j:outputPanel id="forEachPanelWhen">
		<span>
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>		
			<tr>
				<th colspan="14">Allocations</th>
			</tr>
				
			<tr>
	        	<td style="width:20px;">&#160;</td> 
	        	<td style="width:20px;">&#160;</td>    
		        <td><h:outputText style="width: 100px;" value="Geocode" /></td>
		        <td><h:outputText style="width: 100px;" value="Country" /></td>
		        <td><h:outputText style="width: 100px;" value="State" /></td>
		        <td><h:outputText style="width: 100px;" value="County" /></td>
		        <td><h:outputText style="width: 100px;" value="City" /></td>
		        <td><h:outputText style="width: 100px;" value="Zip Code" /></td>
		        <td><h:outputText style="width: 100px;" value="Allocation Percent" /></td>
	        	
				<td width="50px">&#160;</td>
	            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
	            <td width="15px">
					<a4j:commandLink id="addJurisbtn" styleClass="button" rendered="#{!TaxAllocationMatrixViewBean.displayDeleteAction}" 
						reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processAddJurisdictionCommand}" >
						<h:graphicImage value="/images/addButton.png" />
						<f:param name="command" value="#{loopCounter.index}" />
					</a4j:commandLink>
					<h:outputText style="width: 15px;" value="&#160;" rendered="#{TaxAllocationMatrixViewBean.displayDeleteAction}" />
				</td> 
				<td width="1%">&#160;</td> 
			</tr>
			  
			<c:forEach var="allocMatrixJurisDetail" items="#{TaxAllocationMatrixViewBean.selectedMatrixJurisAllocDetails}" varStatus="loopCounter" >
	        <tr>
	        	<td style="width:20px;">&#160;</td>
	        
	        	
	        	<td>
	        	<!-- 
	        		<h:commandButton id="fieldsearch_#{loopCounter.index}" styleClass="image" style="float:left;width:16px;height:16px;" 
								image="/images/search_small.png" immediate="true" 
								
								actionListener="#{TaxAllocationMatrixViewBean.findJurisdictionIdCommand}"
								action="#{TaxAllocationMatrixViewBean.findJurisdictionIdAction}">
								
								<f:param name="command" value="#{loopCounter.index}" />
						</h:commandButton>
						-->
						<a4j:commandButton id="fieldsearch_#{loopCounter.index}" type="button" styleClass="image" style="float:left;width:16px;height:16px;" rendered="#{!TaxAllocationMatrixViewBean.displayDeleteAction}"
								image="/images/search_small.png" immediate="true" >
								<a4j:support event="onclick" actionListener="#{TaxAllocationMatrixViewBean.findJurisdictionIdCommand}" action="#{TaxAllocationMatrixViewBean.findJurisdictionIdAction}" >
									<f:param name="command" value="#{loopCounter.index}" />
								</a4j:support>
						</a4j:commandButton>
						<h:outputText style="width: 15px;" value="&#160;" rendered="#{TaxAllocationMatrixViewBean.displayDeleteAction}" />
	        	</td>
	        	
 
		        <td>           	
					<a4j:outputPanel id="fieldGeocode_#{loopCounter.index}">   
						<h:inputText id="fieldvalueGeocode_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.geocode}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
		        <td>           	
					<a4j:outputPanel id="fieldCountry_#{loopCounter.index}">   
						<h:inputText id="fieldvalueCountry_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.country}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
		        <td>           	
					<a4j:outputPanel id="fieldState_#{loopCounter.index}">   
						<h:inputText id="fieldvalueState_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.state}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
		        <td>           	
					<a4j:outputPanel id="fieldCounty_#{loopCounter.index}">   
						<h:inputText id="fieldvalueCounty_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.county}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
		        <td>           	
					<a4j:outputPanel id="fieldCity_#{loopCounter.index}">   
						<h:inputText id="fieldvalueCity_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.city}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
		        <td>           	
					<a4j:outputPanel id="fieldZip_#{loopCounter.index}">   
						<h:inputText id="fieldvalueZip_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.jurisdiction.zip}" disabled="true" />				 	
		        	</a4j:outputPanel>
		        </td>
	
		        
		        <td> 
		        	<a4j:outputPanel id="fieldvaluepaneWhen_#{loopCounter.index}">   
			<!-- 
						<h:inputText id="fieldvalueWhen_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.allocationPercent}" >
							<a4j:support id="ajaxslistener_#{loopCounter.index}" event="onblur" immediate="true" reRender="balance" ajaxSingle="true" />	
						</h:inputText> 
				 		
						
						<h:inputText id="fieldvalueWhen_#{loopCounter.index}" style="width: 100px;" value="#{allocMatrixJurisDetail.allocationPercent}" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}" 							
									onkeypress="return onlyNumerics(event);">
							<a4j:support id="ajaxslistener_#{loopCounter.index}" event="onchange" actionListener="#{TaxAllocationMatrixViewBean.allocationPercentCommand}"  />
							<f:param name="command" value="#{loopCounter.index}" />
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
							<f:validateDoubleRange minimum="0" maximum="1"/>
						</h:inputText>	
					
					
						<h:inputText id="fieldvalueWhen_#{loopCounter.index}" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}" label="Allocation Percent"
							value="#{allocMatrixJurisDetail.allocationPercent}" style="text-align: right;" onkeypress="return onlyNumerics(event);">
							
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
							<f:validateDoubleRange minimum="0" maximum="1"/>
						</h:inputText>
						
			-->	
		
					 	
						
						
						<h:inputText id="fieldvalueWhen_#{loopCounter.index}"  label="Allocation Percent" disabled="#{TaxAllocationMatrixViewBean.displayDeleteAction}"
							value="#{allocMatrixJurisDetail.allocationPercent}" style="text-align: right;"
							onkeypress="return onlyNumerics(event);">
							<a4j:support id="ajaxslistener_#{loopCounter.index}" event="onblur" reRender="balance3" ajaxSingle="true"  />
							<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
							<f:validateDoubleRange minimum="0" maximum="1"/>
						</h:inputText>	
		     
						
							
		        	</a4j:outputPanel>
		        </td> 
		        
	        
	        
				<td width="50px">&#160;</td>
		 	
	
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				<!--  
				<td width="15px">
				
					<a4j:commandLink id="addWhen_#{loopCounter.index}" styleClass="button" rendered="#{(loopCounter.index eq TaxAllocationMatrixViewBean.lastJurisdictionObject)}"
						reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processAddJurisdictionCommand}" >
						<h:graphicImage value="/images/addButton.png" />
						<f:param name="command" value="#{loopCounter.index}" />
					</a4j:commandLink>
					<h:outputText style="width: 15px;" value="&#160;" rendered="#{!(loopCounter.index eq TaxAllocationMatrixViewBean.lastJurisdictionObject)}" />
				</td>
				-->            
	            <td width="15px">
	            	<a4j:commandLink id="removeWhen_#{loopCounter.index}" styleClass="button"  rendered="#{!TaxAllocationMatrixViewBean.displayDeleteAction}"
	            		reRender="forEachPanelWhen" actionListener="#{TaxAllocationMatrixViewBean.processRemoveJurisdictionCommand}" >
	            		<h:graphicImage value="/images/deleteButton.png" />
	            		<f:param name="command" value="#{loopCounter.index}" />
	            	</a4j:commandLink>
	            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{TaxAllocationMatrixViewBean.displayDeleteAction}" />

	            </td>
	            
	 			<td width="10%">&#160;</td> 
	        </tr>
	    	</c:forEach>
	    	
	    	<tr>
	        	<td style="width:20px;">&#160;</td>  
	        
	        	<td width="30px">&#160;</td>
	        	
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
		    
		        <c:if test="#{TaxAllocationMatrixViewBean.displayDeleteAction}">
			     	<td align="center"><font color="red"><h:outputText style="width:100px; color:ref" 
			     		value="#{(TaxAllocationMatrixViewBean.allowDeleteTaxAllocationMatrix) ? '&#160;' : 'The Matrix line has been used on a Transaction and cannot be deleted. Click Ok to set to inactive.'}" /></font></td>
		        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				</c:if>  
				
				<c:if test="#{!TaxAllocationMatrixViewBean.displayDeleteAction}">
			     	<td colspan="2" align="right"><h:outputText style="width: 200px;" value="Remaining to Allocate:" /></td>
		        	<td class="column-input" style="width: 100px;" >&#160;
						<h:outputText id="balance3" value="#{TaxAllocationMatrixViewBean.remainingJuriAllocationPercent}" >
							<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</td>
				</c:if>
	        	
				
	        	
	        	
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
	            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
	            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
				<td width="10%">&#160;</td> 
			</tr>
	
			<tr>
	        	<td colspan="14">&#160;</td>
			</tr>
		</tbody>
		</table>
		</span>
		</a4j:outputPanel>

		</div> <!-- table-one-content -->
		<div id="table-four-bottom">
		
			<ul class="right">
				<li class="delete"><h:commandLink id="btnDelete" rendered="#{TaxAllocationMatrixViewBean.displayJurisDelete}" immediate="true" action="#{TaxAllocationMatrixViewBean.jurisDeleteAction}"/></li>
				<li class="ok"><h:commandLink id="okBtn" rendered="#{!TaxAllocationMatrixViewBean.readOnlyAction}" action="#{TaxAllocationMatrixViewBean.jurisSaveAction}"/></li>
				<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!TaxAllocationMatrixViewBean.viewAction}" immediate="true" action="#{TaxAllocationMatrixViewBean.jurisCancelAction}" /></li>
			</ul>
		</div>
		</div>
		</div>
		
		
	</h:form>

<ui:include src="/WEB-INF/view/components/jurisdiction_search.xhtml">
	<ui:param name="handler" value="#{TaxAllocationMatrixViewBean.jurisdictionHandler}"/>
	<ui:param name="popupName" value="searchJurisdiction"/>
	<ui:param name="popupForm" value="searchJurisdictionForm"/>
	<ui:param name="jurisId" value="jurisId"/>
	<ui:param name="reRender" value="matrixDetailForm:jurisInput"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{TaxAllocationMatrixViewBean}"/>
	<ui:param name="id" value="warning"/>
	<ui:param name="warning" value="Warning - not all driver values were found.  Save anyway?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBtn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/deleteWarning.xhtml">
	<ui:param name="bean" value="#{TaxAllocationMatrixViewBean}"/>
	<ui:param name="id" value="deletewarning"/>
	<ui:param name="warning" value="Are you sure?"/>
	<ui:param name="okBtn" value="matrixDetailForm:btnDelete"/>
</ui:include>

</ui:define>

</ui:composition>
		
		
</html>
