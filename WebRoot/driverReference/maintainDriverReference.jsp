<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('maintainDriverRefForm:driverNames', 'nameListRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('maintainDriverRefForm:driverReferences', 'referenceListRowIndex'); } );
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="nameListRowIndex" value="#{DriverReferenceBean.selectedNameIndex}" />
<h:inputHidden id="referenceListRowIndex" value="#{DriverReferenceBean.selectedReferenceIndex}" />
<h:form id="maintainDriverRefForm">

<h1><img id="imgDriverReference" alt="Driver Reference" src="../images/headers/hdr-driver-reference.gif" width="250" height="19" /></h1>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSelectDriver.gif" />
	</span>
</div>

<div id="bottom">
	<div id="table-four"><h:messages styleClass="error"/>
	<div id="table-one-top" style="padding-left: 10px">
		<h:outputLabel id="categorySelectionLabel" for="categorySelection" value="Driver: " />
		<h:selectOneMenu id="categorySelection" style = "width:200px"  value="#{DriverReferenceBean.selectedCategory}"
			valueChangeListener="#{DriverReferenceBean.onCategoryChange}">
			<f:selectItems value="#{DriverReferenceBean.categoriesMenuItems}" />
			<a4j:support ajaxSingle="true" event="onchange" 
				reRender="driverNames,driverReferences,addButton,updateButton,deleteButton,viewButton"
				oncomplete="initScrollingTables();" />
		</h:selectOneMenu>
	</div>
	<div id="table-four-content">

	<div class="scrollContainer">
	<div class="scrollInner">

	<rich:dataTable rowClasses="odd-row,even-row" 
		id="driverNames" styleClass="GridContent"
		value="#{DriverReferenceBean.driverNames}" var="nameDTO">

		<a4j:support event="onRowClick"
			onsubmit="selectRow('maintainDriverRefForm:driverNames', this);"
			actionListener="#{DriverReferenceBean.selectedNameRowChanged}"
			reRender="driverReferences,addButton,updateButton,deleteButton,viewButton" />

		<rich:column sortBy="#{nameDTO.driverNamesPK.driverId}">
			<f:facet name="header"><h:outputText value="Driver ID" /></f:facet>
			<h:outputText id="rowDriverId" value="#{nameDTO.driverNamesPK.driverId}" />
		</rich:column>

		<rich:column>
			<f:facet name="header"><h:outputText value="Description" /></f:facet>
			<h:outputText id="rowTransDtlColDesc" value="#{DriverReferenceBean.transDtlColNameToDescriptionMap[nameDTO.transDtlColName]}" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.transDtlColName}">
			<f:facet name="header"><h:outputText value="Trans. Dtl Column Name" /></f:facet>
			<h:outputText id="rowTransDtlColName" value="#{nameDTO.transDtlColName}" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.matrixColName}">
			<f:facet name="header"><h:outputText value="Matrix Column Name" /></f:facet>
			<h:outputText id="rowMatrixColName" value="#{nameDTO.matrixColName}" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.businessUnitFlagBoolean}">
			<f:facet name="header"><h:outputText value="Bus. Unit?" /></f:facet>
			<h:selectBooleanCheckbox id="rowBusinessUnitFlagBoolean" styleClass="check" value="#{nameDTO.businessUnitFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.mandatoryFlagBoolean}">
			<f:facet name="header"><h:outputText value="Mandatory?" /></f:facet>
			<h:selectBooleanCheckbox id="rowMandatoryFlagBoolean" styleClass="check" value="#{nameDTO.mandatoryFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.nullDriverFlagBoolean}">
			<f:facet name="header"><h:outputText value="Null Driver?" /></f:facet>
			<h:selectBooleanCheckbox id="rowNullDriverFlagBoolean" styleClass="check" value="#{nameDTO.nullDriverFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.wildcardFlagBoolean}">
			<f:facet name="header"><h:outputText value="Wildcard Allowed?" /></f:facet>
			<h:selectBooleanCheckbox id="rowWildcardFlagBoolean" styleClass="check" value="#{nameDTO.wildcardFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.rangeFlagBoolean}">
			<f:facet name="header"><h:outputText value="Range Allowed?" /></f:facet>
			<h:selectBooleanCheckbox id="rowRangeFlagBoolean" styleClass="check" value="#{nameDTO.rangeFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.toNumberFlagBoolean}">
			<f:facet name="header"><h:outputText value="Convert to Number?" /></f:facet>
			<h:selectBooleanCheckbox id="rowToNumberFlagBoolean" styleClass="check" value="#{nameDTO.toNumberFlagBoolean}" disabled="true" />
		</rich:column>

		<rich:column sortBy="#{nameDTO.driverWeight}">
			<f:facet name="header"><h:outputText value="Driver Weight" /></f:facet>
			<h:outputText id="rowDriverWeight" value="#{nameDTO.driverWeight}">
				<f:convertNumber integerOnly="true" />
			</h:outputText>
		</rich:column>
	
	</rich:dataTable>

	</div>
	</div>

	</div>

	<div id="table-one-bottom">
		<ul class="right">
		</ul>
	</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
		<img src="../images/containers/STSSecurity-details-open.gif"/>
	</span>
</div>

<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">

	<div class="scrollContainer">
	<div class="scrollInner">

	<rich:dataTable rowClasses="odd-row,even-row" 
		id="driverReferences" styleClass="GridContent"
		value="#{DriverReferenceBean.driverReferences}" var="referenceDTO">

		<a4j:support event="onRowClick"
			onsubmit="selectRow('maintainDriverRefForm:driverReferences', this);"
			actionListener="#{DriverReferenceBean.selectedReferenceRowChanged}"
			reRender="addButton,updateButton,deleteButton,viewButton" />
	
		<rich:column sortBy="#{referenceDTO.driverValue}">
			<f:facet name="header"><h:outputText value="Driver Value" /></f:facet>
			<h:outputText id="rowDriverValue" value="#{referenceDTO.driverValue}" />
		</rich:column>
	
		<rich:column sortBy="#{referenceDTO.driverDesc}">
			<f:facet name="header"><h:outputText value="Driver Description" /></f:facet>
			<h:outputText id="rowDriverDesc" value="#{referenceDTO.driverDesc}" />
		</rich:column>
	
		<rich:column sortBy="#{referenceDTO.userValue}">
			<f:facet name="header"><h:outputText value="User Value" /></f:facet>
			<h:outputText id="rowUserValue" value="#{referenceDTO.userValue}" />
		</rich:column>

	</rich:dataTable>
	
	</div>
	</div>
	
	</div>

	<div id="table-four-bottom">
		<ul class="right">
			<li class="add"><h:commandLink id="addButton"  style="#{(DriverReferenceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!DriverReferenceBean.validSelectedDriverName or DriverReferenceBean.currentUser.viewOnlyBooleanFlag}" action="#{DriverReferenceBean.processActionCommand}" ><f:param name="command" value="addAction" /></h:commandLink></li>
			<li class="update"><h:commandLink id="updateButton" style="#{(DriverReferenceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{DriverReferenceBean.processActionCommand}" disabled="#{!DriverReferenceBean.validSelectedReferenceName or DriverReferenceBean.currentUser.viewOnlyBooleanFlag}"><f:param name="command" value="updateAction" /></h:commandLink></li>
			<li class="delete"><h:commandLink id="deleteButton" style="#{(DriverReferenceBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{DriverReferenceBean.processActionCommand}" disabled="#{!DriverReferenceBean.validSelectedReferenceName or DriverReferenceBean.currentUser.viewOnlyBooleanFlag}" ><f:param name="command" value="deleteAction" /></h:commandLink></li>
			<li class="view"><h:commandLink id="viewButton"  action="#{DriverReferenceBean.processActionCommand}" disabled="#{!DriverReferenceBean.validSelectedReferenceName}"><f:param name="command" value="viewAction" /></h:commandLink></li>
		</ul>
	</div>

	</div>
</div>

</h:form>

</ui:define>

</ui:composition>

</html>
