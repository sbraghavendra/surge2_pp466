<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">

<h:form id="editDriverRefForm">

<h1><img id="imgDriverReference" alt="Driver Reference" src="../images/headers/hdr-driver-reference.gif" width="250" height="19" /></h1>

<div id="top">
	<div id="table-one">
	<div id="table-one-top"><h:messages /></div>
	<div id="table-one-content">
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{DriverReferenceBean.currentActionText}" /></td></tr>
		</thead>

		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Trans. Dtl. Column Name: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.editReferenceName.transDtlColName}"
						id="txtTransDtlColName" style="width: 95%;" size="20" required="true" disabled="true" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Description: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.transDtlColNameToDescriptionMap[DriverReferenceBean.editReferenceName.transDtlColName]}"
						id="txtTransDtlColDesc" style="width: 95%;" size="20" disabled="true" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Driver Value: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.editReferenceName.driverValue}"
						id="txtDriverValue" style="width: 95%;" size="20" required="true" disabled="#{!DriverReferenceBean.isCurrentActionAdd}" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Driver Description: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText id="driverDesc" value="#{DriverReferenceBean.editReferenceName.driverDesc}"
						style="width: 95%;" size="20" required="true" disabled="#{DriverReferenceBean.isCurrentActionDelete or DriverReferenceBean.isCurrentActionView }"/>
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> User Value: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.editReferenceName.userValue}"
						id="txtUserValue" style="width: 95%;" size="20" disabled="#{DriverReferenceBean.isCurrentActionDelete or DriverReferenceBean.isCurrentActionView}" />
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Last Update User ID: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.editReferenceName.updateUserId}"
						id="txtUpdateUserId" style="width: 95%;" size="20" disabled="true"/>
				</td>
			</tr>

			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"> Last Update Timestamp: </td> 
				<td class="column-input" style="width: 75%;">
					<h:inputText value="#{DriverReferenceBean.editReferenceName.updateTimestamp}"
						id="txtUpdateTimestamp" style="width: 95%;" size="20" disabled="true">
						<f:converter converterId="dateTime"/>
					</h:inputText>
				</td>
			</tr>

		</tbody>
	</table>
	</div>

	<div id="table-one-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk" action="#{DriverReferenceBean.processActionCommandResults}"><f:param name="command" value="#{DriverReferenceBean.currentAction}" /></h:commandLink></li>
			<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{DriverReferenceBean.isCurrentActionView}" action="#{DriverReferenceBean.cancelAction}" /></li>		
		</ul>
	</div>
	</div>
</div>

</h:form>

</ui:define>

</ui:composition>

</html>
