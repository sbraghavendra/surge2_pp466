<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('listCodesForm:listCodesTable', 'listCodesTableRowIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="listCodesTableRowIndex" value="#{listCodesBackingBean.selectedRowIndex}"/>
<h:form id="listCodesForm">
<h1><h:graphicImage id="imgListCodes" alt="List Codes" value="/images/headers/hdr-list-codes.gif"/></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div><!--  remove this title once Image is ready to use in the header -->
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
        <tr>
            <td style="width: 10px;">&#160;</td>
            <td>
				<h:selectOneMenu id="SelCodeType" title="Select Code Type"
					style="width: 388px;" 
					binding="#{listCodesBackingBean.codeCodeMenu}"	
					value="#{listCodesBackingBean.codeTypeCode}">																					
					<a4j:support event="onchange" action="#{listCodesBackingBean.retrieveListCodes}" reRender="listCodesTable,t1add,updt3,delt2"/>
				</h:selectOneMenu>
			</td>
        </tr>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right"></ul></div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">

		<div class="scrollContainer">
		<div class="scrollInner" id="resize" >
		 <rich:dataTable rowClasses="odd-row,even-row" 
			id="listCodesTable" styleClass="GridContent"
			value="#{listCodesBackingBean.listCodesList}" var="codeDtlTbl">
			
		     <a4j:support id="clk" event="onRowClick" 
					onsubmit="selectRow('listCodesForm:listCodesTable', this);"
			       	actionListener="#{listCodesBackingBean.selectedRowChanged}" reRender="updt3,delt2"/>
		
			<rich:column id="codeCode" sortBy="#{codeDtlTbl.codeCode}">
				<f:facet name="header"><h:outputText id="id2x" styleClass="headerText" value="Code Code" /></f:facet>
				<h:outputText value="#{codeDtlTbl.codeCode}"/>
			</rich:column>

			<rich:column id="description" sortBy="#{codeDtlTbl.description}" width="300px">
				<f:facet name="header"><h:outputText id="id3x" styleClass="headerText" value="Description"/></f:facet>
				<h:outputText value="#{codeDtlTbl.description}" />
			</rich:column>
		</rich:dataTable>
		</div>
		</div>

	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="t1add" style="#{(listCodesBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{empty listCodesBackingBean.codeTypeCode or listCodesBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{listCodesBackingBean.displayAddAction}" /></li>
		<li class="update"><h:commandLink id="updt3" style="#{(listCodesBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{empty listCodesBackingBean.codeTypeCode or empty listCodesBackingBean.selectedListCodes or listCodesBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{listCodesBackingBean.displayUpdateAction}" /></li>	
		<li class="delete"><h:commandLink id="delt2" style="#{(listCodesBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{empty listCodesBackingBean.codeTypeCode or empty listCodesBackingBean.selectedListCodes or listCodesBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{listCodesBackingBean.displayDeleteAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>
