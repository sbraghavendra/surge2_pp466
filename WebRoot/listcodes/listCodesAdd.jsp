<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="listCodesForm">
<h1><h:graphicImage id="imgListCodes" alt="List Codes" value="/images/headers/hdr-list-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblListCodeMaint" value="#{listCodesBackingBean.actionText}"/> a List Code</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Code Type Code:</td>
				<td style="width:700px;">
				<h:inputText value="#{listCodesBackingBean.selectedListCodes.codeTypeCode}" 
				         disabled="true" id="txtCodeTypeCode"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Code:</td>
				<td>
				<h:inputText value="#{listCodesBackingBean.selectedListCodes.codeCode}" 
						required="true" label="Code" id="txtCodeCode"
						validator="#{listCodesBackingBean.validateCode}"
					 	disabled="#{listCodesBackingBean.viewAction or listCodesBackingBean.deleteAction or listCodesBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" 
						 onblur="upperCaseInputText(this)"
				         maxlength="10" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText value="#{listCodesBackingBean.selectedListCodes.description}" 
					 	 disabled="#{listCodesBackingBean.viewAction or listCodesBackingBean.deleteAction}"
				         style="width:650px;" id="txtDescription" 
				         maxlength="50"/>
                </td>
			</tr>
			<div id="text-display-selection">
				<h:panelGroup rendered="#{!listCodesBackingBean.selectedDefListCodes.disableMessage}">
				<tr>
					<td style="width:50px;">&#160;</td>
					<td>Message:</td>
					<td>
					<h:inputText value="#{listCodesBackingBean.selectedListCodes.message}" 
						 	 disabled="#{listCodesBackingBean.deleteAction}"
					         style="width:650px;" id="txtMessage"
					         maxlength="50" />
	                </td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{!listCodesBackingBean.selectedDefListCodes.disableExplanation}">
				<tr>
					<td style="width:50px;">&#160;</td>
					<td>Explanation:</td>
					<td>
					<h:inputText value="#{listCodesBackingBean.selectedListCodes.explanation}" 
						 	 disabled="#{listCodesBackingBean.deleteAction}"
					         style="width:650px;" id="txtExplanation"
					         maxlength="255" />
	                </td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{!listCodesBackingBean.selectedDefListCodes.disableSeverityLevel}">
				<tr>
					<td style="width:50px;">&#160;</td>
					<td>Severity Level:</td>
					<td>
					<h:inputText value="#{listCodesBackingBean.selectedListCodes.severityLevel}" 
						 	 disabled="#{listCodesBackingBean.deleteAction}"
					         style="width:650px;" id="txtSeverityLevel"
					         maxlength="10" />
	                </td>
				</tr>
				</h:panelGroup>
				<h:panelGroup rendered="#{!listCodesBackingBean.selectedDefListCodes.disableWriteImportLineFlag}">
				<tr>
					<td style="width:50px;">&#160;</td>
					<td>Import Line Flag:</td>
					<td>
	                 	 <h:selectBooleanCheckbox styleClass="check" id="imprtLn"  
	                 	 	 value="#{listCodesBackingBean.selectedListCodes.writeImportLineFlagDisplay}"
						 	 disabled="#{listCodesBackingBean.deleteAction}" >
	                      </h:selectBooleanCheckbox>
	                 </td>
				</tr>
				</h:panelGroup>
				
				<h:panelGroup rendered="#{!listCodesBackingBean.selectedDefListCodes.disableAbortImportFlag}">
				<tr>
					<td style="width:50px;">&#160;</td>
					<td>Abort Import Flag:</td>
					<td>
	                 	 <h:selectBooleanCheckbox styleClass="check" id="abortLn"  
	                 	 	 value="#{listCodesBackingBean.selectedListCodes.abortImportFlagDisplay}" 
						 	 disabled="#{listCodesBackingBean.deleteAction}" >
	                      </h:selectBooleanCheckbox>
	                 </td>
				</tr>	
				</h:panelGroup>
			</div>
							
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{listCodesBackingBean.selectedListCodes.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{listCodesBackingBean.selectedListCodes.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{listCodesBackingBean.okAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{listCodesBackingBean.viewAction}" immediate="true" action="#{listCodesBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>