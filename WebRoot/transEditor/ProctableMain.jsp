<ui:component xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('proctableForm:proctableTable', 'selectedProctableRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('proctableForm:proctableDetailTable', 'selectedProctableDetailRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
//]]>
</script>

<style>
.rich-stglpanel-marker {float: left}
.rich-stglpanel-header {text-align: left}
</style>


</ui:define>
<ui:define name="body">
<f:view>
<h:inputHidden id="selectedProctableRowIndex" value="#{proctableBean.selectedProctableRowIndex}" />
<h:inputHidden id="selectedProctableDetailRowIndex" value="#{proctableBean.selectedProctableDetailRowIndex}" />
    
<h:form id="proctableForm">
     <h1><h:graphicImage id="imgprocesstables-purchasing" alt="Process&#32;&#38;&#32;Tables" rendered="#{proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-purch.png" width="250" height="19" /></h1>
     <h1><h:graphicImage id="imgprocesstables-sales" alt="Process&#32;&#38;&#32;Tables" rendered="#{!proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-sales.png" width="250" height="19" /></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
     	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{proctableBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(proctableBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       	</a4j:commandLink>					
     </div> 
     
     <a4j:outputPanel id="showhidefilter">
     	<c:if test="#{!proctableBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/processtables_panel.xhtml">
						<ui:param name="handlerBean" value="#{proctableBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel >
	      	</div>
	     	</div>
		</c:if>
	</a4j:outputPanel>
     
    <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-Tables.png" width="192"  height="17" />
      </span>
    </div>
   
	<!-- table-four-contentNew  -->     
	<div id="table-four-contentNew">
        
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
    <tbody>
	<tr>
	<td style="height:auto;vertical-align:top;">
 
    <div id="bottom" style="padding: 0 0 3px 0;">
    	<div id="table-four"  >
	    <div id="table-four-top" style="padding: 3px 0 0 10px;" ><h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Tables&#160;&#160;&#160;&#160;&#160;" />
	    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{proctableBean.proctableDataModel.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	    </div>
       
        <div class="scrollInnerNew" id="resize" style="width:620px;">
       	<rich:dataTable rowClasses="odd-row,even-row" id="proctableTable" rows="#{proctableBean.proctableDataModel.pageSize }"  value="#{proctableBean.proctableDataModel}" var="proctableMaintenance"  >
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('proctableForm:proctableTable', this);"  actionListener="#{proctableBean.selectedProctableRowChanged}"
                        reRender="proctableDetailTable,populateproctable,addproctable,updateproctable,deleteproctable,viewproctable,proctableDetailDataTest,addproctableentries
                        ,updateproctableentries,deleteproctableentries,viewproctableentries,entriespageInfo,trScrollDetail" />
                                  	
           	<rich:column id="proctableId" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="proctableId-a4j"
                              styleClass="sort-#{proctableBean.proctableDataModel.sortOrder['proctableId']}"
                              value="Table Id"
                              actionListener="#{proctableBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="proctableTable" />
            	</f:facet>
            	<h:outputText value="#{proctableMaintenance.proctableId}" style="#{(proctableMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>           	
           	          	
           	<rich:column id="proctableName" style="width:120px" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="proctableName-a4j"
                              styleClass="sort-#{proctableBean.proctableDataModel.sortOrder['proctableName']}"
                              value="Name"
                              actionListener="#{proctableBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="proctableTable" />
            	</f:facet>
            	<h:outputText value="#{proctableMaintenance.proctableName}" style="#{(proctableMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           <rich:column id="description" style="width:320px" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="description-a4j"
                              styleClass="sort-#{proctableBean.proctableDataModel.sortOrder['description']}"
                              value="Description"
                              actionListener="#{proctableBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="proctableTable" />
            	</f:facet>
            	<h:outputText value="#{proctableMaintenance.description}" style="#{(proctableMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="activeFlag" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="activeFlag-a4j"
                              styleClass="sort-#{proctableBean.proctableDataModel.sortOrder['activeFlag']}"
                              value="Active?"
                              actionListener="#{proctableBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="proctableTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" value="#{proctableMaintenance.activeFlag == 1}" disabled="#{true}" id="activeFlagId" />
           	</rich:column>
          </rich:dataTable>
          
          <a4j:outputPanel id="custDataTest">
          <c:if test="#{proctableBean.proctableDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
         
         </div>
         <!-- scroll-inner -->

       <rich:datascroller id="trScroll"  for="proctableTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{proctableBean.proctableDataModel.curPage}" />
                                  
       <div id="table-four-bottom">
        	<ul class="right">
			<li class="populate"><h:commandLink id="populateproctable" disabled="#{!proctableBean.displayProctableButtons}" immediate="true" action="#{proctableBean.displayPopulateProctableFromMainAction}" 
												style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
		    </li>         	
			<li class="add"><h:commandLink id="addproctable" immediate="true" action="#{proctableBean.displayAddProctableAction}" disabled="#{proctableBean.currentUser.viewOnlyBooleanFlag}" 
											style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
			</li>          
         	<li class="update"><h:commandLink id="updateproctable" disabled="#{!proctableBean.displayProctableButtons or proctableBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{proctableBean.displayUpdateProctableAction}"
         	 								  style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	 </li>
         	<li class="delete115"><h:commandLink id="deleteproctable" disabled="#{!proctableBean.displayProctableButtons or proctableBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{proctableBean.displayDeleteProctableAction}"
         	 									 style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	 </li>
         	<li class="view"><h:commandLink id="viewproctable" disabled="#{!proctableBean.displayProctableButtons}" immediate="true"  action="#{proctableBean.displayViewProctableAction}"></h:commandLink>
         	 </li>

        	</ul>
       </div> <!-- table-four-bottom -->   
       <!-- end custGrid -->
       
       </div>
    </div>
	</td>
	
	<td style="height:auto;vertical-align:top;" >
  	<div id="bottom" style="padding: 0 0 3px 0;">
       <div id="table-four"  style="margin:0;" >
		<div id="table-four-top" style="padding-left: 2px; padding-bottom: 0;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>
					<td>
						<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Entries&#160;&#160;&#160;" />
					</td>
				 	<td align="left" >
					  	<rich:modalPanel id="entriesrequestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-20" resizeable="false" />
						<a4j:status id="entriespageInfo" forceId="true"
							startText="Request being processed..." 
							stopText="#{proctableBean.pageDescription}"
							onstart="javascript:Richfaces.showModalPanel('entriesrequestProcessed');"
     						onstop="javascript:Richfaces.hideModalPanel('entriesrequestProcessed');" />
					</td>  
					</tr>
				</tbody>
			</table>
		</div>
       
        <div class="scrollInner"  id="resize1" style="width:600px;">
         	<rich:dataTable rowClasses="odd-row,even-row" id="proctableDetailTable" 
         	rows="#{proctableBean.entriesPageSize}"  value="#{proctableBean.selectedProctableDetails}" var="proctableDetailMaintenance" >
         	<a4j:support id="populatedata"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('proctableForm:proctableDetailTable', this);"  actionListener="#{proctableBean.selectedProcDetailtableRowChanged}"
                        reRender="updateproctableentries,deleteproctableentries,viewproctableentries" />

            <rich:column id="proctableDetailId" sortBy="#{proctableDetailMaintenance.proctableDetailId}">
                <f:facet name="header"><h:outputText value="Detail Id" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.proctableDetailId}"/>
            </rich:column>
            
            <!-- in column -->
            <rich:column id="inColumn01Id" rendered="#{!(empty proctableBean.selProctable.inColumnType01)}" sortBy="#{proctableDetailMaintenance.inColumn01}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType01 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName01] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName01]) : proctableBean.selProctable.inColumnName01}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn01}"/>
            </rich:column>
            
            <rich:column id="inColumn02Id" rendered="#{!(empty proctableBean.selProctable.inColumnType02)}" sortBy="#{proctableDetailMaintenance.inColumn02}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType02 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName02] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName02]) : proctableBean.selProctable.inColumnName02}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn02}"/>
            </rich:column>
            
            <rich:column id="inColumn03Id" rendered="#{!(empty proctableBean.selProctable.inColumnType03)}" sortBy="#{proctableDetailMaintenance.inColumn03}" >
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType03 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName03] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName03]) : proctableBean.selProctable.inColumnName03}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn03}"/>
            </rich:column>
            
            <rich:column id="inColumn04Id" rendered="#{!(empty proctableBean.selProctable.inColumnType04)}" sortBy="#{proctableDetailMaintenance.inColumn04}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType04 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName04] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName04]) : proctableBean.selProctable.inColumnName04}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn04}"/>
            </rich:column>
            
            <rich:column id="inColumn05Id" rendered="#{!(empty proctableBean.selProctable.inColumnType05)}" sortBy="#{proctableDetailMaintenance.inColumn05}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType05 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName05] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName05]) : proctableBean.selProctable.inColumnName05}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn05}"/>
            </rich:column>
            
            <rich:column id="inColumn06Id" rendered="#{!(empty proctableBean.selProctable.inColumnType06)}" sortBy="#{proctableDetailMaintenance.inColumn06}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType06 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName06] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName06]) : proctableBean.selProctable.inColumnName06}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn06}"/>
            </rich:column>
            
            <rich:column id="inColumn07Id" rendered="#{!(empty proctableBean.selProctable.inColumnType07)}" sortBy="#{proctableDetailMaintenance.inColumn07}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType07 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName07] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName07]) : proctableBean.selProctable.inColumnName07}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn07}"/>
            </rich:column>
            
            <rich:column id="inColumn08Id" rendered="#{!(empty proctableBean.selProctable.inColumnType08)}" sortBy="#{proctableDetailMaintenance.inColumn08}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType08 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName08] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName08]) : proctableBean.selProctable.inColumnName08}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn08}"/>
            </rich:column>
            
            <rich:column id="inColumn09Id" rendered="#{!(empty proctableBean.selProctable.inColumnType09)}" sortBy="#{proctableDetailMaintenance.inColumn09}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType09 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName09] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName09]) : proctableBean.selProctable.inColumnName09}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn09}"/>
            </rich:column>
            
            <rich:column id="inColumn10Id" rendered="#{!(empty proctableBean.selProctable.inColumnType10)}" sortBy="#{proctableDetailMaintenance.inColumn10}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType10 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName10] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName10]) : proctableBean.selProctable.inColumnName10}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn10}"/>
            </rich:column>
            
            <rich:column id="inColumn11Id" rendered="#{!(empty proctableBean.selProctable.inColumnType11)}" sortBy="#{proctableDetailMaintenance.inColumn11}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType11 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName11] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName11]) : proctableBean.selProctable.inColumnName11}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn11}"/>
            </rich:column>
            
            <rich:column id="inColumn12Id" rendered="#{!(empty proctableBean.selProctable.inColumnType12)}" sortBy="#{proctableDetailMaintenance.inColumn12}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType12 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName12] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName12]) : proctableBean.selProctable.inColumnName12}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn12}"/>
            </rich:column>
            
            <rich:column id="inColumn13Id" rendered="#{!(empty proctableBean.selProctable.inColumnType13)}" sortBy="#{proctableDetailMaintenance.inColumn13}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType13 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName13] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName13]) : proctableBean.selProctable.inColumnName13}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn13}"/>
            </rich:column>
            
            <rich:column id="inColumn14Id" rendered="#{!(empty proctableBean.selProctable.inColumnType14)}" sortBy="#{proctableDetailMaintenance.inColumn14}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType14 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName14] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName14]) : proctableBean.selProctable.inColumnName14}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn14}"/>
            </rich:column>
            
            <rich:column id="inColumn15Id" rendered="#{!(empty proctableBean.selProctable.inColumnType15)}" sortBy="#{proctableDetailMaintenance.inColumn15}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType15 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName15] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName15]) : proctableBean.selProctable.inColumnName15}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn15}"/>
            </rich:column>
            
            <rich:column id="inColumn16Id" rendered="#{!(empty proctableBean.selProctable.inColumnType16)}" sortBy="#{proctableDetailMaintenance.inColumn16}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType16 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName16] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName16]) : proctableBean.selProctable.inColumnName16}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn16}"/>
            </rich:column>
            
            <rich:column id="inColumn17Id" rendered="#{!(empty proctableBean.selProctable.inColumnType17)}" sortBy="#{proctableDetailMaintenance.inColumn17}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType17 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName17] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName17]) : proctableBean.selProctable.inColumnName17}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn17}"/>
            </rich:column>
            
            <rich:column id="inColumn18Id" rendered="#{!(empty proctableBean.selProctable.inColumnType18)}" sortBy="#{proctableDetailMaintenance.inColumn18}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType18 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName18] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName18]) : proctableBean.selProctable.inColumnName18}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn18}"/>
            </rich:column>
            
            <rich:column id="inColumn19Id" rendered="#{!(empty proctableBean.selProctable.inColumnType19)}" sortBy="#{proctableDetailMaintenance.inColumn19}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType19 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName19] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName19]) : proctableBean.selProctable.inColumnName19}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn19}"/>
            </rich:column>
            
            <rich:column id="inColumn20Id" rendered="#{!(empty proctableBean.selProctable.inColumnType20)}" sortBy="#{proctableDetailMaintenance.inColumn20}">
                <f:facet name="header"><h:outputText value="#{(proctableBean.selProctable.inColumnType20 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.selProctable.inColumnName20] : matrixCommonBean.salesColumnNameMap[proctableBean.selProctable.inColumnName20]) : proctableBean.selProctable.inColumnName20}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{proctableDetailMaintenance.inColumn20}"/>
            </rich:column>
            
            <!-- out column -->
            <rich:column id="outColumnName01Id" rendered="#{!(empty proctableBean.selProctable.outColumnType01)}" sortBy="#{proctableDetailMaintenance.outColumnName01}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName01}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType01 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName01] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName01]) : proctableDetailMaintenance.outColumnName01}" />
            </rich:column>
            
            <rich:column id="outColumnName02Id" rendered="#{!(empty proctableBean.selProctable.outColumnType02)}" sortBy="#{proctableDetailMaintenance.outColumnName02}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName02}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType02 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName02] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName02])  : proctableDetailMaintenance.outColumnName02}" />
            </rich:column>
            
            <rich:column id="outColumnName03Id" rendered="#{!(empty proctableBean.selProctable.outColumnType03)}" sortBy="#{proctableDetailMaintenance.outColumnName03}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName03}" style = "color:#336690"  /></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType03 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName03] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName03])  : proctableDetailMaintenance.outColumnName03}" />
            </rich:column>
            
            <rich:column id="outColumnName04Id" rendered="#{!(empty proctableBean.selProctable.outColumnType04)}" sortBy="#{proctableDetailMaintenance.outColumnName04}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName04}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType04 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName04] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName04])  : proctableDetailMaintenance.outColumnName04}" />
            </rich:column>
            
			<rich:column id="outColumnName05Id" rendered="#{!(empty proctableBean.selProctable.outColumnType05)}" sortBy="#{proctableDetailMaintenance.outColumnName05}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName05}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType05 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName05] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName05])  : proctableDetailMaintenance.outColumnName05}" />
            </rich:column>
            
            <rich:column id="outColumnName06Id" rendered="#{!(empty proctableBean.selProctable.outColumnType06)}" sortBy="#{proctableDetailMaintenance.outColumnName06}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName06}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType06 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName06] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName06])  : proctableDetailMaintenance.outColumnName06}" />
            </rich:column>
            
            <rich:column id="outColumnName07Id" rendered="#{!(empty proctableBean.selProctable.outColumnType07)}" sortBy="#{proctableDetailMaintenance.outColumnName07}" >
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName07}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType07 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName07] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName07])  : proctableDetailMaintenance.outColumnName07}" />
            </rich:column>
            
            <rich:column id="outColumnName08Id" rendered="#{!(empty proctableBean.selProctable.outColumnType08)}" sortBy="#{proctableDetailMaintenance.outColumnName08}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName08}"  /></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType08 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName08] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName08])  : proctableDetailMaintenance.outColumnName08}" />
            </rich:column>
            
            <rich:column id="outColumnName09Id" rendered="#{!(empty proctableBean.selProctable.outColumnType09)}" sortBy="#{proctableDetailMaintenance.outColumnName09}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName09}" style = "color:#336690" /></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType09 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName09] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName09])  : proctableDetailMaintenance.outColumnName09}" />
            </rich:column>
            
            <rich:column id="outColumnName10Id" rendered="#{!(empty proctableBean.selProctable.outColumnType10)}" sortBy="#{proctableDetailMaintenance.outColumnName10}">
                <f:facet name="header"><h:outputText value="#{proctableBean.selProctable.outColumnName10}"  style = "color:#336690"/></f:facet>
                <h:outputText value="#{(proctableDetailMaintenance.outColumnType10 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableDetailMaintenance.outColumnName10] : matrixCommonBean.salesColumnNameMap[proctableDetailMaintenance.outColumnName10])  : proctableDetailMaintenance.outColumnName10}" />
            </rich:column>
            
          </rich:dataTable> 	
          

          <a4j:outputPanel id="proctableDetailDataTest">
           		<c:if test="#{proctableBean.proctableDataModel.rowCount == 0}">
					<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          		</c:if>
          		<c:if test="#{!proctableBean.isSelectedProctableDetailsExist and proctableBean.selectedProctableRowIndex >=0}">
					<div class="nodatadisplay"><h:outputText value="No Entities Exist for the selected Table." /></div>
          		</c:if>
          		<c:if test="#{proctableBean.selectedProctableRowIndex == -1 and proctableBean.proctableDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Table Selected." /></div>
          		</c:if>
           </a4j:outputPanel>

		</div>
        <!-- scroll-inner -->
        
            
        <rich:datascroller id="trScrollDetail"  for="proctableDetailTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="entriespageInfo" page="#{proctableBean.entriesPageNumber}" />
        
                                   
       	<div id="table-four-bottom">
        	<ul class="right">
         	<li class="add"><h:commandLink id="addproctableentries" disabled="#{!proctableBean.displayProctableButtons or proctableBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{proctableBean.addProctabledetailAction}" 
         									style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	</li>          
         	<li class="update"><h:commandLink id="updateproctableentries" disabled="#{!proctableBean.displayProctabledetailButtons or proctableBean.currentUser.viewOnlyBooleanFlag }" immediate="true" action="#{proctableBean.updateProctabledetailAction}" 
         									style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	</li>
         	<li class="delete115"><h:commandLink id="deleteproctableentries" disabled="#{!proctableBean.displayProctabledetailButtons or proctableBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{proctableBean.deleteProctabledetailAction}" 
         										style="#{(proctableBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	</li>
         	<li class="view"><h:commandLink id="viewproctableentries" disabled="#{!proctableBean.displayProctabledetailButtons}" immediate="true"  action="#{proctableBean.viewProctabledetailAction}"></h:commandLink>
         	 </li>

        	</ul>
       </div> <!-- table-four-bottom --> 
       	<!-- end custLocnGrid --> 

      	</div>  <!-- table-four -->   
    </div>   <!-- bottom -->
	
	</td>
	</tr>
	</tbody>
	</table>

	</div>
    <!-- end of table-four-contentNew  -->  
     
</h:form>
</f:view>
   
<rich:modalPanel id="execute" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Execute" />
	</f:facet>
	<h:form id="executeForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('execute');" action="#{taxHolidayBean.executeAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('execute'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>
   
</ui:define>
</ui:composition>
</ui:component>
