<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
	<h1><h:graphicImage id="imgprocesstables-purchasing" alt="Process&#32;&#38;&#32;Tables" rendered="#{proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-purch.png" width="250" height="19" /></h1>
	<h1><h:graphicImage id="imgprocesstables-sales" alt="Process&#32;&#38;&#32;Tables" rendered="#{!proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-sales.png" width="250" height="19" /></h1>
     

<h:form id="proctableDetailpdateForm">
<div id="top" style="width:838px;">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:408px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="6"><h:outputText value="#{proctableBean.actionText}"/> a Process Table Entry</td></tr>
	</thead>
	<tbody>			
		<tr>
			<td style="width: 176px;padding-left: 25px;">Table Name:</td>
			<td style="width:150px;">
				<h:inputText id="proctableName" style="width:150px;" value="#{proctableBean.updateProctable.proctableName}" disabled="true" />
			</td>
			<td style="width: 46px;">&#160;</td>
			<td style="width: 90px;">Description:</td>
			<td style="width:285px;">
				<h:inputText id="description" style="width:287px;" value="#{proctableBean.updateProctable.description}" maxlength = "50" disabled="true" />
			</td>
			<td style="width: 5px;">&#160;</td>
		</tr>
		
		 <tr>
			<td style="width: 150px;padding-left: 25px;">Active?:</td>
	        <td style="width:200px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" disabled="true" 
					value="#{proctableBean.updateProctable.activeBooleanFlag}" styleClass="check"  />
			</td>
			<td style="width: 10px;">&#160;</td>
			<td style="width: 90px;">&#160;</td>
			<td style="width:287px;">&#160;</td>
			<td style="width: 5px;">&#160;</td>
		</tr>  
		
	</tbody>
	</table>
	
	<a4j:outputPanel id="addprocessdetail">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>			
		<tr>
	        <th style="width: 150px;">Input Column</th>
	        <th style="width: 100px;"><h:outputText style="width: 100px;" value="Value" /></th>
			<th style="width: 50px;"><h:outputText style="width: 50px;" value="&#160;" /></th>            
            <th style="width: 15px;"><h:outputText style="width: 15px;" value="&#160;" /></th>
            <th style="width: 15px;"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th style="width: 100px;">&#160;</th> 
		</tr>
		   
		<c:forEach var="displaydata" items="#{proctableBean.incolumnsList}" varStatus="counter">
        <tr>
        	<td style="width: 150px;padding-left: 25px;"> 
        	<h:outputText style="width: 150px;" value="#{(displaydata.searchType eq 'COLUMN') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[displaydata.columnheading] : matrixCommonBean.salesColumnNameMap[displaydata.columnheading]) : displaydata.columnheading }" />
	        </td>
			
	  		<c:if test="#{displaydata.isColumnSearchType}">
			<td style="width: 150px;"> 	
				<h:inputText id="fieldColumn_#{counter.index}" rendered="#{displaydata.isColumnSearchType}" style="width: 150px;" maxlength = "100" value="#{displaydata.columnSearchName}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}"/>			
	        </td>
	        
	        </c:if>
			
			<c:if test="#{displaydata.isTextSearchType}">
			<td style="width: 100px;"> 	
				<h:inputText id="fieldText_#{counter.index}" rendered="#{displaydata.isTextSearchType}" style="width: 150px;" maxlength = "100" value="#{displaydata.textSearchName}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />				
	        </td> 
			</c:if>
			<td style="width: 50px;">&#160;</td>
			<td style="width: 15px;">&#160;</td>
			<td style="width: 15px;">&#160;</td>
			<td style="width: 200px;">&#160;</td>
	    </tr>
			</c:forEach>
		  
		<tr>
	        <th style="width: 150px;">Output Column</th>
	        <th style="width: 100px;"><h:outputText  value="Value" /></th>
			<th style= "width:50px">&#160;</th>
			<th style= "width:15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th style= "width:15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th style= "width:100px">&#160;</th> 
		</tr>
			<c:forEach var="displaydata" items="#{proctableBean.outcolumnsList}" varStatus="counter">
			<c:if test="#{displaydata.isComboSearchType}">
	        <tr>
	        	<td style="width: 150px;padding-left: 25px;"> 
        	<h:outputText id = "outputvalueid_#{counter.index}" style="width: 150px;" value="#{(displaydata.searchType eq 'COLUMN') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[displaydata.columnheading] : matrixCommonBean.salesColumnNameMap[displaydata.columnheading]) : displaydata.columnheading }" />
	        </td>
	        <td style="width: 100px;">           	
				<h:selectOneMenu id="fieldColumnColumn_#{counter.index}"  rendered="#{displaydata.isComboSearchType}" style="width: 100px;"  
					value="#{displaydata.operation}" immediate="true" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}">
					<f:selectItems value="#{proctableBean.valuePopulateItems}"/>
					<a4j:support event="onchange" action="#{proctableBean.valueoutObjectColumnChange}" reRender="fieldvaluePanel_#{counter.index}" >
						<f:param name="command" value="#{counter.index}" />
					</a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td style = "width:50px"> 
	        	<a4j:outputPanel id="fieldvaluePanel_#{counter.index}" rendered="#{displaydata.isComboSearchType}">   
	        	
	        		<h:selectOneMenu id="allfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'ALL'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.allSaleTransItems}"/>
					</h:selectOneMenu>
					       	
					<h:selectOneMenu id="sales_basicfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_BASIC'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.basicTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_customerfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_CUSTOMER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.customerTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_invoicefieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_INVOICE'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.invoiceTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_locationfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_LOCATION'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.locationTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_entityfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_ENTITY'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.entityTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_userfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'SALE_USER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.userTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<!-- For transaction, use columns in proctableBean.basicTranbsactionItems -->  
					<h:selectOneMenu id="trans_auditfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_AUDIT'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_GENERALLEDGER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_INVENTORY'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_INVOICE'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_LOCATION'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_PAYMENTINFO'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_PROJECT'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_PURCHASEORDER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_TRANSACTION'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_USER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_VENDOR'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalue_#{counter.index}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" style="width: 180px;" rendered="#{displaydata.operation eq 'TRANS_WORKORDER'}" value="#{displaydata.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionItems}"/>
					</h:selectOneMenu>
					<!-- end of transaction -->
					
					<h:inputText id="fieldvalueText_#{counter.index}" rendered="#{(displaydata.operation eq 'VALUE') or (displaydata.operation eq 'BLANK')}" style="width: 173px;" maxlength = "100" value="#{displaydata.fieldText}" 
					disabled="#{displaydata.operation eq 'BLANK' or proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					 	
	      		</a4j:outputPanel> 
	        </td>
	        <td style= "width:15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
	       <td style= "width:15px">&#160;</td> 
	       <td style= "width:100px">&#160;</td> 
	       </tr>
	       </c:if>
			
    	</c:forEach>

		
	</tbody>
	</table>
	</a4j:outputPanel>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok-add-4"><h:commandLink id="btnOkAdd" value="" disabled="#{!proctableBean.displayAddAction}" action="#{proctableBean.okAddProcdetailTableAction}" /></li>
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{proctableBean.okProcdetailTableAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{proctableBean.displayViewAction}" action="#{proctableBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>


</ui:define>
</ui:composition>
</html>