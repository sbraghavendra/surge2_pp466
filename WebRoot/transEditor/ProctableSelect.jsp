<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Customers&#32;&#38;&#32;Locations" value="/images/headers/hdr-processrules.png"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7">Select Process Table Parameters</td></tr>
	</thead>
	<tbody>			
		<tr>
			<td>&#160;</td>
			<td>Table Name:</td>
			<td style="width:200px;">
				<h:inputText id="proctableName" style="width:200px;" maxlength = "40" value="#{procruleBean.selectedProctable.proctableName}" disabled="true" />
			</td>
			<td>&#160;</td>
			<td>Description:</td>
			<td style="width:300px;">
				<h:inputText id="description" style="width:300px;" maxlength = "50" value="#{procruleBean.selectedProctable.description}" disabled="true" />
			</td>
			<td width="10%">&#160;</td> 
		</tr>
		
		<td colspan="7">&#160;</td>
	</tbody>
	</table>
	
	<a4j:outputPanel id="forEachPanelInColumn"> 
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>	
	
		<tr height="20px">
			<th>&#160;</th>
			<th>In Column</th>
			<th>&#160;</th>
			<th>Override?</th>
			<th>&#160;</th>
			<th>Value</th>
			<th width="100%">&#160;</th>
		</tr>		
		
		<c:forEach var="inColumnObject" items="#{procruleBean.inSelectObject}" varStatus="loopCounter" >
		
        <tr height="20px">
        	<td>&#160;</td>
			<td><h:outputText id="incolumnName_#{loopCounter.index}"  style="width: 150px;" value="#{inColumnObject.columnName}" /></td>
			<td>&#160;</td>
			
			<a4j:outputPanel rendered="#{inColumnObject.displayCheck}">
			<td>
				<h:selectBooleanCheckbox id="indisplayCheck_#{loopCounter.index}" rendered="#{inColumnObject.displayCheck}" value="#{inColumnObject.selected}" styleClass="check" >
					<a4j:support id="ajaxindisplayCheck_#{loopCounter.index}" event="onclick" actionListener="#{procruleBean.selectTableCheckChange}" reRender="incolumnValue_#{loopCounter.index}" >
						<f:param name="command" value="#{loopCounter.index}" />
					</a4j:support>
				</h:selectBooleanCheckbox>
			</td>
			</a4j:outputPanel> 
			
			<a4j:outputPanel rendered="#{!inColumnObject.displayCheck}">
			<td>
				<h:outputText style="width: 150px;" value="&#160;" rendered="#{!inColumnObject.displayCheck}" />
			</td>
			</a4j:outputPanel> 
			
			<td>&#160;</td>
		
	  		<td><h:inputText id="incolumnValue_#{loopCounter.index}" disabled="#{!inColumnObject.selected}" style="width: 200px;" value="#{inColumnObject.columnValue}" /></td> 
			
			<td width="100%">&#160;</td>
        </tr>
    	</c:forEach>
    	
    	<td colspan="7">&#160;</td>
    	
    	<tr height="20px">
			<th>&#160;</th>
			<th>Out Column</th>
			<th>&#160;</th>
			<th>Select?</th>
			<th>&#160;</th>
			<th>&#160;</th>
			<th width="100%">&#160;</th>
		</tr>		   	
 
 		<tr height="20px">
     
			<td align="left" width="300" colspan="4" >
				<div id="embedded-table">
				<h:panelGroup >
					<h:panelGrid columns="2" > 
						<h:panelGrid columns="1" style="width: 110px;" > 
								<c:forEach var="outColumnObject" items="#{procruleBean.outSelectObject}" varStatus="outloopCounter" >	
									<h:outputText id="outcolumnName_#{outloopCounter.index}"  style="width: 110px;" value="#{outColumnObject.columnName}" />
								</c:forEach>						
						</h:panelGrid>
						
						<h:panelGrid columns="1"> 
							<h:selectOneRadio disabledClass="selectOneRadio_Disabled" styleClass="selectOneRadio" id="radio1" layout="pageDirection" value="#{procruleBean.radioSetSelected}" >	
								<c:forEach var="outColumnRadio" items="#{procruleBean.outSelectObject}" varStatus="outloopCounterRadio" >						
									<f:selectItem itemValue="#{outloopCounterRadio.index}"  />
								</c:forEach>
							</h:selectOneRadio>
						</h:panelGrid>
						
						
					</h:panelGrid>
				</h:panelGroup>
				</div>
			</td>
			
			<td>&#160;</td>
			<td>&#160;</td>
			<td width="100%">&#160;</td>
        </tr>
 		
    	<td colspan="7">&#160;</td>
	</tbody>
	</table>
	</a4j:outputPanel>
	
	
	</div>
	</div><!-- table-four-content -->
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{procruleBean.okSelectProcessAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="procrule_update" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>


</ui:define>
</ui:composition>
</html>