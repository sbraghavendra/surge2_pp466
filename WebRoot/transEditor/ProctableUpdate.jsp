<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
	<h1><h:graphicImage id="imgprocesstables-purchasing" alt="Process&#32;&#38;&#32;Tables" rendered="#{proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-purch.png" width="250" height="19" /></h1>
	<h1><h:graphicImage id="imgprocesstables-sales" alt="Process&#32;&#38;&#32;Tables" rendered="#{!proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-sales.png" width="250" height="19" /></h1>
     

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7"><h:outputText value="#{proctableBean.actionText}"/> a Process Table</td></tr>
	</thead>
	<tbody>			
		<tr>
			<td>&#160;</td>
			<td>Table Name:</td>
			<td style="width:200px;">
				<h:inputText id="proctableName" style="width:200px;" maxlength = "40" value="#{proctableBean.updateProctable.proctableName}" disabled="#{!proctableBean.displayAddAction}" />
			</td>
			<td>&#160;</td>
			<td>Description:</td>
			<td style="width:300px;">
				<h:inputText id="description" style="width:300px;" maxlength = "50" value="#{proctableBean.updateProctable.description}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
			</td>
			<td width="10%">&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
	        <td style="width:300px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}"
					value="#{proctableBean.updateProctable.activeBooleanFlag}" styleClass="check"  />
			</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
		
		<tr>
        	<td colspan="7">&#160;</td>
		</tr>
	</tbody>
	</table>
	
	<a4j:outputPanel id="forEachPanelInColumn">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>			
		<tr>
        	<th>&#160;</th>
        	<th width="100px">Input Column</th>
	        <th><h:outputText style="width: 110px;" value="Group/Type" /></th>
	        <th><h:outputText style="width: 173px;" value="Field/Value" /></th>
			<th width="50px">&#160;</th>
			<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="1%">&#160;</th> 
		</tr>
		   
		<c:forEach var="inColumnObject" items="#{proctableBean.inColumnObjectItems}" varStatus="loopCounter" >
        <tr>
        	<td>&#160;</td>
        	
        	<td width="100px"> 
        		<h:outputText style="width: 100px;" value="&#160;Column&#160;#{loopCounter.index  + 1}" />
	        </td>

	        <td width="110px">           	
				<h:selectOneMenu id="valuelocationInColumn_#{loopCounter.index}"  style="width: 100px;" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" 
					value="#{inColumnObject.operation}" immediate="true" >
					<f:selectItems value="#{proctableBean.valuelocationInColumnItems}"/>
					<a4j:support event="onchange" action="#{proctableBean.valuelocationInColumnChange}" reRender="fieldvaluepaneInColumn_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldvaluepaneInColumn_#{loopCounter.index}">   
	        	
	        		<h:selectOneMenu id="allfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'ALL'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.allSaleTransItems}"/>
					</h:selectOneMenu>
					       	
					<h:selectOneMenu id="sales_basicfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_BASIC'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.basicTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_customerfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_CUSTOMER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.customerTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_invoicefieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_INVOICE'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.invoiceTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_locationfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_LOCATION'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.locationTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_entityfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_ENTITY'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.entityTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_userfieldvalueInColumn_#{loopCounter.index}"  style="width: 180px;" rendered="#{inColumnObject.operation eq 'SALE_USER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.userTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<!-- For transaction, use columns in proctableBean.basicTranbsactionItems -->  
					<h:selectOneMenu id="trans_auditfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_AUDIT'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_GENERALLEDGER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_INVENTORY'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_INVOICE'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_LOCATION'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_PAYMENTINFO'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_PROJECT'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_PURCHASEORDER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_TRANSACTION'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_USER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_VENDOR'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueInColumn_#{loopCounter.index}"  style="width: 150px;" rendered="#{inColumnObject.operation eq 'TRANS_WORKORDER'}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" value="#{inColumnObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionItems}"/>
					</h:selectOneMenu>
					<!-- end of transaction -->
					
					<h:inputText id="fieldvalueTextInColumn_#{loopCounter.index}" maxlength = "100" rendered="#{inColumnObject.operation eq 'TEXT'}" style="width: 173px;" value="#{inColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					<h:inputText id="fieldvalueNumberInColumn_#{loopCounter.index}" rendered="#{inColumnObject.operation eq 'NUMBER'}" style="width: 173px;" value="#{inColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					<h:inputText id="fieldvalueDateInColumn_#{loopCounter.index}" rendered="#{inColumnObject.operation eq 'DATE'}" style="width: 173px;" value="#{inColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" /> 	
	        	</a4j:outputPanel>
	        </td>      

			<td width="10%">&#160;</td>
			
			<td width="15px">
				<a4j:commandLink id="addInColumn_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq 19) and ((loopCounter.index eq proctableBean.lastInColumnObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction))}" 
					reRender="forEachPanelInColumn" actionListener="#{proctableBean.processAddInColumnCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq 19) or !(loopCounter.index eq proctableBean.lastInColumnObject)  or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="removeInColumn_#{loopCounter.index}" styleClass="button" rendered="#{!(proctableBean.firstInColumnObject eq proctableBean.lastInColumnObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" 
            		reRender="forEachPanelInColumn" actionListener="#{proctableBean.processRemoveInColumnCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(proctableBean.firstInColumnObject eq proctableBean.lastInColumnObject) or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
            </td>
            
 			<td width="10%">&#160;</td> 
        </tr>
    	</c:forEach>

		<tr>
        	<td colspan="8">&#160;</td>
		</tr>
	</tbody>
	</table>
	</a4j:outputPanel>
	
	<a4j:outputPanel id="forEachPanelOutColumn">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>			
		<tr>
        	<th>&#160;</th>
        	<th width="100px">Output Column</th>
	        <th><h:outputText style="width: 110px;" value="Type" /></th>
	        <th><h:outputText style="width: 173px;" value="Name" /></th>
			<th width="50px">&#160;</th>
			<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="1%">&#160;</th> 
		</tr>
		   
		<c:forEach var="outColumnObject" items="#{proctableBean.outColumnObjectItems}" varStatus="loopCounter" >
        <tr>
        	<td>&#160;</td>
        	
        	<td width="100px"> 
        		<h:outputText style="width: 100px;" value="&#160;Column&#160;#{loopCounter.index  + 1}" />
	        </td>

	        <td width="110px">           	
				<h:selectOneMenu id="valuelocationOutColumn_#{loopCounter.index}"  style="width: 100px;" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" 
					value="#{outColumnObject.operation}" immediate="true" >
					<f:selectItems value="#{proctableBean.valuelocationOutColumnItems}"/>
					<a4j:support event="onchange" action="#{proctableBean.valuelocationOutColumnChange}" reRender="fieldvaluepaneOutColumn_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldvaluepaneOutColumn_#{loopCounter.index}">
					<h:inputText id="fieldvalueTextOutColumn_#{loopCounter.index}" rendered="#{outColumnObject.operation eq 'TEXT'}" style="width: 173px;" value="#{outColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					<h:inputText id="fieldvalueNumberOutColumn_#{loopCounter.index}" rendered="#{outColumnObject.operation eq 'NUMBER'}" style="width: 173px;" value="#{outColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					<h:inputText id="fieldvalueDateOutColumn_#{loopCounter.index}" rendered="#{outColumnObject.operation eq 'DATE'}" style="width: 173px;" value="#{outColumnObject.fieldText}" disabled="#{proctableBean.displayDeleteAction or proctableBean.displayViewAction}" /> 	
	        	</a4j:outputPanel>
	        </td>      

			<td width="10%">&#160;</td>
			
			<td width="15px">
				<a4j:commandLink id="addOutColumn_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq 9) and ((loopCounter.index eq proctableBean.lastOutColumnObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction))}" 
					reRender="forEachPanelOutColumn" actionListener="#{proctableBean.processAddOutColumnCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq 9) or !(loopCounter.index eq proctableBean.lastOutColumnObject)  or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="removeOutColumn_#{loopCounter.index}" styleClass="button" rendered="#{!(proctableBean.firstOutColumnObject eq proctableBean.lastOutColumnObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" 
            		reRender="forEachPanelOutColumn" actionListener="#{proctableBean.processRemoveOutColumnCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(proctableBean.firstOutColumnObject eq proctableBean.lastOutColumnObject) or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
            </td>
            
 			<td width="10%">&#160;</td> 
        </tr>
    	</c:forEach>

		<tr>
        	<td colspan="8">&#160;</td>
		</tr>
	</tbody>
	</table>
	</a4j:outputPanel>
	
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="populate"><h:commandLink id="populateproctable" disabled="#{!(proctableBean.displayAddAction or proctableBean.displayUpdateAction)}" action="#{proctableBean.displayPopulateProctableFromUpdateAction}" ></h:commandLink></li>
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{proctableBean.okProcessTableAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{proctableBean.displayViewAction}" action="#{proctableBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>


</ui:define>
</ui:composition>
</html>