<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgCustLocnAdd" alt="Customers&#32;&#38;&#32;Locations" value="/images/headers/hdr-processrules.png"  /></h1>

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	
	<div id="table-one-content" style="height:418px;">
	
	<a4j:outputPanel id="forEachPanel">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="8">Reorder Process Rules</td></tr>
	</thead>
	<tbody>	
		<tr>
        	<td width="30px">&#160;</td>
        	<td width="220px">Rule Type:</td>
        	<td style="width: 220px;">           	
				<h:selectOneMenu id="ruleTypeMenuItems" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction}" value="#{procruleBean.reorderRuletypeCode}" immediate="true" >
						<f:selectItems value="#{procruleBean.ruleTypeMenuItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.reorderRuleTypeChange}" reRender="forEachPanel" ></a4j:support>
				</h:selectOneMenu>
	        </td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="100px">&#160;</td> 
		</tr>	
	
		<tr>
        	<th width="30px">&#160;</th>
        	<th width="300px">Rules</th>
        	<th width="50%">&#160;</th>
        	<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="100px">&#160;</th> 
		</tr>
		   
		
		<c:forEach var="procrule" items="#{procruleBean.reorderProcruleArray}" varStatus="loopCounter" >
        <tr>
        	<td width="30px" height="20px" >&#160;</td>
        	
        	<td width="300px"> 
        		<h:outputText style="#{(procrule.activeFlag) == 1 ? '' : 'color:#888888;'}" value="#{procrule.procruleName}&#160;-&#160;#{procrule.description}"  />    
	        </td>

			<td width="50%">&#160;</td>
			
			<td width="15px">
            	<a4j:commandLink id="downdown_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.lastReorderProcruleObject)}" 
            		reRender="forEachPanel" actionListener="#{procruleBean.processDownDownReorderCommand}" >
            		<h:graphicImage value="/images/icon_arrow-bottom.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.lastReorderProcruleObject)}" /> 
            </td>
			
			<td width="15px">
            	<a4j:commandLink id="down_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.lastReorderProcruleObject)}" 
            		reRender="forEachPanel" actionListener="#{procruleBean.processDownReorderCommand}" >
            		<h:graphicImage value="/images/icon_arrow-down.jpg" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.lastReorderProcruleObject)}" /> 
            </td>
			
        	<td width="15px">
        		<a4j:commandLink  id="up_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.firstReorderProcruleObject)}" 
        			reRender="forEachPanel" actionListener="#{procruleBean.processUpReorderCommand}" >
        			<h:graphicImage value="/images/icon_arrow-up.jpg" />	
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink >
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.firstReorderProcruleObject)}" />   		  
        	</td>
        	
        	<td width="15px">
        		<a4j:commandLink  id="upup_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.firstReorderProcruleObject)}" 
        			reRender="forEachPanel" actionListener="#{procruleBean.processUpUpReorderCommand}" >
        			<h:graphicImage value="/images/icon_arrow-top.png" />	
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink >
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.firstReorderProcruleObject)}" />   		  
        	</td>

            
 			<td width="100px">&#160;</td> 
        </tr>
    	</c:forEach>

		<tr>
        	<td height="15px" colspan="8">&#160;</td>
		</tr>
		<tr>
        	<td height="15px" colspan="8">&#160;</td>
		</tr>
		<tr>
        	<td height="15px" colspan="8">&#160;</td>
		</tr>
	</tbody>
	</table>
	</a4j:outputPanel>
	
	</div>
	</div>
	
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" disabled="#{procruleBean.isReorderProcessRuleEmpty}" action="#{procruleBean.okProcessRuleAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{procruleBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>


</ui:define>
</ui:composition>
</html>