<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgprocessrules-purchasing" alt="Process&#32;&#38;&#32;Rules" rendered="#{procruleBean.isPurchasingMenu}" url="/images/headers/hdr-processrules-purchasing.png" width="250" height="19" /></h1>
<h1><h:graphicImage id="imgprocessrules-sales" alt="Process&#32;&#38;&#32;Rules" rendered="#{!procruleBean.isPurchasingMenu}" url="/images/headers/hdr-processrules-sales.png" width="250" height="19" /></h1>
     
<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
		<a4j:outputPanel id="reprocessCodeMsg">	
			<table>
			<tr>
				<td style="width:240px;"></td>
				<td style="color:red; font-weight:bold;"><h:outputText value="#{procruleBean.reprocessExplanationMap[procruleBean.updateProcrule.reprocessCode]}" /></td>				
			</tr>			
			</table>			
		</a4j:outputPanel>
		<a4j:outputPanel id="ebableDeleteMsg">
			<c:if test="#{!procruleBean.enableDefinitionDelete and procruleBean.displayDeleteAction}" >		
			<table>
			<tr>
				<td style="width:240px;"></td>
				<td style="color:red; font-weight:bold;"><h:outputText>This Process Rule has been used on a Transaction. Click Ok to set to Inactive.</h:outputText></td>				
			</tr>			
			</table>			
			</c:if>
		</a4j:outputPanel>	
	</div>
	<div id="table-one-content" style="height:418px;">
	<a4j:outputPanel id="selectionPanel">
	<span>
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
	<thead>
		<tr>
			<td colspan="13"><h:outputText value="#{procruleBean.actionText}"/></td>		
		</tr>
	</thead>
	<tbody>	
		<tr>
			<th colspan="13"><h:outputText value="Process Rule"/></th>		
		</tr>		
		<tr>
			<td>&#160;</td>
			<td>Rule Name:</td>
			<td style="width:300px;">
				<h:inputText id="procruleName" style="width:300px;" maxlength="40" onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
					value="#{procruleBean.updateProcrule.procruleName}" disabled="#{!procruleBean.displayAddAction and !procruleBean.displayCopyAddAction}" /></td>
			<td>&#160;</td>
			<td>Description:</td>
			<td colspan="7" style="width:400px;">
				<h:inputText id="description" style="width:400px;" maxlength="50" value="#{procruleBean.updateProcrule.description}" 
				disabled="#{!procruleBean.displayAddAction and !procruleBean.displayCopyAddAction and !procruleBean.displayUpdateAction}" /> </td>
			<td >&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Rule Type:</td>
	        <td style="width: 220px;">           	
				<h:selectOneMenu id="ruleTypeMenuItems" disabled="#{!procruleBean.displayAddAction and !procruleBean.displayCopyAddAction}" value="#{procruleBean.updateProcrule.ruletypeCode}" immediate="true" >
						<f:selectItems value="#{procruleBean.ruleTypeMenuItems}"/>
						<a4j:support event="onchange" actionListener="#{procruleBean.ruleTypeChanged}" reRender="selectionPanel,reprocessCodeMsg"/>  	
				</h:selectOneMenu>
	        </td>
	        <td>&#160;</td>
			<td>Always Apply?:</td>
			<td style="width:200px;">	 	  
				<h:selectBooleanCheckbox id="alwaysFlagId" disabled="#{!procruleBean.displayAddAction and !procruleBean.displayCopyAddAction}" 
					value="#{procruleBean.updateProcrule.alwaysBooleanFlag}" styleClass="check"  >
					<a4j:support event="onclick" reRender="selectionPanel,forEachPanelWhen,reprocessCodeMsg"  action="#{procruleBean.valueAlwaysApplyChange}"/>
				</h:selectBooleanCheckbox>
			</td>
			<td>&#160;</td>
			
			<c:if test="#{!procruleBean.isPurchasingMenu and procruleBean.updateProcrule.ruletypeCode eq 'POST'}" >			
				<td>Reprocess Option:</td>	
				<td style="width: 220px;">           	
					<h:selectOneMenu id="reprocessMenuItems" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or procruleBean.displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction or procruleBean.displayCopyUpdateAction or procruleBean.displayDeleteAllAction or procruleBean.updateProcrule.alwaysBooleanFlag}" value="#{procruleBean.updateProcrule.reprocessCode}" immediate="true" >
						<f:selectItems value="#{procruleBean.reprocessMenuItems}"/>
			  			<a4j:support event="onchange" actionListener="#{procruleBean.reprocessCodeChanged}" reRender="reprocessCodeMsg"/>
					</h:selectOneMenu>
		        </td>
	        </c:if>
	        <c:if test="#{!procruleBean.isPurchasingMenu and !procruleBean.updateProcrule.ruletypeCode eq 'POST'}" >
	        	<td>&#160;</td>
	        	<td>&#160;</td>
	        </c:if>
	        <c:if test="#{procruleBean.isPurchasingMenu}" >   
	        	<td>&#160;</td>
	        	<td>&#160;</td>
	        </c:if>
	        
			<td>&#160;</td>
			<td>Active?:</td>
			<td style="width:200px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" disabled="#{!procruleBean.displayAddAction and !procruleBean.displayCopyAddAction and !procruleBean.displayUpdateAction}" 
					value="#{procruleBean.updateProcrule.activeBooleanFlag}" styleClass="check"  />
			</td>
			<td>&#160;</td>
		</tr>
	</tbody>
	</table>
	</span>
	</a4j:outputPanel>
	
	<a4j:outputPanel id="forEachPanelWhen">
	<c:if test="#{!procruleBean.displayDeleteAllAction and  !procruleBean.displayUpdateAction }" >
	<span>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>		
	
		<tr>
			<th colspan="13"><h:outputText value="Details"/></th>	
		</tr>
	</tbody>
	</table>
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<td style = "width:5px">&#160;</td>
			<td style="width:77px;">Effective Date:</td>
			<td style="width:600px;">
				<rich:calendar binding="#{procruleBean.effectiveDateCalendar}" 
						id="effDate" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{procruleBean.effectiveDate}" popup="true"
						disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or procruleBean.displayUpdateProcRulesDetailAction or procruleBean.displayDeleteAllAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox"/>
				
			</td>
			<td style="width:20px;">Active?:</td>
			<h:panelGroup rendered="#{!procruleBean.isPurchasingMenu}">  
			<td style="width:110px;padding-right:104px">	 
				<h:selectBooleanCheckbox id="activeDetailssalesFlagId"  disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction}"
					value="#{procruleBean.detailActiveBooleanFlag}" styleClass="check"  />
			</td>
			</h:panelGroup>
			
			<h:panelGroup rendered="#{procruleBean.isPurchasingMenu}">   
			<td style="width:110px;padding-right:91px">	 
				<h:selectBooleanCheckbox id="activeDetailspurchFlagId"   disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction}"
					value="#{procruleBean.detailActiveBooleanFlag}" styleClass="check"  />
			</td>
			</h:panelGroup>
		</tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
        	<th>&#160;</th>
        	<th width="50px">When</th>
        	<th width="30px">&#160;</th>     
        	<th><h:outputText style="width: 70px;" value="&#160;" /></th>       
	        <th><h:outputText style="width: 150px;" value="Field" /></th>
	        <th><h:outputText style="width: 80px;" value="Relation" /></th>
	        <th><h:outputText style="width: 80px;" value="&#160;" /></th>
	        <th><h:outputText style="width: 153px;" value="Field/Value" /></th>
        	<th width="30px" align="center"><h:outputText style="width: 30px;" value="&#160;" /></th>
			<th width="15%">&#160;</th>
        	<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="10px">&#160;</th> 
		</tr>
		
		<c:if test="#{procruleBean.updateProcrule.alwaysBooleanFlag}" >
			<td>&#160;</td>
        	<td width="50px">&#160;</td>
        	<td width="30px">&#160;</td>     
        	<td><h:outputText style="width: 70px;" value="&#160;" /></td>       
	        <td><h:outputText style="width: 170px;" value="Always Apply" /></td>
	        <td>&#160;</td>
	        <td>&#160;</td>
	        <td>&#160;</td>
        	<td width="30px" align="center">&#160;</td>
			<td width="15%">&#160;</td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="40%">&#160;</td> 
		</c:if>
		  
		<c:if test="#{!procruleBean.updateProcrule.alwaysBooleanFlag}" >
		<c:forEach var="whenObject" items="#{procruleBean.whenObjectItems}" varStatus="loopCounter" >
        <tr>
        	<td>&#160;</td>
        	
        	<td width="50px"> 
        		<h:outputText style="width: 50px;" value="&#160;" rendered="#{loopCounter.index eq procruleBean.firstWhenObject}" />          	
				<h:selectOneMenu id="operatorWhen_#{loopCounter.index}"  style="width: 50px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
					value="#{whenObject.operatorWhen}" immediate="true" rendered="#{!(loopCounter.index eq procruleBean.firstWhenObject)}" >
					<f:selectItems value="#{procruleBean.operatorItems}"/>
				</h:selectOneMenu>
	        </td>

        	<td width="30px" align="center" >
        		<a4j:commandLink id="openWhenWhen_#{loopCounter.index}" style="#{whenObject.openparenWhen? 'color:#0033FF;font-weight:bold;':'color:#B0B0B0;'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}"
        			reRender="forEachPanelWhen" actionListener="#{procruleBean.processOpenWhenCommand}" > 
        			<h:outputText style="border-style:outset;" value="&#160;&#160;(&#160;&#160;" />
        			<!--
        			<h:graphicImage id="image_#{loopCounter.index}" value="#{whenObject.openparenWhen? '/images/headers/hdr-processrules.png' : '/images/headers/hdr-database.gif'}"/>
        			-->
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink>
        	</td>

	        <td>           	
				<h:selectOneMenu id="operationWhen_#{loopCounter.index}"  style="width: 70px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
					value="#{whenObject.operationWhen}" immediate="true" >
					<f:selectItems value="#{procruleBean.operationGroupItems}"/>
					<a4j:support event="onchange" 
					action="#{procruleBean.valueCategoryWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td>  
	        	<a4j:outputPanel id="fieldpanelWhen_#{loopCounter.index}">   
	        		<h:selectOneMenu id="allWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'ALL'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					      	
					<h:selectOneMenu id="sale_basicWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_BASIC'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_CUSTOMER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoiceWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_INVOICE'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_LOCATION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_ENTITY'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_USER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>		
					</h:selectOneMenu>
					
					<!-- Insert xxxxxx -->
					<h:selectOneMenu id="sale_transactionWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_TRANSACTION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>		
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_SITUS'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>		
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_AMOUNTS'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>		
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'SALE_GENERALLEDGER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>		
					</h:selectOneMenu>
					<!--  -->
			
					
					<!-- For transaction -->  	
					<h:selectOneMenu id="trans_auditWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_AUDIT'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_GENERALLEDGER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_INVENTORY'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoiceWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_INVOICE'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_LOCATION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfoWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_PAYMENTINFO'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_PROJECT'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_PURCHASEORDER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_TRANSACTION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_USER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_VENDOR'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderWhen_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.operationWhen eq 'TRANS_WORKORDER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldWhenChange}" reRender="fieldpanelWhen_#{loopCounter.index},fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>

					<!-- end of transaction -->
				</a4j:outputPanel>
	       </td>
	        
	        <td>           	
				<h:selectOneMenu id="relationWhen_#{loopCounter.index}"  style="width: 70px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
					value="#{whenObject.relationWhen}" immediate="true" >
					<f:selectItems value="#{procruleBean.relationItems}"/>
					
					<a4j:support event="onchange" action="#{procruleBean.relationWhenChange}" reRender="forEachPanelWhen" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <!-- When In Table is selected -->  
	        <c:if id="inTablePanel_#{loopCounter.index}" test="#{(whenObject.relationWhen eq 'InTable')}" >
	       	<td colspan="2">
	       		<h:selectOneMenu id="inTableSet_#{loopCounter.index}"  style="width: 150px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldTableWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.talbeSelectionItems}"/>
				</h:selectOneMenu>
	       	</td> 
			</c:if> 
	        
	         <!-- When In Table is NOT selected --> 
	        <c:if id="notInTablePanel_#{loopCounter.index}" test="#{!(whenObject.relationWhen eq 'InTable')}" >
	        <td>  
	        	<a4j:outputPanel id="valuelocationWhenPanel_#{loopCounter.index}">            	
					<h:selectOneMenu id="valuelocationWhen_#{loopCounter.index}"  style="width: 70px;" disabled="#{whenObject.isFieldValueBoxDisabled or procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
						value="#{whenObject.valuelocationWhen}" immediate="true" >
						<f:selectItems value="#{procruleBean.valuelocationWhenItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.valuelocationWhenChange}" reRender="fieldvaluepaneWhen_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
				</a4j:outputPanel>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldvaluepaneWhen_#{loopCounter.index}">   				
					<!-- SALE, ALL group selection -->
					<h:selectOneMenu id="allfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'ALL' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'ALL' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'ALL' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'ALL' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of ALL group selection -->

					<!-- SALE, BASIC group selection -->
					<h:selectOneMenu id="sale_basicfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_BASIC' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_BASIC' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_BASIC' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_BASIC' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of BASIC group selection -->

					<!-- SALE, CUSTOMER group selection -->
					<h:selectOneMenu id="sale_customerfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_CUSTOMER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_CUSTOMER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_CUSTOMER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_CUSTOMER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of CUSTOMER group selection -->
					
					<!-- SALE, INVOICE group selection -->
					<h:selectOneMenu id="sale_invoicefieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_INVOICE' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_INVOICE' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_INVOICE' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_INVOICE' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of INVOICE group selection -->
					
					<!-- SALE, LOCATION group selection -->
					<h:selectOneMenu id="sale_locationfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_LOCATION' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_LOCATION' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_LOCATION' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_LOCATION' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of LOCATION group selection -->
					
					<!-- SALE, ENTITY group selection -->
					<h:selectOneMenu id="sale_entityfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_ENTITY' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_ENTITY' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_ENTITY' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_ENTITY' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of ENTITY group selection -->

					<!-- SALE, User group selection -->
					<h:selectOneMenu id="sale_userfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_USER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_USER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_USER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_USER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of User group selection -->
					
					<!-- Insert xxxxxx -->
					<!-- SALE, SALE_TRANSACTION selection -->
					<h:selectOneMenu id="sale_transactionfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_TRANSACTION' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_TRANSACTION' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_TRANSACTION' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_TRANSACTION' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_TRANSACTION selection -->
					
					<!-- SALE, SALE_SITUS selection -->
					<h:selectOneMenu id="sale_situsfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_SITUS' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_SITUS' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_SITUS' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_SITUS' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_SITUS selection -->
					
					<!-- SALE, SALE_AMOUNTS selection -->
					<h:selectOneMenu id="sale_amountsfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_AMOUNTS' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_AMOUNTS' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_AMOUNTS' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_AMOUNTS' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_AMOUNTS selection -->
					
					<!-- SALE, SALE_GENERALLEDGER selection -->
					<h:selectOneMenu id="sale_generalledgerfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_GENERALLEDGER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_GENERALLEDGER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_GENERALLEDGER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'SALE_GENERALLEDGER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_GENERALLEDGER selection -->
					
					<!-- Trans group -->

					<!-- TRANS, TRANS_AUDIT group selection -->
					<h:selectOneMenu id="trans_auditfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_AUDIT' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_AUDIT' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_AUDIT' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_AUDIT' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_AUDIT group selection -->
					
					<!-- TRANS, TRANS_GENERALLEDGER group selection -->
					<h:selectOneMenu id="trans_generalLedgerfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_GENERALLEDGER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_GENERALLEDGER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_GENERALLEDGER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_GENERALLEDGER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_GENERALLEDGER group selection -->
					
					<!-- TRANS, TRANS_INVENTORY group selection -->
					<h:selectOneMenu id="trans_inventoryfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVENTORY' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVENTORY' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVENTORY' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVENTORY' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_INVENTORY group selection -->
					
					<!-- TRANS, TRANS_INVOICE group selection -->
					<h:selectOneMenu id="trans_invoicefieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVOICE' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVOICE' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVOICE' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_INVOICE' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_INVOICE group selection -->
					
					<!-- TRANS, TRANS_LOCATION group selection -->
					<h:selectOneMenu id="trans_locationfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_LOCATION' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_LOCATION' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_LOCATION' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_LOCATION' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_LOCATION group selection -->
					
					<!-- TRANS, TRANS_PAYMENTINFO group selection -->
					<h:selectOneMenu id="trans_paymentInfofieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PAYMENTINFO' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PAYMENTINFO' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PAYMENTINFO' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PAYMENTINFO' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PAYMENTINFO group selection -->
					
					<!-- TRANS, TRANS_PROJECT group selection -->
					<h:selectOneMenu id="trans_projectfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PROJECT' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PROJECT' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PROJECT' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PROJECT' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PROJECT group selection -->
					
					<!-- TRANS, TRANS_PURCHASEORDER group selection -->
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PURCHASEORDER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PURCHASEORDER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PURCHASEORDER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_PURCHASEORDER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PURCHASEORDER group selection -->
					
					<!-- TRANS, TRANS_TRANSACTION group selection -->
					<h:selectOneMenu id="trans_transactionfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_TRANSACTION' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_TRANSACTION' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_TRANSACTION' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_TRANSACTION' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_TRANSACTION group selection -->
					
					<!-- TRANS, TRANS_USER group selection -->
					<h:selectOneMenu id="trans_userfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_USER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_USER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_USER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_USER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_USER group selection -->
					
					<!-- TRANS, TRANS_VENDOR group selection -->
					<h:selectOneMenu id="trans_vendorfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_VENDOR' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_VENDOR' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_VENDOR' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_VENDOR' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_VENDOR group selection -->
					
					<!-- TRANS, TRANS_WORKORDER group selection -->
					<h:selectOneMenu id="trans_workOrderfieldvalueWhenNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_WORKORDER' and whenObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueWhenText_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_WORKORDER' and whenObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueWhenDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_WORKORDER' and whenObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueWhenNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{whenObject.valuelocationWhen eq 'TRANS_WORKORDER' and whenObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{whenObject.fieldColumnWhen}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_WORKORDER group selection -->
					
					<h:inputText id="fieldvalueWhen_#{loopCounter.index}" rendered="#{whenObject.valuelocationWhen eq 'VALUE'}" style="width: 153px;" value="#{whenObject.fieldTextWhen}" maxlength="100" disabled="#{whenObject.isFieldValueDisabled or procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" />

	        	</a4j:outputPanel>
	        </td>  
	        </c:if>    

        	<td width="30px" align="center">
        		<a4j:commandLink id="closeWhenWhen_#{loopCounter.index}" style="#{whenObject.closeparenWhen? 'color:#0033FF;font-weight:bold;':'color:#B0B0B0;'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}"
        			reRender="forEachPanelWhen" actionListener="#{procruleBean.processCloseWhenCommand}" ><h:outputText style="border-style:outset;" value="&#160;&#160;)&#160;&#160;" /><f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink>
        	</td>
        
			<td width="50px">&#160;</td>
			
        	<td width="15px">
        		<a4j:commandLink  id="upWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.firstWhenObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
        			reRender="forEachPanelWhen" actionListener="#{procruleBean.processUpWhenCommand}" >
        			<h:graphicImage value="/images/icon_arrow-up.jpg" />	
        			<f:param name="command" value="#{loopCounter.index}" />
        		</a4j:commandLink >
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.firstWhenObject)  or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" />   		  
        	</td>
        	
            <td width="15px">
            	<a4j:commandLink id="downWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(loopCounter.index eq procruleBean.lastWhenObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
            		reRender="forEachPanelWhen" actionListener="#{procruleBean.processDownWhenCommand}" >
            		<h:graphicImage value="/images/icon_arrow-down.jpg" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(loopCounter.index eq procruleBean.lastWhenObject)  or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" /> 
            </td>
			<td width="15px">
				<a4j:commandLink id="addWhen_#{loopCounter.index}" styleClass="button" rendered="#{(loopCounter.index eq procruleBean.lastWhenObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
					reRender="forEachPanelWhen" actionListener="#{procruleBean.processAddWhenCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{!(loopCounter.index eq procruleBean.lastWhenObject)  or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="removeWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(procruleBean.firstWhenObject eq procruleBean.lastWhenObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
            		reRender="forEachPanelWhen" actionListener="#{procruleBean.processRemoveWhenCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(procruleBean.firstWhenObject eq procruleBean.lastWhenObject) or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" />
            </td>
            
 			<td width="10px">&#160;</td> 
        </tr>
    	</c:forEach>
    	</c:if>


		<tr>
        	<td colspan="15">&#160;</td>
		</tr>
	</tbody>
	</table>
	</span>
	</c:if>
	</a4j:outputPanel>
	
	
	<a4j:outputPanel id="forEachPanelSet">
	<c:if test="#{!procruleBean.displayDeleteAllAction and  !procruleBean.displayUpdateAction }" >
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>
		<tr>
        	<th>&#160;</th>
        	<th width="50px">Set</th>
        	<th width="30px">&#160;</th>     
        	<th><h:outputText style="width: 70px;" value="&#160;" /></th>       
	        <th><h:outputText style="width: 150px;" value="Field" /></th>
	        <th width="8px"><h:outputText style="width: 8px;" value="&#160;" /></th>
	        <th><h:outputText style="width: 70px;" value="&#160;" /></th>
	        <th><h:outputText style="width: 153px;" value="Field/Value" /></th>
        	<th width="50px"><h:outputText style="width: 50px;" value="Operator" /></th>
			<th width="50px">&#160;</th>
        	<th width="15px"><h:outputText style="width: 15px;" value="Field/Value" /></th>
            <th width="15%"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>            
            <th width="15px"><h:outputText style="width: 15px;" value="&#160;" /></th>
			<th width="2px">&#160;</th> 
		</tr>
		   
		<c:forEach var="setObject" items="#{procruleBean.setObjectItems}" varStatus="loopCounter" >
        <tr>
        	<td>&#160;</td>
        	
        	<td width="50px"> 
        		<h:outputText style="width: 50px;" value="&#160;" rendered="#{loopCounter.index eq procruleBean.firstSetObject}" />   
        		<h:outputText style="width: 50px;" value="&#160;&#160;And" rendered="#{!(loopCounter.index eq procruleBean.firstSetObject)}" />
	        </td>

        	<td width="30px" align="center"><h:outputText style="width: 30px;" value="&#160;" /> </td>
      
        	<td>           	
				<h:selectOneMenu id="operationSet_#{loopCounter.index}"  style="width: 70px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
					value="#{setObject.operationSet}" immediate="true" >
					<f:selectItems value="#{procruleBean.operationSetGroupItems}"/>
					<a4j:support event="onchange" action="#{procruleBean.valueCategorySetChange}" reRender="forEachPanelSet,fieldpanelSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td>  
	        	<a4j:outputPanel id="fieldpanelSet_#{loopCounter.index}">  
	        	
	        		<!-- For sale -->
	        		<h:selectOneMenu id="allSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'ALL'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					       	
					<h:selectOneMenu id="sale_basicSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_BASIC'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_CUSTOMER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoiceSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_INVOICE'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_LOCATION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entitySet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_ENTITY'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_USER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_TRANSACTION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_SITUS'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_AMOUNTS'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'SALE_GENERALLEDGER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					<!-- end of sale -->
					
					<!-- For transaction -->
					       	
					<h:selectOneMenu id="trans_auditSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_AUDIT'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_GENERALLEDGER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventorySet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_INVENTORY'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoiceSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_INVOICE'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_LOCATION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfoSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_PAYMENTINFO'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_PROJECT'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_PURCHASEORDER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_TRANSACTION'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_USER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_venworkOrder_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_VENDOR'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operationSet eq 'TRANS_WORKORDER'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldSet}" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.fieldSetChange}" reRender="fieldpanelSet_#{loopCounter.index},fieldvaluepanelSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index},operatorGroupSet_#{loopCounter.index},operatorItemSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					<!-- end of transaction -->
					
					<h:inputText id="sprocField_#{loopCounter.index}" rendered="#{setObject.operationSet eq 'SPROC'}" style="width: 142px;" value="#{setObject.sprocName}" maxlength="100" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" />
				</a4j:outputPanel>
	       </td>

		   <c:if id="nonsprocGroupPanel_#{loopCounter.index}" test="#{!(setObject.operationSet eq 'SPROC')}" >

	        <td width="8px"><h:outputText style="width: 8px;" value="=" /></td>
	        
	        <td>           	
				<h:selectOneMenu id="valuelocationSet_#{loopCounter.index}"  style="width: 70px;" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" 
					value="#{setObject.valuelocationSet}" immediate="true" >
					<f:selectItems value="#{procruleBean.valuelocationSetItems}"/>
					<a4j:support event="onchange" action="#{procruleBean.valuelocationSetChange}" reRender="forEachPanelSet,fieldvaluepanelSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldvaluepanelSet_#{loopCounter.index}">   
	        	
					<h:selectOneMenu id="allfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'ALL' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'ALL' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'ALL' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="allfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'ALL' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					 
					<!-- SALE_BASIC group selection --> 
					<h:selectOneMenu id="sale_basicfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_BASIC' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_BASIC' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_BASIC' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_BASIC' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_BASIC group selection -->

					<!-- SALE_CUSTOMER group selection --> 
					<h:selectOneMenu id="sale_customerfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_CUSTOMER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_CUSTOMER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_CUSTOMER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customerfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_CUSTOMER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_CUSTOMER group selection -->

					<!-- SALE_INVOICE group selection --> 
					<h:selectOneMenu id="sale_invoicefieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_INVOICE' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_INVOICE' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_INVOICE' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoicefieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_INVOICE' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_INVOICE group selection -->
	
					<!-- LOCATION group selection -->
					<h:selectOneMenu id="sale_locationfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_LOCATION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_LOCATION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_LOCATION' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_LOCATION' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of LOCATION group selection -->
					
					<!-- ENTITY group selection -->
					<h:selectOneMenu id="sale_entityfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_ENTITY' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_ENTITY' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_ENTITY' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_ENTITY' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of ENTITY group selection -->
					
					<!-- User group selection -->
					<h:selectOneMenu id="sale_userfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_USER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_USER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_userfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_USER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="userfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_USER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of User group selection -->
					
					<!-- SALE_TRANSACTION selection -->
					<h:selectOneMenu id="sale_transactionfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_TRANSACTION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_TRANSACTION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_TRANSACTION' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="transactionfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_TRANSACTION' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_TRANSACTION selection -->
					
					<!-- SALE_SITUS selection -->
					<h:selectOneMenu id="sale_situsfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_SITUS' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_SITUS' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_SITUS' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="situsfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_SITUS' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_SITUS selection -->
					
					<!-- SALE_AMOUNTS selection -->
					<h:selectOneMenu id="sale_amountsfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_AMOUNTS' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_AMOUNTS' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_AMOUNTS' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="amountsfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_AMOUNTS' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_AMOUNTS selection -->
					
					<!-- SALE_GENERALLEDGER selection -->
					<h:selectOneMenu id="sale_generalledgerfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_GENERALLEDGER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_GENERALLEDGER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgerfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_GENERALLEDGER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="generalledgerfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'SALE_GENERALLEDGER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of SALE_GENERALLEDGER selection -->
					
					<!-- Transaction Group -->
					 
					<!-- TRANS_AUDIT group selection --> 
					<h:selectOneMenu id="trans_auditfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_AUDIT' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_AUDIT' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_AUDIT' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_AUDIT' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_AUDIT group selection -->
					
					<!-- TRANS_GENERALLEDGER group selection --> 
					<h:selectOneMenu id="trans_generalLedgerfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_GENERALLEDGER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_GENERALLEDGER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_GENERALLEDGER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_GENERALLEDGER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_GENERALLEDGER group selection -->
					
					<!-- TRANS_INVENTORY group selection --> 
					<h:selectOneMenu id="trans_inventoryfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVENTORY' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVENTORY' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVENTORY' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVENTORY' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_INVENTORY group selection -->
					
					<!-- TRANS_INVOICE group selection --> 
					<h:selectOneMenu id="trans_invoicefieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVOICE' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVOICE' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVOICE' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_INVOICE' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_INVOICE group selection -->
					
					<!-- TRANS_LOCATION group selection --> 
					<h:selectOneMenu id="trans_locationfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_LOCATION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_LOCATION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_LOCATION' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_LOCATION' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_LOCATION group selection -->
					
					<!-- TRANS_PAYMENTINFO group selection --> 
					<h:selectOneMenu id="trans_paymentInfofieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PAYMENTINFO' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PAYMENTINFO' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PAYMENTINFO' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PAYMENTINFO' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PAYMENTINFO group selection -->
					
					<!-- TRANS_PROJECT group selection --> 
					<h:selectOneMenu id="trans_projectfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PROJECT' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PROJECT' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PROJECT' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PROJECT' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PROJECT group selection -->
					
					<!-- TRANS_PURCHASEORDER group selection --> 
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PURCHASEORDER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PURCHASEORDER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PURCHASEORDER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_PURCHASEORDER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_PURCHASEORDER group selection -->
					
					<!-- TRANS_TRANSACTION group selection --> 
					<h:selectOneMenu id="trans_transactionfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_TRANSACTION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_TRANSACTION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_TRANSACTION' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_TRANSACTION' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_TRANSACTION group selection -->
					
					<!-- TRANS_USER group selection --> 
					<h:selectOneMenu id="trans_userfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_USER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_USER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_USER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_USER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_USER group selection -->
					
					<!-- TRANS_VENDOR group selection --> 
					<h:selectOneMenu id="trans_vendorfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_VENDOR' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_VENDOR' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_VENDOR' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_VENDOR' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_VENDOR group selection -->
					
					<!-- TRANS_WORKORDER group selection --> 
					<h:selectOneMenu id="trans_workOrderfieldvalueSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_WORKORDER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_WORKORDER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_WORKORDER' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionDateItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalueSetNull_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TRANS_WORKORDER' and setObject.isNullColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.nullSelectionItems}"/>
					</h:selectOneMenu>
					<!-- end of TRANS_WORKORDER group selection -->
										
					<h:inputText id="fieldvalueSet_#{loopCounter.index}" rendered="#{setObject.valuelocationSet eq 'VALUE'}" style="width: 142px;" value="#{setObject.fieldTextSet}" maxlength="100" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" />
					
					<!-- For table -->
					<h:selectOneMenu id="tablefieldvalueSet_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.valuelocationSet eq 'TABLE'}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.fieldTextSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.talbeSelectionItems}"/>
						<a4j:support event="onchange" action="#{procruleBean.tableNameChange}" reRender="btnTable_#{loopCounter.index},parameterSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
	        	</a4j:outputPanel>
	        </td>    
	        
	        <c:if test="#{(setObject.valuelocationSet eq 'TABLE')}" >
	        <td colspan="3">
	          	<h:inputText id="parameterSet_#{loopCounter.index}" rendered="#{setObject.valuelocationSet eq 'TABLE'}" style="width: 250px;" value="#{setObject.paramTextSet}" disabled="true" />
				<a4j:commandLink id="btnTable_#{loopCounter.index}" styleClass="button" rendered="#{(setObject.valuelocationSet eq 'TABLE') and (!procruleBean.displayDeleteAction and !procruleBean.displayViewAction and !procruleBean.displayUpdateProcRulesDetailAction)}" action="#{procruleBean.processEditSetCommand}">
					<h:graphicImage value="/images/editButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
	        </td>   
	        </c:if>

			<c:if test="#{!(setObject.valuelocationSet eq 'TABLE')}" >
			<!-- Operator +, -, * / -->
        	<td width="50px" align="center">
        		<a4j:outputPanel id="operatorItemSet_#{loopCounter.index}">
        		
	        		<h:selectOneMenu id="operatorNumberSet_#{loopCounter.index}"  style="width: 50px;" rendered="#{setObject.isNumberColumnType}" 
	        				disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operator}" immediate="true" >
						<f:selectItems value="#{procruleBean.numberOperatorItems}"/>
						<a4j:support event="onchange" reRender="operatorGroupSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index}" action="#{procruleBean.operatorSetChange}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="operatorTextSet_#{loopCounter.index}"  style="width: 50px;" rendered="#{setObject.isTextColumnType}" 
	        				disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operator}" immediate="true" >
						<f:selectItems value="#{procruleBean.textOperatorItems}"/>
						<a4j:support event="onchange" reRender="operatorGroupSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index}" action="#{procruleBean.operatorSetChange}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="operatorDateNullSet_#{loopCounter.index}"  style="width: 50px;" rendered="#{setObject.isDateColumnType or setObject.isNullColumnType}" 
	        				disabled="true" value="#{setObject.operator}" immediate="true" >
						<f:selectItems value="#{procruleBean.nullOperatorItems}"/>
						<a4j:support event="onchange" reRender="operatorGroupSet_#{loopCounter.index},operatorpanelSet_#{loopCounter.index}" action="#{procruleBean.operatorSetChange}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
					</h:selectOneMenu>
				</a4j:outputPanel>
        	</td>
        
        	<!-- For operator group -->
			<td>           	
				<h:selectOneMenu id="operatorGroupSet_#{loopCounter.index}"  style="width: 70px;" 
						disabled="#{setObject.isNAOperator or procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction or setObject.isDateColumnType or setObject.isNullColumnType}" 
						value="#{setObject.operatorSet}" immediate="true" >
					<f:selectItems value="#{procruleBean.operatorGroupItems}"/>
					<a4j:support event="onchange" action="#{procruleBean.operatorGroupSetChange}" reRender="operatorpanelSet_#{loopCounter.index}" ><f:param name="command" value="#{loopCounter.index}" /></a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <!-- Operator Field/Value -->
	        <td> 
	        	<a4j:outputPanel id="operatorpanelSet_#{loopCounter.index}">   
					<h:selectOneMenu id="alloperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'ALL' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="alloperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'ALL' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="alloperatorSetDate_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'ALL' and setObject.isDateColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.allSaleTransDateItems}"/>
					</h:selectOneMenu>
					
					<!--  -->

					<h:selectOneMenu id="sale_basicoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_BASIC' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_basicoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_BASIC' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.basicSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customeroperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_CUSTOMER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_customeroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_CUSTOMER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.customerSaleTransTextItems}"/>
					</h:selectOneMenu>
		
					<h:selectOneMenu id="sale_ilocationperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_INVOICE' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_invoiceoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_INVOICE' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceSaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_LOCATION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_locationoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_LOCATION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationSaleTransTextItems}"/>
					</h:selectOneMenu>

					<h:selectOneMenu id="sale_entityoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_ENTITY' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_entityoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_ENTITY' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.entitySaleTransTextItems}"/>
					</h:selectOneMenu>
					
					<!-- User group selection -->
					<h:selectOneMenu id="sale_usvendoreratorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_USER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_useroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_USER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userSaleTransTextItems}"/>
					</h:selectOneMenu>				
					<!-- end of User group selection -->
					
					<!-- SALE_TRANSACTION selection -->
					<h:selectOneMenu id="sale_transactionoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_TRANSACTION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_transactionoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_TRANSACTION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionSaleTransTextItems}"/>
					</h:selectOneMenu>				
					<!-- end of SALE_TRANSACTION selection -->
					
					<!-- SALE_SITUS selection -->
					<h:selectOneMenu id="sale_situsoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_SITUS' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_situsoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_SITUS' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.situsSaleTransTextItems}"/>
					</h:selectOneMenu>				
					<!-- end of SALE_SITUS selection -->
					
					<!-- SALE_AMOUNTS selection -->
					<h:selectOneMenu id="sale_amountsoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_AMOUNTS' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_amountsoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_AMOUNTS' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.amountsSaleTransTextItems}"/>
					</h:selectOneMenu>				
					<!-- end of SALE_AMOUNTS selection -->
					
					<!-- SALE_GENERALLEDGER selection -->
					<h:selectOneMenu id="sale_generalledgeroperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_GENERALLEDGER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sale_generalledgeroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'SALE_GENERALLEDGER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalledgerSaleTransTextItems}"/>
					</h:selectOneMenu>				
					<!-- end of SALE_GENERALLEDGER selection -->
					
					<!-- Transaction group -->

					<h:selectOneMenu id="trans_auditoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_AUDIT' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_auditoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_AUDIT' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgeroperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_GENERALLEDGER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgeroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_GENERALLEDGER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_INVENTORY' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_INVENTORY' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoiceoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_INVOICE' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoiceoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_INVOICE' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_LOCATION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_LOCATION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfooperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PAYMENTINFO' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfooperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PAYMENTINFO' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PROJECT' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PROJECT' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PURCHASEORDER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_PURCHASEORDER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_TRANSACTION' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_TRANSACTION' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_useroperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_USER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_useroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_USER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendoroperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_VENDOR' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendoroperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_VENDOR' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderoperatorSetNumber_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_WORKORDER' and setObject.isNumberColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionNumberItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderoperatorSetText_#{loopCounter.index}"  style="width: 150px;" rendered="#{setObject.operatorSet eq 'TRANS_WORKORDER' and setObject.isTextColumnType}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" value="#{setObject.operatorColumnSet}" maxlength="100" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionTextItems}"/>
					</h:selectOneMenu>
					
					<h:inputText id="operatorSet_#{loopCounter.index}" rendered="#{setObject.operatorSet eq 'VALUE'}" style="width: 142px;" value="#{setObject.operatorTextSet}" maxlength="100" disabled="#{setObject.isNAOperator or procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction or setObject.isDateColumnType or setObject.isNullColumnType}" />
					
	        	</a4j:outputPanel>
	        </td>
        	</c:if>
			</c:if> 

			<c:if id="sprocGroupPanel_#{loopCounter.index}" test="#{(setObject.operationSet eq 'SPROC')}" >
	       	<td width="8px"><h:outputText style="width: 8px;" value="(" /></td>
	       
	       	<td colspan="4">
	       		<h:inputText id="sprocParametersField_#{loopCounter.index}" rendered="#{setObject.operationSet eq 'SPROC'}" style="width: 340px;" value="#{setObject.sprocParameters}" disabled="#{procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction}" />
	       	</td> 
	       
	       	<td width="8px"><h:outputText style="width: 8px;" value="&#160;)" /></td>
			</c:if> 
       	
            <td width="15px">&#160;</td>
            
			<td width="15px" align="right">
				<a4j:commandLink id="addSet_#{loopCounter.index}" styleClass="button" rendered="#{(loopCounter.index eq procruleBean.lastSetObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
					reRender="forEachPanelSet" actionListener="#{procruleBean.processAddSetCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{!(loopCounter.index eq procruleBean.lastSetObject)  or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" />
			</td>            
            <td width="15px" align="right">
            	<a4j:commandLink id="removeSet_#{loopCounter.index}" styleClass="button" rendered="#{!(procruleBean.firstSetObject eq procruleBean.lastSetObject) and !(procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" 
            		reRender="forEachPanelSet" actionListener="#{procruleBean.processRemoveSetCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(procruleBean.firstSetObject eq procruleBean.lastSetObject)  or (procruleBean.displayDeleteAction or procruleBean.displayViewAction or displayUpdateAction or procruleBean.displayUpdateProcRulesDetailAction)}" />
            </td>
            
 			<td width="1px">&#160;</td> 
        </tr>
    	</c:forEach>
	</tbody>
	</table>
	</c:if>
	</a4j:outputPanel>
	
	<c:if test="#{!procruleBean.displayUpdateAction}" >
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>			
			<tr>
	        	<td colspan="15">&#160;</td>
			</tr>	
		</tbody>
		</table>
	</c:if>
	</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="copy-update"><h:commandLink id="copyupdateprocrule" disabled="#{!procruleBean.displayUpdateProcRulesDetailAction}" immediate="true" action="#{procruleBean.displayCopyUpdateProcruleAction}" ></h:commandLink></li>
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{procruleBean.okProcessRuleAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{procruleBean.displayViewAction}" action="#{procruleBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	

</h:form>


</ui:define>
</ui:composition>
</html>