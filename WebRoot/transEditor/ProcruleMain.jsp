<ui:component xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('procruleForm:procruleTable', 'selectedProcruleRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('procruleForm:procruleDetailTable', 'selectedProcruleDetailRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
//]]>
</script>

<style>
.rich-stglpanel-marker {float: left}
.rich-stglpanel-header {text-align: left}
</style>


</ui:define>
<ui:define name="body">
<f:view>
<h:inputHidden id="selectedProcruleRowIndex" value="#{procruleBean.selectedProcruleRowIndex}" />
<h:inputHidden id="selectedProcruleDetailRowIndex" value="#{procruleBean.selectedProcruleDetailRowIndex}" />
    
<h:form id="procruleForm">
     <h1><h:graphicImage id="imgprocessrules-purchasing" alt="Process&#32;&#38;&#32;Rules" rendered="#{procruleBean.isPurchasingMenu}" url="/images/headers/hdr-processrules-purchasing.png" width="250" height="19" /></h1>
     <h1><h:graphicImage id="imgprocessrules-sales" alt="Process&#32;&#38;&#32;Rules" rendered="#{!procruleBean.isPurchasingMenu}" url="/images/headers/hdr-processrules-sales.png" width="250" height="19" /></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
     	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{procruleBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(procruleBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       	</a4j:commandLink>					
     </div> 
     
     <a4j:outputPanel id="showhidefilter">
     	<c:if test="#{!procruleBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/processrules_panel.xhtml">
						<ui:param name="handlerBean" value="#{procruleBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel >
	      	</div>
	     	</div>
		</c:if>
	</a4j:outputPanel>
     
    <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-Rules.png" width="192"  height="17" />
      </span>
    </div>
   
	<!-- table-four-contentNew  -->     
	<div id="table-four-contentNew">
        
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
    <tbody>
	<tr>
	<td style="height:auto;vertical-align:top;">
 
    <div id="bottom" style="padding: 0 0 3px 0;">
    	<div id="table-four"  >
	    <div id="table-four-top" style="padding: 3px 0 0 10px;" >
	    	<!-- 
	    	<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Rules&#160;&#160;&#160;&#160;&#160;" /> 
	    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{procruleBean.procruleDataModel.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	     	-->		
	     			
	     	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>					 
					<td align="left" >
						<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Rules&#160;&#160;&#160;&#160;&#160;" />
					</td>
					<td align="left" width="300" >
						<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
				        	<a4j:status id="pageInfo"  
								startText="Request being processed..." 
								stopText="#{procruleBean.procruleDataModel.pageDescription }"
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
				     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
					</td>
					<td style="width:50%;">&#160;</td>
					</tr>
				</tbody>
			</table>	
	    </div>
       
        <div class="scrollInnerNew" id="resize" style="width:670px;">
       	<rich:dataTable rowClasses="odd-row,even-row" id="procruleTable" rows="#{procruleBean.procruleDataModel.pageSize }"  value="#{procruleBean.procruleDataModel}" var="procruleMaintenance"  >
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('procruleForm:procruleTable', this);"  actionListener="#{procruleBean.selectedProcruleRowChanged}"
                        reRender="procruleDetailTable,addprocrule,copyaddprocrule,updateprocrule,updateprocruleDetail,deleteprocrule,deleteallprocrule,viewprocrule,procruleDetailDataTest,distinctDateItems" />
            <!--                       	
           	<rich:column id="procruleId" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="procruleId-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['procruleId']}"
                              value="Rule Id"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:outputText value="#{procruleMaintenance.procruleId}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	-->
           	<rich:column id="orderNo" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="orderNoId-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['orderNo']}"
                              value="Order No"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:outputText value="#{procruleMaintenance.orderNo}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           	<rich:column id="ruletypeCode" style="width:120px" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="procruleType-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['ruletypeCode']}"
                              value="Rule Type"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:outputText value="#{cacheManager.listCodeMap['RULETYPE'][procruleMaintenance.ruletypeCode].description}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	          	
           	<rich:column id="procruleName" style="width:120px" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="procruleName-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['procruleName']}"
                              value="Name"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:outputText value="#{procruleMaintenance.procruleName}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
           	
           <rich:column id="description" style="width:320px" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="description-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['description']}"
                              value="Description"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:outputText value="#{procruleMaintenance.description}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
                     	
           	<rich:column id="reprocessCode" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="reprocessCode-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['reprocessCode']}"
                              value="Reprocess"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet> 
            	<h:outputText value="#{procruleBean.reprocessDescriptionMap[procruleMaintenance.reprocessCode]}" style="#{(procruleMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
      	
           	<rich:column id="activeFlag" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="activeFlag-a4j"
                              styleClass="sort-#{procruleBean.procruleDataModel.sortOrder['activeFlag']}"
                              value="Active?"
                              actionListener="#{procruleBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="procruleTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check" value="#{procruleMaintenance.activeFlag == 1}" disabled="#{true}" id="activeFlagId" />
           	</rich:column>
          </rich:dataTable>
          
          <a4j:outputPanel id="custDataTest">
          <c:if test="#{procruleBean.procruleDataModel.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
         
         </div>
         <!-- scroll-inner -->

       <rich:datascroller id="trScroll"  for="procruleTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{procruleBean.procruleDataModel.curPage}" />
                                  
       <div id="table-four-bottom">
        	<ul class="right">
        	<li class="reorder"><h:commandLink id="reorderprocrule" immediate="true" action="#{procruleBean.displayReorderProcruleAction}" disabled="#{procruleBean.currentUser.viewOnlyBooleanFlag}"
        										style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink></li>
         	<li class="add"><h:commandLink id="addprocrule" immediate="true" action="#{procruleBean.displayAddProcruleAction}"  disabled="#{procruleBean.currentUser.viewOnlyBooleanFlag}"
         								   style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
         	</li>          
        	
        	<li class="update"><h:commandLink id="updateprocrule" immediate="true" action="#{procruleBean.displayUpdateProcruleAction}"  disabled="#{!procruleBean.displayProcruleUpdateButtons or procruleBean.currentUser.viewOnlyBooleanFlag}"
         								   style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
        	</li>
        	<li class="delete115"><h:commandLink id="deleteallprocrule" disabled="#{!procruleBean.displayProcruleDeleteButtons or procruleBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{procruleBean.displayDeleteAllProcruleAction}"
        								 		 style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" ></h:commandLink>
        	</li>
        	</ul>
       </div> <!-- table-four-bottom -->   
       <!-- end custGrid -->
       
       </div>
    </div>
	</td>
	
	<td style="height:auto;vertical-align:top;" >
  	<div id="bottom" style="padding: 0 0 3px 0;">
       <div id="table-four"  style="margin:0;" >
       <div id="table-four-top" style="padding: 3px 0 0 10px;">
       	<!--  
       		<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Definitions&#160;&#160;&#160;" />
       		
       		<a4j:commandLink id="showTextSqlPanel" style="height:22px;text-decoration: underline;color:#0033FF"  oncomplete="initScrollingTables();"
       				reRender="procruleDetailTable,showTextSqlPanel" actionListener="#{procruleBean.toggleShowSqlLike}" >
       			<h:outputText value="#{(procruleBean.isShowSqlLike) ? 'Show Text' : 'Show SQL'}" />
       		</a4j:commandLink>
      --> 		
       		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
				<tbody>
					<tr>					 
					<td align="left" >
						<h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Definitions&#160;&#160;&#160;" />
					</td>
					<td  align="center">Effective Date:&#160;           	
						<h:selectOneMenu id="distinctDateItems" value="#{procruleBean.selectedEffectiveDate}" immediate="true" >
							<f:selectItems value="#{procruleBean.filterDistinctDateItems}"/>
							<a4j:support event="onchange" actionListener="#{procruleBean.distinctDateChanged}" reRender="procruleDetailTable"/>			
						</h:selectOneMenu>
			        </td>
					
					<td align="right" width="100" valign="baseline">
						<a4j:commandLink id="showTextSqlPanel" style="height:22px;color:#0033FF; text-decoration: underline; "  oncomplete="initScrollingTables();"
       							reRender="procruleDetailTable,showTextSqlPanel" actionListener="#{procruleBean.toggleShowSqlLike}" >
       						<h:outputText value="#{(procruleBean.isShowSqlLike) ? 'Show Text' : 'Show SQL'}" />
       					</a4j:commandLink>
					</td>
					<td style="width:25px;">&#160;</td>
					</tr>
				</tbody>
			</table>	
       </div>
       
        <div class="scrollInner"  id="resize1">
         	<rich:dataTable rowClasses="odd-row,even-row" id="procruleDetailTable" value="#{procruleBean.selectedProcruleDetails}" var="procruleDetailMaintenance" >
          
            <rich:column id="category" style="width:25px" styleClass="column-left">
            	<f:facet name="header"><h:outputText value="Action" /></f:facet>
            	<h:outputText value="#{((procruleDetailMaintenance.condition eq 'When') or (procruleDetailMaintenance.condition eq 'Set')) ? procruleDetailMaintenance.condition : ''}"
            					style="#{(procruleDetailMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
           	</rich:column>
                          
           	<rich:column id="condition">
                <f:facet name="header"><h:outputText value="And/Or" /></f:facet>
                <h:outputText value="#{!((procruleDetailMaintenance.condition eq 'When') or (procruleDetailMaintenance.condition eq 'Set')) ? procruleDetailMaintenance.condition : ''}"
                				style="#{(procruleDetailMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
            </rich:column>
            
            <rich:column id="textlike" style="width:380px" rendered="#{!procruleBean.isShowSqlLike}" styleClass="column-left">
                <f:facet name="header"><h:outputText value="Clause (Text)" /></f:facet>
                <h:outputText value="#{(procruleDetailMaintenance.openparen eq '1')? '(' : '&#160;'}&#160;#{procruleDetailMaintenance.textLike}&#160;#{(procruleDetailMaintenance.closeparen eq '1')? ')' : '&#160;'}"
                				style="#{(procruleDetailMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
            </rich:column>
            
            <rich:column id="sqllike" style="width:380px" rendered="#{procruleBean.isShowSqlLike}" styleClass="column-left">
                <f:facet name="header"><h:outputText value="Clause (SQL)" /></f:facet>
                <h:outputText value="#{(procruleDetailMaintenance.openparen eq '1')? '(' : '&#160;'}&#160;#{procruleDetailMaintenance.sqlLike}&#160;#{(procruleDetailMaintenance.closeparen eq '1')? ')' : '&#160;'}"
                				style="#{(procruleDetailMaintenance.activeFlag) == 1 ? '' : 'color:#888888;'}" />
            </rich:column>
            
          </rich:dataTable> 	
          

          <a4j:outputPanel id="procruleDetailDataTest">
           		<c:if test="#{procruleBean.procruleDataModel.rowCount == 0}">
					<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          		</c:if>
          		<c:if test="#{procruleBean.selectedProcruleRowIndex > 0 and procruleBean.procruleDataModel.rowCount == 0 and procruleBean.procruleDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Definitions exist for the Selected Rule" /></div>
          		</c:if>
          		<c:if test="#{procruleBean.selectedProcruleRowIndex == -1 and procruleBean.procruleDataModel.rowCount > 0}">
					<div class="nodatadisplay"><h:outputText value="No Rules Selected." /></div>
          		</c:if>
           </a4j:outputPanel>

		</div>
        <!-- scroll-inner -->
        
            
        <rich:datascroller id="trScrollDetail"  for="procruleDetailTable"  maxPages="10" oncomplete="initScrollingTables();" style="clear:both;" align="center"
                          stepControls="auto" ajaxSingle="false"  reRender="pageInfo" page="#{procruleBean.procruleDataModel.curPage}" />
        
       	<div id="table-four-bottom">
        	<ul class="right"> 
        	<li class="copy-add"><h:commandLink id="copyaddprocrule" disabled="#{!procruleBean.displayProcruleDetailButtons or procruleBean.currentUser.viewOnlyBooleanFlag}" immediate="true" 
        										style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{procruleBean.displayCopyAddProcruleAction}" ></h:commandLink>
        	</li>          
         	<li class="update"><h:commandLink id="updateprocruleDetail" disabled="#{!procruleBean.displayProcruleDetailButtons or procruleBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{procruleBean.displayUpdateProcruleDetailsAction}"
         									  style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" ></h:commandLink>
            </li>
         	<li class="delete115"><h:commandLink id="deleteprocrule" disabled="#{!procruleBean.displayProcruleDetailButtons or procruleBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{procruleBean.displayDeleteProcruleAction}" 
         										 style="#{(procruleBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
            </li>
         	<li class="view"><h:commandLink id="viewprocrule" disabled="#{!procruleBean.displayProcruleDetailButtons}" immediate="true"  action="#{procruleBean.displayViewProcruleAction}"></h:commandLink>
           </li>

        	</ul>
       </div> <!-- table-four-bottom -->   
       	
       	<!-- end custLocnGrid --> 

      	</div>  <!-- table-four -->   
    </div>   <!-- bottom -->
	
	</td>
	</tr>
	</tbody>
	</table>

	</div>
    <!-- end of table-four-contentNew  -->  
     
</h:form>
</f:view>
   
<rich:modalPanel id="execute" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Execute" />
	</f:facet>
	<h:form id="executeForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('execute');" action="#{taxHolidayBean.executeAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('execute'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>
   
</ui:define>
</ui:composition>
</ui:component>
