<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
	<h1><h:graphicImage id="imgprocesstables-purchasing" alt="Process&#32;&#38;&#32;Tables" rendered="#{proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-purch.png" width="250" height="19" /></h1>
	<h1><h:graphicImage id="imgprocesstables-sales" alt="Process&#32;&#38;&#32;Tables" rendered="#{!proctableBean.isPurchasingMenu}" url="/images/headers/hdr-processtables-sales.png" width="250" height="19" /></h1>
     

<h:form id="custLocnUpdateForm">
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td colspan="7">Populate a Process Table</td></tr>
	</thead>
	<tbody>			
		<tr>
			<td>&#160;</td>
			<td>Table Name:</td>
			<td style="width:200px;">
				<h:inputText id="proctableName" style="width:200px;" maxlength = "40" value="#{proctableBean.updateProctable.proctableName}" disabled="true" />
			</td>
			<td>&#160;</td>
			<td>Description:</td>
			<td style="width:300px;">
				<h:inputText id="description" style="width:300px;" maxlength = "50" value="#{proctableBean.updateProctable.description}" disabled="true" />
			</td>
			<td width="10%">&#160;</td>
		</tr>
		
		<tr>
			<td>&#160;</td>
			<td>Active?:</td>
	        <td style="width:300px;">	 
				<h:selectBooleanCheckbox id="activeFlagId" value="#{proctableBean.updateProctable.activeBooleanFlag}" styleClass="check"  />
			</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>	
		
	</tbody>
	</table>
	</div> <!-- table-one-content -->
	</div>
</div>

<div id="bottom">	
<div id="table-four">

	<div id="table-one-content">
    <div class="scrollInner"  id="resize">
	
	<a4j:outputPanel id="forEachPanelInColumn"> 
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>			
		 
		<tr height="20px">
        	<th>&#160;</th>
			
			<c:if test="#{!(empty proctableBean.updateProctable.inColumnType01)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType01 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName01] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName01]) : proctableBean.updateProctable.inColumnName01}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType02)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType02 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName02] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName02]) : proctableBean.updateProctable.inColumnName02}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType03)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType03 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName03] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName03]) : proctableBean.updateProctable.inColumnName03}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType04)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType04 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName04] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName04]) : proctableBean.updateProctable.inColumnName04}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType05)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType05 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName05] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName05]) : proctableBean.updateProctable.inColumnName05}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType06)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType06 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName06] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName06]) : proctableBean.updateProctable.inColumnName06}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType07)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType07 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName07] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName07]) : proctableBean.updateProctable.inColumnName07}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType08)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType08 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName08] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName08]) : proctableBean.updateProctable.inColumnName08}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType09)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType09 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName09] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName09]) : proctableBean.updateProctable.inColumnName09}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType10)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType10 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName10] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName10]) : proctableBean.updateProctable.inColumnName10}" /></th>	        
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType11)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType11 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName11] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName11]) : proctableBean.updateProctable.inColumnName11}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType12)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType12 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName12] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName12]) : proctableBean.updateProctable.inColumnName12}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType13)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType13 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName13] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName13]) : proctableBean.updateProctable.inColumnName13}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType14)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType14 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName14] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName14]) : proctableBean.updateProctable.inColumnName14}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType15)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType15 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName15] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName15]) : proctableBean.updateProctable.inColumnName15}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType16)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType16 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName16] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName16]) : proctableBean.updateProctable.inColumnName16}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType17)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType17 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName17] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName17]) : proctableBean.updateProctable.inColumnName17}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType18)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType18 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName18] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName18]) : proctableBean.updateProctable.inColumnName18}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType19)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType19 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName19] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName19]) : proctableBean.updateProctable.inColumnName19}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.inColumnType20)}">
	        <th><h:outputText style="width: 150px;" value="#{(proctableBean.updateProctable.inColumnType20 eq 'F') ? ((proctableBean.isPurchasingMenu) ? matrixCommonBean.transactionColumnNameMap[proctableBean.updateProctable.inColumnName20] : matrixCommonBean.salesColumnNameMap[proctableBean.updateProctable.inColumnName20]) : proctableBean.updateProctable.inColumnName20}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType01)}">
	        <th><h:outputText style="width: 100px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 180px;" value="#{proctableBean.updateProctable.outColumnName01}" /></th>
			<th>&#160;</th>
	        </c:if>
			
			<c:if test="#{!(empty proctableBean.updateProctable.outColumnType02)}">
	        <th><h:outputText style="width: 100px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 180px;" value="#{proctableBean.updateProctable.outColumnName02}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType03)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName03}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType04)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName04}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType05)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName05}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType06)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName06}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType07)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName07}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType08)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName08}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType09)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName09}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
	        <c:if test="#{!(empty proctableBean.updateProctable.outColumnType10)}">
	        <th><h:outputText style="width: 150px;" value="&#160;"   /></th>
	        <th><h:outputText style="width: 150px;" value="#{proctableBean.updateProctable.outColumnName10}" /></th>
	        <th>&#160;</th>
	        </c:if>
	        
			<th width="10%">&#160;</th>
			<th width="15px">&#160;</th>            
            <th width="15px">&#160;</th>
			<th width="50px"><h:outputText style="width: 50px;" value="&#160;&#160;&#160;&#160;"/></th> 
		</tr>
			 
		<c:forEach var="inColumnObject" items="#{proctableBean.populateObjectArrayList}" varStatus="loopCounter" >
		
        <tr height="20px">
        
        	<td>&#160;</td>

			<c:forEach var="innerPopulateObject" items="#{inColumnObject}" varStatus="innerloopCounter" >
			
	  		<c:if test="#{innerPopulateObject.isColumnSearchType}">
			<td width="150px" > 	
				<h:inputText id="fieldColumn_#{loopCounter.index}_#{innerloopCounter.index}" rendered="#{innerPopulateObject.isColumnSearchType}" style="width: 150px;" value="#{innerPopulateObject.columnSearchName}" />			
	        </td>
	        <td>&#160;</td>
	        
	        </c:if>
			
			<c:if test="#{innerPopulateObject.isTextSearchType}">
			<td> 	
				<h:inputText id="fieldText_#{loopCounter.index}_#{innerloopCounter.index}" rendered="#{innerPopulateObject.isTextSearchType}" style="width: 150px;" value="#{innerPopulateObject.textSearchName}" />				
	        </td> 
	        <td>&#160;</td>
			</c:if>
	        
	 		<c:if test="#{innerPopulateObject.isComboSearchType}">
	        <td width="100px">           	
				<h:selectOneMenu id="fieldColumnColumn_#{loopCounter.index}_#{innerloopCounter.index}"  rendered="#{innerPopulateObject.isComboSearchType}" style="width: 100px;"  
					value="#{innerPopulateObject.operation}" immediate="true" >
					<f:selectItems value="#{proctableBean.valuePopulateItems}"/>
					<a4j:support event="onchange" action="#{proctableBean.valuePopulateObjectColumnChange}" reRender="fieldvaluePanel_#{loopCounter.index}_#{innerloopCounter.index}" >
						<f:param name="command" value="#{loopCounter.index}" />
						<f:param name="innercommand" value="#{innerloopCounter.index}" />
					</a4j:support>
				</h:selectOneMenu>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldvaluePanel_#{loopCounter.index}_#{innerloopCounter.index}" rendered="#{innerPopulateObject.isComboSearchType}">   
	        	
	        		<h:selectOneMenu id="allfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'ALL'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.allSaleTransItems}"/>
					</h:selectOneMenu>
					       	
					<h:selectOneMenu id="sales_basicfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_BASIC'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.basicTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_customerfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_CUSTOMER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.customerTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_invoicefieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_INVOICE'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.invoiceTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_locationfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_LOCATION'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.locationTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_entityfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_ENTITY'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.entityTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="sales_userfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'SALE_USER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{proctableBean.userTranbsactionItems}"/>
					</h:selectOneMenu>
					
					<!-- For transaction, use columns in proctableBean.basicTranbsactionItems -->  
					<h:selectOneMenu id="trans_auditfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_AUDIT'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.auditTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_generalLedgerfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_GENERALLEDGER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.generalLedgerTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_inventoryfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_INVENTORY'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.inventoryTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_invoicefieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_INVOICE'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.invoiceTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_locationfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_LOCATION'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.locationTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_paymentInfofieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_PAYMENTINFO'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.paymentInfoTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_projectfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_PROJECT'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.projectTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_purchaseOrderfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_PURCHASEORDER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.purchaseOrderTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_transactionfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_TRANSACTION'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.transactionTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_userfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_USER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.userTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_vendorfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_VENDOR'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.vendorTransactionItems}"/>
					</h:selectOneMenu>
					
					<h:selectOneMenu id="trans_workOrderfieldvalue_#{loopCounter.index}_#{innerloopCounter.index}"  style="width: 180px;" rendered="#{innerPopulateObject.operation eq 'TRANS_WORKORDER'}" value="#{innerPopulateObject.fieldColumn}" immediate="true" >
						<f:selectItems value="#{procruleBean.workOrderTransactionItems}"/>
					</h:selectOneMenu>
					<!-- end of transaction -->
					
					<h:inputText id="fieldvalueText_#{loopCounter.index}_#{innerloopCounter.index}" rendered="#{(innerPopulateObject.operation eq 'VALUE') or (innerPopulateObject.operation eq 'BLANK')}" style="width: 173px;" value="#{innerPopulateObject.fieldText}" disabled="#{innerPopulateObject.operation eq 'BLANK' or proctableBean.displayDeleteAction or proctableBean.displayViewAction}" />
					 	
	      		</a4j:outputPanel> 
	        </td>
	        <td>&#160;</td>
	        </c:if>
	        </c:forEach>     

			<td width="10%">&#160;</td>
			
			<td width="15px">
				<a4j:commandLink id="add_#{loopCounter.index}" styleClass="button" rendered="#{((loopCounter.index eq proctableBean.lastPopulateObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction))}" 
					reRender="forEachPanelInColumn" actionListener="#{proctableBean.processAddPopulateCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{!(loopCounter.index eq proctableBean.lastPopulateObject)  or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="remove_#{loopCounter.index}" styleClass="button" rendered="#{!(proctableBean.firstPopulateObject eq proctableBean.lastPopulateObject) and !(proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" 
            		reRender="forEachPanelInColumn" actionListener="#{proctableBean.processRemovePopulateCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(proctableBean.firstPopulateObject eq proctableBean.lastPopulateObject) or (proctableBean.displayDeleteAction or proctableBean.displayViewAction)}" />
            </td>
            
 			<td width="50px"><h:outputText style="width: 50px;" value="&#160;&#160;&#160;&#160;"/></td> 
        </tr>
    	</c:forEach>
	</tbody>
	</table>
	</a4j:outputPanel>
	
	</div>
	</div><!-- table-four-content -->
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" value="" action="#{proctableBean.okPopulateTableAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{proctableBean.cancelAction}" /></li>
	</ul>
	</div>	
	
</div>	
</div>
</h:form>


</ui:define>
</ui:composition>
</html>