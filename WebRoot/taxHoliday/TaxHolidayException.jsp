<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxHolidayExceptionForm:aTaxCodeDetailList', 'aTaxCodeDetailListRowIndex'); } );

function resetState()
{ 
    document.getElementById("taxHolidayExceptionForm:stateMenuMain").value = "";
}
//]]>
</script>
</ui:define>

<ui:define name="body">
	<f:view>
	<h:inputHidden id="aTaxCodeDetailListRowIndex" value="#{taxHolidayBean.selectedJurisdictionDetailRowIndex}"/>

	<h:form id="taxHolidayExceptionForm">

	<h1><img id="imgTaxabilityMatrix" alt="Tax Holidays" src="../images/headers/hdr-taxability-matrix.gif" width="250" height="19" /></h1>
	<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="8">Select Tax Holiday Jurisdiction Exceptions</td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="8">Tax Holiday/TaxCode</th>
		</tr>
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Holiday Code:</td>
          <td style="width: 220px;">
           	<h:inputText  id="filterJurisdictionHolidayCode"
                          disabled="true"                         
                          rendered="true"
                          value="#{taxHolidayBean.filterJurisdictionHolidayCode}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Country:</td>
          <td style="width: 220px;">           	
                <h:selectOneMenu id="filterJurisdictionCountry" 
							disabled="true" 
							value="#{taxHolidayBean.filterJurisdictionCountry}"
							immediate="true">
							<f:selectItems value="#{taxHolidayBean.updateCountryMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">State:</td>
          <td style="width: 220px;">
                <h:selectOneMenu id="filterJurisdictionState"
							disabled="true" 
							value="#{taxHolidayBean.filterJurisdictionState}">
							<f:selectItems value="#{taxHolidayBean.updateStateMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">TaxCode:</td>
          <td colspan="5">
           	<h:inputText style="width: 604px;"
                          id="filterJurisdictionTaxCode"
                          disabled="true" 
                          rendered="true"
                          value="#{taxHolidayBean.filterJurisdictionTaxCode}" />
          </td>
          <td>&#160;</td>
         </tr>
         
        
         
         
		
	</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="7">Jurisdictions</th>
		</tr>
		<tr>
			<td colspan="7">Selection Filters</td>
		</tr>
		
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">County:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu 	id="countyMenuItems"
					binding="#{taxHolidayBean.countyInput}" 
					value="#{taxHolidayBean.filterJurisdictionCounty}">
					<f:selectItems value="#{taxHolidayBean.filterJurisdictionCountyMenuItems}"/>
					 						
				</h:selectOneMenu>
          </td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">City:</td>
          <td style="width: 220px;">
				<h:selectOneMenu 	id="cityMenuItems"  
					binding="#{taxHolidayBean.cityInput}"
					value="#{taxHolidayBean.filterJurisdictionCity}">
					<f:selectItems value="#{taxHolidayBean.filterJurisdictionCityMenuItems}"/>
					 
				</h:selectOneMenu>
          </td>
          
          <td tyle="width:10%;">&#160;</td>
        </tr>
        
        <tr>

          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">STJ:</td>
          <td colspan="4">
				<h:selectOneMenu 	id="stjMenuItems"  style="width: 604px;"
					binding="#{taxHolidayBean.stjInput}"
					value="#{taxHolidayBean.filterJurisdictionStj}">
					<f:selectItems value="#{taxHolidayBean.filterJurisdictionStjMenuItems}"/>
					 
				</h:selectOneMenu>
          </td>

          <td tyle="width:10%;">&#160;</td>
        </tr>

		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	</div>
	</div>

	<div id="bottom">
		<div id="table-four">
			<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
			<div id="table-four-content">

			<h:panelGroup id="pgTableDisplay" rendered="true">
	
			<div id="table-one-bottom">
				<a4j:status id="pageInfo11" 
								startText="Request being processed..." 
							 	stopText="#{taxHolidayBean.pageJurisdictionDescription}"
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
			     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			     <ul class="right">
			      	<li class="clear"><h:commandLink id="taxcodeClearBtn" action="#{taxHolidayBean.resetJurisdictionSearchAction}" /></li>
			      	<li class="search"><h:commandLink id="searchTaxCodeBtn" action="#{taxHolidayBean.retrieveTaxHolidayDetailByJurisdiction}"  /></li>
			      	<li class="viewselected"><h:commandLink id="taxcodeViewselectedBtn" action="#{taxHolidayBean.viewSelectedJurisdictionAction}" /></li>
			     </ul>
		    </div>
    <!-- table-one-bottom -->
	
			<div class="scrollContainer">
				<div class="scrollInner" id="resize">
					<rich:dataTable rowClasses="odd-row,even-row" id="aTaxCodeDetailList" 
						value="#{taxHolidayBean.listTaxHolidayDetailByJurisdiction}" var="taxHolidayDetail" >
				
						<a4j:support id="rowTaxCodeClick" event="onRowClick" status="rowstatus" 
							onsubmit="selectRow('taxHolidayExceptionForm:aTaxCodeDetailList', this);"
							actionListener="#{taxHolidayBean.selectedJurisdictionDetailChanged}"
							reRender="taxHolidayExceptionForm:searchTaxCodeBtn" />
				
							<rich:column style="text-align: center" >
								<f:facet name="header">
									<h:outputText value="Select" />
								</f:facet>
								<h:selectBooleanCheckbox styleClass="check" id="chkTaxCodeSelected" value="#{taxHolidayDetail.selected}" >
									<a4j:support event="onclick" actionListener="#{taxHolidayBean.exceptionCheckboxSelected}"/>
								</h:selectBooleanCheckbox>

							</rich:column>
							
						<rich:column id="taxcodeCountyId" sortBy="#{taxHolidayDetail.taxcodeCounty}" >
							<f:facet name="header">
								<h:outputText styleClass="headerText" value="County" />
							</f:facet>
							<h:outputText value="#{taxHolidayDetail.taxcodeCounty}" />
						</rich:column>
						
						<rich:column id="taxcodeCityId" sortBy="#{taxHolidayDetail.taxcodeCity}" >
							<f:facet name="header">
								<h:outputText styleClass="headerText" value="City" />
							</f:facet>
							<h:outputText value="#{taxHolidayDetail.taxcodeCity}" />
						</rich:column>
						
						<rich:column id="taxcodeStjId" sortBy="#{taxHolidayDetail.taxcodeStj}" >
							<f:facet name="header">
								<h:outputText styleClass="headerText" value="STJ" />
							</f:facet>
							<h:outputText value="#{taxHolidayDetail.taxcodeStj}" />
						</rich:column>
						
					</rich:dataTable>
				</div>
			</div>
	
			<div id="table-one-bottom">
			     <ul class="right">
			      	<li class="clear-all"><h:commandLink id="taxcodeSelectNoneBtn" action="#{taxHolidayBean.selectedJurisdictionNoneAction}" /></li>
			      	<li class="all-counties"><h:commandLink id="btnAllCounties" disabled="#{!taxHolidayBean.displayAllCounties}" action="#{taxHolidayBean.allCountiesAction}" /></li>
					<li class="all-cities"><h:commandLink id="btnAllCities" disabled="#{!taxHolidayBean.displayAllCities}" action="#{taxHolidayBean.allCitiesAction}" /></li>
					<li class="all-stjs"><h:commandLink id="btnAllStjs" disabled="#{!taxHolidayBean.displayAllStjs}" action="#{taxHolidayBean.allStjsAction}" /></li>
					<!--
			      	<li class="select-all"><h:commandLink id="taxcodeSelectAllBtn"  action="#{taxHolidayBean.selectedJurisdictionAllAction}" reRender="" /></li>
			      	-->
			     </ul>
    		</div>
    		<!-- table-one-bottom -->
    
    
    		<!-- below table is hack to fix unwanted scroll bars. don't remove it. -->
    		<table cellpadding="0" cellspacing="0"  width="100%" id="rollover" class="ruler">
			<tbody>	
				<tr>
		        	<td style="width: 50px;">&#160;</td>
		       	</tr>
			</tbody>
			</table>

		</h:panelGroup>
	</div>
	
	<!-- <div id="table-four-bottom"> change to <div id="table-one-bottom"> to make blue color-->
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" action="#{taxHolidayBean.okExceptionAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{taxHolidayBean.cancelExceptionAction}" /></li>
	</ul>
	</div>
	</div>
	</div>
	
	<rich:modalPanel id="loader" zindex="2000" autosized="true"><h:outputText value="Processing Search ..."/></rich:modalPanel>
	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
</h:form>
</f:view>	
</ui:define>
</ui:composition>
</html>
