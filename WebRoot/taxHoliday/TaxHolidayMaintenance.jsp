<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
 	function resetInputs() {
    	document.getElementById("taxHolidayMaintenanceForm:filterEffectiveDate").component.resetSelectedDate();
    }
    
    registerEvent(window, "load", function() { selectRowByIndex('taxHolidayMaintenanceForm:batchTable', 'selectedRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
               
    function resetState()
	{ 
    	document.getElementById("taxHolidayMaintenanceForm:stateMenuMain").value = "";
	}
//]]>
</script>
</ui:define>
<ui:define name="body">
   <f:view>
    <h:inputHidden id="selectedRowIndex" value="#{taxHolidayBean.selectedRowIndex}" />
    <h:form id="taxHolidayMaintenanceForm">
     <h1><h:graphicImage id="imgTaxholidayMaintenance" alt="Tax Holiday Maintenance Status" url="/images/headers/hdr-taxholiday-maintenance.gif" width="250" height="19" /></h1>
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" /></div>
     <div id="top">
      <div id="table-one">
       <div id="table-one-top"><a4j:outputPanel id="message"><h:messages errorClass="error" /></a4j:outputPanel></div>
       <div id="table-one-content" style="height: auto;" onkeypress="return submitEnter(event,'taxHolidayMaintenanceForm:searchBtn')">
        <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
               
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Holiday Code:</td>
          <td style="width: 220px;">
           	<h:inputText  popup="true"
                          id="filterTaxHolidayCode"
                          rendered="true"
                          label="Entry Date"
                          onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" 
                          value="#{taxHolidayBean.filterTaxHoliday.taxHolidayCode}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Country:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="countryMenuMain" 
							value="#{taxHolidayBean.filterTaxHoliday.taxcodeCountryCode}"
							immediate="true"
							valueChangeListener="#{taxHolidayBean.filterCountryChanged}" 
							onchange="resetState();submit();"
					
							>
							<f:selectItems value="#{taxHolidayBean.filterCountryMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">State:</td>
          <td style="width: 220px;">
				<h:selectOneMenu id="stateMenuMain"
							value="#{taxHolidayBean.filterTaxHoliday.taxcodeStateCode}">
							<f:selectItems value="#{taxHolidayBean.filterStateMenuItems}"/>
				</h:selectOneMenu>

          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Description:</td>
          <td colspan="5">
           	<h:inputText style="width: 604px;" popup="true"
                          id="filterDescription"
                          rendered="true"
                          label="Entry Date"
                          value="#{taxHolidayBean.filterTaxHoliday.description}" />
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Effective Date:</td>
          <td style="width: 220px;">
           	<rich:calendar popup="true"
                          id="filterEffectiveDate"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          label="Entry Date"
                          value="#{taxHolidayBean.filterTaxHoliday.effectiveDate}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 100px;">Expiration Date:</td>
          <td style="width: 220px;">
           	<rich:calendar popup="true"
                          id="filterExpirationDate"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          label="Entry Date"
                          value="#{taxHolidayBean.filterTaxHoliday.expirationDate}" />
          </td>
          <td>&#160;</td>
         </tr>
         
         <h:inputHidden id="taxHolidayCodeSelected"
                        value="#{taxHolidayBean.selTaxHoliday.taxHolidayCode}"
                        immediate="true" />
        </table>
       </div>
       <!-- table-one-content -->
       <div id="table-one-bottom">
        <ul class="right">
         <li class="clear">
          <h:commandLink onclick="return resetInputs();" id="btnClear"
                         action="#{taxHolidayBean.resetFilterSearchAction}" />
         </li>
         <li class="search">
          <h:commandLink id="searchBtn"
                         action="#{taxHolidayBean.retrieveFilterTaxHoliday}" />
         </li>
        </ul>
       </div>
       <!-- table-one-bottom -->
      </div>
      <!-- table-one-content -->
     </div>
     <!-- table-one -->
     <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <h:graphicImage url="/images/containers/STSView-taxholidays.gif" width="192"  height="17" />
      </span>
     </div>
     <div id="bottom">
      <div id="table-four">
       <div id="table-four-top">
        <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
        <a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{taxHolidayBean.taxHolidayDataModel.pageDescription }"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
       </div>
       <div id="table-four-content">
        <div class="scrollContainer">
         <div class="scrollInner"  id="resize">
          <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                          rows="#{taxHolidayBean.taxHolidayDataModel.pageSize }"
                          value="#{taxHolidayBean.taxHolidayDataModel}"
                          var="taxHolidayMaintenance">
           	<a4j:support id="ref2tbl"
                        event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('taxHolidayMaintenanceForm:batchTable', this);"
                        actionListener="#{taxHolidayBean.selectedRowChanged}"
                        reRender="taxHolidayMaintenanceForm:addTaxHoliday,taxHolidayMaintenanceForm:copyAddTaxHoliday,taxHolidayMaintenanceForm:updateTaxHoliday,taxHolidayMaintenanceForm:viewTaxHoliday,taxHolidayMaintenanceForm:deleteTaxHoliday,taxHolidayMaintenanceForm:mapTaxHoliday,taxHolidayMaintenanceForm:executeTaxHoliday" />
                        
           	<rich:column id="taxHolidayCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="taxHolidayCode-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['taxHolidayCode']}"
                              value="Holiday Code"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:outputText value="#{taxHolidayMaintenance.taxHolidayCode}" />
           	</rich:column>
           	
           	<rich:column id="taxcodeCountryCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="country-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['taxcodeCountryCode']}"
                              value="Country"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:outputText value="#{cacheManager.countryMap[taxHolidayMaintenance.taxcodeCountryCode]} (#{taxHolidayMaintenance.taxcodeCountryCode})"  />
           	</rich:column>
           	
           	<rich:column id="taxcodeStateCode" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="state-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['taxcodeStateCode']}"
                              value="State"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:outputText value="#{cacheManager.taxCodeStateMap[taxHolidayMaintenance.taxcodeStateCode].name} #{(not empty cacheManager.taxCodeStateMap[taxHolidayMaintenance.taxcodeStateCode].name) ? 
						'(' : ''}#{taxHolidayMaintenance.taxcodeStateCode}#{(not empty cacheManager.taxCodeStateMap[taxHolidayMaintenance.taxcodeStateCode].name) ? ')' : ''}" />
	
           	</rich:column>
           	
           	<rich:column id="description" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="description-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['description']}"
                              value="Description"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	
            	<!--  
            	<h:outputText type="url" value="#{taxHolidayMaintenance.description}"  >
            	</h:outputText>
            	-->

        		<h:outputText id="url" type="url" value="#{taxHolidayMaintenance.description}" />
            	
            	
           	</rich:column>
           	
           	<rich:column id="effectiveDate" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="effectiveDate-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['effectiveDate']}"
                              value="Effective Date"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:outputText value="#{taxHolidayMaintenance.effectiveDate}">
             		<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
            	</h:outputText>
           	</rich:column>
           	
           	<rich:column id="expirationDate" styleClass="column-left">
            	<f:facet name="header">
             	<a4j:commandLink id="expirationDate-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['expirationDate']}"
                              value="Expiration Date"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:outputText value="#{taxHolidayMaintenance.expirationDate}">
             		<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
            	</h:outputText>
            	
           	</rich:column>
           	
           	<rich:column id="executeFlag" styleClass="column-center">
            	<f:facet name="header">
             	<a4j:commandLink id="executeFlag-a4j"
                              styleClass="sort-#{taxHolidayBean.taxHolidayDataModel.sortOrder['executeFlag']}"
                              value="Executed?"
                              actionListener="#{taxHolidayBean.sortAction}"
                              oncomplete="initScrollingTables();"
                              immediate="true"
                              reRender="batchTable" />
            	</f:facet>
            	<h:selectBooleanCheckbox styleClass="check"
                                     value="#{taxHolidayMaintenance.executeFlag == 1}"
                                     disabled="#{true}"
                                     id="executeFlagId" />
           	</rich:column>
          </rich:dataTable>
         </div>
         <!-- scroll-inner -->
        </div>
        <!-- scroll-container -->
       </div>
       <!-- table-four-content -->
       <rich:datascroller id="trScroll"
                          for="batchTable"
                          maxPages="10"
                          oncomplete="initScrollingTables();"
                          style="clear:both;"
                          align="center"
                          stepControls="auto"
                          ajaxSingle="false"
                          reRender="pageInfo"
                          page="#{taxHolidayBean.taxHolidayDataModel.curPage}" />
       <div id="table-four-bottom">
        <ul class="right">
         	<li class="add">
          		<h:commandLink id="addTaxHoliday" immediate="true" action="#{taxHolidayBean.addAction}" 
          					   disabled = "#{taxHolidayBean.currentUser.viewOnlyBooleanFlag}"
          					   style="#{(taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" />
         	</li>
			<li class="copy-add">
				<h:commandLink id="copyAddTaxHoliday" 
							   style="#{(taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
							   disabled="#{!taxHolidayBean.displayButtons or taxHolidayBean.currentUser.viewOnlyBooleanFlag}" immediate="true"	action="#{taxHolidayBean.copyaddAction}" />
			</li>         
         
         	<li class="update">
          		<h:commandLink id="updateTaxHoliday" 
          		 			   style="#{(taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
          					   disabled="#{!taxHolidayBean.displayButtons or taxHolidayBean.isExecuted or taxHolidayBean.currentUser.viewOnlyBooleanFlag}" immediate="true" action="#{taxHolidayBean.updateAction}" />
         	</li>
         	
         	<li class="delete115">
          		<h:commandLink id="deleteTaxHoliday" 
          					   style="#{(taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
          				       disabled="#{!taxHolidayBean.displayButtons or taxHolidayBean.isExecuted or taxHolidayBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{taxHolidayBean.deleteAction}" />
         	</li>
         	
         	<li class="view">
          		<h:commandLink id="viewTaxHoliday" 
          					   disabled="#{!taxHolidayBean.displayButtons or taxHolidayBean.isExecuted}" immediate="true" action="#{taxHolidayBean.viewAction}" />
         	</li>
         	
         	<li class="map">
          		<h:commandLink id="mapTaxHoliday" 
          					   style="#{(taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
          					   disabled="#{!taxHolidayBean.displayButtons or taxHolidayBean.isExecuted  or taxHolidayBean.currentUser.viewOnlyBooleanFlag}" immediate="true"  action="#{taxHolidayBean.mapAction}" />
         	</li>
         	
         	<li class="execute4">
				<a4j:commandLink id="executeTaxHoliday"
 					style="#{!taxHolidayBean.displayExecuteButton? 'display:none;':'display:block;' or (taxHolidayBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
					oncomplete="javascript:Richfaces.showModalPanel('execute');" 
					reRender="executeForm"/>
			</li>
			
        </ul>
       </div> <!-- table-four-bottom -->
      </div>  <!-- table-four -->
     </div>   <!-- bottom -->
    </h:form>
   </f:view>
   
   
   <rich:modalPanel id="execute" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Execute" />
	</f:facet>
	<h:form id="executeForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok" 
                             style="cursor:pointer"
						             onclick="Richfaces.hideModalPanel('execute');" 
                         action="#{taxHolidayBean.executeAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn"
                             style="cursor:pointer"
						value="Cancel" onclick="Richfaces.hideModalPanel('execute'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>
   
   
  </ui:define>
 </ui:composition>
</html>
