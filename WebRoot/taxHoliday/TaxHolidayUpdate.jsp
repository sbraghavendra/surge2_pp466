<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxHolidayUpdateForm:aTaxCodeDetailList', 'aTaxCodeDetailListRowIndex'); } );

function resetState()
{ 
    document.getElementById("taxHolidayUpdateForm:stateMenuMain").value = "";
}
//]]>
</script>
</ui:define>

<ui:define name="body">
	<f:view>
	<h:inputHidden id="aTaxCodeDetailListRowIndex" value="#{taxHolidayBean.selectedTaxCodeDetailRowIndex}"/>

	<h:form id="taxHolidayUpdateForm">

	<h1><img id="imgTaxabilityMatrix" alt="Tax Holidays" src="../images/headers/hdr-taxholiday-maintenance.gif" width="250" height="19" /></h1>
	<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="8"><h:outputText value="#{taxHolidayBean.actionText}"/> a Tax Holiday</td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="6">Tax Holiday</th>
		  <th style="color:red;text-align:right;padding-right: 60px;"><h:outputText value="Copy Map?:" rendered="#{taxHolidayBean.actionText =='Copy/Add'}"></h:outputText>
				<h:selectBooleanCheckbox id="chkCopyMap" value="#{taxHolidayBean.updateTaxHoliday.copyMapFlag}"
					 rendered="#{taxHolidayBean.actionText =='Copy/Add'}" styleClass="check"/>
          </th>
		</tr>
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Holiday Code:</td>
          <td style="width: 220px;">
           	<h:inputText  id="updateTaxHolidayCode"
                          disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayAddActionAccepted or taxHolidayBean.displayCopyAddActionAccepted or taxHolidayBean.displayUpdateAction or taxHolidayBean.displayDeleteAction}" 
                          onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"                          
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.taxHolidayCode}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Country:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="countryMenuMain" 
							disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayAddActionAccepted or taxHolidayBean.displayCopyAddActionAccepted or taxHolidayBean.displayUpdateAction or taxHolidayBean.displayDeleteAction}" 
							value="#{taxHolidayBean.updateTaxHoliday.taxcodeCountryCode}"
							immediate="true"
							valueChangeListener="#{taxHolidayBean.updateCountryChanged}" 
							onchange="resetState();submit();"
							>
							<f:selectItems value="#{taxHolidayBean.updateCountryMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">State:</td>
          <td style="width: 220px;">
				<h:selectOneMenu id="stateMenuMain"
							disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayAddActionAccepted or taxHolidayBean.displayCopyAddActionAccepted or taxHolidayBean.displayUpdateAction or taxHolidayBean.displayDeleteAction}" 
							value="#{taxHolidayBean.updateTaxHoliday.taxcodeStateCode}">
							<f:selectItems value="#{taxHolidayBean.updateStateMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Description:</td>
          <td colspan="5">
           	<h:inputText style="width: 660px;"
                          id="updateDescription"
                          disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.description}" 
                          maxlength="50"/>
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Notes:</td>
          <td colspan="5">
           	<h:inputText style="width: 660px;"
                          id="updateNotes"
                          disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.notes}"
                          maxlength="255" />
          </td>
          <td>&#160;</td>
         </tr>
        
		

		<tr>
			<th colspan="8">Rule</th>
		</tr>
		
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Effective Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="updateEffectiveDate"
                          disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{taxHolidayBean.updateTaxHoliday.effectiveDate}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Expiration Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="updateExpirationDate"
                          disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{taxHolidayBean.updateTaxHoliday.expirationDate}" />
          </td>
          <td>&#160;</td>
         </tr>
		
	
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Taxability:</td>
          <td style="width: 220px;">           	
			<h:selectOneMenu id="taxabilityMenuItems" 
					disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}" 
					value="#{taxHolidayBean.updateTaxHoliday.taxcodeTypeCode}">
				<f:selectItems value="#{taxHolidayBean.taxabilityMenuItems}"/>
			</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Taxable Threshold Amount:</td>
          <td style="width: 220px;">
           	<h:inputText  id="taxableThresholdAmt" onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.taxableThresholdAmt}">
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Override Tax Type:</td>
          <td style="width: 220px;">           	
			<h:selectOneMenu id="overrideTaxtypeCode" 
					disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}" 
					value="#{taxHolidayBean.updateTaxHoliday.overrideTaxtypeCode}">
				<f:selectItems value="#{taxHolidayBean.taxtypeMenuItems}"/>
			</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Taxable Limitation Amount:</td>
          <td style="width: 220px;">
           	<h:inputText  id="taxableLimitationAmt" onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.taxableLimitationAmt}" >
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Rate Type:</td>
          <td style="width: 220px;">           	
			<h:selectOneMenu id="ratetypeCode" 
					disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}" 
					value="#{taxHolidayBean.updateTaxHoliday.ratetypeCode}">
				<f:selectItems value="#{taxHolidayBean.ratetypeMenuItems}"/>
			</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Tax Limitation Amount:</td>
          <td style="width: 220px;">
           	<h:inputText  id="taxLimitationAmt" onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.taxLimitationAmt}" >
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Cap Amount:</td>
          <td style="width: 220px;">
           	<h:inputText  id="capAmt" onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.capAmt}" >
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Percent of Base:</td>
          <td style="width: 220px;">
           	<h:inputText  id="baseChangePct" onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.baseChangePct}" >
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Special Rate:</td>
          <td style="width: 220px;">
           	<h:inputText  id="specialRate"  onkeypress="return onlyBusinessNumerics(event);"
                    disabled="#{taxHolidayBean.displayViewAction or taxHolidayBean.displayDeleteAction}"                           
                    value="#{taxHolidayBean.updateTaxHoliday.specialRate}" >
                    <f:convertNumber minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}" />
            </h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
        
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Last Update User ID:</td>
          <td colspan="5">
           	<h:inputText style="width: 660px;"  id="updateUserId"  disabled="true" rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.updateUserId}" />
          </td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 200px;">Last Update Timestamp:</td>
          <td colspan="5">
           	<h:inputText style="width: 660px;"  id="updateTimestamp"  disabled="true" rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.updateTimestamp}" >
                          <f:converter converterId="dateTime"/>
			</h:inputText>
          </td>
          <td>&#160;</td>
        </tr>
        
		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="okBtn" action="#{taxHolidayBean.okAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" rendered="#{!taxHolidayBean.isViewAction}" action="#{taxHolidayBean.cancelAction}" /></li>
	</ul>
	</div>
	
	
	</div>
	</div>

</h:form>
</f:view>	
</ui:define>
</ui:composition>
</html>
