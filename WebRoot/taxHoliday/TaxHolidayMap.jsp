<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxHolidayUpdateForm:aTaxCodeDetailList', 'aTaxCodeDetailListRowIndex'); } );

function resetState()
{ 
    document.getElementById("taxHolidayUpdateForm:stateMenuMain").value = "";
}
//]]>
</script>
</ui:define>

<ui:define name="body">
	<f:view>
	<h:inputHidden id="aTaxCodeDetailListRowIndex" value="#{taxHolidayBean.selectedTaxCodeDetailRowIndex}"/>

	<h:form id="taxHolidayUpdateForm">

	<h1><img id="imgTaxabilityMatrix" alt="Tax Holidays" src="../images/headers/hdr-taxholiday-maintenance.gif" width="250" height="19" /></h1>
	<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<thead>
		<tr><td style="align:left;" colspan="10"><h:outputText value="Map a Tax Holiday" /></td></tr>
	</thead>
	<tbody>
		<tr>
		  <th colspan="10">Tax Holiday</th>
		</tr>
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Holiday Code:</td>
          <td style="width: 220px;">
           	<h:inputText  id="updateTaxHolidayCode"
                          disabled="true" 
                          onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"                          
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.taxHolidayCode}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Country:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu id="countryMenuMain" 
							disabled="true" 
							value="#{taxHolidayBean.updateTaxHoliday.taxcodeCountryCode}"
							immediate="true"
							valueChangeListener="#{taxHolidayBean.updateCountryChanged}" 
							onchange="resetState();submit();"
							>
							<f:selectItems value="#{taxHolidayBean.updateCountryMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">State:</td>
          <td style="width: 220px;">
				<h:selectOneMenu id="stateMenuMain"
							disabled="true" 
							value="#{taxHolidayBean.updateTaxHoliday.taxcodeStateCode}">
							<f:selectItems value="#{taxHolidayBean.updateStateMenuItems}"/>
				</h:selectOneMenu>
          </td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Description:</td>
          <td colspan="7">
           	<h:inputText style="width: 660px;"
                          id="updateDescription"
                          disabled="true"
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.description}"
                          maxlength="50" />
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Notes:</td>
          <td colspan="7">
           	<h:inputText style="width: 660px;"
                          id="updateNotes"
                          disabled="true"
                          rendered="true"
                          value="#{taxHolidayBean.updateTaxHoliday.notes}" />
          </td>
          <td>&#160;</td>
         </tr>
         
         <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Effective Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="updateEffectiveDate"
                          disabled="true"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{taxHolidayBean.updateTaxHoliday.effectiveDate}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td style="width: 90px;">Expiration Date:</td>
          <td style="width: 220px;">
           	<rich:calendar
                          id="updateExpirationDate"
                          disabled="true"
                          rendered="true"
                          oninputkeypress="return onlyDateValue();"
                          enableManualInput="true"
                          converter="date"
                          datePattern="M/d/yyyy"
                          value="#{taxHolidayBean.updateTaxHoliday.expirationDate}" />
          </td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
         </tr>
		
	</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="10">TaxCodes/Jurisdictions</th>
		</tr>
		<tr>
			<td style="width: 50px;">&#160;</td>
			<td colspan="9" style = "font-weight:bold">Selection Filter</td>
		</tr>
		
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">TaxCode:</td>
          <td style="width: 220px;">
           	<h:inputText  id="filterTaxCodeCode" 
                          disabled="#{taxHolidayBean.displayAddActionNotAccepted or taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}"                          
                          rendered="true"
                          value="#{taxHolidayBean.filterTaxCodeCode}" />
          </td>
          <td>&#160;</td>
          
          <td style="width: 50px;">&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
          <td>&#160;</td>
        </tr>
        
        <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">Description:</td>
          <td colspan="8">
           	<h:inputText style="width: 660px;"
                          id="filterTaxCodeDescription" 
                          disabled="#{taxHolidayBean.displayAddActionNotAccepted or taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}" 
                          rendered="true"
                          value="#{taxHolidayBean.filterTaxCodeDescription}" />
          </td>
  
        </tr>
         
      <tr>
          <td style="width: 50px;">&#160;</td>
          <td style="width: 70px;">County:</td>
          <td style="width: 220px;">           	
				<h:selectOneMenu 	id="countyMenuItems" 
									disabled="#{taxHolidayBean.displayAddActionNotAccepted or taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}" 
									value="#{taxHolidayBean.filterTaxCodeDetailCountry}">
							<f:selectItems value="#{taxHolidayBean.filterCountyMenuItems}"/>
				</h:selectOneMenu>
          </td>
          
          
          <td style="width: 10px;" colspan = "2">&#160;</td>
          <td style="width: 27px;">City:</td>
          <td style="width: 220px;">
				<h:selectOneMenu 	id="cityMenuItems" 
									disabled="#{taxHolidayBean.displayAddActionNotAccepted or taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}" 
									value="#{taxHolidayBean.filterTaxCodeDetailCity}">
							<f:selectItems value="#{taxHolidayBean.filterCityMenuItems}"/>
				</h:selectOneMenu>
          </td>
        
          <td style="width: 28px;" colspan = "2">STJ:</td>
          <td style="width: 220px;">
				<h:selectOneMenu 	id="stjMenuItems" 
									disabled="#{taxHolidayBean.displayAddActionNotAccepted or taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}" 
									value="#{taxHolidayBean.filterTaxCodeDetailStj}">
							<f:selectItems value="#{taxHolidayBean.filterStjMenuItems}"/>
				</h:selectOneMenu>
          </td> 
   
        </tr> 

		</tbody>
	</table>	
	</div> <!-- table-one-content -->
	
	</div>
	</div>

	<div id="bottom">
	<div id="table-four">
<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
	<div id="table-four-content">

	<h:panelGroup id="pgTableDisplay" rendered="true">
	
	<div id="table-one-bottom">
	<a4j:status id="pageInfo11" 
					startText="Request being processed..." 
				 	stopText="#{taxHolidayBean.pageTaxCodeDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     <ul class="right">
      	<li class="clear"><h:commandLink id="taxcodeClearBtn"  action="#{taxHolidayBean.resetTaxCodeSearchAction}" /></li>
      	<li class="search"><h:commandLink id="searchTaxCodeBtn" action="#{taxHolidayBean.retrieveTaxHolidayDetailByTaxCode}" /></li>
      	<li class="viewselected"><h:commandLink id="taxcodeViewselectedBtn" action="#{taxHolidayBean.viewSelectedTaxCodeAction}" /></li>
     </ul>
    </div>
    <!-- table-one-bottom -->
	
	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
	<rich:dataTable rowClasses="odd-row,even-row" id="aTaxCodeDetailList" 
		value="#{taxHolidayBean.listTaxHolidayDetailByTaxCode}" var="taxHolidayDetail" >

		<a4j:support id="rowTaxCodeClick" event="onRowClick" status="rowstatus" 
			
			
			onsubmit="selectRow('taxHolidayUpdateForm:aTaxCodeDetailList', this);"
			actionListener="#{taxHolidayBean.selectedTaxCodeDetailChanged}"
			reRender="taxHolidayUpdateForm:exceptionsBtn" />

			<rich:column style="text-align: center" >
				<f:facet name="header">
					<h:outputText value="Select" />
				</f:facet>
				<h:selectBooleanCheckbox styleClass="check" id="chkTaxCodeSelected" disabled="#{taxHolidayBean.displayCopyAddActionNotAccepted or taxHolidayBean.displayDeleteAction}" 
									value="#{taxHolidayDetail.selected}" >
								<a4j:support event="onclick" actionListener="#{taxHolidayBean.mapCheckboxSelected}"/>
				</h:selectBooleanCheckbox>
				
			</rich:column>
			
		<rich:column id="taxcodeCodeId" sortBy="#{taxHolidayDetail.taxcodeCode}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="TaxCode" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.taxcodeCode}" />
		</rich:column>
		
		<rich:column id="taxcodeCountyId" sortBy="#{taxHolidayDetail.taxcodeCounty}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="County" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.taxcodeCounty}" />
		</rich:column>
		
		<rich:column id="taxcodeCityId" sortBy="#{taxHolidayDetail.taxcodeCity}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="City" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.taxcodeCity}" />
		</rich:column>
		
		<rich:column id="taxcodeStjId" sortBy="#{taxHolidayDetail.taxcodeStj}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="STJ" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.taxcodeStj}" />
		</rich:column>
		
		<rich:column id="jurLevelId" sortBy="#{taxHolidayDetail.jurLevel}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="Jur Level" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.jurLevel}" />
		</rich:column>
		
		<rich:column id="descriptionId" sortBy="#{taxHolidayDetail.description}" >
			<f:facet name="header">
				<h:outputText styleClass="headerText" value="Description" />
			</f:facet>
			<h:outputText value="#{taxHolidayDetail.description}" />
		</rich:column>

	</rich:dataTable>
	</div>
	</div>
	
	<div id="table-one-bottom">
     <ul class="right">
      	<li class="clear-all"><h:commandLink id="taxcodeSelectNoneBtn"  action="#{taxHolidayBean.selectedTaxCodeNoneAction}" /></li>
      	<li class="select-all"><h:commandLink id="taxcodeSelectAllBtn"  action="#{taxHolidayBean.selectedTaxCodeAllAction}" reRender="" /></li>
     </ul>
    </div>
    <!-- table-one-bottom -->
	
	<table cellpadding="0" cellspacing="0"  width="100%" id="rollover" class="ruler">
	<tbody>	
		<tr>
        	<td style="width: 50px;">&#160;</td>
        	<td style="width: 120px;">Last Update User ID:</td>
        	<td style="width: 220px;">
         		<h:inputText  popup="true" id="updateUserId"  rendered="true" disabled="true"
         				value="#{taxHolidayBean.updateTaxHoliday.updateUserId}" />
        	</td>
       
        	<td style="width: 10px;">&#160;</td>
        	<td style="width: 120px;">Last Update Timestamp:</td>
        	<td style="width: 220px;">
         		<h:inputText  popup="true" id="updateTimestamp"  rendered="true" 
         				disabled="true"
         				value="#{taxHolidayBean.updateTaxHoliday.updateTimestamp}" >
         				<f:converter converterId="dateTime"/>
				</h:inputText>		
        	</td>
        	<td style="width: 400px;">&#160;</td>
       	</tr>
	</tbody>
	</table>

	</h:panelGroup>
	</div>
	
	<!-- <div id="table-four-bottom"> change to <div id="table-one-bottom"> to make blue color-->
	<div id="table-one-bottom">
	<ul class="right">
		<li class="exceptions"><h:commandLink id="exceptionsBtn" disabled="#{!taxHolidayBean.displayExceptionAction}"  action="#{taxHolidayBean.exceptionsAction}"/></li>
		<li class="ok"><h:commandLink id="okBtn"  action="#{taxHolidayBean.okMapAction}"/></li>
		<li class="cancel"><h:commandLink id="cancelBtn" immediate="true" action="#{taxHolidayBean.cancelAction}" /></li>
	</ul>
	</div>
	</div>
	</div>
	
	<rich:modalPanel id="loader" zindex="2000" autosized="true"><h:outputText value="Processing Search ..."/></rich:modalPanel>
	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
</h:form>
</f:view>	
</ui:define>
</ui:composition>
</html>
