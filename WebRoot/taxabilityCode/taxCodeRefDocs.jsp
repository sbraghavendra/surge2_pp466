<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('detailForm:refDocsTable', 'selectedRefDocDetailIndex'); } );
	registerEvent(window, "unload", function() { document.getElementById('detailForm:hiddenChangeLink').onclick(); });
	registerEvent(window, "load", function popup () {
		if(document.getElementById('url').value != null && document.getElementById('url').value != ""){
			lo_window = window.open (document.getElementById('url').value, "popup");
			lo_window.focus();
			document.getElementById('url').value = null;
		}
	});
//]]>
</script>
</ui:define>


<ui:define name="body">
<h:inputHidden id="selectedRefDocDetailIndex" value="#{taxCodeBackingBean.selectedRefDocDetailIndex}"/>
<h:inputHidden id="url" value="#{taxCodeBackingBean.url}" />
<h:form id="detailForm">
<a4j:commandLink id="hiddenChangeLink" style="visibility:hidden;display:none" >
	<a4j:support event="onclick" immediate="true" actionListener="#{taxCodeBackingBean.urlResetAction}" />
</a4j:commandLink>
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-bottom">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<c:if test="#{!taxCodeBackingBean.viewRuleOnly}">
					<tr><td colspan="3">Reference Documents</td></tr>
			</c:if>
			<c:if test="#{taxCodeBackingBean.viewRuleOnly}">
					<tr><td colspan="3">View Reference Documents</td></tr>
			</c:if>	
		</thead>
		<tbody>
			<tr>
				<th colspan="3">&#160;TaxCode</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">TaxCode:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode}" 
						required="true" label="TaxCode" id="txtTaxcodeCode"
						validator="#{taxCodeBackingBean.validateTaxCode}"
					 	 disabled="true"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="255" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.description}" 
						required="true" label="Description" id="txtDescription"
					 	 disabled="true"
				         style="width:650px;" 
				         maxlength="255"/>
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="5">&#160;Jurisdiction</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Country:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCountryName} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeCountryCode})" 
					 	 disabled="true"
				         id="txtCountry"
				         maxlength="50"/>
                </td>
                <td>State:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.name} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeStateCode})" 
					 	 disabled="true"
				         id="txtState"
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">County:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCounty}" 
					 	 disabled="true"
				         id="txtCounty"
				         maxlength="50"/>
                </td>
                <td>City:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCity}" 
					 	 disabled="true"
				         id="txtCity"
				         maxlength="50"/>
                </td>         
			</tr>
			<tr>
                <td style="width:50px;">&#160;</td>
				<td class="column-label" >STJ:</td>
                <td colspan="3" >
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeStj}" style="width:650px;" 
					 	 disabled="true"
				         id="txtStj"
				         maxlength="300"/>
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="7">&#160;Rule</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td colspan="5">
					<h:inputText id="effectiveDate" value="#{taxCodeBackingBean.selectedTaxCodeRule.effectiveDate}" disabled="true">
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:inputText>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:inputText id="taxcodeTypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxcodeTypeDesc}" disabled="true" />
                </td>
                <td>Tax Type:</td>
				<td>
					<h:inputText id="taxtypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxtypeDesc}" disabled="true" />
                </td>
                <td>Rate Type:</td>
				<td>
					<h:inputText id="rateTypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.rateTypeDesc}" disabled="true" />
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="7">&#160;Reference Documents</th>
			</tr>
		</tbody>
	</table>
	</div>
	
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner">
				<rich:dataTable rowClasses="odd-row,even-row" id="refDocsTable" styleClass="GridContent" value="#{taxCodeBackingBean.refDocList}" var="item">
					<a4j:support event="onRowClick"
						onsubmit="selectRow('detailForm:refDocsTable', this);"
						immediate="true"
						actionListener="#{taxCodeBackingBean.selectedRefDocChanged}"
						reRender="btnDelete,btnView"
						oncomplete="initScrollingTables();"
					/>
				
					<rich:column sortBy="#{item.referenceDetailPK.refDoc.refDocCode}">
						<f:facet name="header"><h:outputText value="Ref. Doc. Code" style = "color:#336690"/></f:facet>
						<h:outputText id="refDocCode" value="#{item.referenceDetailPK.refDoc.refDocCode}" />
					</rich:column>
					<rich:column sortBy="#{item.referenceDetailPK.refDoc.refTypeCode}">
						<f:facet name="header"><h:outputText value="Ref. Type" style = "color:#336690"/></f:facet>
						<h:outputText id="refTypeCode" value="#{taxCodeBackingBean.refTypeCodeDescMap[item.referenceDetailPK.refDoc.refTypeCode]}" />
					</rich:column>
					<rich:column sortBy="#{item.referenceDetailPK.refDoc.description}">
						<f:facet name="header"><h:outputText value="Description" style = "color:#336690"/></f:facet>
						<h:outputText id="description" value="#{item.referenceDetailPK.refDoc.description}" />
					</rich:column>
					<rich:column sortBy="#{item.referenceDetailPK.refDoc.docName}">
						<f:facet name="header"><h:outputText value="Document Name" style = "color:#336690"/></f:facet>
						<h:outputText id="docName" value="#{item.referenceDetailPK.refDoc.docName}" />
					</rich:column>
					<rich:column sortBy="#{item.referenceDetailPK.refDoc.url}">
						<f:facet name="header"><h:outputText value="URL" style = "color:#336690"/></f:facet>
						<h:outputText id="url" value="#{item.referenceDetailPK.refDoc.url}" />
					</rich:column>
				</rich:dataTable>
			</div>
		</div>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
	    <li class="addNew"><h:commandLink id="btnAddNew" action="#{taxCodeBackingBean.refDocAddNewAction}" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" /></li>
		<li class="addExisting"><h:commandLink id="btnAdd" action="#{taxCodeBackingBean.refDocAddAction}" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" /></li>
		<li class="delete"><h:commandLink id="btnDelete" immediate="true" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.refDocSelected or taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{taxCodeBackingBean.refDocDeleteAction}" /></li>
		<li class="view">
		  <h:commandLink id="btnView" immediate="true" disabled="#{!taxCodeBackingBean.refDocSelected}" action="#{taxCodeBackingBean.displayViewAction}" >
		    <f:param name="fromView" value = "refDocAction" />
		  </h:commandLink>
	    </li>
		<li class="close"><h:commandLink id="btnClose" action="#{taxCodeBackingBean.taxCodeRulesViewAction}" /></li>
	</ul>
	</div>
	
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>