<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="States Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="StateMaint" value="#{supportingCodeBackingBean.actionText}"/> a State</td></tr>
		</thead>
		<tbody>
		<!-- 
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Country:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.country}" 
						required="true" label="Country" id="txtCountry"
					 	 disabled="#{supportingCodeBackingBean.deleteAction}"
				         style="width:650px;" 
				         maxlength="50"/>
                 </td>
			</tr> -->
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Country:</td>
				<td>		
					<h:selectOneMenu id="SelCountry" title="Select a Country"
						style="width: 288px;" 
						value="#{supportingCodeBackingBean.selectedCountryComboValue}"
						disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction or supportingCodeBackingBean.updateAction}"
						>
					<f:selectItems id="selCountryItms" value="#{supportingCodeBackingBean.countryItems}" />
				</h:selectOneMenu>                  
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>State Code:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.taxCodeState}" 
						 label="State Code" id="txtTaxcodeStateCode"
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction or supportingCodeBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="10" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.name}" 
						required="true" label="Name" id="txtName"
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
				         style="width:650px;" 
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>GeoCode:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.geoCode}" 
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
				         style="width:650px;" id="txtGeocode"
				         maxlength="12"/>
                 </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>State Taxability:</td>
				<td>
                 	 <h:selectBooleanCheckbox styleClass="check" id="stateTaxabilityBooleanFlag" 
                 	 	 value="#{supportingCodeBackingBean.selectedState.stateTaxabilityBooleanFlag}" 
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}" >
                      </h:selectBooleanCheckbox>
                 </td>
			</tr>	
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Local Taxability:</td>
				<td>
					<h:selectOneMenu id="SelLocalTax" title="Select a Local Taxability"
						style="width: 288px;" 
						value="#{supportingCodeBackingBean.selectedLocalTaxabilityValue}"
						disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
						>
						<f:selectItems id="selLocalTaxItms" value="#{supportingCodeBackingBean.localTaxabilityItems}" />
					</h:selectOneMenu>
                 </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Certificate Form ID:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.cpFormId}" 
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
				         style="width:650px;" id="txtCpFormId"
				         maxlength="12"/>
                 </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Certificate Level:</td>
				<td>
					<h:selectOneMenu id="SelCertLevel" title="Select Certificate Level"
						style="width: 288px;" 
						value="#{supportingCodeBackingBean.selectedCertificateLevelValue}"
						disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
						>
						<f:selectItems id="selCertLevelItms" value="#{supportingCodeBackingBean.certificateLevelItems}" />
					</h:selectOneMenu>
                 </td>
			</tr>
			
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Active:</td>
				<td>
                 	 <h:selectBooleanCheckbox styleClass="check" id="chkActiveBooleanFlag" 
                 	 	 value="#{supportingCodeBackingBean.selectedState.activeBooleanFlag}" 
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}" >
                      </h:selectBooleanCheckbox>
                 </td>
			</tr>					
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedState.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{supportingCodeBackingBean.okStateAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{supportingCodeBackingBean.viewAction}" immediate="true" action="#{supportingCodeBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>