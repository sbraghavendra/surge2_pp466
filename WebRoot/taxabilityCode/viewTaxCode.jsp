<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('viewTaxCodeForm:docrefTable', 'selectedDocRefIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedDocRefIndex" value="#{viewTaxCodeBean.selectedDocRefIndex}"/>


<h:form id="viewTaxCodeForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="View TaxCode Related Information" value="/images/headers/hdr-taxcode-info.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">

	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6"><h:outputText value="TaxCode"/></td></tr>
		</thead>
		<tbody>	
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">Details: </td>	
				<td colspan="4"><h:outputText id="displaymessageId" value="#{empty viewTaxCodeBean.taxCodeDetail ? '* Matching TaxCode not found' : '&#160;'}" style="color:red; font-weight: bold;" /></td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="TaxCode Country: "/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeCountry" value="#{viewTaxCodeBean.taxcodeCountry}" disabled="true"/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeCountryName" value="#{cacheManager.countryMap[viewTaxCodeBean.taxcodeCountry]}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="TaxCode State: "/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeState" value="#{viewTaxCodeBean.taxcodeState}" disabled="true"/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeStateName" value="#{cacheManager.taxCodeStateMap[viewTaxCodeBean.taxcodeState].name}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="TaxCode Type: "/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeType" value="#{viewTaxCodeBean.taxcodeType}" disabled="true"/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeTypeDescription" value="#{cacheManager.listCodeMap['TCTYPE'][viewTaxCodeBean.taxcodeType].description}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
				
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="TaxCode Code: "/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeCode" value="#{viewTaxCodeBean.taxcodeCode}" disabled="true"/></td>
				<td class="column-description"><h:inputText id="txtTaxcodeCodeDescription" value="#{cacheManager.taxCodeMap[viewTaxCodeBean.taxcodeCode].description}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="Description: " /></td>
				<td colspan="3"><h:inputText id="txtDescription" style="width:692px;" value="#{!empty viewTaxCodeBean.taxCode ? viewTaxCodeBean.taxCode.description : '&#160;'}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="Tax Type: "/></td>
				<td class="column-description"><h:inputText id="txtTaxType" value="#{!empty viewTaxCodeBean.taxCodeDetail ? cacheManager.listCodeMap['TAXTYPE'][viewTaxCodeBean.taxCodeDetail.taxtypeCode].description : '&#160;'}" disabled="true"/></td>
				<td class="column-label"><h:outputText value="ERP TaxCode: "/></td>
				<td class="column-description"><h:inputText id="txtErpTaxcode" value="#{!empty viewTaxCodeBean.taxCodeDetail ? viewTaxCodeBean.taxCodeDetail.erpTaxcode : '&#160;'}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="Measure Type: "/></td>
				<td class="column-description"><h:inputText id="txtMeasureType" value="#{!empty viewTaxCodeBean.taxCodeDetail ? cacheManager.listCodeMap['MEASURETYP'][viewTaxCodeBean.taxCode.measureTypeCode].description : '&#160;'}" disabled="true"/></td>
				<td class="column-label"><h:outputText value="% of Dist. Amt.: "/></td>
				<td class="column-description"><h:inputText id="txtPctDistAmt" value="#{!empty viewTaxCodeBean.taxCodeDetail ? viewTaxCodeBean.taxCode.pctDistAmt : '&#160;'}" disabled="true">
						<f:convertNumber pattern="#0.00"/>	
					</h:inputText></td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="Comments: "/></td>
				<td colspan="3"><h:inputText id="txtComments" style="width:692px;" value="#{!empty viewTaxCodeBean.taxCode ? viewTaxCodeBean.taxCode.comments : '&#160;'}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label"><h:outputText value="Help Text: "/></td>
				<td colspan="3"><h:inputTextarea id="txtHelpText" style="width:692px;" value="#{!empty viewTaxCodeBean.taxCode ? viewTaxCodeBean.taxCode.helptext : '&#160;'}" disabled="true"/></td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="10"><h:outputText value="Override Jurisdiction"/></td></tr>
		</thead>
		<tbody>
			<tr>
				<td class="column-spacer">&#160;</td>
				<td class="column-label">&#160;Jurisdiction: </td>
				<td colspan="5">
				<ui:include src="/WEB-INF/view/components/jurisdiction.xhtml">
					<ui:param name="handler" value="#{viewTaxCodeBean.currentJurisdictionHandler}"/>
					<ui:param name="id" value="jurisOutput"/>
					<ui:param name="readonly" value="true"/>
					<ui:param name="required" value="true"/>
					<ui:param name="showheaders" value="true"/>
					<ui:param name="popupName" value=""/>
					<ui:param name="popupForm" value=""/>
				</ui:include>
				</td>
				<td class="column-spacer">&#160;</td>
			</tr>
		</tbody>
	</table>
	
	</div>
	<div id="table-one-bottom">
	<ul class="right"></ul></div>
	</div>
</div>

<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">

		<div class="scrollContainer">
		<div class="scrollInner">
		
		  <ui:include src="/WEB-INF/view/components/docref_table.xhtml">
			<ui:param name="bean" value="#{viewTaxCodeBean}"/>
			<ui:param name="data" value="#{viewTaxCodeBean.docList}"/>
			<ui:param name="rendered" value="true"/>
			<ui:param name="formName" value="taxabilityForm"/>
			<ui:param name="id" value="docrefTable"/>
			<ui:param name="reRender" value="viewBtn"/>
			</ui:include>
		
		</div>
		</div>

	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="view"><h:commandLink id="viewBtn" disabled="#{!viewTaxCodeBean.viewEnabled}" action="#{viewTaxCodeBean.displayViewAction}" /></li>
		<li class="ok2"><h:commandLink id="ok2Btn" immediate="true" action="#{viewTaxCodeBean.cancelAction}" /></li>
	</ul>

	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>
