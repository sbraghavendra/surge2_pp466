<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:stateTable', 'selectedStateIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:docrefTable', 'selectedDocRefIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:cchTaxTable', 'selectedCCHCodeIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:cchGroupTable', 'selectedCCHGroupIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:cchGroupItemsTable', 'selectedCCHGroupItemIndex'); } );
registerEvent(window, "unload", function() { document.getElementById('taxabilityForm:hiddenChangeLink').onclick(); });

registerEvent(window, "load", function popup () {	
	if(document.getElementById('url').value != null ){
		lo_window = window.open (document.getElementById('url').value, "popup");
		lo_window.focus();
		document.getElementById('url').value = null;
	}
}
);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedStateIndex" value="#{supportingCodeBackingBean.selectedStateIndex}"/>
<h:inputHidden id="selectedDocRefIndex" value="#{supportingCodeBackingBean.selectedDocRefIndex}"/>
<h:inputHidden id="selectedCCHCodeIndex" value="#{supportingCodeBackingBean.selectedCCHCodeIndex}"/>
<h:inputHidden id="selectedCCHGroupIndex" value="#{supportingCodeBackingBean.selectedCCHGroupIndex}"/>
<h:inputHidden id="selectedCCHGroupItemIndex" value="#{supportingCodeBackingBean.selectedCCHGroupItemIndex}"/>

<h:inputHidden id="url" value="#{supportingCodeBackingBean.url}" rendered="#{supportingCodeBackingBean.viewEnabled}"/>
<h:form id="taxabilityForm">
	<a4j:commandLink id="hiddenChangeLink" style="visibility:hidden;display:none" >
			<a4j:support event="onclick" immediate="true" actionListener="#{supportingCodeBackingBean.urlResetAction}" />
		</a4j:commandLink>

<h1><img id="imgTaxabilityCodes" alt="Taxability Codes by Supporting Codes" src="../images/headers/hdr-taxability-codes.gif" width="250" height="19" /></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
        <tr>
            <td style="width: 10px;">&#160;</td>
			<td>
				<h:selectOneMenu id="SelView" title="Select View Type"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedView}">
					<f:selectItems id="selViewItms" value="#{supportingCodeBackingBean.viewItems}" />
					<a4j:support event="onchange" action="#{supportingCodeBackingBean.viewSubmitted}" />
				</h:selectOneMenu>
            </td>
        </tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="selFirstCombo"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedFirstComboValue}"  
					immediate="true" title="Select TaxCode" 
					valueChangeListener="#{supportingCodeBackingBean.firstComboValueChanged}" 
					onchange="submit();">
					<f:selectItem id="selFirstComboItm0" itemLabel="Select a Supporting Code" itemValue="0"/>
	<!--  <f:selectItem id="selFirstComboItm1" itemLabel="CCH Groups/Items" itemValue="1"/> 	bug 5083 -->
	<!--  <f:selectItem id="selFirstComboItm2" itemLabel="CCH Tax Categories" itemValue="2"/>	bug 5083 -->
					<f:selectItem id="selFirstComboItm3" itemLabel="Reference Documents" itemValue="3"/>
					<f:selectItem id="selFirstComboItm4" itemLabel="Reference Types" itemValue="4"/>
					<f:selectItem id="selFirstComboItm5" itemLabel="States" itemValue="5"/>
				</h:selectOneMenu>
			</td>
		</tr>
        <tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 40px;">
				<h:selectOneMenu id="selSecondCombo"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedSecondComboValue}"  
					immediate="true" title="Select Item" 
					rendered="#{supportingCodeBackingBean.cchGroup or supportingCodeBackingBean.cchGItem}" 
					valueChangeListener="#{supportingCodeBackingBean.secondComboValueChanged}" onchange="submit();">
					<f:selectItem id="selSecondComboItm" itemLabel="Select a CCH Group" itemValue="0"/>
					<f:selectItems id="selSecondComboItms" value="#{supportingCodeBackingBean.secondComboItems}" />
				</h:selectOneMenu>&#160;
			</td>
		</tr>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right"></ul></div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner">
	
	<ui:include src="/WEB-INF/view/components/state_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docStateList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.state}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="stateTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/listcode_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docRefTypeList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.ref}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="itemTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/docref_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.doc}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="docrefTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>

	<rich:dataTable rowClasses="odd-row,even-row" id="cchGroupTable" styleClass="GridContent"
		value="#{supportingCodeBackingBean.cchGroupList}" var="group" 
		rendered="#{supportingCodeBackingBean.cchGroup}">
	
		<a4j:support event="onRowClick"
				onsubmit="selectRow('taxabilityForm:cchGroupTable', this);"
				actionListener="#{supportingCodeBackingBean.selectedCCHGroupChanged}" immediate="true"
				reRender="updBtn,delBtn,viewBtn"/>
	
		<rich:column sortBy="#{group.cchTaxMatrixPK.groupCode}">
			<f:facet name="header" ><h:outputText value="Group Code" /></f:facet>
			<h:outputText value="#{group.cchTaxMatrixPK.groupCode}"/>
		</rich:column>
		
		<rich:column sortBy="#{group.groupDesc}">
			<f:facet name="header"><h:outputText value="Description"/></f:facet>
			<h:outputText value="#{group.groupDesc}" />
		</rich:column>
		
		<rich:column sortBy="#{group.effDate}">
			<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
			<h:outputText value="#{group.effDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
			</h:outputText>
		</rich:column>
	</rich:dataTable> 
	
	<rich:dataTable rowClasses="odd-row,even-row" id="cchGroupItemsTable" styleClass="GridContent"
		value="#{supportingCodeBackingBean.cchGroupItemsList}" var="grpitem" 
		rendered="#{supportingCodeBackingBean.cchGItem}">
	
		<a4j:support event="onRowClick"
				onsubmit="selectRow('taxabilityForm:cchGroupItemsTable', this);"
				actionListener="#{supportingCodeBackingBean.selectedCCHGroupItemChanged}" immediate="true"
				reRender="updBtn,delBtn,viewBtn"/>
	
		<rich:column sortBy="#{grpitem.cchTaxMatrixPK.item}">
			<f:facet name="header" ><h:outputText value="Item" /></f:facet>
			<h:outputText value="#{grpitem.cchTaxMatrixPK.item}"/>
		</rich:column>
		
		<rich:column sortBy="#{grpitem.itemDesc}">
			<f:facet name="header"><h:outputText value="Description"/></f:facet>
			<h:outputText value="#{grpitem.itemDesc}" />
		</rich:column>
		
		<rich:column sortBy="#{grpitem.effDate}">
			<f:facet name="header"><h:outputText value="Effective Date"/></f:facet>
			<h:outputText value="#{grpitem.effDate}">
				<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
			</h:outputText>
		</rich:column>
	</rich:dataTable> 
	
	<rich:dataTable rowClasses="odd-row,even-row" id="cchTaxTable" styleClass="GridContent"
		value="#{supportingCodeBackingBean.cchCodeList}" var="code" 
		rendered="#{supportingCodeBackingBean.cchTax}">
	
		<a4j:support event="onRowClick"
				onsubmit="selectRow('taxabilityForm:cchTaxTable', this);"
				actionListener="#{supportingCodeBackingBean.selectedCCHCodeChanged}" immediate="true"
				reRender="updBtn,delBtn,viewBtn"/>
	
		<rich:column sortBy="#{code.codePK.code}">
			<f:facet name="header" ><h:outputText value="Code" /></f:facet>
			<h:outputText value="#{code.codePK.code}"/>
		</rich:column>
		
		<rich:column sortBy="#{code.description}">
			<f:facet name="header"><h:outputText value="Description"/></f:facet>
			<h:outputText value="#{code.description}" />
		</rich:column>
	</rich:dataTable> 
	</div>
	</div>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="addBtn" action="#{supportingCodeBackingBean.displayAddAction}" /></li>
		<li class="update"><h:commandLink id="updBtn" disabled="#{!supportingCodeBackingBean.validSelection}" action="#{supportingCodeBackingBean.displayUpdateAction}" /></li>	
		<li class="delete"><h:commandLink id="delBtn" disabled="#{supportingCodeBackingBean.disableDeleteButton}" action="#{supportingCodeBackingBean.displayDeleteAction}" /></li>
		<li class="view"><h:commandLink id="viewBtn" disabled="#{!supportingCodeBackingBean.viewEnabled}" action="#{supportingCodeBackingBean.displayViewAction}" /></li>
	</ul>
	</div>
	</div>
</div>

</h:form>
</ui:define>

</ui:composition>

</html>
