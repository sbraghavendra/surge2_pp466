<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:itemTable', 'selectedTaxCodeIndex'); } );
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="selectedTaxCodeIndex" value="#{taxCodeBackingBean.selectedTaxCodeIndex}"/>
<h:form id="taxabilityForm">

<h:panelGroup rendered="#{taxCodeBackingBean.findMode}">
<h1><h:graphicImage id="imglookupTaxCode"  alt="Lookup TaxCodes"  url="/images/headers/hdr-lookup-taxcodes.png"></h:graphicImage></h1>
</h:panelGroup>
		
<h:panelGroup rendered="#{!taxCodeBackingBean.findMode}">	
<h1><h:graphicImage id="imgTaxCode" alt="TaxCodes" value="/images/headers/hdr-taxcodes.png" /></h1>
</h:panelGroup>

<div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
  				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{taxCodeBackingBean.togglePanelController.toggleHideSearchPanel}" >
  			<h:outputText value="#{(taxCodeBackingBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
  	</a4j:commandLink>					
</div> 

<a4j:outputPanel id="showhidefilter">
    <c:if test="#{!taxCodeBackingBean.togglePanelController.isHideSearchPanel}">
     	<div id="top">
      	<div id="table-one">
			<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
				<div id="table-one-top" style="padding: 1px 10px 1px 15px;">
					<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;"><tr>
						<td width="70"><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
								reRender="showhidefilter" actionListener="#{taxCodeBackingBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
						<td align="left"><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
				  				reRender="showhidefilter" actionListener="#{taxCodeBackingBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
						<td align="right" width="100"><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>
					</tr></table>
				</div>   
				   
				<div id="table-one-content" style="height: auto;" onkeyup="return submitEnter(event,'taxabilityForm:btnSearch')" >
				    
					<!-- Start toggle panel -->
					<div id="embedded-table">
					<h:panelGrid id="togglePanel" columns="1" width="100%"  >
				
					<rich:simpleTogglePanel id="TaxCodesInformationPanel"   switchType="ajax" label="TaxCodes Information" 
						actionListener="#{taxCodeBackingBean.togglePanelController.togglePanelChanged}" 
						opened="#{taxCodeBackingBean.togglePanelController.togglePanels['TaxCodesInformationPanel']}"  
						bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
							<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
							<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
							
						<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
							<tr>
								<td style="width:50px;">&#160;</td>
								<td style="width: 100px;">&#160;TaxCode:</td>
								<td style="padding-left: 20px;">
									<h:inputText  style="width:400px;" id="enteredTaxCode" value="#{taxCodeBackingBean.enteredTaxCode}" maxlength="255" />
								</td>
							</tr>
							<tr>
								<td style="width:50px;">&#160;</td>
								<td style="width: 100px;">&#160;Description:</td>
								<td style="padding-left: 20px;">
									<h:inputText style="width: 400px;" id="enteredDesc" value="#{taxCodeBackingBean.enteredDesc}" />
								</td>
							</tr>
							<tr>
								<td style="width:50px;">&#160;</td>
								<td style="width: 100px;">&#160;Active/Inactive:</td>
								<td style="padding-left: 20px;">
									<h:selectOneMenu id="selectedActiveInactive" value="#{taxCodeBackingBean.selectedActiveInactive}">
										<f:selectItem id="allItm" itemValue="-1" itemLabel="All" />
										<f:selectItem id="activeItm" itemValue="1" itemLabel="Active Only" />
										<f:selectItem id="inactiveItm" itemValue="0" itemLabel="Inactive Only" />
									</h:selectOneMenu>
								</td>
							</tr>
						</table>	
				    </rich:simpleTogglePanel>
				      
				    </h:panelGrid>
					</div>
				 </div>
				<!-- end of table-one-content -->       
				      
				<div id="table-one-bottom">
					<ul class="right">
						<li class="clear">
							<a4j:commandLink id="btnClear" action="#{taxCodeBackingBean.taxCodeResetFilter}" reRender="enteredTaxCode,enteredDesc,selectedActiveInactive" />
						</li>
						<li class="search">
							<a4j:commandLink id="btnSearch" action="#{taxCodeBackingBean.taxCodeSearchFilter}" reRender="pageInfo,itemTable,copyBtn,updBtn,delBtn,viewBtn,rulesBtn,okBtn"/>
						</li>
					</ul>
				</div>
			</a4j:outputPanel >
      	</div>
     	</div>
	</c:if>
</a4j:outputPanel>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
		<div id="table-four-top">
			<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
			<a4j:status id="pageInfo" 
						startText="Request being processed..." 
						stopText="#{taxCodeBackingBean.pageDescription }"
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
		</div>	
		<div id="table-four-content">
			<div class="scrollContainer">
				<div class="scrollInner" id="resize" >				
					<rich:dataTable rowClasses="odd-row,even-row" id="itemTable" styleClass="GridContent"
						value="#{taxCodeBackingBean.taxCodeList}" var="item">
					
						<a4j:support event="onRowClick"
								onsubmit="selectRow('taxabilityForm:itemTable', this);"
								actionListener="#{taxCodeBackingBean.selectedTaxCodeChanged}" immediate="true"
								reRender="copyBtn,updBtn,delBtn,viewBtn,rulesBtn,okBtn"/>
					
						<rich:column sortBy="#{item.taxCodePK.taxcodeCode}">
							<f:facet name="header"><h:outputText value="TaxCode"/></f:facet>
							<h:outputText id="rowCodecode" value="#{item.taxCodePK.taxcodeCode}"/>
						</rich:column>
						
						<rich:column sortBy="#{item.description}" width="450px">
							<f:facet name="header"><h:outputText value="Description"/></f:facet>
							<h:outputText id="rowDescription" value="#{item.description}" />
						</rich:column>
						
						<rich:column sortBy="#{item.comments}" width="300px">
							<f:facet name="header"><h:outputText value="Comments"/></f:facet>
							<h:outputText id="rowComments" value="#{item.comments}" />
						</rich:column>
						
						<rich:column sortBy="#{item.activeBooleanFlag}" style="text-align:center;">
							<f:facet name="header"><h:outputText value="Active?"/></f:facet>
							<h:selectBooleanCheckbox id="chkActive" value="#{item.activeBooleanFlag}" disabled="true" styleClass="check"/>
						</rich:column>
					</rich:dataTable>
				</div>
			</div>
		</div>
		<div id="table-four-bottom">		
			<ul class="right">
			  	<h:panelGroup rendered="#{!taxCodeBackingBean.findMode}">
					<li class="add"><h:commandLink id="addBtn" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{taxCodeBackingBean.displayAddAction}" /></li>
					<li class="copy-add"><h:commandLink id="copyBtn" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.displayCopyAction}" /></li>
					<li class="update"><h:commandLink id="updBtn" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.displayUpdateAction}" /></li>	
					<li class="delete"><h:commandLink id="delBtn" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.displayDeleteAction}" /></li>
					<li class="view"><h:commandLink id="viewBtn" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.displayViewTaxCodeAction}" /></li>
					<li class="rules"><h:commandLink id="rulesBtn" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.displayRulesAction}" /></li>
				</h:panelGroup>
				<h:panelGroup rendered="#{taxCodeBackingBean.findMode}">
					<li class="ok2"><h:commandLink id="okBtn" immediate="true" disabled="#{!taxCodeBackingBean.validSelection}" action="#{taxCodeBackingBean.findAction}"/></li>
					<!-- <li class="ok2"><h:commandLink id="okBtn" disabled="#{taxCodeBackingBean.disableUpdate}" immediate="true" action="#{taxCodeBackingBean.findAction}"/></li> -->
					<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{taxCodeBackingBean.cancelFindAction}" /></li>
				</h:panelGroup>			
		   	</ul>		
		</div>
	</div>
</div>

</h:form>
</ui:define>
</ui:composition>
</html>