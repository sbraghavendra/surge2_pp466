<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('detailForm:jurisTable', 'selectedJurisdictionIndex'); } );
	registerEvent(window, "load", function() { selectRowByIndex('detailForm:rulesTable', 'selectedTaxCodeRuleIndex'); } );
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedJurisdictionIndex" value="#{taxCodeBackingBean.selectedJurisdictionIndex}"/>
<h:inputHidden id="selectedTaxCodeRuleIndex" value="#{taxCodeBackingBean.selectedTaxCodeRuleIndex}"/>
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top" style="width:974px;">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			
	<!--  
    			<tr><td colspan="4">Define Jurisdictions &amp; Rules</td></tr>
    -->
    			
    			<c:if test="#{!taxCodeBackingBean.viewRuleOnly}">
						<tr><td colspan="4">Define Jurisdictions &amp; Rules</td></tr>
				</c:if>
				<c:if test="#{taxCodeBackingBean.viewRuleOnly}">
						<tr><td colspan="4">View a TaxCode</td></tr>
				</c:if>

	
		</thead>
		<tbody>
			<tr>
				<td style="width:10px;">&#160;</td>
				<td class="column-label">TaxCode:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode} - #{taxCodeBackingBean.selectedTaxCode.description}"
						required="true" label="Description" id="txtDescription1"
					 	 disabled="true"
				         style="width:740px;"
				         maxlength="255"/>
                </td>
			</tr>
		</tbody>
	</table>
	<table width="100%">
		<tbody>
			<tr>
				<th colspan="8">Jurisdictions</th>
			</tr>
			<tr>
				<td style="width:10px;">&#160;</td>
				<td class="column-label">Selection Filter:</td>
				<td><h:outputText id="countryLabel" value="Country:"/></td>
				<td>		
						<h:selectOneMenu id="ddlCountry" binding="#{taxCodeBackingBean.filterHandler.countryInput}" value="#{taxCodeBackingBean.filterHandler.country}" style = "width:126px">
							<f:selectItems id="ddlCountryItms" value="#{taxCodeBackingBean.filterHandler.countryItems}" />
							<a4j:support event="onchange" reRender="ddlState,ddlCounty,ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.filterHandler.countrySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10px;">&#160;</td>
				<td><h:outputText id="stateLabel" value="State:"/></td>	
				<td>	
						<h:selectOneMenu id="ddlState" binding="#{taxCodeBackingBean.filterHandler.stateInput}" value="#{taxCodeBackingBean.filterHandler.state}" style = "width:126px">
							<f:selectItems id="ddlStateItms" value="#{taxCodeBackingBean.filterHandler.stateItems}" />
							<a4j:support event="onchange" reRender="ddlCounty,ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.filterHandler.stateSelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td><h:outputText id="countyLabel" value="County:"/></td>		
				<td>
						<h:selectOneMenu id="ddlCounty" binding="#{taxCodeBackingBean.filterHandler.countyInput}" value="#{taxCodeBackingBean.filterHandler.county}" style = "width:126px">
							<f:selectItems id="ddlCountyItms" value="#{taxCodeBackingBean.filterHandler.countyItems}" />
							<a4j:support event="onchange" reRender="ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.filterHandler.countySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10px;">&#160;</td>
				<td><h:outputText id="cityLabel"  value="City:"/></td>
				<td>		
						<h:selectOneMenu id="ddlCity" binding="#{taxCodeBackingBean.filterHandler.cityInput}" value="#{taxCodeBackingBean.filterHandler.city}" style = "width:126px">
							<f:selectItems id="ddlCityItms" value="#{taxCodeBackingBean.filterHandler.cityItems}" />
							<a4j:support event="onchange" reRender="ddlCounty,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.filterHandler.citySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			
			
			<tr>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td><h:outputText id="stjLabel" value="STJ:"/></td>		
				<td colspan="4">
						<h:selectOneMenu id="ddlStj" binding="#{taxCodeBackingBean.filterHandler.stjInput}" value="#{taxCodeBackingBean.filterHandler.stj}" style = "width:126px">
							<f:selectItems id="ddlStjItms" value="#{taxCodeBackingBean.filterHandler.stjItems}" />
							<a4j:support event="onchange" reRender="ddlCity,ddlCounty"
								immediate="true" actionListener="#{taxCodeBackingBean.filterHandler.stjSelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="clear"><a4j:commandLink id="btnClear" action="#{taxCodeBackingBean.rulesClearFilterAction}" reRender="ddlCountry,ddlState,ddlCounty,ddlCity,ddlStj" /></li>
			<li class="search"><a4j:commandLink id="btnSearch" immediate="true" action="#{taxCodeBackingBean.rulesSearchFilterAction}" reRender="btnRefDoc,btnCopyAdd,btnUpdate,btnDelete,effectivedateBtn,btnView,jurisTable,rulesTable,trScroll,pageInfo" /></li>
		</ul>
	</div>
	</div>
	

</div>	
<div id="bottom" style="width:1015px;">
	<div id="table-four">
	<div id="table-four-top">
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText="#{taxCodeBackingBean.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	</div>

			
	<div id="table-four-content">
	<div class="scrollContainer">
		<div class="scrollInner">
			<rich:dataTable rowClasses="odd-row,even-row" id="jurisTable" styleClass="GridContent" value="#{taxCodeBackingBean.jurisdictionList}" var="item" rows="#{taxCodeBackingBean.jurisTablePageSize}">
				<a4j:support event="onRowClick"
					onsubmit="selectRow('detailForm:jurisTable', this);"
					immediate="true"
					actionListener="#{taxCodeBackingBean.selectedJurisdictionChanged}"
					reRender="rulesTable,btnRefDoc,btnCopyAdd,btnUpdate,btnDelete,effectivedateBtn,btnView"
					oncomplete="initScrollingTables();"
				/>
								
				
				<rich:column sortBy="#{item.taxcodeCountryCode}" sortOrder="ASCENDING" width="100px">
					<f:facet name="header"><h:outputText value="Country" style = "color:#336690"/></f:facet>
					<h:outputText id="rowCountry" value="#{item.taxCodeCountryName} (#{item.taxcodeCountryCode})" />
				</rich:column>
				<rich:column sortBy="#{item.taxcodeStateCode}" sortOrder="ASCENDING" width="100px" >
					<f:facet name="header"><h:outputText value="State" style = "color:#336690"/></f:facet>
					<h:outputText id="rowState" value="#{item.name} (#{item.taxcodeStateCode})" />
				</rich:column>
				<rich:column sortBy="#{item.taxCodeCounty}" sortOrder="ASCENDING" width="100px" >
					<f:facet name="header"><h:outputText value="County" style = "color:#336690"/></f:facet>
					<h:outputText id="rowCounty" value="#{item.taxCodeCounty}" />
				</rich:column>
				<rich:column sortBy="#{item.taxCodeCity}" sortOrder="ASCENDING" width="100px" >
					<f:facet name="header"><h:outputText value="City" style = "color:#336690"/></f:facet>
					<h:outputText id="rowCity" value="#{item.taxCodeCity}" />
				</rich:column>
				<rich:column sortBy="#{item.taxCodeStj}" sortOrder="ASCENDING" width="380px" >
					<f:facet name="header"><h:outputText value="STJ" style = "color:#336690"/></f:facet>
					<h:outputText id="rowStj" value="#{item.taxCodeStj}" />
				</rich:column>
				<rich:column sortBy="#{item.jurLevel}" sortOrder="ASCENDING" width="100px" >
					<f:facet name="header"><h:outputText value="Level" style = "color:#336690"/></f:facet>
					<h:outputText id="rowJurLevel" value="#{item.jurLevel}" />
				</rich:column>
			</rich:dataTable>
		</div>
		<rich:datascroller id="trScroll" for="jurisTable" maxPages="10" oncomplete="initScrollingTables();"
				style="clear:both;" align="center" stepControls="auto" ajaxSingle="false" page="#{taxCodeBackingBean.jurisTablePageNumber}"
				reRender="pageInfo" />
	</div>
	</div>
	<div id="table-four-bottom">
		
	</div>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<td>&#160;</td>
				</tr>
			</tbody>
		</table>
	<div id="table-four-top" style="min-height: 0px;padding: 3px 0 0 10px;">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<th colspan="2">Rules</th>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner" id="resize">
				<rich:dataTable rowClasses="odd-row,even-row" id="rulesTable" styleClass="GridContent" value="#{taxCodeBackingBean.taxCodeRuleList}" var="item">
					<a4j:support event="onRowClick"
						onsubmit="selectRow('detailForm:rulesTable', this);"
						immediate="true"
						actionListener="#{taxCodeBackingBean.selectedTaxcodeRuleChanged}"
						reRender="btnRefDoc,btnCopyAdd,btnUpdate,btnDelete,effectivedateBtn,btnView"
					/>
					
					<rich:column sortBy="#{item.taxcodeDetailId}">
						<f:facet name="header"><h:outputText value="ID" style = "color:#336690"/></f:facet>
						<h:outputText id="rowid" value="#{item.taxcodeDetailId}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}" />
					</rich:column>
					<rich:column sortBy="#{item.effectiveDate}" sortOrder="DESCENDING" >
						<f:facet name="header"><h:outputText value="Effective Date" style = "color:#336690"/></f:facet>
						<h:outputText id="roweffectiveDate" value="#{item.effectiveDate}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.taxcodeTypeDesc}">
						<f:facet name="header"><h:outputText value="Taxability" style = "color:#336690"/></f:facet>
						<h:outputText id="rowtaxcodeTypeDesc" value="#{item.taxcodeTypeDesc}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}"/>
					</rich:column>
					<rich:column sortBy="#{item.taxtypeDesc}" >
						<f:facet name="header"><h:outputText value="Tax Type" style = "color:#336690"/></f:facet>
						<h:outputText id="rowtaxtypeDesc" value="#{item.taxtypeDesc}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}"/>
					</rich:column>
					<rich:column sortBy="#{item.rateTypeDesc}" >
						<f:facet name="header"><h:outputText value="Rate Type" style = "color:#336690"/></f:facet>
						<h:outputText id="rowrateTypeDesc" value="#{item.rateTypeDesc}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}" />
					</rich:column>
					<rich:column sortBy="#{item.taxableThresholdAmt}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Taxable Threshold Amt" style = "color:#336690"/></f:facet>
						<h:outputText id="rowtaxableThresholdAmt" value="#{item.taxableThresholdAmt}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.minimumTaxableAmt}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Min. Taxable Amt" style = "color:#336690"/></f:facet>
						<h:outputText id="rowmintaxableAmt" value="#{item.minimumTaxableAmt}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.maximumTaxableAmt}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Max. Taxable Amt" style = "color:#336690"/></f:facet>
						<h:outputText id="rowmaxtaxableAmt" value="#{item.maximumTaxableAmt}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.maximumTaxAmt}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Max. Tax Amt" style = "color:#336690"/></f:facet>
						<h:outputText id="rowmaxtaxAmt" value="#{item.maximumTaxAmt}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.baseChangePct}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Pct of Base" style = "color:#336690"/></f:facet>
						<h:outputText id="rowbaseChangePct" value="#{item.baseChangePct}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.specialRate}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Special Rate" style = "color:#336690"/></f:facet>
						<h:outputText id="rowspecialRate" value="#{item.specialRate}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.specialSetAmt}" style="text-align:right'">
						<f:facet name="header"><h:outputText value="Special Set Amt" style = "color:#336690"/></f:facet>
						<h:outputText id="rowspecialSetAmt" value="#{item.specialSetAmt}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
								<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.expirationDate}">
						<f:facet name="header"><h:outputText value="Expiration Date" style = "color:#336690"/></f:facet>
						<h:outputText id="rowexpirationDate" value="#{item.expirationDate}" style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}">
							<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
						</h:outputText>
					</rich:column>
					<rich:column sortBy="#{item.activeBooleanFlag}" style="text-align:center'">
						<f:facet name="header"><h:outputText value="Active?" style = "color:#336690"/></f:facet>
						<h:selectBooleanCheckbox id="chkActive" value="#{item.activeBooleanFlag}" disabled="true" styleClass="check"
						style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}"/>
					</rich:column>
					
					<rich:column sortBy="#{item.refdoccountBooleanFlag}" style="text-align:center'">
						<f:facet name="header"><h:outputText value="Ref. Doc.?" style = "color:#336690"/></f:facet>
						<h:selectBooleanCheckbox id="chkRefdoccount" value="#{item.refdoccountBooleanFlag}" disabled="true" styleClass="check"
						style="#{(item.activeFlag) == 1 ? '' : 'color:#888888'}"/>
					</rich:column>
				</rich:dataTable>
			</div>
		</div>
	</div>
	
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ref-doc"><h:commandLink id="btnRefDoc" disabled="#{!taxCodeBackingBean.ruleSelected}" action="#{taxCodeBackingBean.refDocsAction}" /></li>
			<li class="add"><h:commandLink id="btnAdd" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{taxCodeBackingBean.taxCodeRuleAddAction}" disabled="#{taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" /></li>
			<li class="copy-add"><h:commandLink id="btnCopyAdd" disabled="#{!taxCodeBackingBean.ruleSelected or taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{taxCodeBackingBean.taxCodeRuleCopyAddAction}" /></li>
			<li class="update"><h:commandLink id="btnUpdate" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.ruleSelected or taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{taxCodeBackingBean.taxCodeRuleUpdateAction}" /></li>
			<li class="delete115"><h:commandLink id="btnDelete" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!taxCodeBackingBean.ruleSelected or taxCodeBackingBean.viewRuleOnly or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{taxCodeBackingBean.taxCodeRuleDeleteAction}" /></li>
			<li class="effectivedate"><h:commandLink id="effectivedateBtn" style="#{(taxCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{!taxCodeBackingBean.effectivedateSelection or taxCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{taxCodeBackingBean.displayBackDateAction}" /></li>
	 		<li class="view"><h:commandLink id="btnView" disabled="#{!taxCodeBackingBean.ruleSelected or taxCodeBackingBean.viewRuleOnly}" action="#{taxCodeBackingBean.taxCodeRuleViewAction}" /></li>
			<li class="close"><h:commandLink id="btnClose" action="#{taxCodeBackingBean.viewAction}" /></li>	
		</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>