<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblTaxcodeMaint" value="#{taxCodeBackingBean.actionText}"/> a Reference Document<h:outputText value="#{taxCodeBackingBean.deleteAction? ' from ':' to '}"/>a TaxCode Rule</td></tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="3">&#160;TaxCode</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">TaxCode:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode}" 
						required="true" label="TaxCode" id="txtTaxcodeCode"
						validator="#{taxCodeBackingBean.validateTaxCode}"
					 	 disabled="true"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="255" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.description}" 
						required="true" label="Description" id="txtDescription"
					 	 disabled="true"
				         style="width:650px;" 
				         maxlength="255"/>
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="5">&#160;Jurisdiction</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Country:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCountryName} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeCountryCode})" 
					 	 disabled="true"
				         id="txtCountry"
				         maxlength="50"/>
                </td>
                <td>State:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.name} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeStateCode})" 
					 	 disabled="true"
				         id="txtState"
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">County:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCounty}" 
					 	 disabled="true"
				         id="txtCounty"
				         maxlength="50"/>
                </td>
                <td>City:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCity}" 
					 	 disabled="true"
				         id="txtCity"
				         maxlength="50"/>
                </td>
			</tr>
			
			
			<tr>
                <td style="width:50px;">&#160;</td>
				<td class="column-label" >STJ:</td>
                <td colspan="3" >
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeStj}" style="width:650px;" 
					 	 disabled="true"
				         id="txtStj"
				         maxlength="300"/>
                </td>
			</tr>
			
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="7">&#160;Rule</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td colspan="5">
					<h:inputText id="effectiveDate" value="#{taxCodeBackingBean.selectedTaxCodeRule.effectiveDate}" disabled="true">
						<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
					</h:inputText>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:inputText id="taxcodeTypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxcodeTypeDesc}" disabled="true" />
                </td>
                <td>Tax Type:</td>
				<td>
					<h:inputText id="taxtypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxtypeDesc}" disabled="true" />
                </td>
                <td>Rate Type:</td>
				<td>
					<h:inputText id="rateTypeDesc" value="#{taxCodeBackingBean.selectedTaxCodeRule.rateTypeDesc}" disabled="true" />
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="3">&#160;Reference Documents</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Ref. Doc. Code:</td>
				<td>
					<h:inputText id="refDocCode" disabled="#{taxCodeBackingBean.deleteAction}" onkeyup="upperCaseInputText(this)"  binding="#{taxCodeBackingBean.refDocCodeInput}" value="#{taxCodeBackingBean.updateRefDocDetail.referenceDetailPK.refDoc.refDocCode}" style="float:left;width:300px" />
					<a4j:commandButton id="refDocLookupId" styleClass="image"
						image="/images/search_small.png" 
						rendered="#{!taxCodeBackingBean.deleteAction}" 
						immediate="true"
						actionListener="#{taxCodeBackingBean.popupDocRefSearchAction}"
						action="#{taxCodeBackingBean.findRefDocAction}" >
					</a4j:commandButton>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Ref. Type:</td>
				<td>
					<h:inputText id="refTypeCodeDesc" value="#{taxCodeBackingBean.refTypeCodeDescMap[taxCodeBackingBean.updateRefDocDetail.referenceDetailPK.refDoc.refTypeCode]}" disabled="true" />
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td>
					<h:inputText id="description" value="#{taxCodeBackingBean.updateRefDocDetail.referenceDetailPK.refDoc.description}" style="width:650px;" disabled="true" />
				</td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.updateRefDocDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.updateRefDocDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk" action="#{taxCodeBackingBean.refDocsOkAction}" /></li>
			<li class="cancel"><h:commandLink id="btnCancel" action="#{taxCodeBackingBean.refDocsCancelAction}" /></li>
		</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>