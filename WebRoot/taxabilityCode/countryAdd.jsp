<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="States Maint" value="/images/headers/hdr-supporting-codes.gif" /></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="StateMaint" value="#{supportingCodeBackingBean.actionText}"/> a Country</td></tr>
		</thead>
		<tbody>
	
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Country Code:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCountry.country}" 
						 label="Country Code" id="txtTaxcodeCountryCode"
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction or supportingCodeBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="10" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCountry.name}" 
						label="Name" id="txtName"
					 	disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}"
				        style="width:650px;" 
				        maxlength="50"/>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Active:</td>
				<td>
                 	 <h:selectBooleanCheckbox styleClass="check" id="chkActiveBooleanFlag" 
                 	 	 value="#{supportingCodeBackingBean.selectedCountry.activeBooleanFlag}" 
					 	 disabled="#{supportingCodeBackingBean.viewAction or supportingCodeBackingBean.deleteAction}" >
                      </h:selectBooleanCheckbox>
                 </td>
			</tr>					
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCountry.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCountry.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{supportingCodeBackingBean.okCountryAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{supportingCodeBackingBean.viewAction}"  action="#{supportingCodeBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>