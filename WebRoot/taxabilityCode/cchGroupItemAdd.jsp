<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgCchGroupItemAdd" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{supportingCodeBackingBean.actionText}"/> a CCH Item</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>CCH Group:</td>
				<td>
					<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.cchTaxMatrixPK.groupCode}" 
						 	 disabled="true"
					         style="width:300px;"
					         maxlength="4" />
					<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.groupDesc}" 
						 	 disabled="true"
					         style="width:344px;" 
					         maxlength="30"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>CCH Item:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.cchTaxMatrixPK.item}" 
						required="true" label="CCH Item"
						validator="#{supportingCodeBackingBean.validateCCHGroupItemCode}"
					 	 disabled="#{supportingCodeBackingBean.deleteAction or supportingCodeBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="3" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.itemDesc}" 
						required="true" label="Description"
					 	 disabled="#{supportingCodeBackingBean.deleteAction}"
				         style="width:650px;" 
				         maxlength="100"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Effective Date:</td>
				<td>
				<rich:calendar popup="true" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{supportingCodeBackingBean.deleteAction}"
						value="#{supportingCodeBackingBean.selectedCCHGroupItem.effDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" id="effDate" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.updateUserId}" 
				         disabled="true"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedCCHGroupItem.updateTimestamp}" 
				         disabled="true"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink action="#{supportingCodeBackingBean.okCCHGroupItemAction}" /></li>
		<li class="cancel"><h:commandLink immediate="true" action="#{supportingCodeBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>