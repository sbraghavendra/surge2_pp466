<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblTaxcodeMaint" value="#{taxCodeBackingBean.actionText}"/> a Rule <h:outputText id="lblTaxcodeMaintSuffix" value="#{taxCodeBackingBean.actionTextSuffix}"/></td></tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="3">&#160;TaxCode</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">TaxCode:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode}" 
						required="true" label="TaxCode" id="txtTaxcodeCode"
						validator="#{taxCodeBackingBean.validateTaxCode}"
					 	 disabled="true"
				          style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="40" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.description}" 
						required="true" label="Description" id="txtDescription"
					 	 disabled="true"
				         style="width:650px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Comment:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.comments}" 
					 	 disabled="true"
				         style="width:650px;" id="txtComments"
				         maxlength="3000"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
				<h:selectBooleanCheckbox id="chkActive" value="#{taxCodeBackingBean.selectedTaxCode.activeBooleanFlag}" 
					disabled="true" styleClass="check"/>
                </td>
			</tr>
		</tbody>
	</table>
	<c:if test="#{taxCodeBackingBean.showJurisInAdd}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<th colspan="5">&#160;Jurisdiction</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Country:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCountryName} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeCountryCode})" 
					 	 disabled="true"
				         id="txtCountry"
				         maxlength="50"/>
                </td>
                <td>State:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.name} (#{taxCodeBackingBean.selectedJurisdiction.taxcodeStateCode})" 
					 	 disabled="true"
				         id="txtState"
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">County:</td>
				<td>
					<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCounty}" 
					 	 disabled="true"
				         id="txtCounty"
				         maxlength="50"/>
                </td>
                <td>City:</td>
                <td>
                	<h:inputText value="#{taxCodeBackingBean.selectedJurisdiction.taxCodeCity}" 
					 	 disabled="true"
				         id="txtCity"
				         maxlength="50"/>
                </td>
			</tr>
			
			<c:if test="#{not empty taxCodeBackingBean.jurisdictionFromTransaction.stj1Name}">
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ1:</td>
				<td colspan="3" >
					<h:inputText value="#{taxCodeBackingBean.jurisdictionFromTransaction.stj1Name}" 
						 style="width:650px;" 
					 	 disabled="true"
				         id="txtstj1Name"
				         maxlength="50"/>
                </td>
			</tr>
			</c:if>
			
			<c:if test="#{not empty taxCodeBackingBean.jurisdictionFromTransaction.stj2Name}">
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ2:</td>
				<td colspan="3" >
					<h:inputText value="#{taxCodeBackingBean.jurisdictionFromTransaction.stj2Name}" 
						 style="width:650px;" 
					 	 disabled="true"
				         id="txtstj2Name"
				         maxlength="50"/>
                </td>
			</tr>
			</c:if>
			
			<c:if test="#{not empty taxCodeBackingBean.jurisdictionFromTransaction.stj3Name}">
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ3:</td>
				<td colspan="3" >
					<h:inputText value="#{taxCodeBackingBean.jurisdictionFromTransaction.stj3Name}" 
						 style="width:650px;" 
					 	 disabled="true"
				         id="txtstj3Name"
				         maxlength="50"/>
                </td>
			</tr>
			</c:if>
			
			<c:if test="#{not empty taxCodeBackingBean.jurisdictionFromTransaction.stj4Name}">
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ4:</td>
				<td colspan="3" >
					<h:inputText value="#{taxCodeBackingBean.jurisdictionFromTransaction.stj4Name}" 
						 style="width:650px;" 
					 	 disabled="true"
				         id="txtstj4Name"
				         maxlength="50"/>
                </td>
			</tr>
			</c:if>
			
			<c:if test="#{not empty taxCodeBackingBean.jurisdictionFromTransaction.stj5Name}">
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ5:</td>
				<td colspan="3" >
					<h:inputText value="#{taxCodeBackingBean.jurisdictionFromTransaction.stj5Name}" 
						 style="width:650px;" 
					 	 disabled="true"
				         id="txtstj5Name"
				         maxlength="50"/>
                </td>
			</tr>
			</c:if>
			
			
		</tbody>
	</table>
	</c:if>
	<div class="tab">
	<table cellpadding="0" cellspacing="0" width="100%">
	  	<tr><th colspan="3">&#160;Rules </th>
	  	</tr>
	  	
	  	<tr>
			<td><button class="tablinks" onclick="openTab(event, 'stateTab'); return false;">State</button></td>
	 		<td><button class="tablinks" onclick="openTab(event, 'countyTab'); return false;">County</button></td>
	  		<td><button class="tablinks" onclick="openTab(event, 'cityTab'); return false;">City</button></td>
	  	</tr>
	</table>
	</div>
	<div id = "stateTab" class="ruleTabContent">
		<rich:simpleTogglePanel id="stateTaxabilityRulesPanel"   switchType="ajax" label="Taxability Rules" opened="true"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction or taxCodeBackingBean.deleteAction}"
						value="#{taxCodeBackingBean.selectedTaxCodeRule.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="stateeffDate" />
                </td>
                <td style="width:10px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:selectOneMenu id="statetaxability" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxcodeTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="statetaxabilityItms" value="#{taxCodeBackingBean.taxabilityItems}" />
						<a4j:support event="onclick" id="statetaxabilitySupport" actionListener="#{taxCodeBackingBean.selectedTaxabilityChangeAction}"
							reRender="taxType,rateType,DistributionRulesPanel,allocBucket,allocProrateByCode,allocConditionCode,exemptReason"  />
					</h:selectOneMenu>
                </td>
                <td style="width:50px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
				<td class="column-label">Good/Svc Type:</td>
				<td>
					<h:selectOneMenu id="stategoodSvcType" value="#{taxCodeBackingBean.selectedTaxCodeRule.goodSvcTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="stateserviceTypeItms" value="#{taxCodeBackingBean.serviceTypeItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Tax Type:</td>
				<td style="width:100px;" >
					<h:selectOneMenu id="statetaxType" value="#{taxCodeBackingBean.selectedTaxCodeRule.taxtypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="statetaxTypeItms" value="#{taxCodeBackingBean.taxTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Situs</td>
                <td style="width:50px;">&#160;</td>
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Rate Type:</td>
				<td style="width:100px;">
					<h:selectOneMenu id="staterateType" value="#{taxCodeBackingBean.selectedTaxCodeRule.rateTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="staterateTypeItms" value="#{taxCodeBackingBean.rateTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Transaction</td>
                <td style="width:50px;">&#160;</td>
                <td class="column-label">Exempt Reason:</td>
				<td>
					<h:selectOneMenu id="stateexemptReason" value="#{taxCodeBackingBean.selectedTaxCodeRule.exemptReason}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction or taxCodeBackingBean.selectedTaxCodeRule.taxcodeTypeCode != 'E'}">
						<f:selectItems id="stateexemptReasonItms" value="#{taxCodeBackingBean.exemptReasonItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Citation Reference:</td>
				<td colspan="5">
					<h:inputText value="#{taxCodeBackingBean.selectedTaxCodeRule.citationRef}" 
					 	 disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
				         style="width:650px;" id="statetxtcitationRef"
				         maxlength="50"/>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
					<h:selectBooleanCheckbox id="statechkTdActive" value="#{taxCodeBackingBean.selectedTaxCodeRule.activeBooleanFlag}" 
						disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" styleClass="check"/>
                </td>             
                <td style="width:50px;">&#160;</td>    
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	<rich:simpleTogglePanel id="stateOverrideTaxRatesRulesPanel"   switchType="ajax" label="Override Tax Rates Rules" opened="#{taxCodeBackingBean.isOverride}"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>		
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
						<td style="width:13%;">Tax Processing Type:</td>
						<td style="width:87%;text-align:left;"> 
							<h:selectOneMenu id="statetaxProcessTypeCode" value="#{taxCodeBackingBean.updateTaxCodeRule.taxProcessTypeCode}"
												disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
								<f:selectItems id="statetaxProcessTypeCodeItms" value="#{taxCodeBackingBean.processTypeItems}" />
							</h:selectOneMenu>
							<h:outputText value="&#160;"/>
						</td>
						<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;border-bottom: none;">&#160;</td>
				<td colspan="5" style = "border-bottom: none;">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="stateSpecialRateSetAmountId" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.specialRateChecked}" >
									<a4j:support id="stateddsupport4" event="onclick" reRender="specialRateId,specialSetAmtId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Special Rate or Set Amount:</td>
							<td style="width:100px;">
								<h:inputText id="statespecialRateId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialRate}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td style="width:15px;">&#160;or&#160;</td>
							<td style="width:100px;">
								<h:inputText id="statespecialSetAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialSetAmt}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax at a rate or set amount different from the Jurisdiction)&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="width:15px;">&#160;</td>
							<td style="width:158px;">&#160;</td>
							<td style="width:150px;">(enter .01 through .99)</td>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">&#160;</td>
							<td>&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
				  	<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px;">
								<h:selectBooleanCheckbox id="stateMaxtaxAmount" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.maxTaxAmountChecked}" >
									<a4j:support id="stateddsupport5" event="onclick" reRender="maxTaxAmountRadio,taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId,taxProcessTypeCode,groupByDriver" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">&#160;MaxTax Amount:</td>
							<td style="width:50%;">&#160;</td>	
						</tr>
						
						<tr>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">
								<h:panelGroup >
									<h:panelGrid columns="2"> 		
										<h:panelGrid columns="1" style="height:125px;margin:0px 0px 14px 0px;"> 
											<h:selectOneRadio id="statemaxTaxAmountRadio" disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" disabledClass="selectOneRadio_Disabled" styleClass="selectOneRadio" layout="pageDirection" value="#{taxCodeBackingBean.maxTaxAmountOption}" >	
												<f:selectItem itemValue="1" itemLabel="&#160;Taxable Threshold Amount:"/>
												<f:selectItem itemValue="2" itemLabel="&#160;Minimum Taxable Amount:"/> 
												<f:selectItem itemValue="3" itemLabel="&#160;Maximum Taxable Amount:"/>
												<f:selectItem itemValue="4" itemLabel="&#160;Maximum Tax Amount:"/>
												
												<a4j:support id="stateddsupport6" event="onclick" 
                        							ajaxSingle="true" reRender="taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId"
                        							oncomplete="resetAmounts()" />
											</h:selectOneRadio>
											<h:outputText value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Group By Driver:"/>
										</h:panelGrid>
										<h:panelGrid columns="2" style="height:125px; margin:0px 0px 7px 0px"> 
											<h:inputText id="statetaxableThresholdAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.taxableThresholdAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '1' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax entire amount when equal or greater)"/>
											
											<h:inputText id="stateminimumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.minimumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '2' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or over amount)"/>
											
											<h:inputText id="statemaximumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '3' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or under amount)"/>
											
											<h:inputText id="statemaximumTaxAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '4' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(highest allowed calculated tax amount)"/>
											
											<h:selectOneMenu id="stategroupByDriver" value="#{taxCodeBackingBean.updateTaxCodeRule.groupByDriver}"
												disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
												<f:selectItems id="stategroupByDriverItms" value="#{taxCodeBackingBean.groupByDriverItems}" />
											</h:selectOneMenu>
											<h:outputText value="&#160;"/>
											
										</h:panelGrid>
									</h:panelGrid>
								</h:panelGroup>
							</td>
							<td style="width:50%;">&#160;</td>
						</tr>		
				
					</tbody>
					</table>
			 	</div>	
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="statePercentofBase" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.percentBaseChecked}" >
									<a4j:support id="stateddsupport7" event="onclick" reRender="percentofBaseId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Percent of Base:</td>
							<td style="width:100px;">
								<h:inputText id="statepercentofBaseId" value="#{taxCodeBackingBean.updateTaxCodeRule.baseChangePct}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.percentBaseChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax only a portion of the base amount)&#160;</td>
							 <td style="width:5%;">(enter .01 through .99)</td> 
               				 <td style="width:30%;">&#160;</td>
						</tr>
					</tbody>
					</table>
		 		</div>
				</td>
			</tr>
			
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	</div>
	<div id = "countyTab" class="ruleTabContent">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
			<tr>
				<th colspan="6">&#160;Rule - County&#160;&#160;
					<h:selectBooleanCheckbox id="chkAddCountyRule" value="#{taxCodeBackingBean.addCountyRule}" 
						styleClass="check">
						<a4j:support event="onclick"
							reRender="countyTaxabilityRulesPanel,countyOverrideTaxRatesRulesPanel"  />
					</h:selectBooleanCheckbox>
				</th>
			</tr>
			</tbody>
		</table>
		<rich:simpleTogglePanel id="countyTaxabilityRulesPanel"   switchType="ajax" label="Taxability Rules" opened="true"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction or taxCodeBackingBean.deleteAction}"
						value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="countyeffDate" />
                </td>
                <td style="width:10px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:selectOneMenu id="countytaxability" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.taxcodeTypeCode}"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="countytaxabilityItms" value="#{taxCodeBackingBean.taxabilityItems}" />
						<a4j:support event="onclick" id="countytaxabilitySupport" actionListener="#{taxCodeBackingBean.selectedTaxabilityChangeAction}"
							reRender="taxType,rateType,DistributionRulesPanel,allocBucket,allocProrateByCode,allocConditionCode,exemptReason"  />
					</h:selectOneMenu>
                </td>
                <td style="width:50px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
				<td class="column-label">Good/Svc Type:</td>
				<td>
					<h:selectOneMenu id="countygoodSvcType" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.goodSvcTypeCode}"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="countyserviceTypeItms" value="#{taxCodeBackingBean.serviceTypeItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Tax Type:</td>
				<td style="width:100px;" >
					<h:selectOneMenu id="countytaxType" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.taxtypeCode}"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="countytaxTypeItms" value="#{taxCodeBackingBean.taxTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Situs</td>
                <td style="width:50px;">&#160;</td>
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Rate Type:</td>
				<td style="width:100px;">
					<h:selectOneMenu id="countyrateType" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.rateTypeCode}"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="countyrateTypeItms" value="#{taxCodeBackingBean.rateTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Transaction</td>
                <td style="width:50px;">&#160;</td>
                <td class="column-label">Exempt Reason:</td>
				<td>
					<h:selectOneMenu id="countyexemptReason" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.exemptReason}"
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction or taxCodeBackingBean.selectedTaxCodeCountyRule.taxcodeTypeCode != 'E'}">
						<f:selectItems id="countyexemptReasonItms" value="#{taxCodeBackingBean.exemptReasonItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Citation Reference:</td>
				<td colspan="5">
					<h:inputText value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.citationRef}" 
					 	 disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
				         style="width:650px;" id="countytxtcitationRef"
				         maxlength="50"/>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
					<h:selectBooleanCheckbox id="countychkTdActive" value="#{taxCodeBackingBean.selectedTaxCodeCountyRule.activeBooleanFlag}" 
						disabled="#{!taxCodeBackingBean.addCountyRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" styleClass="check"/>
                </td>             
                <td style="width:50px;">&#160;</td>    
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	<rich:simpleTogglePanel id="countyOverrideTaxRatesRulesPanel"   switchType="ajax" label="Override Tax Rates Rules" opened="#{taxCodeBackingBean.isOverride}"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>		
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
						<td style="width:13%;">Tax Processing Type:</td>
						<td style="width:87%;text-align:left;"> 
							<h:selectOneMenu id="countytaxProcessTypeCode" value="#{taxCodeBackingBean.updateTaxCodeRule.taxProcessTypeCode}"
												disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
								<f:selectItems id="countytaxProcessTypeCodeItms" value="#{taxCodeBackingBean.processTypeItems}" />
							</h:selectOneMenu>
							<h:outputText value="&#160;"/>
						</td>
						<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;border-bottom: none;">&#160;</td>
				<td colspan="5" style = "border-bottom: none;">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="countySpecialRateSetAmountId" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.specialRateChecked}" >
									<a4j:support id="countyddsupport4" event="onclick" reRender="specialRateId,specialSetAmtId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Special Rate or Set Amount:</td>
							<td style="width:100px;">
								<h:inputText id="countyspecialRateId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialRate}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td style="width:15px;">&#160;or&#160;</td>
							<td style="width:100px;">
								<h:inputText id="countyspecialSetAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialSetAmt}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax at a rate or set amount different from the Jurisdiction)&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="width:15px;">&#160;</td>
							<td style="width:158px;">&#160;</td>
							<td style="width:150px;">(enter .01 through .99)</td>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">&#160;</td>
							<td>&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
				  	<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px;">
								<h:selectBooleanCheckbox id="countyMaxtaxAmount" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.maxTaxAmountChecked}" >
									<a4j:support id="countyddsupport5" event="onclick" reRender="maxTaxAmountRadio,taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId,taxProcessTypeCode,groupByDriver" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">&#160;MaxTax Amount:</td>
							<td style="width:50%;">&#160;</td>	
						</tr>
						
						<tr>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">
								<h:panelGroup >
									<h:panelGrid columns="2"> 		
										<h:panelGrid columns="1" style="height:125px;margin:0px 0px 14px 0px;"> 
											<h:selectOneRadio id="countymaxTaxAmountRadio" disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" disabledClass="selectOneRadio_Disabled" styleClass="selectOneRadio" layout="pageDirection" value="#{taxCodeBackingBean.maxTaxAmountOption}" >	
												<f:selectItem itemValue="1" itemLabel="&#160;Taxable Threshold Amount:"/>
												<f:selectItem itemValue="2" itemLabel="&#160;Minimum Taxable Amount:"/> 
												<f:selectItem itemValue="3" itemLabel="&#160;Maximum Taxable Amount:"/>
												<f:selectItem itemValue="4" itemLabel="&#160;Maximum Tax Amount:"/>
												
												<a4j:support id="countyddsupport6" event="onclick" 
                        							ajaxSingle="true" reRender="taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId"
                        							oncomplete="resetAmounts()" />
											</h:selectOneRadio>
											<h:outputText value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Group By Driver:"/>
										</h:panelGrid>
										<h:panelGrid columns="2" style="height:125px; margin:0px 0px 7px 0px"> 
											<h:inputText id="countytaxableThresholdAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.taxableThresholdAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '1' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax entire amount when equal or greater)"/>
											
											<h:inputText id="countyminimumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.minimumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '2' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or over amount)"/>
											
											<h:inputText id="countymaximumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '3' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or under amount)"/>
											
											<h:inputText id="countymaximumTaxAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '4' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(highest allowed calculated tax amount)"/>
											
											<h:selectOneMenu id="countygroupByDriver" value="#{taxCodeBackingBean.updateTaxCodeRule.groupByDriver}"
												disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
												<f:selectItems id="countygroupByDriverItms" value="#{taxCodeBackingBean.groupByDriverItems}" />
											</h:selectOneMenu>
											<h:outputText value="&#160;"/>
											
										</h:panelGrid>
									</h:panelGrid>
								</h:panelGroup>
							</td>
							<td style="width:50%;">&#160;</td>
						</tr>		
				
					</tbody>
					</table>
			 	</div>	
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="countyPercentofBase" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.percentBaseChecked}" >
									<a4j:support id="countyddsupport7" event="onclick" reRender="percentofBaseId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Percent of Base:</td>
							<td style="width:100px;">
								<h:inputText id="countypercentofBaseId" value="#{taxCodeBackingBean.updateTaxCodeRule.baseChangePct}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.percentBaseChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax only a portion of the base amount)&#160;</td>
							 <td style="width:5%;">(enter .01 through .99)</td> 
               				 <td style="width:30%;">&#160;</td>
						</tr>
					</tbody>
					</table>
		 		</div>
				</td>
			</tr>
			
		</tbody>
	</table>				
	</rich:simpleTogglePanel>	
	
	</div>
	<div id = "cityTab" class="ruleTabContent">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<tbody>
				<tr>
					<th colspan="6">&#160;Rule - City&#160;&#160;
						<h:selectBooleanCheckbox id="chkAddCityRule" value="#{taxCodeBackingBean.addCityRule}" 
							styleClass="check">
							<a4j:support event="onclick"
								reRender="cityTaxabilityRulesPanel,cityOverrideTaxRatesRulesPanel"  />
						</h:selectBooleanCheckbox>
					</th>
				</tr>
			</tbody>
		</table>
		<rich:simpleTogglePanel id="cityTaxabilityRulesPanel"   switchType="ajax" label="Taxability Rules" opened="true"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction or taxCodeBackingBean.deleteAction}"
						value="#{taxCodeBackingBean.selectedTaxCodeCityRule.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="cityeffDate" />
                </td>
                <td style="width:10px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:selectOneMenu id="citytaxability" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.taxcodeTypeCode}"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="citytaxabilityItms" value="#{taxCodeBackingBean.taxabilityItems}" />
						<a4j:support event="onclick" id="citytaxabilitySupport" actionListener="#{taxCodeBackingBean.selectedTaxabilityChangeAction}"
							reRender="taxType,rateType,DistributionRulesPanel,allocBucket,allocProrateByCode,allocConditionCode,exemptReason"  />
					</h:selectOneMenu>
                </td>
                <td style="width:50px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
				<td class="column-label">Good/Svc Type:</td>
				<td>
					<h:selectOneMenu id="citygoodSvcType" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.goodSvcTypeCode}"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="cityserviceTypeItms" value="#{taxCodeBackingBean.serviceTypeItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Tax Type:</td>
				<td style="width:100px;" >
					<h:selectOneMenu id="citytaxType" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.taxtypeCode}"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="citytaxTypeItms" value="#{taxCodeBackingBean.taxTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Situs</td>
                <td style="width:50px;">&#160;</td>
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Rate Type:</td>
				<td style="width:100px;">
					<h:selectOneMenu id="cityrateType" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.rateTypeCode}"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="cityrateTypeItms" value="#{taxCodeBackingBean.rateTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Transaction</td>
                <td style="width:50px;">&#160;</td>
                <td class="column-label">Exempt Reason:</td>
				<td>
					<h:selectOneMenu id="cityexemptReason" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.exemptReason}"
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction or taxCodeBackingBean.selectedTaxCodeCityRule.taxcodeTypeCode != 'E'}">
						<f:selectItems id="cityexemptReasonItms" value="#{taxCodeBackingBean.exemptReasonItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Citation Reference:</td>
				<td colspan="5">
					<h:inputText value="#{taxCodeBackingBean.selectedTaxCodeCityRule.citationRef}" 
					 	 disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
				         style="width:650px;" id="citytxtcitationRef"
				         maxlength="50"/>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
					<h:selectBooleanCheckbox id="citychkTdActive" value="#{taxCodeBackingBean.selectedTaxCodeCityRule.activeBooleanFlag}" 
						disabled="#{!taxCodeBackingBean.addCityRule or taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" styleClass="check"/>
                </td>             
                <td style="width:50px;">&#160;</td>    
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	<rich:simpleTogglePanel id="cityOverrideTaxRatesRulesPanel"   switchType="ajax" label="Override Tax Rates Rules" opened="#{taxCodeBackingBean.isOverride}"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>		
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
						<td style="width:13%;">Tax Processing Type:</td>
						<td style="width:87%;text-align:left;"> 
							<h:selectOneMenu id="citytaxProcessTypeCode" value="#{taxCodeBackingBean.updateTaxCodeRule.taxProcessTypeCode}"
												disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
								<f:selectItems id="citytaxProcessTypeCodeItms" value="#{taxCodeBackingBean.processTypeItems}" />
							</h:selectOneMenu>
							<h:outputText value="&#160;"/>
						</td>
						<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;border-bottom: none;">&#160;</td>
				<td colspan="5" style = "border-bottom: none;">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="citySpecialRateSetAmountId" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.specialRateChecked}" >
									<a4j:support id="cityddsupport4" event="onclick" reRender="specialRateId,specialSetAmtId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Special Rate or Set Amount:</td>
							<td style="width:100px;">
								<h:inputText id="cityspecialRateId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialRate}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td style="width:15px;">&#160;or&#160;</td>
							<td style="width:100px;">
								<h:inputText id="cityspecialSetAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialSetAmt}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax at a rate or set amount different from the Jurisdiction)&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="width:15px;">&#160;</td>
							<td style="width:158px;">&#160;</td>
							<td style="width:150px;">(enter .01 through .99)</td>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">&#160;</td>
							<td>&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
				  	<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px;">
								<h:selectBooleanCheckbox id="cityMaxtaxAmount" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.maxTaxAmountChecked}" >
									<a4j:support id="cityddsupport5" event="onclick" reRender="maxTaxAmountRadio,taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId,taxProcessTypeCode,groupByDriver" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">&#160;MaxTax Amount:</td>
							<td style="width:50%;">&#160;</td>	
						</tr>
						
						<tr>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">
								<h:panelGroup >
									<h:panelGrid columns="2"> 		
										<h:panelGrid columns="1" style="height:125px;margin:0px 0px 14px 0px;"> 
											<h:selectOneRadio id="citymaxTaxAmountRadio" disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" disabledClass="selectOneRadio_Disabled" styleClass="selectOneRadio" layout="pageDirection" value="#{taxCodeBackingBean.maxTaxAmountOption}" >	
												<f:selectItem itemValue="1" itemLabel="&#160;Taxable Threshold Amount:"/>
												<f:selectItem itemValue="2" itemLabel="&#160;Minimum Taxable Amount:"/> 
												<f:selectItem itemValue="3" itemLabel="&#160;Maximum Taxable Amount:"/>
												<f:selectItem itemValue="4" itemLabel="&#160;Maximum Tax Amount:"/>
												
												<a4j:support id="cityddsupport6" event="onclick" 
                        							ajaxSingle="true" reRender="taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId"
                        							oncomplete="resetAmounts()" />
											</h:selectOneRadio>
											<h:outputText value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Group By Driver:"/>
										</h:panelGrid>
										<h:panelGrid columns="2" style="height:125px; margin:0px 0px 7px 0px"> 
											<h:inputText id="citytaxableThresholdAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.taxableThresholdAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '1' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax entire amount when equal or greater)"/>
											
											<h:inputText id="cityminimumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.minimumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '2' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or over amount)"/>
											
											<h:inputText id="citymaximumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '3' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or under amount)"/>
											
											<h:inputText id="citymaximumTaxAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.maxTaxAmountOption != '4' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(highest allowed calculated tax amount)"/>
											
											<h:selectOneMenu id="citygroupByDriver" value="#{taxCodeBackingBean.updateTaxCodeRule.groupByDriver}"
												disabled="#{!taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
												<f:selectItems id="citygroupByDriverItms" value="#{taxCodeBackingBean.groupByDriverItems}" />
											</h:selectOneMenu>
											<h:outputText value="&#160;"/>
											
										</h:panelGrid>
									</h:panelGrid>
								</h:panelGroup>
							</td>
							<td style="width:50%;">&#160;</td>
						</tr>		
				
					</tbody>
					</table>
			 	</div>	
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="cityPercentofBase" disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.percentBaseChecked}" >
									<a4j:support id="cityddsupport7" event="onclick" reRender="percentofBaseId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Percent of Base:</td>
							<td style="width:100px;">
								<h:inputText id="citypercentofBaseId" value="#{taxCodeBackingBean.updateTaxCodeRule.baseChangePct}" style="text-align: right;"
			                		disabled="#{!taxCodeBackingBean.percentBaseChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax only a portion of the base amount)&#160;</td>
							 <td style="width:5%;">(enter .01 through .99)</td> 
               				 <td style="width:30%;">&#160;</td>
						</tr>
					</tbody>
					</table>
		 		</div>
				</td>
			</tr>
			
		</tbody>
	</table>				
	</rich:simpleTogglePanel>	
	
	</div>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCodeRule.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCodeRule.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" disabled="#{empty taxCodeBackingBean.selectedTaxCode.taxCodePK}" action="#{taxCodeBackingBean.okTaxCodeRuleAction}" 
		onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{taxCodeBackingBean.taxCodeRulesViewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>

<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>
<script type="text/javascript">
	function openTab(evt, tabName) {
		tabcontent = document.getElementsByClassName("ruleTabContent");
		
		for(i = 0; i &lt; tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		
		tablinks = document.getElementsByClassName("tablinks");
		for(i = 0; i &lt; tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" activeTab", "");
		}
		
		document.getElementById(tabName).style.display="block";
		evt.currentTarget.className += " activeTab";
		
	}
</script>
</ui:define>


</ui:composition>
</html>
