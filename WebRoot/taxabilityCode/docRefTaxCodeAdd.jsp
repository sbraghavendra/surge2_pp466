<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<!-- //fixed for issue 0004550 -->
<script type="text/javascript">
//<![CDATA[
function myFunc() {
 var check =document.getElementById("detailForm:txtRefDocCode").value;
            document.getElementById("detailForm:NameCheck").value=check;
}
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:form id="detailForm">
<h:inputHidden id="NameCheck" value="Default"/>
<h1><h:graphicImage id="imgTaxabilityCodes" alt="TaxCode Reference Docs" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblReferenceDocuments" value="#{supportingCodeBackingBean.actionText}"/> a Reference Document<h:outputText value="#{supportingCodeBackingBean.deleteAction? ' from ':' to '}"/>TaxCode</td></tr>
		</thead>
		<tbody>
		    <tr>
				<td style="width:50px;">&#160;</td>
				<td>TaxCode Country:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeCountryCode}" 
					 	 disabled="true" id="txtCountry"
				         style="width:300px;"/>
				<h:inputText value="#{supportingCodeBackingBean.countryMap[supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeCountryCode]}"
					 	 disabled="true" id="txtCountryName"
				         style="width:344px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>TaxCode State:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeStateCode}" 
					 	 disabled="true" id="txtState"
				         style="width:300px;"/>
				<h:inputText value="#{supportingCodeBackingBean.stateMap[supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeStateCode].name}"
					 	 disabled="true" id="txtStateName"
				         style="width:344px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>TaxCode Type:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeTypeCode}" 
					 	 disabled="true" id="txtTaxcodeTypeCode"
				         style="width:300px;"/>
				<h:inputText value="#{supportingCodeBackingBean.taxCodeTypeMap[supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeTypeCode].description}"
					 	 disabled="true" id="txtTaxcodeTypeDescription"
				         style="width:344px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>TaxCode:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeCode}" 
					 	 disabled="true" id="txtTaxcodeCode"
				         style="width:300px;"/>
				<h:inputText value="#{supportingCodeBackingBean.taxCodeMap[supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.taxcodeDetail.taxcodeCode].description}"
					 	 disabled="true" id="txtTaxcodeDescription"
				         style="width:344px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Name:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.refDoc.refDocCode}" 
					 	 disabled="#{supportingCodeBackingBean.deleteAction}"
						required="true" label="Name" id="txtRefDocCode"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         style="width:300px;" 
				         maxlength="50"/>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.refDoc.description}" 
					 	 disabled="true" id="txtRefDocDescription"
				         style="width:344px;"/>
				<a4j:commandButton id="#{id}_search" styleClass="image"
						image="/images/search_small.png" 
						rendered="#{!taxabilityTaxCodeBackingBean.deleteAction}" 
						onclick="javascript:myFunc();"
						immediate="true"
						actionListener="#{supportingCodeBackingBean.popupDocRefSearchAction}"
						oncomplete="javascript:Richfaces.showModalPanel('docRefSearch');" 
						reRender="searchForm">
				</a4j:commandButton>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Type:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.refDoc.refTypeCode}" 
					 	 disabled="true" id="txtRefTypeCode"
				         style="width:300px;"/>
				<h:inputText value="#{supportingCodeBackingBean.refTypeMap[supportingCodeBackingBean.selectedDocRefDetail.referenceDetailPK.refDoc.refTypeCode].description}" 
					 	 disabled="true" id="txtRefTypeDescription"
				         style="width:344px;"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{supportingCodeBackingBean.selectedDocRefDetail.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{supportingCodeBackingBean.okDocRefDetailAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{supportingCodeBackingBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>

<ui:include src="/WEB-INF/view/components/docref_search.xhtml">
	<ui:param name="handler" value="#{supportingCodeBackingBean.docRefHandler}"/>
	<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
	<ui:param name="reRender" value="detailForm"/>
</ui:include>

</ui:define>

</ui:composition>

</html>