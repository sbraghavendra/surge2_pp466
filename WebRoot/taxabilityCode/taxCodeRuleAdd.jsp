<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { displayBackdateWarning(); } );
registerEvent(window, "load", function() { displayBackdateExistWarning(); } );

function displayBackdateExistWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 1) {
	 	javascript:Richfaces.showModalPanel('backdateexistwarning');
	 }
}

function displayBackdateWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 2) {
	 	javascript:Richfaces.showModalPanel('backdatewarning');
	 }
}

function resetAmounts() {
	if(document.getElementById('detailForm:taxableThresholdAmtId').value != 0){
		document.getElementById('detailForm:taxableThresholdAmtId').value = "0.00";
	}
	if(document.getElementById('detailForm:minimumTaxableAmtId').value != 0){
		document.getElementById('detailForm:minimumTaxableAmtId').value = "0.00";
	}
	if(document.getElementById('detailForm:maximumTaxableAmtId').value != 0){
		document.getElementById('detailForm:maximumTaxableAmtId').value = "0.00";
	}
	if(document.getElementById('detailForm:maximumTaxAmtId').value != 0){
		document.getElementById('detailForm:maximumTaxAmtId').value = "0.00";
	}
	
}

//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="displayBackdateWarning" immediate="true" value="#{taxCodeBackingBean.displayBackdateWarning}"/>
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
				<td colspan="3"><h:outputText id="lblTaxcodeMaint" value="#{taxCodeBackingBean.actionText}"/> a TaxCode Rule</td>
				<td colspan="5" style="text-align:right;">Rule ID:&#160;<h:outputText id="ruleId" value="#{taxCodeBackingBean.updateTaxCodeRule.taxcodeDetailId==null ? '*NEW':taxCodeBackingBean.updateTaxCodeRule.taxcodeDetailId}"/>&#160;
																		<h:outputText id="ruleId1" value="#{taxCodeBackingBean.ruleUsed ? '(Used on Trxn)':''}"/>&#160;</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="8">&#160;TaxCode</th>
			</tr>
			<tr>
				<td style="width:3px;">&#160;</td>
				<td  class="column-label" >TaxCode:</td>
                <td >
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode}" 
						required="true" label="TaxCode" id="txtTaxcodeCode"
						validator="#{taxCodeBackingBean.validateTaxCode}"
					 	 disabled="true"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="255" />
                </td>
				<td style="width:10px;">&#160;</td>  
				<td style="width:10px;">&#160;</td>  
				<td style="width:10px;">&#160;</td>
			    <td style="width:20%;">&#160;</td>
				<td style="width:10px;">&#160;</td>

			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td colspan="4">
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.description}" 
						required="true" label="Description" id="txtDescription"
					 	 disabled="true"
				         style="width:650px;" 
				         maxlength="120"/>
                </td>
                <td style="width:10%;">&#160;</td>
                  <td style="width:10%;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Comment:</td>
				<td colspan="4">
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.comments}" 
					 	 disabled="true"
				         style="width:650px;" id="txtComments"
				         maxlength="3000"/>
                </td>
                <td style="width:10%;">&#160;</td>
                  <td style="width:10%;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td colspan="4">
				<h:selectBooleanCheckbox id="chkActive" value="#{taxCodeBackingBean.selectedTaxCode.activeBooleanFlag}" 
					disabled="true" styleClass="check"/>
                </td>
			</tr>
		</tbody>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
           <tbody>
			<tr>
				<th colspan="8">&#160;Jurisdiction</th>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label" >Level:</td>
				<td>		
						<h:selectOneMenu id="ddlLevel" binding="#{taxCodeBackingBean.updateHandler.levelInput}" value="#{taxCodeBackingBean.updateHandler.level}"
								disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlLevelItms" value="#{taxCodeBackingBean.updateHandler.levelItems}" />
							<a4j:support id="ddsupport8" event="onchange" reRender="ddlCountry,ddlState,ddlCounty,ddlCity,ddlStj"
								immediate="true" action="#{taxCodeBackingBean.levelSelectedAction}" actionListener="#{taxCodeBackingBean.updateHandler.levelSelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:50px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:20%;">&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Country:</td>
				<td>		
						<h:selectOneMenu id="ddlCountry" binding="#{taxCodeBackingBean.updateHandler.countryInput}" value="#{taxCodeBackingBean.updateHandler.country}"
							disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateHandler.disableCountry or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlCountryItms" value="#{taxCodeBackingBean.updateHandler.countryItems}" />
							<a4j:support id="ddsupport9" event="onchange" reRender="ddlState,ddlCounty,ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.updateHandler.countrySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:50px;">&#160;</td>
				<td>State:</td>	
				<td>	
						<h:selectOneMenu id="ddlState" binding="#{taxCodeBackingBean.updateHandler.stateInput}" value="#{taxCodeBackingBean.updateHandler.state}"
							disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateHandler.disableState or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlStateItms" value="#{taxCodeBackingBean.updateHandler.stateItems}" />
							<a4j:support id="ddsupport10" event="onchange" reRender="ddlCounty,ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.updateHandler.stateSelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10%;">&#160;</td>
				<td style="width:20%;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">County:</td>		
				<td>
						<h:selectOneMenu id="ddlCounty" binding="#{taxCodeBackingBean.updateHandler.countyInput}" value="#{taxCodeBackingBean.updateHandler.county}"
							disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateHandler.disableCounty or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlCountyItms" value="#{taxCodeBackingBean.updateHandler.countyItems}" />
							<a4j:support id="ddsupport" event="onchange" reRender="ddlCity,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.updateHandler.countySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:50px;">&#160;</td>
				<td>City:</td>
				<td>		
						<h:selectOneMenu id="ddlCity" binding="#{taxCodeBackingBean.updateHandler.cityInput}" value="#{taxCodeBackingBean.updateHandler.city}"
							disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateHandler.disableCounty or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlCityItms" value="#{taxCodeBackingBean.updateHandler.cityItems}" />
							<a4j:support id="ddsupport1" event="onchange" reRender="ddlCounty,ddlStj"
								immediate="true" actionListener="#{taxCodeBackingBean.updateHandler.citySelected}"/>
						</h:selectOneMenu>
				</td>
				<td style="width:10%;">&#160;</td>
				<td style="width:20%;">&#160;</td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">STJ</td>		
				<td colspan="4">
						<h:selectOneMenu id="ddlStj" binding="#{taxCodeBackingBean.updateHandler.stjInput}" value="#{taxCodeBackingBean.updateHandler.stj}"
							disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateHandler.disableCounty or taxCodeBackingBean.updateAction or taxCodeBackingBean.copyUpdateAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" >
							<f:selectItems id="ddlStjItms" value="#{taxCodeBackingBean.updateHandler.stjItems}" />
							<a4j:support id="ddsupport2" event="onchange" reRender="ddlCity,ddlCounty"
								immediate="true" actionListener="#{taxCodeBackingBean.updateHandler.stjSelected}"/>
						</h:selectOneMenu>
				</td>
				
				<td style="width:10%;">&#160;</td>
				<td style="width:20%;">&#160;</td>
			</tr>

			<tr>
				<th colspan="8">&#160;Rule</th>
			</tr>
			
		</tbody>
	</table>
	
	<rich:simpleTogglePanel id="TaxabilityRulesPanel"   switchType="ajax" label="Taxability Rules" opened="true"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Effective Date:</td>
				<td>
					<rich:calendar
						popup="true"  enableManualInput="true" 
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction or taxCodeBackingBean.deleteAction}"
						value="#{taxCodeBackingBean.updateTaxCodeRule.effectiveDate}"
						converter="date"
						datePattern="M/d/yyyy"
						showApplyButton="false" id="effDate" />
                </td>
                <td style="width:10px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Taxability:</td>
				<td>
					<h:selectOneMenu id="taxability" value="#{taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="taxabilityItms" value="#{taxCodeBackingBean.taxabilityItems}" />
						<a4j:support event="onclick" id="taxabilitySupport" actionListener="#{taxCodeBackingBean.taxabilityChangeAction}"
							reRender="taxType,rateType,DistributionRulesPanel,allocBucket,allocProrateByCode,allocConditionCode,exemptReason"  />
					</h:selectOneMenu>
                </td>
                <td style="width:50px;">&#160;</td>
                <td style="width:50px;">&#160;</td>
                
				<td class="column-label">Good/Svc Type:</td>
				<td>
					<h:selectOneMenu id="goodSvcType" value="#{taxCodeBackingBean.updateTaxCodeRule.goodSvcTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="serviceTypeItms" value="#{taxCodeBackingBean.serviceTypeItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Tax Type:</td>
				<td style="width:100px;" >
					<h:selectOneMenu id="taxType" value="#{taxCodeBackingBean.updateTaxCodeRule.taxtypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="taxTypeItms" value="#{taxCodeBackingBean.taxTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Situs</td>
                <td style="width:50px;">&#160;</td>
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Purchasing Rate Type:</td>
				<td style="width:100px;">
					<h:selectOneMenu id="rateType" value="#{taxCodeBackingBean.updateTaxCodeRule.rateTypeCode}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="rateTypeItms" value="#{taxCodeBackingBean.rateTypeItems}" />
					</h:selectOneMenu>
                </td>
                <td style="width:200px; color:red">&#160;&#160;*Sales: Determined by Transaction</td>
                <td style="width:50px;">&#160;</td>
                <td class="column-label">Exempt Reason:</td>
				<td>
					<h:selectOneMenu id="exemptReason" value="#{taxCodeBackingBean.updateTaxCodeRule.exemptReason}"
						disabled="#{taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction or taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode != 'E'}">
						<f:selectItems id="exemptReasonItms" value="#{taxCodeBackingBean.exemptReasonItems}" />
					</h:selectOneMenu>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Citation Reference:</td>
				<td colspan="5">
					<h:inputText value="#{taxCodeBackingBean.updateTaxCodeRule.citationRef}" 
					 	 disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
				         style="width:650px;" id="txtcitationRef"
				         maxlength="50"/>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
					<h:selectBooleanCheckbox id="chkTdActive" value="#{taxCodeBackingBean.updateTaxCodeRule.activeBooleanFlag}" 
						disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" styleClass="check"/>
                </td>             
                <td style="width:50px;">&#160;</td>    
                <td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
				<td style="width:10px;">&#160;</td>
			</tr>
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	<rich:simpleTogglePanel id="OverrideTaxRatesRulesPanel"   switchType="ajax" label="Override Tax Rates Rules" opened="#{taxCodeBackingBean.isOverride}"
		bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
			<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
			<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>		
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
						<td style="width:13%;">Tax Processing Type:</td>
						<td style="width:87%;text-align:left;"> 
							<h:selectOneMenu id="taxProcessTypeCode" value="#{taxCodeBackingBean.updateTaxCodeRule.taxProcessTypeCode}"
												disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
								<f:selectItems id="taxProcessTypeCodeItms" value="#{taxCodeBackingBean.processTypeItems}" />
							</h:selectOneMenu>
							<h:outputText value="&#160;"/>
						</td>
						<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;border-bottom: none;">&#160;</td>
				<td colspan="5" style = "border-bottom: none;">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="SpecialRateSetAmountId" disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.specialRateChecked}" >
									<a4j:support id="ddsupport4" event="onclick" reRender="specialRateId,specialSetAmtId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Special Rate or Set Amount:</td>
							<td style="width:100px;">
								<h:inputText id="specialRateId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialRate}" style="text-align: right;"
			                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or !taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td style="width:15px;">&#160;or&#160;</td>
							<td style="width:100px;">
								<h:inputText id="specialSetAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.specialSetAmt}" style="text-align: right;"
			                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or !taxCodeBackingBean.specialRateChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax at a rate or set amount different from the Jurisdiction)&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="width:15px;">&#160;</td>
							<td style="width:158px;">&#160;</td>
							<td style="width:150px;">(enter .01 through .99)</td>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">&#160;</td>
							<td>&#160;</td>
							<td style="width:10%;">&#160;</td>
						</tr>
					</tbody>
					</table>
					</div>
				</td>
			</tr>
			
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
				  	<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px;">
								<h:selectBooleanCheckbox id="MaxtaxAmount" disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.maxTaxAmountChecked}" >
									<a4j:support id="ddsupport5" event="onclick" reRender="maxTaxAmountRadio,taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId,taxProcessTypeCode,groupByDriver" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">&#160;MaxTax Amount:</td>
							<td style="width:50%;">&#160;</td>	
						</tr>
						
						<tr>
							<td style="width:10px;">&#160;</td>
							<td style="width:100px;">
								<h:panelGroup >
									<h:panelGrid columns="2"> 		
										<h:panelGrid columns="1" style="height:125px;margin:0px 0px 14px 0px;"> 
											<h:selectOneRadio id="maxTaxAmountRadio" disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" disabledClass="selectOneRadio_Disabled" styleClass="selectOneRadio" layout="pageDirection" value="#{taxCodeBackingBean.maxTaxAmountOption}" >	
												<f:selectItem itemValue="1" itemLabel="&#160;Taxable Threshold Amount:"/>
												<f:selectItem itemValue="2" itemLabel="&#160;Minimum Taxable Amount:"/> 
												<f:selectItem itemValue="3" itemLabel="&#160;Maximum Taxable Amount:"/>
												<f:selectItem itemValue="4" itemLabel="&#160;Maximum Tax Amount:"/>
												
												<a4j:support id="ddsupport6" event="onclick" 
                        							ajaxSingle="true" reRender="taxableThresholdAmtId,minimumTaxableAmtId,maximumTaxableAmtId,maximumTaxAmtId"
                        							oncomplete="resetAmounts()" />
											</h:selectOneRadio>
											<h:outputText value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Group By Driver:"/>
										</h:panelGrid>
										<h:panelGrid columns="2" style="height:125px; margin:0px 0px 7px 0px"> 
											<h:inputText id="taxableThresholdAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.taxableThresholdAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.maxTaxAmountOption != '1' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax entire amount when equal or greater)"/>
											
											<h:inputText id="minimumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.minimumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.maxTaxAmountOption != '2' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or over amount)"/>
											
											<h:inputText id="maximumTaxableAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxableAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.maxTaxAmountOption != '3' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(tax only equal or under amount)"/>
											
											<h:inputText id="maximumTaxAmtId" value="#{taxCodeBackingBean.updateTaxCodeRule.maximumTaxAmt}" style="text-align: right;"
						                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.maxTaxAmountOption != '4' or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
						                		onkeypress="return onlyNumerics(event);">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:inputText>
											<h:outputText value="(highest allowed calculated tax amount)"/>
											
											<h:selectOneMenu id="groupByDriver" value="#{taxCodeBackingBean.updateTaxCodeRule.groupByDriver}"
												disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or !taxCodeBackingBean.maxTaxAmountChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
												<f:selectItems id="groupByDriverItms" value="#{taxCodeBackingBean.groupByDriverItems}" />
											</h:selectOneMenu>
											<h:outputText value="&#160;"/>
											
										</h:panelGrid>
									</h:panelGrid>
								</h:panelGroup>
							</td>
							<td style="width:50%;">&#160;</td>
						</tr>		
				
					</tbody>
					</table>
			 	</div>	
				</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td colspan="5">
					<div id="embedded-table">
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
					<tbody>
						<tr>
							<td style="vertical-align:top;padding-left:0px">
								<h:selectBooleanCheckbox id="PercentofBase" disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}" 
									styleClass="check" value="#{taxCodeBackingBean.percentBaseChecked}" >
									<a4j:support id="ddsupport7" event="onclick" reRender="percentofBaseId" />
								</h:selectBooleanCheckbox>
							</td>	
							<td style="width:150px;">Percent of Base:</td>
							<td style="width:100px;">
								<h:inputText id="percentofBaseId" value="#{taxCodeBackingBean.updateTaxCodeRule.baseChangePct}" style="text-align: right;"
			                		disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or !taxCodeBackingBean.percentBaseChecked or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}"
			                		onkeypress="return onlyNumerics(event);">
										<f:convertNumber minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
								</h:inputText>
							</td>
							<td>&#160;(tax only a portion of the base amount)&#160;</td>
							 <td style="width:5%;">(enter .01 through .99)</td> 
               				 <td style="width:30%;">&#160;</td>
						</tr>
					</tbody>
					</table>
		 		</div>
				</td>
			</tr>
			
		</tbody>
	</table>		
	</rich:simpleTogglePanel>	
	
	<rich:simpleTogglePanel id="DistributionRulesPanel"   switchType="ajax" label="Distribution Rule (Taxability = &#34;Distribute&#34;)" opened="#{taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode == 'D'}"
			bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
				<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
				<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>		
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>	
					<h:outputText value="Distribution Bucket:"/>
				</td>
				<td>
					<h:selectOneMenu id="allocBucket" value="#{taxCodeBackingBean.updateTaxCodeRule.allocBucket}"
						disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode != 'D' or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="allocBucketItems" value="#{taxCodeBackingBean.allocationBucketItems}" />
					</h:selectOneMenu>
				</td>
				<td>
					<h:outputText value="(put the distributed amount in this bucket)"/>
				</td>
				<td>&#160;</td>
				<td style="width:5%;">&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>	
				<td>		
					<h:outputText value="Distribute By:"/>
				</td>
				<td>
					<h:selectOneMenu id="allocProrateByCode" value="#{taxCodeBackingBean.updateTaxCodeRule.allocProrateByCode}"
						disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode != 'D' or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="allocateByItems" value="#{taxCodeBackingBean.allocateByItems}" />
					</h:selectOneMenu>
				</td>
				<td>
					<h:outputText value="(use this value to determine the prorate percentage to distribute)"/>
				</td>
				<td>&#160;</td>
				<td style="width:5%;">&#160;</td>
			</tr>
			
			<tr>		
				<td style="width:50px;">&#160;</td>			
				<td>	
					<h:outputText value="Distribute Condition:"/>
				</td>
				<td>
					<h:selectOneMenu id="allocConditionCode" value="#{taxCodeBackingBean.updateTaxCodeRule.allocConditionCode}"
						disabled="#{taxCodeBackingBean.updateHandler.disableForSTJ or taxCodeBackingBean.updateTaxCodeRule.taxcodeTypeCode != 'D' or taxCodeBackingBean.readOnlyAction or (taxCodeBackingBean.updateAction and taxCodeBackingBean.ruleUsed) or taxCodeBackingBean.deleteAction or taxCodeBackingBean.backdateAction}">
						<f:selectItems id="allocateConditionItems" value="#{taxCodeBackingBean.allocateConditionItems}" />
					</h:selectOneMenu>
				</td>
				<td>
					<h:outputText value="(distribute to only transactions of this type)"/>
				</td>
				<td>&#160;</td>
				<td style="width:5%;">&#160;</td>
			</tr>

		</tbody>
	</table>
	</rich:simpleTogglePanel>
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td colspan="4">
				<h:inputText value="#{taxCodeBackingBean.updateTaxCodeRule.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:606px;" />
                </td>
                <td style="width:10%;">&#160;</td>
                <td style="width:20%;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td colspan="4">
				<h:inputText value="#{taxCodeBackingBean.updateTaxCodeRule.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:606px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
                <td style="width:10%;">&#160;</td>
                <td style="width:20%;">&#160;</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="copy-update"><a4j:commandLink id="copyUpdateRule" rendered="#{taxCodeBackingBean.updateAction}" immediate="true" 
				action="#{taxCodeBackingBean.taxCodeRuleCopyUpdateAction}" />
		</li>
		<li class="ok-add-4"><h:commandLink id="okAddRule" action="#{taxCodeBackingBean.okAddTaxCodeRuleAction}" rendered="#{taxCodeBackingBean.addAction}" /></li>
		<li class="ok"><h:commandLink id="btnOk" rendered="#{!taxCodeBackingBean.backdateAction}" action="#{taxCodeBackingBean.okTaxCodeRuleAction}" /></li>
		<li class="ok"><h:commandLink id="okBackdate" rendered="#{taxCodeBackingBean.backdateAction}" action="#{taxCodeBackingBean.backdateAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{taxCodeBackingBean.viewOnlyAction}" immediate="true" action="#{taxCodeBackingBean.taxCodeRulesViewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{taxCodeBackingBean}"/>
	<ui:param name="id" value="backdatewarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than today. Proceed?"/>
	<ui:param name="okBtn" value="detailForm:okBackdate"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{taxCodeBackingBean}"/>
	<ui:param name="id" value="backdateexistwarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than an existing Rule. Proceed?"/>
	<ui:param name="okBtn" value="detailForm:okBackdate"/>
</ui:include>

</ui:define>
</ui:composition>
</html>