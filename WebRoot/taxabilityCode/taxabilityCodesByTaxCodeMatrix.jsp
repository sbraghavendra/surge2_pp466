<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:taxCodeDefTable', 'selectedTaxCodeIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:docrefTable', 'selectedDocRefDetailIndex'); } );

registerEvent(window, "load", function popup () {	
	if(document.getElementById('url').value != null ){
		lo_window = window.open (document.getElementById('url').value, "popup");
		lo_window.focus();
		document.getElementById('url').value = null;
	}
}
);
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedTaxCodeIndex" value="#{supportingCodeBackingBean.selectedTaxCodeIndex}"/>
<h:inputHidden id="selectedDocRefDetailIndex" value="#{supportingCodeBackingBean.selectedDocRefDetailIndex}"/>

<h:inputHidden id="url" value="#{supportingCodeBackingBean.url}" renderd="#{!supportingCodeBackingBean.disableViewButton}"/>

<h:form id="taxabilityForm">

<h1><img id="imgTaxabilityCodes" alt="Taxability Codes by TaxCode" src="../images/headers/hdr-taxability-codes.gif" width="250" height="19" /></h1>
<div class="tab"><img src="../images/containers/STSSelection-filter-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="898" id="rollover" class="ruler">
        <tr>
            <td style="width: 10px;">&#160;</td>
			<td>
				<h:selectOneMenu id="SelView" title="Select View Type"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedView}">
					<f:selectItems id="selViewItms" value="#{supportingCodeBackingBean.viewItems}" />
					<a4j:support event="onchange" action="#{supportingCodeBackingBean.viewSubmitted}" />
				</h:selectOneMenu>
            </td>
        </tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="selFirstCombo"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedFirstComboValue}"  
					title="Select TaxCode" 
					valueChangeListener="#{supportingCodeBackingBean.firstComboValueChanged}" >
					<f:selectItem id="selFirstComboItm" itemLabel="Select a TaxCode" itemValue="0"/>
					<f:selectItems id="selFirstComboItms" value="#{supportingCodeBackingBean.firstComboItems}" />
					
					<a4j:support ajaxSingle="true" event="onchange" reRender="selSecondCombo,selThirdCombo,addBtn,updBtn,delBtn,viewBtn,url" action="taxability_taxcode" />
				</h:selectOneMenu>
			</td>
		</tr>
        <tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 40px;">
				<h:selectOneMenu id="selSecondCombo"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedSecondComboValue}"  
					title="Select Item" 
					valueChangeListener="#{supportingCodeBackingBean.secondComboValueChanged}" >
					<f:selectItem id="selSecondComboItm" itemLabel="Select a TaxCode Type" itemValue="0"/>
					<f:selectItems id="selSecondComboItms" value="#{supportingCodeBackingBean.secondComboItems}" />
					
					<a4j:support ajaxSingle="true" event="onchange" reRender="selThirdCombo,addBtn,updBtn,delBtn,viewBtn,url" action="taxability_taxcode" />
				</h:selectOneMenu>
			</td>
		</tr>
        <tr>
			<td style="width: 10px;">&#160;</td>
			<td style="padding-left: 60px;">
				<h:selectOneMenu id="selThirdCombo"
					style="width: 288px;" value="#{supportingCodeBackingBean.selectedThirdComboValue}"  
					title="Select Exempt Category" 
					valueChangeListener="#{supportingCodeBackingBean.thirdComboValueChanged}" >
					<f:selectItem id="selThirdComboItm" itemLabel="Select a TaxCode State" itemValue="0"/>
					<f:selectItems id="selThirdComboItms" value="#{supportingCodeBackingBean.thirdComboItems}" />
					
					<a4j:support ajaxSingle="true" event="onchange" reRender="addBtn,updBtn,delBtn,viewBtn,url" action="taxability_taxcode" />
				</h:selectOneMenu>
			</td>
		</tr>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right"></ul></div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>

<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top"></div>
	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner">

	<ui:include src="/WEB-INF/view/components/taxcode_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.taxCodeList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.viewChange}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="taxCodeDefTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/listcode_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.taxCodeTypeList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.firstComboValueChange}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="typeTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/state_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.taxCodeStateList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.secondComboValueChange}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="typeStateTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>

	<ui:include src="/WEB-INF/view/components/docref_detail_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docRefList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.thirdComboValueChange}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="docrefTable"/>
		<ui:param name="reRender" value="updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	</div>
	</div>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="addBtn" disabled="#{supportingCodeBackingBean.disableAddButton}" action="#{supportingCodeBackingBean.displayAddAction}" /></li>
		<li class="update"><h:commandLink id="updBtn" disabled="#{supportingCodeBackingBean.disableUpdateButton}" action="#{supportingCodeBackingBean.displayUpdateAction}" /></li>	
		<li class="delete"><h:commandLink id="delBtn" disabled="#{supportingCodeBackingBean.disableDeleteButton}" action="#{supportingCodeBackingBean.displayDeleteAction}" /></li>
		<li class="view"><h:commandLink id="viewBtn" disabled="#{supportingCodeBackingBean.disableViewButton}" action="#{supportingCodeBackingBean.displayViewAction}" /></li>
	</ul>
	</div>
	</div>
</div>

</h:form>
</ui:define>

</ui:composition>

</html>
