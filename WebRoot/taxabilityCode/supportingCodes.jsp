<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
	registerEvent(window, "load", function() { selectRowByIndex('taxabilityForm:docrefTable', 'selectedDocRefIndex'); } );
	registerEvent(window, "unload", function() { document.getElementById('taxabilityForm:hiddenChangeLink').onclick(); });
	registerEvent(window, "load", function popup () {
		if(document.getElementById('url').value != null && document.getElementById('url').value != ''){
			lo_window = window.open (document.getElementById('url').value, "popup");
			lo_window.focus();
			document.getElementById('url').value = null;
		}
	});
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:inputHidden id="selectedDocRefIndex" value="#{supportingCodeBackingBean.selectedDocRefIndex}"/>
<h:inputHidden id="url" value="#{supportingCodeBackingBean.url}" rendered="#{supportingCodeBackingBean.viewEnabled}"/>
<h:form id="taxabilityForm">

<a4j:commandLink id="hiddenChangeLink" style="visibility:hidden;display:none" >
	<a4j:support event="onclick" immediate="true" actionListener="#{supportingCodeBackingBean.urlResetAction}" />
</a4j:commandLink>

<h1><h:graphicImage id="imgTaxabilityCodes" alt="Supporting Codes" value="/images/headers/hdr-supporting-codes.gif" /></h1>
<div class="tab"><h:graphicImage value="/images/containers/STSSelection-filter-open.gif" /></div>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height: 100px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tr>
			<td style="width: 100px;">&#160;Supporting Code:</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="supportingCode"
					style="width: 208px;" value="#{supportingCodeBackingBean.selectedSupportingCode}"  
					immediate="true" title="Select Supporting Code"
					valueChangeListener="#{supportingCodeBackingBean.supportingCodeSelectionChanged}" 
					onchange="submit();">
					<f:selectItem id="selFirstComboItm0" itemLabel="Select a Supporting Code" itemValue="0"/>
					<f:selectItem id="selFirstComboItm2" itemLabel="Countries" itemValue="2"/>
					<f:selectItem id="selFirstComboItm5" itemLabel="States" itemValue="5"/>
					<f:selectItem id="selFirstComboItm3" itemLabel="Reference Documents" itemValue="3"/>
					<f:selectItem id="selFirstComboItm4" itemLabel="Reference Types" itemValue="4"/>
				</h:selectOneMenu>
			</td>
		</tr>
		<c:if test="#{supportingCodeBackingBean.displayRefTypeForFilter}">
		<tr>
			<td style="width: 100px;">&#160;Ref. Type:</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="refType"
					style="width: 208px;" value="#{supportingCodeBackingBean.selectedRefType}"  
					immediate="true" title="Select TaxCode">
					<f:selectItems id="selRefTypeComboItms" value="#{supportingCodeBackingBean.refTypeItems}" />
				</h:selectOneMenu>
			</td>
		</tr>
		</c:if>
		<c:if test="#{supportingCodeBackingBean.displayCodeEntryForFilter}">
		<tr>
			<td style="width: 100px;">&#160;Code:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="enteredCode" value="#{supportingCodeBackingBean.enteredCode}"  onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" />
			</td>
		</tr>
		</c:if>
		<c:if test="#{supportingCodeBackingBean.selectedSupportingCode == '5'}">
		<tr>
			<td style="width: 100px;">&#160;Country:</td>
			<td style="padding-left: 20px;">
				<h:selectOneMenu id="country"
					style="width: 208px;" value="#{supportingCodeBackingBean.enteredStateCountry}"  
					immediate="true" title="Select Country">
					<f:selectItems id="selCountryComboItms" value="#{supportingCodeBackingBean.countryItemsForFilter}" />
				</h:selectOneMenu>
			</td>
		</tr>
		<tr>
			<td style="width: 100px;">&#160;State Name:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="enteredState" value="#{supportingCodeBackingBean.enteredState}" />
			</td>
		</tr>
		</c:if>
		<c:if test="#{supportingCodeBackingBean.selectedSupportingCode == '2'}">
		<tr>
			<td style="width: 100px;">&#160;Country Name:</td>
			<td style="padding-left: 20px;">
				<h:inputText style="width: 200px;" id="enteredCountry" value="#{supportingCodeBackingBean.enteredCountry}" />
			</td>
		</tr>
		</c:if>
	</table>
	</div>
	<div id="table-one-bottom">
		<ul class="right">
			<li class="clear">
				<a4j:commandLink id="btnClear" action="#{supportingCodeBackingBean.resetFilter}" reRender="refType,enteredCode,enteredCountry,country,enteredState" />
			</li>
			<li class="search">
				<a4j:commandLink id="btnSearch" action="#{supportingCodeBackingBean.searchFilter}" reRender="taxabilityForm" />
			</li>
			
		</ul>
	</div>
	</div>
</div>
<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSSecurity-details-open.gif" />
	</span>
</div>
<div id="bottom" >
	<div id="table-four">
	<div id="table-four-top" >
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px;">
			<tbody>
				<tr>
				<td style="width:60px;">&#160;</td>
				<td align="left" >
					<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
					<a4j:status id="pageInfo" 
								startText="Request being processed..." 
								stopText=""
								onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
			     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
				</td>
				<td style="width:100;">&#160;</td>
				<td align="right" style="color:red; font-weight:bold;width:300;"><h:outputText id = "warningMessageId" value="#{supportingCodeBackingBean.displayWarningMessage}"  /></td>				
				<td style="width:50%;">&#160;</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="table-four-content">
	<div class="scrollContainer">
	<div class="scrollInner" id="resize">
	
	<ui:include src="/WEB-INF/view/components/country_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docCountryFilteredList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.country}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="countryTable"/>
		<ui:param name="reRender" value="viewCountryBtn,viewStateBtn,viewRefDocBtn,viewReftypeBtn,quickUpdateBtn,updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/state_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docStateFilteredList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.state}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="stateTable"/>
		<ui:param name="reRender" value="viewCountryBtn,viewStateBtn,viewRefDocBtn,viewReftypeBtn,quickUpdateBtn,updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/listcode_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docRefTypeFilteredList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.ref}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="itemTable"/>
		<ui:param name="reRender" value="viewCountryBtn,viewReftypeBtn,viewStateBtn,viewRefDocBtn,quickUpdateBtn,updBtn,delBtn,viewBtn,url"/>
	</ui:include>
	
	<ui:include src="/WEB-INF/view/components/docref_table.xhtml">
		<ui:param name="bean" value="#{supportingCodeBackingBean}"/>
		<ui:param name="data" value="#{supportingCodeBackingBean.docList}"/>
		<ui:param name="rendered" value="#{supportingCodeBackingBean.doc}"/>
		<ui:param name="formName" value="taxabilityForm"/>
		<ui:param name="id" value="docrefTable"/>
		<ui:param name="reRender" value="viewCountryBtn,viewStateBtn,viewRefDocBtn,viewReftypeBtn,quickUpdateBtn,updBtn,delBtn,viewBtn,url"/>
	</ui:include>
		
	</div>
	</div>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="quickUpdate"><h:commandLink id="quickUpdateBtn" disabled="#{supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag or !supportingCodeBackingBean.quickUpdateEnable}" action="#{supportingCodeBackingBean.displayQuickUpdateAction}" 
										  style="#{(!supportingCodeBackingBean.quickUpdateEnable or supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>								  
		<li class="add"><h:commandLink id="addBtn" disabled="#{supportingCodeBackingBean.selectedSupportingCode == '0' or supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{supportingCodeBackingBean.displaySupportingCodesAddAction}" 
									   style="#{(supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>
		<li class="update"><h:commandLink id="updBtn" disabled="#{!supportingCodeBackingBean.supportingCodesValidSelection or supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{supportingCodeBackingBean.displaySupportingCodesUpdateAction}" 
										  style="#{(supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"/></li>	
		<li class="delete"><h:commandLink id="delBtn" disabled="#{supportingCodeBackingBean.supportingCodesDisableDeleteButton or supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag}" action="#{supportingCodeBackingBean.displaySupportingCodesDeleteAction}"
										  style="#{(supportingCodeBackingBean.supportingCodesDisableDeleteButton or supportingCodeBackingBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" /></li>
		<li class="view"><h:commandLink id="viewCountryBtn" disabled="#{!supportingCodeBackingBean.isCountry or !supportingCodeBackingBean.supportingCodesValidSelection}" action="#{supportingCodeBackingBean.displaySupportingCodesViewAction}" /></li>
		<li class="view"><h:commandLink id="viewStateBtn" disabled="#{!supportingCodeBackingBean.isState or !supportingCodeBackingBean.supportingCodesValidSelection}" action="#{supportingCodeBackingBean.displaySupportingCodesViewAction}" /></li>
		<li class="view"><h:commandLink id="viewRefDocBtn" disabled="#{!supportingCodeBackingBean.isDoc or !supportingCodeBackingBean.supportingCodesValidSelection or !supportingCodeBackingBean.viewEnabled}" action="#{supportingCodeBackingBean.displayViewAction}" /></li>
		<li class="view"><h:commandLink id="viewReftypeBtn" disabled="#{!supportingCodeBackingBean.isRef or !supportingCodeBackingBean.supportingCodesValidSelection}" action="#{supportingCodeBackingBean.displayRefTypeViewAction}" /></li>
	</ul>
	</div>
	</div>
</div>

</h:form>
</ui:define>

</ui:composition>

</html>