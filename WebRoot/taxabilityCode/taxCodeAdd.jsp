<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblTaxcodeMaint" value="#{taxCodeBackingBean.actionText}"/> a TaxCode</td></tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="2">&#160;TaxCode</th>
				<th style="color:red;text-align:right;padding-right: 80px;"><h:outputText value="Copy Rules?:" rendered="#{taxCodeBackingBean.actionText =='Copy/Add'}"></h:outputText>
				<h:selectBooleanCheckbox id="chkCopyRules" value="#{taxCodeBackingBean.selectedTaxCode.copyRulesBooleanFlag}"
					 rendered="#{taxCodeBackingBean.actionText =='Copy/Add'}" styleClass="check"/>
                </th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">TaxCode:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.taxCodePK.taxcodeCode}" 
						required="true" label="TaxCode" id="txtTaxcodeCode"
						validator="#{taxCodeBackingBean.validateTaxCode}"
					 	 disabled="#{taxCodeBackingBean.readOnlyAction or taxCodeBackingBean.updateAction}"
				         style="width:650px;"
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="255" />
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Description:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.description}" 
						required="true" label="Description" id="txtDescription"
					 	 disabled="#{taxCodeBackingBean.readOnlyAction}"
				         style="width:650px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Comments:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.comments}" 
					 	 disabled="#{taxCodeBackingBean.readOnlyAction}"
				         style="width:650px;" id="txtComments"
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Active?:</td>
				<td>
				<h:selectBooleanCheckbox id="chkActive" value="#{taxCodeBackingBean.selectedTaxCode.activeBooleanFlag}" 
					disabled="#{taxCodeBackingBean.readOnlyAction}" styleClass="check"/>
                </td>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedTaxCode.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{taxCodeBackingBean.okTaxCodeAction}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" disabled="#{taxCodeBackingBean.viewOnlyAction}" immediate="true" action="#{taxCodeBackingBean.cancelTaxCodeAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>