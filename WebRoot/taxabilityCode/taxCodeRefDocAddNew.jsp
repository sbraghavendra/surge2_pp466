<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[

function callClick(){
	var val = document.detailForm.uploadFile.value;
	var ind = val.lastIndexOf('\\');
	document.getElementById('detailForm:docName').value = val.substring(ind+1,val.size);
	document.getElementById('detailForm:url').value = val.substring(0,ind);
}
function copyMe(from,to){
	var fromValue = document.getElementById(from).value;
	var lastIndex = fromValue.lastIndexOf("\\");
  	if(lastIndex > -1){	
    	document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    }
    else{
    	lastIndex = fromValue.lastIndexOf("//");
    	if(lastIndex > -1){	
    		document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    	}
    	else{
    		document.getElementById(to).value = fromValue;
    	}
    }
  	if(document.getElementById('detailForm:docName').value!=null && document.getElementById('detailForm:docName').value!=""){
  		document.getElementById('detailForm:url').disabled=true;
  	}else{
  		document.getElementById('detailForm:url').disabled=false;
  	}
  	
}
function disableDoc(){
	var url=document.getElementById('detailForm:url').value;
	if(url!=null && url!=""){
  		document.getElementById('detailForm:docName').disabled=true;
  		document.getElementById('detailForm:uploadFile').disabled=true;
  	}else{
  		document.getElementById('detailForm:uploadFile').disabled=false;
  		document.getElementById('detailForm:docName').disabled=false;
  	}
  	
}
function disableUrl(){
	var doc=document.getElementById('detailForm:docName').value;
	if(doc!=null && doc!=""){
  		document.getElementById('detailForm:url').disabled=true;
  	}else{
  		document.getElementById('detailForm:url').disabled=false;
  	}
  	
}

//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="detailForm" enctype="multipart/form-data">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Reference Doc Maint" value="/images/headers/hdr-taxability-codes.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText id="lblReferenceDocumentMaint" value=""/>Add a Reference Document</td></tr>
		</thead>
		<tbody>
		    <tr>
				<th colspan="3">Reference Document</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td><FONT color="red">*</FONT>Reference Code:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedReferenceDocument.refDocCode}" 
						required="true" label="Reference Code" id="txtRefDocCode"
						 style="width:650px;" 
				         onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)"
				         maxlength="40" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Type Code:</td>
				<td>
				<h:selectOneMenu 
						value="#{taxCodeBackingBean.selectedReferenceDocument.refTypeCode}"
						required="true" label="Type Code" id="ddlRefTypeCode" 
					 	 disabled="#{taxCodeBackingBean.deleteAction}">
				         <f:selectItems value="#{taxCodeBackingBean.refTypeList}"/>
				</h:selectOneMenu>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedReferenceDocument.description}" 
					 	 disabled="#{taxCodeBackingBean.deleteAction}"
				         style="width:650px;" id="txtDescription" 
				         maxlength="50"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Keywords:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedReferenceDocument.keyWords}" 
					 	 disabled="#{taxCodeBackingBean.deleteAction}"
				         style="width:650px;" id="txtKeywords" 
				         maxlength="100"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Document Name:</td>
                
                 <td>
	           		<h:inputText id="docName" value="#{taxCodeBackingBean.selectedReferenceDocument.docName}"  disabled="#{taxCodeBackingBean.deleteAction}"
					         style="width:650px;" maxlength="50" onchange="disableUrl()"/>
		        		        	<t:inputFileUpload id="uploadFile"
	            	binding="#{taxCodeBackingBean.uploadFile}" 
	            value="#{taxCodeBackingBean.uploadedFile}" 
	            immediate="true"
                storage="file"
                style="margin: 0px;font-size: 12px;border: 1px;width: 70px;"
                >
                <a4j:support event="onchange" 
                	ajaxSingle="true"                           
         			immediate="true" 
         			oncomplete="copyMe('detailForm:uploadFile','detailForm:docName')" reRender="btnOk,btnCancel"
                 />
              
            </t:inputFileUpload>
                </td>
         
			</tr>			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Or URL:</td>
				<td>
				<h:inputText id="url" value="#{taxCodeBackingBean.selectedReferenceDocument.url}" 
					 	 disabled="#{taxCodeBackingBean.deleteAction}"  
				         style="width:650px;" 
				         maxlength="255" onchange="disableDoc()"/>
               </td>
            </tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last User ID:</td>
				<td>
				<h:inputText id="txtUpdateUserId" value="#{taxCodeBackingBean.selectedReferenceDocument.updateUserId}" 
				         disabled="true" 
				         style="width:650px;" />
                </td>
            </tr>
            <tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Timestamp:</td>
				<td>
				<h:inputText value="#{taxCodeBackingBean.selectedReferenceDocument.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
            </tr>
            
		   </tbody>
		   </table>
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="btnOk" action="#{taxCodeBackingBean.refDocsOkAction}"  /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{taxCodeBackingBean.refDocsCancelAction}"  /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>