<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
           	registerEvent(window, "load", function() { selectRowByIndex('lookupRefDocForm:searchResultsTable', 'selectedRefDocIndex'); } );
           	registerEvent(window, "unload", function() { document.getElementById('lookupRefDocForm:hiddenLookupLink').onclick(); });
           	registerEvent(window, "load", function popup () {
        		if(document.getElementById('urlLookup').value != null && document.getElementById('urlLookup').value != ""){
        			lo_window = window.open (document.getElementById('urlLookup').value, "popup");
        			lo_window.focus();
        			document.getElementById('urlLookup').value = null;
        		}
        	});
//]]>
</script>
</ui:define>
<ui:define name="body">
<h:inputHidden id="selectedRefDocIndex" value="#{taxCodeBackingBean.docRefHandler.selectedRefDocIndex}"/>
<h:inputHidden id="urlLookup" value="#{taxCodeBackingBean.docRefHandler.urlLookup}" />
<h:form id="lookupRefDocForm">
<a4j:commandLink id="hiddenLookupLink" style="visibility:hidden;display:none" >
	<a4j:support event="onclick" immediate="true" actionListener="#{taxCodeBackingBean.docRefHandler.urlLookupResetAction}" />
</a4j:commandLink>
<h1><h:graphicImage id="imgTaxabilityCodes" alt="Taxability Codes by Type" 
    url="/images/headers/hdr-lookup-reference-documents.png"></h:graphicImage></h1>

<div class="tab">
<h:graphicImage id="image1" url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
 <a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
		reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{taxCodeBackingBean.togglePanelController.toggleHideSearchPanel}" >
	<h:outputText value="#{(taxCodeBackingBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
 </a4j:commandLink>		
</div>

<a4j:outputPanel id="showhidefilter">
  <c:if test="#{!taxCodeBackingBean.togglePanelController.isHideSearchPanel}">
	<div id="top">
	<div id="table-one">
     <a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
		<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/reference_documents_panel.xhtml">
			<ui:param name="handler" value="#{taxCodeBackingBean.docRefHandler}"/>
			<ui:param name="readonly" value="false"/>					
		</a4j:include>
     </a4j:outputPanel>
	</div>
	</div>
</c:if>
</a4j:outputPanel>
<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/hdr-View Ref Docs.png" />
	</span>
</div>
 <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
<div id="bottom">
	<div id="table-four">
		<div id="table-four-top">
		<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
		 <tbody>
		  <tr>
		 	 <td align="center" >
				<a4j:status id="pageInfo"
					startText="Request being processed..." 
					stopText="#{taxCodeBackingBean.docRefHandler.refDocLookupDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
   						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
   						
					
			 </td>  
			 <td style="width:20px;">&#160;</td>
		  </tr>
		 </tbody>
		</table>
		 
		</div>	
		<div id="table-four-content">
	      <div class="scrollContainer">
			<div class="scrollInner" id="resize" >				
			  <rich:dataTable rowClasses="odd-row,even-row" id="searchResultsTable" width="780px" 
				value="#{taxCodeBackingBean.docRefHandler.refDocLookupDataModel}" var="doc" style="text-align: left;"
				rows="#{taxCodeBackingBean.docRefHandler.refDocLookupDataModel.pageSize}">
				<a4j:support id="clickHandler" event="onRowClick" status="rowstatus"
					onsubmit="selectRow('lookupRefDocForm:searchResultsTable', this);"
					actionListener="#{taxCodeBackingBean.docRefHandler.selectItem}"
					reRender="okBtn,viewBtn"
					oncomplete="initScrollingTables();"
				/>
			
				<rich:column sortBy="#{doc.refDocCode}" >
					<f:facet name="header" ><h:outputText value="Ref. Doc. Code" style = "color:#336690" /></f:facet>
					<h:outputText id="rowRefDocCode" value="#{doc.refDocCode}"/>
				</rich:column>
				
				<rich:column sortBy="#{doc.refTypeCode}">
					<f:facet name="header"><h:outputText value="Reftype" style = "color:#336690"/></f:facet>
					<h:outputText id="rowRefTypeCode" value="#{doc.refTypeCode}" />
				</rich:column>
				
				<rich:column sortBy="#{doc.description}">
					<f:facet name="header"><h:outputText value="Description" style = "color:#336690" /></f:facet>
					<h:outputText id="rowDescription" value="#{doc.description}" />
				</rich:column>
				
				<rich:column sortBy="#{doc.keyWords}">
					<f:facet name="header"><h:outputText value="Keywords" style = "color:#336690" /></f:facet>
					<h:outputText id="rowKeywords" value="#{doc.keyWords}" />
				</rich:column>
				
				<rich:column sortBy="#{doc.docName}">
					<f:facet name="header"><h:outputText value="Document Name" style = "color:#336690"/></f:facet>
					<h:outputText id="rowDocname" value="#{doc.docName}" />
				</rich:column>
				
				<rich:column sortBy="#{doc.url}">
					<f:facet name="header"><h:outputText value="Path/Url" style = "color:#336690"/></f:facet>
					<h:outputText id="rowDocurl" value="#{doc.url}" />
				</rich:column>
				
			  </rich:dataTable>
			</div>
			
		  </div>
		  <rich:datascroller id="trScroll" for="searchResultsTable" maxPages="10" oncomplete="initScrollingTables();"
			 style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  
			 page="#{taxCodeBackingBean.docRefHandler.refDocLookupDataModel.curPage}" reRender="pageInfo" /> 
		</div>
		<div id="table-four-bottom">
			<ul class="right">
			<li class="ok"><h:commandLink id="okBtn" disabled="#{!taxCodeBackingBean.docRefHandler.validSearchResult}" actionListener="#{taxCodeBackingBean.docRefSelectedListener}" action="#{taxCodeBackingBean.docRefHandler.lookuprefDocOkAction}" /></li>
			<li class="cancel"><h:commandLink id="btnCancel" action="#{taxCodeBackingBean.docRefHandler.cancelAction}" /></li>
			<li class="view">
			   <h:commandLink id="viewBtn" disabled="#{!taxCodeBackingBean.docRefHandler.refDocSelected}" action="#{taxCodeBackingBean.displayViewAction}">
			     <f:param name="fromView" value = "lookupRefDocAction" />
			   </h:commandLink>
			</li>
		</ul>
		</div>
	</div>
</div> 

</h:form>
</ui:define>
</ui:composition>
</html>