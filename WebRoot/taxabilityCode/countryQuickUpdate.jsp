<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</ui:define>

<ui:define name="body">
<h:form id="detailForm">
<h1><h:graphicImage id="imgTaxabilityCodes" alt="States Maint" value="/images/headers/hdr-supporting-codes.gif" /></h1>
<div id="top">
	<div id="table-one">	
	<div id="table-four-top">
		<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
		<a4j:status id="pageInfo" 
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     
	</div>
	<div id="table-one-content">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
			<thead>
				<tr style="height:16px"><td colspan="3"><h:outputText id="CountryMaint" value="Quick Update - Country Active Flag" /></td></tr>
			</thead>
		</table>
	</div>
		
	<div id="table-four-content">
		<div class="scrollContainer">
			<div class="scrollInner" id="resize">	
				<rich:dataTable rowClasses="odd-row,even-row" id="countryTable" styleClass="GridContent"
					value="#{supportingCodeBackingBean.countryQuickUpdateList}" var="state">
					
					<rich:column sortBy="#{state.country}" >
						<f:facet name="header"><h:outputText value="Country" style="color: blue;"/></f:facet>
						<h:outputText id="rowCountry" value="#{state.country}" />
					</rich:column>
					
					<rich:column sortBy="#{state.name}" >
						<f:facet name="header"><h:outputText value="Name" style="color: blue;"/></f:facet>
						<h:outputText id="rowName" value="#{state.name}" />
					</rich:column>

					<rich:column style="text-align:center;width:50px;">
						<f:facet name="header">	
							<h:panelGroup id="headerid" >
								<h:outputText value="Active?" style="color:red;vertical-align:3px;"/>		
								<h:selectBooleanCheckbox id="chkSelectAll" title="Select All/Deselect All?" value="#{supportingCodeBackingBean.allSelected}" style="vertical-align:baseline;width:30px;" styleClass="check" >
										<a4j:support event="onclick" reRender="countryTable" actionListener="#{supportingCodeBackingBean.selectAllChange}"
										oncomplete="initScrollingTables();"  />								
								</h:selectBooleanCheckbox>
							</h:panelGroup>		
						</f:facet>
						<h:selectBooleanCheckbox id="chkNewActive" value="#{state.activeTempBooleanFlag}" styleClass="check" >
								<a4j:support event="onclick" reRender="countryTable" actionListener="#{supportingCodeBackingBean.selectSingleChange}" 
								   />								
						</h:selectBooleanCheckbox>
					</rich:column>	
				</rich:dataTable>
			</div>
		</div>
	</div>

	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk" action="#{supportingCodeBackingBean.okQuickUpdateAction}" /></li>
			<li class="cancel"><h:commandLink id="btnCancel" immediate="true" action="#{supportingCodeBackingBean.viewAction}" /></li>
		</ul>
	</div>
	
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>