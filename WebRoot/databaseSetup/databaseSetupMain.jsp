<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="dbSetupForm">

<h1><h:graphicImage id="imgDatabase" alt="Database" value="/images/headers/hdr-database.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top"></div>
	<div id="table-one-content" >
	<div class="scrollInner" id="resize" >
	<div id="embedded-table">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3">Database Setup</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>				
					<span style="text-decoration:underline;">Initial Setup Steps</span>
					<ol>					
					<li>					
					    <h:commandLink id="t1ok" action="import_def_main_dbs" value="Enter Import Definition/Spec" style="font-weight:bold;" />					    
					</li>
					<li>	
						<h:outputLabel id="tmdv1" rendered="#{!databaseSetupBean.enterImportDefinitionSpecDoneFlag}" value="Enter Drivers" />					
						<h:commandLink id="tmdv" rendered="#{databaseSetupBean.enterImportDefinitionSpecDoneFlag}" action="matrix_view" value="Enter Drivers" style="font-weight:bold;" />							
					</li>
					<li>
						<h:outputLabel id="teel1" rendered="#{!databaseSetupBean.enterDriversDoneFlag}" value="Enter Entity Levels" />				      
					    <h:commandLink id="teel" rendered="#{databaseSetupBean.enterDriversDoneFlag}" action="security_entitylevel_update_dbs" value="Enter Entity Levels" style="font-weight:bold;" />					        
					</li>
					<li>
						<h:outputLabel id="tse1" rendered="#{!databaseSetupBean.enterEntityLevelsDoneFlag}" value="Enter Entities" />	
					    <h:commandLink id="tse" rendered="#{databaseSetupBean.enterEntityLevelsDoneFlag}" action="entity_main" value="Enter Entities" style="font-weight:bold;" />					      
					</li>
					<li> 
						<h:outputLabel id="tpref1" rendered="#{!databaseSetupBean.enterEntitiesDoneFlag}" value="Enter Preferences" />	
					    <h:commandLink id="tpref" rendered="#{databaseSetupBean.enterEntitiesDoneFlag}" action="filepref_admin_general_dbs" value="Enter Preferences" style="font-weight:bold;" />
					</li>
					</ol>	
					
<!--					<span style="text-decoration:underline;">Database Setup</span>  -->
<!--					<ul>  -->					
<!--						<li><a href="#">Browse STS Corporate Objects</a></li>  -->
<!--					</ul>  -->				
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	<div id="table-one-bottom">
	</div>
	</div>
</div>
</h:form>
</ui:define>

</ui:composition>

</html>