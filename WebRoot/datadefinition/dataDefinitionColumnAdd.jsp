<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="dataDefColumnAddForm">

<h1><h:graphicImage id="imgDataDefinitions" alt="Data Definitions" value="/images/headers/hdr-data-defintions.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6"><h:outputText value="#{dataDefinitionBean.actionText}"/> a Table Column Definition</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Table Name:</td>
				<td style="width:388px;" colspan="3" >
				<h:inputText value="#{dataDefinitionBean.dataDefinitionColumnDTONew.tableName}"     
						 disabled="true" id="txtTableName"
				         style="width:388px;" />
                </td>
                <td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Column Name:</td>
				<td style="width:388px;" colspan="3">
				<h:selectOneMenu id="SelColumnName" title=""
					required="true" label="Column Name"
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction or dataDefinitionBean.updateAction}"
                    validator="#{dataDefinitionBean.validateColumnName}"	
					style="width:394px;" 
					binding="#{dataDefinitionBean.columnNameMenu}"	
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.columnName}">																					
				</h:selectOneMenu>
				</td>
				<td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Data Type:</td>
				<td style="width: 200px;" >
				<h:selectOneMenu id="SelDataType" title=""
					required="true" label="Data Type"
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
					style="width: 200px;" 
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.dataType}">
					<f:selectItems id="SelDataTypeItems" value="#{dataDefinitionBean.dataTypeMenu}" />
				</h:selectOneMenu>
				</td>
				<td style="width:100px; text-align:right;" >Length:</td>
				<td style="width:80px;">
				<h:inputText id="SelLength" title=""
					required="true" label="Length"
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
					style="width: 80px; text-align:right;" 
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.datalength}"
					maxlength="5"
					onkeypress="return onlyIntegerValue(event)"	>
					<f:convertNumber integerOnly="true" />
					<f:validateLongRange minimum="1" maximum="99999" />
				</h:inputText>
				</td>
				<td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Heading for Screen:</td>
				<td style="width:388px;" colspan="3" >
				<h:inputText id="SelHeadingforScreen" title=""
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
					style="width: 388px;" 
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.description}"
					maxlength="50" >																					
				</h:inputText>
				</td>
				<td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Abbr. Heading:</td>
				<td style="width:388px;" colspan="3">
				<h:inputText id="SelAbbrHeading" title=""
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
					style="width: 388px;" 
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.abbrDesc}"
					maxlength="20" >																					
				</h:inputText>
				</td>
				<td style="width:300px;">&#160;</td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Desc. Column:</td>
				<td style="width:388px;" colspan="3">
				<h:selectOneMenu id="SelDescriptionColumn" title=""
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
					style="width:394px;" 
					binding="#{dataDefinitionBean.descriptionColumnMenu}"		
					value="#{dataDefinitionBean.dataDefinitionColumnDTONew.descColumnName}">																					
				</h:selectOneMenu>
				</td>
				<td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Definition:</td>
				<td style="width:388px;" colspan="3">
				<h:inputTextarea value="#{dataDefinitionBean.dataDefinitionColumnDTONew.definition}"  
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"
				    style="width: 388px;" id="txtDefinition"
				    maxlength="255" >
				</h:inputTextarea>
                </td>
                <td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Column Mapable?:</td>
				<td style="width:388px;" colspan="3">
                 	 <h:selectBooleanCheckbox styleClass="check" id="SelColumnMapable"  
                 	 	 value="#{dataDefinitionBean.dataDefinitionColumnDTONew.mapFlagDisplay}"
					 	 disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}">
                      </h:selectBooleanCheckbox>
                 </td>
                 <td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Limited View?:</td>
				<td style="width:388px;" colspan="3">
                 	 <h:selectBooleanCheckbox styleClass="check" id="SelLimitedView"  
                 	 	 value="#{dataDefinitionBean.dataDefinitionColumnDTONew.minimumViewFlagDisplay}"
					 	 disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}" >
                      </h:selectBooleanCheckbox>
                 </td>
                 <td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td  style="width:388px;" colspan="3">
				<h:inputText value="#{dataDefinitionBean.dataDefinitionColumnDTONew.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
                         size="4"				
				         style="width: 388px;" />
                </td>
                <td style="width:300px;">&#160;</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td  style="width:388px;" colspan="3">
				<h:inputText value="#{dataDefinitionBean.dataDefinitionColumnDTONew.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         size="4"
				         style="width: 388px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
                <td style="width:300px;">&#160;</td>
			</tr>
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="dtDefColumnAdd" action="#{dataDefinitionBean.okColumnAction}" /></li>
		<li class="cancel"><h:commandLink id="dtDefColumnCancel" immediate="true" disabled="#{dataDefinitionBean.viewAction}" action="#{dataDefinitionBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>

</html>