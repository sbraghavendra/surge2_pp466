<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">
<h:form id="dataDefTableAddForm">

<h1><h:graphicImage id="imgDataDefinitions" alt="Data Definitions" value="/images/headers/hdr-data-defintions.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-one-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="3"><h:outputText value="#{dataDefinitionBean.actionText}"/> a Table Definition</td></tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Table Name:</td>
				<td style="width:700px;">
				<h:selectOneMenu id="SelDefinition" title=""
					required="true" label="Table Name"
					disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction or dataDefinitionBean.updateAction}"
                    validator="#{dataDefinitionBean.validateTableName}"	
					style="width: 388px;" 
					binding="#{dataDefinitionBean.tableNameMenu}"	
					value="#{dataDefinitionBean.dataDefinitionTableDTONew.tableName}">																					
				</h:selectOneMenu>
				</td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText value="#{dataDefinitionBean.dataDefinitionTableDTONew.description}" 
						 disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"	
                         size="4" id="txtDescription"
				         style="width:250px;" 
				         maxlength="50" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Definition:</td>
				<td>
				<h:inputTextarea value="#{dataDefinitionBean.dataDefinitionTableDTONew.definition}"     
						 disabled="#{dataDefinitionBean.viewAction or dataDefinitionBean.deleteAction}"                     				
				         style="width:250px;" id="txtDefinition"
				         maxlength="255" >
				</h:inputTextarea>
                </td>
			</tr>
			
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText value="#{dataDefinitionBean.dataDefinitionTableDTONew.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
                         size="4"				
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText value="#{dataDefinitionBean.dataDefinitionTableDTONew.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         size="4"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
			
		</tbody>
	</table>
	</div>
	<div id="table-one-bottom">
	<ul class="right">
		<li class="ok"><h:commandLink id="dtDefTblAdd" action="#{dataDefinitionBean.okTableAction}" /></li>
		<li class="cancel"><h:commandLink id="dtDefTblCancel" disabled="#{dataDefinitionBean.viewAction}" immediate="true" action="#{dataDefinitionBean.viewAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>

</html>