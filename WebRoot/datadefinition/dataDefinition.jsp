<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('dataDefForm:tableTable', 'tableTableRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('dataDefForm:columnTable', 'columnnTableRowIndex'); } );
//]]>
</script>
<style type="text/css">
	.rich-mpnl-body {
    background-color: #d4e3e8;
    padding: 0px 0 0 0px;
}
	.rich-mpnl-header {
	background: #79a3c9;
	font-size: 12px;
	text-align:left;
}
	.disabledradiotext {
    color : #C0C0C0;
}
	
</style>
</ui:define>

<ui:define name="body">
<h:inputHidden id="tableTableRowIndex" value="#{dataDefinitionBean.selectedTableIndex}"/>
<h:inputHidden id="columnnTableRowIndex" value="#{dataDefinitionBean.selectedColumnIndex}"/>
<h:form id="dataDefForm">

<h1><h:graphicImage id="imgDataDefinitions" alt="Data Definitions" value="/images/headers/hdr-data-defintions.gif"/></h1>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSTable Definitions.gif" />
	</span>
</div>

<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />

<div id="bottom">
	<div id="table-four">
		<div id="table-one-top">
		
		</div>
		<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner">
			<rich:dataTable rowClasses="odd-row,even-row" 
				styleClass="GridContent" id="tableTable" 
				value="#{dataDefinitionBean.dataDefinitionTableList}" var="dataDeftblDTO">

		        <a4j:support event="onRowClick" 
						onsubmit="selectRow('dataDefForm:tableTable', this);"
		                actionListener="#{dataDefinitionBean.selectedTableRowChanged}" 
						reRender="columnTable,tableAddBtn,tableUpdateBtn,tableDeleteBtn,tableViewBtn,columnAddBtn,columnUpdateBtn,columnDeleteBtn,tableResetDescsBtn,pageInfo,trScroll,"
						oncomplete="initScrollingTables();"   />		

				<rich:column id="tableName" sortBy="#{dataDeftblDTO.tableName}" width="252px">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Table Name" /></f:facet>
				 	<h:outputText value="#{dataDeftblDTO.tableName}"/>
				</rich:column>
				
				<rich:column id="tableDescription" sortBy="#{dataDeftblDTO.description}" width="252px">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Description" /></f:facet>
				 	<h:outputText value="#{dataDeftblDTO.description}"/>
				</rich:column>
		
			</rich:dataTable>
		</div>
		</div>	
		</div>
		<div id="table-one-bottom">
			<ul class="right">
			<li class="resetdescriptionsBtn">
					<a4j:commandLink id="tableResetDescsBtn" style="#{(empty dataDefinitionBean.selectedDataDefinitionTable)? 'display:none;':'display:block;' or (dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
					immediate="true" actionListener="#{dataDefinitionBean.displayResetDescsTableAction}"
					oncomplete="javascript:Richfaces.showModalPanel('resetcolumndescriptions'); return false;" 
					reRender="resetcolumndescriptionsForm,tableResetDescsBtn"/>
					
			</li>
				<li class="add2"><h:commandLink id="tableAddBtn" disabled="#{dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{dataDefinitionBean.displayAddTableAction}" /></li>
				<li class="update2"><h:commandLink id="tableUpdateBtn" disabled="#{empty dataDefinitionBean.selectedDataDefinitionTable or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{dataDefinitionBean.displayUpdateTableAction}" /></li>	
				<li class="deletebtm"><h:commandLink id="tableDeleteBtn" disabled="#{empty dataDefinitionBean.selectedDataDefinitionTable or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{dataDefinitionBean.displayDeleteTableAction}" /></li>	
				<li class="view"><h:commandLink id="tableViewBtn"  disabled="#{empty dataDefinitionBean.selectedDataDefinitionTable}"  action="#{dataDefinitionBean.displayViewTableAction}" /></li>		
				
			</ul>
		</div>
	</div>
</div>

<div class="wrapper">
	<span class="block-right">&#160;</span>
	<span class="block-left tab">
	<img src="../images/containers/STSTable Column Definitions.gif" />
	</span>
</div>

<div id="bottom">
	<div id="table-four">
	<div id="table-four-top">
		<a4j:status id="pageInfo"  
			startText="Request being processed..." 
			stopText="#{dataDefinitionBean.pageDescription}"
			onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     		onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	</div>
	<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner">
			<rich:dataTable rowClasses="odd-row,even-row" 
				styleClass="Tabruler" id="columnTable"                
		        headerClass="header" 
				value="#{dataDefinitionBean.dataDefinitionColumnList}" var="dataDefColDTO"
	            rows="#{dataDefinitionBean.datadefcolumnPageSize}">
				
		        <a4j:support event="onRowClick" 
						onsubmit="selectRow('dataDefForm:columnTable', this);"
		                actionListener="#{dataDefinitionBean.selectedColumnRowChanged}"
		                reRender="columnAddBtn,columnUpdateBtn,columnViewBtn,columnDeleteBtn" />		

				<rich:column id="columnName" sortBy="#{dataDefColDTO.columnName}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Column Name" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.columnName}"/>
				</rich:column>
				
				<rich:column id="dataType" sortBy="#{dataDefColDTO.dataType}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Data Type" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.dataType}"/>
				</rich:column>
		
				<rich:column id="datalength" sortBy="#{dataDefColDTO.datalength}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Data Length" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.datalength}"/>
				</rich:column>		
		
				<rich:column id="headingforScreen" sortBy="#{dataDefColDTO.description}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Heading for Screen" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.description}"/>
				</rich:column>	
		
				<rich:column id="abbrDesc" sortBy="#{dataDefColDTO.abbrDesc}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Abbr Desc" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.abbrDesc}"/>
				</rich:column>
				
				<rich:column id="descColumnName" sortBy="#{dataDefColDTO.descColumnName}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Desc Column Name" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.descColumnName}"/>
				</rich:column>
				<rich:column id="definition" sortBy="#{dataDefColDTO.description}">
		        	<f:facet name="header"><h:outputText styleClass="headerText" value="Definition" /></f:facet>
				 	<h:outputText value="#{dataDefColDTO.definition}"/>
				</rich:column>	
				<rich:column id="mapFlag" style="text-align: center;" sortBy="#{dataDefColDTO.mapFlagDisplay}">
		        	<f:facet name="header">
		        		<h:outputText styleClass="headerText" value="Mapable" />
				 	</f:facet>
				 	<h:selectBooleanCheckbox styleClass="check" value="#{dataDefColDTO.mapFlagDisplay}" disabled="true" />
				</rich:column>	
				<rich:column id="minimumViewFlag" style="text-align: center;" sortBy="#{dataDefColDTO.minimumViewFlagDisplay}">
		        	<f:facet name="header">
		        		<h:outputText styleClass="headerText" value="Limited View" />
				 	</f:facet>
				 	<h:selectBooleanCheckbox styleClass="check" value="#{dataDefColDTO.minimumViewFlagDisplay}" disabled="true" />
				</rich:column>						
			</rich:dataTable>
		</div>
	<rich:datascroller id="trScroll" for="columnTable" maxPages="10" oncomplete="initScrollingTables();"
			 style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  
			 page="#{dataDefinitionBean.datadefcolumnPageNumber}" reRender="pageInfo" /> 
		</div>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="add"><h:commandLink id="columnAddBtn" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" value="" disabled="#{empty dataDefinitionBean.selectedDataDefinitionTable or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" action="#{dataDefinitionBean.displayAddColumnAction}"/></li>
		<li class="update"><h:commandLink id="columnUpdateBtn" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" value="" disabled="#{empty dataDefinitionBean.selectedDataDefinitionColumn or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" action="#{dataDefinitionBean.displayUpdateColumnAction}"/></li>
		<li class="delete"><h:commandLink id="columnDeleteBtn" style="#{(dataDefinitionBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" value="" disabled="#{empty dataDefinitionBean.selectedDataDefinitionColumn or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" action="#{dataDefinitionBean.displayDeleteColumnAction}"/></li>	
		<li class="view"><h:commandLink id="columnViewBtn" value="" disabled="#{empty dataDefinitionBean.selectedDataDefinitionColumn}" action="#{dataDefinitionBean.displayViewColumnAction}"/></li>

	</ul>
	</div>
	</div>
</div>
</h:form>
<ui:include src="/WEB-INF/view/components/reset_column_descriptions.xhtml">
	<ui:param name="bean" value="#{dataDefinitionBean}"/>
</ui:include>
</ui:define>

</ui:composition>

</html>