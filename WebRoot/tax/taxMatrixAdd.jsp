<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:c="http://java.sun.com/jstl/core"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMDetailList', 'aMDetailListRowIndex'); } );
registerEvent(window, "load", function() { displayWarning(); } );
registerEvent(window, "load", function() { initBUFlag(); } );
registerEvent(window, "load", function() { displayBackdateWarning(); } );
registerEvent(window, "load", function() { displayBackdateExistWarning(); } );

function displayWarning() {
 var warning = document.getElementById('displayWarning');
 var val = parseInt(warning.value);
 if (val != 0) {
 	javascript:Richfaces.showModalPanel('warning');
 }
}

function initBUFlag() {
	var ctls = null;
	var panel = null;
	
	panel = document.getElementById('matrixDetailForm:driverPanelCopyAdd');
	if(panel!=null) {
		ctls = panel.getElementsByTagName('input');
	}
	else{
		panel = document.getElementById('matrixDetailForm:driverPanelView');
		ctls = panel.getElementsByTagName('input');
	}
	
	var flag = document.getElementById('matrixDetailForm:globalMatrixLine').checked;
	for(i=0;i<ctls.length;i++) {
		if (ctls[i].id.indexOf('buflag0') != -1) {
			if(#{TaxMatrixViewBean.isGlobalMandatory}){
				ctls[i].disabled = true;
				ctls[i].checked = !flag;
			}
			else{
	 			ctls[i].disabled = flag;
	 			if (flag) ctls[i].checked = false;
	 		}
	 	} else if (ctls[i].id.indexOf('buflag1') != -1) {
	 		ctls[i].disabled = flag;
	 		if (flag) ctls[i].value = '*ALL';
	 	} else if (ctls[i].id.indexOf('buflag2') != -1) {
	 		ctls[i].disabled = flag;
	 	} else if (ctls[i].id.indexOf('buflag3') != -1) {
	 		if (flag) ctls[i].value = '';
	 	} 
	}
}

function displayBackdateExistWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 1) {
	 	javascript:Richfaces.showModalPanel('backdateexistwarning');
	 }
}

function displayBackdateWarning() {
	 var warning = document.getElementById('displayBackdateWarning');
	 var val = parseInt(warning.value);
	 if (val == 2) {
	 	javascript:Richfaces.showModalPanel('backdatewarning');
	 }
}

//]]>
</script>
</ui:define>

<ui:define name="body">
 <c:if test="#{loginBean.isPurchasingSelected}">   
 	<h:inputHidden id="displayBackdateWarning" immediate="true" value="#{TaxMatrixViewBean.displayBackdateWarning}"/>
	<h:inputHidden id="displayWarning" value="#{TaxMatrixViewBean.displayWarning}"/>
	<h:inputHidden id="aMDetailListRowIndex" value="#{TaxMatrixViewBean.selectedTransactionIndex}"/>
</c:if>
 <c:if test="#{!loginBean.isPurchasingSelected}"> 
    <h:inputHidden id="displayBackdateWarning" immediate="true" value="#{GSBMatrixViewBean.displayBackdateWarning}"/>
   <h:inputHidden id="displayWarning" value="#{GSBMatrixViewBean.displayWarning}"/>
	<h:inputHidden id="aMDetailListRowIndex" value="#{GSBMatrixViewBean.selectedTransactionIndex}"/>
 </c:if>
	<h:form id="matrixDetailForm">
	  <h:panelGroup rendered="#{!TaxMatrixViewBean.findMode and !loginBean.isPurchasingSelected}">
		<h1><h:graphicImage id="imageTax"  alt="Good-Services-Matrix Sales" url="/images/headers/hdr-Goods-Services-Matrix-Sales.png"></h:graphicImage></h1>
		</h:panelGroup>
		<h:panelGroup rendered="#{!TaxMatrixViewBean.findMode and loginBean.isPurchasingSelected}">
		<h1><h:graphicImage id="imageTax1"  alt="Good-Services-Matrix-Purchasing" url="/images/headers/hdr-Goods-Services-Matrix-Purchasing.png"></h:graphicImage></h1>
		</h:panelGroup>
		<h:panelGroup rendered="#{TaxMatrixViewBean.findMode}">
			<h1><h:graphicImage id="image1"  alt="Taxability Matrix"  url="/images/headers/hdr-lookup-taxability-matrix-id.gif"></h:graphicImage>  </h1>
		</h:panelGroup>
	<div id="top" style = "width:985px">
	<div id="table-one">
	<div id="table-four-top">
	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
			<tbody>
			<tr>
				<td align="left" style="font-weight: bold ;font-size : 11px">
					<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
				</td>
				<td style="width:80%;" >
				</td>				
				<td>
					<h:outputText value="&#160;W&#160;" style="visibility:visible;background-color:#00b300;" />
				</td>
				<td>
					<h:outputText value="&#160;=&#160;Wildcard Allowed&#160;&#160;" />
				</td>
				<td>
					<h:outputText value="&#160;N&#160;" style="visibility:visible;background-color:#ff0080;" />
				</td>
				<td>
					<h:outputText value="&#160;=&#160;Null Allowed&#160;&#160;" />
				</td>
				<td style="width: 20px;">&#160;</td>
			</tr>
			</tbody>
		</table>		
		
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr>
			     <c:if test="#{loginBean.isPurchasingSelected}"> 
				<td colspan="5"><h:outputText id="headerId" value="#{TaxMatrixViewBean.actionText}"/> a Goods &amp; Services Matrix Line <h:outputText value="#{TaxMatrixViewBean.actionTextSuffix}"/></td>
				<td colspan="4" style="text-align:right;">Goods &amp; Services Matrix ID:&#160;<h:outputText id="hdrMatrixId" value="#{TaxMatrixViewBean.matrixId}"/></td>
				</c:if>
				 <c:if test="#{!loginBean.isPurchasingSelected}"> 
				 <td colspan="5"><h:outputText id="headerId" value="#{GSBMatrixViewBean.actionText}"/> a Goods &amp; Services Matrix Line <h:outputText value="#{GSBMatrixViewBean.actionTextSuffix}"/></td>
				<td colspan="4" style="text-align:right;">Goods &amp; Services Matrix ID:&#160;<h:outputText id="hdrMatrixId" value="#{GSBMatrixViewBean.matrixId}"/></td>
				 </c:if>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="5">Driver Selection</th>
				 <c:if test="#{loginBean.isPurchasingSelected}"> 
				<h:panelGroup id="pgDefaultMatrixLine" rendered="#{!TaxMatrixViewBean.addFromTransactionAction}" >
					<th colspan="4" style="text-align:right;">
					<h:outputText id="txtDefaultMatrixLine" 
                        value="#{TaxMatrixViewBean.selectedMatrix.defaultBooleanFlag ? 'Default Matrix Line':'&#160;'}"	 
						style="color:red; font-weight: bold;" />
					</th>
				</h:panelGroup>
				</c:if>
				 <c:if test="#{!loginBean.isPurchasingSelected}"> 
				 <h:panelGroup id="pgDefaultMatrixLine" rendered="#{!GSBMatrixViewBean.addFromTransactionAction}" >
					<th colspan="4" style="text-align:right;">
					<h:outputText id="txtDefaultMatrixLine" 
                        value="#{GSBMatrixViewBean.selectedMatrix.defaultBooleanFlag ? 'Default Matrix Line':'&#160;'}"	 
						style="color:red; font-weight: bold;" />
					</th>
				</h:panelGroup>
				 </c:if>
				 <c:if test="#{loginBean.isPurchasingSelected}"> 
				<h:panelGroup id="pgSpace1" rendered="#{TaxMatrixViewBean.addFromTransactionAction}" >
      				<th colspan="4">&#160;</th>
      			</h:panelGroup>
      			</c:if>
      			 <c:if test="#{!loginBean.isPurchasingSelected}"> 
      			 <h:panelGroup id="pgSpace1" rendered="#{GSBMatrixViewBean.addFromTransactionAction}" >
      				<th colspan="4">&#160;</th>
      			</h:panelGroup>
      			 </c:if>
			</tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td class="column-spacer" style="width:15px;">&#160;</td>
			<td style="width:130px;"><h:outputText value="Entity:" ></h:outputText></td>
			<td>
				<c:if test="#{loginBean.isPurchasingSelected}"> 
					<div id="embedded-table">
  							<h:panelGrid columns="2" >
  								<c:if test="#{TaxMatrixViewBean.isAddAction}">
									<h:selectOneMenu id="entityCode" style="float:left;width:140px;" 
										disabled="#{!TaxMatrixViewBean.isAddAction}"
										value="#{TaxMatrixViewBean.entityCode}" immediate="true">
										<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
	            					</h:selectOneMenu>
	            				</c:if>
	            				<c:if test="#{!TaxMatrixViewBean.isAddAction}">
	            					<h:inputText id="entityCodeCode" style="width:200px;"  disabled="true"
										value="#{TaxMatrixViewBean.entityCodeDesc}" >
									</h:inputText>		
	            				</c:if>
            					<c:if test="#{!(TaxMatrixViewBean.isEntityCheck or !TaxMatrixViewBean.isAddAction)}">								
									<h:commandButton id="entityIdBtn" styleClass="image" 
										disabled="#{TaxMatrixViewBean.isEntityCheck or !TaxMatrixViewBean.isAddAction}" style="float:left;width:16px;height:16px;" 
										image="/images/search_small.png" immediate="true" 
										action="#{commonCallBack.findTaxMatrixIdUpdateAction}">
									</h:commandButton>
								</c:if>
						</h:panelGrid>
					</div>	
  				</c:if>
  				<c:if test="#{!loginBean.isPurchasingSelected}"> 
					<div id="embedded-table">
  							<h:panelGrid columns="2" >	
								<h:selectOneMenu id="entityCode" style="float:left;width:140px;" 
									disabled="#{!GSBMatrixViewBean.isAddAction}"
									value="#{GSBMatrixViewBean.entityCode}" immediate="true">
									<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
            					</h:selectOneMenu>							
								<h:commandButton id="entityIdBtn" styleClass="image" 
										disabled="#{GSBMatrixViewBean.isEntityCheck or !GSBMatrixViewBean.isAddAction}" style="float:left;width:16px;height:16px;" 
										image="/images/search_small.png" immediate="true" 
										action="#{commonCallBack.findTaxMatrixIdUpdateAction}">
							</h:commandButton>
						</h:panelGrid>
					</div>	
  				</c:if>
			</td>
		
			<td class="column-spacer" style="width:100px;">&#160;</td>
			      
		  	<td class="column-spacer">&#160;</td>
		  		<c:if test="#{loginBean.isPurchasingSelected}"> 
			 		<h:panelGroup id="pgGlobalLine" rendered="#{!empty TaxMatrixViewBean.globalLineLabel}">
      					<td class="column-label"><h:outputText value="#{TaxMatrixViewBean.globalLineLabel}" /></td>
						<td class="column-input-small-search">
							<h:selectBooleanCheckbox id="globalMatrixLine" styleClass="check" 
	                            rendered="#{!empty TaxMatrixViewBean.globalLineLabel}"
	                            value="#{TaxMatrixViewBean.driverGlobalFlag}"
	                            binding="#{TaxMatrixViewBean.driverGlobalFlagBind}"
								immediate="true"	
								disabled="#{TaxMatrixViewBean.readOnlyDrivers and !TaxMatrixViewBean.addFromTransactionAction}" 
								actionListener="#{TaxMatrixViewBean.valueChange}" >
								<a4j:support id="ajaxSelectBooleanCheckbox" event="onchange" ajaxSingle="true" />
							</h:selectBooleanCheckbox>
						</td>
						<td class="column-spacer">&#160;</td>
					</h:panelGroup>
				</c:if>
				 
				<c:if test="#{!loginBean.isPurchasingSelected}"> 
				 	<h:panelGroup id="pgGlobalLine" rendered="#{!empty GSBMatrixViewBean.globalLineLabel}">
	      				<td class="column-label"><h:outputText 
							value="#{GSBMatrixViewBean.globalLineLabel}" /></td>
						<td class="column-input-small-search">
							<h:selectBooleanCheckbox id="globalMatrixLine" styleClass="check" 
	                            rendered="#{!empty GSBMatrixViewBean.globalLineLabel}"
	                            value="#{GSBMatrixViewBean.driverGlobalFlag}"
	                            binding="#{GSBMatrixViewBean.driverGlobalFlagBind}"
								immediate="true"	
								disabled="#{GSBMatrixViewBean.readOnlyDrivers and !GSBMatrixViewBean.addFromTransactionAction}" 
								actionListener="#{GSBMatrixViewBean.valueChange}" >
								<a4j:support id="ajaxSelectBooleanCheckbox" event="onchange" ajaxSingle="true" />
							</h:selectBooleanCheckbox>
						</td>
						<td class="column-description">&#160;</td>
					</h:panelGroup>
				</c:if>
				<c:if test="#{loginBean.isPurchasingSelected}"> 
					<h:panelGroup id="pgDefaultLine" rendered="#{(!TaxMatrixViewBean.addFromTransactionAction) and matrixCommonBean.userDefaultMatrixLineFlag}">
						<td style="width:100px;"><h:outputText value="Default Line:"/></td> 
						<td style="width:15px;">
							<h:selectBooleanCheckbox id="defaultMatrixLine" styleClass="check" 
								value="#{TaxMatrixViewBean.selectedMatrix.defaultBooleanFlag}" immediate="true"	
								disabled="#{TaxMatrixViewBean.readOnlyDrivers}" >
								<a4j:support id="ajaxdefaultMatrixLine" event="onclick" reRender="txtDefaultMatrixLine" limitToList="true" ajaxSingle="true" />
							</h:selectBooleanCheckbox>
						</td>
						<td style="width:15px;">&#160;</td>
					</h:panelGroup>
				</c:if>
				<c:if test="#{!loginBean.isPurchasingSelected}"> 
					<h:panelGroup id="pgDefaultLine" rendered="#{(!GSBMatrixViewBean.addFromTransactionAction) and matrixCommonBean.userDefaultMatrixLineFlag}">
						<td style="width:100px;"><h:outputText value="Default Line:"/></td> 
						<td class="column-input-small-search">
							<h:selectBooleanCheckbox id="defaultMatrixLine" styleClass="check" 
								value="#{GSBMatrixViewBean.selectedMatrix.defaultBooleanFlag}" immediate="true"	
								disabled="#{GSBMatrixViewBean.readOnlyDrivers}" >
								<a4j:support id="ajaxdefaultMatrixLine" event="onclick" reRender="txtDefaultMatrixLine" limitToList="true" ajaxSingle="true" />
							</h:selectBooleanCheckbox>
						</td>
						<td class="column-description">&#160;</td>
					</h:panelGroup>
				</c:if>
			<td style="width:40px;">&#160;</td>
			<td class="column-spacer">&#160;</td>
			<td class="column-spacer" style="width:15px;">&#160;</td>
		</tr>
		
		</tbody>
	</table>
		 <c:if test="#{loginBean.isPurchasingSelected}"> 
	    <h:panelGrid id="driverPanelCopyAdd" style = "width:100%"
		rendered="#{!TaxMatrixViewBean.readOnlyDrivers}"
		binding="#{TaxMatrixViewBean.copyAddPanel}"
		styleClass="panel-ruler" />
       </c:if>
     <c:if test="#{!loginBean.isPurchasingSelected}">
	<h:panelGrid id="driverPanelCopyAdd" style = "width:100%"
		rendered="#{!GSBMatrixViewBean.readOnlyDrivers}"
		binding="#{GSBMatrixViewBean.copyAddPanel}"
		styleClass="panel-ruler"/>	
		</c:if>
		<c:if test="#{loginBean.isPurchasingSelected}">
	<h:panelGrid id="driverPanelView" style = "width:100%"
		rendered="#{TaxMatrixViewBean.readOnlyDrivers}"
		binding="#{TaxMatrixViewBean.viewPanel}"
		styleClass="panel-ruler"/>
		</c:if>
		<c:if test="#{!loginBean.isPurchasingSelected}">
    <h:panelGrid id="driverPanelView" style = "width:100%"
		rendered="#{GSBMatrixViewBean.readOnlyDrivers}"
		binding="#{GSBMatrixViewBean.viewPanel}"
		styleClass="panel-ruler"/></c:if>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<th colspan="11">Details</th>
		</tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Dates:</td>
			<td colspan="9">
			<table border="0" cellpadding="0" cellspacing="0" id="embedded-table">
			
				<tr>
					<td>
					Effective&#160;
					  <c:if test="#{loginBean.isPurchasingSelected}">
					 <rich:calendar binding="#{TaxMatrixViewBean.effectiveDateCalendar}" 
						id="effDate" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{TaxMatrixViewBean.selectedMatrix.effectiveDate}" popup="true"
						disabled="#{TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.addFromTransactionAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox"/>
						</c:if>
						
						 <c:if test="#{!loginBean.isPurchasingSelected}">
					 <rich:calendar binding="#{GSBMatrixViewBean.effectiveDateCalendar}" 
						id="effDate" enableManualInput="true"
						required="true" label="Effective Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{GSBMatrixViewBean.selectedMatrix.effectiveDate}" popup="true"
						disabled="#{GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.addFromTransactionAction}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox"/>
						</c:if>
						</td>
					<td style="width:23px;">&#160;</td>
					<td>
					Expires&#160; 
						 <c:if test="#{loginBean.isPurchasingSelected}">
					<rich:calendar id="expDate" enableManualInput="true"
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{TaxMatrixViewBean.selectedMatrix.expirationDate}" popup="true"
						disabled="#{TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"
						validator="#{TaxMatrixViewBean.validateExpirationDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox" />
						</c:if>
						 <c:if test="#{!loginBean.isPurchasingSelected}">
						 <rich:calendar id="expDate" enableManualInput="true"
						required="true" label="Expiration Date"
						oninputkeypress="return onlyDateValue(event);"
						value="#{GSBMatrixViewBean.selectedMatrix.expirationDate}" popup="true"
						disabled="#{GSBMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"
						validator="#{GSBMatrixViewBean.validateExpirationDate}"
						converter="date" datePattern="M/d/yyyy"
						showApplyButton="false" inputClass="textbox" />
						 </c:if>
						</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>If Distribution:&#160;</td>
			<td colspan="9">
			<table border="0" cellpadding="0" cellspacing="0" id="embedded-table">
				<tr>
					<td>
						<h:outputLabel id="relationLabel" for="relation" value="Relation " />
							 <c:if test="#{loginBean.isPurchasingSelected}">
						<h:selectOneMenu id="relation" style="width: 125px;" 
							disabled="#{TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"
							value="#{TaxMatrixViewBean.selectedMatrix.relationSign}">
							<f:selectItems value="#{matrixCommonBean.relationItems}"/>
							<a4j:support id="relation_handler" event="onchange" reRender="amount,elseHold,elseTaxCode,taxCodeThenBtn,taxCodeElseBtn" status="rowstatus"
								immediate="true" actionListener="#{TaxMatrixViewBean.relationSelected}"/>
						</h:selectOneMenu>
						</c:if>
							 <c:if test="#{!loginBean.isPurchasingSelected}">
							 <h:selectOneMenu id="relation" style="width: 125px;" 
							disabled="#{GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}"
							value="#{GSBMatrixViewBean.selectedMatrix.relationSign}">
							<f:selectItems value="#{matrixCommonBean.relationItems}"/>
							<a4j:support id="relation_handler" event="onchange" reRender="amount,elseHold,elseTaxCode" status="rowstatus"
								immediate="true" actionListener="#{GSBMatrixViewBean.relationSelected}"/>
						</h:selectOneMenu>
							 </c:if>
					</td>
					<td>
						<h:outputLabel id="amountLabel" for="amount" value="Amount " />
						 <c:if test="#{loginBean.isPurchasingSelected}">
						<h:inputText id="amount" style="width: 100px;" 
							label="Amount" 
							disabled="#{TaxMatrixViewBean.elseDisabled or TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction}"
							value="#{TaxMatrixViewBean.selectedMatrix.relationAmount}" 
							onkeypress="return onlyIntegerValue(event);"
							
							 >
						<f:convertNumber integerOnly="true" groupingUsed="false" />	
	
						</h:inputText>
						</c:if>
						 <c:if test="#{!loginBean.isPurchasingSelected}">
						 <h:inputText id="amount" style="width: 100px;" 
							label="Amount" 
							disabled="#{GSBMatrixViewBean.elseDisabled or GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction}"
							value="#{GSBMatrixViewBean.selectedMatrix.relationAmount}" 
							onkeypress="return onlyIntegerValue(event);"
							
							 >
						<f:convertNumber integerOnly="true" groupingUsed="false" />	
	
						</h:inputText>
						 </c:if>
					</td>
					<td>
						<h:outputLabel id="amountInfoLabel" value="&#160;(Value must be an integer.)" />
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Then:</td>
			<td colspan="9">
			<table border="0" cellpadding="0" cellspacing="0" id="embedded-table">
				<tr>
					<td>Hold&#160;
					 <c:if test="#{loginBean.isPurchasingSelected}">
						<h:selectBooleanCheckbox styleClass="check"
							id="thenHold" value="#{TaxMatrixViewBean.selectedMatrix.thenHoldCodeBooleanFlag}"
							disabled="#{TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}">
						</h:selectBooleanCheckbox> 
						</c:if>
						 <c:if test="#{!loginBean.isPurchasingSelected}">
						 <h:selectBooleanCheckbox styleClass="check"
							id="thenHold" value="#{GSBMatrixViewBean.selectedMatrix.thenHoldCodeBooleanFlag}"
							disabled="#{GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}">
						</h:selectBooleanCheckbox> 
						 </c:if>
					</td>
					<td><rich:spacer width="10px"/></td>
					<td colspan="7">
					<c:if test="#{loginBean.isPurchasingSelected}">
						<ui:include src="/WEB-INF/view/components/taxcode_code_nocallback.xhtml">
							<ui:param name="handler" value="#{TaxMatrixViewBean.thenTaxCodeHandler}"/>
							<ui:param name="id" value="thenTaxCode"/>
							<ui:param name="readonly" value="#{TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"/>
						
							<ui:param name="label" value="Then TaxCode State"/>
							<ui:param name="type" value="Then TaxCode Type"/>
							<ui:param name="TaxCode" value="Then TaxCode"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="cchrequired" value="false"/>
							<ui:param name="cchreadonly" value="#{TaxMatrixViewBean.readOnlyAction}"/>
							<ui:param name="cchRead" value="true"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
							<ui:param name="displaymessage" value="#{TaxMatrixViewBean.thenTaxCodeStateWarning}"/>
						</ui:include>
					</c:if>
						
					<c:if test="#{!loginBean.isPurchasingSelected}">
						<ui:include src="/WEB-INF/view/components/taxcode_code_nocallback.xhtml">
							<ui:param name="handler" value="#{GSBMatrixViewBean.thenTaxCodeHandler}"/>
							<ui:param name="id" value="thenTaxCode"/>
							<ui:param name="readonly" value="#{GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}"/>
						
							<ui:param name="label" value="Then TaxCode State"/>
							<ui:param name="type" value="Then TaxCode Type"/>
							<ui:param name="TaxCode" value="Then TaxCode"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="cchrequired" value="false"/>
							<ui:param name="cchreadonly" value="#{GSBMatrixViewBean.readOnlyAction}"/>
							<ui:param name="cchRead" value="true"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
							<ui:param name="displaymessage" value="#{GSBMatrixViewBean.thenTaxCodeStateWarning}"/>
						</ui:include>
						</c:if>
					</td>
					
					<td>
					<c:if test="#{loginBean.isPurchasingSelected}">
						<h:commandButton id="taxCodeThenBtn" styleClass="image" 
							disabled="#{TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png"
							action="#{TaxMatrixViewBean.findTaxCodeThenAction}">
						</h:commandButton>
					</c:if>
					</td>
					<td>
					<c:if test="#{!loginBean.isPurchasingSelected}">
						<h:commandButton id="taxCodeThenBtn" styleClass="image" 
							disabled="#{GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png" 
							action="#{GSBMatrixViewBean.findTaxCodeThenAction}">
						</h:commandButton>
					</c:if>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Else:</td>
			<td colspan="9">
			<table border="0" cellpadding="0" cellspacing="0" id="embedded-table">
				<tr>
					<td>Hold&#160;
					 <c:if test="#{loginBean.isPurchasingSelected}">
						<h:selectBooleanCheckbox styleClass="check"
							id="elseHold" value="#{TaxMatrixViewBean.selectedMatrix.elseHoldCodeBooleanFlag}" 
							disabled="#{TaxMatrixViewBean.elseDisabled or TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}">
						</h:selectBooleanCheckbox>
						</c:if>
						 <c:if test="#{!loginBean.isPurchasingSelected}">
						<h:selectBooleanCheckbox styleClass="check"
							id="elseHold" value="#{GSBMatrixViewBean.selectedMatrix.elseHoldCodeBooleanFlag}" 
							disabled="#{GSBMatrixViewBean.elseDisabled or GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}">
						</h:selectBooleanCheckbox>
						</c:if>
					</td>
					<td><rich:spacer width="10px"/></td>
					<td colspan="7">
					 <c:if test="#{loginBean.isPurchasingSelected}">
						<ui:include src="/WEB-INF/view/components/taxcode_code_nocallback.xhtml">
							<ui:param name="handler" value="#{TaxMatrixViewBean.elseTaxCodeHandler}"/>
							<ui:param name="id" value="elseTaxCode"/>
							<ui:param name="readonly" value="#{TaxMatrixViewBean.elseDisabled or TaxMatrixViewBean.updateAction or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"/>
							
							<ui:param name="allornothing" value="false"/>
							<ui:param name="label" value="Else TaxCode State"/>
							<ui:param name="type" value="Else TaxCode Type"/>
							<ui:param name="TaxCode" value="Else TaxCode"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="cchrequired" value="false"/>
							<ui:param name="cchreadonly" value="#{TaxMatrixViewBean.elseDisabled or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"/>
							<ui:param name="cchRead" value="true"/>
							<ui:param name="popupName" value="searchTaxCodeElse"/>
							<ui:param name="popupForm" value="searchTaxCodeFormElse"/>
							<ui:param name="displaymessage" value="#{TaxMatrixViewBean.elseTaxCodeStateWarning}"/>
						</ui:include>
					</c:if>
					
					<c:if test="#{!loginBean.isPurchasingSelected}">
						 <ui:include src="/WEB-INF/view/components/taxcode_code_nocallback.xhtml">
							<ui:param name="handler" value="#{GSBMatrixViewBean.elseTaxCodeHandler}"/>
							<ui:param name="id" value="elseTaxCode"/>
							<ui:param name="readonly" value="#{GSBMatrixViewBean.elseDisabled or GSBMatrixViewBean.updateAction or GSBMatrixViewBean.readOnlyAction  or GSBMatrixViewBean.backdateAction}"/>
							
							<ui:param name="allornothing" value="false"/>
							<ui:param name="label" value="Else TaxCode State"/>
							<ui:param name="type" value="Else TaxCode Type"/>
							<ui:param name="TaxCode" value="Else TaxCode"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="cchrequired" value="false"/>
							<ui:param name="cchreadonly" value="#{GSBMatrixViewBean.elseDisabled or GSBMatrixViewBean.readOnlyAction  or GSBMatrixViewBean.backdateAction}"/>
							<ui:param name="cchRead" value="true"/>
							<ui:param name="popupName" value="searchTaxCodeElse"/>
							<ui:param name="popupForm" value="searchTaxCodeFormElse"/>
							<ui:param name="displaymessage" value="#{GSBMatrixViewBean.elseTaxCodeStateWarning}"/>
						</ui:include>
						 </c:if>
					</td>
					
					<td>
					 <c:if test="#{loginBean.isPurchasingSelected}">
						<h:commandButton id="taxCodeElseBtn" styleClass="image" 
							disabled="#{TaxMatrixViewBean.elseDisabled or TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png"
							action="#{TaxMatrixViewBean.findTaxCodeElseAction}">
						</h:commandButton>
					</c:if>
					</td>
					
					<td>
					 <c:if test="#{!loginBean.isPurchasingSelected}">
						<h:commandButton id="taxCodeElseBtn" styleClass="image" 
							disabled="#{GSBMatrixViewBean.elseDisabled or GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}" style="float:left;width:16px;height:16px;" 
							image="/images/search_small.png"
							action="#{GSBMatrixViewBean.findTaxCodeElseAction}">
						</h:commandButton>
					</c:if>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Comments:</td>
			<td colspan="9">
			 <c:if test="#{loginBean.isPurchasingSelected}">
				<h:inputText id="comments" style="width:678px;" 
					disabled="#{TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"
					value="#{TaxMatrixViewBean.selectedMatrix.comments}"
					maxlength="255"/>
					</c:if>
					 <c:if test="#{!loginBean.isPurchasingSelected}">
				<h:inputText id="comments" style="width:678px;" 
					disabled="#{GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}"
					value="#{GSBMatrixViewBean.selectedMatrix.comments}"/>
					</c:if>
			</td>
		</tr>
		<c:if test="#{loginBean.isPurchasingSelected}">
		<c:if test="#{!TaxMatrixViewBean.addFromTransactionAction}">
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Active?:</td>
			<td colspan="9">
				<h:selectBooleanCheckbox id="active" 
					disabled="#{TaxMatrixViewBean.readOnlyAction or TaxMatrixViewBean.backdateAction}"
					value="#{TaxMatrixViewBean.selectedMatrix.activeBooleanFlag}"/>
			</td>
		</tr>
		</c:if>
		</c:if>	<c:if test="#{!loginBean.isPurchasingSelected}">
		<c:if test="#{!GSBMatrixViewBean.addFromTransactionAction}">
		<tr>
			<td style="width: 10px;">&#160;</td>
			<td>Active?:</td>
			<td colspan="9">
				<h:selectBooleanCheckbox id="active" 
					disabled="#{GSBMatrixViewBean.readOnlyAction or GSBMatrixViewBean.backdateAction}"
					value="#{GSBMatrixViewBean.selectedMatrix.activeBooleanFlag}"/>
			</td>
		</tr>
		</c:if>
		</c:if>
		    	<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<c:if test="#{loginBean.isPurchasingSelected}">
				
				<h:inputText value="#{TaxMatrixViewBean.selectedMatrix.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
				         </c:if>
				         <c:if test="#{!loginBean.isPurchasingSelected}">
				     <h:inputText value="#{GSBMatrixViewBean.selectedMatrix.updateUserId}" 
				         disabled="true" id="txtUpdateUserId"
				         style="width:650px;" />
				         
				         </c:if>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				 <c:if test="#{loginBean.isPurchasingSelected}">
				<h:inputText value="#{TaxMatrixViewBean.selectedMatrix.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
				</c:if>
				 <c:if test="#{!loginBean.isPurchasingSelected}">
				<h:inputText value="#{GSBMatrixViewBean.selectedMatrix.updateTimestamp}" 
				         disabled="true" id="txtUpdateTimestamp"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
				</c:if>
                </td>
			</tr>
		</tbody>
	</table>
	
	</div> <!-- table-one-content -->
	
	<h:panelGroup id="btnHeader">
    <div id="table-four-bottom">
	<ul class="right">
       	<c:if test="#{loginBean.isPurchasingSelected}">
		<li class="copy-update">
			<a4j:commandLink id="btnCopyUpdate" rendered="#{TaxMatrixViewBean.updateAction}" immediate="true" 
				actionListener="#{TaxMatrixViewBean.copyUpdateListener}" reRender="btnHeader,hdrMatrixId,headerId,thenTaxCode,elseTaxCode,effDate,thenHold,elseHold,relation,amount"/>
		</li>
		<li class="view115">
      		<h:commandLink id="viewTrans" immediate="true" rendered="#{TaxMatrixViewBean.addFromTransactionAction}"
                     action="#{TaxMatrixViewBean.viewSourceTransactionAction}"/>
    	</li>
		<li class="viewWhatIf">
      				<h:commandLink id="viewWhatIfTrans" 
                     disabled="#{!TaxMatrixViewBean.validTransactionSelection}" 
                     action="#{TaxMatrixViewBean.viewTransactionAction}"/>
    	</li>

		<li class="whatif"><a4j:commandLink id="btnWhatIf" rendered="#{!TaxMatrixViewBean.readOnlyAction and !TaxMatrixViewBean.backdateAction}" actionListener="#{TaxMatrixViewBean.displayTransactionsAction}" reRender="msg,aMDetailList,trScroll,pageInfo,effDate" oncomplete="initScrollingTables();" /></li>
		<li class="ok"><h:commandLink id="okBtn" rendered="#{!TaxMatrixViewBean.readOnlyAction and !TaxMatrixViewBean.backdateAction and !TaxMatrixViewBean.entityCodeError}" action="#{TaxMatrixViewBean.saveAction}"
					onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')" /></li>
		<li class="ok"><h:commandLink id="okBtndel" rendered="#{TaxMatrixViewBean.readOnlyAction and TaxMatrixViewBean.deleteAction and !TaxMatrixViewBean.backdateAction}" action="#{TaxMatrixViewBean.deleteAction}"/></li>
		<li class="ok"><h:commandLink id="okBtnview" rendered="#{TaxMatrixViewBean.readOnlyAction and TaxMatrixViewBean.viewAction and !TaxMatrixViewBean.backdateAction}" immediate="true" action="#{TaxMatrixViewBean.cancelAction}"/></li>
		<li class="ok"><h:commandLink id="okBackdate" rendered="#{TaxMatrixViewBean.backdateAction}" action="#{TaxMatrixViewBean.backdateAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!TaxMatrixViewBean.viewAction}" immediate="true" action="#{TaxMatrixViewBean.cancelAction}" /></li>
		</c:if>
		
		<c:if test="#{!loginBean.isPurchasingSelected}">
		<li class="copy-update">
			<a4j:commandLink id="btnCopyUpdate" rendered="#{GSBMatrixViewBean.updateAction}" immediate="true" 
			actionListener="#{GSBMatrixViewBean.copyUpdateListener}" reRender="btnHeader,hdrMatrixId,headerId,thenTaxCode,elseTaxCode,effDate,thenHold,elseHold,relation,amount"/>
		</li>
		<li class="whatif"><a4j:commandLink id="btnWhatIf" rendered="#{!GSBMatrixViewBean.readOnlyAction and !GSBMatrixViewBean.backdateAction}" actionListener="#{GSBMatrixViewBean.displayTransactionsAction}" reRender="msg,aMDetailList,trScroll,pageInfo,effDate" oncomplete="initScrollingTables();" /></li>
		<li class="ok"><h:commandLink id="okBtn" rendered="#{!GSBMatrixViewBean.readOnlyAction and !GSBMatrixViewBean.backdateAction}" action="#{GSBMatrixViewBean.saveAction}" 
						onclick="javascript:Richfaces.showModalPanel('transactionProcessModal')"/></li>
		<li class="ok"><h:commandLink id="okBtnview" rendered="#{GSBMatrixViewBean.readOnlyAction and GSBMatrixViewBean.viewAction and !GSBMatrixViewBean.backdateAction}" immediate="true" action="#{GSBMatrixViewBean.cancelAction}"/></li>
		<li class="ok"><h:commandLink id="okBackdate" rendered="#{GSBMatrixViewBean.backdateAction}" action="#{GSBMatrixViewBean.backdateAction}"/></li>
		<li class="ok"><h:commandLink id="okBtndel" rendered="#{GSBMatrixViewBean.readOnlyAction and GSBMatrixViewBean.deleteAction and !GSBMatrixViewBean.backdateAction}" action="#{GSBMatrixViewBean.deleteAction}"/></li>
		<li class="cancel"><h:commandLink id="btnCancel" rendered="#{!GSBMatrixViewBean.viewAction}" immediate="true" action="#{GSBMatrixViewBean.cancelAction}" /></li>
		<li class="view115">
      		<h:commandLink id="viewTrans" immediate="true" rendered="#{GSBMatrixViewBean.addFromTransactionAction}"
                     action="#{GSBMatrixViewBean.viewSourceTransactionAction}"/>
    	</li>
    	
			</c:if>
	</ul>
	</div>
	</h:panelGroup>
	</div>
	</div>
	<c:if test="#{loginBean.isPurchasingSelected}">
	<h:panelGroup id="pgTableDisplay" rendered="#{!TaxMatrixViewBean.readOnlyAction}">
	 <div id="bottom">
	<div id="table-four">
<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
	<div id="table-four-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr><th colspan="2">Visual Verification (note no transactions are processed):</th></tr>
		<tr>
			<th align="left" width="350">
				<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText="#{TaxMatrixViewBean.transactionDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			</th>
			<th align="right">
				<!-- <h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{TaxMatrixViewBean.displayAllFields}" styleClass="check">
					<a4j:support event="onclick" reRender="aMDetailList,trScroll,pageInfo" immediate="true"
						actionListener="#{TaxMatrixViewBean.displayChange}"
						oncomplete="initScrollingTables();"/>
				</h:selectBooleanCheckbox>
				<h:outputText  value="Display All Fields" />    -->
				<h:outputText  value="View:&#160;" />
				<h:outputText  value="#{TaxMatrixViewBean.selectedUserNameLocal}&#47;" />
				<h:outputText  value="#{TaxMatrixViewBean.searchColumnNameLocal}&#160;&#160;" />
			</th> 
		</tr>
		</tbody>
	</table>
	
	
	
	<ui:include src="/WEB-INF/view/components/transactiondetail_table.xhtml">
		<ui:param name="formName" value="matrixDetailForm"/>
		<ui:param name="bean" value="#{TaxMatrixViewBean}"/>
		<ui:param name="dataModel" value="#{TaxMatrixViewBean.transactionDataModel}"/>
		<ui:param name="singleSelect" value="true"/>
		<ui:param name="multiSelect" value="false"/>
		<ui:param name="doubleClick" value="false"/>
		<ui:param name="selectReRender" value="viewWhatIfTrans"/>
		<ui:param name="scrollReRender" value="pageInfo"/>
	</ui:include>
	

	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td colspan="9">
			Processed:&#160;<h:selectBooleanCheckbox id="chkProcessed" value="#{TaxMatrixViewBean.processedTransactions}" styleClass="check"/>
			Suspended:&#160;<h:selectBooleanCheckbox id="chkSuspended" value="#{TaxMatrixViewBean.suspendedTransactions}" styleClass="check"/>
			</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	</h:panelGroup>
	</c:if>
	<c:if test="#{!loginBean.isPurchasingSelected}">
		<h:panelGroup id="pgTableDisplay" rendered="#{!GSBMatrixViewBean.readOnlyAction}">
	<div id="bottom">
	<div id="table-four">
<!--<div id="table-four-top" style="padding-left: 23px;"></div> -->
	<div id="table-four-content">
	
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr><th colspan="2">Visual Verification (note no transactions are processed):</th></tr>
		<tr>
			<th align="left" width="350">
				<a4j:status id="pageInfo" 
					startText="Request being processed..." 
					stopText="#{GSBMatrixViewBean.transactionSaleDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
			</th>
			<th align="right">
				<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{GSBMatrixViewBean.displayAllFields}" styleClass="check">
					<a4j:support event="onclick" reRender="aMDetailList,trScroll,pageInfo" immediate="true"
						actionListener="#{GSBMatrixViewBean.displayChange}"
						oncomplete="initScrollingTables();"/>
				</h:selectBooleanCheckbox>
				<h:outputText  value="Display All Fields" />
			</th>
		</tr>
		</tbody>
	</table>
	
	
	
	<ui:include src="/WEB-INF/view/components/transactiondetai_salel_table.xhtml">
		<ui:param name="formName" value="matrixDetailForm"/>
		<ui:param name="bean" value="#{GSBMatrixViewBean}"/>
		<ui:param name="dataModel" value="#{GSBMatrixViewBean.transactionSaleDataModel}"/>
		<ui:param name="singleSelect" value="true"/>
		<ui:param name="multiSelect" value="false"/>
		<ui:param name="doubleClick" value="false"/>
		<ui:param name="scrollReRender" value="pageInfo"/>
	</ui:include>
	
	<c:if test="#{loginBean.isPurchasingSelected}">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<tbody>
		<tr>
			<td colspan="9">
			Processed:&#160;<h:selectBooleanCheckbox id="chkProcessed" value="#{GSBMatrixViewBean.processedTransactions}" styleClass="check"/>
			Suspended:&#160;<h:selectBooleanCheckbox id="chkSuspended" value="#{GSBMatrixViewBean.suspendedTransactions}" styleClass="check"/>
			</td>
		</tr>
		</tbody>
	</table>
	</c:if>
	</div>
	</div>
	</div>
	</h:panelGroup>
	</c:if>
	
	<!--<h:inputHidden rendered="#{!TaxMatrixViewBean.readOnlyAction}" value=" " validator="#{TaxMatrixViewBean.validateDrivers}"/>-->
	<rich:modalPanel id="loader" zindex="2000" autosized="true">
          <h:outputText value="Processing Search ..."/>
	</rich:modalPanel>
	
	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	
</h:form>
	
<rich:modalPanel id="transactionProcessModal" zindex="2000" autosized="true">
	<h:outputText value="Processing..."/>
</rich:modalPanel>


<c:if test="#{loginBean.isPurchasingSelected}">

<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{TaxMatrixViewBean.driverHandler}"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{TaxMatrixViewBean}"/>
	<ui:param name="id" value="warning"/>
	<ui:param name="warning" value="Warning - not all driver values were found.  Save anyway?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBtn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{TaxMatrixViewBean}"/>
	<ui:param name="id" value="backdatewarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than today. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{TaxMatrixViewBean}"/>
	<ui:param name="id" value="backdateexistwarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than an existing Rule. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

</c:if>

<c:if test="#{!loginBean.isPurchasingSelected}">

<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{GSBMatrixViewBean.driverHandler}"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{GSBMatrixViewBean}"/>
	<ui:param name="id" value="warning"/>
	<ui:param name="warning" value="Warning - not all driver values were found.  Save anyway?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBtn"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{GSBMatrixViewBean}"/>
	<ui:param name="id" value="backdatewarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than today. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

<ui:include src="/WEB-INF/view/components/warning.xhtml">
	<ui:param name="bean" value="#{GSBMatrixViewBean}"/>
	<ui:param name="id" value="backdateexistwarning"/>
	<ui:param name="warning" value="The Effective Date is earlier than an existing Rule. Proceed?"/>
	<ui:param name="okBtn" value="matrixDetailForm:okBackdate"/>
</ui:include>

</c:if>

</ui:define>

</ui:composition>
	
</html>
