<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
registerEvent(window, "load", function() { selectRowByIndex('transForm:aMList', 'aMListRowIndex'); } );
registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetail', 'aMDetailListRowIndex'); } );
registerEvent(window, "unload", function() { document.getElementById('transForm:hiddenSaveLink').onclick(); });

function autoSelectFirstRow() {
	document.getElementById('aMDetailListRowIndex').value = 0;
	clickRowByIndex("transForm:aMDetail", "aMDetailListRowIndex");
}

function resetState()
{ 
    document.getElementById("transForm:stateMenuMain").value = "";
}

//]]>
</script>
</ui:define>

<ui:define name="body">
	<c:if test="#{loginBean.isPurchasingSelected}">   
		<h:inputHidden id="aMListRowIndex" value="#{TaxMatrixViewBean.selectedMasterRowIndex}"/>
		<h:inputHidden id="aMDetailListRowIndex" value="#{TaxMatrixViewBean.selectedRowIndex}"/>
	</c:if>
	
	<c:if test="#{!loginBean.isPurchasingSelected}">   
		<h:inputHidden id="aMListRowIndex" value="#{GSBMatrixViewBean.selectedMasterRowIndex}"/>
		<h:inputHidden id="aMDetailListRowIndex" value="#{GSBMatrixViewBean.selectedRowIndex}"/>
	</c:if>
	
	<h:form id="transForm">
	 	<c:if test="#{loginBean.isPurchasingSelected}"> 
			<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
				<a4j:support event="onclick" actionListener="#{TaxMatrixViewBean.filterSaveAction}" />
			</a4j:commandLink>
		</c:if>
			
		<c:if test="#{!loginBean.isPurchasingSelected}"> 
		 	<a4j:commandLink id="hiddenSaveLink" style="visibility:hidden;display:none" >
				<a4j:support event="onclick" actionListener="#{GSBMatrixViewBean.filterSaveAction}" />
			</a4j:commandLink>
		</c:if>
		
		<h:panelGroup rendered="#{!TaxMatrixViewBean.findMode and !loginBean.isPurchasingSelected}">
			<h1><h:graphicImage id="imageTax"  alt="Good-Services-Matrix Sales" url="/images/headers/hdr-Goods-Services-Matrix-Sales.png"></h:graphicImage></h1>
		</h:panelGroup>
		<h:panelGroup rendered="#{!TaxMatrixViewBean.findMode and loginBean.isPurchasingSelected}">
			<h1><h:graphicImage id="imageTax1"  alt="Good-Services-Matrix-Purchasing" url="/images/headers/hdr-Goods-Services-Matrix-Purchasing.png"></h:graphicImage></h1>
		</h:panelGroup>
		<h:panelGroup rendered="#{TaxMatrixViewBean.findMode}">
			<h1><h:graphicImage id="image1"  alt="Taxability Matrix"  url="/images/headers/hdr-lookup-taxability-matrix-id.gif"></h:graphicImage>  </h1>
		</h:panelGroup>
		
		<div class="tab">
			<h:graphicImage id="image2" alt="Selection Filter" url="/images/containers/STSSelection-filter-open.gif"></h:graphicImage>
			&#160;&#160;&#160;
			<a4j:commandLink id="toggleHideSearchPanel"
				style="height:22px;text-decoration: underline;color:#0033FF"
				reRender="showhidefilter,toggleHideSearchPanel"
				actionListener="#{TaxMatrixViewBean.togglePanelController.toggleHideSearchPanel}">
				<h:outputText value="#{(TaxMatrixViewBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
			</a4j:commandLink>
		</div>
			
		<a4j:outputPanel id="showhidefilter">
			<c:if test="#{!TaxMatrixViewBean.togglePanelController.isHideSearchPanel}">
		        <div id="top">
				<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<div id="table-one-top" style="padding-left: 10px;">
			            <table cellpadding="0" cellspacing="0" style="width: 100%;">
			              	<tr>
			              		<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
					          		reRender="showhidefilter" actionListener="#{TaxMatrixViewBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
				          		<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
							        reRender="showhidefilter" actionListener="#{TaxMatrixViewBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
				          		<td style="width: 50%"></td>
				          		<td style="padding-right: 10px; padding-left: 250px; padding-top:3px; width: 200px"><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>
			             	</tr>
			           	</table>
	               	</div> <!-- "table-one-top" -->
		               
				   	<div id="table-one-content"	onkeyup="return submitEnter(event,'transForm:searchBtn')">
				   	<div id="embedded-table">
				   		<h:panelGrid id="togglePanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
				   			<rich:simpleTogglePanel id="TaxDriverPanel" switchType="ajax"
									label="Drivers"
									actionListener="#{TaxMatrixViewBean.togglePanelController.togglePanelChanged}"
									opened="#{TaxMatrixViewBean.togglePanelController.togglePanels['TaxDriverPanel']}"
									bodyClass="togglepanel-body" headerClass="togglepanel-header"
									styleClass="togglepanel" >
				   				<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
				   				<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
							  	
							  	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
				              	<tbody>
						 	     	<tr>
					             	<td class="column-spacer">&#160;</td>
					             	<td class="column-label" style="width:118px;" >Entity:</td>
					             	<td>
						              	<c:if test="#{loginBean.isPurchasingSelected}">						
											<h:selectOneMenu id="entityCode" style="float:left;width:140px;" 
												disabled="#{TaxMatrixViewBean.isEntityCheck}"
												value="#{TaxMatrixViewBean.entityCodeFilter}" immediate="true">
												<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
                							</h:selectOneMenu>
											<h:commandButton id="entityIdBtn" styleClass="image" 
												disabled="#{TaxMatrixViewBean.isEntityCheck}" style="float:left;width:16px;height:16px;" 
												image="/images/search_small.png" immediate="true" 
												action="#{commonCallBack.findTaxMatrixIdAction}">
											</h:commandButton>
								     	</c:if>
								       	<c:if test="#{!loginBean.isPurchasingSelected}">
											<h:selectOneMenu id="entityCode" style="float:left;width:140px;" 
												disabled="#{GSBMatrixViewBean.isEntityCheck}"
												value="#{GSBMatrixViewBean.entityCodeFilter}" immediate="true">
												<f:selectItems value="#{matrixCommonBean.allEntityItems}" />
                							</h:selectOneMenu>
								     		<h:commandButton id="entityIdBtn" styleClass="image" style="float:left;width:15px;height:15px;" 
												disabled="#{GSBMatrixViewBean.isEntityCheck}"
												image="/images/search_small.png" immediate="true" 
												action="#{commonCallBack.findTaxMatrixIdAction}">
											</h:commandButton>
					                   </c:if>
						         	</td>
					          		<td class="column-label">&#160;</td>
					       			<td>&#160;</td>
				           			</tr>
				           		</tbody>            
				           		</table>

				          		<c:if test="#{!loginBean.isPurchasingSelected}">        
				            		<h:panelGrid id="filterPanel" binding="#{GSBMatrixViewBean.filterSelectionPanel}" styleClass="panel-ruler"/>
				                </c:if>
				                
				                <c:if test="#{loginBean.isPurchasingSelected}">
				                	<h:panelGrid id="filterPanel" binding="#{TaxMatrixViewBean.filterSelectionPanel}" styleClass="panel-ruler"/>
				                </c:if>
	
						 	</rich:simpleTogglePanel>
						</h:panelGrid>
						
						<h:panelGrid id="togglePanel2" columns="1" width="100%" cellpadding="0" cellspacing="0" >			
							<rich:simpleTogglePanel id="TaxPanel" switchType="ajax" label="Misc. Information"
									actionListener="#{TaxMatrixViewBean.togglePanelController.togglePanelChanged}"
									opened="#{TaxMatrixViewBean.togglePanelController.togglePanels['TaxPanel']}"
									bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel">
								<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
								<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
							        
							    <table cellpadding="0" cellspacing="0" id="rollover" class="ruler" width="100%">
			                        <tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Effective Date:</td>
										<td>
											<rich:calendar id="effectiveDate" value="#{TaxMatrixViewBean.effectiveDate}" popup="true" converter="date" 
												datePattern="MM/dd/yyyy" showApplyButton="false" rendered="#{loginBean.isPurchasingSelected}"/>
											<rich:calendar id="effectiveDate1" value="#{GSBMatrixViewBean.effectiveDate}" popup="true" converter="date" 
												datePattern="MM/dd/yyyy" showApplyButton="false" rendered="#{!loginBean.isPurchasingSelected}"/>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">TaxCode:</td>
										<td colspan="3">
											<c:if test="#{loginBean.isPurchasingSelected}">        
									     		<ui:include src="/WEB-INF/view/components/taxcode_code_search.xhtml">
													<ui:param name="handler" value="#{TaxMatrixViewBean.filterTaxCodeHandler}"/>
													<ui:param name="bean" value="#{TaxMatrixViewBean}"/>															
													<ui:param name="id" value="filterTaxCode" />
													<ui:param name="readonly" value="false" />
													<ui:param name="required" value="false" />
													<ui:param name="allornothing" value="false" />
													<ui:param name="showheaders" value="false" />
													<ui:param name="forId" value="false" />
												</ui:include>
								       		</c:if>
								          
											<c:if test="#{!loginBean.isPurchasingSelected}">        
												<ui:include src="/WEB-INF/view/components/taxcode_code_search.xhtml">
													<ui:param name="handler" value="#{GSBMatrixViewBean.filterTaxCodeHandler}"/>
													<ui:param name="bean" value="#{GSBMatrixViewBean}"/>															
													<ui:param name="id" value="filterTaxCode" />
													<ui:param name="readonly" value="false" />
													<ui:param name="required" value="false" />
													<ui:param name="allornothing" value="false" />
													<ui:param name="showheaders" value="false" />
													<ui:param name="forId" value="false" />
												</ui:include>
								          	</c:if>
										</td>
										<td class="column-spacer" width="20%">&#160;</td>
									</tr>
								
		                			<tr>	
								   		<td class="column-spacer">&#160;</td>									   					
					    				<c:if test="#{loginBean.isPurchasingSelected}">
						  					<h:panelGroup rendered="#{!empty matrixCommonBean.userDefaultActiveFlag}">
												<td class="column-label">Active?</td>
												<td class="column-input">
									  				<h:selectOneMenu id="ddlActive" value="#{TaxMatrixViewBean.activeFilter}">
									  					<f:selectItems value="#{matrixCommonBean.activeItems}"/>
													</h:selectOneMenu>
												</td>
											</h:panelGroup>
										</c:if>
												
										<c:if test="#{!loginBean.isPurchasingSelected}">
									 		<h:panelGroup rendered="#{!empty matrixCommonBean.userDefaultActiveFlag}">
												<td class="column-label">Active?</td>
												<td class="column-input">
													<h:selectOneMenu id="ddlActive" value="#{GSBMatrixViewBean.activeFilter}">		
														<f:selectItems value="#{matrixCommonBean.activeItems}"/>
													</h:selectOneMenu>
												</td>
											</h:panelGroup>
										</c:if>
										
										<td class="column-spacer">&#160;</td>
											
								   		<c:if test="#{loginBean.isPurchasingSelected}">
											<h:panelGroup rendered="#{matrixCommonBean.userDefaultMatrixLineFlag}">
												<td class="column-label">Default Line:</td>
												<td class="column-input">
													<h:selectOneMenu id="ddlDefault" value="#{TaxMatrixViewBean.defaultLineFilter}">
														<f:selectItems value="#{matrixCommonBean.defaultLineItems}"/>
													</h:selectOneMenu>
												</td>
											</h:panelGroup>
										</c:if>
											
										<c:if test="#{!loginBean.isPurchasingSelected}">
											<h:panelGroup rendered="#{matrixCommonBean.userDefaultMatrixLineFlag}">
												<td class="column-label">Default Line:</td>
												<td class="column-input">  
									   				<h:selectOneMenu id="ddlDefault" value="#{GSBMatrixViewBean.defaultLineFilter}">
														<f:selectItems value="#{matrixCommonBean.defaultLineItems}" />
													</h:selectOneMenu>		
												</td>
											</h:panelGroup>
										</c:if>
											
						                <c:if test="#{loginBean.isPurchasingSelected}">
											<h:panelGroup rendered="#{!empty TaxMatrixViewBean.globalLineLabel}">
												<td class="column-label">
													<h:outputText value="#{TaxMatrixViewBean.globalLineLabel}" />
												</td>
												<td class="column-input">
													<h:selectOneMenu id="ddlGlobal" value="#{TaxMatrixViewBean.globalCompanyNumberFilter}">
														<f:selectItems value="#{matrixCommonBean.globalCompnyNumberItems}" />
													</h:selectOneMenu>
												</td>
											</h:panelGroup>
										</c:if>
											
									  	<c:if test="#{!loginBean.isPurchasingSelected}">
											<h:panelGroup rendered="#{!empty GSBMatrixViewBean.globalLineLabel}">
												<td class="column-label">
													<h:outputText value="#{GSBMatrixViewBean.globalLineLabel}" />
												</td>
												<td class="column-input">
													<h:selectOneMenu id="ddlGlobal" value="#{GSBMatrixViewBean.globalCompanyNumberFilter}">
														<f:selectItems value="#{matrixCommonBean.globalCompnyNumberItems}" />
													</h:selectOneMenu>
												</td>
											</h:panelGroup>
										</c:if>
										
										<h:panelGroup rendered="#{empty GSBMatrixViewBean.globalLineLabel and !loginBean.isPurchasingSelected}">
											<td class="column-label" width="20%">&#160;</td>
										</h:panelGroup>
										
										<h:panelGroup rendered="#{empty TaxMatrixViewBean.globalLineLabel and loginBean.isPurchasingSelected}">
											<td class="column-label" width="20%">&#160;</td>
										</h:panelGroup>
									</tr>
									
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Tax Matrix ID:</td>
										<td class="column-input">
											<h:inputText id="taxMatrixId"
												style="text-align:right;"
												value="#{GSBMatrixViewBean.filterSelection.taxMatrixId}"
												onkeypress="return onlyIntegerValue(event);"
												label="Tax Matrix ID" rendered="#{!loginBean.isPurchasingSelected}">
												<f:convertNumber integerOnly="true" />
											</h:inputText>
												<h:inputText id="taxMatrixId1"
												style="text-align:right;"
												value="#{TaxMatrixViewBean.filterSelection.taxMatrixId}"
												onkeypress="return onlyIntegerValue(event);"
												label="Tax Matrix ID" rendered="#{loginBean.isPurchasingSelected}">
												<f:convertNumber integerOnly="true" />
											</h:inputText>
										</td>
										<td colspan="6" class="column-spacer">&#160;</td>
									</tr>
								</table>
							</rich:simpleTogglePanel>
						</h:panelGrid>
						
					</div> <!-- "embedded-table" -->
					</div> <!-- "table-one-content" -->
					
					<div id="table-one-bottom">
					<ul class="right">
						<li class="clear">
							<c:if test="#{loginBean.isPurchasingSelected}">
								<a4j:commandLink id="btnClear" action="#{TaxMatrixViewBean.resetFilter}" reRender="showhidefilter" ></a4j:commandLink>
							</c:if>
							<c:if test="#{!loginBean.isPurchasingSelected}">
								<a4j:commandLink id="btnClear" action="#{GSBMatrixViewBean.resetFilter}" reRender="showhidefilter"></a4j:commandLink>
							</c:if>
						</li>
						<li class="search">
							<c:if test="#{loginBean.isPurchasingSelected}">
								<a4j:commandLink id="searchBtn" action="#{TaxMatrixViewBean.updateMatrixList}" reRender="showhidefilter"></a4j:commandLink>
					   		</c:if>
					   		<c:if test="#{!loginBean.isPurchasingSelected}">
					   			<a4j:commandLink id="searchBtn" action="#{GSBMatrixViewBean.updateMatrixList}" reRender="showhidefilter"></a4j:commandLink>
					   		</c:if>
						</li>
					</ul>
				</div> <!-- "table-one-bottom" -->
				</a4j:outputPanel>
				</div> <!-- "table-one" -->
				</div> <!-- "top" -->
			</c:if>
		</a4j:outputPanel> <!-- "showhidefilter" -->
		
		<rich:modalPanel id="loader" zindex="2000" autosized="true">
             <h:outputText value="Processing Search ..." />
     	</rich:modalPanel>
     	
     	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
     
		<div class="wrapper">
		<span class="block-right">&#160;</span> 
		<span class="block-left tab">
				<h:graphicImage id="image3"
  				 alt="Tax Matrix Lines And Details"
  				 url="/images/containers/subhdr-GoodsServices_Lines_Details.png">
				</h:graphicImage>
		</span></div>

		<div id="bottom">
		<div id="table-four">
		<div id="table-four-top" style="padding-left: 15px;">
		<c:if test="#{!loginBean.isPurchasingSelected}">
			<a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{GSBMatrixViewBean.matrixDataModel.pageDescription}"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     	</c:if>
     	<c:if test="#{loginBean.isPurchasingSelected}">
     		<a4j:status id="pageInfo"  
				startText="Request being processed..." 
				stopText="#{TaxMatrixViewBean.matrixDataModel.pageDescription}"
				onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
     	</c:if>
     			
		</div>
		<div id="table-four-content">
		<div class="scrollContainer">
		<div class="scrollInner" id="resize">
		 <c:if test="#{!loginBean.isPurchasingSelected}">
		<rich:dataTable rowClasses="odd-row,even-row" id="aMList"
			value="#{GSBMatrixViewBean.matrixDataModel}" var="matrix"
			rows="#{GSBMatrixViewBean.matrixDataModel.pageSize}">
			
			<a4j:support event="onRowClick"
					onsubmit="selectRow('transForm:aMList', this);"
					actionListener="#{GSBMatrixViewBean.selectedMasterRowChanged}"
					reRender="aMDetail,copyAddBtn,deleteBtn,viewBtn,effectivedateBtn,updateBtn,okBtn"
					oncomplete="initScrollingTables();" />
		  	<rich:column id="entityId" style="text-align:left;">
					<f:facet name="header">
						<a4j:commandLink id="s_entityId" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['entityId']}"
							value="Entity"
							actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					
					<h:outputText value="#{GSBMatrixViewBean.entityMap[matrix.entityId]}" />
				</rich:column>		
			<rich:column id="defaultFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_defaultFlag" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['defaultFlag']}"
						value="Default" 
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{matrix.defaultBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="driverGlobalFlag"  style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_driverGlobalFlag" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driverGlobalFlag']}"
						value="Global" 
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{matrix.driverGlobalBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="driver01" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_01']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver01" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver01']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_01']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver01}" />
			</rich:column>
			
			<rich:column id="driver02" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_02']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver02" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver02']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_02']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver02}" />
			</rich:column>
			
			<rich:column id="driver03" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_03']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver03" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver03']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_03']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver03}" />
			</rich:column>
			
			<rich:column id="driver04" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_04']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver04" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver04']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_04']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver04}" />
			</rich:column>
			
			<rich:column id="driver05" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_05']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver05" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver05']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_05']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver05}" />
			</rich:column>
			
			<rich:column id="driver06" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_06']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver06" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver06']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_06']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver06}" />
			</rich:column>
			
			<rich:column id="driver07" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_07']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver07" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver07']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_07']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver07}" />
			</rich:column>
			
			<rich:column id="driver08" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_08']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver08" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver08']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_08']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver08}" />
			</rich:column>
			
			<rich:column id="driver09" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_09']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver09" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver09']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_09']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver09}" />
			</rich:column>
			
			<rich:column id="driver10" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_10']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver10" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver10']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_10']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver10}" />
			</rich:column>
			
			<rich:column id="driver11" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_11']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver11" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver11']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_11']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver11}" />
			</rich:column>
			
			<rich:column id="driver12" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_12']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver12" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver12']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_12']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver12}" />
			</rich:column>
			
			<rich:column id="driver13" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_13']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver13" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver13']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_13']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver13}" />
			</rich:column>
			
			<rich:column id="driver14" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_14']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver14" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver14']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_14']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver14}" />
			</rich:column>
			
			<rich:column id="driver15" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_15']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver15" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver15']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_15']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver15}" />
			</rich:column>
			
			<rich:column id="driver16" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_16']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver16" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver16']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_16']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver16}" />
			</rich:column>
			
			<rich:column id="driver17" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_17']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver17" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver17']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_17']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver17}" />
			</rich:column>
			
			<rich:column id="driver18" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_18']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver18" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver18']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_18']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver18}" />
			</rich:column>
			
			<rich:column id="driver19" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_19']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver19" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver19']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_19']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver19}" />
			</rich:column>
			
			<rich:column id="driver20" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_20']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver20" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver20']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_20']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver20}" />
			</rich:column>
			
			<rich:column id="driver21" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_21']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver21" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver21']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_21']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver21}" />
			</rich:column>
			
			<rich:column id="driver22" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_22']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver22" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver22']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_22']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver22}" />
			</rich:column>
			
			<rich:column id="driver23" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_23']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver23" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver23']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_23']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver23}" />
			</rich:column>
			
			<rich:column id="driver24" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_24']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver24" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver24']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_24']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver24}" />
			</rich:column>
			
			<rich:column id="driver25" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_25']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver25" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver25']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_25']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver25}" />
			</rich:column>
			
			<rich:column id="driver26" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_26']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver26" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver26']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_26']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver26}" />
			</rich:column>
			
			<rich:column id="driver27" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_27']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver27" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver27']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_27']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver27}" />
			</rich:column>
			
			<rich:column id="driver28" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_28']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver28" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver28']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_28']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver28}" />
			</rich:column>
			
			<rich:column id="driver29" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_29']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver29" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver29']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_29']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver29}" />
			</rich:column>
			
			<rich:column id="driver30" rendered="#{!empty matrixCommonBean.gsbMatrixColumnMap['DRIVER_30']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver30" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['driver30']}"
						value="#{matrixCommonBean.gsbMatrixColumnMap['DRIVER_30']}"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver30}" />
			</rich:column>
			
			<rich:column id="binaryWeight">
				<f:facet name="header">
					<a4j:commandLink id="s_binaryWeight" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['binaryWeight']}"
						value="Binary Weight"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.binaryWeight}" />
			</rich:column>

			<rich:column id="defaultBinaryWeight">
				<f:facet name="header">
					<a4j:commandLink id="s_defaultBinaryWeight" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortOrder['defaultBinaryWeight']}"
						value="Default Weight"
						actionListener="#{GSBMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.defaultBinaryWeight}" />
			</rich:column>
		
		</rich:dataTable>
		</c:if>
		<c:if test="#{loginBean.isPurchasingSelected}">
		<rich:dataTable rowClasses="odd-row,even-row" id="aMList"
			value="#{TaxMatrixViewBean.matrixDataModel}" var="matrix"
			rows="#{TaxMatrixViewBean.matrixDataModel.pageSize}">
			
			<a4j:support event="onRowClick"
					onsubmit="selectRow('transForm:aMList', this);"
					actionListener="#{TaxMatrixViewBean.selectedMasterRowChanged}"
					reRender="aMDetail,copyAddBtn,deleteBtn,viewBtn,suspendBtn,effectivedateBtn,updateBtn,okBtn"
					oncomplete="initScrollingTables();" />
		  	<rich:column id="entityId" style="text-align:left;">
					<f:facet name="header">
						<a4j:commandLink id="s_entityId" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['entityId']}"
							value="Entity"
							actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
							immediate="true" reRender="aMList"/>
					</f:facet>
					
					<h:outputText value="#{TaxMatrixViewBean.entityMap[matrix.entityId]}" />
				</rich:column>		
			<rich:column id="defaultFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_defaultFlag" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['defaultFlag']}"
						value="Default" 
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{matrix.defaultBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="driverGlobalFlag"  style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_driverGlobalFlag" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driverGlobalFlag']}"
						value="Global" 
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{matrix.driverGlobalBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="driver01" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver01" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver01']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_01']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver01}" />
			</rich:column>
			
			<rich:column id="driver02" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver02" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver02']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_02']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver02}" />
			</rich:column>
			
			<rich:column id="driver03" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver03" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver03']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_03']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver03}" />
			</rich:column>
			
			<rich:column id="driver04" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver04" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver04']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_04']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver04}" />
			</rich:column>
			
			<rich:column id="driver05" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver05" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver05']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_05']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver05}" />
			</rich:column>
			
			<rich:column id="driver06" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver06" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver06']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_06']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver06}" />
			</rich:column>
			
			<rich:column id="driver07" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver07" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver07']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_07']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver07}" />
			</rich:column>
			
			<rich:column id="driver08" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver08" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver08']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_08']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver08}" />
			</rich:column>
			
			<rich:column id="driver09" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver09" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver09']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_09']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver09}" />
			</rich:column>
			
			<rich:column id="driver10" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver10" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver10']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_10']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver10}" />
			</rich:column>
			
			<rich:column id="driver11" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver11" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver11']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_11']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver11}" />
			</rich:column>
			
			<rich:column id="driver12" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver12" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver12']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_12']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver12}" />
			</rich:column>
			
			<rich:column id="driver13" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver13" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver13']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_13']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver13}" />
			</rich:column>
			
			<rich:column id="driver14" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver14" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver14']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_14']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver14}" />
			</rich:column>
			
			<rich:column id="driver15" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver15" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver15']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_15']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver15}" />
			</rich:column>
			
			<rich:column id="driver16" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver16" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver16']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_16']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver16}" />
			</rich:column>
			
			<rich:column id="driver17" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver17" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver17']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_17']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver17}" />
			</rich:column>
			
			<rich:column id="driver18" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver18" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver18']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_18']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver18}" />
			</rich:column>
			
			<rich:column id="driver19" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver19" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver19']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_19']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver19}" />
			</rich:column>
			
			<rich:column id="driver20" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver20" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver20']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_20']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver20}" />
			</rich:column>
			
			<rich:column id="driver21" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver21" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver21']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_21']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver21}" />
			</rich:column>
			
			<rich:column id="driver22" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver22" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver22']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_22']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver22}" />
			</rich:column>
			
			<rich:column id="driver23" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver23" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver23']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_23']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver23}" />
			</rich:column>
			
			<rich:column id="driver24" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver24" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver24']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_24']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver24}" />
			</rich:column>
			
			<rich:column id="driver25" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver25" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver25']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_25']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver25}" />
			</rich:column>
			
			<rich:column id="driver26" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver26" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver26']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_26']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver26}" />
			</rich:column>
			
			<rich:column id="driver27" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver27" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver27']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_27']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver27}" />
			</rich:column>
			
			<rich:column id="driver28" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver28" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver28']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_28']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver28}" />
			</rich:column>
			
			<rich:column id="driver29" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver29" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver29']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_29']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver29}" />
			</rich:column>
			
			<rich:column id="driver30" rendered="#{!empty matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}">
				<f:facet name="header">
					<a4j:commandLink id="s_driver30" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['driver30']}"
						value="#{matrixCommonBean.taxMatrixColumnMap['DRIVER_30']}"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.driver30}" />
			</rich:column>
			
			<rich:column id="binaryWeight">
				<f:facet name="header">
					<a4j:commandLink id="s_binaryWeight" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['binaryWeight']}"
						value="Binary Weight"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.binaryWeight}" />
			</rich:column>

			<rich:column id="defaultBinaryWeight">
				<f:facet name="header">
					<a4j:commandLink id="s_defaultBinaryWeight" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortOrder['defaultBinaryWeight']}"
						value="Default Weight"
						actionListener="#{TaxMatrixViewBean.sortAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMList"/>
				</f:facet>
				<h:outputText value="#{matrix.defaultBinaryWeight}" />
			</rich:column>
		
		</rich:dataTable>
		</c:if>
		</div>
		</div>
         <c:if test="#{loginBean.isPurchasingSelected}">
		<rich:datascroller id="trScroll" for="aMList" maxPages="10" oncomplete="initScrollingTables();"
			style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"
			page="#{TaxMatrixViewBean.matrixDataModel.curPage}" 
			reRender="pageInfo"/>
			</c:if>
			 <c:if test="#{!loginBean.isPurchasingSelected}">
			   
		<rich:datascroller id="trScroll" for="aMList" maxPages="10" oncomplete="initScrollingTables();"
			style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"
			page="#{GSBMatrixViewBean.matrixDataModel.curPage}" 
			reRender="pageInfo"/>
			 </c:if>
			
	    <div class="scrollContainer">
		<div class="scrollInner" id="resize">
		     <c:if test="#{!loginBean.isPurchasingSelected}">
			<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail" 
                value="#{GSBMatrixViewBean.selectedMatrixDetails}" var="taxMatrixDetail" >
                
            <a4j:support event="onRowClick"
				onsubmit="selectRow('transForm:aMDetail', this);"
				actionListener="#{GSBMatrixViewBean.selectedRowChanged}"
				reRender="copyAddBtn,deleteBtn,viewBtn,effectivedateBtn,updateBtn,okBtn"/>
	
			<rich:column id="taxMatrixId">
				<f:facet name="header">
					<a4j:commandLink id="s_taxMatrixId" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['taxMatrixId']}"
						value="Matrix ID" 
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();"
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.taxMatrixId}" />
			</rich:column>
			
			<rich:column id="effectiveDate">
				<f:facet name="header">
					<a4j:commandLink id="s_effectiveDate" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['effectiveDate']}"
						value="Effective Date"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.effectiveDate}">
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>
			
			<rich:column id="relationSign">
				<f:facet name="header">
					<a4j:commandLink id="s_relationSign" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['relationSign']}"
						value="Relation"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.relationSign}" />
			</rich:column>
			
			<rich:column id="relationAmount">
				<f:facet name="header">
					<a4j:commandLink id="s_relationAmount" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['relationAmount']}"
						value="Amount"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.relationAmount}" />
			</rich:column>

			<rich:column id="thenHoldCodeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_thenHoldCodeFlag" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['thenHoldCodeFlag']}"
						value="Then:Hold"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.thenHoldCodeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="thenTaxcodeCode">
				<f:facet name="header">
					<h:outputText value="Then:TaxCode"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.thenTaxcodeCode}" />
			</rich:column>
			<rich:column id="thenTaxcodeCodeDescription">
              <f:facet name="header">
                <h:outputText value="Description"/>
              </f:facet>
             <h:outputText value="#{cacheManager.taxCodeMap[taxMatrixDetail.thenTaxcodeCode].description}"  />
             </rich:column>

<!--  4544
			<rich:column id="thenCchTaxcatCode">
				<f:facet name="header">
					<a4j:commandLink id="s_thenCchTaxcatCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['thenCchTaxcatCode']}"
						value="Then:CCH Tax Cat"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchCodeMap[taxMatrixDetail.thenCchTaxcatCode].description}" />
			</rich:column>

			<rich:column id="thenCchGroupCode">
				<f:facet name="header">
					<a4j:commandLink id="s_thenCchGroupCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['thenCchGroupCode']}"
						value="Then:CCH Group"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchGroupMap[taxMatrixDetail.thenCchGroupCode].groupDesc}" />
			</rich:column>
-->
			<rich:column id="elseHoldCodeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_elseHoldFlag" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['elseHoldFlag']}"
						value="Else:Hold"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.elseHoldCodeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="elseTaxcodeCode">
				<f:facet name="header">
					<h:outputText value="Else:TaxCode"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.elseTaxcodeCode}" />
			</rich:column>
			<rich:column id="elseTaxcodeCodeDescription">
             <f:facet name="header">
                <h:outputText value="Description"/>
             </f:facet>
             <h:outputText value="#{cacheManager.taxCodeMap[taxMatrixDetail.elseTaxcodeCode].description}"  />
             </rich:column>
			
<!--  4544 
			<rich:column id="elseCchTaxcatCode">
				<f:facet name="header">
					<a4j:commandLink id="s_elseCchTaxcatCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['elseCchTaxcatCode']}"
						value="Else:CCH Tax Cat"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchCodeMap[taxMatrixDetail.elseCchTaxcatCode].description}" />
			</rich:column>

			<rich:column id="elseCchGroupCode">
				<f:facet name="header">
					<a4j:commandLink id="s_elseCchGroupCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['elseCchGroupCode']}"
						value="Else:CCH Group"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchGroupMap[taxMatrixDetail.elseCchGroupCode].groupDesc}" />
			</rich:column>
-->			
			<rich:column id="expirationDate">
				<f:facet name="header">
					<a4j:commandLink id="s_expirationDate" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['expirationDate']}"
						value="Expiration Date"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.expirationDate}">
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>

			<rich:column id="comments" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_comments" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['comments']}"
						value="Comments"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.hasComments}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="activeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_activeFlag" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['activeFlag']}"
						value="Active?"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.activeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>

			<rich:column id="updateUserId">
				<f:facet name="header">
					<a4j:commandLink id="s_updateUserId" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['updateUserId']}"
						value="Last Update UserId"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.updateUserId}" />
			</rich:column>

			<rich:column id="updateTimestamp">
				<f:facet name="header">
					<a4j:commandLink id="s_updateTimestamp" styleClass="sort-#{GSBMatrixViewBean.matrixDataModel.sortDetailOrder['updateTimestamp']}"
						value="Time Stamp"
						actionListener="#{GSBMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>
            </rich:dataTable>
            </c:if>
             <c:if test="#{loginBean.isPurchasingSelected}">
             	<rich:dataTable rowClasses="odd-row,even-row" id="aMDetail" 
                value="#{TaxMatrixViewBean.selectedMatrixDetails}" var="taxMatrixDetail" >
                
            <a4j:support event="onRowClick"
				onsubmit="selectRow('transForm:aMDetail', this);"
				actionListener="#{TaxMatrixViewBean.selectedRowChanged}"
				reRender="copyAddBtn,deleteBtn,viewBtn,suspendBtn,effectivedateBtn,updateBtn,okBtn"/>
	
			<rich:column id="taxMatrixId">
				<f:facet name="header">
					<a4j:commandLink id="s_taxMatrixId" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['taxMatrixId']}"
						value="Matrix ID" 
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();"
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.taxMatrixId}" />
			</rich:column>
			
			<rich:column id="effectiveDate">
				<f:facet name="header">
					<a4j:commandLink id="s_effectiveDate" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['effectiveDate']}"
						value="Effective Date"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.effectiveDate}">
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>
			
			<rich:column id="relationSign">
				<f:facet name="header">
					<a4j:commandLink id="s_relationSign" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['relationSign']}"
						value="Relation"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.relationSign}" />
			</rich:column>
			
			<rich:column id="relationAmount">
				<f:facet name="header">
					<a4j:commandLink id="s_relationAmount" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['relationAmount']}"
						value="Amount"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.relationAmount}" />
			</rich:column>

			<rich:column id="thenHoldCodeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_thenHoldCodeFlag" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['thenHoldCodeFlag']}"
						value="Then:Hold"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.thenHoldCodeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="thenTaxcodeCode">
				<f:facet name="header">
					<h:outputText value="Then:TaxCode"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.thenTaxcodeCode}" />
			</rich:column>
			<rich:column id="thenTaxcodeCodeDescription">
              <f:facet name="header">
                <h:outputText value="Description"/>
              </f:facet>
             <h:outputText value="#{cacheManager.taxCodeMap[taxMatrixDetail.thenTaxcodeCode].description}"  />
             </rich:column>
			
<!--  4544
			<rich:column id="thenCchTaxcatCode">
				<f:facet name="header">
					<a4j:commandLink id="s_thenCchTaxcatCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['thenCchTaxcatCode']}"
						value="Then:CCH Tax Cat"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchCodeMap[taxMatrixDetail.thenCchTaxcatCode].description}" />
			</rich:column>

			<rich:column id="thenCchGroupCode">
				<f:facet name="header">
					<a4j:commandLink id="s_thenCchGroupCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['thenCchGroupCode']}"
						value="Then:CCH Group"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchGroupMap[taxMatrixDetail.thenCchGroupCode].groupDesc}" />
			</rich:column>
-->
			<rich:column id="elseHoldCodeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_elseHoldFlag" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['elseHoldFlag']}"
						value="Else:Hold"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.elseHoldCodeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="elseTaxcodeCode">
				<f:facet name="header">
					<h:outputText value="Else:TaxCode"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.elseTaxcodeCode}" />
			</rich:column>
			<rich:column id="elseTaxcodeCodeDescription">
            <f:facet name="header">
           <h:outputText value="Description"/>
           </f:facet>
           <h:outputText value="#{cacheManager.taxCodeMap[taxMatrixDetail.elseTaxcodeCode].description}"  />
           </rich:column>
			
<!--  4544 
			<rich:column id="elseCchTaxcatCode">
				<f:facet name="header">
					<a4j:commandLink id="s_elseCchTaxcatCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['elseCchTaxcatCode']}"
						value="Else:CCH Tax Cat"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchCodeMap[taxMatrixDetail.elseCchTaxcatCode].description}" />
			</rich:column>

			<rich:column id="elseCchGroupCode">
				<f:facet name="header">
					<a4j:commandLink id="s_elseCchGroupCode" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['elseCchGroupCode']}"
						value="Else:CCH Group"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{cacheManager.cchGroupMap[taxMatrixDetail.elseCchGroupCode].groupDesc}" />
			</rich:column>
-->			
			<rich:column id="expirationDate">
				<f:facet name="header">
					<a4j:commandLink id="s_expirationDate" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['expirationDate']}"
						value="Expiration Date"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.expirationDate}">
					<f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
				</h:outputText>
			</rich:column>

			<rich:column id="comments" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_comments" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['comments']}"
						value="Comments"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.hasComments}" disabled="true" styleClass="check"/>
			</rich:column>
			
			<rich:column id="activeFlag" style="text-align:center;">
				<f:facet name="header">
					<a4j:commandLink id="s_activeFlag" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['activeFlag']}"
						value="Active?"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:selectBooleanCheckbox value="#{taxMatrixDetail.activeBooleanFlag}" disabled="true" styleClass="check"/>
			</rich:column>

			<rich:column id="updateUserId">
				<f:facet name="header">
					<a4j:commandLink id="s_updateUserId" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['updateUserId']}"
						value="Last Update UserId"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.updateUserId}" />
			</rich:column>

			<rich:column id="updateTimestamp">
				<f:facet name="header">
					<a4j:commandLink id="s_updateTimestamp" styleClass="sort-#{TaxMatrixViewBean.matrixDataModel.sortDetailOrder['updateTimestamp']}"
						value="Time Stamp"
						actionListener="#{TaxMatrixViewBean.sortDetailAction}" oncomplete="initScrollingTables();" 
						immediate="true" reRender="aMDetail"/>
				</f:facet>
				<h:outputText value="#{taxMatrixDetail.updateTimestamp}">
					<f:converter converterId="dateTime"/>
				</h:outputText>
			</rich:column>
            </rich:dataTable>
             </c:if>
		</div>
		</div>
			
		</div>
		
		<div id="table-four-bottom">
		<ul class="right">
	   <c:if test="#{loginBean.isPurchasingSelected}">
		<h:panelGroup rendered="#{!TaxMatrixViewBean.findMode}">
			<li class="add"><h:commandLink id="btnAdd" disabled = "#{TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  action="#{TaxMatrixViewBean.displayAddActions}" /></li>
		
			
	 
			<li class="copy-add"><h:commandLink id="copyAddBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{!TaxMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxMatrixViewBean.displayCopyAddAction}" /></li>
			<li class="update"><h:commandLink id="updateBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{!TaxMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxMatrixViewBean.displayUpdateAction}" /></li>
			<li class="delete115"><h:commandLink id="deleteBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  disabled="#{!TaxMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{TaxMatrixViewBean.displayDeleteAction}" /></li>
			<li class="suspend">
				<a4j:commandLink id="suspendBtn" style="#{(!TaxMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
					immediate="true" actionListener="#{TaxMatrixViewBean.displaySuspendAction}" disabled = "#{!TaxMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}"
					oncomplete="javascript:Richfaces.showModalPanel('suspend'); return false;" 
					reRender="suspendForm"/>
			</li>
			<li class="effectivedate"><h:commandLink id="effectivedateBtn" disabled="#{!TaxMatrixViewBean.effectivedateSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"  action="#{TaxMatrixViewBean.displayBackDateAction}" /></li>
			<li class="view"><h:commandLink id="viewBtn" disabled="#{!TaxMatrixViewBean.validSelection}" action="#{TaxMatrixViewBean.displayViewAction}" /></li>
			<li class="close"><h:commandLink id="btnClose" action="#{TaxMatrixViewBean.closeAction}" disabled="#{!entityBackingBean.isEntitySelected}"/></li>
		</h:panelGroup>
		
		</c:if>
		        <c:if test="#{!loginBean.isPurchasingSelected}">
		        	<h:panelGroup rendered="#{!GSBMatrixViewBean.findMode}">
			<li class="add"><h:commandLink id="btnAdd" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled = "#{TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{GSBMatrixViewBean.displayAddActions}" /></li>
				
		
			
			<li class="copy-add"><h:commandLink id="copyAddBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!GSBMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{GSBMatrixViewBean.displayCopyAddAction}" /></li>
			<li class="update"><h:commandLink id="updateBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!GSBMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{GSBMatrixViewBean.displayUpdateAction}" /></li>
			<li class="delete115"><h:commandLink id="deleteBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!GSBMatrixViewBean.validSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{GSBMatrixViewBean.displayDeleteAction}" /></li>
			<li class="effectivedate"><h:commandLink id="effectivedateBtn" style="#{(TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!GSBMatrixViewBean.effectivedateSelection or TaxMatrixViewBean.currentUser.viewOnlyBooleanFlag}" action="#{GSBMatrixViewBean.displayBackDateAction}" /></li>
			<li class="view"><h:commandLink id="viewBtn" disabled="#{!GSBMatrixViewBean.validSelection}" action="#{GSBMatrixViewBean.displayViewAction}" /></li>
		    <li class="close"><h:commandLink id="btnClose" action="#{TaxMatrixViewBean.closeAction}" disabled="#{!entityBackingBean.isEntitySelected}"/></li>
		</h:panelGroup>
		 </c:if>
		  <c:if test="#{loginBean.isPurchasingSelected}">
		<h:panelGroup rendered="#{TaxMatrixViewBean.findMode}">
			<li class="ok2"><h:commandLink id="okBtn" disabled="#{!TaxMatrixViewBean.validSelection}" immediate="true" action="#{TaxMatrixViewBean.findAction}"/></li>
			<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{TaxMatrixViewBean.cancelFindAction}" /></li>
		</h:panelGroup>
		</c:if>
		 <c:if test="#{!loginBean.isPurchasingSelected}">
		 <h:panelGroup rendered="#{GSBMatrixViewBean.findMode}">
			<li class="ok2"><h:commandLink id="okBtn" disabled="#{!GSBMatrixViewBean.validSelection}" immediate="true" action="#{GSBMatrixViewBean.findAction}"/></li>
			<li class="cancel2"><h:commandLink id="btnCancel" immediate="true" action="#{GSBMatrixViewBean.cancelFindAction}" /></li>
		</h:panelGroup>
		 </c:if>
		</ul>
		</div>

		</div>
		</div>
	</h:form>
	
	 <c:if test="#{loginBean.isPurchasingSelected}">       
<ui:include src="/WEB-INF/view/components/suspend_matrix_dialog.xhtml">
	<ui:param name="bean" value="#{TaxMatrixViewBean}"/>
	<ui:param name="detail" value="#{TaxMatrixViewBean.prossdTrDatailBean}"/>
	<ui:param name="type" value="Tax"/>
	<ui:param name="isTax" value="true"/>
</ui:include>
</c:if>

<c:if test="#{loginBean.isPurchasingSelected}">	
<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{TaxMatrixViewBean.driverHandler}"/>
</ui:include>
</c:if>

<c:if test="#{!loginBean.isPurchasingSelected}">	
<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
	<ui:param name="handler" value="#{GSBMatrixViewBean.driverHandler}"/>
</ui:include>
</c:if>

</ui:define>

</ui:composition>

</html>
