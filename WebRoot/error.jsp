<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/landing.xhtml">

<ui:define name="body">
	<h:form id="LoginForm">
		<a4j:queue/>
		
		<div id="header">
			<div id="logo"><h:outputText binding="#{errorBean.appVersion}" value="#{errorBean.appVersionString}"><f:attribute name="appVersionString" value="PinPoint v${build.appversion}" /></h:outputText></div>
		</div>
		
		<div id="top-index" >
			<div id="table-one-index" style="height: 443px;">
				<div id="table-one-top" style="padding-left: 5px;">
					<a4j:status id="pageInfo" />     						
					<div class="error">
						Ooops!
                    </div>
                    <a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
				</div>
				
				<div id="table-one-content" style="font-size: 14px; padding: 5px;">
					An internal issue has occurred in the application. Please email the information below to <br/>
					the support team at <a href="mailto:#{errorBean.supportEmail}?subject=#{errorBean.appVersionString} Error Report">#{errorBean.supportEmail}</a>.
                    
                    <br/><br/>
                    <h:inputTextarea cols="60" rows="20" value="#{errorBean.error}" style="width: 568px;" />
                    <br/>
                    <h:commandLink action="#{errorBean.backAction}"> Back</h:commandLink>
				</div>
			</div>
		</div>
		
	</h:form>
</ui:define>

</ui:composition>
</html>