<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   		registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetailList', 'aMDetailListRowIndex'); } );
   //]]>
   </script>
  </ui:define>

  <ui:define name="body">
  	<h:inputHidden id="aMDetailListRowIndex" value="#{bcpSaleTransactionBean.selectedRowIndex}"/>
    <div id="bottom">
    
    <h:form id="filterForm">
    	<a4j:queue/> 
    	
    	<h1>
    	<table cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td align="left" >
	    				<h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process/Transactions" value="/images/headers/hdr-import-map-and-process-sale.gif" />
	    			</td>
	    			<td width="200" align="right" >
						<a4j:commandLink id="viewTrans" style="height:22px;text-decoration: underline;color:#0033FF"
	       					rendered="#{importMapBean.isPcoBatch}" action="importmap_transactions" >
	       					<h:outputText value="View Purchasing Transactions" style="width:200px"/>
	    					</a4j:commandLink>
					</td>
					<td width="30">
					</td>
    			</tr>
			</tbody>
		</table>  	
    	</h1>
    	
    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
    </h:form>
    
    <div class="wrapper">
		<span class="block-right">&#160;</span> 
		<span class="block-left-no-bg tab">
			<img src="../images/containers/STSView-transactions-open.gif"/>
		</span>
	</div>
    
    <h:form id="transForm">
    <div id="table-four">
     <div id="table-four-top" style="padding-left: 23px;">
     	<table cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
				<td align="left" >
					<a4j:status id="pageInfoHeader"  
						startText="Request being processed..." 
						stopText="View Transactions for Batch # #{bcpSaleTransactionBean.batchId}"
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
						
				</td>
				
				
				<td align="right" width="200">
				<rich:spacer width="20px"/>
					<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{bcpSaleTransactionBean.displayAllFields}" styleClass="check">
						<a4j:support event="onclick" reRender="aMDetailList,pageInfo"
							actionListener="#{bcpSaleTransactionBean.displayChange}"
							oncomplete="initScrollingTables();"/>
					</h:selectBooleanCheckbox>
					<h:outputText value="Display All Fields"/>
				</td>
				<td style="width:20px;">&#160;</td>
				</tr>
			</tbody>
		</table>
     </div>
     
     <div id="table-four-content">
     	<ui:include src="/WEB-INF/view/components/transactiondetai_salel_table.xhtml">
			<ui:param name="formName" value="transForm"/>
			<ui:param name="bean" value="#{bcpSaleTransactionBean}"/>
			<ui:param name="dataModel" value="#{bcpSaleTransactionBean.bcpSaleTransactionDataModel}"/>
			<ui:param name="singleSelect" value="true"/>
			<ui:param name="multiSelect" value="false"/>
			<ui:param name="doubleClick" value="true"/>
			<ui:param name="selectReRender" value="pageInfo"/>
			<ui:param name="scrollReRender" value="pageInfo"/>
		</ui:include>
     </div>
     
     <div id="table-four-bottom">
		<h:outputText id="pageInfo" value="#{bcpSaleTransactionBean.bcpSaleTransactionDataModel.pageDescription}"/>
		<ul class="right">
      		<li class="close">
        		<h:commandLink id="closeBtn" action="#{bcpSaleTransactionBean.closeAction}" />
      		</li>
      	</ul>
     </div>
    </div>
    
    </h:form>
    <!-- t4 -->
   </div>
   <!-- bottom -->

   </ui:define>
 </ui:composition>
</html>
