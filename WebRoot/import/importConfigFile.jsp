<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script  type="text/javascript">
//<![CDATA[
function copyMe(from,to){
	var fromValue = document.getElementById(from).value;
	var lastIndex = fromValue.lastIndexOf("\\");
  	if(lastIndex > -1){	
    	document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    }
    else{
    	lastIndex = fromValue.lastIndexOf("//");
    	if(lastIndex > -1){	
    		document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    	}
    	else{
    		document.getElementById(to).value = fromValue;
    	}
    }
}
function changeStatus(status) {		
	if (document.getElementById('impStatus')) {							
			document.getElementById('impStatus').innerText = status;
			document.getElementById('impStatus').style.fontWeight = 'bold';
	}
	return false;
}

//]]>
</script>
</ui:define>
<ui:define name="body">
<h:form id="importForm" enctype="multipart/form-data">
<h1><h:graphicImage id="configurationBatches" alt="Configuration Batches" value="/images/headers/hdr-configuration_batches.png"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-bottom">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content" style="height:418px;">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="4">Import File</td></tr>
		</thead>
		<tbody>
			<tr>
				<th colspan="4">Select File to import</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">File Name:</td>
		    <td colspan="2">
            <h:inputText id="fileName"
                label="File Name"
                value="#{configImportBean.fileName}"
                disabled="#{configImportBean.displayOk || configImportBean.displayClose}"
                required="true"
                size="70"
                />
	        <t:inputFileUpload id="uploadFile"
	            binding="#{configImportBean.uploadFile}" 
	            value="#{configImportBean.uploadedFile}" 
	            disabled="#{configImportBean.displayOk || configImportBean.displayClose}"
	            immediate="true"
                storage="file"
                style="margin: 0px;font-size: 12px;border: 1px;width: 70px;"                
                
                >
                <a4j:support event="onchange" 
                	ajaxSingle="true"                           
         			immediate="true" 
         			oncomplete="copyMe('importForm:uploadFile','importForm:fileName')" reRender="btnSelect,btnOk,btnCancel,closeBtn"
                 />
              
            </t:inputFileUpload>
          </td>
          </tr>
		<tr>
          <td style="width: 50px;">&#160;</td>
          <td class="column-label">Clear or Append?:</td>
          <td colspan="2">
           <h:selectOneMenu id="selAppend"
                            style="width: 300px;"
                            binding="#{configImportBean.appendMenu}"
                            value="#{configImportBean.append}" />
          </td>
        </tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	   
		<tbody>
		   <tr>
				<th colspan="3">&#160;File Information</th>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">File Type:</td>
				<td>
				<h:inputText value="#{configImportBean.fileTypeDescription}"
						disabled="true" label="File Type" id="fileType"
					     style="width:650px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Bytes in File:</td>
				<td>
				<h:inputText value="#{configImportBean.noOfBytesInFile}" 
						disabled="true" label="Bytes in File" id="noOfBytes"
					     style="width:200px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Estimated Rows in File:</td>
				<td>
				<h:inputText value="#{configImportBean.noOfRows}" 
						disabled="true" label="Estimated Rows in File" id="rows"
					     style="width:200px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Batch Number:</td>
				<td>
				<h:inputText value="#{configImportBean.batchNumber}" 
						disabled="true" label="Batch Number" id="batchNo"
					     style="width:200px;" 
				         maxlength="120"/>
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td class="column-label">Import Status:</td>
 				<td style="font-weight: bold;">				
          			<h:outputText id="impStatus" escape="false" style="#{configImportBean.error eq true ? 'color:red;':'color:black;'}font-weight: bold;" value="#{configImportBean.importStatus}" />          		
                </td>
        	</tr>
		</tbody>
	</table>
	</div>
	<c:if test="#{configImportBean.displayClose || configImportBean.displayOk}">
	<a4j:outputPanel id="progressPanel" style="width: 100%">
              <rich:progressBar id="progressBar"
                                value="#{configImportBean.progress}"
                                styleClass="sts-progress-bar"
                                interval="2000"
                                mode="ajax"
                                label="Upload progress: #{configImportBean.progress} %"
                                enabled="true"
                                minValue="0"
                                maxValue="99"                                
                                reRender="impStatus"
                               
                         
                                >
              </rich:progressBar>
      </a4j:outputPanel>
    </c:if>        
	<div id="table-four-bottom">
	
	<ul class="right">
		<li class="select"><h:commandLink id="btnSelect" immediate="true" disabled="#{!configImportBean.displaySelect}" action="#{configImportBean.validateImportedFile}" /></li>
		<li class="ok"><h:commandLink id="btnOk" immediate="true" disabled="#{!configImportBean.displayOk}" action="#{configImportBean.importOkSubmit}" /></li>
		<li class="cancel"><h:commandLink id="btnCancel" immediate="true" disabled="#{!configImportBean.displayCancel}"  action="#{configImportBean.cancelAction}" /></li>
		<li class="close"><h:commandLink id="closeBtn" immediate="true" disabled="#{!configImportBean.displayClose}"  action="#{configImportBean.closeAction}" /></li>
		
	</ul>
	</div>
	</div>
</div>
</h:form>
</ui:define>
</ui:composition>
</html>