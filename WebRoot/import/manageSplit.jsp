<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 	<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  		<ui:define name="script">
		   <script type="text/javascript">
		   //<![CDATA[
		   		registerEvent(window, "load", function() { displayIndividualJurisWarning(); } );
		   		
		   		function displayIndividualJurisWarning() {
		   		 var warning = document.getElementById('displayIndividualJurisWarning');
		   		 var val = parseInt(warning.value);
		   		 if (val != 0) {
		   		 	javascript:Richfaces.showModalPanel('individualJurisWarningId');
		   		 }
		   		}
		   //]]>
		   </script>
  		</ui:define>

  		<ui:define name="body">
  			<h:inputHidden id="displayIndividualJurisWarning" value="#{currentSplittingToolBean[0].displayIndividualJurisWarning}"/>
  			<h:form id="matrixDetailForm">
  				<div id="bottom">
				    <h1>
				    	<h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process/Transactions" value="/images/headers/hdr-import-map-and-process.gif" />
				    </h1>
				    
				    <div id="top">
				    	<div id="table-one">
				    		<div id="table-one-top">
								<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
							</div>
							
							<div id="table-one-content">
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
									<thead>
										<tr>
											<td colspan="5">Transaction Splitting Tool</td>
											<td colspan="4" style="text-align:right;"></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5">Original Transaction: ${currentSplittingToolBean[0].currentTransactionId}</th>
											<th colspan="4"></th>
										</tr>
									</tbody>
								</table>
								<h:panelGrid id="driverPanel" rendered="true" binding="#{currentSplittingToolBean[0].originalFilterSelectionPanel}" styleClass="panel-ruler" />
								<table cellpadding="0" cellspacing="0" class="ruler">
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Distribution Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="glLineItmDistAmt" value="#{currentSplittingToolBean[0].currentTransaction.glLineItmDistAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-label">Freight Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceFreightAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceFreightAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-label">Discount Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceDiscountAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceDiscountAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td colspan="2" class="column-label">&nbsp;</td>
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
									<tbody>
										<tr>
											<th colspan="5">#{currentSplittingToolBean[0].actionText} a Split Transaction:</th>
											<th colspan="41"></th>
										</tr>
									</tbody>
								</table>
								<h:panelGrid id="editdriverPanel" rendered="true" binding="#{currentSplittingToolBean[0].editFilterSelectionPanel}" styleClass="panel-ruler" />
								<table cellpadding="0" cellspacing="0" class="ruler">
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Distribution Amount:</td>
										<td class="column-input">
											<h:inputText
												id="distributionAmountControl" 
												style="text-align: right"
												value="#{currentSplittingToolBean[0].distributionAmount}"
												disabled="#{currentSplittingToolBean[0].inDeleteAction}">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-label">Freight Amount:</td>
										<td class="column-input">
											<h:inputText
												id="freightAmountControl" 
												style="text-align: right"
												value="#{currentSplittingToolBean[0].freightAmount}" 
												disabled="#{currentSplittingToolBean[0].inDeleteAction}">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-label">Discount Amount:</td>
										<td class="column-input">
											<h:inputText
												id="discountAmountControl" 
												style="text-align: right"
												value="#{currentSplittingToolBean[0].discountAmount}" 
												disabled="#{currentSplittingToolBean[0].inDeleteAction}">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td colspan="2" class="column-label">&nbsp;</td>
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Comments</td>
										<td class="column-input" colspan="7">
											<h:inputText id="comments" value="#{currentSplittingToolBean[0].comments}" style="width:678px;" disabled="#{currentSplittingToolBean[0].inDeleteAction}"/>
										</td>
									</tr>
									<tr style="background: none repeat scroll 0 0 #79A3C9;">
										<td class="column-spacer">&#160;</td>
										<td class="column-label"><b>Total Distribution <br/>Amount:</b></td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="totalDistributionAmount" value="#{currentSplittingToolBean[0].totalDistributionAmount}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-label"><b>Remaining Distribution <br/>Amount:</b></td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingDistributionAmount" value="#{currentSplittingToolBean[0].remainingDistributionAmount}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td colspan="4" class="column-label">&nbsp;</td>
									</tr>
								</table>
							</div>
				    	</div>
				    </div>
				</div>
				
			    <div id="table-four">
				     <div id="table-one-bottom">
						<h:outputText id="pageInfo" value="#{currentSplittingToolBean[0].transactionDataModel.pageDescription}"/>
						<ul class="right">
							<li class="jurissplitenabled">
				      			<h:commandLink id="jurisSplitEnabledBtn" disabled="${!currentSplittingToolBean[0].displayAddJurisSplitEnabled}" action="#{currentSplittingToolBean[0].jurisSplitAction}" />
				      		</li>
				      		<li class="jurissplit">
				        		<h:commandLink id="jurisSplitBtn" disabled="${!currentSplittingToolBean[0].displayAddJurisSplit}" action="#{currentSplittingToolBean[0].jurisSplitAction}" />
				      		</li>
				      		<li class="ok">
				        		<h:commandLink id="saveBtn" action="#{currentSplittingToolBean[0].okAction}" />
				      		</li>
				      		<li class="cancel">
				        		<h:commandLink id="cancelBtn" action="#{currentSplittingToolBean[0].manageCancelAction}" />
				      		</li>
				      	</ul>
				     </div>
			    </div>
			</h:form>
			
			<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
				<ui:param name="handler" value="#{currentSplittingToolBean[0].driverHandler}"/>
			</ui:include>
			
			<ui:include src="/WEB-INF/view/components/generic_warning.xhtml">
				<ui:param name="id" value="individualJurisWarningId"/>
				<ui:param name="formId" value="individualJurisWarningFormId"/>
				<ui:param name="panelGridId" value="individualJurisWarningPanelGridId"/>
				<ui:param name="okBtnId" value="individualJurisWarningOkBtnId"/>
				<ui:param name="title" value="Warning"/>
				<ui:param name="warning" value="Global Allocation will be deleted. Continue?"/>
				<ui:param name="okBtn" value="matrixDetailForm:jurisSplitBtn"/>
				<ui:param name="bean" value="#{currentSplittingToolBean[0]}"/>
				<ui:param name="cancelAction" value="individualJurisWarningCancel"/>
			</ui:include>
   		</ui:define>
 	</ui:composition>
</html>
