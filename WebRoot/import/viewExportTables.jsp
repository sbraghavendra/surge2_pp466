<?xml version="1.0" encoding="iso-8859-1"?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:c="http://java.sun.com/jstl/core"
	xmlns:rich="http://richfaces.org/rich"
	xmlns:t="http://myfaces.apache.org/tomahawk"
	xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
	<ui:define name="script">
		<script type="text/javascript">
			//         
			registerEvent(window, "load",
					function() {
						selectRowByIndex('exportForm:exportTable',
								'tableTableRowIndex');
					});
			//
		</script>
	</ui:define>
	<ui:define name="body">
		<h:inputHidden id="tableTableRowIndex"
			value="#{configImportBean.selectedTableIndex}" />
		<h:form id="exportForm" enctype="multipart/form-data">

			<div id="top">
				<div id="table-one" style="width: 1000px;">
					<div id="table-four-top">
						<a4j:outputPanel id="msg">
							<h:messages errorClass="error" />
						</a4j:outputPanel>
					</div>
					<div id="table-one-content">
						<table cellpadding="0" cellspacing="0" width="1000px"
							id="rollover" class="ruler">
							<thead>
								<tr>
									<td colspan="2">Export Table</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th colspan="2">Select Table to export</th>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="table-four-top"></div>
					<div id="table-four-content">
						<div class="scrollContainer">
							<div class="scrollInner">
								<rich:dataTable rowClasses="odd-row,even-row"
									styleClass="GridContent" style="width: 900px" id="exportTable"
									value="#{configImportBean.dataDefinitionTableList}"
									var="dataDeftblDTO">
									<a4j:support event="onRowClick"
										onsubmit="selectRow('exportForm:exportTable', this);"
										actionListener="#{configImportBean.selectedTableRowChanged}"
										reRender="createTemplate,exportAction,btnCancel"
										oncomplete="initScrollingTables();" />
									<rich:column id="tableName"
										sortBy="#{dataDeftblDTO.description}" width="450px">
										<f:facet name="header">
											<h:outputText styleClass="headerText" value="Table"
												style="color: blue;" />
										</f:facet>
										<h:outputText
											value="#{dataDeftblDTO.description!=null?dataDeftblDTO.description:dataDeftblDTO.tableName}" />
									</rich:column>

									<rich:column id="tableDescription"
										sortBy="#{dataDeftblDTO.tableName}" width="450px">
										<f:facet name="header">
											<h:outputText styleClass="headerText" value="Name"
												style="color: blue" />
										</f:facet>
										<h:outputText value="#{dataDeftblDTO.tableName}" />
									</rich:column>

								</rich:dataTable>
							</div>
						</div>
					</div>
					<div id="table-four-bottom">
						<ul class="right">
							<li class="createTemplate"><h:commandLink
									id="createTemplate" action="#{configImportBean.createTemplate}"
									immediate="true"
									disabled="#{!configImportBean.displayCreateTemplate}" /></li>
							<li class="export"><h:commandLink id="exportAction"
									immediate="true" disabled="#{!configImportBean.displayExport}"
									action="#{configImportBean.exportTableToFile}" /></li>
							<li class="close"><h:commandLink id="btnCancel"
									immediate="true" disabled="#{!configImportBean.displayCancel}"
									action="#{configImportBean.cancelAction}" /></li>
						</ul>
					</div>
				</div>
			</div>
		</h:form>
	</ui:define>
</ui:composition>
</html>