<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   registerEvent(window, "load", function() { selectRowByIndex('importMapForm:batchTable', 'selectedRowIndex'); } );
   registerEvent(window, "load", adjustHeight);
   //]]>
   </script>
  </ui:define>

	<ui:define name="body">
    <h:inputHidden id="selectedRowIndex" value="#{importMapSaleBean.selectedRowIndex}" />
  <h:form id="importMapForm">
   <div id="bottom">
    <h1>
     <h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process" value="/images/headers/hdr-import-map-and-process-sale.gif" />
    </h1>
    <div id="table-four">
     <div id="table-four-top">
     
       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{importMapSaleBean.batchMaintenanceDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 	
     
       <h:messages errorClass="error" />
     </div>
     <div id="table-four-content">
       <ui:include src="/WEB-INF/view/components/batch_status.xhtml">
        <ui:param name="bean"
                  value="#{importMapSaleBean}" />
		<ui:param name="showSelected"
                  value="true" />
        <ui:param name="onsubmit"
                  value="selectRow('importMapForm:batchTable', this);" />
        <ui:param name="reRender"
                  value="errorActionId,processActionId,statisticsActionId,viewActionId"/>
       </ui:include>
     </div>     <!-- t4-content -->

      <div id="table-four-bottom">
       <ul class="right">
        <!--
        <li class="view">
         <h:commandLink id="viewActionId" action="#{importMapSaleBean.viewAction}"
                        disabled="#{! importMapSaleBean.displayView}"></h:commandLink>
        </li>
        -->
        <li class="add">
         <h:commandLink id="addAction" action="#{importMapSaleBean.addImportMapAction}"
                        rendered="true"></h:commandLink>
        </li>
        <li class="select-all">
         <h:commandLink id="selectAllAction" action="#{importMapSaleBean.selectAllAction}"
                        rendered="true"></h:commandLink>
        </li>
        <li class="process">
          <h:commandLink id="processActionId"
                         disabled="#{! importMapSaleBean.displayProcess}"
                         action="#{importMapSaleBean.processAction}" />
        </li>
        <li class="statistics">
          <h:commandLink id="statisticsActionId"
                         disabled="#{(importMapSaleBean.selectedRowIndex == -1)}"
                         action="#{importMapSaleBean.statisticsAction}" />
        </li>
        <li class="error3">
          <h:commandLink id="errorActionId"
                         disabled="#{! importMapSaleBean.displayError}"
                         action="#{importMapSaleBean.errorsAction}" />
        </li>
        <li class="refresh">
         <h:commandLink value="" id="btnRefresh"
                        action="#{importMapSaleBean.refreshAction}"
                        rendered="true"></h:commandLink>
        </li>
       </ul>
      </div>      <!-- t4-bottom -->
    </div>
    <!-- t4 -->
   </div>
   <!-- bottom -->
  </h:form>
	</ui:define>
 </ui:composition>
</html>
