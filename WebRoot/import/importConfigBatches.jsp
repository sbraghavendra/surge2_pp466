<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   registerEvent(window, "load", function() { selectRowByIndex('configImportForm:batchTable', 'selectedBatchIndex'); } );
   registerEvent(window, "load", adjustHeight);
   //]]>
   </script>
  </ui:define>

<ui:define name="body">
    <h:inputHidden id="selectedBatchIndex"
                   value="#{configImportBean.selectedBatchIndex}" />
    <h:form id="configImportForm">

    <h:panelGroup rendered="#{!configImportBean.findMode}">
     	
     	<h1><h:graphicImage id="configurationBatches" alt="Configuration Batches" value="/images/headers/hdr-configuration_batches.png" width="250" height="19"/></h1>
    </h:panelGroup>
	<h:panelGroup rendered="#{configImportBean.findMode}">
		<h1><h:graphicImage id="configurationBatches1" alt="Configuration Batches" value="/images/headers/hdr-configuration_batches.png" width="250" height="19"/></h1>
    </h:panelGroup>
   <div id="bottom">
   <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
  	 		<ui:param name="bean" value="#{configImportBean}"/>
  	 		<ui:param name="configBatches" value="true"/>		
   </a4j:include>
   <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
    
    <div id="table-four">
     <div id="table-four-top">
     
       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{configImportBean.batchMaintenanceDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 	
     
       <h:message for = "configImportForm" errorClass="error" />
       
     </div>
     <div id="table-four-content">
       <ui:include src="/WEB-INF/view/components/batch_status.xhtml">
        <ui:param name="bean"
                  value="#{configImportBean}" />
        <ui:param name="showSelected"
                  value="true" />
		<ui:param name="onsubmit"
                  value="selectRow('configImportForm:batchTable', this);" />
        <ui:param name="reRender"
                  value="errorActionId,processActionId,statisticsActionId,viewActionId,importAction,exportAction,updateBatch"/>
       </ui:include>
     </div>     <!-- t4-content -->

      <div id="table-four-bottom">
       <ul class="right">
           <li class="update">
               <h:commandLink id="updateBatch"
                              disabled="#{(configImportBean.selectedBatchIndex == -1 or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)}"
                              style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                              immediate="true"
                              action="#{configImportBean.updateUserFieldsAction}" />
           </li>
        <li class="view">
         <h:commandLink id="viewActionId" action="#{configImportBean.viewAction}"
                        disabled="#{(configImportBean.selectedBatchIndex == -1 )}"></h:commandLink>
        </li>
        <li class="importCustLocn">
         <h:commandLink id="importAction" action="#{configImportBean.importAction}"
                        disabled="#{!configImportBean.displayImport or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                        style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
        </li>
         <li class="export">
         <h:commandLink id="exportAction" action="#{configImportBean.viewExportAction}"
                        disabled="#{!configImportBean.displayExport or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                        style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"></h:commandLink>
        </li>
        <li class="select-all">
         <h:commandLink id="selectAllAction" action="#{configImportBean.selectAllAction}"></h:commandLink>
        </li>
        <li class="process">
          <h:commandLink id="processActionId"
                         disabled="#{! configImportBean.displayProcess or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         action="#{configImportBean.processAction}" />
        </li>
        <li class="statistics">
          <h:commandLink id="statisticsActionId"
                         disabled="#{(configImportBean.selectedBatchIndex == -1)}"
                         action="#{configImportBean.statisticsAction}" />
        </li>
        <li class="error3">
          <h:commandLink id="errorActionId"
                         disabled="#{! configImportBean.displayError}"
                         action="#{configImportBean.errorsAction}" />
        </li>
        
       </ul>
      </div>      <!-- t4-bottom -->
    </div>
    <!-- t4 -->
   </div>
   <!-- bottom -->
  
  </h:form>
 
	</ui:define>
 </ui:composition>
</html>
