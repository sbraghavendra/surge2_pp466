<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   registerEvent(window, "load", function() { selectRowByIndex('importMapForm:batchTable', 'selectedRowIndex'); } );
   registerEvent(window, "load", adjustHeight);
   //]]>
   </script>
  </ui:define>

	<ui:define name="body">
    <h:inputHidden id="selectedRowIndex" value="#{importMapBean.selectedRowIndex}" />
  <h:form id="importMapForm">
   <div id="bottom">
    <h1><h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process" rendered="#{importMapBean.isPurchasingMenu and !importMapBean.initialAOLOnFlag}" url="/images/headers/hdr-import-map-and-process.gif" width="250" height="19" /></h1>
    <h1><h:graphicImage id="imgImportMapProcessSales" alt="Import/Map/Process" rendered="#{!importMapBean.isPurchasingMenu and !importMapBean.initialAOLOnFlag}" url="/images/headers/hdr-import-map-and-process-sale.gif" width="250" height="19" /></h1>
    <h1><h:graphicImage id="imgImportMapProcessAlert" alt="Import/Map/Process" rendered="#{importMapBean.initialAOLOnFlag}" url="/images/headers/hdr-alert-on-logon-batches.png" width="250" height="19" /></h1>
    
   <h:panelGroup id="aolBatchPanel" rendered="#{!importMapBean.initialAOLOnFlag}" >
     <a4j:include id = "batchSelectionIncludeId" ajaxRendered="true" viewId ="/WEB-INF/view/components/batchSelectionFilter.xhtml" > 
    	 <ui:param name="bean" value="#{importMapBean}"/>
    </a4j:include>
   </h:panelGroup>
   <div class="wrapper">
      <span class="block-right">&#160;</span>
      <span class="block-left tab">
       <img src="../images/containers/STSView-batches.gif"
            width="192"
            height="17" />
      </span>
     </div>
    <div id="table-four">
     <div id="table-four-top">
     
       <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{importMapBean.batchMaintenanceDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 	
     
       <h:messages errorClass="error" />
       <h:outputLabel id="messageaol"
                      class="error"
                      value="#{importMapBean.messageAOL}"
                      rendered="#{importMapBean.initialAOLOnFlag}" />
     </div>
    <div id="table-four-content">
       <a4j:include viewId="/WEB-INF/view/components/batch_status.xhtml">
        <ui:param name="bean"
                  value="#{importMapBean}" />
		<ui:param name="showSelected"
                  value="#{!importMapBean.initialAOLOnFlag}" />
        <ui:param name="onsubmit"
                  value="selectRow('importMapForm:batchTable', this);" />
        <ui:param name="reRender"
                  value="errorActionId,processActionId,statisticsActionId,viewActionId,updateBatch"/>
       </a4j:include>
     </div>      <!-- t4-content -->

      <div id="table-four-bottom">
       <ul class="right">
           <li class="update">
               <h:commandLink id="updateBatch"
                              disabled="#{(importMapBean.selectedRowIndex == -1 or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)}"
                              style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                              immediate="true"
                              action="#{importMapBean.updateUserFieldsAction}" />
           </li>

           <li class="view">
         <h:commandLink id="viewActionId" action="#{importMapBean.viewAction}"
                        disabled="#{! importMapBean.displayView}"></h:commandLink>
        </li>
        <li class="add">
         <h:commandLink id="addAction" action="#{importMapBean.addImportMapAction}"
          				disabled = "#{dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                        style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                        rendered="#{!importMapBean.initialAOLOnFlag}"></h:commandLink>
        </li>
        <li class="select-all">
         <h:commandLink id="selectAllAction" action="#{importMapBean.selectAllAction}"
                        rendered="#{!importMapBean.initialAOLOnFlag}"></h:commandLink>
        </li>
        <li class="process">
          <h:commandLink id="processActionId"
                         disabled="#{! importMapBean.displayProcess or dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag}"
                         style="#{(dataUtilityGLExtractBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}"
                         action="#{importMapBean.processAction}" />
        </li>
        <li class="statistics">
          <h:commandLink id="statisticsActionId"
                         disabled="#{(importMapBean.selectedRowIndex == -1)}"
                         action="#{importMapBean.statisticsAction}" />
        </li>
        <li class="error3">
          <h:commandLink id="errorActionId"
                         disabled="#{! importMapBean.displayError}"
                         action="#{importMapBean.errorsAction}" />
        </li>
        <li class="refresh">
         <h:commandLink value="" id="btnRefresh"
                        action="#{importMapBean.refreshAction}"
                        rendered="#{!importMapBean.initialAOLOnFlag}"></h:commandLink>
        </li>

       </ul>
      </div>      <!-- t4-bottom -->
    </div>
    <!-- t4 -->
   </div>
   <!-- bottom -->
  </h:form>
	</ui:define>
 </ui:composition>
</html>
