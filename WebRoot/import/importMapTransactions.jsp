<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  <ui:define name="script">
   <script type="text/javascript">
   //<![CDATA[
   		registerEvent(window, "load", function() { selectRowByIndex('transForm:aMDetailList', 'aMDetailListRowIndex'); } );
   		registerEvent(window, "load", function() { displayWarning(); } );
   		
   		function displayWarning() {
   		 var warning = document.getElementById('displayWarning');
   		 var val = parseInt(warning.value);
   		 if (val != 0) {
   		 	javascript:Richfaces.showModalPanel('warning');
   		 }
   		}
   		
   		function resetState()
   		{ 
   		    document.getElementById("filterForm:state").value = "";
   		}
   //]]>
   </script>
  </ui:define>

  <ui:define name="body">
  	<h:inputHidden id="aMDetailListRowIndex" value="#{bcpTransactionDetailBean.selectedRowIndex}"/>
  	<h:inputHidden id="displayWarning" value="#{bcpTransactionDetailBean.futureSplitWarning}"/>
    <div id="bottom">
    
    <h:form id="filterForm">
    	<a4j:queue/> 
    	<h1>
    	<table cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td align="left" >
	    				<h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process/Transactions" value="/images/headers/hdr-import-map-and-process.gif" />
	    			</td>
	    			<td width="200" align="right" >
	    				<a4j:commandLink id="viewTrans" style="height:22px;text-decoration: underline;color:#0033FF"
       						rendered="#{importMapBean.isPcoBatch}" action="importmap_saletransactions" >
       						<h:outputText value="View Sale Transactions" style="width:200px"/>
    					</a4j:commandLink>	
					</td>
					<td width="30">
					</td>
    			</tr>
			</tbody>
		</table>  	
    	</h1>
    	<div class="tab">
			<h:graphicImage id="image2"
  				 alt="Selection Filter" 
  				 url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
     		<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{bcpTransactionDetailBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(bcpTransactionDetailBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       		</a4j:commandLink>		
     	</div> 
     	
     	<a4j:outputPanel id="showhidefilter">
	       	<c:if test="#{!bcpTransactionDetailBean.togglePanelController.isHideSearchPanel}">
		     	<div id="top">
		      	<div id="table-one">
					<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
						<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/taxBcpSearch_panel.xhtml">
							<ui:param name="handlerBean" value="#{bcpTransactionDetailBean}"/>
							<ui:param name="readonly" value="false"/>					
						</a4j:include>
					</a4j:outputPanel >
		      	</div>
		     	</div>
			</c:if>  
		</a4j:outputPanel>
     	
    	<!--     	
    	<div id="top">
			<div id="table-one">
				<div id="table-one-top">
					<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
				</div>
				
				<div id="table-one-content" style="height:auto;" onkeyup="return submitEnter(event,'filterForm:searchBtn')" >
					<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
						<tbody>
							<tr>
								<td class="column-spacer">&#160;</td>								
								<td class="column-label"> Country: </td> 
								<td class="column-input">
									<h:selectOneMenu id="countryMenuMain" 
										value="#{bcpTransactionDetailBean.selectedCountry}"
										immediate="true"
										valueChangeListener="#{bcpTransactionDetailBean.searchCountryChanged}" 
										onchange="resetState();submit();"
								
										>
										<f:selectItems value="#{bcpTransactionDetailBean.countryMenuItems}"/>
									</h:selectOneMenu>
								</td>
								
								<td class="column-label"> State: </td> 
								<td class="column-input">
									<h:selectOneMenu id="state"
										value="#{bcpTransactionDetailBean.selectedState}">
										<f:selectItems value="#{bcpTransactionDetailBean.stateMenuItems}"/>
									</h:selectOneMenu>
								</td>
								<td class="column-label">&#160;</td> 
								<td class="column-input">&#160;</td>
								<td class="column-spacer">&#160;</td>
							</tr>
						</tbody>
					</table>
					<h:panelGrid id="driverPanel" binding="#{bcpTransactionDetailBean.filterSelectionPanel}" styleClass="panel-ruler" />
				</div>
				
				<div id="table-one-bottom">
					<ul class="right">
						<li class="clear">
							<a4j:commandLink id="btnClear" action="#{bcpTransactionDetailBean.resetFilter}" 
								reRender="state,driverPanel"/>
						</li>
						<li class="search">
							<a4j:commandLink id="searchBtn"
							action="#{bcpTransactionDetailBean.listTransactions}" 
							reRender="aMDetailList,trScroll,pageInfo" oncomplete="initScrollingTables();"   >
							</a4j:commandLink>
						</li>
					</ul>
				</div>
			</div>
		</div>
    	-->
    	
    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
    </h:form>
    
    <div class="wrapper">
		<span class="block-right">&#160;</span> 
		<span class="block-left-no-bg tab">
			

			<img src="../images/containers/STSView-transactions-open.gif"/>
			
		</span>
	</div>
	
	
	
	
	<!--  
	<div id="links">
	<h:commandLink id="viewTrans" rendered="#{importMapBean.isPcoBatch}"  action="importmap_saletransactions" >
		<h:outputText value="View Sale Transactions"/></h:commandLink></div>
    -->
    <h:form id="transForm">
    <div id="table-four">
     <div id="table-four-top" style="padding-left: 23px;">
     	<table cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
				<td align="left" >
					<a4j:status id="pageInfoHeader"  
						startText="Request being processed..." 
						stopText="View Transactions for Batch # #{bcpTransactionDetailBean.batchId}"
						onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
						
				</td>

				<td align="right" width="200">
				<rich:spacer width="20px"/>
					<h:selectBooleanCheckbox id="chkDisplayAllFields" value="#{bcpTransactionDetailBean.displayAllFields}" styleClass="check">
						<a4j:support event="onclick" reRender="aMDetailList,pageInfo"
							actionListener="#{bcpTransactionDetailBean.displayChange}"
							oncomplete="initScrollingTables();"/>
					</h:selectBooleanCheckbox>
					<h:outputText value="Display All Fields"/>
				</td>
				<td style="width:20px;">&#160;</td>
				</tr>
			</tbody>
		</table>
     </div>
     
     <div id="table-four-content">
     	<ui:include src="/WEB-INF/view/components/transactiondetail_table.xhtml">
			<ui:param name="formName" value="transForm"/>
			<ui:param name="bean" value="#{bcpTransactionDetailBean}"/>
			<ui:param name="dataModel" value="#{bcpTransactionDetailBean.bcpTransactionDataModel}"/>
			<ui:param name="singleSelect" value="true"/>
			<ui:param name="multiSelect" value="false"/>
			<ui:param name="doubleClick" value="true"/>
			<ui:param name="selectReRender" value="pageInfo,splitBtn,futureSplitBtn"/>
			<ui:param name="scrollReRender" value="pageInfo,splitBtn,futureSplitBtn"/>
		</ui:include>
     </div>
     
     <div id="table-four-bottom">
		<h:outputText id="pageInfo" value="#{bcpTransactionDetailBean.bcpTransactionDataModel.pageDescription}"/>
		<ul class="right">
			<li class="futuresplit">
        		<h:commandLink id="futureSplitBtn" disabled="#{!bcpTransactionDetailBean.displayFutureSplit}" action="#{bcpTransactionDetailBean.futureSplitAction}" />
      		</li>
			<li class="split">
        		<h:commandLink id="splitBtn" disabled="#{!bcpTransactionDetailBean.displaySplit}" action="#{bcpTransactionDetailBean.splitAction}" />
      		</li>
      		<li class="close">
        		<h:commandLink id="closeBtn" action="#{bcpTransactionDetailBean.closeAction}" />
      		</li>
      	</ul>
     </div>
    </div>
    
    </h:form>
    <!-- t4 -->
   </div>
   <!-- bottom -->
   
   
   	<ui:include src="/WEB-INF/view/components/driver_search.xhtml">
		<ui:param name="handler" value="#{bcpTransactionDetailBean.driverHandler}"/>
	</ui:include>
   
   	<ui:include src="/WEB-INF/view/components/warning.xhtml">
		<ui:param name="bean" value="#{bcpTransactionDetailBean}"/>
		<ui:param name="id" value="warning"/>
		<ui:param name="warning" value="Warning - Flag Transaction #{bcpTransactionDetailBean.selectedTransaction.bcpTransactionId} for future split?"/>
		<ui:param name="okBtn" value="transForm:futureSplitBtn"/>
	</ui:include>

   </ui:define>
 </ui:composition>
</html>
