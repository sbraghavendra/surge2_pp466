<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 	<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  		<ui:define name="script">
		   <script type="text/javascript">
		   //<![CDATA[
		   		registerEvent(window, "load", function() { selectRowByIndex('matrixDetailForm:aMDetailListSplit', 'aMDetailListSplitRowIndex'); } );
		   		registerEvent(window, "load", function() { displayWarnings(); } );
		   		
		   		function displayWarnings() {
		   			displayWarning();
		   			displayGlobalJurisWarning();
		   			displayIndividualJurisWarning();
		   		}
		   		
		   		function displayWarning() {
		   		 var warning = document.getElementById('displayWarning');
		   		 var val = parseInt(warning.value);
		   		 if (val != 0) {
		   		 	javascript:Richfaces.showModalPanel('warning');
		   		 }
		   		}
		   		
		   		function displayGlobalJurisWarning() {
		   		 var warning = document.getElementById('displayGlobalJurisWarning');
		   		 var val = parseInt(warning.value);
		   		 if (val != 0) {
		   		 	javascript:Richfaces.showModalPanel('globalJurisWarningId');
		   		 }
		   		}

		   		function displayIndividualJurisWarning() {
		   		 var warning = document.getElementById('displayIndividualJurisWarning');
		   		 var val = parseInt(warning.value);
		   		 if (val != 0) {
		   		 	javascript:Richfaces.showModalPanel('individualJurisWarningId');
		   		 }
		   		}
		   //]]>
		   </script>
  		</ui:define>

  		<ui:define name="body">
  			<h:inputHidden id="displayWarning" value="#{currentSplittingToolBean[0].displayWarning}"/>
  			<h:inputHidden id="displayGlobalJurisWarning" value="#{currentSplittingToolBean[0].displayGlobalJurisWarning}"/>
			<h:inputHidden id="displayIndividualJurisWarning" value="#{currentSplittingToolBean[0].displayIndividualJurisWarning}"/>
  			<h:inputHidden id="aMDetailListSplitRowIndex" value="#{currentSplittingToolBean[0].selectedSplitRowIndex}"/>
  			<h:form id="matrixDetailForm">
	  			<a4j:queue/>
	  			<div id="bottom">
					<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-allocate-transactions.png" width="250" height="19" /></h1>
				    				    
				    <div id="top">
				    	<div id="table-one">
				    		<div id="table-four-top">
								<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
							</div>
							
							<div id="table-one-content">
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
									<thead>
										<tr>
											<td colspan="5">Transaction Splitting Tool</td>
											<td colspan="4" style="text-align:right;">Transaction ID: ${currentSplittingToolBean[0].currentTransactionId}&#160;</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5">Drivers</th>
											<th colspan="41"></th>
										</tr>
									</tbody>
								</table>
								
				<!-- 				<h:panelGrid id="driverPanel" rendered="true" binding="#{currentSplittingToolBean[0].filterSelectionPanel}" styleClass="panel-ruler" /> -->
								
								<h:panelGrid id="driverPanel" rendered="true" binding="#{currentSplittingToolBean[0].originalFilterSelectionPanel}" styleClass="panel-ruler" />
								
								
								
								<table cellpadding="0" cellspacing="0" width="100%" class="ruler">
									<tr>
										<th colspan="7">Amounts</th>
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Distribution Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="glLineItmDistAmt" value="#{currentSplittingToolBean[0].currentTransaction.glLineItmDistAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Distribution Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingGlLineItmDistAmt" value="#{currentSplittingToolBean[0].remainingGlLineItmDistAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>	
										<td class="column-spacer"  style="width:20%" >&#160;</td>									
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Freight Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceFreightAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceFreightAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Freight Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingInvoiceFreightAmt" value="#{currentSplittingToolBean[0].remainingInvoiceFreightAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer" style="width:20%" >&#160;</td>
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Discount Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceDiscountAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceDiscountAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Discount Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingInvoiceDiscountAmt" value="#{currentSplittingToolBean[0].remainingInvoiceDiscountAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer" style="width:20%" >&#160;</td>
									</tr>
								</table>
								
<a4j:outputPanel id="forEachPanelWhen">
	<span>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>		
		<tr>
			<th colspan="12">Split Transactions</th>
		</tr>
			
		<tr>
        	<td style="width:20px;">&#160;</td>    
	        <td><h:outputText style="width: 180px;" value="TaxCode" /></td>
	        <td><h:outputText style="width: 250px;" value="Description" /></td>
	        
	        <td><h:outputText style="width: 100px;" value="Distribution Amount" /></td>
	        <td><h:outputText style="width: 100px;" value="Freight Amount" /></td>
	        <td><h:outputText style="width: 100px;" value="Discount Amount" /></td>
        	
			<td width="50px">&#160;</td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px">
				<a4j:commandLink id="addWhenTop" styleClass="button" rendered="#{!(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" 
					reRender="forEachPanelWhen" actionListener="#{currentSplittingToolBean[0].processAddWhenCommand}" >
					<h:graphicImage value="/images/addButton.png" />
					<f:param name="command" value="#{loopCounter.index}" />
				</a4j:commandLink>
				<h:outputText style="width: 15px;" value="&#160;" rendered="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" />
			</td> 
			<td width="1%">&#160;</td> 
		</tr>
		  
		<c:forEach var="taxAllocMatrixDetail" items="#{currentSplittingToolBean[0].selectedMatrixTaxAllocDetails}" varStatus="loopCounter" >
        <tr>
        	<td style="width:20px;">&#160;</td>
        	
	        <td>  
	        	<a4j:outputPanel id="fieldpanelWhen_#{loopCounter.index}">   
	        		<h:selectOneMenu id="allWhen_#{loopCounter.index}"  style="width: 180px;" value="#{taxAllocMatrixDetail.taxcodeCode}" disabled="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" >
						<f:selectItems value="#{currentSplittingToolBean[0].taxCodeItems}"/>
						
						<a4j:support id="fieldTaxCode_#{loopCounter.index}" event="onchange" immediate="true" reRender="forEachPanelWhen" actionListener="#{currentSplittingToolBean[0].taxcodeCodeSelected}" >
							<f:param name="command" value="#{loopCounter.index}" />
						</a4j:support>
					</h:selectOneMenu>	
				</a4j:outputPanel>
	        </td>
	        
	        <td>           	
				<a4j:outputPanel id="fieldDescription_#{loopCounter.index}">   
					<h:inputText id="fieldvalueDescription_#{loopCounter.index}" style="width: 250px;" value="#{not empty taxAllocMatrixDetail.description ? taxAllocMatrixDetail.description : cacheManager.taxCodeMap[taxAllocMatrixDetail.taxcodeCode].description}" disabled="true" />				 	
	        	</a4j:outputPanel>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fielddistributionpanel_#{loopCounter.index}">   
		
					<h:inputText id="fielddistributionvalue_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.glLineItmDistAmt}" onkeypress="return onlyNumerics(event);"
						disabled="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" >
						<a4j:support id="ajaxsdistributionlistener_#{loopCounter.index}" event="onblur" reRender="remainingGlLineItmDistAmt,saveasmatrixBtn,saveBtn,oktaxabilityBtn,oktaxjurisBtn" ajaxSingle="true" />
						<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fieldfreightpanel_#{loopCounter.index}">   
		
					<h:inputText id="fieldfreightvalue_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.invoiceFreightAmt}" onkeypress="return onlyNumerics(event);"
						disabled="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" >
						<a4j:support id="ajaxsfreightlistener_#{loopCounter.index}" event="onblur" reRender="remainingInvoiceFreightAmt" ajaxSingle="true" />
						<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>	
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fielddiscountpanel_#{loopCounter.index}">   
		
					<h:inputText id="fielddiscountvalue_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.invoiceDiscountAmt}" onkeypress="return onlyNumerics(event);"
						disabled="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" >
						<a4j:support id="ajaxsdiscountlistener_#{loopCounter.index}" event="onblur" reRender="remainingInvoiceDiscountAmt" ajaxSingle="true" />
						<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>
	   <!--   
	        <td> 
	        	<a4j:outputPanel id="fieldvaluepaneWhen_#{loopCounter.index}">   
		
					<h:inputText id="fieldvalueWhen_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.allocationPercent}" onkeypress="return onlyNumerics(event);"
						disabled="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" >
						<a4j:support id="ajaxslistener_#{loopCounter.index}" event="onblur" reRender="balance" ajaxSingle="true" />
						<f:convertNumber minFractionDigits="2" maxFractionDigits="10"/>
						<f:validateDoubleRange minimum="0" maximum="1"/>	
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>      
	 -->   
        
        
			<td width="50px">&#160;</td>
	 	
			<td width="50px">		
				<a4j:commandButton id="splitDetailBtn1_#{loopCounter.index}" 
			                	rendered="#{currentSplittingToolBean[0].jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId]}"
			                	oncomplete="initScrollingTables();"
			                	type="button" styleClass="image" style="float:left;width:16px;height:16px;" 
			                	action="#{currentSplittingToolBean[0].individualJurisSplitAction_jeesmon}"
			                 	image="/images/splitEnabledButton.png" immediate="true">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{currentSplittingToolBean[0].jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
				
			                
			               <a4j:commandButton id="splitDetailBtn2_#{loopCounter.index}" 
			                	rendered="#{!currentSplittingToolBean[0].jurisSplitInfoMap[taxAllocMatrixDetail.instanceCreatedId] and !currentSplittingToolBean[0].displayDeleteAction}"
			                	oncomplete="initScrollingTables();"
			                	type="button" styleClass="image" style="float:left;width:16px;height:16px;" 
			                	action="#{currentSplittingToolBean[0].individualJurisSplitAction_jeesmon}"
			                	image="/images/splitButton.png" immediate="true">
								<a4j:actionparam name="jurisSplitInstanceIdParam"
									assignTo="#{currentSplittingToolBean[0].jurisSplitInstanceId}"
									value="#{taxAllocMatrixDetail.instanceCreatedId}"/>
			                </a4j:commandButton>
        		
        		<h:outputText style="width: 15px;" value="&#160;" rendered="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction  or currentSplittingToolBean[0].displayUpdateAction)}" />   		  
        	</td>
		
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
        
			<td width="15px">
				<h:outputText style="width: 15px;" value="&#160;"  />
			</td>            
            <td width="15px">
            	<a4j:commandLink id="removeWhen_#{loopCounter.index}" styleClass="button" rendered="#{!(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" 
            		reRender="forEachPanelWhen,remainingGlLineItmDistAmt,remainingInvoiceFreightAmt,remainingInvoiceDiscountAmt" actionListener="#{currentSplittingToolBean[0].processRemoveWhenCommand}" >
            		<h:graphicImage value="/images/deleteButton.png" />
            		<f:param name="command" value="#{loopCounter.index}" />
            	</a4j:commandLink>
            	<h:outputText style="width: 15px;" value="&#160;" rendered="#{(currentSplittingToolBean[0].displayDeleteAction or currentSplittingToolBean[0].displayViewAction)}" />
            </td>
            
 			<td width="1%">&#160;</td> 
        </tr>
    	</c:forEach>
    	
    	<!--  
    	<tr>
        	<td style="width:20px;">&#160;</td>   
	        <td><h:outputText style="width: 180px;" value="&#160;" /></td>
	        
	        <c:if test="#{currentSplittingToolBean[0].displayDeleteAction}">
		     	<td align="center"><font color="red"><h:outputText style="width:470px; color:ref" 
		     		value="#{(currentSplittingToolBean[0].allowDeleteTaxAllocationMatrix) ? '&#160;' : 'The Matrix line has been used on a Transaction and cannot be deleted. Click Ok to set to inactive.'}" /></font></td>
	        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			</c:if>  
			
			<c:if test="#{!currentSplittingToolBean[0].displayDeleteAction}">
		     	<td align="right"><h:outputText style="width: 470px;" value="Remaining to Allocate:" /></td>
	        	<td class="column-input" style="width: 100px;" >&#160;
					<h:outputText id="balance" value="#{currentSplittingToolBean[0].remainingAllocationPercent}" >
						<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
					</h:outputText>
				</td>
			</c:if>
        	
			<td width="50px">&#160;</td>
        	<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>            
            <td width="15px"><h:outputText style="width: 15px;" value="&#160;" /></td>
			<td width="1%">&#160;</td> 
		</tr>
-->
		<tr>
        	<td colspan="15">&#160;</td>
		</tr>
	</tbody>
	</table>
	</span>
</a4j:outputPanel>								
								
							</div>
							
							<!-- 
							<div id="table-two-top" style="padding: 0px">
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
									<tr>
										<td class="column-spacer">&#160;</td>
										<th>Split Transactions:</th>
										<th>
											Total Distribution Amount: 
											<h:outputText value="#{currentSplittingToolBean[0].totalDistributionAmount}">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:outputText>
										</th>
										<th>
											Remaining Distribution Amount: 
											<h:outputText value="#{currentSplittingToolBean[0].remainingDistributionAmount}">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2"  maxFractionDigits="#{filePreferenceBackingBean.maxdecimalplace}"/>
											</h:outputText>
										</th>
									</tr>
								</table>
							</div>

 							--> 

						
				    	</div>
				    	
				    	<div id="table-four-bottom">
						<h:outputText id="pageInfo" value="#{currentSplittingToolBean[0].transactionDataModel.pageDescription}"/>
						<ul class="right">
						
						
							<li><h:commandLink id="individualJurisSplitHiddenBtn" style="display: none;" rendered="true" action="#{currentSplittingToolBean[0].individualJurisSplitAction_jeesmon}"/></li>	
							<!--  
				      		<li class="add">
				        		<h:commandLink id="addBtn" disabled="#{!currentSplittingToolBean[0].displayAdd}" action="#{currentSplittingToolBean[0].addAction}" />
				      		</li>
				      		<li class="update">
				        		<h:commandLink id="updateBtn" disabled="#{!currentSplittingToolBean[0].displayEdit}" action="#{currentSplittingToolBean[0].updateAction}" />
				      		</li>
				      		<li class="delete">
				        		<h:commandLink id="deleteBtn" disabled="#{!currentSplittingToolBean[0].displayEdit}" action="#{currentSplittingToolBean[0].deleteAction}" />
				      		</li>
				      		-->
				      		<li class="jurissplitenabled">
				      			<h:commandLink id="jurisSplitEnabledBtn" disabled="#{!currentSplittingToolBean[0].displayGlobalJurisSplitEnabled}" action="#{currentSplittingToolBean[0].globalJurisSplitAction}"/>
				      		</li>
				      		<li class="jurissplit">
				        		<h:commandLink id="jurisSplitBtn" disabled="#{currentSplittingToolBean[0].displayGlobalJurisSplitEnabled}" action="#{currentSplittingToolBean[0].globalJurisSplitAction}" />
				      		</li>
				      		<!--  
				      		<li class="viewsplit">
				        		<h:commandLink id="viewSplitBtn" disabled="#{!currentSplittingToolBean[0].displayEdit}" action="#{currentSplittingToolBean[0].viewSplitAction}" />
				      		</li>
				      		-->
				      		<li class="vieworiginal">
				        		<h:commandLink id="viewOriginalBtn" action="#{currentSplittingToolBean[0].viewCurrentAction}" />
				      		</li>
				      		<li class="saveasmatrix">
				        		<h:commandLink id="saveasmatrixBtn" disabled="#{!currentSplittingToolBean[0].displaySave}" action="#{currentSplittingToolBean[0].saveAsMatrixAction}" />
				      		</li>
				      			
				      		<li class="saveonetime">
				        		<h:commandLink id="saveBtn" disabled="#{!currentSplittingToolBean[0].displaySave}" action="#{currentSplittingToolBean[0].saveOneTimeAction}"
				        					   onclick="javascript:Richfaces.showModalPanel('processingLoader');" />
				      		</li>
				      		<rich:modalPanel id="processingLoader" zindex="2000" autosized="true">
				              <h:outputText value="Processing...."/>
				    	    </rich:modalPanel>
				      		<li class="cancel">
				        		<h:commandLink id="cancelBtn" action="#{currentSplittingToolBean[0].cancelAction}" />
				      		</li>
				      	</ul>
				     </div>
				     
				    </div>
				</div>
				
			
			    <!-- 
			     	<div id="table-four-content">
				     	<ui:include src="/WEB-INF/view/components/tax_alloc_detail_table.xhtml">
							<ui:param name="formName" value="matrixDetailForm"/>
							<ui:param name="bean" value="#{currentSplittingToolBean[0]}"/>
							<ui:param name="dataModel" value="#{currentSplittingToolBean[0].splitTransactionDataModel}"/>
							<ui:param name="singleSelect" value="true"/>
							<ui:param name="multiSelect" value="false"/>
							<ui:param name="doubleClick" value="true"/>
							<ui:param name="selectReRender" value="updateBtn,deleteBtn,viewSplitBtn,jurisSplitEnabledBtn,jurisSplitBtn"/>
							<ui:param name="scrollReRender" value="updateBtn,deleteBtn,viewSplitBtn,jurisSplitEnabledBtn,jurisSplitBtn"/>
						</ui:include>
				     </div>
				   -->   
				     
			  
			</h:form>
			
			<ui:include src="/WEB-INF/view/components/save_alloc_warning.xhtml">
				<ui:param name="bean" value="#{currentSplittingToolBean[0]}"/>
				<ui:param name="id" value="warning"/>
				<ui:param name="warning" value="Confirm Save Allocations"/>
				<ui:param name="okBtn" value="matrixDetailForm:saveBtn"/>
			</ui:include>
			
			<ui:include src="/WEB-INF/view/components/generic_warning.xhtml">
				<ui:param name="id" value="globalJurisWarningId"/>
				<ui:param name="formId" value="globalJurisWarningFormId"/>
				<ui:param name="panelGridId" value="globalJurisWarningPanelGridId"/>
				<ui:param name="okBtnId" value="globalJurisWarningOkBtnId"/>
				<ui:param name="title" value="Warning"/>
				<ui:param name="warning" value="Any individual Allocations will be deleted. Continue?"/>
				<ui:param name="okBtn" value="matrixDetailForm:jurisSplitBtn"/>
				<ui:param name="bean" value="#{currentSplittingToolBean[0]}"/>
				<ui:param name="cancelAction" value="globalJurisWarningCancel"/>
			</ui:include>

			<ui:include src="/WEB-INF/view/components/generic_warning.xhtml">
				<ui:param name="id" value="individualJurisWarningId"/>
				<ui:param name="formId" value="individualJurisWarningFormId"/>
				<ui:param name="panelGridId" value="individualJurisWarningPanelGridId"/>
				<ui:param name="okBtnId" value="individualJurisWarningOkBtnId"/>
				<ui:param name="title" value="Warning"/>
				<ui:param name="warning" value="Global Allocation will be deleted. Continue?"/>
				<ui:param name="okBtn" value="matrixDetailForm:individualJurisSplitHiddenBtn"/>
				<ui:param name="bean" value="#{currentSplittingToolBean[0]}"/>
				<ui:param name="cancelAction" value="individualJurisWarningCancel"/>
			</ui:include>

   		</ui:define>
 	</ui:composition>
</html>
