<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
	    xmlns:t="http://myfaces.apache.org/tomahawk">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
            registerEvent(window, "load", function() { selectRowByIndex('form:specTable', 'selectedRowIndex'); } );
            //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <h:inputHidden id="selectedRowIndex" value="#{importMapAddBean.selectedRowIndex}" />
      <h1><h:graphicImage id="imgImportMapProcess" alt="Import/Map/Process" rendered="#{importMapBean.isPurchasingMenu}" url="/images/headers/hdr-import-map-and-process.gif" width="250" height="19" /></h1>
      <h1><h:graphicImage id="imgImportMapProcessSales" alt="Import/Map/Process" rendered="#{!importMapBean.isPurchasingMenu}" url="/images/headers/hdr-import-map-and-process-sale.gif" width="250" height="19" /></h1>
      
      <img src="../images/containers/STSSelectSpec.gif" />
      <a4j:form id="form">
        <div id="top" style="margin-top: 4px">
          <div id="table-one">
            <div id="table-one-top">
              <a4j:outputPanel ajaxRendered="true" id="msg">
                <h:messages globalOnly="true" errorClass="error" />
              </a4j:outputPanel>
            </div>
            <div id="table-four-content" style="min-height: 160px">
              <div class="scrollContainer">
                <div class="scrollInner" style="height: 160px;">
                  <rich:dataTable rowClasses="odd-row,even-row" id="specTable"
                                  value="#{importMapAddBean.importSpecList}"
                                  var="row">
                    <a4j:support id="a4j-specTable"
                                 event="onRowClick"
                                 onsubmit="selectRow('form:specTable', this);"
                                 reRender="addForm:fileSelector,addForm:databaseSelector,addForm:fileDetails,addForm:addDbActionId,addForm:btnConnVerify"
                                 actionListener="#{importMapAddBean.selectedRowChanged}"
                                 />
                                 
                    <rich:column id="importSpecType">
                      <f:facet name="header">
                        <h:outputText value="Type" />
                      </f:facet>
                      <h:outputText id="rowImportSpecType" value="#{cacheManager.listCodeMap['IMPDEFTYPE'][row.importSpecType].description}" />
                    </rich:column>
                              
                    <rich:column id="importSpecCode">
                      <f:facet name="header">
                        <h:outputText value="Code" />
                      </f:facet>
                      <h:outputText id="rowImportSpecCode" value="#{row.importSpecCode}" />
                    </rich:column>
                    <rich:column id="description">
                      <f:facet name="header">
                        <h:outputText value="Description" />
                      </f:facet>
                      <h:outputText id="rowDescription" value="#{row.description}" />
                    </rich:column>
                    <!--Code modified/added as part of Midtier project in Dec 2008
                    <rich:column id="submitJobFlag">
                      <f:facet name="header">
                        <h:outputText value="Submit Job Flag" />
                      </f:facet>
                      <h:outputText value="#{row.submitJobFlag}" />
                    </rich:column>-->
                    <rich:column id="importDefinitionCode">
                      <f:facet name="header">
                        <h:outputText value="Import Definition Code" />
                      </f:facet>
                      <h:outputText id="rowImportDefCode" value="#{row.importDefinitionCode}" />
                    </rich:column>
                    <rich:column id="defaultDirectory">
                      <f:facet name="header">
                        <h:outputText value="Default Directory" />
                      </f:facet>
                      <h:outputText id="rowDefaultDirectory" value="#{row.defaultDirectory}" />
                    </rich:column>
                    <rich:column id="lastImportFileName">
                      <f:facet name="header">
                        <h:outputText value="Last Import File Name" />
                      </f:facet>
                      <h:outputText id="rowLastImportFileName" value="#{row.lastImportFileName}" />
                    </rich:column>
                    <rich:column id="comments">
                      <f:facet name="header">
                        <h:outputText value="Comments" />
                      </f:facet>
                      <h:outputText id="rowComments" value="#{row.comments}" />
                    </rich:column>
                  </rich:dataTable>
                </div>                <!-- scrollInner-->
              </div>              <!-- scrollContainer -->
            </div>            <!-- t1-content -->
          </div>          <!-- t1 -->
        </div>        <!-- top -->
      </a4j:form>

      <a4j:form id="addForm">
        <div class="wrapper">
          <span class="block-right">&#160;</span>
          <span class="block-left tab">
            <img src="../images/containers/STSSecurity-details-open.gif" />
          </span>
        </div>
        <div id="bottom">
          <div id="table-four">
            <div id="table-four-top">
            </div>
            <div id="table-four-content">
              <ul class="basic-form" id="selectImportFileLabel">
                <li class="heading">Select Import File</li>
              </ul>
              <a4j:outputPanel id="a4j-fileselector" ajaxRendered="true">
                <h:panelGrid id="fileSelector"
                             rendered="#{importMapAddBean.displayFileSelector and !importMapAddBean.isDatabaseImport}"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0">
                  <h:outputLabel value="Import File Type:" />
                  <h:outputText id="txtFileType"
                                value="#{cacheManager.listCodeMap['IMPFILETYP'][importMapAddBean.selectedDefinition.importFileTypeCode].description}" />
                  
                  <h:outputLabel value="Import File Name:" />
                  <a4j:region id="fileSelectRegion">
                    <h:panelGroup id="fileSelectorPanel">
                      <h:selectOneMenu
                          id="fileItemMenu"
                          binding="#{importMapAddBean.selectMenu}"
                          immediate="true"
                          style="width: 300px;">
                        <a4j:support id="a4j-file-listener"
                                     event="onchange"
                                     onsubmit="turnOff('addForm:addActionId', true)"
                                     oncomplete="turnOff('addForm:addActionId', false)"
                                     reRender="a4j-status-file-selector"
                                     actionListener="#{importMapAddBean.getFileDetails}" />
                      </h:selectOneMenu>
                      <rich:spacer width="6px"/>
                      
                      
                      <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       				  <a4j:status id="a4j-status-file-selector"  
							startText="Validating file..." 
							onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     						onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" /> 	

                      
                      <a4j:outputPanel id="msg">
                        <h:message errorClass="error"  warnClass="warning" for="fileItemMenu"/>
                      </a4j:outputPanel>
                      
                    </h:panelGroup>
                  
                  </a4j:region>

                </h:panelGrid>
                
                <h:panelGrid id="databaseSelector"
                           rendered="#{importMapAddBean.isDatabaseImport}"
                           columns="2"
                           styleClass="basic-form"
                           columnClasses="left-indent,right"
                           cellspacing="0"
                           cellpadding="0">
                  	<h:outputLabel value="Import File Type:" />
                  	<h:outputText id="txtImportFileType" value="#{cacheManager.listCodeMap['IMPFILETYP'][importMapAddBean.selectedDefinition.importFileTypeCode].description}" />
                  	<h:outputText value="Import File Name:" />
                  	<h:outputText id="txtImportFileName"  value="#{importMapAddBean.selectedDefinition.dbTableName}" />
                  	<h:outputText value="Verification:" />
                  	<h:outputText id="txtRowImporting"  value="#{importMapAddBean.verificationMessage}&#160;" />
              	</h:panelGrid>

              	<h:panelGrid id="fileDetails"
                           rendered="#{(! empty importMapAddBean.selectedFile) and (!importMapAddBean.isDatabaseImport)}"
                           columns="2"
                           styleClass="basic-form"
                           columnClasses="left-indent,right"
                           cellspacing="0"
                           cellpadding="0">
                  <h:outputLabel value="Bytes In File:" />
                  <h:outputText id="txtBytes"
                                value="#{importMapAddBean.bytesInFile}">
                    <f:convertNumber/>
                  </h:outputText>
                  
                  <h:outputText value="Estimated Rows In File:" />
                  <h:outputText id="txtRows"
                                value="#{importMapAddBean.estimatedRows}">
                    <f:convertNumber/>
                  </h:outputText>
              </h:panelGrid>            

              <h:panelGrid id="batchNumber"
                           rendered="#{importMapAddBean.displayBatch}"
                           columns="2"
                           styleClass="basic-form"
                           columnClasses="left-indent,right"
                           cellspacing="0"
                           cellpadding="0">
                <h:outputText value="Batch Number:" />
                <h:outputText id="txtBatchNo"
                              value="#{importMapAddBean.newBatch.batchId}">
                  <f:convertNumber/>
                </h:outputText>
              </h:panelGrid>
              </a4j:outputPanel>
             <a4j:region id="fileupload">
			 <h:panelGroup id="fileuploadPanel">
              <a4j:outputPanel id="progressPanel" style="width: 100%">
              <rich:progressBar id="progressBar"
                                rendered="true"
                                value="#{importMapAddBean.progress}"
                                styleClass="sts-progress-bar"
                                interval="2000"
                                mode="ajax"
                                label="Upload progress: #{importMapAddBean.progress} %"
                                enabled="#{importMapAddBean.displayProgress}"
                                minValue="0"
                                maxValue="99"
                                reRenderAfterComplete="closeActionId,addActionId,addDbActionId,cancelActionId,msg,progressPanel">
                		<f:facet name="complete">
                  			<h:outputText id="txtComplete" style="text-align: right; width: 50%; margin:auto" value="Upload Complete"/>
                		</f:facet>
              </rich:progressBar>
                           <rich:modalPanel id="panel"  zindex="2000" moveable="false" width="0"
										top="-30" height="0" left="-30" resizeable="false"/>
									<a4j:status id="a4j-status-progressbar" 
										onstart="javascript:Richfaces.showModalPanel('panel');"/>
									</a4j:outputPanel>
							</h:panelGroup>
						</a4j:region>
                        </div>

            <div id="table-four-bottom">
              <a4j:outputPanel id="a4j-commands" ajaxRendered="true">
              <ul class="right">         	
              	  <li class="ok2">
                  	<a4j:commandLink id="addDbActionId" rendered="#{importMapAddBean.displayDatabaseOk  and importMapAddBean.allowDatabaseImport}"
           						   	reRender="progressPanel" action="#{importMapAddBean.addDatabaseAction}" 
           						   	onclick="Richfaces.showModalPanel('loader');" 
								   	oncomplete="Richfaces.hideModalPanel('loader');" >     
                  	</a4j:commandLink>
                  </li>
 
                  <li class="verify">
                  	<a4j:commandLink id="btnConnVerify" rendered="#{importMapAddBean.displayOk and importMapAddBean.isDatabaseImport}"
									reRender="addForm:databaseSelector,addDbActionId,progressPanel" action="#{importMapAddBean.verifyImportConnection}" 
									onclick="Richfaces.showModalPanel('loader');" 
									oncomplete="Richfaces.hideModalPanel('loader');" >	
									</a4j:commandLink>
				  </li>
                  
                  <li class="ok2">
                  	<a4j:commandLink id="addActionId"
                                   	rendered="#{importMapAddBean.displayOk and (!importMapAddBean.isDatabaseImport)}"
                                   	reRender="progressPanel"
                                   	action="#{importMapAddBean.addAction}" 
                                   	onclick="this.style.visibility='hidden';"/>
                  </li>
                  <li class="close">
                  	<a4j:commandLink id="closeActionId"
                                   style="display: #{importMapAddBean.displayComplete ? '' : 'none'}"
                                   action="#{importMapAddBean.closeAction}"/>
                  </li>
                  <li class="cancel">
                  	<h:commandLink id="cancelActionId" immediate="true"
                                   style="display: #{importMapAddBean.displayComplete ? 'none': ''}"
                                   action="#{importMapAddBean.cancelAction}" />
                  </li>
              </ul>
              </a4j:outputPanel>
              
              <rich:modalPanel id="loader" zindex="2000" autosized="true">
         			<h:outputText value="Verifying..."/>
    		  </rich:modalPanel>
    
            </div>
          </div>
        </div>
      </a4j:form>
    </ui:define>
  </ui:composition>
</html>
