<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
 	<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
  		<ui:define name="script">
		   <script type="text/javascript">
		   //<![CDATA[
		   	
		   //]]>
		   </script>
  		</ui:define>

  		<ui:define name="body">
  			<h:form id="matrixDetailForm">
	  			<a4j:queue/>
	  			<div id="bottom">
					<h1><img id="imgTransactionProcess" alt="Transaction Process" src="../images/headers/hdr-allocate-transactions.png" width="250" height="19" /></h1>
				    
				    <div id="top">
				    	<div id="table-one">
				    		<div id="table-four-top">
								<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
							</div>
							
							<div id="table-one-content">
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
									<thead>
										<tr>
											<td colspan="5">Transaction Splitting Tool - Save as Matrix</td>
											<td colspan="4" style="text-align:right;">Transaction ID: ${currentSplittingToolBean[0].currentTransactionId}&#160;</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5">Driver Selection</th>
											<th colspan="41"></th>
										</tr>
									</tbody>
								</table>
								
								<h:panelGrid id="driverPanel" rendered="true" binding="#{currentSplittingToolBean[0].filterSelectionPanel}" styleClass="panel-ruler" />
								
					 
								<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
								<tbody>
								<tr>
									<th colspan="9">Details</th>
								</tr>
								<tr>
									<td style="width:20px;">&#160;</td>
									<td style="width:200px;">Effective Date:</td>
									<td style="width: 150px;">
										<rich:calendar binding="#{currentSplittingToolBean[0].effectiveDateCalendar}" 
												id="effDate" enableManualInput="true"
												required="true" label="Effective Date"
												oninputkeypress="return onlyDateValue(event);"
												value="#{currentSplittingToolBean[0].effectiveDate}" popup="true"
								
												disabled="false"
												converter="date" datePattern="M/d/yyyy"
												showApplyButton="false" inputClass="textbox"/>
									</td>
									<td style="width:23px;">&#160;</td>
									<td style="width:150px;">
										<h:outputText value="Future Split?:"/>
										<h:selectBooleanCheckbox id="futureSplitControl" styleClass="check" 
											disabled="false"
											value="#{currentSplittingToolBean[0].futureSplitBooleanFlag}" immediate="true">
											<a4j:support event="onclick" />
										</h:selectBooleanCheckbox>
									</td>
									<td style="width:600px;">&#160;</td>
								</tr>
								<tr>
									<td style="width:20px;">&#160;</td>
									<td style="width:200px;">Expiration Date:</td>
									<td>
										<rich:calendar binding="#{currentSplittingToolBean[0].expirationDateCalendar}" 
												id="expDate" enableManualInput="true"
												required="true" label="Expiration Date"
												oninputkeypress="return onlyDateValue(event);"
												value="#{currentSplittingToolBean[0].expirationDate}" popup="true"
												disabled="false"
				
												converter="date" datePattern="M/d/yyyy"
												showApplyButton="false" inputClass="textbox" />
									</td>
									<td style="width:23px;">&#160;</td>
									<td style="width:150px;">
										<h:outputText value="Active?:"/>
										<h:selectBooleanCheckbox id="activeControl" styleClass="check" 
											disabled="#{false}}"
											value="#{currentSplittingToolBean[0].activeBooleanFlag}" immediate="true">
											<a4j:support event="onclick"  />
										</h:selectBooleanCheckbox>
									</td>
									<td style="width:600px;">&#160;</td>
								</tr>
								
								<tr>
									<td style="width:20px;">&#160;</td>
									<td style="width:200px;">Comments:</td>
									<td colspan="7">
										<h:inputText id="matrixcomments" style="width:678px;" 
											disabled="false}"
											value="#{currentSplittingToolBean[0].matrixComments}"/>
									</td>
								</tr>
								
								<tr>
						        	<td colspan="9">&#160;</td>
								</tr>
								
								</tbody>
							</table>
								
								<table cellpadding="0" cellspacing="0" width="100%" class="ruler">
									<tr>
										<th colspan="7">Amounts</th>
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Distribution Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="glLineItmDistAmt" value="#{currentSplittingToolBean[0].currentTransaction.glLineItmDistAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Distribution Amountxx:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingGlLineItmDistAmt" value="#{currentSplittingToolBean[0].remainingGlLineItmDistAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>	
										<td class="column-spacer"  style="width:20%" >&#160;</td>									
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Freight Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceFreightAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceFreightAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Freight Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingInvoiceFreightAmt" value="#{currentSplittingToolBean[0].remainingInvoiceFreightAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer" style="width:20%" >&#160;</td>
									</tr>
									<tr>
										<td class="column-spacer">&#160;</td>
										<td class="column-label">Discount Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="invoiceDiscountAmt" value="#{currentSplittingToolBean[0].currentTransaction.invoiceDiscountAmt}" disabled="true">
												<f:convertNumber type="number" groupingUsed="false" minFractionDigits="2" maxFractionDigits="2"/>
											</h:inputText>
										</td>
										<td class="column-spacer">&#160;</td>
										<td class="column-label" style="width:180px" >Remaining Discount Amount:</td>
										<td class="column-input">
											<h:inputText style="text-align: right" id="remainingInvoiceDiscountAmt" value="#{currentSplittingToolBean[0].remainingInvoiceDiscountAmt}" disabled="true">
												<f:convertNumber type="number" minFractionDigits="2" maxFractionDigits="10"/>
											</h:inputText>
										</td>
										<td class="column-spacer" style="width:20%" >&#160;</td>
									</tr>
								</table>
														
<a4j:outputPanel id="forEachPanelWhen">
	<span>
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
	<tbody>		
		<tr>
			<th colspan="5">Allocations</th>
		</tr>
			
		<tr>
        	<td style="width:20px;">&#160;</td>    
	        <td><h:outputText style="width: 280px;" value="TaxCode" /></td>
	        <td><h:outputText style="width: 350px;" value="Description" /></td>
	        
	        <td><h:outputText style="width: 100px;" value="Allocation Percent" /></td>
			<td width="1%">&#160;</td> 
		</tr>
		  
		<c:forEach var="taxAllocMatrixDetail" items="#{currentSplittingToolBean[0].selectedMatrixTaxAllocDetails}" varStatus="loopCounter" >
        <tr>
        	<td style="width:20px;">&#160;</td>
        	
	        <td>  
	        	<a4j:outputPanel id="fieldpanelWhen_#{loopCounter.index}">   
	        		
					
					
					<h:inputText id="fieldTaxCode_#{loopCounter.index}" style="width: 280px;" value="#{taxAllocMatrixDetail.taxcodeCode}" disabled="true" />
				</a4j:outputPanel>
	        </td>
	        
	        <td>           	
				<a4j:outputPanel id="fieldDescription_#{loopCounter.index}">   
					<h:inputText id="fieldvalueDescription_#{loopCounter.index}" style="width: 450px;" value="#{not empty taxAllocMatrixDetail.description ? taxAllocMatrixDetail.description : cacheManager.taxCodeMap[taxAllocMatrixDetail.taxcodeCode].description}" disabled="true" />				 	
	        	</a4j:outputPanel>
	        </td>
	        
	        <td> 
	        	<a4j:outputPanel id="fielddistributionpanel_#{loopCounter.index}">   
		
					<h:inputText id="fielddistributionvalue_#{loopCounter.index}" style="text-align: right;" value="#{taxAllocMatrixDetail.allocationPercent}" 
						disabled="true" >
						
						<f:convertNumber minFractionDigits="2" maxFractionDigits="12"/>
					</h:inputText> 	
					
	        	</a4j:outputPanel>
	        </td>
   
 			<td width="50%">&#160;</td> 
        </tr>
    	</c:forEach>
    	
    	
		<tr>
        	<td colspan="15">&#160;</td>
		</tr>
	</tbody>
	</table>
	</span>
</a4j:outputPanel>								
								
							</div>
			
						
				    	</div>
				    	
				    	<div id="table-four-bottom">
						<h:outputText id="pageInfo" value="#{currentSplittingToolBean[0].transactionDataModel.pageDescription}"/>
						<ul class="right">
				      		<li class="oktaxability">
				        		<h:commandLink id="oktaxabilityBtn" disabled="#{!currentSplittingToolBean[0].displaySave}" action="#{currentSplittingToolBean[0].saveTaxabilityAction}" />
				      		</li>			      		
				      		<li class="oktaxjuris">
				        		<h:commandLink id="oktaxjurisBtn" disabled="#{!currentSplittingToolBean[0].displaySave}" action="#{currentSplittingToolBean[0].saveTaxabilityJurisdictionAction}" />
				      		</li>
				      		<li class="cancel">
				        		<h:commandLink id="cancelSaveBtn" action="#{currentSplittingToolBean[0].cancelSaveAction}" />
				      		</li>    		
				      	
				      	</ul>
				     </div>
				     
				    </div>
				</div>
				
			  
			</h:form>

   		</ui:define>
 	</ui:composition>
</html>
