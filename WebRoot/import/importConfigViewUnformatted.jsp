<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
  <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
    <ui:define name="script">
      <script type="text/javascript">
        //<![CDATA[
        //]]>
      </script>
    </ui:define>
    <ui:define name="body">
      <f:view>
        <h:form id="importConfigViewForm">
          <h1><h:graphicImage id="configurationBatches" alt="Configuration Batches" value="/images/headers/hdr-configuration_batches.png" width="250" height="19"/></h1>
          <div id="bottom">
            <div id="table-four">
              <div id="table-four-top">
                <a4j:outputPanel id="msg">
                  <h:messages errorClass="error" />
                </a4j:outputPanel>
                
                <rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
       			<a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{configImportBean.importConfigTextDataModel.pageDescription}"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
     				onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />  
     				
              </div>
              <div id="table-four-content">
                <ul class="basic-form">
                  <li class="heading">View a Import Config Batch - UnFormatted</li>
                  <li class="sub-heading-dark">Batch</li>
                </ul>
                <h:panelGrid id="batchPanel"
                             columns="8"
                             styleClass="basic-form"
                             columnClasses="left-indent,right,right,right,right,right,right,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="Batch ID:" />
                  <h:inputText size="20" id="txtBatchId"
                               value="#{configImportBean.selectedBatch.batchId}"
                               disabled="#{true}" />
                  <h:outputLabel value="Update Date:" />
                  <h:inputText size="20" id="txtUpdateDate"
                               value="#{configImportBean.selectedBatch.ts01}"
                               disabled="#{true}" />
                  <h:outputLabel value="Append:" />
                  <h:inputText size="20" id="append"
                               value="#{configImportBean.selectedBatch.vc03}"
                               disabled="#{true}" />
                  <h:outputLabel value="File Type:" />
                  <h:inputText size="20" id="txtFileType"
                               value="#{configImportBean.selectedBatch.vc01}"
                               disabled="#{true}" />
                </h:panelGrid>
                <h:panelGrid id="batchFile"
                             columns="2"
                             styleClass="basic-form"
                             columnClasses="left-indent,right"
                             cellspacing="0"
                             cellpadding="0"
                             style="width: 100%;">
                  <h:outputLabel value="File Name:" />
                  <h:inputText size="92" id="txtFileName"
                               value="#{configImportBean.selectedBatch.vc02}"
                               disabled="#{true}" />
                </h:panelGrid>
                <ul class="basic-form">
                  <li class="sub-heading-light">Import Config</li>
                </ul>
                <div class="scrollContainer">
                  <div class="scrollInner"
                       id="resize">
                    <rich:dataTable rowClasses="odd-row,even-row" id="batchTable"
                                    rows="#{configImportBean.importConfigTextDataModel.pageSize }"
                                    value="#{configImportBean.importConfigTextDataModel}"
                                    var="row">
	                    <rich:column>
                        <f:facet name="header">
                          <h:outputText value="Line"/>
                        </f:facet>
                        <h:outputText id="txtLine" value="#{row.line-1}" />
			                </rich:column>
	                    <rich:column>
                        <f:facet name="header">
                          <h:outputText value="Text"/>
                        </f:facet>
                        <h:outputText id="txtText" value="#{row.text}" />
			                </rich:column>
                    </rich:dataTable>
                  </div>
		              <rich:datascroller id="scroll" for="batchTable" maxPages="10" oncomplete="initScrollingTables();"
			                               align="center" stepControls="auto" ajaxSingle="false" 
                                     reRender="pageInfo"
			                               page="#{configImportBean.importConfigTextDataModel.curPage}" />
                </div>
              </div>
              <!--  content -->
                <div id="table-four-bottom">
                  <ul class="right">
                    <li class="formatted">
                      <h:commandLink id="viewufr"
                                     action="#{configImportBean.viewFormattedAction}" />
                    </li>
                    <li class="ok2">
                      <h:commandLink id="cancelAction"
                                     action="#{configImportBean.cancelAction}" />
                    </li>
                  </ul>
                </div>
                <!-- table-four-bottom -->
            </div>
            <!-- t4 -->
          </div>
        </h:form>
      </f:view>
    </ui:define>
  </ui:composition>
</html>
