<ui:component xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
      
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script type="text/javascript">
//<![CDATA[
    registerEvent(window, "load", function() { selectRowByIndex('aidFileManagerForm:specTable', 'selectedSpecRowIndex'); } );
    registerEvent(window, "load", function() { selectRowByIndex('aidFileManagerForm:fileTable', 'selectedFileRowIndex'); } );
    registerEvent(window, "load", adjustHeight);
               
//]]>
</script>

<style>
.rich-stglpanel-marker {float: left}
.rich-stglpanel-header {text-align: left}
</style>


</ui:define>
<ui:define name="body">
<f:view>
<h:inputHidden id="selectedSpecRowIndex" value="#{aidFileManagerBean.selectedSpecRowIndex}" />
<h:inputHidden id="selectedFileRowIndex" value="#{aidFileManagerBean.selectedFileRowIndex}" />
    
<h:form id="aidFileManagerForm">
     <h1><h:graphicImage id="imgAIDFileManagerMain" alt="AID&#160;File&#160;Manager" url="/images/headers/hdr-aid-file-manager.png" width="250" height="19" /></h1>
     
     <div class="tab"><h:graphicImage url="/images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
     	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF"
       				reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{aidFileManagerBean.togglePanelController.toggleHideSearchPanel}" >
       			<h:outputText value="#{(aidFileManagerBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
       	</a4j:commandLink>					
     </div> 
     
     <a4j:outputPanel id="showhidefilter">
     	<c:if test="#{!aidFileManagerBean.togglePanelController.isHideSearchPanel}">
	     	<div id="top">
	      	<div id="table-one">
				<a4j:outputPanel  id="contentToggle" ajaxRendered="true" layout="block">
					<a4j:include ajaxRendered="true" viewId="/WEB-INF/view/components/aid_specfile_panel.xhtml">
						<ui:param name="handlerBean" value="#{aidFileManagerBean}"/>
						<ui:param name="readonly" value="false"/>					
					</a4j:include>
				</a4j:outputPanel>
	      	</div>
	     	</div>
		</c:if>
	</a4j:outputPanel>

    <div class="wrapper">
		<span class="block-right">&#160;</span>
		<span class="block-left tab">
		<img src="../images/containers/STSSecurity-details-open.gif" />
		</span>
	</div>
    
	<!-- table-four-contentNew  -->     
	<div id="table-four-contentNew">
        
	<table cellpadding="0" cellspacing="0" style="width:auto;height:100%">
    <tbody>
	<tr>
	<td style="width: 600px;height:auto;vertical-align:top;">
 
    <div id="bottom" style="padding: 0 0 3px 0;">
    	<div id="table-four"  >
	    <div id="table-four-top" style="padding: 3px 0 0 10px;" ><h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Import Specs&#160;&#160;&#160;&#160;&#160;" />
	    	<rich:modalPanel id="requestProcessed" zindex="2000" moveable="false" width="0" top="-30" height="0" left="-30" resizeable="false" />
	        <a4j:status id="pageInfo"  
					startText="Request being processed..." 
					stopText="#{aidFileManagerBean.pageDescription }"
					onstart="javascript:Richfaces.showModalPanel('requestProcessed');"
	     			onstop="javascript:Richfaces.hideModalPanel('requestProcessed');" />
	    </div>
       
        <div class="scrollInnerNew" id="resize" >
       	<rich:dataTable rowClasses="odd-row,even-row" id="specTable"  value="#{aidFileManagerBean.importSpecList}" var="importSpec"  >
           	<a4j:support id="ref2tbl"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('aidFileManagerForm:specTable', this);"  actionListener="#{aidFileManagerBean.selectedSpecRowChanged}"
                        reRender="fileTable,downloadFile,viewFile,fileTable,fileTableDisplay,addFile,deleteFile,viewlog,reimport,fileDataTest,fileDisplayItems" />
                                  	
           	<rich:column id="importSpecType" style="width:180px;" sortBy="#{importSpec.importSpecType}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Import Spec Type"/>
				</f:facet>
				<h:outputText id="rowimportSpecType" value="#{importSpec.importSpecType}-#{cacheManager.listCodeMap['BATCHTYPE'][importSpec.importSpecType].description}" style="width:200px;text-align:center;"/>
			</rich:column>
           	
           	<rich:column id="importSpecCode" style="width:150px;" sortBy="#{importSpec.importSpecCode}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Import Spec Code"/>
				</f:facet>
				<h:outputText id="rowimportSpecCode" value="#{importSpec.importSpecCode}" style="width:200px;text-align:center;"/>
			</rich:column>
			
			<rich:column id="description" style="width:180px;" sortBy="#{importSpec.description}">
				<f:facet name="header">
					<h:outputText styleClass="headerText" value="Description"/>
				</f:facet>
				<h:outputText id="rowdescription" value="#{importSpec.description}" style="width:200px;text-align:center;"/>
			</rich:column>
           	
          </rich:dataTable>
          
          <a4j:outputPanel id="specDataTest">
          <c:if test="#{aidFileManagerBean.rowCount == 0}">
				<div class="nodatadisplay"><h:outputText value="Click Search to retrieve." /></div>
          </c:if>
          </a4j:outputPanel>
         
         </div>
         <!-- scroll-inner --> 
       <!-- end custGrid -->
       
       </div>
    </div>
	</td>
	
	<td style="width: 600px;height:auto;vertical-align:top;" >
  	<div id="bottom" style="padding: 0 0 3px 0;">
       <div id="table-four"  style="margin:0;" >
       <div id="table-four-top" style="padding: 3px 0 0 10px;"><h:outputText style="font-weight: bolder; color:#336699;font-size: 12px;" value="Display:&#160;&#160;&#160;" />
       		<h:selectOneMenu id="fileDisplayItems" style="width:130px;" value="#{aidFileManagerBean.selectedFileDisplay}" immediate="true" >
				<f:selectItems value="#{aidFileManagerBean.fileDisplayItems}"/>
				<a4j:support event="onchange" actionListener="#{aidFileManagerBean.fileDisplayChanged}" reRender="fileTable,downloadFile,viewFile,fileTableDisplay,addFile,deleteFile,viewlog,reimport"/>			
			</h:selectOneMenu>&#160;&#160;
			<h:outputText id="fileTableDisplay" style="font-size: 10px;" value="#{aidFileManagerBean.filPageDescription}" />
       </div>
       
        <div class="scrollInner"  id="resize1" >
         	<rich:dataTable rowClasses="odd-row,even-row" id="fileTable"  
         			value="#{aidFileManagerBean.fileItemList}" var="fileItem" >
           	<a4j:support id="ref2tblLocn"  event="onRowClick" status="rowstatus"
                        onsubmit="selectRow('aidFileManagerForm:fileTable', this);"  actionListener="#{aidFileManagerBean.selectedFileRowChanged}"
                        reRender="downloadFile,viewFile,fileTableDisplay,addFile,deleteFile,viewlog,reimport" />
             
            <rich:column id="selected" styleClass="column-center">        	
            	<f:facet name="header">
	            	<a4j:commandLink id="selected-a4j" value="Select"   styleClass="sort-#{aidFileManagerBean.sortOrder['selected']}"   actionListener="#{aidFileManagerBean.sortAction}"                       
	                	 oncomplete="initScrollingTables();" immediate="true" reRender="fileTable" />
	           	</f:facet>  	
            	<h:selectBooleanCheckbox styleClass="check" value="#{fileItem.selected}" disabled="#{false}" id="selectedFlagId" >
            		<a4j:support id="ajaxiCheck_selectedFlag" event="onclick" actionListener="#{aidFileManagerBean.selectTableCheckChange}" >
					</a4j:support>
            	</h:selectBooleanCheckbox>	
           	</rich:column>
        	
        	<rich:column id="fileName" styleClass="column-left">
	            <f:facet name="header">
	            	<a4j:commandLink id="fileName-a4j" value="File Name"   styleClass="sort-#{aidFileManagerBean.sortOrder['fileName']}"   actionListener="#{aidFileManagerBean.sortAction}"                       
	                	 oncomplete="initScrollingTables();" immediate="true" reRender="fileTable" />
	           	</f:facet>
	           	<h:outputText id="rowfileName" value="#{fileItem.fileName}" style="width:200px;text-align:left;"/>
        	</rich:column>
        	
        	<rich:column id="fileDate" styleClass="column-left">
	            <f:facet name="header">
	            	<a4j:commandLink id="fileDate-a4j" value="Date Modified"   styleClass="sort-#{aidFileManagerBean.sortOrder['fileDate']}"   actionListener="#{aidFileManagerBean.sortAction}"                       
	                	 oncomplete="initScrollingTables();" immediate="true" reRender="fileTable" />
	           	</f:facet>
	           	<h:outputText id="rowfileDate" value="#{fileItem.fileDate}" style="width:200px;text-align:left;">
	           		<f:converter converterId="dateTime"/>
				</h:outputText>
        	</rich:column>

			<rich:column id="fileSize" styleClass="column-left">
	            <f:facet name="header">
	            	<a4j:commandLink id="fileSize-a4j" value="Size"   styleClass="sort-#{aidFileManagerBean.sortOrder['fileSize']}"   actionListener="#{aidFileManagerBean.sortAction}"                       
	                	 oncomplete="initScrollingTables();" immediate="true" reRender="fileTable" />
	           	</f:facet>
	           	<h:outputText id="rowfileSize" value="#{fileItem.fileSize}&#160;KB" style="width:200px;text-align:left;"/>
        	</rich:column>
			 
          	</rich:dataTable>

          <a4j:outputPanel id="fileDataTest">
          		<c:if test="#{aidFileManagerBean.selectedSpecRowIndex == -1}">
					<div class="nodatadisplay"><h:outputText value="No Import Spec Selected." /></div>
          		</c:if>
           </a4j:outputPanel>
		</div>
                
       	<div id="table-four-bottom">
        <ul class="right">
        	<li class="download"><h:commandLink id="downloadFile" style="#{(aidFileManagerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!aidFileManagerBean.displayDownloadButtons or aidFileManagerBean.currentUser.viewOnlyBooleanFlag}"  immediate="true" action="#{aidFileManagerBean.displayDownloadAction}" /></li>   
        	<li class="view115"><h:commandLink id="viewFile" disabled="#{!aidFileManagerBean.displayViewButtons}" immediate="true" action="#{aidFileManagerBean.displayViewAction}" /></li>
        	<li class="add"><h:commandLink id="addFile" style="#{(aidFileManagerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!aidFileManagerBean.displayAddButtons or aidFileManagerBean.currentUser.viewOnlyBooleanFlag}"  immediate="true" action="#{aidFileManagerBean.displayAddAction}" /></li>
        	<li class="delete115"><a4j:commandLink id="deleteFile" style="#{!aidFileManagerBean.displayDeleteButtons? 'display:none;':'display:block;' or (aidFileManagerBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" immediate="true"  
        		oncomplete="javascript:Richfaces.showModalPanel('executeDelete');" reRender="executeDeleteForm" /></li>
        	<li class="viewlog"><h:commandLink id="viewlog" disabled="#{!aidFileManagerBean.displayLogButtons}"  immediate="true" action="#{aidFileManagerBean.displayLogAction}" /></li>     	
        	<li class="reimport"><a4j:commandLink id="reimport" style="#{!aidFileManagerBean.displayReimportButtons? 'display:none;':'display:block;' or aidFileManagerBean.currentUser.viewOnlyBooleanFlag ? 'display:none;':'display:block;'}"  immediate="true" 
        	     oncomplete="javascript:Richfaces.showModalPanel('executeReimport');" reRender="executeReimportForm" /></li>
        </ul>
       	</div> 
		
      	</div>  
    </div>   
	</td>

	
	
	</tr>
	</tbody>
	</table>

	</div>
    <!-- end of table-four-contentNew  -->  
     
</h:form>
</f:view>
   
<rich:modalPanel id="executeDelete" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Delete" />
	</f:facet>
	<h:form id="executeDeleteForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('executeDelete');" action="#{aidFileManagerBean.displayDeleteAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('executeDelete'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>

<rich:modalPanel id="executeReimport" zindex="2000" autosized="true">
	<f:facet name="header">
		<h:outputText value="Confirm Reimport" />
	</f:facet>
	<h:form id="executeReimportForm">
		<h:panelGrid columns="1" columnClasses="column-left" headerClass="column-left" cellpadding="2" cellspacing="2">
			<f:facet name="header">
				<h:outputText value="Are you sure?"/>
			</f:facet>
			
			<h:panelGroup style="text-align:center;">
				<h:commandButton id="okBtn" value="Ok"  style="cursor:pointer" onclick="Richfaces.hideModalPanel('executeReimport');" action="#{aidFileManagerBean.displayReimportAction}" />
				<rich:spacer width="20px"/>
				<h:commandButton id="cancelBtn" style="cursor:pointer" value="Cancel" onclick="Richfaces.hideModalPanel('executeReimport'); return false;" />
			</h:panelGroup>
			
		</h:panelGrid>
	</h:form>
</rich:modalPanel>
   
</ui:define>
</ui:composition>
</ui:component>
