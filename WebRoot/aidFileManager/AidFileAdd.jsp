<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
	<script type="text/javascript"></script>
</ui:define>
<ui:define name="body">
<h:form enctype="multipart/form-data" id="form">
<h1><h:graphicImage id="imgAIDFileAdd" alt="AID&#160;File&#160;Manager" url="/images/headers/hdr-aid-file-manager.png" width="250" height="19" /></h1>
            
<div id="bottom">
<div id="table-four">
<div id="table-four-top">
<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel></div>
<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
    	<thead>
        	<tr><td colspan="7"><h:outputText value="#{aidFileManagerBean.actionText}"/></td></tr>
         </thead>
         <tbody>
         	<tr><th colspan="7">&#160;</th></tr>
            <tr>
				<td style="width: 50px;">&#160;</td>
	          	<td style="width: 100px;">Selected&#160;File&#160;to&#160;Upload:</td>
	          	<td colspan="3" style="width: 650px;vertical-align:center" >
					<t:inputFileUpload id="uploadFile"  binding="#{aidFileManagerBean.uploadFile}" size="70" style="width: 650px;height: 22px;vertical-align:top" immediate="true">                 		 
	                </t:inputFileUpload>&#160;&#160;&#160;
				</td>	
                <td>&#160;</td>			
	          	<td>&#160;</td>
			</tr>
					
			<tr><td colspan="7">&#160;</td></tr>					
			<tr>
				<td style="width: 50px;">&#160;</td>
	          	<td style="width: 100px;">&#160;</td>
				<td style="width:300px;">File&#160;uploaded:<h:outputLabel style="width: 300px; color:red; font-weight:bold; align:left;" 
					value="#{aidFileManagerBean.uploadedFileName}" /></td>	
	          	<td>&#160;</td>
	          	<td>&#160;</td>
	 			<td>&#160;</td>
	 			<td>&#160;</td>
			</tr>
			
			<tr>
				<td style="width: 50px;">&#160;</td>
	          	<td style="width: 100px;">&#160;</td>
				<td style="width:300px;">Size:<h:outputLabel style="width: 300px; color:red; font-weight:bold; align:left;" 
					value="#{aidFileManagerBean.uploadedFileSize}" /></td>	
	          	<td>&#160;</td>
	          	<td>&#160;</td>
	          	<td>&#160;</td>
				<td>&#160;</td>
			</tr>
                 
            <tr><th colspan="7">&#160;</th></tr>              
       	</tbody>
    </table>
</div>
            
<rich:modalPanel id="uploading" zindex="2000" autosized="true">
	<h:outputText value="File is now uploading..."/>
</rich:modalPanel>
      
<div id="table-four-bottom">
	<ul class="right">
		<li class="upload">
        	  	<h:commandLink id="btnUpload" action="#{aidFileManagerBean.fileUploadAction}" onclick="javascript:Richfaces.showModalPanel('uploading');" />
		</li>
		<li class="close"><h:commandLink id="closelBtn" action="#{aidFileManagerBean.closeAction}" ></h:commandLink></li>
  </ul>
</div>
            
</div> <!-- t4 -->
</div> <!-- top -->
</h:form>
</ui:define>
</ui:composition>
</html>
