<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="script">
	<script type="text/javascript">
	//<![CDATA[
	//]]>
	</script>
</ui:define>

<ui:define name="body">
<h1><h:graphicImage id="imgAIDFileManagerMain" alt="AID&#160;File&#160;Manager" url="/images/headers/hdr-aid-file-manager.png" width="250" height="19" /></h1>

<h:form id="custLocnUpdateForm">
<div id="bottom">
	<div id="table-four">
	<div id="table-four-top"><h:messages errorClass="error" /></div>
	<div id="table-one-content" >
	<div class="scrollContainer">
	<div class="scrollInner" style="height:580px;width:100%;">
		<table cellpadding="0" cellspacing="0" id="rollover" class="ruler">
			<thead>
				<tr><td colspan="3" style="width:100%;"><h:outputText value="#{aidFileManagerBean.actionText}"/></td></tr>
			</thead>
			<tbody>			
				<tr><th colspan="3" style="width:100%;"><h:outputText value="#{aidFileManagerBean.filePrefix}&#160;&#160;#{aidFileManagerBean.fileNameText}"/></th></tr>  
				<tr>
					<td>&#160;</td>
					<td style="width:100%;vertical-align:text-top;">
						<p><font size="2">
							<pre>
								<h:outputText value="#{aidFileManagerBean.fileContent}" id="fileContent"/>
							</pre>
						</font></p>
					</td>		
					<td>&#160;</td>
				</tr>
			</tbody>
		</table>
	</div>
	</div>
	</div>
	</div>
	
	<div id="table-four-bottom">
		<ul class="right">
			<li class="ok"><h:commandLink id="btnOk" value="" action="#{aidFileManagerBean.okViewAction}" /></li>
		</ul>
	</div>	
</div>	

</h:form>
</ui:define>
</ui:composition>
</html>