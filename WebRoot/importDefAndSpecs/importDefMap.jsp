<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="body">
<h1>
  <h:graphicImage id="imgImportDefsSpecs" alt="Import Defs and Specs" value="/images/headers/hdr-import-defs-and-specs.gif" />
</h1>
<div id="bottom">
<h:form id="form">
	<div id="table-four">
    	<div id="table-four-top">
        	<a4j:outputPanel id="msg"><h:messages globalOnly="true" errorClass="error" /></a4j:outputPanel>
      	</div>
      	<div id="table-bare-content">
        	<ul class="basic-form">
        		<c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'FD'}">
          			<li class="heading">Map Flat Delimited Import File</li>
          		</c:if>
          		<c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'DB'}">
          			<li class="heading">Map a Database Table Import File</li>
          		</c:if>
          		<li class="sub-heading-light">Import Definition</li>
        	</ul>
        	<h:panelGrid id="importDefinitionCodePanelGrid" columns="3"
                     styleClass="basic-form"
                     columnClasses="left-indent,right,right"
                     cellspacing="0"
                     cellpadding="0">
                     
	        	<h:outputLabel value="Definition Type:" />
	          	<h:inputText size="80" id="txtImportDefinitionType"
	                       value="#{cacheManager.listCodeMap['IMPDEFTYPE'][importDefinitionMappingBean.selectedImportDef.importDefinitionType].description}"
	                       disabled="true" />
	       		<h:outputLabel value="&#160;"/>
	       
	         	<h:outputLabel value="Definition Code:" />
	          	<h:inputText size="80" id="txtImportDefinition"
	                       value="#{importDefinitionMappingBean.selectedImportDef.importDefinitionCode}"
	                       disabled="true" />
	          	<h:outputLabel value="&#160;"/>
	          
	          	<h:outputLabel value="Description:" />
	          	<h:inputText size="160" id="txtImportDefDesc"
	                       value="#{importDefinitionMappingBean.selectedImportDef.description}"
	                       disabled="true" />
	          	<h:outputLabel value="&#160;"/>
	            
	            <!-- test for 'FD' --> 
	            <c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'FD'}">                       
	          	<h:outputLabel value="Delimiter Character:" />
	            <h:panelGroup>
	          		<h:inputText id="DelimChar"
	                       size="2"
	                       label="Delimiter Character"
	                       disabled="true"
	                       value="#{importDefinitionMappingBean.selectedImportDef.flatDelChar}" />
	          
	            	<rich:spacer width="50px" />
	            	<h:outputLabel value="Text Qualifier:" />
	            	<rich:spacer width="10px" />
	            	<h:inputText id="TextQual"
	                         size="1"
	                         label="Text Qualifier"
	                         disabled="true"
	                         value="#{importDefinitionMappingBean.selectedImportDef.flatDelTextQual}" />
	            	<rich:spacer width="50px" />
	            	<h:outputLabel value="First Line Headers?:" />
	            	<rich:spacer width="10px" />
	            	<h:selectBooleanCheckbox styleClass="check"
	                                     disabled="true" id="chkLine1HeadFlag"
	                                     value="#{importDefinitionMappingBean.selectedImportDef.flatDelLine1HeadFlagUI}" />
	          	</h:panelGroup>
	          	</c:if>
	          	<!-- end of test -->
	          	
	          	<!-- test for 'DB' --> 
	            <c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'DB'}">                       
	          	<h:outputLabel value="Connection:" />
	            <h:panelGroup >
	          		<h:inputText id="Connection"
	                       size="80"
	                       label="Delimiter Character"
	                       disabled="true"
	                       value="#{importDefinitionMappingBean.selectedImportDef.importConnectionCode}" />
	          
	            	<h:outputLabel value="&#160;"/>
	            	
	            	<h:outputLabel value="&#160;"/>
	            	
	            	<rich:spacer width="10px" />
	          	</h:panelGroup>
	          	<h:outputLabel value="&#160;"/>
   	
	          	<h:outputLabel value="Table Name:" />
	            <h:inputText id="tableName"
	                         size="80"
	                         label="Table Name"
	                         disabled="true"
	                         value="#{importDefinitionMappingBean.selectedImportDef.dbTableName}" />
	          	</c:if>
	          	<!-- end of test -->     
          
        	</h:panelGrid>
        
        	<c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'FD'}">
        	<h:panelGrid id="sampleFileNamePanelGrid" columns="2"
                     styleClass="basic-form"
                     columnClasses="left-indent,right"
                     cellspacing="0"
                     cellpadding="0">
                     
          		<h:outputLabel value="Sample File Name:" />
          		<h:panelGroup>
            		<h:inputText id="SampleFileName"
                        label="Sample File Name"
                        disabled="true"
                        value="#{importDefinitionMappingBean.selectedImportDef.sampleFileName}"
                        size="160" />
            		<h:message style="padding-left: 4px"
                       	styleClass="error"
                       	for="SampleFileName" />
          		</h:panelGroup>  
        	</h:panelGrid>
        	</c:if>
        	
        	<h:outputLabel value="&#160;"/>


        	<ul class="basic-form">
          		<li class="sub-heading-light">
                	<h:outputText id="currentActionLabel" value="#{importDefinitionMappingBean.currentMenu}"/>
          		</li>
        	</ul>
        	
        	<a4j:outputPanel id="columnMappings" 
                         style="display: #{importDefinitionMappingBean.showMappings ? '' : 'none'}">
            	<table style="width: 100%; text-align: left">
              		<tr>
                		<td colspan="3" style="color: #666">Columns in grey have been assigned</td>
                		<td colspan="2" style="color: red">Columns in red are not in Import File Header</td>
              		</tr>
              		<tr>
                		<td style="width: 19%">
                  			<div class="scrollContainer">
                    		<div id="selectImportColScroller" class="scrollInner" style="height: 330px" >
                      		<rich:dataTable rowClasses="odd-row,even-row" id="selectImportCol"
                                      styleClass="dataTableListBoxLined"
                                      style="width: 100%; padding-top: 0;"
                                      value="#{importDefinitionMappingBean.importColumns}"
                                      var="row">
                        		<a4j:support id="a4j-selectMapping"
                                     actionListener="#{importDefinitionMappingBean.importColumnChanged}"
                                     onsubmit="selectRow('form:selectImportCol', this);"
                                     event="onRowClick" />
                       			 <rich:column breakBefore="true" id="fileImportColumn">
                          			<f:facet name="header">
                            			<h:outputText value="Import File Column" />
                          			</f:facet>
                          			<h:outputText style="#{row.isAssigned ? 'color: #b8b8b8' : ''}" value="#{row.name}" />
                        		</rich:column>
                      		</rich:dataTable>
                    		</div>
                  			</div>
                		</td>
                		<td style="width: 21%">
                  			<div class="scrollContainer">
                    		<div id="selectTransColScroller" class="scrollInner"  style="height: 330px">
                      		<rich:dataTable rowClasses="odd-row,even-row" id="selectTransCol"
                                      styleClass="dataTableListBoxLined"
                                      style="width: 100%; padding-top: 0; height:100% !important"
                                      value="#{importDefinitionMappingBean.transactionColumns}"
                                      var="row">
                        		<a4j:support id="a4j-selectMapping"
                                     actionListener="#{importDefinitionMappingBean.transColumnChanged}"
                                     onsubmit="selectRow('form:selectTransCol', this);"
                                     event="onRowClick" />
                        		<rich:column breakBefore="true" id="transactionColumn">
                          			<f:facet name="header">
                            			<h:outputText value="Transaction Column" />
                          			</f:facet>
                          			<h:outputText value="#{row}" />
                        		</rich:column>
                      		</rich:dataTable>
                    		</div>
                  			</div>
                		</td>
                		
                		<td>
                  			<a4j:outputPanel id="assignOutputPanel" ajaxRendered="true">
                    		<ul class="block-list" style="margin-top: 10em; margin: auto;">
                      			<li>
                        			<a4j:commandButton id="assignAction"
                                           image="/images/actionbuttons/NCSTSAssign.JPG"
                                           disabled="#{importDefinitionMappingBean.selectedImportIndex == -1 or importDefinitionMappingBean.selectedTransactionIndex == -1}"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="mapping, selectTransCol, selectImportCol"
                                           action="#{importDefinitionMappingBean.assignAction}" />
                      				</li>
                      			<li>
                        			<a4j:commandButton id="assignAllAction"
                                           image="/images/actionbuttons/NCSTSAssignAll.jpg"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="mapping, selectTransCol, selectImportCol, save"
                                           action="#{importDefinitionMappingBean.assignAllAction}" />
                      			</li>
                      			<li>
                        			<a4j:commandButton id="removeAction"
                                           image="/images/actionbuttons/NCSTSRemove.JPG"
                                           disabled="#{importDefinitionMappingBean.selectedMappingIndex == -1}"
                                           type="button"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="mapping, selectTransCol, selectImportCol"
                                           action="#{importDefinitionMappingBean.removeAction}" />
                      			</li>
                      			<li>
                        			<a4j:commandButton id="removeAllAction"
                                           image="/images/actionbuttons/NCSTSRemoveAll.JPG"
                                           type="button"
                                           onclick="if (! confirm('Are you sure you want to Remove All?')) {return false}"
                                           style="border-style:solid; border-width: 0px;border-color: SlateGrey"
                                           reRender="mapping, selectTransCol, selectImportCol"
                                           action="#{importDefinitionMappingBean.removeAllAction}" />
                      			</li>
                    		</ul>
                  			</a4j:outputPanel>
                		</td>
                		
                		<td>
                  			<h1 style="margin: 0; font-size: 120%; font-weight: bold; line-height: 18pt; text-align: center">Mapped Columns</h1>
                  			<div class="scrollContainer">
                    		<div id="mappingScroller" class="scrollInner" style="height: 315px" >
                      			<rich:dataTable rowClasses="odd-row,even-row" id="mapping"
                                      styleClass="dataTableListBoxLined"
                                      sortMode="single"
                                      style="width: 100%"
                                      value="#{importDefinitionMappingBean.mappings}"
                                      var="row">
                        			<a4j:support id="a4j-selectMapping"
                                     actionListener="#{importDefinitionMappingBean.mappingColumnChanged}"
                                     onsubmit="selectRow('form:mapping', this);"
                                     reRender="removeAction,update"
                                     event="onRowClick" />
                        <rich:column breakBefore="true"
                                     id="importColumnName">
                          <f:facet name="header">
                            <a4j:commandLink id="a4j-mic"
                                             styleClass="sort-#{importDefinitionMappingBean.sortOrder['importColumnName']}"
                                             value="Import File Column"
                                             actionListener="#{importDefinitionMappingBean.sortAction}"
                                             oncomplete="initScrollingTables();"
                                             immediate="true"
                                             reRender="mapping" />
                          </f:facet>
                          <h:outputText style="#{row.existsInImport ? '' : 'color: red'}"
                                        value="#{row.importColumnName}" />
                        </rich:column>
                        <rich:column id="transDtlColumnName">
                          <f:facet name="header">
                            <a4j:commandLink id="a4j-tdc"
                                             styleClass="sort-#{importDefinitionMappingBean.sortOrder['transDtlColumnName']}"
                                             value="Transaction Column"
                                             actionListener="#{importDefinitionMappingBean.sortAction}"
                                             oncomplete="initScrollingTables();"
                                             immediate="true"
                                             reRender="mapping" />
                          </f:facet>
                          <h:outputText value="#{row.transDtlColumnName}" />
                        </rich:column>
                        <rich:column id="mappingSelect"
                                     width="10%">
                          <f:facet name="header">
                            <h:outputText id="id4x"
                                          value="Select Clause" />
                          </f:facet>
                          <a4j:outputPanel id="selectEllipsisOutputPanel" ajaxRendered="true">
                            <h:outputText value="#{row.selectEllipsis}" />
                          </a4j:outputPanel>
                        </rich:column>
                      </rich:dataTable>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </a4j:outputPanel>
          
          <div class="scrollInner" id="resize">
          	<a4j:outputPanel id="viewFormatted" style="display: #{importDefinitionMappingBean.showFormatted ? '' : 'none'}">
            	<rich:dataTable rowClasses="odd-row,even-row" id="formatted"
                          value="#{importDefinitionMappingBean.formattedData}" var="def">
              		<rich:columns value="#{importDefinitionMappingBean.formattedColumns}" var="col" index="row">
                		<f:facet name="header">
                  			<h:outputText value="#{col}"/>
                		</f:facet>
                		<h:outputText value="#{def[row]}"/>
              		</rich:columns>
            	</rich:dataTable>
          	</a4j:outputPanel>
          
          	<a4j:outputPanel id="viewUnformatted" style="display: #{importDefinitionMappingBean.showUnformatted ? '' : 'none'}">
            	<rich:dataTable rowClasses="odd-row,even-row" id="unformatted"
                                columns="2"
                                headerClass="column-left"
                                columnClasses="column-right, column-monospace"
                                value="#{importDefinitionMappingBean.unformattedData}"
                                var="row">
              		<rich:column>
                		<f:facet name="header">
                  			<h:outputText value="Line" />
                		</f:facet>
                		<h:outputText value="#{row.line-1}" />
              		</rich:column>
              		<rich:column>
                		<f:facet name="header">
                  			<h:outputText value="Text" />
                		</f:facet>
                  		<h:outputText value="#{row.text}" />
                	</rich:column>
              	</rich:dataTable>
       		</a4j:outputPanel>
            
            
            <h:panelGrid id="sampleFileNamePanelGridUpdate" columns="2"
                       styleClass="basic-form"
                       columnClasses="left-indent,right"
                       cellspacing="0"
                       cellpadding="0">

			<h:outputLabel value="&#160;"/>
			<h:outputLabel value="&#160;"/>

            <h:outputLabel value="Last Update User ID:"/>
            <h:inputText id="LastUpdateUserID"
                                 size="45"
                                 disabled="true"
                                 value="#{importDefinitionMappingBean.selectedImportDef.updateUserId}" />

          	<h:outputLabel value="Last Update Timestamp:"/>
          	<h:inputText id="LastUpdateTimestamp"
                       size="45"
                       disabled="true"
                       value="#{importDefinitionMappingBean.selectedImportDef.updateTimestamp}">
            	<f:converter converterId="dateTime"/>
          	</h:inputText>
        </h:panelGrid>
          
          
          </div>
      </div> <!-- t4-bare-content -->
      <div id="table-four-bottom">
         
		<a4j:outputPanel id="tablefourbottomOutputPanel" ajaxRendered="true">
          	<ul class="right">
            	<li class="ok2">
                	<a4j:commandLink id="save"
                               style="display: #{importDefinitionMappingBean.showSaveButton ? '' : 'none' }"
                               action="#{importDefinitionMappingBean.saveAction}" />
            	</li>
            	<li class="update">
                	<a4j:commandLink id="update"
                                 style="display: #{importDefinitionMappingBean.showUpdateButton ? '' : 'none'}"
                                 onclick="javascript:Richfaces.showModalPanel('selectPanel');" />
            	</li>
            	<li class="map">
                	<a4j:commandLink id="mapAction"
                                 style="display: #{importDefinitionMappingBean.showMappingsButton ? '' : 'none' }"
                                 action="#{importDefinitionMappingBean.showMappingsAction}" 
                                 reRender="viewFormatted, columnMappings, viewUnformatted, mapAction, formattedAction, unformattedAction, cancelAction, currentActionLabel"/>
            	</li>
            	<li class="formatted">
                	<a4j:commandLink id="formattedAction"
                                 style="display: #{importDefinitionMappingBean.showFormattedButton ? '' : 'none' }"
                                 action="#{importDefinitionMappingBean.showFormattedAction}"
                                 reRender="viewFormatted, columnMappings, viewUnformatted, mapAction, formattedAction, unformattedAction, cancelAction, currentActionLabel"/>
            	</li>
            	<c:if test="#{importDefinitionMappingBean.selectedImportDef.importFileTypeCode == 'FD'}">
            	<li class="unformatted">
                	<a4j:commandLink id="unformattedAction"
                                 style="display: #{importDefinitionMappingBean.showUnformattedButton ? '' : 'none' }"
                                 action="#{importDefinitionMappingBean.showUnformattedAction}" 
                                 reRender="viewFormatted, columnMappings, viewUnformatted, mapAction, formattedAction, unformattedAction, cancelAction, currentActionLabel"/>
            	</li>
            	</c:if>
            	<li class="cancel">
                	<a4j:commandLink id="cancelAction"
                                 immediate="true"
                                 style="display: #{importDefinitionMappingBean.showCancelButton ? '' : 'none' }"
                                 action="import_specs_main"/>
            	</li>
          	</ul>
        </a4j:outputPanel>
        </div> <!-- t4-bottom -->
    </div> <!-- t4 -->
</h:form>
<rich:modalPanel id="selectPanel"
                 autosized="true">
  <f:facet name="header">
    <h:panelGroup>
      <h:outputText value="Update Mapping Detail"></h:outputText>
    </h:panelGroup>
  </f:facet>
  <h:form id="modalSelectForm">
    <a4j:outputPanel id="updateMappingOutputPanel" ajaxRendered="true">
      <h:panelGrid id="updateMapping"
                   class="basic-form"
                   columnClasses="left, right"
                   columns="2">
        <h:outputLabel value="Import Column" />
        <h:inputText id="mappingImportColumn"
                     disabled="true"
                     value="#{importDefinitionMappingBean.mappings[importDefinitionMappingBean.selectedMappingIndex].importColumnName}" />
        <h:outputLabel value="Trans. Detail Column" />
        <h:inputText id="mappingTransDtlColumn"
                     disabled="true"
                     value="#{importDefinitionMappingBean.mappings[importDefinitionMappingBean.selectedMappingIndex].transDtlColumnName}" />
        <h:outputLabel value="Select clause" />
        <h:inputTextarea id="mappingSelectClause"
                         rows="20"
                         cols="60"
                         value="#{importDefinitionMappingBean.mappings[importDefinitionMappingBean.selectedMappingIndex].selectClause}" />
      </h:panelGrid>
    </a4j:outputPanel>
    <a4j:commandButton id="mappingOK"
                       oncomplete="#{rich:component('selectPanel')}.hide();"
                       action="#{importDefinitionMappingBean.defineSelectAction}"
                       value="OK" />
    <rich:spacer width="20px" />
    <a4j:commandButton id="mappingCancel"
                       immediate="true"
                       bypassUpdates="true"
                       onclick="Richfaces.hideModalPanel('selectPanel');"
                       value="Cancel" />
</h:form>
</rich:modalPanel>
</div> <!-- bottom -->
</ui:define>
</ui:composition>
</html>
