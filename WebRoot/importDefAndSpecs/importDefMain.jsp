<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
	<script type="text/javascript">
        //<![CDATA[ 
        registerEvent(window, "load", function() { selectRowByIndex('selectTypeForm:importDefTable', 'importDefTableRowIndex'); } );
        registerEvent(window, "load", function() { selectRowByIndex('selectTypeForm:importSpecTable', 'importSpecTableRowIndex'); } );
        registerEvent(window, "load", function() { selectRowByIndex('importDefAndSpecsBean.selectedDefUsingConnIndex', 'importDefUsingConnIndex'); } );
        registerEvent(window, "load", function() { selectRowByIndex('importDefAndSpecsBean.selectedSpecUsingDefIndex', 'importSpecUsingDefIndex'); } );
       //]]>
    </script>
</ui:define>
    
<ui:define name="body">
<h:inputHidden id="importDefTableRowIndex" value="#{importDefAndSpecsBean.selectedDefIndex}" />
<h:inputHidden id="importSpecTableRowIndex" value="#{importDefAndSpecsBean.selectedSpecIndex}" />
<h:inputHidden id="importDefUsingConnIndex" value="#{importDefAndSpecsBean.selectedDefUsingConnIndex}" />
<h:inputHidden id="importSpecUsingDefIndex" value="#{importDefAndSpecsBean.selectedSpecUsingDefIndex}" />
      
<h:form id="selectTypeForm">
	<h1><h:graphicImage id="imgImportDefsSpecs" alt="Import Defs and Specs" value="/images/headers/hdr-import-defs-and-specs.gif" /></h1>
  	<div class="tab">
	    <img src="../images/containers/STSSelection-filter-open.gif" />&#160;&#160;&#160;
	   	<a4j:commandLink id="toggleHideSearchPanel" style="height:22px;text-decoration: underline;color:#0033FF" 
			reRender="showhidefilter,toggleHideSearchPanel" actionListener="#{importDefAndSpecsBean.togglePanelController.toggleHideSearchPanel}" >
			<h:outputText value="#{(importDefAndSpecsBean.togglePanelController.isHideSearchPanel) ? 'show' : 'hide'}" />
		</a4j:commandLink>					
    </div> 
    <a4j:queue/>
     
    <a4j:outputPanel id="showhidefilter">
		<c:if test="#{!importDefAndSpecsBean.togglePanelController.isHideSearchPanel}">    
		    <div id="top">
			<div id="table-one">
			<div id="table-one-top" style="padding-left: 10px;">
				<table cellpadding="0" cellspacing="0" style="width: 100%;">
					<tr>					
						<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnCollapse" style="text-decoration: underline;color:#0033FF;"
						 	reRender="showhidefilter" actionListener="#{importDefAndSpecsBean.togglePanelController.collapseTogglePanels}" ><h:outputText value="collapse all"/></a4j:commandLink></td>
						<td style="padding-left: 10px;" ><a4j:commandLink id="toggleCustLocnExpand" style="text-decoration: underline;color:#0033FF;"  
							reRender="showhidefilter" actionListener="#{importDefAndSpecsBean.togglePanelController.expandTogglePanels}" ><h:outputText value="expand all"/></a4j:commandLink></td>
						<td style="width: 80%"></td>
						<td style="padding-right: 10px;" ><h:outputText style="color:white;" value="Wildcard&#160;=&#160;%" /></td>
					</tr>
				</table>
			</div>
    
          	<div id="table-one-content" style="height:auto;" >
          		<div id="embedded-table">
				<rich:simpleTogglePanel id="selectConfigComponent"   switchType="ajax" label="Select" 
					actionListener="#{importDefAndSpecsBean.togglePanelController.togglePanelChanged}" 
					opened="#{importDefAndSpecsBean.togglePanelController.togglePanels['selectConfigComponent']}"  
					bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
					<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
					<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
					
					<table width="100%" cellpadding="0"  cellspacing="0" border="2" >
						<tbody>
							<tr>
								<td class="column-label7">
							            <h:outputLabel id="SelImportScreenTypeLabel" for="SelImportScreenType" value="Config&#160;Component: " /> 
								</td>
								<td class="column-input2">	
									<h:selectOneMenu id="SelImportScreenType"
								                               value="#{importDefAndSpecsBean.screenType}"
								                               valueChangeListener="#{importDefAndSpecsBean.onScreenTypeChange}"
								                               onchange="submit();">
								    	<f:selectItems value="#{importDefAndSpecsBean.screenTypeMenuItems}" />												  
								    </h:selectOneMenu>
								 </td>
								 <td  width="50%">&#160;</td>
							 </tr>
						</tbody>
				    </table>
				</rich:simpleTogglePanel>				
				</div>
				
				<div id="embedded-table">
				<rich:simpleTogglePanel id="defSpecInformation"   switchType="ajax" label="Definition/Spec Information" 
					actionListener="#{importDefAndSpecsBean.togglePanelController.togglePanelChanged}" 
					opened="#{importDefAndSpecsBean.togglePanelController.togglePanels['defSpecInformation']}"  
					bodyClass="togglepanel-body" headerClass="togglepanel-header" styleClass="togglepanel" >
					<f:facet name="openMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-e.png"></h:graphicImage></f:facet>
					<f:facet name="closeMarker"><h:graphicImage style="vertical-align:middle;" url="/images/triangle-s.png"></h:graphicImage></f:facet>
						
					<table width="100%" cellpadding="0"  cellspacing="0" border="2" >
						<tbody>
							<tr>
								<td class="column-label7">
									<h:outputLabel id="defSpecTypeLabel" value="Type: " />
								</td>
								<td class="column-input2">
									<c:if test="#{importDefAndSpecsBean.isConnectionScreen}"> 
										<h:selectOneMenu id="defConnectionType" value="#{importDefAndSpecsBean.connectionType}">
											<f:selectItems value="#{importDefAndSpecsBean.connectionTypeCodeItems}" />				  
										</h:selectOneMenu>
									</c:if> 							
									<c:if test="#{!importDefAndSpecsBean.isConnectionScreen}"> 
										<h:selectOneMenu id="defSpecType" value="#{importDefAndSpecsBean.importDefSpecType}">
						                	<f:selectItems value="#{importDefAndSpecsBean.importDefSpecTypeItems}" />
						            	</h:selectOneMenu>
									</c:if>		                             
							 	</td>
								<td  width="50%">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-label7">
									<h:outputLabel id="defSpecCodeLabel" for="defSpecCodeText" value="Code: "	/>
								</td>
								<td class="column-input2">
									<h:inputText id="defSpecCodeText" value="#{importDefAndSpecsBean.defSpecCode}" />
								</td>
								<td  width="50%">&#160;</td>
							</tr>
							
							<tr>
								<td class="column-label7">
									<h:outputLabel id="defDescriptionLabel" for="defDescriptionText" value="Description: "	/>
								</td>
								<td class="column-input2">
									<h:inputText id="defDescriptionText" value="#{importDefAndSpecsBean.defDescriptionText}" />
								</td>
								<td  width="50%">&#160;</td>
							</tr>
	
							<tr>
								<td class="column-label7">
									<h:outputLabel id="defCommentsLabel" for="defCommentsText" value="Comments: "	/>
								</td>
								<td class="column-input2">
									<h:inputText id="defCommentsText" value="#{importDefAndSpecsBean.defCommentsText}" />
								</td>
								<td  width="50%">&#160;</td>
							</tr>	               
						</tbody>
					</table>	
				</rich:simpleTogglePanel>
				</div>
          	</div>       
      	
      
		    <div id="table-one-bottom">
				<ul class="right">
					<li class="clear">
						<a4j:commandLink id="btnClear" action="#{importDefAndSpecsBean.resetSearchFilter}" reRender="showhidefilter" >
						</a4j:commandLink>
					</li>
					<li class="search">
						<h:commandLink id="searchBtn"  action="#{importDefAndSpecsBean.listDefAndSpecs}" >
			
						</h:commandLink>
					</li> 
				</ul>
			</div> <!-- table-one-bottom -->
			
			</div>
			</div>
		</c:if>
    </a4j:outputPanel>
 
   
</h:form>            
      
<div class="wrapper">
	<span class="block-right">&#160;</span> 
	<span class="block-left tab">
		<img src="../images/containers/STSImportConfig-ComponentDetails.png"/>									   
	</span>
</div>
		
<h:form id="componentDetailsForm">
		<!-- default queue for the form is created -->
   		<a4j:queue/>
   		
		<div id="bottom">
			<div id="table-four" style="vertical-align: bottom;">
				<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
					<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
						<tbody>
							<tr>
								
								<c:if test="#{importDefAndSpecsBean.isConnectionScreen}"> 
									<td width="200" style="font-weight: bold;">Connections</td>	
								</c:if>
								<c:if test="#{importDefAndSpecsBean.isDefinitionScreen}"> 
									<td width="200" style="font-weight: bold;">Definitions</td>	
								</c:if>
								<c:if test="#{importDefAndSpecsBean.isSpecScreen}"> 
									<td width="200" style="font-weight: bold;">Specs</td>
								</c:if>					
								<td align="center" >
									<c:if test="#{importDefAndSpecsBean.isConnectionScreen}"> 
										<a4j:status id="cdfPageInfo"
										startText="Request being processed..." 
										stopText="#{importDefAndSpecsBean.importConnectionModel.pageDescription}" />	
									</c:if>
									<c:if test="#{importDefAndSpecsBean.isDefinitionScreen}"> 
										<a4j:status id="cdfPageInfo"
										startText="Request being processed..." 
										stopText="#{importDefAndSpecsBean.importDefinitionModel.pageDescription}" />	
									</c:if>
									<c:if test="#{importDefAndSpecsBean.isSpecScreen}"> 
										<a4j:status id="cdfPageInfo"
										startText="Request being processed..." 
										stopText="#{importDefAndSpecsBean.importSpecModel.pageDescription}" />	
									</c:if>									
								</td>
								<td align="center" width="270">&#160;</td>
								<td align="right" width="150">&#160;</td>
								<td style="width:20px;">&#160;</td>
							</tr>
						</tbody>
					</table>
				</div>
		
			<h:panelGroup id="listPanel" >		
				<h:panelGroup id="connPanel" rendered="#{importDefAndSpecsBean.isConnectionScreen}">
				<!--
					<ui:include src="/WEB-INF/view/components/import_config_component_table.xhtml">
						<ui:param name="formName" value="componentDetailsForm"/>
						<ui:param name="bean" value="#{importDefAndSpecsBean}"/>
						<ui:param name="dataModel" value="#{importDefAndSpecsBean.importConnectionModel}"/>
						<ui:param name="richListId" value="importConnectionList" />
						<ui:param name="scrollerId" value="importConnectionScroller" />
						<ui:param name="binding" value="#{importDefAndSpecsBean.importConnectionTable}" />
						<ui:param name="singleSelect" value="true"/>
						<ui:param name="multiSelect" value="false"/>
						<ui:param name="doubleClick" value="true"/>
						<ui:param name="selectReRender" value="defUsingConn,connComponentsButtons,defComponentsBottomButtons"/>
						<ui:param name="scrollReRender" value="cdfPageInfo"/>
					</ui:include>
				-->
					<div class="scrollContainer">
					<div class="scrollInner" id="resize">
					<rich:dataTable rowClasses="odd-row,even-row" id="importConnectionList"                               
                                    value="#{importDefAndSpecsBean.importConnectionModel}"
                                    var="connectionRow" rows="#{importDefAndSpecsBean.importConnectionModel.pageSize}">
         
	                            <a4j:support event="onRowClick" status="rowstatus"
									onsubmit="selectRow('componentDetailsForm:importConnectionList', this);"
									actionListener="#{importDefAndSpecsBean.selectedItemChanged}"
									reRender="defUsingConn,connComponentsButtons,defComponentsBottomButtons"
									immediate="true"  oncomplete="initScrollingTables();" />
                                  
                     			<rich:column id="importConnectionType" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="importConnectionType-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['importConnectionType']}"
					                              value="Connection Type"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.importConnectionType}" />
                     			</rich:column>
                     			
                     			<rich:column id="importConnectionCode" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="importConnectionCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['importConnectionCode']}"
					                              value="Connection Code"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.importConnectionCode}" />
                     			</rich:column>
                     			
                     			<rich:column id="description" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="description-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['description']}"
					                              value="Description"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.description}" />
                     			</rich:column>
                     			
                     			<rich:column id="dbDriverClassName" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="dbDriverClassName-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['dbDriverClassName']}"
					                              value="DB Driver Class Name"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.dbDriverClassName}" />
                     			</rich:column>
                     			
                     			<rich:column id="dbUrl" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="dbUrl-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['dbUrl']}"
					                              value="DB URL"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.dbUrl}" />
                     			</rich:column>
                     			
                     			<rich:column id="dbUserName" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="dbUserName-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['dbUserName']}"
					                              value="DB User Name"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.dbUserName}" />
                     			</rich:column>
                     			
                     			<rich:column id="dbPassword" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="dbPassword-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['dbPassword']}"
					                              value="DB Password"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.dbPassword}" />
                     			</rich:column>
                     			
                     			<rich:column id="comments" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="comments-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importConnectionModel.sortOrder['comments']}"
					                              value="Comments"
					                              actionListener="#{importDefAndSpecsBean.sortConnectionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importConnectionList" />
                       				</f:facet>
                       				<h:outputText value="#{connectionRow.comments}" />
                     			</rich:column>

                            </rich:dataTable>  
                            </div>
							</div>
							<rich:datascroller id="importConnectionScroller" for="importConnectionList" maxPages="10" oncomplete="initScrollingTables();"
							 	style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  immediate="true"  
							 	page="#{importDefAndSpecsBean.importConnectionModel.curPage}" reRender="cdfPageInfo" /> 
				
				</h:panelGroup>
				<h:panelGroup id="defPanel" rendered="#{importDefAndSpecsBean.isDefinitionScreen}">
				<!--
					<ui:include src="/WEB-INF/view/components/import_config_component_table.xhtml">
						<ui:param name="formName" value="componentDetailsForm"/>
						<ui:param name="bean" value="#{importDefAndSpecsBean}"/>
						<ui:param name="dataModel" value="#{importDefAndSpecsBean.importDefinitionModel}"/>
						<ui:param name="richListId" value="importDefinitionList" />
						<ui:param name="scrollerId" value="importDefinitionScroller" />					
						<ui:param name="binding" value="#{importDefAndSpecsBean.importDefinitionTable}" />
						<ui:param name="singleSelect" value="true"/>
						<ui:param name="multiSelect" value="false"/>
						<ui:param name="doubleClick" value="true"/>
						<ui:param name="selectReRender" value="specUsingDef,defComponentsButtons,specComponentsBottomButtons"/>
						<ui:param name="scrollReRender" value="cdfPageInfo"/>
					</ui:include>
				-->
					<div class="scrollContainer">
					<div class="scrollInner" id="resize">
					<rich:dataTable rowClasses="odd-row,even-row" id="importDefinitionList"                               
                                    value="#{importDefAndSpecsBean.importDefinitionModel}"
                                    var="definitionRow" rows="#{importDefAndSpecsBean.importDefinitionModel.pageSize}">
         
	                            <a4j:support event="onRowClick" status="rowstatus"
									onsubmit="selectRow('componentDetailsForm:importDefinitionList', this);"
									actionListener="#{importDefAndSpecsBean.selectedItemChanged}"
									reRender="specUsingDef,defComponentsButtons,specComponentsBottomButtons"
									immediate="true"  oncomplete="initScrollingTables();" />
                                  
                                <!--    
                            	<rich:column id="importDefinitionTypeRow" sortBy="#{definitionRow.importDefinitionType}">
                       				<f:facet name="header">
                       					<h:outputText id="id1x" value="Def.Type" />
                       				</f:facet>
                   					<h:outputText value="#{cacheManager.listCodeMap['IMPDEFTYPE'][definitionRow.importDefinitionType].description}" />
                     			</rich:column>
                     			-->
                     			<rich:column id="importDefinitionType" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="id1x-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['importDefinitionType']}"
					                              value="Def.Type"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                       				</f:facet>
                   					<h:outputText value="#{cacheManager.listCodeMap['IMPDEFTYPE'][definitionRow.importDefinitionType].description}" />
                     			</rich:column>
                     			
                     			<rich:column id="importDefinitionCode" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="importDefinitionCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['importDefinitionCode']}"
					                              value="Def. Code"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                       				</f:facet>
                       				<h:outputText value="#{definitionRow.importDefinitionCode}" />
                     			</rich:column>
                                    
                                <rich:column id="description" styleClass="column-left">
				                    <f:facet name="header">
				                    	<a4j:commandLink id="description-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['description']}"
					                              value="Description"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />			                    
				                    </f:facet>
				                    <h:outputText value="#{definitionRow.description}" />
				                </rich:column>
                                    
                                <rich:column id="importFileTypeCode" styleClass="column-left">
		                        	<f:facet name="header">
		                        		<a4j:commandLink id="importFileTypeCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['importFileTypeCode']}"
					                              value="File Type"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />	                        	
		                        	</f:facet>
		                        	<h:outputText value="#{cacheManager.listCodeMap['IMPFILETYP'][definitionRow.importFileTypeCode].description}" />
		                    	</rich:column>
		                    	
		                    	<rich:column id="flatDelChar" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="flatDelChar-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['flatDelChar']}"
					                              value="Flat Del Char"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.flatDelChar}" />
		 						</rich:column>   
		 						
		 						<rich:column id="flatDelTextQual" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="flatDelTextQual-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['flatDelTextQual']}"
					                              value="Flat Del Text Qual"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.flatDelTextQual}" />
		 						</rich:column>   
		 						
		 						<rich:column id="flatDelLine1HeadFlag" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="flatDelLine1HeadFlag-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['flatDelLine1HeadFlag']}"
					                              value="Flat Delimited 1st Line Header"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>              
                     				<h:selectBooleanCheckbox styleClass="check" value="#{definitionRow.flatDelLine1HeadFlag == 1}" disabled="#{true}" />               				
		 						</rich:column>   
		 						
		 						<rich:column id="importConnectionCode" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="importConnectionCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['importConnectionCode']}"
					                              value="Import Connection"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.importConnectionCode}" />
		 						</rich:column>
		 						
		 						<rich:column id="dbTableName" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="dbTableName-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['dbTableName']}"
					                              value="DB Table Name"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.dbTableName}" />
		 						</rich:column>

                                <rich:column id="comments" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="comments-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['comments']}"
					                              value="Comments"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.comments}" />
		 						</rich:column>
		 						
		 						<rich:column id="sampleFileName" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="sampleFileName-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['sampleFileName']}"
					                              value="Sample File Name"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />		
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.sampleFileName}" />
		 						</rich:column>
		 						
		 						<rich:column id="mapUpdateUserId" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="mapUpdateUserId-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['mapUpdateUserId']}"
					                              value="Map Update UserId"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.mapUpdateUserId}" />
		 						</rich:column>
		 						
		 						<rich:column id="mapUpdateTimestamp" styleClass="column-left">
                     				<f:facet name="header">
                     					<a4j:commandLink id="mapUpdateTimestamp-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importDefinitionModel.sortOrder['mapUpdateTimestamp']}"
					                              value="Map Update Timestamp"
					                              actionListener="#{importDefAndSpecsBean.sortDefinitionAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importDefinitionList" />
                     				</f:facet>
                     				<h:outputText value="#{definitionRow.mapUpdateTimestamp}" >
                     					<f:converter converterId="dateTime"/>
									</h:outputText>
		 						</rich:column>
                                
                            </rich:dataTable>  
                            </div>
							</div>
							<rich:datascroller id="importDefinitionScroller" for="importDefinitionList" maxPages="10" oncomplete="initScrollingTables();"
							 	style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  immediate="true"  
							 	page="#{importDefAndSpecsBean.importDefinitionModel.curPage}" reRender="cdfPageInfo" /> 
							 	
				</h:panelGroup>
				<h:panelGroup id="specPanel" rendered="#{importDefAndSpecsBean.isSpecScreen}">
					<!--
					<ui:include src="/WEB-INF/view/components/import_config_component_table.xhtml" >
						<ui:param name="formName" value="componentDetailsForm"/>
						<ui:param name="bean" value="#{importDefAndSpecsBean}"/>
						<ui:param name="dataModel" value="#{importDefAndSpecsBean.importSpecModel}"/>
						<ui:param name="richListId" value="importSpecList" />
						<ui:param name="scrollerId" value="importSpecScroller" />
						<ui:param name="binding" value="#{importDefAndSpecsBean.importSpecTable}" />
						<ui:param name="singleSelect" value="true"/>
						<ui:param name="multiSelect" value="false"/>
						<ui:param name="doubleClick" value="true"/>
						<ui:param name="selectReRender" value="impSDetail,specComponentsButtons"/>
						<ui:param name="scrollReRender" value="cdfPageInfo"/>
					</ui:include>
					-->
					<div class="scrollContainer">
					<div class="scrollInner" id="resize">
					<rich:dataTable rowClasses="odd-row,even-row" id="importSpecList"                               
                                    value="#{importDefAndSpecsBean.importSpecModel}"
                                    var="specRow" rows="#{importDefAndSpecsBean.importSpecModel.pageSize}">
         
	                            <a4j:support event="onRowClick" status="rowstatus"
									onsubmit="selectRow('componentDetailsForm:importSpecList', this);"
									actionListener="#{importDefAndSpecsBean.selectedItemChanged}"
									reRender="impSDetail,specComponentsButtons"
									immediate="true"  oncomplete="initScrollingTables();" />
                                  
                     			<rich:column id="importSpecType" styleClass="column-left">
                       				<f:facet name="header">
                       					<a4j:commandLink id="importSpecType-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['importSpecType']}"
					                              value="Import Spec Type"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
                       				</f:facet>
                       				<h:outputText value="#{cacheManager.listCodeMap['IMPDEFTYPE'][specRow.importSpecType].description}" />
                     			</rich:column>
                     			           
		                      	<rich:column id="importSpecCode" styleClass="column-left">
		                        	<f:facet name="header">
		                          		<a4j:commandLink id="importSpecCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['importSpecCode']}"
					                              value="Import Spec Code"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.importSpecCode}" />
		                      	</rich:column>
		                      
		                      	<rich:column id="description" styleClass="column-left">
		                        	<f:facet name="header">
		                          		<a4j:commandLink id="descrption-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['description']}"
					                              value="Description"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.description}" />
		                      	</rich:column>
		                      
		                      <rich:column id="importDefinitionCode" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="importDefinitionCode-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['importDefinitionCode']}"
					                              value="Import Definition Code"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{specRow.importDefinitionCode}" />
		                      </rich:column>

		                      <rich:column id="defaultDirectory" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="defaultDirectory-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['defaultDirectory']}"
					                              value="Default Dir"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{specRow.defaultDirectory}" />
		                      </rich:column>
		                      <rich:column id="lastImportFileName" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="lastImportFileName-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['lastImportFileName']}"
					                              value="Last Import File Name"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{specRow.lastImportFileName}" />
		                      </rich:column>
		                      <rich:column id="comments" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="comments-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['comments']}"
					                              value="Comments"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{specRow.comments}" />
		                      </rich:column>
		                      
		                      <rich:column id="whereClause" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="whereClause-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['whereClause']}"
					                              value="Where Clause"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{specRow.whereClause}" />
		                      </rich:column>
		                      	
							  <rich:column id="filesizedupres" styleClass="column-left">
		                        <f:facet name="header">
		                          	<a4j:commandLink id="filesizedupres-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['filesizedupres']}"
					                              value="File Size Dup. Res."
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        </f:facet>
		                        <h:outputText value="#{importDefAndSpecsBean.fileDupMap[specRow.filesizedupres]}" />
		                      </rich:column>

								<rich:column id="filenamedupres" styleClass="column-left">
		                        	<f:facet name="header">
		                          		<a4j:commandLink id="filenamedupres-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['filenamedupres']}"
					                              value="File Name Dup. Res."
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{importDefAndSpecsBean.fileDupMap[specRow.filenamedupres]}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="importdatemap" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="importdatemap-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['importdatemap']}"
					                              value="Import Data Map"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.importdatemap}" />
		                      	</rich:column>
		                      	<!--  
		                      	<rich:column id="donotimport" sortBy="#{specRow.donotimport}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Donot Import" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.donotimport}" />
		                      	</rich:column>

								<rich:column id="replacewith" sortBy="#{specRow.replacewith}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Replace With" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.replacewith}" />
		                      	</rich:column>
		                      	-->
		                      	
		                      	<rich:column id="batcheofmarker" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="batcheofmarker-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['batcheofmarker']}"
					                              value="Batch EOF Marker"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.batcheofmarker}" />
		                      	</rich:column>

								<rich:column id="batchcountmarker" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="batchcountmarker-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['batchcountmarker']}"
					                              value="Batch Count Marker"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.batchcountmarker}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="batchtotalmarker" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="batchtotalmarker-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['batchtotalmarker']}"
					                              value="Batch Total Marker"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.batchtotalmarker}" />
		                      	</rich:column>
		                      	<!--
		                      	<rich:column id="delete0amtflag" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="delete0amtflag-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['delete0amtflag']}"
					                              value="Delete 0 Amt Flag"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{specRow.delete0amtflag == 1}" disabled="#{true}" />
		                      	</rich:column>

								<rich:column id="holdtransflag" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="holdtransflag-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['holdtransflag']}"
					                              value="Hold Trans. Flag"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{specRow.holdtransflag == 1}" disabled="#{true}" />
		                      	</rich:column>
								
								<rich:column id="holdtransamt" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="holdtransamt-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['holdtransamt']}"
					                              value="Hold Trans. Amt."
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.holdtransamt}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="autoprocflag" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="autoprocflag-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['autoprocflag']}"
					                              value="Auto-process Flag"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{specRow.autoprocflag == 1}" disabled="#{true}" />
		                      	</rich:column>

								<rich:column id="autoprocamt" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="autoprocamt-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['autoprocamt']}"
					                              value="Auto-process Amt."
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.autoprocamt}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="autoprocid" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="autoprocid-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['autoprocid']}"
					                              value="Auto-process TaxCode"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:outputText value="#{specRow.autoprocid}" />
		                      	</rich:column>
								-->
								<rich:column id="autoprocessFlag" styleClass="column-left">
		                        	<f:facet name="header">
		                          			<a4j:commandLink id="autoprocessFlag-a4j"
					                              styleClass="sort-#{importDefAndSpecsBean.importSpecModel.sortOrder['autoprocessFlag']}"
					                              value="AID Auto-process Flag"
					                              actionListener="#{importDefAndSpecsBean.sortSpecAction}"
					                              oncomplete="initScrollingTables();"
					                              immediate="true"
					                              reRender="importSpecList" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{specRow.autoprocessFlag == 1}" disabled="#{true}" />
		                      	</rich:column>              			
                     			
                            </rich:dataTable>  
                            </div>
							</div>
							<rich:datascroller id="importSpecScroller" for="importSpecList" maxPages="10" oncomplete="initScrollingTables();"
							 	style="clear:both;" align="center" stepControls="auto" ajaxSingle="false"  immediate="true"  
							 	page="#{importDefAndSpecsBean.importSpecModel.curPage}" reRender="cdfPageInfo" /> 
					
				</h:panelGroup>
		</h:panelGroup>		
		
		<div id="table-four-bottom">				
				<h:panelGroup id="componentsButtons" >
					<h:panelGroup id = "specComponentsButtons" rendered="#{importDefAndSpecsBean.isSpecScreen}">
						<ul class="right">
							<li class="add"><h:commandLink id="btnAddDef"  disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displayImportSpecAddAction}"  /></li>
							
							<!-- action="" -->
						 	<!--  <li class="copy-add"><h:commandLink id="copyAddBtnDef"  /></li>-->
							<li class="copy-add"><h:commandLink id="updateBtnDef"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validSpecSelection or !importDefAndSpecsBean.isSpecUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
				                                   action="#{importDefAndSpecsBean.displayImportSpecCopyAddAction}" /></li>
				                                   
							<li class="update"><h:commandLink id="copyAddBtnDef"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validSpecSelection or !importDefAndSpecsBean.isSpecUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
				                                   action="#{importDefAndSpecsBean.displayImportSpecUpdateAction}" /></li>
                                 
							<li class="delete115"><h:commandLink id="deleteBtnDef" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validSpecSelection or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                               						  	action="#{importDefAndSpecsBean.displayImportSpecDeleteAction}" /></li>
							<li class="map"><h:commandLink id="mapBtnDef"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validSpecSelection or !importDefAndSpecsBean.isSpecUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                                 							action="#{importDefAndSpecsBean.displayImportSpecMapAction}"  /></li>
                            <li class="view"><h:commandLink id="viewBtnDef"  disabled="#{!importDefAndSpecsBean.validSpecSelection}"
				                                   action="#{importDefAndSpecsBean.displayImportSpecViewAction}" /></li>
							<!-- <li class="view"><h:commandLink id="viewBtnDef"  /></li>  -->
						</ul>	
					</h:panelGroup>
					<h:panelGroup id = "defComponentsButtons" rendered="#{importDefAndSpecsBean.isDefinitionScreen}">
						<ul class="right">
							<li class="add"><h:commandLink id="btnAddSpec" disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayImportDefAddAction}" /></li>
							
							<!-- action="" -->
						 	<!-- <li class="copy-add"><h:commandLink id="copyAddBtnSpec"  /></li>  -->				
							<li class="copy-add"><h:commandLink id="copyAddBtnSpec"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validDefSelection or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                                							 action="#{importDefAndSpecsBean.displayImportDefCopyAddAction}"  /></li>
							
							<li class="update"><h:commandLink id="updateBtnSpec"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validDefSelection or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                                							 action="#{importDefAndSpecsBean.displayImportDefUpdateAction}"  /></li>
							<li class="delete115"><h:commandLink id="deleteBtnSpec" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.canDeleteDef or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                               							  action="#{importDefAndSpecsBean.displayImportDefDeleteAction}" /></li>
							<li class="map"><h:commandLink id="mapBtnSpec"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.canMapDef  or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
                               							  action="#{importDefAndSpecsBean.displayImportDefMapAction}"  /></li>
						  
							<!-- <li class="view"><h:commandLink id="viewBtnSpec"  /></li> -->
							<li class="view"><h:commandLink id="viewBtnSpec" disabled="#{importDefAndSpecsBean.selectedDefIndex == -1}"
															action="#{importDefAndSpecsBean.displayImportDefViewAction}" /></li>
						</ul>	
					</h:panelGroup>
					<h:panelGroup id = "connComponentsButtons" rendered="#{importDefAndSpecsBean.isConnectionScreen}">
						<ul class="right">
							<li class="add"><h:commandLink id="btnAddConn" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayImportConnectionAddAction}" /></li>
							
							<!-- action="" -->
						 	<li class="copy-add"><h:commandLink id="copyAddBtnConn" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayImportConnectionCopyAddAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>
							
							<li class="update"><h:commandLink id="updateBtnConn" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayImportConnectionUpdateAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>
							<li class="delete115"><h:commandLink id="deleteBtnConn" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayImportConnectionDeleteAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>
							<!-- <li class="map"><h:commandLink id="mapBtnConn" action="#{importDefAndSpecsBean.displayImportConnectionMapAction}"  rendered="#{! empty importDefAndSpecsBean.selectedImportConnectionComponent }" /></li> -->
							<li class="view"><h:commandLink id="viewBtnConn" action="#{importDefAndSpecsBean.displayImportConnectionViewAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }"/></li>
						</ul>	
					</h:panelGroup>
				</h:panelGroup>				
			
		</div>	
		</div>
	</div>
</h:form>

<div class="wrapper">
	<span class="block-right">&#160;</span> 
	<span class="block-left tab">&#160;</span>
</div>
	
<h:form id="usageNUsersForm">
<a4j:queue/>
	<div id="bottom">
		<div id="table-four" style="vertical-align: bottom;">
			<div id="table-four-top" style="padding-left: 23px; padding-bottom: 0;">
				<table cellpadding="0" cellspacing="0" style="width: 100%; height: 22px; vertical-align: bottom;">
					<tbody>
						<tr>
							<td width="200" style="font-weight: bold;" >#{importDefAndSpecsBean.usageNUsersFormCaption}</td>
							<td align="center" >
							<a4j:status id="subListInfo" forceId="true"
								startText="Request being processed..." 
								stopText="" /> 
																				
							</td>
							<td align="center" width="270">&#160;</td>
							<td align="right" width="150">&#160;</td>
							<td style="width:20px;">&#160;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="table-four-content">
				<h:panelGroup id="subListPanel" >
					<h:panelGroup rendered="#{importDefAndSpecsBean.isConnectionScreen}">
						<div class="scrollContainer">
					    <div class="scrollInner" id="resize">
							<rich:dataTable rowClasses="odd-row,even-row" id="defUsingConn"
                                    rendered="#{importDefAndSpecsBean.isConnectionScreen}"
                                    value="#{importDefAndSpecsBean.definitionsUsingConnection}"
                                    var="defUsingConnRow">
         
	                            <a4j:support id="ref2tbl" event="onRowClick" status="rowstatus"
									onsubmit="selectRow('usageNUsersForm:defUsingConn', this);"
									actionListener="#{importDefAndSpecsBean.selectedDefUsingConnRowChanged}"
									reRender="defComponentsBottomButtons"/>
                                    
                            	<rich:column id="importDefinitionTypeId" sortBy="#{defUsingConnRow.importDefinitionType}">
                       				<f:facet name="header"><h:outputText id="id1x" value="Def.Type" styleClass="column-sort-background" /></f:facet>
                   					<h:outputText value="#{cacheManager.listCodeMap['IMPDEFTYPE'][defUsingConnRow.importDefinitionType].description}" />
                     			</rich:column>
                                    
                                <rich:column id="importDefinitionCodeId" sortBy="#{defUsingConnRow.importDefinitionCode}">
                       				<f:facet name="header"><h:outputText id="id2x" value="Def. Code" styleClass="column-sort-background" /></f:facet>
                       				<h:outputText value="#{defUsingConnRow.importDefinitionCode}" />
                     			</rich:column>
                                    
                                <rich:column id="descriptionId" sortBy="#{defUsingConnRow.description}">
				                    <f:facet name="header"><h:outputText id="id3x" value="Description" styleClass="column-sort-background" /></f:facet>
				                    <h:outputText value="#{defUsingConnRow.description}" />
				                </rich:column>
                                    
                                <rich:column id="fileTypeId" sortBy="#{defUsingConnRow.importFileTypeCode}">
		                        	<f:facet name="header"><h:outputText id="id4x" value="File Type" styleClass="column-sort-background" /></f:facet>
		                        	<h:outputText value="#{cacheManager.listCodeMap['IMPFILETYP'][defUsingConnRow.importFileTypeCode].description}" />
		                    	</rich:column>
		                    	
		                    	<rich:column id="importConnectionCodeId" sortBy="#{defUsingConnRow.importConnectionCode}">
		                        	<f:facet name="header"><h:outputText id="id5x" value="Import Connection" styleClass="column-sort-background" /></f:facet>
		                        	<h:outputText value="#{defUsingConnRow.importConnectionCode}" />
		                    	</rich:column>
		                    	
		                    	<rich:column id="dbTableNameId" sortBy="#{defUsingConnRow.dbTableName}">
		                        	<f:facet name="header"><h:outputText id="id6x" value="DB Table Name" styleClass="column-sort-background" /></f:facet>
		                        	<h:outputText value="#{defUsingConnRow.dbTableName}" />
		                    	</rich:column>
		                    	
		                    	<!--  
		                    	<rich:column id="flatDelCharId" sortBy="#{defUsingConnRow.flatDelChar}">
                     				<f:facet name="header"><h:outputText value="Flat Del Char" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.flatDelChar}" />
		 						</rich:column>   
		 						
		 						<rich:column id="flatDelTextQualId" sortBy="#{defUsingConnRow.flatDelTextQual}">
                     				<f:facet name="header"><h:outputText value="Flat Del Text Qual" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.flatDelTextQual}" />
		 						</rich:column>   
		 						
		 						<rich:column id="flatDelLine1HeadFlagId" sortBy="#{defUsingConnRow.flatDelLine1HeadFlag}">
                     				<f:facet name="header"><h:outputText value="Flat Delimited 1st Line Header" /></f:facet>              
                     				<h:selectBooleanCheckbox styleClass="check" value="#{defUsingConnRow.flatDelLine1HeadFlag == 1}" disabled="#{true}" />               				
		 						</rich:column>   
                				--> 
                                <rich:column id="commentsId" sortBy="#{defUsingConnRow.comments}">
                     				<f:facet name="header"><h:outputText value="Comments" styleClass="column-sort-background" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.comments}" />
		 						</rich:column>
		 						
		 						<!-- 
		 						<rich:column id="sampleFileNameId" sortBy="#{defUsingConnRow.sampleFileName}">
                     				<f:facet name="header"><h:outputText value="Sample File Name" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.sampleFileName}" />
		 						</rich:column>
		 						
		 						<rich:column id="mapUpdateUserIdId" sortBy="#{defUsingConnRow.mapUpdateUserId}">
                     				<f:facet name="header"><h:outputText value="Map Update UserId" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.mapUpdateUserId}" />
		 						</rich:column>
		 						
		 						<rich:column id="mapUpdateTimestampId" sortBy="#{defUsingConnRow.mapUpdateTimestamp}">
                     				<f:facet name="header"><h:outputText value="Map Update Timestamp" /></f:facet>
                     				<h:outputText value="#{defUsingConnRow.mapUpdateTimestamp}" />
		 						</rich:column> 
		 						-->   
                            </rich:dataTable>        
					    </div>
				        </div>
					</h:panelGroup>
					
					<h:panelGroup rendered="#{importDefAndSpecsBean.isDefinitionScreen}">				
						<div class="scrollContainer">
			            <div class="scrollInner" id="resize">
		                    <rich:dataTable rowClasses="odd-row,even-row" id="specUsingDef"
		                                    rendered="#{importDefAndSpecsBean.isDefinitionScreen}"
		                                    value="#{importDefAndSpecsBean.selectedImportSpecList}"
		                                    var="row">
		                      <a4j:support id="ref3tbl" event="onRowClick"
		                                   	onsubmit="selectRow('usageNUsersForm:specUsingDef', this);"
		                                   	actionListener="#{importDefAndSpecsBean.selectedSpecUsingDefRowChanged}"
		                                   	reRender="specComponentsBottomButtons" />
		                                   	

		                      <rich:column id="importSpecTypeId" sortBy="#{row.importSpecType}">
                       				<f:facet name="header">
                       					<h:outputText value="Import Spec Type" styleClass="column-sort-background" />
                       				</f:facet>
                   					<h:outputText value="#{cacheManager.listCodeMap['IMPDEFTYPE'][row.importSpecType].description}" />
                     			</rich:column>
		                                   
		                      <rich:column id="importSpecCodeId" sortBy="#{row.importSpecCode}">
		                        <f:facet name="header">
		                          <h:outputText value="Import Spec Code" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.importSpecCode}" />
		                      </rich:column>
		                      
		                      <rich:column id="descrptionId" sortBy="#{row.description}">
		                        <f:facet name="header">
		                          <h:outputText value="Description" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.description}" />
		                      </rich:column>
		                      
		                      <rich:column id="importDefinitionCodeId" sortBy="#{row.importDefinitionCode}">
		                        <f:facet name="header">
		                          <h:outputText value="Import Definition Code" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.importDefinitionCode}" />
		                      </rich:column>

		                      <rich:column id="defaultDirectoryId" sortBy="#{row.defaultDirectory}">
		                        <f:facet name="header">
		                          <h:outputText value="Default Dir" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.defaultDirectory}" />
		                      </rich:column>
		                      <rich:column id="lastImportFileNameId" sortBy="#{row.lastImportFileName}">
		                        <f:facet name="header">
		                          <h:outputText value="Last Import File Name" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.lastImportFileName}" />
		                      </rich:column>
		                      <rich:column id="commentsId" sortBy="#{row.comments}">
		                        <f:facet name="header">
		                          <h:outputText value="Comments" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.comments}" />
		                      </rich:column>
		                      
		                      <rich:column id="whereClauseId" sortBy="#{row.whereClause}">
		                        <f:facet name="header">
		                          <h:outputText value="Where Clause" styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{row.whereClause}" />
		                      </rich:column>
		                      	
							  <rich:column id="filesizedupresId" sortBy="#{row.filesizedupres}">
		                        <f:facet name="header">
		                          <h:outputText value="File Size Dup. Res." styleClass="column-sort-background" />
		                        </f:facet>
		                        <h:outputText value="#{importDefAndSpecsBean.fileDupMap[row.filesizedupres]}" />
		                      </rich:column>

								<rich:column id="filenamedupresId" sortBy="#{row.filenamedupres}">
		                        	<f:facet name="header">
		                          		<h:outputText value="File Name Dup. Res." styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{importDefAndSpecsBean.fileDupMap[row.filenamedupres]}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="importdatemapId" sortBy="#{row.importdatemap}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Import Data Map" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.importdatemap}" />
		                      	</rich:column>
		                      	<!--  
		                      	<rich:column id="donotimportId" sortBy="#{row.donotimport}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Donot Import" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.donotimport}" />
		                      	</rich:column>

								<rich:column id="replacewithId" sortBy="#{row.replacewith}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Replace With" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.replacewith}" />
		                      	</rich:column>
		                      	-->
		                      	
		                      	<rich:column id="batcheofmarkerId" sortBy="#{row.batcheofmarker}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Batch EOF Marker" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.batcheofmarker}" />
		                      	</rich:column>

								<rich:column id="batchcountmarkerId" sortBy="#{row.batchcountmarker}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Batch Count Marker" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.batchcountmarker}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="batchtotalmarkerId" sortBy="#{row.batchtotalmarker}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Batch Total Marker" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.batchtotalmarker}" />
		                      	</rich:column>
		                      	<!--  
		                      	<rich:column id="delete0amtflagId" sortBy="#{row.delete0amtflag}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Delete 0 Amt Flag" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{row.delete0amtflag == 1}" disabled="#{true}" />
		                      	</rich:column>

								<rich:column id="holdtransflagId" sortBy="#{row.holdtransflag}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Hold Trans. Flag" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{row.holdtransflag == 1}" disabled="#{true}" />
		                      	</rich:column>
								
								<rich:column id="holdtransamtId" sortBy="#{row.holdtransamt}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Hold Trans. Amt." styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.holdtransamt}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="autoprocflagId" sortBy="#{row.autoprocflag}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Auto-process Flag" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{row.autoprocflag == 1}" disabled="#{true}" />
		                      	</rich:column>

								<rich:column id="autoprocamtId" sortBy="#{row.autoprocamt}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Auto-process Amt." styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.autoprocamt}" />
		                      	</rich:column>
		                      	
		                      	<rich:column id="autoprocidId" sortBy="#{row.autoprocid}">
		                        	<f:facet name="header">
		                          		<h:outputText value="Auto-process TaxCode" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:outputText value="#{row.autoprocid}" />
		                      	</rich:column>
								-->
								<rich:column id="autoprocessFlagId" sortBy="#{row.autoprocessFlag}">
		                        	<f:facet name="header">
		                          		<h:outputText value="AID Auto-process Flag" styleClass="column-sort-background" />
		                        	</f:facet>
		                        	<h:selectBooleanCheckbox styleClass="check" value="#{row.autoprocessFlag == 1}" disabled="#{true}" />
		                      	</rich:column>

		                    </rich:dataTable>

		                  </div>
	                </div>
					
					
					
					</h:panelGroup>
					<h:panelGroup rendered="#{importDefAndSpecsBean.isSpecScreen}">
						<div class="scrollContainer">
							<div class="scrollInner" id="resize">
					
								<rich:dataTable rowClasses="odd-row,even-row" id="impSDetail"
		                                    rendered="#{importDefAndSpecsBean.isSpecScreen}"
		                                    value="#{importDefAndSpecsBean.userList}"
		                                    var="userDtl">
					             	<rich:column id="userCode" sortBy="#{userDtl.userCode}">
					                	<f:facet name="header">
					                    	<h:outputText value="User Code" styleClass="column-sort-background" />
					                    </f:facet>
					                    <h:outputText value="#{userDtl.userCode}" />
					                </rich:column>
			                      	<rich:column id="userName" sortBy="#{userDtl.userName}">
			                        	<f:facet name="header">
			                          		<h:outputText value="User Name" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:outputText value="#{userDtl.userName}" />
			                      	</rich:column>
			                      	<rich:column id="activeFlag" sortBy="#{userDtl.activeFlagBoolean}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Active Flag" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:selectBooleanCheckbox styleClass="check"
			                                 value="#{userDtl.activeFlagBoolean}"
			                                 disabled="True" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="defaultMatrixLineFlag" sortBy="#{userDtl.defaultMatrixLineFlag}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Default Matrix Line Flag" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:selectBooleanCheckbox styleClass="check"
			                                 value="#{userDtl.defaultMatrixLineFlag == 1}"
			                                 disabled="True" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="globalBuFlag" sortBy="#{userDtl.globalBuFlag}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Global B.U. Flag" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:selectBooleanCheckbox styleClass="check"
			                                 value="#{userDtl.globalBuFlag == 1}"
			                                 disabled="True" />
			                      	</rich:column>
			                      				                   
			                      	<rich:column id="adminFlag" sortBy="#{userDtl.adminFlag}">
										<f:facet name="header">
											<h:outputText value="Administrative?" styleClass="column-sort-background" />
										</f:facet>
										<h:outputText value="#{importDefAndSpecsBean.administrativeMap[userDtl.adminFlag]}" />	
									</rich:column>
			                	
			                      	<rich:column id="addNexusFlag" sortBy="#{userDtl.addNexusFlag}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Add Nexus Flag" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:selectBooleanCheckbox styleClass="check"
			                                 value="#{userDtl.addNexusFlag == 1}"
			                                 disabled="True" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="lastModule" sortBy="#{userDtl.lastModule}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Last Module" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:outputText value="#{userDtl.lastModule}" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="lastUsedEntityId" sortBy="#{userDtl.lastUsedEntityId}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Last Used Entity Id" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:outputText value="#{userDtl.lastUsedEntityId}" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="updateUserId" sortBy="#{userDtl.updateUserId}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Update User Id" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:outputText value="#{userDtl.updateUserId}" />
			                      	</rich:column>
			                      	
			                      	<rich:column id="updateTimestamp" sortBy="#{userDtl.updateTimestamp}">
			                        	<f:facet name="header">
			                          		<h:outputText value="Update Timestamp" styleClass="column-sort-background" />
			                        	</f:facet>
			                        	<h:outputText value="#{userDtl.updateTimestamp}"  >
			                        		<f:converter converterId="dateTime"/>
			                        	</h:outputText>
			                      	</rich:column>
					                              
                   					</rich:dataTable>	
                   				</div>
                   			</div>							
						</h:panelGroup>						
				</h:panelGroup>
			</div>
		
		<!-- Bottom datagrid -->	
		<div id="table-four-bottom">				
				<h:panelGroup id="componentsBottomButtons" >
					<h:panelGroup id = "defComponentsBottomButtons" rendered="#{importDefAndSpecsBean.isConnectionScreen}">
						<ul class="right">
							<li class="add">		<h:commandLink id="btnAddDefBottom" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.validDefSelection or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}" action="#{importDefAndSpecsBean.displayDefUsingConnAddAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }"  /></li>
							<li class="copy-add">	<h:commandLink id="copyAddBtnDefBottom" disabled="#{!importDefAndSpecsBean.displayUpdateDefUsingConn or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
														style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displayDefUsingConnCopyAddAction}" /></li>
							<li class="update">		<h:commandLink id="updateBtnDefBottom"    disabled="#{!importDefAndSpecsBean.displayUpdateDefUsingConn or !importDefAndSpecsBean.isDefUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"
				                                   		style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displayDefUsingConnUpdateAction}" /></li>                               
							<li class="delete115">	<h:commandLink id="deleteBtnDefBottom" disabled="#{!importDefAndSpecsBean.displayUpdateDefUsingConn}"
                               					  		style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displayDefUsingConnDeleteAction}" /></li>
							<li class="map">		<h:commandLink id="mapBtnDefBottom"          disabled="#{!importDefAndSpecsBean.displayUpdateDefUsingConn or !importDefAndSpecsBean.isDefUpdatePcoAllow}"
                                 						style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displayDefUsingConnMapAction}"  /></li>
                            <li class="view">		<h:commandLink id="viewBtnDefBottom" disabled="#{!importDefAndSpecsBean.displayUpdateDefUsingConn}"
                               					  		action="#{importDefAndSpecsBean.displayDefUsingConnViewAction}" /></li>
                                 					
                                 					
						</ul>	
					</h:panelGroup>
				 
				 	  
					<h:panelGroup id = "specComponentsBottomButtons" rendered="#{importDefAndSpecsBean.isDefinitionScreen}">
						<ul class="right">
							<li class="add">		<h:commandLink id="btnAddSpecBottom" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" action="#{importDefAndSpecsBean.displaySpecUsingDefAddAction}" disabled="#{importDefAndSpecsBean.selectedDefIndex == -1 or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}" /></li>
							<li class="copy-add">	<h:commandLink id="copyAddBtnSpecBottom" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.displayUpdateSpecUsingDef or !importDefAndSpecsBean.isSpecUpdatePcoAllow or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}"
														action="#{importDefAndSpecsBean.displaySpecUsingDefCopyAddAction}" /></li>
							<li class="update">		<h:commandLink id="updateBtnSpecBottom"  style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.displayUpdateSpecUsingDef or !importDefAndSpecsBean.isSpecUpdatePcoAllow or dataDefinitionBean.currentUser.viewOnlyBooleanFlag}"
				                                   		action="#{importDefAndSpecsBean.displaySpecUsingDefUpdateAction}" /></li>                               
							<li class="delete115">	<h:commandLink id="deleteBtnSpecBottom" style="#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" disabled="#{!importDefAndSpecsBean.displayUpdateSpecUsingDef}"
                               					  		action="#{importDefAndSpecsBean.displaySpecUsingDefDeleteAction}" /></li>
						<!--  	<li class="map">		<h:commandLink id="mapBtnSpecBottom"          disabled="#{!importDefAndSpecsBean.displayUpdateSpecUsingDef}"
                                 						action="#{importDefAndSpecsBean.displayDefUsingConnMapAction}"  /></li>-->
                            <li class="map">
                            	<h:commandLink id="mapBtnSpecBottom" action="#{importDefAndSpecsBean.displaySpecUsingDefMapAction}" 
                            				  style = "#{(importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag)? 'display:none;':'display:block;'}" 
                            				  disabled = "#{!importDefAndSpecsBean.displayUpdateSpecUsingDef or !importDefAndSpecsBean.isSpecUpdatePcoAllow or importDefAndSpecsBean.currentUser.viewOnlyBooleanFlag}"/>
                            </li>
                                 						
                            <li class="view">		<h:commandLink id="viewBtnSpecBottom"  disabled="#{!importDefAndSpecsBean.displayUpdateSpecUsingDef}"
                               					  		action="#{importDefAndSpecsBean.displaySpecUsingDefViewAction}" /></li>
                             <!--     					
							<li class="add"><h:commandLink id="btnAddSpecBottom" action="#{importDefAndSpecsBean.displayImportDefAddAction}" /></li>
							<li class="update"><h:commandLink id="updateBtnSpecBottom"  disabled="#{importDefAndSpecsBean.selectedDefIndex == -1}"
                                							 action="#{importDefAndSpecsBean.displayImportDefUpdateAction}"  /></li>
							<li class="delete115"><h:commandLink id="deleteBtnSpecBottom" disabled="#{!importDefAndSpecsBean.canDeleteDef}"
                               							  action="#{importDefAndSpecsBean.displayImportDefDeleteAction}" /></li>
							<li class="map"><h:commandLink id="mapBtnSpecBottom"  disabled="#{!importDefAndSpecsBean.canMapDef}"
                               							  action="#{importDefAndSpecsBean.displayImportDefMapAction}"  /></li>
                             --> 					
						</ul>	
					</h:panelGroup>
					
					<!--
					<h:panelGroup id = "connComponentsButtons" rendered="#{importDefAndSpecsBean.isConnectionScreen}">
						<ul class="right">			
						 	<li class="copy-add"><h:commandLink id="copyAddBtnConnBottom" action="#{importDefAndSpecsBean.displayImportConnectionCopyAddAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>	
							<li class="update"><h:commandLink id="updateBtnConnBottom" action="#{importDefAndSpecsBean.displayImportConnectionUpdateAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>
							<li class="delete115"><h:commandLink id="deleteBtnConnBottom" action="#{importDefAndSpecsBean.displayImportConnectionDeleteAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }" /></li>					
							<li class="view"><h:commandLink id="viewBtnConn" actionBottom="#{importDefAndSpecsBean.displayImportConnectionViewAction}" rendered="#{! empty importDefAndSpecsBean.indexImportConnectionComponent }"/></li>
						</ul>	
					</h:panelGroup>
					-->
				</h:panelGroup>				
			
		</div>		
	</div>	
</div>
</h:form>
	
</ui:define>
</ui:composition>
</html>
