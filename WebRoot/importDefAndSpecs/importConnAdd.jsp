<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:a4j="http://richfaces.org/a4j">
<ui:composition template="/WEB-INF/view/templates/standard.xhtml">
 
<ui:define name="body">
<h1>Import Connections</h1>

<h:form id="importConnAddForm" enctype="multipart/form-data" >
<a4j:queue/>

<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<h:messages errorClass="error" />
	</div>

	<div id="table-one-content" style="height:900px;">
		<table cellpadding="0" cellspacing="0" width="913" id="rollover" class="ruler">
			<thead>
				<tr><td colspan="4"><h:outputText value="#{importDefAndSpecsBean.actionText}" /></td></tr>
			</thead>
			<tbody>
				<tr><th class="togglepanel" colspan="4">Connection</th></tr>
				<tr>
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Connection Code:</td>
					<td class="column-input2">
						<h:inputText id="ImportConnCode"		                        
			                    label="Connection Code"
			                    value="#{importDefAndSpecsBean.selectedImportConnectionComponent.importConnectionCode}"
			                    disabled="#{(importDefAndSpecsBean.readOnlyAction) or (importDefAndSpecsBean.currentAction eq 'UpdateConn')}"
			                    onkeypress="upperCaseInputTextKey(event,this)" 
								onblur="upperCaseInputText(this)" />	
					</td>
					<td>&#160;</td>						
				</tr>
				<tr>
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Description:</td>
					<td class="column-input2">
					 <h:inputText id="ImportConnDescription"		                        
			                    label="Description"
			                    value="#{importDefAndSpecsBean.selectedImportConnectionComponent.description}"
			                    disabled="#{importDefAndSpecsBean.readOnlyAction}" />
					</td>
					<td>&#160;</td>
				</tr>
				<tr>
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Comments:</td>
					<td class="column-input2">
					 <h:inputText id="ImportConnComments"		                        
			                        label="Comments"
			                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.comments}"
			                        disabled="#{importDefAndSpecsBean.readOnlyAction}"/>
					</td>
					<td>&#160;</td>
				</tr>
				
				<tr>
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Connection Type:</td>
					<td class="column-input3">
						<h:selectOneMenu id="selConnectionType" disabled="#{importDefAndSpecsBean.readOnlyAction}"
							value="#{importDefAndSpecsBean.selectedImportConnectionComponent.importConnectionType}">
							<f:selectItems value="#{importDefAndSpecsBean.connectionTypeCodeItems}" />
						 <a4j:support event="onchange" reRender="importConnAddForm,btnConnVerify,t1ok" /> 													  
						</h:selectOneMenu>
					</td>
					<td>&#160;</td>
					
				</tr>	
				<tr>						
					<th class="togglepanel" colspan="4">Connection Parameters</th>						
				</tr>
				
				<c:if test="#{importDefAndSpecsBean.doShowConnectionPropertiesSubForm}" >

					<tr>
						<td style="width:25px;">&#160;</td>
						<td class="column-label3">Driver Class Name:</td>
						<td class="column-input2">
							<h:inputText id="ImportConnDriverClassName"
					                        
					                        label="Driver Class Name"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.dbDriverClassName}"
					                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
					                         />
						</td>
						<td>&#160;</td>
					</tr>
					
					
					<tr>
						<td style="width:25px;">&#160;</td>
						<td class="column-label3">URL:</td>
						<td class="column-input2">
							<h:inputText id="ImportConnUrl"
					                        
					                        label="URL:"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.dbUrl}"
					                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
					                         />
						</td>
						<td>&#160;</td>
					</tr>
					
					<tr>
						<td style="width:25px;">&#160;</td>
						<td class="column-label3">User Name:</td>
						<td class="column-input3">
							<h:inputText id="ImportConnUserName"
					                        
					                        label="User Name"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.dbUserName}"
					                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
					                         />
						</td>
						<td>&#160;</td>
					</tr>

					<tr>
						<td style="width:25px;">&#160;</td>
						<td class="column-label3">Password:</td>
						<td class="column-input3">
							<h:inputText id="ImportConnPassword"
					                        
					                        label="Password"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.dbPassword}"
					                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
					                         />
						</td>
						<td>&#160;</td>
					</tr>							
						
					<tr>
						<td style="width:25px;">&#160;</td>
						<td class="column-label3">Verification:</td>
						<td class="column-input2">
							<h:inputText id="ImportConnVerification" label="Verification" style="width:650px;"
					                        value="#{importDefAndSpecsBean.verificationErrorMessage}" disabled="true"/>
						</td>
						<td style="width:10%;">&#160;</td>
					</tr>
												
				</c:if>
				<tr>					
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Last Update User ID:</td>
					<td class="column-input2">
						<h:inputText id="ImportConnLastUpdUID"
					                        label="Last Update User ID"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.updateUserId}"						                        
					                        disabled="true" />
					</td>
					<td>&#160;</td>
				</tr>						
				<tr>		
					<td style="width:25px;">&#160;</td>
					<td class="column-label3">Last Update Timestamp:</td>
					<td class="column-input2">
						<h:inputText id="ImportConnLastUpdTS"
					                        label="Last Update Timestamp"
					                        value="#{importDefAndSpecsBean.selectedImportConnectionComponent.updateTimestamp}"						                        
					                        disabled="true">
					                <f:converter converterId="dateTime"/>        
					    </h:inputText>                    
					</td>
					<td>&#160;</td>
				</tr>			
			</tbody>			               
    	</table>  
    </div>
	</div>  
	
	<div id="table-four-bottom">
	<ul class="right">
		<li class="verify"><h:commandLink id="btnConnVerify" 
			rendered="#{importDefAndSpecsBean.doShowConnectionPropertiesSubForm and !(importDefAndSpecsBean.currentAction eq 'DeleteConn') and !(importDefAndSpecsBean.currentAction eq 'ViewConn')}" 
			action="#{importDefAndSpecsBean.verifyConnProperties}" />
		</li>

		<li class="ok"><h:commandLink id="t1ok" action="#{importDefAndSpecsBean.importConnOkAction}" 
					rendered="#{importDefAndSpecsBean.doShowConnectionPropertiesSubForm}"/>
		</li>
		
		<li class="cancel"><h:commandLink id="t1cancel" action="#{importDefAndSpecsBean.importConnCancelAction}" rendered="#{!(importDefAndSpecsBean.currentAction eq 'ViewConn')}" />
		</li>				
	</ul>	
	</div>				
</div>	
</h:form>

</ui:define>
</ui:composition>
</html>