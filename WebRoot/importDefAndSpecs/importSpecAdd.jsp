<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:a4j="http://richfaces.org/a4j">

<ui:composition template="/WEB-INF/view/templates/standard.xhtml">

<ui:define name="body">

<script type="text/javascript">
//<![CDATA[
function copyMe(from,to){
    document.getElementById(to).value = getDir(document.getElementById(from).value);
}
  function getDir(path) {
      dir = path;
      last = path.lastIndexOf('\\');
      if (last == -1) last = path.lastIndexOf('/');
      if (last == -1) return path;
      return path.substr(0,last);
  }
// function show_drives_list()
// {
// 	try
// 	{
// 		//list <select> element:
// 		var list = document.getElementById("selectDriveList");
// 		//clear list:
// 		list.options.length=0
	
// 		var fso = new ActiveXObject("Scripting.FileSystemObject");

// 		var enumDrives = new Enumerator(fso.Drives);

// 		for (; !enumDrives.atEnd(); enumDrives.moveNext())
// 		{
// 			var driveItem = enumDrives.item();
// 			var driveLetter = driveItem.DriveLetter;
// 			//alert (driveItem.DriveLetter);
// 			list.options[list.options.length]=new Option(driveLetter + ":", driveLetter+ ":", false, false);
// 		}
// 	}
// 	catch (_err)
// 	{
// 		alert ("Failed to Populate Drive List:\n" + _err.description );	
// 	}
// }

// function show_folders(recurseFolder)
// {
// 	try
// 	{
// 		//list  <select> element:
// 		var list = document.getElementById("selectFolderList");
// 		var dirList = document.getElementById("selectDriveList");
		
// 		var fso = new ActiveXObject("Scripting.FileSystemObject");
// 		var i;
// 		if (recurseFolder == "0") {
			
// 			for (i = 0; i < dirList.length; i++) {
// 				if (dirList[i].selected) {
// 					var parentFolder = fso.GetFolder(dirList[i].value + "/");
// 				} 
// 			}
			
// 		} else {
// 			for (i = 0; i < list.length; i++) {
// 				if (list[i].selected) {
// 					var parentFolder = fso.GetFolder(list[i].value + "/");
// 				} 
// 			}
// 		}
		
// 		var enumFolders = new Enumerator(parentFolder.SubFolders);

// 		//clear list:
// 		list.options.length=0

// 		for (; !enumFolders.atEnd(); enumFolders.moveNext())
// 		{
// 			var folderItem = enumFolders.item();
// 			list.options[list.options.length]=new Option(folderItem.Name, folderItem, false, false);
// 		}
// 		return true;

// 	}
// 	catch (_err)
// 	{
// 		alert ("Failed to Populate Folder List:\n" + _err.description );	
// 		return false;
// 	}
// }

// function populate_selected_folder()
// {
// 	try
// 	{
// 		//list  <select> element:
// 		var list = document.getElementById("selectFolderList");
// 		var textbox = document.getElementById("selectedFolder");
// 		var i;
		
// 		for (i = 0; i < list.length; i++) {
// 			if (list[i].selected) {
// 				textbox.value = list[i].value;
// 			} 
// 		}
		
// 		return true;

// 	}
// 	catch (_err)
// 	{
// 		alert ("Failed to Populate Folder Path:\n" + _err.description );	
// 		return false;
// 	}
// }

// function create_new_folder()
// {
// 	var newFolderName;
	
// 	newFolderName = inputbox();

// }
//]]>
</script>


<h:form id="form" >

<h1><h:graphicImage id="imgImportDefsSpecs" alt="Import Defs and Specs" value="/images/headers/hdr-import-defs-and-specs.gif"/></h1>
<div id="top">
	<div id="table-one">
	<div id="table-four-top">
		<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
	</div>
	<div id="table-one-content">
	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler">
		<thead>
			<tr><td colspan="6"><h:outputText value="#{importDefAndSpecsBean.actionText}"/></td></tr>
		</thead>
		<tbody><tr><th colspan="5">Spec</th></tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Spec Type:</td>
				<td style="width:700px;">
		           <h:selectOneMenu id="SelImprtSpecType"
                                title="Select Import Spec Type"
	           			        label="Import Spec Type"
	           			        rendered="true"
                      	        style="width: 388px;"
                      	        immediate="true" 
                      	        disabled="#{importDefAndSpecsBean.isAddDefaultAction or !importDefAndSpecsBean.addAction}"
                      	        value="#{importDefAndSpecsBean.selectedImportSpec.importSpecType}">
                    
                    <f:selectItems value="#{importDefAndSpecsBean.importSpecTypeItems}" />
                    
                    <a4j:support 
                        event="onchange"  
                        ajaxSingle="true" 
                        reRender="SelImprtDefdrp,msg" action="import_spec_add"/>
                      
    	         </h:selectOneMenu>				
        </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Spec Code:</td>
				<td>				
				<h:inputText id="ImprtSpecCode" 
                     value="#{importDefAndSpecsBean.selectedImportSpec.importSpecCode}" 
                     label="Import Spec Code"
                     validator="#{importDefAndSpecsBean.validateSpecCode}"	
					           onkeypress="upperCaseInputTextKey(event,this)" onblur="upperCaseInputText(this)" 
					           disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Description:</td>
				<td>
				<h:inputText id="desc" value="#{importDefAndSpecsBean.selectedImportSpec.description}" label="Description"
							disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
				         	style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Comments:</td>
				<td>
				<h:inputText id="comments" value="#{importDefAndSpecsBean.selectedImportSpec.comments}" 
					 	         disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Import Definition:</td>
				<td>
	           <h:selectOneMenu id="SelImprtDefdrp" title="Select Import Definition" style="width: 388px;" 
                             label="Import Definition"
					 	     disabled="#{importDefAndSpecsBean.isAddDefaultAction or importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
                      		 value="#{importDefAndSpecsBean.selectedImportSpec.importDefinitionCode}"
                      		 valueChangeListener="#{importDefAndSpecsBean.onImportDefinitionChange}"
                      		 onchange="submit();">
                      		  
                      		    
                    <f:selectItems value="#{importDefAndSpecsBean.importDefinitionCodeItems}" />
                    
               </h:selectOneMenu>	
                 </td>
			</tr>
<!-- 			<tr> -->
<!-- 				<td style="width:50px;">&#160;</td> -->
<!-- 				<td>Where Clause:</td> -->
<!-- 				<td> -->
<!-- 				<h:inputText id="whereClause" value="#{importDefAndSpecsBean.selectedImportSpec.whereClause}"  -->
<!-- 					 	         disabled="#{importDefAndSpecsBean.deleteAction or importDefAndSpecsBean.mapAction}" -->
<!-- 				         style="width:650px;" /> -->
<!--                 </td> -->
<!-- 			</tr> -->
		
	
		<h:panelGroup rendered="#{!importDefAndSpecsBean.mapAction}">
		<tr><th colspan="5">Options</th></tr>
		
		<h:panelGroup id = "dbpanel" rendered="#{importDefAndSpecsBean.selectedSpecFileTypeCode == 'DB'}">
		<tr><th colspan="5">Database Table Options</th></tr>
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Where Clause:</td>
			<td colspan="3">
			 	<h:inputText id="whereClause" value="#{importDefAndSpecsBean.selectedImportSpec.whereClause}" 
					disabled="#{importDefAndSpecsBean.readOnlyAction}" style="width:650px;" />
            </td>
		</tr>
		
		<tr>
			<td style="width:50px;">&#160;</td>
			<td>Verification:</td>
			<td colspan="3">
				<h:inputText id="ImportConnVerification" label="Verification"
		                        value="#{importDefAndSpecsBean.verificationErrorMessage}" style="width:650px;" disabled="true"/>
			</td>
		</tr>
			
		</h:panelGroup>
		
		<h:panelGroup id = "fdpanel1" rendered="#{importDefAndSpecsBean.selectedSpecFileTypeCode == 'FD'}">
		<tr><th colspan="5">File Delimited File Options</th></tr>
		<tr>
			<td style="width:50px;">&#160;</td>
			
			<td>Default Directory:</td>
			<td>
				<h:inputText id="defaultDirectoryForSpec" 
						disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
		    			value="#{importDefAndSpecsBean.selectedImportSpec.defaultDirectory}" style="width:650px;" >
               </h:inputText>	
               <a4j:commandButton  id="getDirec" 
               			disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.mapAction}"
					  	styleClass="image" image="/images/search_small.png" immediate="true"
					  	actionListener="#{directoryBrowserBackingBean.searchActionListener}"
					  	oncomplete="javascript:Richfaces.showModalPanel('panel');" reRender="panelForm"/>
			</td>
		</tr>
		<tr>
				<td style="width:50px;">&#160;</td>
				<td>File Size Duplicate Restriction:</td>
				<td>
				<h:selectOneRadio id="gridLayout1" disabled="#{importDefAndSpecsBean.readOnlyAction}" 
						value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.fileSizeDuplicateRestriction}" > 
                    <f:selectItem itemValue="0" itemLabel="Full"/>
                    <f:selectItem itemValue="1" itemLabel="Partial (Warning)"/>  
                    <f:selectItem itemValue="2" itemLabel="None"/>
                   
				</h:selectOneRadio>				
				</td>
				<td style="width:300px;">&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>File Name Duplicate Restriction:</td>
				<td>
				<h:selectOneRadio id="gridLayout2" disabled="#{importDefAndSpecsBean.readOnlyAction}"
						value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.fileNameDuplicateRestriction}" > 
                    <f:selectItem itemValue="0" itemLabel="Full"/>
                    <f:selectItem itemValue="1" itemLabel="Partial (Warning)"/>  
                    <f:selectItem itemValue="2" itemLabel="None"/> 
				</h:selectOneRadio>				
				</td>
				<td style="width:300px;">&#160;</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Default Import Date Map:</td>
				<td colspan="2">
			    <h:inputText id="defaultImportDateMap" disabled="#{importDefAndSpecsBean.readOnlyAction}"
			    		value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.defaultImportDateMap}" size="25" >   
                </h:inputText>							
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Do Not Import/Replace With Characters:</td>
				<td colspan="2">
			    	<h:inputText id="doNotImportMarkers"
				            value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.doNotImportMarkers}" 
                       		size="25" style="background-color: #F5F5F5;" disabled="true" tabindex="-1" > 
          			</h:inputText>&#160;&#160;
          			
          			<c:if test="#{!importDefAndSpecsBean.readOnlyAction}">
						<a4j:commandLink id="doNotImportBtn" immediate="true" value="Click to update"
							action="#{importDefAndSpecsBean.parseValuesToEditControls}"
							oncomplete="javascript:Richfaces.showModalPanel('doNotImportReplaceWithCharacters');" 
							reRender="doNotImportReplaceWithForm,msg"/>	
					</c:if>	
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>&#160;</td>
				<td colspan="2">
			    	<h:inputText id="replaceWithMarkers"
	                     	style="background-color: #F5F5F5;" disabled="true" tabindex="-1" 
				            value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.replaceWithMarkers}"  size="25" >    
          			</h:inputText>&#160;&#160;<h:message id="replaceWithErrorMessages" for="replaceWithMarkers" errorClass="error" />
				</td>
				<td>&#160;</td>
			</tr>
	        	<tr><th colspan="5">Batch End-of-File Options</th></tr>
	        	<tr>
				<td>&#160;</td>
				<td>End-of-File Marker:</td>
				<td colspan="2">
			    <h:inputText id="endOfFileMarker" disabled="#{importDefAndSpecsBean.readOnlyAction}" 
			    		value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.endOfFileMarker}" size="25" ></h:inputText>							
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Batch Count Marker:</td>
				<td colspan="2">
			    <h:inputText id="batchCountMarker" disabled="#{importDefAndSpecsBean.readOnlyAction}"
			    		value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.batchCountMarker}" size="25" ></h:inputText>						    
				</td>
				<td>&#160;</td>
			</tr>
			<tr>
				<td>&#160;</td>
				<td>Batch Total $ Marker:</td>
				<td colspan="2">
			    <h:inputText id="batchTotalMarker" disabled="#{importDefAndSpecsBean.readOnlyAction}"
			    		value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.batchTotalMarker}" size="25" ></h:inputText>			    
				</td>
				<td>&#160;</td>
			</tr>
			</h:panelGroup>
			<!--
			<h:panelGroup id = "dbfdpanel" rendered="#{importDefAndSpecsBean.selectedSpecFileTypeCode == 'DB' or importDefAndSpecsBean.selectedSpecFileTypeCode == 'FD'}">
			<tr><th colspan="5">Process Options</th></tr>
			<tr>
				<td>&#160;</td>
				<td>Delete Transactions with Amount=0?:</td>
				<td colspan="2">
                <h:selectBooleanCheckbox styleClass="check" id="deleteTransactionWithZeroAmountFlag" 
                			disabled="#{importDefAndSpecsBean.readOnlyAction}"
                         	value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.deleteTransactionWithZeroAmountFlag}">	 
                </h:selectBooleanCheckbox>				                
			                
				</td>
				<td>&#160;</td>
			</tr>
			<tr>	
				<td>&#160;</td>
				<td>Hold Transactions with Amount &gt; ?:</td>
				<td colspan="2">
                	<h:selectBooleanCheckbox styleClass="check" id="holdTransactionAmountFlag" 
                			disabled="#{importDefAndSpecsBean.readOnlyAction}"
                			valueChangeListener="#{importDefAndSpecsBean.vclTransactionAmount}"
                            value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.holdTransactionAmountFlag}">
                            <a4j:support id="a4j-holdTrWA" event="onclick" 
                        		ajaxSingle="true" reRender="holdTransactionAmount" />  
                	</h:selectBooleanCheckbox>						                
			    <h:inputText id="holdTransactionAmount" onkeypress="return onlyNumerics(event);"
			    	value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.holdTransactionAmount}" 
			    	disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.importMapProcessPreferenceDTO.holdTransactionAmountFlag==false}" size="25" >    
                </h:inputText>			                
				</td>
				<td>&#160;</td>
			</tr>
			<tr>				
				<td>&#160;</td>
				<td>Process Transaction with Amount &lt; ?:</td>
				<td colspan="3">
                <h:selectBooleanCheckbox styleClass="check" id="processTransactionAmountFlag" 
                			disabled="#{importDefAndSpecsBean.readOnlyAction}"
                			valueChangeListener="#{importDefAndSpecsBean.vclApplyTaxCode}"
                            value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.processTransactionAmountFlag}">	
                         <a4j:support event="onclick" reRender="processTransactionAmount,filterTaxCode_code" />  
                        </h:selectBooleanCheckbox>			                
			    <h:inputText id="processTransactionAmount" onkeypress="return onlyNumerics(event);"
			    		binding="#{importDefAndSpecsBean.processTransactionAmountInput}"
			    		value="#{importDefAndSpecsBean.importMapProcessPreferenceDTO.processTransactionAmount}" size="25" 
			    		disabled="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.importMapProcessPreferenceDTO.processTransactionAmountFlag==false}">     
                </h:inputText>
                      
                        <ui:include src="/WEB-INF/view/components/taxcode_code_combox.xhtml">                      	
							<ui:param name="handler" value="#{importDefAndSpecsBean.filterTaxCodeHandler}"/>
							<ui:param name="id" value="filterTaxCode"/>
							<ui:param name="readonly" value="#{importDefAndSpecsBean.readOnlyAction or importDefAndSpecsBean.importMapProcessPreferenceDTO.processTransactionAmountFlag==false}"/>
							<ui:param name="required" value="false"/>
							<ui:param name="allornothing" value="false"/>
							<ui:param name="showheaders" value="false"/>
							<ui:param name="forId" value="false"/>
							<ui:param name="popupName" value="searchTaxCode"/>
							<ui:param name="popupForm" value="searchTaxCodeForm"/>
						
						</ui:include>
						</td>
				<td>&#160;</td>
			</tr>
			</h:panelGroup>
			-->
			
			<h:panelGroup id = "fdpanel2" rendered="#{importDefAndSpecsBean.selectedSpecFileTypeCode == 'FD'}">
			<tr><th colspan="5">AID Options</th></tr>
			<tr>
				<td>&#160;</td>
				<td>Auto Process?:</td>
				<td colspan="2">
                	<h:selectBooleanCheckbox styleClass="check" id="autoprocessFlag" 
                			disabled="#{importDefAndSpecsBean.readOnlyAction}"
                         	value="#{importDefAndSpecsBean.selectedImportSpec.autoprocessBooleanFlag}">	 
                	</h:selectBooleanCheckbox>				                                
				</td>
				<td>&#160;</td>
			</tr>
			</h:panelGroup>
					
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update User ID:</td>
				<td>
				<h:inputText id="userId" value="#{importDefAndSpecsBean.selectedImportSpec.updateUserId}" 
				         disabled="true"
				         style="width:650px;" />
                </td>
			</tr>
			<tr>
				<td style="width:50px;">&#160;</td>
				<td>Last Update Timestamp:</td>
				<td>
				<h:inputText id="timestamp" value="#{importDefAndSpecsBean.selectedImportSpec.updateTimestamp}" 
				         disabled="true"
				         style="width:650px;">
					<f:converter converterId="dateTime"/>
				</h:inputText>
                </td>
			</tr>
			</h:panelGroup>
			</tbody>
	</table>

	<h:panelGroup rendered="#{importDefAndSpecsBean.mapAction}">
		<table cellpadding="0" cellspacing="0" width="100%" id="rollover2" class="ruler">
			<thead>
				<tr><td>Users</td></tr>
			</thead>
			<tbody>
	        <tr>
	          	<td><table><tr><td style="padding-left: 20px; width: 390px">Denied:</td>
	            <td style="width: 300px">xxGranted:</td></tr></table></td>
	        </tr>
			<tr><td>
				<rich:pickList id="usersPickList"
					 sourceListWidth="300"
					 targetListWidth="300"
	                       copyAllControlLabel="Grant All"
	                       copyControlLabel="Grant"
	                       removeAllControlLabel="Deny All"
	                       removeControlLabel="Deny"
					             value="#{importDefAndSpecsBean.userCodeList}">
					<f:selectItems  value="#{importDefAndSpecsBean.allUsersMenuItems}"/>
				</rich:pickList>
			</td></tr>
			</tbody>
		</table>
	</h:panelGroup>
	
	</div>
	<div id="table-four-bottom">
	<ul class="right">
		<li class="verify"><h:commandLink id="btnConnVerify" 
			rendered="#{(importDefAndSpecsBean.selectedSpecFileTypeCode == 'DB') and !(importDefAndSpecsBean.currentAction eq 'DeleteConn') and !(importDefAndSpecsBean.currentAction eq 'ViewConn')}" 
			action="#{importDefAndSpecsBean.verifyWhereClause}" />
		</li>
		<li class="ok"><h:commandLink id="t1ok" action="#{importDefAndSpecsBean.importSpecOkAction}" /></li>
		<li class="cancel"><h:commandLink id="t1cancel" disabled="#{importDefAndSpecsBean.currentAction eq 'ViewSpec'}" immediate="true" action="#{importDefAndSpecsBean.goToMainAction}" /></li>
	</ul>
	</div>
	</div>
</div>
</h:form>
	<ui:include src="/WEB-INF/view/components/networkDirectoryBrowser.xhtml">
					<ui:param name="handler" value="#{directoryBrowserBackingBean}"/>
					</ui:include>
<rich:modalPanel id="doNotImportReplaceWithCharacters" zindex="2000" autosized="true" >
	<f:facet name="header">
		<h:outputText value="Do Not Import/Replace With Characters" />
	</f:facet>
	
	<h:form id="doNotImportReplaceWithForm">
		<div align="left" style="padding-left: 10px;">
			<a4j:outputPanel id="msg"><h:messages errorClass="error" /></a4j:outputPanel>
		</div>
		<table>
			<tr>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td></td>
				<td>DNI</td>
				<td></td>
				<td>RW</td>
				<td></td>
				<td>DNI = Do Not Import</td>
			</tr>
			<tr>
				<td>1-</td>
				<td><h:inputText id="dni1" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni1}" maxlength="1" tabindex="1"/></td>
				<td>=</td>
				<td><h:inputText id="rw1" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw1}" maxlength="2" tabindex="2"/></td>
				<td></td>
				<td>11-</td>
				<td><h:inputText id="dni11" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni11}" maxlength="1" tabindex="21"/></td>
				<td>=</td>
				<td><h:inputText id="rw11" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw11}" maxlength="2" tabindex="22"/></td>
				<td></td>
				<td>21-</td>
				<td><h:inputText id="dni21" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni21}" maxlength="1" tabindex="41"/></td>
				<td>=</td>
				<td><h:inputText id="rw21" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw21}" maxlength="2" tabindex="42"/></td>
				<td></td>
				<td>31-</td>
				<td><h:inputText id="dni31" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni31}" maxlength="1" tabindex="61"/></td>
				<td>=</td>
				<td><h:inputText id="rw31" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw31}" maxlength="2" tabindex="62"/></td>
				<td></td>
				<td>41-</td>
				<td><h:inputText id="dni41" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni41}" maxlength="1" tabindex="81"/></td>
				<td>=</td>
				<td><h:inputText id="rw41" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw41}" maxlength="2" tabindex="82"/></td>
				<td></td>
				<td>RW = Replace With</td>
			</tr>
			<tr>
				<td>2-</td>
				<td><h:inputText id="dni2" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni2}" maxlength="1" tabindex="3"/></td>
				<td>=</td>
				<td><h:inputText id="rw2" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw2}" maxlength="2" tabindex="4"/></td>
				<td></td>
				<td>12-</td>
				<td><h:inputText id="dni12" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni12}" maxlength="1" tabindex="23"/></td>
				<td>=</td>
				<td><h:inputText id="rw12" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw12}" maxlength="2" tabindex="24"/></td>
				<td></td>
				<td>22-</td>
				<td><h:inputText id="dni22" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni22}" maxlength="1" tabindex="43"/></td>
				<td>=</td>
				<td><h:inputText id="rw22" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw22}" maxlength="2" tabindex="44"/></td>
				<td></td>
				<td>32-</td>
				<td><h:inputText id="dni32" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni32}" maxlength="1" tabindex="63"/></td>
				<td>=</td>
				<td><h:inputText id="rw32" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw32}" maxlength="2" tabindex="64"/></td>
				<td></td>
				<td>42-</td>
				<td><h:inputText id="dni42" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni42}" maxlength="1" tabindex="83"/></td>
				<td>=</td>
				<td><h:inputText id="rw42" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw42}" maxlength="2" tabindex="84"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>3-</td>
				<td><h:inputText id="dni3" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni3}" maxlength="1" tabindex="5"/></td>
				<td>=</td>
				<td><h:inputText id="rw3" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw3}" maxlength="2" tabindex="6"/></td>
				<td></td>
				<td>13-</td>
				<td><h:inputText id="dni13" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni13}" maxlength="1" tabindex="25"/></td>
				<td>=</td>
				<td><h:inputText id="rw13" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw13}" maxlength="2" tabindex="26"/></td>
				<td></td>
				<td>23-</td>
				<td><h:inputText id="dni23" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni23}" maxlength="1" tabindex="45"/></td>
				<td>=</td>
				<td><h:inputText id="rw23" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw23}" maxlength="2" tabindex="46"/></td>
				<td></td>
				<td>33-</td>
				<td><h:inputText id="dni33" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni33}" maxlength="1" tabindex="65"/></td>
				<td>=</td>
				<td><h:inputText id="rw33" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw33}" maxlength="2" tabindex="66"/></td>
				<td></td>
				<td>43-</td>
				<td><h:inputText id="dni43" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni43}" maxlength="1" tabindex="85"/></td>
				<td>=</td>
				<td><h:inputText id="rw43" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw43}" maxlength="2" tabindex="86"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>4-</td>
				<td><h:inputText id="dni4" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni4}" maxlength="1" tabindex="7"/></td>
				<td>=</td>
				<td><h:inputText id="rw4" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw4}" maxlength="2" tabindex="8"/></td>
				<td></td>
				<td>14-</td>
				<td><h:inputText id="dni14" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni14}" maxlength="1" tabindex="27"/></td>
				<td>=</td>
				<td><h:inputText id="rw14" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw14}" maxlength="2" tabindex="28"/></td>
				<td></td>
				<td>24-</td>
				<td><h:inputText id="dni24" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni24}" maxlength="1" tabindex="47"/></td>
				<td>=</td>
				<td><h:inputText id="rw24" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw24}" maxlength="2" tabindex="48"/></td>
				<td></td>
				<td>34-</td>
				<td><h:inputText id="dni34" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni34}" maxlength="1" tabindex="67"/></td>
				<td>=</td>
				<td><h:inputText id="rw34" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw34}" maxlength="2" tabindex="68"/></td>
				<td></td>
				<td>44-</td>
				<td><h:inputText id="dni44" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni44}" maxlength="1" tabindex="87"/></td>
				<td>=</td>
				<td><h:inputText id="rw44" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw44}" maxlength="2" tabindex="88"/></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>5-</td>
				<td><h:inputText id="dni5" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni5}" maxlength="1" tabindex="9"/></td>
				<td>=</td>
				<td><h:inputText id="rw5" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw5}" maxlength="2" tabindex="10"/></td>
				<td></td>
				<td>15-</td>
				<td><h:inputText id="dni15" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni15}" maxlength="1" tabindex="29"/></td>
				<td>=</td>
				<td><h:inputText id="rw15" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw15}" maxlength="2" tabindex="30"/></td>
				<td></td>
				<td>25-</td>
				<td><h:inputText id="dni25" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni25}" maxlength="1" tabindex="49"/></td>
				<td>=</td>
				<td><h:inputText id="rw25" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw25}" maxlength="2" tabindex="50"/></td>
				<td></td>
				<td>35-</td>
				<td><h:inputText id="dni35" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni35}" maxlength="1" tabindex="69"/></td>
				<td>=</td>
				<td><h:inputText id="rw35" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw35}" maxlength="2" tabindex="70"/></td>
				<td></td>
				<td>45-</td>
				<td><h:inputText id="dni45" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni45}" maxlength="1" tabindex="89"/></td>
				<td>=</td>
				<td><h:inputText id="rw45" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw45}" maxlength="2" tabindex="90"/></td>
				<td></td>
				<td>Legend</td>
			</tr>
			<tr>
				<td>6-</td>
				<td><h:inputText id="dni6" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni6}" maxlength="1" tabindex="11"/></td>
				<td>=</td>
				<td><h:inputText id="rw6" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw6}" maxlength="2" tabindex="12"/></td>
				<td></td>
				<td>16-</td>
				<td><h:inputText id="dni16" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni16}" maxlength="1" tabindex="31"/></td>
				<td>=</td>
				<td><h:inputText id="rw16" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw16}" maxlength="2" tabindex="32"/></td>
				<td></td>
				<td>26-</td>
				<td><h:inputText id="dni26" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni26}" maxlength="1" tabindex="51"/></td>
				<td>=</td>
				<td><h:inputText id="rw26" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw26}" maxlength="2" tabindex="52"/></td>
				<td></td>
				<td>36-</td>
				<td><h:inputText id="dni36" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni36}" maxlength="1" tabindex="71"/></td>
				<td>=</td>
				<td><h:inputText id="rw36" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw36}" maxlength="2" tabindex="72"/></td>
				<td></td>
				<td>46-</td>
				<td><h:inputText id="dni46" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni46}" maxlength="1" tabindex="91"/></td>
				<td>=</td>
				<td><h:inputText id="rw46" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw46}" maxlength="2" tabindex="92"/></td>
				<td></td>
				<td>~s = space</td>
			</tr>
			<tr>
				<td>7-</td>
				<td><h:inputText id="dni7" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni7}" maxlength="1" tabindex="13"/></td>
				<td>=</td>
				<td><h:inputText id="rw7" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw7}" maxlength="2" tabindex="14"/></td>
				<td></td>
				<td>17-</td>
				<td><h:inputText id="dni17" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni17}" maxlength="1" tabindex="33"/></td>
				<td>=</td>
				<td><h:inputText id="rw17" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw17}" maxlength="2" tabindex="34"/></td>
				<td></td>
				<td>27-</td>
				<td><h:inputText id="dni27" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni27}" maxlength="1" tabindex="53"/></td>
				<td>=</td>
				<td><h:inputText id="rw27" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw27}" maxlength="2" tabindex="54"/></td>
				<td></td>
				<td>37-</td>
				<td><h:inputText id="dni37" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni37}" maxlength="1" tabindex="73"/></td>
				<td>=</td>
				<td><h:inputText id="rw37" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw37}" maxlength="2" tabindex="74"/></td>
				<td></td>
				<td>47-</td>
				<td><h:inputText id="dni47" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni47}" maxlength="1" tabindex="93"/></td>
				<td>=</td>
				<td><h:inputText id="rw47" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw47}" maxlength="2" tabindex="94"/></td>
				<td></td>
				<td>~a = <img src="../images/preferences/currency_sign.GIF" /></td>
			</tr>
			<tr>
				<td>8-</td>
				<td><h:inputText id="dni8" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni8}" maxlength="1" tabindex="15"/></td>
				<td>=</td>
				<td><h:inputText id="rw8" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw8}" maxlength="2" tabindex="16"/></td>
				<td></td>
				<td>18-</td>
				<td><h:inputText id="dni18" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni18}" maxlength="1" tabindex="35"/></td>
				<td>=</td>
				<td><h:inputText id="rw18" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw18}" maxlength="2" tabindex="36"/></td>
				<td></td>
				<td>28-</td>
				<td><h:inputText id="dni28" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni28}" maxlength="1" tabindex="55"/></td>
				<td>=</td>
				<td><h:inputText id="rw28" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw28}" maxlength="2" tabindex="56"/></td>
				<td></td>
				<td>38-</td>
				<td><h:inputText id="dni38" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni38}" maxlength="1" tabindex="75"/></td>
				<td>=</td>
				<td><h:inputText id="rw38" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw38}" maxlength="2" tabindex="76"/></td>
				<td></td>
				<td>48-</td>
				<td><h:inputText id="dni48" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni48}" maxlength="1" tabindex="95"/></td>
				<td>=</td>
				<td><h:inputText id="rw48" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw48}" maxlength="2" tabindex="96"/></td>
				<td></td>
				<td>~b = <img src="../images/preferences/section_sign.GIF"/></td>
			</tr>
			<tr>
				<td>9-</td>
				<td><h:inputText id="dni9" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni9}" maxlength="1" tabindex="17"/></td>
				<td>=</td>
				<td><h:inputText id="rw9" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw9}" maxlength="2" tabindex="18"/></td>
				<td></td>
				<td>19-</td>
				<td><h:inputText id="dni19" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni19}" maxlength="1" tabindex="37"/></td>
				<td>=</td>
				<td><h:inputText id="rw19" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw19}" maxlength="2" tabindex="38"/></td>
				<td></td>
				<td>29-</td>
				<td><h:inputText id="dni29" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni29}" maxlength="1" tabindex="57"/></td>
				<td>=</td>
				<td><h:inputText id="rw29" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw29}" maxlength="2" tabindex="58"/></td>
				<td></td>
				<td>39-</td>
				<td><h:inputText id="dni39" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni39}" maxlength="1" tabindex="77"/></td>
				<td>=</td>
				<td><h:inputText id="rw39" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw39}" maxlength="2" tabindex="78"/></td>
				<td></td>
				<td>49-</td>
				<td><h:inputText id="dni49" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni49}" maxlength="1" tabindex="97"/></td>
				<td>=</td>
				<td><h:inputText id="rw49" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw49}" maxlength="2" tabindex="98"/></td>
				<td></td>
				<td>~c = <img src="../images/preferences/diamond_sign.GIF"/></td>
			</tr>
			<tr>
				<td>10-</td>
				<td><h:inputText id="dni10" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni10}" maxlength="1" tabindex="19"/></td>
				<td>=</td>
				<td><h:inputText id="rw10" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw10}" maxlength="2" tabindex="20"/></td>
				<td></td>
				<td>20-</td>
				<td><h:inputText id="dni20" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni20}" maxlength="1" tabindex="39"/></td>
				<td>=</td>
				<td><h:inputText id="rw20" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw20}" maxlength="2" tabindex="40"/></td>
				<td></td>
				<td>30-</td>
				<td><h:inputText id="dni30" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni30}" maxlength="1" tabindex="59"/></td>
				<td>=</td>
				<td><h:inputText id="rw30" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw30}" maxlength="2" tabindex="60"/></td>
				<td></td>
				<td>40-</td>
				<td><h:inputText id="dni40" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni40}" maxlength="1" tabindex="79"/></td>
				<td>=</td>
				<td><h:inputText id="rw40" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw40}" maxlength="2" tabindex="80"/></td>
				<td></td>
				<td>50-</td>
				<td><h:inputText id="dni50" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.dni50}" maxlength="1" tabindex="99"/></td>
				<td>=</td>
				<td><h:inputText id="rw50" size="2" value="#{importDefAndSpecsBean.doNotImportCharBean.rw50}" maxlength="2" tabindex="100"/></td>
				<td></td>
				<td>~d = <img src="../images/preferences/middle_point.GIF"/></td>
			</tr>
		</table>
		                    
		<a4j:commandButton id="btnOk" value="Ok" 
        	actionListener="#{importDefAndSpecsBean.moveEditControlToValuesListener}"          
           	oncomplete="if (#{importDefAndSpecsBean.controlToValuesSucceed}) {Richfaces.hideModalPanel('doNotImportReplaceWithCharacters');}"            
                       reRender="msg,doNotImportReplaceWithForm,doNotImportMarkers,replaceWithMarkers" >
		</a4j:commandButton>
  		<rich:spacer width="10px"/>
  		<a4j:commandButton id="btnCancel" immediate="true" type="reset"
			onclick="Richfaces.hideModalPanel('doNotImportReplaceWithCharacters');" value="Cancel">
		</a4j:commandButton>
		
	</h:form>
</rich:modalPanel>
<!-- <rich:modalPanel id="directoryBrowser" autosized="true" zindex="2000" onshow="show_drives_list();"> -->
<!--     <f:facet name="header"> -->
<!--         <h:outputText value="Directory Browser"/> -->
<!--     </f:facet> -->
<!--     <h:form id="frmDirectoryBrowser"> -->
<!-- 	    <table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler"> -->
<!-- 	    	<tr> -->
<!-- 	    		<td> -->
<!-- 	    			Selected Folder:  -->
<!-- 	    		</td> -->
<!-- 	    		<td> -->
<!-- 	    			<input type="text" id="selectedFolder"  -->
<!-- 	    				name="selectedFolder" readonly="true" size="50"/> -->
<!-- 	    		</td> -->
<!-- 	    	</tr> -->
<!-- 	    </table> -->
<!--     	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler"> -->
<!--     		<tr> -->
<!--     			<td> -->
<!--     				Drive: -->
<!--     			</td> -->
<!--     			<td> -->
<!--     				<select id="selectDriveList" name="selectDriveList" size="5" -->
<!--     					onclick="show_folders('0');">  -->
<!-- 					</select> -->
<!--     			</td> -->
<!--     			<td> -->
<!--     				Folder: -->
<!--     			</td> -->
<!--     			<td width="100%"> -->
<!--     				<select id="selectFolderList" name="selectFolderList" size="5" -->
<!--     					onclick="populate_selected_folder();"  -->
<!--     					ondblclick="show_folders('1');">  -->
<!-- 					</select> -->
<!--     			</td> -->
<!-- 			</tr> -->
<!--     	</table> -->
<!--     	<table cellpadding="0" cellspacing="0" width="100%" id="rollover" class="ruler"> -->
<!--     		<tr> -->
<!--     			<a4j:commandButton id="btnNewFolder" immediate="true" value="New Folder" -->
<!-- 					onclick="create_new_folder();"> -->
<!-- 				</a4j:commandButton> -->
<!--     			<a4j:commandButton id="btnOk" immediate="true" value="Ok" -->
<!-- 					onclick="copyMe('selectedFolder','form:defDirFile');Richfaces.hideModalPanel('directoryBrowser')"> -->
<!-- 				</a4j:commandButton> -->
<!--     			<a4j:commandButton id="btnCancel" immediate="true" type="reset" -->
<!-- 					onclick="Richfaces.hideModalPanel('directoryBrowser')" value="Cancel"> -->
<!-- 				</a4j:commandButton> -->
<!--     		</tr> -->
<!--     	</table> -->
<!-- 	</h:form> -->
<!-- </rich:modalPanel> -->

</ui:define>

</ui:composition>

</html>