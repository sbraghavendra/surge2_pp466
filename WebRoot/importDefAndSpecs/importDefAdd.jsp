<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:c="http://java.sun.com/jstl/core"
      xmlns:rich="http://richfaces.org/rich"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:a4j="http://richfaces.org/a4j">
 <ui:composition template="/WEB-INF/view/templates/standard.xhtml">
<ui:define name="script">
<script  type="text/javascript">
//<![CDATA[
function copyMe(from,to){
	var fromValue = document.getElementById(from).value;
	
	var lastIndex = fromValue.lastIndexOf("\\");
  	if(lastIndex > -1){	
    	document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    }
    else{
    	lastIndex = fromValue.lastIndexOf("//");
    	if(lastIndex > -1){	
    		document.getElementById(to).value = fromValue.substring(lastIndex + 1);
    	}
    	else{
    		document.getElementById(to).value = fromValue;
    	}
    }
}
//]]>
</script>
</ui:define>
<ui:define name="body">
   <h:form id="importDefAddForm" enctype="multipart/form-data">
    <h1>
     <h:graphicImage id="imgImportDefsSpecs" alt="Import Defs and Specs" value="/images/headers/hdr-import-defs-and-specs.gif" />
    </h1>
    <div id="top">
     <div id="table-one">
      <div id="table-four-top">
       <a4j:outputPanel id="msg">
        <h:messages errorClass="error" />
       </a4j:outputPanel>
      </div>
      <div id="table-one-content"
           style="height:418px;">
       <table cellpadding="0"
              cellspacing="0"
              width="100%"
              id="rollover"
              class="ruler">
        <thead>
         <tr>
          <td colspan="6">
           <h:outputText value="#{importDefAndSpecsBean.actionText}" />
          </td>
         </tr>
        </thead>
        <tbody>
		<tr><th class="DefinitionPanel" colspan="2">Definition</th>
		 <th style="color:red;text-align:right;padding-right: 60px;"><h:outputText value="Copy Map?:" rendered="#{importDefAndSpecsBean.currentAction =='copyAddDef'}"></h:outputText>
				<h:selectBooleanCheckbox id="chkCopyMap" value="#{importDefAndSpecsBean.selectedImportDef.copyMapFlag}"
					 rendered="#{importDefAndSpecsBean.currentAction == 'copyAddDef'}" styleClass="check"/>
        </th></tr>
         
         <tr>
          <td>&#160;</td>
          <td class="column-label3">Definition Type:</td>
          <td class="column-input2">
           <h:selectOneMenu id="ImportDefType"
                        required="true"
                        label="Import Definition Type"
                        value="#{importDefAndSpecsBean.selectedImportDef.importDefinitionType}"
                        disabled="#{!importDefAndSpecsBean.addAction}" 
                        style="column-input2"
                        >
                <f:selectItems id="ddlImportDefTypeItms" value="#{importDefAndSpecsBean.importDefTypeItems}" />
           </h:selectOneMenu>
          </td>
         </tr>

         <tr>
          <td>&#160;</td>
          <td class="column-label3">Definition Code:</td>
          <td class="column-input2">
           <h:inputText id="ImportDefCode"
                        label="Import Definition Code"
                        value="#{importDefAndSpecsBean.selectedImportDef.importDefinitionCode}"
                        disabled="#{importDefAndSpecsBean.updateAction or importDefAndSpecsBean.readOnlyAction}"
                        style="column-input2"
					    onkeypress="upperCaseInputTextKey(event,this)" 
						onblur="upperCaseInputText(this)" />
          </td>
         </tr>

         <tr>
          <td style="width:50px;">&#160;</td>
          <td>Description:</td>
          <td style="width:700px;">
           <h:inputText id="ImportDefDescription"
                        label="Import Definition Description"
                        value="#{importDefAndSpecsBean.selectedImportDef.description}"
                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
                        style="width:650px;" />
          </td>
         </tr>
         <tr>
          <td style="width:50px;">&#160;</td>
          <td>Comments:</td>
          <td>
           <h:inputTextarea id="Comments"
                            disabled="#{importDefAndSpecsBean.readOnlyAction}"
                            value="#{importDefAndSpecsBean.selectedImportDef.comments}"
                            style="width:650px;" />
          </td>
         </tr>
         
         
         <tr>
          <td>&#160;</td>
          <td class="column-label3">Import File Type:</td>
          <td class="column-input2">
           <h:selectOneMenu id="ImportFileType"
                        value="#{importDefAndSpecsBean.selectedImportDef.importFileTypeCode}"
                        disabled="#{!importDefAndSpecsBean.addAction or importDefAndSpecsBean.displayUpdateDefUsingConn or !empty importDefAndSpecsBean.indexImportConnectionComponent or importDefAndSpecsBean.readOnlyAction}"    
                        valueChangeListener="#{importDefAndSpecsBean.onImportFileTypeChange}"
						onchange="submit();"      
                        style="column-input2" >
                <f:selectItems id="ddlImportFileTypeItms" value="#{importDefAndSpecsBean.importFileTypeItems}" />
           </h:selectOneMenu>
          </td>
         </tr>
         
        <tr><th class="ParametersPanel" colspan="4">Import File Parameters</th></tr>
         
        <c:if test="#{importDefAndSpecsBean.isDatabaseType}"> 
	       	<tr>
	          	<td>&#160;</td>
	          	<td class="column-label3">Connection:</td>
	          	<td class="column-input2">
	           		<h:selectOneMenu id="ConnectionType"
	                        value="#{importDefAndSpecsBean.selectedImportDef.importConnectionCode}"
	                        disabled="#{importDefAndSpecsBean.isAddDefaultAction or !importDefAndSpecsBean.addAction or importDefAndSpecsBean.displayUpdateDefUsingConn or !empty importDefAndSpecsBean.indexImportConnectionComponent or importDefAndSpecsBean.readOnlyAction}" 
	                        valueChangeListener="#{importDefAndSpecsBean.onConnectionChange}"
							onchange="submit();"
	                        style="column-input2" >
	                	<f:selectItems id="ConnectionTypeItms" value="#{importDefAndSpecsBean.importConnectionItems}" />
	           		</h:selectOneMenu>
	          	</td>
	         </tr>
         
	         <tr>
	          	<td>&#160;</td>
	          	<td class="column-label3">Table Name:</td>
	          	<td class="column-input2">
	           		<h:selectOneMenu id="TableNameType"
	                        label="Table Name Type"
	                        value="#{importDefAndSpecsBean.selectedImportDef.dbTableName}"
	                        disabled="#{!importDefAndSpecsBean.addAction or importDefAndSpecsBean.readOnlyAction}" 
	                        style="column-input2" >
	                	<f:selectItems id="TableNameTypeItms" value="#{importDefAndSpecsBean.importTableItems}" />
	           		</h:selectOneMenu>
          		</td>
         	</tr>
         </c:if>
         
         <c:if test="#{importDefAndSpecsBean.isFlatDelimitedType}"> 
         	<tr>
	          	<td style="width:50px;">&#160;</td>
	          	<td class="column-label3">Character Encoding:</td>
	          	<td class="column-input2">
	           		<h:selectOneMenu id="encoding"
	                        value="#{importDefAndSpecsBean.selectedImportDef.encoding}"
	                        disabled="#{importDefAndSpecsBean.readOnlyAction}"      
	                        style="column-input2" >
	                <f:selectItems id="ddlEncodingItms" value="#{importDefAndSpecsBean.encodingItems}" />
	           		</h:selectOneMenu>
	           	</td>
	        </tr>
	     	<tr>
	        	<td style="width:50px;">&#160;</td>
				<td>Delimiter Character:</td>
				<td>
					<table class="embedded-table" style="width: 100%">
						<tr>
							<td style="border-bottom: none;">
							<h:selectOneRadio id="gridLayout" layout="pageDirection" disabled="#{importDefAndSpecsBean.readOnlyAction}" value="#{importDefAndSpecsBean.flatDelCharSelected}"> 
			                    <f:selectItem itemValue="~t" itemLabel="&#160;Tab"/>
			                    <f:selectItem itemValue="~n" itemLabel="&#160;New Line"/>  
			                    <f:selectItem itemValue="~f" itemLabel="&#160;Form Feed"/>
			                    <f:selectItem itemValue="~b" itemLabel="&#160;Backspace"/>
			                    <f:selectItem itemValue="�" itemLabel="&#160;&#167;"/>
			                    <f:selectItem itemValue="OTHER" itemLabel="&#160;Other:"/>
							</h:selectOneRadio>
							</td>
							<td valign="bottom" style="border-bottom: none;">
								<h:inputText id="DelimCharOther" 
			                        maxlength="2"
			                        size="2"
			                        label="Delimiter Character"
			                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
			                        value="#{importDefAndSpecsBean.flatDelCharEntered}"/>
							</td>
							<td style="width:100%;"></td>
						 </tr>
					 </table>
				 </td>		
			 </tr>
	
	         <tr>
	          	<td style="width:50px;">&#160;</td>
	          	<td>Text Qualifier:</td>
	          	<td>
	           		<h:inputText id="TextQual" 
	                        maxlength="1"
	                        size="1"
	                        label="Text Qualifier"
	                        disabled="#{importDefAndSpecsBean.readOnlyAction}"
	                        value="#{importDefAndSpecsBean.selectedImportDef.flatDelTextQual}"/>
	          	</td>
	         </tr>
	         <tr>
	          	<td style="width:50px;">&#160;</td>
	          	<td>First Line Headers?:</td>
	          	<td>
	           		<h:selectBooleanCheckbox styleClass="check" id="chkLine1HeadFlag"
	                                    disabled="#{importDefAndSpecsBean.readOnlyAction}"
	                                    value="#{importDefAndSpecsBean.selectedImportDef.flatDelLine1HeadFlagUI}" />
	          	</td>
	         </tr>
	         <tr>
	          	<td style="width:50px;">&#160;</td>
	          	<td>Sample File Name:</td>
	          	<td>
	            	<h:inputText id="sampleFileName"
		                readonly="true"
		                label="Sample File Name"
		                disabled="#{importDefAndSpecsBean.readOnlyAction}"
		                value="#{importDefAndSpecsBean.sampleFileName}"
		                required="true"
		                size="70"
		                style="vertical-align:middle"/>
		        	<rich:spacer width="5px" />
			        <t:inputFileUpload id="uploadFileControl"
			            binding="#{importDefAndSpecsBean.uploadFile}"
		                storage="file"
		                size="70"
		                disabled="#{importDefAndSpecsBean.readOnlyAction}"
		  				style="width:68px;vertical-align:middle">

		                <a4j:support event="onchange" 
		                	ajaxSingle="true"                           
		         			immediate="true"    
		         			oncomplete="copyMe('importDefAddForm:uploadFileControl','importDefAddForm:sampleFileName')"
		                    reRender="sampleFileName" />
	            	</t:inputFileUpload>
	          	</td>
	         </tr>
         </c:if>
         
         <tr>
          <td style="width:50px;">&#160;</td>
          <td>Last Update User ID:</td>
          <td>
           <h:inputText id="LastUpdateUserID"
                        disabled="true"
                        value="#{importDefAndSpecsBean.selectedImportDef.updateUserId}"
                        style="width:650px;" />
          </td>
         </tr>
         <tr>
          <td style="width:50px;">&#160;</td>
          <td>Last Update Timestamp:</td>
          <td>
           <h:inputText id="LastUpdateTimestamp"
                        disabled="true"
                        value="#{importDefAndSpecsBean.selectedImportDef.updateTimestamp}"
                        style="width:650px;">
                <f:converter converterId="dateTime"/>
			</h:inputText>
          </td>
         </tr>
        </tbody>
       </table>
      </div>
      <div id="table-four-bottom">
       <ul class="right">
        <li class="ok">
         <h:commandLink id="t1ok"
                        action="#{importDefAndSpecsBean.importDefOkAction}" />
        </li>
        <li class="cancel">
         <a4j:commandLink id="t1cancel" rendered="#{!(importDefAndSpecsBean.currentAction eq 'ViewDef')}"
                        immediate="true"
                        action="#{importDefAndSpecsBean.goToMainAction}" />
        </li>
       </ul>
      </div>
     </div>
    </div>
   </h:form>
  </ui:define>
 </ui:composition>
</html>
