<ui:component 
	xmlns:ui="http://java.sun.com/jsf/facelets">
	
            <!-- MENU STARTS HERE -->
            <div class="NCSTSContainerMenu"> 
                 <script type='text/javascript' src='../scripts/NCSTSMainMenu.js'> </script>
                 <table border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td style="width: 30px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','file','../images/menus/NCSTSMainMenuFile-Over.jpg','../images/menus/NCSTSMainMenuFile-Out.jpg',1);" onmouseout="procMenuItem('out','file','../images/menus/NCSTSMainMenuFile-Over.jpg','../images/menus/NCSTSMainMenuFile-Out.jpg',1);" href="#"><img name="file" src="../images/menus/NCSTSMainMenuFile-Out.jpg" alt="Tile"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','transactions','../images/menus/NCSTSMainMenuTransactions-Over.jpg','../images/menus/NCSTSMainMenuTransactions-Out.jpg',1);" onmouseout="procMenuItem('out','transactions','../images/menus/NCSTSMainMenuTransactions-Over.jpg','../images/menus/NCSTSMainMenuTransactions-Out.jpg',1);" href="#"><img name="transactions" src="../images/menus/NCSTSMainMenuTransactions-Out.jpg" alt="Transactions"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','datautility','../images/menus/NCSTSMainMenuDataUtility-Over.jpg','../images/menus/NCSTSMainMenuDataUtility-Out.jpg',1);" onmouseout="procMenuItem('out','datautility','../images/menus/NCSTSMainMenuDataUtility-Over.jpg','../images/menus/NCSTSMainMenuDataUtility-Out.jpg',1);" href="#"><img name="datautility" src="../images/menus/NCSTSMainMenuDataUtility-Out.jpg" alt="Data Utility"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','maintenance','../images/menus/NCSTSMainMenuMaintenance-Over.jpg','../images/menus/NCSTSMainMenuMaintenance-Out.jpg',1); procMenu('over');" onmouseout="procMenuItem('out','maintenance','../images/menus/NCSTSMainMenuMaintenance-Over.jpg','../images/menus/NCSTSMainMenuMaintenance-Out.jpg',1);  procMenu('out');" href="#"><img name="maintenance" src="../images/menus/NCSTSMainMenuMaintenance-Out.jpg" alt="Maintenance"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','security','../images/menus/NCSTSMainMenuSecurity-Over.jpg','../images/menus/NCSTSMainMenuSecurity-Out.jpg',1);" onmouseout="procMenuItem('out','security','../images/menus/NCSTSMainMenuSecurity-Over.jpg','../images/menus/NCSTSMainMenuSecurity-Out.jpg',1);" href="#"><img name="security" src="../images/menus/NCSTSMainMenuSecurity-Out.jpg" alt="Security"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','utilities','../images/menus/NCSTSMainMenuUtilities-Over.jpg','../images/menus/NCSTSMainMenuUtilities-Out.jpg',1);" onmouseout="procMenuItem('out','utilities','../images/menus/NCSTSMainMenuUtilities-Over.jpg','../images/menus/NCSTSMainMenuUtilities-Out.jpg',1);" href="#"><img name="utilities" src="../images/menus/NCSTSMainMenuUtilities-Out.jpg" alt="Utilities"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','setup','../images/menus/NCSTSMainMenuSetup-Over.jpg','../images/menus/NCSTSMainMenuSetup-Out.jpg',1);" onmouseout="procMenuItem('out','setup','../images/menus/NCSTSMainMenuSetup-Over.jpg','../images/menus/NCSTSMainMenuSetup-Out.jpg',1);" href="#"><img name="setup" src="../images/menus/NCSTSMainMenuSetup-Out.jpg" alt="Setup"/></a></td>
                        <td style="width: 50px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','help','../images/menus/NCSTSMainMenuHelp-Over.jpg','../images/menus/NCSTSMainMenuHelp-Out.jpg',1);" onmouseout="procMenuItem('out','help','../images/menus/NCSTSMainMenuHelp-Over.jpg','../images/menus/NCSTSMainMenuHelp-Out.jpg',1);" href="#"><img name="help" src="../images/menus/NCSTSMainMenuHelp-Out.jpg" alt="Help"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','reports','../images/menus/NCSTSMainMenuReports-Over.jpg','../images/menus/NCSTSMainMenuReports-Out.jpg',1);" onmouseout="procMenuItem('out','Reports','../images/menus/NCSTSMainMenuReports-Over.jpg','../images/menus/NCSTSMainMenuReports-Out.jpg',1);" href="#"><img name="reports" src="../images/menus/NCSTSMainMenuReports-Out.jpg" alt="Reports"/></a></td>
                        <td style="width: 20px;">&#160;</td>
                        <td ><a onmouseover="procMenuItem('over','logout','../images/menus/NCSTSMainMenuLogout-Over.jpg','../images/menus/NCSTSMainMenuLogout-Out.jpg',1);" onmouseout="procMenuItem('out','logout','../images/menus/NCSTSMainMenuLogout-Over.jpg','../images/menus/NCSTSMainMenuLogout-Out.jpg',1);" href="/ncsts/NCSTSLogin.jsf"><img name="logout" src="../images/menus/NCSTSMainMenuLogout-Out.jpg" alt="Logout"/></a></td>

                    </tr>
                 </table>
            </div>  
               
            <div id="maintenancedropdown" class="NCSTSMenuDropDown" onmouseover="procMenu('over');" onmouseout="procMenu('out');">
                 <table border="0" cellspacing="0" >
                     <tr><td id="taxability" onmouseover="procMenuChoice('over', 'taxability');" onmouseout="procMenuChoice('out', 'taxability');"><a href="/ncsts/tax/taxMatrix_1.jsf"><span class="NCSTSText" >Taxability Matrix</span></a></td></tr>
                     <tr><td id="location" onmouseover="procMenuChoice('over', 'location');" onmouseout="procMenuChoice('out', 'location');"><a href="/ncsts/locationMatrix/locationMatrix.jsf"><span class="NCSTSText" >Location Matrix</span></a></td></tr>
                     <tr><td id="allocation" onmouseover="procMenuChoice('over', 'allocation');" onmouseout="procMenuChoice('out', 'allocation');"><a href="/ncsts/allocationMatrix/allocationMatrix.jsf"><span class="NCSTSText" >Allocation Matrix</span></a></td></tr>
                     <tr><td id="taxcodes" onmouseover="procMenuChoice('over', 'taxcodes');" onmouseout="procMenuChoice('out', 'taxcodes');"><a href="/ncsts/taxabilityCode/taxabilityCodesByState.jsf"><span class="NCSTSText" >Taxability Codes</span></a></td></tr>
                     <tr><td id="taxrates" onmouseover="procMenuChoice('over', 'taxrates');" onmouseout="procMenuChoice('out', 'taxrates');"><a href="/ncsts/taxJurisdiction/jurisdictionTaxRate.jsf"><span class="NCSTSText" >Taxability Rates</span></a></td></tr>
                 </table>
             </div>        
            <!-- MENU ENDS HERE -->

</ui:component>