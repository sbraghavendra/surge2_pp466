package com.ncsts.web;

import javax.servlet.ServletContext;

import org.springframework.web.context.ConfigurableWebApplicationContext;

public class ContextLoader extends
		org.springframework.web.context.ContextLoader {

	private static final String ERROR_CONFIG_LOC_PARAM = "errorConfigLocation";
	
	@Override
	protected void customizeContext(ServletContext servletContext,
			ConfigurableWebApplicationContext applicationContext) {
		super.customizeContext(servletContext, applicationContext);
		
		Object error = servletContext.getAttribute(ContextLoaderListener.ERROR_KEY);
		if(error != null) {
			String contextLocation = servletContext.getInitParameter(ERROR_CONFIG_LOC_PARAM);
			if(contextLocation != null) {
				String[] configLocs = new String[]{contextLocation};
				applicationContext.setConfigLocations(configLocs);
			}
		}		
	}
}
