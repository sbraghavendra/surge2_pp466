package com.ncsts.web;

import javax.servlet.ServletContextEvent;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

import com.ncsts.view.bean.RoutingDataSource;

public class ContextLoaderListener extends
		org.springframework.web.context.ContextLoaderListener {

	public static final String ERROR_KEY = "context-init-error";
	private static final String DB_CHECK_CONFIG_LOC_PARAM = "dbCheckConfigLocation";
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		ClassPathXmlApplicationContext ctx = null;
		String dbName = null;
		
		try {
			String contextLocation = event.getServletContext().getInitParameter(DB_CHECK_CONFIG_LOC_PARAM);
			if(contextLocation != null) {
				boolean skip = false;
				String dbProperties = System.getProperty("db.properties");
				if(!StringUtils.hasText(dbProperties)) {
					dbProperties = event.getServletContext().getInitParameter("db.properties");
					if(StringUtils.hasText(dbProperties)) {
						System.setProperty("db.properties", dbProperties);
					}
					else {
						skip = true;
					}
				}
				
				if(!skip) {
					ctx = new ClassPathXmlApplicationContext(contextLocation);
					RoutingDataSource dataSource = ctx.getBean("dataSource", RoutingDataSource.class);
					dbName = dataSource.getDefaultDatabaseName();
					dataSource.getConnection();
					((AbstractApplicationContext) ctx).close();
				}
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			event.getServletContext().setAttribute(ERROR_KEY, e.getMessage() + "[" + dbName + "]");
			if(ctx != null) {
				try {
					((AbstractApplicationContext) ctx).close();
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
		super.contextInitialized(event);
	}

	@Override
	protected ContextLoader createContextLoader() {
		return new ContextLoader();
	}
}
