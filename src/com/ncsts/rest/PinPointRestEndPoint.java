package com.ncsts.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.services.impl.DiagnosticService;
import com.ncsts.ws.message.BatchProcessRequest;
import com.ncsts.ws.message.BatchProcessResponse;
import com.ncsts.ws.message.BatchStatusRequest;
import com.ncsts.ws.message.BatchStatusResponse;
import com.ncsts.ws.message.BillTransactionProcessRequest;
import com.ncsts.ws.message.BillTransactionProcessResponse;
import com.ncsts.ws.message.EchoRequest;
import com.ncsts.ws.message.EchoResponse;
import com.ncsts.ws.message.JurisdictionRequest;
import com.ncsts.ws.message.JurisdictionResponse;
import com.ncsts.ws.message.MicroApiTransactionProcessRequest;
import com.ncsts.ws.message.MicroApiTransactionProcessResponse;
import com.ncsts.ws.message.PingRequest;
import com.ncsts.ws.message.PingResponse;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualRequest;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualResponse;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationRequest;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedResponse;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceRequest;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceResponse;
import com.ncsts.ws.message.PurchaseTransactionPushProrateRequest;
import com.ncsts.ws.message.PurchaseTransactionPushProrateResponse;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificRequest;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificResponse;
import com.ncsts.ws.message.PurchaseTransactionReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionSuspendResponse;
import com.ncsts.ws.message.RealTimeTransactionProcessRequest;
import com.ncsts.ws.message.RealTimeTransactionProcessResponse;
import com.ncsts.ws.message.SaleTransactionDocumentProcessRequest;
import com.ncsts.ws.message.SaleTransactionDocumentProcessResponse;
import com.ncsts.ws.message.SaleTransactionProcessRequest;
import com.ncsts.ws.message.StatusResponse;
import com.ncsts.ws.service.PinPointService;

@Controller
@RequestMapping(value = "/")
public class PinPointRestEndPoint {
	@Autowired
	private PinPointService service;

	@Autowired
	private DiagnosticService diagnosticService;
	
	@RequestMapping(value = "/ping", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public PingResponse ping(PingRequest request) {
		PingResponse response = new PingResponse();
		response.setResponse("Alive");
		
		return response;
	}
	
	@RequestMapping(value = "/echo", method = RequestMethod.POST)
	@ResponseBody
	public EchoResponse getEcho(@RequestBody EchoRequest request) {
		EchoResponse response = new EchoResponse();
		response.setResponse(request.getRequest());
		
		return response;
	}
	
	@RequestMapping(value = "/echo/{request:.*}", method = RequestMethod.GET)
	@ResponseBody
	public String getEchoString(@PathVariable("request") String request) {
		return request;
	}

	@RequestMapping(value = "/status", method = RequestMethod.POST)
	public @ResponseBody StatusResponse getStatus() {
		StatusResponse statusResponse = diagnosticService.getCurrentStatus();
		return statusResponse;
	}

	@RequestMapping(value = "/batchStatus", method = RequestMethod.POST)
	@ResponseBody
	public BatchStatusResponse getBatchStatus(@RequestBody BatchStatusRequest request) {
		return service.getBatchStatus(request);
	}
	
	@RequestMapping(value = "/realTimeTransactionProcess", method = RequestMethod.POST)
	@ResponseBody
	public RealTimeTransactionProcessResponse getBatchStatus(@RequestBody RealTimeTransactionProcessRequest request) {
		return service.getRealTimeTransactionProcess(request);
	}
	
	@RequestMapping(value = "/saleTransactionProcess", method = RequestMethod.POST)
	@ResponseBody
	public SaleTransactionDocumentProcessResponse getSaleTransactionProcess(@RequestBody SaleTransactionProcessRequest request) {
		return service.getSaleTransactionProcess(request);
	}
	
	@RequestMapping(value = "/saleTransactionDocumentProcess", method = RequestMethod.POST)
	@ResponseBody
	public SaleTransactionDocumentProcessResponse getSaleTransactionDocumentProcess(@RequestBody SaleTransactionDocumentProcessRequest request) {
		return service.getSaleTransactionDocumentProcess(request);
	}

	@RequestMapping(value = "/purchaseTransactionProcess", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getPurchaseTransactionProcess(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getPurchaseTransactionProcess(request);
	}

	
	@RequestMapping(value = "/purchaseTransactionTaxEstimate", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getPurchaseTransactionTaxEstimate(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getPurchaseTransactionTaxEstimate(request);
	}

	@RequestMapping(value = "/transEntity", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransEntity(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransEntity(request);
	}
	
	@RequestMapping(value = "/transEntityNexus", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransEntityNexus(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransEntityNexus(request);
	}	

	@RequestMapping(value = "/transLocation", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransLocation(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransLocation(request);
	}
	
	@RequestMapping(value = "/transGoodSvc", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransGoodSvc(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransGoodSvc(request);
	}
	
	@RequestMapping(value = "/transTaxCodeDtl", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransTaxCodeDtl(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransTaxCodeDtl(request);
	}

    @RequestMapping(value = "/transTaxRates", method = RequestMethod.POST)
    @ResponseBody
    public MicroApiTransactionProcessResponse getTransTaxRates(@RequestBody MicroApiTransactionProcessRequest request) {
        return service.getTransTaxRates(request);
    }

    /*
	@RequestMapping(value = "/basicLocation", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionGetLocationResponse getBasicLocation(@RequestBody PurchaseTransactionGetLocationRequest request) {
		return service.getBasicLocation(request);
	}
	*/
    
	@RequestMapping(value = "/transCalcAllTaxes", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransCalcAllTaxes(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransCalcAllTaxes(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionInvoiceVerification", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionInvoiceVerificationResponse getPurchaseTransactionInvoiceVerification(@RequestBody PurchaseTransactionInvoiceVerificationRequest request) {
		return service.getPurchaseTransactionInvoiceVerification(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionReverse", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionReverseResponse getPurchaseTransactionReverse(@RequestBody PurchaseTransactionReverseRequest request) {
		return service.getPurchaseTransactionReverse(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionLoadAndReverse", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionLoadAndReverseResponse getPurchaseTransactionLoadAndReverse(@RequestBody PurchaseTransactionLoadAndReverseRequest request) {
		return service.getPurchaseTransactionLoadAndReverse(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionEvaluateForAccrual", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionEvaluateForAccrualResponse getPurchaseTransactionEvaluateForAccrual(@RequestBody PurchaseTransactionEvaluateForAccrualRequest request) {
		return service.getPurchaseTransactionEvaluateForAccrual(request);
	}
	
	@RequestMapping(value = "/billTransactionProcess", method = RequestMethod.POST)
	@ResponseBody
	public BillTransactionProcessResponse getBillTransactionProcess(@RequestBody BillTransactionProcessRequest request, LogSourceEnum callingFromMethod) {
		return service.getBillTransactionProcess(request, callingFromMethod);
	}
	
	@RequestMapping(value = "/billTransactionProcess", method = RequestMethod.POST)
	@ResponseBody
	public BillTransactionProcessResponse getBillTransactionProcess(@RequestBody BillTransactionProcessRequest request) {
		return service.getBillTransactionProcess(request,LogSourceEnum.BillTransactionProcess);
	}

	@RequestMapping(value = "/transStatus", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransStatus(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransStatus(request);
	}
	
	@RequestMapping(value = "/transFullExemption", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransFullExemption(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransFullExemption(request);
	}
	
	@RequestMapping(value = "/transSuspend", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getTransSuspend(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getTransSuspend(request);
	}
	
	@RequestMapping(value = "/transReprocess", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getTransReprocess(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getTransReprocess(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionProcessBatch", method = RequestMethod.POST)
	@ResponseBody
	public BatchProcessResponse getPurchaseTransactionProcessBatch(@RequestBody BatchProcessRequest request) {
		return service.getPurchaseTransactionProcessBatch(request);
	}
	
	@RequestMapping(value = "/transVariance", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getTransVariance(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getTransVariance(request);
	}
	
	@RequestMapping(value = "/transRelease", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getTransRelease(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getTransRelease(request);
	}
	
	@RequestMapping(value = "/transReverse", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessResponse getTransReverse(@RequestBody PurchaseTransactionProcessRequest request) {
		return service.getTransReverse(request);
	}
	
	@RequestMapping(value = "/transVendorNexus", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransVendorNexus(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransVendorNexus(request);
	}

	@RequestMapping(value = "/transSitus", method = RequestMethod.POST)
	@ResponseBody
	public MicroApiTransactionProcessResponse getTransSitus(@RequestBody MicroApiTransactionProcessRequest request) {
		return service.getTransSitus(request);
	}


	@RequestMapping(value = "/jurisdiction", method = RequestMethod.POST)
	@ResponseBody
	public JurisdictionResponse getJurisdiction(@RequestBody JurisdictionRequest request) {
		return service.getJurisdiction(request);
	}

	@RequestMapping(value = "/purchaseTransactionPushProrate", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionPushProrateResponse getPurchaseTransactionPushProrate(@RequestBody PurchaseTransactionPushProrateRequest request) {
		return service.getPurchaseTransactionPushProrate(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionPushPreference", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionPushPreferenceResponse getPurchaseTransactionPushPreference(@RequestBody PurchaseTransactionPushPreferenceRequest request) {
		return service.getPurchaseTransactionPushPreference(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionPushSpecific", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionPushSpecificResponse getPurchaseTransactionPushSpecific(@RequestBody PurchaseTransactionPushSpecificRequest request) {
		return service.getPurchaseTransactionPushSpecific(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionLoadAndReprocess", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionLoadAndReprocessResponse getPurchaseTransactionLoadAndReprocess(@RequestBody PurchaseTransactionLoadAndReprocessRequest request) {
		return service.getPurchaseTransactionLoadAndReprocess(request);
	}

	@RequestMapping(value = "/purchaseTransactionProcessSuspended", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionProcessSuspendedResponse getPurchaseTransactionProcessSuspended(@RequestBody PurchaseTransactionProcessSuspendedRequest request) {
		return service.getPurchaseTransactionProcessSuspended(request);
	}
	
	///////////////////// AC
	@RequestMapping(value = "/purchaseTransactionReprocess", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionReprocessResponse getPurchaseTransactionReprocess(@RequestBody PurchaseTransactionReprocessRequest request) {
		return service.getPurchaseTransactionReprocess(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionRelease", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionReleaseResponse getPurchaseTransactionRelease(@RequestBody PurchaseTransactionReleaseRequest request) {
		return service.getPurchaseTransactionRelease(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionLoadAndRelease", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionLoadAndReleaseResponse getPurchaseTransactionLoadAndRelease(@RequestBody PurchaseTransactionLoadAndReleaseRequest request) {
		return service.getPurchaseTransactionLoadAndRelease(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionSuspend", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionSuspendResponse getPurchaseTransactionSuspend(@RequestBody PurchaseTransactionSuspendRequest request) {
		return service.getPurchaseTransactionSuspend(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionLoadAndSuspend", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionSuspendResponse getPurchaseTransactionLoadAndSuspend(@RequestBody PurchaseTransactionLoadAndSuspendRequest request) {
		return service.getPurchaseTransactionLoadAndSuspend(request);
	}
	
	@RequestMapping(value = "/purchaseTransactionLoad", method = RequestMethod.POST)
	@ResponseBody
	public PurchaseTransactionLoadResponse getPurchaseTransactionLoad(@RequestBody PurchaseTransactionLoadRequest request) {
		return service.getPurchaseTransactionLoad(request);
	}
}
