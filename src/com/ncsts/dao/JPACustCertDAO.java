/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;

import com.ncsts.domain.Cust;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.view.util.SqlHelper;



public class JPACustCertDAO extends JPAGenericDAO<CustCert, Long> implements 
		CustCertDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	@SuppressWarnings("unused")
	private CustDAO custDAO;
	
	public Session getCurrentSession(){
		return createLocalSession();
		
	}

	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<CustCert> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<CustCert> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<CustCert> returnCust = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCust;
	}
	
	public CustCert find(CustCert exampleInstance) {
		List<CustCert> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@Transactional
	public CustLocnEx findCustLocnEx(Long custLocnExId) throws DataAccessException {	
		CustLocnEx custLocnEx = null;
		List<CustLocnEx> list = new ArrayList<CustLocnEx>();
		try {
			Session localSession = this.createLocalSession();
			Criteria criteria = localSession.createCriteria(CustLocnEx.class);
			
			criteria.add(Restrictions.eq("custLocnExId",custLocnExId));

        	list = criteria.list();
        	
        	if(list!=null && list.size()>0){
        		custLocnEx = (CustLocnEx)list.get(0);
        	}
        
        	this.closeLocalSession(localSession);
        	localSession=null;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		
		return custLocnEx;
	}
	
	@Transactional
	public void addCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(updateCustLocnEx);
			session.flush();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(updateCustLocnEx);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
	
			session.update(updateCustLocnEx);
			session.delete(updateCustLocnEx);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public boolean validateUniqueExemption(CustCert aCustCert) throws DataAccessException {	
		//The Certificate Number, Exempt Reason, Invoice Number, and Effective Date, and Certificate State must be unique.
		boolean uniqueCert = false;
		
		String sqlQuery  = "SELECT count(*) count FROM TB_CUST_LOCN_EX WHERE CUST_LOCN_ID = " + aCustCert.getCustLocnId() + " and ENTITY_ID = " + 
				aCustCert.getEntityId() + " and EXEMPTION_TYPE_CODE = '" + aCustCert.getExemptionTypeCode() + "'";

		Session session = createLocalSession();
		org.hibernate.Query query = null;
		try {
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			Long returnLong = id.longValue();
			if(returnLong.longValue() == 0l){
				uniqueCert=true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return uniqueCert;
	}
	
	@Transactional
	public boolean validateUniqueCertificate(CustCert updateCustCert) throws DataAccessException {	
		//The Certificate Number, Exempt Reason, Invoice Number, and Effective Date, and Certificate State must be unique.
		boolean uniqueCert = false;
		
		String sqlQuery  = "SELECT count(*) count FROM TB_CUST_CERT WHERE (1=1) ";

		if(updateCustCert.getCertNbr()!=null && updateCustCert.getCertNbr().length()>0)
			sqlQuery  = sqlQuery  + "AND UPPER(CERT_NBR) = '"+updateCustCert.getCertNbr().toUpperCase()+"' ";
		
		if(updateCustCert.getExemptReasonCode()!=null && updateCustCert.getExemptReasonCode().length()>0)
			sqlQuery  = sqlQuery  + "AND UPPER(EXEMPT_REASON_CODE) = '"+updateCustCert.getExemptReasonCode().toUpperCase()+"' ";
		
		if(updateCustCert.getInvoiceNbr()!=null && updateCustCert.getInvoiceNbr().length()>0) {
			sqlQuery  = sqlQuery  + "AND UPPER(INVOICE_NBR) = '"+updateCustCert.getInvoiceNbr().toUpperCase()+"' ";
		}
		else{
			sqlQuery  = sqlQuery  + "AND (INVOICE_NBR = '' OR INVOICE_NBR IS NULL) ";
		}
		
		if(updateCustCert.getCertState()!=null && updateCustCert.getCertState().length()>0)
			sqlQuery  = sqlQuery  + "AND CERT_STATE = '"+updateCustCert.getCertState()+"' ";
			
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		if(updateCustCert.getEffectiveDate()!=null){		
			String effectiveDate = df.format(updateCustCert.getEffectiveDate());
			sqlQuery  = sqlQuery  + "AND EFFECTIVE_DATE = to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
		}

		Session session = createLocalSession();
		org.hibernate.Query query = null;
		try {
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			Long returnLong = id.longValue();
			if(returnLong.longValue() == 0l){
				uniqueCert=true;
			}
			
			
			if(!uniqueCert && updateCustCert.getCustCertId()!=null){
				//When updating with the same values
				q = entityManager.createNativeQuery(sqlQuery +  "AND CUST_CERT_ID = "+updateCustCert.getCustCertId().intValue());
				id = (BigDecimal) q.getSingleResult();
				
				returnLong = id.longValue();
				if(returnLong.longValue() == 1l){
					//Updating the original data
					uniqueCert=true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return uniqueCert;
	}
	
	@Transactional
	public void validateCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(updateCustCert);
			session.flush();
			
			//Add a new CustLocnEx
			CustLocnEx aCustLocnEx = new CustLocnEx();
			aCustLocnEx.setCustLocnId(updateCustCert.getCustLocnId());
			aCustLocnEx.setEntityId(updateCustCert.getEntityId());
			aCustLocnEx.setExemptionTypeCode("2");//Certificate
			aCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			aCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			aCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			aCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			aCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			aCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			aCustLocnEx.setCityFlag(updateCustCert.getCityFlag());
			aCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			aCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			aCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			aCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			aCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());
			aCustLocnEx.setActiveFlag(updateCustCert.getActiveFlag());		
			aCustLocnEx.setCustCertId(updateCustCert.getCustCertId());			
			session.save(aCustLocnEx);
			session.flush();
			
			//Update selected CustLocnEx
			//custLocnEx.setCustCertId(updateCustCert.getCustCertId());
			session.update(custLocnEx);
			session.flush();
			

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateCustCertExemp(CustCert updateCustCert, CustLocnEx updateCustLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(updateCustCert);
			
			Criteria criteria = session.createCriteria(CustLocnEx.class);
			criteria.add(Restrictions.eq("custCertId",updateCustCert.getCustCertId()));
			List<CustLocnEx> list = criteria.list();
        	CustLocnEx custLocnEx = null;
        	if(list!=null && list.size()>0){
        		custLocnEx = (CustLocnEx)list.get(0);
        		custLocnEx.setEffectiveDate(updateCustLocnEx.getEffectiveDate());
        		custLocnEx.setExpirationDate(updateCustLocnEx.getExpirationDate());
        		custLocnEx.setExemptReasonCode(updateCustLocnEx.getExemptReasonCode());
        		custLocnEx.setCountryFlag(updateCustLocnEx.getCountryFlag());
        		custLocnEx.setStateFlag(updateCustLocnEx.getStateFlag());
        		custLocnEx.setCountyFlag(updateCustLocnEx.getCountyFlag());
    			custLocnEx.setCityFlag(updateCustLocnEx.getCityFlag());
    			custLocnEx.setStj1Flag(updateCustLocnEx.getStj1Flag());
    			custLocnEx.setStj2Flag(updateCustLocnEx.getStj2Flag());
    			custLocnEx.setStj3Flag(updateCustLocnEx.getStj3Flag());
    			custLocnEx.setStj4Flag(updateCustLocnEx.getStj4Flag());
    			custLocnEx.setStj5Flag(updateCustLocnEx.getStj5Flag());		
    			session.update(custLocnEx);
        	}

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void addCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(updateCustCert);
			session.flush();
			
			custLocnEx.setCustCertId(updateCustCert.getCustCertId());
			session.save(custLocnEx);
			session.flush();
			
			//Update temporarily exemption with 'V'
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDateStr = "";
			if(updateCustCert.getEffectiveDate()!=null){		
				effectiveDateStr = df.format(updateCustCert.getEffectiveDate());
			}

			String sql = "update TB_CUST_LOCN_EX set ACTIVE_FLAG = 'V' " + 
							"where EXEMPTION_TYPE_CODE = 1 and CUST_LOCN_ID = " + updateCustCert.getCustLocnId() + " and ENTITY_ID = "+ updateCustCert.getEntityId().intValue() + " " +
							"and TRUNC(EFFECTIVE_DATE) = to_date('" + effectiveDateStr + "', 'mm/dd/yyyy') " +
							"and CUST_LOCN_EX_ID NOT IN (" + custLocnEx.getCustLocnExId() +  ") ";
				
			query = session.createSQLQuery(sql);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void addCustCert(CustCert updateCustCert) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(updateCustCert);
			session.flush();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateCustCert(CustCert updateCustCert) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(updateCustCert);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteCustCert(CustCert updateCustCert) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			List<CustLocnEx> list = new ArrayList<CustLocnEx>();
			Criteria criteria = session.createCriteria(CustLocnEx.class);
			criteria.add(Restrictions.eq("custCertId",updateCustCert.getCustCertId()));
        	list = criteria.list();
        	CustLocnEx custLocnEx = null;
        	if(list!=null && list.size()>0){
        		custLocnEx = (CustLocnEx)list.get(0);
        	}
        	
        	if(custLocnEx==null){
        		return;
        	}
        	
        	session.update(custLocnEx);
			session.delete(custLocnEx);
	
			session.update(updateCustCert);
			session.delete(updateCustCert);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	private String sqlCustCert = 
			"select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " +
	
			"TB_CUST_CERT.CUST_CERT_ID, " +
			"TB_CUST_CERT.CERT_NBR, " +
			"TB_CUST_CERT.EXEMPT_REASON_CODE, " + 
			"TB_CUST_CERT.INVOICE_NBR, " +
			"TB_CUST_CERT.CERT_CUST_NAME, " +
			"TB_CUST_CERT.COUNTRY, " +
			"TB_CUST_CERT.CERT_STATE, " +
//			"TB_CUST_CERT.CERT_IMAGE, " +
			"TB_CUST_CERT.APPLY_TO_STATE_FLAG, " +
			"TB_CUST_CERT.BLANKET_FLAG, " +
			"TB_CUST_CERT.COUNTRY_FLAG, " +
			"TB_CUST_CERT.STATE_FLAG, " +
			"TB_CUST_CERT.COUNTY_FLAG, " +
			"TB_CUST_CERT.CITY_FLAG, " +
			"TB_CUST_CERT.STJ1_FLAG, " +
			"TB_CUST_CERT.STJ2_FLAG, " +
			"TB_CUST_CERT.STJ3_FLAG, " +
			"TB_CUST_CERT.STJ4_FLAG, " +
			"TB_CUST_CERT.STJ5_FLAG, " +
			"TB_CUST_CERT.ACTIVE_FLAG, " +	
			"TB_CUST_CERT.EFFECTIVE_DATE, " +
			"TB_CUST_CERT.EXPIRATION_DATE, " +
			
			"TB_CUST.CUST_NBR,  " +
			"TB_CUST.CUST_NAME,  " +
			
			"TB_CUST_LOCN.CUST_LOCN_CODE,  " +
			"TB_CUST_LOCN.LOCATION_NAME,  " +
			
			"TB_ENTITY.ENTITY_CODE, " + 
			"TB_ENTITY.ENTITY_NAME, " +
			"TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE, " +

			"TB_CUST.CUST_ID, " +
			"TB_CUST_LOCN.CUST_LOCN_ID, " +
			"TB_ENTITY.ENTITY_ID, " +
			
			
			//Add from Michael:
			" tb_cust_locn_ex.cust_locn_ex_id, " +
			
			"tb_cust_locn_ex.effective_date AS ex_effective_date, " +
			"tb_cust_locn_ex.expiration_date AS ex_expiration_date, " +
			"tb_cust_locn_ex.exempt_reason_code AS ex_exempt_reason_code, " +
			"tb_cust_locn_ex.country_flag AS ex_country_flag, " +
			"tb_cust_locn_ex.state_flag AS ex_state_flag, " +
			"tb_cust_locn_ex.county_flag AS ex_county_flag, " +
			"tb_cust_locn_ex.city_flag AS ex_city_flag, " +
			"tb_cust_locn_ex.stj1_flag AS ex_stj1_flag, " +
			"tb_cust_locn_ex.stj2_flag AS ex_stj2_flag, " +
			"tb_cust_locn_ex.stj3_flag AS ex_stj3_flag, " +
			"tb_cust_locn_ex.stj4_flag AS ex_stj4_flag, " +
			"tb_cust_locn_ex.stj5_flag AS ex_stj5_flag, " +
			"tb_cust_locn_ex.active_flag AS ex_active_flag, " +
			
			"TB_CUST_LOCN.COUNTRY AS custLocnCountry, " +
			"TB_CUST_LOCN.STATE AS custLocnState, " +
			"TB_CUST_LOCN.CITY AS custLocnCity, " +
			"TB_CUST_LOCN.ACTIVE_FLAG AS custLocnActiveFlag " +

//			"FROM TB_CUST, TB_CUST_LOCN, TB_CUST_LOCN_EX, TB_CUST_CERT, TB_ENTITY " +
//			"WHERE TB_CUST.CUST_ID = TB_CUST_LOCN.CUST_ID AND " +
//			"TB_CUST_LOCN.CUST_LOCN_ID =TB_CUST_CERT.CUST_LOCN_ID AND " +
//			"TB_ENTITY.ENTITY_ID=TB_CUST_CERT.ENTITY_ID AND " +

			"FROM ((tb_cust INNER JOIN tb_cust_locn ON tb_cust.cust_id=tb_cust_locn.cust_id " +
			"INNER JOIN tb_cust_locn_ex ON tb_cust_locn.cust_locn_id=tb_cust_locn_ex.cust_locn_id) " +
			"LEFT JOIN tb_cust_cert ON tb_cust_locn_ex.cust_cert_id=tb_cust_cert.cust_cert_id) " +
			"LEFT JOIN tb_entity ON tb_cust_locn_ex.entity_id=tb_entity.entity_id " +
			"LEFT JOIN tb_list_code cust_extype ON tb_cust_locn_ex.exemption_type_code=cust_extype.code_code AND cust_extype.code_type_code='EXTYPE' " +
			"LEFT JOIN tb_list_code cust_exreason ON tb_cust_locn_ex.exempt_reason_code=cust_exreason.code_code AND cust_exreason.code_type_code='EXREASON' " +
			"LEFT JOIN tb_list_code cert_certtype ON tb_cust_cert.cert_type_code=cert_certtype.code_code AND cert_certtype.code_type_code='CERTTYPE' " +
			"LEFT JOIN tb_list_code cert_exreason ON tb_cust_cert.exempt_reason_code=cert_exreason.code_code AND cert_exreason.code_type_code='EXREASON' " +
			"WHERE " +
	
	
			":extendWhere) WHERE RowNumber BETWEEN :minrow AND :maxrow ";

	
	public Long count(CustCert exampleInstance) {
	
		Long returnLong = 0L;
		try {
			String sqlQuery = "select count(*) count FROM ((tb_cust INNER JOIN tb_cust_locn ON tb_cust.cust_id=tb_cust_locn.cust_id " +
					"INNER JOIN tb_cust_locn_ex ON tb_cust_locn.cust_locn_id=tb_cust_locn_ex.cust_locn_id) " +
					"LEFT JOIN tb_cust_cert ON tb_cust_locn_ex.cust_cert_id=tb_cust_cert.cust_cert_id) " +
					"LEFT JOIN tb_entity ON tb_cust_locn_ex.entity_id=tb_entity.entity_id " +
					"LEFT JOIN tb_list_code cust_extype ON tb_cust_locn_ex.exemption_type_code=cust_extype.code_code AND cust_extype.code_type_code='EXTYPE' " +
					"LEFT JOIN tb_list_code cust_exreason ON tb_cust_locn_ex.exempt_reason_code=cust_exreason.code_code AND cust_exreason.code_type_code='EXREASON' " +
					"LEFT JOIN tb_list_code cert_certtype ON tb_cust_cert.cert_type_code=cert_certtype.code_code AND cert_certtype.code_type_code='CERTTYPE' " +
					"LEFT JOIN tb_list_code cert_exreason ON tb_cust_cert.exempt_reason_code=cert_exreason.code_code AND cert_exreason.code_type_code='EXREASON' " +
					"WHERE :extendWhere ";
			
			String extendWhere = getWhereClause(exampleInstance);
			sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return returnLong;
	}
	
	private String getWhereClause(CustCert exampleInstance){
		String extendWhere = "(1=1) ";

		if(exampleInstance.getCustNbr()!=null && exampleInstance.getCustNbr().length()>0){
			if(exampleInstance.getCustNbr().substring(exampleInstance.getCustNbr().length()-1, exampleInstance.getCustNbr().length()).equals("%") ||
					exampleInstance.getCustNbr().substring(0, 1).equals("%")){
				extendWhere = extendWhere + " AND UPPER(TB_CUST.CUST_NBR) like '"+ SqlHelper.removeApostrophe(exampleInstance.getCustNbr().toUpperCase()) + "' ";
			}
			else{
				extendWhere = extendWhere + " AND UPPER(TB_CUST.CUST_NBR) = '"+ SqlHelper.removeApostrophe(exampleInstance.getCustNbr().toUpperCase()) + "' ";
			}
		}
		
		if(exampleInstance.getCustName()!=null && exampleInstance.getCustName().length()>0){
			if(exampleInstance.getCustName().substring(exampleInstance.getCustName().length()-1, exampleInstance.getCustName().length()).equals("%") ||
					exampleInstance.getCustName().substring(0, 1).equals("%")){
				extendWhere = extendWhere + " AND UPPER(TB_CUST.CUST_NAME) like '"+ SqlHelper.removeApostrophe(exampleInstance.getCustName().toUpperCase()) + "' ";
			}
			else{
				extendWhere = extendWhere + " AND UPPER(TB_CUST.CUST_NAME) = '"+ SqlHelper.removeApostrophe(exampleInstance.getCustName().toUpperCase()) + "' ";
			}
		}
		
		if(exampleInstance.getCustActiveFlag()!=null && exampleInstance.getCustActiveFlag().length()>0)
			extendWhere = extendWhere + "AND TB_CUST.ACTIVE_FLAG = '"+exampleInstance.getCustActiveFlag()+"' ";
		
		if(exampleInstance.getCustLocnCode()!=null && exampleInstance.getCustLocnCode().length()>0){
			if(exampleInstance.getCustLocnCode().substring(exampleInstance.getCustLocnCode().length()-1, exampleInstance.getCustLocnCode().length()).equals("%") ||
					exampleInstance.getCustLocnCode().substring(0, 1).equals("%")){
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.CUST_LOCN_CODE) like '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnCode().toUpperCase()) + "' ";
			}
			else{
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.CUST_LOCN_CODE) = '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnCode().toUpperCase()) + "' ";
			}
		}
		
		if(exampleInstance.getCustLocnName()!=null && exampleInstance.getCustLocnName().length()>0){
			if(exampleInstance.getCustLocnName().substring(exampleInstance.getCustLocnName().length()-1, exampleInstance.getCustLocnName().length()).equals("%") ||
					exampleInstance.getCustLocnName().substring(0, 1).equals("%")){
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.LOCATION_NAME) like '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnName().toUpperCase()) + "' ";
			}
			else{
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.LOCATION_NAME) = '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnName().toUpperCase()) + "' ";
			}
		}
		
		if(exampleInstance.getCustLocnCountry()!=null && exampleInstance.getCustLocnCountry().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.COUNTRY = '"+SqlHelper.removeApostrophe(exampleInstance.getCustLocnCountry())+"' ";
		
		if(exampleInstance.getCustLocnState()!=null && exampleInstance.getCustLocnState().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.STATE = '"+SqlHelper.removeApostrophe(exampleInstance.getCustLocnState())+"' ";
		
		if(exampleInstance.getCustLocnCity()!=null && exampleInstance.getCustLocnCity().length()>0){
			if(exampleInstance.getCustLocnCity().substring(exampleInstance.getCustLocnCity().length()-1, exampleInstance.getCustLocnCity().length()).equals("%") ||
					exampleInstance.getCustLocnCity().substring(0, 1).equals("%")){
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.CITY) like '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnCity().toUpperCase()) + "' ";
			}
			else{
				extendWhere = extendWhere + " AND UPPER(TB_CUST_LOCN.CITY) = '"+ SqlHelper.removeApostrophe(exampleInstance.getCustLocnCity().toUpperCase()) + "' ";
			}
		}
		
		if(exampleInstance.getCustLocnActiveFlag()!=null && exampleInstance.getCustLocnActiveFlag().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.ACTIVE_FLAG = '"+exampleInstance.getCustLocnActiveFlag()+"' ";
		
		if(exampleInstance.getEntityId()!=null && exampleInstance.getEntityId().intValue()!=0) //Set -1 for not match entity code
			extendWhere = extendWhere + "AND TB_CUST_LOCN_EX.ENTITY_ID = " + exampleInstance.getEntityId() +" ";
		
		if(exampleInstance.getExemptionTypeCode()!=null && exampleInstance.getExemptionTypeCode().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE = '"+SqlHelper.removeApostrophe(exampleInstance.getExemptionTypeCode())+"' ";
		
		if(exampleInstance.getCountry()!=null && exampleInstance.getCountry().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_CERT.COUNTRY = '"+SqlHelper.removeApostrophe(exampleInstance.getCountry())+"' ";
		
		if(exampleInstance.getState()!=null && exampleInstance.getState().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_CERT.CERT_STATE = '"+SqlHelper.removeApostrophe(exampleInstance.getState())+"' ";
			
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String extendWhereCert = "";
		if(exampleInstance.getEffectiveDate()!=null){		
			String effectiveDate = df.format(exampleInstance.getEffectiveDate());
			extendWhereCert = extendWhereCert + "AND TB_CUST_CERT.EFFECTIVE_DATE >= to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getEffectiveDateThru()!=null){		
			String effectiveDateThru = df.format(exampleInstance.getEffectiveDateThru());
			extendWhereCert = extendWhereCert + "AND TB_CUST_CERT.EFFECTIVE_DATE <= to_date('"+effectiveDateThru+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getExpirationDate()!=null){
			String expirationDate = df.format(exampleInstance.getExpirationDate());
			extendWhereCert = extendWhereCert + "AND TB_CUST_CERT.EXPIRATION_DATE <= to_date('"+expirationDate+"', 'mm/dd/yyyy') ";
		}
		
		String extendWhereEx = "";
		if(exampleInstance.getEffectiveDate()!=null){		
			String effectiveDate = df.format(exampleInstance.getEffectiveDate());
			extendWhereEx = extendWhereEx + "AND TB_CUST_LOCN_EX.EFFECTIVE_DATE >= to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getEffectiveDateThru()!=null){		
			String effectiveDateThru = df.format(exampleInstance.getEffectiveDateThru());
			extendWhereEx = extendWhereEx + "AND TB_CUST_LOCN_EX.EFFECTIVE_DATE <= to_date('"+effectiveDateThru+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getExpirationDate()!=null){
			String expirationDate = df.format(exampleInstance.getExpirationDate());
			extendWhereEx = extendWhereEx + "AND TB_CUST_LOCN_EX.EXPIRATION_DATE <= to_date('"+expirationDate+"', 'mm/dd/yyyy') ";
		}
		
		extendWhere = extendWhere + "AND ( (TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE='2' "+ extendWhereCert +") OR (TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE='1' AND TB_CUST_LOCN_EX.ACTIVE_FLAG<>'V' "+ extendWhereEx +")) ";
		
		return extendWhere;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<CustCert> find(CustCert exampleInstance,  OrderBy orderBy, int firstRow, int maxResults){
		String sqlQuery = sqlCustCert;

		String strOrderBy = getOrderByToken(orderBy);
		String extendWhere = getWhereClause(exampleInstance);

		sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
		sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
		sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
		sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));
		
		List<CustCert> returnList=null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			returnList = parseCustCertReturnList(iter);
		}
		return returnList;
		
	}
	
	private List<CustCert> parseCustCertReturnList(Iterator<Object> iter){
		List<CustCert> returnList = new ArrayList<CustCert>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			CustCert custCert=new CustCert();
			
			//RowNumber = (BigDecimal)objarray[0];
			
            //BigDecimal is = (BigDecimal)objarray[1];
            //if (is!=null) custCert.setCustCertId(is.longValue());
			//For data model purpose
			BigDecimal is = (BigDecimal)objarray[1];
            if (is!=null) custCert.setTrueCustCertId(is.longValue());
            
            String certNbr = (String) objarray[2];
            custCert.setCertNbr(certNbr);
            
            String exemptReasonCode = (String) objarray[3];
            custCert.setExemptReasonCode(exemptReasonCode);
            
            String invoiceNbr = (String) objarray[4];
            custCert.setInvoiceNbr(invoiceNbr);
            
            String certCustName = (String) objarray[5];
            custCert.setCertCustName(certCustName);
            
            String country = (String) objarray[6];
            custCert.setCountry(country);
            
            String certState = (String) objarray[7];
            custCert.setCertState(certState);
            
            Character applyToStateFlag = (Character) objarray[8];
            if (applyToStateFlag != null) custCert.setApplyToStateFlag(applyToStateFlag.toString());
            
            Character blanketFlag = (Character) objarray[9];
            if (blanketFlag != null) custCert.setBlanketFlag(blanketFlag.toString());
            
            Character countryFlag = (Character) objarray[10];
            if (countryFlag != null) custCert.setCountryFlag(countryFlag.toString());
            
            Character stateFlag = (Character) objarray[11];
            if (stateFlag != null) custCert.setStateFlag(stateFlag.toString());
            
            Character countyFlag = (Character) objarray[12];
            if (countyFlag != null) custCert.setCountyFlag(countyFlag.toString());
            
            Character cityFlag = (Character) objarray[13];
            if (cityFlag != null) custCert.setCityFlag(cityFlag.toString());
            
            Character stj1Flag = (Character) objarray[14];
            if (stj1Flag != null) custCert.setStj1Flag(stj1Flag.toString());
            
            Character stj2Flag = (Character) objarray[15];
            if (stj2Flag != null) custCert.setStj2Flag(stj2Flag.toString());
            
            Character stj3Flag = (Character) objarray[16];
            if (stj3Flag != null) custCert.setStj3Flag(stj3Flag.toString());
            
            Character stj4Flag = (Character) objarray[17];
            if (stj4Flag != null) custCert.setStj4Flag(stj4Flag.toString());
            
            Character stj5Flag = (Character) objarray[18];
            if (stj5Flag != null) custCert.setStj5Flag(stj5Flag.toString());
            
            Character activeFlag = (Character) objarray[19];
            if (activeFlag != null) custCert.setActiveFlag(activeFlag.toString());
            
            Date effectiveDate = (Date) objarray[20];
            custCert.setEffectiveDate(effectiveDate);
            
            Date expirationDate = (Date) objarray[21];
            custCert.setExpirationDate(expirationDate);
            
            String custNbr = (String) objarray[22];
            custCert.setCustNbr(custNbr);
            
            String custName = (String) objarray[23];
            custCert.setCustName(custName);
            
            String custLocnCode = (String) objarray[24];
            custCert.setCustLocnCode(custLocnCode);
            
            String locationName = (String) objarray[25];
            custCert.setCustLocnName(locationName);
            
            String entityCode = (String) objarray[26];
            custCert.setEntityCode(entityCode);
            
            String entityName = (String) objarray[27];
            custCert.setEntityName(entityName);
            
            String exemptionTypeCode = (String) objarray[28];
            custCert.setExemptionTypeCode(exemptionTypeCode);
            
			BigDecimal custId = (BigDecimal)objarray[29];
            if (custId!=null) custCert.setCustId(custId.longValue());
            
            BigDecimal custLocnId = (BigDecimal)objarray[30];
            if (custLocnId!=null) custCert.setCustLocnId(custLocnId.longValue());
            
            BigDecimal entityId = (BigDecimal)objarray[31];
            if (entityId!=null) custCert.setEntityId(entityId.longValue());
            
            BigDecimal custLocnExId = (BigDecimal)objarray[32];
            if (custLocnExId!=null) custCert.setCustLocnExId(custLocnExId.longValue());
            
            //For data model purpose
            if (custLocnExId!=null) custCert.setCustCertId(custLocnExId.longValue());
            
            if(custCert.getExemptionTypeCode()!=null && custCert.getExemptionTypeCode().equals("1")){
            	 effectiveDate = (java.sql.Timestamp) objarray[33];
	           	 if(effectiveDate!=null){
	           		 custCert.setEffectiveDate(new java.util.Date(effectiveDate.getTime()));
	           	 }
                
	           	 expirationDate = (java.sql.Timestamp) objarray[34];
	           	 if(expirationDate!=null){
	           		 custCert.setExpirationDate(new java.util.Date(expirationDate.getTime()));
	           	 }
                 
                 exemptReasonCode = (String) objarray[35];
                 custCert.setExemptReasonCode(exemptReasonCode);
                 
                 countryFlag = (Character) objarray[36];
                 if (countryFlag != null) custCert.setCountryFlag(countryFlag.toString());
                 
                 stateFlag = (Character) objarray[37];
                 if (stateFlag != null) custCert.setStateFlag(stateFlag.toString());
                 
                 countyFlag = (Character) objarray[38];
                 if (countyFlag != null) custCert.setCountyFlag(countyFlag.toString());
                 
                 cityFlag = (Character) objarray[39];
                 if (cityFlag != null) custCert.setCityFlag(cityFlag.toString());
                 
                 stj1Flag = (Character) objarray[40];
                 if (stj1Flag != null) custCert.setStj1Flag(stj1Flag.toString());
                 
                 stj2Flag = (Character) objarray[41];
                 if (stj2Flag != null) custCert.setStj2Flag(stj2Flag.toString());
                 
                 stj3Flag = (Character) objarray[42];
                 if (stj3Flag != null) custCert.setStj3Flag(stj3Flag.toString());
                 
                 stj4Flag = (Character) objarray[43];
                 if (stj4Flag != null) custCert.setStj4Flag(stj4Flag.toString());
                 
                 stj5Flag = (Character) objarray[44];
                 if (stj5Flag != null) custCert.setStj5Flag(stj5Flag.toString());
                 
                 activeFlag = (Character) objarray[45];
                 if (activeFlag != null) custCert.setActiveFlag(activeFlag.toString());
            }
            
            String custLocnCountry = (String) objarray[46];
            if (custLocnCountry != null) custCert.setCustLocnCountry(custLocnCountry.toString());
            
            String custLocnState = (String) objarray[47];
            if (custLocnState != null) custCert.setCustLocnState(custLocnState.toString());
            
            String custLocnCity = (String) objarray[48];
            if (custLocnCity != null) custCert.setCustLocnCity(custLocnCity.toString());
            
            Character custLocnActiveFlag = (Character) objarray[49];
            if (custLocnActiveFlag != null) custCert.setCustLocnActiveFlag(custLocnActiveFlag.toString());
                      
            returnList.add(custCert);                
		}
		
		return returnList;
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
        }
		
		//Default order
		if(orderByToken.length()==0){
			orderByToken.append(" TB_CUST_CERT.CUST_CERT_ID ASC ");
		}

		return orderByToken.toString();
	}
	
	private Map<String,String> columnMapping = null;
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			
			columnMapping.put("custCertId".toLowerCase(), "TB_CUST_CERT.CUST_CERT_ID"); 
			columnMapping.put("certNbr".toLowerCase(), "TB_CUST_CERT.CERT_NBR");
			columnMapping.put("exemptReasonCode".toLowerCase(), "TB_CUST_CERT.EXEMPT_REASON_CODE");
			columnMapping.put("invoiceNbr".toLowerCase(), "TB_CUST_CERT.INVOICE_NBR");
			columnMapping.put("certCustName".toLowerCase(), "TB_CUST_CERT.CERT_CUST_NAME");
			columnMapping.put("country".toLowerCase(), "TB_CUST_CERT.COUNTRY");
			columnMapping.put("state".toLowerCase(), "TB_CUST_CERT.CERT_STATE");
			columnMapping.put("applyToStateFlag".toLowerCase(), "TB_CUST_CERT.APPLY_TO_STATE_FLAG");
			columnMapping.put("blanketFlag".toLowerCase(), "TB_CUST_CERT.BLANKET_FLAG");
			columnMapping.put("countryFlag".toLowerCase(), "TB_CUST_CERT.COUNTRY_FLAG");
			columnMapping.put("stateFlag".toLowerCase(), "TB_CUST_CERT.STATE_FLAG");
			columnMapping.put("countyFlag".toLowerCase(), "TB_CUST_CERT.COUNTY_FLAG");
			columnMapping.put("cityFlag".toLowerCase(), "TB_CUST_CERT.CITY_FLAG");
			columnMapping.put("stj1Flag".toLowerCase(), "TB_CUST_CERT.STJ1_FLAG");
			columnMapping.put("stj2Flag".toLowerCase(), "TB_CUST_CERT.STJ2_FLAG");
			columnMapping.put("stj3Flag".toLowerCase(), "TB_CUST_CERT.STJ3_FLAG");
			columnMapping.put("stj4Flag".toLowerCase(), "TB_CUST_CERT.STJ4_FLAG");
			columnMapping.put("stj5Flag".toLowerCase(), "TB_CUST_CERT.STJ5_FLAG");
			columnMapping.put("activeFlag".toLowerCase(), "TB_CUST_CERT.ACTIVE_FLAG");
			columnMapping.put("effectiveDate".toLowerCase(), "TB_CUST_CERT.EFFECTIVE_DATE");
			columnMapping.put("expirationDate".toLowerCase(), "TB_CUST_CERT.EXPIRATION_DATE");
			columnMapping.put("custNbr".toLowerCase(), "TB_CUST.CUST_NBR");
			columnMapping.put("custName".toLowerCase(), "TB_CUST.CUST_NAME");
			columnMapping.put("custLocnCode".toLowerCase(), "TB_CUST_LOCN.CUST_LOCN_CODE");
			columnMapping.put("custLocnName".toLowerCase(), "TB_CUST_LOCN.LOCATION_NAME");
			columnMapping.put("entityCode".toLowerCase(), "TB_ENTITY.ENTITY_CODE");
			columnMapping.put("entityName".toLowerCase(), "TB_ENTITY.ENTITY_NAME");
			columnMapping.put("exemptionTypeCode".toLowerCase(), "TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE");
			columnMapping.put("custid".toLowerCase(), "TB_CUST.CUST_ID");
			columnMapping.put("custLocnId".toLowerCase(), "TB_CUST_LOCN.CUST_LOCN_ID");
			columnMapping.put("entityid".toLowerCase(), "TB_ENTITY.ENTITY_ID");
			columnMapping.put("custLocnExId".toLowerCase(), "tb_cust_locn_ex.cust_locn_ex_id");			
			columnMapping.put("custLocnCountry".toLowerCase(), "TB_CUST_LOCN.COUNTRY");
			columnMapping.put("custLocnState".toLowerCase(), "TB_CUST_LOCN.STATE");
			columnMapping.put("custLocnCity".toLowerCase(), "TB_CUST_LOCN.CITY");
			columnMapping.put("custLocnActiveFlag".toLowerCase(), "TB_CUST_LOCN.ACTIVE_FLAG");
		}
		
		String columnName = columnMapping.get(name.toLowerCase());
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
}
