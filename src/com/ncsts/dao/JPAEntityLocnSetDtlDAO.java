package com.ncsts.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.EntityLocnSetDtl;

public class JPAEntityLocnSetDtlDAO extends JPAGenericDAO<EntityLocnSetDtl,Long> implements EntityLocnSetDtlDAO {
	
	@Transactional
	public Long count(Long entityLocnSetId) throws DataAccessException {
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createQuery(" select count(*) from EntityLocnSetDtl entityLocnSetDtl where entityLocnSetDtl.entityLocnSetId = ?");
	  	query.setParameter(1,entityLocnSetId );
	    Long count = (Long) query.getSingleResult();
		return count;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityLocnSetDtl> findAllEntityLocnSetDtls() throws DataAccessException {
	    return getJpaTemplate().find(" select entityLocnSetDtl from EntityLocnSetDtl entityLocnSetDtl");	  
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityLocnSetDtl> findAllEntityLocnSetDtlsByEntityLocnSetId(Long entityLocnSetId) throws DataAccessException {
		return getJpaTemplate().find(" select entityLocnSetDtl from EntityLocnSetDtl entityLocnSetDtl where entityLocnSetDtl.entityLocnSetId = ? order by entityLocnSetDtl.sfEntityId", entityLocnSetId);	  
	}
	  
	@SuppressWarnings("deprecation")
	@Transactional
	public Long persist(EntityLocnSetDtl entityLocnSetDtl) throws DataAccessException {
		getJpaTemplate().persist(entityLocnSetDtl);
		getJpaTemplate().flush();
		Long entityLocnSetDtlId = entityLocnSetDtl.getEntityLocnSetDtlId();
		
		return entityLocnSetDtlId;
	}	
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long id) throws DataAccessException {
		EntityLocnSetDtl entityLocnSetDtl = getJpaTemplate().find(EntityLocnSetDtl.class, id);
		if( entityLocnSetDtl != null){
			getJpaTemplate().remove(entityLocnSetDtl);
		}else{
			logger.warn("EntityLocnSetDtl Object not found for deletion");
		}
	} 
	  
	public void saveOrUpdate(EntityLocnSetDtl entityLocnSetDtl) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.update(entityLocnSetDtl);
		t.commit();
		closeLocalSession(session);
		session=null;		
	}
	
	@Transactional
	public void deleteEntityLocnSet(Long entityLocnSetId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			//remove(TB_ENTITY);
			String hqlDelete = "DELETE TB_ENTITY_LOCN_SET_DTL WHERE ENTITY_LOCN_SET_ID = " + entityLocnSetId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Delete all existing TB_NEXUS_DEF
			//hqlDelete = "DELETE FROM TB_NEXUS_DEF WHERE ENTITY_ID = " + entityId.intValue();
			//query = session.createSQLQuery(hqlDelete);
			//query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute deleteEntityLocnSet: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	  
}


