package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.ImportConnection;
import com.ncsts.common.helper.OrderBy;

public interface ImportConnectionDAO extends GenericDAO<ImportConnection, String> {
	
	//public abstract void add(ImportConnection importConnection) throws DataAccessException;
	public abstract void update(ImportConnection importConnection) throws DataAccessException;
	public abstract void delete(String importConnectionCode) throws DataAccessException;
	public abstract List<ImportConnection> getConnectionsByType(String type);
	public List<ImportConnection> getAllRecords(ImportConnection exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults);
	public Long count(ImportConnection exampleInstance);
	public List<ImportConnection> getConnectionsByCode(String importConnectionCode) throws DataAccessException;
}
