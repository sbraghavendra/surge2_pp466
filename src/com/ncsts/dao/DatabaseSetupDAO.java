package com.ncsts.dao;

/**
 * @author AChen
 * 
 */

import org.springframework.dao.DataAccessException;

public interface DatabaseSetupDAO {
	public abstract boolean isEnterImportDefinitionSpecDone() throws DataAccessException;
	public abstract boolean isEnterDriversDone() throws DataAccessException;
	public abstract boolean isEnterEntityLevelsDone() throws DataAccessException;
	public abstract boolean isEnterEntitiesDone() throws DataAccessException;
}