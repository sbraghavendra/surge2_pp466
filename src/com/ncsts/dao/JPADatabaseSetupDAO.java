package com.ncsts.dao;

/**
 * @author AChen
 */

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;


public class JPADatabaseSetupDAO extends JpaDaoSupport implements DatabaseSetupDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

	@Transactional(readOnly=true)
	public boolean isEnterImportDefinitionSpecDone() throws DataAccessException{
		String sql = " SELECT COUNT(*) " +
					 " FROM TB_IMPORT_SPEC a left outer join TB_IMPORT_DEFINITION b ON (a.import_definition_code = b.import_definition_code ) " +
					 " left outer join TB_IMPORT_DEFINITION_DETAIL c ON (b.import_definition_code = c.import_definition_code) ";
		
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
   	 	List<Number> countList = query.list();
   	 	
   	 	boolean returnBoolean = false;
		
        if(countList==null || countList.size()==0){
        	returnBoolean =  false;
        }
		
        if(((Number)countList.get(0)).intValue()>0){
        	returnBoolean = true;
	    }
	    else{
	    	returnBoolean = false;
	    }
        
        closeLocalSession(session);
        session=null;
        
        return returnBoolean;
	} 
		
	@Transactional(readOnly=true)
	public boolean isEnterDriversDone() {
		String sql =	" SELECT COUNT(*) FROM TB_DRIVER_NAMES WHERE DRIVER_NAMES_CODE = 'T' ";

		Session session = createLocalSession();
		
		org.hibernate.Query query = session.createSQLQuery(sql);
   	 	List<Number> countList = query.list();
		
   	 	boolean returnBoolean = false;
   	 
        if(countList==null || countList.size()==0){
        	returnBoolean = false;
        }
		
        if(((Number)countList.get(0)).intValue()>0){
        	returnBoolean = true;
	    }
	    else{
	    	returnBoolean = false;
	    }
        
        closeLocalSession(session);
        session=null;
        
        return returnBoolean;
	}

	@Transactional(readOnly=true)
	public boolean isEnterEntityLevelsDone(){
		String sql =	" SELECT COUNT(*) FROM TB_ENTITY_LEVEL ";

		Session session = createLocalSession();
		
		org.hibernate.Query query = session.createSQLQuery(sql);
   	 	List<Number> countList = query.list();
   	 	
   	 	boolean returnBoolean = false;
		
        if(countList==null || countList.size()==0){
        	returnBoolean = false;
        }
		
        if(((Number)countList.get(0)).intValue()>0){
        	returnBoolean = true;
	    }
	    else{
	    	returnBoolean = false;
	    }
        
        closeLocalSession(session);
        session=null;
        
        return returnBoolean;
	}

	@Transactional(readOnly=true)
	public boolean isEnterEntitiesDone() {
		String sql =	" SELECT COUNT(*) FROM TB_ENTITY ";

		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
   	 	List<Number> countList = query.list();
   	 	
   	 	boolean returnBoolean = false;
		
        if(countList==null || countList.size()==0){
        	returnBoolean = false;
        }
		
        if(((Number)countList.get(0)).intValue()>0){
        	returnBoolean = true;
	    }
	    else{
	    	returnBoolean = false;
	    }
        
        closeLocalSession(session);
        session=null;
        
        return returnBoolean;
	}
}
