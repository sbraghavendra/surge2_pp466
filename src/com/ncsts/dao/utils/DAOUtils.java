package com.ncsts.dao.utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import com.seconddecimal.billing.util.DateUtils;
import org.springframework.util.Assert;

public class DAOUtils {
	
	public static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.DECIMAL);
		sqlTypes.put(java.util.Date.class, Types.DATE);
	}

	public static void setLongOrNull(CallableStatement stmt, int index, Long val) throws SQLException {
		if(val == null) {
			stmt.setNull(index, java.sql.Types.NUMERIC);
		}
		else {
			stmt.setLong(index, val);
		}
	}
	
	public static void setFloatOrNull(CallableStatement stmt, int index, Float val) throws SQLException {
		if(val == null) {
			stmt.setNull(index, java.sql.Types.FLOAT);
		}
		else {
			stmt.setFloat(index, val);
		}
	}
	
	public static void setDoubleOrNull(CallableStatement stmt, int index, Double val) throws SQLException {
		if(val == null) {
			stmt.setNull(index, java.sql.Types.DOUBLE);
		}
		else {
			stmt.setDouble(index, val);
		}
	}
	
	public static void setBigDecimalOrNull(CallableStatement stmt, int index, BigDecimal val) throws SQLException {
		if(val == null) {
			stmt.setNull(index, java.sql.Types.DECIMAL);
		}
		else {
			stmt.setBigDecimal(index, val);
		}
	}
	
	// converts MY_STRANGE_NAME
	// to MyStrangeName
	public static String convSqlToBeanName(String sqlName) {
		Assert.notNull(sqlName);
		StringBuffer result = new StringBuffer();
		String[] chunks = sqlName.toLowerCase().split("_");
		for (int i = 0; i < chunks.length; i++) {
			result.append(Character.toUpperCase(chunks[i].charAt(0))
					+ chunks[i].substring(1));
		}

		return result.toString();
	}
	
	public static void setStatementValue(PreparedStatement statement, Object value, int columnIndex, Type fieldType, String fieldName) throws SQLException {
		switch (DAOUtils.sqlTypes.get(fieldType)) {
		case Types.BIGINT:
			if (value != null) {
				statement.setLong(columnIndex, (Long) value);
			} else
				statement.setNull(columnIndex, Types.BIGINT);
			break;

		case Types.VARCHAR:
			if (value != null) {
				statement.setString(columnIndex, (String) value);
			} else
				statement.setNull(columnIndex, Types.VARCHAR);
			break;

		case Types.DATE:
			if (value != null) {
				statement.setDate(columnIndex,
						DateUtils.getSqlDate((java.util.Date) value));
			} else
				statement.setNull(columnIndex, Types.TIMESTAMP);
			break;

		case Types.FLOAT:
			if (value != null) {
				statement.setFloat(columnIndex, (Float) value);
			} else
				statement.setNull(columnIndex, Types.FLOAT);
			break;

		case Types.BOOLEAN:
			if (value != null) {
				statement.setBoolean(columnIndex, (Boolean) value);
			} else
				statement.setNull(columnIndex, Types.BOOLEAN);
			break;

		case Types.DOUBLE:
			if (value != null) {
				statement.setDouble(columnIndex, (Double) value);
			} else
				statement.setNull(columnIndex, Types.DOUBLE);
			break;

		case Types.DECIMAL:
			if (value != null) {
				statement.setBigDecimal(columnIndex, (BigDecimal) value);
			} else
				statement.setNull(columnIndex, Types.DECIMAL);
			break;

		default:
			if (value != null) {
				System.err.println("RV, db: " + fieldName + " bn: "
						+ value);
				statement.setObject(columnIndex, value);

			} else
				System.err.println("null v, db: " + fieldName
						+ " bn: " + value);
			statement.setNull(columnIndex, DAOUtils.sqlTypes.get(fieldType));
		}
	
	}
	
}
