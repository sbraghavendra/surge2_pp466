package com.ncsts.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.impl.SessionImpl;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Idable;

public class JPAGenericDAO<T extends Idable<ID>,ID extends Serializable> extends JpaDaoSupport implements GenericDAO<T,ID> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> persistentClass;
	private Session session;
	
	public Class<T> getObjectClass() {
		return persistentClass;
	}
	
	@SuppressWarnings("unchecked")
	public JPAGenericDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Long count() {
		logger.info("----- JPAGenericDAO.count() enter ------");
        Query query = createQuery("select count(*) from " + persistentClass.getCanonicalName());
		logger.info("----- JPAGenericDAO.count() just before return ------");        
        return (Long)query.getSingleResult();
	}
	
	public Long count(T exampleInstance, String... excludeProperty) {
		logger.info("----- JPAGenericDAO.count(..., ...) enter ------");		

        Session session = createLocalSession();
		Criteria crit = session.createCriteria(persistentClass);
        
        // Add the example and excludes
        if (exampleInstance != null){
            Example example = Example.create(exampleInstance);
            example.ignoreCase();
            if (excludeProperty != null){
                for (String exclude : excludeProperty) {
                    example.excludeProperty(exclude);
                }
            }

            // SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
            ryanExampleHandling(crit, example, exampleInstance);
            
            crit.add(example);
            
            // Hook to further extend criteria 
            extendExampleCriteria(crit, exampleInstance);
        }
        
        crit.setProjection(Projections.rowCount()); // We just want a row count
		logger.info("----- JPAGenericDAO.count(..., ...) exit ------");   
		
		Long returnLong = ((Number)crit.list().get(0)).longValue();
		
		closeLocalSession(session);
		session=null;
		
        return returnLong;
	}
	
	@Transactional
	public T findById(ID id) throws DataAccessException {
	    return getJpaTemplate().find(persistentClass, id);
	}
	 
	@SuppressWarnings("unchecked")
	public List<T> findById(Collection<ID> ids) {
		List<T> result = null;
		try {
			// Need to create sample object in order to
			// invoke method that provides name of id
			T object = persistentClass.newInstance();
			
			Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);
			
	        criteria.add(Restrictions.in(object.getIdPropertyName(), ids));
			result = (List<T>) criteria.list();
			
			closeLocalSession(session);
			session=null;    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	 
	public List<T> findAll() {
		List<T> result = null;
		try {
			// Need to create sample object in order to
			// invoke method that provides name of id
			T object = persistentClass.newInstance();
			result = findAllSorted(object.getIdPropertyName(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<T> findAllSorted(String sortField, boolean ascending) {
		List<T> result = getJpaTemplate().find(
				"from " + persistentClass.getCanonicalName() + 
				" order by " + sortField + ((ascending)? " asc":" desc"));
		return result;
	}
	
	protected void extendExampleCriteria(Criteria criteria, T exampleInstance) {
		// Hook to provide opportunity to adjust findByExample criteria
	}
	
	public List<T> findByExample(T exampleInstance, int count, String... excludeProperty){
		return findByExampleSorted(exampleInstance, count, exampleInstance.getIdPropertyName(), true, excludeProperty);
	}
	 
	@SuppressWarnings("unchecked")
	public List<T> findByExampleSorted(T exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty){
		List<T> list = null;
         
        try {
        	Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);
        	
            if (count > 0) {
            	criteria.setMaxResults(count);
            }
            
            Example example = Example.create(exampleInstance);
            example.ignoreCase();
            if (excludeProperty != null) {
	            for (String exclude : excludeProperty) {
	                example.excludeProperty(exclude);
	            }
            }
            
            // SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
            ryanExampleHandling(criteria, example, exampleInstance);
            
            criteria.add(example);
            
            // Return results using predictable ordering
           	criteria.addOrder((ascending)? Order.asc(sortField):Order.desc(sortField));
            
            // Hook to further extend criteria 
            extendExampleCriteria(criteria, exampleInstance);

            list = criteria.list();
            
            closeLocalSession(session);
            session=null;
            
        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the entities by example");
        }
        
        return list; 
	}
	
	@Transactional(readOnly = false)
	public void save(T instance) {
		entityManager.persist(instance);
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(T instance) throws DataAccessException {
		entityManager.merge(instance);
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(T instance)throws DataAccessException {
		T obj = findById(instance.getId());
		if(obj != null) {
			getJpaTemplate().remove(obj);
		} else {
			logger.warn("Object not found for delete!");
		}
	}

    @SuppressWarnings("unchecked")
	public List<T> getPage(int startIndex, int endIndex, OrderBy orderBy,
                                    T exampleInstance, String... excludeProperties){
        logger.trace("BEGIN getPage(int, int, OrderBy, Object, String[]): " + startIndex + " | " + endIndex + " | " + orderBy + " | " + exampleInstance + " | " + excludeProperties);
        
        List<T> returnList = new ArrayList<T>();

        try {
        	Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);

            criteria.setFirstResult(startIndex);
            criteria.setMaxResults(endIndex - startIndex);
            
            if (exampleInstance != null){
                Example example = Example.create(exampleInstance);
                example.ignoreCase();
                
                if (excludeProperties != null){
                    for (String exclude : excludeProperties) {
                        example.excludeProperty(exclude);
                    }
                }
                
                // SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
                ryanExampleHandling(criteria, example, exampleInstance);
                
                criteria.add(example);

                // Hook to further extend criteria 
                extendExampleCriteria(criteria, exampleInstance);
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            // Fetch data and remove from session cache
            returnList = criteria.list();

            closeLocalSession(session);
            session=null;

        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the entities by example");
        }
        
        return returnList;
    }
    

    @SuppressWarnings("unchecked")
	public List<T> getPage(int startIndex, int endIndex, OrderBy orderBy){
        logger.trace("BEGIN JPAGenericDAO.getPage()");
        if (startIndex < 0){
            logger.warn("startIndex < 0 - setting to 0"); 
            startIndex = 0;
        }
        if (endIndex < startIndex){
            logger.warn("endIndex < startIndex - setting to " + startIndex);
            endIndex = startIndex;
        }
        String className = persistentClass.getCanonicalName();
        int maxResults = endIndex - startIndex;
        if (maxResults > 0){ // Only remove last row if the maxResults is greater than 0
            maxResults--; // Exclude last row (endIndex)
        }
        
        // Create the query string of the form
        // "from MyClass o order by MyClass.myField
        String queryString = "from " + className + " o ";
        
        Query query = createQuery(queryString + " " + orderBy.getHQLOrderByClause(className, "o"));
        query.setFirstResult(startIndex);
        query.setMaxResults(maxResults); // Use the query with the order by in place
        logger.trace("query: " + query);

        // Fetch data and remove from session cache
        List<T> results = query.getResultList();
        evictList(getSession(), results);
        
        //Prevent memory leak
        getSession().clear();
        
        logger.trace("END JPAGenericDAO.getPage()");        
        return results;
    }
    
    protected Session getSession() {
    	if (session == null) {
	        EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	        session = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
    	} 
    	//logger.info("===== JPAGenericDAO.getSession() Hibernate Session Collection Count = " + session.getStatistics().getCollectionCount() + "====");
    	logger.info("===== JPAGenericDAO.getSession() Hibernate Session Entity Count = " + session.getStatistics().getEntityCount() + "====" );    	
    	return session;
    }
    
    protected Session createLocalSession() {
	    EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
    
    /* get a copy of a criteria (may not work for example instances */
    public Criteria create(DetachedCriteria criteria) {
      try {
          ByteArrayOutputStream baostream = new ByteArrayOutputStream();
          ObjectOutputStream oostream = new ObjectOutputStream(baostream);
          oostream.writeObject(criteria);
          oostream.flush();
          oostream.close();
          ByteArrayInputStream baistream = new ByteArrayInputStream(baostream.toByteArray());
          ObjectInputStream oistream = new ObjectInputStream(baistream);
          DetachedCriteria copy = (DetachedCriteria)oistream.readObject();
          oistream.close();           
          return copy.getExecutableCriteria(getSession());
      } catch(Throwable t) {
          throw new HibernateException(t);
      }
    }
    
    public Criteria createCriteria() {
		logger.info("----- JPAGenericDAO.createCriteria () enter/exit ------");    	
    	return getSession().createCriteria(persistentClass);
    }
    
    @Transactional
    protected Query createQuery(String hql) {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	return  entityManager.createQuery(hql);
    }

    @Transactional
    protected Query createNativeQuery(String sql) {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	return  entityManager.createNativeQuery(sql);
    }
    
    protected void evictList(Session session, Collection<T> list) {
    	logger.info("enter JPAGenericDAO.evictList()");
    	int i = 0;
    	//logger.info("===== JPAGenericDAO.evictList() BEFORE Hibernate Session Collection Count = " + session.getStatistics().getCollectionCount() + "====");
    	logger.info("===== JPAGenericDAO.evictList() BEFORE Hibernate Session Entity Count = " + session.getStatistics().getEntityCount() + "====" );    	
        for (T row : list) {
        	session.evict(row);
        	//logger.info("evict loop counter = " + i++);
        }
    	//logger.info("===== JPAGenericDAO.evictList() AFTER Hibernate Session Collection Count = " + session.getStatistics().getCollectionCount() + "====");
    	logger.info("===== JPAGenericDAO.evictList() AFTER Hibernate Session Entity Count = " + session.getStatistics().getEntityCount() + "====" );        
    	logger.info("exit JPAGenericDAO.evictList()");        
    }
    
    protected void ryanExampleHandling(Criteria criteria, Example example, T exampleInstance) {
    	// Iterate the properties
    	logger.info("---- JPAGenericDAO.ryanExampleHandling enter -----");
    	for (Method m : persistentClass.getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && 
    			m.getReturnType().equals(String.class) &&
    			(m.getParameterTypes().length == 0)) {
				try {
	    			
					String value = (String) m.invoke(exampleInstance);
	    			if (value != null) {
	    				if (value.equalsIgnoreCase("*NULL") || value.equalsIgnoreCase("*ALL") || value.contains("%")) {
		    				String field = Util.lowerCaseFirst(m.getName().substring(3));
		    				logger.debug("Special handling for field: " + field + " - " + value);
		    				// Exclude from the example, since we are handling it
		    				example.excludeProperty(field);
		    				
		    				if (value.equalsIgnoreCase("*ALL")) {
		    					// Nothing more to do than exclude it
		    				} else if (value.equalsIgnoreCase("*NULL")) {
		    					criteria.add(Restrictions.isNull(field));
		    				} else {
		    					//0001650: Wildcards with What if		    				
		    					boolean startWild = value.startsWith("%");
		    					boolean endWild = value.endsWith("%");
		    					//String subValue = value;
		    					if(startWild && endWild){ //Like %12345% or %123%45%
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
		    					}
		    					else if(startWild && !endWild){ //Like $34567 or $34&567
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
		    					}
		    					else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		    					}
		    					else{ //Like 12%345 or else
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		    					}
		    				}
	    				}
	    				
	    				if (m.getName().endsWith("Flag") && value.equals("0")) {
		    				String field = Util.lowerCaseFirst(m.getName().substring(3));
		    				logger.debug("Special handling for flag: " + field + " - " + value);
		    				// Exclude from the example, since we are handling it
		    				example.excludeProperty(field);
	    					criteria.add(Restrictions.or(
	    							Restrictions.isNull(field),
	    							Restrictions.eq(field, value)));
	    				}
	    			}
	    			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    	}
    	logger.info("---- JPAGenericDAO.ryanExampleHandling exit -----");
    }
    
    @SuppressWarnings("unchecked")
    @Transactional
	protected List<String> listCodeCheck(String codeTypeCode){
    	List<String> find = getJpaTemplate().find(" SELECT listCodes.id.codeCode " +
				" FROM ListCodes listCodes " +
				" where listCodes.id.codeTypeCode = ?1 order by listCodes.description ", codeTypeCode );
    	return find;
    }

    @SuppressWarnings("deprecation")
	public String getDBProperties() {
    	StringBuilder sb = new StringBuilder();
    	try {
    		Session localSession = createLocalSession();
    		DatabaseMetaData dbmd = localSession.connection().getMetaData();
    		sb.append("Connection URL: ").append(dbmd.getURL());
    		closeLocalSession(localSession);
    	}
    	catch(Exception e) {}
    	
    	return sb.toString();
    }
    
    @SuppressWarnings("deprecation")
    public Connection getDBConnection() {
    	Connection conn = null;
    	try {
    		Session localSession = createLocalSession();		
    		conn = ((SessionImpl)localSession).connection();
    	}
    	catch(Exception e) {}
    	
    	return conn;
    }

    public void addWildcardCriteria(Criteria criteria, String field, String value){
		if(value!=null && value.length()>0){
			boolean startWild = value.startsWith("%");
			boolean  endWild = value.endsWith("%");
			if(startWild && endWild){ //Like %12345% or %123%45%
				criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
	 		}
	 		else if(startWild && !endWild){ //Like $34567 or $34&567
	 			criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
	 		}
	 		else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
	 			criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
	 		}
	 		else{
	 			criteria.add(Restrictions.eq(field, value).ignoreCase()); 
	 		}
		}
	}
    
    public void addSqlRestriction(Criteria criteria, String field, String value){
		if(value!=null && value.length()>0){
			CharSequence cs = "%";	
			if(value.contains(cs)){
				criteria.add(Restrictions.sqlRestriction("(upper(" + field + ") like '" +value.toUpperCase() + "')"));
			}
			else{
				criteria.add(Restrictions.sqlRestriction("(upper(" + field + ") = '" + value.toUpperCase() + "')"));
			}
		}
	}
}
