package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.VendorNexusDtl;

public interface VendorNexusDetailDAO extends GenericDAO<VendorNexusDtl, Long> {
	public List<VendorNexusDtl> findAllVendorNexusDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFlag) 
		throws DataAccessException;
	public void bulkExpireVendorNexusDetail(String countryCode, String stateCode, Date expirationDate) throws DataAccessException;
	public void bulkExpireRegistrationDetail(String countryCode, String stateCode, Date expirationDate) throws DataAccessException;
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException;
	public boolean isVendorNexusExist(Long entityId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException;
	public boolean isVendorNexusActive(Long entityId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException;
	public List<VendorNexusDtl> getVendorNexusDtlList(Long vendorId, Date glDate, String whereClause, String orderBy) throws DataAccessException;
	public List<NexusDefinitionDetail> getEntityNexusDtlList(Long entityId, Date glDate, String whereClause, String orderBy) throws DataAccessException;
		
}
