package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.support.JpaDaoSupport;

import com.ncsts.domain.BCPBillTransaction;

public class JPABcpSaleTransactionExDAO extends JpaDaoSupport implements BcpSaleTransactionExDAO {
	private static final Log logger = LogFactory
			.getLog(BcpSaleTransactionExDAO.class);

	@PersistenceContext
	protected EntityManager entityManager;

	private Class<BCPBillTransaction> persistentClass;
	private Session session;

	public Class<BCPBillTransaction> getObjectClass() {
		return persistentClass;
	}

	public JPABcpSaleTransactionExDAO() {
		this.persistentClass = BCPBillTransaction.class;
	}

	@SuppressWarnings("unchecked")
	public List<BCPBillTransaction> findByExample(
			BCPBillTransaction exampleInstance, String[] sortField,
			boolean[] ascending, int offset, int count) {
		List<BCPBillTransaction> list = null;

		try {
			Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);

			if(offset >= 0) {
				criteria.setFirstResult(offset);
			}
			
			if (count > 0) {
				criteria.setMaxResults(count);
			}
			
			Example example = Example.create(exampleInstance);
			example.ignoreCase();

			criteria.add(example);

			if(sortField != null && ascending != null && sortField.length == ascending.length) {
				for(int i=0; i<sortField.length; i++) {
					criteria.addOrder((ascending[i]) ? Order.asc(sortField[i]) : Order
							.desc(sortField[i]));
				}
			}

			list = criteria.list();

			closeLocalSession(session);
			session = null;
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}

		return list;
	}
	
	public Long count(
			BCPBillTransaction exampleInstance) {		
		Long returnLong = 0L;

		try {
			Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);

			Example example = Example.create(exampleInstance);
			example.ignoreCase();

			criteria.add(example);

			criteria.setProjection(Projections.rowCount());
			
			returnLong = ((Number)criteria.list().get(0)).longValue();

			closeLocalSession(session);
			session = null;
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}

		return returnLong;
	}

	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate()
				.getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) ? ((HibernateEntityManager) entityManager)
				.getSession() : ((HibernateEntityManager) entityManager
				.getDelegate()).getSession();

		logger.info("===== createLocalSession() Hibernate Session Entity Count = "
				+ localsession.getStatistics().getEntityCount() + "====");
		return localsession;
	}

	protected void closeLocalSession(Session localSession) {
		if (localSession != null && localSession.isOpen()) {
			localSession.clear();
			localSession.close();
		}
	}
}
