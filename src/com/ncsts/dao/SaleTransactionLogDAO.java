package com.ncsts.dao;

import com.seconddecimal.billing.domain.SaleTransactionLog;


public interface SaleTransactionLogDAO {

	public void save(SaleTransactionLog saleTransactionLog);
}
