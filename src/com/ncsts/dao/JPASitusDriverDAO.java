package com.ncsts.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ncsts.dto.SitusDriverSitusDriverDetailDTO;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverAdmin;
import com.ncsts.domain.SitusDriverDetail;

public class JPASitusDriverDAO extends JPAGenericDAO<SitusDriver, Long> implements SitusDriverDAO {

	private Logger logger = Logger.getLogger(this.getClass());
	private Map<String,String> columnMapping = null;
	
	@PersistenceContext 
	private EntityManager entityManager;

	public Session getCurrentSession(){
		return createLocalSession();
	}
	
	@Transactional
	public Long count(SitusDriver exampleInstance) {
		Session localSession = this.createLocalSession();
		Long returnLong = new Long(0);
		try{
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		}
		catch(Exception e){	
			e.printStackTrace();
			logger.error("Error while fetching count() for SitusDrivers " + e.getMessage());
		}
		finally{
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}

	@Transactional
	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<SitusDriver> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<SitusDriver> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<SitusDriver> returnSitusDriver = null;
		try{
			returnSitusDriver = find(criteria.getExecutableCriteria(localSession));
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("Error while finding SitusDrivers by criteria" + e.getMessage());
		}
		finally{
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnSitusDriver;
	}
	
	@Transactional
	public SitusDriver find(SitusDriver exampleInstance) {
		List<SitusDriver> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();

			columnMapping.put("transactiontypecode", "TRANSACTION_TYPE_CODE");
			columnMapping.put("ratetypecode", "RATETYPE_CODE");
			columnMapping.put("methoddeliverycode", "METHOD_DELIVERY_CODE");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				else{
					orderByToken.append(" ORDER BY ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	@SuppressWarnings("unchecked")
	public List<SitusDriver> find(SitusDriver exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		List<SitusDriver> list = new ArrayList<SitusDriver>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error while finding SitusDrivers by criteria" + hbme.getMessage());
		}
		return new ArrayList<SitusDriver>();
	}
	
	private Criteria generateCriteriaBatch(SitusDriver exampleInstance, Session localSession) {
		Criteria criteria = localSession.createCriteria(SitusDriver.class);
        
		if(exampleInstance.getTransactionTypeCode() != null && exampleInstance.getTransactionTypeCode().length()>0) {
		   criteria.add(Restrictions.ilike("transactionTypeCode",exampleInstance.getTransactionTypeCode()+"%"));//Not case sensitive
		}    
		if(exampleInstance.getRatetypeCode() != null && exampleInstance.getRatetypeCode().length()>0) {
			criteria.add(Restrictions.ilike("ratetypeCode",exampleInstance.getRatetypeCode()+"%"));//Not case sensitive
		} 
		if(exampleInstance.getMethodDeliveryCode() != null && exampleInstance.getMethodDeliveryCode().length()>0) {
			criteria.add(Restrictions.ilike("methodDeliveryCode",exampleInstance.getMethodDeliveryCode()+"%"));//Not case sensitive
		} 
		
		if(exampleInstance.getCustomFlag() != null && exampleInstance.getCustomFlag().length()>0) {
			criteria.add(Restrictions.ilike("customFlag",exampleInstance.getCustomFlag()+"%"));//Not case sensitive
		} 
		          
        return criteria;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SitusDriverDetail> findAllSitusDriverDetailBySitusDriverId(Long situsDriverId) throws DataAccessException {
		return getJpaTemplate().find(" select situsDriverDetail from SitusDriverDetail situsDriverDetail where situsDriverDetail.situsDriverId = ? order by situsDriverDetail.driverId", situsDriverId);	  
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void addSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
	
			//Since there are two sequences..
			if(situsDriver.getCustomBooleanFlag()){
				session.save(situsDriver);
				
				// Now do an insert for each
				for (SitusDriverDetail situsDriverDetail : situsDriverDetailList) {
					situsDriverDetail.setSitusDriverId(situsDriver.getSitusDriverId());
					session.save(situsDriverDetail);
				}
			}
			else{
				SitusDriverAdmin situsDriverAdmin = new SitusDriverAdmin();
				BeanUtils.copyProperties(situsDriver, situsDriverAdmin);
				session.save(situsDriverAdmin);
				
				// Now do an insert for each
				for (SitusDriverDetail situsDriverDetail : situsDriverDetailList) {
					situsDriverDetail.setSitusDriverId(situsDriverAdmin.getSitusDriverId());
					session.save(situsDriverDetail);
				}
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while adding a SitusDriver" + e.getMessage() +"\nDriver:"+situsDriver);
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(situsDriver);
			
			// Now do an insert for each user
			for (SitusDriverDetail situsDriverDetail : situsDriverDetailList) {
				session.update(situsDriverDetail);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while updating a SitusDriver" + e.getMessage() +"\nDriver:"+situsDriver);
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteSitusDriver(SitusDriver situsDriver) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			// First delete all details
			String hqlDelete = "delete TB_SITUS_DRIVER_DETAIL WHERE SITUS_DRIVER_ID = " + situsDriver.getSitusDriverId();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
		    hqlDelete = "delete TB_SITUS_DRIVER WHERE  SITUS_DRIVER_ID = " + situsDriver.getSitusDriverId();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while deleting a SitusDriver" + e.getMessage() +"\nDriver:"+situsDriver);
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getDistinctSitusDriverColumn(String distinctColumn, String whereClause) throws DataAccessException {
		//like distinctColumn=transaction_type_code
		//whereClause=transaction_type_code='SALE' AND ratetype_code='0' AND method_delivery_code = '1'
		
		List<String> returnList = new ArrayList<String>();

		
		//City list
		StringBuffer query1 = new StringBuffer();
		query1.append("SELECT DISTINCT " + distinctColumn + " FROM TB_SITUS_DRIVER WHERE (1=1) ");
		if ((whereClause != null) && !whereClause.equals("")) {
			query1.append(" AND " + whereClause);
		}

		Query q1 = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(query1.toString());
		
		List<Object> list1= q1.getResultList();
		Iterator<Object> iter1=list1.iterator();
		if(list1!=null){
			while(iter1.hasNext()){
				String name = (String)iter1.next();	
	            returnList.add(name);                
			}
		}

		return returnList;
	}

	public List<SitusDriverSitusDriverDetailDTO> getSitusDriverDetailWithSitusInfo(String transactionTypeCode, String rateTypeCode, String methodDeliveryCode) {
		Session localSession = createLocalSession();
		List<SitusDriverSitusDriverDetailDTO> result = new ArrayList<SitusDriverSitusDriverDetailDTO>();
		try {
			final String SITUS_DRIVER_SELECT = "SELECT tb_situs_driver_detail.location_type_code as locationTypeCode, tb_situs_driver.method_delivery_code as methodDeliveryCode, " +
					"tb_situs_driver.transaction_type_code as transactionTypeCode, " +
					"tb_situs_driver.ratetype_code as ratetypeCode " +
					"FROM tb_situs_driver, " +
					"tb_situs_driver_detail " +
					"WHERE (tb_situs_driver.transaction_type_code = :transactionTypeCodePar OR tb_situs_driver.transaction_type_code = '*ALL') " +
					"AND (tb_situs_driver.ratetype_code = :rateTypeCodePar OR tb_situs_driver.ratetype_code = '*ALL') " +
					"AND (tb_situs_driver.method_delivery_code = :methodDeliveryCodePar OR method_delivery_code = '*ALL') " +
					"AND tb_situs_driver.situs_driver_id = tb_situs_driver_detail.situs_driver_id " +
					"ORDER BY tb_situs_driver.transaction_type_code DESC, " +
					"tb_situs_driver.ratetype_code DESC, " +
					"tb_situs_driver.method_delivery_code DESC, " +
					"tb_situs_driver_detail.driver_id";

			org.hibernate.Query query = localSession.createSQLQuery(SITUS_DRIVER_SELECT)
                    .addScalar("locationTypeCode")
                    .addScalar("methodDeliveryCode")
                    .addScalar("transactionTypeCode")
                    .addScalar("ratetypeCode")
					.setParameter("transactionTypeCodePar", transactionTypeCode)
					.setParameter("rateTypeCodePar", rateTypeCode)
					.setParameter("methodDeliveryCodePar", methodDeliveryCode)

					.setResultTransformer(Transformers.aliasToBean(SitusDriverSitusDriverDetailDTO.class));
			result = query.list();



		} catch (HibernateException hbe) {
			logger.error("Error while searching SitusDrivers by getSitusDriverDetailWithSitusInfo" + hbe.getMessage());
			hbe.printStackTrace();

		} finally {
			closeLocalSession(localSession);
		}
		return result;
	}
	
}
