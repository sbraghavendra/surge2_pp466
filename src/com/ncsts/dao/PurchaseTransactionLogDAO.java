package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.PurchaseTransactionLog;

public interface PurchaseTransactionLogDAO extends GenericDAO<PurchaseTransactionLog, Long> {
	
	public Long count(long purchtransId);
	public List<PurchaseTransactionLog> getAllGLTransactions(PurchaseTransactionLog exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds)
			throws DataAccessException;
	
	public Long getTransactionCount(PurchaseTransactionLog exampleInstance, 
			String sqlWhere, List<String> nullFileds);
	
	public List<PurchaseTransactionLog> findPurchaseTransactionLogList(
			Long purchtransId) throws DataAccessException;
}
