package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;
import com.ncsts.domain.ProruleAudit;

public interface ProcruleDAO extends GenericDAO<Procrule,Long> {

	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<Procrule> find(Criteria criteria);
	public abstract List<Procrule> find(DetachedCriteria criteria);
	public abstract Procrule find(Procrule exampleInstance);
	public abstract List<Procrule> find(Procrule exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public Long count(Procrule exampleInstance);

	public void addProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public void updateProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public void deleteProcrule(Procrule updateProcrule) throws DataAccessException;
	public void deleteProcruleDetail(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public List<ProcruleDetail> findProcruleDetail(Long procruleId, Date effectiveDate) throws DataAccessException;
	public List<String> findProcruleDetailDistinctDate(Long procruleId) throws DataAccessException;
	public List<Procrule> getAllProcrule() throws DataAccessException;
	public List<ProruleAudit> getAllProcAuditRecords(String sessionId)throws DataAccessException;
	public List<Procrule> getProcrule(String moduleCode, String ruletypeCode) throws DataAccessException;
	public void reorderProcruleList(List<Procrule> updateProcruleList) throws DataAccessException;
	public boolean getIsRuleNameExist(String procruleName, String moduleCode) throws DataAccessException;	
	public boolean getIsRuleNameDateExist(String procruleName, String moduleCode, Date effectiveDate) throws DataAccessException;
	public List<ProruleAudit> getProcRuleRecords(String id) throws DataAccessException;
	public boolean isAllowDeleteRules(Long procruleId) throws DataAccessException;
	public boolean isAllowDeleteDefinitions(Long procruleId, Date effectiveDate) throws DataAccessException;
}
