/**
 * This class handles the actual code that accesses the database for the
 * Jurisdiction Tax Rates.
 * 
 * @author Jay Jungalwala
 */
package com.ncsts.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.services.PurchaseTransactionService;

@Transactional
public class JPAJurisdictionTaxrateDAO extends JPAGenericDAO<JurisdictionTaxrate, Long> implements JurisdictionTaxrateDAO {
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	protected void extendExampleCriteria(Criteria criteria, JurisdictionTaxrate exampleInstance) {
        // Add a restriction if there is a non-null Jurisdiction Taxrate ID
		if ((exampleInstance != null) && (exampleInstance.getJurisdictionTaxrateId() != null)) {
        	logger.debug("Restricting Jurisdiction Taxrate search to Jurisdiction Taxrate ID: " + exampleInstance.getJurisdictionTaxrateId());
			criteria.add(Restrictions.idEq(exampleInstance.getJurisdictionTaxrateId()));
		}
		if ((exampleInstance != null) && (exampleInstance.getJurisdiction() != null)) {
			logger.debug("Restrictin Jurisdiction Taxrate search to Jurisdiction ID: " + exampleInstance.getJurisdiction().getJurisdictionId());
			criteria.add(Restrictions.eq("jurisdiction", exampleInstance.getJurisdiction()));
		}
	}
	
	public Date findByDate(Long jurisdictionId) throws DataAccessException {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	//Query query = entityManager.createQuery(" select min(jurisTaxrate.effectiveDate) from JurisdictionTaxrate jurisTaxrate where jurisTaxrate.jurisdiction.jurisdictionId = ?");
		Query query = entityManager.createQuery("SELECT min(transactionDetail.glDate) FROM PurchaseTransaction transactionDetail WHERE transactionDetail.transactionInd = 'S' AND transactionDetail.suspendInd = 'R' AND transactionDetail.shiptoJurisdictionId = ?");
		query.setParameter(1, jurisdictionId);
        Date date = (Date)query.getSingleResult();
		return date;
	}
	
	public long getCount() throws DataAccessException {
		return count(); 
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(JurisdictionTaxrate instance) throws DataAccessException {
		Transaction tx = null;
		try {
			Session session = createLocalSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(instance);
			tx.commit();
			session.flush();
			session.clear();
						
			//Call New Process Suspended Transaction Service
			//purchaseTransactionService.processSuspendedTransactions(instance.getId(), PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_RATE, false);
			
/* ***!SPROC!*** Commented on 7/26/2016
			CallableStatement cstmnt = con.prepareCall("{call sp_tb_jurisdiction_taxrate_a_i(?)}");
			cstmnt.setLong(1, instance.getId());
			cstmnt.execute();
			cstmnt.close();
			con.commit();
			con.close();
*/			
			closeLocalSession(session);
			session=null;
		} catch (Exception e) {
			logger.error("Jurisdiction tax rate update error: " + e);
			if (tx != null) {
				tx.rollback();
			}
		} 
	}

	public JurisdictionTaxrate findByIdInitialized(final Long id) throws DataAccessException {
		JurisdictionTaxrate jurisdictionTaxrate = getJpaTemplate().execute(
				new JpaCallback<JurisdictionTaxrate>() {
					@Override
					public JurisdictionTaxrate doInJpa(EntityManager em)
							throws PersistenceException {
						JurisdictionTaxrate obj = em.find(JurisdictionTaxrate.class, id);
						if(obj != null && obj.getJurisdiction() != null) {
							obj.getJurisdiction().getId();
						}
						return obj;
					}
				}
		);
		
	    return jurisdictionTaxrate;
	}
	
	public long getTaxrateUsedCount(Long taxrateId) throws DataAccessException {
    	Session session = createLocalSession();
		Criteria criteria = session.createCriteria(PurchaseTransaction.class);
		
		Criterion criterion1 = Restrictions.or(Restrictions.eq("countryTaxrateId", taxrateId), Restrictions.eq("stateTaxrateId", taxrateId));
		Criterion criterion2 = Restrictions.or(criterion1, Restrictions.eq("countyTaxrateId", taxrateId));
		Criterion criterion3 = Restrictions.or(criterion2, Restrictions.eq("cityTaxrateId", taxrateId));	
		Criterion criterion4 = Restrictions.or(criterion3, Restrictions.eq("stj1TaxrateId", taxrateId));
		Criterion criterion5 = Restrictions.or(criterion4, Restrictions.eq("stj2TaxrateId", taxrateId));
		Criterion criterion6 = Restrictions.or(criterion5, Restrictions.eq("stj3TaxrateId", taxrateId));
		Criterion criterion7 = Restrictions.or(criterion6, Restrictions.eq("stj4TaxrateId", taxrateId));
		Criterion criterion8 = Restrictions.or(criterion7, Restrictions.eq("stj5TaxrateId", taxrateId));
		Criterion criterion9 = Restrictions.or(criterion8, Restrictions.eq("stj6TaxrateId", taxrateId));
		Criterion criterion10 = Restrictions.or(criterion9, Restrictions.eq("stj7TaxrateId", taxrateId));
		Criterion criterion11 = Restrictions.or(criterion10, Restrictions.eq("stj8TaxrateId", taxrateId));
		Criterion criterion12 = Restrictions.or(criterion11, Restrictions.eq("stj9TaxrateId", taxrateId));
		Criterion criterion13 = Restrictions.or(criterion12, Restrictions.eq("stj10TaxrateId", taxrateId));	
		criteria.add(criterion13);

		criteria.setProjection(Projections.rowCount());
		
		List< ? > result = criteria.list();	
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
    	return returnLong;
    }

	public JurisdictionTaxrate findByIdRateTypeCode(Long jurisdictionId, String ratetypeCode, Date glDate) {
		JurisdictionTaxrate result = null;
		Session localSession = createLocalSession();
		try {

			org.hibernate.Query query = localSession.createQuery(" from JurisdictionTaxrate where " +
//					" jurisdictionId = :jurisdictionId and " +
					" jurisdiction.jurisdictionId = :jurisdictionId and " +
					" effectiveDate <= :effectiveDate and" +
					" expirationDate >= :expirationDate and "+
			 		(("1".equals(ratetypeCode))?" (ratetypeCode = :ratetypeCode OR ratetypeCode = '0') ":" ratetypeCode = :ratetypeCode ") +
					" order by ratetypeCode desc , effectiveDate desc");

			List<JurisdictionTaxrate> jurList = query.setParameter("jurisdictionId", jurisdictionId)
					.setParameter("effectiveDate", glDate)
					.setParameter("expirationDate", glDate)
					.setParameter("ratetypeCode", ratetypeCode).list();

			if (jurList!=null && jurList.size()!=0) {
				result = jurList.get(0);
			} else {
				throw new HibernateException("Could not find JurisdictionTaxrate!");
			}

		} catch (HibernateException hbe) {
			logger.error(hbe.getMessage());
			hbe.printStackTrace();
		} finally {
			closeLocalSession(localSession);
		}

		return result;
	}

	@Override
	public List<JurisdictionTaxrate> getTaxRateList(Long jurisdictionId, String rateType, Date glDate) {
		List<JurisdictionTaxrate> result = new ArrayList<JurisdictionTaxrate>();
		Session localSession = createLocalSession();
		try {
			/*
			*SELECT jurisdiction_taxrate_id,
			RATETYPE_CODE,
			country_sales_tier1_rate,
			country_use_tier1_rate,
			state_sales_tier1_rate,
			state_sales_tier2_rate,
			state_sales_tier3_rate,
			state_use_tier1_rate,
			state_use_tier2_rate,
			state_use_tier3_rate,
			state_sales_tier1_max_amt,
			state_sales_tier2_min_amt,
			state_sales_tier2_max_amt,
			state_sales_maxtax_amt,
			county_sales_tier1_rate,
			county_use_tier1_rate,
			county_sales_tier1_max_amt,
			county_sales_maxtax_amt,
			city_sales_tier1_rate,
			city_use_tier1_rate,
			city_sales_tier1_max_amt,
			city_sales_tier2_rate,
			city_use_tier2_rate,
			stj1_name,
			stj1_sales_rate,
			stj1_use_rate,
			stj2_name,
			stj2_sales_rate,
			stj2_use_rate,
			stj3_name,
			stj3_sales_rate,
			stj3_use_rate,
			stj4_name,
			stj4_sales_rate,
			stj4_use_rate,
			stj5_name,
			stj5_sales_rate,
			stj5_use_rate,
			STATE_USE_TIER1_SETAMT,
			STATE_USE_TIER2_SETAMT,
			STATE_USE_TIER3_SETAMT,
			STATE_SALES_TIER1_SETAMT,
			STATE_SALES_TIER2_SETAMT,
			STATE_SALES_TIER3_SETAMT,
			tb_jurisdiction_taxrate.effective_date
			FROM tb_jurisdiction_taxrate,
			tb_jurisdiction
			WHERE tb_jurisdiction_taxrate.jurisdiction_id = ?
			AND (ratetype_code = ? OR ratetype_code = '0')
			AND trunc(tb_jurisdiction_taxrate.effective_date) <= ?
			AND trunc(tb_jurisdiction_taxrate.expiration_date) >= ?
			AND tb_jurisdiction_taxrate.jurisdiction_id = tb_jurisdiction.jurisdiction_id
			ORDER BY ratetype_code DESC, effective_date DESC
			* */


			result = localSession.createQuery("select jtrd.jurisdictionTaxrateId, jtrd.ratetypeCode, " +
					"jtrd.countrySalesTier1Rate, jtrd.countryUseTier1Rate, " +
					"jtrd.stateSalesTier1Rate, jtrd.stateSalesTier2Rate, jtrd.stateSalesTier3Rate, " +
					"jtrd.stateUseTier1Rate, jtrd.stateUseTier2Rate, jtrd.stateUseTier3Rate, " +
					"jtrd.stateSalesTier1MaxAmt, jtrd.stateSalesTier2MinAmt, jtrd.stateSalesTier2MaxAmt, jtrd.stateSalesMaxtaxAmt, " +
					"jtrd.stateUseTier1Setamt, jtrd.stateUseTier2Setamt, jtrd.stateUseTier3Setamt, " +
					"jtrd.stateSalesTier1Setamt, jtrd.stateSalesTier2Setamt, jtrd.stateSalesTier3Setamt, " +
					"jtrd.countySalesTier1Rate, jtrd.countyUseTier1Rate, jtrd.countySalesTier1MaxAmt, jtrd.countySalesMaxtaxAmt, " +
					"jtrd.citySalesTier1Rate, jtrd.cityUseTier1Rate, jtrd.citySalesTier1MaxAmt, jtrd.citySalesTier2Rate, jtrd.cityUseTier2Rate, " +
					"jtrd.stj1SalesRate, jtrd.stj1UseRate, " +
					"jtrd.stj2SalesRate, jtrd.stj2UseRate, " +
					"jtrd.stj3SalesRate, jtrd.stj3UseRate, " +
					"jtrd.stj4SalesRate, jtrd.stj4UseRate, " +
					"jtrd.stj5SalesRate, jtrd.stj5UseRate " +
					"from JurisdictionTaxrate as jtrd, Jurisdiction as jur " +
					" WHERE  jtrd.jurisdiction.jurisdictionId = :jurisdictionId AND " +
					" (jtrd.ratetypeCode =:ratetypeCode OR jtrd.ratetypeCode='0') AND " +
					" jtrd.effectiveDate <= :effectiveDate AND " +
					" jtrd.expirationDate >= :expirationDate AND " +
					" jtrd.jurisdiction.jurisdictionId = jur.jurisdictionId " +
					" order by jtrd.ratetypeCode desc, jtrd.effectiveDate desc  ")
					.setParameter("jurisdictionId", jurisdictionId)
					.setParameter("ratetypeCode", rateType)
					.setParameter("effectiveDate", glDate)
					.setParameter("expirationDate", glDate)
					.list();
			//" order by jtrd.ratetypeCode desc, jtrd.effectiveDaten desc ").list();
			////" jtrd.jurisdiction.jurisdictionId = jur.jurisdictionId " +
		} catch (HibernateException hbe) {
			logger.error(hbe.getMessage());
			hbe.printStackTrace();
		} finally {
			closeLocalSession(localSession);
		}

		return result;
	}

}
