package com.ncsts.dao;
/*
 * Author: Jim M. Wilson
 * Created: Aug 20, 2008
 * $Date: 2008-08-21 12:32:57 -0500 (Thu, 21 Aug 2008) $: - $Revision: 1872 $: 
 */

import java.util.List;
import org.springframework.dao.DataAccessException;
import com.ncsts.domain.GlExtractAetna;

public interface GlExtractAetnaDAO extends GenericDAO<GlExtractAetna,Long> {

	public List<GlExtractAetna> getPage(int firstRow, int rows) throws DataAccessException;
	public Long count();
}
