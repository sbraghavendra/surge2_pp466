package com.ncsts.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.Option;
import com.ncsts.domain.TransactionHeader;

public class JPADwColumnDAO extends JpaDaoSupport implements DwColumnDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TransactionHeader> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
		Session session = null;
		Transaction tx = null;
		org.hibernate.Query query = null;
		List<TransactionHeader> transactionHeaders = new ArrayList<TransactionHeader>();
		try {
			session = createLocalSession();
			query = session.createQuery("select df.dwColumnPK.windowName, df.dwColumnPK.userCode, df.dwColumnPK.dataWindowName from TransactionHeader df where df.dwColumnPK.windowName=:windowName and (df.dwColumnPK.userCode='*GLOBAL' or df.dwColumnPK.userCode=:userCode) group by df.dwColumnPK.windowName, df.dwColumnPK.dataWindowName, df.dwColumnPK.userCode order by df.dwColumnPK.userCode, df.dwColumnPK.dataWindowName");
			query.setParameter("windowName",windowName);
			query.setParameter("userCode",userCode);

			TransactionHeader transactionHeader = null;
			List<String[]> objArrayList = (List<String[]>) query.list();
			for(Iterator<?> pairs  = objArrayList.iterator(); pairs.hasNext();){
				Object[] pair = (Object[]) pairs.next();				
				transactionHeader = new TransactionHeader();	
				transactionHeader.setUserCode((String)pair[1].toString());
				transactionHeader.setDataWindowName((String)pair[2].toString());
				transactionHeaders.add(transactionHeader);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session = null;
		}
		
		return transactionHeaders;
	}
	//public List<String> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
	//	return getJpaTemplate().find("select distinct df.dwColumnPK.dataWindowName from TransactionHeader df where df.dwColumnPK.windowName=? and (df.dwColumnPK.userCode='*GLOBAL' or df.dwColumnPK.userCode=?) order by df.dwColumnPK.dataWindowName", windowName, userCode);
	//}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException {
		return getJpaTemplate().find("select distinct df.dwColumnPK.dataWindowName from TransactionHeader df where df.dwColumnPK.windowName=? and df.dwColumnPK.userCode=? order by df.dwColumnPK.dataWindowName", windowName, userCode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TransactionHeader> getColumnsByDataWindowName(String windowName, String dataWindowName) throws DataAccessException {
		return getJpaTemplate().find("select df from TransactionHeader df where df.dwColumnPK.windowName=? and df.dwColumnPK.dataWindowName=? order by df.columnId", windowName, dataWindowName);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TransactionHeader> getColumnsByUserandDataWindowName(String windowName, String dataWindowName,String usercode) throws DataAccessException {
		List<TransactionHeader> test = getJpaTemplate().find("select df from TransactionHeader df where df.dwColumnPK.dataWindowName = ?", dataWindowName);
		List<TransactionHeader> test2 = getJpaTemplate().find("select  df from TransactionHeader df where df.dwColumnPK.windowName=?", windowName);
		return getJpaTemplate().find("select  df from TransactionHeader df where df.dwColumnPK.windowName=? and df.dwColumnPK.dataWindowName=? and df.dwColumnPK.userCode=? order by df.columnId", windowName, dataWindowName,usercode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getUserByFilterNameandUserCode(String windowName, String filterName, String usercode) throws DataAccessException {
		List<String> list = getJpaTemplate().find("select distinct df.dwColumnPK.userCode from TransactionHeader df where df.dwColumnPK.windowName=? and df.dwColumnPK.dataWindowName=? and df.dwColumnPK.usercode=?", windowName, filterName, usercode);
		if(list!=null && list.size()>0){
			return (String)list.get(0);
		}
		else{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName and df.dwColumnPK.userCode=:userCode";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        query.setString("userCode",userCode);
	        int rowCount = query.executeUpdate();
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void updateColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			//Delete columns
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        int rowCount = query.executeUpdate();
	        
	        //Add columns
	        for(TransactionHeader transactionHeader : transactionHeaders){
	        	session.save(transactionHeader);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void updateColumnsByUserAndFilterName(String windowName, String filterName, String selectedUserName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			//Delete columns
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName and df.dwColumnPK.userCode=:selectedUserName";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        query.setString("selectedUserName",selectedUserName);
	        int rowCount = query.executeUpdate();
	        for(TransactionHeader transactionHeader : transactionHeaders){
	        	session.save(transactionHeader);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void addColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        int rowCount = query.executeUpdate();
			
	        //Add columns
	        for(TransactionHeader transactionHeader : transactionHeaders){
	        	session.save(transactionHeader);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException {
		Session session = createLocalSession();
		String hql = "select count(*) from TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName";
		org.hibernate.Query query = session.createQuery(hql);
        query.setString("windowName",windowName);
        query.setString("dataWindowName",filterName);     
        Long count = ((Long)query.uniqueResult());
        closeLocalSession(session);
		session=null;
		
		if(count==0L){
			return false;
		}
		else{
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void addColumnsByUserAndFilterName(String windowName, String filterName,String selectedUserName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName and df.dwColumnPK.userCode=:selectedUserName";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        query.setString("selectedUserName",selectedUserName);
	        int rowCount = query.executeUpdate();
			
	        //Add columns
	        for(TransactionHeader transactionHeader : transactionHeaders){
	        	session.save(transactionHeader);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void deleteColumnsByUserAndFilterName(String windowName, String filterName, String selectedUserName) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			String hql = "delete TransactionHeader df where df.dwColumnPK.windowName=:windowName and df.dwColumnPK.dataWindowName=:dataWindowName and df.dwColumnPK.userCode=:selectedUserName";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("dataWindowName",filterName);
	        query.setString("selectedUserName",selectedUserName);
	        int rowCount = query.executeUpdate();
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
}
