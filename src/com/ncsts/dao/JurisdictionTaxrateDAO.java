/**
 * This interface it the data access abstract methods for Jurisdiction Tax Rates.
 * 
 * @author Jay Jungalwala
 */
package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.JurisdictionTaxrate;


public interface JurisdictionTaxrateDAO extends GenericDAO<JurisdictionTaxrate, Long> {
	
	public Date findByDate(Long jurisdictionId) throws DataAccessException ;
	public long getCount() throws DataAccessException;
	public JurisdictionTaxrate findByIdInitialized(final Long id) throws DataAccessException;
	public long getTaxrateUsedCount(Long taxrateId) throws DataAccessException;

	JurisdictionTaxrate findByIdRateTypeCode(Long jurisdictionTaxrateId, String ratetypeCode, Date glDate);

	List<JurisdictionTaxrate> getTaxRateList(Long jurisdictionId, String rateType, Date glDate);

}
