package com.ncsts.dao;

/**
 * @author AChen
 * 
 */

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;

public class JPAUtilitiesMenuDAO extends JpaDaoSupport implements
		UtilitiesMenuDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}

	private String getErrorMessage(Exception e){
		if(e.getMessage().length()>=100){
			return e.getMessage().substring(0,100);
		}
    	else{ 
    		return e.getMessage();
    	}
	}
	
	@SuppressWarnings("deprecation")
	private String executeCallableStatement(String callableStatement){
		CallableStatement cs  = null;
		Session session = createLocalSession();
		try {
        	// PDB - method is deprecated, but no alternative has been provided yet
        	cs  = session.connection().prepareCall(callableStatement);
            cs.clearParameters();
            cs.execute();
        } 
        catch (HibernateException e) {return getErrorMessage(e);} 
        catch (SQLException e) {return getErrorMessage(e);}	
        finally {
  	      if (cs != null) {
  	        try {cs.close();}
  	        catch (SQLException e) {}
  	      }
  	      
  	      closeLocalSession(session);
  	      session=null;
  	    }
        
		return "";		
	}
	
	public String runMasterControlProgram() throws DataAccessException{
		return executeCallableStatement("{call SP_BATCH()}");//SP_BATCH
	}
	
	public String resumeBusService(Long jobNum, boolean resume) throws DataAccessException{
		return executeCallableStatement("BEGIN DBMS_JOB.BROKEN(" + jobNum.toString() + ", " + Boolean.toString(resume).toUpperCase() + ", SYSDATE); END;");
	}
	
	public String createBusService() throws DataAccessException {
		String callableStatement = 
				"DECLARE " +
				"X NUMBER; " +
				"BEGIN " +
				"  SYS.DBMS_JOB.SUBMIT " +
				"    ( job       => X " +
				"     ,what      => 'sp_batch;' " +
				"     ,next_date => to_date(SYSDATE,'dd/mm/yyyy hh24:mi:ss') " +
				"     ,interval  => 'SYSDATE + 1/1440' " +
				"     ,no_parse  => TRUE " +
				"    ); " +
				"END; ";

		CallableStatement cs  = null;
		Session session = createLocalSession();
		try {
        	// PDB - method is deprecated, but no alternative has been provided yet
        	cs  = session.connection().prepareCall(callableStatement);
            cs.clearParameters();
            cs.execute();
        } 
        catch (HibernateException e) {return getErrorMessage(e);} 
        catch (SQLException e) {return getErrorMessage(e);}	
        finally {
  	      if (cs != null) {
  	        try {cs.close();}
  	        catch (SQLException e) {}
  	      }
  	      
  	      closeLocalSession(session);
  	      session=null;
  	    }
        
		return "";		
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> findBusProperty() throws DataAccessException {	
		Map<String, String> propertyMap = new LinkedHashMap<String, String>();
		
		String sql = "SELECT job, what, next_date, next_sec, broken, interval FROM all_jobs WHERE schema_user = 'STSCORP' AND LOWER(what) = 'sp_batch;'";
		Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(sql);
		
		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		
		if(list!=null){
			if(iter.hasNext()){
				Object[] objarray= (Object[])iter.next();

				BigDecimal job = (BigDecimal) objarray[0];	
				propertyMap.put("JOB", job.toString());
				
				String what = (String) objarray[1];	
				propertyMap.put("WHAT", what);
				
				Timestamp nextdate = (Timestamp) objarray[2];	
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				propertyMap.put("NEXT_DATE", df.format(new Date(nextdate.getTime())));
				
				String nextSec = (String) objarray[3];	
				propertyMap.put("NEXT_SEC", nextSec);
				
				String broken = (String) objarray[4];	
				propertyMap.put("BROKEN", broken);
				
				String interval = (String) objarray[5];	
				propertyMap.put("INTERVAL", interval);     
			}
		}

		return propertyMap;	
	}
	
	public String resetDatawindowSyntax(String userCode) throws DataAccessException{
		Session session = createLocalSession();
		try {
	    	Transaction t = session.beginTransaction();
	    	org.hibernate.Query query = session.createSQLQuery("DELETE FROM TB_PAGE_ATTRIBUTES WHERE USER_ID = :userCode");
			query.setParameter("userCode", userCode);
			int rows = query.executeUpdate();
			logger.debug(rows + " TB_PAGE_ATTRIBUTES rows deleted");
			
			org.hibernate.Query query1 = session.createSQLQuery("DELETE FROM TB_DW_COLUMN WHERE USER_CODE = :userCode");
			query1.setParameter("userCode", userCode);
			int rows1 = query1.executeUpdate();
			logger.debug(rows1 + " TB_DW_COLUMN rows deleted");
	    
			t.commit();
		}
		catch (HibernateException e) {return getErrorMessage(e);}
		finally {
			closeLocalSession(session);
			session=null;
  	    }
		
		return "";	
	}
	
	@SuppressWarnings("deprecation")
	public String buildIndices(String indexParam) throws DataAccessException{
		CallableStatement cs  = null;
		Session session = createLocalSession();
        try {
        	// PDB - method is deprecated, but no alternative has been provided yet
        	cs  = session.connection().prepareCall("{call SP_REINDEX(?)}");
            cs.clearParameters();
            cs.setString(1, indexParam);
            cs.execute();
        } 
        catch (HibernateException e) {return getErrorMessage(e);} 
        catch (SQLException e) {return getErrorMessage(e);}	
        finally {
	      if (cs != null) {
	        try {cs.close();}
	        catch (SQLException e) {}
	      }
	      
	      closeLocalSession(session);
	      session=null;
	    }
        
		return "";
	}
	
	public String reweightMatrices() throws DataAccessException{
		return executeCallableStatement("{call SP_REWEIGHT_MATRIX()}");
	}
	
	public String resetAllSequences() throws DataAccessException{
		return executeCallableStatement("{call SP_MAX_ALL()}");
	}
	
	public String rebuildDriverReferences() throws DataAccessException{
		return executeCallableStatement("{call SP_DRIVER_REFERENCE_MAINT()}");
	}
	
	@SuppressWarnings("deprecation")
	public String truncateBCPTables() throws DataAccessException{
		CallableStatement cs  = null;
		Session session = createLocalSession();
        try {
        	// PDB - method is deprecated, but no alternative has been provided yet
        	// Call a sp with OUT prameter
        	cs  = session.connection().prepareCall("{? = call SP_TRUNCATE_BCP_TABLES()}");
            cs.clearParameters();
            cs.registerOutParameter (1, Types.DOUBLE);
            cs.execute();
            
            logger.debug("Return value is: " + cs.getDouble (1));

            if(cs.getDouble (1) > 0){
              return "Other Users are in application. Cannot Truncate tables.";
            }
        } 
        catch (HibernateException e) {return getErrorMessage(e);} 
        catch (SQLException e) {return getErrorMessage(e);}	
        finally {
	      if (cs != null) {
	        try {cs.close();}
	        catch (SQLException e) {}
	      }
	      
	      closeLocalSession(session);
	      session=null;
	    }
        
		return "";
	}
}
