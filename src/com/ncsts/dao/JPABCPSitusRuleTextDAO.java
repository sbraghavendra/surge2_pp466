package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.BCPSitusRuleText;
import com.ncsts.domain.BCPSitusRuleTextPK;
import com.ncsts.domain.BatchMaintenance;

public class JPABCPSitusRuleTextDAO extends JPAGenericDAO<BCPSitusRuleText, BCPSitusRuleTextPK> implements BCPSitusRuleTextDAO {

	@Override
	@Transactional
	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPSitusRuleText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
			
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}

	@Override
	@Transactional
	public List<BCPSitusRuleText> getPage(BatchMaintenance batch,
			int firstRow, int maxResults) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPSitusRuleText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		criteria.addOrder(Order.asc("id.line"));
		try {
			List<BCPSitusRuleText> returnBCPSitusRuleText = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPSitusRuleText;	
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPSitusRuleText>();
		}
	}
}
