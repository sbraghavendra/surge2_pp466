package com.ncsts.dao;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.*;

import oracle.jdbc.OracleTypes;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LogMemory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.AllocationMatrix;

import javax.persistence.EntityManager;

/**
 * @author Paul Govindan
 *
 */
public class JPAAllocationMatrixDAO extends JPAMatrixDAO<AllocationMatrix> implements AllocationMatrixDAO {
	private Map<String,String> columnMapping = null;
	
	@SuppressWarnings("unchecked")
	public AllocationMatrix findByIdWithDetails(Long id) {
		Session session = getSession();
		AllocationMatrix matrix = (AllocationMatrix) session.load(AllocationMatrix.class, id);
		try{
			// ***** Force init, then evict ***** 
			matrix.getAllocationMatrixDetails().size();
		}catch(Exception ex){
			return null;
		}
		
		session.evict(matrix);
		return matrix;
	}
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	private void buildDriverWhereClause(StringBuffer where, String driverName, String value){
		if (value!=null && !value.trim().equals("")){
			// TO ALLOW EXCACT OR WILDCARD SEARCH 02-04-2009 - BUG# 0003810
			if(value.endsWith("%")){
				where.append(" AND " + driverName + " LIKE '" + value.trim().toUpperCase()+"'");
			}else {
				where.append(" AND " + driverName + " = '" + value.trim().toUpperCase() + "'");
			}
		}
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 30; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}
			
			columnMapping.put("binaryweight", "BINARY_WEIGHT");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	// 02-04-2009 Method Modified for exact search and also wild card search
	// BUG# 0003810
	private String getWhereClauseToken(AllocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			Long allocationMatrixId){
		
		StringBuffer whereClauseExt = new StringBuffer();

    	if (geocode!=null && !geocode.equals("")) {
    		if(geocode.endsWith("%")){
        	appendANDToWhere(whereClauseExt, "UPPER(GEOCODE) LIKE '" + geocode.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "GEOCODE ='" + geocode.toUpperCase() +"'");
			}
	    }
    	if (city!=null && !city.equals("")) {
    		if(city.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(CITY) LIKE '" + city.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "CITY ='" + city.toUpperCase() +"'");	
			}
    	}
    
    	if (county!=null && !county.equals("")) {
    		if(county.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(COUNTY) LIKE '" + county.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "COUNTY = '" + county.toUpperCase() +"'");	
			}
    	}
    
    	if (state!=null && !state.equals("")) {
    		appendANDToWhere(whereClauseExt, "UPPER(STATE) LIKE '" + state.toUpperCase() +"'");
    	}
    	
    	if (country!=null && !country.equals("")) {
    		appendANDToWhere(whereClauseExt, "UPPER(COUNTRY) LIKE '" + country.toUpperCase() +"'");
    	}
    
    	if (zip!=null && !zip.equals("")) {
    		if(zip.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(ZIP) LIKE '" + zip.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "ZIP = '" + zip.toUpperCase() +"'");	
			}
    	}	    

	    //build 30 drivers
	    try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        for (int i = 1; i <= 30; i++) {
	        	name = "Driver" + formatter.format(i);
	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	buildDriverWhereClause(whereClauseExt, getDriverColumnName(name.toLowerCase()), (String)theMethod.invoke(exampleInstance, new Object[]{}));
	        }
        }
        catch (Exception ex) {
        }
        
	    if (allocationMatrixId!=null && !allocationMatrixId.equals("") && allocationMatrixId!=0) {
	    	appendANDToWhere(whereClauseExt, "TB_ALLOCATION_MATRIX.ALLOCATION_MATRIX_ID = " + allocationMatrixId.longValue());
	    }
	    
	    logger.debug("appendANDToWhere: " + whereClauseExt.toString());
		
	    return whereClauseExt.toString();
	}

	//Midtier project code modified - january 2009
	private List<AllocationMatrix> getAllocationMatrixList(ResultSet rs){
		List<AllocationMatrix> result = new ArrayList<AllocationMatrix>();
		
		long pseudoId = 1;
		try{
			while(rs.next()){
				AllocationMatrix allocationMatrix = new AllocationMatrix();
				allocationMatrix.setId(new Long(pseudoId++));
	            
	            try{
	            	NumberFormat formatter = new DecimalFormat("00");
	            	Method theMethod = null;
	            	String name = "";
	            	String value = "";
	            	for (int i = 1; i <= 30; i++) {
	            		name = "setDriver" + formatter.format(i);
	            		value = rs.getString(i);
	            		theMethod = allocationMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
	            		theMethod.setAccessible(true);
		        	
	            		theMethod.invoke(allocationMatrix, new Object[]{value});
	            	}
	            }
	            catch (Exception ex) {
	            	logger.info("ex = " + ex.getMessage());
	            }
	            allocationMatrix.setBinaryWeight(rs.getLong(31));
	
	            result.add(allocationMatrix);                
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return result;
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				else{
					orderByToken.append(" ORDER BY ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	//Midtier project code modified - january 2009
	@SuppressWarnings("unchecked")
	@Transactional
	public List<AllocationMatrix> getAllRecords(AllocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			Long allocationMatrixId,
			OrderBy orderBy, int firstRow, int maxResults) {
		
		List<AllocationMatrix> result = null;

		Connection con = null;
		try {
			String whereClauseToken = getWhereClauseToken(exampleInstance, geocode, city, county, state, country, zip, allocationMatrixId);
			logger.info("****************** "+whereClauseToken);
			String orderByToken = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			if (con != null) {
					CallableStatement cstatement = con
							.prepareCall("{ ? = call SP_ALLOCATION_MTRX_MASTR_LIST(?,?)}");
					System.out.println(" super.getCursor() =  " + super.getCursor(con));
					cstatement.registerOutParameter(1, super.getCursor(con));						
					cstatement.setString(2, whereClauseToken);
					cstatement.setString(3, orderByToken);
					//cstatement.registerOutParameter(3, OracleTypes.CURSOR);
					System.out.println("\t Begin stored procedure call SP_ALLOCATION_MTRX_MASTR_LIST \n");
					long begintime = System.currentTimeMillis();
					cstatement.execute();
					ResultSet rs = (ResultSet) cstatement.getObject(1);
					result = getAllocationMatrixList(rs);
					String time = Long.toString(System.currentTimeMillis() - begintime);
					System.out.println("\t *** Executing Store procedure, took " + time + " milliseconds. \n");
					cstatement.close();
					con.commit();
					con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}

		LogMemory.executeGC();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<AllocationMatrix> getDetailRecords(AllocationMatrix allocationMatrix, OrderBy orderBy) {
		
		List<AllocationMatrix> result = null;
				
		Session session = createLocalSession(); 	
        try {
            Criteria criteria = session.createCriteria(AllocationMatrix.class);
                      
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 30; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = allocationMatrix.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(allocationMatrix, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		criteria.add(Restrictions.eq(name.toLowerCase(), value));
    	        	}
    	        }
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            result = criteria.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
        }
        
        closeLocalSession(session);
        session=null;
			
        LogMemory.executeGC();
   	
		return result;
	}
	
	//Midtier project code modified - january 2009
	@Transactional
	public Long count(AllocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			Long allocationMatrixId) {
		
		Long count = 0l;
		String whereClauseToken = getWhereClauseToken(exampleInstance, geocode, city, county, state, country, zip, allocationMatrixId);

		Connection con = null;
		try {
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				CallableStatement cstatement = con
						.prepareCall("{ ? = call SP_ALLOCATION_MTRX_MASTR_COUNT(?)}");
				System.out.println(" super.getCursor() =  " + super.getCursor(con));					
				cstatement.registerOutParameter(1, super.getCursor(con));					
				cstatement.setString(2, whereClauseToken);
				//cstatement.registerOutParameter(2, OracleTypes.CURSOR);
				System.out.println("\t Begin stored procedure call \n");
				long begintime = System.currentTimeMillis();

				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);
				while(rs.next()){
					count = rs.getLong(1);
				}
				String time = Long.toString(System.currentTimeMillis()
						- begintime);
				System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
				cstatement.close();
				con.commit();
				con.close();

			}
		} catch (SQLException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		return count;
	}

}
