package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.BCPBillTransaction;

public class JPABCPTransactionDetailSaleDAO extends JPAGenericDAO<BCPBillTransaction,Long> implements BCPTransactionDetailSaleDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	@Override
	@Transactional(readOnly = false)
	public void saveSplits(BCPBillTransaction original, 
			List<BCPBillTransaction> splits, 
			List<Long> deletedIds,
			List<BCPBillTransaction> allocationSplits){
		
		Long splitSubTransId = original.getSplitSubtransId();
		if(splitSubTransId == null) {
			splitSubTransId = getNextSplitSubTransId();
			original.setSplitSubtransId(splitSubTransId);
		}
		
		if(deletedIds != null) {
			for(Long id : deletedIds) {
				BCPBillTransaction t = findById(id);
				if(t != null && splitSubTransId.equals(t.getSplitSubtransId())) {
					remove(t);
				}
			}
		}
		
		if(splits != null) {
			for (BCPBillTransaction BCPBillTransaction : splits) {
				if(BCPBillTransaction.getSplitSubtransId() == null) {
					BCPBillTransaction.setSplitSubtransId(splitSubTransId);
				}
				
				if(BCPBillTransaction.getBcpTransactionId() == null) {
					save(BCPBillTransaction);
				}
				else {
					update(BCPBillTransaction);
				}
			}
		}
		
		if(allocationSplits != null) {
			for (BCPBillTransaction BCPBillTransaction : allocationSplits) {
				if(BCPBillTransaction.getSplitSubtransId() == null) {
					BCPBillTransaction.setSplitSubtransId(splitSubTransId);
				}
				
				if(BCPBillTransaction.getBcpTransactionId() == null) {
					save(BCPBillTransaction);
				}
				else {
					update(BCPBillTransaction);
				}
			}
		}
		
		update(original);
	}
	
	@Transactional
	public long getNextSplitSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_SPLIT_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
	
	@Transactional(readOnly = false)
	public long getNextAllocSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_ALLOCATION_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
}
