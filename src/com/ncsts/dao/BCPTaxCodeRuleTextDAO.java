package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRuleText;
import com.ncsts.domain.BCPTaxCodeRuleTextPK;
import com.ncsts.domain.BatchMaintenance;

public interface BCPTaxCodeRuleTextDAO extends GenericDAO<BCPTaxCodeRuleText, BCPTaxCodeRuleTextPK> {
	public Long count(BatchMaintenance batch);
	public List<BCPTaxCodeRuleText> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
