package com.ncsts.dao.sequence;

import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.SequenceGenerator;
import com.ncsts.domain.PurchaseTransaction;

import java.io.Serializable;


public class PurchaseTransactionSequenceGenerator extends SequenceGenerator {
    @Override
    public Serializable generate(SessionImplementor session, Object obj)
            throws HibernateException {
        PurchaseTransaction purchaseTransaction = (PurchaseTransaction) obj;
        if (purchaseTransaction.getPurchtransId() != null && purchaseTransaction.getPurchtransId() > 0) {
            return purchaseTransaction.getPurchtransId();
        } else {
            return super.generate(session, obj);
        }
    }
}
