package com.ncsts.dao.sequence;

import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.SequenceGenerator;
import com.ncsts.domain.BillTransaction;

import java.io.Serializable;


public class BillTransactionSequenceGenerator extends SequenceGenerator {
    @Override
    public Serializable generate(SessionImplementor session, Object obj)
            throws HibernateException {
        BillTransaction billTransaction = (BillTransaction) obj;
        if (billTransaction.getBilltransId() != null && billTransaction.getBilltransId() > 0) {
            return billTransaction.getBilltransId();
        } else {
            return super.generate(session, obj);
        }
    }
}
