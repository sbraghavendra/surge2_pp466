package com.ncsts.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;

import com.ncsts.domain.NexusDefinitionLog;

public class JPANexusDefinitionLogDAO extends JPAGenericDAO<NexusDefinitionLog, Long> implements
		NexusDefinitionLogDAO {

	@Override
	protected void ryanExampleHandling(Criteria criteria, Example example,
			NexusDefinitionLog exampleInstance) {
		//overriding default ryanExampleHandling so that fields with value "*ALL" are not excluded from findByExample query
	}
}
