package com.ncsts.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.RoleMenu;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.VendorItem;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.EntityLocnSetDtl;
import com.ncsts.domain.FisYear;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.Registration;
import com.ncsts.domain.RegistrationDetail;

public class JPAVendorItemDAO extends JPAGenericDAO<VendorItem,Long> implements VendorItemDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

    @Transactional
	public VendorItem findById(Long vendorId) throws DataAccessException {
	    return getJpaTemplate().find(VendorItem.class, vendorId);
	}
	  
     @Transactional
	 public Long count(Long entityId) throws DataAccessException {
		 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		  	Query query = entityManager.createQuery(" select count(*) from EntityItem entityItem where entityItem.parentEntityId = ?");
		  	query.setParameter(1,entityId );
		    Long count = (Long) query.getSingleResult();
			return count;
		  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public VendorItem findByCode(String vendorCode) throws DataAccessException {
		  VendorItem vendorItem = null;
		  List<VendorItem> list = getJpaTemplate().find(" select vendorItem from VendorItem vendorItem where vendorItem.vendorCode = ?" ,vendorCode);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  vendorItem = list.get(0);
			}
		  return vendorItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public VendorItem findByName(String vendorName) throws DataAccessException {
		  VendorItem vendorItem = null;
		  List<VendorItem> list = getJpaTemplate().find(" select vendorItem from VendorItem vendorItem where vendorItem.vendorName = ?" ,vendorName);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  vendorItem = list.get(0);
			}
		  return vendorItem;
	  }
 
    public void saveOrUpdate(VendorItem vendorItem) throws DataAccessException {	
    	Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.saveOrUpdate(vendorItem);
		t.commit();
		closeLocalSession(session);
		session=null;	
	}

	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long id) throws DataAccessException {
		 EntityItem entityItem = getJpaTemplate().find(EntityItem.class, id);
		 if( entityItem != null){
			 getJpaTemplate().remove(entityItem);
		 }
		 else{
			 logger.warn("EntityItem Object not found for deletion");
		 }
	} 

	
	@Transactional
	public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException {
		String entityCode = "";
		EntityItem entityItem = getJpaTemplate().find(EntityItem.class, entityId);
		if( entityItem != null){
			entityCode = entityItem.getEntityCode();
		}
		
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	 	String sql = 	" SELECT SUM(COUNT) FROM ((SELECT COUNT(*) AS COUNT FROM TB_PURCHTRANS WHERE ENTITY_CODE = ?1) " + 
	 					"UNION ALL (SELECT COUNT(*) AS COUNT FROM TB_LOCATION_MATRIX WHERE ENTITY_ID = ?2) " + 
	 					"UNION ALL (SELECT COUNT(*) AS COUNT FROM TB_ENTITY WHERE PARENT_ENTITY_ID = ?3)) ";
	 	
	  	Query query = entityManager.createNativeQuery(sql);
	  	query.setParameter(1,entityCode );
	  	query.setParameter(2,entityId );
	  	query.setParameter(3,entityId );
	    Long count  = ((BigDecimal)query.getSingleResult()).longValue();
	    
	    if(count.intValue()>0){
	    	return false;
	    }
	    else{
	    	return true;
	    }
	}
	
	@SuppressWarnings({ "null" })
	public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException {

	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
	        tx.begin();
	       	if(selectedComponents.substring(0, 1).equals("1"))
			{
			  if(toEntityId<0){
				  
			  String hqldeletDefault = "DELETE FROM tb_entity_default WHERE entity_id = " + fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeletDefault);
			  query.executeUpdate();
			  String hqldeleteQueryDtl="DELETE FROM tb_entity_locn_set_dtl WHERE entity_locn_set_id IN (SELECT entity_locn_set_id FROM tb_entity_locn_set WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_entity_locn_set WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{
				  String hqldeleteDefault = "DELETE FROM tb_entity_default WHERE entity_id = " + toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteDefault);
				  query.executeUpdate();
				  query = session.createQuery ("FROM EntityDefault e WHERE e.entityId = :columnName");
				  query.setParameter("columnName", fromEntityId);
				
					List<EntityDefault> list= query.list();
                 // ScrollableResults itemCursor =query.scroll();
					for (int i = 0; i < list.size(); i++) {
					 EntityDefault entityDefaultOld =(EntityDefault)list.get(i);
					 EntityDefault entityDefaultNew=new EntityDefault();
					 entityDefaultNew.setEntityId(toEntityId);
					 entityDefaultNew.setDefaultCode(entityDefaultOld.getDefaultCode());
					 entityDefaultNew.setDefaultValue(entityDefaultOld.getDefaultValue());
					// entityDefaultNew.setEntityDefaultId(entityDefaultId)(entityDefaultOld.getDefaultValue());
                      session.saveOrUpdate(entityDefaultNew);
                      }
                  
                 String hqldeleteLoc=" DELETE FROM tb_entity_locn_set_dtl WHERE entity_locn_set_id IN (" +
                         "  SELECT entity_locn_set_id FROM tb_entity_locn_set WHERE entity_id = "+ toEntityId.intValue() +")";
                 query=session.createSQLQuery(hqldeleteLoc);
                 query.executeUpdate();
                 String hqldelLocSet= " DELETE FROM tb_entity_locn_set WHERE entity_id ="+  toEntityId.intValue();
                 query=session.createSQLQuery(hqldelLocSet);
                 query.executeUpdate();//SELECT entity_locn_set_id, locn_set_code, name, active_flag FROM tb_entity_locn_set WHERE entity_id = an_from_entity_id)
                 query = session.createQuery ("FROM EntityLocnSet e WHERE e.entityId  = :columnName");
				 query.setParameter("columnName", fromEntityId);
                 List<EntityLocnSet> list1= query.list();
					for (int i = 0; i < list1.size(); i++) {
						EntityLocnSet entityDefaultOld = (EntityLocnSet)list1.get(i);
						EntityLocnSet entityDefaultNew = new EntityLocnSet();
						entityDefaultNew.setEntityId(toEntityId);
						entityDefaultNew.setEntityLocnSetId(entityDefaultOld.getEntityLocnSetId());
						entityDefaultNew.setLocnSetCode(entityDefaultOld.getLocnSetCode());
						entityDefaultNew.setName(entityDefaultOld.getName());
						entityDefaultNew.setActiveFlag(entityDefaultOld.getActiveFlag());
	                    session.saveOrUpdate(entityDefaultNew);
	                    query = session.createQuery ("FROM EntityLocnSetDtl e WHERE e.entityLocnSetId = :columnName");
	  				    query.setParameter("columnName",entityDefaultOld.getEntityLocnSetId());
	  				     List<EntityLocnSetDtl> list2= query.list();
	  				     for (int j = 0; j < list2.size(); j++) {
	  				    	EntityLocnSetDtl entityLoncSetOld =(EntityLocnSetDtl)list2.get(j);
	  				    	EntityLocnSetDtl entityLoncSetNew=new EntityLocnSetDtl();
	  				    	//description, sf_entity_id, poo_entity_id, poa_entity_id, effective_date
	  				    	entityLoncSetNew.setDescription(entityLoncSetOld.getDescription());
	  				    	entityLoncSetNew.setSfEntityId(entityLoncSetOld.getSfEntityId());
	  				    	entityLoncSetNew.setPooEntityId(entityLoncSetOld.getPooEntityId());
	  				    	entityLoncSetNew.setPoaEntityId(entityLoncSetOld.getPoaEntityId());
	  				    	entityLoncSetNew.setEffectiveDate(entityLoncSetOld.getEffectiveDate());
	  				    	session.saveOrUpdate(entityLoncSetNew);
	  				        }
	                      }
                        }
			          }
			//-  Position 2 = Nexus
			if(selectedComponents.substring(1, 2).equals("1"))
			{
			  if(toEntityId<0){ //-- If To Entity is negative, then delete From Entity
			 
			  String hqldeleteQueryDtl="DELETE FROM tb_nexus_def_detail WHERE nexus_def_id IN (SELECT nexus_def_id FROM tb_nexus_def WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_nexus_def WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{// -- Else, perform copies
			         //-- Nexus defs & details
				  String hqldeleteDef="DELETE FROM tb_nexus_def_detail WHERE nexus_def_id IN (SELECT nexus_def_id FROM tb_nexus_def WHERE entity_id = "+toEntityId.intValue()+")";
				  query = session.createSQLQuery(hqldeleteDef);
				  query.executeUpdate();
				  String hqldeleteQueryLocn="DELETE FROM tb_nexus_def WHERE entity_id = "+toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteQueryLocn);
				  query.executeUpdate();
				  //////Master Table
				  query = session.createQuery ("FROM NexusDefinition n WHERE n.entityId  = :columnName");
				  query.setParameter("columnName", fromEntityId);
                  List<NexusDefinition> nexList= query.list();
				  for (int j = 0; j < nexList.size(); j++) {
                   	  NexusDefinition nexOld = (NexusDefinition) nexList.get(j); 
                	  NexusDefinition nexNew=new NexusDefinition();
                	  nexNew.setEntityId(toEntityId);
                	  nexNew.setNexusCountryCode(nexOld.getNexusCountryCode());
                	  nexNew.setNexusStateCode(nexOld.getNexusStateCode());
                	  nexNew.setNexusCounty(nexOld.getNexusCounty());
                	  nexNew.setNexusCity(nexOld.getNexusCity());
                	  nexNew.setNexusStj(nexOld.getNexusStj());
                	  nexNew.setNexusFlag(nexOld.getNexusFlag());
                   	  session.saveOrUpdate(nexNew);
                      query = session.createQuery ("FROM NexusDefinitionDetail e WHERE e.nexusDefId  = :columnName");
  				      query.setParameter("columnName", nexOld.getNexusDefId());
  				      List<NexusDefinitionDetail> nexDetail= query.list();
  				      for (int i = 0; i< nexDetail.size(); i++) {
  				    	NexusDefinitionDetail ndold =(NexusDefinitionDetail) nexDetail.get(i);
  				    	NexusDefinitionDetail ndnew= new  NexusDefinitionDetail();
  				    	ndnew.setNexusDefId(nexNew.getNexusDefId());
  				    	ndnew.setEffectiveDate(ndold.getEffectiveDate());
  				    	ndnew.setExpirationDate(ndold.getExpirationDate());
  				    	ndnew.setNexusTypeCode(ndold.getNexusTypeCode());
  				    	ndnew.setActiveFlag(ndold.getActiveFlag());
  				    	session.saveOrUpdate(ndnew);
  				       } 
                      }
                     }
			        }
			    //  --  Position 3 = Registrations
			 if(selectedComponents.substring(2, 3).equals("1"))
			  {
			  if(toEntityId<0){ //-- If To Entity is negative, then delete From Entity
			 
			  String hqldeleteQueryDtl="DELETE FROM tb_registration_detail WHERE registration_id IN (SELECT registration_id FROM tb_registration WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_registration WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{// -- Else, perform copies
			         //-- Nexus defs & details
				  String hqldeleteDef="DELETE FROM tb_registration_detail WHERE registration_id IN (SELECT registration_id FROM tb_registration WHERE entity_id = "+toEntityId.intValue()+")";
				  query = session.createSQLQuery(hqldeleteDef);
				  query.executeUpdate();
				  String hqldeleteQueryLocn="DELETE FROM tb_registration WHERE entity_id = "+toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteQueryLocn);
				  query.executeUpdate();
				  //////Master Table
				  query = session.createQuery ("FROM Registration n WHERE n.entityId  = :columnName");
				  query.setParameter("columnName", fromEntityId);
                  List<Registration> reglist = query.list();
				    for (int i = 0; i< reglist.size(); i++) {
                  	  Registration regOld = (Registration) reglist.get(i); 
                	  Registration regNew=new Registration();
                	  regNew.setEntityId(toEntityId);
                   	  regNew.setRegCountryCode(regOld.getRegCountryCode());
                	  regNew.setRegStateCode(regOld.getRegStateCode());
                	  regNew.setRegCounty(regOld.getRegCounty());
                	  regNew.setRegCity(regOld.getRegCity());
                	  regNew.setRegStj(regOld.getRegStj());
                      session.saveOrUpdate(regNew);
                      query = session.createQuery ("FROM RegistrationDetail e WHERE e.registrationId  = :columnName1");
  				      query.setParameter("columnName1", regOld.getRegistrationId());
  				      List<RegistrationDetail> regDetaillist = query.list();
  					   for(int j = 0; j < regDetaillist.size(); j++) {
  				    	RegistrationDetail rdetailold =(RegistrationDetail) regDetaillist.get(j);
  				    	RegistrationDetail rdetailnew = new RegistrationDetail();
  				    	rdetailnew.setRegistrationId(regNew.getRegistrationId());
  				    	rdetailnew.setEffectiveDate(rdetailold.getEffectiveDate());
  				    	rdetailnew.setExpirationDate(rdetailold.getExpirationDate());
  				    	rdetailnew.setRegTypeCode(rdetailold.getRegTypeCode());
  				    	rdetailnew.setRegNo(rdetailold.getRegNo());
  				    	rdetailnew.setActiveFlag(rdetailold.getActiveFlag());
  				    	session.saveOrUpdate(rdetailnew);;
  				    	
  				       } 
                      }
                     }
			        }
			
			tx.commit();
			
			//cstmnt = session.connection().prepareCall("{call sp_entity_components_copy(?,?,?)}");
			//cstmnt.setLong(1, fromEntityId.longValue());
			//cstmnt.setLong(2, toEntityId.longValue());
			//cstmnt.setString(3, selectedComponents);
			//cstmnt.execute();
		} catch (Exception e) {
			logger.error("Failed to execute setSubEntityInactive: " + e);
			e.printStackTrace();
			
			
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	@Transactional
	public void deleteVendor(Long vendorId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			//remove(TB_ENTITY);
			String hqlDelete = "DELETE TB_VENDOR WHERE VENDOR_ID = " + vendorId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Delete all existing TB_NEXUS_DEF
			//hqlDelete = "DELETE FROM TB_NEXUS_DEF WHERE ENTITY_ID = " + entityId.intValue();
			//query = session.createSQLQuery(hqlDelete);
			//query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute deleteTEntity: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void disableVendor(Long vendorId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			String hqlUpdate = "UPDATE TB_VENDOR SET ACTIVE_FLAG = '0' WHERE VENDOR_ID = :vendorId";
			query = session.createSQLQuery(hqlUpdate);
			query.setParameter("vendorId", vendorId);
			query.executeUpdate();
			
			hqlUpdate = "UPDATE TB_VENDOR_NEXUS_DTL SET ACTIVE_FLAG = '0' WHERE VENDOR_NEXUS_ID in (SELECT VENDOR_NEXUS_ID FROM TB_VENDOR_NEXUS WHERE VENDOR_ID = :vendorId)";
			query = session.createSQLQuery(hqlUpdate);
			query.setParameter("vendorId", vendorId);
			query.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute disableVendor: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	public boolean isVendorUsed(String vendorCode, String vendorName) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(PurchaseTransaction.class);
		criteria.add(Restrictions.eq("vendorCode", vendorCode));
		criteria.add(Restrictions.eq("vendorName", vendorName));

		criteria.setProjection(Projections.rowCount());
		Long returnLong = ((Number)criteria.list().get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		if(returnLong>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	@Transactional(readOnly = false)
	public Long count(VendorItem exampleInstance, String whereClause) {
		
		String extendWhere = generateWhereClause(exampleInstance);
	
		Long returnLong = 0L;
		try {
			String sqlQuery = "select count(*) count FROM TB_VENDOR WHERE :extendWhere " ;
			sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClause + " and " + extendWhere);
			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return returnLong;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<VendorItem> getAllRecords(VendorItem exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults, String whereClause) {
		
		String sqlQuery = sqlVendor;

		String strOrderBy = getOrderByToken(orderBy);
		String extendWhere = generateWhereClause(exampleInstance);

		sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
		sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClause + " and " + extendWhere);
		sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
		sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));
		
		List<VendorItem> returnList=null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			returnList = parseVendorReturnList(iter);
		}
		return returnList;
		
	}
	
	private Map<String,String> columnMapping = null;
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			
			columnMapping.put("vendorId".toLowerCase(), "VENDOR_ID"); 		
			columnMapping.put("vendorCode".toLowerCase(), "VENDOR_CODE"); 
			columnMapping.put("vendorName".toLowerCase(), "VENDOR_NAME");
			columnMapping.put("geocode".toLowerCase(), "GEOCODE");
			columnMapping.put("addressLine1".toLowerCase(), "ADDRESS_LINE_1");
			columnMapping.put("addressLine2".toLowerCase(), "ADDRESS_LINE_2");
			columnMapping.put("city".toLowerCase(), "CITY");
			columnMapping.put("county".toLowerCase(), "COUNTY");
			columnMapping.put("state".toLowerCase(), "STATE");
			columnMapping.put("zip".toLowerCase(), "ZIP");
			columnMapping.put("zipplus4".toLowerCase(), "ZIPPLUS4");
			columnMapping.put("country".toLowerCase(), "COUNTRY");
			columnMapping.put("activeFlag".toLowerCase(), "ACTIVE_FLAG");
		}
		
		String columnName = columnMapping.get(name.toLowerCase());
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
        }
		
		//Default order
		if(orderByToken.length()==0){
			orderByToken.append(" VENDOR_CODE ASC");
		}

		return orderByToken.toString();
	}
	
	private List<VendorItem> parseVendorReturnList(Iterator<Object> iter){
		List<VendorItem> returnList = new ArrayList<VendorItem>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			VendorItem vendorItem=new VendorItem();
			
			//RowNumber = (BigDecimal)objarray[0];
			
			BigDecimal is = (BigDecimal)objarray[1];
            if (is!=null) vendorItem.setVendorId(is.longValue());
    
            String vendorCode = (String) objarray[2];
            vendorItem.setVendorCode(vendorCode);
            
            String vendorName = (String) objarray[3];
            vendorItem.setVendorName(vendorName);
            
            String geocode = (String) objarray[4];
            vendorItem.setGeocode(geocode);
            
            String addressLine1 = (String) objarray[5];
            vendorItem.setAddressLine1(addressLine1);
            
            String addressLine2 = (String) objarray[6];
            vendorItem.setAddressLine2(addressLine2);
            
            String city = (String) objarray[7];
            vendorItem.setCity(city);
            
            String county = (String) objarray[8];
            vendorItem.setCounty(county);
            
            String state = (String) objarray[9];
            vendorItem.setState(state);
            
            String zip = (String) objarray[10];
            vendorItem.setZip(zip);
            
            String zipplus4 = (String) objarray[11];
            vendorItem.setZipplus4(zipplus4);
            
            String country = (String) objarray[12];
            vendorItem.setCountry(country);
            
           // String activeFlag = (String) objarray[13];
           // vendorItem.setActiveFlag(activeFlag);  
            Character activeFlag = (Character) objarray[13];
            if (activeFlag != null) vendorItem.setActiveFlag(activeFlag.toString());
            
            String updateUserId = (String) objarray[14];
            vendorItem.setUpdateUserId(updateUserId);
            
            String updateTimestamp = (String) objarray[15];
            java.util.Date d = null;
            try{
            	d = f.parse(updateTimestamp);
            }
            catch(Exception e){
            	
            }
            if (d!=null) vendorItem.setUpdateTimestamp(new java.util.Date(d.getTime()));

            returnList.add(vendorItem);                
		}
		
		return returnList;
	}
	
	private Criteria generateCriteriaBatch(EntityItem exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(EntityItem.class);
		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		 Map<String, String > criteriaMap = new LinkedHashMap<String, String >();
		 criteriaMap.put("addressLine1", exampleInstance.getAddressLine1());
		 criteriaMap.put("addressLine2", exampleInstance.getAddressLine2());
		 
		 criteriaMap.put("geocode", exampleInstance.getGeocode());
		 criteriaMap.put("city", exampleInstance.getCity());
		 criteriaMap.put("county", exampleInstance.getCounty());
		 criteriaMap.put("state", exampleInstance.getState());
		 criteriaMap.put("country", exampleInstance.getCountry());
		 criteriaMap.put("zip", exampleInstance.getZip());
		 criteriaMap.put("zipPlus4", exampleInstance.getZipPlus4());
		 
		 for (String entityKey : criteriaMap.keySet()) {
			 field = entityKey;
		 	 value = (String)criteriaMap.get(entityKey);
		 	
		 	 if (value != null && value.trim().length()>0) {
		 		 startWild = value.startsWith("%");
		 		 endWild = value.endsWith("%");
		 		 if(startWild && endWild){ //Like %12345% or %123%45%
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else if(startWild && !endWild){ //Like $34567 or $34&567
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else{
		 			 criteria.add(Restrictions.eq(field, value).ignoreCase()); 
		 		 }
		 	 }
    	}

        return criteria;
	}
	
	private String generateWhereClause(VendorItem exampleInstance) {
		String whereClause = " 1=1 ";

		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		 Map<String, String > criteriaMap = new LinkedHashMap<String, String >();
		 criteriaMap.put("VENDOR_CODE", exampleInstance.getVendorCode());
		 criteriaMap.put("VENDOR_NAME", exampleInstance.getVendorName());
		 
		 criteriaMap.put("ADDRESS_LINE_1", exampleInstance.getAddressLine1());
		 criteriaMap.put("ADDRESS_LINE_2", exampleInstance.getAddressLine2());
		 
		 criteriaMap.put("GEOCODE", exampleInstance.getGeocode());
		 criteriaMap.put("CITY", exampleInstance.getCity());
		 criteriaMap.put("COUNTY", exampleInstance.getCounty());
		 criteriaMap.put("STATE", exampleInstance.getState());
		 criteriaMap.put("COUNTRY", exampleInstance.getCountry());
		 criteriaMap.put("ZIP", exampleInstance.getZip());
		 criteriaMap.put("ZIPPLUS4", exampleInstance.getZipplus4());
		 
		 for (String entityKey : criteriaMap.keySet()) {
			 field = entityKey;
		 	 value = (String)criteriaMap.get(entityKey);
		 	
		 	 if (value != null && value.trim().length()>0) {
		 		 startWild = value.startsWith("%");
		 		 endWild = value.endsWith("%");
		 		 if(startWild || endWild){ //Like %12345 or 12345%	 	
		 			whereClause += " and ( upper("+field+") like '"+ value.toUpperCase() + "' )";
		 		 }
		 		
		 		 else{
		 			whereClause += " and ( upper("+field+") = '"+ value.toUpperCase() + "' )";
		 			
		 		 }
		 	 }
    	}

        return whereClause;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityItem> findAllEntityItemsByLevelAndParent(Long level, Long parentId) throws DataAccessException {
		if(parentId!=null){
			return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityLevelId = ? and entityItem.parentEntityId = ? order by entityItem.entityCode", level, parentId);	  
		}
		else{
			return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityLevelId = ? order by entityItem.entityCode", level);
		}
	}
	
	SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
	private String sqlVendor = 
			"select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " +
				"VENDOR_ID, VENDOR_CODE, VENDOR_NAME, " + 
				"GEOCODE, ADDRESS_LINE_1, ADDRESS_LINE_2, " +
				"CITY, COUNTY, STATE, ZIP, ZIPPLUS4, COUNTRY, ACTIVE_FLAG, " +
				"UPDATE_USER_ID, to_char(UPDATE_TIMESTAMP,'MM/dd/yyyy hh24:mi:ss') as updateTimestamp  " +	
				"FROM ( " +
					"SELECT " +
					"VENDOR_ID, VENDOR_CODE, VENDOR_NAME, " + 
					"GEOCODE, ADDRESS_LINE_1, ADDRESS_LINE_2, " +
					"CITY, COUNTY, STATE, ZIP, ZIPPLUS4, COUNTRY, ACTIVE_FLAG, " +
					"UPDATE_USER_ID, UPDATE_TIMESTAMP " +
					"FROM TB_VENDOR " +
					"WHERE :extendWhere " +
				  ") " +
			") WHERE RowNumber BETWEEN :minrow AND :maxrow ";
	
	@Transactional
    public boolean isNexusDefinitionsUsed(Long vendorID) throws DataAccessException {
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	boolean isUsed = true;
    	
    	try {
    		query = session.createSQLQuery("SELECT COUNT(*) FROM TB_VENDOR_NEXUS WHERE VENDOR_ID = :vendorID");
    		query.setParameter("vendorID", vendorID);
    		List<?> result = query.list();
    		long count = ((BigDecimal) result.get(0)).longValue();
    		
    		if(count == 0) {
    			isUsed = false;	
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return isUsed;
    }
}


