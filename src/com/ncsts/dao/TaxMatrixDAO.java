package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;

/**
 * @author vinays singh
 *
 */
public interface TaxMatrixDAO extends MatrixDAO<TaxMatrix> {

	public List<TaxMatrix> getAllRecords(TaxMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag, String activeFlag, Long taxMatrixId,
			OrderBy orderBy, int firstRow, int maxResults,String moduleCode);

	public Long count(TaxMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag, String activeFlag, Long taxMatrixId,String moduleCode);
	
	public List<TaxMatrix> getDetailRecords(TaxMatrix taxMatrix, OrderBy orderBy,String modulecode);
	
	public List<TaxMatrix> getMatchingTaxMatrixLines(PurchaseTransaction exampleInstance);
}
