package com.ncsts.dao;

import java.math.BigDecimal;


import org.springframework.dao.DataAccessException;

public interface CallGLExtractDao {
	public abstract String callGlExtractProc(String where_clause,
			String file_name, int execution_mode, int return_code)
			throws DataAccessException;

	public BigDecimal getTransactionCount(String GLdate) throws DataAccessException;

	public BigDecimal getRecordCount(String GLdate) throws DataAccessException;
	
	public BigDecimal getTaxAmountCount(String GLdate) throws DataAccessException;
}
