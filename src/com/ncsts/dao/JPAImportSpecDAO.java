package com.ncsts.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ImportSpecUser;
import com.ncsts.domain.ImportSpecUserPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;


public class JPAImportSpecDAO extends JPAGenericDAO<ImportSpec, ImportSpecPK> implements ImportSpecDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private Class<ImportSpec> persistentClass = ImportSpec.class;
	
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

    @Transactional
	public ImportSpec findByPK(ImportSpecPK pk) throws DataAccessException{		
		return getJpaTemplate().find(ImportSpec.class, pk);
	}		
	
	@SuppressWarnings("unchecked")
	public List<ImportSpec> getAllForUserByType(String type) throws DataAccessException {
		Session session = createLocalSession();
		Query q = session.createQuery("FROM ImportSpec spec "
				+ "where spec.id.importSpecCode in ("
				+ "select isu.id.importSpecCode from ImportSpecUser isu"
				+ " where isu.id.userCode = :user and isu.id.importSpecType = :code)" 
				+ " order by spec.id.importSpecCode asc");
		q.setParameter("user", Auditable.currentUserCode());
		q.setParameter("code", type);
		
		List<ImportSpec> returnImportSpec = q.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnImportSpec;
	}
	
	@SuppressWarnings("unchecked")
	public List<ImportSpec> getAllImportSpec() throws DataAccessException {
		List<ImportSpec> list = null;
		Session session = createLocalSession();
		
		// First, get List Codes with CODE_TYPE_CODE='BATCHTYP'
		List<String> batchtypeCodeCodes = null;
		try {
			Criteria criteria = session.createCriteria(ListCodes.class);
			criteria.add(Restrictions.eq("id.codeTypeCode", "IMPDEFTYPE"));
			criteria.setProjection(Projections.property("id.codeCode"));
			batchtypeCodeCodes = criteria.list();
		} catch (HibernateException e) {
			logger.warn("Failed to retrieve ListCodes: " + e);
			e.printStackTrace();
		}
		
		// Now get the list of ImportSpec, optionally with an IMPORT_SPEC_TYPE in the list
		try {
			Criteria criteria = session.createCriteria(ImportSpec.class);
			if ((batchtypeCodeCodes != null) && (batchtypeCodeCodes.size() > 0)) {
				logger.debug("Restricting spec search");
				criteria.add(Restrictions.in("id.importSpecType", batchtypeCodeCodes));
			}
			list = criteria.list();
		} catch (HibernateException e) {
			logger.warn("Failed to retrieve specs: " + e);
			e.printStackTrace();
		}
		
		closeLocalSession(session);
		session=null;

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ImportSpec> getImportSpecByDefCode(String defCode) throws DataAccessException{
		
		List<ImportSpec> find = getJpaTemplate().find(" select ims from ImportSpec ims where ims.importDefinitionCode = ?1 order by ims.importSpecPK.importSpecType,ims.importSpecPK.importSpecCode", defCode );				
		
		if(find != null)
			logger.debug(" from JPAImportSpecDAO.getImportSpecByDefCode :::list size is ::"+find.size());
		else
			logger.debug(" from JPAImportSpecDAO.getImportSpecByDefCode :::list size is ::"+find);
       
		return find;		
	}
	
	@Transactional
	public ImportSpec addImportSpecRecord(ImportSpec instance) throws DataAccessException{
		//TODO: Handle this exception to pass the mapping error to UI
		
		if (instance.getImportSpecPK().getImportSpecType() == null) {
			//TODO Handle this case properly later to catch the exception and notify service layer
           logger.error(" Can not insert Null Values in PK colums");
           logger.info(" User Defined Error::prblem in JPAImportSpecDAO.addImportSpecRecord () ::Can not inser Null Values in PK colums" );
		}
		List<String> find = listCodeCheck("IMPDEFTYPE");
		if(find.contains(instance.getImportSpecType())){
		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		}
		else {
			logger.info("Batch Type Code not in the tb_listCode");
		}
		return instance;		
		
	}
		
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(ImportSpec importSpec) throws DataAccessException{
		List<String> find = listCodeCheck("IMPDEFTYPE");
		if(find.contains(importSpec.getImportSpecType())){
			Session session = createLocalSession();
            Transaction t = session.beginTransaction();
            t.begin();
            session.update(importSpec);
            t.commit();
            closeLocalSession(session);
            session=null;	
		}
		else {
			logger.info("Batch Type Code not in the tb_listCode");
		}
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void delete(ImportSpecPK pk) throws DataAccessException{
		
		ImportSpec lc = findByPK(pk);
		if(lc!=null){
			getJpaTemplate().remove(lc);
		}else{
			// TODO : Handle this exception case
			logger.info("ImportSpec Object not found for deletion");
		}	
	
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////// Methods related to Import Spec User Details ////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	 
	/**
	 * This methid will return List of Used Detail for a given ImportSpec Object
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<User> getImportSpecUserBySpec(ImportSpecPK specPK) throws DataAccessException{
	
		/*1.	 SQL equalent query	
 		select a.user_code, a.user_name,a.active_flag from tb_user a
		where a.user_code in (select b.user_code from tb_import_spec_user b , tb_import_spec c 
					where  b.IMPORT_SPEC_TYPE = c.IMPORT_SPEC_TYPE 
					      and b.IMPORT_SPEC_CODE= c.IMPORT_SPEC_CODE)
		
		*2.select a.user_code, a.user_name,a.active_flag from tb_user a
			where a.user_code in (select b.user_code from tb_import_spec_user b  
			where  b.IMPORT_SPEC_TYPE = 'IP' 
			      and b.IMPORT_SPEC_CODE= 'ANDREW_TEST')
		*/

				
		List<User> find = getJpaTemplate().find(" select usr from User usr where " +
										" usr.userCode in ( select ispusr.importSpecUserPK.userCode from ImportSpecUser ispusr " +
										" where ispusr.importSpecUserPK.importSpecType=?1  " +
										"  and ispusr.importSpecUserPK.importSpecCode = ?2) ", 
										specPK.getImportSpecType(), specPK.getImportSpecCode());
										
		
		if(find != null)
			logger.debug(" from JPAListCodesDAO.getImportSpecUserBySpec :::list size is ::"+find.size());
		else
			logger.debug(" from JPAListCodesDAO.getImportSpecUserBySpec :::list size is ::"+find);
       
		return find;		
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
    public void updateImportSpecUserBySpec(ImportSpec importSpec, List<String> userCodeList) throws DataAccessException {
		String importSpecType = importSpec.getImportSpecPK().getImportSpecType();
		String importSpecCode = importSpec.getImportSpecPK().getImportSpecCode();
		
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			// First delete all existing specs
			String hqlDelete = "delete ImportSpecUser where id.importSpecType = :ist and id.importSpecCode = :isc";
			Query query = session.createQuery(hqlDelete);
			query.setString("ist", importSpecType);
			query.setString("isc", importSpecCode);
			query.executeUpdate();
			
			// Now do an insert for each user
			for (String userCode : userCodeList) {
				if (StringUtils.isEmpty(userCode)) {
					continue;
				}

				ImportSpecUser exampleInstance =
					new ImportSpecUser(new ImportSpecUserPK(importSpecType, importSpecCode, userCode));
				session.persist(exampleInstance);
			}
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_IMPORT_SPEC_UESR: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	private HibernateEntityManager getEntityManager() {
		return (HibernateEntityManager)getJpaTemplate().getEntityManagerFactory().createEntityManager();
	}

	
    @SuppressWarnings("unchecked")
    @Transactional
	protected List<String> listCodeCheck(String codeTypeCode){
    	List<String> find = getJpaTemplate().find(" SELECT listCodes.id.codeCode " +
				" FROM ListCodes listCodes " +
				" where listCodes.id.codeTypeCode = ?1 order by listCodes.description ", codeTypeCode );
    	return find;
    }

    @Transactional(readOnly=true)
	public Long count(ImportSpec exampleInstance) {
		Long ret = 0L;	
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(ImportSpec.class);
			if(exampleInstance.getImportSpecType()!=null && exampleInstance.getImportSpecType().length()>0){
        		criteria.add(Restrictions.eq("importSpecPK.importSpecType", exampleInstance.getImportSpecType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importSpecPK.importSpecCode", exampleInstance.getImportSpecCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());
			
			criteria.setProjection(Projections.rowCount());
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
		} catch (Exception e) {
				e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportSpec> getAllRecords(ImportSpec exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<ImportSpec> list =  null;
		Session session = createLocalSession();
		try { 	
			Criteria criteria = session.createCriteria(ImportSpec.class);
			if(exampleInstance.getImportSpecType()!=null && exampleInstance.getImportSpecType().length()>0){
        		criteria.add(Restrictions.eq("importSpecPK.importSpecType", exampleInstance.getImportSpecType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importSpecPK.importSpecCode", exampleInstance.getImportSpecCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());

			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                    	if(field.getName().equalsIgnoreCase("importSpecType") || field.getName().equalsIgnoreCase("importSpecCode")){
                    		criteria.addOrder( Order.asc("importSpecPK." + field.getName()) );
                    	}
                    	else{
                    		criteria.addOrder( Order.asc(field.getName()) );
                    	}
                    } else {
                    	if(field.getName().equalsIgnoreCase("importSpecType") || field.getName().equalsIgnoreCase("importSpecCode")){
                    		criteria.addOrder( Order.desc("importSpecPK." + field.getName()) );
                    	}
                    	else{
                    		criteria.addOrder( Order.desc(field.getName()) );
                    	}
                    }
                }
            }
            else{
            	criteria.addOrder(Order.asc("importSpecPK.importSpecCode"));
            }
            
            list = criteria.list();
		} 
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}
}
