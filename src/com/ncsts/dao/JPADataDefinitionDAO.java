package com.ncsts.dao;
/**
 * @author Muneer Basha
 */
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.DataDefinitionTable;

public class JPADataDefinitionDAO extends JpaDaoSupport implements
  			DataDefinitionDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

	@SuppressWarnings("unchecked")
	@Transactional
	public List<DataDefinitionTable> getAllDataDefinitionTable() throws DataAccessException {
		logger.debug("getAllDataDefinitionTable");
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query query = entityManager.createQuery("select dataDefinitionTable from DataDefinitionTable dataDefinitionTable order by dataDefinitionTable.tableName ");
		return query.getResultList();
	}

	@Transactional
	public DataDefinitionTable findById(String tableName) throws DataAccessException {
		return getJpaTemplate().find(DataDefinitionTable.class, tableName);
	}	

	@SuppressWarnings("unchecked")
	public List<DataDefinitionColumn> getAllDataDefinitionColumn(DataDefinitionColumn exampleInstance, int count, String... excludeProperty) throws DataAccessException{
		List<DataDefinitionColumn> list=null;
		Session session = createLocalSession();
        
        try {
            Criteria criteria = session.createCriteria(DataDefinitionColumn.class);
            if (count > 0) {
            	criteria.setMaxResults(count);
            }
            Example example = Example.create(exampleInstance);
            for (String exclude : excludeProperty) {
                example.excludeProperty(exclude);
            }
            criteria.add(example);
            list = criteria.list();
            
        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the DataDefinitionColumn by example");
        }
        
        closeLocalSession(session);
        session=null;
        
        return list; 	
   }

	@SuppressWarnings("unchecked")
	@Transactional
	public List <DataDefinitionColumn> getAllDataDefinitionColumnByTable(String tableName) 
	                                    throws DataAccessException{
		  logger.debug(" in DAO :::::tableName:::"+tableName) ;
		  List<DataDefinitionColumn> find = getJpaTemplate().find(" SELECT ddc " + 
			            " FROM DataDefinitionColumn ddc " + 
			            " WHERE ddc.dataDefinitionColumnPK.tableName = ?1 order by ddc.dataDefinitionColumnPK.columnName asc",             
			            tableName);
		    
		  if(tableName.equalsIgnoreCase("TB_PURCHTRANS")){
			 //CCH 
			 DataDefinitionColumn cchTaxcatCodeColumn = null;
			 DataDefinitionColumn cchGroupCodeColumn = null;
			 DataDefinitionColumn cchItemCodeColumn = null; 
			 
			 for (DataDefinitionColumn ddc : find) {
      			String columnName = ddc.getDataDefinitionColumnPK().getColumnName();
      			if (columnName.equalsIgnoreCase("CCH_TAXCAT_CODE")){
      				cchTaxcatCodeColumn = ddc;
      			}
      			else if(columnName.equalsIgnoreCase("CCH_GROUP_CODE")){
      				cchGroupCodeColumn = ddc;
      			}
      			else if(columnName.equalsIgnoreCase("CCH_ITEM_CODE")){
      				cchItemCodeColumn = ddc;
      			}
      		}
			 
			//To prevent ConcurrentModificationException
			if(cchTaxcatCodeColumn!=null) find.remove(cchTaxcatCodeColumn);
			if(cchGroupCodeColumn!=null) find.remove(cchGroupCodeColumn);
			if(cchItemCodeColumn!=null) find.remove(cchItemCodeColumn);
		  }
			
		return find;	  
	}
	  
	@Transactional
	public DataDefinitionColumn findByDataDefinitionColumnPK(DataDefinitionColumnPK dataDefinitionColumnPK) 
	                                                throws DataAccessException{
		 DataDefinitionColumn ddc = getJpaTemplate().find(DataDefinitionColumn.class, dataDefinitionColumnPK);
		 return ddc;
	}
	  
	@Transactional
	public DataDefinitionTable addNewDataDefinitionTable(DataDefinitionTable instance) throws DataAccessException{
		if (instance.getTableName() == null) {
			//TODO Handle this case properly later 
	          logger.error(" Cannot insert Null Values into DataDefinitionTable ");
		}
		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		logger.debug(" Add Row to Data Definition table is successfull");
		return instance;		  
	}
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(DataDefinitionTable dataDefinitionTable) throws DataAccessException{
	  
		logger.debug("JPADataDefinitionDAO -> update: " + dataDefinitionTable.getTableName());
	  
		Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.update(dataDefinitionTable);
        t.commit();
        
        closeLocalSession(session);
        session=null;
	}
  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void delete(String tableName) throws DataAccessException{
		DataDefinitionTable dataDefinitionTable = getJpaTemplate().find(DataDefinitionTable.class, tableName);
		if(dataDefinitionTable !=null){
			getJpaTemplate().remove(dataDefinitionTable);
		}else {
			logger.info("DataDefinitionTable Object not found for deletion");
		}
	}
	  
	    @SuppressWarnings("unchecked")
	    @Transactional
		public List<DataDefinitionColumn> getAllDataDefinitionColumnByColumnName(String columnName) 
		                                    throws DataAccessException{
			  logger.debug(" in DAO :::::tableName:::"+columnName) ;
			  List<DataDefinitionColumn> find = getJpaTemplate().find(" SELECT ddc " + 
				            " FROM DataDefinitionColumn ddc " + 
				            " WHERE ddc.dataDefinitionColumnPK.tableName = 'TB_PURCHTRANS' "+
				            " and ddc.dataDefinitionColumnPK.columnName like ?1 ",             
				            columnName);
			return find;	  
		}
	
		@SuppressWarnings("unchecked")
		@Transactional
		public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDesc(String columnName, String description, String dataType) throws DataAccessException {
			List<DataDefinitionColumn>list=null;
			logger.info("Inside JPADATADEFINITIONDAO getAllDataDefinitionByColumnNameAndDesc");
			/*List <DataDefinitionColumn> find= getJpaTemplate().
				find("select ddc from DataDefinitionColumn ddc where ddc.dataDefinitionColumnPK.tableName = 'TB_PURCHTRANS' and " +
						"ddc.dataDefinitionColumnPK.columnName like ?1 and ddc.description like ?2 and ddc.dataType like ?3", columnName, description, dataType);
			return find;*/
			Session session = createLocalSession();
			
			try {
				Criteria criteria = session.createCriteria(DataDefinitionColumn.class);
				criteria.add(Restrictions.eq("dataDefinitionColumnPK.tableName", "TB_PURCHTRANS")); 
				if(columnName!=null){
					criteria.add(Restrictions.ilike("dataDefinitionColumnPK.columnName", columnName));
				}
				if(description!=null){
					criteria.add(Restrictions.ilike("description", description));
				}
				if(dataType!=null){
					criteria.add(Restrictions.ilike("dataType", dataType));
				}
				criteria.addOrder(Order.asc("dataDefinitionColumnPK.columnName"));
				list = criteria.list();
				
			} catch (HibernateException e) {
				logger.info("Hibernate Exception "+e.getMessage());
				e.printStackTrace();
			}
			
			closeLocalSession(session);
			session=null;

			return list;
		}  
	
		@SuppressWarnings("unchecked")
		public List<DataDefinitionColumn> getAllDataDefinitionByColumnNamePCO() throws DataAccessException {
			
			 logger.debug(" Inside getAllDataDefinitionByColumnNamePCO");
			 String query=(" SELECT ddc " + 
			            " FROM DataDefinitionColumn ddc " + 
			            " WHERE ddc.dataDefinitionColumnPK.tableName = 'TB_PURCHTRANS' OR ddc.dataDefinitionColumnPK.tableName='TB_SALETRANS' order by ddc.dataDefinitionColumnPK.columnName asc");
			
			  List<DataDefinitionColumn> find = getJpaTemplate().find(query);
			    
			 
				 //CCH 
				 DataDefinitionColumn cchTaxcatCodeColumn = null;
				 DataDefinitionColumn cchGroupCodeColumn = null;
				 DataDefinitionColumn cchItemCodeColumn = null; 
				 
				 for (DataDefinitionColumn ddc : find) {
	      			String columnName = ddc.getDataDefinitionColumnPK().getColumnName();
	      			if (columnName.equalsIgnoreCase("CCH_TAXCAT_CODE")){
	      				cchTaxcatCodeColumn = ddc;
	      			}
	      			else if(columnName.equalsIgnoreCase("CCH_GROUP_CODE")){
	      				cchGroupCodeColumn = ddc;
	      			}
	      			else if(columnName.equalsIgnoreCase("CCH_ITEM_CODE")){
	      				cchItemCodeColumn = ddc;
	      			}
	      	
				 
				//To prevent ConcurrentModificationException
				if(cchTaxcatCodeColumn!=null) find.remove(cchTaxcatCodeColumn);
				if(cchGroupCodeColumn!=null) find.remove(cchGroupCodeColumn);
				if(cchItemCodeColumn!=null) find.remove(cchItemCodeColumn);
			  }
				
			return find;
			
		}  
		
	@SuppressWarnings("unchecked")
	public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDescDriver(String columnName, String description, String dataType,String driverCode) throws DataAccessException {
		List<DataDefinitionColumn>list=null;
		logger.info("Inside JPADATADEFINITIONDAO getAllDataDefinitionByColumnNameAndDesc");
		/*List <DataDefinitionColumn> find= getJpaTemplate().
			find("select ddc from DataDefinitionColumn ddc where ddc.dataDefinitionColumnPK.tableName = 'TB_TRANSACTION_DETAIL' and " +
					"ddc.dataDefinitionColumnPK.columnName like ?1 and ddc.description like ?2 and ddc.dataType like ?3", columnName, description, dataType);
		return find;*/
		Session session = createLocalSession();
		
		try {
			Criteria criteria = session.createCriteria(DataDefinitionColumn.class);//Fixed for issue 0004622
			//if(driverCode.equalsIgnoreCase("T")){
			//criteria.add(Expression.or(Expression.eq("dataDefinitionColumnPK.tableName", "TB_TRANS_USER"), Expression.eq("dataDefinitionColumnPK.tableName", "TB_TRANSACTION_DETAIL")));
			//}else
			if(driverCode.equalsIgnoreCase("GSB")){//PP-396
				criteria.add(Restrictions.eq("dataDefinitionColumnPK.tableName", "TB_BILLTRANS"));
			}else{
				criteria.add(Restrictions.eq("dataDefinitionColumnPK.tableName", "TB_PURCHTRANS")); 	
			}
			if(columnName!=null){
				criteria.add(Restrictions.ilike("dataDefinitionColumnPK.columnName", columnName));
			}
			if(description!=null){
				criteria.add(Restrictions.ilike("description", description));
			}
			if(dataType!=null){
				criteria.add(Restrictions.ilike("dataType", dataType));
			}
			
			//Currently VARCHAR2, and CHAR in PinPoint, we also want to include VARCHAR, NCHAR, NVARCHAR2 	
			criteria.add(Restrictions.ilike("dataType", "%CHAR%"));
			
			criteria.addOrder(Order.asc("dataDefinitionColumnPK.columnName"));
			list = criteria.list();
			
		} catch (HibernateException e) {
			logger.info("Hibernate Exception "+e.getMessage());
			e.printStackTrace();
		}
		
		closeLocalSession(session);
		session=null;

		return list;
	}  
	  
    @Transactional
	public DataDefinitionColumn addNewDataDefinitionColumn(DataDefinitionColumn instance) throws DataAccessException{
    	
    	logger.info("addNewDataDefinitionColumn instance column: " + instance.getDataDefinitionColumnPK().getTableName() + " Column: " + instance.getDataDefinitionColumnPK().getColumnName());
    	
		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		logger.debug(" Add Row to Data Column table is successfull");
		return instance;		  
	}  
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(DataDefinitionColumn dataDefinitionColumn) throws DataAccessException{
	  
		logger.info("JPADataDefinitionDAO -> update getTableName: " + dataDefinitionColumn.getDataDefinitionColumnPK().getTableName());
		logger.info("JPADataDefinitionDAO -> update getColumnName: " + dataDefinitionColumn.getDataDefinitionColumnPK().getColumnName());
	  
		Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.update(dataDefinitionColumn);
        t.commit();
 
        closeLocalSession(session);
        session=null;
	}
  
	@SuppressWarnings("deprecation")
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void delete(DataDefinitionColumnPK columnPK) throws DataAccessException{
		
			//Midtier project code modified - january 2009
			Session session = createLocalSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				
				org.hibernate.Query query = session.createQuery("delete from DataDefinitionColumn d where d.id.tableName = '"+columnPK.getTableName()+"' and d.id.columnName = '"+columnPK.getColumnName()+"'");
				/*query.setString(1, columnPK.getTableName());
				query.setString(2, columnPK.getColumnName());*/
				int row = query.executeUpdate();
				String tableName=columnPK.getTableName().toLowerCase();
			
				if (row > 0) {
								
						if(tableName.equals("TB_PURCHTRANS")){
							org.hibernate.Query query1 = session.createQuery("DELETE FROM DriverReference d WHERE d.id.transDetailColName = '"+columnPK.getColumnName()+"'");
							query1.executeUpdate();
						}
						/*CallableStatement cstatement = con
								.prepareCall("{call sp_tb_data_def_column_a_d (?,?)}");
						cstatement.setString(1, columnPK.getTableName());
						cstatement.setString(2, columnPK.getColumnName());
						System.out
								.println("\t Begin stored procedure call sp_tb_data_def_column_a_d \n");
						long begintime = System.currentTimeMillis();
						cstatement.execute();
						String time = Long.toString(System.currentTimeMillis()
								- begintime);
						System.out
								.println("\t *** Executing Store procedure, took "
										+ time + " milliseconds. \n");
						cstatement.close();
						con.commit();
						con.close();*/
						
						
					
				} else {
					logger
							.warn("DataDefinitionColumn Object not found for deletion");
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					logger.debug("Rolling back");
					tx.rollback();
				}
			}
			
			closeLocalSession(session);
			session=null;

		/*} 
		else {
			logger.warn("DataDefinitionColumn Object not found for deletion");
		}*/	
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getColumnNameList(String tableName) throws DataAccessException{
		String sql = "SELECT COLUMN_NAME FROM TB_VIEW_COLUMN_NAMES WHERE TABLE_NAME = UPPER(:tableName) ORDER BY COLUMN_NAME";
	
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
		query.setParameter("tableName", tableName);
		
		List<String> returnList = query.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getTableNameList() throws DataAccessException{
		String sql = "SELECT TABLE_NAME FROM TB_VIEW_TABLE_NAMES ORDER BY TABLE_NAME";
	
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
	
		List<String> returnList = query.list();
		
		closeLocalSession(session);
		session=null;

		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDataTypeList() throws DataAccessException{
		String sql = "SELECT CODE_CODE FROM TB_LIST_CODE WHERE (CODE_TYPE_CODE = 'DATATYPE') ORDER BY CODE_CODE ASC";
	
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);

		List<String> returnList = query.list();
		
		closeLocalSession(session);
		session=null;

		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDescriptionColumnList(String tableName) throws DataAccessException{
		String sql = "select COLUMN_NAME from TB_DATA_DEF_COLUMN where TABLE_NAME = :tableName ORDER BY COLUMN_NAME";
	
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
		query.setParameter("tableName", tableName);

		List<String> returnList = query.list();
		
		closeLocalSession(session);
		session=null;

		return returnList;
	}
	
	 	@Transactional
	    public List getExportDataDefinitionTables()
	        throws DataAccessException{
	        logger.debug("getAllDataDefinitionTable");
	        EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	        javax.persistence.Query query = entityManager.createQuery("select dataDefinitionTable from DataDefinitionTable dataDefinitionTable where dataDefinitionTable.exportFlag=?  order by dataDefinitionTable.tableName");
	        query.setParameter(1, "1");
	        return query.getResultList();
	    }
	 	@SuppressWarnings("unchecked")
	 	public DataDefinitionTable getDataDefinitionByConfigType(String configFileType) throws DataAccessException{
	 		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
			Query query = entityManager.createQuery("select dataDefinitionTable from DataDefinitionTable dataDefinitionTable where dataDefinitionTable.configFileTypeCode=?");
			query.setParameter(1, configFileType);
			List<DataDefinitionTable> dataDefList= query.getResultList();
			if(dataDefList!=null && dataDefList.size()>0)
				return dataDefList.get(0);
			return null;
	 	}

	@Override
	public List<DataDefinitionTable> getAllImportExportTypes() throws DataAccessException {
		Session localSession = createLocalSession();
		org.hibernate.Query query = localSession.createQuery("select dataDefinitionTable from DataDefinitionTable dataDefinitionTable where dataDefinitionTable.configFileTypeCode is not null and (dataDefinitionTable.importFlag='1' OR dataDefinitionTable.exportFlag='1') order by dataDefinitionTable.description");
		List<DataDefinitionTable> dataDefList= query.list();

		closeLocalSession(localSession);


		return dataDefList;
	}
}
