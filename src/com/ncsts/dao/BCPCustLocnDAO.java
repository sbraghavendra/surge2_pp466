package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.BatchMaintenance;

public interface BCPCustLocnDAO extends GenericDAO<BCPCustLocn, Long> {
	public Long count(BatchMaintenance batch);
	public List<BCPCustLocn> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
	public List<BCPCustLocn> getPage(BatchMaintenance batch, int firstRow,int maxResults, boolean setOrder) throws DataAccessException;
	public List<BCPCustLocn> findByBatchId(Long batchId) throws DataAccessException;
}
