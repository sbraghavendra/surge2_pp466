package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRule;
import com.ncsts.domain.BatchMaintenance;

public interface BCPTaxCodeRuleDAO extends GenericDAO<BCPTaxCodeRule, Long> {
	public Long count(BatchMaintenance batch);
	public List<BCPTaxCodeRule> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
