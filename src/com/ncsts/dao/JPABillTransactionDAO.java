package com.ncsts.dao;

import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.CustEntity;

import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE;

public class JPABillTransactionDAO extends JpaDaoSupport implements BillTransactionDAO {
    
    @Override
    public void save(BillTransaction transaction) {
    	System.out.println("BilltransId: " + transaction.getBilltransId());
    	System.out.println("getID: " + transaction.getId());
        Session session = createLocalSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            if (transaction.getBilltransId()==null) {
                session.persist(transaction);            
            } else {
              Long count =  (Long)session.createCriteria(BillTransaction.class)
                        .add(Restrictions.eq("billtransId", transaction.getBilltransId()))
                        .setProjection(Projections.rowCount()).uniqueResult();
                if (count!=null && count>0L) {
                    session.merge(transaction);
                } else {
                    session.save(transaction);
                }
            }
            System.out.println("Before commit");
            tx.commit();
            System.out.println("After commit");
            System.out.println("After commit billtrans id " + transaction.getBilltransId());
            
            if(!isBilltransExist("TB_BILLTRANS_USER", transaction.getBilltransId())) {
            	insertBilltransTable(session, "TB_BILLTRANS_USER", transaction.getBilltransId());
            }
            if(!isBilltransExist("TB_BILLTRANS_JURDTL", transaction.getBilltransId())){
            	insertBilltransTable(session, "TB_BILLTRANS_JURDTL", transaction.getBilltransId());
            }
            if(!isBilltransExist("TB_BILLTRANS_LOCN", transaction.getBilltransId())){
            	insertBilltransTable(session, "TB_BILLTRANS_LOCN", transaction.getBilltransId());
            }
        } catch (Exception hbe) {
            logger.error("Error while persisting BillTransaction!" + hbe.getMessage());
            hbe.printStackTrace();

            transaction.setErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
            transaction.setFatalErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
            transaction.setErrorText("Failed to write BillTransaction! \n"+hbe.getMessage());

            if (tx!=null)
                tx.rollback();
        } finally {
            closeLocalSession(session);
        }
    }
    
    public BillTransaction findById(Long billtransId) {
        return getJpaTemplate().find(BillTransaction.class, billtransId);
    }

    protected Session createLocalSession() {
        EntityManager entityManager = getJpaTemplate()
                .getEntityManagerFactory().createEntityManager();
        Session localsession = (entityManager instanceof HibernateEntityManager) ? ((HibernateEntityManager) entityManager)
                .getSession() : ((HibernateEntityManager) entityManager
                .getDelegate()).getSession();

        logger.info("===== createLocalSession() Hibernate Session Entity Count = "
                + localsession.getStatistics().getEntityCount() + "====");
        return localsession;
    }

    protected void closeLocalSession(Session localSession) {
        if (localSession != null && localSession.isOpen()) {
            localSession.clear();
            localSession.close();
        }
    }
    
    public Long findLastBillTransactionId(){
    	Session localsession = null;
    	Long bTransId = null;
	    try {
			Query query = null;
			localsession = createLocalSession();
			query = localsession.createSQLQuery("SELECT last_number FROM user_sequences WHERE sequence_name = 'SQ_TB_BILLTRANS_ID'");			
			List<BigDecimal> result = query.list();	
			bTransId = ((BigDecimal)result.get(0)).longValue();
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		finally {
			closeLocalSession(localsession);
		}
	    
	    return bTransId;
    }
    
	public boolean isBilltransExist(String tableName, Long id) {
    	String sql = "";
    	if(tableName.equalsIgnoreCase("TB_BILLTRANS_USER")) {
    		sql = "SELECT count(*) FROM TB_BILLTRANS_USER WHERE BILLTRANS_USER_ID=" + id;
    	}
    	else if(tableName.equalsIgnoreCase("TB_BILLTRANS_JURDTL")) {
    		sql = "SELECT count(*) FROM TB_BILLTRANS_JURDTL WHERE BILLTRANS_JURDTL_ID=" + id;
    	}
    	else if(tableName.equalsIgnoreCase("TB_BILLTRANS_LOCN")) {
    		sql = "SELECT count(*) FROM TB_BILLTRANS_LOCN WHERE BILLTRANS_LOCN_ID=" + id;
    	}
    	else {
    		return false;
    	}
    	
    	javax.persistence.Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(sql);
		Long l  = ((BigDecimal)q.getSingleResult()).longValue();
		return (l>0) ? true : false;
	}
	
	public void insertBilltransTable(Session session, String tableName, Long id) {
    	String sql = "";
    	if(tableName.equalsIgnoreCase("TB_BILLTRANS_USER")) {
    		sql = "INSERT INTO TB_BILLTRANS_USER (BILLTRANS_USER_ID) VALUES (:id)";
    	}
    	else if(tableName.equalsIgnoreCase("TB_BILLTRANS_JURDTL")) {
    		sql = "INSERT INTO TB_BILLTRANS_JURDTL (BILLTRANS_JURDTL_ID) VALUES (:id)";
    	}
    	else if(tableName.equalsIgnoreCase("TB_BILLTRANS_LOCN")) {
    		sql = "INSERT INTO TB_BILLTRANS_LOCN (BILLTRANS_LOCN_ID) VALUES (:id)";
    	}
    	else {
    		return;
    	}
    	
    	org.hibernate.SQLQuery query = session.createSQLQuery(sql);
    	query.setParameter("id", id);
		query.executeUpdate();
	}
}
