package com.ncsts.dao;

import java.util.Date;
import java.util.List;


import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.ErrorCodeDTO;

public interface BatchDAO {
	
	public List<BatchDTO> getAllBatchRecords(String sqlQuery , String batchTypeCode);
	public List<BatchDTO> getAOLBatchRecords(String sqlQuery , String entryTimestamp, String statusCode);
	public void processTaxrateUpdate(Long batchId);
}
