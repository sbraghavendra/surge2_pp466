package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Menu;

public class JPAMenuDAO extends JpaDaoSupport implements MenuDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	@Transactional
	public Menu findById(String menuCode) throws DataAccessException {
		return getJpaTemplate().find(Menu.class, menuCode);
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Menu> findAllMenus() throws DataAccessException {
		return getJpaTemplate().find(" select menu from Menu menu");	  
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findAllMainMenuCodes() throws DataAccessException {
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createNativeQuery("SELECT MENU_CODE FROM TB_MAIN_MENU");
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Menu> findMenusByRole(String roleCode) throws DataAccessException{
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createQuery("select menu from Menu menu where menu.menuCode in (select rm.id.menuCode from RoleMenu rm where rm.id.roleCode = ?)");
		q.setParameter(1, roleCode);
		return q.getResultList();
	}
		  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(String id) throws DataAccessException {
		Menu menu = getJpaTemplate().find(Menu.class, id);
		if( menu != null){
			getJpaTemplate().remove(menu);
		}else{
			logger.warn("Menu Object not found for deletion");
		}
	} 
	  
	public void saveOrUpdate (Menu menu) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.saveOrUpdate(menu);
		t.commit();
		
		closeLocalSession(session);
		session=null;	
	}	
		
	@Transactional
	public String persist (Menu menu) throws DataAccessException {
		getJpaTemplate().persist(menu);
		String key = menu.getMenuDTO().getMenuCode();
		return key;
	}	  
}


