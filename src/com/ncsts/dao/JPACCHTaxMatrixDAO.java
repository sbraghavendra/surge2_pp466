package com.ncsts.dao;

import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CCHTaxMatrixPK;

/**
 * @author Paul Govindan
 *
 */

public class JPACCHTaxMatrixDAO extends JPAGenericDAO<CCHTaxMatrix,CCHTaxMatrixPK> implements CCHTaxMatrixDAO {
}


