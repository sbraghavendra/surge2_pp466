package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPSitusRuleText;
import com.ncsts.domain.BCPSitusRuleTextPK;
import com.ncsts.domain.BatchMaintenance;

public interface BCPSitusRuleTextDAO extends GenericDAO<BCPSitusRuleText, BCPSitusRuleTextPK> {
	public Long count(BatchMaintenance batch);
	public List<BCPSitusRuleText> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
