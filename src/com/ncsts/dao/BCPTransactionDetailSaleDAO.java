package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;

public interface BCPTransactionDetailSaleDAO extends GenericDAO<BCPBillTransaction, Long> {
	public void saveSplits(BCPBillTransaction original, 
			List<BCPBillTransaction> splits, 
			List<Long> deletedIds,
			List<BCPBillTransaction> allocationSplits);
	public long getNextAllocSubTransId();
}
