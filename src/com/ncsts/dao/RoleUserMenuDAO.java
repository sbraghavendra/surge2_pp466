package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.RoleMenu;

public interface RoleUserMenuDAO {
	
	public List<RoleMenu> findRoleMenuByRoleCode(String roleCode) throws DataAccessException;
	
	public void saveOrUpdate(RoleMenu roleMenu) throws DataAccessException;
	
	public void deleteRoleMenu(RoleMenu roleMenu) throws DataAccessException;

}
