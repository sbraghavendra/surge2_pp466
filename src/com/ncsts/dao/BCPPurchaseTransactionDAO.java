package com.ncsts.dao;

import com.ncsts.domain.BCPPurchaseTransaction;

import java.util.List;

public interface BCPPurchaseTransactionDAO  {
    public List<BCPPurchaseTransaction> findBCPPurchaseTransactionsByBatchId(Long processBatchNo, int firstRow, int maxResults);
    public int findBCPCountByBatchId(Long processBatchNo);
    public void deleteBCPPurchaseTransactionsByBatchId(Long processBatchNo);
}
