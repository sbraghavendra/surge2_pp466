package com.ncsts.dao;


import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.BatchDTO;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;
/**
 * 
 * @author Muneer
 *
 */

public interface BatchMaintenanceDAO extends GenericDAO<BatchMaintenance,Long> {

	public abstract BatchMaintenance findByBatchId(Long batchId) throws DataAccessException;
	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<BatchMaintenance> find(Criteria criteria);
	public abstract List<BatchMaintenance> find(DetachedCriteria criteria);
	public abstract BatchMaintenance find(BatchMaintenance exampleInstance);
	public abstract List<BatchMaintenance> find(BatchMaintenance exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	
	public abstract List<BatchMaintenance> findByBatchMaintenance(BatchMaintenance batchMaintenance);
	public Long count(BatchMaintenance exampleInstance);
	public abstract List<BatchMaintenance> getBatchMaintenanceByBatchTypeCode(String batchTypeCode) throws DataAccessException;
	public abstract List<BatchMaintenance> getBatchMaintenanceByBatchStatusCode(String batchStatusCode) throws DataAccessException;
	public abstract List<BatchMaintenance> getAllBatchMaintenanceData() throws DataAccessException;   
	public abstract void update(BatchMaintenance batchMaintenance) throws DataAccessException;
	public abstract void remove(long batchId) throws DataAccessException;
    // Need to check if BatchMaintenance object have all data required for Statistics 
	// Need another method for BatchErrors
	public List<BatchMaintenance> getAllBatchesByBatchTypeCode(String batchTypeCode) throws DataAccessException; // this method added by anand
	public List<BatchMaintenance> findFlaggedProcessTaxRateBatches() throws DataAccessException;
	public String doGLExtract(String date,String fileName,String exeMode, String returnCode) throws DataAccessException; // this method added by anand

	public abstract BatchMaintenance importTaxRateUpdates(BatchMaintenance batchMaintenance,
			TaxRateUploadParser parser) throws DataAccessException;
	public abstract BatchMaintenance importMapUpdates(BatchMaintenance batchMaintenance,
			ImportMapUploadParser parser) throws DataAccessException;
	
	public abstract BatchMaintenance saveBatch(BatchMaintenance batchMaintenance) throws DataAccessException; // this method added by anand
	
  public List<Object[]> getBatchProcesses(Long batchId);
  public List<BatchErrorLog> getBatchErrors(BatchErrorLog batch);
  public abstract BatchErrorLog save(BatchErrorLog error) throws DataAccessException;

  public BatchMaintenance getBatchMaintananceData(long batchId) throws DataAccessException;//Method added by Krishna
	
    public List<BatchMaintenance> getAllBatchesByBatchTypeCode(String codeTypeCode,
    	List<String> batchtypeCodeCode, List<String> batchstatusCodeCode, List<String> errorsevCodeCode);
    public BatchMetadata findBatchMetadataById(String id) throws DataAccessException;
    void saveBatchMetadata(BatchMetadata batchMetadata);
	List<BatchMetadata> loadValidMetadata(boolean isPurchasingSelected);
	public boolean hasExtractedTransactions(List<Long> ids);
  	public BigDecimal getBatchSequence(String batchType) 	throws DataAccessException;

  	public abstract Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz);
	public BatchMaintenance getPreviousBatchInSequence(BatchMaintenance batch) throws DataAccessException;

	public List<BatchMaintenance> findFlaggedBatches(String batchTypeCode, String batchStatusCode) throws DataAccessException;
	public List<BatchMaintenance> findByStatusUpdateUser(String batchTypeCode, String batchStatusCode, String statusUpdateUserId) throws DataAccessException;
	public List<BatchMaintenance> findProcessedBatchesNotNotified() throws DataAccessException;
	public void createNewBatchGL(BatchMaintenance batchMaintenance,String glDate);

	public void batchDelete(Long batchId);
	public void reindex(String index);
	public void maxAll();
	public void reweightMatrix();
	public void truncateBcpTable();
	public void driverReferenceMaint();
	public boolean checkRunningBatches();
	void incProcessCount();
}
