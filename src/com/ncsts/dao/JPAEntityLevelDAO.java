package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.EntityLevel;
import com.ncsts.dto.EntityLevelDTO;
//import com.sun.xml.internal.stream.Entity;

public class JPAEntityLevelDAO extends JpaDaoSupport implements EntityLevelDAO {
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

    @Transactional
    public EntityLevel findById(Long entityId) throws DataAccessException {
    	return getJpaTemplate().find(EntityLevel.class, entityId);
    }
	  
	 
/*	  @SuppressWarnings("unchecked")
	public List<EntityLevelDTO> findAllEntityLevels() throws DataAccessException {
	      return getJpaTemplate().find("select entity.entityLevelId, entity.transDetailColumnName, entity.description, driverNames.businessUnitFlag from EntityLevel entity, DriverNames driverNames where entity.transDetailColumnName = driverNames.transDtlColName");	  
	  }*/

	@SuppressWarnings("unchecked")
	public List findAllEntityLevels() throws DataAccessException {
		 //  String hql = "select e, dn from EntityLevel e, DriverNames dn where e.transDetailColumnName = dn.transDtlColName";
		 String hql = "select entityLevel from EntityLevel entityLevel order by entityLevel.entityLevelId";		  
		 Session session = createLocalSession();
	     Query qry = session.createQuery(hql);
	     List list = qry.list();
	       
	     closeLocalSession(session);
	     session=null;
	       
	     return list;     
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityLevel> findByTransDtlColName(String columnName) throws DataAccessException {
		return getJpaTemplate().
			find("from EntityLevel entityLevel where entityLevel.transDetailColumnName = ?1", columnName);
	}

	
	public void save (EntityLevelDTO entityLevelDTO)throws DataAccessException{
		DataDefinitionColumn dataDefinitionColumn = null;
		dataDefinitionColumn = checkCOlumnName(entityLevelDTO.getTransDetailColumnName());
		if(dataDefinitionColumn!=null){
			List<EntityLevel> find = findByTransDtlColName(entityLevelDTO.getTransDetailColumnName());
			if(find.size()==0){
				EntityLevel entityLevel = new EntityLevel();
				BeanUtils.copyProperties(entityLevelDTO, entityLevel);
				Session session = createLocalSession();
				Transaction t = session.beginTransaction();
				t.begin();
				session.save(entityLevel);
				t.commit();
	
				closeLocalSession(session);
				session=null;
		}else {
			logger.info("Duplicate Column Name");
		}
		}else {
			logger.info("Transaction Column Name Invalid");
		}
		
	}
	@Transactional
	public void update (EntityLevelDTO entityLevelDTO)throws DataAccessException{
		logger.info("Enter the Update Entity (JPADAO MEthod)");
		DataDefinitionColumn dataDefinitionColumn = null;
		dataDefinitionColumn = checkCOlumnName(entityLevelDTO.getTransDetailColumnName());
		
		if(dataDefinitionColumn!=null || entityLevelDTO.getEntityLevelId()==0L){
		
			List<EntityLevel> find = findByTransDtlColName(entityLevelDTO.getTransDetailColumnName());
			if(find !=null && find.size()>0 &&  find.get(0).getEntityLevelId().longValue() == entityLevelDTO.getEntityLevelId().longValue()
					|| find.size()==0 ){
				EntityLevel entityLevel = new EntityLevel();
				BeanUtils.copyProperties(entityLevelDTO, entityLevel);
				logger.info("Entity level data Inside JPA DAO "+entityLevel.getEntityLevelId() );
				Session session = createLocalSession();
				Transaction t = session.beginTransaction();
				t.begin();
				session.update(entityLevel);
				t.commit();
		
				closeLocalSession(session);
				session=null;
			}else {
				logger.info("Duplicate Column Name");
			}
		}else {
			logger.info("Transaction Column Name Invalid");
		}	
	}

	@Transactional
	private DataDefinitionColumn checkCOlumnName(String colName){
		DataDefinitionColumnPK dataDefinitionColumnPK = new DataDefinitionColumnPK();
		dataDefinitionColumnPK.setColumnName(colName);
		dataDefinitionColumnPK.setTableName("TB_PURCHTRANS");
		DataDefinitionColumn dataDefinitionColumn = null;
		dataDefinitionColumn = getJpaTemplate().find(DataDefinitionColumn.class, dataDefinitionColumnPK);
		
		return dataDefinitionColumn;
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove (Long entityLevelId) throws DataAccessException{
		EntityLevel entityLevel = new EntityLevel();
		entityLevel = getJpaTemplate().find(EntityLevel.class, entityLevelId);
		if(entityLevel!=null){
			getJpaTemplate().remove(entityLevel);
		}
		else {
			logger.info("No Object Found To Delete");
		}
	}

	@SuppressWarnings("unchecked")
	public String findByBU() throws DataAccessException {
		Session session = createLocalSession();
		Query q = session.createQuery("select min(entityLevel.description) from EntityLevel entityLevel, DriverNames driverNames " +
											"where entityLevel.transDetailColumnName = driverNames.transDtlColName "  +
											"and driverNames.businessUnitFlag = '1'" );
		String returnString = (String)q.uniqueResult();
		
		closeLocalSession(session);
		session=null;
		
		return returnString;
	}
}