package com.ncsts.dao;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import oracle.jdbc.OracleTypes;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Matrix;
import com.ncsts.domain.MatrixDriver;
import com.ncsts.view.util.SqlHelper;

/**
 * @author vinays singh
 *
 */
@Transactional
public class JPAMatrixDAO<M extends Matrix> extends JPAGenericDAO<M,Long> {

	@Override
	protected void extendExampleCriteria(Criteria criteria, M exampleInstance) {
        Long id = (exampleInstance == null)? null:exampleInstance.getId();
        if ((id != null) && (id > 0L)) {
        	logger.debug("Restricting Matrix search to Id: " + id);
        	criteria.add(Restrictions.idEq(id));
        }
	}
	
	protected String extendedDriverWhereClause(M matrix) {
		return null;
	}
	
	protected int getCursor(java.sql.Connection con) throws SQLException {
		int cursor = 0;		
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {
			cursor =  OracleTypes.CURSOR;					
		} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {
			cursor = Types.OTHER;					
		}			
		return cursor;
	}
	
	
	public boolean matrixExists(M matrix, boolean checkDate) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		StringBuilder sql = new StringBuilder();
		String glue = " where";
		String ModuleCode="";
		
		sql.append("select count(*) from " + getObjectClass().getCanonicalName() + " m");
		if (checkDate) {
			sql.append(glue + " m.effectiveDate = :date");
			glue = " and";
		}
		
		for (int i = 1; i <= matrix.getDriverCount(); i++) {
			String value = matrix.getDriver(new Long(i));
			if ((value != null) && !value.equals("")) {
				sql.append(String.format("%s upper(m.driver%02d) = '%s'", glue, i, SqlHelper.removeApostrophe(value.toUpperCase())));
				glue = " and";
			}
		}
		
		if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.GSBMatrix"))
		{
			//ModuleCode="BILLSALE";
			sql.append(" and m.moduleCode='BILLSALE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.GSBMatrix) matrix).getEntityId() );
			glue=" and";
		
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.TaxMatrix"))
		{
			//ModuleCode="PURCHUSE";
			sql.append(" and m.moduleCode='PURCHUSE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.TaxMatrix) matrix).getEntityId() );
			glue=" and";
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.LocationMatrix"))
		{
			sql.append(" and m.entityId=" + ((com.ncsts.domain.LocationMatrix) matrix).getEntityId() );
		}
		
		String where = extendedDriverWhereClause(matrix);
		if ((where != null) && !where.equals("")) {
			sql.append(glue + " " + where);
		}
		
	
		Query query = entityManager.createQuery(sql.toString());
		if (checkDate) {
			query.setParameter("date", matrix.getEffectiveDate());
		}
		Long count = (Long) query.getSingleResult();
		return (count > 0L);
	}
	
	public Date minEffectiveDate(M matrix) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		StringBuilder sql = new StringBuilder();
		String glue = " where";

		sql.append("select min(m.effectiveDate) from " + getObjectClass().getCanonicalName() + " m");
		
		for (int i = 1; i <= matrix.getDriverCount(); i++) {
			String value = matrix.getDriver(new Long(i));
			if ((value != null) && !value.equals("")) {
				sql.append(String.format("%s upper(m.driver%02d) = '%s'", glue, i, value.toUpperCase()));
				glue = " and";
			}
		}
		
		if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.GSBMatrix"))
		{
			sql.append(" and m.moduleCode='BILLSALE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.GSBMatrix) matrix).getEntityId() );
			glue=" and";
		
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.TaxMatrix"))
		{
			sql.append(" and m.moduleCode='PURCHUSE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.TaxMatrix) matrix).getEntityId() );
			glue=" and";
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.LocationMatrix"))
		{
			sql.append(" and m.entityId=" + ((com.ncsts.domain.LocationMatrix) matrix).getEntityId() );
		}
		
		String where = extendedDriverWhereClause(matrix);
		if ((where != null) && !where.equals("")) {
			sql.append(glue + " " + where);
		}		
	
		Query query = entityManager.createQuery(sql.toString());
		Object dateObject = query.getSingleResult();
		Date minGLDate = null;
		if(dateObject!=null){
			minGLDate = (Date)dateObject;
		}
	
		return minGLDate;
	}
	
	public Long matrixEffectiveDateCount(M matrix) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		StringBuilder sql = new StringBuilder();
		String glue = " where";
		String ModuleCode="";
		
		sql.append("select count(*) from " + getObjectClass().getCanonicalName() + " m");

		sql.append(glue + " m.effectiveDate > :date");
		glue = " and";
		
		for (int i = 1; i <= matrix.getDriverCount(); i++) {
			String value = matrix.getDriver(new Long(i));
			if ((value != null) && !value.equals("")) {
				sql.append(String.format("%s upper(m.driver%02d) = '%s'", glue, i, value.toUpperCase()));
				glue = " and";
			}
		}
		
		if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.GSBMatrix"))
		{
			//ModuleCode="BILLSALE";
			sql.append(" and m.moduleCode='BILLSALE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.GSBMatrix) matrix).getEntityId() );
			glue=" and";
		
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.TaxMatrix"))
		{
			//ModuleCode="PURCHUSE";
			sql.append(" and m.moduleCode='PURCHUSE'");
			sql.append(" and m.entityId=" + ((com.ncsts.domain.TaxMatrix) matrix).getEntityId() );
			glue=" and";
		}
		else if(getObjectClass().getCanonicalName().equalsIgnoreCase("com.ncsts.domain.LocationMatrix"))
		{
			sql.append(" and m.entityId=" + ((com.ncsts.domain.LocationMatrix) matrix).getEntityId() );
		}
		
		String where = extendedDriverWhereClause(matrix);
		if ((where != null) && !where.equals("")) {
			sql.append(glue + " " + where);
		}
		
	
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("date", matrix.getEffectiveDate());

		Long count = (Long) query.getSingleResult();
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public List<MatrixDriver> getUniqueDrivers(M filter) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		List<MatrixDriver> result = new ArrayList<MatrixDriver>();
		StringBuilder sql = new StringBuilder();
		sql.append("select new list(");
		for (int i = 1; i <= filter.getDriverCount(); i++) {
			sql.append(String.format("%sm.driver%02d", ((i == 1)? " ":", "), i));
		}
		sql.append(") from " + getObjectClass().getCanonicalName() + " m");
		String glue = " where";
		
		for (int i = 1; i <= filter.getDriverCount(); i++) {
			String value = filter.getDriver(new Long(i));
			if ((value != null) && !value.equals("")) {
				sql.append(String.format("%s upper(m.driver%02d) like '%s%%'", 
							glue, i, value.toUpperCase()));
				glue = " and";
			}
		}
		
		String where = extendedDriverWhereClause(filter);
		if ((where != null) && !where.equals("")) {
			sql.append(glue + " " + where);
		}
		sql.append(" group by");
		for (int i = 1; i <= filter.getDriverCount(); i++) {
			sql.append(String.format("%sm.driver%02d", ((i == 1)? " ":", "), i));
		}
		sql.append(" order by");
		for (int i = 1; i <= filter.getDriverCount(); i++) {
			sql.append(String.format("%sm.driver%02d", ((i == 1)? " ":", "), i));
		}
		Query query = entityManager.createQuery(sql.toString());
		List<List<String>> list = query.getResultList();
		for (List<String> row : list) {
			result.add(new MatrixDriver(row));
		}
		return result;
	}



}
