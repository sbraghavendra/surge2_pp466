package com.ncsts.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.dto.TaxCodeDetailDTO;

/**
 * @author Paul Govindan
 *
 */

public interface TaxCodeDetailDAO extends GenericDAO<TaxCodeDetail,Long> {

    public List<String[]> findTaxCodes() throws DataAccessException;
    public List<String> findTaxCodeTypeForState(String stateCode) throws DataAccessException;
    
    public List<String> findTaxCodeCounty(String countryCode, String stateCode) throws DataAccessException;
    public List<String> findTaxCodeCity(String countryCode, String stateCode) throws DataAccessException;  
    public List<String> findTaxCodeStj(String countryCode, String stateCode) throws DataAccessException;
    public List<TaxCodeDetail> findTaxCodeDetailListByCountrCity(String countryCode, String stateCode, String taxcodeCode, String taxcodeCounty, String taxcodeCity, String description) throws DataAccessException;

    public List<TaxCodeDetail> findTaxCodeCode(String stateCode, String taxCodeType) throws DataAccessException;
    public List<TaxCodeDetail> findTaxCodeDetailList(String stateCoountry, String stateCode, String taxCodeType, String taxcodeCode) throws DataAccessException;
    public List<TaxCodeDetail> findTaxCodeDetailListByTaxCode(String taxcodeCode) throws DataAccessException;
    public List<TaxCode> findTaxCodeListByTaxCode(String taxcodeCode) throws DataAccessException;

    public TaxCode findTaxCode(TaxCodePK id) throws DataAccessException;
    public List<TaxCode> getAllTaxCode() throws DataAccessException;
    public List<TaxCode> findTaxCodeList(String stateCode, String taxCodeType, String pattern) throws DataAccessException;
    public List<TaxCode> findTaxCodeListByCountry(String countryCode, String stateCode, String taxCodeType, String pattern) throws DataAccessException;
    
    public List<TaxCode> findByTaxCodeCode(String taxCodeTypeCode) throws DataAccessException;
    public List<TaxCode> findTaxCodeListByStateCode(String stateCode, String taxCodeType) throws DataAccessException;
    TaxCodeDetail findByJurLevelBroadSearch(String taxCodeCode, String jurLevel, String taxcodeCountryCode, String taxcodeStateCode, String taxCodeCounty,
                                                  String taxcodeCity, String stjName, Date glDate);

    public void removeTaxCode(TaxCode taxCode) throws DataAccessException, IllegalAccessException;
    public boolean isTaxCodeUsed(TaxCode taxCode) throws DataAccessException;
    public void updateDetail(TaxCode taxCode, Set<String> states, Map<String,Long> stateJurisdictionMap);
    public void updateBackdate(TaxCodeDetail taxCodeDetail)throws DataAccessException;
    public void updateDetail(TaxCodeDetail taxCodeDetail) throws DataAccessException;

    public List<ReferenceDocument> getAllDocumentReferences() throws DataAccessException;
    public List<ReferenceDocument> getDocumentReferencesByTaxCodeId(Long id) throws DataAccessException;
    public List<ReferenceDocument> getDocumentReferencesByTypeAndCode(String refType, String code) throws DataAccessException;
    public List<ReferenceDetail> getDocumentReferences(String taxCodeType, String taxCode, String taxStateCode) throws DataAccessException;
    public List<ReferenceDetail> getReferenceDetails(Long taxcodeDetailId) throws DataAccessException;

    public void addDocumentReference(ReferenceDetail detail);
    public void removeDocumentReference(ReferenceDetail detail);

    public List<ListCodes> findListCodeForTaxCode(String taxCode) throws DataAccessException;
    public List<ListCodes> findListCodeForState(String stateCode) throws DataAccessException;
    public void update(TaxCodeDetail taxCodeDetail)throws DataAccessException;

    public List<CCHCode> getAllCCHCode() throws DataAccessException;
    public List<CCHTaxMatrix> getAllCCHGroup() throws DataAccessException;
    public List<CCHTaxMatrix> getAllCCHGroupItems(String groupCode) throws DataAccessException;

    /*public List<Object> getTaxcodeDetails(String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId);*/
    
    public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId);
    public List<TaxCodeDetailDTO> getTaxcodeDetailsByCountry(String taxCodeCountry, String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId);
    public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxcodeCode, String taxCodeCountry, String taxCodeState, String taxCodeCounty, String taxCodeCity, boolean showLocalTaxability, boolean activeFlag, boolean definedFlag) throws DataAccessException;
    public List<TaxCodeDetailDTO> getTaxcodeDetailsJurisList(String taxcodeCode, String taxCodeCountry, String taxCodeState, String taxCodeCounty, String taxCodeCity, String taxCodeStj) throws DataAccessException;
    public List<TaxCodeDetailDTO> getTaxcodeRules(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel) throws DataAccessException;
    public long getRulesusedCount(Long ruleId) throws DataAccessException;
    public long getTaxCodeRulesCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String stj, Date effectiveDate, String jurLevel) throws DataAccessException;   
    public long getTaxCodeJurisdInfoCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String stj, String jurLevel) throws DataAccessException;
    public long getAllocBucketCount(Long taxcodeDetailId, String allocBucket) throws DataAccessException;   
    public Date minEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel) throws DataAccessException;
    public Long matrixEffectiveDateCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel, Date backEffectiveDate);
    public Date getRuleEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String transInd, String suspendInd) throws DataAccessException;
    public Long[] addRulesFromTransaction(TaxCodeDetailDTO stateRule, TaxCodeDetailDTO countyRule, TaxCodeDetailDTO cityRule) throws DataAccessException;
	//public void processTransactions(Long[] ruleIds) throws Exception;

}

