/**
 * This class is a general class containing methods to be used across many parts
 * of the project.
 * 
 * @author Jay Jungalwala
 */
package com.ncsts.dao;

import java.util.Calendar;
import java.util.Date;

public class Util {

	/**
	 * 
	 * This method returns a String to be used in a SQL query for searching date
	 * information.
	 * 
	 * For example if you passing a Date object with the date
	 * <code>January 28, 2008</code> it will return a string,
	 * "TO_DATE('2008-01-28','YYYY-MM-DD')".
	 * <p>
	 * Example of Use: <code>
	 * <pre>
	 * Util daoUtil = new Util();
	 * filter = &quot;(t.updateTimestamp &gt;= &quot; + daoUtil.&lt;b&gt;createToDateSQLString&lt;/b&gt;(taxRate.getUpdateTimestamp(), 0);
	 * filter = filter + &quot;AND t.updateTimestamp &lt; &quot; + daoUtil.&lt;b&gt;createToDateSQLString&lt;/b&gt;(taxRate.getUpdateTimestamp(), 1) + &quot;)&quot;;
	 * 
	 * Result:
	 * WHERE (t.updateTimestamp &gt;=  TO_DATE('2008-01-28','YYYY-MM-DD') AND t.updateTimestamp &lt;  TO_DATE('2008-01-29','YYYY-MM-DD') )
	 * </pre> 
	 * 
	 * </code>
	 * </p>
	 * 
	 * @param date
	 *            Date object which the specific date you want to covert to the
	 *            SQL string
	 * @param dayOffset
	 *            Integer representing how many days you want to add to the date
	 *            represented in the calendar object.
	 * 
	 * @return String - String with SQL "like" query.
	 * 
	 * @author Jay Jungalwala
	 */
	public String createToDateSQLString(Date date, Integer dayOffset) {
		// expiration_Date < TO_DATE('9999-12-31 23:59:59','YYYY-MM-DD
		// HH24:MI:SS');

		String dateString;

		Calendar calendar = Calendar.getInstance();
		Calendar MAXDATE = Calendar.getInstance();
		MAXDATE.set(9999, Calendar.DECEMBER, 31, 23, 59, 59);

		calendar.setTime(date);

		Integer year = 0;
		Integer month = 0;
		Integer day = calendar.get(Calendar.DAY_OF_MONTH);

		if (dayOffset > 0) {
			calendar.add(Calendar.DAY_OF_MONTH, dayOffset.intValue());
			day = calendar.get(Calendar.DAY_OF_MONTH);
		}

		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH) + 1;

		// Handle special case where date (with offset) is larger than
		// year=9999, month=12 and day=31
		if (calendar.after(MAXDATE)) {
			dateString = " TO_DATE('9999-12-31 23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
		} else {

			dateString = " TO_DATE('" + year + "-";

			// Ensure that Month is two digits
			if (month.intValue() < 10) {
				dateString = dateString + "0";
			}
			dateString = dateString + month + "-";

			// Ensure that day is two digits
			if (day.intValue() < 10) {
				dateString = dateString + "0";
			}
			dateString = dateString + day;
			dateString = dateString + "','YYYY-MM-DD') ";
		}
		return dateString;
	}

	/**
	 * <p>
	 * This method returns the String representation of a Double with trailing
	 * zeros stripped off.
	 * </p>
	 * Example of Use: <code>
	 * <pre>
	 * Util daoUtil = new Util();
	 * Double d;
	 * d= 12.3/3;
	 * System.out.println(&quot;d=&quot; + d);
	 * System.out.println(&quot;d=&quot; + daoUtil.Double2String(d));
	 * 
	 * Result:
	 * d=4.1000000000000005
	 * d=4.1
	 * </pre> 
	 * 
	 * </code>
	 * </p>
	 * 
	 * @param d
	 *            Double that you want converted
	 * @return String - String representation of Double with tailing zeros
	 *         removed.
	 * 
	 * @author Jay Jungalwala
	 */

	public String Double2String(Double d) {
		String s = String.format("%1$f", d);
		return s.replaceAll("(?!^)0+$", "");
	}
}
