package com.ncsts.dao;

import com.ncsts.domain.BillTransaction;

public interface BillTransactionDAO  {
    void save(BillTransaction transaction);
    public BillTransaction findById(Long billtransId);
    public Long findLastBillTransactionId();
}
