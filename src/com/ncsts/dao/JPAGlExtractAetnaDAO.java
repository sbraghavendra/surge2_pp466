/*
 * Author: Jim M. Wilson
 * Created: Aug 20, 2008
 * $Date: 2008-08-21 12:32:57 -0500 (Thu, 21 Aug 2008) $: - $Revision: 1872 $: 
 */

package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.GlExtractAetna;

public class JPAGlExtractAetnaDAO extends JPAGenericDAO<com.ncsts.domain.GlExtractAetna, Long> 
implements 	GlExtractAetnaDAO{

	@Transactional(readOnly=true)
	public List<GlExtractAetna> getPage(int firstRow, int rows)	throws DataAccessException {
		Query q = entityManager.createNativeQuery("select * from TB_TMP_GL_EXT_AETNA_PROD_1");
		q.setFirstResult(firstRow);
		q.setMaxResults(rows);
		List<Object[]> list = q.getResultList();
		List<GlExtractAetna> aetnaList = new ArrayList<GlExtractAetna>();
		Iterator<Object[]> itr = list.iterator();
		long pseudoId = firstRow;
		while(itr.hasNext()){
			aetnaList.add(new GlExtractAetna(itr.next(), pseudoId++));
		}
		return aetnaList;
	}

	@Transactional(readOnly=true)
	public Long count() {
		Query q = entityManager.createNativeQuery("select count(*) from TB_TMP_GL_EXT_AETNA_PROD_1");
		Long l  = ((BigDecimal)q.getSingleResult()).longValue();
		return l;
	}
	
	
}
