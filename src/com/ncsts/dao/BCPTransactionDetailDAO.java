package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BCPPurchaseTransaction;

public interface BCPTransactionDetailDAO extends GenericDAO<BCPPurchaseTransaction, Long> {
	public void saveSplits(BCPPurchaseTransaction original, 
			List<BCPPurchaseTransaction> splits, 
			List<Long> deletedIds,
			List<BCPPurchaseTransaction> allocationSplits);
	public long getNextAllocSubTransId();
}
