package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TaxAllocationMatrix;

public interface TaxAllocationMatrixDAO extends MatrixDAO<TaxAllocationMatrix> {
	public TaxAllocationMatrix findByIdWithDetails(Long id);
	public List<TaxAllocationMatrix> getDetailRecords(TaxAllocationMatrix taxAllocationMatrix, OrderBy orderBy);
	public boolean isTaxAllocationUsedinTransaction(Long taxAllocationMatrixId);
	
	public List<TaxAllocationMatrix> getAllRecords(TaxAllocationMatrix exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults);

	public Long count(TaxAllocationMatrix exampleInstance);
}
