package com.ncsts.dao;

import java.sql.SQLException;
import java.util.List;

import com.ncsts.domain.TempLocationMatrixCount;
import com.ncsts.domain.TempStatistics;
import com.ncsts.domain.TempTaxMatrixCount;
/*
 * Midtier project code modified - january 2009
 */
public interface DatabaseTransactionDAO {
	
	/**
	 * Will return all the records for database statistics Transactions Tab
	 * @return
	 */
	public List<TempStatistics> getTransactionsStatistics(String whereClauseforSearch)throws SQLException;
	
	public List<TempStatistics> getTransactionsStatistics();
	
	/**
	 * Will return all the records for database statistics TaxMatrix tab
	 * @return
	 */
	
	public TempTaxMatrixCount getTaxMatrixStatistics(String andClauseforSearch);
	
	
	/**
	 * Will return all the records for database statistics LocationMatrix tab
	 * @return
	 */

	public TempLocationMatrixCount getLocationMatrixStatistics(String andClauseforSearch);
	
	
	public Long count(String SPName,String whereClauseToken);
	
	public Long activeCount(String SPName,String whereClauseToken);
	
	public Long drilldown(String grpBy,String whereClauseToken);
	

	/**
	 * 
	 * @param sqlQuery
	 * @return
	 */
	/*public Object getObjectFromNativeQuery(String sqlQuery);
	
	public Object getObjectFromNativeQuery(String sqlQuery, String name, Object param);*/
	
	public List<Object[]> getListOfObjectFromNativeQuery(String sqlQuery);
}
