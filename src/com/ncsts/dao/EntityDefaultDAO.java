package com.ncsts.dao;

import java.util.HashMap;
import java.util.List;

import org.jgroups.tests.perf.Data;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.EntityDefault;


public interface EntityDefaultDAO extends GenericDAO<EntityDefault,Long> {
	
    public abstract Long count(Long entityId) throws DataAccessException;
    
    public abstract List<EntityDefault> findAllEntityDefaults() throws DataAccessException;
    
    public List<EntityDefault> findAllEntityDefaultsByEntityId(Long entityId) throws DataAccessException;
    
    public List<EntityDefault> findEntityDefaultsByDefaultCode(Long entityId, String defaultCode) throws DataAccessException;

    void findAllEntityDefaultsByEntityIdAsMap(Long entityId, HashMap<String, String> entityDefaults) throws DataAccessException;
    
    public Long persist(EntityDefault entityDefault)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(EntityDefault entityDefault) throws DataAccessException;
    
    public void deleteEntity(Long entityId) throws DataAccessException;
      
}

