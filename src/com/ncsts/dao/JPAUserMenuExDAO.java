package com.ncsts.dao;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.domain.UserMenuEx;
import com.ncsts.domain.UserMenuExPK;

public class JPAUserMenuExDAO extends JpaDaoSupport implements UserMenuExDAO{
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<UserMenuEx> findByUserEntity(String userCode,Long entityId) throws DataAccessException {		  
		List<UserMenuEx> list = new ArrayList<UserMenuEx>();
		List<Object[]> olist = getJpaTemplate().find("select userMenuEx.exceptionType ,userMenuEx.id.userCode, userMenuEx.id.entityId, userMenuEx.id.menuCode from UserMenuEx userMenuEx where userMenuEx.id.userCode = ?1 and userMenuEx.id.entityId = ?2" ,userCode,entityId);
		
		for(int i=0; i<olist.size(); i++){
			Object[] objects = olist.get(i);
			UserMenuEx newExc = new UserMenuEx();
			newExc.setExceptionType((String)objects[0]);
			newExc.setId(new UserMenuExPK((String)objects[1],(Long)objects[2],(String)objects[3]));	
			list.add(newExc);	
		}
		
		return list;
	  }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<UserMenuEx> findByUserCode(String userCode) throws DataAccessException {		  
		List<UserMenuEx> list = new ArrayList<UserMenuEx>();
		List<Object[]> olist = getJpaTemplate().find("select userMenuEx.exceptionType ,userMenuEx.id.userCode, userMenuEx.id.entityId, userMenuEx.id.menuCode from UserMenuEx userMenuEx where userMenuEx.id.userCode = ?",userCode);
		
		for(int i=0; i<olist.size(); i++){
			Object[] objects = olist.get(i);
			UserMenuEx newExc = new UserMenuEx();
			newExc.setExceptionType((String)objects[0]);
			newExc.setId(new UserMenuExPK((String)objects[1],(Long)objects[2],(String)objects[3]));	
			list.add(newExc);	
		}
		
		return list;		
	  }
	
	@Transactional(propagation = Propagation.NEVER, readOnly = false) 
	public void saveOrUpdate (UserMenuEx userMenuEx) throws DataAccessException {	
	  logger.debug("inside save exceprion menu ");
	  Session session = createLocalSession();
      Transaction t = session.beginTransaction();
      t.begin();
      session.saveOrUpdate(userMenuEx);
      t.commit();
      
      closeLocalSession(session);
      session=null;		
    }	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserMenuEx userMenuEx) throws DataAccessException {
		try {
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			// Following will attach the object to the session
			session.update(userMenuEx);
			session.delete(userMenuEx);
			t.commit();
			closeLocalSession(session);
			session=null;
		} catch (DataAccessException e) {
			e.printStackTrace();
		}

	}

}

