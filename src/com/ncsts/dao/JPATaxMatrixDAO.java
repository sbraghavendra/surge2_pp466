package com.ncsts.dao;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LogMemory;
import com.ncsts.common.SQLQuery;
import com.ncsts.common.Util;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.view.bean.MatrixCommonBean;

/**
 * @author vinays singh
 *
 */
@Transactional
public class JPATaxMatrixDAO extends JPAMatrixDAO<TaxMatrix> implements TaxMatrixDAO {

	private Map<String,String> columnMapping = null;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	@Override
	protected String extendedDriverWhereClause(TaxMatrix matrix) {
		String extendedWhere = "";
			
		Boolean globalNeed = matrix.getDriverGlobalBooleanFlag();
		if(globalNeed!=null && globalNeed.booleanValue()==true){
			extendedWhere = extendedWhere + " m.driverGlobalFlag = '1' ";
		}
		else{
			extendedWhere = extendedWhere + " m.driverGlobalFlag <> '1' ";	
		}
	
		return extendedWhere;
	}
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	// 02-04-2009 Method Modified for exact search and also wild card search
	// BUG# 0003810
	private void buildDriverWhereClause(StringBuffer where, String driverName, String value){
		if (value!=null && !value.trim().equals("")){
			if(value.endsWith("%")){
			where.append(" AND " + driverName + " LIKE '" + value.trim().toUpperCase() + "'");
			}else {
			where.append(" AND " + driverName + " = '" + value.trim().toUpperCase() + "'");
			}
		}
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 30; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}
			
			columnMapping.put("defaultflag", "DEFAULT_FLAG");
			columnMapping.put("matrixstatecode", "MATRIX_STATE_CODE");
			columnMapping.put("driverglobalflag", "DRIVER_GLOBAL_FLAG");
			columnMapping.put("binaryweight", "BINARY_WEIGHT");
			columnMapping.put("defaultbinaryweight", "DEFAULT_BINARY_WEIGHT");
			columnMapping.put("entityid", "ENTITY_ID");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getWhereClauseToken(TaxMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag, String activeFlag, Long taxMatrixId,String moduleCode){
		
		StringBuffer whereClauseExt = new StringBuffer();
		
		//for country selection
	    if (matrixCountryCode!=null && !matrixCountryCode.equals("")) {
	    	appendANDToWhere(whereClauseExt, "MATRIX_COUNTRY_CODE='" + matrixCountryCode +"'");
	    }

	    //for state selection
	    if (matrixStateCode!=null && !matrixStateCode.equals("")) {
	    	appendANDToWhere(whereClauseExt, "MATRIX_STATE_CODE='" + matrixStateCode +"'");
	    }

	    if (driverGlobalFlag!=null && !driverGlobalFlag.equals("")) {//getDriverGlobalFlag()
	    	if (driverGlobalFlag.equals(MatrixCommonBean.GLOBAL_COMPANY_NUMBER_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "driver_global_flag='1'");
	    	}
	    	else if (driverGlobalFlag.equals(MatrixCommonBean.GLOBAL_COMPANY_NUMBER_NON_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "(driver_global_flag='0' OR driver_global_flag IS NULL)");
	    	}
	    }

	    if (defaultFlag!=null && !defaultFlag.equals("")) {
	    	if (defaultFlag.equals(MatrixCommonBean.DEFAULT_LINE_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "default_flag='1'");
	    	}
	    	else if (defaultFlag.equals(MatrixCommonBean.DEFAULT_LINE_NON_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "(default_flag='0' OR default_flag IS NULL)");
	    	}
	    }
	    
	    if (activeFlag!=null && !activeFlag.equals("")) {
	    	if (activeFlag.equals(MatrixCommonBean.ACTIVE_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "active_flag='1'");
	    	}
	    	else if (activeFlag.equals(MatrixCommonBean.ACTIVE_NON_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "(active_flag='0' OR active_flag IS NULL)");
	    	}
	    }
	    
	    //build 30 drivers
	    try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        for (int i = 1; i <= 30; i++) {
	        	name = "Driver" + formatter.format(i);
	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	buildDriverWhereClause(whereClauseExt, getDriverColumnName(name.toLowerCase()), (String)theMethod.invoke(exampleInstance, new Object[]{}));
	        }
        }
        catch (Exception ex) {
        }
	    
	    //for tax code
	    if (taxcodeCode!=null && !taxcodeCode.equals("")) {
	    	if(taxcodeCode.endsWith("%")){
	    	logger.info("\t ********************** %%%% "+taxcodeCode);
	    	appendANDToWhere(whereClauseExt, 
	    		"(  (then_taxcode_code LIKE '" + taxcodeCode + "') " +
	            "OR (else_taxcode_code LIKE '" + taxcodeCode + "') ) ");
	    	}else {
	    		logger.info("\t Inside else !!!!!!!!!!!!!! "+taxcodeCode);
	    		appendANDToWhere(whereClauseExt, 
	    	    		"(  (then_taxcode_code = '" + taxcodeCode + "') " +
	    	            "OR (else_taxcode_code = '" + taxcodeCode + "') ) ");
			}
	    }

	    //for tax_matrix_id
	    if (taxMatrixId!=null && !taxMatrixId.equals("") && taxMatrixId!=0) {
	    	appendANDToWhere(whereClauseExt, "tax_matrix_id = " + taxMatrixId.longValue());
	    }

		if (exampleInstance.getEntityId() != null
				&& exampleInstance.getEntityId().longValue() >= 0L) {
			appendANDToWhere(whereClauseExt, "ENTITY_ID = "
					+ exampleInstance.getEntityId().longValue());
		} else if (exampleInstance.getEntityId() != null
				&& exampleInstance.getEntityId().longValue() == -1l) {
			appendANDToWhere(whereClauseExt,
					"ENTITY_ID = " + 999999999999999999l);
		}
	    if (moduleCode!=null && !moduleCode.equals("")) {
	    	appendANDToWhere(whereClauseExt, "MODULE_CODE = '" + moduleCode + "'");
	    }
	    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    if (effectiveDate!=null && !effectiveDate.equals("")) {
	    	String effectiveDatestr = df.format(effectiveDate);
	    	appendANDToWhere(whereClauseExt, "EFFECTIVE_DATE >= to_date('"+effectiveDatestr+"', 'mm/dd/yyyy') ");
	    	
	    }
	  
	    //for CCH
	    if(cchTaxCat!=null && !cchTaxCat.equals("")){
	    	appendANDToWhere(whereClauseExt, 
	    		"(  (THEN_CCH_TAXCAT_CODE = '" + cchTaxCat + "') " + 
	    		"OR (ELSE_CCH_TAXCAT_CODE = '"+ cchTaxCat + "') )");
	    }

	    if(cchGroup!=null && !cchGroup.equals("")){
	    	appendANDToWhere(whereClauseExt, 
	    		"(  (THEN_CCH_GROUP_CODE = '" + cchGroup + "') " + 
	    		"OR (ELSE_CCH_GROUP_CODE = '" + cchGroup + "') )");
	    }
	    return whereClauseExt.toString();
	    
	}
	
	//Midtier project code modified - january 2009
	private List<TaxMatrix> getTaxMatrixList(ResultSet rs){
		List<TaxMatrix> result = new ArrayList<TaxMatrix>();
		String defaultFlag = null;
		String driverGlobalFlag = null;
	
		try{
			long pseudoId = 1;
			while(rs.next()){
				TaxMatrix taxMatrix = new TaxMatrix();
			
				taxMatrix.setId(new Long(pseudoId++));
				
				taxMatrix.setEntityId(rs.getLong(1));
				
	            defaultFlag =  rs.getString(2);
	            if (defaultFlag != null)taxMatrix.setDefaultFlag(defaultFlag);  
	            
	            driverGlobalFlag = rs.getString(3);
	            if (driverGlobalFlag != null)taxMatrix.setDriverGlobalFlag(driverGlobalFlag);      
	            
	            
	            try{
	            	NumberFormat formatter = new DecimalFormat("00");
	            	Method theMethod = null;
	            	String name = "";
	            	String value = "";
	            	for (int i = 1; i <= 30; i++) {
	            		name = "setDriver" + formatter.format(i);
	            		value = rs.getString(i+3);
	            		theMethod = taxMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
	            		theMethod.setAccessible(true);
	            		theMethod.invoke(taxMatrix, new Object[]{value});
	            	}
	            }
	            catch (Exception ex) {
	            	logger.info("ex = " + ex.getMessage());
	            }
	          
	            
	            BigDecimal binaryWeight = rs.getBigDecimal(34);
	            if (binaryWeight!=null) taxMatrix.setBinaryWeight(binaryWeight.longValue());
	            
	            BigDecimal defaultBinaryWeight = rs.getBigDecimal(35);
	            if (defaultBinaryWeight!=null) taxMatrix.setDefaultBinaryWeight(defaultBinaryWeight.longValue());
	         
	           
	            
	            result.add(taxMatrix);                
			}
		}catch(Exception e){
			
		}
		return result;
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	// test method to test the pg SP call
	@Transactional
	public List<TaxMatrix> getAllRecords(TaxMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag, String activeFlag, Long taxMatrixId,
			OrderBy orderBy, int firstRow, int maxResults,String moduleCode) {
		logger.debug("int firstRow, int maxResults :"+ firstRow+" "+ maxResults);
		
		
			List<TaxMatrix> result = null;
			Connection con = null;
			try {
			String whereClauseToken = getWhereClauseToken(exampleInstance, taxcodeState, taxcodeType, taxcodeCode, 
					cchGroup, cchTaxCat, defaultFlag, matrixStateCode, matrixCountryCode,effectiveDate, driverGlobalFlag, activeFlag, taxMatrixId, moduleCode);
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			String strOrderBy = getOrderByToken(orderBy);
					
					con = getJpaTemplate().getJpaDialect().getJdbcConnection(
							entityManager, true).getConnection();
					if (con != null) {	
						String sqlQuery = SQLQuery.SP_TB_TAX_MATRIX_MASTER_LIST;
						
						if(strOrderBy==null || strOrderBy.length()==0){
							strOrderBy = "DRIVER_01 asc";
						}
						
						sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
						sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);
						sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
						sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));

						List<TaxMatrix> returnList=null;
						Query q = entityManager.createNativeQuery(sqlQuery);

						List<Object> list= q.getResultList();
						Iterator<Object> iter=list.iterator();
						if(list!=null){
							returnList = getTaxMatrixList(iter);
						}
						return returnList;
					}
				} catch (SQLException se) {
					se.printStackTrace();
				}
				finally{
					if (con != null) {
						try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
					}
				}
				
				LogMemory.executeGC();
				
				return result;	
			}	
	
	
	private List<TaxMatrix> getTaxMatrixList(Iterator<Object> iter) {
		Character defaultFlag = null;
		Character driverGlobalFlag = null;
		List<TaxMatrix> returnList = new ArrayList<TaxMatrix>();
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			TaxMatrix taxMatrix=new TaxMatrix();
            BigDecimal is = (BigDecimal)objarray[0];
            if (is!=null) taxMatrix.setId(is.longValue());//Row number
           BigDecimal entity=(BigDecimal)objarray[1];
           if(entity!=null) taxMatrix.setEntityId(entity.longValue());
           defaultFlag =  (Character)objarray[2];
           if (defaultFlag != null)taxMatrix.setDefaultFlag(defaultFlag.toString());  
           driverGlobalFlag = (Character)objarray[3];
           if (driverGlobalFlag != null)taxMatrix.setDriverGlobalFlag(driverGlobalFlag.toString());      
            try{
            	NumberFormat formatter = new DecimalFormat("00");
            	Method theMethod = null;
            	String name = "";
            	String value = "";
            	for (int i = 1; i <= 30; i++) {
            		name = "setDriver" + formatter.format(i);
            		value = (String) objarray[i+3];
            		theMethod = taxMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
            		theMethod.setAccessible(true);
	        	
            		theMethod.invoke(taxMatrix, new Object[]{value});
            	}
            }
            catch (Exception ex) {
            	logger.info("ex = " + ex.getMessage());
            }
            
            BigDecimal binaryWeight = (BigDecimal) objarray[34];
            if (binaryWeight !=null) taxMatrix.setBinaryWeight(binaryWeight.longValue());
            
            
            BigDecimal defaultBinaryWeight = (BigDecimal) objarray[35];
            if (defaultBinaryWeight!=null) taxMatrix.setDefaultBinaryWeight(defaultBinaryWeight.longValue());
         
            returnList.add(taxMatrix);                
		}
		
		return returnList;
	}

	@SuppressWarnings("unchecked")
	public List<TaxMatrix> getDetailRecords(TaxMatrix taxMatrix, OrderBy orderBy,String modulecode) {
		logger.debug("List<TaxMatrix> getDetailRecords called");
		
		List<TaxMatrix> result = null;
				
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(TaxMatrix.class);
            
      	    if (taxMatrix.getDriverGlobalFlag()!=null && !taxMatrix.getDriverGlobalFlag().equals("")) {
      	    	criteria.add(Restrictions.eq("driverGlobalFlag", taxMatrix.getDriverGlobalFlag()));
      	    }

      	    if (taxMatrix.getDefaultFlag()!=null && !taxMatrix.getDefaultFlag().equals("")) {
      	    	criteria.add(Restrictions.eq("defaultFlag", taxMatrix.getDefaultFlag()));
      	    }
            
      	   if (taxMatrix.getActiveFlag()!=null && !taxMatrix.getActiveFlag().equals("")) {
  	    	criteria.add(Restrictions.eq("activeFlag", taxMatrix.getActiveFlag()));
  	       }
      	   if (taxMatrix.getEntityId()!=null && taxMatrix.getEntityId().longValue()>=0L) {
     	    	criteria.add(Restrictions.eq("entityId", taxMatrix.getEntityId()));
     	         } 
      	  if (modulecode!=null && !modulecode.equals(""))   {
       		 criteria.add(Restrictions.eq("moduleCode", modulecode)); 
       	   }
      	   if (taxMatrix.getEffectiveDate()!=null && taxMatrix.getEffectiveDate().equals("")) {
  	    	criteria.add(Restrictions.eq("effectiveDate", taxMatrix.getEffectiveDate()));
  	         }
      	    
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 30; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = taxMatrix.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(taxMatrix, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		criteria.add(Restrictions.eq(name.toLowerCase(), value));
    	        	}
    	        }
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            result = criteria.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<TaxMatrix> getDetailRecords");
        }
        
        closeLocalSession(session);
        session=null;
			
        LogMemory.executeGC();
	
		return result;
	}

	//Midtier project code modified - january 2009 
	@Transactional
	public Long count(TaxMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode, Date effectiveDate,String driverGlobalFlag, String activeFlag, Long taxMatrixId,String moduleCode) {
		
		Long count = 0l;
		
		try {
			String extendWhere = getWhereClauseToken(exampleInstance, taxcodeState, taxcodeType, taxcodeCode, 
					cchGroup, cchTaxCat, defaultFlag, matrixStateCode, matrixCountryCode,effectiveDate, driverGlobalFlag,activeFlag, taxMatrixId,moduleCode);
					String sqlQuery = SQLQuery.SP_TB_TAX_MATRIX_MASTER_COUNT;//Replace function to Native SQL
					sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
					Query q = entityManager.createNativeQuery(sqlQuery);
					BigDecimal id = (BigDecimal) q.getSingleResult();
				    count = id.longValue();
					
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in executing SP");
		} finally {
		}		
		return count;
	}	
	
	//Midtier project code modified - january 2009
	@Transactional(readOnly = false)
	public void save(TaxMatrix instance){		
		Transaction tx = null;
		Session session = createLocalSession();
		try {	
			tx = session.beginTransaction();
			session.saveOrUpdate(instance);
			tx.commit();
			session.flush();
			session.clear();
			
			//purchaseTransactionService.processSuspendedTransactions(instance.getId(), PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_GS, false);
			
			/* ***!SPROC!*** Commented on 8/15/2017
			CallableStatement cstmnt = con.prepareCall("{call SP_TB_TAX_MATRIX_A_I(?)}");
			cstmnt.setLong(1, instance.getId());
			cstmnt.execute();
			cstmnt.close();
			con.commit();
			*/
		} catch (Exception e) {
			logger.error("Tax Matrix update error: " + e);
			if (tx != null) {
				tx.rollback();
			}
		}
		finally{
			closeLocalSession(session);
			session=null;
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxMatrix> getMatchingTaxMatrixLines(PurchaseTransaction exampleInstance){
		List<TaxMatrix> listTaxMatrix = null;	
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		if(exampleInstance.getGlDate()==null){
			return null;
		}
		
		String dateString = df.format(exampleInstance.getGlDate());// using G/L date
		
		String entityId="";
		EntityItem entityItem = null;
		List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityCode = ?" , exampleInstance.getEntityCode());
		if(list != null && list.size() >0 && list.get(0) != null){
			entityItem = list.get(0);
			entityId = entityItem.getId().toString();
		}
		else{
			return null;
		}
		
		String sql = "select * from tb_tax_matrix where module_code='PURCHUSE' and active_flag='1' and default_flag='0' and binary_weight>0 :whereClause order by binary_weight desc, significant_digits desc, effective_date desc ";
		
		StringBuffer whereClause = new StringBuffer();	
		whereClause.append(" and entity_id=" + entityId);
		whereClause.append(" and effective_date <= to_date('" + dateString + "','mm/dd/yyyy') and expiration_date >= to_date('" + dateString + "','mm/dd/yyyy') ");
		List<DriverNames> driverNames = getJpaTemplate().find("from DriverNames dn where dn.id.drvNamesCode = ?1 order by dn.id.driverId", TaxMatrix.DRIVER_CODE);	
		try {
			String matrixColumnName = "";
			String transactionColumnName = "";
	        String value = "";
			for (DriverNames dn :driverNames) {
				value = "";		
				matrixColumnName = dn.getMatrixColName();
				transactionColumnName = Util.columnToProperty(dn.getTransDtlColName());		
				
				value = (String) Util.getProperty(exampleInstance, transactionColumnName);
				if(value==null || value.length()==0){
					value="";
				}
				value=value.toUpperCase();
				
				if(MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NULL and Wildcard
					whereClause.append(" and (");
					whereClause.append(" ('"+value+"' is null and (upper("+matrixColumnName+")='*ALL' or upper("+matrixColumnName+")='*NULL'))");
					whereClause.append(" or");
					whereClause.append(" ('"+value+"' is not null and (upper("+matrixColumnName+")='*ALL' or upper('"+value+"') like upper("+matrixColumnName+")))");
					whereClause.append(" )");
				}
				else if(MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && !MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NULL and NOT Wildcard
					whereClause.append(" and (");
					whereClause.append(" ('"+value+"' is null and (upper("+matrixColumnName+")='*ALL' or upper("+matrixColumnName+")='*NULL'))");
					whereClause.append(" or");
					whereClause.append(" ('"+value+"' is not null and (upper("+matrixColumnName+")='*ALL' or upper('"+value+"')=upper("+matrixColumnName+")))");
					whereClause.append(" )");		
				}
				else if(!MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NOT NULL and Wildcard
					whereClause.append(" and (");
					whereClause.append(" upper("+matrixColumnName+")='*ALL' or upper('"+value+"') like upper("+matrixColumnName+")");
					whereClause.append(" )");
				}
				else if(!MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && !MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NOT NULL and NOT Wildcard
					whereClause.append(" and (");  
					whereClause.append(" upper("+matrixColumnName+")='*ALL' or upper('"+value+"')=upper("+matrixColumnName+")");
					whereClause.append(" )");				
				}
			}
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        	return null;
        }	
		
		sql = sql.replace(":whereClause", whereClause.toString());	
		Session session = createLocalSession();
        try {
        	org.hibernate.SQLQuery q = session.createSQLQuery(sql);
        	System.out.println(sql);
    		q.addEntity(TaxMatrix.class);
    		
			listTaxMatrix = q.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<TaxMatrix> getMatchingTaxMatrixLines");
        }
        finally{
        	closeLocalSession(session);
            session=null;
        }
        
        return listTaxMatrix;	
	}

}
