	package com.ncsts.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;

public class JPAImportDefinitionDAO extends JPAGenericDAO<ImportDefinition, String> implements ImportDefinitionDAO{
	@SuppressWarnings("unused")
	@PersistenceContext
	private EntityManager entityManager;
	
	//private Class<ImportDefinition> persistentClass = ImportDefinition.class;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinitionDetail> getAllRecords(){
		return getJpaTemplate().find("from ImportDefinitionDetail imp");
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinition> getAllImportDefinition(){
		return getJpaTemplate().find("from ImportDefinition imp");
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinition> getAllImportDefinition(String type){
		return getJpaTemplate().find("from ImportDefinition imp where imp.importDefinitionType = ?1", type);		
	}
	
	@Transactional
	public ImportDefinition addImportDefRecord(ImportDefinition instance) throws DataAccessException{
		
		if (instance.getImportDefinitionCode() == null) {
			//TODO Handle this case properly later to catch the exception and notify service layer
           logger.error(" Can not inser Null Values in PK colums");
           logger.info(" User Defined Error::prblem in JPAImportDefDAO.addImportDefRecord () ::Can not insert Null Values in PK colums" );
		}
		//changes made:::: added lines 47 and 48  for fixing the bug 0003884
		List<String> find = listCodeCheck("IMPFILETYP");
		if(find.contains(instance.getImportFileTypeCode())){
		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		
		}else {
			logger.info("Import File Type Code not in the tb_listCode");
		}
		return instance;
	}
	
	@Transactional
	public ImportDefinitionDetail add(ImportDefinitionDetail detail) throws DataAccessException {
		getJpaTemplate().persist(detail);	
		getJpaTemplate().flush();
		
		return detail;
	}
	
	@Transactional
	public void update(ImportDefinition importDefinition) throws DataAccessException{
		List<String> find = listCodeCheck("IMPFILETYP");
		if(find.contains(importDefinition.getImportFileTypeCode())){
			Session session = createLocalSession();
            Transaction t = session.beginTransaction();
            t.begin();
            session.update(importDefinition);
            t.commit();
            closeLocalSession(session);
            session=null;
            
		}else {
			logger.info("Import File Type Code not in the tb_listCode");
		}
	}

	@Transactional
	public void delete(String importDefCode) throws DataAccessException{
		
		ImportDefinition idef = findByPK(importDefCode);
		if(idef!=null){
			getJpaTemplate().remove(idef);
		}else{
			logger.info("ImportDefinition Object not found for deletion");
		}
	}
	
	@Transactional
	public ImportDefinition findByPK(String importDefCode) throws DataAccessException{
		return getJpaTemplate().find(ImportDefinition.class, importDefCode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinitionDetail> getAllHeaders(String code){
		return getJpaTemplate().
		    find("from ImportDefinitionDetail imp where imp.importDefinitionDetailPK.importDefinitionCode  = ?" , code);
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinitionDetail> getResetImpDefColHeadings(String code,String tablename){
		return getJpaTemplate().
		    find("from ImportDefinitionDetail imp where imp.importDefinitionDetailPK.importDefinitionCode  = ? and imp.importDefinitionDetailPK.tablename = ?" , code , tablename);
	}
	
	
	@Transactional
	public void replace(String code, List<ImportDefinitionDetail> list) {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			query = session.createQuery("delete ImportDefinitionDetail t where t.importDefinitionDetailPK.importDefinitionCode = :code");
			query.setString("code", code);
			int rows = query.executeUpdate();
			
			logger.debug(rows + " import def detail rows deleted");
			for (ImportDefinitionDetail detail : list ) {
			    session.merge(detail);	
			}
			
			Date timeNow = new Date();
			query = session.createQuery("update ImportDefinition t set t.mapUpdateUserId = :mapUpdateUserId, t.mapUpdateTimestamp = :mapUpdateTimestamp, t.updateUserId = :updateUserId, t.updateTimestamp = :updateTimestamp where t.importDefinitionCode = :code");
			query.setString("code", code);
			query.setString("mapUpdateUserId", Auditable.currentUserCode());
			query.setTimestamp("mapUpdateTimestamp", timeNow);	
			query.setString("updateUserId", Auditable.currentUserCode());
			query.setTimestamp("updateTimestamp",timeNow);
			rows = query.executeUpdate();
	
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
    @SuppressWarnings("unchecked")
    @Transactional
	protected List<String> listCodeCheck(String codeTypeCode){
    	List<String> find = getJpaTemplate().find(" SELECT listCodes.id.codeCode " +
				" FROM ListCodes listCodes " +
				" where listCodes.id.codeTypeCode = ?1 order by listCodes.description ", codeTypeCode );
    	return find;
    }

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinition> getDefinitionsByConnection(
			String connectionCode) {
		return getJpaTemplate().find("from ImportDefinition imp where imp.importConnectionCode = ?1", connectionCode);
	}

	@Transactional(readOnly=true)
	public Long count(ImportDefinition exampleInstance) {
		Long ret = 0L;	
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(ImportDefinition.class);
			if(exampleInstance.getImportDefinitionType()!=null && exampleInstance.getImportDefinitionType().length()>0){
        		criteria.add(Restrictions.eq("importDefinitionType", exampleInstance.getImportDefinitionType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importDefinitionCode", exampleInstance.getImportDefinitionCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());
			
			criteria.setProjection(Projections.rowCount());
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
		} catch (Exception e) {
				e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportDefinition> getAllRecords(ImportDefinition exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<ImportDefinition> list =  null;
		Session session = createLocalSession();
		try { 	
			Criteria criteria = session.createCriteria(ImportDefinition.class);
			if(exampleInstance.getImportDefinitionType()!=null && exampleInstance.getImportDefinitionType().length()>0){
        		criteria.add(Restrictions.eq("importDefinitionType", exampleInstance.getImportDefinitionType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importDefinitionCode", exampleInstance.getImportDefinitionCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());

			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            else{
            	criteria.addOrder(Order.asc("importDefinitionCode"));
            }
            
            list = criteria.list();
		} 
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<String> getImportDefinitionCodes(String tablename){
		List<String> impdefcodes = getJpaTemplate().find("SELECT id.importDefinitionCode from ImportDefinition id where id.importDefinitionCode in("
				+ "select idl.importDefinitionDetailPK.importDefinitionCode from ImportDefinitionDetail idl where idl.importDefinitionDetailPK.tablename = ?1"
				+ " group by idl.importDefinitionDetailPK.importDefinitionCode)",tablename);
		return impdefcodes;
	}
	
	public void addImportDefinitionDetail(ImportDefinitionDetail importDefinitionDetail) {
		Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.save(importDefinitionDetail);
        t.commit();
        closeLocalSession(session);
        session=null;
	}
	
}
