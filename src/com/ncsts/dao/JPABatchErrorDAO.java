package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.dto.ErrorLogDTO;
import org.hibernate.criterion.Order;

/**
 * 
 */
public class JPABatchErrorDAO extends JPAGenericDAO<ErrorLogDTO, Long> 
implements BatchErrorDAO {

	@SuppressWarnings("unchecked")
	public List<BatchErrorLog> getPage(BatchErrorLog batchErrorLog, int firstRow, int maxResults)
			throws DataAccessException {

		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BatchErrorLog.class);
		criteria.add(Restrictions.eq("batchId", batchErrorLog.getBatchId()));
		if(batchErrorLog.getProcessType()!=null){
			criteria.add(Restrictions.eq("processType", batchErrorLog.getProcessType()));
		}
		if(batchErrorLog.getProcessTimestamp()!=null){
			criteria.add(Restrictions.eq("processTimestamp", batchErrorLog.getProcessTimestamp()));
		}
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		criteria.addOrder(Order.desc("processTimestamp"));
		criteria.addOrder(Order.asc("rowNo"));
		criteria.addOrder(Order.asc("columnNo"));
		
		try {
			List<BatchErrorLog> returnBatchErrorLog = criteria.list();
			return returnBatchErrorLog;
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BatchErrorLog>();
		}finally{
			try{
			if(session!=null){
				closeLocalSession(session);
				session=null;
			}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public Long count(BatchErrorLog batchErrorLog) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BatchErrorLog.class);
		criteria.add(Restrictions.eq("batchId", batchErrorLog.getBatchId()));
		if(batchErrorLog.getProcessType()!=null){
			criteria.add(Restrictions.eq("processType", batchErrorLog.getProcessType()));
		}
		if(batchErrorLog.getProcessTimestamp()!=null){
			criteria.add(Restrictions.eq("processTimestamp", batchErrorLog.getProcessTimestamp()));
		}
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
		
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}
}
