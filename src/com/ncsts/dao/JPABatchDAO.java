package com.ncsts.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.SequenceManager;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.ErrorCodeDTO;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;
import com.ncsts.services.impl.JurisdictionServiceImpl;
import com.ncsts.services.impl.JurisdictionTaxrateServiceImpl;

@Transactional
public class JPABatchDAO extends JpaDaoSupport implements BatchDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<BatchDTO> getAllBatchRecords(String sqlQuery,
			String batchTypeCode) {
		List<BatchDTO> returnList = null;
		Query q = entityManager.createNativeQuery(sqlQuery);
		q.setParameter(1, batchTypeCode);
		List<Object> list = q.getResultList();
		Iterator<Object> iter = list.iterator();
		if (list != null) {
			returnList = parseBatchReturnList(iter);
		}
		return returnList;

	}
	protected Session createLocalSession() {
	    EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<BatchDTO> getAOLBatchRecords(String sqlQuery,
			String entryTimestamp, String statusCode) {
		List<BatchDTO> returnList = null;
		Query q = entityManager.createNativeQuery(sqlQuery);
		q.setParameter(1, entryTimestamp);
		q.setParameter(2, statusCode);
		List<Object> list = q.getResultList();
		Iterator<Object> iter = list.iterator();
		if (list != null) {
			returnList = parseBatchReturnList(iter);
		}
		return returnList;
	}

	private List<BatchDTO> parseBatchReturnList(Iterator<Object> iter) {
		List<BatchDTO> returnList = new ArrayList<BatchDTO>();

		while (iter.hasNext()) {
			Object[] objarray = (Object[]) iter.next();
			BatchDTO batchDTO = new BatchDTO();
			BigDecimal is = (BigDecimal) objarray[0];
			if (is != null)
				batchDTO.setBatchId(is.longValue());
			String batchType = (String) objarray[1];
			batchDTO.setBatchTypeCode(batchType);
			java.sql.Timestamp schedStartTimestamp = (java.sql.Timestamp) objarray[3];
			if(schedStartTimestamp!=null){
				batchDTO.setSchedStartTimestamp(new java.util.Date(schedStartTimestamp.getTime()));
			}
			java.sql.Timestamp actualStartTimestamp = (java.sql.Timestamp) objarray[4];
			if(actualStartTimestamp!=null){
				batchDTO.setActualStartTimestamp(new java.util.Date(actualStartTimestamp.getTime()));
			}
			java.sql.Timestamp actualEndTimestamp = (java.sql.Timestamp) objarray[5];
			if(actualEndTimestamp!=null){
				batchDTO.setActualEndTimestamp(new java.util.Date(actualEndTimestamp.getTime()));
			}
			String batchStatusCode = (String) objarray[6];
			batchDTO.setBatchStatusCode(batchStatusCode);
			Character heldFlag = (Character) objarray[7];
			if (heldFlag != null)
				batchDTO.setHeldFlag(heldFlag.toString());
			String errorSevCode = (String) objarray[8];
			batchDTO.setErrorSevCode(errorSevCode);
			BigDecimal totalBytes = (BigDecimal) objarray[9];
			if (totalBytes != null)
				batchDTO.setTotalBytes(totalBytes.longValue());
			BigDecimal totalRows = (BigDecimal) objarray[10];
			if (totalRows != null)
				batchDTO.setTotalRows(totalRows.longValue());
			BigDecimal processedBytes = (BigDecimal) objarray[11];
			if (processedBytes != null)
				batchDTO.setProcessedBytes(processedBytes.longValue());
			BigDecimal processedRows = (BigDecimal) objarray[12];
			if (processedRows != null)
				batchDTO.setProcessedRows(processedRows.longValue());
			returnList.add(batchDTO);
		}

		return returnList;
	}
	/*
	 * This method is replacing the SProc 'sp_getErrorCode'
	 */
	@Transactional 
	private ErrorCodeDTO getErrorCode(String av_error_code, Long batchId, String av_process_type, Date ad_process_timestamp) {
		ErrorCodeDTO result = null;

		String fetchErrorConditionInfoQuery = "SELECT ERRORCODE.severity_level, ERRORSEV.write_import_line_flag, ERRORSEV.abort_import_flag" + 
									"  FROM ( SELECT * FROM tb_list_code WHERE code_type_code = 'ERRORCODE' ) ERRORCODE," + 
									"  ( SELECT * FROM tb_list_code WHERE code_type_code = 'ERRORSEV' ) ERRORSEV" + 
									" WHERE ERRORCODE.severity_level = ERRORSEV.code_code AND" + 
									"  ERRORCODE.code_code ='"+ av_error_code +"' ";
		System.out.println("fetchErrorConditionInfoQuery"+fetchErrorConditionInfoQuery);
		try {
			//-- Fetch error condition information
			Object[] errorConditionInfo = (Object[])entityManager.createNativeQuery(fetchErrorConditionInfoQuery).getSingleResult();
			//TODO: CHECK WHAT WE ARE GETTING FROM THE DB FOR THE CHAR VARIABLES!!! Might require conversion of some sort.
			result = new ErrorCodeDTO((String)errorConditionInfo[0] , ((Character)errorConditionInfo[1]) , ((Character)errorConditionInfo[2]));
			
		} catch (Throwable t) {
			result = new ErrorCodeDTO("90",'0','1');
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		  
		    	String effectiveDatestr = sdf.format(ad_process_timestamp);
		//-- Write error to log
		//TODO: sq_tb_batch_error_log_id.nextval needs to be replaced once we get rid of sequences. Either get the appropriate number with a separate query - or otherwise perhaps it will be auto-generated and can be omitted
		String writeToLogQuery = " INSERT INTO tb_batch_error_log " +
				" (batch_error_log_id, batch_id, process_type, process_timestamp, row_no, column_no, error_def_code) VALUES" +
				" (sq_tb_batch_error_log_id.nextval, "+batchId+", '"+ av_process_type+"', to_date('"+effectiveDatestr+"', 'mm/dd/yyyy'), 0, 0, '"+av_error_code+"')";
		
		entityManager.createNativeQuery(writeToLogQuery).executeUpdate();

		return result;
	}
	
	@Transactional
	public void processTaxrateUpdate(Long batchId) {
		Session session = createLocalSession();

		String batchStatusCode  = null;
		long errorSeverityCode  = 0;
		long startingSequence  = 0;
		long processedRows  = 0;
		long severityLevel= 0;
        long batchErrorLogId = 0;
		char importLineFlag='1';		
		char abortImportFlag  = '0';	
		Long jurisdictionId = null;
		char jurCustomFlag = '\0';
		Long jurisdictionTaxrateId = null;
		char rateCustomFlag = '\0';
		char rateModifiedFlag = '\0';
		Date sysDate = new Date(System.currentTimeMillis()); //TODO verify is in UTC as per JLS
		long releaseNumber = 0;
		Date updateDate = null;
        long errorCount = 0;
		
        Transaction tx = null;
		try {
			//-- Confirm batch exists and is flagged for processing
			boolean badRead = false;
			boolean e_wrongdata = false;
			try {
				//String query = "SELECT batch_status_code FROM tb_batch WHERE batch_id = :batchId";
				org.hibernate.Query query1 = session.createSQLQuery("SELECT batch_status_code FROM tb_batch WHERE batch_id = :batchId");
				query1.setParameter("batchId", batchId);
				List<String> results1 = (List<String>)query1.list();
				if (null==results1 || results1.size()<1) {
					badRead = true;
				} 
				else {
					batchStatusCode  = results1.get(0);
					if (null==batchStatusCode ) {
						badRead = true;
					}
				}
			} 
			catch (Throwable T) {
				//badread
				badRead = true;
			}
			finally {
				if (!"FP".equals(batchStatusCode)) {
					e_wrongdata = true;
				}
				if (badRead || e_wrongdata) {
					errorCount ++;
					ErrorCodeDTO errorCodeInfo = getErrorCode((badRead)?"TR8":"TR9", batchId, "P", sysDate);

					severityLevel = errorCodeInfo.getSeverityLevelAsInt(); 
					//v_write_import_line_flag = errorCodeInfo.getAv_write_import_line_flag();
					abortImportFlag = errorCodeInfo.getAv_abort_import_flag();
					
					if (severityLevel > errorSeverityCode) {
						errorSeverityCode  = severityLevel;
					}
				}
			} //finally
				
			//-- Abort Procedure?
			if ('1' == abortImportFlag) {
				batchStatusCode  = "ABP";
				throw new Exception();
			}
			
			//-- Get starting sequence no.
			try {
				String getStartingQuery = SequenceManager.getQuerySequenceCurrentValue("SQ_TB_PROCESS_COUNT");
				SQLQuery startingQuery = session.createSQLQuery(getStartingQuery);
				List<Number> startingQueryResults = startingQuery.list();
				startingSequence  = startingQueryResults.get(0).longValue();
			}
			catch (Throwable t) {
				//default to 0
				logger.error("Failed to get starting sequence no. "+t.getMessage());
			}
			
			// -- Get batch information			
			String batchInfoQuerySql = "SELECT UPPER(VC02), NU01, TS01 FROM tb_batch WHERE batch_Id = :batchId";
			SQLQuery batchInfoQuery = session.createSQLQuery(batchInfoQuerySql);
			batchInfoQuery.setParameter("batchId", batchId );
			List<Object[]> batchInfoResults = batchInfoQuery.list();
			if (null!=batchInfoResults && batchInfoResults.size()>0) {
				Object[] resultArr = batchInfoResults.get(0);
				if (null!= resultArr && resultArr.length==3) {
					//vc_taxrate_update_type = (String)resultArr[0];
					releaseNumber = ((Number)resultArr[1]).longValue();
					updateDate = (Date)resultArr[2];
				}
			}
					
			// -- Update batch as processing
			try {
				String updateBatchSql = "UPDATE tb_batch SET BATCH_STATUS_CODE = :statusCode, ERROR_SEV_CODE = :errorCode, START_ROW = :startRow, ACTUAL_START_TIMESTAMP = :startTimestamp WHERE batch_id = :batchId";
				SQLQuery updateBatchQuery = session.createSQLQuery(updateBatchSql);
				updateBatchQuery.setString("statusCode", "XP");
				updateBatchQuery.setString("errorCode", "");
				updateBatchQuery.setParameter("startRow", startingSequence);
				updateBatchQuery.setParameter("startTimestamp", sysDate);
				updateBatchQuery.setParameter("batchId", batchId);
				updateBatchQuery.executeUpdate();
			} catch (Throwable t) {
				errorCount++;
				ErrorCodeDTO errorCodeInfo = getErrorCode("TR1", batchId, "P", sysDate);

				severityLevel = errorCodeInfo.getSeverityLevelAsInt(); 
				importLineFlag = errorCodeInfo.getAv_write_import_line_flag();
				abortImportFlag = errorCodeInfo.getAv_abort_import_flag();
				
				if (severityLevel > errorSeverityCode ) {
					errorSeverityCode  = severityLevel;
				}
			}
					
			// -- Abort Procedure?
			if ('1' == abortImportFlag) {
				batchStatusCode  = "ABP";
				throw new Exception();
			}
			
			//   COMMIT;
			//ac logger.debug("attempting commit for transaction so far. batch_taxrate_update ["+batchId+"]");
			//ac entityManager.getTransaction().commit();
			//ac logger.debug("committed.");
			
			tx = session.beginTransaction();
			
			//-- ****** Read and Process Transactions ****** -----------------------------------------
            List<BCPJurisdictionTaxRate> bcpCursorQuery = entityManager
                    .createQuery("from BCPJurisdictionTaxRate where batch_id = :an_batch_id")
                    .setParameter("an_batch_id", batchId).getResultList();

			Iterator<BCPJurisdictionTaxRate> bcpIter = bcpCursorQuery.iterator();
			
			while (null!=bcpIter && bcpIter.hasNext()) {
				BCPJurisdictionTaxRate bcpJurisdictionTaxRate = bcpIter.next();
				//-- Increment Process Count
				SequenceManager.getNextVal("sq_tb_process_count",entityManager);
				processedRows++;	
				
			    //-- Find jurisdiction
				try {
					// NOTICE: the country comparison is commented out in the original sproc, and hence ignored in this impl as well.	
					String findJurisdictionQuerySql = "SELECT JURISDICTION_ID, CUSTOM_FLAG FROM TB_JURISDICTION "
	                        + "WHERE GEOCODE = :geocode AND STATE = :state "
	                        + "AND COUNTY = :county AND CITY = :city AND ZIP = :zip";
	                SQLQuery findJurisditionQuery = session
	                        .createSQLQuery(findJurisdictionQuerySql);
	                // TODO: check if casting is needed here, or if setParameter will handle it

					//TODO: check if casting is needed here, or if setParameter will handle it
					findJurisditionQuery.setParameter("geocode", bcpJurisdictionTaxRate.getGeocode()); 
					findJurisditionQuery.setParameter("state", bcpJurisdictionTaxRate.getState());
					findJurisditionQuery.setParameter("county", bcpJurisdictionTaxRate.getCounty());
					findJurisditionQuery.setParameter("city", bcpJurisdictionTaxRate.getCity());
					findJurisditionQuery.setParameter("zip", bcpJurisdictionTaxRate.getZip());
					List<Object[]> findJurisdictionResults = findJurisditionQuery.list();
					if(findJurisdictionResults!=null && findJurisdictionResults.size()>0){
						jurisdictionId = ((Number)findJurisdictionResults.get(0)[0]).longValue();
						
						Character flagStr = (Character)findJurisdictionResults.get(0)[1];
						if(flagStr!=null && flagStr.equals('1')){
							jurCustomFlag = '1';
						}
						else{
							jurCustomFlag = '0';
						}
					}			
				} 
				catch (Throwable t) {
					logger.error("Error while looking for Jurisdiction: "+t.getMessage());
					jurisdictionId = 0l;    //Long
					jurCustomFlag = '0';  //Char
				}

				//-- If it does not exist, add it				
				if (null == jurisdictionId || 0 == jurisdictionId) {
			        // -- Insert new jurisdiction
					Jurisdiction jur = new Jurisdiction();
					
					populateJurisdiction(jur, bcpJurisdictionTaxRate);
					jur.setCustomFlag("0");
					jur.setModifiedFlag("0");
					jur.setUpdateUserId("STSCORP");
					entityManager.persist(jur);
					
					jurisdictionId = jur.getJurisdictionId();
				}
				else {
				    //jurisdiction exists
					if ('1' == jurCustomFlag) { //is custom?			    	  
						//-- If it does exist and is custom then write errog log warning
						errorSeverityCode  = 10;
			    	  
						// v_batch_error_log_id = SequenceManager.getNextVal("sq_tb_batch_error_log_id",entityManager);
						SequenceManager.getNextVal("sq_tb_batch_error_log_id",entityManager);
						BatchErrorLog newError = new BatchErrorLog();
						newError.setBatchId(bcpJurisdictionTaxRate.getBatchId());
						newError.setProcessType("TR");
						newError.setProcessTimestamp(sysDate);
						newError.setRowNo(processedRows);
						newError.setColumnNo(0l);
						newError.setErrorDefCode("TR1");
						newError.setImportHeaderColumn("GeoCode|State|County|City|Zip");
						newError.setImportColumnValue(bcpJurisdictionTaxRate.getGeocode()+"|"+bcpJurisdictionTaxRate.getState()+"|"+bcpJurisdictionTaxRate.getCounty()+"|"+bcpJurisdictionTaxRate.getCity()+"|"+bcpJurisdictionTaxRate.getZip());
						newError.setTransDtlColumnName("n/a");
						newError.setTransDtlDatatype("n/a");
						newError.setImportRow("n/a");
			    	  
						entityManager.persist(newError);
			    	  
					}
					//-- Update Jurisdiction   -- MBF07

					String updateJurisdictionSql = " UPDATE TB_JURISDICTION SET TB_JURISDICTION.IN_OUT = :bcpInOut, TB_JURISDICTION.DESCRIPTION = :bcpDescription"  
			      									+ " WHERE TB_JURISDICTION.JURISDICTION_ID = :jurId";
					SQLQuery updateJurisdictionQuery = session.createSQLQuery(updateJurisdictionSql);
					updateJurisdictionQuery.setParameter("bcpInOut", bcpJurisdictionTaxRate.getInOut());
					updateJurisdictionQuery.setParameter("bcpDescription", bcpJurisdictionTaxRate.getDescription());
					updateJurisdictionQuery.setParameter("jurId", jurisdictionId);
			      
					int numUpdated = updateJurisdictionQuery.executeUpdate();
					logger.debug("updated jurisdiction with id ["+jurisdictionId+"], affected ["+numUpdated+"] lines");
				}//endif
					
			    //-- Find jurisdiction taxrate
				try {
					String findJurisdictionTaxRateQuerySql = "SELECT JURISDICTION_TAXRATE_ID, CUSTOM_FLAG, MODIFIED_FLAG " + 
							" FROM TB_JURISDICTION_TAXRATE " + 
							" WHERE JURISDICTION_ID = :v_jurisdiction_id " + 
							" AND EFFECTIVE_DATE = :bcp.rate_effective_date " + 
							" AND RATETYPE_CODE = :bcp.ratetype_code";

					SQLQuery findJurisditionTaxRateQuery = session.createSQLQuery(findJurisdictionTaxRateQuerySql);					
					findJurisditionTaxRateQuery.setParameter("v_jurisdiction_id", jurisdictionId); 
					findJurisditionTaxRateQuery.setParameter("bcp.rate_effective_date", bcpJurisdictionTaxRate.getRateEffectiveDate());
					findJurisditionTaxRateQuery.setParameter("bcp.ratetype_code", bcpJurisdictionTaxRate.getRatetypeCode());
					List<Object[]> findJurisdictionResults = findJurisditionTaxRateQuery.list();

					jurisdictionTaxrateId = ((Number)findJurisdictionResults.get(0)[0]).longValue();

					Character rateCustomFlagStr = (Character)findJurisdictionResults.get(0)[1];
					if(rateCustomFlagStr!=null && rateCustomFlagStr.equals('1')){
						rateCustomFlag = '1';
					}
					else{
						rateCustomFlag = '0';
					}
					
					Character rateModifiedFlagStr = (Character)findJurisdictionResults.get(0)[2];
					if(rateModifiedFlagStr!=null && rateModifiedFlagStr.equals('1')){
						rateModifiedFlag = '1';
					}
					else{
						rateModifiedFlag = '0';
					}
				}
				catch (Throwable t) {
					logger.error("error finding jurisdiction taxrate");
					jurisdictionTaxrateId = 0l;
					rateCustomFlag = '0';
					rateModifiedFlag = '0';
				}

				
				if (null == jurisdictionTaxrateId || 0 == jurisdictionTaxrateId) { 
					//-- If it does not exist, add it
					JurisdictionTaxrate newJurTaxRate = new JurisdictionTaxrate();

					//-- Retrieve Jurisdiction				
					List<Jurisdiction> jurisdictionCursorQuery = entityManager.createQuery("from Jurisdiction where jurisdictionId = " + jurisdictionId).getResultList();
					Iterator<Jurisdiction> jurisdictionIter = jurisdictionCursorQuery.iterator();
					Jurisdiction juris = null;	
					if (null!=jurisdictionIter && jurisdictionIter.hasNext()) {
						juris = jurisdictionIter.next();
					}
					
					populateTaxrate(newJurTaxRate, bcpJurisdictionTaxRate);
					newJurTaxRate.setJurisdiction(juris);
					newJurTaxRate.setCustomFlag("0");
					newJurTaxRate.setModifiedFlag("0");
					newJurTaxRate.setUpdateUserId("STSCORP");
                    entityManager.persist(newJurTaxRate);	
				} 
				else {
					//taxrate exists
					if (('\0' == jurCustomFlag || '0'==jurCustomFlag) && ('1'==rateCustomFlag)) {
						// -- If it does exist and jurisdiction is not custom but rate is custom (modified or not) then write errog log warning					
						errorSeverityCode  = 10l;
						errorSeverityCode  = SequenceManager.getNextVal("sq_tb_batch_error_log_id",entityManager);
					   
						DateFormat df=new SimpleDateFormat("mm/dd/yyyy"); //TODO: check that this is the same as Oracle's TO_CHAR(_,'mm/dd/yyyy')
						String bcpRateEffectiveDateStr = df.format(bcpJurisdictionTaxRate.getRateEffectiveDate());
					   
				    	BatchErrorLog newError = new BatchErrorLog();
				    	newError.setBatchId(bcpJurisdictionTaxRate.getBatchId());
				    	newError.setProcessType("TR");
				    	newError.setProcessTimestamp(sysDate);
				    	newError.setRowNo(processedRows);
				    	newError.setColumnNo(0l);
				    	newError.setErrorDefCode("TR2");
				    	newError.setImportHeaderColumn("GeoCode|State|County|City|Zip|Eff. Date");
				    	// newError.setImportColumnValue(bcpGeocode+"|"+bcpState+"|"+bcpCounty+"|"+bcpCity+"|"+bcpZip+"|"+bcpRateEffectiveDateStr);
				    	newError.setImportColumnValue(bcpJurisdictionTaxRate.getGeocode()+"|"+bcpJurisdictionTaxRate.getState()+"|"+bcpJurisdictionTaxRate.getCountry()+"|"+bcpJurisdictionTaxRate.getCity()+"|"+bcpJurisdictionTaxRate.getZip());
				    	newError.setTransDtlColumnName("n/a");
				    	newError.setTransDtlDatatype("n/a");
				    	newError.setImportRow("n/a");
				    	  
				    	 entityManager.persist(newError); 
					} 
					else if (('\0'==jurCustomFlag || '0'==jurCustomFlag )
							&& ('\0'==rateCustomFlag || '0'==rateCustomFlag) 
							&&  ('1'==rateModifiedFlag)) {
						//-- If it does exist and jurisdiction is not custom and rate is not custom but rate is modified then write error log warning
						errorSeverityCode  = 10l;
						batchErrorLogId  = SequenceManager.getNextVal("sq_tb_batch_error_log_id",entityManager);
					    
						DateFormat df=new SimpleDateFormat("mm/dd/yyyy"); //TODO: check that this is the same as Oracle's TO_CHAR(_,'mm/dd/yyyy')
						String bcpRateEffectiveDateStr = df.format(bcpJurisdictionTaxRate.getRateEffectiveDate());
						   
						BatchErrorLog newError = new BatchErrorLog();
						newError.setBatchId(bcpJurisdictionTaxRate.getBatchId());
						newError.setProcessType("TR");
						newError.setProcessTimestamp(sysDate);
						newError.setRowNo(processedRows);
						newError.setColumnNo(0l);
						newError.setErrorDefCode("TR3");
						newError.setImportHeaderColumn("GeoCode|State|County|City|Zip|Eff. Date");
						// newError.setImportColumnValue(bcpGeocode+"|"+bcpState+"|"+bcpCounty+"|"+bcpCity+"|"+bcpZip+"|"+bcpRateEffectiveDateStr);
						newError.setImportColumnValue(bcpJurisdictionTaxRate.getGeocode()+"|"+bcpJurisdictionTaxRate.getState()+"|"+bcpJurisdictionTaxRate.getCountry()+"|"+bcpJurisdictionTaxRate.getCity()+"|"+bcpJurisdictionTaxRate.getZip());
						newError.setTransDtlColumnName("n/a");
						newError.setTransDtlDatatype("n/a");
						newError.setImportRow("n/a");
			    	  
						entityManager.persist(newError);	   						
					}
					
					//-- Update TaxRate				
					List<JurisdictionTaxrate> jurisdictionTaxrateCursorQuery = entityManager.createQuery("from JurisdictionTaxrate where jurisdictionTaxrateId = :an_jurisdictionTaxrateId")
			                    .setParameter("an_jurisdictionTaxrateId", jurisdictionTaxrateId).getResultList();

					Iterator<JurisdictionTaxrate> jurisdictionTaxrateIter = jurisdictionTaxrateCursorQuery.iterator();
					JurisdictionTaxrate taxrate = null;	
					if (null!=jurisdictionTaxrateIter && jurisdictionTaxrateIter.hasNext()) {
						taxrate = jurisdictionTaxrateIter.next();
					}
					
					populateTaxrate(taxrate, bcpJurisdictionTaxRate);
					taxrate.setModifiedFlag("0");
					taxrate.setUpdateUserId("STSCORP");

                    entityManager.persist(taxrate);
				}

			} // end-loop bcp_juris_taxrate_cursor
		
			//-- Remove Batch from BCP Transactions Table			
			session.createSQLQuery("DELETE FROM TB_BCP_JURISDICTION_TAXRATE WHERE batch_id = :anBatchId").setParameter("anBatchId", batchId).executeUpdate();
			
			//-- Update Options table with last month/year updated
			DateFormat df = new SimpleDateFormat("yyyy_MM");
			String vd_update_date_string = df.format(updateDate);
			session.createSQLQuery("UPDATE tb_option SET value = :date WHERE  option_code = 'LASTTAXRATEDATEUPDATE' AND option_type_code = 'SYSTEM' AND user_code = 'SYSTEM'").setParameter("date", vd_update_date_string).executeUpdate();
			
			//-- Update Options table with last release number
			session.createSQLQuery("UPDATE tb_option SET value = :release WHERE  option_code = 'LASTTAXRATERELUPDATE' AND option_type_code = 'SYSTEM' AND user_code = 'SYSTEM'").setParameter("release", releaseNumber).executeUpdate();
			
			//-- Update batch with final totals
			Date actualEndTimeStamp = new Date(System.currentTimeMillis());
			session.createSQLQuery("UPDATE tb_batch SET BATCH_STATUS_CODE = 'P', ERROR_SEV_CODE = :errorSevCode, PROCESSED_ROWS = :processedRows, actual_end_timestamp = :actualEndTimestamp WHERE  batch_Id = :batchId ")
			.setParameter("errorSevCode", errorSeverityCode)
			.setParameter("processedRows", processedRows)
			.setParameter("actualEndTimestamp", actualEndTimeStamp)
			.setParameter("batchId",batchId).executeUpdate();
			
			//-- commit all updates
			//	entityManager.getTransaction().commit();
			tx.commit();
		} 
		catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
			
			batchStatusCode  = "ABP";
			
			//-- Update batch with error codes
			String abortSql = " UPDATE tb_batch SET batch_status_code = :batchStatusCode," + 
									" error_sev_code = :errorSevCode," + 
									" actual_end_timestamp = :timeStamp" + 
									" WHERE batch_id = :batchId";
			SQLQuery abortyQuery = session.createSQLQuery(abortSql);
			abortyQuery.setParameter("batchStatusCode", batchStatusCode);
			abortyQuery.setParameter("errorSevCode", errorSeverityCode);
			abortyQuery.setParameter("timeStamp", new Date(System.currentTimeMillis()));
			abortyQuery.setParameter("batchId", batchId);
			abortyQuery.executeUpdate();			
		}
		finally {		
			closeLocalSession(session);
			session=null;
		}						
	} 
	
	private void populateJurisdiction(Jurisdiction jurisdiction, BCPJurisdictionTaxRate bcptaxrate){
		jurisdiction.setCountry(bcptaxrate.getCountry());  
		jurisdiction.setGeocode(bcptaxrate.getGeocode());
		jurisdiction.setState(bcptaxrate.getState());
		jurisdiction.setCounty(bcptaxrate.getCounty());
		jurisdiction.setCity(bcptaxrate.getCity());
		jurisdiction.setReportingCity(bcptaxrate.getReportingCity());
		jurisdiction.setReportingCityFips(bcptaxrate.getReportingCityFips());
		jurisdiction.setStj1Name(bcptaxrate.getStj1Name());
		jurisdiction.setStj2Name(bcptaxrate.getStj2Name());
		jurisdiction.setStj3Name(bcptaxrate.getStj3Name());
		jurisdiction.setStj4Name(bcptaxrate.getStj4Name());
		jurisdiction.setStj5Name(bcptaxrate.getStj5Name());
		jurisdiction.setZip(bcptaxrate.getZip());
		jurisdiction.setZipplus4(bcptaxrate.getZipplus4());
		jurisdiction.setInOut(bcptaxrate.getInOut());
		jurisdiction.setEffectiveDate(bcptaxrate.getJurEffectiveDate());
		jurisdiction.setExpirationDate(bcptaxrate.getJurExpirationDate());
		jurisdiction.setDescription(bcptaxrate.getDescription());
		jurisdiction.setCountryNexusindCode(bcptaxrate.getCountryNexusindCode());
		jurisdiction.setStateNexusindCode(bcptaxrate.getStateNexusindCode());
		jurisdiction.setCountyNexusindCode(bcptaxrate.getCountyNexusindCode());
		jurisdiction.setCityNexusindCode(bcptaxrate.getCityNexusindCode());
		jurisdiction.setStj1NexusindCode(bcptaxrate.getStj1NexusindCode());
		jurisdiction.setStj2NexusindCode(bcptaxrate.getStj2NexusindCode());
		jurisdiction.setStj3NexusindCode(bcptaxrate.getStj3NexusindCode());
		jurisdiction.setStj4NexusindCode(bcptaxrate.getStj4NexusindCode());
		jurisdiction.setStj5NexusindCode(bcptaxrate.getStj5NexusindCode());
		jurisdiction.setStj1LocalCode(bcptaxrate.getStj1LocalCode());
		jurisdiction.setStj2LocalCode(bcptaxrate.getStj2LocalCode());
		jurisdiction.setStj3LocalCode(bcptaxrate.getStj3LocalCode());
		jurisdiction.setStj4LocalCode(bcptaxrate.getStj4LocalCode());
		jurisdiction.setStj5LocalCode(bcptaxrate.getStj5LocalCode());
	}
	
	private void populateTaxrate(JurisdictionTaxrate taxrate, BCPJurisdictionTaxRate bcptaxrate){
		taxrate.setRatetypeCode(bcptaxrate.getRatetypeCode());
		taxrate.setEffectiveDate(bcptaxrate.getRateEffectiveDate());
		taxrate.setExpirationDate(bcptaxrate.getRateExpirationDate());
		taxrate.setCountrySalesTier1Rate((null!=bcptaxrate.getCountrySalesTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier2Rate((null!=bcptaxrate.getCountrySalesTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier3Rate((null!=bcptaxrate.getCountrySalesTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier1Setamt((null!=bcptaxrate.getCountrySalesTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier2Setamt((null!=bcptaxrate.getCountrySalesTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier3Setamt((null!=bcptaxrate.getCountrySalesTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier1MaxAmt((null!=bcptaxrate.getCountrySalesTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier2MinAmt((null!=bcptaxrate.getCountrySalesTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier2MaxAmt((null!=bcptaxrate.getCountrySalesTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountrySalesTier2EntamFlag(bcptaxrate.getCountrySalesTier2EntamFlag());
		taxrate.setCountrySalesTier3EntamFlag(bcptaxrate.getCountrySalesTier3EntamFlag());
		taxrate.setCountrySalesMaxtaxAmt((null!=bcptaxrate.getCountrySalesMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountrySalesMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier1Rate((null!=bcptaxrate.getCountryUseTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier2Rate((null!=bcptaxrate.getCountryUseTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier3Rate((null!=bcptaxrate.getCountryUseTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier1Setamt((null!=bcptaxrate.getCountryUseTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier2Setamt((null!=bcptaxrate.getCountryUseTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier3Setamt((null!=bcptaxrate.getCountryUseTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier1MaxAmt((null!=bcptaxrate.getCountryUseTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier2MinAmt((null!=bcptaxrate.getCountryUseTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier2MaxAmt((null!=bcptaxrate.getCountryUseTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountryUseTier2EntamFlag(bcptaxrate.getCountryUseTier2EntamFlag());
		taxrate.setCountryUseTier3EntamFlag(bcptaxrate.getCountryUseTier3EntamFlag());
		taxrate.setCountryUseMaxtaxAmt((null!=bcptaxrate.getCountryUseMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountryUseMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountryUseSameSales(bcptaxrate.getCountryUseSameSales());
		taxrate.setStateSalesTier1Rate((null!=bcptaxrate.getStateSalesTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier1Rate()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier2Rate((null!=bcptaxrate.getStateSalesTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier2Rate()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier3Rate((null!=bcptaxrate.getStateSalesTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier3Rate()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier1Setamt((null!=bcptaxrate.getStateSalesTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier2Setamt((null!=bcptaxrate.getStateSalesTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier3Setamt((null!=bcptaxrate.getStateSalesTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier1MaxAmt((null!=bcptaxrate.getStateSalesTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier2MinAmt((null!=bcptaxrate.getStateSalesTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier2MaxAmt((null!=bcptaxrate.getStateSalesTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateSalesTier2EntamFlag(bcptaxrate.getStateSalesTier2EntamFlag());
		taxrate.setStateSalesTier3EntamFlag(bcptaxrate.getStateSalesTier3EntamFlag());
		taxrate.setStateSalesMaxtaxAmt((null!=bcptaxrate.getStateSalesMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateSalesMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier1Rate((null!=bcptaxrate.getStateUseTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier1Rate()) : BigDecimal.ZERO);
		taxrate.setStateUseTier2Rate((null!=bcptaxrate.getStateUseTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier2Rate()) : BigDecimal.ZERO);
		taxrate.setStateUseTier3Rate((null!=bcptaxrate.getStateUseTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier3Rate()) : BigDecimal.ZERO);
		taxrate.setStateUseTier1Setamt((null!=bcptaxrate.getStateUseTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier2Setamt((null!=bcptaxrate.getStateUseTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier3Setamt((null!=bcptaxrate.getStateUseTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier1MaxAmt((null!=bcptaxrate.getStateUseTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier2MinAmt((null!=bcptaxrate.getStateUseTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier2MaxAmt((null!=bcptaxrate.getStateUseTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateUseTier2EntamFlag(bcptaxrate.getStateUseTier2EntamFlag());
		taxrate.setStateUseTier3EntamFlag(bcptaxrate.getStateUseTier3EntamFlag());
		taxrate.setStateUseMaxtaxAmt((null!=bcptaxrate.getStateUseMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getStateUseMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setStateUseSameSales(bcptaxrate.getStateUseSameSales());
		taxrate.setCountySalesTier1Rate((null!=bcptaxrate.getCountySalesTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier2Rate((null!=bcptaxrate.getCountySalesTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier3Rate((null!=bcptaxrate.getCountySalesTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier1Setamt((null!=bcptaxrate.getCountySalesTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier2Setamt((null!=bcptaxrate.getCountySalesTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier3Setamt((null!=bcptaxrate.getCountySalesTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier1MaxAmt((null!=bcptaxrate.getCountySalesTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier2MinAmt((null!=bcptaxrate.getCountySalesTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier2MaxAmt((null!=bcptaxrate.getCountySalesTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountySalesTier2EntamFlag(bcptaxrate.getCountySalesTier2EntamFlag());
		taxrate.setCountySalesTier3EntamFlag(bcptaxrate.getCountySalesTier3EntamFlag());
		taxrate.setCountySalesMaxtaxAmt((null!=bcptaxrate.getCountySalesMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountySalesMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier1Rate((null!=bcptaxrate.getCountyUseTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier2Rate((null!=bcptaxrate.getCountyUseTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier3Rate((null!=bcptaxrate.getCountyUseTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier1Setamt((null!=bcptaxrate.getCountyUseTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier2Setamt((null!=bcptaxrate.getCountyUseTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier3Setamt((null!=bcptaxrate.getCountyUseTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier1MaxAmt((null!=bcptaxrate.getCountyUseTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier2MinAmt((null!=bcptaxrate.getCountyUseTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier2MaxAmt((null!=bcptaxrate.getCountyUseTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountyUseTier2EntamFlag(bcptaxrate.getCountyUseTier2EntamFlag());
		taxrate.setCountyUseTier3EntamFlag(bcptaxrate.getCountyUseTier3EntamFlag());
		taxrate.setCountyUseMaxtaxAmt((null!=bcptaxrate.getCountyUseMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCountyUseMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCountyUseSameSales(bcptaxrate.getCountyUseSameSales());
		taxrate.setCitySalesTier1Rate((null!=bcptaxrate.getCitySalesTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier2Rate((null!=bcptaxrate.getCitySalesTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier3Rate((null!=bcptaxrate.getCitySalesTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier1Setamt((null!=bcptaxrate.getCitySalesTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier2Setamt((null!=bcptaxrate.getCitySalesTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier3Setamt((null!=bcptaxrate.getCitySalesTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier1MaxAmt((null!=bcptaxrate.getCitySalesTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier2MinAmt((null!=bcptaxrate.getCitySalesTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier2MaxAmt((null!=bcptaxrate.getCitySalesTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCitySalesTier2EntamFlag(bcptaxrate.getCitySalesTier2EntamFlag());
		taxrate.setCitySalesTier3EntamFlag(bcptaxrate.getCitySalesTier3EntamFlag());
		taxrate.setCitySalesMaxtaxAmt((null!=bcptaxrate.getCitySalesMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCitySalesMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier1Rate((null!=bcptaxrate.getCityUseTier1Rate()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier1Rate()) : BigDecimal.ZERO);
		taxrate.setCityUseTier2Rate((null!=bcptaxrate.getCityUseTier2Rate()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier2Rate()) : BigDecimal.ZERO);
		taxrate.setCityUseTier3Rate((null!=bcptaxrate.getCityUseTier3Rate()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier3Rate()) : BigDecimal.ZERO);
		taxrate.setCityUseTier1Setamt((null!=bcptaxrate.getCityUseTier1Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier1Setamt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier2Setamt((null!=bcptaxrate.getCityUseTier2Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier2Setamt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier3Setamt((null!=bcptaxrate.getCityUseTier3Setamt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier3Setamt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier1MaxAmt((null!=bcptaxrate.getCityUseTier1MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier1MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier2MinAmt((null!=bcptaxrate.getCityUseTier2MinAmt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier2MinAmt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier2MaxAmt((null!=bcptaxrate.getCityUseTier2MaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseTier2MaxAmt()) : BigDecimal.ZERO);
		taxrate.setCityUseTier2EntamFlag(bcptaxrate.getCityUseTier2EntamFlag());
		taxrate.setCityUseTier3EntamFlag(bcptaxrate.getCityUseTier3EntamFlag());
		taxrate.setCityUseMaxtaxAmt((null!=bcptaxrate.getCityUseMaxtaxAmt()) ? BigDecimal.valueOf(bcptaxrate.getCityUseMaxtaxAmt()) : BigDecimal.ZERO);
		taxrate.setCityUseSameSales(bcptaxrate.getCityUseSameSales());
		taxrate.setStj1SalesRate((null!=bcptaxrate.getStj1SalesRate()) ? BigDecimal.valueOf(bcptaxrate.getStj1SalesRate()) : BigDecimal.ZERO);
		taxrate.setStj1SalesSetamt((null!=bcptaxrate.getStj1SalesSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj1SalesSetamt()) : BigDecimal.ZERO);
		taxrate.setStj2SalesRate((null!=bcptaxrate.getStj2SalesRate()) ? BigDecimal.valueOf(bcptaxrate.getStj2SalesRate()) : BigDecimal.ZERO);
		taxrate.setStj2SalesSetamt((null!=bcptaxrate.getStj2SalesSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj2SalesSetamt()) : BigDecimal.ZERO);
		taxrate.setStj3SalesRate((null!=bcptaxrate.getStj3SalesRate()) ? BigDecimal.valueOf(bcptaxrate.getStj3SalesRate()) : BigDecimal.ZERO);
		taxrate.setStj3SalesSetamt((null!=bcptaxrate.getStj3SalesSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj3SalesSetamt()) : BigDecimal.ZERO);
		taxrate.setStj4SalesRate((null!=bcptaxrate.getStj4SalesRate()) ? BigDecimal.valueOf(bcptaxrate.getStj4SalesRate()) : BigDecimal.ZERO);
		taxrate.setStj4SalesSetamt((null!=bcptaxrate.getStj4SalesSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj4SalesSetamt()) : BigDecimal.ZERO);
		taxrate.setStj5SalesRate((null!=bcptaxrate.getStj5SalesRate()) ? BigDecimal.valueOf(bcptaxrate.getStj5SalesRate()) : BigDecimal.ZERO);
		taxrate.setStj5SalesSetamt((null!=bcptaxrate.getStj5SalesSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj5SalesSetamt()) : BigDecimal.ZERO);
		taxrate.setStj1UseRate((null!=bcptaxrate.getStj1UseRate()) ? BigDecimal.valueOf(bcptaxrate.getStj1UseRate()) : BigDecimal.ZERO);
		taxrate.setStj1UseSetamt((null!=bcptaxrate.getStj1UseSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj1UseSetamt()) : BigDecimal.ZERO);
		taxrate.setStj2UseRate((null!=bcptaxrate.getStj2UseRate()) ? BigDecimal.valueOf(bcptaxrate.getStj2UseRate()) : BigDecimal.ZERO);
		taxrate.setStj2UseSetamt((null!=bcptaxrate.getStj2UseSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj2UseSetamt()) : BigDecimal.ZERO);
		taxrate.setStj3UseRate((null!=bcptaxrate.getStj3UseRate()) ? BigDecimal.valueOf(bcptaxrate.getStj3UseRate()) : BigDecimal.ZERO);
		taxrate.setStj3UseSetamt((null!=bcptaxrate.getStj3UseSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj3UseSetamt()) : BigDecimal.ZERO);
		taxrate.setStj4UseRate((null!=bcptaxrate.getStj4UseRate()) ? BigDecimal.valueOf(bcptaxrate.getStj4UseRate()) : BigDecimal.ZERO);
		taxrate.setStj4UseSetamt((null!=bcptaxrate.getStj4UseSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj4UseSetamt()) : BigDecimal.ZERO);
		taxrate.setStj5UseRate((null!=bcptaxrate.getStj5UseRate()) ? BigDecimal.valueOf(bcptaxrate.getStj5UseRate()) : BigDecimal.ZERO);
		taxrate.setStj5UseSetamt((null!=bcptaxrate.getStj5UseSetamt()) ? BigDecimal.valueOf(bcptaxrate.getStj5UseSetamt()) : BigDecimal.ZERO);
		taxrate.setStjUseSameSales(bcptaxrate.getStjUseSameSales());
		taxrate.setCombinedUseRate((null!=bcptaxrate.getCombinedUseRate()) ? BigDecimal.valueOf(bcptaxrate.getCombinedUseRate()) : BigDecimal.ZERO);
		taxrate.setCombinedSalesRate((null!=bcptaxrate.getCombinedSalesRate()) ? BigDecimal.valueOf(bcptaxrate.getCombinedSalesRate()) : BigDecimal.ZERO);
		//taxrate.setUpdateFlag(bcptaxrate.getUpdateFlag());
	}	
}

		

