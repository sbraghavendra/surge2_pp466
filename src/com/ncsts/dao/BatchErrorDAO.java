package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.dto.ErrorLogDTO;

/**
 *
 */
public interface BatchErrorDAO 	extends GenericDAO<ErrorLogDTO, Long> {
	public Long count(BatchErrorLog batchErrorLog);
	public List<BatchErrorLog> getPage(BatchErrorLog batchErrorLog, int firstRow, int maxResults) throws DataAccessException;
}
