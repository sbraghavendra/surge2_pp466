package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BCPBillTransaction;

public interface BcpSaleTransactionExDAO {
	public List<BCPBillTransaction> findByExample(
			BCPBillTransaction exampleInstance, String[] sortField,
			boolean[] ascending, int offset, int count);

	public Long count(BCPBillTransaction exampleInstance);
}
