package com.ncsts.dao;

import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHCodePK;

/**
 * @author Paul Govindan
 *
 */

public interface CCHCodeDAO extends GenericDAO<CCHCode,CCHCodePK> {
}
