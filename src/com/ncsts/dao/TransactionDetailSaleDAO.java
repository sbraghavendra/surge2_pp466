package com.ncsts.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.TransactionDetail;
import com.seconddecimal.billing.domain.SaleTransaction;

public interface TransactionDetailSaleDAO extends GenericDAO<SaleTransaction, Long> {

	public Long count(SaleTransaction exampleInstance);
	
	public List<SaleTransaction> getAllRecords(SaleTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults);

	HashMap<String, HashMap<String, String>> getNexusInfo(Long saleTransactionId);
	
}
