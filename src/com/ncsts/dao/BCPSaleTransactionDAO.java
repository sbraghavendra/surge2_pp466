package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;

public interface BCPSaleTransactionDAO {
	 public List<BCPBillTransaction> findBCPBillTransactionsByBatchId(Long processBatchNo, int firstRow, int maxResults);
	 public int findBCPCountByBatchId(Long processBatchNo);
	 public void deleteBCPBillTransactionsByBatchId(Long processBatchNo);

}
