package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.BillTransactionLog;

public interface BillTransactionLogDAO extends GenericDAO<BillTransactionLog, Long> {

}
