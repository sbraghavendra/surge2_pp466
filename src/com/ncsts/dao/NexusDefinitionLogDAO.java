package com.ncsts.dao;

import com.ncsts.domain.NexusDefinitionLog;

public interface NexusDefinitionLogDAO extends GenericDAO<NexusDefinitionLog, Long> {

}
