package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BCPConfigStagingText;

public interface ConfigImportDAO {
	public List<BCPConfigStagingText> getImportedData(Long batchId);
}
