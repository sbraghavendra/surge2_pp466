package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRule;
import com.ncsts.domain.BatchMaintenance;

public class JPABCPTaxCodeRuleDAO extends JPAGenericDAO<BCPTaxCodeRule, Long> implements BCPTaxCodeRuleDAO {

	@Override
	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPTaxCodeRule.class);
		criteria.add(Restrictions.eq("batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
		
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}

	@Override
	public List<BCPTaxCodeRule> getPage(BatchMaintenance batch, int firstRow,
			int maxResults) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPTaxCodeRule.class);
		criteria.add(Restrictions.eq("batchId", batch.getBatchId()));
		
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		try {
			List<BCPTaxCodeRule> returnBCPTaxCodeRule = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPTaxCodeRule;
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPTaxCodeRule>();
		}
	}
}
