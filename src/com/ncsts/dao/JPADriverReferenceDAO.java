package com.ncsts.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ncsts.domain.DataDefinitionColumnDrivers;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;

public class JPADriverReferenceDAO extends JpaDaoSupport implements DriverReferenceDAO {

	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

	@SuppressWarnings("unchecked")
	@Transactional
	public List<DriverReference> findDriverReference(String transDtlColName, String driverValue, String driverDesc, String userValue, int count) throws DataAccessException {
		List<DriverReference> result = null;
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String sql = "SELECT d FROM DriverReference d where d.id.transDetailColName = :columnName";
		boolean hasParams = false;
		if ((driverValue != null) && !driverValue.equals("")) {
			sql += " and upper(d.id.driverValue) like :driverValue";
			hasParams = true;
		}
		if ((driverDesc != null) && !driverDesc.equals("")) {
			sql += " and upper(d.driverDesc) like :driverDesc";
			hasParams = true;
		}
		if ((userValue != null) && !userValue.equals("")) {
			sql += " and upper(d.userValue) like :userValue";
			hasParams = true;
		}
		
		Query query = entityManager.createQuery(sql + " order by d.id.driverValue");
		query.setParameter("columnName", transDtlColName);
		
		//PP-436
		if ((driverValue != null) && !driverValue.equals("")) {
			query.setParameter("driverValue", "%" + driverValue.toUpperCase() + "%");
		}
		if ((driverDesc != null) && !driverDesc.equals("")) {
			query.setParameter("driverDesc", "%" + driverDesc.toUpperCase() + "%");
		}
		if ((userValue != null) && !userValue.equals("")) {
			query.setParameter("userValue", "%" + userValue.toUpperCase() + "%");
		}
		
		if (count > 0) {
			query.setMaxResults(count);	// Only return a few at a time
		}
		
		result = query.getResultList();
		//PP-177
		//if (((result == null) || (result.size() == 0)) && hasParams) {
			// No matches, so return list of values without any filtering
		//	result = findByTransactionDetailColumnName(transDtlColName, count);
		//}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<DriverReference> findByDriverName(String drvValue, String columnName ,int count) throws DataAccessException {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query query = entityManager.createQuery(" SELECT driver FROM DriverReference driver where driver.id.transDetailColName = :columnName and driver.id.driverValue like :pattern"); 
		query.setParameter("columnName", columnName);
		query.setParameter("pattern", drvValue + "%");	
		if (count > 0) {
			query.setMaxResults(count);	// Only return a few at a time
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<DriverReference> findByTransactionDetailColumnName(String columnName, int count) throws DataAccessException {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query query = entityManager.createQuery(
				"from DriverReference d where d.id.transDetailColName = :columnName order by d.id.driverValue");
		query.setParameter("columnName", columnName);
		if (count > 0) {
			query.setMaxResults(count);
		}
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public DriverReference getDriverReference(String transDtlColName, String driverValue) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query query = entityManager.createQuery("select d from DriverReference d where" + 
        		" d.id.transDetailColName = :columnName and" +
        		" upper(d.id.driverValue) = :driverValue");
		query.setParameter("columnName", transDtlColName);
		query.setParameter("driverValue", driverValue.toUpperCase());
		List<DriverReference> result = (List<DriverReference>) query.getResultList();
        return ((result == null) || (result.size() == 0))? null:result.get(0);
	}

	@Transactional
	public Long countDriverReference(String transDtlColName, String driverValue) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query query = entityManager.createQuery("select count(*) from DriverReference d where" + 
        		" d.id.transDetailColName = :columnName and" +
        		" upper(d.id.driverValue) = :driverValue");
		query.setParameter("columnName", transDtlColName);
		query.setParameter("driverValue", driverValue.toUpperCase());
        return (Long)query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getDriverNamesByDrvCodeMatrixCol(String drvCode, String mtrxCol) throws DataAccessException {
		List<String> find = getJpaTemplate().find(
				"select dn.transDtlColName from DriverNames dn"
						+ " where dn.id.drvNamesCode = ?1 and dn.matrixColName = ?2", drvCode, mtrxCol);

		return find;
	}	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getDriverCategories() throws DataAccessException {
		List<String> driverCatories = getJpaTemplate().find(
				"select distinct dn.driverNamesCode from DriverNames dn " + "order by dn.driverNamesCode");
		return driverCatories;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<DriverNames> getDriverNamesByCode(String code) throws DataAccessException {
		List<DriverNames> driverNames;
		if ((code == null) || code.equals("")) {
			driverNames = getJpaTemplate().find(
					"from DriverNames dn order by dn.id.driverId");
		} else {
			driverNames = getJpaTemplate().find(
					"from DriverNames dn where dn.id.drvNamesCode = ?1 order by dn.id.driverId", code.toUpperCase());
		}

		return driverNames;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(DriverReference driverRef) throws DataAccessException {
		try {
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			// Following will attach the object to the session
			session.update(driverRef);
			session.delete(driverRef);
			t.commit();
			closeLocalSession(session);
			session=null;
		} catch (DataAccessException e) {
			e.printStackTrace();
		}

	}

	@Transactional(propagation = Propagation.NEVER, readOnly = false)
	public void saveOrUpdate(DriverReference driverRef) throws DataAccessException {
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.saveOrUpdate(driverRef);
		t.commit();
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional
	public DriverNames findDriverForTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException {
		DriverNames driverNames = null;
		// bug 4934
		List<?> find = getJpaTemplate().find("select dn from DriverNames dn where "
				+ "dn.transDtlColName = ?1 and dn.id.drvNamesCode = ?2 and dn.matrixColName <> ?3",
				columnName, code, matrixColName);
		if(find != null && find.size() >0 && find.get(0) != null){
			driverNames = (DriverNames)find.get(0);
			logger.debug("Driver Name is " +find.get(0));
	        logger.debug("Driver Id returned is " + driverNames.getDriverId()); 
		}
		
		return driverNames;
	}
	
	@Transactional
	public DriverNames findTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException {
		DriverNames driverNames = null;
		// Add matrixColName condition for bug 4356
		
		/* bug 4934, move the following code to findDriverForTransactionDetailColumnName to prevent duplicate
		List<?> find = getJpaTemplate().find("select dn from DriverNames dn where "
				+ "dn.transDtlColName = ?1 and dn.id.drvNamesCode = ?2 and dn.matrixColName <> ?3",
				columnName, code, matrixColName);
		if(find != null && find.size() >0 && find.get(0) != null){
			driverNames = (DriverNames)find.get(0);
			logger.debug("Driver Name is " +find.get(0));
	        logger.debug("Driver Id returned is " + driverNames.getDriverId()); 
	        return driverNames;
		}
		*/
		driverNames = findDriverForTransactionDetailColumnName(columnName, code, matrixColName);
		if(driverNames!=null){
			return driverNames;
		}
		
		String hql = null;
		String colName = matrixColName.toLowerCase().replaceAll("_", "");
		if (code.equals("T")) {
			hql = "select count(*) from TaxMatrix t where t."
				+ colName + " is not null and t.moduleCode = 'PURCHUSE' " 
				+ " and t." + colName + " <> \'*ALL\'"; 
		} else if (code.equals("L")) {
			hql = "select count(*) from LocationMatrix l where l."
				+ colName + " is not null " 
				+ " and l." + colName + " <> \'*ALL\'"; 
		} else if (code.equals("E")) {
			hql = "select count(*) from GlExtractMap e where e."
				+ colName + " is not null"; 
		}else if (code.equals("GSB")) {
			hql = "select count(*) from TaxMatrix g where g."
					+ colName + " is not null and g.moduleCode = 'BILLSALE' " 
					+ " and g." + colName + " <> \'*ALL\'"; 
		}
		
		List<?> find = getJpaTemplate().find(hql);
		if (((Long)find.get(0)) > 0) {
			driverNames = new DriverNames();
			driverNames.setMatrixColName(matrixColName);
			driverNames.setTransDtlColName(columnName);
		}
		return driverNames;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Long getDriverNamesId(String drvCode) throws DataAccessException {
		Long driverId = 0L;
		List<Long> find = getJpaTemplate().find(
				"select max(dn.driverNamesPK.driverId) from DriverNames dn"
						+ " where dn.id.drvNamesCode = ?1", drvCode);
		if(find != null && find.size() >0 && find.get(0) != null){
			driverId = find.get(0);
		}
		logger.debug("Driver Id returned is " + driverId);	
		return driverId;
	}
	
	@Transactional(propagation = Propagation.NEVER, readOnly = false)
	public void saveOrUpdateNames(DriverNames driverName) throws DataAccessException {
		Session session = createLocalSession();

		Transaction t = session.beginTransaction();
		t.begin();
		session.saveOrUpdate(driverName);
		t.commit();
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteNames(DriverNames driverName) throws DataAccessException {
		try {
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			// Following will attach the object to the session
			session.update(driverName);
			session.delete(driverName);
			t.commit();
			closeLocalSession(session);
			session=null;
		} catch (DataAccessException e) {
			e.printStackTrace();
		}

	}
	
	//4356 BB
	//I think this is all kind of broken?
	//like driverID tends to be a Matrix Column Name like "driver_07", not a specific Trans.Detail Column Name like "Tax Pd to Vendor"
	//is there some kind of design flaw, like historic data wise? i.e. we have transactions that use driver_07 but if the column that 
	//referred to changed, wouldn't that be a problem?
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean getTransactionDetailUpdate(String category,String driverId) throws DataAccessException{
		logger.info("------------------>>>Inside Jpa call >>>--------------------------------------------------- ");
		List list=null;
		boolean matrixIsInUse=false;
		//boolean driverInUse=false;
		String driverProperty=driverId.replace("_", " ").replace(" ", "").toLowerCase();
		/* see 4356 comment below....
		List<String> listColumnName =getJpaTemplate().find(
				"select transDtlColName from DriverNames driverNames where driverNames.id.drvNamesCode=?1 and driverNames.matrixColName=?2",category,driverId);
				
		String trRetailColumnName=listColumnName.get(0);*/
		//*************************************************************************************************************
		//*************************************here table column name is dynamic***************************************
		//*************************************************************************************************************
		
		if(category.equals("E")){
			list=getJpaTemplate().find("select glExtractMap from GlExtractMap glExtractMap where glExtractMap."+driverProperty+"!=?1","*ALL");
			matrixIsInUse=(list.size()>0);
		}
		
		if(category.equals("L")){
			list=getJpaTemplate().find("select locationMatrix from LocationMatrix locationMatrix where locationMatrix."+driverProperty+"!=?1","*ALL");
			matrixIsInUse=(list.size()>0);
		}
		
		if(category.equals("T")){
			list=getJpaTemplate().find("select taxMatrix from TaxMatrix taxMatrix where taxMatrix."+driverProperty+"!=?1","*ALL");
			matrixIsInUse=(list.size()>0);
		}
		if(category.equals("GSB")){
			list=getJpaTemplate().find("select taxMatrix from GSBMatrix taxMatrix where taxMatrix."+driverProperty+"!=?1","*ALL");
			matrixIsInUse=(list.size()>0);
		}
		//according to mfullers comment 7468 on 4356 we should not be paying attention to DriverReference at all
		/*if(category.equals("E")||category.equals("L")||category.equals("T")){
		list=getJpaTemplate().find("select d from DriverReference d where d.id.transDetailColName = ?1 and upper(d.id.driverValue) != ?2",trRetailColumnName,"*ALL");
		driverInUse=(list.size()>0);
		}*/
		return (!matrixIsInUse); // && !driverInUse );
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String findCurrentTrColumnName(String category, String driverId) throws DataAccessException {
		List<String> listColumnName =getJpaTemplate().find(
				"select transDtlColName from DriverNames driverNames where driverNames.id.drvNamesCode=?1 and driverNames.matrixColName=?2",category,driverId);
		return listColumnName.get(0);
		
	}
	
	@Transactional
	public boolean allowColChange(String code, String matrixColName) throws DataAccessException {
		String hql = null;
		String colName = matrixColName.toLowerCase().replaceAll("_", "");
		if (code.equals("T")) {
			hql = "select count(*) from TaxMatrix t where t."
				+ colName + " is not null and t.moduleCode = 'PURCHUSE' " 
				+ " and t." + colName + " <> \'*ALL\'"; 
		} else if (code.equals("L")) {
			hql = "select count(*) from LocationMatrix l where l."
				+ colName + " is not null " 
				+ " and l." + colName + " <> \'*ALL\'"; 
		} else if (code.equals("E")) {
			hql = "select count(*) from GlExtractMap e where e."
				+ colName + " is not null"; 
		}else if (code.equals("GSB")) {
			hql = "select count(*) from TaxMatrix g where g."
					+ colName + " is not null and g.moduleCode = 'BILLSALE' " 
					+ " and g." + colName + " <> \'*ALL\'"; 
		} 

		List<?> find = getJpaTemplate().find(hql);
		return ((Long)find.get(0)) <= 0;
	}

	@Transactional
	public Map<String, DataDefinitionColumnDrivers> getValidDrivers(String driverNamesCode, String tableName) {

		Map<String, DataDefinitionColumnDrivers> result = new LinkedHashMap();
		Session session = createLocalSession();

		try {
			List<DataDefinitionColumnDrivers> drivers  = session.createCriteria(DataDefinitionColumnDrivers.class)
					.add(Restrictions.eq("driverNamesCode", driverNamesCode))
				//	.add(Restrictions.eq("dataDefinitionColumnPK.tableName", tableName))
					.add(Restrictions.eq("activeFlag", "1"))
					.addOrder(Order.asc("driverId")).list();
			for (DataDefinitionColumnDrivers driver : drivers) {
				result.put(driver.getTransDtlColName(), driver);
			}

		} catch (HibernateException hibex) {
			hibex.printStackTrace();
			logger.error("Error in finding List<LocationMatrix> getLocationMatrixByDrivers"+hibex.getMessage());
		} finally {
			closeLocalSession(session);
		}


		return result;
	}

	
}
