package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.BCPPurchaseTransaction;

public class JPABCPTransactionDetailDAO extends JPAGenericDAO<BCPPurchaseTransaction,Long> implements BCPTransactionDetailDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	@Override
	@Transactional(readOnly = false)
	public void saveSplits(BCPPurchaseTransaction original, 
			List<BCPPurchaseTransaction> splits, 
			List<Long> deletedIds,
			List<BCPPurchaseTransaction> allocationSplits){
		
		Long splitSubTransId = original.getSplitSubtransId();
		if(splitSubTransId == null) {
			splitSubTransId = getNextSplitSubTransId();
			original.setSplitSubtransId(splitSubTransId);
		}
		
		if(deletedIds != null) {
			for(Long id : deletedIds) {
				BCPPurchaseTransaction t = findById(id);
				if(t != null && splitSubTransId.equals(t.getSplitSubtransId())) {
					remove(t);
				}
			}
		}
		
		if(splits != null) {
			for (BCPPurchaseTransaction BCPPurchaseTransaction : splits) {
				if(BCPPurchaseTransaction.getSplitSubtransId() == null) {
					BCPPurchaseTransaction.setSplitSubtransId(splitSubTransId);
				}
				
				if(BCPPurchaseTransaction.getBcpTransactionId() == null) {
					save(BCPPurchaseTransaction);
				}
				else {
					update(BCPPurchaseTransaction);
				}
			}
		}
		
		if(allocationSplits != null) {
			for (BCPPurchaseTransaction BCPPurchaseTransaction : allocationSplits) {
				if(BCPPurchaseTransaction.getSplitSubtransId() == null) {
					BCPPurchaseTransaction.setSplitSubtransId(splitSubTransId);
				}
				
				if(BCPPurchaseTransaction.getBcpTransactionId() == null) {
					save(BCPPurchaseTransaction);
				}
				else {
					update(BCPPurchaseTransaction);
				}
			}
		}
		
		update(original);
	}
	
	@Transactional
	public long getNextSplitSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_SPLIT_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
	
	@Transactional(readOnly = false)
	public long getNextAllocSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_ALLOCATION_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
}
