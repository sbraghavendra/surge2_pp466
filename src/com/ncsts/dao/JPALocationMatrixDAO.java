package com.ncsts.dao;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LogMemory;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.view.bean.MatrixCommonBean;
import com.ncsts.common.Util;

/**
 * @author Anand
 *
 */

public class JPALocationMatrixDAO extends JPAMatrixDAO<LocationMatrix> implements LocationMatrixDAO {
	
	private Map<String,String> columnMapping = null;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	@Transactional(readOnly = false)
	public void save(LocationMatrix instance) {
		Jurisdiction j = instance.getJurisdiction();
		if (j != null) {
			j = entityManager.find(Jurisdiction.class, j.getJurisdictionId());
			instance.setJurisdiction(j);
		}
		instance.setTransactionDetail(new HashSet<TransactionDetail>());
		Transaction tx = null;
		Session session = createLocalSession();
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(instance);
			tx.commit();
			session.flush();
			session.clear();
			
			//purchaseTransactionService.processSuspendedTransactions(instance.getId(), PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_LOCN, false);
			
			/* ***!SPROC!*** Commented on 8/15/2017
			CallableStatement cstmnt = con.prepareCall("{call SP_TB_LOCATION_MATRIX_A_I(?)}");
			cstmnt.setLong(1, instance.getId());
			cstmnt.execute();
			cstmnt.close();
			con.commit();
			*/
		} catch (Exception e) {
			logger.error("Location Matrix update error: " + e);
			if (tx != null) {
				tx.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
	}
	
	@Override
	protected void extendExampleCriteria(Criteria criteria, LocationMatrix exampleInstance) {
        Long id = (exampleInstance == null)? null:exampleInstance.getId();
        if ((id != null) && (id > 0L)) {
        	logger.debug("Restricting Matrix search to Id: " + id);
        	criteria.add(Restrictions.idEq(id));
        } else {

        	// Add a restriction if there is a non-null jurisdiction
            if ((exampleInstance != null) && (exampleInstance.getJurisdiction() != null)) {
            	Criteria jurisdictionCrit = criteria.createCriteria("jurisdiction");
            	Jurisdiction jurisdiction = exampleInstance.getJurisdiction(); 
                id = jurisdiction.getJurisdictionId();
                if ((id != null) && (id > 0L)) {
                	logger.debug("Restricting Location Matrix search to Jurisdiction: " + id);
                	jurisdictionCrit.add(Restrictions.idEq(id));
                } else {
                	logger.debug("Including jurisdiction in location matrix search");
                	extendJurisdictionCriteria(jurisdictionCrit, "geocode", jurisdiction.getGeocode());
                	extendJurisdictionCriteria(jurisdictionCrit, "city", jurisdiction.getCity());
                	extendJurisdictionCriteria(jurisdictionCrit, "county", jurisdiction.getCounty());
                	extendJurisdictionCriteria(jurisdictionCrit, "state", jurisdiction.getState());
                	extendJurisdictionCriteria(jurisdictionCrit, "zip", jurisdiction.getZip());
                }
            }
        }
	}
	
	private void extendJurisdictionCriteria(Criteria criteria, String field, String value) {
		if ((value != null) && !value.trim().equals("")) {
			criteria.add(Restrictions.ilike(field, value, MatchMode.START));
		}
	}
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	private void buildDriverWhereClause(StringBuffer where, String driverName, String value){
		// TO ALLOW EXCACT OR WILDCARD SEARCH 02-04-2009 - BUG# 0003810
		if (value!=null && !value.trim().equals("")){
			if(value.endsWith("%")){
				where.append(" AND " + driverName + " LIKE '" + value.trim().toUpperCase()+"'");
			}else {
				where.append(" AND " + driverName + " = '" + value.trim().toUpperCase() + "'");
			}
		}
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 10; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}
			
			columnMapping.put("defaultflag", "DEFAULT_FLAG");
			columnMapping.put("binarywt", "BINARY_WEIGHT");
			columnMapping.put("defltbinarywt", "DEFAULT_BINARY_WEIGHT");
			columnMapping.put("entityid", "ENTITY_ID");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	// 02-04-2009 Method Modified for exact search and also wild card search
	// BUG# 0003810
	private String getWhereClauseToken(LocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			String defaultFlag, Long locationMatrixId){
		
		StringBuffer whereClauseExt = new StringBuffer();

		if (geocode!=null && !geocode.equals("")) {
    		if(geocode.endsWith("%")){
        	appendANDToWhere(whereClauseExt, "UPPER(GEOCODE) LIKE '" + geocode.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "GEOCODE ='" + geocode.toUpperCase() +"'");
			}
	    }
    	if (city!=null && !city.equals("")) {
    		if(city.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(CITY) LIKE '" + city.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "CITY ='" + city.toUpperCase() +"'");	
			}
    	}
    
    	if (county!=null && !county.equals("")) {
    		if(county.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(COUNTY) LIKE '" + county.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "COUNTY = '" + county.toUpperCase() +"'");	
			}
    	}
    
    	if (state!=null && !state.equals("")) {
    		//4346 SUB-BUG: in this query, "STATE" is ambiguous because it appears in both TB_LOCATION_MATRIX and TB_JURISDICTION
    		//but because the other WHERE things here are only in TB_JURISDICTION, I'm assuming this is the correct one
    		appendANDToWhere(whereClauseExt, "UPPER(TB_JURISDICTION.STATE) LIKE '" + state.toUpperCase() +"'");
    	}
    	
    	if (country!=null && !country.equals("")) {
    		appendANDToWhere(whereClauseExt, "UPPER(TB_JURISDICTION.COUNTRY) LIKE '" + country.toUpperCase() +"'");
    	}
    
    	if (zip!=null && !zip.equals("")) {
    		if(zip.endsWith("%")){
    		appendANDToWhere(whereClauseExt, "UPPER(ZIP) LIKE '" + zip.toUpperCase() +"'");
    		}else {
    			appendANDToWhere(whereClauseExt, "ZIP = '" + zip.toUpperCase() +"'");	
			}
    	}	    
	    //build 10 drivers
	    try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        for (int i = 1; i <= 10; i++) {
	        	name = "Driver" + formatter.format(i);
	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	buildDriverWhereClause(whereClauseExt, getDriverColumnName(name.toLowerCase()), (String)theMethod.invoke(exampleInstance, new Object[]{}));
	        }
        }
        catch (Exception ex) {
        }
        
        if (defaultFlag!=null && !defaultFlag.equals("")) {
	    	if (defaultFlag.equals(MatrixCommonBean.DEFAULT_LINE_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "default_flag='1'");
	    	}
	    	else if (defaultFlag.equals(MatrixCommonBean.DEFAULT_LINE_NON_GLOBAL_VALUE)) {
	    		appendANDToWhere(whereClauseExt, "default_flag='0' OR default_flag IS NULL");
	    	}
	    }

	    //for tax_matrix_id
	    if (locationMatrixId!=null && !locationMatrixId.equals("") && locationMatrixId!=0) {
	    	appendANDToWhere(whereClauseExt, "LOCATION_MATRIX_ID = " + locationMatrixId.longValue());
	    }
	    
	    //for entity_id
	    if (exampleInstance.getEntityId()!=null && exampleInstance.getEntityId().longValue()>=0L) {
	    	appendANDToWhere(whereClauseExt, "ENTITY_ID = " + exampleInstance.getEntityId().longValue());
	    }
	    
	    logger.debug("appendANDToWhere: " + whereClauseExt.toString());
		
	    return whereClauseExt.toString();
	}
	
	//Midtier project code modified - january 2009
	private List<LocationMatrix> getLocationMatrixList(ResultSet rs){
		List<LocationMatrix> result = new ArrayList<LocationMatrix>();
		
		long pseudoId = 1;
		try{
		while(rs.next()){
			LocationMatrix locationMatrix = new LocationMatrix();
			
			locationMatrix.setId(new Long(pseudoId++));
			locationMatrix.setEntityId(rs.getLong(1));
            
            try{
            	NumberFormat formatter = new DecimalFormat("00");
            	Method theMethod = null;
            	String name = "";
            	String value = "";
            	for (int i = 1; i <= 10; i++) {
            		name = "setDriver" + formatter.format(i);
            		value = rs.getString(i+1);
            		theMethod = locationMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
            		theMethod.setAccessible(true);
	        	
            		theMethod.invoke(locationMatrix, new Object[]{value});
            	}
            }
            catch (Exception ex) {
            	logger.info("ex = " + ex.getMessage());
            }
            
            locationMatrix.setBinaryWt(rs.getLong(12));
            locationMatrix.setDefaultFlag(rs.getString(13)); 
            locationMatrix.setDefltBinaryWt(rs.getLong(14));
            result.add(locationMatrix);                
		}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return result;
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				else{
					orderByToken.append(" ORDER BY ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	//Midtier project code modified - january 2009
	@Transactional
	public List<LocationMatrix> getAllRecords(LocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			String defaultFlag, Long locationMatrixId,
			OrderBy orderBy, int firstRow, int maxResults) {
		
		List<LocationMatrix> result = null;

		Connection con = null;
		try {
			
			String whereClauseToken = getWhereClauseToken(exampleInstance, geocode, city, county, state, country, zip, defaultFlag, locationMatrixId);
			String orderByToken = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			
			if (con != null) {
					CallableStatement cstatement = con
							.prepareCall("{ ? = call SP_TB_LOCATION_MTRX_MASTR_LIST(?,?,?,?)}");
					System.out.println(" super.getCursor() =  " + super.getCursor(con));
					cstatement.registerOutParameter(1, super.getCursor(con));						
					cstatement.setString(2, whereClauseToken);
					cstatement.setString(3, orderByToken);
					cstatement.setInt(4, firstRow);
					cstatement.setInt(5, maxResults);
					//cstatement.registerOutParameter(5, OracleTypes.CURSOR);
					System.out.println("\t Begin stored procedure call SP_TB_LOCATION_MTRX_MASTR_LIST on " + con.getMetaData().getURL());
					System.out.println("\t Arguments In are '"+whereClauseToken+"'  '"+orderByToken+"'  '"+firstRow+"'  '"+maxResults+"' ");
					long begintime = System.currentTimeMillis();
					cstatement.execute();
					ResultSet rs = (ResultSet) cstatement.getObject(1);
					result = getLocationMatrixList(rs);
					String time = Long.toString(System.currentTimeMillis() - begintime);
					System.out.println("\t *** Executing Store procedure, took " + time + " milliseconds. \n");
					cstatement.close();
					rs.close();
					con.commit();
					con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<LocationMatrix> getDetailRecords(LocationMatrix locationMatrix, OrderBy orderBy) {
		
		List<LocationMatrix> result = null;
				
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(LocationMatrix.class);
             
      	    if (locationMatrix.getDefaultFlag()!=null && !locationMatrix.getDefaultFlag().equals("")) {
      	    	criteria.add(Restrictions.eq("defaultFlag", locationMatrix.getDefaultFlag()));
      	    }
      	    
      	    if (locationMatrix.getEntityId()!=null && locationMatrix.getEntityId().longValue()>=0L) {
    	    	criteria.add(Restrictions.eq("entityId", locationMatrix.getEntityId()));
    	    }
                  
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 10; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = locationMatrix.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(locationMatrix, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		criteria.add(Restrictions.eq(name.toLowerCase(), value));
    	        	}
    	        }
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }

            result = criteria.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<LocationMatrix> getDetailRecords");
        }
        
        closeLocalSession(session);
        session=null;
			
        LogMemory.executeGC();
				
		return result;
		
	}

/*	@Override
	public List<LocationMatrix> getLocationMatrixByDrivers(Map<String, DriverNames> availableDrivers) {
*//*

		Session session = createLocalSession();

		Criteria criteria = session.createCriteria(DataDefinitionColumnDrivers.class).add(Restrictions.eq("driverNamesCode",null));

		try {

		} catch (HibernateException hibex) {
			hibex.printStackTrace();
			logger.error("Error in finding List<LocationMatrix> getLocationMatrixByDrivers"+hibex.getMessage());
		} finally {
			closeLocalSession(session);
		}

*//*


		return null;
	}*/






	//Midtier project code modified - january 2009
	@Transactional
	public Long count(LocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			String defaultFlag, Long locationMatrixId) {
		Long count = 0l;
		String whereClauseToken = getWhereClauseToken(exampleInstance, geocode,
				city, county, state, country, zip, defaultFlag, locationMatrixId);
		
		Connection con = null;
		try {
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				CallableStatement cstatement = con
						.prepareCall("{ ? = call SP_LOCATION_MTRX_MASTR_COUNT(?)}");
				System.out.println(" super.getCursor() =  " + super.getCursor(con));					
				cstatement.registerOutParameter(1, super.getCursor(con));				
				cstatement.setString(2, whereClauseToken);
				//cstatement.registerOutParameter(2, OracleTypes.CURSOR);
				System.out.println("\t Begin stored procedure call SP_LOCATION_MTRX_MASTR_COUNT \n");
				long begintime = System.currentTimeMillis();

				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);
				while(rs.next()){
					count = rs.getLong(1);
				}
				String time = Long.toString(System.currentTimeMillis()
						- begintime);
				System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
				cstatement.close();
				con.commit();
				con.close();

			}
		} catch (SQLException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public List<LocationMatrix> getMatchingLocationMatrixLines(PurchaseTransaction exampleInstance){
		List<LocationMatrix> listLocationMatrix = null;	
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String dateString = df.format(new Date());//today
		
		String entityId="";
		EntityItem entityItem = null;
		List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityCode = ?" , exampleInstance.getEntityCode());
		if(list != null && list.size() >0 && list.get(0) != null){
			entityItem = list.get(0);
			entityId = entityItem.getId().toString();
		}
		else{
			return null;
		}
		
		String sql = "select * from tb_location_matrix where default_flag='0' and binary_weight>0 :whereClause order by binary_weight desc, significant_digits desc, effective_date desc ";
		
		StringBuffer whereClause = new StringBuffer();	
		whereClause.append(" and entity_id=" + entityId);
		whereClause.append(" and effective_date <= to_date('" + dateString + "','mm/dd/yyyy') and expiration_date >= to_date('" + dateString + "','mm/dd/yyyy') ");
		List<DriverNames> driverNames = getJpaTemplate().find("from DriverNames dn where dn.id.drvNamesCode = ?1 order by dn.id.driverId", LocationMatrix.DRIVER_CODE);	
		try {
			String matrixColumnName = "";
			String transactionColumnName = "";
	        String value = "";
			for (DriverNames dn :driverNames) {
				value = "";		
				matrixColumnName = dn.getMatrixColName();
				transactionColumnName = Util.columnToProperty(dn.getTransDtlColName());		
				
				value = (String) Util.getProperty(exampleInstance, transactionColumnName);
				if(value==null || value.length()==0){
					value="";
				}
				value=value.toUpperCase();
				
				if(MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NULL and Wildcard
					whereClause.append(" and (");
					whereClause.append(" ('"+value+"' is null and (upper("+matrixColumnName+")='*ALL' or upper("+matrixColumnName+")='*NULL'))");
					whereClause.append(" or");
					whereClause.append(" ('"+value+"' is not null and (upper("+matrixColumnName+")='*ALL' or upper('"+value+"') like upper("+matrixColumnName+")))");
					whereClause.append(" )");
				}
				else if(MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && !MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NULL and NOT Wildcard
					whereClause.append(" and (");
					whereClause.append(" ('"+value+"' is null and (upper("+matrixColumnName+")='*ALL' or upper("+matrixColumnName+")='*NULL'))");
					whereClause.append(" or");
					whereClause.append(" ('"+value+"' is not null and (upper("+matrixColumnName+")='*ALL' or upper('"+value+"')=upper("+matrixColumnName+")))");
					whereClause.append(" )");		
				}
				else if(!MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NOT NULL and Wildcard
					whereClause.append(" and (");
					whereClause.append(" upper("+matrixColumnName+")='*ALL' or upper('"+value+"') like upper("+matrixColumnName+")");
					whereClause.append(" )");
				}
				else if(!MatrixCommonBean.getFlagValue(dn.getNullDriverFlag()) && !MatrixCommonBean.getFlagValue(dn.getWildcardFlag())){
					//-- NOT NULL and NOT Wildcard
					whereClause.append(" and (");  
					whereClause.append(" upper("+matrixColumnName+")='*ALL' or upper('"+value+"')=upper("+matrixColumnName+")");
					whereClause.append(" )");				
				}
			}
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        	return null;
        }	
		
		sql = sql.replaceAll(":whereClause", whereClause.toString());	
		Session session = createLocalSession();
        try {
        	org.hibernate.SQLQuery q = session.createSQLQuery(sql);
        	System.out.println(sql);
    		q.addEntity(LocationMatrix.class);
    		
			listLocationMatrix = q.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<LocationMatrix> getMatchingLocationMatrixLines");
        }
        finally{
        	closeLocalSession(session);
            session=null;
        }
        
        return listLocationMatrix;	
	}
	
}
