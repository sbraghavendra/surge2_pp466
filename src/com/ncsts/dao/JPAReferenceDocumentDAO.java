package com.ncsts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ReferenceDocument;

/**
 * @author Paul Govindan
 *
 */

public class JPAReferenceDocumentDAO extends JPAGenericDAO<ReferenceDocument,String> implements ReferenceDocumentDAO {
	
	public void update(ReferenceDocument refDoc) throws DataAccessException {
		System.out.println("enter JPAReferenceDocumentDAO.update()");
		List<String> find = listCodeCheck("REFTYPE");
		if(find.contains(refDoc.getRefTypeCode())){
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			session.saveOrUpdate(refDoc);
			t.commit();
			closeLocalSession(session);
			session=null;
		}
		else{
			System.out.println("Reftype code not in lists");
		}
	}	

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDocument> search(ReferenceDocument exampleInstance, int firstRow, int maxResults,OrderBy orderBy) {
		List<ReferenceDocument> refDocLookupList = null;
		Session session = createLocalSession();
		try{
			Criteria criteria = getCriteria(exampleInstance, session);
			criteria.setFirstResult(firstRow);
			if(maxResults > 0){
				criteria.setMaxResults(maxResults);
			}
			if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
			refDocLookupList = criteria.list();
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the refDoc search");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		return refDocLookupList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDocument> searchForRefDoc(ReferenceDocument refDoc) {
		List<ReferenceDocument> refDocLookupList = null;
		Session session = createLocalSession();
		try{
			Criteria criteria = getCriteria(refDoc, session);
			refDocLookupList = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the given reference document search");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		return refDocLookupList;
	}
	
	private Criteria getCriteria(ReferenceDocument exampleInstance, Session session) {
		Criteria criteria = session.createCriteria(ReferenceDocument.class);
		if(StringUtils.hasText(exampleInstance.getRefDocCode())) {
			criteria.add(Restrictions.ilike("refDocCode", exampleInstance.getRefDocCode(),MatchMode.ANYWHERE));
		}
		
		if(StringUtils.hasText(exampleInstance.getRefTypeCode())) {
			criteria.add(Restrictions.ilike("refTypeCode", exampleInstance.getRefTypeCode(),MatchMode.ANYWHERE));
		}
		if(StringUtils.hasText(exampleInstance.getDescription())) {
			criteria.add(Restrictions.ilike("description", exampleInstance.getDescription(),MatchMode.ANYWHERE));
		}
		
		if(StringUtils.hasText(exampleInstance.getKeyWords())){
			criteria.add(Restrictions.ilike("keyWords", exampleInstance.getKeyWords(),MatchMode.ANYWHERE));
		}
		
		if(StringUtils.hasText(exampleInstance.getDocName())) {
			criteria.add(Restrictions.ilike("docName", exampleInstance.getDocName(),MatchMode.ANYWHERE));
		}
		
		if(StringUtils.hasText(exampleInstance.getUrl())) {
			criteria.add(Restrictions.ilike("url", exampleInstance.getUrl(),MatchMode.ANYWHERE));
		}
		return criteria;
		
	}
	@Transactional(readOnly=true)
	public Long count(ReferenceDocument exampleInstance){
		Session session = createLocalSession();

		Criteria criteria = getCriteria(exampleInstance,session);
		criteria.setProjection(Projections.rowCount()); 
		List<Object> list= criteria.list();
		Long ret = ((Number)list.get(0)).longValue();
		logger.info("exit JPAReferenceDocumentDAO.count() = " + ret );	
		closeLocalSession(session);
		session=null;

        return ret;
	}
	
}


