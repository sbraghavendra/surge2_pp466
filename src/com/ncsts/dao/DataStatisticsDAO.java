package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DataStatistics;

/**
 * @author Muneer Basha
 *
 */

public interface DataStatisticsDAO {
	
    public abstract DataStatistics findById(Long dataStatisticsId) throws DataAccessException;    
    public abstract List<DataStatistics> findAllDataStatisticsLines() throws DataAccessException;
    public abstract void update(DataStatistics dataStatistics) throws DataAccessException;
    public abstract void remove(Long dataStatisticsId) throws DataAccessException;
    public abstract DataStatistics addNewStatisticsLine(DataStatistics instance) throws DataAccessException;  
    public abstract boolean isValidWhereClause(String whereClause);
    public abstract boolean isValidGroupByOnDrilldown(String groupByOnDrilldown);
}

