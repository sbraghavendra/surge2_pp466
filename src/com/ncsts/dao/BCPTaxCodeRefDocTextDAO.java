package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRefDocText;
import com.ncsts.domain.BCPTaxCodeRefDocTextPK;
import com.ncsts.domain.BatchMaintenance;

public interface BCPTaxCodeRefDocTextDAO extends GenericDAO<BCPTaxCodeRefDocText, BCPTaxCodeRefDocTextPK> {
	public Long count(BatchMaintenance batch);
	public List<BCPTaxCodeRefDocText> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
