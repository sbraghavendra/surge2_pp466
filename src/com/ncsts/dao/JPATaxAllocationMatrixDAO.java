package com.ncsts.dao;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.SQLQuery;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionAllocationMatrixDetail;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxAllocationMatrixDetail;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.view.bean.MatrixCommonBean;
import com.ncsts.view.util.ArithmeticUtils;

public class JPATaxAllocationMatrixDAO extends JPAMatrixDAO<TaxAllocationMatrix> implements TaxAllocationMatrixDAO {

	private Map<String,String> columnMapping = null;
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	private void buildDriverWhereClause(StringBuffer where, String driverName, String value){
		if (value!=null && !value.trim().equals("")){
			if(value.endsWith("%")){
			where.append(" AND " + driverName + " LIKE '" + value.trim().toUpperCase() + "'");
			}else {
			where.append(" AND " + driverName + " = '" + value.trim().toUpperCase() + "'");
			}
		}
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 30; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getWhereClauseToken(TaxAllocationMatrix exampleInstance){
		
		if(exampleInstance==null){
			return "";
		}
		
		StringBuffer whereClauseExt = new StringBuffer();
		
	    //build 30 drivers
	    try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        for (int i = 1; i <= 30; i++) {
	        	name = "Driver" + formatter.format(i);
	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	buildDriverWhereClause(whereClauseExt, getDriverColumnName(name.toLowerCase()), (String)theMethod.invoke(exampleInstance, new Object[]{}));
	        }
        }
        catch (Exception ex) {
        }
	    

	    //for tax_matrix_id
	    if (exampleInstance.getTaxAllocationMatrixId()!=null && exampleInstance.getTaxAllocationMatrixId()!=0) {
	    	appendANDToWhere(whereClauseExt, "TAX_ALLOC_MATRIX_ID = " + exampleInstance.getTaxAllocationMatrixId().longValue());
	    }

	 
		
	    return whereClauseExt.toString();
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	@Transactional
	public List<TaxAllocationMatrix> getAllRecords(TaxAllocationMatrix exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		logger.debug("int firstRow, int maxResults :"+ firstRow+" "+ maxResults);
		List<TaxAllocationMatrix> result = null;
		Connection con = null;
		try {
			String whereClauseToken = getWhereClauseToken(exampleInstance);
			String strOrderBy = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			if (con != null) {	
				String sqlQuery = SQLQuery.TB_TAX_ALLOC_MATRIX_MASTER_LIST;
				
				if(strOrderBy==null || strOrderBy.length()==0){
					strOrderBy = "DRIVER_01 asc";
				}
				
				sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
				sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);
				sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
				sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));

				List<TaxAllocationMatrix> returnList=null;
				Query q = entityManager.createNativeQuery(sqlQuery);

				List<Object> list= q.getResultList();
				Iterator<Object> iter=list.iterator();
				if(list!=null){
					returnList = getTaxAllocationMatrixList(iter);
				}
				return returnList;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return result;
	}
	
	private List<TaxAllocationMatrix> getTaxAllocationMatrixList(Iterator<Object> iter){
		List<TaxAllocationMatrix> returnList = new ArrayList<TaxAllocationMatrix>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			TaxAllocationMatrix taxAllocationMatrix=new TaxAllocationMatrix();
            BigDecimal is = (BigDecimal)objarray[0];
            if (is!=null) taxAllocationMatrix.setId(is.longValue());//Row number
            
            try{
            	NumberFormat formatter = new DecimalFormat("00");
            	Method theMethod = null;
            	String name = "";
            	String value = "";
            	for (int i = 1; i <= 30; i++) {
            		name = "setDriver" + formatter.format(i);
            		value = (String) objarray[i];
            		theMethod = taxAllocationMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
            		theMethod.setAccessible(true);
	        	
            		theMethod.invoke(taxAllocationMatrix, new Object[]{value});
            	}
            }
            catch (Exception ex) {
            	logger.info("ex = " + ex.getMessage());
            }
            
            BigDecimal binaryWeight = (BigDecimal) objarray[31];
            if (binaryWeight !=null) taxAllocationMatrix.setBinaryWeight(binaryWeight.longValue());
         
            returnList.add(taxAllocationMatrix);                
		}
		
		return returnList;
	}
	
	@Transactional(readOnly = false)
	public Long count(TaxAllocationMatrix exampleInstance) {
		Long returnLong = 0L;
		try {
			String sqlQuery = SQLQuery.TB_TAX_ALLOC_MATRIX_MASTER_COUNT;
			
			String extendWhere = getWhereClauseToken(exampleInstance);
			sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return returnLong;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxAllocationMatrix> getDetailRecordsxxxx(
			TaxAllocationMatrix taxAllocationMatrix, OrderBy orderBy) {
		
		logger.debug("List<TaxAllocationMatrix> getDetailRecords called");
		
		List<TaxAllocationMatrix> result = null;
		
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(TaxAllocationMatrix.class);
            
            try {
    	    	criteria.add(Restrictions.eq("taxAllocationMatrixId", taxAllocationMatrix.getTaxAllocationMatrixId()));
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            result = criteria.list();
        }
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<TaxAllocationMatrix> getDetailRecords");
        }
        
        closeLocalSession(session);
        session=null;
			
        LogMemory.executeGC();
        
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaxAllocationMatrix> getDetailRecords(TaxAllocationMatrix taxAllocationMatrix, OrderBy orderBy) {
		logger.debug("List<TaxMatrix> getDetailRecords called");
		
		List<TaxAllocationMatrix> result = null;
				
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(TaxAllocationMatrix.class);
            
      	    if (taxAllocationMatrix.getBinaryWeight()!=null && taxAllocationMatrix.getBinaryWeight()!=0) {
      	    	criteria.add(Restrictions.eq("binaryWeight", taxAllocationMatrix.getBinaryWeight()));
      	    }
    
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 30; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = taxAllocationMatrix.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(taxAllocationMatrix, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		criteria.add(Restrictions.eq(name.toLowerCase(), value));
    	        	}
    	        }
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            result = criteria.list();
            
            //Get split global
            if(result != null && result.size()>0){	
				for(TaxAllocationMatrix t : result) {					
					List<JurisdictionAllocationMatrixDetail> jurisDetails = t.getJurisdictionAllocationMatrixDetails();
					if(jurisDetails != null) {
						for(JurisdictionAllocationMatrixDetail d : jurisDetails) {
							if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() == 0L) {			
								t.setGlobalSplit(true);
							}
						}
					}
				}
			}
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<TaxMatrix> getDetailRecords");
        }
        
        closeLocalSession(session);
        session=null;
			
        LogMemory.executeGC();
	
		return result;
	}

	@Override
	public void save(TaxAllocationMatrix instance) {
		populateTaxAllocationMatrixDetailId(instance);		
		super.save(instance);
	}
	
	@Override
	public void update(TaxAllocationMatrix instance) {
		populateTaxAllocationMatrixDetailId(instance);
		super.update(instance);
	}
	
	@Transactional
	private void populateTaxAllocationMatrixDetailId(TaxAllocationMatrix instance) {
		if(instance != null) {
			List<TaxAllocationMatrixDetail> details = instance.getTaxAllocationMatrixDetails();
			if(details != null) {
				String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_TAX_ALLOC_MATRIX_DTL_ID");
				Query q = entityManager.createNativeQuery(sql);
				
				BigDecimal allocPercentTotal = ArithmeticUtils.ZERO;
				TaxAllocationMatrixDetail last = null;
				
				Map<Long, Long> instanceIdDetailIdMap = new HashMap<Long, Long>();
				for(TaxAllocationMatrixDetail detail : details) {
					if(detail.getTaxAllocationMatrixDetailId() == null) {
						BigDecimal id = (BigDecimal)q.getSingleResult();
						detail.setTaxAllocationMatrixDetailId(id.longValue());
						instanceIdDetailIdMap.put(detail.getInstanceCreatedId(), id.longValue());
					}
					else {
						instanceIdDetailIdMap.put(detail.getInstanceCreatedId(), detail.getTaxAllocationMatrixDetailId().longValue());
					}
					
					allocPercentTotal = ArithmeticUtils.add(allocPercentTotal, detail.getAllocationPercent());
					last = detail;
				}
				
				//0001340: Tax Allocation percentages do not add up to 1.00000000 - if needed, adjust the last one to make it correct. 
				if(allocPercentTotal.compareTo(new BigDecimal("1")) > 0) {
					last.setAllocationPercent(ArithmeticUtils.subtract(last.getAllocationPercent(), (ArithmeticUtils.subtract(allocPercentTotal, new BigDecimal("1")))));
				}
				
				List<JurisdictionAllocationMatrixDetail> jurisDetails = instance.getJurisdictionAllocationMatrixDetails();
				if(jurisDetails != null) {
					Map<Long, List<JurisdictionAllocationMatrixDetail>> map = new HashMap<Long, List<JurisdictionAllocationMatrixDetail>>();
					
					for(JurisdictionAllocationMatrixDetail detail : jurisDetails) {
						if(detail.getTaxAllocationMatrixDetailId() == null || detail.getTaxAllocationMatrixDetailId() < 0L) {
							detail.setTaxAllocationMatrixDetailId(instanceIdDetailIdMap.get(detail.getTaxAllocationMatrixDetailInstanceId()));
						}
						
						List<JurisdictionAllocationMatrixDetail> jurisAllocs = null;
						if(map.containsKey(detail.getTaxAllocationMatrixDetailId())) {
							jurisAllocs = map.get(detail.getTaxAllocationMatrixDetailId());
						}
						else {
							jurisAllocs = new ArrayList<JurisdictionAllocationMatrixDetail>();
							map.put(detail.getTaxAllocationMatrixDetailId(), jurisAllocs);
						}
						
						//If detail.getTaxAllocationMatrixDetailId() == null, it is global.
						if(detail.getTaxAllocationMatrixDetailId()==null){
							detail.setTaxAllocationMatrixDetailId(0L);
						}
						jurisAllocs.add(detail);
					}
					
					for(Map.Entry<Long, List<JurisdictionAllocationMatrixDetail>> e : map.entrySet()) {
						allocPercentTotal = ArithmeticUtils.ZERO;
						JurisdictionAllocationMatrixDetail jurisLast = null;
						
						for(JurisdictionAllocationMatrixDetail detail : e.getValue()) {
							allocPercentTotal = ArithmeticUtils.add(allocPercentTotal, detail.getAllocationPercent());
							jurisLast = detail;
						}
						
						//0001340: Tax Allocation percentages do not add up to 1.00000000 - if needed, adjust the last one to make it correct. 
						if(allocPercentTotal.compareTo(new BigDecimal("1")) > 0) {
							jurisLast.setAllocationPercent(ArithmeticUtils.subtract(jurisLast.getAllocationPercent(), (ArithmeticUtils.subtract(allocPercentTotal, new BigDecimal("1")))));
						}
					}
				}
			}
		}
	}
	
	@Transactional
	public boolean isTaxAllocationUsedinTransaction(Long taxAllocationMatrixId) {	
		boolean isUsed = true;
		
		String sqlQuery  = "SELECT count(*) FROM TB_PURCHTRANS WHERE TAX_ALLOC_MATRIX_ID = " + taxAllocationMatrixId;
		Session session = createLocalSession();
		org.hibernate.Query query = null;
		try {
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();			
			Long returnLong = id.longValue();
			if(returnLong.longValue() == 0l){
				isUsed=false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return isUsed;
	}

	@Override
	public TaxAllocationMatrix findByIdWithDetails(Long id) {
		Session session = getSession();
		TaxAllocationMatrix matrix = (TaxAllocationMatrix) session.load(TaxAllocationMatrix.class, id);
		// ***** Force init, then evict ***** 
		try{
			matrix.getTaxAllocationMatrixDetails().size();
		}catch(Exception ex){
			return null;
		}
		for(JurisdictionAllocationMatrixDetail d : matrix.getJurisdictionAllocationMatrixDetails()) {
			Jurisdiction j = new Jurisdiction();
			BeanUtils.copyProperties(d.getJurisdiction(), j);
		}
		session.evict(matrix);
		return matrix;
	}
}
