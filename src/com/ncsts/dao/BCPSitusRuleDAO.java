package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPSitusRule;
import com.ncsts.domain.BatchMaintenance;

public interface BCPSitusRuleDAO extends GenericDAO<BCPSitusRule, Long> {
	public Long count(BatchMaintenance batch);
	public List<BCPSitusRule> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
