package com.ncsts.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.SQLQuery;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.utils.DAOUtils;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.GlExportLog;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TempTransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TransactionDetailSaleService;

public class JPATransactionDetailDAO extends JPAGenericDAO<PurchaseTransaction,Long> implements
		TransactionDetailDAO {
	
	@Autowired
	private TransactionDetailSaleService transactionDetailSaleService;
	
	
	// this field list is used by callRealTimeTransactionProcessProc. 
	//If you need to add another field to TRANSACTION_DETAILS rowtype - just add it to this list, make sure it's separated by comma
	
	/*private final String RT_TRANSACTION_PROCESS_RAWTYPE_FIELDS = "PURCHTRANS_ID, SOURCE_TRANSACTION_ID, PROCESS_BATCH_NO, GL_EXTRACT_BATCH_NO,    " +
																"ARCHIVE_BATCH_NO, ALLOCATION_MATRIX_ID, ALLOCATION_SUBTRANS_ID, ENTERED_DATE, TRANSACTION_STATUS," +
																"GL_DATE, GL_COMPANY_NBR, GL_COMPANY_NAME, GL_DIVISION_NBR, GL_DIVISION_NAME, GL_CC_NBR_DEPT_ID," +
																"GL_CC_NBR_DEPT_NAME, GL_LOCAL_ACCT_NBR, GL_LOCAL_ACCT_NAME, GL_LOCAL_SUB_ACCT_NBR, GL_LOCAL_SUB_ACCT_NAME,"+
																"GL_FULL_ACCT_NBR, GL_FULL_ACCT_NAME, GL_LINE_ITM_DIST_AMT, ORIG_GL_LINE_ITM_DIST_AMT, VENDOR_NBR,"+
																"VENDOR_NAME, VENDOR_ADDRESS_LINE_1, VENDOR_ADDRESS_LINE_2, VENDOR_ADDRESS_LINE_3, VENDOR_ADDRESS_LINE_4, "+
																"VENDOR_ADDRESS_CITY, VENDOR_ADDRESS_COUNTY, VENDOR_ADDRESS_STATE, VENDOR_ADDRESS_ZIP, VENDOR_ADDRESS_COUNTRY,"+
																"VENDOR_TYPE, VENDOR_TYPE_NAME, INVOICE_NBR, INVOICE_DESC, INVOICE_DATE, INVOICE_FREIGHT_AMT,"+
																"INVOICE_DISCOUNT_AMT, INVOICE_TAX_AMT, INVOICE_TOTAL_AMT, INVOICE_TAX_FLG, INVOICE_LINE_NBR,"+
																"INVOICE_LINE_NAME, INVOICE_LINE_TYPE, INVOICE_LINE_TYPE_NAME, INVOICE_LINE_AMT, INVOICE_LINE_TAX,"+
																"AFE_PROJECT_NBR, AFE_PROJECT_NAME, AFE_CATEGORY_NBR, AFE_CATEGORY_NAME, AFE_SUB_CAT_NBR, AFE_SUB_CAT_NAME,"+
																"AFE_USE, AFE_CONTRACT_TYPE, AFE_CONTRACT_STRUCTURE, AFE_PROPERTY_CAT, INVENTORY_NBR, INVENTORY_NAME, "+
																"INVENTORY_CLASS, PO_NBR, PO_NAME, PO_DATE, PO_LINE_NBR, PO_LINE_NAME, PO_LINE_TYPE, PO_LINE_TYPE_NAME,"+
																"SHIP_TO_LOCATION, SHIP_TO_LOCATION_NAME, SHIP_TO_ADDRESS_LINE_1, SHIP_TO_ADDRESS_LINE_2, SHIP_TO_ADDRESS_LINE_3, "+
																"SHIP_TO_ADDRESS_LINE_4, SHIP_TO_ADDRESS_CITY, SHIP_TO_ADDRESS_COUNTY, SHIP_TO_ADDRESS_STATE, SHIP_TO_ADDRESS_ZIP, "+
																"SHIP_TO_ADDRESS_COUNTRY, WO_NBR, WO_NAME, WO_DATE, WO_TYPE, WO_TYPE_DESC, WO_CLASS, WO_CLASS_DESC, WO_ENTITY, "+
																"WO_ENTITY_DESC, WO_LINE_NBR, WO_LINE_NAME, WO_LINE_TYPE, WO_LINE_TYPE_DESC, WO_SHUT_DOWN_CD, WO_SHUT_DOWN_CD_DESC,"+ 
																"VOUCHER_ID, VOUCHER_NAME, VOUCHER_DATE, VOUCHER_LINE_NBR, VOUCHER_LINE_DESC, CHECK_NBR, CHECK_NO, CHECK_DATE,"+
																"CHECK_AMT, CHECK_DESC, USER_TEXT_01, USER_TEXT_02, USER_TEXT_03, USER_TEXT_04, USER_TEXT_05, USER_TEXT_06,"+
																"USER_TEXT_07, USER_TEXT_08, USER_TEXT_09, USER_TEXT_10, USER_TEXT_11, USER_TEXT_12, USER_TEXT_13, USER_TEXT_14, "+
																"USER_TEXT_15, USER_TEXT_16, USER_TEXT_17, USER_TEXT_18, USER_TEXT_19, USER_TEXT_20, USER_TEXT_21, USER_TEXT_22, "+
																"USER_TEXT_23, USER_TEXT_24, USER_TEXT_25, USER_TEXT_26, USER_TEXT_27, USER_TEXT_28, USER_TEXT_29, USER_TEXT_30, "+
																"USER_TEXT_31, USER_TEXT_32, USER_TEXT_33, USER_TEXT_34, USER_TEXT_35, USER_TEXT_36, USER_TEXT_37, USER_TEXT_38, "+
																"USER_TEXT_39, USER_TEXT_40, USER_TEXT_41, USER_TEXT_42, USER_TEXT_43, USER_TEXT_44, USER_TEXT_45, USER_TEXT_46, "+
																"USER_TEXT_47, USER_TEXT_48, USER_TEXT_49, USER_TEXT_50, USER_TEXT_51, USER_TEXT_52, USER_TEXT_53, USER_TEXT_54, "+
																"USER_TEXT_55, USER_TEXT_56, USER_TEXT_57, USER_TEXT_58, USER_TEXT_59, USER_TEXT_60, USER_TEXT_61, USER_TEXT_62, "+
																"USER_TEXT_63, USER_TEXT_64, USER_TEXT_65, USER_TEXT_66, USER_TEXT_67, USER_TEXT_68, USER_TEXT_69, USER_TEXT_70, "+
																"USER_TEXT_71, USER_TEXT_72, USER_TEXT_73, USER_TEXT_74, USER_TEXT_75, USER_TEXT_76, USER_TEXT_77, USER_TEXT_78, "+
																"USER_TEXT_79, USER_TEXT_80, USER_TEXT_81, USER_TEXT_82, USER_TEXT_83, USER_TEXT_84, USER_TEXT_85, USER_TEXT_86, "+
																"USER_TEXT_87, USER_TEXT_88, USER_TEXT_89, USER_TEXT_90, USER_TEXT_91, USER_TEXT_92, USER_TEXT_93, USER_TEXT_94, "+
																"USER_TEXT_95, USER_TEXT_96, USER_TEXT_97, USER_TEXT_98, USER_TEXT_99, USER_TEXT_100, "+
																
																
																"USER_NUMBER_01, USER_NUMBER_02, USER_NUMBER_03, USER_NUMBER_04, USER_NUMBER_05, USER_NUMBER_06, USER_NUMBER_07, "+
																"USER_NUMBER_08, USER_NUMBER_09, USER_NUMBER_10, USER_NUMBER_11, USER_NUMBER_12, USER_NUMBER_13, USER_NUMBER_14, "+
																"USER_NUMBER_15, USER_NUMBER_16, USER_NUMBER_17, USER_NUMBER_18, USER_NUMBER_19, USER_NUMBER_20, USER_NUMBER_21, "+
																"USER_NUMBER_22, USER_NUMBER_23, USER_NUMBER_24, USER_NUMBER_25," +

																"USER_DATE_01, USER_DATE_02, USER_DATE_03, USER_DATE_04, USER_DATE_05, USER_DATE_06, USER_DATE_07, USER_DATE_08, "+ 
																"USER_DATE_09, USER_DATE_10, USER_DATE_11, USER_DATE_12, USER_DATE_13, USER_DATE_14, USER_DATE_15, USER_DATE_16, "+
																"USER_DATE_17, USER_DATE_18, USER_DATE_19, USER_DATE_20, USER_DATE_21, USER_DATE_22, USER_DATE_23, USER_DATE_24, "+
																"USER_DATE_25, "+
																
																"COMMENTS, TB_CALC_TAX_AMT, STATE_USE_AMOUNT, "+
																"STATE_USE_TIER2_AMOUNT, STATE_USE_TIER3_AMOUNT, COUNTY_USE_AMOUNT, COUNTY_LOCAL_USE_AMOUNT, CITY_USE_AMOUNT, "+
																"CITY_LOCAL_USE_AMOUNT, TRANSACTION_STATE_CODE, AUTO_TRANSACTION_STATE_CODE, TRANSACTION_IND, SUSPEND_IND, "+
																"TAXCODE_CODE, MANUAL_TAXCODE_IND, TAX_MATRIX_ID, LOCATION_MATRIX_ID, "+
																"JURISDICTION_ID, JURISDICTION_TAXRATE_ID, MANUAL_JURISDICTION_IND, STATE_USE_RATE, STATE_USE_TIER2_RATE, "+
																"STATE_USE_TIER3_RATE, STATE_SPLIT_AMOUNT, STATE_TIER2_MIN_AMOUNT, STATE_TIER2_MAX_AMOUNT, STATE_MAXTAX_AMOUNT, "+
																"COUNTY_USE_RATE, COUNTY_LOCAL_USE_RATE, COUNTY_SPLIT_AMOUNT, COUNTY_MAXTAX_AMOUNT, COUNTY_SINGLE_FLAG, COUNTY_DEFAULT_FLAG, "+
																"CITY_USE_RATE, CITY_LOCAL_USE_RATE, CITY_SPLIT_AMOUNT, CITY_SPLIT_USE_RATE, CITY_SINGLE_FLAG, CITY_DEFAULT_FLAG, COMBINED_USE_RATE, "+
																"LOAD_TIMESTAMP, GL_EXTRACT_UPDATER, GL_EXTRACT_TIMESTAMP, GL_EXTRACT_FLAG, GL_LOG_FLAG, GL_EXTRACT_AMT, AUDIT_FLAG, AUDIT_USER_ID, "+
																"AUDIT_TIMESTAMP, MODIFY_USER_ID, MODIFY_TIMESTAMP, UPDATE_USER_ID, UPDATE_TIMESTAMP, COUNTRY_USE_AMOUNT, TRANSACTION_COUNTRY_CODE, "+
																"AUTO_TRANSACTION_COUNTRY_CODE, COUNTRY_USE_RATE";  */
													               
	
	
	private final String RT_TRANSACTION_PROCESS_RAWTYPE_FIELDS = "PURCHTRANS_ID, PROCESSTYPE_CODE, BCP_TRANSACTION_ID, PROCESS_BATCH_NO,    " +
																"GL_EXTRACT_BATCH_NO, ENTERED_DATE, LOAD_TIMESTAMP, TRANSACTION_STATE_CODE, AUTO_TRANSACTION_STATE_CODE," +
																"TRANSACTION_COUNTRY_CODE, AUTO_TRANSACTION_COUNTRY_CODE, TRANSACTION_IND, SUSPEND_IND, DIRECT_PAY_PERMIT_FLAG, CALCULATE_IND," +
																"ENTITY_ID, ENTITY_CODE, UNLOCK_ENTITY_ID, CP_FILING_ENTITY_CODE, ALLOCATION_MATRIX_ID,"+
																"ALLOCATION_SUBTRANS_ID, TAX_ALLOC_MATRIX_ID, SPLIT_SUBTRANS_ID, MULTI_TRANS_CODE, TAXCODE_CODE,"+
																"MANUAL_TAXCODE_IND, TAX_MATRIX_ID, PROCRULE_SESSION_ID, SOURCE_TRANSACTION_ID, TRANSACTION_STATUS, "+
																"COMMENTS, URL_LINK, PROCESSING_NOTES, REJECT_FLAG, GL_DATE,"+
																"GL_COMPANY_NBR, GL_COMPANY_NAME, GL_DIVISION_NBR, GL_DIVISION_NAME, GL_CC_NBR_DEPT_ID, GL_CC_NBR_DEPT_NAME,"+
																"GL_LOCAL_ACCT_NBR, GL_LOCAL_ACCT_NAME, GL_LOCAL_SUB_ACCT_NBR, GL_LOCAL_SUB_ACCT_NAME, GL_FULL_ACCT_NBR,"+
																"GL_FULL_ACCT_NAME, GL_LINE_ITM_DIST_AMT, PRODUCT_CODE, PRODUCT_NAME, VENDOR_NBR,"+
																"VENDOR_NAME, VENDOR_ADDRESS_LINE_1, VENDOR_ADDRESS_LINE_2, VENDOR_ADDRESS_LINE_3, VENDOR_ADDRESS_LINE_4, VENDOR_ADDRESS_CITY,"+
																"VENDOR_ADDRESS_COUNTY, VENDOR_ADDRESS_STATE, VENDOR_ADDRESS_ZIP, VENDOR_ADDRESS_COUNTRY, VENDOR_TYPE, VENDOR_TYPE_NAME, "+
																"INVOICE_NBR, INVOICE_DESC, INVOICE_DATE, INVOICE_FREIGHT_AMT, INVOICE_DISCOUNT_AMT, INVOICE_TAX_AMT, INVOICE_TOTAL_AMT, INVOICE_TAX_FLG,"+
																"INVOICE_LINE_NBR, INVOICE_LINE_NAME, INVOICE_LINE_TYPE, INVOICE_LINE_TYPE_NAME, INVOICE_LINE_AMT, "+
																"INVOICE_LINE_TAX, INVOICE_LINE_COUNT, INVOICE_LINE_WEIGHT, INVOICE_LINE_FREIGHT_AMT, INVOICE_LINE_DISC_AMT, "+
																"INVOICE_LINE_DISC_TYPE_CODE, AFE_PROJECT_NBR, AFE_PROJECT_NAME, AFE_CATEGORY_NBR, AFE_CATEGORY_NAME, AFE_SUB_CAT_NBR, AFE_SUB_CAT_NAME, AFE_USE, AFE_CONTRACT_TYPE, "+
																"AFE_CONTRACT_STRUCTURE, AFE_PROPERTY_CAT, INVENTORY_NBR, INVENTORY_NAME, INVENTORY_CLASS, INVENTORY_CLASS_NAME, PO_NBR,"+ 
																"PO_NAME, PO_DATE, PO_LINE_NBR, PO_LINE_NAME, PO_LINE_TYPE, PO_LINE_TYPE_NAME, WO_NBR, WO_NAME,"+
																"WO_DATE, WO_TYPE, WO_TYPE_DESC, WO_CLASS, WO_CLASS_DESC, WO_ENTITY, WO_ENTITY_DESC, WO_LINE_NBR, WO_LINE_NAME, WO_LINE_TYPE, WO_LINE_TYPE_DESC, WO_SHUT_DOWN_CD, WO_SHUT_DOWN_CD_DESC, "+
																"VOUCHER_ID, VOUCHER_NAME, VOUCHER_DATE, VOUCHER_LINE_NBR, VOUCHER_LINE_DESC, CHECK_NBR, CHECK_NO, CHECK_DATE, CHECK_AMT, CHECK_DESC, GL_EXTRACT_FLAG," +
																"GL_EXTRACT_UPDATER,GL_EXTRACT_TIMESTAMP,TAXABLE_AMT,COUNTRY_TIER1_TAXABLE_AMT,COUNTRY_TIER2_TAXABLE_AMT,COUNTRY_TIER3_TAXABLE_AMT,STATE_TIER1_TAXABLE_AMT," +
																"STATE_TIER2_TAXABLE_AMT,STATE_TIER3_TAXABLE_AMT,COUNTY_TIER1_TAXABLE_AMT,COUNTY_TIER2_TAXABLE_AMT,COUNTY_TIER3_TAXABLE_AMT,CITY_TIER1_TAXABLE_AMT,CITY_TIER2_TAXABLE_AMT,"+
																"CITY_TIER3_TAXABLE_AMT,STJ1_TAXABLE_AMT,STJ2_TAXABLE_AMT,STJ3_TAXABLE_AMT,STJ4_TAXABLE_AMT,STJ5_TAXABLE_AMT,STJ6_TAXABLE_AMT,STJ7_TAXABLE_AMT,STJ8_TAXABLE_AMT,STJ9_TAXABLE_AMT,"+
																"STJ10_TAXABLE_AMT,TB_CALC_TAX_AMT,COUNTRY_TIER1_TAX_AMT,COUNTRY_TIER2_TAX_AMT,COUNTRY_TIER3_TAX_AMT,STATE_TIER1_TAX_AMT,STATE_TIER2_TAX_AMT,STATE_TIER3_TAX_AMT,COUNTY_TIER1_TAX_AMT,"+
																"COUNTY_TIER2_TAX_AMT,COUNTY_TIER3_TAX_AMT,CITY_TIER1_TAX_AMT,CITY_TIER2_TAX_AMT,CITY_TIER3_TAX_AMT,STJ1_TAX_AMT,STJ2_TAX_AMT,STJ3_TAX_AMT,STJ4_TAX_AMT,STJ5_TAX_AMT,STJ6_TAX_AMT,STJ7_TAX_AMT," +
																"STJ8_TAX_AMT,STJ9_TAX_AMT,STJ10_TAX_AMT,COUNTY_LOCAL_TAX_AMT,CITY_LOCAL_TAX_AMT,DISTR_01_AMT,DISTR_02_AMT,DISTR_03_AMT,DISTR_04_AMT,DISTR_05_AMT,DISTR_06_AMT,DISTR_07_AMT,DISTR_08_AMT,DISTR_09_AMT,"+
																"DISTR_10_AMT,LOCATION_DRIVER_01,LOCATION_DRIVER_02,LOCATION_DRIVER_03,LOCATION_DRIVER_04,LOCATION_DRIVER_05,LOCATION_DRIVER_06,LOCATION_DRIVER_07,LOCATION_DRIVER_08,LOCATION_DRIVER_09,LOCATION_DRIVER_10," +
																"TAX_DRIVER_01,TAX_DRIVER_02,TAX_DRIVER_03,TAX_DRIVER_04,TAX_DRIVER_05,TAX_DRIVER_06,TAX_DRIVER_07,TAX_DRIVER_08,TAX_DRIVER_09,TAX_DRIVER_10,TAX_DRIVER_11,TAX_DRIVER_12," +
																"TAX_DRIVER_13,TAX_DRIVER_14,TAX_DRIVER_15,TAX_DRIVER_16,TAX_DRIVER_17,TAX_DRIVER_18,TAX_DRIVER_19,TAX_DRIVER_20,TAX_DRIVER_21,TAX_DRIVER_22,TAX_DRIVER_23,TAX_DRIVER_24,TAX_DRIVER_25," +
																"TAX_DRIVER_26,TAX_DRIVER_27,TAX_DRIVER_28,TAX_DRIVER_29,TAX_DRIVER_30,AUDIT_FLAG,AUDIT_USER_ID,AUDIT_TIMESTAMP,MODIFY_USER_ID,MODIFY_TIMESTAMP,UPDATE_USER_ID,UPDATE_TIMESTAMP"+
																"COUNTRY_SITUS_MATRIX_ID,STATE_SITUS_MATRIX_ID,COUNTY_SITUS_MATRIX_ID,CITY_SITUS_MATRIX_ID,STJ1_SITUS_MATRIX_ID,STJ2_SITUS_MATRIX_ID,STJ3_SITUS_MATRIX_ID,STJ4_SITUS_MATRIX_ID,STJ5_SITUS_MATRIX_ID,COUNTRY_CODE,"+
																"STATE_CODE,COUNTY_NAME,CITY_NAME,STJ1_NAME,STJ2_NAME,STJ3_NAME,STJ4_NAME,STJ5_NAME,STJ6_NAME,STJ7_NAME,STJ8_NAME,STJ9_NAME,STJ10_NAME,COUNTRY_VENDOR_NEXUS_DTL_ID,"+
																"STATE_VENDOR_NEXUS_DTL_ID,COUNTY_VENDOR_NEXUS_DTL_ID,CITY_VENDOR_NEXUS_DTL_ID,COUNTRY_TAXCODE_DETAIL_ID,STATE_TAXCODE_DETAIL_ID,COUNTY_TAXCODE_DETAIL_ID,CITY_TAXCODE_DETAIL_ID," +
																"STJ1_TAXCODE_DETAIL_ID,STJ2_TAXCODE_DETAIL_ID,STJ3_TAXCODE_DETAIL_ID,STJ4_TAXCODE_DETAIL_ID,STJ5_TAXCODE_DETAIL_ID,STJ6_TAXCODE_DETAIL_ID,STJ7_TAXCODE_DETAIL_ID,STJ8_TAXCODE_DETAIL_ID," +
																"STJ9_TAXCODE_DETAIL_ID,STJ10_TAXCODE_DETAIL_ID,COUNTRY_TAXTYPE_CODE,STATE_TAXTYPE_CODE,COUNTY_TAXTYPE_CODE,CITY_TAXTYPE_CODE,STJ1_TAXTYPE_CODE,STJ2_TAXTYPE_CODE,STJ3_TAXTYPE_CODE," +
																"STJ4_TAXTYPE_CODE,STJ5_TAXTYPE_CODE,STJ6_TAXTYPE_CODE,STJ7_TAXTYPE_CODE,STJ8_TAXTYPE_CODE,STJ9_TAXTYPE_CODE,STJ10_TAXTYPE_CODE,COUNTRY_TAXRATE_ID,STATE_TAXRATE_ID,COUNTY_TAXRATE_ID," +
																"CITY_TAXRATE_ID,STJ1_TAXRATE_ID,STJ2_TAXRATE_ID,STJ3_TAXRATE_ID,STJ4_TAXRATE_ID,STJ5_TAXRATE_ID,STJ6_TAXRATE_ID,STJ7_TAXRATE_ID,STJ8_TAXRATE_ID,STJ9_TAXRATE_ID,STJ10_TAXRATE_ID,COUNTRY_TAXCODE_OVER_FLAG," +
																"STATE_TAXCODE_OVER_FLAG,COUNTY_TAXCODE_OVER_FLAG,CITY_TAXCODE_OVER_FLAG,STJ1_TAXCODE_OVER_FLAG,STJ2_TAXCODE_OVER_FLAG,STJ3_TAXCODE_OVER_FLAG,STJ4_TAXCODE_OVER_FLAG,STJ5_TAXCODE_OVER_FLAG,STJ6_TAXCODE_OVER_FLAG," +
																"STJ7_TAXCODE_OVER_FLAG,STJ8_TAXCODE_OVER_FLAG,STJ9_TAXCODE_OVER_FLAG,STJ10_TAXCODE_OVER_FLAG," +
																"SHIPTO_LOCATION,SHIPTO_LOCATION_NAME,SHIPTO_MANUAL_JUR_IND,SHIPTO_ENTITY_ID,SHIPTO_ENTITY_CODE,SHIPTO_LAT,SHIPTO_LONG,SHIPTO_LOCN_MATRIX_ID,SHIPTO_JURISDICTION_ID,SHIPTO_GEOCODE,SHIPTO_ADDRESS_LINE_1,SHIPTO_ADDRESS_LINE_2," +
																"SHIPTO_CITY,SHIPTO_COUNTY,SHIPTO_STATE_CODE,SHIPTO_ZIP,SHIPTO_ZIPPLUS4,SHIPTO_COUNTRY_CODE,SHIPTO_STJ1_NAME,SHIPTO_STJ2_NAME,SHIPTO_STJ3_NAME,SHIPTO_STJ4_NAME,SHIPTO_STJ5_NAME,SHIPFROM_ENTITY_ID,SHIPFROM_ENTITY_CODE," +
																"SHIPFROM_LAT,SHIPFROM_LONG,SHIPFROM_LOCN_MATRIX_ID,SHIPFROM_JURISDICTION_ID,SHIPFROM_GEOCODE,SHIPFROM_ADDRESS_LINE_1,SHIPFROM_ADDRESS_LINE_2,SHIPFROM_CITY,SHIPFROM_COUNTY,SHIPFROM_STATE_CODE,SHIPFROM_ZIP,SHIPFROM_ZIPPLUS4,"+
																"HIPFROM_COUNTRY_CODE,SHIPFROM_STJ1_NAME,SHIPFROM_STJ2_NAME,SHIPFROM_STJ3_NAME,SHIPFROM_STJ4_NAME,SHIPFROM_STJ5_NAME,ORDRACPT_ENTITY_ID,ORDRACPT_ENTITY_CODE,ORDRACPT_LAT,ORDRACPT_LONG,ORDRACPT_LOCN_MATRIX_ID,ORDRACPT_JURISDICTION_ID,"+
																"SORDRACPT_GEOCODE,ORDRACPT_ADDRESS_LINE_1,ORDRACPT_ADDRESS_LINE_2,ORDRACPT_CITY,ORDRACPT_COUNTY,ORDRACPT_STATE_CODE,ORDRACPT_ZIP,ORDRACPT_ZIPPLUS4,ORDRACPT_COUNTRY_CODE,ORDRACPT_STJ1_NAME,ORDRACPT_STJ2_NAME,ORDRACPT_STJ3_NAME,ORDRACPT_STJ4_NAME," +
																"ORDRACPT_STJ5_NAME,ORDRORGN_ENTITY_ID,ORDRORGN_ENTITY_CODE,ORDRORGN_LAT,ORDRORGN_LONG,ORDRORGN_LOCN_MATRIX_ID,ORDRORGN_JURISDICTION_ID,ORDRORGN_GEOCODE,ORDRORGN_ADDRESS_LINE_1,ORDRORGN_ADDRESS_LINE_2,ORDRORGN_CITY,ORDRORGN_COUNTY,ORDRORGN_STATE_CODE,ORDRORGN_ZIP,"+
																"ORDRORGN_ZIPPLUS4,ORDRORGN_COUNTRY_CODE,ORDRORGN_STJ1_NAME,ORDRORGN_STJ2_NAME,ORDRORGN_STJ3_NAME,ORDRORGN_STJ4_NAME,ORDRORGN_STJ5_NAME,FIRSTUSE_ENTITY_ID,FIRSTUSE_ENTITY_CODE,FIRSTUSE_LAT,FIRSTUSE_LONG,FIRSTUSE_LOCN_MATRIX_ID,FIRSTUSE_JURISDICTION_ID,FIRSTUSE_GEOCODE,FIRSTUSE_ADDRESS_LINE_1," +
																"FIRSTUSE_ADDRESS_LINE_2,FIRSTUSE_CITY,FIRSTUSE_COUNTY,FIRSTUSE_STATE_CODE,FIRSTUSE_ZIP,FIRSTUSE_ZIPPLUS4,FIRSTUSE_COUNTRY_CODE,FIRSTUSE_STJ1_NAME,FIRSTUSE_STJ2_NAME,FIRSTUSE_STJ3_NAME,FIRSTUSE_STJ4_NAME,FIRSTUSE_STJ5_NAME,BILLTO_ENTITY_ID,BILLTO_ENTITY_CODE,BILLTO_LAT,BILLTO_LONG,BILLTO_LOCN_MATRIX_ID," +
																"BILLTO_JURISDICTION_ID,BILLTO_GEOCODE,BILLTO_ADDRESS_LINE_1,BILLTO_ADDRESS_LINE_2,BILLTO_CITY,BILLTO_COUNTY,BILLTO_STATE_CODE,BILLTO_ZIP,BILLTO_ZIPPLUS4,BILLTO_COUNTRY_CODE,BILLTO_STJ1_NAME,BILLTO_STJ2_NAME,BILLTO_STJ3_NAME,BILLTO_STJ4_NAME,BILLTO_STJ5_NAME,TTLXFR_ENTITY_ID,TTLXFR_ENTITY_CODE,TTLXFR_LAT,TTLXFR_LONG,"+
																"TTLXFR_LOCN_MATRIX_ID,TTLXFR_JURISDICTION_ID,TTLXFR_GEOCODE,TTLXFR_ADDRESS_LINE_1,TTLXFR_ADDRESS_LINE_2,TTLXFR_CITY,TTLXFR_COUNTY,TTLXFR_STATE_CODE,TTLXFR_ZIP,TTLXFR_ZIPPLUS4,TTLXFR_COUNTRY_CODE,TTLXFR_STJ1_NAME,TTLXFR_STJ2_NAME,TTLXFR_STJ3_NAME,TTLXFR_STJ4_NAME,TTLXFR_STJ5_NAME"+
																"COUNTRY_TIER1_RATE,COUNTRY_TIER2_RATE,COUNTRY_TIER3_RATE,COUNTRY_TIER1_SETAMT,COUNTRY_TIER2_SETAMT,COUNTRY_TIER3_SETAMT,COUNTRY_TIER1_MAX_AMT,COUNTRY_TIER2_MIN_AMT,COUNTRY_TIER2_MAX_AMT,COUNTRY_TIER2_ENTAM_FLAG,COUNTRY_TIER3_ENTAM_FLAG,COUNTRY_MAXTAX_AMT,COUNTRY_TAXABLE_THRESHOLD_AMT,"+
																"COUNTRY_MINIMUM_TAXABLE_AMT,COUNTRY_MAXIMUM_TAXABLE_AMT,COUNTRY_MAXIMUM_TAX_AMT,COUNTRY_BASE_CHANGE_PCT,COUNTRY_TAX_PROCESS_TYPE_CODE,STATE_TIER1_RATE,STATE_TIER2_RATE,"+
																"STATE_TIER3_RATE,STATE_TIER1_SETAMT,STATE_TIER2_SETAMT,STATE_TIER3_SETAMT,STATE_TIER1_MAX_AMT,STATE_TIER2_MIN_AMT,STATE_TIER2_MAX_AMT,STATE_TIER2_ENTAM_FLAG,STATE_TIER3_ENTAM_FLAG,STATE_MAXTAX_AMT,STATE_TAXABLE_THRESHOLD_AMT,STATE_MINIMUM_TAXABLE_AMT,STATE_MAXIMUM_TAXABLE_AMT,STATE_MAXIMUM_TAX_AMT,"+
																"STATE_BASE_CHANGE_PCT,STATE_TAX_PROCESS_TYPE_CODE,COUNTY_TIER1_RATE,COUNTY_TIER2_RATE,COUNTY_TIER3_RATE,COUNTY_TIER1_SETAMT,COUNTY_TIER2_SETAMT,COUNTY_TIER3_SETAMT,COUNTY_TIER1_MAX_AMT,COUNTY_TIER2_MIN_AMT,COUNTY_TIER2_MAX_AMT,COUNTY_TIER2_ENTAM_FLAG,COUNTY_TIER3_ENTAM_FLAG," +
																"COUNTY_MAXTAX_AMT,COUNTY_TAXABLE_THRESHOLD_AMT,COUNTY_MINIMUM_TAXABLE_AMT,COUNTY_MAXIMUM_TAXABLE_AMT,COUNTY_MAXIMUM_TAX_AMT,COUNTY_BASE_CHANGE_PCT,COUNTY_TAX_PROCESS_TYPE_CODE,"+
																"CITY_TIER1_RATE,CITY_TIER2_RATE,CITY_TIER3_RATE,CITY_TIER1_SETAMT,CITY_TIER2_SETAMT,CITY_TIER3_SETAMT,CITY_TIER1_MAX_AMT,CITY_TIER2_MIN_AMT,CITY_TIER2_MAX_AMT,CITY_TIER2_ENTAM_FLAG,CITY_TIER3_ENTAM_FLAG,CITY_MAXTAX_AMT,CITY_TAXABLE_THRESHOLD_AMT,"+
																"CITY_MINIMUM_TAXABLE_AMT,CITY_MAXIMUM_TAXABLE_AMT,CITY_MAXIMUM_TAX_AMT,CITY_BASE_CHANGE_PCT,CITY_TAX_PROCESS_TYPE_CODE,STJ1_RATE,STJ1_SETAMT,STJ2_RATE,STJ2_SETAMT,STJ3_RATE,STJ3_SETAMT,STJ4_RATE,STJ4_SETAMT,STJ5_RATE,STJ5_SETAMT,STJ6_RATE,STJ6_SETAMT,STJ7_RATE,STJ7_SETAMT,"+
																"STJ8_RATE,STJ8_SETAMT,STJ9_RATE,STJ9_SETAMT,STJ10_RATE,STJ10_SETAMT,COMBINED_RATE," +
																"USER_TEXT_01, USER_TEXT_02, USER_TEXT_03, USER_TEXT_04, USER_TEXT_05, USER_TEXT_06,"+
																"USER_TEXT_07, USER_TEXT_08, USER_TEXT_09, USER_TEXT_10, USER_TEXT_11, USER_TEXT_12, USER_TEXT_13, USER_TEXT_14, "+
																"USER_TEXT_15, USER_TEXT_16, USER_TEXT_17, USER_TEXT_18, USER_TEXT_19, USER_TEXT_20, USER_TEXT_21, USER_TEXT_22, "+
																"USER_TEXT_23, USER_TEXT_24, USER_TEXT_25, USER_TEXT_26, USER_TEXT_27, USER_TEXT_28, USER_TEXT_29, USER_TEXT_30, "+
																"USER_TEXT_31, USER_TEXT_32, USER_TEXT_33, USER_TEXT_34, USER_TEXT_35, USER_TEXT_36, USER_TEXT_37, USER_TEXT_38, "+
																"USER_TEXT_39, USER_TEXT_40, USER_TEXT_41, USER_TEXT_42, USER_TEXT_43, USER_TEXT_44, USER_TEXT_45, USER_TEXT_46, "+
																"USER_TEXT_47, USER_TEXT_48, USER_TEXT_49, USER_TEXT_50, USER_TEXT_51, USER_TEXT_52, USER_TEXT_53, USER_TEXT_54, "+
																"USER_TEXT_55, USER_TEXT_56, USER_TEXT_57, USER_TEXT_58, USER_TEXT_59, USER_TEXT_60, USER_TEXT_61, USER_TEXT_62, "+
																"USER_TEXT_63, USER_TEXT_64, USER_TEXT_65, USER_TEXT_66, USER_TEXT_67, USER_TEXT_68, USER_TEXT_69, USER_TEXT_70, "+
																"USER_TEXT_71, USER_TEXT_72, USER_TEXT_73, USER_TEXT_74, USER_TEXT_75, USER_TEXT_76, USER_TEXT_77, USER_TEXT_78, "+
																"USER_TEXT_79, USER_TEXT_80, USER_TEXT_81, USER_TEXT_82, USER_TEXT_83, USER_TEXT_84, USER_TEXT_85, USER_TEXT_86, "+
																"USER_TEXT_87, USER_TEXT_88, USER_TEXT_89, USER_TEXT_90, USER_TEXT_91, USER_TEXT_92, USER_TEXT_93, USER_TEXT_94, "+
																"USER_TEXT_95, USER_TEXT_96, USER_TEXT_97, USER_TEXT_98, USER_TEXT_99, USER_TEXT_100, "+
																"USER_NUMBER_01, USER_NUMBER_02, USER_NUMBER_03, USER_NUMBER_04, USER_NUMBER_05, USER_NUMBER_06, USER_NUMBER_07, "+
																"USER_NUMBER_08, USER_NUMBER_09, USER_NUMBER_10, USER_NUMBER_11, USER_NUMBER_12, USER_NUMBER_13, USER_NUMBER_14, "+
																"USER_NUMBER_15, USER_NUMBER_16, USER_NUMBER_17, USER_NUMBER_18, USER_NUMBER_19, USER_NUMBER_20, USER_NUMBER_21, "+
																"USER_NUMBER_22, USER_NUMBER_23, USER_NUMBER_24, USER_NUMBER_25," +
													
																"USER_DATE_01, USER_DATE_02, USER_DATE_03, USER_DATE_04, USER_DATE_05, USER_DATE_06, USER_DATE_07, USER_DATE_08, "+ 
																"USER_DATE_09, USER_DATE_10, USER_DATE_11, USER_DATE_12, USER_DATE_13, USER_DATE_14, USER_DATE_15, USER_DATE_16, "+
																"USER_DATE_17, USER_DATE_18, USER_DATE_19, USER_DATE_20, USER_DATE_21, USER_DATE_22, USER_DATE_23, USER_DATE_24, "+
																"USER_DATE_25, ";
	
	private static final String PURCHTRANS_LOCATION_MATRIX_ID_QUERY = "Select count(*) from PurchaseTransaction td where " + 
			"((td.shiptoLocnMatrixId = :locationMatrixId) or (td.shipfromLocnMatrixId = :locationMatrixId) or (td.ordracptLocnMatrixId = :locationMatrixId) or " +
			"(td.ordrorgnLocnMatrixId = :locationMatrixId) or (td.firstuseLocnMatrixId = :locationMatrixId) or (td.billtoLocnMatrixId = :locationMatrixId) or " +
			"(td.ttlxfrLocnMatrixId = :locationMatrixId))";
														
	@Override
	protected void extendExampleCriteria(Criteria criteria, PurchaseTransaction exampleInstance) {
		// Hook to provide opportunity to adjust findByExample criteria
		Long id = (exampleInstance == null)? null:exampleInstance.getPurchtransId(); 
		if ((id != null) && (id > 0L)) {
			criteria.add(Restrictions.eq("purchtransId", id));
		}
	}
	
	//these functions are just to make it easy to log the inputs to stored procedures
	void setParamLong(CallableStatement call, int argNum, long value,String comment) throws SQLException{
		System.out.println("Setting Param "+argNum+":"+value+"("+comment+")");
		call.setLong(argNum, value);
	}
	void setParamString(CallableStatement call, int argNum, String value,String comment) throws SQLException{
		System.out.println("Setting Param "+argNum+":"+value+"("+comment+")");
		call.setString(argNum, value);
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void updatetoHoldTransaction(PurchaseTransaction instance) throws DataAccessException {
		super.update(instance);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(PurchaseTransaction instance) throws DataAccessException {
		update(instance, null);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(PurchaseTransaction instance, PurchaseTransaction oriTransactionDetail) throws DataAccessException {
		Connection con = null;
		try {
			String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
			String pco = "";
			List<String> list = getJpaTemplate().find(qry);
			if ((list != null) && (list.size() > 0)) {
				pco = (String)list.get(0);
			}

			/* ***!SPROC!*** commented on 8/03/2016
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			
			//Clean up tb_tmp_transaction_detail
			PreparedStatement p = con.prepareStatement("delete from tb_tmp_transaction_detail");
			p.executeUpdate();
			p.close();
			con.commit();
			*/
			
			//0005238
			if(instance.getIsReprocessing()) {							
				if(instance.getManualTaxcodeInd()==null ) {
					instance.setTaxMatrixId(null);
					instance.setTaxcodeCode(null);
					instance.setStateTaxcodeDetailId(null);
					instance.setCountyTaxcodeDetailId(null);
					instance.setCityTaxcodeDetailId(null);
					instance.setStateTaxcodeTypeCode(null);  
					instance.setStateTaxcodeDetailId(null);
					instance.setCountyTaxcodeTypeCode(null);
					instance.setCountyTaxcodeDetailId(null);
					instance.setCityTaxcodeTypeCode(null);	
					instance.setCityTaxcodeDetailId(null);
				}
				
				if(instance.getShiptoManualJurInd()==null) {
					instance.setShiptoLocnMatrixId(null);
					instance.setShiptoJurisdictionId(null);
					instance.setStateTaxrateId(null);
					instance.setStateTaxcodeDetailId(null);
					instance.setStateTaxtypeCode(null);
					
					if(instance.getAutoTransactionCountryCode()!=null) 
						instance.setTransactionCountryCode(instance.getAutoTransactionCountryCode());
					
					if(instance.getAutoTransactionStateCode()!=null) 
						instance.setTransactionStateCode(instance.getAutoTransactionStateCode());
				}				
			}

			/* ***!SPROC!*** commented on 8/03/2016
			//Save updated transaction in temp table, tb_tmp_transaction_detail
			TempTransactionDetail tempTransactionDetail = new TempTransactionDetail(); 
			BeanUtils.copyProperties(tempTransactionDetail, instance);
			p = tempTransactionDetail.getUpdateStatement(con);
			p.executeUpdate();
			p.close();
			con.commit();  
			
			
			//0005977
			if (pco != null && pco.equals("1") && tempTransactionDetail.getTransactionInd().equalsIgnoreCase("S")) {			
				transactionDetailSaleService.suspendAndLog(tempTransactionDetail.getTransactionDetailId());
			}
			
			//execute SP_TB_TRANSACTION_DETAIL_B_U with TransactionId
			CallableStatement cstmnt = con.prepareCall("{call SP_TB_TRANSACTION_DETAIL_B_U(?)}");
			cstmnt.setLong(1, instance.getId());
			cstmnt.execute();		
			cstmnt.close();
			
			con.setAutoCommit(false); //Prevent instance committed
			con.close();
			con = null;
			*/
			updateTransaction(instance);
		} 
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Transaction update error: " + e.toString());
			//if (con != null) {
				//try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			//}
		} 
	}
	
	/*  //0001643: TaxCode content
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(TransactionDetail instance, TransactionDetail oriTransactionDetail) throws DataAccessException {
		Connection con = null;
		Transaction tx = null;
		try {
			//
			 // Transaction updates were originally processed with a sequence of stored procedures that
			 // were invoked from Oracle triggers. The first populated an Oracle global temporary session
			 // table with updated transaction values. The next stored procedure retrieved this temp row,
			 // compared it with the original permanent row to determine how it should be processed, and
			 // then updated the original transaction. The third stored procedure recorded certain updated
			 // values to an audit table.
			 // 
			 // The code below performs the task of the first stored procedure with prepared statements,
			 // followed by the second and third from the original sequence within a single JDBC
			 // (not Hibernate) connection transaction. 
			 //
		
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			PreparedStatement p = con.prepareStatement("delete from tb_tmp_transaction_detail");
			p.executeUpdate();
			p.close();
			con.commit();
			
			TempTransactionDetail tempTransactionDetail = new TempTransactionDetail();
			BeanUtils.copyProperties(tempTransactionDetail, instance);
			p = tempTransactionDetail.getUpdateStatement(con);
			p.executeUpdate();
			p.close();
			con.commit();
			
			//Get original transaction
			//0000968
			//Original transaction has been updated for suspend action
			TransactionDetail oldTransactionDetail = null;
			if(oriTransactionDetail==null){
				oldTransactionDetail = findById(instance.getId());
			}
			else{
				oldTransactionDetail = oriTransactionDetail;
			}
			
			//0001400: Processing Held Transactions
			boolean processTransaction = true;
			
			if((oldTransactionDetail.getTransactionInd().equalsIgnoreCase("H")) && 
			   (oldTransactionDetail.getJurisdictionId().intValue()==tempTransactionDetail.getJurisdictionId().intValue())      ){
				processTransaction = false;
			}	
			
			//Actually update transaction
			if(processTransaction){
				TransactionUpdate tp = new TransactionUpdateImpl();
				tp.processTransaction(con, tempTransactionDetail);
				con.commit();
			}

			GlExportLog glExportLog = new GlExportLog();
			glExportLog.processGlExportLog(con, oldTransactionDetail, tempTransactionDetail);
			
			con.close();
			
			//Save transaction
			BeanUtils.copyProperties(instance, tempTransactionDetail);
			Session session = createLocalSession();
			tx = session.beginTransaction();
			tx.begin();
			session.saveOrUpdate(instance);
			tx.commit();
			session.flush();
			closeLocalSession(session);
			session=null;
		} 
		catch (Exception e) {
			logger.error("Transaction update error: " + e.toString());
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
			if (tx != null) {
				tx.rollback();
			}
		} 
	}
	*/
	
	@Transactional(readOnly=true)
	public Long count(PurchaseTransaction exampleInstance, Date effectiveDate,
			Date expirationDate, Boolean includeUnprocessed,
			Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter) {
		logger.info("enter JPATransactionDetailDAO.count()");
		
		//0004691
		Session session = createLocalSession();

		Criteria criteria = generateCriteria(session, exampleInstance, 
				effectiveDate, expirationDate, 
				includeUnprocessed, includeProcessed, includeSuspended, 
				glExtractFlagIsNull); 
		logger.info("effectiveDate = " + effectiveDate);
		logger.info("expirationDate = " + expirationDate);		
		logger.info("includeUnprocessed = " + includeUnprocessed);	
		logger.info("includeProcessed = " + includeProcessed);	
		logger.info("includeSuspended = " + includeSuspended);	
		logger.info("glExtractFlagIsNull = " + glExtractFlagIsNull);			
		logger.info("advancedFilter = " + advancedFilter);

		if(advancedFilter!=null && advancedFilter.length()>0){
			criteria.add(Restrictions.sqlRestriction("("+advancedFilter+")"));
		}
		

		criteria.setProjection(Projections.rowCount()); // We just want a row count
		List test= criteria.list();

		Long ret = ((Number)criteria.list().get(0)).longValue();
		logger.info("exit JPATransactionDetailDAO.count() = " + ret );	
		System.out.println(ret);
		closeLocalSession(session);
		session=null;

        return ret;
	}
	
	@Transactional(readOnly=true)
	public Date minGLDate(PurchaseTransaction exampleInstance, Date effectiveDate,
			Date expirationDate, Boolean includeUnprocessed,
			Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull) {
		
		//0004691
		Session session = createLocalSession();
		Criteria criteria = generateCriteria(session, exampleInstance, 
				effectiveDate, expirationDate, 
				includeUnprocessed, includeProcessed, includeSuspended, 
				glExtractFlagIsNull); 
 
		criteria.setProjection(Projections.min("glDate"));
		
        Object dateObject = criteria.list().get(0);
        Date minGLDate = null;
        if(dateObject==null){
        	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        	try {
        		java.util.Calendar cal = new java.util.GregorianCalendar();

        		//5205
        		int year = cal.get(java.util.Calendar.YEAR);             // 2002
        		int month = cal.get(java.util.Calendar.MONTH);           // 0=Jan, 1=Feb, ...
        		int day = cal.get(java.util.Calendar.DAY_OF_MONTH);      // 1...
        		minGLDate = format.parse( (month+1) + "/" + day + "/" + year);
        		
        		//minGLDate = format.parse("01/01/1900");
        	}
        	catch(ParseException pe) {
        	}
        }
        else{
        	minGLDate = (Date)dateObject;
        }
		
        logger.debug("minGLDate: " + minGLDate.toString());
        
        closeLocalSession(session);
        session=null;

        return minGLDate;
	}
	
	//SaveAs
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public String getProcessSQLStatement(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults) {
		
		List<String> whereClause = new ArrayList<String>();
		Class<?> cls = exampleInstance.getClass();

		// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
		String transactionColumn = "";
    	for (Method m : cls.getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && 
    			m.getReturnType().equals(String.class) &&
    			(m.getParameterTypes().length == 0)) {
				try { 			
					String value = (String) m.invoke(exampleInstance);
					transactionColumn = "";
	    			if (value != null) {
	    				String field = com.ncsts.common.Util.lowerCaseFirst(m.getName().substring(3));
	    				
	    				if(field.equalsIgnoreCase("idPropertyName") || field.equalsIgnoreCase("processStatus")){
	    					//check processStatus in PurchaseTransaction ?
	    					continue;
	    				}
	    				
	    				try{
	    					Field f = cls.getDeclaredField(field);//TransactionInd
	    					transactionColumn = f.getAnnotation(Column.class).name();
	    				}
	    				catch(Exception e){
	    					System.out.println(e.toString());
	    				}
	    				
	    				if (value.equalsIgnoreCase("*NULL") || value.equalsIgnoreCase("*ALL") || value.contains("%")) {
		    				if (value.equalsIgnoreCase("*ALL")) {
		    					// Nothing more to do than exclude it
		    				} else if (value.equalsIgnoreCase("*NULL")) {
		    					whereClause.add(transactionColumn + " is null");
		    					System.out.println(transactionColumn + " is null");
		    					
		    				} else {
		    					whereClause.add(transactionColumn + " like '" + value + "'");
		    					System.out.println(transactionColumn + " like '" + value + "'");
		    				}
	    				}   				
	    				else if (m.getName().endsWith("Flag") && value.equals("0")) {
	    					whereClause.add(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    				}
	    				else{
	    					whereClause.add(transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " = '"+value+ "'");
	    				}
	    			}
	    			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    	}
    	
    	// Add in processed/suspended filters
		if ((includeUnprocessed != null) && includeUnprocessed) {
			try{
				Field f = cls.getDeclaredField("transactionInd");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " <> 'P'");
				System.out.println(transactionColumn + " <> 'P'");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		String processedCriteria = "";
		try{
			Field f = cls.getDeclaredField("transactionInd");
			transactionColumn = f.getAnnotation(Column.class).name();
			processedCriteria = transactionColumn + " = 'P'";
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
		String processed = ((includeProcessed == null) || !includeProcessed)? null:processedCriteria;		
		
		String suspendedCriteria = "";
		try{
			Field f = cls.getDeclaredField("transactionInd");
			transactionColumn = f.getAnnotation(Column.class).name();
			
			suspendedCriteria = transactionColumn + " = 'S'";
			
			f = cls.getDeclaredField("suspendInd");
			transactionColumn = f.getAnnotation(Column.class).name();
			
			suspendedCriteria = suspendedCriteria + " and " + transactionColumn + " is null ";
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
		String suspended = ((includeSuspended == null) || includeSuspended.equals(""))? 
				null:suspendedCriteria;
		
		if ((processed != null) && (suspended != null)) {
			whereClause.add("(" + processedCriteria + ") or (" + suspendedCriteria + ")");
			System.out.println("(" + processedCriteria + ") or (" + suspendedCriteria + ")");
		} else if (processed != null) {
			whereClause.add(processedCriteria);
			System.out.println(processedCriteria);
			
		} else if (suspended != null) {
			whereClause.add(suspendedCriteria);
			System.out.println(suspendedCriteria);
		}
		
		// glExtractFlag
		if ((glExtractFlagIsNull != null) && glExtractFlagIsNull) {
			try{
				Field f = cls.getDeclaredField("glExtractFlag");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " is null" + " or " + transactionColumn + " = 0");
				System.out.println(transactionColumn + " is null" + " or " + transactionColumn + " = 0");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		// Add in date restrictions
		if (effectiveDate != null) {
			try{
				Field f = cls.getDeclaredField("glDate");
				transactionColumn = f.getAnnotation(Column.class).name();
				SimpleDateFormat format= new SimpleDateFormat("yyyy/MM/dd");
				whereClause.add(transactionColumn + " >= to_date('" +format.format(effectiveDate)+ "', 'yyyy/mm/dd')");
				System.out.println(transactionColumn + " >= to_date('" +format.format(effectiveDate)+ "', 'yyyy/mm/dd')");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		if (expirationDate != null) {
			try{
				Field f = cls.getDeclaredField("glDate");
				transactionColumn = f.getAnnotation(Column.class).name();
				SimpleDateFormat format= new SimpleDateFormat("yyyy/MM/dd");
				whereClause.add(transactionColumn + " <= to_date('" +format.format(expirationDate)+ "', 'yyyy/mm/dd')");
				System.out.println(transactionColumn + " <= to_date('" +format.format(expirationDate)+ "', 'yyyy/mm/dd')");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//transactionDetailId
		Long id = (exampleInstance == null)? null:exampleInstance.getPurchtransId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("purchtransId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//jurisdictionId
		id = (exampleInstance == null)? null:exampleInstance.getShiptoJurisdictionId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("shiptoJurisdictionId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//glExtractBatchNo
		id = (exampleInstance == null)? null:exampleInstance.getGlExtractBatchNo(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("glExtractBatchNo");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//taxMatrixId
		id = (exampleInstance == null)? null:exampleInstance.getTaxMatrixId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("taxMatrixId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//locationMatrixId
		id = (exampleInstance == null)? null:exampleInstance.getShiptoLocnMatrixId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("shiptoLocnMatrixId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//processBatchNo
		id = (exampleInstance == null)? null:exampleInstance.getProcessBatchNo(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("processBatchNo");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}

		if(advancedFilter!=null && advancedFilter.length()>0){
			whereClause.add(advancedFilter);
			System.out.println(advancedFilter);
		}
		
		//Maxrow
		whereClause.add("ROWNUM <= " + maxResults);

		String orderByStr = "";
		if (orderBy != null){
        	orderByStr = "";
            for (FieldSortOrder field : orderBy.getFields()){
            	if(orderByStr.length()==0){
            		orderByStr = " order by ";
            	}
            	else{
            		orderByStr = orderByStr + " , ";
            	}
            	
                if (field.getAscending()){  
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " asc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}

                } else {
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " desc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}
                }
            }
        }
		
		String whereClauseStr = "";
		//if(whereClause)
		for (String criteriaStr : whereClause){
        	if(whereClauseStr.length()==0){
        		whereClauseStr = "(" + criteriaStr + ")";
        	}
        	else{
        		whereClauseStr = whereClauseStr + " and (" + criteriaStr + ")";
        	}
        }
		
		if(whereClauseStr.length()>0){
			whereClauseStr = "where " + whereClauseStr;
		}
		
		String sqlStatement = "select * from TB_PURCHTRANS " + whereClauseStr + " " + orderByStr;  

		
		System.out.println("xxxxxxxxxxxxxx SQL: " + sqlStatement);
		
		
		String sql = " SELECT COUNT(*) from TB_PURCHTRANS " + whereClauseStr;
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
		List<Number> countList = query.list();
		System.out.println("xxxxxxxxxxxxxx COUNT(*): " + ((Number)countList.get(0)).intValue());
		closeLocalSession(session);
		session=null;
		
		System.out.println(sqlStatement);
		return sqlStatement;
	}
	
	//SaveAs
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public String getProcessSQLCriteria(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults) {
		
		List<String> whereClause = new ArrayList<String>();
		Class<?> cls = exampleInstance.getClass();
		
		//TODO:DEBUGGING CODE
		
		// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
		String transactionColumn = "";
    	for (Method m : cls.getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && 
    			m.getReturnType().equals(String.class) &&
    			(m.getParameterTypes().length == 0)) {
				try { 			
					String value = (String) m.invoke(exampleInstance);
					transactionColumn = "";
	    			if (value != null) {
	    				String field = com.ncsts.common.Util.lowerCaseFirst(m.getName().substring(3));
	    				
	    				if(field.equalsIgnoreCase("idPropertyName") || field.equalsIgnoreCase("processStatus") || 
	    						field.equalsIgnoreCase("transactionCountryCode") || field.equalsIgnoreCase("transactionStateCode") ||
	    						field.equalsIgnoreCase("transactionInd") || field.equalsIgnoreCase("suspendInd") ||
	    						field.equalsIgnoreCase("multiTransCodeDesc")){
	    					//check processStatus in PurchaseTransaction ?
	    					continue;
	    				}
	    				
	    				try{
	    					Field f = cls.getDeclaredField(field);//TransactionInd
	    					transactionColumn = f.getAnnotation(Column.class).name();
	    				}
	    				catch(Exception e){
	    					System.out.println(e.toString());
	    				}
	    				
	    				if (value.equalsIgnoreCase("*NULL") || value.equalsIgnoreCase("*ALL") || value.contains("%")) {
		    				if (value.equalsIgnoreCase("*ALL")) {
		    					// Nothing more to do than exclude it
		    				} else if (value.equalsIgnoreCase("*NULL")) {
		    					whereClause.add(transactionColumn + " is null");
		    					System.out.println(transactionColumn + " is null");
		    					
		    				} else {
		    					whereClause.add(transactionColumn + " like '" + value + "'");
		    					System.out.println(transactionColumn + " like '" + value + "'");
		    				}
	    				}   				
	    				else if (m.getName().endsWith("Flag") && value.equals("0")) {
	    					whereClause.add(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    				}
	    				else if (!transactionColumn.isEmpty()){
	    					whereClause.add(transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " = '"+value+ "'");
	    				}
	    			}
	    			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    	}

		if(exampleInstance.getTransactionCountryCode()!=null && exampleInstance.getTransactionCountryCode().length()>0){
			whereClause.add("TRANSACTION_COUNTRY_CODE='"+exampleInstance.getTransactionCountryCode()+"'");
		}
		if(exampleInstance.getTransactionStateCode()!=null && exampleInstance.getTransactionStateCode().length()>0){
			whereClause.add("TRANSACTION_STATE_CODE='"+exampleInstance.getTransactionStateCode()+"'");
		}
		if(exampleInstance.getTransactionInd()!=null && exampleInstance.getTransactionInd().length()>0){
			whereClause.add("TRANSACTION_IND='"+exampleInstance.getTransactionInd()+"'");
		}
		if(exampleInstance.getSuspendInd()!=null && exampleInstance.getSuspendInd().length()>0){
			whereClause.add("SUSPEND_IND='"+exampleInstance.getSuspendInd()+"'");
		}
    	
    	// Add in processed/suspended filters
		if ((includeUnprocessed != null) && includeUnprocessed) {
			whereClause.add("TRANSACTION_IND<>'P'");
		}
		
		String processedSql = ((includeProcessed == null) || !includeProcessed)? null : "TRANSACTION_IND='P'";		
		String suspendedSql = ((includeSuspended == null) || includeSuspended.equals(""))? null: "(TRANSACTION_IND='S' and SUSPEND_IND='"+includeSuspended+"')";
		
		if ((processedSql != null) && (suspendedSql != null)) {
			whereClause.add("(" + processedSql + " or " + suspendedSql + ")");
		} else if (processedSql != null) {
			whereClause.add(processedSql);
		} else if (suspendedSql != null) {
			whereClause.add(suspendedSql);
		}

		// glExtractFlag
		if ((glExtractFlagIsNull != null) && glExtractFlagIsNull) {
			try{
				Field f = cls.getDeclaredField("glExtractFlag");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " is null" + " or " + transactionColumn + " = 0");
				System.out.println(transactionColumn + " is null" + " or " + transactionColumn + " = 0");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//if search for CLOSE, TransactionInd = "P" and GlExtractFlag = 1
		if(exampleInstance.getGlExtractFlag()!=null && exampleInstance.getGlExtractFlag()=="1"){
			try{
				Field f = cls.getDeclaredField("glExtractFlag");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " is not null" + " and " + transactionColumn + " = 1");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		// Add in date restrictions
		if (effectiveDate != null) {
			try{
				Field f = cls.getDeclaredField("glDate");
				transactionColumn = f.getAnnotation(Column.class).name();
				SimpleDateFormat format= new SimpleDateFormat("yyyy/MM/dd");
				whereClause.add(transactionColumn + " >= to_date('" +format.format(effectiveDate)+ "', 'yyyy/mm/dd')");
				System.out.println(transactionColumn + " >= to_date('" +format.format(effectiveDate)+ "', 'yyyy/mm/dd')");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		if (expirationDate != null) {
			try{
				Field f = cls.getDeclaredField("glDate");
				transactionColumn = f.getAnnotation(Column.class).name();
				SimpleDateFormat format= new SimpleDateFormat("yyyy/MM/dd");
				whereClause.add(transactionColumn + " <= to_date('" +format.format(expirationDate)+ "', 'yyyy/mm/dd')");
				System.out.println(transactionColumn + " <= to_date('" +format.format(expirationDate)+ "', 'yyyy/mm/dd')");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//transactionDetailId
		Long id = (exampleInstance == null)? null:exampleInstance.getPurchtransId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("purchtransId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//jurisdictionId
		id = (exampleInstance == null)? null:exampleInstance.getShiptoJurisdictionId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("shiptoJurisdictionId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//glExtractBatchNo
		id = (exampleInstance == null)? null:exampleInstance.getGlExtractBatchNo(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("glExtractBatchNo");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//taxMatrixId
		id = (exampleInstance == null)? null:exampleInstance.getTaxMatrixId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("taxMatrixId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//locationMatrixId
		id = (exampleInstance == null)? null:exampleInstance.getShiptoLocnMatrixId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("shiptoLocnMatrixId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		//processBatchNo
		id = (exampleInstance == null)? null:exampleInstance.getProcessBatchNo(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("processBatchNo");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}

		if(advancedFilter!=null && advancedFilter.length()>0){
			whereClause.add(advancedFilter);
			System.out.println(advancedFilter);
		}
		
		String whereClauseStr = "";
		//if(whereClause)
		for (String criteriaStr : whereClause){
        	if(whereClauseStr.length()==0){
        		whereClauseStr = "(" + criteriaStr + ")";
        	}
        	else{
        		whereClauseStr = whereClauseStr + " and (" + criteriaStr + ")";
        	}
        }
	
		return whereClauseStr;
	}
	
	//SaveAs
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public String getStatisticsSQLStatement(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults, List<String> nullFileds) {
		
		List<String> whereClause = new ArrayList<String>();
		Class<?> cls = exampleInstance.getClass();

		// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
		String transactionColumn = "";
    	for (Method m : cls.getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && 
    			m.getReturnType().equals(String.class) &&
    			(m.getParameterTypes().length == 0)) {
				try { 			
					String value = (String) m.invoke(exampleInstance);
					transactionColumn = "";
	    			if (value != null) {
	    				String field = com.ncsts.common.Util.lowerCaseFirst(m.getName().substring(3));
	    				
	    				if(field.equalsIgnoreCase("idPropertyName") || field.equalsIgnoreCase("processStatus")){
	    					//check processStatus in PurchaseTransaction ?
	    					continue;
	    				}
	    				
	    				try{
	    					Field f = cls.getDeclaredField(field);//TransactionInd
	    					transactionColumn = f.getAnnotation(Column.class).name();
	    				}
	    				catch(Exception e){
	    					System.out.println(e.toString());
	    				}
	    				
	    				if (value.equalsIgnoreCase("*NULL") || value.equalsIgnoreCase("*ALL") || value.contains("%")) {
		    				if (value.equalsIgnoreCase("*ALL")) {
		    					// Nothing more to do than exclude it
		    				} else if (value.equalsIgnoreCase("*NULL")) {
		    					whereClause.add(transactionColumn + " is null");
		    					System.out.println(transactionColumn + " is null");
		    					
		    				} else {
		    					whereClause.add(transactionColumn + " like '" + value + "'");
		    					System.out.println(transactionColumn + " like '" + value + "'");
		    				}
	    				}   				
	    				else if (m.getName().endsWith("Flag") && value.equals("0")) {
	    					whereClause.add(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " is null" + " or " + transactionColumn + " = '"+value+ "'");
	    				}
	    				else{
	    					whereClause.add(transactionColumn + " = '"+value+ "'");
	    					System.out.println(transactionColumn + " = '"+value+ "'");
	    				}
	    			}
	    			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    	}
    	
    	
    	//for drill down view on selecting null columns
		for(String nullField: nullFileds){
			try{
				Field f = cls.getDeclaredField(nullField);
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " is null");
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("select PURCHTRANS_ID from tb_purchtrans  left outer join TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = tb_purchtrans.JURISDICTION_ID ");
		
		if ((sqlWhere != null) && !sqlWhere.equals("")) {
			//3464
			sql.append(sqlWhere);
		}

		whereClause.add("PURCHTRANS_ID in (" + sql.toString() + ")");
			
		//transactionDetailId
		Long id = (exampleInstance == null)? null:exampleInstance.getPurchtransId(); 
		if ((id != null) && (id > 0L)) {
			try{
				Field f = cls.getDeclaredField("transactionDetailId");
				transactionColumn = f.getAnnotation(Column.class).name();
				whereClause.add(transactionColumn + " = " + id.toString());
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
		}
	
		//Maxrow
		whereClause.add("ROWNUM <= " + maxResults);

		String orderByStr = "";
		if (orderBy != null){
        	orderByStr = "";
            for (FieldSortOrder field : orderBy.getFields()){
            	if(orderByStr.length()==0){
            		orderByStr = " order by ";
            	}
            	else{
            		orderByStr = orderByStr + " , ";
            	}
            	
                if (field.getAscending()){  
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " asc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}

                } else {
                    try{
    					Field f = cls.getDeclaredField(field.getName());
    					transactionColumn = f.getAnnotation(Column.class).name();
    					orderByStr = orderByStr + transactionColumn + " desc ";
    				}
    				catch(Exception e){
    					System.out.println(e.toString());
    				}
                }
            }
        }
		
		String whereClauseStr = "";
		//if(whereClause)
		for (String criteriaStr : whereClause){
        	if(whereClauseStr.length()==0){
        		whereClauseStr = "(" + criteriaStr + ")";
        	}
        	else{
        		whereClauseStr = whereClauseStr + " and (" + criteriaStr + ")";
        	}
        }
		
		if(whereClauseStr.length()>0){
			whereClauseStr = "where " + whereClauseStr;
		}
		
		String sqlStatement = "select * from tb_purchtrans " + whereClauseStr + " " + orderByStr;  

		
		System.out.println("xxxxxxxxxxxxxx SQL: " + sqlStatement);
		
		
		String sql1 = " SELECT COUNT(*) from tb_purchtrans " + whereClauseStr;
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql1);
		List<Number> countList = query.list();
		System.out.println("xxxxxxxxxxxxxx COUNT(*): " + ((Number)countList.get(0)).intValue());
		closeLocalSession(session);
		session=null;
		
		return sqlStatement;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<PurchaseTransaction> getAllRecords(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter, 
			OrderBy orderBy, int firstRow, int maxResults) {
		List<PurchaseTransaction> transactionList = null;


		//0004691
		Session session = createLocalSession();
		try { 
			//0004691
			Criteria criteria = generateCriteria(session, exampleInstance, 
					effectiveDate, expirationDate, 
					includeUnprocessed, includeProcessed, includeSuspended, 
					glExtractFlagIsNull); 
			
			if(advancedFilter!=null && advancedFilter.length()>0){
				criteria.add(Restrictions.sqlRestriction("("+advancedFilter+")"));
			}
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            transactionList = criteria.list();
            
			LogMemory.executeGC();
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return transactionList;
	}
	
	// Method modified for 3925
	private Criteria generateCriteria(Session localSession, PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull) {

		//0004691
		Criteria criteria = localSession.createCriteria(PurchaseTransaction.class);
		
		
		if (exampleInstance != null) {
			Example example = Example.create(exampleInstance);
			 example.ignoreCase();
			example.excludeProperty("processStatus");
	
			// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
	        ryanExampleHandling(criteria, example, exampleInstance);
	        
	        //4193
	        example.excludeProperty("transactionCountryCode");
			example.excludeProperty("transactionStateCode");
			example.excludeProperty("transactionInd");
			example.excludeProperty("suspendInd");
			
			criteria.add(example);
		}

		
		//4193
		if(exampleInstance.getTransactionCountryCode()!=null && exampleInstance.getTransactionCountryCode().length()>0){
			criteria.add(Restrictions.sqlRestriction("TRANSACTION_COUNTRY_CODE='"+exampleInstance.getTransactionCountryCode()+"'"));
		}
		if(exampleInstance.getTransactionStateCode()!=null && exampleInstance.getTransactionStateCode().length()>0){
			criteria.add(Restrictions.sqlRestriction("TRANSACTION_STATE_CODE='"+exampleInstance.getTransactionStateCode()+"'"));
		}
		if(exampleInstance.getTransactionInd()!=null && exampleInstance.getTransactionInd().length()>0){
			criteria.add(Restrictions.sqlRestriction("TRANSACTION_IND='"+exampleInstance.getTransactionInd()+"'"));
		} 
		if(exampleInstance.getSuspendInd()!=null && exampleInstance.getSuspendInd().length()>0){
			criteria.add(Restrictions.sqlRestriction("SUSPEND_IND='"+exampleInstance.getSuspendInd()+"'"));
		}
		
		// Add in processed/suspended filters
		if ((includeUnprocessed != null) && includeUnprocessed) {
			//criteria.add(Restrictions.ne("transactionInd", "P").ignoreCase()); //4193 ignoreCase
			//4193 ignoreCase
			criteria.add(Restrictions.sqlRestriction("TRANSACTION_IND<>'P'"));
		}
			
		String processedSql = ((includeProcessed == null) || !includeProcessed)? null : "TRANSACTION_IND='P'";
		
		String suspendedSql = ((includeSuspended == null) || includeSuspended.equals(""))? null: "(TRANSACTION_IND='S' and SUSPEND_IND='"+includeSuspended+"')";
		
		if ((processedSql != null) && (suspendedSql != null)) {
			//4193 ignoreCase 
			//criteria.add(Restrictions.or(processed, suspended));
			criteria.add(Restrictions.sqlRestriction("(" + processedSql + " or " + suspendedSql + ")"));
		} else if (processedSql != null) {
			//4193 ignoreCase criteria.add(processed);
			criteria.add(Restrictions.sqlRestriction(processedSql));
		} else if (suspendedSql != null) {
			//4193 ignoreCase criteria.add(suspended);
			criteria.add(Restrictions.sqlRestriction(suspendedSql));
		}
		
//		// glExtractFlag
		if ((glExtractFlagIsNull != null) && glExtractFlagIsNull) {
			Criterion glExtractFlagIsNullRestrictions = Restrictions.or(
						Restrictions.isNull("glExtractFlag"), Restrictions.eq("glExtractFlag", new String("0")));
			criteria.add(glExtractFlagIsNullRestrictions);
		}
		
		// Add in date restrictions
		if (effectiveDate != null) {
			criteria.add(Restrictions.ge("glDate", effectiveDate));
		}
		
		if (expirationDate != null) {
			criteria.add(Restrictions.le("glDate", expirationDate));
		}
		
		extendExampleCriteria(criteria, exampleInstance);
		return criteria;
	}
	
	
	

	@Transactional(readOnly=true)
	public boolean getTaxMatrixInUse(Long taxMatrixId) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String baseQuery = "Select count(*) from PurchaseTransaction td where td.taxMatrixId = :taxMatrixId";
		
		Query query = entityManager.createQuery(baseQuery);
		query.setParameter("taxMatrixId", taxMatrixId);
		return ((Long) query.getSingleResult() > 0L);
	}
	
	@Transactional(readOnly=true)
	public boolean getGSBMatrixInUse(Long taxMatrixId) {
		
		BigDecimal returnLong = null;
		try {
			String sqlQuery = "select count(*) count FROM TB_BILLTRANS WHERE tax_matrix_id = '" + taxMatrixId + "'";			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return (returnLong!=null && returnLong.longValue()>0L);
	}
		
	@Transactional(readOnly=true)
	public ProcessedTransactionDetail getSuspendedTaxMatrixTransactionCounts(Long taxMatrixId, Date asOfDate) {
		ProcessedTransactionDetail result = new ProcessedTransactionDetail();
		result.setMatrixId(taxMatrixId);
		result.setAsOfDate(asOfDate);
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String baseQuery = "Select count(*) from PurchaseTransaction td where td.taxMatrixId = :taxMatrixId";
		logger.debug("Getting suspend counts for " + taxMatrixId + " as of " + asOfDate);
		
		// Transactions Total
		Query query = entityManager.createQuery(baseQuery);
		query.setParameter("taxMatrixId", taxMatrixId);
		result.setTotalTransactions((Long) query.getSingleResult());
		
		// Transactions Before
		query = entityManager.createQuery(baseQuery + " and td.glDate < :asOfDate");
		query.setParameter("taxMatrixId", taxMatrixId);
		query.setParameter("asOfDate", asOfDate);
		result.setBeforeAsOfDateTransactions((Long) query.getSingleResult());

		// Transactions Extracted
		query = entityManager.createQuery(baseQuery + 
					" and td.glDate >= :asOfDate" +
					" and td.manualTaxcodeInd is null" +
					" and td.glExtractFlag = '1'");
		query.setParameter("taxMatrixId", taxMatrixId);
		query.setParameter("asOfDate", asOfDate);
		result.setExtractedGlTransactions((Long) query.getSingleResult());

		// Transactions Extracted Not
		query = entityManager.createQuery(baseQuery + 
					" and td.glDate >= :asOfDate" +
					" and td.manualTaxcodeInd is null" +
					" and (td.glExtractFlag is null or td.glExtractFlag <> '1')");
		query.setParameter("taxMatrixId", taxMatrixId);
		query.setParameter("asOfDate", asOfDate);
		result.setNonExtractedGlTransactions((Long) query.getSingleResult());

		// Transactions Manual
		query = entityManager.createQuery(baseQuery + 
					" and td.glDate >= :asOfDate" +
					" and td.manualTaxcodeInd is not null");
		query.setParameter("taxMatrixId", taxMatrixId);
		query.setParameter("asOfDate", asOfDate);
		result.setManuallyChangedTransactions((Long) query.getSingleResult());
		return result;
	}

	public List<PurchaseTransaction> getTransactions(){
		
		
		return null;
	}
	
	static String[] suspendColumn = {	
		"COMMENTS",
		"TB_CALC_TAX_AMT",
		"STATE_USE_AMOUNT",
		"STATE_USE_TIER2_AMOUNT",
		"STATE_USE_TIER3_AMOUNT",
		"COUNTY_USE_AMOUNT",
		"COUNTY_LOCAL_USE_AMOUNT",
		"CITY_USE_AMOUNT",
		"CITY_LOCAL_USE_AMOUNT",
		"TRANSACTION_STATE_CODE",
		"AUTO_TRANSACTION_STATE_CODE",
		"TRANSACTION_IND",
		"SUSPEND_IND",
		"TAXCODE_DETAIL_ID",
		"TAXCODE_STATE_CODE",
		"TAXCODE_TYPE_CODE",
		"TAXCODE_CODE",
		"MANUAL_TAXCODE_IND",
		"TAX_MATRIX_ID",
		"JURISDICTION_ID",
		"JURISDICTION_TAXRATE_ID",
		"MANUAL_JURISDICTION_IND",
		"MEASURE_TYPE_CODE",
		"STATE_USE_RATE",
		"STATE_USE_TIER2_RATE",
		"STATE_USE_TIER3_RATE",
		"STATE_SPLIT_AMOUNT",
		"STATE_TIER2_MIN_AMOUNT",
		"STATE_TIER2_MAX_AMOUNT",
		"STATE_MAXTAX_AMOUNT",
		"COUNTY_USE_RATE",
		"COUNTY_LOCAL_USE_RATE",
		"COUNTY_SPLIT_AMOUNT",
		"COUNTY_MAXTAX_AMOUNT",
		"COUNTY_SINGLE_FLAG",
		"COUNTY_DEFAULT_FLAG",
		"CITY_USE_RATE",
		"CITY_LOCAL_USE_RATE",
		"CITY_SPLIT_AMOUNT",
		"CITY_SPLIT_USE_RATE",
		"CITY_SINGLE_FLAG",
		"CITY_DEFAULT_FLAG",
		"COMBINED_USE_RATE",
		"AUDIT_FLAG",
		"AUDIT_USER_ID",
		"AUDIT_TIMESTAMP",
		"MODIFY_USER_ID",
		"MODIFY_TIMESTAMP",
		"UPDATE_USER_ID",
		"UPDATE_TIMESTAMP",
		"COUNTRY_USE_AMOUNT",
		"TRANSACTION_COUNTRY_CODE",
		"TAXCODE_COUNTRY_CODE",
		"COUNTRY_USE_RATE",
		"SPLIT_SUBTRANS_ID",
		"MULTI_TRANS_CODE",
		"TAX_ALLOC_MATRIX_ID",
		"STATE_TAXCODE_DETAIL_ID",
		"COUNTY_TAXCODE_DETAIL_ID",
		"CITY_TAXCODE_DETAIL_ID",
		"TAXTYPE_USED_CODE",
		"STATE_TAXABLE_AMT",
		"COUNTY_TAXABLE_AMT",
		"CITY_TAXABLE_AMT",
		"STATE_TAXCODE_TYPE_CODE",
		"COUNTY_TAXCODE_TYPE_CODE",
		"CITY_TAXCODE_TYPE_CODE",
		"ENTITY_CODE",
		"STJ1_RATE",
		"STJ2_RATE",
		"STJ3_RATE",
		"STJ4_RATE",
		"STJ5_RATE",
		"STJ1_AMOUNT",
		"STJ2_AMOUNT",
		"STJ3_AMOUNT",
		"STJ4_AMOUNT",
		"STJ5_AMOUNT",
		"PRODUCT_CODE",
		"REJECT_FLAG",
		"STATE_USE_TIER1_SETAMT",
		"STATE_USE_TIER2_SETAMT",
		"STATE_USE_TIER3_SETAMT"
		
	};

	@SuppressWarnings("unchecked")
	@Transactional
	public void suspendTaxMatrixTransactions(PurchaseTransactionService purchaseTransactionService, Long taxMatrixId, Date asOfDate, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine) {
		
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> list = getJpaTemplate().find(qry);
		if ((list != null) && (list.size() > 0)) {
			pco = (String)list.get(0);
		}
	    
		// Update query - needs to be transactional!
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	org.hibernate.Query query;
    	
    	//0006134
    	String whereClause = null;	
		if (suspendExtracted) {//this is WILL � also suspend extracted
			whereClause =
				" and GL_DATE >= :asOfDate" +
				" and MANUAL_TAXCODE_IND is null";
		}
		else{// this is WILL NOT � only suspend non-extracted
			whereClause = 
				" and GL_DATE >= :asOfDate" +
				" and MANUAL_TAXCODE_IND is null" +
				" and (GL_EXTRACT_FLAG is null or GL_EXTRACT_FLAG <> '1')";
		}
		
		//4730, Insert into TB_GL_EXPORT_LOG 
		String whereClauseLog = null;
		if (suspendExtracted) {//this is WILL � reverse only extracted transactions
			whereClauseLog =
                 " and GL_DATE >= :asOfDate" +
                 " and MANUAL_TAXCODE_IND is null" +
                 " and ((GL_EXTRACT_FLAG is not null and GL_EXTRACT_FLAG='1')" +
                 "  or  (GL_LOG_FLAG is not null and GL_LOG_FLAG='1'))";
		}
		else{// this is WILL NOT � only suspend non-extracted
			whereClauseLog = 
                   " and GL_DATE >= :asOfDate" +
                   " and MANUAL_TAXCODE_IND is null" +
                   " and (GL_EXTRACT_FLAG is null or GL_EXTRACT_FLAG <> '1')" +
                   " and (GL_LOG_FLAG is not null and GL_LOG_FLAG=1)";
		}
				
		long sysdate = System.currentTimeMillis();
  	  	  
  	  	//  -- 1. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
  	  	//  --    This is to offset the previous tax amount that had been sent into ERP system.
  	  	Class<?> cls = GlExportLog.class;
	    Field [] fields = cls.getDeclaredFields();
	    
	    StringBuffer columnBuf = new StringBuffer("");
	    boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if ((col != null)) {
				idx++;
				if (first)
					first = false;
				else {
					columnBuf.append(", ");
				}
				columnBuf.append(col.name());
			}
		}
		
		String valueStr = columnBuf.toString();
		//*-1.0
		valueStr = valueStr.replace("TB_CALC_TAX_AMT", "TB_CALC_TAX_AMT*-1.0");
		valueStr = valueStr.replace("STATE_USE_AMOUNT", "STATE_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("STATE_USE_TIER2_AMOUNT", "STATE_USE_TIER2_AMOUNT*-1.0");
		valueStr = valueStr.replace("STATE_USE_TIER3_AMOUNT", "STATE_USE_TIER3_AMOUNT*-1.0");
		valueStr = valueStr.replace("COUNTY_USE_AMOUNT", "COUNTY_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("COUNTY_LOCAL_USE_AMOUNT", "COUNTY_LOCAL_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("CITY_USE_AMOUNT", "CITY_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("CITY_LOCAL_USE_AMOUNT", "CITY_LOCAL_USE_AMOUNT*-1.0");	
		valueStr = valueStr.replace("STJ1_AMOUNT", "STJ1_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ2_AMOUNT", "STJ2_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ3_AMOUNT", "STJ3_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ4_AMOUNT", "STJ4_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ5_AMOUNT", "STJ5_AMOUNT*-1.0");
		valueStr = valueStr.replace("GL_EXTRACT_FLAG", "NULL");

		//use local user id & timestamp
		valueStr = valueStr.replace("UPDATE_USER_ID", ":updateUserId");
		valueStr = valueStr.replace("UPDATE_TIMESTAMP", ":updateTimestamp");
		//Clicking ok button after update count fails
//		String exportSql = "insert into TB_GL_EXPORT_LOG (" + columnBuf.toString() + ") select " + valueStr + " from tb_purchtrans WHERE TAX_MATRIX_ID = :taxMatrixId  ";	
//		if (whereClauseLog != null) {
//			query = session.createSQLQuery(exportSql + whereClauseLog);
//			query.setParameter("taxMatrixId", taxMatrixId);
//			query.setParameter("asOfDate", asOfDate);
//			
//			query.setParameter("updateUserId", Auditable.currentUserCode()); 
//			query.setParameter("updateTimestamp", new Date(sysdate));
//			
//			int rows = query.executeUpdate();
//			logger.debug(rows + " TB_GL_EXPORT_LOG rows inserted");
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}   	   
//		System.out.println("************ exportSql: " + exportSql + whereClauseLog);		
		
		//Insert into tb_purchtrans_LOG
		Map<String,String> mapColumnValue = new HashMap<String,String>();	
		mapColumnValue.put("TRANSACTION_IND","S");
		mapColumnValue.put("SUSPEND_IND","T");
		mapColumnValue.put("GL_LINE_ITM_DIST_AMT","(CASE WHEN GL_EXTRACT_AMT IS NOT NULL AND GL_EXTRACT_AMT <> 0 THEN GL_EXTRACT_AMT ELSE GL_LINE_ITM_DIST_AMT END)");
		mapColumnValue.put("TAXCODE_CODE","");
		mapColumnValue.put("MANUAL_TAXCODE_IND","");
		mapColumnValue.put("TAX_MATRIX_ID","NULL");
		mapColumnValue.put("TB_CALC_TAX_AMT","0" );
		mapColumnValue.put("STATE_USE_AMOUNT","0");
		mapColumnValue.put("STATE_USE_TIER2_AMOUNT","0");
		mapColumnValue.put("STATE_USE_TIER3_AMOUNT","0");
		mapColumnValue.put("COUNTY_USE_AMOUNT","0");
		mapColumnValue.put("COUNTY_LOCAL_USE_AMOUNT","0");
		mapColumnValue.put("CITY_USE_AMOUNT","0");
		mapColumnValue.put("CITY_LOCAL_USE_AMOUNT","0");
		mapColumnValue.put("STATE_USE_RATE","0");
		mapColumnValue.put("STATE_USE_TIER2_RATE","0");
		mapColumnValue.put("STATE_USE_TIER3_RATE","0");
		mapColumnValue.put("STATE_TIER2_MIN_AMOUNT","0");
		mapColumnValue.put("STATE_TIER2_MAX_AMOUNT","0");
		mapColumnValue.put("COUNTY_USE_RATE","0");
		mapColumnValue.put("COUNTY_LOCAL_USE_RATE","0");
		mapColumnValue.put("COUNTY_SPLIT_AMOUNT","0");
		mapColumnValue.put("COUNTY_MAXTAX_AMOUNT","0");
		mapColumnValue.put("COUNTY_SINGLE_FLAG","");
		mapColumnValue.put("COUNTY_DEFAULT_FLAG","");
		mapColumnValue.put("CITY_USE_RATE","0");
		mapColumnValue.put("CITY_LOCAL_USE_RATE","0");
		mapColumnValue.put("CITY_SPLIT_AMOUNT","0");
		mapColumnValue.put("CITY_SPLIT_USE_RATE","0");
		mapColumnValue.put("CITY_SINGLE_FLAG","");
		mapColumnValue.put("CITY_DEFAULT_FLAG","");
		mapColumnValue.put("COMBINED_USE_RATE","0");
		mapColumnValue.put("STATE_SPLIT_AMOUNT","0");
		mapColumnValue.put("STATE_MAXTAX_AMOUNT","0");
		mapColumnValue.put("STJ1_AMOUNT","0");
		mapColumnValue.put("STJ2_AMOUNT","0");
		mapColumnValue.put("STJ3_AMOUNT","0");
		mapColumnValue.put("STJ4_AMOUNT","0");
		mapColumnValue.put("STJ5_AMOUNT","0");
		mapColumnValue.put("TAXCODE_DETAIL_ID","NULL");
		mapColumnValue.put("TAXCODE_STATE_CODE","");
		mapColumnValue.put("TAXCODE_TYPE_CODE","");
		mapColumnValue.put("MEASURE_TYPE_CODE","");
		mapColumnValue.put("TAXCODE_COUNTRY_CODE","");
		mapColumnValue.put("STATE_TAXCODE_DETAIL_ID","NULL");
		mapColumnValue.put("COUNTY_TAXCODE_DETAIL_ID","NULL");
		mapColumnValue.put("CITY_TAXCODE_DETAIL_ID","NULL");
		mapColumnValue.put("TAXTYPE_USED_CODE","");
		mapColumnValue.put("STATE_TAXABLE_AMT","0");
		mapColumnValue.put("COUNTY_TAXABLE_AMT","0");
		mapColumnValue.put("CITY_TAXABLE_AMT","0");
		mapColumnValue.put("STATE_TAXCODE_TYPE_CODE","");
		mapColumnValue.put("COUNTY_TAXCODE_TYPE_CODE","");
		mapColumnValue.put("CITY_TAXCODE_TYPE_CODE","");
		mapColumnValue.put("GL_EXTRACT_FLAG","(CASE WHEN GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1' THEN NULL ELSE GL_EXTRACT_FLAG END)");
		mapColumnValue.put("GL_LOG_FLAG","(CASE WHEN GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1' THEN 1 ELSE GL_LOG_FLAG END)");

		StringBuffer columnAllBuf = new StringBuffer("");
		StringBuffer valueAllBuf = new StringBuffer("");		
		columnAllBuf.append("PURCHTRANS_ID,LOG_SOURCE,GL_EXTRACT_BATCH_NO,GL_EXTRACT_TIMESTAMP,B_AUTO_TRANS_COUNTRY_CODE,A_AUTO_TRANS_COUNTRY_CODE,");
		valueAllBuf.append("PURCHTRANS_ID,'suspendTaxMatrixTransactions()',NULL,NULL,AUTO_TRANSACTION_COUNTRY_CODE AS B_AUTO_TRANS_COUNTRY_CODE,AUTO_TRANSACTION_COUNTRY_CODE AS A_AUTO_TRANS_COUNTRY_CODE,");

		String asValue = "";
		for(int i=0; i<suspendColumn.length; i++){
			columnAllBuf.append("B_" + suspendColumn[i] + "," + "A_"+suspendColumn[i] + ",");
			asValue = mapColumnValue.get(suspendColumn[i]);
			if(asValue != null){ //"0 AS A_TB_CALC_TAX_AMT"
				if(asValue.equalsIgnoreCase("NULL") || asValue.equalsIgnoreCase("0") || asValue.contains("CASE")){
	    		}
	    		else{
	    			asValue = "'" + asValue + "'";
	    		}
				valueAllBuf.append(suspendColumn[i] + " AS " + "B_" + suspendColumn[i] + "," + asValue + " AS " + "A_" + suspendColumn[i] + ",");
			}
			else{ //"TB_CALC_TAX_AMT AS A_TB_CALC_TAX_AMT"
				valueAllBuf.append(suspendColumn[i] + " AS " + "B_" + suspendColumn[i] + "," + suspendColumn[i] + " AS " + "A_" + suspendColumn[i] + ",");
			}
    	}
		
		//use local user id & timestamp
		columnAllBuf.append("UPDATE_USER_ID,UPDATE_TIMESTAMP");
		valueAllBuf.append(":updateUserId,:updateTimestamp");

//		String exportSuspendSql = "insert into tb_purchtrans_LOG (" + columnAllBuf.toString() + ") select " + valueAllBuf.toString() + " from tb_purchtrans WHERE TAX_MATRIX_ID = :taxMatrixId  ";	
//		if (whereClause != null) {
//			query = session.createSQLQuery(exportSuspendSql + whereClause);
//			query.setParameter("taxMatrixId", taxMatrixId);
//			query.setParameter("asOfDate", asOfDate);		
//			query.setParameter("updateUserId", Auditable.currentUserCode()); 
//			query.setParameter("updateTimestamp", new Date(sysdate));
//			
//			int rows = query.executeUpdate();
//			logger.debug(rows + " tb_purchtrans_LOG rows inserted");
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}   	   
//		System.out.println("************ exportSuspendSql: " + exportSuspendSql + whereClause);
//		
		//0005977	
		if (pco != null && pco.equals("1")) {
			String saleSql = "SELECT PURCHTRANS_ID FROM tb_purchtrans WHERE tax_matrix_id = " + taxMatrixId.toString();
			Long transId = null;
			query = session.createSQLQuery(saleSql);
			List<BigDecimal> returnList = query.list();
			for(int i=0; i< returnList.size(); i++){
				transId = ((BigDecimal)returnList.get(i)).longValue();
				purchaseTransactionService.suspendAndLog(new Long(transId), SuspendReason.SUSPEND_GS, taxMatrixId, suspendExtracted);
			}
		}

		/*
		//Update transactions		
		String baseSql = 
			"UPDATE tb_transaction_detail SET " +
			"TRANSACTION_IND = 'S', " +
			"SUSPEND_IND = 'T', " +
			 "gl_line_itm_dist_amt = " + 
	            "CASE " +
	            "WHEN " +
	            "gl_extract_amt IS NOT NULL AND gl_extract_amt <> 0 " + 
	            "THEN " + 
	            "gl_extract_amt " +
	            "ELSE " + 
	            "gl_line_itm_dist_amt " +
	            "END, " + 
//ac			"TAXCODE_DETAIL_ID = NULL, " +
//ac			"TAXCODE_STATE_CODE = '', " +
//ac			"TAXCODE_TYPE_CODE = '', " +
			"TAXCODE_CODE = '', " +
			"MANUAL_TAXCODE_IND = '', " +
			"TAX_MATRIX_ID = NULL, " +
			"TB_CALC_TAX_AMT = 0, " + 
			"STATE_USE_AMOUNT = 0, " +
			"STATE_USE_TIER2_AMOUNT = 0, " +
			"STATE_USE_TIER3_AMOUNT = 0, " +
			"COUNTY_USE_AMOUNT = 0, " +
			"COUNTY_LOCAL_USE_AMOUNT = 0, " +
			"CITY_USE_AMOUNT = 0, " +
			"CITY_LOCAL_USE_AMOUNT = 0, " +
			"STATE_USE_RATE = 0, " +
			"STATE_USE_TIER2_RATE = 0, " +
			"STATE_USE_TIER3_RATE = 0, " +
			"STATE_TIER2_MIN_AMOUNT = 0, " +
			"STATE_TIER2_MAX_AMOUNT = 0, " +
			"COUNTY_USE_RATE= 0, " +
			"COUNTY_LOCAL_USE_RATE = 0, " +
			"COUNTY_SPLIT_AMOUNT= 0, " +
			"COUNTY_MAXTAX_AMOUNT= 0, " +
			"COUNTY_SINGLE_FLAG= '', " +
			"COUNTY_DEFAULT_FLAG= '', " +
			"CITY_USE_RATE= 0, " +
			"CITY_LOCAL_USE_RATE= 0, " +
			"CITY_SPLIT_AMOUNT= 0, " +
			"CITY_SPLIT_USE_RATE= 0, " +
			"CITY_SINGLE_FLAG = '', " +
			"CITY_DEFAULT_FLAG = '', " +
			"COMBINED_USE_RATE = 0, " +
//CCH		"CCH_TAXCAT_CODE = '', " +
//CCH		"CCH_GROUP_CODE = '', " +
//CCH		"CCH_ITEM_CODE = '', " +
			"STATE_SPLIT_AMOUNT = 0, " +
			"STATE_MAXTAX_AMOUNT = 0, " + 
			
			"STJ1_AMOUNT = 0, " +
			"STJ2_AMOUNT = 0, " +
			"STJ3_AMOUNT = 0, " +
			"STJ4_AMOUNT = 0, " +
			"STJ5_AMOUNT = 0, " +
			
			//0004447			
			"TAXCODE_DETAIL_ID = NULL, " +
			"TAXCODE_STATE_CODE = '', " +
			"TAXCODE_TYPE_CODE = '', " +
			"MEASURE_TYPE_CODE = '', " +
			"TAXCODE_COUNTRY_CODE = '', " +
			"STATE_TAXCODE_DETAIL_ID = NULL, " +
			"COUNTY_TAXCODE_DETAIL_ID = NULL, " +
			"CITY_TAXCODE_DETAIL_ID = NULL, " +
			"TAXTYPE_USED_CODE = '', " +
			"STATE_TAXABLE_AMT = 0, " +
			"COUNTY_TAXABLE_AMT = 0, " +
			"CITY_TAXABLE_AMT = 0, " +
			"STATE_TAXCODE_TYPE_CODE = '', " +
			"COUNTY_TAXCODE_TYPE_CODE = '', " +
			"CITY_TAXCODE_TYPE_CODE = '', " +			
			"GL_EXTRACT_FLAG =  " +
			 	"CASE  " +
			 	"WHEN  " +
			  		"GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1'  " +
			  	"THEN  " +
			  		"NULL " +
			  	"ELSE  " +
			  		"GL_EXTRACT_FLAG  " +
			  	"END,  " +
			"GL_LOG_FLAG =  " +
			 	"CASE  " +
			 	"WHEN  " +
			  		"GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1'  " +
			  	"THEN  " +
			  		"1 " +
			    "ELSE  " +
			  		"GL_LOG_FLAG " +
			    "END " +
			"WHERE TAX_MATRIX_ID = :taxMatrixId ";
		*/
		
		//Update transactions
		StringBuffer  baseSqlBuf = new StringBuffer("");
		baseSqlBuf.append("UPDATE tb_purchtrans SET ");
		String setValue = "";
		int count = 0;
		for (String setColumn : mapColumnValue.keySet()) {
			count++;
    		setValue = mapColumnValue.get(setColumn);    	
    		
    		if(setValue.equalsIgnoreCase("NULL") || setValue.equalsIgnoreCase("0") || setValue.contains("CASE")){
    			baseSqlBuf.append(setColumn + " = " + setValue);
    		}
    		else{
    			baseSqlBuf.append(setColumn + " = '" + setValue + "'");
    		}
    		
    		if(count < mapColumnValue.size()){
    			baseSqlBuf.append(", ");
    		}
    		else{
    			baseSqlBuf.append(" ");
    		}
    	}
		baseSqlBuf.append("WHERE TAX_MATRIX_ID = :taxMatrixId ");
					
		String baseSql = baseSqlBuf.toString();
//		if (whereClause != null) {
//			query = session.createSQLQuery(baseSql + whereClause);
//			query.setParameter("taxMatrixId", taxMatrixId);
//			query.setParameter("asOfDate", asOfDate);
//			int rows = query.executeUpdate();
//			logger.debug(rows + " transaction detail rows suspended");
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}

		if (deleteMatrixLine) {
			query = session.createSQLQuery(
					"update tb_purchtrans set TAX_MATRIX_ID = null where TAX_MATRIX_ID = :taxMatrixId");
			query.setParameter("taxMatrixId", taxMatrixId);
			int rows = query.executeUpdate();
			logger.debug(rows + " transaction detail rows cleared");
		} else {
			logger.debug("Matrix not cleared");
		}
		
		if (deleteMatrixLine) {
			query = session.createSQLQuery(
					"delete from TB_TAX_MATRIX where TAX_MATRIX_ID = :taxMatrixId");
			query.setParameter("taxMatrixId", taxMatrixId);
			int rows = query.executeUpdate();
			logger.debug(rows + " tax matrix rows deleted");
		} else {
			logger.debug("Matrix not deleted");
		}
		
		// All done
		t.commit();
		
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional(readOnly=true)
	public boolean getLocationMatrixInUse(Long locationMatrixId) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String baseQuery = PURCHTRANS_LOCATION_MATRIX_ID_QUERY;
		
		Query query = entityManager.createQuery(baseQuery);
		query.setParameter("locationMatrixId", locationMatrixId);
		return ((Long) query.getSingleResult() > 0L);
	}
		
	@Transactional(readOnly=true)
	public ProcessedTransactionDetail getSuspendedLocationMatrixTransactionCounts(Long locationMatrixId) {
		ProcessedTransactionDetail result = new ProcessedTransactionDetail();
		result.setMatrixId(locationMatrixId);
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String baseQuery =  PURCHTRANS_LOCATION_MATRIX_ID_QUERY;
		logger.debug("Getting suspend counts for " + locationMatrixId);
		
		PurchaseTransaction test;
		
		//Query testQuery = entityManager.createQuery("SELECT * FROM PurchaseTransaction td WHERE td.shiptoLocnMatrixId = ")
		// Transactions Total
		Query query = entityManager.createQuery(baseQuery);
		query.setParameter("locationMatrixId", locationMatrixId);
		result.setTotalTransactions((Long) query.getSingleResult());
		
		// Transactions Extracted
		query = entityManager.createQuery(baseQuery + 
					" and td.shiptoManualJurInd is null" +
					" and td.glExtractFlag = '1'");
		query.setParameter("locationMatrixId", locationMatrixId);
		result.setExtractedGlTransactions((Long) query.getSingleResult());

		// Transactions Extracted Not
		query = entityManager.createQuery(baseQuery + 
					" and td.shiptoManualJurInd is null" +
					" and (td.glExtractFlag is null or td.glExtractFlag <> '1')");
		query.setParameter("locationMatrixId", locationMatrixId);
		result.setNonExtractedGlTransactions((Long) query.getSingleResult());

		// Transactions Manual
		query = entityManager.createQuery(baseQuery + 
					" and td.shiptoManualJurInd is not null");
		query.setParameter("locationMatrixId", locationMatrixId);
		result.setManuallyChangedTransactions((Long) query.getSingleResult());
		return result;
	}
	
	//Method modified for 4072 
	@SuppressWarnings("unchecked")
	@Transactional
	public void suspendLocationMatrixTransactions(PurchaseTransactionService purchaseTransactionService, Long locationMatrixId, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine) {
		
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> list = getJpaTemplate().find(qry);
		if ((list != null) && (list.size() > 0)) {
			pco = (String)list.get(0);
		}
   
		// Update query - needs to be transactional!
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	org.hibernate.Query query;
    	
    	String whereClause = null;
    	if (suspendExtracted) {//this is WILL � also suspend extracted
			whereClause =
				" and MANUAL_TAXCODE_IND is null";
		}
		else{// this is WILL NOT � only suspend non-extracted
			whereClause = 
				" and MANUAL_TAXCODE_IND is null" +
				" and (GL_EXTRACT_FLAG is null or GL_EXTRACT_FLAG <> '1')";
		}
    	
    	//4730, Insert into TB_GL_EXPORT_LOG 
		String whereClauseLog = null;
		if (suspendExtracted) {//this is WILL � reverse only extracted transactions
			whereClauseLog =
                 " and MANUAL_TAXCODE_IND is null" +
                 " and ((GL_EXTRACT_FLAG is not null and GL_EXTRACT_FLAG='1')" +
                 "  or  (GL_LOG_FLAG is not null and GL_LOG_FLAG='1'))";
		}
		else{// this is WILL NOT � only suspend non-extracted
			whereClauseLog = 
                   " and MANUAL_TAXCODE_IND is null" +
                   " and (GL_EXTRACT_FLAG is null or GL_EXTRACT_FLAG <> '1')" +
                   " and (GL_LOG_FLAG is not null and GL_LOG_FLAG=1)";
		}
		
		long sysdate = System.currentTimeMillis();
	  	  
  	  	//  -- 1. Create a before image for this transaction into TO_GL_EXPORT_LOG table with the negative tax amount.
  	  	//  --    This is to offset the previous tax amount that had been sent into ERP system.
  	  	Class<?> cls = GlExportLog.class;
	    Field [] fields = cls.getDeclaredFields();
	    
	    StringBuffer columnBuf = new StringBuffer("");
	    boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if ((col != null)) {
				idx++;
				if (first)
					first = false;
				else {
					columnBuf.append(", ");
				}
				columnBuf.append(col.name());
			}
		}
		
		String valueStr = columnBuf.toString();
		//*-1.0
		valueStr = valueStr.replace("TB_CALC_TAX_AMT", "TB_CALC_TAX_AMT*-1.0");
		valueStr = valueStr.replace("STATE_USE_AMOUNT", "STATE_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("STATE_USE_TIER2_AMOUNT", "STATE_USE_TIER2_AMOUNT*-1.0");
		valueStr = valueStr.replace("STATE_USE_TIER3_AMOUNT", "STATE_USE_TIER3_AMOUNT*-1.0");
		valueStr = valueStr.replace("COUNTY_USE_AMOUNT", "COUNTY_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("COUNTY_LOCAL_USE_AMOUNT", "COUNTY_LOCAL_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("CITY_USE_AMOUNT", "CITY_USE_AMOUNT*-1.0");
		valueStr = valueStr.replace("CITY_LOCAL_USE_AMOUNT", "CITY_LOCAL_USE_AMOUNT*-1.0");	
		valueStr = valueStr.replace("STJ1_AMOUNT", "STJ1_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ2_AMOUNT", "STJ2_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ3_AMOUNT", "STJ3_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ4_AMOUNT", "STJ4_AMOUNT*-1.0");
		valueStr = valueStr.replace("STJ5_AMOUNT", "STJ5_AMOUNT*-1.0");
		valueStr = valueStr.replace("GL_EXTRACT_FLAG", "NULL");
		valueStr = valueStr.replace("GL_LOG_FLAG", "0");

		//use local user id & timestamp
		valueStr = valueStr.replace("UPDATE_USER_ID", ":updateUserId");
		valueStr = valueStr.replace("UPDATE_TIMESTAMP", ":updateTimestamp");


		//commented this. when click on "ok" btn after click on "update counts" fails
//		String exportSql = "insert into TB_GL_EXPORT_LOG (" + columnBuf.toString() + ") select " + valueStr + " FROM tb_purchtrans where  "
//					+"(SHIPTO_LOCN_MATRIX_ID = :locationMatrixId) or (SHIPFROM_LOCN_MATRIX_ID = :locationMatrixId) or (ORDRACPT_LOCN_MATRIX_ID = :locationMatrixId) or " +
//				"(ORDRORGN_LOCN_MATRIX_ID = :locationMatrixId) or (FIRSTUSE_LOCN_MATRIX_ID = :locationMatrixId) or (BILLTO_LOCN_MATRIX_ID = :locationMatrixId) or " +
//				"(TTLXFR_LOCN_MATRIX_ID = :locationMatrixId)";
//		if (whereClauseLog != null) {
//			query = session.createSQLQuery(exportSql + whereClauseLog);
//			query.setParameter("locationMatrixId", locationMatrixId);
//			query.setParameter("updateUserId", Auditable.currentUserCode()); 
//			query.setParameter("updateTimestamp", new Date(sysdate));
//			
//			int rows = query.executeUpdate();
//			logger.debug(rows + " TB_GL_EXPORT_LOG rows inserted");
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}   	   
//		System.out.println("************ exportSql: " + exportSql + whereClauseLog);
		
		//end
		
    	
    	//Insert into tb_purchtrans_LOG
		Map<String,String> mapColumnValue = new HashMap<String,String>();	
		mapColumnValue.put("TRANSACTION_IND","S");
		mapColumnValue.put("SUSPEND_IND","L");
		mapColumnValue.put("GL_LINE_ITM_DIST_AMT","(CASE WHEN GL_EXTRACT_AMT IS NOT NULL AND GL_EXTRACT_AMT <> 0 THEN GL_EXTRACT_AMT ELSE GL_LINE_ITM_DIST_AMT END)");
		mapColumnValue.put("JURISDICTION_ID","NULL");
		mapColumnValue.put("JURISDICTION_TAXRATE_ID","NULL");
		mapColumnValue.put("MANUAL_JURISDICTION_IND","");
		//mapColumnValue.put("LOCATION_MATRIX_ID","NULL");
		mapColumnValue.put("TB_CALC_TAX_AMT","0"); 
		mapColumnValue.put("STATE_USE_AMOUNT","0");
		mapColumnValue.put("COUNTY_USE_AMOUNT","0");
		mapColumnValue.put("COUNTY_LOCAL_USE_AMOUNT","0");
		mapColumnValue.put("CITY_USE_AMOUNT","0");
		mapColumnValue.put("CITY_LOCAL_USE_AMOUNT","0");
		mapColumnValue.put("STATE_USE_RATE","0");
		mapColumnValue.put("COUNTY_USE_RATE","0");
		mapColumnValue.put("COUNTY_LOCAL_USE_RATE","0");
		mapColumnValue.put("COUNTY_SPLIT_AMOUNT","0");
		mapColumnValue.put("COUNTY_MAXTAX_AMOUNT","0");
		mapColumnValue.put("COUNTY_SINGLE_FLAG","");
		mapColumnValue.put("COUNTY_DEFAULT_FLAG","");
		mapColumnValue.put("CITY_USE_RATE","0");
		mapColumnValue.put("CITY_LOCAL_USE_RATE","0");
		mapColumnValue.put("CITY_SPLIT_AMOUNT","0");
		mapColumnValue.put("CITY_SPLIT_USE_RATE","0");
		mapColumnValue.put("CITY_SINGLE_FLAG","");
		mapColumnValue.put("CITY_DEFAULT_FLAG","");
		mapColumnValue.put("COMBINED_USE_RATE","0");
		//mapColumnValue.put("CCH_TAXCAT_CODE","");
		//mapColumnValue.put("CCH_GROUP_CODE","");
		//mapColumnValue.put("CCH_ITEM_CODE","");
		mapColumnValue.put("STATE_SPLIT_AMOUNT","0");
		mapColumnValue.put("STATE_MAXTAX_AMOUNT","0");
		mapColumnValue.put("GL_EXTRACT_FLAG","(CASE WHEN GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1' THEN NULL ELSE GL_EXTRACT_FLAG END)");
		mapColumnValue.put("GL_LOG_FLAG","(CASE WHEN GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1' THEN 1 ELSE GL_LOG_FLAG END)");

		StringBuffer columnAllBuf = new StringBuffer("");
		StringBuffer valueAllBuf = new StringBuffer("");		
		columnAllBuf.append("PURCHTRANS_ID,LOG_SOURCE,GL_EXTRACT_BATCH_NO,GL_EXTRACT_TIMESTAMP,B_AUTO_TRANS_COUNTRY_CODE,A_AUTO_TRANS_COUNTRY_CODE,");
		valueAllBuf.append("PURCHTRANS_ID,'suspendLocationMatrixTransactions()',NULL,NULL,AUTO_TRANSACTION_COUNTRY_CODE AS B_AUTO_TRANS_COUNTRY_CODE,AUTO_TRANSACTION_COUNTRY_CODE AS A_AUTO_TRANS_COUNTRY_CODE,");

		String asValue = "";
		for(int i=0; i<suspendColumn.length; i++){
			columnAllBuf.append("B_" + suspendColumn[i] + "," + "A_"+suspendColumn[i] + ",");
			asValue = mapColumnValue.get(suspendColumn[i]);
			if(asValue != null){ //"0 AS A_TB_CALC_TAX_AMT"
				if(asValue.equalsIgnoreCase("NULL") || asValue.equalsIgnoreCase("0") || asValue.contains("CASE")){
	    		}
	    		else{
	    			asValue = "'" + asValue + "'";
	    		}
				valueAllBuf.append(suspendColumn[i] + " AS " + "B_" + suspendColumn[i] + "," + asValue + " AS " + "A_" + suspendColumn[i] + ",");
			}
			else{ //"TB_CALC_TAX_AMT AS A_TB_CALC_TAX_AMT"
				valueAllBuf.append(suspendColumn[i] + " AS " + "B_" + suspendColumn[i] + "," + suspendColumn[i] + " AS " + "A_" + suspendColumn[i] + ",");
			}
    	}
		
		//use local user id & timestamp
		columnAllBuf.append("UPDATE_USER_ID,UPDATE_TIMESTAMP");
		valueAllBuf.append(":updateUserId,:updateTimestamp");

		//commented this. when click on "ok" btn after click on "update counts" fails
//		String exportSuspendSql = "insert into tb_purchtrans_LOG (" + columnAllBuf.toString() + ") select " + valueAllBuf.toString() + " from tb_purchtrans WHERE  " +
//				"(td.shiptoLocnMatrixId = :locationMatrixId) or (td.shipfromLocnMatrixId = :locationMatrixId) or (td.ordracptLocnMatrixId = :locationMatrixId) or " +
//				"(td.ordrorgnLocnMatrixId = :locationMatrixId) or (td.firstuseLocnMatrixId = :locationMatrixId) or (td.billtoLocnMatrixId = :locationMatrixId) or " +
//				"(td.ttlxfrLocnMatrixId = :locationMatrixId)";	
//		if (whereClause != null) {
//			query = session.createSQLQuery(exportSuspendSql + whereClause);
//			query.setParameter("locationMatrixId", locationMatrixId);	
//			query.setParameter("updateUserId", Auditable.currentUserCode()); 
//			query.setParameter("updateTimestamp", new Date(System.currentTimeMillis()));
//			
//			System.out.println(exportSuspendSql + whereClause);
//			
//			int rows = query.executeUpdate();
//			logger.debug(rows + " tb_purchtrans_LOG rows inserted");
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}   	   
//		System.out.println("************ exportSuspendSql: " + exportSuspendSql + whereClause);
//    	
//    	//0005977	
		if (pco != null && pco.equals("1")) {
			String saleSql = "SELECT PURCHTRANS_LOCN_ID FROM TB_PURCHTRANS_LOCN  where "
					+"(SHIPTO_LOCN_MATRIX_ID = :locationMatrixId) or (SHIPFROM_LOCN_MATRIX_ID = :locationMatrixId) or (ORDRACPT_LOCN_MATRIX_ID = :locationMatrixId) or " +
				"(ORDRORGN_LOCN_MATRIX_ID = :locationMatrixId) or (FIRSTUSE_LOCN_MATRIX_ID = :locationMatrixId) or (BILLTO_LOCN_MATRIX_ID = :locationMatrixId) or " +
				"(TTLXFR_LOCN_MATRIX_ID = :locationMatrixId)";
			Long transId = null;
			query = session.createSQLQuery(saleSql);
			query.setParameter("locationMatrixId", locationMatrixId);
			List<BigDecimal> returnList = query.list();
			for(int i=0; i< returnList.size(); i++){
				transId = ((BigDecimal)returnList.get(i)).longValue();
				purchaseTransactionService.suspendAndLog(new Long(transId), SuspendReason.SUSPEND_LOCN, locationMatrixId, suspendExtracted);
			}
		}
		
		/*
		String baseSql = 
			"UPDATE tb_transaction_detail SET " +
			"TRANSACTION_IND = 'S', " +
			"SUSPEND_IND = 'L', " +
			 "gl_line_itm_dist_amt = " + 
	            "CASE " +
	            "WHEN " +
	            "gl_extract_amt IS NOT NULL AND gl_extract_amt <> 0 " + 
	            "THEN " + 
	            "gl_extract_amt " +
	            "ELSE " + 
	            "gl_line_itm_dist_amt " +
	            "END, " + 
			"JURISDICTION_ID = NULL, " +
			"JURISDICTION_TAXRATE_ID = NULL, " +
			"MANUAL_JURISDICTION_IND = '', " +
			"LOCATION_MATRIX_ID = NULL, " +
			"TB_CALC_TAX_AMT = 0, " + 
			"STATE_USE_AMOUNT = 0, " +
			"COUNTY_USE_AMOUNT = 0, " +
			"COUNTY_LOCAL_USE_AMOUNT = 0, " +
			"CITY_USE_AMOUNT = 0, " +
			"CITY_LOCAL_USE_AMOUNT = 0, " +
			"STATE_USE_RATE = 0, " +
			"COUNTY_USE_RATE= 0, " +
			"COUNTY_LOCAL_USE_RATE = 0, " +
			"COUNTY_SPLIT_AMOUNT= 0, " +
			"COUNTY_MAXTAX_AMOUNT= 0, " +
			"COUNTY_SINGLE_FLAG= '', " +
			"COUNTY_DEFAULT_FLAG= '', " +
			"CITY_USE_RATE= 0, " +
			"CITY_LOCAL_USE_RATE= 0, " +
			"CITY_SPLIT_AMOUNT= 0, " +
			"CITY_SPLIT_USE_RATE= 0, " +
			"CITY_SINGLE_FLAG = '', " +
			"CITY_DEFAULT_FLAG = '', " +
			"COMBINED_USE_RATE = 0, " +
			"CCH_TAXCAT_CODE = '', " +
			"CCH_GROUP_CODE = '', " +
			"CCH_ITEM_CODE = '', " +
			"STATE_SPLIT_AMOUNT = 0, " +
			"STATE_MAXTAX_AMOUNT = 0 " + 
			"GL_EXTRACT_FLAG =  " +
			 	"CASE  " +
			 	"WHEN  " +
			  		"GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1'  " +
			  	"THEN  " +
			  		"NULL " +
			  	"ELSE  " +
			  		"GL_EXTRACT_FLAG  " +
			  	"END,  " +
		  	"GL_LOG_FLAG =  " +
			 	"CASE  " +
			 	"WHEN  " +
			  		"GL_EXTRACT_FLAG IS NOT NULL AND GL_EXTRACT_FLAG = '1'  " +
			  	"THEN  " +
			  		"1 " +
			    "ELSE  " +
			  		"GL_LOG_FLAG " +
			    "END " +
			"WHERE LOCATION_MATRIX_ID = :locationMatrixId ";
		*/
		
		//Update transactions
		StringBuffer  baseSqlBuf = new StringBuffer("");
		baseSqlBuf.append("UPDATE tb_purchtrans SET ");
		String setValue = "";
		int count = 0;
		for (String setColumn : mapColumnValue.keySet()) {
			count++;
    		setValue = mapColumnValue.get(setColumn);    	
    		
    		if(setValue.equalsIgnoreCase("NULL") || setValue.equalsIgnoreCase("0") || setValue.contains("CASE")){
    			baseSqlBuf.append(setColumn + " = " + setValue);
    		}
    		else{
    			baseSqlBuf.append(setColumn + " = '" + setValue + "'");
    		}
    		
    		if(count < mapColumnValue.size()){
    			baseSqlBuf.append(", ");
    		}
    		else{
    			baseSqlBuf.append(" ");
    		}
    	}
		baseSqlBuf.append("  where " + 
				"(shiptoLocnMatrixId = :locationMatrixId) or (shipfromLocnMatrixId = :locationMatrixId) or (ordracptLocnMatrixId = :locationMatrixId) or " +
				"(ordrorgnLocnMatrixId = :locationMatrixId) or (irstuseLocnMatrixId = :locationMatrixId) or (billtoLocnMatrixId = :locationMatrixId) or " +
				"(ttlxfrLocnMatrixId = :locationMatrixId)");
					
		String baseSql = baseSqlBuf.toString();
				
		//commented this. when click on "ok" btn after click on "update counts" fails
//		if (whereClause != null) {
//			query = session.createSQLQuery(baseSql + whereClause);
//			query.setParameter("locationMatrixId", locationMatrixId);
//			int rows = 0;
//			try {
//				rows = query.executeUpdate();
//			} catch (HibernateException e) {
//				e.printStackTrace();
//			}
//			session.flush();
//			logger.debug(rows + " transaction detail rows suspended");
//			
//		} else {
//			logger.debug("Nothing to suspend - check input flags!");
//		}
//		
		if (deleteMatrixLine) {
			query = session.createSQLQuery(
					"delete from TB_LOCATION_MATRIX where LOCATION_MATRIX_ID = :locationMatrixId");
			query.setParameter("locationMatrixId", locationMatrixId);
			int rows= 0;
			try {
				rows = query.executeUpdate();
			} catch (HibernateException e) {
				e.printStackTrace();
			}
			logger.debug(rows + " matrix rows deleted");
		} else {
			logger.debug("Matrix not deleted");
		}
		
		// All done
		t.commit();
		
		//PP-183 end 
		
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional
	public void suspendTransaction(PurchaseTransactionService purchaseTransactionService, Long id, SuspendReason suspendReason) {
		purchaseTransactionService.suspendAndLog(id, suspendReason, null, true);
	}
	
	public void acceptHeldTransaction(Long id) {
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	
    	// Query needs to come from session thats in a transaction
		org.hibernate.Query query = session.createSQLQuery("update tb_purchtrans " +
				"set TRANSACTION_IND = 'P' where PURCHTRANS_ID = " + id.toString());
		int rows = query.executeUpdate();
		logger.debug(rows + " transactions updated");
	
		t.commit();
		
		closeLocalSession(session);
		session=null;
	}
	public void updateaActiveFlagMatrixInUse(Long taxMatrixId){
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	
    	org.hibernate.Query query = session.createSQLQuery("update tb_tax_matrix " +
				"set active_flag=:activeFlag, UPDATE_USER_ID=:updateUserId, UPDATE_TIMESTAMP=:updateTimestamp where tax_matrix_id = :taxMatrixId ");
    	
		query.setParameter("activeFlag", "0");		
		query.setParameter("updateUserId", Auditable.currentUserCode()); 
		query.setParameter("updateTimestamp", new Date(System.currentTimeMillis()));
		query.setParameter("taxMatrixId", taxMatrixId);

		int rows = query.executeUpdate();
		logger.debug(rows + " transactions updated");
	
		t.commit();
		
		closeLocalSession(session);
		session=null;
	}
	
	public void appendComment(Long id, String comment) {
		// Convenience interface to package single id in list 
		List<Long> ids = new ArrayList<Long>();
		ids.add(id);
		appendComment(ids, comment);
	}
	
	public void appendComment(Collection<Long> ids, String comment) {
		// Update query - needs to be transactional!
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	
    	// Query needs to come from session thats in a tranasction
		org.hibernate.Query query = session.createSQLQuery("update TB_PURCHTRANS " +
				"set COMMENTS = case COMMENTS when NULL then '' else COMMENTS || '\n' end || :comments " +
				"where PURCHTRANS_ID in (:ids)");
		query.setParameter("comments", comment);
		query.setParameterList("ids", ids);
		int rows = query.executeUpdate();
		logger.debug(rows + " transactions updated");
		
		// All done
		t.commit();
		
		closeLocalSession(session);
		session=null;
	}
	
	// Method modified for 3943, in SQLQuery.java new query is added DATABASE_TRANSACTION_VIEWTEMP
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object> getTransactionDetailRecordsWithDynamicWhere(String whereClause) throws DataAccessException {
		logger.debug("Inside JPA ");
		List<Object> list = null;
		String sqlQuery = SQLQuery.DATABASE_TRANSACTION_VIEWTEMP + whereClause +")";
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createNativeQuery(sqlQuery);
		//q.setFirstResult(0);
		q.setMaxResults(10000);
		list = q.getResultList();
		logger.debug("list size in JPA " + list.size());		
		return list;
	}
	
	@Transactional
	public void saveBcpData(BCPPurchaseTransaction instance) {
		entityManager.persist(instance);
	}
	
	@Transactional(readOnly=true)
	public Long getStatisticsCount(PurchaseTransaction exampleInstance, 
			String sqlWhere, List<String> nullFileds) {
		
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(PurchaseTransaction.class);
		
		if (exampleInstance != null) {
			Example example = Example.create(exampleInstance);
	
			// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
	        ryanExampleHandling(criteria, example, exampleInstance);
	        example.excludeProperty("processStatus");
	        
			criteria.add(example);
			
			//for drill down 
			for(String nullField: nullFileds)
				    criteria.add(Restrictions.isNull(nullField));
		}
	
		StringBuilder sql = new StringBuilder();
		sql.append("select PURCHTRANS_LOCN_ID from TB_PURCHTRANS_LOCN left outer join TB_JURISDICTION  ON TB_JURISDICTION.JURISDICTION_ID = TB_PURCHTRANS_LOCN.SHIPTO_JURISDICTION_ID");
		
		if ((sqlWhere != null) && !sqlWhere.equals("")) {
			//3464 
			//replace ( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) ) AND to avoid sql join duplication
			//StringBuffer wer = new StringBuffer(sqlWhere);
			//int begin  = wer.indexOf("( tb_transaction_detail.jurisdiction_id =");
			//int end  ="( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) ) AND".length()+begin;
			//wer = wer.replace(begin,end,"");
			//sql.append(wer.toString());
			sql.append(sqlWhere);
		}
		
		criteria.add(Restrictions.sqlRestriction("PURCHTRANS_ID in (" + sql.toString() + ")"));
		extendExampleCriteria(criteria, exampleInstance);
		
		criteria.setProjection(Projections.rowCount()); // We just want a row count
		
		Long returnLong = ((Number)criteria.list().get(0)).longValue();
		
		closeLocalSession(session);
		session=null;
		
        return returnLong;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<PurchaseTransaction> getAllStatisticsRecords(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults, List<String> nullFileds) {
		logger.info("Inside getAllStatisticsRecords");
		List<PurchaseTransaction> transactionList = null;

		//0004691
		Session session = createLocalSession();
		try {	
			//0004691
			Criteria criteria = session.createCriteria(PurchaseTransaction.class);
			if (exampleInstance != null) {
				Example example = Example.create(exampleInstance);
		
				// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
		        ryanExampleHandling(criteria, example, exampleInstance);
		        example.excludeProperty("processStatus");
		        
				criteria.add(example);

				//for drill down view on selecting null columns
				 for(String nullField: nullFileds)
					    criteria.add(Restrictions.isNull(nullField));
			}
			
			StringBuilder sql = new StringBuilder();
			sql.append("select PURCHTRANS_LOCN_ID from TB_PURCHTRANS_LOCN  left outer join TB_JURISDICTION ON TB_JURISDICTION.JURISDICTION_ID = TB_PURCHTRANS_LOCN.SHIPTO_JURISDICTION_ID ");
			
			if ((sqlWhere != null) && !sqlWhere.equals("")) {
				//3464
				//replace ( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) ) AND to avoid sql join duplication
				//StringBuffer wer = new StringBuffer(sqlWhere);
				//int begin  = wer.indexOf("( tb_transaction_detail.jurisdiction_id =");
				//int end  ="( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) ) AND".length()+begin;
				//wer = wer.replace(begin,end,"");
				//sql.append(wer.toString());
				sql.append(sqlWhere);
			}
			
			criteria.add(Restrictions.sqlRestriction("PURCHTRANS_ID in (" + sql.toString() + ")"));

			extendExampleCriteria(criteria, exampleInstance);
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
			transactionList = criteria.list();
			
			//0004691
			LogMemory.executeGC();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return transactionList;
	}
	
	public boolean isValidAdvancedFilter(String advancedFilter){
		
		String whereClauseKeys[] = {"ALTER", "CREATE", "DROP", "GRANT", "PURGE", "RENAME", "REVOKE", "TRUNCATE", "DELETE",
						   "INSERT", "LOCK", "MERGE", "UPDATE" };
	    List<String> whereClauseList = Arrays.asList(whereClauseKeys);
	    for(String invalidkey : whereClauseList){
	    	if(advancedFilter.toLowerCase().contains(invalidkey.toLowerCase())) {
	    		return false;
	    	}
	    }
//		String sql = "SELECT count(*) FROM tb_purchtrans INNER JOIN TB_TRANS_USER_PURCH ON tb_purchtrans.PURCHTRANS_ID = TB_TRANS_USER_PURCH.TRANS_USER_ID WHERE (" + advancedFilter + ")";
		String sql = "SELECT count(*) FROM tb_purchtrans JOIN tb_purchtrans_locn ON tb_purchtrans.purchtrans_id = tb_purchtrans_locn.PURCHTRANS_LOCN_ID "
				+ "JOIN tb_purchtrans_jurdtl ON tb_purchtrans.purchtrans_id = tb_purchtrans_jurdtl.PURCHTRANS_JURDTL_ID "
				+ "JOIN tb_purchtrans_user ON tb_purchtrans.purchtrans_id = tb_purchtrans_user.PURCHTRANS_USER_ID WHERE (" + advancedFilter + ")";
		try{
			Session session = createLocalSession();
			org.hibernate.Query query = session.createSQLQuery(sql);
			query.list();
			
			closeLocalSession(session);
			session=null;
			
			return true;
		}
		catch(HibernateException e){
			e.printStackTrace();
			return false;
		}
	}

	public void saveColumn(TransactionHeader transactionHeader) {
		try {
			Session session = createLocalSession();
			Transaction transaction = session.beginTransaction();
			transaction.begin();
			session.persist(transactionHeader);
			transaction.commit();
			session.flush();
			
			closeLocalSession(session);
			session=null;
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getColumnHeaders(String userCode, String columnType, String windowName, String dataWindowName) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userCode", userCode);
		params.put("allColumns", columnType);
		
		StringBuilder queryBuffer = new StringBuilder("SELECT COLUMN_NAME FROM TB_DW_COLUMN WHERE USER_CODE = UPPER(:userCode) AND ALL_COLUMNS = UPPER(:allColumns) AND ");
		if(StringUtils.hasText(windowName)) {
			queryBuffer.append("UPPER(WINDOW_NAME) = UPPER(:windowName) AND ");
			params.put("windowName", windowName);
		}
		else {
			queryBuffer.append("WINDOW_NAME is null AND");
		}
		if(StringUtils.hasText(dataWindowName)) {
			queryBuffer.append("UPPER(DATAWINDOW_NAME) = UPPER(:dataWindowName) ");
			params.put("dataWindowName", dataWindowName);
		}
		else {
			queryBuffer.append("DATAWINDOW_NAME is null ");
		}
		queryBuffer.append("ORDER BY COLUMN_ORDER ASC");
		
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(queryBuffer.toString());
		for(Map.Entry<String, String> e : params.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}
		
		List<String> returnString = query.list();
		
		closeLocalSession(session);
		session=null;

		return returnString;
	}

	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void removeColumns(String userCode, String columnType, String windowName, String dataWindowName) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userCode", userCode);
		params.put("allColumns", columnType);
		
		StringBuilder queryBuffer = new StringBuilder("delete from TransactionHeader t where t.userCode = UPPER(:userCode) AND t.allColumns = UPPER(:allColumns) AND ");
		if(StringUtils.hasText(windowName)) {
			queryBuffer.append("UPPER(t.windowName) = UPPER(:windowName) AND ");
			params.put("windowName", windowName);
		}
		else {
			queryBuffer.append("t.windowName is null AND");
		}
		if(StringUtils.hasText(dataWindowName)) {
			queryBuffer.append("UPPER(t.dataWindowName) = UPPER(:dataWindowName)");
			params.put("dataWindowName", dataWindowName);
		}
		else {
			queryBuffer.append("t.dataWindowName is null");
		}
		
		Session session = createLocalSession();
		org.hibernate.Query query = session.createQuery(queryBuffer.toString());
		for(Map.Entry<String, String> e : params.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}
		query.executeUpdate();

		closeLocalSession(session);
		session=null;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveColumns(String userCode, String columnType,
			String windowName, String dataWindowName,
			List<TransactionHeader> transactionHeaders) {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();			
			
			//delete existing
			Map<String, String> params = new HashMap<String, String>();
			params.put("userCode", userCode);
			params.put("allColumns", columnType);
			
			StringBuilder queryBuffer = new StringBuilder("delete from TransactionHeader t where t.userCode = UPPER(:userCode) AND t.allColumns = UPPER(:allColumns) AND ");
			if(StringUtils.hasText(windowName)) {
				queryBuffer.append("UPPER(t.windowName) = UPPER(:windowName) AND ");
				params.put("windowName", windowName);
			}
			else {
				queryBuffer.append("t.windowName is null AND");
			}
			if(StringUtils.hasText(dataWindowName)) {
				queryBuffer.append("UPPER(t.dataWindowName) = UPPER(:dataWindowName)");
				params.put("dataWindowName", dataWindowName);
			}
			else {
				queryBuffer.append("t.dataWindowName is null");
			}
			
			org.hibernate.Query query = session.createQuery(queryBuffer.toString());
			for(Map.Entry<String, String> e : params.entrySet()) {
				query.setParameter(e.getKey(), e.getValue());
			}
			query.executeUpdate();
			session.flush();
			
			//insert new
			queryBuffer = new StringBuilder("insert into TB_DW_COLUMN(WINDOW_NAME, DATAWINDOW_NAME, USER_CODE, COLUMN_ORDER, COLUMN_NAME, COLUMN_WIDTH, COLUMN_X, ALL_COLUMNS) ")
				.append("VALUES (:windowName, :dataWindowName, :userCode, :columnOrder, :columnName, :columnWidth, :columnX, :allCoumns)");
			query = session.createSQLQuery(queryBuffer.toString());
			if(transactionHeaders != null) {
				for (TransactionHeader transactionHeader : transactionHeaders) {
					query.setParameter("windowName", transactionHeader.getWindowName());
					query.setParameter("dataWindowName", transactionHeader.getDataWindowName());
					query.setParameter("userCode", transactionHeader.getUserCode());
					query.setParameter("columnOrder", transactionHeader.getColumnId());
					query.setParameter("columnName", transactionHeader.getColumnName());
					query.setParameter("columnWidth", transactionHeader.getColumnWidth());
					query.setParameter("columnX", transactionHeader.getColumnX());
					query.setParameter("allCoumns", transactionHeader.getAllColumns());
					query.executeUpdate();
				}
			}
			session.flush();
			session.clear();
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				logger.debug("Rolling back");
				logger.debug("Exception:"+e);
				try {
					tx.rollback();
				}
				catch(Exception ex) {
					logger.error(ex);
				}
			}
		} 
		
		closeLocalSession(session);
		session=null;		
	}
	
	@Override
	@Transactional(readOnly = false)
	public void saveSplits(PurchaseTransaction original,
			List<PurchaseTransaction> splits,
			List<Long> deletedIds,
			List<PurchaseTransaction> allocationSplits) {
		Long splitSubTransId = getNextSplitSubTransId();
		
		original.setSplitSubtransId(splitSubTransId);
		
		if(splits != null) {
			for (PurchaseTransaction transactionDetail : splits) {
				if(transactionDetail.getSplitSubtransId() == null) {
					transactionDetail.setSplitSubtransId(splitSubTransId);
				}
				fixValues(transactionDetail);
				
				if(transactionDetail.getPurchtransId() == null) {
					save(transactionDetail);
				}
				else {
					update(transactionDetail);
				}
			}
		}
		
		if(deletedIds != null) {
			for(Long id : deletedIds) {
				PurchaseTransaction t = findById(id);
				if(t != null && splitSubTransId.equals(t.getSplitSubtransId())) {
					remove(t);
				}
			}
		}
		
		if(allocationSplits != null) {
			for (PurchaseTransaction transactionDetail : allocationSplits) {
				if(transactionDetail.getSplitSubtransId() == null) {
					transactionDetail.setSplitSubtransId(splitSubTransId);
				}
				fixValues(transactionDetail);
				
				if(transactionDetail.getPurchtransId() == null) {
					save(transactionDetail);
				}
				else {
					update(transactionDetail);
				}
			}
		}
		
		fixValues(original);
		entityManager.merge(original);
		
		entityManager.flush();
	}
	
	@Transactional
	private long getNextSplitSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_SPLIT_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
	
	@Transactional(readOnly = false)
	public long getNextAllocSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_ALLOCATION_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
	
	@SuppressWarnings("deprecation")
	@Transactional(readOnly = false)
	public String callTransactionProcessProc() {
		Session session = createLocalSession();
	    CallableStatement st  = null;
	    
	    String errorCode = null;
	    try {
	    	st  = session.connection().prepareCall("{call SP_TRANSACTION_PROCESS(?)}");
	    	st.clearParameters();
	    	st.registerOutParameter (1, Types.VARCHAR);
	    	st.execute();
	    	errorCode = st.getString(1);
	    }
	    catch (HibernateException e) {
	    	e.printStackTrace();
	    }
	    catch (SQLException e) {
	    	e.printStackTrace();
	    }
	    
		return errorCode;
	}
	
	private void fixValues(PurchaseTransaction td) {
		if("S".equalsIgnoreCase(td.getMultiTransCode()) || "A".equalsIgnoreCase(td.getMultiTransCode())) {
			/*
			 * 	When splitting transactions in tb_purchtrans, all of the fields from tb_calc_tax_amt to gl_extract_amt need to be set to null.
				EXCEPT: transaction_state_code & auto_transaction_state_code.
				  - If auto_transaction_state_code = *NULL then set transaction_state_code = NULL.
				  - Else set transaction_state_code = auto_transaction_state_code where auto_transaction_state_code IS NOT NULL.
				  - Always set auto_transaction_state_code to NULL.
			 */
			td.setTbCalcTaxAmt(null);
			td.setStateTier1TaxAmt(null);
			td.setStateTier2TaxAmt(null);
			td.setStateTier3TaxAmt(null);
			td.setCountyTier1TaxAmt(null);
			td.setCountyLocalTaxAmt(null);
			td.setCityTier1TaxAmt(null);
			td.setCityLocalTaxAmt(null);
			td.setTransactionInd(null);
			td.setSuspendInd(null);
			td.setStateTier1Rate(null);
			td.setStateTier2Rate(null);
			td.setStateTier3Rate(null);
			td.setStateTier1MaxAmt(null);
			td.setStateTier2MinAmt(null);
			td.setStateTier2MaxAmt(null);
			td.setStateMaxtaxAmt(null);
			td.setCountyTier1Rate(null);
			td.setCountyTier2MinAmt(null);
			td.setCountyMaxtaxAmt(null);
			td.setCityTier1Rate(null);
			td.setCityTier2MinAmt(null);
			td.setCityTier2Rate(null);
			td.setCombinedRate(null);
			td.setGlExtractUpdater(null);
			td.setGlExtractTimestamp(null);
			td.setGlExtractFlag(null);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	@Transactional(readOnly = false)
	public String callRealTimeTransactionProcessProc(PurchaseTransaction trans, String calcFlag, String writeFlag) {

		Class<?> cls = trans.getClass();
		String [] fields = RT_TRANSACTION_PROCESS_RAWTYPE_FIELDS.split(",\\s*");
		
		StringBuffer rowTypeFields = new StringBuffer();
						
		for(String field: fields) 
			rowTypeFields.append("trans.").append(field.trim()).append(" := ?;\n");
							
		StringBuffer rtTransProcessSql = new StringBuffer();

		rtTransProcessSql.append("DECLARE\n");
		rtTransProcessSql.append("trans tb_tmp_transaction_detail%rowtype;\n");
		rtTransProcessSql.append("ac_calc_flag char;\n");
		rtTransProcessSql.append("ac_write_flag char;\n");
		rtTransProcessSql.append("ac_trans_id number;\n");
		rtTransProcessSql.append("ac_error_code varchar2(5000);\n");
		rtTransProcessSql.append("BEGIN\n");
		rtTransProcessSql.append("ac_calc_flag := ?;\n");             
		rtTransProcessSql.append("ac_write_flag := ?;\n");            		
		rtTransProcessSql.append(rowTypeFields);
		rtTransProcessSql.append("sp_realtime_wrapper(trans, ac_calc_flag, ac_write_flag, ac_trans_id, ac_error_code);\n");
		rtTransProcessSql.append("? := ac_trans_id;\n");
		rtTransProcessSql.append("? := ac_error_code;\n");
		rtTransProcessSql.append("END;\n");

		Session session = createLocalSession();
	    CallableStatement callableStmt  = null;
	    
	    String errorCode = null;
	    try {
	    	callableStmt  = session.connection().prepareCall(rtTransProcessSql.toString());
	    	callableStmt.clearParameters();
	    	callableStmt.setString(1, calcFlag);
	    	callableStmt.setString(2, writeFlag);
	    	int columnIndex = 3;
	    	for(String field: fields) {	
				Method getter = null;
				Object value = null;
				try {
					String getterName = "get"+DAOUtils.convSqlToBeanName(field);
					
					getter = cls.getMethod(getterName, null);
					value = getter.invoke(trans, null);										 
					DAOUtils.setStatementValue(callableStmt, value, columnIndex, getter.getReturnType(), field);					
					columnIndex++;
					
				} catch (IllegalArgumentException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				} 
	    		
	    	}

	    	callableStmt.registerOutParameter(columnIndex, Types.NUMERIC);
			callableStmt.registerOutParameter(columnIndex+1, Types.VARCHAR);
	    	callableStmt.execute();

			Long tmpTransId = callableStmt.getLong(columnIndex);
	    	errorCode = callableStmt.getString(columnIndex+1);

			TempTransactionDetail tempPt = (TempTransactionDetail)session.get(TempTransactionDetail.class, tmpTransId);

			try {
				BeanUtils.copyProperties(trans, tempPt);
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				logger.error(e1);
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				logger.error(e1);
			}
	    }
	    catch (HibernateException e) {
	    	logger.error(e.getMessage());
	    	e.printStackTrace();
	    }
	    catch (SQLException e) {
	    	logger.error(e.getMessage());
	    	e.printStackTrace();
	    }
		finally {
			closeLocalSession(session);
		}
		return errorCode;
	}
	
	
	
	public long getTransactionCountByTaxcodeDetail(Long taxcodeDetailId) throws DataAccessException {
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.or(Restrictions.or(Restrictions.eq("stateTaxcodeDetailId", taxcodeDetailId), Restrictions.eq("countyTaxcodeDetailId", taxcodeDetailId)), Restrictions.eq("cityTaxcodeDetailId", taxcodeDetailId)));
		criteria.setProjection(Projections.rowCount());
		List<?> count = criteria.list();
		return ((Number) count.get(0)).longValue();
	}

	@SuppressWarnings("unchecked")
	public List<PurchaseTransaction> getAllGLTransactions(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds)
			throws DataAccessException {
	List<PurchaseTransaction> transactionList = null;

		//0004691
		Session session = createLocalSession();
		try {	
			//0004691
			Criteria criteria = session.createCriteria(PurchaseTransaction.class);
			if (exampleInstance != null) {
				Example example = Example.create(exampleInstance);
		
				// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
		        ryanExampleHandling(criteria, example, exampleInstance);
		        example.excludeProperty("processStatus");
		        
				criteria.add(example);

				//for drill down view on selecting null columns
				 for(String nullField: nullFileds)
					    criteria.add(Restrictions.isNull(nullField));
			}
			
			StringBuilder sql = new StringBuilder();
			sql.append("select tb_purchtrans.PURCHTRANS_ID from tb_purchtrans_LOG join tb_purchtrans on tb_purchtrans.PURCHTRANS_ID = tb_purchtrans_LOG.PURCHTRANS_ID");
			
			if ((sqlWhere != null) && !sqlWhere.equals("")) {
				
				sql.append(" WHERE tb_purchtrans.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy') AND (tb_purchtrans.gl_extract_flag IS NULL OR tb_purchtrans.gl_extract_flag <> '1')");
			}
			
			System.out.println("sql:::"+sql);
			criteria.add(Restrictions.sqlRestriction("PURCHTRANS_ID in (" + sql.toString() + ")"));

			extendExampleCriteria(criteria, exampleInstance);
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
			transactionList = criteria.list();
			
			//0004691
			LogMemory.executeGC();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return transactionList;
	}
	
	
	
	@Transactional(readOnly=true)
	public Long getTransactionCount(PurchaseTransaction exampleInstance, 
			String sqlWhere, List<String> nullFileds) {
		
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(PurchaseTransaction.class);
		
		if (exampleInstance != null) {
			Example example = Example.create(exampleInstance);
	
			// SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
	        ryanExampleHandling(criteria, example, exampleInstance);
	        example.excludeProperty("processStatus");
	        
			criteria.add(example);
			
			//for drill down 
			for(String nullField: nullFileds)
				    criteria.add(Restrictions.isNull(nullField));
		}
	
		StringBuilder sql = new StringBuilder();
		
		sql.append("select TB_PURCHTRANS.PURCHTRANS_ID from tb_purchtrans_LOG join TB_PURCHTRANS on TB_PURCHTRANS.PURCHTRANS_ID = tb_purchtrans_LOG.PURCHTRANS_ID");
		
		if ((sqlWhere != null) && !sqlWhere.equals("")) {
			
			sql.append(" WHERE TB_PURCHTRANS.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy')AND (TB_PURCHTRANS.gl_extract_flag IS NULL OR TB_PURCHTRANS.gl_extract_flag <> '1')");
		}
		
		System.out.println(sql);
		criteria.add(Restrictions.sqlRestriction("PURCHTRANS_ID in (" + sql.toString() + ")"));
		extendExampleCriteria(criteria, exampleInstance);
		
		criteria.setProjection(Projections.rowCount()); // We just want a row count
		
		Long returnLong = ((Number)criteria.list().get(0)).longValue();
		
		closeLocalSession(session);
		session=null;
		
        return returnLong;
	}
	
	public void updateTransaction(PurchaseTransaction updatedTransaction) {
		
		try{
			Session session = createLocalSession();
	        Transaction t = session.beginTransaction();
	        t.begin();
	        session.update(updatedTransaction);
	        t.commit();
	        
	        //Prevent memory leak
	        closeLocalSession(session);
	        session=null;
		}catch(Exception e) {
			System.out.println("Exception occured while updating transaction"+e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	public static void main (String [] args) {
		new JPATransactionDetailDAO().callRealTimeTransactionProcessProc(null, null, null);
	}
}