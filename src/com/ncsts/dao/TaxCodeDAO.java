package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;

public interface TaxCodeDAO extends GenericDAO<TaxCode,TaxCodePK> {
	public List<TaxCode> findTaxCodeList(String taxcodeCode, String desc, String activeFlag) throws DataAccessException;
	public List<TaxCode> getTaxCodeList(TaxCode exampleInstance, int firstRow, int maxResults,OrderBy orderBy);
	public Long count(TaxCode exampleInstance);
}
