package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.RoleMenu;

public class JPARoleUserMenuDAO extends JpaDaoSupport implements RoleUserMenuDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<RoleMenu> findRoleMenuByRoleCode(String roleCode) throws DataAccessException{
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createQuery("select rm from RoleMenu rm  where rm.id.roleCode = ?");
		q.setParameter(1, roleCode);
		return q.getResultList();
	}
	
	@Transactional(propagation = Propagation.NEVER, readOnly = false) 
	public void saveOrUpdate (RoleMenu roleMenu) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.saveOrUpdate(roleMenu);
		t.commit();
		closeLocalSession(session);
		session=null;	
	}	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteRoleMenu(RoleMenu roleMenu) throws DataAccessException {
		try {
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			// Following will attach the object to the session
			session.update(roleMenu);
			session.delete(roleMenu);
			t.commit();
			closeLocalSession(session);
			session=null;
		} catch (DataAccessException e) {
			e.printStackTrace();
		}

	}

}
