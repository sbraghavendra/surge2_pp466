package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRefDoc;
import com.ncsts.domain.BatchMaintenance;

public interface BCPTaxCodeRefDocDAO extends GenericDAO<BCPTaxCodeRefDoc, Long> {
	public Long count(BatchMaintenance batch);
	public List<BCPTaxCodeRefDoc> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
