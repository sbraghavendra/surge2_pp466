package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import com.ncsts.domain.Matrix;
import com.ncsts.domain.MatrixDriver;

/**
 * @author vinays singh
 *
 */
public interface MatrixDAO<M extends Matrix> extends GenericDAO<M,Long> {

	public boolean matrixExists(M matrix, boolean checkDate);
	public List<MatrixDriver> getUniqueDrivers(M filter);
	public Date minEffectiveDate(M matrix);
	public Long matrixEffectiveDateCount(M matrix);
}
