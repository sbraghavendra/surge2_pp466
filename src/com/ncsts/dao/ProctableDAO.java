package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;

public interface ProctableDAO extends GenericDAO<Proctable,Long> {

	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<Proctable> find(Criteria criteria);
	public abstract List<Proctable> find(DetachedCriteria criteria);
	public abstract Proctable find(Proctable exampleInstance);
	public abstract List<Proctable> find(Proctable exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public Long count(Proctable exampleInstance);

	public void addProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException;
	public void updateProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException;
	public void deleteProctableDetail(ProctableDetail aProctableDetail) throws DataAccessException;
	public void deleteProctable(Proctable updateProctable) throws DataAccessException;
	public List<ProctableDetail> findProctableDetail(Long proctableId) throws DataAccessException;
	public ProctableDetail findProctableDetailById(Long proctableId) throws DataAccessException;
	public List<Proctable> getAllProctable() throws DataAccessException;
	public void reorderProctableList(List<Proctable> updateProctableList) throws DataAccessException;
	
	public void populateProcruleDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableDetailList) throws DataAccessException;
	public void updateProctableDetail(Proctable aProctable, ArrayList changedArrayList) throws DataAccessException;
	public void addProctableDetail(Proctable updateProctable, ProctableDetail aProctableDetail) throws DataAccessException;
}
