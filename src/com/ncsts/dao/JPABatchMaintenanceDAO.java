/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.SequenceManager;
import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.License;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.view.util.BCPTransaction;
import com.ncsts.view.util.BatchTypeSpecific;
import com.ncsts.view.util.HibernateUtils;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;


/**
 * @author Muneer
 *
 */

public class JPABatchMaintenanceDAO extends JPAGenericDAO<BatchMaintenance, Long> implements 
		BatchMaintenanceDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	@SuppressWarnings("unused")
	private BatchMaintenanceDAO batchMaintenanceDAO;
	
	public static final String BILLING_TAX_TYPE = "Billing/Sales";
	public static final String PURCHASING_TAX_TYPE = "Purchasing/Use";
	public static final String ZERO = "0";
	public static final String ONE = "1";
	public static final String UTS = "uts";

	@SuppressWarnings("unchecked")
	@Transactional
	public List<BatchMaintenance> getAllBatchMaintenanceData()
	throws DataAccessException {
		// TODO Auto-generated method stub
		return getJpaTemplate().find("select bm from BatchMaintenance bm order by bm.batchId");
	}
	
	@Transactional
	public BatchMaintenance findByBatchId(Long batchId) throws DataAccessException{
		return getJpaTemplate().find(BatchMaintenance.class, batchId);
	}	
	
	public Session getCurrentSession(){
		return createLocalSession();
		
	}

/*	public List<BatchMaintenance> findByBatchMaintenance(BatchMaintenance batchMaintenance, String... excludeProperty) 
    throws DataAccessException {
	
		return getJpaTemplate().find("select bm from BatchMaintenance bm order by bm.batchId");
	}*/
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)

	public List<BatchMaintenance> findByBatchMaintenance(BatchMaintenance batchMaintenance) 

    throws DataAccessException {
		List<BatchMaintenance> list = null;
		
		logger.debug(" BatchManitenance Search from DAO ::::");
		logger.info("Entry stamp time "+ batchMaintenance.getEntryTimestamp());
		logger.info("Batch status code "+ batchMaintenance.getBatchStatusCode());
		logger.info("Batch type code "+ batchMaintenance.getBatchTypeCode());
		
				
		
		if(batchMaintenance.getBatchId() == null){
			
			logger.debug(" Search inside batchMaintenance.getBatchId() == null from DAO1 ::::");
		
			Session localSession = createLocalSession();
            try {
            	Criteria criteria = localSession.createCriteria(BatchMaintenance.class) ;
            	if (batchMaintenance.getEntryTimestamp() != null) {
                    criteria.add(Expression.ge("entryTimestamp",
                                               batchMaintenance.getEntryTimestamp()));
                }                
                if (batchMaintenance.getBatchTypeCode() != null) {
                    criteria.add(Expression.eq("batchTypeCode",
                                           batchMaintenance.getBatchTypeCode()));
                }                
                if (batchMaintenance.getBatchStatusCode() != null) {
                      criteria.add(Expression.eq("batchStatusCode",batchMaintenance.getBatchStatusCode()));
                }                
                criteria.addOrder(Order.desc("batchId"));
   
                    list = criteria.list();
                }
                catch (HibernateException hbme) {
                    //String message = "Error in finding the entities by example";
            }
            
            closeLocalSession(localSession);
            localSession=null;
				
            LogMemory.executeGC();  
		}else{
			list = new ArrayList<BatchMaintenance>();
			list.add(findByBatchId(batchMaintenance.getBatchId()));
			
		}
        if( list != null)logger.debug(" Batch list size is :"+list.size());        
		return list; 		
	}
	
	public List<BatchMaintenance> findByStatusUpdateUser(String batchTypeCode, String batchStatusCode, String statusUpdateUserId) throws DataAccessException {
		List<BatchMaintenance> list = new ArrayList<BatchMaintenance>();
		
		if(statusUpdateUserId!=null && statusUpdateUserId.length()>0){
			Session localSession = createLocalSession();
            try {
            	Criteria criteria = localSession.createCriteria(BatchMaintenance.class) ;
       
                criteria.add(Expression.eq("statusUpdateUserId", statusUpdateUserId));
      
                if (batchTypeCode!=null && batchTypeCode.length()>0) {
                    criteria.add(Expression.eq("batchTypeCode", batchTypeCode));
                } 
                if (batchStatusCode!=null && batchStatusCode.length()>0) {
                    criteria.add(Expression.eq("batchStatusCode", batchStatusCode));
                }      
                criteria.addOrder(Order.desc("batchId"));  
                list = criteria.list();
            }catch (HibernateException hbme) {
                    //String message = "Error in finding the entities by example";
            }
            
            closeLocalSession(localSession);
            localSession=null;
		}
   
		return list; 		
	}	
	
	@SuppressWarnings("unchecked")
	public List<BatchMaintenance> findFlaggedBatches(String batchTypeCode,
			String batchStatusCode) throws DataAccessException {
		
		List<BatchMaintenance> list = null;
		Session localSession = createLocalSession();

		try {
			Date now = new Date();
			
			Criteria criteria = localSession
					.createCriteria(BatchMaintenance.class);
			
			criteria.add(Expression.eq("batchStatusCode", batchStatusCode));
			criteria.add(Expression.eq("batchTypeCode", batchTypeCode));
			criteria.add(Restrictions.or(Expression.isNull("heldFlag"), Expression.ne("heldFlag", "1")));
			criteria.add(Restrictions.or(Expression.isNull("schedStartTimestamp"), Expression.le("schedStartTimestamp", now)));
			
			criteria.addOrder(Order.desc("batchId"));

			list = criteria.list();
		} catch (HibernateException hbme) {
			System.out.println(hbme.getStackTrace());
			hbme.printStackTrace();
		}

		closeLocalSession(localSession);
		localSession = null;
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<BatchMaintenance> findProcessedBatchesNotNotified() throws DataAccessException {
		
		List<BatchMaintenance> list = null;
		Session localSession = createLocalSession();
		try {
			Criteria criteria = localSession.createCriteria(BatchMaintenance.class);
			criteria.add(Expression.eq("batchStatusCode", "P"));
			criteria.add(Restrictions.or(Expression.eq("batchTypeCode", "PCO"), Expression.eq("batchTypeCode", "IP")));
			
			criteria.add(Restrictions.or(Expression.isNull("emailSentFlag"), Expression.eq("emailSentFlag", "0")));
			//criteria.add(Expression.isNull("ts10"));		
			criteria.addOrder(Order.asc("batchId"));

			list = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
		}

		closeLocalSession(localSession);
		localSession = null;
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BatchMaintenance> getBatchMaintenanceByBatchTypeCode(String batchTypeCode) throws DataAccessException {
		return getJpaTemplate().find("select bm from BatchMaintenance bm where bm.batchTypeCode = ?1 ", batchTypeCode);
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BatchMaintenance> getBatchMaintenanceByBatchStatusCode(String batchStatusCode) throws DataAccessException {
		return getJpaTemplate().find("select bm from BatchMaintenance bm where bm.batchStatusCode = ?1 ", batchStatusCode);
	}	
	/**
	 * @author Anand
	 * repeated method - to get the output order by batchId desc 
	 * @return
	 * @throws DataAccessException
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BatchMaintenance> getAllBatchesByBatchTypeCode(String batchTypeCode) throws DataAccessException {
		// TODO Auto-generated method stub
		return getJpaTemplate().find("select bm from BatchMaintenance bm where bm.batchTypeCode = ?1 order by batchId desc",batchTypeCode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<BatchMaintenance> findFlaggedProcessTaxRateBatches() throws DataAccessException {
		return getJpaTemplate().find("select bm from BatchMaintenance bm where bm.batchTypeCode = 'TR' and bm.batchStatusCode = 'FP' order by bm.ts01 asc, bm.nu01 asc");
	}

	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(BatchMaintenance batchMaintenance) throws DataAccessException {
		//getJpaTemplate().merge(locationMatrix);
		List<String> find = listCodeCheck("BATCHSTAT");
		List<String> findErr = listCodeCheck("ERRORSEV");
			
		if((	find.contains(batchMaintenance.getBatchStatusCode()) 
				&& ( (batchMaintenance.getErrorSevCode()==null)||( batchMaintenance.getErrorSevCode().length()==1)) )
				||  ( (batchMaintenance.getErrorSevCode()!=null)&&( batchMaintenance.getErrorSevCode().length()==0))
				|| findErr.contains(batchMaintenance.getErrorSevCode())){
			
			Session session = createLocalSession();
            Transaction t = session.beginTransaction();
            t.begin();
            session.update(batchMaintenance);
            t.commit();
            
            //Prevent memory leak
            closeLocalSession(session);
            session=null;
		}
		else{
			logger.info("Batch Status Code/Error Seviority Code not in the tb_listCode");
		}
	}
	
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(long batchId)
			throws DataAccessException {
		BatchMaintenance bm = findByBatchId(batchId);
		if(bm!=null){
		getJpaTemplate().remove(bm);
		}else{
			logger.info("Batch Maintenance Object not found for deletion");
		}
	}
	
	/**
	 * @author Anand
	 */
	@Transactional
	public BatchMaintenance saveBatch(BatchMaintenance batchMaintenance)
			throws DataAccessException {
		batchMaintenance.setBatchId(null); //new record
		getJpaTemplate().persist(batchMaintenance);
		getJpaTemplate().flush();
		return batchMaintenance;
	}
	
	/**
	 * Wrap all activity into single transaction when importing a tax rate update
	 * 1) Create the batch
	 * 2) Create all the BCPJurisdiction records
	 * 2.5) Create all BCPJurisdictionText records (the text file parsed line at a time)
	 * 3) Create any ErrorLog records
	 */
	
	// METHOD MODIFIED FOR BUG# - 3914
	@Transactional
	public BatchMaintenance importTaxRateUpdates(BatchMaintenance batchMaintenance,
			TaxRateUploadParser parser) throws DataAccessException {
		Session session = createLocalSession();
		session.setCacheMode(CacheMode.IGNORE); 
		batchMaintenance.setBatchId(null); // new record
		
		getJpaTemplate().persist(batchMaintenance);
		
		Iterator<BCPJurisdictionTaxRate> truIter = parser.iterator();
		int i = 1;
		while (truIter.hasNext()) {
			BCPJurisdictionTaxRate tr = truIter.next();
			
			String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "sq_tb_bcp_juris_taxrate_id");
			
			Query q = entityManager.createNativeQuery(sql);
			//4947
			if (tr != null && tr.getLine()!=null) {
				BCPJurisdictionTaxRateText txt = new BCPJurisdictionTaxRateText(batchMaintenance.getBatchId(), tr.getLine(), tr.getText());
				BigDecimal id = (BigDecimal)q.getSingleResult();
				logger.info("ID "+id.longValue());
				tr.setBcpJurisdictionTaxRateId(id.longValue());
				tr.setBatchId(batchMaintenance.getbatchId());
				getJpaTemplate().persist(tr);
				getJpaTemplate().persist(txt);
				// TODO: I wish I knew how to get Hibernate config settings at runtime
				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + i);				
					getJpaTemplate().flush();
					session.flush();
					session.clear();
					logger.debug("End Flush");
					parser.setLinesWritten(i);
				}
				tr = null;
				txt = null;
			}
		}
		
		getJpaTemplate().flush();	
		
		//Prevent memory leak
		session.flush();
		session.clear();

		// perform the checksum and generate any more ErrorLogs
		parser.checksum();
				
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			error.setBatchErrorLogId(null);
			error.setProcessType("TR");
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			getJpaTemplate().persist(error);
		}
		getJpaTemplate().flush();
		parser.setLinesWritten(i);
		
		//Prevent memory leak
		closeLocalSession(session);
		session=null;

		return batchMaintenance;
	}
	
	/**
	 * Wrap all activity into single transaction when importing a map updates
	 * 1) Create the batch
	 * 2) Create all the BCPTransactionDetail records
	 * 3) Create any ErrorLog records
	 */
	@Transactional
	public BatchMaintenance importMapUpdates(BatchMaintenance batchMaintenance,
			ImportMapUploadParser parser) throws DataAccessException {

		Session session = createLocalSession();
		session.setCacheMode(CacheMode.IGNORE); 
		Iterator<BCPTransaction> truIter = parser.iterator();

		int i = 1;
		while (truIter.hasNext()) {
			BCPTransaction bcpTransaction = truIter.next();
			if(bcpTransaction==null){
				continue;
			}	
				
			BCPPurchaseTransaction td = bcpTransaction.getBcpPurchaseTransaction();
			if (td != null) {
				td.setProcessBatchNo(batchMaintenance.getbatchId());
				getJpaTemplate().persist(td);

				// TODO: I wish I knew how to get Hibernate config settings at runtime
				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + i);
			
					getJpaTemplate().flush();
					session.flush();
					session.clear();
					logger.debug("End Flush");
					parser.setLinesWritten(i-1);
				}
				td = null;
			}
		}
		getJpaTemplate().flush();
		
		//Prevent memory leak
		session.flush();
		session.clear();

		// perform the checksum and generate any more ErrorLogs
		parser.checksum();
		parser.setLinesWritten(i-1);
				
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			error.setBatchErrorLogId(null);
			error.setProcessType("IP");
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			getJpaTemplate().persist(error);
		}
		getJpaTemplate().flush();
		
		closeLocalSession(session);
		session=null;

		return batchMaintenance;
	}

	@Transactional
	public BatchErrorLog save(BatchErrorLog error) throws DataAccessException {
		getJpaTemplate().persist(error);
		getJpaTemplate().flush();
		return error;
	}

	/**
	 * @author Anand
	 * @throws DataAccessException
	 *
	 * I believe this is now obsolete, but leaving here for now. JMW
	 */

	public String doGLExtract(String date, String fileName, String exeMode,
			String returnCode) throws DataAccessException {
		// TODO Auto-generated method stub
		//Query q = entityManager.createNamedQuery(arg0)
		return null;
	}
	
	
	@Transactional(readOnly=true)
	public List<?> getErrorLog(String sqlQuery , Long batchId)
			throws DataAccessException {
		logger.debug("Before calling query=================================");
		Query q = entityManager.createNativeQuery(sqlQuery);
		q.setParameter(1,batchId);
		return q.getResultList();
	}


	@Transactional
	public BatchMaintenance getBatchMaintananceData(long batchId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return getJpaTemplate().find(BatchMaintenance.class, batchId);
	}
	
    @SuppressWarnings("unchecked")
	public List<BatchMaintenance> getAllBatchesByBatchTypeCode(String codeTypeCode,
    	List<String> batchtypeCodeCode, List<String> batchstatusCodeCode, List<String> errorsevCodeCode) {
    	List<BatchMaintenance> list = new ArrayList<BatchMaintenance>();
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	Session session = createLocalSession();
    		
		try {
			Criteria criteria = session.createCriteria(BatchMaintenance.class);
			criteria.add(Restrictions.eq("batchTypeCode", codeTypeCode));
			if ((batchtypeCodeCode != null) && (batchtypeCodeCode.size() > 0)) {
				logger.debug("batchtypeCodeCode: " + batchtypeCodeCode);
				criteria.add(Restrictions.in("batchTypeCode", batchtypeCodeCode));
			}
			if ((batchstatusCodeCode != null) && (batchstatusCodeCode.size() > 0)) {
				logger.debug("batchstatusCodeCode: " + batchstatusCodeCode);
				criteria.add(Restrictions.or(Restrictions.in("batchStatusCode", batchstatusCodeCode), Restrictions.isNull("batchStatusCode")));
			}
			if ((errorsevCodeCode != null) && (errorsevCodeCode.size() > 0)) {
				logger.debug("errorsevCodeCode: " + errorsevCodeCode);
				criteria.add(Restrictions.or(Restrictions.in("errorSevCode", errorsevCodeCode), Restrictions.isNull("errorSevCode")));
			}
            criteria.addOrder(Order.asc("batchId"));
            
            list = criteria.list();
		} catch (HibernateException e) {
            logger.error("Error in finding the entities by example");
        	e.printStackTrace();
		}
		
		closeLocalSession(session);
		session=null;
		
		return list;
    }

    public BatchMetadata findBatchMetadataById(String id) throws DataAccessException {
    	BatchMetadata batchMetadata = getJpaTemplate().find(BatchMetadata.class, id);
    	if (batchMetadata == null) {
    		batchMetadata = new BatchMetadata();
    	}
    	return batchMetadata;
    }
    
    @Override
    public void incProcessCount() {
    	Session session = createLocalSession();
    	
		long count = SequenceManager.getNextVal("sq_tb_process_count",entityManager);
		logger.info("sq_tb_process_count inc to : " + count);
		closeLocalSession(session);
    }

	@Override
	public void saveBatchMetadata(BatchMetadata batchMetadata) {
		Session localSession = createLocalSession();
		try {
			if (batchMetadata != null) {
				if (batchMetadata.getBatchTypeCode() != null) {
					BatchMetadata existing = (BatchMetadata) localSession.get(BatchMetadata.class, batchMetadata.getBatchTypeCode());
					if (existing == null) {
						localSession.save(batchMetadata);
					} else {
						localSession.merge(batchMetadata);
					}
					localSession.flush();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
		}


	}

	@SuppressWarnings("unchecked")
    public List<Object[]> getBatchProcesses(Long batchId) {
    	List<Object[]> result = (List<Object[]>)
    	getJpaTemplate().find("select distinct processTimestamp, processType, batchId" +
    			" from BatchErrorLog e where e.batchId = ?1 order by processTimestamp desc", batchId);
    	return result;
    }
    @SuppressWarnings("unchecked")
	public List<BatchErrorLog> getBatchErrors(BatchErrorLog b) {
    	List<BatchErrorLog> result = (List<BatchErrorLog>)
    	getJpaTemplate().find("select e from BatchErrorLog e where e.batchId = ?1 and e.processType=?2 and e.processTimestamp=?3",
    			b.getBatchId(), b.getProcessType(), b.getProcessTimestamp());
    	return result;
    }
   	
    public Long count(BatchMaintenance exampleInstance) {
		Session localSession = this.createLocalSession();
		
		Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
		if(exampleInstance.getSearchBatchTypeSpecificList() != null  && exampleInstance.getSearchBatchTypeSpecificList().size() > 0 ) {
			criteria = generateBatchSpecificsCriteria(exampleInstance, criteria);
		}
		criteria.setProjection(Projections.rowCount()); 
		List<?> result = criteria.list();
		Long returnLong = ((Number)result.get(0)).longValue();
		
		closeLocalSession(localSession);
		localSession=null;
		
		return returnLong;
	}

	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	@SuppressWarnings("unchecked")
	public List<BatchMaintenance> find(Criteria criteria) {
		return criteria.list();
	}
	public List<BatchMaintenance> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<BatchMaintenance> returnBatchMaintenance = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnBatchMaintenance;
	}
	

	public BatchMaintenance find(BatchMaintenance exampleInstance) {
		List<BatchMaintenance> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<BatchMaintenance> find(BatchMaintenance exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults) {
		logger.info("Entering the gatAllRecords");
		List<BatchMaintenance> list = new ArrayList<BatchMaintenance>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			if(exampleInstance.getSearchBatchTypeSpecificList() != null  && exampleInstance.getSearchBatchTypeSpecificList().size() > 0 ) {
				criteria = generateBatchSpecificsCriteria(exampleInstance, criteria);
			}
			
			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
		}
		return new ArrayList<BatchMaintenance>();
	}
	
	private Criteria generateCriteriaBatch(BatchMaintenance exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(BatchMaintenance.class);
		try {
		if(exampleInstance.getBatchId() != null && exampleInstance.getBatchId().longValue() > 0) {
			criteria.add(Restrictions.eq("batchId", exampleInstance.getBatchId()));
		}
		
		if (exampleInstance.getEntryTimestamp() != null) { 
            criteria.add(Restrictions.ge("entryTimestamp", exampleInstance.getEntryTimestamp()));
        }                
	    if (exampleInstance.getBatchTypeCode() != null)
	    {
	      this.logger.info("BATCH TYPE " + exampleInstance.getBatchTypeCode());
	      
	      if ((exampleInstance.getBatchTypeCode() != null) && (exampleInstance.getBatchTypeCode().equalsIgnoreCase("IP") 
	    		  && exampleInstance.isAttachPcoToCriteria())) {
	        criteria.add(Restrictions.or(Restrictions.eq("batchTypeCode", "IP"), Restrictions.eq("batchTypeCode", "PCO")));
	      } else if ((exampleInstance.getBatchTypeCode() != null) && (exampleInstance.getBatchTypeCode().equalsIgnoreCase("IPS") 
	    		  && exampleInstance.isAttachPcoToCriteria())) {
	        criteria.add(Restrictions.or(Restrictions.eq("batchTypeCode", "IPS"), Restrictions.eq("batchTypeCode", "PCO")));
	      } 
	      else if((exampleInstance.getBatchTypeCode() != null) && (exampleInstance.getBatchTypeCode().equalsIgnoreCase("PCO"))) {
	    	  if(BILLING_TAX_TYPE.equals(exampleInstance.getLastModuelId())) {
	    		  criteria.add(Restrictions.in("batchTypeCode", new String[] {"PCO","IPS"}));
	    	  }
	    	  else {
	    		  criteria.add(Restrictions.in("batchTypeCode", new String[] {"PCO","IP"}));
	    	  }
	      }
	      
	      else {
	        criteria.add(Restrictions.eq("batchTypeCode", exampleInstance.getBatchTypeCode()));
	      }
	      
	    }
	    if (exampleInstance.getBatchStatusCode() != null)
	    {
	      this.logger.info("BATCH TYPE " + exampleInstance.getBatchStatusCode());
	      criteria.add(Restrictions.eq("batchStatusCode", exampleInstance.getBatchStatusCode()));
	    }
	    if (exampleInstance.getErrorSevCode() != null)
	    {
	      this.logger.info("Error Sev Code " + exampleInstance.getErrorSevCode());
	      criteria.add(Restrictions.eq("errorSevCode", exampleInstance.getErrorSevCode()));
	    }
	    if (ZERO.equals(exampleInstance.getHeldFlag()))
	    {	
	    	criteria.add(Restrictions.or(Expression.isNull("heldFlag"), Expression.ne("heldFlag", ONE)));
	    }
	    if (ONE.equals(exampleInstance.getHeldFlag()))
	    {	
	    	criteria.add(Restrictions.eq("heldFlag", exampleInstance.getHeldFlag()));
	    }
	    
	    if (exampleInstance.getYearPeriod() != null) {
	    	criteria.add(Restrictions.eq("yearPeriod", exampleInstance.getYearPeriod()));
	    }
	    
	    if (exampleInstance.isCalledFromConfigImportExpMenu() && exampleInstance.getVc01() != null)
	    {
	      this.logger.info("File type code " + exampleInstance.getVc01());
	      criteria.add(Restrictions.eq("vc01", exampleInstance.getVc01()));
	    }
	    
		} catch (Exception e) {
			e.printStackTrace();
		}

        return criteria;
	}
	
	private Criteria generateBatchSpecificsCriteria(BatchMaintenance exampleInstance, Criteria criteria) {
		List<BatchTypeSpecific> searchBatchTypeSpecificList = exampleInstance.getSearchBatchTypeSpecificList();
	    try{
	    	for(BatchTypeSpecific batchTypeSpecific : searchBatchTypeSpecificList) {
		    	prepareInputSearchCriteria(batchTypeSpecific, exampleInstance, criteria);
		    }
	    }catch(Exception e) {
				System.out.println("Exception occured in class JPABatchMaintenance in method generateCriteriaBatch() :   "+e.getMessage());
			}
		return criteria;
	}
	
	private Criteria prepareInputSearchCriteria(BatchTypeSpecific batchTypeSpecific, BatchMaintenance exampleInstance, Criteria criteria){
		String batchFieldType = batchTypeSpecific.getBatchFieldtype();
		String batchFieldName = batchTypeSpecific.getBatchFieldname();
    	if("String".equals(batchFieldType)) {
    		String value = (String)Util.getProperty(exampleInstance, batchFieldName);
    		criteria = generateInputSearchCriteria(batchFieldName, value,  criteria);
    	}
    	if("Date".equals(batchFieldType)) {
    		Date value = (Date)Util.getProperty(exampleInstance, batchFieldName);
    		criteria =  generateInputSearchCriteria(batchFieldName, value,  criteria);
    	}
    	if("Double".equals(batchFieldType)) {
    		Double value = (Double)Util.getProperty(exampleInstance, batchFieldName);
    		criteria = generateInputSearchCriteria(batchFieldName, value,  criteria);
    	}
    	if("BigDecimal".equals(batchFieldType)) {
    		BigDecimal value = (BigDecimal)Util.getProperty(exampleInstance, batchFieldName);
    		criteria = generateInputSearchCriteria(batchFieldName, value,  criteria);
    	}
		return criteria;
	}
	
	private Criteria generateInputSearchCriteria(String batchFieldName, Object inputValue, Criteria criteria)  {
		if(inputValue instanceof String){
			String batchFieldValue = (String)inputValue;
			criteria  = Util.generateStringwildCardCriteria(batchFieldName, batchFieldValue,  criteria);
	 	 }
		else {
			batchFieldName = Util.lowerCaseFirst(batchFieldName);
			if(inputValue instanceof Date){
				Date value = (Date)inputValue;
				if(batchFieldName.startsWith(UTS)) {
					HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
					if (session.getAttribute("timezone") != null) {
					    try{
					    	Date endDate = DateUtils.addHours(value, 24);
					    	criteria.add(Restrictions.ge(batchFieldName, value)); 
					    	criteria.add(Restrictions.lt(batchFieldName, endDate));
						    
					    }catch(Exception e) {
					    	e.printStackTrace();
					    }
						
					}
				}
				else {
					criteria.add(Restrictions.eq(batchFieldName, value));
				}
				
			}
			if(inputValue instanceof Double){
				Double value = (Double)inputValue;
				criteria.add(Restrictions.eq(batchFieldName, value));
			}
			if(inputValue instanceof BigDecimal){
				BigDecimal value = (BigDecimal)inputValue;
				criteria.add(Restrictions.eq(batchFieldName, value));
			}
		}
		return criteria;
	}

	public BigDecimal getBatchSequence(String batchType)	throws DataAccessException {
		String sql = "select last_number from all_sequences where sequence_name = :sequenceName";
		
		Session session = createLocalSession();
		org.hibernate.Query query = session.createSQLQuery(sql);
		
		if ("IP".equalsIgnoreCase(batchType) || "PCO".equalsIgnoreCase(batchType))
			query.setParameter("sequenceName", "SQ_TB_PROCESS_COUNT");
		else if ("TR".equalsIgnoreCase(batchType))
			query.setParameter("sequenceName", "SQ_TB_PROCESS_COUNT");	
		else if ("SDM".equalsIgnoreCase(batchType))
			query.setParameter("sequenceName", "sq_tb_process_count");
		else if ("TCR".equalsIgnoreCase(batchType))
			query.setParameter("sequenceName", "sq_tb_process_count");
		else if ("IPS".equalsIgnoreCase(batchType))
			query.setParameter("sequenceName", "sq_tb_billtrans_id");
		else		
			assert false : "Unsupported batch type: " + batchType;
		
		BigDecimal returnBigDecimal = new BigDecimal(0);
			
		if("IP".equalsIgnoreCase(batchType) || "PCO".equalsIgnoreCase(batchType)  || "TR".equalsIgnoreCase(batchType) || "SDM".equalsIgnoreCase(batchType)
				 || "TCR".equalsIgnoreCase(batchType) || "IPS".equalsIgnoreCase(batchType)){
			returnBigDecimal = (BigDecimal)query.uniqueResult();
		}
		
		closeLocalSession(session);
		session=null;

		return returnBigDecimal;
	}

	@Transactional
	public Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz) {
		HibernateUtils hu = new HibernateUtils((HibernateEntityManagerFactory)getJpaTemplate().getEntityManagerFactory());
		return hu.getColumnToProperties(clazz);
	}
	
	public BatchMaintenance getPreviousBatchInSequence(BatchMaintenance batch) throws DataAccessException {
		Object b = null;
		Session session = createLocalSession();
		
		try {
			org.hibernate.SQLQuery qry = session.createSQLQuery("select * from tb_batch where"
				+ " batch_type_code = 'TR'"
				+ " and batch_status_code not in('D','ABI')"
				+ " and ts01 <= :nxtDate"
				+ " order by ts01 desc, nu01 desc");
			List<?> l = qry.addEntity(BatchMaintenance.class)
				.setDate("nxtDate", batch.getTs01())
				.setMaxResults(32)
				.list();
			int i;
			for (i = 0; i < l.size(); i++) {
				BatchMaintenance bm = (BatchMaintenance)l.get(i);
				if (bm.getBatchId().equals(batch.getBatchId()))
					break;
			}
			b = (++i) < l.size() ? l.get(i) : null; 
		} catch (HibernateException e) {
            logger.error("Error finding transaction batches for validation");
        	e.printStackTrace();
		}
		
		BatchMaintenance returnBatchMaintenance = (BatchMaintenance)b;
		
		closeLocalSession(session);
		session=null;
		
		return returnBatchMaintenance;
	}
	
	protected BatchMaintenance addBatchUsingJDBC(
			BatchMaintenance batchMaintenance, Connection conn) {
		// Get next batch Id
		long nextId = -1;
		Statement stat = null;
		try {
			stat = conn.createStatement();

			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_batch_id", "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ addBatchUsingJDBC() failed: "
					+ e.toString());
			return null;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		// Set new batch id
		batchMaintenance.setBatchId(nextId);
		PreparedStatement prep = null;
		try {
			String sql = batchMaintenance.getRawAddStatement();
			prep = conn.prepareStatement(sql);
			batchMaintenance.populateAddPreparedStatement(conn, prep);
			prep.addBatch();
			prep.executeBatch();
			conn.commit();
		} catch (Exception e) {
			return null;
		} finally {
			if (prep != null) {
				try {
					prep.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	public void createNewBatchGL(BatchMaintenance batch, String glDate) {
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}
		
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			Criteria criteria = session.createCriteria(BatchMaintenance.class);
			criteria.add(Expression.eq("yearPeriod", batch.getYearPeriod()));
			criteria.setProjection(Projections.max("relSeqn"));
			List<?> result = criteria.list();
			if (result == null || result.get(0) == null) {
				batch.setRelSeqn(new Long(1));
			} else {
				Long returnLong = ((Long) result.get(0)).longValue();
				if (returnLong != null) {
					batch.setRelSeqn(new Long(returnLong.longValue()+1));
				} else {
					batch.setRelSeqn(new Long(1));
				}
			
			}
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date dGlDate = df.parse(glDate);

			Date dNow = new Date();
			batch.setBatchTypeCode("GM");
			batch.setSchedStartTimestamp(dNow);
			batch.setActualStartTimestamp(dNow);
			batch.setEntryTimestamp(dNow);
			batch.setBatchStatusCode("XP");
			batch.setHeldFlag("0");
			batch.setTs01(dGlDate);
			session.persist(batch);
			tx.commit();
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();

			if (tx != null) {
				tx.rollback();
			}
			return;
		} finally {
			closeLocalSession(session);
			session = null;
		}	
		
		Session session1 = createLocalSession();
		Transaction tx1 = null;
		try {
			tx1 = session1.beginTransaction();
	
			if (pco != null && pco.equals("1")){
				
			}
			
			//Update tb_purchtrans
			org.hibernate.Query query1 = null;
			Long extract_flag=1l;
			String glExtractUpdater=batch.getUpdateUserId();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			String glcurrenttimeupdate = formatter.format(new Date());	
			
			String hqlUpdate1 = "";
			if (pco != null && pco.equals("1")){		
				hqlUpdate1 =	"UPDATE tb_purchtrans " +
								"SET gl_extract_flag = '1', "+
								"    gl_extract_updater = '"+glExtractUpdater+"', "+
								"    gl_extract_timestamp = to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss'), "+
								"    gl_extract_batch_no = " + batch.getbatchId() +
								" WHERE purchtrans_id IN ( " +
								"   SELECT DISTINCT tdl.purchtrans_id " +
								"   FROM tb_purchtrans_log tdl " +
								"   LEFT OUTER JOIN tb_purchtrans td ON td.purchtrans_id = tdl.purchtrans_id " +
								"   WHERE td.gl_date <= to_date('"+ glDate +"','mm/dd/yyyy hh24:mi:ss') " +
								"   AND (td.transaction_ind = 'P') " +
								"   AND (tdl.gl_extract_batch_no IS NULL OR tdl.gl_extract_batch_no = 0)) ";
			}
			else{
				hqlUpdate1 = 	"UPDATE tb_purchtrans " +
								"SET gl_extract_flag = '1', " +
								"    gl_extract_updater = '"+glExtractUpdater+"', "+
								"    gl_extract_timestamp =  to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss'), "+
								"    gl_extract_batch_no = "+batch.getbatchId()+
								" WHERE purchtrans_id IN ( " +
								"   SELECT DISTINCT tdl.purchtrans_id " +
								"   FROM tb_purchtrans_log tdl " +
								"   LEFT OUTER JOIN tb_purchtrans td ON td.purchtrans_id = tdl.purchtrans_id " +
								"   WHERE td.gl_date <= to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss') " +
								"   AND (td.transaction_ind = 'P') " +
								"   AND (tdl.gl_extract_batch_no IS NULL OR tdl.gl_extract_batch_no = 0)) ";
			}
			
			query1 = session1.createSQLQuery(hqlUpdate1);
			query1.executeUpdate();

			//Update tb_purchtrans_LOG
			org.hibernate.Query query2 = null;
			String 	hqlUpdate2 = "update tb_purchtrans_LOG ptl set " +
								 " ptl.GL_EXTRACT_BATCH_NO = "+batch.getbatchId()+", "+
								 " ptl.GL_EXTRACT_TIMESTAMP =  to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss') "+
								 " WHERE ptl.purchtrans_id IN (SELECT pt.purchtrans_id"+
								 " FROM tb_purchtrans pt WHERE pt.gl_extract_batch_no = "+batch.getbatchId()+")" +
								 " AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
			query2 = session1.createSQLQuery(hqlUpdate2);
			query2.executeUpdate();

			//Update batch
			org.hibernate.Query query3 = null;
			String hqlUpdate3 ="UPDATE tb_batch SET batch_status_code='P', " +
					" nu01 = (SELECT COUNT(*) FROM tb_purchtrans_LOG "+
					" WHERE GL_EXTRACT_BATCH_NO ='"+ batch.getbatchId()+"'), "+
					" processed_rows = (SELECT COUNT(*) FROM tb_purchtrans "+
					" WHERE GL_EXTRACT_BATCH_NO ='"+ batch.getbatchId()+"'), "+
					" update_timestamp=to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss'), "+
					" actual_end_timestamp=to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy hh24:mi:ss') "+
					" WHERE batch_id = "+ batch.getbatchId();
			query3 = session1.createSQLQuery(hqlUpdate3);
			query3.executeUpdate();
				
			tx1.commit();
			session1.flush();
		} catch (Exception e1) {
			e1.printStackTrace();

			if (tx1 != null) {
				tx1.rollback();
			}
		} finally {
			closeLocalSession(session1);
			session1 = null;
		}
	}

	private void updateTransactionDetailTest(BatchMaintenance batch) {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
		String hqlUpdate ="update tb_purchtrans_LOG set " +
		"GL_EXTRACT_BATCH_NO = '"+batch.getbatchId()+"'"+
		" WHERE purchtrans_id IN (SELECT purchtrans_id"+
		" FROM tb_purchtrans WHERE gl_extract_batch_no ='"+batch.getbatchId()+"')";
		query = session.createSQLQuery(hqlUpdate);
		query.executeUpdate();
		tx.commit();
		
	} catch (Exception e) {
		logger.error("Failed to execute setSubEntityInactive: " + e);
		e.printStackTrace();
		
		if (tx != null) {
			tx.rollback();
		
		}
	} finally {
		closeLocalSession(session);
		session=null;
		
	}
	
	}


	private  void updateTransactionDetail(BatchMaintenance batch,String sqlWhere) {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		Long extract_flag=1l;
		String glExtractUpdater=batch.getUpdateUserId();
		Date myDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String glcurrenttimeupdate = formatter.format(myDate);
		
		try {
			tx = session.beginTransaction();
			String hqlUpdate ="update tb_purchtrans SET " +
			"gl_extract_flag = '"+extract_flag+"', "+
			"gl_extract_updater = '"+glExtractUpdater+"', "+
			"gl_extract_timestamp =  to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy'), "+
			"gl_extract_batch_no = '"+batch.getbatchId()+"'"+
			" WHERE purchtrans_id IN (SELECT purchtrans_id"+
			" FROM tb_purchtrans WHERE tb_purchtrans.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy')" +
					"AND (tb_purchtrans.gl_extract_flag IS NULL OR tb_purchtrans.gl_extract_flag <> '1'))";
			  
	        query = session.createSQLQuery(hqlUpdate);
	        query.executeUpdate();
       		tx.commit();
			
		} catch (Exception e) {
			logger.error("Failed to execute setSubEntityInactive: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}	
	}


	private void batchUpdate(BatchMaintenance batch) {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		Date myDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String glcurrenttimeupdate = formatter.format(myDate);
		
		try {
			tx = session.beginTransaction();
			String hqlUpdate ="UPDATE tb_batch SET batch_status_code='P', " +
					"total_rows = (SELECT COUNT(*) FROM tb_purchtrans_LOG "+
					"WHERE gl_extract_batch_id ='"+ batch.getbatchId()+"'), "+
					"update_timestamp=to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy'), "+
					"actual_end_timestamp=to_date('"+ glcurrenttimeupdate +"','mm/dd/yyyy') "+
					"WHERE batch_id ='"+ batch.getbatchId()+"'";
			query = session.createSQLQuery(hqlUpdate);
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute setSubEntityInactive: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	/////////////////////////////////
	public void batchDelete(Long batchId) { 
		System.out.println("batch delete");
		BatchMaintenance bm = findByBatchId(batchId);
		if (bm == null || !bm.getBatchStatusCode().equalsIgnoreCase("FD")) {
			return;
		}

		Session session = null;
		Transaction tx = null;
		try {
			session = createLocalSession();
			tx = session.beginTransaction();
			bm.setBatchStatusCode("ABD");
			bm.setActualStartTimestamp(new Date());
			System.out.println("Deleting...");
			session.update(bm);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return;
		} finally {
			closeLocalSession(session);
			session = null;
		}

		String batchTypeCode = bm.getBatchTypeCode().toUpperCase();
		session = null;
		tx = null;
		org.hibernate.Query query = null;
		try {
			session = createLocalSession();
			tx = session.beginTransaction();

			if (batchTypeCode.equals("IP") || batchTypeCode.equals("IPS")
					|| batchTypeCode.equals("PCO")) {
				if (batchTypeCode.equals("IP") || batchTypeCode.equals("PCO")) {
					// DELETE FROM tb_purchtrans WHERE process_batch_no
					// = an_batch_id;
					query = session
							.createQuery("delete TransactionDetail t where t.processBatchNo = :batchId");
					query.setLong("batchId", bm.getBatchId());
					query.executeUpdate();

					// DELETE FROM tb_bcp_transactions WHERE process_batch_no =
					// an_batch_id;
					query = session
							.createQuery("delete BCPTransactionDetail t where t.processBatchNo = :batchId");
					query.setLong("batchId", bm.getBatchId());
					query.executeUpdate();
				}

				if (batchTypeCode.equals("IPS") || batchTypeCode.equals("PCO")) {
					// DELETE FROM tb_saletrans where process_batch_no =
					// an_batch_id;
					query = session
							.createQuery("delete BillTransaction t where t.processBatchNo = :batchId");
					query.setLong("batchId", bm.getBatchId());
					query.executeUpdate();

					// DELETE FROM tb_bcp_saletrans WHERE process_batch_no =
					// an_batch_id;
					query = session
							.createQuery("delete BCPBillTransaction t where t.processBatchNo = :batchId");
					query.setLong("batchId", bm.getBatchId());
					query.executeUpdate();
				}
			} else if (batchTypeCode.equals("TR")) {
				// DELETE FROM tb_bcp_jurisdiction_taxrate WHERE batch_id =
				// an_batch_id;
				query = session
						.createQuery("delete BCPJurisdictionTaxRate t where t.id.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();

				// DELETE FROM tb_bcp_juri_taxrate_text WHERE batch_id =
				// an_batch_id;
				query = session
						.createQuery("delete BCPJurisdictionTaxRateText t where t.id.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();

			} else if (batchTypeCode.equals("TCR")) {
				// DELETE FROM tb_bcp_taxcode_rules WHERE batch_id =
				// an_batch_id;
				query = session
						.createQuery("delete BCPTaxCodeRule t where t.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();

				// DELETE FROM tb_bcp_taxcode_rules_text WHERE batch_id =
				// an_batch_id;
				query = session
						.createQuery("delete BCPTaxCodeRuleText t where t.id.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();
			} else if (batchTypeCode.equals("SDM")) {
				// DELETE FROM tb_bcp_situs_rules WHERE batch_id = an_batch_id;
				query = session
						.createQuery("delete BCPSitusRule t where t.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();

				// DELETE FROM tb_bcp_situs_rules_text WHERE batch_id =
				// an_batch_id;
				query = session
						.createQuery("delete BCPSitusRuleText t where t.id.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();
			} else if (batchTypeCode.equals("CI")) {
				query = session
						.createQuery("delete BCPConfigStagingText t where t.id.batchId = :batchId");
				query.setLong("batchId", bm.getBatchId());
				query.executeUpdate();
			} else if (batchTypeCode.equals("DL")) {
			} else if (batchTypeCode.equals("FP")) {
			}

			// All batch types
			query = session
					.createQuery("delete BatchErrorLog b where b.batchId = :batchId");
			query.setLong("batchId", bm.getBatchId());
			query.executeUpdate();

			// UPDATE tb_batch
			bm.setBatchStatusCode("D");
			bm.setActualEndTimestamp(new Date());
			session.update(bm);

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session = null;
		}
	}

	public void reindex(String index) {
		Session session = null;
		Transaction tx = null;
		org.hibernate.Query query = null;
		String indexTablespace = "indx"; // Default
		try {
			session = createLocalSession();

			// Get indexTablespace
			query = session
					.createQuery("from Option o where o.codePK.optionCode='INDEX_TABLESPACE' and  o.codePK.optionTypeCode='ADMIN' and  o.codePK.userCode='ADMIN'");
			List<Option> results1 = (List<Option>) query.list();
			if (null != results1 && results1.size() > 0) {
				Option option = results1.get(0);
				if (option != null && option.getValue() != null
						&& option.getValue().length() > 0) {
					indexTablespace = option.getValue();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			closeLocalSession(session);
			session = null;
		}

		// "1" = Build Transaction Detail Indexes only
		// "2" = Build Tax Matrix Indexes only
		// "3" = Build Location Matrix Indexes only
		// "9" = Build All Indexes

		// -- Transaction Indexes
		if (index.equals("1") || index.equals("9")) {
			String transactionDetailIndexesQuery = "SELECT index_name FROM all_indexes WHERE index_name like 'IDX_TB_TRANS_DTL_DRIVER_%' "
					+ "OR index_name like 'IDX_TB_TRANS_DTL_TDRIVER_%' OR index_name like 'IDX_TB_TRANS_DTL_LDRIVER_%' "
					+ "AND owner = 'STSCORP' ";
			session = null;
			tx = null;
			try {
				session = createLocalSession();
				tx = session.beginTransaction();

				// -- drop all driver indexes
				query = session.createSQLQuery(transactionDetailIndexesQuery);
				List<String> results = (List<String>) query.list();
				for (int i = 0; i < results.size(); i++) {
					try {
						query = session.createSQLQuery("DROP INDEX "
								+ results.get(i));
						System.out.println("DROP INDEX " + results.get(i));
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				// -- create all driver indexes
				query = session.createQuery("FROM DriverNames d WHERE d.driverNamesPK.drvNamesCode IN ('T','L') ORDER BY d.driverNamesPK.drvNamesCode, d.driverNamesPK.driverId");
				List<DriverNames> driverNamesList = (List<DriverNames>) query.list();
				DriverNames driverName = null;
				String driverNamesCode = "";
				String matrixColumnName = "";
				String transDtlColumnName = "";
				String indexStatement = "";
				for (int i = 0; i < driverNamesList.size(); i++) {
					driverName = (DriverNames) driverNamesList.get(i);
					driverNamesCode = driverName.getDrvNamesCode(); // L or T
					matrixColumnName = driverName.getMatrixColName(); // DRIVER_01
					transDtlColumnName = driverName.getTransDtlColName(); // GL_CC_NBR_DEPT_ID

					indexStatement = "CREATE INDEX idx_tb_trans_dtl_"
							+ driverNamesCode + matrixColumnName
							+ " ON tb_purchtrans ("
							+ transDtlColumnName + ") nologging tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					indexStatement = "CREATE INDEX idx_tb_trans_dtl_"
							+ driverNamesCode + matrixColumnName + "_UP"
							+ " ON tb_purchtrans (UPPER("
							+ transDtlColumnName + ")) nologging tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					tx.rollback();
				}
				return;
			} finally {
				closeLocalSession(session);
				session = null;
			}
		}

		// -- Tax Matrix Indexes
		if (index.equals("2") || index.equals("9")) {
			// -- Tax Matrix table
			String taxMatrixIndexQuery = "SELECT index_name FROM all_indexes WHERE index_name like 'IDX_TB_TAX_MATRIX_DRIVER_%' AND owner = 'STSCORP' ";
			session = null;
			tx = null;
			try {
				session = createLocalSession();
				tx = session.beginTransaction();

				// -- drop all driver indexes
				query = session.createSQLQuery(taxMatrixIndexQuery);
				List<String> results = (List<String>) query.list();
				for (int i = 0; i < results.size(); i++) {
					try {
						query = session.createSQLQuery("DROP INDEX " + results.get(i));
						System.out.println("DROP INDEX " + results.get(i));
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				// -- create all driver indexes
				query = session.createQuery("FROM DriverNames d WHERE d.driverNamesPK.drvNamesCode IN ('T') ORDER BY d.driverNamesPK.driverId");
				List<DriverNames> driverNamesList = (List<DriverNames>) query.list();
				DriverNames driverName = null;
				String matrixColumnName = "";
				String indexStatement = "";
				for (int i = 0; i < driverNamesList.size(); i++) {
					driverName = (DriverNames) driverNamesList.get(i);
					matrixColumnName = driverName.getMatrixColName(); // DRIVER_01

					indexStatement = "CREATE INDEX idx_tb_tax_matrix_"
							+ matrixColumnName
							+ " ON tb_tax_matrix ("
							+ matrixColumnName
							+ ") nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					indexStatement = "CREATE INDEX idx_tb_tax_matrix_"
							+ matrixColumnName
							+ "_UP ON tb_tax_matrix (UPPER("
							+ matrixColumnName
							+ ")) nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					tx.rollback();
				}
				return;
			} finally {
				closeLocalSession(session);
				session = null;
			}

			// -- Jur Alloc Matrix table
			String jurallocMatrixIndexQuery = "SELECT index_name FROM all_indexes WHERE index_name like 'IDX_TB_ALL_MATRIX_DRIVER_%' AND owner = 'STSCORP' ";
			session = null;
			tx = null;
			try {
				session = createLocalSession();
				tx = session.beginTransaction();

				// -- drop all driver indexes
				query = session.createSQLQuery(jurallocMatrixIndexQuery);
				List<String> results = (List<String>) query.list();
				for (int i = 0; i < results.size(); i++) {
					try {
						query = session.createSQLQuery("DROP INDEX " + results.get(i));
						System.out.println("DROP INDEX " + results.get(i));
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				// -- create all driver indexes
				query = session.createQuery("FROM DriverNames d WHERE d.driverNamesPK.drvNamesCode IN ('T') ORDER BY d.driverNamesPK.driverId");
				List<DriverNames> driverNamesList = (List<DriverNames>) query.list();
				DriverNames driverName = null;
				String matrixColumnName = "";
				String indexStatement = "";
				for (int i = 0; i < driverNamesList.size(); i++) {
					driverName = (DriverNames) driverNamesList.get(i);
					matrixColumnName = driverName.getMatrixColName(); // DRIVER_01

					indexStatement = "CREATE INDEX idx_tb_all_matrix_"
							+ matrixColumnName
							+ " ON tb_allocation_matrix ("
							+ matrixColumnName
							+ ") nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					indexStatement = "CREATE INDEX idx_tb_all_matrix_"
							+ matrixColumnName
							+ "_UP ON tb_allocation_matrix (UPPER("
							+ matrixColumnName
							+ ")) nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					tx.rollback();
				}
				return;
			} finally {
				closeLocalSession(session);
				session = null;
			}

			// -- TaxCode Alloc Matrix table
			String taxallocMatrixIndexQuery = "SELECT index_name FROM all_indexes WHERE index_name like 'IDX_TB_TAL_MATRIX_DRIVER_%' AND owner = 'STSCORP' ";
			session = null;
			tx = null;
			try {
				session = createLocalSession();
				tx = session.beginTransaction();

				// -- drop all driver indexes
				query = session.createSQLQuery(taxallocMatrixIndexQuery);
				List<String> results = (List<String>) query.list();
				for (int i = 0; i < results.size(); i++) {
					try {
						query = session.createSQLQuery("DROP INDEX " + results.get(i));
						System.out.println("DROP INDEX " + results.get(i));
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				// -- create all driver indexes
				query = session.createQuery("FROM DriverNames d WHERE d.driverNamesPK.drvNamesCode IN ('T') ORDER BY d.driverNamesPK.driverId");
				List<DriverNames> driverNamesList = (List<DriverNames>) query.list();
				DriverNames driverName = null;
				String matrixColumnName = "";
				String indexStatement = "";
				for (int i = 0; i < driverNamesList.size(); i++) {
					driverName = (DriverNames) driverNamesList.get(i);
					matrixColumnName = driverName.getMatrixColName(); // DRIVER_01

					indexStatement = "CREATE INDEX idx_tb_tal_matrix_"
							+ matrixColumnName
							+ " ON tb_tax_alloc_matrix ("
							+ matrixColumnName
							+ ") nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;

					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					indexStatement = "CREATE INDEX idx_tb_tal_matrix_"
							+ matrixColumnName
							+ "_UP ON tb_tax_alloc_matrix (UPPER("
							+ matrixColumnName
							+ ")) nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;

					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					tx.rollback();
				}
				return;
			} finally {
				closeLocalSession(session);
				session = null;
			}

		}

		// -- Location Matrix Indexes
		if (index.equals("3") || index.equals("9")) {
			String locnMatrixIndexQuery = "SELECT index_name FROM all_indexes WHERE index_name like 'IDX_TB_LOC_MATRIX_DRIVER_%' AND owner = 'STSCORP' ";
			session = null;
			tx = null;
			try {
				session = createLocalSession();
				tx = session.beginTransaction();

				// -- drop all driver indexes
				query = session.createSQLQuery(locnMatrixIndexQuery);
				List<String> results = (List<String>) query.list();
				for (int i = 0; i < results.size(); i++) {
					try {
						query = session.createSQLQuery("DROP INDEX " + results.get(i));
						System.out.println("DROP INDEX " + results.get(i));
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}

				// -- create all driver indexes
				query = session.createQuery("FROM DriverNames d WHERE d.driverNamesPK.drvNamesCode IN ('L') ORDER BY d.driverNamesPK.driverId");
				List<DriverNames> driverNamesList = (List<DriverNames>) query.list();
				DriverNames driverName = null;
				String matrixColumnName = "";
				String indexStatement = "";
				for (int i = 0; i < driverNamesList.size(); i++) {
					driverName = (DriverNames) driverNamesList.get(i);
					matrixColumnName = driverName.getMatrixColName(); // DRIVER_01
					indexStatement = "CREATE INDEX idx_tb_loc_matrix_"
							+ matrixColumnName
							+ " ON tb_location_matrix ("
							+ matrixColumnName
							+ ") nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					indexStatement = "CREATE INDEX idx_tb_loc_matrix_"
							+ matrixColumnName
							+ "_UP ON tb_location_matrix (UPPER("
							+ matrixColumnName
							+ ")) nologging parallel 5 compute statistics tablespace "
							+ indexTablespace;
					try {
						query = session.createSQLQuery(indexStatement);
						System.out.println(indexStatement);
						query.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (tx != null) {
					tx.rollback();
				}
				return;
			} finally {
				closeLocalSession(session);
				session = null;
			}
		}
	}
	
	private void createOrReplaceSequence(Session session, String sequenceName, String columnName, String tableName, Long start){
		String sequenceNameUpperCase = sequenceName.toUpperCase();
		org.hibernate.Query query = null;
		List<BigDecimal> results = null;
		
	   	Long vn_max_id = start;
	   	
	   	if(!(columnName==null || columnName.length()==0 || tableName==null || tableName.length()==0 )){
		   	query = session.createSQLQuery("SELECT MAX("+columnName+") FROM "+tableName);
			results = (List<BigDecimal>)query.list();
			if (results!=null && results.size()>0 && results.get(0)!=null) {
				vn_max_id = ((BigDecimal)results.get(0)).longValue() + 1L;
			} 
	   	}
		
		Long vnCount = 0L;
	   	query = session.createSQLQuery("SELECT count(*) FROM all_sequences WHERE sequence_name = '"+sequenceNameUpperCase+ "' AND sequence_owner = 'STSCORP'");
		results = (List<BigDecimal>)query.list();
		if (results!=null && results.size()>0 && results.get(0)!=null) {
			vnCount = ((BigDecimal)results.get(0)).longValue();
		} 

		if(vnCount>0L){
			if(!(columnName==null || columnName.length()==0 || tableName==null || tableName.length()==0 )){
				String dropQuery = "DROP SEQUENCE STSCORP."+sequenceNameUpperCase;
				try {
					query = session.createSQLQuery(dropQuery);
					System.out.println(dropQuery);
					query.executeUpdate();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			else{
				System.out.println("-------------------------------- No need to create " + sequenceNameUpperCase);
				return;
			}
		}

		String createSeqQuery = "CREATE SEQUENCE STSCORP."+sequenceNameUpperCase+" INCREMENT BY 1 START WITH " +
			      vn_max_id + " MAXVALUE 1.0E27 MINVALUE 1 NOCYCLE NOCACHE NOORDER";
		try {
			query = session.createSQLQuery(createSeqQuery);
			System.out.println(createSeqQuery);
			query.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		String grantSeqQuery = "GRANT SELECT ON STSCORP."+sequenceNameUpperCase+" TO STSUSER";
		try {
			query = session.createSQLQuery(grantSeqQuery);
			System.out.println(grantSeqQuery);
			query.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("--------------------------------");
	}

	public void maxAll(){
		Session session = null;
	    Transaction tx = null;
		try {
			session = createLocalSession();
		   	tx = session.beginTransaction();

		   	createOrReplaceSequence(session, "sq_gl_extract_batch_id", "gl_extract_batch_no", "tb_gl_report_log", 1L);	  	
		   	createOrReplaceSequence(session, "sq_tb_allocation_matrix_id", "allocation_matrix_id", "tb_allocation_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_allocation_subtrans_id", "allocation_subtrans_id", "tb_purchtrans", 1L);
		   	createOrReplaceSequence(session, "sq_tb_batch_error_log_id", "batch_error_log_id", "tb_batch_error_log", 1L);
		   	createOrReplaceSequence(session, "sq_tb_batch_id", "batch_id", "tb_batch", 1L);
		   	createOrReplaceSequence(session, "sq_tb_bcp_juris_taxrate_id", "bcp_jurisdiction_taxrate_id", "tb_bcp_jurisdiction_taxrate", 1L);
		   	createOrReplaceSequence(session, "sq_tb_bcp_situs_rules_id", "bcp_situs_rules_id", "tb_bcp_situs_rules", 1L);
		   	createOrReplaceSequence(session, "sq_tb_bcp_taxcode_rule_id", "bcp_taxcode_rule_id", "tb_bcp_taxcode_rules", 1L);
		   	createOrReplaceSequence(session, "sq_tb_bcp_transactions", "bcp_transaction_id", "tb_bcp_transactions", 1L);
		   	createOrReplaceSequence(session, "sq_tb_entity_default_id", "entity_default_id", "tb_entity_default", 1L);
		   	createOrReplaceSequence(session, "sq_tb_entity_id", "entity_id", "tb_entity", 1L);
		   	createOrReplaceSequence(session, "sq_tb_entity_locn_set_dtl_id", "entity_locn_set_dtl_id", "tb_entity_locn_set_dtl", 1L);
		   	createOrReplaceSequence(session, "sq_tb_entity_locn_set_id", "entity_locn_set_id", "tb_entity_locn_set", 1L);
		   	createOrReplaceSequence(session, "sq_tb_gl_extract_map_id", "gl_extract_map_id", "tb_gl_extract_map", 1L);
		   	createOrReplaceSequence(session, "sq_tb_jurisdiction_id", "jurisdiction_id", "tb_jurisdiction", 1L);
		   	createOrReplaceSequence(session, "sq_tb_jurisdiction_taxrate_id", "jurisdiction_taxrate_id", "tb_jurisdiction_taxrate", 1L);
		   	createOrReplaceSequence(session, "sq_tb_location_matrix_id", "location_matrix_id", "tb_location_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_nexus_def_detail_id", "nexus_def_detail_id", "tb_nexus_def_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_nexus_def_id", "nexus_def_id", "tb_nexus_def", 1L);
		   	createOrReplaceSequence(session, "sq_tb_nexus_def_log_id", "nexus_def_log_id", "tb_nexus_def_log", 1L);
		   	createOrReplaceSequence(session, "sq_tb_saletrans_id", "saletrans_id", "tb_saletrans", 1L);
		   	createOrReplaceSequence(session, "sq_tb_scenario_id", "scenario_id", "tb_scenario", 1L);
		   	createOrReplaceSequence(session, "sq_tb_situs_driver_detail_id", "situs_driver_detail_id", "tb_situs_driver_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_situs_driver_sys", "situs_driver_id", "tb_situs_driver", 1L);
		   	createOrReplaceSequence(session, "sq_tb_situs_driver_usr", "situs_driver_id", "tb_situs_driver", 1L);
		   	createOrReplaceSequence(session, "sq_tb_situs_matrix_sys", "situs_matrix_id", "tb_situs_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_situs_matrix_usr", "situs_matrix_id", "tb_situs_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_split_subtrans_id", "split_subtrans_id", "tb_purchtrans", 1L);
		   	createOrReplaceSequence(session, "sq_tb_tax_alloc_matrix_dtl_id", "tax_alloc_matrix_dtl_id", "tb_tax_alloc_matrix_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_tax_alloc_matrix_id", "tax_alloc_matrix_id", "tb_tax_alloc_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_tax_matrix_id", "tax_matrix_id", "tb_tax_matrix", 1L);
		   	createOrReplaceSequence(session, "sq_tb_taxcode_detail_id", "taxcode_detail_id", "tb_taxcode_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_taxcode_detail_sys", "taxcode_detail_id", "tb_taxcode_detail WHERE custom_flag = '0'", 1L);
		   	createOrReplaceSequence(session, "sq_tb_taxcode_detail_usr", "taxcode_detail_id", "tb_taxcode_detail WHERE custom_flag = '1'", 1000001L);
		   	createOrReplaceSequence(session, "sq_tb_trans_detail_excp_id", "transaction_detail_excp_id", "tb_purchtrans_excp", 1L);
		   	createOrReplaceSequence(session, "sq_tb_purchtrans_id", "purchtrans_id", "tb_purchtrans", 1L);
		   	createOrReplaceSequence(session, "sq_tb_config_log", "config_log_id", "tb_config_log", 1L);
		   	createOrReplaceSequence(session, "sq_tb_config_key_id", "row_key_id", "tb_config_log", 1L);   	
		   	createOrReplaceSequence(session, "sq_related_trans_id", "related_subtrans_id", "tb_saletrans", 1L);
		   	createOrReplaceSequence(session, "sq_tb_config_log_id", "config_log_id", "tb_config_log", 1L);
		   	createOrReplaceSequence(session, "sq_tb_cust_cert_id", "cust_cert_id", "tb_cust_cert", 1L);
		   	createOrReplaceSequence(session, "sq_tb_cust_id", "cust_id", "tb_cust", 1L);
		   	createOrReplaceSequence(session, "sq_tb_cust_locn_ex_id", "cust_locn_ex_id", "tb_cust_locn_ex", 1L);
		   	createOrReplaceSequence(session, "sq_tb_cust_locn_id", "cust_locn_id", "tb_cust_locn", 1L);
		   	createOrReplaceSequence(session, "sq_tb_procrule_audit_id", "procrule_audit_id", "tb_procrule_audit", 1L);
		   	createOrReplaceSequence(session, "sq_tb_procrule_detail_id", "procrule_detail_id", "tb_procrule_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_procrule_id", "procrule_id", "tb_procrule", 1L);
		   	createOrReplaceSequence(session, "sq_tb_proctable_detail_id", "proctable_detail_id", "tb_proctable_detail", 1L);
		   	createOrReplaceSequence(session, "sq_tb_proctable_id", "proctable_id", "tb_proctable", 1L);
		   	createOrReplaceSequence(session, "sq_tb_saletrans_log_id", "saletrans_log_id", "tb_saletrans_log", 1L);   	
		   	createOrReplaceSequence(session, "sq_tb_bcp_config_staging_id", "", "", 1L);
		   	createOrReplaceSequence(session, "sq_tb_bcp_billtrans_id", "", "", 1L);
		   	createOrReplaceSequence(session, "sq_tb_process_count", "", "", 1L);
		   	
		   	tx.commit();
		} catch (Exception e) {
			e.printStackTrace();	
			if (tx != null) {
				tx.rollback();
			}
			return;
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	public String getBinaryWeightStatement(int driverNumber){
		StringBuilder statementBuilder = new StringBuilder();
		NumberFormat formatter = new DecimalFormat("00");
		Double driverWeight = new Double(0.0);
		String driver = "";
		for(int i=1; i<=driverNumber; i++){
			if(i>1){
				statementBuilder.append("+");
			}
			driver = "driver"+formatter.format(i);
			driverWeight = java.lang.Math.pow(2, driverNumber - i);
			statementBuilder.append(" (CASE WHEN ("+driver+" is null or "+driver+" = '*ALL') THEN 0 ELSE "+new BigDecimal(driverWeight).toPlainString()+" END) ");
		}

		return statementBuilder.toString();
	}
	
	public String getSignificantDigitsStatement(int driverNumber){
		StringBuilder statementBuilder = new StringBuilder();
		NumberFormat formatter = new DecimalFormat("00");
		String driver = "";
		String checkWildCard = "";
		for(int i=1; i<=driverNumber; i++){
			if(i>1){
				statementBuilder.append(" || '.' || ");
			}
			driver = "driver"+formatter.format(i);
			checkWildCard = "(CASE WHEN (locate('%',"+driver+")>0 and LENGTH(locate('%',"+driver+"))=1) " +
									"THEN CONCAT('00',locate('%',"+driver+")) " + 
									"ELSE (CASE WHEN (locate('%',"+driver+")>0 and LENGTH(locate('%',"+driver+"))=2) " + 
												"THEN CONCAT('0',locate('%',"+driver+")) " + 
												"ELSE (CASE WHEN (locate('%',"+driver+")>0 and LENGTH(locate('%',"+driver+"))=3) " + 
															"THEN CONCAT('',locate('%',"+driver+")) " + 
															"ELSE '999' " +	
															"END) " +
												"END) " +
									"END) ";
			statementBuilder.append("(CASE WHEN ("+driver+" is null or "+driver+" = '*ALL') THEN '000' ELSE " + checkWildCard + " END)");	
		}
		
		return statementBuilder.toString();
	}
	
	public void reweightMatrix(){
		Session session = null;
	    Transaction tx = null;
	    org.hibernate.Query query = null;
		try {
			session = createLocalSession();
		   	tx = session.beginTransaction();
		   	 
		   	StringBuilder statementBuilder = null;
		   	Double driverGlobalWeight = null;
		   	String queryBinaryWeightMatrix = "";
		   	String querySignificantDigitsMatrix = "";
		   	int rowUpdate = 0;
		   	int numberOfDrivers = 0;

		   	//For Tax Matrix
		   	numberOfDrivers = 30;
		   	
	   		//-- Update binary weight for normal tax matrix lines
		   	statementBuilder = new StringBuilder();
		   	statementBuilder.append(getBinaryWeightStatement(numberOfDrivers));
			if(statementBuilder.length()>0){
				statementBuilder.append("+");
			}
			driverGlobalWeight = java.lang.Math.pow(2,numberOfDrivers);
			statementBuilder.append(" (CASE WHEN (driverGlobalFlag is not null and driverGlobalFlag = '1') THEN "+new BigDecimal(driverGlobalWeight).toPlainString()+" ELSE 0 END) ");

	   		queryBinaryWeightMatrix = "update TaxMatrix set binaryWeight= " +statementBuilder.toString()+ ", defaultBinaryWeight=0  where (defaultFlag is null or defaultFlag != '1') ";
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for normal tax matrix lines
			querySignificantDigitsMatrix = "update TaxMatrix set significantDigits= " +getSignificantDigitsStatement(numberOfDrivers)+ ", defaultSignificantDigits=null  where (defaultFlag is null or defaultFlag != '1') ";
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();

			//-- Update binary weight for default tax matrix lines
			statementBuilder = new StringBuilder();
		   	statementBuilder.append(getBinaryWeightStatement(numberOfDrivers));
			if(statementBuilder.length()>0){
				statementBuilder.append("+");
			}
			driverGlobalWeight = java.lang.Math.pow(2,numberOfDrivers);
			statementBuilder.append(" (CASE WHEN (driverGlobalFlag is not null and driverGlobalFlag = '1') THEN "+new BigDecimal(driverGlobalWeight).toPlainString()+" ELSE 0 END) ");

			queryBinaryWeightMatrix = "update TaxMatrix set defaultBinaryWeight= " +statementBuilder.toString()+ ", binaryWeight=0  where (defaultFlag is not null and defaultFlag = '1') ";
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for default tax matrix lines
			querySignificantDigitsMatrix = "update TaxMatrix set defaultSignificantDigits = " +getSignificantDigitsStatement(numberOfDrivers)+ ", significantDigits=null  where (defaultFlag is not null or defaultFlag = '1') ";
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();
		
			//For Location Matrix
			numberOfDrivers = 10;
		   	
		   	//-- Update binary weight for normal location matrix lines
	   		queryBinaryWeightMatrix = "update LocationMatrix set binaryWeight= " +getBinaryWeightStatement(numberOfDrivers)+ ", defaultBinaryWeight=0  where (defaultFlag is null or defaultFlag != '1') ";
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for normal location matrix lines
			querySignificantDigitsMatrix = "update LocationMatrix set significantDigits= " +getSignificantDigitsStatement(numberOfDrivers)+ ", defaultSignificantDigits=null  where (defaultFlag is null or defaultFlag != '1') ";
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();

			//-- Update binary weight for default location matrix lines
			queryBinaryWeightMatrix = "update LocationMatrix set defaultBinaryWeight= " +getBinaryWeightStatement(numberOfDrivers)+ ", binaryWeight=0  where (defaultFlag is not null and defaultFlag = '1') ";
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for default location matrix lines
			querySignificantDigitsMatrix = "update LocationMatrix set defaultSignificantDigits = " +getSignificantDigitsStatement(numberOfDrivers)+ ", significantDigits=null  where (defaultFlag is not null or defaultFlag = '1') ";
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();
			
			//For Jurisdiction Allocation 
			numberOfDrivers = 30;
			
			//-- Update binary weight for jurisdiction allocation lines
	   		queryBinaryWeightMatrix = "update AllocationMatrix set binaryWeight= " +getBinaryWeightStatement(numberOfDrivers) ;
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for jurisdiction allocation lines
			querySignificantDigitsMatrix = "update AllocationMatrix set significantDigits= " +getSignificantDigitsStatement(numberOfDrivers);
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();
			
			//For taxcode split lines
			numberOfDrivers = 30;
			
			//-- Update binary weight for jurisdiction allocation lines
	   		queryBinaryWeightMatrix = "update TaxAllocationMatrix set binaryWeight= " +getBinaryWeightStatement(numberOfDrivers) ;
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
			//-- Update significant digits for jurisdiction allocation lines
			querySignificantDigitsMatrix = "update TaxAllocationMatrix set significantDigits= " +getSignificantDigitsStatement(numberOfDrivers);
			query = session.createQuery(querySignificantDigitsMatrix);
			System.out.println(querySignificantDigitsMatrix);
			rowUpdate = query.executeUpdate();
			
			
			//For situs matrix lines
			numberOfDrivers = 30;
			
			//-- Update binary weight for jurisdiction allocation lines
	   		queryBinaryWeightMatrix = "update SitusMatrix set binaryWeight= " +getBinaryWeightStatement(numberOfDrivers) ;
			query = session.createQuery(queryBinaryWeightMatrix);
			System.out.println(queryBinaryWeightMatrix);
			rowUpdate = query.executeUpdate();
			
		   	tx.commit();
		} catch (Exception e) {
			e.printStackTrace();	
			if (tx != null) {
				tx.rollback();
			}
			return;
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	@SuppressWarnings("unchecked")
	public void truncateBcpTable(){
		Session session = null;
	    Transaction tx = null;
	    org.hibernate.Query query = null;
	    int rowUpdate = 0;
	    List<Long> results = null;
	    String hqlDeleteBatches = "";
		try {
			session = createLocalSession();
		   	tx = session.beginTransaction();
		   	
		   	Long vnCount = 0L;
			
			//-- *** Import/Process Transactions ***
			//   -- Delete batches
			hqlDeleteBatches = 	"delete from BCPTransactionDetail bcp where " + 
						"bcp.processBatchNo in (select distinct bcpb.processBatchNo from BCPTransactionDetail bcpb, Batch b where " +
						"bcpb.processBatchNo=b.batchId and b.batchStatusCode in ('ABI','ABP','D','P')) ";  
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();

			//-- Truncate tb_bcp_transactions
			query = session.createQuery("select count(*) from BCPTransactionDetail");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_transactions";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}
			
			//-- *** Tax Rate Updates - Tax Rates ***
			//-- Delete batches   
			hqlDeleteBatches = 	"delete from BCPJurisdictionTaxRate bcp where " + 
						"bcp.id.batchId in (select distinct bcpb.id.batchId from BCPJurisdictionTaxRate bcpb, Batch b where " +
						"bcpb.id.batchId=b.batchId and b.batchStatusCode in ('ABTR','D','TR')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_jurisdiction_taxrate
			query = session.createQuery("select count(*) from BCPJurisdictionTaxRate");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_jurisdiction_taxrate";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}
			
			//-- *** Tax Rate Updates - Text Table ***
			//-- Delete batches 
			hqlDeleteBatches = 	"delete from BCPJurisdictionTaxRateText bcp where " + 
						"bcp.id.batchId in (select distinct bcpb.id.batchId from BCPJurisdictionTaxRateText bcpb, Batch b where " +
						"bcpb.id.batchId=b.batchId and b.batchStatusCode in ('ABTR','D','TR')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_juri_taxrate_text
			query = session.createQuery("select count(*) from BCPJurisdictionTaxRateText");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_juri_taxrate_text";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}
			
			//-- *** Sales Transactions ***
			//-- Delete batches 
			hqlDeleteBatches = 	"delete from BCPBillTransaction bcp where " + 
						"bcp.processBatchNo in (select distinct bcpb.processBatchNo from BCPBillTransaction bcpb, Batch b where " +
						"bcpb.processBatchNo=b.batchId and b.batchStatusCode in ('ABI', 'ABP','D','P')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_saletrans
			query = session.createQuery("select count(*) from BCPBillTransaction");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_billtrans";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}

			//-- *** Situs Rules ***
			//-- Delete batches   
			hqlDeleteBatches = 	"delete from BCPSitusRule bcp where " + 
						"bcp.batchId in (select distinct bcpb.batchId from BCPSitusRule bcpb, Batch b where " +
						"bcpb.batchId=b.batchId and b.batchStatusCode in ('ABSDM', 'D','SDM')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_situs_rules
			query = session.createQuery("select count(*) from BCPSitusRule");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE TB_BCP_SITUS_RULES";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}
			
			//-- *** Situs Rules - Text table ***
			//-- Delete batches   
			hqlDeleteBatches = 	"delete from BCPSitusRuleText bcp where " + 
						"bcp.id.batchId in (select distinct bcpb.id.batchId from BCPSitusRuleText bcpb, Batch b where " +
						"bcpb.id.batchId=b.batchId and b.batchStatusCode in ('ABSDM', 'D','SDM')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_situs_rules_text
			query = session.createQuery("select count(*) from BCPSitusRuleText");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_situs_rules_text";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}

			//-- *** TaxCode Rules ***
			//-- Delete batches
			hqlDeleteBatches = 	"delete from BCPTaxCodeRule bcp where " + 
						"bcp.batchId in (select distinct bcpb.batchId from BCPTaxCodeRule bcpb, Batch b where " +
						"bcpb.batchId=b.batchId and b.batchStatusCode in ('ABTCR', 'D','TCR')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_taxcode_rules
			query = session.createQuery("select count(*) from BCPTaxCodeRule");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_taxcode_rules";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}

			//-- *** TaxCode Rules - Text table ***
			//-- Delete batches
			hqlDeleteBatches = 	"delete from BCPTaxCodeRuleText bcp where " + 
						"bcp.id.batchId in (select distinct bcpb.id.batchId from BCPTaxCodeRuleText bcpb, Batch b where " +
						"bcpb.id.batchId=b.batchId and b.batchStatusCode in ('ABTCR', 'D','TCR')) ";
			query = session.createQuery(hqlDeleteBatches);
			rowUpdate = query.executeUpdate();
				
			//-- Truncate tb_bcp_taxcode_rules_text
			query = session.createQuery("select count(*) from BCPTaxCodeRuleText");
			if(((Long)query.uniqueResult())==0L){
				String truncateQuery = "TRUNCATE TABLE tb_bcp_taxcode_rules_text";
				query = session.createSQLQuery(truncateQuery);		
				rowUpdate = query.executeUpdate();
			}
			   
		   	tx.commit();
		} catch (Exception e) {
			e.printStackTrace();	
			if (tx != null) {
				tx.rollback();
			}
			return;
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void driverReferenceMaintBK() {
		Session session = null;
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			session = createLocalSession();
			tx = session.beginTransaction();

			// Get indexTablespace
			
			//String sqlxx = 	"SELECT DISTINCT dn.transDtlColName, ddc.descColumnName FROM DriverNames dn " + 
			//				"LEFT OUTER JOIN DataDefinitionColumn ddc ON ddc.dataDefinitionColumnPK.tableName = 'tb_purchtrans' " +
			//				"AND ddc.dataDefinitionColumnPK.columnName = dn.transDtlColName " +
			//				"WHERE dn.driverNamesPK.drvNamesCode IN ('T', 'L')";
			//String sqlxx = 	"SELECT DISTINCT dn.transDtlColName, ddc.descColumnName FROM DriverNames dn " + 
			//		", DataDefinitionColumn ddc WHERE ddc.dataDefinitionColumnPK.tableName = 'tb_purchtrans' " +
			//		"AND ddc.dataDefinitionColumnPK.columnName = dn.transDtlColName " +
			//		"and dn.driverNamesPK.drvNamesCode IN ('T', 'L')";
			
			Map<String,String> columnMap = new LinkedHashMap<String,String>();
			//List<String> columnlist = new ArrayList<String>();
			String sql = 	"SELECT DISTINCT dn.transDtlColName FROM DriverNames dn WHERE dn.driverNamesPK.drvNamesCode IN ('T', 'L')";
			int i = 0;
			query = session.createQuery(sql);
			List<String> objList = (List<String>) query.list();
			for(Iterator pairs  = objList.iterator(); pairs.hasNext();){
				String pair = (String) pairs.next();
				columnMap.put(pair, "");
			}
			
			List<String> desclist = new ArrayList<String>();
			sql = 	"SELECT DISTINCT ddc.dataDefinitionColumnPK.columnName, ddc.descColumnName FROM DataDefinitionColumn ddc WHERE ddc.dataDefinitionColumnPK.tableName = 'tb_purchtrans' and ddc.descColumnName is not null";
			String value = "";
			query = session.createQuery(sql);
			List<String[]> objArrayList = (List<String[]>) query.list();
			for(Iterator<?> pairs  = objArrayList.iterator(); pairs.hasNext();){
				Object[] pair = (Object[]) pairs.next();
				value = columnMap.get(pair[0].toString());
				if(value!=null){
					columnMap.put(pair[0].toString(), pair[1].toString());
				}
			}
			
			i = 0;
			for (String columnName : columnMap.keySet()) {
				String columnDesc = columnMap.get(columnName);
				i++;
				System.out.println(i + " : " + columnName + " , " + columnDesc);
			}
			
			
			/*
			i = 0;
			sql = 	
					
					"SELECT 'GL_COMPANY_NBR', td.glCompanyNbr, MIN(td.glCompanyName) " +
					"FROM TransactionDetail td " +
					"WHERE td.glCompanyNbr IS NOT NULL AND td.glCompanyNbr  IN " +
					"   (SELECT dr.driverReferencePK.driverValue " +
					"    FROM DriverReference dr " +
					"    WHERE dr.driverReferencePK.transDetailColName = 'GL_COMPANY_NBR') " +
					"GROUP BY 'GL_COMPANY_NBR', td.glCompanyNbr ";
					
					
					
				//	"SELECT DISTINCT ddc.dataDefinitionColumnPK.columnName, ddc.descColumnName FROM DataDefinitionColumn ddc WHERE ddc.dataDefinitionColumnPK.tableName = 'tb_purchtrans' and ddc.descColumnName is not null";
			value = "";
			query = session.createQuery(sql);
			List<String[]> objTestList = (List<String[]>) query.list();
			for(Iterator<?> pairs = objTestList.iterator(); pairs.hasNext();){
				Object[] pair = (Object[]) pairs.next();
				i++;
				
				System.out.println(i + " : " + pair[0].toString());
				System.out.println(i + " : " + pair[1].toString());
				System.out.println(i + " : " + pair[2].toString());
			}
			*/
			
			/*
			IF PURCHASING THEN
//loop thru T & L drivers
INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description)
SELECT 'GL_COMPANY_NBR', b.cono, b.code FROM (
SELECT a.GL_COMPANY_NBR as cono, MIN(a.GL_COMPANY_NAME) as code
FROM (
   SELECT td., tup.
   FROM tb_purchtrans td
   LEFT OUTER JOIN TB_TRANS_USER_PURCH tup ON tup.TRANS_USER_ID = td.purchtrans_id) a
WHERE a.GL_COMPANY_NBR IS NOT NULL AND a.GL_COMPANY_NBR NOT IN
   (SELECT driver_value
    FROM tb_driver_reference
    WHERE trans_dtl_column_name = 'GL_COMPANY_NBR')
GROUP BY GL_COMPANY_NBR) b
END

IF SALES THEN
//loop thru GSB drivers
INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description)
SELECT 'COMPANY_NBR', b.cono, b.code FROM (
SELECT a.COMPANY_NBR as cono, MIN(a.COMPANY_DESC) as code
FROM (
   SELECT st., tus.
   FROM TB_SALETRANS st
   LEFT OUTER JOIN TB_TRANS_USER_SALE tus ON tus.TRANS_USER_ID = st.SALETRANS_ID) a
WHERE a.COMPANY_NBR IS NOT NULL AND a.COMPANY_NBR NOT IN
   (SELECT driver_value
    FROM tb_driver_reference
    WHERE trans_dtl_column_name = 'COMPANY_NBR')
GROUP BY COMPANY_NBR) b
END
			*/
			
			
			
			//IF CLIENT IS AUTHORIZED TO SALES 
			String columNname = "COMPANY_NBR";
			String columDesc = "COMPANY_DESC";
			
			String sqlSalesDelete  = "delete from TB_DRIVER_REFERENCE where TRANS_DTL_COLUMN_NAME='" + columNname + "'";
			query = session.createSQLQuery(sqlSalesDelete);
			int countDelete = query.executeUpdate();

			String sqlSalesMinInsert = 
			"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
			"SELECT '" + columNname + "', b.cono, b.code FROM ( " +
			"		SELECT a." + columNname + " as cono, MIN(a." + columDesc + ") as code " +
			"		FROM ( " +
			"		   SELECT st.*, tus.* " +
			"		   FROM TB_SALETRANS st " +
			"		   LEFT OUTER JOIN TB_TRANS_USER_SALE tus ON tus.TRANS_USER_ID = st.SALETRANS_ID) a " +
			"		WHERE a." + columNname + " IS NOT NULL AND a." + columNname + " NOT IN " +
			"		   (SELECT driver_value " +
			"		    FROM tb_driver_reference " +
			"		    WHERE trans_dtl_column_name = '" + columNname + "') " +
			"		GROUP BY " + columNname + ") b ";
			
			String sqlSalesNullInsert = 
					"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
					"SELECT '" + columNname + "', b.cono, b.code FROM ( " +
					"		SELECT a." + columNname + " as cono, NULL as code " +
					"		FROM ( " +
					"		   SELECT st.*, tus.* " +
					"		   FROM TB_BILLTRANS st " +
					"		   LEFT OUTER JOIN TB_TRANS_USER_SALE tus ON tus.TRANS_USER_ID = st.BILLTRANS_ID) a " +
					"		WHERE a." + columNname + " IS NOT NULL AND a." + columNname + " NOT IN " +
					"		   (SELECT driver_value " +
					"		    FROM tb_driver_reference " +
					"		    WHERE trans_dtl_column_name = '" + columNname + "') " +
					"		GROUP BY " + columNname + ") b ";
			
			
			
			
			query = session.createSQLQuery(sqlSalesNullInsert);
			int countInsert = query.executeUpdate();
			
			tx.commit();
			
			/*
			query = session.createQuery(sql);
			List<Object[]> objList = (List<Object[]>) query.list();
			for(Iterator<?> pairs  = objList.iterator(); pairs.hasNext();){
				Object[] pair = (Object[]) pairs.next();
				
				System.out.println("pair[0].toString() : " + pair[0].toString());
				System.out.println("pair[1].toString() : " + pair[1].toString());

			}
			*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			closeLocalSession(session);
			session = null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void driverReferenceMaint() {
		String sqlPurchaseMinInsert = 
				"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
				"SELECT ':columNname', b.cono, b.code FROM ( " +
				"		SELECT a.:columNname as cono, MIN(a.:columDesc) as code " +
				"		FROM ( " +
				"		   SELECT td.*, tup.* " +
				"		   FROM tb_purchtrans td " +
				"		   LEFT OUTER JOIN TB_TRANS_USER_PURCH tup ON tup.TRANS_USER_ID = td.purchtrans_id) a " +
				"		WHERE a.:columNname IS NOT NULL AND a.:columNname NOT IN " +
				"		   (SELECT driver_value " +
				"		    FROM tb_driver_reference " +
				"		    WHERE trans_dtl_column_name = ':columNname') " +
				"		GROUP BY :columNname) b ";
				
		String sqlPurchaseNullInsert = 
				"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
				"SELECT ':columNname', b.cono, b.code FROM ( " +
				"		SELECT a.:columNname as cono, NULL as code " +
				"		FROM ( " +
				"		   SELECT td.*, tup.* " +
				"		   FROM tb_purchtrans td " +
				"		   LEFT OUTER JOIN TB_TRANS_USER_PURCH tup ON tup.TRANS_USER_ID = td.purchtrans_id) a " +
				"		WHERE a.:columNname IS NOT NULL AND a.:columNname NOT IN " +
				"		   (SELECT driver_value " +
				"		    FROM tb_driver_reference " +
				"		    WHERE trans_dtl_column_name = ':columNname') " +
				"		GROUP BY :columNname) b ";
		
		String sqlSalesMinInsert = 
				"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
				"SELECT ':columNname', b.cono, b.code FROM ( " +
				"		SELECT a.:columNname as cono, MIN(a.:columDesc) as code " +
				"		FROM ( " +
				"		   SELECT st.*, tus.* " +
				"		   FROM TB_BILLTRANS st " +
				"		   LEFT OUTER JOIN TB_TRANS_USER_SALE tus ON tus.TRANS_USER_ID = st.BILLTRANS_ID) a " +
				"		WHERE a.:columNname IS NOT NULL AND a.:columNname NOT IN " +
				"		   (SELECT driver_value " +
				"		    FROM tb_driver_reference " +
				"		    WHERE trans_dtl_column_name = ':columNname') " +
				"		GROUP BY :columNname) b ";
				
		String sqlSalesNullInsert = 
				"INSERT INTO tb_driver_reference (trans_dtl_column_name, driver_value, driver_description) " +
				"SELECT ':columNname', b.cono, b.code FROM ( " +
				"		SELECT a.:columNname as cono, NULL as code " +
				"		FROM ( " +
				"		   SELECT st.*, tus.* " +
				"		   FROM TB_BILLTRANS st " +
				"		   LEFT OUTER JOIN TB_TRANS_USER_SALE tus ON tus.TRANS_USER_ID = st.BILLTRANS_ID) a " +
				"		WHERE a.:columNname IS NOT NULL AND a.:columNname NOT IN " +
				"		   (SELECT driver_value " +
				"		    FROM tb_driver_reference " +
				"		    WHERE trans_dtl_column_name = ':columNname') " +
				"		GROUP BY :columNname) b ";
		
		String qryLicense = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'LICENSEKEY' AND o.codePK.optionTypeCode = 'SYSTEM' AND o.codePK.userCode = 'SYSTEM' ";  
		License license  = null;
		List<String> alist = getJpaTemplate().find(qryLicense);
		if ((alist != null) && (alist.size() > 0)) {
	    	license = new License((String)alist.get(0));
		}
				
		Session session = null;
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			session = createLocalSession();
			tx = session.beginTransaction();
			
			String truncateQuery = "TRUNCATE TABLE tb_driver_reference";
			query = session.createSQLQuery(truncateQuery);		
			query.executeUpdate();		
			
			//IF CLIENT IS AUTHORIZED TO PURCHASING 
			if((license != null && license.hasPurchaseLicense())) {
				String sqlPurchase = 	"SELECT DISTINCT dn.trans_dtl_column_name, ddc.desc_column_name " +
										"FROM tb_driver_names dn " +
										"LEFT OUTER JOIN tb_data_def_column ddc ON ddc.table_name = 'tb_purchtrans' AND ddc.column_name = dn.trans_dtl_column_name " +
										"WHERE dn.driver_names_code IN ('T', 'L')";
				String columNname = null;
				String columDesc = 	null;
				query = session.createSQLQuery(sqlPurchase);
				List<String[]> objArrayList = (List<String[]>) query.list();
				for(Iterator<?> pairs  = objArrayList.iterator(); pairs.hasNext();){
					Object[] pair = (Object[]) pairs.next();
					columNname = null;
					columDesc = null;
					if(pair[0]!=null){
						columNname = pair[0].toString();
					}
					if(pair[1]!=null){
						columDesc = pair[1].toString();
					}
					
					if(columDesc!=null){
						int icount = session.createSQLQuery(sqlPurchaseMinInsert.replaceAll(":columNname", columNname).replaceAll(":columDesc", columDesc)).executeUpdate();
					}
					else{
						int icount = session.createSQLQuery(sqlPurchaseNullInsert.replaceAll(":columNname", columNname)).executeUpdate();
					}
				}
			}
			
			//IF CLIENT IS AUTHORIZED TO SALES
			if((license != null && license.hasSaleLicense())) {
				String sqlSales = 	"SELECT DISTINCT dn.trans_dtl_column_name, ddc.desc_column_name " +
									"FROM tb_driver_names dn " +
									"LEFT OUTER JOIN tb_data_def_column ddc ON ddc.table_name = 'TB_BILLTRANS' AND ddc.column_name = dn.trans_dtl_column_name " +
									"WHERE dn.driver_names_code = 'GSB'";
				String columNname = null;
				String columDesc = 	null;
				query = session.createSQLQuery(sqlSales);
				List<String[]> objArrayList = (List<String[]>) query.list();
				for(Iterator<?> pairs  = objArrayList.iterator(); pairs.hasNext();){
					Object[] pair = (Object[]) pairs.next();
					columNname = null;
					columDesc = null;
					if(pair[0]!=null){
						columNname = pair[0].toString();
					}
					if(pair[1]!=null){
						columDesc = pair[1].toString();
					}
					
					if(columDesc!=null){
						int icount = session.createSQLQuery(sqlSalesMinInsert.replaceAll(":columNname", columNname).replaceAll(":columDesc", columDesc)).executeUpdate();
					}
					else{
						int icount = session.createSQLQuery(sqlSalesNullInsert.replaceAll(":columNname", columNname)).executeUpdate();
					}
				}
			}

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			closeLocalSession(session);
			session = null;
		}
		
	}

	public List<BatchMetadata> loadValidMetadata(boolean isPurchasingSelected) {

		String listCodeMessage = (isPurchasingSelected) ? "1" : "2";
		List<BatchMetadata> result = new ArrayList<BatchMetadata>();
		Session localSession = createLocalSession();
		try {
			DetachedCriteria listCodesCriteria = DetachedCriteria.forClass(ListCodes.class)
					.add(Restrictions.eq("listCodesPK.codeTypeCode", "BATCHTYPE"))
					.add(Restrictions.or(Restrictions.eq("message", "3"), Restrictions.eq("message", listCodeMessage)))
					.setProjection(Property.forName("listCodesPK.codeCode"));

			result = localSession.createCriteria(BatchMetadata.class)
					.add(Property.forName("batchTypeCode").in(listCodesCriteria))
					.add(Expression.disjunction()
									.add(Restrictions.isNotNull("uvc01Desc"))
									.add(Restrictions.isNotNull("uvc02Desc"))
									.add(Restrictions.isNotNull("uvc03Desc"))
									.add(Restrictions.isNotNull("uvc04Desc"))
									.add(Restrictions.isNotNull("uvc05Desc"))
									.add(Restrictions.isNotNull("uvc06Desc"))
									.add(Restrictions.isNotNull("uvc07Desc"))
									.add(Restrictions.isNotNull("uvc08Desc"))
									.add(Restrictions.isNotNull("uvc09Desc"))
									.add(Restrictions.isNotNull("uvc10Desc"))
									.add(Restrictions.isNotNull("unu01Desc"))
									.add(Restrictions.isNotNull("unu02Desc"))
									.add(Restrictions.isNotNull("unu03Desc"))
									.add(Restrictions.isNotNull("unu04Desc"))
									.add(Restrictions.isNotNull("unu05Desc"))
									.add(Restrictions.isNotNull("unu06Desc"))
									.add(Restrictions.isNotNull("unu07Desc"))
									.add(Restrictions.isNotNull("unu08Desc"))
									.add(Restrictions.isNotNull("unu09Desc"))
									.add(Restrictions.isNotNull("unu10Desc"))
									.add(Restrictions.isNotNull("uts01Desc"))
									.add(Restrictions.isNotNull("uts02Desc"))
									.add(Restrictions.isNotNull("uts03Desc"))
									.add(Restrictions.isNotNull("uts04Desc"))
									.add(Restrictions.isNotNull("uts05Desc"))
									.add(Restrictions.isNotNull("uts06Desc"))
									.add(Restrictions.isNotNull("uts07Desc"))
									.add(Restrictions.isNotNull("uts08Desc"))
									.add(Restrictions.isNotNull("uts09Desc"))
									.add(Restrictions.isNotNull("uts10Desc"))
					)
					.list();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		} finally {
			closeLocalSession(localSession);
		}
		return result;
	}
	
	public boolean hasExtractedTransactions(List<Long> ids){		
		//SELECT count(*) FROM tb_purchtrans td
		//WHERE (td.gl_extract_batch_no IS NOT NULL AND td.gl_extract_batch_no > 0)
		//AND td.process_batch_no IN {selected batch ids};
		 
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(PurchaseTransaction.class);	
		criteria.add(Restrictions.isNotNull("glExtractBatchNo"));
		criteria.add(Restrictions.gt("glExtractBatchNo", new Long(0L)));
		criteria.add(Restrictions.in("processBatchNo", ids));	
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
			
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		if(returnLong>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkRunningBatches() {
		boolean batchesRunning = false;
		Long runningCount = 0L;
		try {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();

		Query q = entityManager.createQuery("SELECT count(b) from BatchMaintenance b where b.batchStatusCode like 'X%'");
		
		runningCount = (Long) q.getSingleResult();
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(runningCount > 0)
			batchesRunning = true; 
		
		return batchesRunning;
	}
}
