package com.ncsts.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import oracle.jdbc.OracleTypes;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.common.LogMemory;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.services.PurchaseTransactionService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
/**
 * @author Paul Govindan
 *
 */

public class JPATaxCodeDetailDAO  extends JPAGenericDAO<TaxCodeDetail,Long> implements TaxCodeDetailDAO {
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;

	@SuppressWarnings("unchecked")
	@Transactional
	public List<String[]> findTaxCodes() throws DataAccessException{
		List<String[]> listTaxCode=getJpaTemplate().find("select distinct td.taxcodeStateCode,td.taxcodeTypeCode from TaxCodeDetail td");
		return listTaxCode;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeTypeForState(String stateCode) throws DataAccessException{
		List<String> listTaxCode = getJpaTemplate().find("select distinct td.taxcodeTypeCode from TaxCodeDetail td where td.taxcodeStateCode = ?1 order by td.taxcodeTypeCode", stateCode);
		return listTaxCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeCounty(String countryCode, String stateCode) throws DataAccessException{
		List<String> listCounty = getJpaTemplate().find("select distinct upper(td.taxCodeCounty)  from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeCountryCode = ?2 order by upper(td.taxCodeCounty) ", stateCode, countryCode);
		return listCounty;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeCity(String countryCode, String stateCode) throws DataAccessException{
		List<String> listCity = getJpaTemplate().find("select distinct upper(td.taxCodeCity) from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeCountryCode = ?2  order by upper(td.taxCodeCity) ", stateCode, countryCode);
		return listCity;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeStj(String countryCode, String stateCode) throws DataAccessException{
		List<String> listStj = getJpaTemplate().find("select distinct upper(td.taxCodeStj) from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeCountryCode = ?2  order by upper(td.taxCodeStj) ", stateCode, countryCode);
		return listStj;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxCodeDetail> findTaxCodeDetailListByCountrCity(String countryCode, String stateCode, String taxcodeCode, String taxcodeCounty, String taxcodeCity, String description) throws DataAccessException{
		StringBuffer query = new StringBuffer();
		String glue = "";
		query.append("select td.* from tb_taxcode_detail td inner join  tb_taxcode tc on ");
		query.append("((tc.taxcode_code = td.taxcode_code) and (tc.taxcode_type_code=td.taxcode_type_code))");
		query.append(" where 1=1 ");

		if ((countryCode != null) && !countryCode.equals("")) {
			query.append(glue + " and td.taxcode_country_code = " + countryCode);
		}
		
		if ((stateCode != null) && !stateCode.equals("")) {
			query.append(glue + " and td.taxcode_state_code = " + stateCode);
		}
		if ((taxcodeCode != null) && !taxcodeCode.equals("")) {
			query.append(glue + " and upper(td.taxcode_code) like " + taxcodeCode.toUpperCase() + "%");
		}
		
		if ((taxcodeCounty != null) && !taxcodeCounty.equals("")) {
			query.append(glue + " and upper(td.taxcode_county) = " + taxcodeCounty.toUpperCase());
		}
		
		if ((taxcodeCity != null) && !taxcodeCity.equals("")) {
			query.append(glue + " and upper(td.taxcode_city) = " + taxcodeCity.toUpperCase());
		}
		
		if ((description != null) && !description.equals("")) {
			query.append(glue + " and upper(tc.description) like " + description.toUpperCase() + "%");
		}
	
		query.append(" order by td.taxcode_code"); 
		
		Session session = createLocalSession();
		org.hibernate.SQLQuery q = session.createSQLQuery(query.toString());
		q.addEntity(TaxCodeDetail.class);
		
		List<TaxCodeDetail> returnTaxCodeDetail = q.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnTaxCodeDetail;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCodeDetail> findTaxCodeCode(String stateCode, String taxCodeType) throws DataAccessException{
		List<TaxCodeDetail> listTaxCode = getJpaTemplate().find("select td from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeTypeCode = ?2 order by td.taxcodeStateCode, td.taxcodeTypeCode" , stateCode, taxCodeType);
		return listTaxCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TaxCode> findTaxCodeList(String stateCode, String taxCodeType, String pattern) throws DataAccessException {
		StringBuffer query = new StringBuffer();
		String glue = "";
		query.append("select tc.* from tb_taxcode tc inner join  tb_taxcode_detail td on ");
		query.append("((tc.taxcode_code = td.taxcode_code) and (tc.taxcode_type_code=td.taxcode_type_code))");
		query.append(" where ");

		if (((stateCode != null) && !stateCode.equals("")) || 
			((taxCodeType != null) && !taxCodeType.equals(""))) {
			if ((stateCode != null) && !stateCode.equals("")) {
				query.append(glue + " td.taxcode_state_code = :state");
				glue = " and";
			}
			if ((taxCodeType != null) && !taxCodeType.equals("")) {
				query.append(glue + " td.taxcode_type_code = :type");
				glue = " and";
			}
			glue = " and";
		}

		if ((pattern != null) && !pattern.equals("")) {
			query.append(glue + " upper(td.taxcode_code) like :pattern");
		}
		query.append(" order by tc.taxcode_code"); 
		
		Session session = createLocalSession();
		org.hibernate.SQLQuery q = session.createSQLQuery(query.toString());
		q.addEntity(TaxCode.class);
		if ((stateCode != null) && !stateCode.equals("")) {
			q.setParameter("state", stateCode);
		}
		if ((taxCodeType != null) && !taxCodeType.equals("")) {
			q.setParameter("type", taxCodeType);
		}
		if ((pattern != null) && !pattern.equals("")) {
			q.setParameter("pattern", pattern.toUpperCase() + "%");
		}
		
		List<TaxCode> returnTaxCode = q.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnTaxCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TaxCode> findTaxCodeListByCountry(String countryCode, String stateCode, String taxCodeType, String pattern) throws DataAccessException {
		StringBuffer query = new StringBuffer();
		String glue = "";
		query.append("select tc.* from tb_taxcode tc inner join  tb_taxcode_detail td on ");
		query.append("((tc.taxcode_code = td.taxcode_code) and (tc.taxcode_type_code=td.taxcode_type_code))");
		query.append(" where ");

		if (((countryCode != null) && !countryCode.equals("")) ||
			((stateCode != null) && !stateCode.equals("")) || 
			((taxCodeType != null) && !taxCodeType.equals(""))) {
			
			if ((countryCode != null) && !countryCode.equals("")) {
				query.append(glue + " td.taxcode_country_code = :country");
				glue = " and";
			}
			
			if ((stateCode != null) && !stateCode.equals("")) {
				query.append(glue + " td.taxcode_state_code = :state");
				glue = " and";
			}
			if ((taxCodeType != null) && !taxCodeType.equals("")) {
				query.append(glue + " td.taxcode_type_code = :type");
				glue = " and";
			}
			glue = " and";
		}

		if ((pattern != null) && !pattern.equals("")) {
			query.append(glue + " upper(td.taxcode_code) like :pattern");
		}
		query.append(" order by tc.taxcode_code"); 
		
		Session session = createLocalSession();
		org.hibernate.SQLQuery q = session.createSQLQuery(query.toString());
		q.addEntity(TaxCode.class);
		
		if ((countryCode != null) && !countryCode.equals("")) {
			q.setParameter("country", countryCode);
		}
		if ((stateCode != null) && !stateCode.equals("")) {
			q.setParameter("state", stateCode);
		}
		if ((taxCodeType != null) && !taxCodeType.equals("")) {
			q.setParameter("type", taxCodeType);
		}
		if ((pattern != null) && !pattern.equals("")) {
			q.setParameter("pattern", pattern.toUpperCase() + "%");
		}
		
		List<TaxCode> returnTaxCode = q.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnTaxCode;
	}
 
	@SuppressWarnings("unchecked")
	@Transactional
	public TaxCode findTaxCode(TaxCodePK id) throws DataAccessException{
		TaxCode taxCode = getJpaTemplate().find(TaxCode.class, id); 
		return taxCode;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCodeDetail> findTaxCodeDetailList(String stateCountry, String stateCode, String taxCodeType, String taxcodeCode) throws DataAccessException{
		List<TaxCodeDetail> listTaxCode = getJpaTemplate().find(
				"select td from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeTypeCode = ?2 and td.taxcodeCode = ?3 and td.taxcodeCountryCode = ?4" + 
				" order by td.taxcodeCode", 
				stateCode, taxCodeType, taxcodeCode, stateCountry);
		return listTaxCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCodeDetail> findTaxCodeDetailListByTaxCode(String taxcodeCode) throws DataAccessException{
		List<TaxCodeDetail> listTaxCode = getJpaTemplate().find(
				"select td from TaxCodeDetail td where td.taxcodeCode = ?1 " + 
				" order by td.taxcodeCode", taxcodeCode);
		return listTaxCode;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCode> findTaxCodeListByTaxCode(String taxcodeCode) throws DataAccessException{
		List<TaxCode> listTaxCode = getJpaTemplate().find(
				"select tc from TaxCode tc where tc.taxCodePK.taxcodeCode = ?1 " + 
				" order by tc.taxCodePK.taxcodeCode", taxcodeCode);
		return listTaxCode;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCode> findTaxCodeListByStateCode(String stateCode, String taxCodeType) throws DataAccessException{
		//0000024: Add Federal Level into PinPoint, format here AL-US
		String state = stateCode.substring(0,2);
		String country = stateCode.substring(3,5);
		List<TaxCode> listTaxCode = getJpaTemplate().find(
				"select tc from TaxCode tc  where tc.taxCodePK.taxcodeCode in " +
				"(select td.taxcodeCode from TaxCodeDetail td where td.taxcodeStateCode = ?1 and td.taxcodeCountryCode = ?2 and td.taxcodeTypeCode = ?3) " +
				"and tc.taxCodePK.taxcodeTypeCode = ?4 " +
				"order by tc.taxCodePK.taxcodeCode", 
				state, country, taxCodeType, taxCodeType);
		return listTaxCode;
	}



	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCode> findByTaxCodeCode(String taxCodeTypeCode) throws DataAccessException {
		return getJpaTemplate().find("Select tc from TaxCode tc where tc.taxCodePK.taxcodeTypeCode = ?1 order by tc.taxCodePK.taxcodeCode",
		taxCodeTypeCode);
	}
	
	/**
	 * @author Anand
	 * taxability use case - taxcode tab
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCode> getAllTaxCode() throws DataAccessException {
		return getJpaTemplate().find("Select tc from TaxCode tc  where tc.activeFlag = '1' order by tc.taxCodePK.taxcodeCode");
	}
	
	@Transactional
    public boolean isTaxCodeUsed(TaxCode taxCode) throws DataAccessException {
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	boolean isUsed = true;
    	
    	try {
    		query = session.createSQLQuery("SELECT COUNT(*) FROM TB_TAX_MATRIX WHERE THEN_TAXCODE_CODE = :taxcodeCode OR ELSE_TAXCODE_CODE = :taxcodeCode");
    		query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());
    		List<?> result = query.list();
    		long count = ((BigDecimal) result.get(0)).longValue();
    		
    		if(count == 0) {
    			isUsed = false;	
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return isUsed;
    }
	
	@Transactional
    public void removeTaxCode(TaxCode taxCode) throws IllegalAccessException {
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();
    	org.hibernate.Query query;
    	
    	boolean removed = false;
    	try {
    		query = session.createSQLQuery("SELECT COUNT(*) FROM TB_TAX_MATRIX WHERE THEN_TAXCODE_CODE = :taxcodeCode OR ELSE_TAXCODE_CODE = :taxcodeCode");
    		query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());
    		List<?> result = query.list();
    		long count = ((BigDecimal) result.get(0)).longValue();
    		
    		if(count == 0) {
    			query = session.createSQLQuery(
					"delete from TB_REFERENCE_DETAIL " +
    				"where TAXCODE_DETAIL_ID in " +
    				"(select TAXCODE_DETAIL_ID from TB_TAXCODE_DETAIL where TAXCODE_CODE = :taxcodeCode)");
    			query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());
    			int rows = query.executeUpdate();
    			logger.debug(rows + " reference detail rows deleted");
    			
    			query = session.createSQLQuery(
					"delete from TB_TAXCODE_DETAIL where TAXCODE_CODE = :taxcodeCode");
    			query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());
    			rows = query.executeUpdate();
    			logger.debug(rows + " taxcode detail rows deleted");

    			query = session.createSQLQuery(
					"delete from TB_TAXCODE where TAXCODE_CODE = :taxcodeCode");
				query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());
				rows = query.executeUpdate();
				logger.debug(rows + " taxcode rows deleted");
    			
    			t.commit();
    			
    			removed = true;
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
			if (t != null) {
				logger.debug("Rolling back");
				t.rollback();
			}
    	}
		
		closeLocalSession(session);
		session=null;
		
		if(!removed) {
			throw new IllegalAccessException("Taxcode is in use");
		}
    }
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(TaxCodeDetail taxCodeDetail)throws DataAccessException {
		TaxCodeDetail obj = findById(taxCodeDetail.getId());
		if(obj != null) {
			obj.setActiveFlag(taxCodeDetail.getActiveFlag());
			getJpaTemplate().persist(obj);
			getJpaTemplate().flush();
		} else {
			logger.warn("Object not found for delete!");
		}
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void updateBackdate(TaxCodeDetail taxCodeDetail)throws DataAccessException {
		TaxCodeDetail obj = findById(taxCodeDetail.getId());
		if(obj != null) {
			obj.setEffectiveDate(taxCodeDetail.getEffectiveDate());
			getJpaTemplate().persist(obj);
			getJpaTemplate().flush();
		} else {
			logger.warn("Object not found for delete!");
		}
	}

	@Transactional
	public void updateDetail(TaxCodeDetail taxCodeDetail) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(taxCodeDetail);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
    
	@SuppressWarnings({"unchecked","deprecation"})
	@Transactional(readOnly = false)
    public void updateDetail(TaxCode taxCode, Set<String> states,
			Map<String, Long> stateJurisdictionMap) {
		// First, remove all the states that no longer exist
		Session session = createLocalSession();
		Transaction t = null;
		org.hibernate.Query query;
		try {
			t = session.beginTransaction();
			
			//Do clean-up
			query = session.createQuery("from TaxCodeDetail d where d.taxcodeCode = :taxcodeCode");
			query.setParameter("taxcodeCode", taxCode.getTaxCodePK().getTaxcodeCode());

			Map<String, TaxCodeDetail> curDetailOri = new HashMap<String, TaxCodeDetail>();
			Map<String, TaxCodeDetail> curDetail = new HashMap<String, TaxCodeDetail>();
			for (TaxCodeDetail detail : (List<TaxCodeDetail>) query.list()) {
				String key = detail.getTaxcodeStateCode()+"-"+detail.getTaxcodeCountryCode();
				if(!states.contains(key)){
					remove(detail);
				}
				else{
					curDetail.put(key, detail);
				}
			}
						
			for (String state : states) {
				// Now, create a record for each state that doesn't already
				// exist
				TaxCodeDetail detail = (curDetail.containsKey(state)) ? curDetail
						.get(state)
						: new TaxCodeDetail();
						
				String stateStr = "No";
				String countryStr = "No";		
				String[] stateArr = state.split("-");
				if(stateArr.length==2){
					stateStr = stateArr[0];
					countryStr = stateArr[1];
				}	
						
				detail.setTaxcodeStateCode(stateStr);
				detail.setTaxcodeCountryCode(countryStr);
				detail.setTaxcodeCode(taxCode.getTaxcodeCode());
				detail.setTaxcodeTypeCode(null);
				//TODO: other new fields
				
				if (curDetail.containsKey(state)) {
					update(detail);
				} else {
					save(detail);
				}				
			}
			// All done
			t.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (t != null) {
				logger.debug("Rolling back");
				t.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
    }
	
	
	/**
	 * @author Anand
	 * @return List<ListCode>
	 * *This method will return the list of ListCode for taxCode tab of taxability matrix
	 * select * from TB_List_Code where code_type_code = 'TCTYPE' and 
	 * CODE_CODE in (select TAXCODE_TYPE_CODE from TB_TAXCODE where TAXCODE_CODE = 'COMPSVC')
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> findListCodeForTaxCode(String taxCode) throws DataAccessException {
		return getJpaTemplate().find("Select lc from ListCodes lc where lc.id.codeTypeCode = 'TCTYPE' and lc.id.codeCode in " +
			"(Select tc.id.taxcodeTypeCode from TaxCode tc where tc.taxCodePK.taxcodeCode = ?1) order by lc.id.codeCode",taxCode);
	}
  
	/**
	 * @author Anand
	 * @return List<ListCode>
	 */ 
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> findListCodeForState(String stateCode) throws DataAccessException {
		//0000024: Add Federal Level into PinPoint, format here AL-US
		String state = stateCode.substring(0,2);
		String country = stateCode.substring(3,5);
		return getJpaTemplate().find("Select lc from ListCodes lc where lc.id.codeTypeCode = 'TCTYPE' and lc.id.codeCode in " +
			"(Select tcd.taxcodeTypeCode from TaxCodeDetail tcd where tcd.taxcodeStateCode = ?1 and tcd.taxcodeCountryCode = ?2) order by lc.id.codeCode",state, country);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDetail> getDocumentReferences(String taxCodeType, String taxCode, String taxStateCode)
		throws DataAccessException {
		//0000024: Add Federal Level into PinPoint, format here AL-US
		String state = taxStateCode.substring(0,2);
		String country = taxStateCode.substring(3,5);

		Query q = entityManager.createQuery("FROM ReferenceDetail dtl "
				+ "inner join fetch dtl.id.taxcodeDetail "
				+ "inner join fetch dtl.id.refDoc "
				+ "where dtl.id.taxcodeDetail in ("
				+ "select t from TaxCodeDetail t"
				+ " where t.taxcodeTypeCode = :taxCodeType and t.taxcodeCode = :taxCode and t.taxcodeStateCode = :taxStateCode and t.taxcodeCountryCode = :taxCountryCode)"
				+ " order by dtl.id.refDoc.refDocCode");
		/*
		String sql = "SELECT B.* FROM TB_REFERENCE_DOCUMENT B, TB_REFERENCE_DETAIL A " +
			"WHERE A.REFERENCE_DOCUMENT_CODE = B.REFERENCE_DOCUMENT_CODE(+) AND A.TAXCODE_DETAIL_ID in " +
			"(select TAXCODE_DETAIL_ID from TB_TAXCODE_Detail where " + 
			"TAXCODE_TYPE_CODE = ? and TAXCODE_CODE = ? and TAXCODE_STATE_CODE = ?) ORDER BY B.REFERENCE_DOCUMENT_CODE";
		Query q = entityManager.createNativeQuery(sql);
		*/ 	  
		q.setParameter("taxCodeType",taxCodeType);
		q.setParameter("taxCode",taxCode);
		q.setParameter("taxStateCode",state);
		q.setParameter("taxCountryCode",country);
		return (List<ReferenceDetail>) q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDetail> getReferenceDetails(Long taxcodeDetailId)
		throws DataAccessException {
		Query q = entityManager.createQuery("FROM ReferenceDetail dtl "
				+ "inner join fetch dtl.id.taxcodeDetail "
				+ "inner join fetch dtl.id.refDoc "
				+ "where dtl.id.taxcodeDetail.taxcodeDetailId = :taxcodeDetailId");
		q.setParameter("taxcodeDetailId",taxcodeDetailId);
		return (List<ReferenceDetail>) q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDocument> getAllDocumentReferences() throws DataAccessException {
		return (List<ReferenceDocument>) getJpaTemplate().find("SELECT d FROM ReferenceDocument d order by d.refDocCode");
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDocument> getDocumentReferencesByTypeAndCode(String refType, String code) throws DataAccessException {
		StringBuffer sb = new StringBuffer("SELECT d FROM ReferenceDocument d ");
		
		Map<String, String> params = new HashMap<String, String>();
		
		String where = "WHERE ";
		if(StringUtils.hasText(refType)) {
			sb.append(where).append("upper(refTypeCode) = :refTypeCode ");
			where = "";
			params.put("refTypeCode", refType.toUpperCase());
		}
		
		if(StringUtils.hasText(code)) {
			if(where.length() > 0) {
				sb.append(where);
				where = "";
			}
			else {
				sb.append("AND ");
			}
			sb.append("upper(refDocCode) like :refDocCode ");
			params.put("refDocCode",code.toUpperCase());
		}
		sb.append("order by d.refDocCode");
		
		return (List<ReferenceDocument>) getJpaTemplate().findByNamedParams(sb.toString(), params);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ReferenceDocument> getDocumentReferencesByTaxCodeId(Long id) throws DataAccessException {
		String sql = "SELECT d FROM  ReferenceDocument d " +
		"WHERE  d.refDocCode in (select dtl.id.refDoc.refDocCode  from ReferenceDetail dtl where dtl.id.taxcodeDetail.taxcodeDetailId = ?1 )";
		return (List<ReferenceDocument>) getJpaTemplate().find(sql, id);
	}
	
	@Transactional(readOnly = false)
    public void addDocumentReference(ReferenceDetail detail) {
    	entityManager.persist(detail);
    }
    
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeDocumentReference(ReferenceDetail detail) {
		ReferenceDetail obj = getJpaTemplate().find(ReferenceDetail.class, detail.getReferenceDetailPK());
		if(obj != null) {
			getJpaTemplate().remove(obj);
		} else {
			logger.warn("Object not found for deletion");
		}
    }

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<CCHCode> getAllCCHCode() throws DataAccessException {
		List<CCHCode> result = getJpaTemplate().find("SELECT cch FROM CCHCode cch WHERE " +
				"(cch.id.fileName = 'DETAIL') AND (cch.id.fieldName= 'TAXCAT') ORDER BY cch.id.code ASC");
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<CCHTaxMatrix> getAllCCHGroup() throws DataAccessException {
		List<CCHTaxMatrix> result = getJpaTemplate().find("SELECT cch FROM CCHTaxMatrix cch WHERE " +
				"(cch.id.recType = 'G') ORDER BY cch.id.groupCode ASC");
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<CCHTaxMatrix> getAllCCHGroupItems(String groupCode) throws DataAccessException {
		return getJpaTemplate().find("Select cchMatrix from CCHTaxMatrix cchMatrix where cchMatrix.id.recType = 'I' and cchMatrix.id.groupCode=?1 order by cchMatrix.id.item", groupCode);
	}
	
	public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxCodeState, String taxCodeType,
			String taxCode, String description, Long detailId){
		
		return getTaxcodeDetailsByCountry(null, taxCodeState, taxCodeType, taxCode, description, detailId);
	}
	
	//Midtier project code modified - january 2009
	@Transactional
	public List<TaxCodeDetailDTO> getTaxcodeDetailsByCountry(String taxCodeCountry, String taxCodeState, String taxCodeType,
			String taxCode, String description, Long detailId){
		logger.debug("inside JPA ");
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		StringBuffer sql = new StringBuffer();
		
		if(taxCodeCountry != null && !taxCodeCountry.equals(""))
			sql.append("AND (   TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE = '"+taxCodeCountry+"') ");
		if(taxCodeState != null && !taxCodeState.equals(""))
			sql.append("AND (   TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = '"+taxCodeState+"') ");
		if(taxCodeType != null && !taxCodeType.equals(""))
					sql.append("AND (  TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = '"+taxCodeType+"' ) ");
		if(taxCode != null && !taxCode.equalsIgnoreCase(""))
							sql.append("AND (  UPPER (TB_TAXCODE_DETAIL.TAXCODE_CODE) LIKE UPPER ('"+taxCode+"' || '%') ) ");
		if(description != null && !description.equals(""))
									sql.append("AND (   UPPER (TB_TAXCODE.DESCRIPTION) LIKE UPPER ('"+description+"' || '%')) ");
		if(detailId!= null && detailId > 0)
										sql.append("AND (  TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID = "+detailId+") ");
		sql.append("ORDER BY TB_TAXCODE_DETAIL.TAXCODE_CODE ASC");
		
		logger.debug("and clause: "+ sql.toString());
		List<TaxCodeDetailDTO> taxCodeDetailDTO= new ArrayList<TaxCodeDetailDTO>();
		Connection con = null;
		try{
			//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				String sqlQuery="SELECT "+
  " TB_TAXCODE_DETAIL.TAXCODE_CODE, "+ 
  " TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE, "+
  " TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE, "+
  " TB_TAXCODE_DETAIL.TAXCODE_DETAIL_ID, "+
  " tstate.NAME, TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE, "+
  " tlist.DESCRIPTION AS TAXCODE_TYPE_DESCRIPTION, "+
  " tcode.DESCRIPTION AS TAXCODE_DESCRIPTION, tcode.COMMENTS, "+
  " tcode.ERP_TAXCODE, TB_TAXCODE_DETAIL.TAXTYPE_CODE, "+
  " tlist1.DESCRIPTION AS TAX_TYPE_DESCRIPTION, "+
  " TB_TAXCODE_DETAIL.JURISDICTION_ID, tjur.GEOCODE, "+
  " tjur.CITY, tjur.COUNTY, tjur.STATE, "+
  " tjur.ZIP "+
  " FROM (("+
  " ((TB_TAXCODE_DETAIL INNER JOIN TB_TAXCODE_STATE tstate ON TB_TAXCODE_DETAIL.TAXCODE_STATE_CODE = tstate.TAXCODE_STATE_CODE AND "+
  " TB_TAXCODE_DETAIL.TAXCODE_COUNTRY_CODE = tstate.TAXCODE_COUNTRY_CODE) "+ 
  " INNER JOIN TB_TAXCODE tcode ON TB_TAXCODE_DETAIL.TAXCODE_CODE = tcode.TAXCODE_CODE) "+
  " INNER JOIN TB_LIST_CODE tlist ON TB_TAXCODE_DETAIL.TAXCODE_TYPE_CODE = tlist.CODE_CODE) "+
  " LEFT JOIN  TB_LIST_CODE tlist1 ON tlist1.CODE_TYPE_CODE = 'TAXTYPE' AND tlist1.CODE_CODE = tcode.taxcode_code) "+
  " LEFT JOIN  TB_JURISDICTION  tjur ON tjur.JURISDICTION_ID = TB_TAXCODE_DETAIL.JURISDICTION_ID "+
  " WHERE TB_TAXCODE_DETAIL.ACTIVE_FLAG = '1' AND tlist.CODE_TYPE_CODE = 'TCTYPE'"+
  " "+sql.toString()+" ";
  Query q = entityManager.createNativeQuery(sqlQuery);
  System.out.println("sqlQuery"+sqlQuery);
				//CallableStatement cstatement = con
				//		.prepareCall("{ ? = call SP_TAXCODE_DETAIL_LIST(?)}");
				//System.out.println(" super.getCursor() =  " + getCursor(con));
				//cstatement.registerOutParameter(1, getCursor(con));					
				//cstatement.setString(2, sql.toString());
				//cstatement.registerOutParameter(2, OracleTypes.CURSOR);
						
						
				//System.out.println("\t Begin stored procedure call SP_TAXCODE_DETAIL_LIST \n");
				//long begintime = System.currentTimeMillis();
			//	cstatement.execute();
			//	ResultSet rs = (ResultSet) cstatement.getObject(1);
				
				//List<Object> list= q.getResultList();
				//Iterator<Object> iter=list.iterator();
  
  PreparedStatement p = con.prepareStatement(sqlQuery);				
	
	long begintime = System.currentTimeMillis();
	ResultSet rs = p.executeQuery();
	while(rs!=null && rs.next()){
		
			TaxCodeDetailDTO taxCodeDetail = new TaxCodeDetailDTO();
			taxCodeDetail.setTaxcodeCode(rs.getString(1));
			taxCodeDetail.setTaxcodeStateCode(rs.getString(2));
			taxCodeDetail.setTaxcodeCountryCode(rs.getString(3));
			taxCodeDetail.setTaxcodeDetailId(rs.getLong(4));
			taxCodeDetail.setName(rs.getString(5));
			taxCodeDetail.setTaxcodeTypeDesc(rs.getString(6));
			taxCodeDetail.setDescription(rs.getString(7));
			taxCodeDetail.setComments(rs.getString(8));
			taxCodeDetail.setTaxtypeCode(rs.getString(9));
			taxCodeDetail.setErpTaxcode(rs.getString(10));
			taxCodeDetail.setTaxtypeDesc(rs.getString(11));
			
			
			taxCodeDetailDTO.add(taxCodeDetail);

		
					
				}
			
				String time = Long.toString(System.currentTimeMillis()
			- begintime);
			System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
			    p.close();
				con.commit();
				con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return taxCodeDetailDTO;
	}
	
	

	protected int getCursor(java.sql.Connection con) throws SQLException {
		int cursor = 0;		
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {
			cursor =  OracleTypes.CURSOR;					
		} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {
			cursor = Types.OTHER;					
		}			
		return cursor;
	}

	@Override
	@Transactional
	public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxcodeCode, String taxCodeCountry,
			String taxCodeState, String taxCodeCounty, String taxCodeCity, boolean showLocalTaxability, boolean activeFlag, boolean definedFlag)
			throws DataAccessException {
		
		List<TaxCodeDetailDTO> taxCodeDetailDTO= new ArrayList<TaxCodeDetailDTO>();
		Connection con = null;
		String whereClauseToken = getWhereClauseToken(taxcodeCode, taxCodeCountry,taxCodeState,taxCodeCounty,taxCodeCity);	
		try{
			//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				/*CallableStatement cstatement = con
						.prepareCall("{ ? = call SP_TAXCODE_DETAIL_JURIS_LIST(?, ?, ?, ?, ?, ?, ?, ?)}");
				cstatement.registerOutParameter(1, getCursor(con));
				cstatement.setString(2, taxcodeCode);
				cstatement.setString(3, taxCodeCountry);
				cstatement.setString(4, taxCodeState);
				cstatement.setString(5, taxCodeCounty);
				cstatement.setString(6, taxCodeCity);
				if(showLocalTaxability) {
					cstatement.setString(7, "1");
				}
				else {
					cstatement.setString(7, null);
				}
				
				if(activeFlag) {
					cstatement.setString(8, "1");
				}
				else {
					cstatement.setString(8, null);
				}
				
				if(definedFlag) {
					cstatement.setString(9, "1");
				}
				else {
					cstatement.setString(9, null);
				}*/
				
				String sqlQuery="SELECT DISTINCT "+
      " tb_taxcode_detail.taxcode_country_code, "+
      " tlist.description AS taxcode_country_name, "+
      " tb_taxcode_detail.taxcode_state_code, "+
      " tstate.name AS taxcode_state_name, "+
      " tb_taxcode_detail.taxcode_county, "+
      " tb_taxcode_detail.taxcode_city, "+
      " tb_taxcode_detail.taxcode_stj, "+
      " tb_taxcode_detail.jur_level "+
      " FROM tb_taxcode_detail "+
      " LEFT JOIN tb_taxcode_state tstate ON tb_taxcode_detail.taxcode_country_code=tstate.taxcode_country_code "+
      " AND tb_taxcode_detail.taxcode_state_code=tstate.taxcode_state_code "+
      " LEFT JOIN tb_list_code tlist ON tb_taxcode_detail.taxcode_country_code=tlist.code_code "+
      " AND tlist.code_type_code='COUNTRY' "+
      " WHERE 1=1 :extendWhere";
				sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);

				System.out.println("\t Begin PreparedStatement .. \n");
				 PreparedStatement p = con.prepareStatement(sqlQuery);				
					
					long begintime = System.currentTimeMillis();
					ResultSet rs = p.executeQuery();
				while(rs!=null && rs.next()){
					int ruleCount = rs.getInt("HAVE_RULES_FLAG");
					if(definedFlag && ruleCount <= 0) {
						continue;
					}
					TaxCodeDetailDTO taxCodeDetail = new TaxCodeDetailDTO();
					taxCodeDetail.setTaxcodeCountryCode(rs.getString("COUNTRY"));
					taxCodeDetail.setTaxCodeCountryName(rs.getString("COUNTRY_NAME"));
					taxCodeDetail.setTaxcodeStateCode(rs.getString("TAXCODE_STATE_CODE"));
					taxCodeDetail.setName(rs.getString("TAXCODE_STATE_NAME"));
					taxCodeDetail.setLocalTaxabilityCode(rs.getString("LOCAL_TAXABILITY_CODE"));
					taxCodeDetail.setTaxCodeCounty(rs.getString("TAXCODE_COUNTY"));
					taxCodeDetail.setTaxCodeCity(rs.getString("TAXCODE_CITY"));
					taxCodeDetail.setHaveRulesFlag(ruleCount > 0);
					taxCodeDetail.setRuleCount(ruleCount);
					taxCodeDetailDTO.add(taxCodeDetail);
				}
			
				String time = Long.toString(System.currentTimeMillis()
						- begintime);
				System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
				//cstatement.close();
				p.close();
				con.commit();
				con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return taxCodeDetailDTO;
	}
	
	private String getWhereClauseToken(String taxcodeCode,
			String taxCodeCountry, String taxCodeState, String taxCodeCounty,
			String taxCodeCity) {
	        StringBuffer whereClauseExt = new StringBuffer();
		
	    	//for country selection
		    if (taxcodeCode!=null && !taxcodeCode.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_code='" + taxcodeCode +"'");
		    }
		    if (taxCodeCountry!=null && !taxCodeCountry.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_country_code='" + taxCodeCountry +"'");
		    }
		    if (taxCodeState!=null && !taxCodeState.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_state_code='" + taxCodeState +"'");
		    }
		    if (taxCodeCounty!=null && !taxCodeCounty.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_county='" + taxCodeCounty +"'");
		    }
		    if (taxCodeCity!=null && !taxCodeCity.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_city='" + taxCodeCity +"'");
		    }
		    if (taxCodeCity!=null && !taxCodeCity.equals("")) {
		    	appendANDToWhere(whereClauseExt, "tb_taxcode_detail.taxcode_stj='" + taxCodeCity +"'");
		    }
		    
	    
	return whereClauseExt.toString();
	}
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	@Transactional
	public List<TaxCodeDetailDTO> getTaxcodeDetailsJurisList(String taxcodeCode, String taxCodeCountry,
			String taxCodeState, String taxCodeCounty, String taxCodeCity, String taxCodeStj)
			throws DataAccessException {
		
		List<TaxCodeDetailDTO> taxCodeDetailDTO= new ArrayList<TaxCodeDetailDTO>();
		Connection con = null;
		try{
			String whereClauseToken = getWhereClauseToken(taxcodeCode, taxCodeCountry,taxCodeState,taxCodeCounty,taxCodeCity);	
			//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				/*con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				CallableStatement cstatement = con
						.prepareCall("{ ? = call SP_TAXCODE_DETAIL_JURIS_LIST(?, ?, ?, ?, ?, ?)}");
				cstatement.registerOutParameter(1, getCursor(con));
				cstatement.setString(2, taxcodeCode);
				cstatement.setString(3, taxCodeCountry);
				cstatement.setString(4, taxCodeState);
				cstatement.setString(5, taxCodeCounty);
				cstatement.setString(6, taxCodeCity);
				cstatement.setString(7, taxCodeStj);

				System.out.println("\t Begin stored procedure call SP_TAXCODE_DETAIL_JURIS_LIST \n");
				long begintime = System.currentTimeMillis();
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);*/
				String sqlQuery="SELECT DISTINCT "+
					      " tb_taxcode_detail.taxcode_country_code, "+
					      " tlist.description AS taxcode_country_name, "+
					      " tb_taxcode_detail.taxcode_state_code, "+
					      " tstate.name AS taxcode_state_name, "+
					      " tb_taxcode_detail.taxcode_county, "+
					      " tb_taxcode_detail.taxcode_city, "+
					      " tb_taxcode_detail.taxcode_stj, "+
					      " tb_taxcode_detail.jur_level "+
					      " FROM tb_taxcode_detail "+
					      " LEFT JOIN tb_taxcode_state tstate ON tb_taxcode_detail.taxcode_country_code=tstate.taxcode_country_code "+
					      " AND tb_taxcode_detail.taxcode_state_code=tstate.taxcode_state_code "+
					      " LEFT JOIN tb_list_code tlist ON tb_taxcode_detail.taxcode_country_code=tlist.code_code "+
					      " AND tlist.code_type_code='COUNTRY' "+
					      " WHERE 1=1 :extendWhere";
				      sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);

									System.out.println("\t Begin PreparedStatement .. \n");
									 PreparedStatement p = con.prepareStatement(sqlQuery);				
										
										long begintime = System.currentTimeMillis();
										ResultSet rs = p.executeQuery();
									while(rs!=null && rs.next()){
				
					
					TaxCodeDetailDTO taxCodeDetail = new TaxCodeDetailDTO();
					taxCodeDetail.setTaxcodeCountryCode(rs.getString("taxcode_country_code"));
					taxCodeDetail.setTaxCodeCountryName(rs.getString("taxcode_country_name"));
					
					taxCodeDetail.setTaxcodeStateCode(rs.getString("taxcode_state_code"));
					taxCodeDetail.setName(rs.getString("taxcode_state_name"));
					

					taxCodeDetail.setTaxCodeCounty(rs.getString("taxcode_county"));
					taxCodeDetail.setTaxCodeCity(rs.getString("taxcode_city"));
					
					taxCodeDetail.setTaxCodeStj(rs.getString("taxcode_stj"));
					taxCodeDetail.setJurLevel(rs.getString("jur_level"));
					taxCodeDetailDTO.add(taxCodeDetail);
				}
			
				String time = Long.toString(System.currentTimeMillis()
						- begintime);
				System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
				p.close();
				con.commit();
				con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return taxCodeDetailDTO;
	}
	
	@Transactional(readOnly = false)
	public List<TaxCodeDetailDTO> getTaxcodeRules(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel) throws DataAccessException {
		List<TaxCodeDetailDTO> taxCodeDetailDTO= new ArrayList<TaxCodeDetailDTO>();
		Connection con = null;
		
		String 	sqlStr = "";				
				sqlStr += "SELECT ";
				sqlStr += "  tb_taxcode_detail.taxcode_detail_id, ";
				sqlStr += "  tb_taxcode_detail.effective_date, ";
				sqlStr += "  tb_taxcode_detail.taxcode_type_code, ";
				sqlStr += "  tctype.description AS taxcode_type_desc, ";
				sqlStr += "  tb_taxcode_detail.taxtype_code, ";
				sqlStr += "  taxtype.description AS override_taxtype_desc, ";
				sqlStr += "  tb_taxcode_detail.ratetype_code, ";
				sqlStr += "  ratetype.description as rattype_desc, ";
				sqlStr += "  tb_taxcode_detail.taxable_threshold_amt, ";
				sqlStr += "  tb_taxcode_detail.base_change_pct, ";
				sqlStr += "  tb_taxcode_detail.special_rate, ";
				sqlStr += "  tb_taxcode_detail.expiration_date, ";
				sqlStr += "  tb_taxcode_detail.custom_flag, ";
				sqlStr += "  tb_taxcode_detail.active_flag, ";		    
				sqlStr += "  tb_taxcode_detail.taxcode_stj, ";
				sqlStr += "  tb_taxcode_detail.jur_level, ";
				sqlStr += "  tb_taxcode_detail.sort_no, ";			
				sqlStr += "  tb_taxcode_detail.citation_ref, ";
				sqlStr += "  tb_taxcode_detail.exempt_reason, ";
				sqlStr += "  tb_taxcode_detail.good_svc_type_code, ";
				sqlStr += "  tb_taxcode_detail.special_setamt, ";
				sqlStr += "  tb_taxcode_detail.minimum_taxable_amt, ";
				sqlStr += "  tb_taxcode_detail.maximum_taxable_amt, ";
				sqlStr += "  tb_taxcode_detail.maximum_tax_amt, ";
				sqlStr += "  tb_taxcode_detail.group_by_driver, ";
				sqlStr += "  tb_taxcode_detail.tax_process_type_code, ";
				sqlStr += "  tb_taxcode_detail.alloc_bucket, ";
				sqlStr += "  tb_taxcode_detail.alloc_prorate_by_code, ";
				sqlStr += "  tb_taxcode_detail.alloc_condition_code, ";
				sqlStr += "  tb_taxcode_detail.update_timestamp, ";
				sqlStr += "  tb_taxcode_detail.update_user_id, ";
				sqlStr += "  refdoc.refdoccount ";
				sqlStr += "FROM tb_taxcode_detail ";
				sqlStr += "  LEFT JOIN tb_list_code tctype ON tb_taxcode_detail.taxcode_type_code=tctype.code_code AND tctype.code_type_code='TCTYPE' ";
				sqlStr += "  LEFT JOIN tb_list_code taxtype ON tb_taxcode_detail.taxtype_code=taxtype.code_code AND taxtype.code_type_code='TAXTYPE' ";
				sqlStr += "  LEFT JOIN tb_list_code ratetype ON tb_taxcode_detail.ratetype_code=ratetype.code_code AND ratetype.code_type_code='RATETYPE' ";		
				sqlStr += "  LEFT JOIN (SELECT taxcode_detail_id, count(*) AS refdoccount FROM tb_reference_detail GROUP BY taxcode_detail_id) refdoc ON refdoc.taxcode_detail_id=tb_taxcode_detail.taxcode_detail_id  "; 
				sqlStr += "WHERE tb_taxcode_detail.taxcode_code='" + taxcodeCode + "' ";
				sqlStr +=   "AND tb_taxcode_detail.taxcode_country_code='" + taxcodeCountryCode + "' ";
				sqlStr +=   "AND tb_taxcode_detail.taxcode_state_code='" + taxcodeStateCode + "' ";
			      
			      if(taxcodeCounty==null || taxcodeCounty.length()==0){
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_county is null ";
			      }
			      else{
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_county='" + taxcodeCounty + "' ";
			      }
			      
			      if(taxcodeCity==null || taxcodeCity.length()==0){
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_city is null ";
			      }
			      else{
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_city='" + taxcodeCity + "' ";
			      }
			      
			      if(taxcodeStj==null || taxcodeStj.length()==0){
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_stj is null ";
			      }
			      else{
			    	  sqlStr += "AND tb_taxcode_detail.taxcode_stj='" + taxcodeStj + "' ";
			      }
			      
			      if(jurLevel==null || jurLevel.length()==0){
			    	  sqlStr += "AND tb_taxcode_detail.jur_level is null ";
			      }
			      else{
			    	  sqlStr += "AND tb_taxcode_detail.jur_level='" + jurLevel + "' ";
			      }

			      sqlStr += "ORDER BY tb_taxcode_detail.effective_date  ";
		
		try{
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				PreparedStatement p = con.prepareStatement(sqlStr);				
		
				long begintime = System.currentTimeMillis();
				ResultSet rs = p.executeQuery();
				while(rs!=null && rs.next()){
					TaxCodeDetailDTO taxCodeDetail = new TaxCodeDetailDTO();
					taxCodeDetail.setTaxcodeCode(taxcodeCode);
					taxCodeDetail.setTaxcodeCountryCode(taxcodeCountryCode);
					taxCodeDetail.setTaxcodeStateCode(taxcodeStateCode);
					taxCodeDetail.setTaxCodeCounty(taxcodeCounty);
					taxCodeDetail.setTaxCodeCity(taxcodeCity);
					taxCodeDetail.setTaxCodeStj(taxcodeStj);
					taxCodeDetail.setJurLevel(jurLevel);
					
					taxCodeDetail.setTaxcodeDetailId(rs.getLong("TAXCODE_DETAIL_ID"));
					taxCodeDetail.setEffectiveDate(rs.getDate("EFFECTIVE_DATE"));
					taxCodeDetail.setTaxcodeTypeCode(rs.getString("TAXCODE_TYPE_CODE"));
					taxCodeDetail.setTaxcodeTypeDesc(rs.getString("taxcode_type_desc")); //tctype.description AS taxcode_type_desc	
					taxCodeDetail.setTaxtypeCode(rs.getString("taxtype_code"));
					taxCodeDetail.setTaxtypeDesc(rs.getString("override_taxtype_desc"));				
					taxCodeDetail.setRateTypeCode(rs.getString("RATETYPE_CODE"));
					taxCodeDetail.setRateTypeDesc(rs.getString("rattype_desc"));	
					taxCodeDetail.setTaxableThresholdAmt(rs.getBigDecimal("TAXABLE_THRESHOLD_AMT"));				
					taxCodeDetail.setBaseChangePct(rs.getBigDecimal("BASE_CHANGE_PCT"));
					taxCodeDetail.setSpecialRate(rs.getBigDecimal("SPECIAL_RATE"));
					taxCodeDetail.setExpirationDate(rs.getDate("EXPIRATION_DATE"));	
					taxCodeDetail.setCustomFlag(rs.getString("CUSTOM_FLAG"));		
					taxCodeDetail.setActiveFlag(rs.getString("ACTIVE_FLAG"));			
					taxCodeDetail.setTaxCodeStj(rs.getString("TAXCODE_STJ"));
					taxCodeDetail.setJurLevel(rs.getString("JUR_LEVEL"));
					taxCodeDetail.setSortNo(rs.getLong("SORT_NO"));
					
					taxCodeDetail.setCitationRef(rs.getString("CITATION_REF"));
					taxCodeDetail.setExemptReason(rs.getString("EXEMPT_REASON"));
					taxCodeDetail.setGoodSvcTypeCode(rs.getString("GOOD_SVC_TYPE_CODE"));
					
					taxCodeDetail.setSpecialSetAmt(rs.getBigDecimal("SPECIAL_SETAMT"));
					taxCodeDetail.setMinimumTaxableAmt(rs.getBigDecimal("MINIMUM_TAXABLE_AMT"));
					taxCodeDetail.setMaximumTaxableAmt(rs.getBigDecimal("MAXIMUM_TAXABLE_AMT"));
					taxCodeDetail.setMaximumTaxAmt(rs.getBigDecimal("MAXIMUM_TAX_AMT"));
					
					taxCodeDetail.setGroupByDriver(rs.getString("GROUP_BY_DRIVER"));
					taxCodeDetail.setTaxProcessTypeCode(rs.getString("TAX_PROCESS_TYPE_CODE"));
					taxCodeDetail.setAllocBucket(rs.getString("ALLOC_BUCKET"));
					taxCodeDetail.setAllocProrateByCode(rs.getString("ALLOC_PRORATE_BY_CODE"));
					taxCodeDetail.setAllocConditionCode(rs.getString("ALLOC_CONDITION_CODE"));	
					
					java.sql.Timestamp updateTimestamp = (java.sql.Timestamp) rs.getTimestamp("UPDATE_TIMESTAMP");
		            if(updateTimestamp!=null){
		            	taxCodeDetail.setUpdateTimestamp(new java.util.Date(updateTimestamp.getTime()));
		            }
					taxCodeDetail.setUpdateUserId(rs.getString("UPDATE_USER_ID"));
					taxCodeDetail.setRefdoccount(rs.getBigDecimal("REFDOCCOUNT"));
					
					taxCodeDetailDTO.add(taxCodeDetail);
				}
				
				rs.close();
			
				String time = Long.toString(System.currentTimeMillis() - begintime);
				System.out.println("\t *** Executing getTaxcodeRules(), took " + time + " milliseconds. \n");
				p.close();
				con.commit();
				con.close();
				
			}
		} catch (SQLException se) {
			se.printStackTrace();
			logger.error("Error in executing SP");
		}catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in executing SP");
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return taxCodeDetailDTO;
	}
	
	public long getRulesusedCount(Long ruleId) throws DataAccessException {
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	long count = 0;
    	
    	String sqlStr = "SELECT count(*) FROM tb_purchtrans_jurdtl " +
				    	"WHERE country_taxcode_detail_id = :ruleId " +
				    	"OR state_taxcode_detail_id = :ruleId " +
				    	"OR county_taxcode_detail_id = :ruleId " +
				    	"OR city_taxcode_detail_id = :ruleId "; 	
    	try {
    		query = session.createSQLQuery(sqlStr);
    		query.setParameter("ruleId", ruleId);
    		List<?> result = query.list();
    		count = ((BigDecimal) result.get(0)).longValue();
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return count;
	}
	
	public long getTaxCodeRulesCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, Date effectiveDate, String jurLevel)
		throws DataAccessException {
		//TODO: JJ - Make sure following (mainly sqlRestriction) works with Postgres
		
		Session session = createLocalSession();
		Criteria criteria = getCriteria(session,taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, taxcodeStj, jurLevel);

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		criteria.add(Expression.sqlRestriction("{alias}.EFFECTIVE_DATE = to_date(?, 'mm/dd/yyyy')", sdf.format(effectiveDate), Hibernate.STRING));
		criteria.setProjection(Projections.rowCount());
		
		List<?> count = criteria.list();
		closeLocalSession(session);
		session = null;
		return ((Number) count.get(0)).longValue();
	}
	
	public long getTaxCodeJurisdInfoCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj,String jurLevel)
			throws DataAccessException {
			
			Session session = createLocalSession();
			Criteria criteria = getCriteria(session,taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, taxcodeStj, jurLevel);

			criteria.setProjection(Projections.rowCount());
			
			List<?> count = criteria.list();
			closeLocalSession(session);
			session = null;
			return ((Number) count.get(0)).longValue();
		}
	
	public Criteria getCriteria(Session session,String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj,String jurLevel) {
		Criteria criteria = session.createCriteria(TaxCodeDetail.class);
		criteria.add(Expression.eq("taxcodeCode", taxcodeCode.toUpperCase()).ignoreCase());
		criteria.add(Expression.eq("taxcodeCountryCode", taxcodeCountryCode).ignoreCase());
		criteria.add(Expression.eq("taxcodeStateCode", taxcodeStateCode).ignoreCase());
		criteria.add(Expression.eq("taxCodeCounty", taxcodeCounty).ignoreCase());
		criteria.add(Expression.eq("taxCodeCity", taxcodeCity).ignoreCase());
		criteria.add(Expression.eq("taxCodeStj", taxcodeStj).ignoreCase());
		criteria.add(Expression.eq("jurLevel", jurLevel).ignoreCase());
		return criteria;
	}
	
	public long getAllocBucketCount(Long taxcodeDetailId, String allocBucket)
		throws DataAccessException {

		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(TaxCodeDetail.class);
		if(taxcodeDetailId != null){
			criteria.add(Expression.ne("taxcodeDetailId", taxcodeDetailId));
		}
		criteria.add(Expression.eq("allocBucket", allocBucket).ignoreCase());


		List<?> count = criteria.list();
		closeLocalSession(session);
		session = null;
		
		return count.size();
	}
	
	public Date minEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel)
		throws DataAccessException {

		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(TaxCodeDetail.class);
		criteria.add(Expression.eq("taxcodeCode", taxcodeCode.toUpperCase()).ignoreCase());
		criteria.add(Expression.eq("taxcodeCountryCode", taxcodeCountryCode).ignoreCase());
		criteria.add(Expression.eq("taxcodeStateCode", taxcodeStateCode).ignoreCase());
		criteria.add(Expression.eq("taxCodeCounty", taxcodeCounty).ignoreCase());
		criteria.add(Expression.eq("taxCodeCity", taxcodeCity).ignoreCase());
		criteria.add(Expression.eq("taxCodeStj", taxcodeStj).ignoreCase());
		criteria.add(Expression.eq("jurLevel", jurLevel).ignoreCase());
		criteria.setProjection(Projections.min("effectiveDate"));
		Object dateObject = criteria.list().get(0);
        Date minGLDate = null;
        if(dateObject!=null){
        	minGLDate = (Date)dateObject;
        }

		return minGLDate;
	}
	
	public Long matrixEffectiveDateCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel, Date backEffectiveDate)
			throws DataAccessException {

			Session session = createLocalSession();
			Criteria criteria = session.createCriteria(TaxCodeDetail.class);
			criteria.add(Expression.eq("taxcodeCode", taxcodeCode.toUpperCase()).ignoreCase());
			criteria.add(Expression.eq("taxcodeCountryCode", taxcodeCountryCode).ignoreCase());
			criteria.add(Expression.eq("taxcodeStateCode", taxcodeStateCode).ignoreCase());
			criteria.add(Expression.eq("taxCodeCounty", taxcodeCounty).ignoreCase());
			criteria.add(Expression.eq("taxCodeCity", taxcodeCity).ignoreCase());
			criteria.add(Expression.eq("taxCodeStj", taxcodeStj).ignoreCase());
			criteria.add(Expression.eq("jurLevel", jurLevel).ignoreCase());		
			criteria.add(Expression.gt("effectiveDate", backEffectiveDate));		
			criteria.setProjection(Projections.rowCount());
			
			List< ? > result = criteria.list();	
			Long returnLong = ((Number) result.get(0)).longValue();

			return returnLong;
		}
	
	@Override
	@SuppressWarnings("unchecked")
	public Date getRuleEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String transInd, String suspendInd) {
		boolean hasCounty = StringUtils.hasText(taxcodeCounty);
		boolean hasCity = StringUtils.hasText(taxcodeCity);
		
		Map<String, String> params = new HashMap<String, String>();
		
		//Redoing this as Java SQL
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT MIN(pt.glDate) ");
		if(hasCounty || hasCity)
			sql.append("FROM PurchaseTransaction pt, Jurisdiction j ");
		else
			sql.append("FROM PurchaseTransaction pt ");
		
		sql.append("WHERE ");
		
		if(hasCounty || hasCity)
			sql.append("pt.shiptoJurisdictionId = j.jurisdictionId AND ");
		
		sql.append("pt.taxcodeCode = :taxCode AND pt.transactionCountryCode = :countryCode ");
		sql.append("AND pt.transactionStateCode = :stateCode ");

		if(hasCounty) {
			sql.append("AND j.county = :county ");
			params.put("county", taxcodeCounty);
		}
		
		if(hasCity) {
			sql.append("AND j.city = :city ");
			params.put("city", taxcodeCity);
		}
		sql.append("AND pt.transactionInd = :transInd AND pt.suspendInd= :suspendInd");

		
		params.put("taxCode", taxcodeCode);
		params.put("countryCode", taxcodeCountryCode);
		params.put("stateCode", taxcodeStateCode);
		params.put("transInd", transInd);
		params.put("suspendInd", suspendInd);

		Query q = entityManager.createQuery(sql.toString());
		for (Map.Entry<String, String> e : params.entrySet()) {
			q.setParameter(e.getKey(), e.getValue());
		}
		
		Date effectiveDate = null;
		List<Date> result = q.getResultList();
		if(result != null && result.size() > 0) {
			effectiveDate = result.get(0);
		}
		
		return effectiveDate;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Long[] addRulesFromTransaction(TaxCodeDetailDTO stateRule, TaxCodeDetailDTO countyRule, TaxCodeDetailDTO cityRule) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		Long stateRuleId = null;
		Long countyRuleId = null;
		Long cityRuleId = null;
		
		if(stateRule != null) {
			if(stateRule.getTaxcodeDetailId() == null) {
				//new state rule, so need to save
				TaxCodeDetail td = new TaxCodeDetail();
				td.setTaxcodeCode(stateRule.getTaxcodeCode());
				td.setTaxcodeCountryCode(stateRule.getTaxcodeCountryCode());
				td.setTaxcodeStateCode(stateRule.getTaxcodeStateCode());
				td.setTaxCodeCounty(stateRule.getTaxCodeCounty());
				td.setTaxCodeCity(stateRule.getTaxCodeCity());
				td.setEffectiveDate(stateRule.getEffectiveDate());
				try {
					td.setExpirationDate(sdf.parse("12/31/9999"));
				}
				catch(ParseException e){}
				td.setTaxcodeTypeCode(stateRule.getTaxcodeTypeCode());
				td.setTaxtypeCode(stateRule.getTaxtypeCode());
				if(!"".equals(stateRule.getRateTypeCode())) {
					td.setRateTypeCode(stateRule.getRateTypeCode());
				}
				td.setTaxableThresholdAmt(stateRule.getTaxableThresholdAmt());
				td.setBaseChangePct(stateRule.getBaseChangePct());
				td.setSpecialRate(stateRule.getSpecialRate());
				td.setCustomFlag("1");
				td.setModifiedFlag("0");
				td.setActiveFlag(stateRule.getActiveFlag());
				
				//0005054
				td.setTaxCodeStj(stateRule.getTaxCodeStj());
				td.setJurLevel(stateRule.getJurLevel());	
				td.setSortNo(stateRule.getSortNo());	
				
				save(td);
				
				stateRuleId = td.getTaxcodeDetailId();
			}
			else {
				stateRuleId = stateRule.getTaxcodeDetailId();
			}
		}
		
		if(countyRule != null) {
			TaxCodeDetail td = new TaxCodeDetail();
			td.setTaxcodeCode(countyRule.getTaxcodeCode());
			td.setTaxcodeCountryCode(countyRule.getTaxcodeCountryCode());
			td.setTaxcodeStateCode(countyRule.getTaxcodeStateCode());
			td.setTaxCodeCounty(countyRule.getTaxCodeCounty());
			td.setTaxCodeCity(countyRule.getTaxCodeCity());
			td.setEffectiveDate(countyRule.getEffectiveDate());
			try {
				td.setExpirationDate(sdf.parse("12/31/9999"));
			}
			catch(ParseException e){}
			td.setTaxcodeTypeCode(countyRule.getTaxcodeTypeCode());
			td.setTaxtypeCode(countyRule.getTaxtypeCode());
			if(!"".equals(countyRule.getRateTypeCode())) {
				td.setRateTypeCode(countyRule.getRateTypeCode());
			}
			td.setTaxableThresholdAmt(countyRule.getTaxableThresholdAmt());
			td.setBaseChangePct(countyRule.getBaseChangePct());
			td.setSpecialRate(countyRule.getSpecialRate());
			td.setCustomFlag("1");
			td.setModifiedFlag("0");
			td.setActiveFlag(countyRule.getActiveFlag());
			
			//0005054
			td.setTaxCodeStj(countyRule.getTaxCodeStj());
			td.setJurLevel(countyRule.getJurLevel());	
			td.setSortNo(countyRule.getSortNo());
			
			save(td);
			
			countyRuleId = td.getTaxcodeDetailId();
		}
		
		if(cityRule != null) {
			TaxCodeDetail td = new TaxCodeDetail();
			td.setTaxcodeCode(cityRule.getTaxcodeCode());
			td.setTaxcodeCountryCode(cityRule.getTaxcodeCountryCode());
			td.setTaxcodeStateCode(cityRule.getTaxcodeStateCode());
			td.setTaxCodeCounty(cityRule.getTaxCodeCounty());
			td.setTaxCodeCity(cityRule.getTaxCodeCity());
			td.setEffectiveDate(cityRule.getEffectiveDate());
			try {
				td.setExpirationDate(sdf.parse("12/31/9999"));
			}
			catch(ParseException e){}
			td.setTaxcodeTypeCode(cityRule.getTaxcodeTypeCode());
			td.setTaxtypeCode(cityRule.getTaxtypeCode());
			if(!"".equals(cityRule.getRateTypeCode())) {
				td.setRateTypeCode(cityRule.getRateTypeCode());
			}
			td.setTaxableThresholdAmt(cityRule.getTaxableThresholdAmt());
			td.setBaseChangePct(cityRule.getBaseChangePct());
			td.setSpecialRate(cityRule.getSpecialRate());
			td.setCustomFlag("1");
			td.setModifiedFlag("0");
			td.setActiveFlag(cityRule.getActiveFlag());
			
			//0005054
			td.setTaxCodeStj(cityRule.getTaxCodeStj());
			td.setJurLevel(cityRule.getJurLevel());	
			td.setSortNo(cityRule.getSortNo());
			
			save(td);
			
			cityRuleId = td.getTaxcodeDetailId();
		}
		
		return new Long[]{stateRuleId, countyRuleId, cityRuleId};
	}
	
	//Moving Processing to PurchaseTransaction.processSuspendedTransactions
//	@Override
//	@Transactional
//	public void processTransactions(Long[] ruleIds) throws Exception {
//		if(ruleIds != null && ruleIds.length == 3) {
//			try {			
//				/*
//				 * After writing the Rule(s) to the table, the processing stored procedure sp_tb_taxcode_detail_a_i will be called. 
//				 * First, if the City Rule is created call the SProc passing the taxcode_detail_id for the City Rule. 
//				 * Then, if the County Rule is created call the SProc passing the taxcode_detail_id for the County Rule. 
//				 * If either the City or the County Rules were created, then do not call the SProc for the State Rule. 
//				 * However, if only the State Rule was created then call the SProc passing the taxcode_detail_id for the State Rule.
//				 */
//				
//				//city rule
//				if(ruleIds[2] != null) {
//					//purchaseTransactionService.processSuspendedTransactions(ruleIds[2], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, false);
//					/* ***!SPROC!*** Commented on 8/15/2017
//					CallableStatement cstmnt = con.prepareCall("{call SP_TB_TAXCODE_DETAIL_A_I(?)}");
//					cstmnt.setLong(1, ruleIds[2]);
//					cstmnt.execute();
//					cstmnt.close();
//					con.commit();
//					*/
//				}
//				
//				//county rule
//				if(ruleIds[1] != null) {
//					//purchaseTransactionService.processSuspendedTransactions(ruleIds[1], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, false);
//					/* ***!SPROC!*** Commented on 8/15/2017
//					CallableStatement cstmnt = con.prepareCall("{call SP_TB_TAXCODE_DETAIL_A_I(?)}");
//					cstmnt.setLong(1, ruleIds[1]);
//					cstmnt.execute();
//					cstmnt.close();
//					con.commit();
//					*/
//				}
//				
//				if(ruleIds[1] == null && ruleIds[2] == null) {
//					if(ruleIds[0] == null) {
//						throw new Exception("Invalid State Rule");
//					}
//					
//					//state rule
//					//purchaseTransactionService.processSuspendedTransactions(ruleIds[0], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, false);
//					
//					/* ***!SPROC!*** Commented on 8/15/2017
//					CallableStatement cstmnt = con.prepareCall("{call SP_TB_TAXCODE_DETAIL_A_I(?)}");
//					cstmnt.setLong(1, ruleIds[0]);
//					cstmnt.execute();
//					cstmnt.close();
//					con.commit();
//					*/
//				}
//			} catch (SQLException se) {
//				se.printStackTrace();
//			}
//			finally{
//			}
//		}
//		else {
//			throw new Exception("Invalid rules");
//		}
//	}

	@Override
	public TaxCodeDetail findByJurLevelBroadSearch(String taxcodeCode, String jurLevel, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String stjName, Date glDate) {
		TaxCodeDetail result = null;
		//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localSession = createLocalSession();
		try {
			
			Option customOverrideFlag = getJpaTemplate().find(Option.class, new OptionCodePK ("CUSTOMTCOR", "SYSTEM", "SYSTEM"));
			String customOverride; 
			if(customOverrideFlag != null && customOverrideFlag.getValue().equals("1"))
				customOverride = "desc";
			else
				customOverride =  "asc";
			
			//org.hibernate.Query query = localSession
			String sql = " from TaxCodeDetail where activeFlag='1' " +
					"and taxcodeCode = :taxcodeCode " +
					"and  jurLevel=:jurLevel " +
					"and taxcodeCountryCode = :taxcodeCountryCode " +
					"and (taxcodeStateCode = :taxcodeStateCode or taxcodeStateCode='*ALL' ) " +
					"and (taxCodeCounty = :taxcodeCounty or taxCodeCounty='*ALL' ) " +
					"and (taxCodeCity = :taxcodeCity or taxCodeCity='*ALL' ) " +
					"and (taxCodeStj = :taxCodeStj or taxCodeStj = '*ALL' )" +
					"and effectiveDate <= :effectiveDate "+
					"and expirationDate >= :expirationDate " +
					"order by sortNo desc, effectiveDate desc, customFlag " + customOverride;
			
			
			
				
			
			org.hibernate.Query query = localSession.createQuery(sql);

			List<TaxCodeDetail> taxCodeDetails = query.setParameter("taxcodeCode", taxcodeCode)
					.setParameter("jurLevel", jurLevel)
					.setParameter("taxcodeCountryCode",taxcodeCountryCode)
					.setParameter("taxcodeStateCode",taxcodeStateCode)
					.setParameter("taxcodeCounty",taxcodeCounty)
					.setParameter("taxcodeCity",taxcodeCity)
					.setParameter("taxCodeStj", stjName)
					.setParameter("effectiveDate",glDate)
					.setParameter("expirationDate",glDate)
					.list();

			if (taxCodeDetails != null && taxCodeDetails.size() > 0)
				result = taxCodeDetails.get(0);

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.error("findByJurLevelBroadSearch - "+e.getMessage());
		} finally {
			closeLocalSession(localSession);
		}
		return result;
	}
}



