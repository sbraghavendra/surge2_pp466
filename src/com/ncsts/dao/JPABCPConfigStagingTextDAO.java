/*
 * Author: Jim M. Wilson
 * Created: Aug 23, 2008
 * $Date: 2008-08-29 16:28:03 -0500 (Fri, 29 Aug 2008) $: - $Revision: 2020 $: 
 */

package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPConfigStagingText;
import com.ncsts.domain.BCPConfigStagingTextPK;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchMaintenance;

/**
 * 
 */
public class JPABCPConfigStagingTextDAO extends JPAGenericDAO<BCPConfigStagingText, BCPConfigStagingTextPK> 
implements BCPConfigStagingTextDAO {

	@SuppressWarnings("unchecked")
	public List<BCPConfigStagingText> getPage(BatchMaintenance batch, int firstRow, int maxResults)
			throws DataAccessException {

		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPConfigStagingText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		criteria.addOrder(Order.asc("id.line"));
		try {
			List<BCPConfigStagingText> returnBCPJurisdictionTaxRateText = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPJurisdictionTaxRateText;	
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPConfigStagingText>();
		}
	}

	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPConfigStagingText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
			
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}

	//private Criteria generateCriteria(BatchMaintenance batch) {
	//	return createCriteria().add(Restrictions.eq("id.batchId", batch.getBatchId()));
	//}	

}
