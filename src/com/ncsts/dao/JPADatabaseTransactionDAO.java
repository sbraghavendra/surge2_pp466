package com.ncsts.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import oracle.jdbc.OracleTypes;

import org.hibernate.HibernateException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.TempLocationMatrixCount;
import com.ncsts.domain.TempStatistics;
import com.ncsts.domain.TempTaxMatrixCount;

/*
 * Midtier project code modified - january 2009
 */

public class JPADatabaseTransactionDAO extends JpaDaoSupport implements DatabaseTransactionDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Will return all the records for database statistics Transactions Tab
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TempStatistics> getTransactionsStatistics(){	
		try {
			updateTempStatistics("sp_db_statistics", null);
		} catch (SQLException e) {
			logger.info("Exception while Updating the TempStatistics : "+e.getMessage());
		}
		return getJpaTemplate().find("select tempStatistics from TempStatistics tempStatistics  ");
		
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TempStatistics> getTransactionsStatistics(String whereClauseforSearch)throws SQLException{
		if(whereClauseforSearch == ""){
			whereClauseforSearch = null;
		}
		updateTempStatistics("sp_db_statistics",whereClauseforSearch);
		return getJpaTemplate().find("select tempStatistics from TempStatistics tempStatistics  ");
		
	}	
	
	@Transactional
	public void updateTempStatistics(String spName,String whereClause)throws SQLException{
	    logger.info("-------------------------------------------------");	
	    Connection con = null;
		try {
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			logger.info(" updateTempStatistics ");
			logger.info(" spName = " + spName + " whereClause = " + whereClause );
			if (con != null) {
				    String qry = "{call "+ spName +"(?)}";
					logger.info(" Begin stored procedure call "+ qry );				    
					CallableStatement cstatement = con
							.prepareCall(qry);				
					cstatement.setString(1, whereClause);
					cstatement.execute();
					cstatement.close();
					con.commit();
					con.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		logger.info("-------------------------------------------------");			
	}
	
	/**
	 * Will return all the records for database statistics TaxMatrix Tab
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public TempTaxMatrixCount getTaxMatrixStatistics(String andClauseforSearch){
		logger.info("-------------------------------------------------");			
		logger.info(" getTaxMatrixStatistics ");
		logger.info(" andClauseforSearch = " + andClauseforSearch );		
		TempTaxMatrixCount tempTaxMatrixCount = null;
		try {
			updateTempStatistics("SP_TAX_MTRX_DRIVERS_COUNT",andClauseforSearch);
			List<TempTaxMatrixCount> list = getJpaTemplate().find("select tempTaxMatrixCount from TempTaxMatrixCount tempTaxMatrixCount");
			if (list != null)
				tempTaxMatrixCount = list.get(0);
		} catch (SQLException se) {
			logger.error("Error in executing SP " + se.getMessage());
		}
		logger.info("-------------------------------------------------");		
		return tempTaxMatrixCount;
		
	}	
	
	/**
	 * Will return all the records for database statistics LocationMatrix tab
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public TempLocationMatrixCount getLocationMatrixStatistics(String andClauseforSearch){
		logger.info("-------------------------------------------------");			
		logger.info(" getLocationMatrixStatistics ");
		logger.info(" andClauseforSearch = " + andClauseforSearch );			
		TempLocationMatrixCount tempLocationMatrixCount = null;
		try{
			updateTempStatistics("SP_LOC_MTRX_DRIVERS_COUNT",andClauseforSearch);
			List<TempLocationMatrixCount> list = getJpaTemplate().find("select tempLocationMatrixCount from TempLocationMatrixCount tempLocationMatrixCount");
			if(list != null && list.size() > 0 )
				tempLocationMatrixCount = list.get(0); 
		} catch (SQLException se) {
			logger.info("Error in executing SP " + se.getMessage() );
		}
		logger.info("-------------------------------------------------");		
		return tempLocationMatrixCount;
		
	}	
	
	
/*	@Transactional(readOnly=true)
	public Object getObjectFromNativeQuery(String sqlQuery){
		//List<DatabaseTransactionStatisticsDTO> list=null;
		Query q = entityManager.createNativeQuery(sqlQuery);
		//list=(List<DatabaseTransactionStatistics>)q.getSingleResult();
		return q.getSingleResult();
	}
	
	@Transactional(readOnly=true)
	public Object getObjectFromNativeQuery(String sqlQuery, String name, Object param){
		Query q = entityManager.createNativeQuery(sqlQuery);
		q.setParameter(name, param);
		return q.getSingleResult();
	}*/
	
	@Transactional(readOnly=true)
	public List getListOfObjectFromNativeQuery(String sqlQuery){
		List list=null;
		Query q = entityManager.createNativeQuery(sqlQuery);
		list=q.getResultList();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Long count(String spName,String whereClauseToken) {
		logger.info("-------------------------------------------------");			
		Long count = 0l;
		Connection con = null;
		try {
			logger.info(" count ");
			logger.info(" spName = " + spName + " whereClauseToken = " + whereClauseToken );						
			//0002178, and add @Transactional
			//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				String qry = "{ ? = call "+ spName +"(?) }";
				logger.info(" Begin stored procedure call "+ qry );				
				CallableStatement cstatement = con
						.prepareCall(qry);
				logger.info(" db statistcis getCursor() =  " + spName + " ; " + getCursor(con));
				cstatement.registerOutParameter(1, getCursor(con));					
				cstatement.setString(2, whereClauseToken);
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);
				while(rs.next()){
					count = rs.getLong(1);
					logger.info("count " + count);
				}
				cstatement.close();
				con.commit();
				con.close();

			}
		} catch (SQLException se) {
			logger.info("Error in executing SP " + se.getMessage());
		}catch (HibernateException hbme) {
			logger.info("Error in executing SP " + hbme.getMessage() );
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		logger.info("-------------------------------------------------");		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public Long activeCount(String spName,String whereClauseToken) {
		logger.info("-------------------------------------------------");			
		logger.info(" activeCount ");
		logger.info(" spName = " + spName + " whereClauseToken = " + whereClauseToken );			
		Long count = 0l;
		Connection con = null;
		try {
			//0002178, and add @Transactional
			//EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();         
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				con.setAutoCommit(false); //hack for PostGres to avoid cursor "<unnamed portal 1>" does not exist
				String qry = "{ ? = call "+ spName +"(?,?,?) }";
				logger.info(" Begin stored procedure call "+ qry );				
				CallableStatement cstatement = con
						.prepareCall(qry);
				logger.info(" db statistcis getCursor() =  " + spName + " ; " +  getCursor(con));
				cstatement.registerOutParameter(1, getCursor(con));				
				cstatement.setString(2, whereClauseToken);
				cstatement.setDate(3, new java.sql.Date(System.currentTimeMillis()));
				cstatement.setDate(4, new java.sql.Date(System.currentTimeMillis()));
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);
				while(rs.next()){
					count = rs.getLong(1);
					logger.info("activeCount " + count);
				}
				cstatement.close();
				con.commit();
				con.close();
			}
		} catch (SQLException se) {
			logger.info("Error in executing SP " + se.getMessage());
		}catch (HibernateException hbme) {
			logger.info("Error in executing SP " + hbme.getMessage());
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		logger.info("-------------------------------------------------");			
		return count;
	}
	
	@Transactional
	public Long drilldown(String grpBy,String whereClauseToken) {
		logger.info("-------------------------------------------------");			
		logger.info(" drilldown ");
		logger.info(" grpBy = " + grpBy + " whereClauseToken = " + whereClauseToken );			
		Long count = 0l;
		Connection con = null;
		try {
			con = getJpaTemplate().getJpaDialect()
					.getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
				String qry = "{call sp_compute_distamt_1 (?,?)}";
				logger.info(" Begin stored procedure call "+ qry );					
				CallableStatement cstatement = con
						.prepareCall(qry);
				cstatement.setString(1, grpBy);
				cstatement.setString(2, whereClauseToken);
				logger.info(" Begin stored procedure call sp_compute_distamt_1 ");
				cstatement.execute();
				cstatement.close();
				con.commit();
				con.close();
			}
		} catch (SQLException se) {
			logger.info("Error in executing SP " + se.getMessage());
		}catch (HibernateException hbme) {
			logger.info("Error in executing SP " + hbme.getMessage());
		} 
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		logger.info("-------------------------------------------------");			
		return count;
	}
	
	private int getCursor(java.sql.Connection con) throws SQLException {
		int cursor = 0;		
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {
			cursor =  OracleTypes.CURSOR;					
		} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {
			cursor = Types.OTHER;					
		}			
		return cursor;
	}	
	
}


