package com.ncsts.dao;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.AllocationMatrix;

/**
 * 
 * @author Paul Govindan
 * 
 *
 */
public interface AllocationMatrixDAO extends MatrixDAO<AllocationMatrix> {

	public AllocationMatrix findByIdWithDetails(Long id);
	
	public List<AllocationMatrix> getAllRecords(AllocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			Long allocationMatrixId,
			OrderBy orderBy, int firstRow, int maxResults);

	public Long count(AllocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			Long allocationMatrixId);
	
	public List<AllocationMatrix> getDetailRecords(AllocationMatrix allocationMatrix, OrderBy orderBy);
}
