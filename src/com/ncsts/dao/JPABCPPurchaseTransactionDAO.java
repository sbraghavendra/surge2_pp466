package com.ncsts.dao;

import com.ncsts.domain.BCPPurchaseTransaction;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

public class JPABCPPurchaseTransactionDAO extends JpaDaoSupport implements BCPPurchaseTransactionDAO {
    
    @SuppressWarnings("unchecked")
   	@Transactional(readOnly=true)
    public List<BCPPurchaseTransaction> findBCPPurchaseTransactionsByBatchId(Long processBatchNo, int firstRow, int maxResults) {
    	if (processBatchNo == null) return null;
    	
		List<BCPPurchaseTransaction> transactionList = null;
		Session localSession = createLocalSession();
		try { 
			Criteria criteria = localSession.createCriteria(BCPPurchaseTransaction.class);
			criteria.add(Restrictions.eq("processBatchNo", processBatchNo));
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            criteria.addOrder(Order.asc("vendorCode"));
            criteria.addOrder(Order.asc("invoiceNbr"));
            criteria.addOrder(Order.asc("invoiceDate"));

            transactionList = criteria.list();		
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in findBCPPurchaseTransactionsByBatchId");
		}
		finally{
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return transactionList;
    }
    
   	@Transactional(readOnly=true)
    public int findBCPCountByBatchId(Long processBatchNo) {
    	if (processBatchNo == null) return 0;
    	
    	int rowCount = 0;  	
		Session localSession = createLocalSession();
		try { 
			Criteria criteria = localSession.createCriteria(BCPPurchaseTransaction.class);
			criteria.add(Restrictions.eq("processBatchNo", processBatchNo));
			
			criteria.setProjection(Projections.rowCount());
			List<?> count = criteria.list();
			rowCount = ((Number) count.get(0)).intValue();		
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in findBCPCountByBatchId");
		}
		finally{
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return rowCount;
    }
   	
   	@Transactional
	public void deleteBCPPurchaseTransactionsByBatchId(Long processBatchNo){
		Session session = createLocalSession();
		try {
			String hql = "delete from BCPPurchaseTransaction where processBatchNo= :id"; 
			int iCount = session.createQuery(hql).setLong("id", processBatchNo).executeUpdate();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}

    protected Session createLocalSession() {
        EntityManager entityManager = getJpaTemplate()
                .getEntityManagerFactory().createEntityManager();
        Session localsession = (entityManager instanceof HibernateEntityManager) ? ((HibernateEntityManager) entityManager)
                .getSession() : ((HibernateEntityManager) entityManager
                .getDelegate()).getSession();

        logger.info("===== createLocalSession() Hibernate Session Entity Count = "
                + localsession.getStatistics().getEntityCount() + "====");
        return localsession;
    }

    protected void closeLocalSession(Session localSession) {
        if (localSession != null && localSession.isOpen()) {
            localSession.clear();
            localSession.close();
        }
    }
}
