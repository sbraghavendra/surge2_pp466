package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.view.bean.JurisdictionItem;

public interface NexusDefinitionDAO extends GenericDAO<NexusDefinition, Long> {
	public NexusDefinition findOneByExample(NexusDefinition exampleInstance) throws DataAccessException;
	public boolean removeNexusDefinition(List<NexusDefinition> nexusDefinitionArray) throws DataAccessException;
	public boolean addNexusDefinition(List<NexusDefinition> nexusDefinitionArray, List<NexusDefinitionDetail> nexusDefinitionDetailArray) throws DataAccessException;
	public boolean isIndependentCounty(Long entityId, String geocode, String country, String state, String county, String city, String zip, String stj) throws DataAccessException;
}
