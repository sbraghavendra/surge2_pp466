package com.ncsts.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import com.ncsts.common.LogMemory;
import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.Role;

public class JPATransactionDetailBillDAO extends JPAGenericDAO<BillTransaction,Long> implements TransactionDetailBillDAO {
	
	@Transactional(readOnly=true)
	public Long count(BillTransaction exampleInstance) {
		Long ret = 0L;	
		
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(BillTransaction.class);
			Example example = Example.create(exampleInstance);
			exampleHandling(criteria, example, exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			extendExampleCriteria(criteria, exampleInstance);
			
			criteria.setProjection(Projections.rowCount()); // We just want a row count
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in JPATransactionDetailBillDAO::()");
		} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error in finding the entities by example");
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}
	
	protected void exampleHandling(Criteria criteria, Example example, BillTransaction exampleInstance) {
    	// Iterate the properties
    	logger.info("---- JPAGenericDAO.ryanExampleHandling enter -----");
    	for (Method m : getObjectClass().getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && m.getReturnType().equals(String.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					String value = (String) m.invoke(exampleInstance);
	    			if (value != null && value.trim().length()==0) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}
	    			else if (value != null && value.trim().length()>=0) {
	    				value = value.trim();
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			
		    			if(field.equalsIgnoreCase("calculateInd") || field.equalsIgnoreCase("creditInd") || field.equalsIgnoreCase("taxPaidToVendorFlag")  || field.equalsIgnoreCase("exemptInd") ){
		    				//1, y, Y, t, T
		    				//0, n, N, f, F, null
		    				if(value.equals("1")){
		    					criteria.add(Restrictions.or(Restrictions.or(Restrictions.ilike(field, "1", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "Y", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "y", MatchMode.START), Restrictions.ilike(field, "t", MatchMode.START)))), Restrictions.ilike(field, "T", MatchMode.START)));
		    					
	    						example.excludeProperty(field);
		    				}
		    				else if(value.equals("0")){
		    					criteria.add(Restrictions.or(Expression.isNull(field), Restrictions.not(Restrictions.or(Restrictions.or(Restrictions.ilike(field, "1", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "Y", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "y", MatchMode.START), Restrictions.ilike(field, "t", MatchMode.START)))), Restrictions.ilike(field, "T", MatchMode.START)))));
	    						example.excludeProperty(field);
		    				}
		    			}
		    			else if(field.equalsIgnoreCase("processStatus") || field.startsWith("shiptoStj") || field.startsWith("shipfromStj") || field.startsWith("ordrorgnStj")  || 
		    					field.startsWith("ordracptStj") || field.startsWith("firstuseStj") || field.startsWith("billtoStj") ){
		    				//For STJ 1 to 5 together, we will cover them below.
		    				example.excludeProperty(field);
		    			}
		    			else {
		    				boolean startWild = value.startsWith("%");
	    					boolean endWild = value.endsWith("%");
	    					if(startWild && endWild){ //Like %12345% or %123%45%
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
	    						example.excludeProperty(field);
	    					}
	    					else if(startWild && !endWild){ //Like $34567 or $34&567
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
	    						example.excludeProperty(field);
	    					}
	    					else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
	    						example.excludeProperty(field);
	    					}
		    			}
	    			}	
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Long.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Long value = (Long) m.invoke(exampleInstance);
	    			if (value != null && value.longValue()==0l) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}	
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Float.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Float value = (Float) m.invoke(exampleInstance);
	    			if (value != null && value.floatValue()==0.0) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(BigDecimal.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					BigDecimal value = (BigDecimal) m.invoke(exampleInstance);
	    			if (value != null && value.compareTo(BigDecimal.ZERO)==0) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Double.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Double value = (Double) m.invoke(exampleInstance);
	    			if (value != null && value.doubleValue()==0d) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			
		    			if(field.equalsIgnoreCase("invoiceTaxAmt")){
		    				int x = 0;
		    				x = 0;
		    			}
		    			
		    			example.excludeProperty(field);		    		 
	    			}	   			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else{	
    		}
    	}
  	
    	//Search STJ
    	//0003518: Selection Filter - Locations page
    	String stjWhere = "";
    	String stj1Where = "";
    	String stj2Where = "";
    	String stj3Where = "";
    	String stj4Where = "";
    	String stj5Where = "";
    	boolean startWild = false;
    	boolean endWild = false;
    	String stj = "";
    	
    	stj = exampleInstance.getShiptoStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(SHIPTO_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPTO_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPTO_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPTO_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPTO_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(SHIPTO_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPTO_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPTO_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPTO_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPTO_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	

    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getShipfromStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(SHIPFROM_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPFROM_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPFROM_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPFROM_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPFROM_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(SHIPFROM_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPFROM_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPFROM_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPFROM_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPFROM_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	

    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getOrdrorgnStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(ORDRORGN_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRORGN_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRORGN_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRORGN_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRORGN_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(ORDRORGN_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRORGN_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRORGN_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRORGN_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRORGN_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getOrdracptStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(ORDRACPT_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRACPT_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRACPT_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRACPT_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRACPT_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(ORDRACPT_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRACPT_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRACPT_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRACPT_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRACPT_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
		
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getFirstuseStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(FIRSTUSE_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(FIRSTUSE_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(FIRSTUSE_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(FIRSTUSE_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(FIRSTUSE_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(FIRSTUSE_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(FIRSTUSE_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(FIRSTUSE_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(FIRSTUSE_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(FIRSTUSE_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getBilltoStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(BILLTO_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(BILLTO_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(BILLTO_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(BILLTO_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(BILLTO_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(BILLTO_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(BILLTO_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(BILLTO_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(BILLTO_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(BILLTO_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}


    	
    	// Add in glDate restrictions
    	String gldateFilter = "";
		if (exampleInstance.getGlDate() == null && exampleInstance.getGlToDate() == null) {
			example.excludeProperty("glDate");
		}
		else{
			example.excludeProperty("glDate");
			
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

			if(exampleInstance.getGlDate()!=null){		
				String effectiveDate = df.format(exampleInstance.getGlDate());
				if(gldateFilter!=null && gldateFilter.length()>0){
					gldateFilter = gldateFilter + " AND ";
				}
				gldateFilter = gldateFilter + " GL_DATE >= to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
			}
			
			if(exampleInstance.getGlToDate()!=null){		
				String effectiveDateThru = df.format(exampleInstance.getGlToDate());
				if(gldateFilter!=null && gldateFilter.length()>0){
					gldateFilter = gldateFilter + " AND ";
				}
				gldateFilter = gldateFilter + " GL_DATE <= to_date('"+effectiveDateThru+"', 'mm/dd/yyyy') ";
			}
		}
		
		String sqlTotal = "";
		if(gldateFilter!=null && gldateFilter.length()>0){	
			if(sqlTotal.length()>0){
				sqlTotal = sqlTotal + " and ";
	    	}
			sqlTotal = sqlTotal + gldateFilter;
		}
		
		if(stjWhere!=null && stjWhere.length()>0){	
			if(sqlTotal.length()>0){
				sqlTotal = sqlTotal + " and ";
	    	}
			sqlTotal = sqlTotal + stjWhere;
		}
		
		if(sqlTotal.length()>0){
			criteria.add(Restrictions.sqlRestriction(sqlTotal));
    	}
    }
	
	@Override
	protected void extendExampleCriteria(Criteria criteria, BillTransaction exampleInstance) {
		// Hook to provide opportunity to adjust findByExample criteria
		Long id = (exampleInstance == null)? null:exampleInstance.getBilltransId(); 
		if ((id != null) && (id > 0L)) {
			criteria.add(Restrictions.eq("billtransId", id));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<BillTransaction> getAllRecords(BillTransaction exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<BillTransaction> transactionList =  new ArrayList<BillTransaction>();

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(BillTransaction.class);
			Example example = Example.create(exampleInstance);
			exampleHandling(criteria, example, exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			extendExampleCriteria(criteria, exampleInstance);
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            List<BillTransaction> tList = criteria.list();
            for(BillTransaction st : tList){
            	BillTransaction ts = new BillTransaction();
            	org.springframework.beans.BeanUtils.copyProperties(st, ts);
            	transactionList.add(ts);
            }
            
			LogMemory.executeGC();
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return transactionList;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public HashMap<String, HashMap<String, String>>  getNexusInfo(Long billTransactionId) {
		Session localSession = createLocalSession();

		HashMap<String, HashMap<String, String>> result = new HashMap<String, HashMap<String, String>>();
		result.put("Country", new HashMap<String, String>());
		result.get("Country").put("NexusType", "None");
		result.get("Country").put("NexusInd", "None");
		result.put("State", new HashMap<String, String>());
		result.get("State").put("NexusType", "None");
		result.get("State").put("NexusInd", "None");
		result.put("County", new HashMap<String, String>());
		result.get("County").put("NexusType", "None");
		result.get("County").put("NexusInd", "None");
		result.put("City", new HashMap<String, String>());
		result.get("City").put("NexusType", "None");
		result.get("City").put("NexusInd", "None");
		result.put("STJ1", new HashMap<String, String>());
		result.get("STJ1").put("NexusType", "None");
		result.get("STJ1").put("NexusInd", "None");
		result.put("STJ2", new HashMap<String, String>());
		result.get("STJ2").put("NexusType", "None");
		result.get("STJ2").put("NexusInd", "None");
		result.put("STJ3", new HashMap<String, String>());
		result.get("STJ3").put("NexusType", "None");
		result.get("STJ3").put("NexusInd", "None");
		result.put("STJ4", new HashMap<String, String>());
		result.get("STJ4").put("NexusType", "None");
		result.get("STJ4").put("NexusInd", "None");
		result.put("STJ5", new HashMap<String, String>());
		result.get("STJ5").put("NexusType", "None");
		result.get("STJ5").put("NexusInd", "None");
		result.put("STJ6", new HashMap<String, String>());
		result.get("STJ6").put("NexusType", "None");
		result.get("STJ6").put("NexusInd", "None");
		result.put("STJ7", new HashMap<String, String>());
		result.get("STJ7").put("NexusType", "None");
		result.get("STJ7").put("NexusInd", "None");
		result.put("STJ8", new HashMap<String, String>());
		result.get("STJ8").put("NexusType", "None");
		result.get("STJ8").put("NexusInd", "None");
		result.put("STJ9", new HashMap<String, String>());
		result.get("STJ9").put("NexusType", "None");
		result.get("STJ9").put("NexusInd", "None");
		result.put("STJ10", new HashMap<String, String>());
		result.get("STJ10").put("NexusType", "None");
		result.get("STJ10").put("NexusInd", "None");



		try {
			//country
			List<Object[]> countryRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.country_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.country_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.country_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.country_nexusind_code " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (countryRecord!=null && countryRecord.size()>0) {
				if (countryRecord.get(0)[0]!=null)
					result.get("Country").put("NexusType", (String)countryRecord.get(0)[0]);

				if (countryRecord.get(0)[1]!=null)
					result.get("Country").put("NexusInd", (String)countryRecord.get(0)[1]);
			}

			//state
			List<Object[]> stateRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.state_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.state_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.state_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.state_nexusind_code " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stateRecord!=null && stateRecord.size()>0) {
				if (stateRecord.get(0)[0]!=null)
					result.get("State").put("NexusType", (String)stateRecord.get(0)[0]);

				if (stateRecord.get(0)[1]!=null)
					result.get("State").put("NexusInd", (String)stateRecord.get(0)[1]);
			}

			//county
			List<Object[]> countyRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.county_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.county_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.county_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.county_nexusind_code " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (countyRecord!=null && countyRecord.size()>0) {
				if (countyRecord.get(0)[0]!=null)
					result.get("County").put("NexusType", (String)countyRecord.get(0)[0]);

				if (countyRecord.get(0)[1]!=null)
					result.get("County").put("NexusInd", (String)countyRecord.get(0)[1]);
			}

			//city
			List<Object[]> cityRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.city_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.city_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.city_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.city_nexusind_code " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (cityRecord!=null && cityRecord.size()>0) {
				if (cityRecord.get(0)[0]!=null)
					result.get("City").put("NexusType", (String)cityRecord.get(0)[0]);

				if (cityRecord.get(0)[1]!=null)
					result.get("City").put("NexusInd", (String)cityRecord.get(0)[1]);
			}

			//STJxx
			for(int i=1; i<=10; i++) {
				String iNumber = ""+i;
				List<Object[]> stjRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
						" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
						" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj"+iNumber+"_nexus_def_detail_id AND ndd.active_flag='1' " +
						//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj10_taxtype_used_code " +
						" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
						//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj10_situs_jur_id " +
						" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ"+iNumber+"_NEXUSIND_CODE " +
						//" CASE bj.stj10_name " +
						//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
						//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
						//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
						//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
						//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
						//"   END " +
						" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
				if (stjRecord!=null && stjRecord.size()>0) {
					if (stjRecord.get(0)[0]!=null)
						result.get("STJ"+iNumber).put("NexusType", (String)stjRecord.get(0)[0]);

					if (stjRecord.get(0)[1]!=null)
						result.get("STJ"+iNumber).put("NexusInd", (String)stjRecord.get(0)[1]);
				}
			}
			
			/*
			//stj1
			List<Object[]> stj1Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj1_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj1_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj1_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ1_NEXUSIND_CODE " +
					//" CASE bj.stj1_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END" +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj1Record!=null && stj1Record.size()>0) {
				if (stj1Record.get(0)[0]!=null)
					result.get("STJ1").put("NexusType", (String)stj1Record.get(0)[0]);

				if (stj1Record.get(0)[1]!=null)
					result.get("STJ1").put("NexusInd", (String)stj1Record.get(0)[1]);
			}

			//stj2
			List<Object[]> stj2Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj2_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj2_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj2_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ2_NEXUSIND_CODE " +
					//" CASE bj.stj2_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj2Record!=null && stj2Record.size()>0) {
				if (stj2Record.get(0)[0]!=null)
					result.get("STJ2").put("NexusType", (String)stj2Record.get(0)[0]);

				if (stj2Record.get(0)[1]!=null)
					result.get("STJ2").put("NexusInd", (String)stj2Record.get(0)[1]);
			}

			//stj3
			List<Object[]> stj3Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj3_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj3_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj3_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ3_NEXUSIND_CODE " +
					//" CASE bj.stj3_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj3Record!=null && stj3Record.size()>0) {
				if (stj3Record.get(0)[0]!=null)
					result.get("STJ3").put("NexusType", (String)stj3Record.get(0)[0]);

				if (stj3Record.get(0)[1]!=null)
					result.get("STJ3").put("NexusInd", (String)stj3Record.get(0)[1]);
			}

			//stj4
			List<Object[]> stj4Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj4_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj4_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj4_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ4_NEXUSIND_CODE " +
					//" CASE bj.stj4_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj4Record!=null && stj4Record.size()>0) {
				if (stj4Record.get(0)[0]!=null)
					result.get("STJ4").put("NexusType", (String)stj4Record.get(0)[0]);

				if (stj4Record.get(0)[1]!=null)
					result.get("STJ4").put("NexusInd", (String)stj4Record.get(0)[1]);
			}

			//stj5
			List<Object[]> stj5Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj5_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj5_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj5_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ5_NEXUSIND_CODE " +
					//" CASE bj.stj5_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj5Record!=null && stj5Record.size()>0) {
				if (stj5Record.get(0)[0]!=null)
					result.get("STJ5").put("NexusType", (String)stj5Record.get(0)[0]);

				if (stj5Record.get(0)[1]!=null)
					result.get("STJ5").put("NexusInd", (String)stj5Record.get(0)[1]);
			}


			//stj6
			List<Object[]> stj6Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj6_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj6_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj6_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ6_NEXUSIND_CODE " +
					//" CASE bj.stj6_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj6Record!=null && stj6Record.size()>0) {
				if (stj6Record.get(0)[0]!=null)
					result.get("STJ6").put("NexusType", (String)stj6Record.get(0)[0]);

				if (stj6Record.get(0)[1]!=null)
					result.get("STJ6").put("NexusInd", (String)stj6Record.get(0)[1]);
			}

			//stj7
			List<Object[]> stj7Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj7_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj7_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj7_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ7_NEXUSIND_CODE " +
					//" CASE bj.stj7_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj7Record!=null && stj7Record.size()>0) {
				if (stj7Record.get(0)[0]!=null)
					result.get("STJ7").put("NexusType", (String)stj7Record.get(0)[0]);

				if (stj7Record.get(0)[1]!=null)
					result.get("STJ7").put("NexusInd", (String)stj7Record.get(0)[1]);
			}

			//stj8
			List<Object[]> stj8Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj8_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj8_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj8_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ8_NEXUSIND_CODE " +
					//" CASE bj.stj8_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj8Record!=null && stj8Record.size()>0) {
				if (stj8Record.get(0)[0]!=null)
					result.get("STJ8").put("NexusType", (String)stj8Record.get(0)[0]);

				if (stj8Record.get(0)[1]!=null)
					result.get("STJ8").put("NexusInd", (String)stj8Record.get(0)[1]);
			}

			//stj9
			List<Object[]> stj9Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj9_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj9_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj9_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ9_NEXUSIND_CODE " +
					//" CASE bj.stj9_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj9Record!=null && stj9Record.size()>0) {
				if (stj9Record.get(0)[0]!=null)
					result.get("STJ9").put("NexusType", (String)stj9Record.get(0)[0]);

				if (stj9Record.get(0)[1]!=null)
					result.get("STJ9").put("NexusInd", (String)stj9Record.get(0)[1]);
			}

			//stj10
			List<Object[]> stj10Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_billtrans st " +
					" LEFT OUTER JOIN TB_BILLTRANS_JURDTL bj ON bj.billtrans_jurdtl_id=st.billtrans_Id " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=bj.stj10_nexus_def_detail_id AND ndd.active_flag='1' " +
					//" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=bj.stj10_taxtype_used_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					//" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = bj.stj10_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=BJ.STJ10_NEXUSIND_CODE " +
					//" CASE bj.stj10_name " +
					//"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					//"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					//"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					//"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					//"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					//"   END " +
					" WHERE st.billtrans_Id IN (:billtransId)").setParameter("billtransId", billTransactionId).list();
			if (stj10Record!=null && stj10Record.size()>0) {
				if (stj10Record.get(0)[0]!=null)
					result.get("STJ10").put("NexusType", (String)stj10Record.get(0)[0]);

				if (stj10Record.get(0)[1]!=null)
					result.get("STJ10").put("NexusInd", (String)stj10Record.get(0)[1]);
			}


			int i = 0;
			*/
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		} finally {
			closeLocalSession(localSession);
		}
		return result;
	}
	
	public String zeroDefault(BigDecimal b) {
		if(b==null){return "0.00";}
		else if(b.compareTo(BigDecimal.ZERO)==0) {return "0.00";}
		else{return b.toString();}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public HashMap<String, String> getTaxRateInfo(Long billtransId) {
		
		//Change STATE to COUNTY , CITY
		//Change SALES to USE

		//ALWAYS_MINIMUM 		STATE_TIER1_TAXABLE_AMT	STATE_SALES_TIER1_RATE	STATE_SALES_TIER1_SETAMT	STATE_SALES_TIER1_MAX_AMT	STATE_TIER1_TAX_AMT
		//STATE_SALES_TIER2_MIN_AMT	STATE_TIER2_TAXABLE_AMT	STATE_SALES_TIER2_RATE	STATE_SALES_TIER2_SETAMT	STATE_SALES_TIER2_MAX_AMT	STATE_TIER2_TAX_AMT
		//STATE_SALES_TIER3_MIN_AMT_FAKE	STATE_TIER3_TAXABLE_AMT	STATE_SALES_TIER3_RATE	STATE_SALES_TIER3_SETAMT	ALWAYS_MAXIMUM		STATE_TIER3_TAX_AMT
		//STATE_SALES_MAXTAX_AMT
		
		HashMap<String, String> result = new HashMap<String, String>();
		try {
			BillTransaction billtrans = getJpaTemplate().find(BillTransaction.class, billtransId);
			if(billtrans == null){
				return result;
			}

			BigDecimal combibedRate = new BigDecimal("0.00");
			result.put("COMBINED_RATES", "0.00");
			
			//For Country
			if(billtrans.getCountryTaxrateId()!=null && billtrans.getCountryTaxrateId() > 0L) {
				JurisdictionTaxrate jurisdictionTaxrate = getJpaTemplate().find(JurisdictionTaxrate.class, billtrans.getCountryTaxrateId());
				if(jurisdictionTaxrate!=null){
					if(billtrans.getCountryTaxtypeUsedCode()!=null && billtrans.getCountryTaxtypeUsedCode().contentEquals("S")) {//for sales		
						if(jurisdictionTaxrate.getCountrySalesTier1Rate()!=null) {
							combibedRate = combibedRate.add(jurisdictionTaxrate.getCountrySalesTier1Rate());
						}
					}
					else if(billtrans.getCountryTaxtypeUsedCode()!=null && (billtrans.getCountryTaxtypeUsedCode().contentEquals("U") || 
																					billtrans.getCountryTaxtypeUsedCode().contentEquals("V"))) {//for use
						if(jurisdictionTaxrate.getCountryUseTier1Rate()!=null) {
							combibedRate = combibedRate.add(jurisdictionTaxrate.getCountryUseTier1Rate());
						}
					}
				}
			}

			//Start of State information
			result.put("STATE_ALWAYS_MINIMUM", "0.00");
			result.put("STATE_ALWAYS_MAXIMUM", "No Limit");	
			result.put("STATE_TIER1_TAXABLE_AMT", zeroDefault(billtrans.getStateTier1TaxableAmt()));
			result.put("STATE_TIER2_TAX_AMT", zeroDefault(billtrans.getStateTier2TaxAmt()));
			result.put("STATE_TIER1_TAX_AMT", zeroDefault(billtrans.getStateTier1TaxAmt()));
			result.put("STATE_TIER2_TAXABLE_AMT", zeroDefault(billtrans.getStateTier2TaxableAmt()));	
			result.put("STATE_TIER3_TAXABLE_AMT", zeroDefault(billtrans.getStateTier3TaxableAmt()));
			result.put("STATE_TIER3_TAX_AMT", zeroDefault(billtrans.getStateTier3TaxAmt()));
			
			if(billtrans.getStateTaxrateId()!=null && billtrans.getStateTaxrateId() > 0L) {
				JurisdictionTaxrate taxrate = getJpaTemplate().find(JurisdictionTaxrate.class, billtrans.getStateTaxrateId());
				if(taxrate!=null){
					if(billtrans.getStateTaxtypeUsedCode()!=null && billtrans.getStateTaxtypeUsedCode().contentEquals("S")) {//for sales		
						result.put("STATE_SALES_TIER1_RATE", zeroDefault(taxrate.getStateSalesTier1Rate()));
						result.put("STATE_SALES_TIER1_SETAMT", zeroDefault(taxrate.getStateSalesTier1Setamt()));
						
						if(taxrate.getStateSalesTier1MaxAmt()!=null && taxrate.getStateSalesTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("STATE_SALES_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("STATE_SALES_TIER1_MAX_AMT", zeroDefault(taxrate.getStateSalesTier1MaxAmt()));	
						}					
						result.put("STATE_SALES_TIER2_MIN_AMT", zeroDefault(taxrate.getStateSalesTier2MinAmt()));	
						result.put("STATE_SALES_TIER2_RATE", zeroDefault(taxrate.getStateSalesTier2Rate()));
						result.put("STATE_SALES_TIER2_SETAMT", zeroDefault(taxrate.getStateSalesTier2Setamt()));
						
						if(taxrate.getStateSalesTier2MaxAmt()!=null && taxrate.getStateSalesTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("STATE_SALES_TIER2_MAX_AMT", "No Limit");
							result.put("STATE_SALES_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("STATE_SALES_TIER2_MAX_AMT", zeroDefault(taxrate.getStateSalesTier2MaxAmt()));
							result.put("STATE_SALES_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getStateSalesTier2MaxAmt()));
						}			
						result.put("STATE_SALES_TIER3_RATE", zeroDefault(taxrate.getStateSalesTier3Rate()));
						result.put("STATE_SALES_TIER3_SETAMT", zeroDefault(taxrate.getStateSalesTier3Setamt()));
						result.put("STATE_SALES_MAXTAX_AMT", zeroDefault(taxrate.getStateSalesMaxtaxAmt()));
						
						if(taxrate.getStateSalesTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getStateSalesTier1Rate());
						}
					}
					else if(billtrans.getStateTaxtypeUsedCode()!=null && (billtrans.getStateTaxtypeUsedCode().contentEquals("U") || billtrans.getStateTaxtypeUsedCode().contentEquals("V"))) {//for use
						result.put("STATE_USE_TIER1_RATE", zeroDefault(taxrate.getStateUseTier1Rate()));
						result.put("STATE_USE_TIER1_SETAMT", zeroDefault(taxrate.getStateUseTier1Setamt()));	
						
						if(taxrate.getStateUseTier1MaxAmt()!=null && taxrate.getStateUseTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("STATE_USE_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("STATE_USE_TIER1_MAX_AMT", zeroDefault(taxrate.getStateUseTier1MaxAmt()));	
						}			
						result.put("STATE_USE_TIER2_MIN_AMT", zeroDefault(taxrate.getStateUseTier2MinAmt()));	
						result.put("STATE_USE_TIER2_RATE", zeroDefault(taxrate.getStateUseTier2Rate()));
						result.put("STATE_USE_TIER2_SETAMT", zeroDefault(taxrate.getStateUseTier2Setamt()));	
						
						if(taxrate.getStateUseTier2MaxAmt()!=null && taxrate.getStateUseTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("STATE_USE_TIER2_MAX_AMT", "No Limit");
							result.put("STATE_USE_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("STATE_USE_TIER2_MAX_AMT", zeroDefault(taxrate.getStateUseTier2MaxAmt()));
							result.put("STATE_USE_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getStateUseTier2MaxAmt()));
						}		
						result.put("STATE_USE_TIER3_RATE", zeroDefault(taxrate.getStateUseTier3Rate()));
						result.put("STATE_USE_TIER3_SETAMT", zeroDefault(taxrate.getStateUseTier3Setamt()));
						result.put("STATE_USE_MAXTAX_AMT", zeroDefault(taxrate.getStateUseMaxtaxAmt()));
						
						if(taxrate.getStateUseTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getStateUseTier1Rate());
						}
					}
				}
			}
			
			if((billtrans.getStateTier3TaxableAmt()==null || billtrans.getStateTier3TaxableAmt().compareTo(BigDecimal.ZERO)==0) && 
					(billtrans.getStateTier3TaxAmt()==null || billtrans.getStateTier3TaxAmt().compareTo(BigDecimal.ZERO)==0)) {				
				result.put("STATE_TIER3_TAXABLE_AMT", "");	
				result.put("STATE_ALWAYS_MAXIMUM", "");
				result.put("STATE_TIER3_TAX_AMT", "");
				
				if(billtrans.getStateTaxtypeUsedCode()!=null && billtrans.getStateTaxtypeUsedCode().contentEquals("S")) {//for sales		
					result.put("STATE_SALES_TIER3_MIN_AMT_FAKE", "");
					result.put("STATE_SALES_TIER3_RATE", "");
					result.put("STATE_SALES_TIER3_SETAMT", "");
				}
				else if(billtrans.getStateTaxtypeUsedCode()!=null && (billtrans.getStateTaxtypeUsedCode().contentEquals("U") || billtrans.getStateTaxtypeUsedCode().contentEquals("V"))) {//for use
					result.put("STATE_USE_TIER3_MIN_AMT_FAKE", "");
					result.put("STATE_USE_TIER3_RATE", "");
					result.put("STATE_USE_TIER3_SETAMT", "");
				}
			}
			//End of State information
			
			//Start of County information
			result.put("COUNTY_ALWAYS_MINIMUM", "0.00");
			result.put("COUNTY_ALWAYS_MAXIMUM", "No Limit");	
			result.put("COUNTY_TIER1_TAXABLE_AMT", zeroDefault(billtrans.getCountyTier1TaxableAmt()));
			result.put("COUNTY_TIER2_TAX_AMT", zeroDefault(billtrans.getCountyTier2TaxAmt()));
			result.put("COUNTY_TIER1_TAX_AMT", zeroDefault(billtrans.getCountyTier1TaxAmt()));
			result.put("COUNTY_TIER2_TAXABLE_AMT", zeroDefault(billtrans.getCountyTier2TaxableAmt()));	
			result.put("COUNTY_TIER3_TAXABLE_AMT", zeroDefault(billtrans.getCountyTier3TaxableAmt()));
			result.put("COUNTY_TIER3_TAX_AMT", zeroDefault(billtrans.getCountyTier3TaxAmt()));
			
			if(billtrans.getCountyTaxrateId()!=null && billtrans.getCountyTaxrateId() > 0L) {
				JurisdictionTaxrate taxrate = getJpaTemplate().find(JurisdictionTaxrate.class, billtrans.getCountyTaxrateId());
				if(taxrate!=null){
					if(billtrans.getCountyTaxtypeUsedCode()!=null && billtrans.getCountyTaxtypeUsedCode().contentEquals("S")) {//for sales		
						result.put("COUNTY_SALES_TIER1_RATE", zeroDefault(taxrate.getCountySalesTier1Rate()));
						result.put("COUNTY_SALES_TIER1_SETAMT", zeroDefault(taxrate.getCountySalesTier1Setamt()));
						
						if(taxrate.getCountySalesTier1MaxAmt()!=null && taxrate.getCountySalesTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("COUNTY_SALES_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("COUNTY_SALES_TIER1_MAX_AMT", zeroDefault(taxrate.getCountySalesTier1MaxAmt()));	
						}					
						result.put("COUNTY_SALES_TIER2_MIN_AMT", zeroDefault(taxrate.getCountySalesTier2MinAmt()));	
						result.put("COUNTY_SALES_TIER2_RATE", zeroDefault(taxrate.getCountySalesTier2Rate()));
						result.put("COUNTY_SALES_TIER2_SETAMT", zeroDefault(taxrate.getCountySalesTier2Setamt()));
						
						if(taxrate.getCountySalesTier2MaxAmt()!=null && taxrate.getCountySalesTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("COUNTY_SALES_TIER2_MAX_AMT", "No Limit");
							result.put("COUNTY_SALES_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("COUNTY_SALES_TIER2_MAX_AMT", zeroDefault(taxrate.getCountySalesTier2MaxAmt()));
							result.put("COUNTY_SALES_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getCountySalesTier2MaxAmt()));
						}			
						result.put("COUNTY_SALES_TIER3_RATE", zeroDefault(taxrate.getCountySalesTier3Rate()));
						result.put("COUNTY_SALES_TIER3_SETAMT", zeroDefault(taxrate.getCountySalesTier3Setamt()));
						result.put("COUNTY_SALES_MAXTAX_AMT", zeroDefault(taxrate.getCountySalesMaxtaxAmt()));
						
						if(taxrate.getCountySalesTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getCountySalesTier1Rate());
						}
					}
					else if(billtrans.getCountyTaxtypeUsedCode()!=null && (billtrans.getCountyTaxtypeUsedCode().contentEquals("U") || billtrans.getCountyTaxtypeUsedCode().contentEquals("V"))) {//for use
						result.put("COUNTY_USE_TIER1_RATE", zeroDefault(taxrate.getCountyUseTier1Rate()));
						result.put("COUNTY_USE_TIER1_SETAMT", zeroDefault(taxrate.getCountyUseTier1Setamt()));	
						
						if(taxrate.getCountyUseTier1MaxAmt()!=null && taxrate.getCountyUseTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("COUNTY_USE_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("COUNTY_USE_TIER1_MAX_AMT", zeroDefault(taxrate.getCountyUseTier1MaxAmt()));	
						}			
						result.put("COUNTY_USE_TIER2_MIN_AMT", zeroDefault(taxrate.getCountyUseTier2MinAmt()));	
						result.put("COUNTY_USE_TIER2_RATE", zeroDefault(taxrate.getCountyUseTier2Rate()));
						result.put("COUNTY_USE_TIER2_SETAMT", zeroDefault(taxrate.getCountyUseTier2Setamt()));	
						
						if(taxrate.getCountyUseTier2MaxAmt()!=null && taxrate.getCountyUseTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("COUNTY_USE_TIER2_MAX_AMT", "No Limit");
							result.put("COUNTY_USE_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("COUNTY_USE_TIER2_MAX_AMT", zeroDefault(taxrate.getCountyUseTier2MaxAmt()));
							result.put("COUNTY_USE_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getCountyUseTier2MaxAmt()));
						}		
						result.put("COUNTY_USE_TIER3_RATE", zeroDefault(taxrate.getCountyUseTier3Rate()));
						result.put("COUNTY_USE_TIER3_SETAMT", zeroDefault(taxrate.getCountyUseTier3Setamt()));
						result.put("COUNTY_USE_MAXTAX_AMT", zeroDefault(taxrate.getCountyUseMaxtaxAmt()));
						
						if(taxrate.getCountyUseTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getCountyUseTier1Rate());
						}
					}
				}
			}
			
			if((billtrans.getCountyTier3TaxableAmt()==null || billtrans.getCountyTier3TaxableAmt().compareTo(BigDecimal.ZERO)==0) && 
					(billtrans.getCountyTier3TaxAmt()==null || billtrans.getCountyTier3TaxAmt().compareTo(BigDecimal.ZERO)==0)) {				
				result.put("COUNTY_TIER3_TAXABLE_AMT", "");	
				result.put("COUNTY_ALWAYS_MAXIMUM", "");
				result.put("COUNTY_TIER3_TAX_AMT", "");
				
				if(billtrans.getCountyTaxtypeUsedCode()!=null && billtrans.getCountyTaxtypeUsedCode().contentEquals("S")) {//for sales		
					result.put("COUNTY_SALES_TIER3_MIN_AMT_FAKE", "");
					result.put("COUNTY_SALES_TIER3_RATE", "");
					result.put("COUNTY_SALES_TIER3_SETAMT", "");
				}
				else if(billtrans.getCountyTaxtypeUsedCode()!=null && (billtrans.getCountyTaxtypeUsedCode().contentEquals("U") || billtrans.getCountyTaxtypeUsedCode().contentEquals("V"))) {//for use
					result.put("COUNTY_USE_TIER3_MIN_AMT_FAKE", "");
					result.put("COUNTY_USE_TIER3_RATE", "");
					result.put("COUNTY_USE_TIER3_SETAMT", "");
				}
			}
			//End of County information

			//Start of City information
			result.put("CITY_ALWAYS_MINIMUM", "0.00");
			result.put("CITY_ALWAYS_MAXIMUM", "No Limit");	
			result.put("CITY_TIER1_TAXABLE_AMT", zeroDefault(billtrans.getCityTier1TaxableAmt()));
			result.put("CITY_TIER2_TAX_AMT", zeroDefault(billtrans.getCityTier2TaxAmt()));
			result.put("CITY_TIER1_TAX_AMT", zeroDefault(billtrans.getCityTier1TaxAmt()));
			result.put("CITY_TIER2_TAXABLE_AMT", zeroDefault(billtrans.getCityTier2TaxableAmt()));	
			result.put("CITY_TIER3_TAXABLE_AMT", zeroDefault(billtrans.getCityTier3TaxableAmt()));
			result.put("CITY_TIER3_TAX_AMT", zeroDefault(billtrans.getCityTier3TaxAmt()));
			
			if(billtrans.getCityTaxrateId()!=null && billtrans.getCityTaxrateId() > 0L) {
				JurisdictionTaxrate taxrate = getJpaTemplate().find(JurisdictionTaxrate.class, billtrans.getCityTaxrateId());
				if(taxrate!=null){
					if(billtrans.getCityTaxtypeUsedCode()!=null && billtrans.getCityTaxtypeUsedCode().contentEquals("S")) {//for sales		
						result.put("CITY_SALES_TIER1_RATE", zeroDefault(taxrate.getCitySalesTier1Rate()));
						result.put("CITY_SALES_TIER1_SETAMT", zeroDefault(taxrate.getCitySalesTier1Setamt()));
						
						if(taxrate.getCitySalesTier1MaxAmt()!=null && taxrate.getCitySalesTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("CITY_SALES_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("CITY_SALES_TIER1_MAX_AMT", zeroDefault(taxrate.getCitySalesTier1MaxAmt()));	
						}					
						result.put("CITY_SALES_TIER2_MIN_AMT", zeroDefault(taxrate.getCitySalesTier2MinAmt()));	
						result.put("CITY_SALES_TIER2_RATE", zeroDefault(taxrate.getCitySalesTier2Rate()));
						result.put("CITY_SALES_TIER2_SETAMT", zeroDefault(taxrate.getCitySalesTier2Setamt()));
						
						if(taxrate.getCitySalesTier2MaxAmt()!=null && taxrate.getCitySalesTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("CITY_SALES_TIER2_MAX_AMT", "No Limit");
							result.put("CITY_SALES_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("CITY_SALES_TIER2_MAX_AMT", zeroDefault(taxrate.getCitySalesTier2MaxAmt()));
							result.put("CITY_SALES_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getCitySalesTier2MaxAmt()));
						}			
						result.put("CITY_SALES_TIER3_RATE", zeroDefault(taxrate.getCitySalesTier3Rate()));
						result.put("CITY_SALES_TIER3_SETAMT", zeroDefault(taxrate.getCitySalesTier3Setamt()));
						result.put("CITY_SALES_MAXTAX_AMT", zeroDefault(taxrate.getCitySalesMaxtaxAmt()));
						
						if(taxrate.getCitySalesTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getCitySalesTier1Rate());
						}
					}
					else if(billtrans.getCityTaxtypeUsedCode()!=null && (billtrans.getCityTaxtypeUsedCode().contentEquals("U") || billtrans.getCityTaxtypeUsedCode().contentEquals("V"))) {//for use
						result.put("CITY_USE_TIER1_RATE", zeroDefault(taxrate.getCityUseTier1Rate()));
						result.put("CITY_USE_TIER1_SETAMT", zeroDefault(taxrate.getCityUseTier1Setamt()));	
						
						if(taxrate.getCityUseTier1MaxAmt()!=null && taxrate.getCityUseTier1MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("CITY_USE_TIER1_MAX_AMT", "No Limit");
						}
						else {
							result.put("CITY_USE_TIER1_MAX_AMT", zeroDefault(taxrate.getCityUseTier1MaxAmt()));	
						}			
						result.put("CITY_USE_TIER2_MIN_AMT", zeroDefault(taxrate.getCityUseTier2MinAmt()));	
						result.put("CITY_USE_TIER2_RATE", zeroDefault(taxrate.getCityUseTier2Rate()));
						result.put("CITY_USE_TIER2_SETAMT", zeroDefault(taxrate.getCityUseTier2Setamt()));	
						
						if(taxrate.getCityUseTier2MaxAmt()!=null && taxrate.getCityUseTier2MaxAmt().compareTo(BigDecimal.valueOf(999999999l))==0) {
							result.put("CITY_USE_TIER2_MAX_AMT", "No Limit");
							result.put("CITY_USE_TIER3_MIN_AMT_FAKE", "No Limit");
						}
						else {
							result.put("CITY_USE_TIER2_MAX_AMT", zeroDefault(taxrate.getCityUseTier2MaxAmt()));
							result.put("CITY_USE_TIER3_MIN_AMT_FAKE", zeroDefault(taxrate.getCityUseTier2MaxAmt()));
						}		
						result.put("CITY_USE_TIER3_RATE", zeroDefault(taxrate.getCityUseTier3Rate()));
						result.put("CITY_USE_TIER3_SETAMT", zeroDefault(taxrate.getCityUseTier3Setamt()));
						result.put("CITY_USE_MAXTAX_AMT", zeroDefault(taxrate.getCityUseMaxtaxAmt()));
						
						if(taxrate.getCityUseTier1Rate()!=null) {
							combibedRate = combibedRate.add(taxrate.getCityUseTier1Rate());
						}
					}
				}
			}
			
			if((billtrans.getCityTier3TaxableAmt()==null || billtrans.getCityTier3TaxableAmt().compareTo(BigDecimal.ZERO)==0) && 
					(billtrans.getCityTier3TaxAmt()==null || billtrans.getCityTier3TaxAmt().compareTo(BigDecimal.ZERO)==0)) {				
				result.put("CITY_TIER3_TAXABLE_AMT", "");	
				result.put("CITY_ALWAYS_MAXIMUM", "");
				result.put("CITY_TIER3_TAX_AMT", "");
				
				if(billtrans.getCityTaxtypeUsedCode()!=null && billtrans.getCityTaxtypeUsedCode().contentEquals("S")) {//for sales		
					result.put("CITY_SALES_TIER3_MIN_AMT_FAKE", "");
					result.put("CITY_SALES_TIER3_RATE", "");
					result.put("CITY_SALES_TIER3_SETAMT", "");
				}
				else if(billtrans.getCityTaxtypeUsedCode()!=null && (billtrans.getCityTaxtypeUsedCode().contentEquals("U") || billtrans.getCityTaxtypeUsedCode().contentEquals("V"))) {//for use
					result.put("CITY_USE_TIER3_MIN_AMT_FAKE", "");
					result.put("CITY_USE_TIER3_RATE", "");
					result.put("CITY_USE_TIER3_SETAMT", "");
				}
			}
			//End of City information
			
			//For STJ 1 to 5
			for(int i=1; i <= 5; i++){			
				BigDecimal stjTaxableAmt = valueOfBigDecimal(billtrans,"getStj"+i+"TaxableAmt");
				BigDecimal stjTaxAmt = valueOfBigDecimal(billtrans,"getStj"+i+"TaxAmt");			
				if((stjTaxableAmt==null || stjTaxableAmt.compareTo(BigDecimal.ZERO)==0) && 
						(stjTaxAmt==null || stjTaxAmt.compareTo(BigDecimal.ZERO)==0)) {
					continue;
				}

				result.put("STJ"+i+"_TAXABLE_AMT", zeroDefault(stjTaxableAmt));
				result.put("STJ"+i+"_TAX_AMT", zeroDefault(stjTaxAmt));
				Long stjTaxrateId = valueOfLong(billtrans,"getStj"+i+"TaxrateId");
				if(stjTaxrateId!=null && stjTaxrateId > 0L) {
					JurisdictionTaxrate taxrate = getJpaTemplate().find(JurisdictionTaxrate.class, stjTaxrateId);
					if(taxrate!=null){
						String taxtypeUsedCode = valueOfString(billtrans,"getStj"+i+"TaxtypeUsedCode");
						if(taxtypeUsedCode!=null && taxtypeUsedCode.contentEquals("S")) {//for sales		
							result.put("STJ"+i+"_SALES_RATE", zeroDefault(valueOfBigDecimal(taxrate,"getStj"+i+"SalesRate")));
							result.put("STJ"+i+"_SALES_SETAMT", zeroDefault(valueOfBigDecimal(taxrate,"getStj"+i+"SalesSetamt")));
							
							if(valueOfBigDecimal(taxrate,"getStj"+i+"SalesRate")!=null) {
								combibedRate = combibedRate.add(valueOfBigDecimal(taxrate,"getStj"+i+"SalesRate"));
							}
						}
						else if(taxtypeUsedCode!=null && (taxtypeUsedCode.contentEquals("U") || taxtypeUsedCode.contentEquals("V"))) {//for use
							result.put("STJ"+i+"_USE_RATE", zeroDefault(valueOfBigDecimal(taxrate,"getStj"+i+"UseRate")));
							result.put("STJ"+i+"_USE_SETAMT", zeroDefault(valueOfBigDecimal(taxrate,"getStj"+i+"UseSetamt")));						
							if(valueOfBigDecimal(taxrate,"getStj"+i+"UseRate")!=null) {
								combibedRate = combibedRate.add(valueOfBigDecimal(taxrate,"getStj"+i+"UseRate"));
							}
						}
					}
				}
			}
			
			//For STJ 6 to 10
			Session localSession = createLocalSession();
			try {
				for(int i=6; i<=10; i++) {
					BigDecimal stjTaxableAmt = valueOfBigDecimal(billtrans,"getStj"+i+"TaxableAmt");
					BigDecimal stjTaxAmt = valueOfBigDecimal(billtrans,"getStj"+i+"TaxAmt");		
					if((stjTaxableAmt==null || stjTaxableAmt.compareTo(BigDecimal.ZERO)==0) && 
							(stjTaxAmt==null || stjTaxAmt.compareTo(BigDecimal.ZERO)==0)) {
						continue;
					}

					result.put("STJ"+i+"_TAXABLE_AMT", zeroDefault(stjTaxableAmt));
					result.put("STJ"+i+"_TAX_AMT", zeroDefault(stjTaxAmt));
					Long stjTaxrateId = valueOfLong(billtrans,"getStj"+i+"TaxrateId");
					String stjName = valueOfString(billtrans,"getStj"+i+"Name");
					if((stjTaxrateId!=null && stjTaxrateId > 0L) && (stjName!=null && stjName.length()>0)) {
						List<Object[]> stjRecord = localSession.createSQLQuery(
								"SELECT jt.jurisdiction_id, jt.jurisdiction_taxrate_id, " +
								"   CASE " +
								"      WHEN j.stj1_name = '"+stjName+"' THEN jt.stj1_sales_rate " +
								"      WHEN j.stj2_name = '"+stjName+"' THEN jt.stj2_sales_rate " +
								"      WHEN j.stj3_name = '"+stjName+"' THEN jt.stj3_sales_rate " +
								"      WHEN j.stj4_name = '"+stjName+"' THEN jt.stj4_sales_rate " +
								"      WHEN j.stj5_name = '"+stjName+"' THEN jt.stj5_sales_rate " +
								"   END stj"+i+"_sales_rate, " +
								"   CASE " +
								"      WHEN j.stj1_name = '"+stjName+"' THEN jt.stj1_use_rate " +
								"      WHEN j.stj2_name = '"+stjName+"' THEN jt.stj2_use_rate " +
								"      WHEN j.stj3_name = '"+stjName+"' THEN jt.stj3_use_rate " +
								"      WHEN j.stj4_name = '"+stjName+"' THEN jt.stj4_use_rate " +
								"      WHEN j.stj5_name = '"+stjName+"' THEN jt.stj5_use_rate " +
								"   END stj"+i+"_use_rate, " +
								"	CASE " +
							    "  	   WHEN j.stj1_name = '"+stjName+"' THEN jt.stj1_sales_setamt " +
							    "      WHEN j.stj2_name = '"+stjName+"' THEN jt.stj2_sales_setamt " +
							    "      WHEN j.stj3_name = '"+stjName+"' THEN jt.stj3_sales_setamt " +
							    "      WHEN j.stj4_name = '"+stjName+"' THEN jt.stj4_sales_setamt " +
							    "      WHEN j.stj5_name = '"+stjName+"' THEN jt.stj5_sales_setamt " +
							    " 	END stj"+i+"_sales_setamt, " +
							    "	CASE " +
							    "  	   WHEN j.stj1_name = '"+stjName+"' THEN jt.stj1_use_setamt " +
							    "  	   WHEN j.stj2_name = '"+stjName+"' THEN jt.stj2_use_setamt " +
							    "  	   WHEN j.stj3_name = '"+stjName+"' THEN jt.stj3_use_setamt " +
							    "      WHEN j.stj4_name = '"+stjName+"' THEN jt.stj4_use_setamt " +
							    "      WHEN j.stj5_name = '"+stjName+"' THEN jt.stj5_use_setamt " +
							    "	END stj"+i+"_use_setamt " +
								"FROM tb_jurisdiction_taxrate jt " +
								"LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = jt.jurisdiction_id " +
								"WHERE jt.jurisdiction_taxrate_id = :taxrateid").setParameter("taxrateid", stjTaxrateId).list();

						Double salesrate = ((Number)stjRecord.get(0)[2]).doubleValue();
						Double userate = ((Number)stjRecord.get(0)[3]).doubleValue();
						Double salessetamt = ((Number)stjRecord.get(0)[4]).doubleValue();
						Double usesetamt = ((Number)stjRecord.get(0)[5]).doubleValue();
						
						if (stjRecord!=null && stjRecord.size()>0) {
							String taxtypeUsedCode = valueOfString(billtrans,"getStj"+i+"TaxtypeUsedCode");
							if(taxtypeUsedCode!=null && taxtypeUsedCode.contentEquals("S")) {//for sales	
								if (salesrate!=null) {	
									result.put("STJ"+i+"_SALES_RATE", zeroDefault(new BigDecimal(salesrate.toString())));
									combibedRate = combibedRate.add(new BigDecimal(salesrate.toString()));
								}
								if (salessetamt!=null) {
									result.put("STJ"+i+"_SALES_SETAMT", zeroDefault(new BigDecimal(salessetamt.toString())));
								}
							}
							else if(taxtypeUsedCode!=null && (taxtypeUsedCode.contentEquals("U") || taxtypeUsedCode.contentEquals("V"))) {//for use
								if (userate!=null) {
									result.put("STJ"+i+"_USE_RATE", zeroDefault(new BigDecimal(userate.toString())));						
									combibedRate = combibedRate.add(new BigDecimal(userate.toString()));
								}
								if (usesetamt!=null) {
									result.put("STJ"+i+"_USE_SETAMT", zeroDefault(new BigDecimal(usesetamt.toString())));						
								}
							}
						}							
					}
				}
		
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			} finally {
				closeLocalSession(localSession);
			}
			
			result.put("COMBINED_RATES", ""+combibedRate.doubleValue());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		} finally {
		}
		return result;
	}
	
	public BigDecimal valueOfBigDecimal(Object instance, String name) {
		try {
			Method aMethod = instance.getClass().getDeclaredMethod(name, new Class[]{});
			return (BigDecimal)aMethod.invoke(instance, new Object[]{});
		} catch(Exception e) {
			return null;
		} finally {
		}
	}
	
	public String valueOfString(Object instance, String name) {
		try {
			Method aMethod = instance.getClass().getDeclaredMethod(name, new Class[]{});
			return (String)aMethod.invoke(instance, new Object[]{});
		} catch(Exception e) {
			return null;
		} finally {
		}
	}
	
	public Long valueOfLong(Object instance, String name) {
		try {
			Method aMethod = instance.getClass().getDeclaredMethod(name, new Class[]{});
			return (Long)aMethod.invoke(instance, new Object[]{});
		} catch(Exception e) {
			return null;
		} finally {
		}
	}
	
	

}