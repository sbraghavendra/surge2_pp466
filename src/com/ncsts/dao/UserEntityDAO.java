package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.UserDTO;

/**
 * 
 * @author Paul Govindan
 * 
 *
 */
public interface UserEntityDAO {
	
	public abstract List<UserEntity> findByUserEntity(UserEntity userEntity, int count, String... excludeProperty) 
	    throws DataAccessException;		
	
	public List<UserEntity> findByUserCode(String userCode) throws DataAccessException;
	
    public abstract List<UserEntity> findAll() throws DataAccessException;	
    
    public List<Object[]> findByRoleCode(String roleCode) throws DataAccessException;
    
    public List<Object[]> findForUserCode(String userCode) throws DataAccessException;
	
    public void saveOrUpdate(UserEntity userEntity) throws DataAccessException;
    
    public void delete(UserEntity userEntity) throws DataAccessException;

  	public EntityItemDTO getEntityItemDTO(UserDTO user) throws DataAccessException;
  	
  	public UserEntity find(Long id) throws DataAccessException;
  	
  	 public UserEntity findById(UserEntityPK userEntityPK) throws DataAccessException;
  	public List<UserEntity> findByRole(String role) throws DataAccessException;
}