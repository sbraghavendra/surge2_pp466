package com.ncsts.dao;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BillScenario;
import com.ncsts.domain.PurchaseScenario;

public interface ScenarioDAO extends GenericDAO<BillScenario, Long> {

	public Long count(BillScenario exampleInstance);
	
	public List<BillScenario> getAllRecords(BillScenario exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	
	public List<String> getScenarioNames();
	
	public int deleteByName(String name);
	
	public Long count(PurchaseScenario exampleInstance);
	
	public List<PurchaseScenario> getAllRecords(PurchaseScenario exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	
	public List<String> getPurchaseScenarioNames();
	
	public int deleteByNamePurchase(String name);
	
	public void savePurchaseScenario(PurchaseScenario instance);
	
	public List<PurchaseScenario> findByExample(PurchaseScenario exampleInstance, int count, String... excludeProperty);
	public List<PurchaseScenario> findByExampleSorted(PurchaseScenario exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty);
	
}
