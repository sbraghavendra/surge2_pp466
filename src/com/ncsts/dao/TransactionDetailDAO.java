package com.ncsts.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.services.PurchaseTransactionService;

public interface TransactionDetailDAO extends GenericDAO<PurchaseTransaction, Long> {

	public Long count(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter);
	
	public Date minGLDate(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull);
	
	public List<PurchaseTransaction> getAllRecords(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	
	public String getProcessSQLStatement(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	
	public String getProcessSQLCriteria(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	
	public String getStatisticsSQLStatement(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds);
	
	public boolean getTaxMatrixInUse(Long taxMatrixId);
	public boolean getGSBMatrixInUse(Long taxMatrixId);
	public void updateaActiveFlagMatrixInUse(Long taxMatrixId);
	public ProcessedTransactionDetail getSuspendedTaxMatrixTransactionCounts(Long taxMatrixId, Date asOfDate) throws DataAccessException;
	public void suspendTaxMatrixTransactions(PurchaseTransactionService service, Long taxMatrixId, Date asOfDate, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine);
	
	public boolean getLocationMatrixInUse(Long locationMatrixId);
	public ProcessedTransactionDetail getSuspendedLocationMatrixTransactionCounts(Long locationMatrixId) throws DataAccessException;
	public void suspendLocationMatrixTransactions(PurchaseTransactionService service, Long locationMatrixId, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine);
	
	public void suspendTransaction(PurchaseTransactionService purchaseTransactionService, Long id, SuspendReason suspendReason);
	public void updatetoHoldTransaction(PurchaseTransaction instance);
	public void acceptHeldTransaction(Long id);
	public void appendComment(Long id, String comment);
	public void appendComment(Collection<Long> ids, String comment);
	
	public List<Object> getTransactionDetailRecordsWithDynamicWhere(String whereClause) throws DataAccessException ;
	
	public List<PurchaseTransaction> getAllStatisticsRecords(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds);
	
	public Long getStatisticsCount(PurchaseTransaction exampleInstance, 
			String sqlWhere, List<String> nullFileds);
	
	public List<PurchaseTransaction> getAllGLTransactions(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds) throws DataAccessException;
	
	/**
	 * This method will save data in TB_BCP_TRANSACTIONS table.
	 * The data saved here is imported from the file
	 * @param instance
	 */
	public void saveBcpData(BCPPurchaseTransaction instance) ;
	
	public boolean isValidAdvancedFilter(String advancedFilter);
	//3987
	public void saveColumn(TransactionHeader transactionHeader);
	public List<String> getColumnHeaders(String userCode, String columnType, String windowName, String dataWindowName);
	public void removeColumns(String userCode, String columnType, String windowName, String dataWindowName);
	public void saveColumns(String userCode, String columnType, String windowName, String dataWindowName, List<TransactionHeader> transactionHeaders);
	
	public void saveSplits(PurchaseTransaction original, List<PurchaseTransaction> splits, List<Long> deletedIds,
			List<PurchaseTransaction> allocationSplits);
	public long getNextAllocSubTransId();
	public String callTransactionProcessProc();
	public String callRealTimeTransactionProcessProc(PurchaseTransaction trans, String calcFlag, String writeFlag);
	public long getTransactionCountByTaxcodeDetail(Long taxcodeDetailId) throws DataAccessException;
	public Long getTransactionCount(PurchaseTransaction exampleInstance, 
			String sqlWhere, List<String> nullFileds);
	public void updateTransaction(PurchaseTransaction updatedTransaction);
}
