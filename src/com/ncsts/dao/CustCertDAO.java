package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;

public interface CustCertDAO extends GenericDAO<CustCert,Long> {

	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<CustCert> find(Criteria criteria);
	public abstract List<CustCert> find(DetachedCriteria criteria);
	public abstract CustCert find(CustCert exampleInstance);
	public abstract List<CustCert> find(CustCert exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public Long count(CustCert exampleInstance);
	
	public void addCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException;
	public void updateCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException;
	public void deleteCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException;
	
	public void addCustCert(CustCert updateCustCert) throws DataAccessException;
	public void updateCustCert(CustCert updateCustCert) throws DataAccessException;
	public void deleteCustCert(CustCert updateCustCert) throws DataAccessException;
	
	public CustLocnEx findCustLocnEx(Long custLocnExId) throws DataAccessException;
	
	public void addCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException;
	public void updateCustCertExemp(CustCert updateCustCert, CustLocnEx updateCustLocnEx) throws DataAccessException;
	public void validateCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException;
	public boolean validateUniqueCertificate(CustCert updateCustCert) throws DataAccessException;
	public boolean validateUniqueExemption(CustCert aCustCert) throws DataAccessException;
	
//	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
//	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
//	public void deleteCustEntityList(Cust updateCust) throws DataAccessException;
//	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException;
//	public Cust getCustByNumber(String custNbr) throws DataAccessException;
	
}
