package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.BillTransaction;
import com.seconddecimal.billing.domain.SaleTransaction;

public interface SaleTransactionExDAO {
	public List<BillTransaction> findByExample(
			BillTransaction exampleInstance, String sortField,
			boolean ascending, int offset, int count);
	
	public Long count(BillTransaction exampleInstance);
	
	public void save(BillTransaction instance);
	
	public boolean deleteBatch(Long batchId);
	void delete(BillTransaction instance);

	void cleanUpRelatedTransactions(BillTransaction trans);
}
