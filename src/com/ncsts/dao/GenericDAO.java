package com.ncsts.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Idable;

public interface GenericDAO<T extends Idable<ID>,ID extends Serializable> {

	public Class<T> getObjectClass();
	public String getDBProperties();
	public Connection getDBConnection();
	
	public Long count();
	public Long count(T exampleInstance, String... excludeProperty);
	
	public T findById(ID id) throws DataAccessException;
	
	public List<T> findById(Collection<ID> ids);
	
	public List<T> findAll();
	public List<T> findAllSorted(String sortField, boolean ascending);
	
	public List<T> findByExample(T exampleInstance, int count, String... excludeProperty);
	public List<T> findByExampleSorted(T exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty);
	
	public void save(T instance);
	public void update(T instance) throws DataAccessException;
	public void remove(T instance)throws DataAccessException;

    public List<T> getPage(int startIndex, int endIndex, OrderBy orderBy);
    public List<T> getPage(int startIndex, int endIndex, OrderBy orderBy, T exampleInstance, String... excludeProperty);
}
