package com.ncsts.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;

//HibernateDaoSupport
public class HibCallGLExtractDao extends JpaDaoSupport  implements	CallGLExtractDao {
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(HibCallGLExtractDao.class);
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
   
	@SuppressWarnings("deprecation")
	public String callGlExtractProc(String where_clause, String file_name, int execution_mode, int return_code) throws DataAccessException {
	   Session session = createLocalSession();
       CallableStatement st  = null;
       int retuenCode = -1;
       String returnMessage = "";
       try {
    	   // PDB - method is deprecated, but no alternative has been provided yet
    	   st  = session.connection().prepareCall("{call SP_GL_EXTRACT(?,?,?,?)}");
    	   st.clearParameters();
    	   
    	   st.setString (1, where_clause);
    	   st.setString (2,file_name);
    	   st.setDouble (3, new Double(execution_mode));
    	   st.registerOutParameter (4, Types.DOUBLE); //RETURN NUMBER
    	   
    	   st.execute();
    	   retuenCode = new Double(st.getDouble(4)).intValue();
       } catch (HibernateException e) {
    	   e.printStackTrace();
    	   returnMessage = e.getMessage();
       } catch (SQLException e) {
    	   e.printStackTrace();
    	   returnMessage = e.getMessage();
       }
       
       finally {
 	      if (st != null) {try {st.close();}catch (SQLException e) {}}
 	      
 	      closeLocalSession(session);
 	      session=null;
 	   }
       
       if(retuenCode!=0){
    	   if(returnMessage!=null && returnMessage.length()>200){
    		   returnMessage = returnMessage.substring(0, 200);
    	   }
    	   return returnMessage;
       }
       else{
    	   return "";
       }
	}
	
	public BigDecimal getTransactionCount(String GLdate) throws DataAccessException{
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}
		
	    EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager(); 
	    String sqlQuery = "";
	    if (pco != null && pco.equals("1")){
	    	sqlQuery = "SELECT COUNT(*) FROM (SELECT DISTINCT ptl.purchtrans_id " +
	    		    	"FROM tb_purchtrans_log ptl LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
	    		    	"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
	    		    	"AND (pt.transaction_ind = 'P') " +
	    		    	"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0)) ";
	    }
	    else{
	    	sqlQuery = "SELECT COUNT(*) FROM (SELECT DISTINCT ptl.purchtrans_id " +
	    				"FROM tb_purchtrans_log ptl LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
	    				"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
	    				"AND (pt.transaction_ind = 'P') " +
	    				"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0)) ";
	    }	

    	Query q = entityManager.createNativeQuery(sqlQuery);
    	try{
    		BigDecimal id = (BigDecimal) q.getSingleResult();
    		if(id!=null)
    			return id;
    	}catch(Exception e){}
    			
    	return BigDecimal.ZERO;	
	}
	
    public BigDecimal getRecordCount(String GLdate) throws DataAccessException{
    	String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}

		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String sqlQuery = "";
	    if (pco != null && pco.equals("1")){
	    	sqlQuery = 	"SELECT COUNT(*) FROM tb_purchtrans_log ptl " +
	    				"LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
	    				"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
	    				"AND (pt.transaction_ind = 'P') " +
	    				"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
	    }
	    else{
	    	sqlQuery =  "SELECT COUNT(*) FROM tb_purchtrans_log ptl " +
	    				"LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
	    				"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
	    				"AND (pt.transaction_ind = 'P') " +
	    				"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
	    }	
	    
	    Query q = entityManager.createNativeQuery(sqlQuery);
    	try{
    		BigDecimal id = (BigDecimal) q.getSingleResult();
    		if(id!=null)
    			return id;
    	}catch(Exception e){}
    			
    	return BigDecimal.ZERO;
	}
    
    public BigDecimal getTaxAmountCount(String GLdate) throws DataAccessException{
    	String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}
		
	    EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager(); 
	    String sqlQuery = "";
	    if (pco != null && pco.equals("1")){
	    	sqlQuery =  "SELECT SUM(NVL(ptl.a_tb_calc_tax_amt,0) - NVL(ptl.b_tb_calc_tax_amt,0)) " +
	    				"FROM tb_purchtrans_log ptl " +
	    				"LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
	    				"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
	    				"AND (pt.transaction_ind = 'P') " +
	    				"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
	    }
	    else{
	    	sqlQuery =  "SELECT SUM(NVL(ptl.a_tb_calc_tax_amt,0) - NVL(ptl.b_tb_calc_tax_amt,0)) " +
	    				"FROM tb_purchtrans_log ptl " +
    					"LEFT OUTER JOIN tb_purchtrans pt ON pt.purchtrans_id = ptl.purchtrans_id " +
    					"WHERE pt.gl_date <= to_date('"+ GLdate +"','mm/dd/yyyy') " +
    					"AND (pt.transaction_ind = 'P') " +
    					"AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
	    }	

	    
    	Query q = entityManager.createNativeQuery(sqlQuery);
    	try{
    		BigDecimal id = (BigDecimal) q.getSingleResult();
    		if(id!=null)
    			return id;
    	}catch(Exception e){}
    			
    	return BigDecimal.ZERO;
	}
  
}
