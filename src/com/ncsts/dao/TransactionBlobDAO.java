package com.ncsts.dao;


import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.TransactionBlob;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;
/**
 * 
 * @author Muneer
 *
 */

public interface TransactionBlobDAO extends GenericDAO<TransactionBlob,Long> {

	public abstract TransactionBlob findByBatchId(Long batchId) throws DataAccessException;
	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<TransactionBlob> find(Criteria criteria);
	public abstract List<TransactionBlob> find(DetachedCriteria criteria);
	public abstract TransactionBlob find(TransactionBlob exampleInstance);
	public abstract List<TransactionBlob> find(TransactionBlob exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	
	public abstract List<TransactionBlob> findByTransactionBlob(TransactionBlob transactionBlob);
	public Long count(TransactionBlob exampleInstance);
//	public abstract List<TransactionBlob> getTransactionBlobByBatchTypeCode(String batchTypeCode) throws DataAccessException;
//	public abstract List<TransactionBlob> getTransactionBlobByBatchStatusCode(String batchStatusCode) throws DataAccessException;
	public abstract List<TransactionBlob> getAllTransactionBlobData() throws DataAccessException;   
	public abstract void update(TransactionBlob transactionBlob) throws DataAccessException;
	public abstract void remove(long fieldId) throws DataAccessException;
    // Need to check if TransactionBlob object have all data required for Statistics 
	// Need another method for BatchErrors
//	public List<TransactionBlob> getAllBatchesByBatchTypeCode(String batchTypeCode) throws DataAccessException; // this method added by anand
//	public String doGLExtract(String date,String fileName,String exeMode, String returnCode) throws DataAccessException; // this method added by anand

//	public abstract TransactionBlob importTaxRateUpdates(TransactionBlob transactionBlob,
//			TaxRateUploadParser parser) throws DataAccessException;
//	public abstract TransactionBlob importMapUpdates(TransactionBlob transactionBlob,
//			ImportMapUploadParser parser) throws DataAccessException;
	
	public abstract TransactionBlob saveBatch(TransactionBlob transactionBlob) throws DataAccessException; // this method added by anand
	
//  public List<Object[]> getBatchProcesses(Long batchId);
//  public List<BatchErrorLog> getBatchErrors(BatchErrorLog batch);
 // public abstract BatchErrorLog save(BatchErrorLog error) throws DataAccessException;

//  public TransactionBlob getBatchMaintananceData(long batchId) throws DataAccessException;//Method added by Krishna
	
 //   public List<TransactionBlob> getAllBatchesByBatchTypeCode(String codeTypeCode,
 //   	List<String> batchtypeCodeCode, List<String> batchstatusCodeCode, List<String> errorsevCodeCode);
 //   public BatchMetadata findBatchMetadataById(String id) throws DataAccessException;
    
//  	public BigDecimal getBatchSequence(String batchType) 	throws DataAccessException;

  	public abstract Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz);
//	public TransactionBlob getPreviousBatchInSequence(TransactionBlob batch) throws DataAccessException;
  	
  	public void saveTransactionBlob(Long id, String tempFileName);
  	
  	public void executeTransactions(TransactionBlob transactionBlob, String sqlScripts);

}
