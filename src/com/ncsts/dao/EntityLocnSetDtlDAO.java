package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.EntityLocnSetDtl;


public interface EntityLocnSetDtlDAO  extends GenericDAO<EntityLocnSetDtl,Long> {
	
    public abstract Long count(Long entityLocnSetId) throws DataAccessException;
    
    public abstract List<EntityLocnSetDtl> findAllEntityLocnSetDtls() throws DataAccessException;
    
    public List<EntityLocnSetDtl> findAllEntityLocnSetDtlsByEntityLocnSetId(Long entityLocnSetId) throws DataAccessException;
    
    public Long persist(EntityLocnSetDtl entityLocnSetDtl)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(EntityLocnSetDtl entityLocnSetDtl) throws DataAccessException;
    
    public void deleteEntityLocnSet(Long entityLocnSetId) throws DataAccessException;
      
}

