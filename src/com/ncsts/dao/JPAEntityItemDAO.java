package com.ncsts.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.EntityLocnSetDtl;
import com.ncsts.domain.FisYear;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.Registration;
import com.ncsts.domain.RegistrationDetail;

public class JPAEntityItemDAO extends JPAGenericDAO<EntityItem,Long> implements EntityItemDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

    @Transactional
	public EntityItem findById(Long entityId) throws DataAccessException {
	    return getJpaTemplate().find(EntityItem.class, entityId);
	}
	  
     @Transactional
	 public Long count(Long entityId) throws DataAccessException {
		 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		  	Query query = entityManager.createQuery(" select count(*) from EntityItem entityItem where entityItem.parentEntityId = ?");
		  	query.setParameter(1, entityId );
		    Long count = (Long) query.getSingleResult();
			return count;
		  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public EntityItem findByCode(String entityCode) throws DataAccessException {
		  EntityItem entityItem = null;
		  List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityCode = ?" ,entityCode);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  entityItem = list.get(0);
			}
		  return entityItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<Object[]> findByJurisdictionId(Long jurisdictionId) throws DataAccessException {
		  List<Object[]> entityItemList = getJpaTemplate().find(" select entityItem.entityId,entityItem.entityCode from EntityItem entityItem where entityItem.jurisdictionId = ?" ,jurisdictionId);
		  if(entityItemList != null && entityItemList.size() >0){
			  return entityItemList;
			}
		  return null;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public EntityItem findByName(String entityName) throws DataAccessException {
		  EntityItem entityItem = null;
		  List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityName = ?" ,entityName);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  entityItem = list.get(0);
			}
		  return entityItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public EntityItem findByCode(String entityCode, Long parentId) throws DataAccessException {
		  EntityItem entityItem = null;
		  List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityCode = ? and entityItem.parentEntityId = ? " ,entityCode,parentId);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  entityItem = list.get(0);
			}
		  return entityItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public EntityItem findByName(String entityName, Long parentId) throws DataAccessException {
		  EntityItem entityItem = null;
		  List<EntityItem> list = getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityName = ? and entityItem.parentEntityId = ? " ,entityName,parentId);
		  if(list != null && list.size() >0 && list.get(0) != null){
			  entityItem = list.get(0);
			}
		  return entityItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<EntityItem> findAllEntityItemsWithCustomer(Long custId) throws DataAccessException {
		  String query = "select * from TB_ENTITY inner join TB_CUST_ENTITY on  TB_CUST_ENTITY.ENTITY_ID = TB_ENTITY.ENTITY_ID " +
				  "where TB_ENTITY.ENTITY_ID <> 0 and TB_CUST_ENTITY.CUST_ID = " + custId.intValue() +
				  "order by ENTITY_CODE ";
		  
		  Session session = createLocalSession();
		  org.hibernate.SQLQuery q = session.createSQLQuery(query);
		  q.addEntity(EntityItem.class);
		
		  List<EntityItem> returnEntityItem = q.list();
		
		  closeLocalSession(session);
		  session=null;
		
		  return returnEntityItem;
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<EntityItem> findAllEntityItemsWithFilter(Long level, String entityCode, String entityName) throws DataAccessException {
		  
		  String whereClause = "";
		  
		  StringBuffer query = new StringBuffer();
		  String glue = "";
		  query.append("select * from TB_ENTITY where ENTITY_ID >= 0 ");
		  
		  if(level!=null && !level.equals(0l)){
			  whereClause = whereClause + " and ENTITY_LEVEL_ID = " + level.intValue();
		  }
		  
		  if(entityCode!=null && entityCode.length()>0){
			  whereClause = whereClause + "  and UPPER(ENTITY_CODE) like '" + entityCode.toUpperCase() + "%'";
		  }
		  
		  if(entityName!=null && entityName.length()>0){
			  whereClause = whereClause + "  and UPPER(ENTITY_NAME) like '" + entityName.toUpperCase() + "%'";
		  }
		  
		  query.append(whereClause);
		  query.append(" order by ENTITY_CODE ");
		
		  Session session = createLocalSession();
		  org.hibernate.SQLQuery q = session.createSQLQuery(query.toString());
		  q.addEntity(EntityItem.class);
		
		  List<EntityItem> returnEntityItem = q.list();
		
		  closeLocalSession(session);
		  session=null;
		
		  return returnEntityItem;
	  }
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityItem> findAllEntityItems() throws DataAccessException {
	      return getJpaTemplate().find(" select entityItem from EntityItem entityItem order by entityItem.entityCode");	  
	  }
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityItem> findAllEntityItemsBylockflag() throws DataAccessException {
	      return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.lockFlag = '0' order by entityItem.entityCode");	  
	  }
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<EntityItem> findAllEntityItemsByLevel(Long level) throws DataAccessException {
		      return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityLevelId = ? order by entityItem.entityCode", level);	  
		  }
	  
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<EntityItem> findAllEntityItemsByParent(Long parentId) throws DataAccessException{
		      return getJpaTemplate().find("select entityItem from EntityItem entityItem where entityItem.parentEntityId = ? order by entityItem.entityCode", parentId);	  
		}
	  
	    @SuppressWarnings("deprecation")
	    @Transactional
		public Long persist (EntityItem entityItem) throws DataAccessException {
	    	Session session = createLocalSession();
			Connection con = session.connection();
			if("*ALL".equalsIgnoreCase(entityItem.getEntityCode())){
				entityItem.setEntityId(0l);
				entityItem.setParentEntityId(0l);
				entityItem.setEntityLevelId(1l);
			}else{
				EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
							
				try {
					if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {	
						/*org.hibernate.Query q = session.createSQLQuery(" select sq_tb_tax_matrix_id.nextval from dual ");
						id = ((BigDecimal)q.uniqueResult()).longValue();
						System.out.println("oracle next val = " + id);*/		
						Query q = entityManager.createNativeQuery("select sq_tb_entity_id.nextval from dual");
						BigDecimal id = (BigDecimal)q.getSingleResult();
						entityItem.setEntityId(id.longValue());
					} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {	
						org.hibernate.Query q = session.createSQLQuery(" select nextval ('sq_tb_entity_id') ");	
						Object obj = q.uniqueResult();
						long id = ((BigInteger) obj).longValue();
						entityItem.setEntityId(id);					
					}
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}						
			}
			getJpaTemplate().persist(entityItem);
			getJpaTemplate().flush();
			//String key = entityItem.getEntityCode();
			Long entityId = entityItem.getEntityId();
			
			closeLocalSession(session);
			session=null;
			return entityId;
		}	
	  
	  @Transactional( propagation = Propagation.REQUIRED, readOnly = false )
		public void remove(Long id) throws DataAccessException {
		  EntityItem entityItem = getJpaTemplate().find(EntityItem.class, id);
			if( entityItem != null){
				getJpaTemplate().remove(entityItem);
			}else{
				logger.warn("EntityItem Object not found for deletion");
			}
		} 
		  
		public void saveOrUpdate (EntityItem entityItem) throws DataAccessException {	
			Session session = createLocalSession();
			Transaction t = session.beginTransaction();
			t.begin();
			session.update(entityItem);
			t.commit();
			closeLocalSession(session);
			session=null;		
		}
	
	@Transactional
	public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException {
		String entityCode = "";
		EntityItem entityItem = getJpaTemplate().find(EntityItem.class, entityId);
		if( entityItem != null){
			entityCode = entityItem.getEntityCode();
		}
		
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	 	String sql = 	" SELECT SUM(COUNT) FROM ((SELECT COUNT(*) AS COUNT FROM TB_PURCHTRANS WHERE ENTITY_CODE = ?1) " + 
	 					"UNION ALL (SELECT COUNT(*) AS COUNT FROM TB_LOCATION_MATRIX WHERE ENTITY_ID = ?2) " + 
	 					"UNION ALL (SELECT COUNT(*) AS COUNT FROM TB_ENTITY WHERE PARENT_ENTITY_ID = ?3)) ";
	 	
	  	Query query = entityManager.createNativeQuery(sql);
	  	query.setParameter(1,entityCode );
	  	query.setParameter(2,entityId );
	  	query.setParameter(3,entityId );
	    Long count  = ((BigDecimal)query.getSingleResult()).longValue();
	    
	    if(count.intValue()>0){
	    	return false;
	    }
	    else{
	    	return true;
	    }
	}
	
	@SuppressWarnings({ "null" })
	public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException {

	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
	        tx.begin();
	       	if(selectedComponents.substring(0, 1).equals("1"))
			{
			  if(toEntityId<0){
				  
			  String hqldeletDefault = "DELETE FROM tb_entity_default WHERE entity_id = " + fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeletDefault);
			  query.executeUpdate();
			  String hqldeleteQueryDtl="DELETE FROM tb_entity_locn_set_dtl WHERE entity_locn_set_id IN (SELECT entity_locn_set_id FROM tb_entity_locn_set WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_entity_locn_set WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{
				  String hqldeleteDefault = "DELETE FROM tb_entity_default WHERE entity_id = " + toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteDefault);
				  query.executeUpdate();
				  query = session.createQuery ("FROM EntityDefault e WHERE e.entityId = :columnName");
				  query.setParameter("columnName", fromEntityId);
				
					List<EntityDefault> list= query.list();
                 // ScrollableResults itemCursor =query.scroll();
					for (int i = 0; i < list.size(); i++) {
					 EntityDefault entityDefaultOld =(EntityDefault)list.get(i);
					 EntityDefault entityDefaultNew=new EntityDefault();
					 entityDefaultNew.setEntityId(toEntityId);
					 entityDefaultNew.setDefaultCode(entityDefaultOld.getDefaultCode());
					 entityDefaultNew.setDefaultValue(entityDefaultOld.getDefaultValue());
					// entityDefaultNew.setEntityDefaultId(entityDefaultId)(entityDefaultOld.getDefaultValue());
                      session.saveOrUpdate(entityDefaultNew);
                      }
                  
                 String hqldeleteLoc=" DELETE FROM tb_entity_locn_set_dtl WHERE entity_locn_set_id IN (" +
                         "  SELECT entity_locn_set_id FROM tb_entity_locn_set WHERE entity_id = "+ toEntityId.intValue() +")";
                 query=session.createSQLQuery(hqldeleteLoc);
                 query.executeUpdate();
                 String hqldelLocSet= " DELETE FROM tb_entity_locn_set WHERE entity_id ="+  toEntityId.intValue();
                 query=session.createSQLQuery(hqldelLocSet);
                 query.executeUpdate();//SELECT entity_locn_set_id, locn_set_code, name, active_flag FROM tb_entity_locn_set WHERE entity_id = an_from_entity_id)
                 query = session.createQuery ("FROM EntityLocnSet e WHERE e.entityId  = :columnName");
				 query.setParameter("columnName", fromEntityId);
                 List<EntityLocnSet> list1= query.list();
					for (int i = 0; i < list1.size(); i++) {
						EntityLocnSet entityDefaultOld = (EntityLocnSet)list1.get(i);
						EntityLocnSet entityDefaultNew = new EntityLocnSet();
						entityDefaultNew.setEntityId(toEntityId);
						entityDefaultNew.setEntityLocnSetId(entityDefaultOld.getEntityLocnSetId());
						entityDefaultNew.setLocnSetCode(entityDefaultOld.getLocnSetCode());
						entityDefaultNew.setName(entityDefaultOld.getName());
						entityDefaultNew.setActiveFlag(entityDefaultOld.getActiveFlag());
	                    session.saveOrUpdate(entityDefaultNew);
	                    query = session.createQuery ("FROM EntityLocnSetDtl e WHERE e.entityLocnSetId = :columnName");
	  				    query.setParameter("columnName",entityDefaultOld.getEntityLocnSetId());
	  				     List<EntityLocnSetDtl> list2= query.list();
	  				     for (int j = 0; j < list2.size(); j++) {
	  				    	EntityLocnSetDtl entityLoncSetOld =(EntityLocnSetDtl)list2.get(j);
	  				    	EntityLocnSetDtl entityLoncSetNew=new EntityLocnSetDtl();
	  				    	//description, sf_entity_id, poo_entity_id, poa_entity_id, effective_date
	  				    	entityLoncSetNew.setDescription(entityLoncSetOld.getDescription());
	  				    	entityLoncSetNew.setSfEntityId(entityLoncSetOld.getSfEntityId());
	  				    	entityLoncSetNew.setPooEntityId(entityLoncSetOld.getPooEntityId());
	  				    	entityLoncSetNew.setPoaEntityId(entityLoncSetOld.getPoaEntityId());
	  				    	entityLoncSetNew.setEffectiveDate(entityLoncSetOld.getEffectiveDate());
	  				    	session.saveOrUpdate(entityLoncSetNew);
	  				        }
	                      }
                        }
			          }
			//-  Position 2 = Nexus
			if(selectedComponents.substring(1, 2).equals("1"))
			{
			  if(toEntityId<0){ //-- If To Entity is negative, then delete From Entity
			 
			  String hqldeleteQueryDtl="DELETE FROM tb_nexus_def_detail WHERE nexus_def_id IN (SELECT nexus_def_id FROM tb_nexus_def WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_nexus_def WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{// -- Else, perform copies
			         //-- Nexus defs & details
				  String hqldeleteDef="DELETE FROM tb_nexus_def_detail WHERE nexus_def_id IN (SELECT nexus_def_id FROM tb_nexus_def WHERE entity_id = "+toEntityId.intValue()+")";
				  query = session.createSQLQuery(hqldeleteDef);
				  query.executeUpdate();
				  String hqldeleteQueryLocn="DELETE FROM tb_nexus_def WHERE entity_id = "+toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteQueryLocn);
				  query.executeUpdate();
				  //////Master Table
				  query = session.createQuery ("FROM NexusDefinition n WHERE n.entityId  = :columnName");
				  query.setParameter("columnName", fromEntityId);
                  List<NexusDefinition> nexList= query.list();
				  for (int j = 0; j < nexList.size(); j++) {
                   	  NexusDefinition nexOld = (NexusDefinition) nexList.get(j); 
                	  NexusDefinition nexNew=new NexusDefinition();
                	  nexNew.setEntityId(toEntityId);
                	  nexNew.setNexusCountryCode(nexOld.getNexusCountryCode());
                	  nexNew.setNexusStateCode(nexOld.getNexusStateCode());
                	  nexNew.setNexusCounty(nexOld.getNexusCounty());
                	  nexNew.setNexusCity(nexOld.getNexusCity());
                	  nexNew.setNexusStj(nexOld.getNexusStj());
                	  nexNew.setNexusFlag(nexOld.getNexusFlag());
                   	  session.saveOrUpdate(nexNew);
                      query = session.createQuery ("FROM NexusDefinitionDetail e WHERE e.nexusDefId  = :columnName");
  				      query.setParameter("columnName", nexOld.getNexusDefId());
  				      List<NexusDefinitionDetail> nexDetail= query.list();
  				      for (int i = 0; i< nexDetail.size(); i++) {
  				    	NexusDefinitionDetail ndold =(NexusDefinitionDetail) nexDetail.get(i);
  				    	NexusDefinitionDetail ndnew= new  NexusDefinitionDetail();
  				    	ndnew.setNexusDefId(nexNew.getNexusDefId());
  				    	ndnew.setEffectiveDate(ndold.getEffectiveDate());
  				    	ndnew.setExpirationDate(ndold.getExpirationDate());
  				    	ndnew.setNexusTypeCode(ndold.getNexusTypeCode());
  				    	ndnew.setActiveFlag(ndold.getActiveFlag());
  				    	session.saveOrUpdate(ndnew);
  				       } 
                      }
                     }
			        }
			    //  --  Position 3 = Registrations
			 if(selectedComponents.substring(2, 3).equals("1"))
			  {
			  if(toEntityId<0){ //-- If To Entity is negative, then delete From Entity
			 
			  String hqldeleteQueryDtl="DELETE FROM tb_registration_detail WHERE registration_id IN (SELECT registration_id FROM tb_registration WHERE entity_id = "+fromEntityId.intValue()+")";
			  query = session.createSQLQuery(hqldeleteQueryDtl);
			  query.executeUpdate();
			  String hqldeleteQueryLocn="DELETE FROM tb_registration WHERE entity_id = "+fromEntityId.intValue();
			  query = session.createSQLQuery(hqldeleteQueryLocn);
			  query.executeUpdate();
			  }else{// -- Else, perform copies
			         //-- Nexus defs & details
				  String hqldeleteDef="DELETE FROM tb_registration_detail WHERE registration_id IN (SELECT registration_id FROM tb_registration WHERE entity_id = "+toEntityId.intValue()+")";
				  query = session.createSQLQuery(hqldeleteDef);
				  query.executeUpdate();
				  String hqldeleteQueryLocn="DELETE FROM tb_registration WHERE entity_id = "+toEntityId.intValue();
				  query = session.createSQLQuery(hqldeleteQueryLocn);
				  query.executeUpdate();
				  //////Master Table
				  query = session.createQuery ("FROM Registration n WHERE n.entityId  = :columnName");
				  query.setParameter("columnName", fromEntityId);
                  List<Registration> reglist = query.list();
				    for (int i = 0; i< reglist.size(); i++) {
                  	  Registration regOld = (Registration) reglist.get(i); 
                	  Registration regNew=new Registration();
                	  regNew.setEntityId(toEntityId);
                   	  regNew.setRegCountryCode(regOld.getRegCountryCode());
                	  regNew.setRegStateCode(regOld.getRegStateCode());
                	  regNew.setRegCounty(regOld.getRegCounty());
                	  regNew.setRegCity(regOld.getRegCity());
                	  regNew.setRegStj(regOld.getRegStj());
                      session.saveOrUpdate(regNew);
                      query = session.createQuery ("FROM RegistrationDetail e WHERE e.registrationId  = :columnName1");
  				      query.setParameter("columnName1", regOld.getRegistrationId());
  				      List<RegistrationDetail> regDetaillist = query.list();
  					   for(int j = 0; j < regDetaillist.size(); j++) {
  				    	RegistrationDetail rdetailold =(RegistrationDetail) regDetaillist.get(j);
  				    	RegistrationDetail rdetailnew = new RegistrationDetail();
  				    	rdetailnew.setRegistrationId(regNew.getRegistrationId());
  				    	rdetailnew.setEffectiveDate(rdetailold.getEffectiveDate());
  				    	rdetailnew.setExpirationDate(rdetailold.getExpirationDate());
  				    	rdetailnew.setRegTypeCode(rdetailold.getRegTypeCode());
  				    	rdetailnew.setRegNo(rdetailold.getRegNo());
  				    	rdetailnew.setActiveFlag(rdetailold.getActiveFlag());
  				    	session.saveOrUpdate(rdetailnew);;
  				    	
  				       } 
                      }
                     }
			        }
			
			if(toEntityId<0){
				//delete entity
				String hqlDelete = "DELETE TB_ENTITY WHERE ENTITY_ID = " + fromEntityId.intValue();
				query = session.createSQLQuery(hqlDelete);
				query.executeUpdate();
			}
			 
			tx.commit();
			
			//cstmnt = session.connection().prepareCall("{call sp_entity_components_copy(?,?,?)}");
			//cstmnt.setLong(1, fromEntityId.longValue());
			//cstmnt.setLong(2, toEntityId.longValue());
			//cstmnt.setString(3, selectedComponents);
			//cstmnt.execute();
		} catch (Exception e) {
			logger.error("Failed to execute setSubEntityInactive: " + e);
			e.printStackTrace();
			
			
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	
	public void setSubEntityInactive(Long entityId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlUpdate = "UPDATE  TB_ENTITY SET ACTIVE_FLAG='0' WHERE PARENT_ENTITY_ID = " + entityId.intValue();
			query = session.createSQLQuery(hqlUpdate);
			query.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute setSubEntityInactive: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		List<EntityItem> list = findAllEntityItemsByParent(entityId);
		if(list != null && list.size() >0){
			EntityItem aEntityItem = null;
			for(int i=0; i< list.size(); i++){
				aEntityItem = (EntityItem)list.get(i);
				setSubEntityInactive(aEntityItem.getEntityId());
			}
		}
	}
	
	@Transactional
	public void deleteEntity(Long entityId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			//remove(TB_ENTITY);
			String hqlDelete = "DELETE TB_ENTITY WHERE ENTITY_ID = " + entityId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Delete all existing TB_NEXUS_DEF
			hqlDelete = "DELETE FROM TB_NEXUS_DEF WHERE ENTITY_ID = " + entityId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute deleteTEntity: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional(readOnly = false)
	public Long count(EntityItem exampleInstance, String whereClause) {
		
		String extendWhere = generateWhereClause(exampleInstance);
	
		Long returnLong = 0L;
		try {
			String sqlQuery = "select count(*) count FROM TB_ENTITY WHERE :extendWhere " ;
			sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClause + " and " + extendWhere);
			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return returnLong;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<EntityItem> getAllRecords(EntityItem exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults, String whereClause) {
		
		String sqlQuery = sqlEntity;

		String strOrderBy = getOrderByToken(orderBy);
		String extendWhere = generateWhereClause(exampleInstance);

		sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
		sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClause + " and " + extendWhere);
		sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
		sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));
		
		List<EntityItem> returnList=null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			returnList = parseCustCertReturnList(iter);
		}
		return returnList;
		
	}
	
	private Map<String,String> columnMapping = null;
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			
			columnMapping.put("entityLevelId".toLowerCase(), "entity_level_id"); 
			columnMapping.put("entityId".toLowerCase(), "entity_id");			
			columnMapping.put("description".toLowerCase(), "description");
			columnMapping.put("fein".toLowerCase(), "fein");
			columnMapping.put("jurisdictionId".toLowerCase(), "jurisdiction_id");
			columnMapping.put("geocode".toLowerCase(), "geocode");
			columnMapping.put("addressLine1".toLowerCase(), "address_line_1");
			columnMapping.put("addressLine2".toLowerCase(), "address_line_2");
			columnMapping.put("city".toLowerCase(), "city");
			columnMapping.put("county".toLowerCase(), "county");
			columnMapping.put("state".toLowerCase(), "state");
			columnMapping.put("zip".toLowerCase(), "zip");
			columnMapping.put("zipPlus4".toLowerCase(), "zipplus4");
			columnMapping.put("country".toLowerCase(), "country");
			columnMapping.put("exemptGraceDays".toLowerCase(), "exempt_grace_days");
			columnMapping.put("lockFlag".toLowerCase(), "lock_flag");
			columnMapping.put("activeFlag".toLowerCase(), "active_flag");	
			
			//Transient fields
			columnMapping.put("compCode".toLowerCase(), "comp_code");
			columnMapping.put("divnCode".toLowerCase(), "divn_code");
			columnMapping.put("locnCode".toLowerCase(), "locn_code");	
		}
		
		String columnName = columnMapping.get(name.toLowerCase());
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
        }
		
		//Default order
		if(orderByToken.length()==0){
			orderByToken.append(" comp_code, divn_code, locn_code ");
		}

		return orderByToken.toString();
	}
	
	private List<EntityItem> parseCustCertReturnList(Iterator<Object> iter){
		List<EntityItem> returnList = new ArrayList<EntityItem>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			EntityItem entityItem=new EntityItem();
			
			//RowNumber = (BigDecimal)objarray[0];
			
			BigDecimal is = (BigDecimal)objarray[1];
            if (is!=null) entityItem.setEntityLevelId(is.longValue());
            
            BigDecimal entityId = (BigDecimal)objarray[2];
            if (entityId!=null) entityItem.setEntityId(entityId.longValue());
            
            String compCode = (String) objarray[3];
            entityItem.setCompCode(compCode.trim());
            
            String compName = (String) objarray[4];
            entityItem.setCompName(compName.trim());
            
            String divnCode = (String) objarray[5];
            entityItem.setDivnCode(divnCode.trim());
            
            String divnName = (String) objarray[6];
            entityItem.setDivnName(divnName.trim());
            
            String locnCode = (String) objarray[7];
            entityItem.setLocnCode(locnCode.trim());
            
            String locnName = (String) objarray[8];
            entityItem.setLocnName(locnName.trim());
            
            String description = (String) objarray[9];
            entityItem.setDescription(description);
            
            String fein = (String) objarray[10];
            entityItem.setFein(fein);
            
            BigDecimal jurisdictionId = (BigDecimal)objarray[11];
            if (jurisdictionId!=null) entityItem.setJurisdictionId(jurisdictionId.longValue());
            
            String geocode = (String) objarray[12];
            entityItem.setGeocode(geocode);
            
            String addressLine1 = (String) objarray[13];
            entityItem.setAddressLine1(addressLine1);
            
            String addressLine2 = (String) objarray[14];
            entityItem.setAddressLine2(addressLine2);
            
            String city = (String) objarray[15];
            entityItem.setCity(city);
            
            String county = (String) objarray[16];
            entityItem.setCounty(county);
            
            String state = (String) objarray[17];
            entityItem.setState(state);
            
            String zip = (String) objarray[18];
            entityItem.setZip(zip);
            
            String zipplus4 = (String) objarray[19];
            entityItem.setZipPlus4(zipplus4);
            
            String country = (String) objarray[20];
            entityItem.setCountry(country);
            
            BigDecimal exemptGraceDays = (BigDecimal)objarray[21];
            if (exemptGraceDays!=null) entityItem.setExemptGraceDays(exemptGraceDays.longValue());
            
            Character lockFlag = (Character) objarray[22];
            if (lockFlag != null) entityItem.setLockFlag(lockFlag.toString());
            
            Character activeFlag = (Character) objarray[23];
            if (activeFlag != null) entityItem.setActiveFlag(activeFlag.toString());
            
            String entityCode = (String) objarray[24];
            entityItem.setEntityCode(entityCode);
            
            String entityName = (String) objarray[25];
            entityItem.setEntityName(entityName);
            
            BigDecimal parentEntityId = (BigDecimal)objarray[26];
            if (parentEntityId!=null) entityItem.setParentEntityId(parentEntityId.longValue());
            
            String updateUserId = (String) objarray[27];
            entityItem.setUpdateUserId(updateUserId);
            
            java.sql.Timestamp updateTimestamp = (java.sql.Timestamp) objarray[28];
            if (updateTimestamp!=null) entityItem.setUpdateTimestamp(new java.util.Date(updateTimestamp.getTime()));

            returnList.add(entityItem);                 
		}
		
		return returnList;
	}
	
	private Criteria generateCriteriaBatch(EntityItem exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(EntityItem.class);
		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		 Map<String, String > criteriaMap = new LinkedHashMap<String, String >();
		 criteriaMap.put("addressLine1", exampleInstance.getAddressLine1());
		 criteriaMap.put("addressLine2", exampleInstance.getAddressLine2());
		 
		 criteriaMap.put("geocode", exampleInstance.getGeocode());
		 criteriaMap.put("city", exampleInstance.getCity());
		 criteriaMap.put("county", exampleInstance.getCounty());
		 criteriaMap.put("state", exampleInstance.getState());
		 criteriaMap.put("country", exampleInstance.getCountry());
		 criteriaMap.put("zip", exampleInstance.getZip());
		 criteriaMap.put("zipPlus4", exampleInstance.getZipPlus4());
		 
		 for (String entityKey : criteriaMap.keySet()) {
			 field = entityKey;
		 	 value = (String)criteriaMap.get(entityKey);
		 	
		 	 if (value != null && value.trim().length()>0) {
		 		 startWild = value.startsWith("%");
		 		 endWild = value.endsWith("%");
		 		 if(startWild && endWild){ //Like %12345% or %123%45%
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else if(startWild && !endWild){ //Like $34567 or $34&567
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
		 			 criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		 			 //example.excludeProperty(field);
		 		 }
		 		 else{
		 			 criteria.add(Restrictions.eq(field, value).ignoreCase()); 
		 		 }
		 	 }
    	}

        return criteria;
	}
	
	private String generateWhereClause(EntityItem exampleInstance) {
		String whereClause = " 1=1 ";

		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		 Map<String, String > criteriaMap = new LinkedHashMap<String, String >();
		 criteriaMap.put("ADDRESS_LINE_1", exampleInstance.getAddressLine1());
		 criteriaMap.put("ADDRESS_LINE_2", exampleInstance.getAddressLine2());
		 
		 criteriaMap.put("GEOCODE", exampleInstance.getGeocode());
		 criteriaMap.put("CITY", exampleInstance.getCity());
		 criteriaMap.put("COUNTY", exampleInstance.getCounty());
		 criteriaMap.put("STATE", exampleInstance.getState());
		 criteriaMap.put("COUNTRY", exampleInstance.getCountry());
		 criteriaMap.put("ZIP", exampleInstance.getZip());
		 criteriaMap.put("ZIPPLUS4", exampleInstance.getZipPlus4());
		 
		 for (String entityKey : criteriaMap.keySet()) {
			 field = entityKey;
		 	 value = (String)criteriaMap.get(entityKey);
		 	
		 	 if (value != null && value.trim().length()>0) {
		 		 startWild = value.startsWith("%");
		 		 endWild = value.endsWith("%");
		 		if(startWild || endWild){ //Like %12345 or 12345%	 	
		 			whereClause += " and ( upper("+field+") like '"+ value.toUpperCase() + "')";
		 		 }
		 		
		 		 else{
		 			whereClause += " and ( upper("+field+") = '"+ value.toUpperCase() + "')";
		 			
		 		 }
		 	 }
    	}

        return whereClause;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityItem> findAllEntityItemsByLevelAndParent(Long level, Long parentId) throws DataAccessException {
		if(parentId!=null){
			return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityLevelId = ? and entityItem.parentEntityId = ? order by entityItem.entityCode", level, parentId);	  
		}
		else{
			return getJpaTemplate().find(" select entityItem from EntityItem entityItem where entityItem.entityLevelId = ? order by entityItem.entityCode", level);
		}
	}
	
	private String sqlEntity = 
			"select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " +
	
			"entity_level_id, entity_id, comp_code, comp_name,divn_code, divn_name,locn_code, locn_name, " +
			"description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			
			"FROM ( " +
			"SELECT * FROM  " +
			"( " +
			"SELECT entity_level_id, entity_id, " +
			"       entity_code AS comp_code, entity_name AS comp_name, " +
			"       ' ' AS divn_code, ' ' AS divn_name, " +
			"       ' ' AS locn_code, ' ' as locn_name, " +
			"       description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity " +
			"WHERE entity_level_id=0 " +
			"UNION ALL " +
			"SELECT entity_level_id, entity_id, " +
			"      entity_code AS comp_code, entity_name AS comp_name, " +
			"       ' ' AS divn_code, ' ' AS divn_name, " +
			"       ' ' AS locn_code, ' ' as locn_name, " +
			"       description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity " +
			"WHERE entity_level_id=1 " +
			
			"UNION ALL " +
			"SELECT enty.entity_level_id, entity_id, " +
			"       (SELECT entity_code FROM tb_entity WHERE entity_id=enty.parent_entity_id) comp_code, " +
			"       (SELECT entity_name FROM tb_entity WHERE entity_id=enty.parent_entity_id) comp_name, " +
			"       enty.entity_code AS divn_code, enty.entity_name AS divn_name, " +
			"       ' ' AS locn_code, ' ' as locn_name, " +
			"       description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity enty " +
			"WHERE enty.entity_level_id=2 " +
/*			
			"UNION ALL " +
			"SELECT enty.entity_level_id, entity_id, " +
			"       (SELECT entity_code FROM tb_entity WHERE entity_id=(SELECT parent_entity_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)) comp_code, " +
			"       (SELECT entity_name FROM tb_entity WHERE entity_id=(SELECT parent_entity_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)) comp_name, " +
			"       (SELECT entity_code FROM tb_entity WHERE entity_id=enty.parent_entity_id) divn_code, " +
			"       (SELECT entity_name FROM tb_entity WHERE entity_id=enty.parent_entity_id) divn_name, " +
			"       enty.entity_code AS locn_code, enty.entity_name as locn_name, " +
			"       description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity enty " +
			"WHERE enty.entity_level_id=3 " +
*/

			"UNION ALL " +
			"SELECT enty.entity_level_id, entity_id, " +
			"		(SELECT entity_code FROM tb_entity WHERE entity_id=enty.parent_entity_id) comp_code, " +
			"		(SELECT entity_name FROM tb_entity WHERE entity_id=enty.parent_entity_id) comp_name, " +
			"		' ' AS divn_code, ' ' AS divn_name, " +
			"		enty.entity_code AS locn_code, enty.entity_name as locn_name, " +
			"		description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity enty " +
			"WHERE enty.entity_level_id=3 AND (SELECT entity_level_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)=1 " +
			
			"UNION ALL " +
			"SELECT enty.entity_level_id, entity_id, " +
			"		(SELECT entity_code FROM tb_entity WHERE entity_id=(SELECT parent_entity_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)) comp_code, " +
			"		(SELECT entity_name FROM tb_entity WHERE entity_id=(SELECT parent_entity_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)) comp_name, " +
			"		(SELECT entity_code FROM tb_entity WHERE entity_id=enty.parent_entity_id) divn_code, " +
			"		(SELECT entity_name FROM tb_entity WHERE entity_id=enty.parent_entity_id) divn_name, " +
			"		enty.entity_code AS locn_code, enty.entity_name as locn_name, " +
			"		description, fein, jurisdiction_id, geocode, address_line_1, address_line_2, city, county, state, zip, zipplus4, country, exempt_grace_days, lock_flag, active_flag, " +
			"       entity_code,  entity_name, parent_entity_id, update_user_id, update_timestamp " +
			"FROM tb_entity enty " +
			"WHERE enty.entity_level_id=3 AND (SELECT entity_level_id FROM tb_entity WHERE entity_id=enty.parent_entity_id)=2 " +


			")  " +
			") " +

			"WHERE " +
			":extendWhere) WHERE RowNumber BETWEEN :minrow AND :maxrow ";
}


