package com.ncsts.dao;

import java.sql.Connection;

import com.ncsts.domain.TempTransactionDetail;

public interface TransactionUpdate {
	public void processTransaction(Connection con,
			TempTransactionDetail tempTrans) throws Exception;
}
