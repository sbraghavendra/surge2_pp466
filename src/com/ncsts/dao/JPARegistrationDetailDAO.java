package com.ncsts.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.RegistrationDetail;

public class JPARegistrationDetailDAO extends JPAGenericDAO<RegistrationDetail, Long> implements RegistrationDetailDAO {
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<RegistrationDetail> findAllRegistrationDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFag) 
		throws DataAccessException {
		
		StringBuffer sql =  new StringBuffer("Select ndd from RegistrationDetail ndd where ndd.registrationId in " +
				"(select registrationId from Registration where regCountryCode = ?1 and regStateCode = ?2 and " +
				"regCounty = ?3 and regCity = ?4 and regStj = ?5 and entityId = ?6)");
		
		List<RegistrationDetail> list = null;
		list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId);

		return list;
	}
}
