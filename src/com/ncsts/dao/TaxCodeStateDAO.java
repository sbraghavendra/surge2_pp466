package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.TaxCodeState;
import com.ncsts.domain.TaxCodeStatePK;
/**
 * 
 * @author Anand
 *
 */
public interface TaxCodeStateDAO {
	public List<TaxCodeState> getActiveTaxCodeState() throws DataAccessException;
	public List<TaxCodeState> findByTaxCode(String taxCodeType, String taxcodeCode) throws DataAccessException;
	public List<TaxCodeState> findByStateName(String stateName);
	public List<TaxCodeState> findByStateCode(String stateCode);
	
	public List<TaxCodeState> getAllTaxCodeState(); 
	public TaxCodeState findByPK(TaxCodeStatePK pk);
	
	public TaxCodeState add(TaxCodeState instance) throws DataAccessException;
    public void update(TaxCodeState instance) throws DataAccessException;
    public void delete(TaxCodeStatePK pk) throws DataAccessException;
    public void batchQuickUpdate(List<TaxCodeState> quickUpdateList) throws DataAccessException;
    public List<TaxCodeState> findByExampleSorted(String country, String state, String activeFlag)
    	throws DataAccessException;
    
	public boolean isCountryUsed(String countryName);
	public void deleteCountry(TaxCodeState instance);
	public void updateCountry(TaxCodeState instance);
	public List<TaxCodeState> findByCountryName(String countryName);

}
