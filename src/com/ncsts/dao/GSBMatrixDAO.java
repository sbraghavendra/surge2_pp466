package com.ncsts.dao;
import java.util.Date;
import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.GSBMatrix;


public interface GSBMatrixDAO extends MatrixDAO<GSBMatrix> {

	public List<GSBMatrix> getAllRecords(GSBMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag,String activeFlag, Long taxMatrixId,
			OrderBy orderBy, int firstRow, int maxResults,String moduleCode);

	public Long count(GSBMatrix exampleInstance, 
			String taxcodeState, String taxcodeType, String taxcodeCode, 
			String cchGroup, String cchTaxCat,
			String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag,String activeFlag, Long taxMatrixId,String moduleCode);
	
	public List<GSBMatrix> getDetailRecords(GSBMatrix taxMatrix, OrderBy orderBy,String modulecode);
}

