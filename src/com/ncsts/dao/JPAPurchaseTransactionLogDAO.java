package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.PurchaseTransactionLog;

public class JPAPurchaseTransactionLogDAO extends JPAGenericDAO<PurchaseTransactionLog, Long> implements PurchaseTransactionLogDAO {

	String sqlColumn = "ptl.PURCHTRANS_LOG_ID, ptl.PURCHTRANS_ID, ptl.LOG_SOURCE, ptl.GL_EXTRACT_BATCH_NO, ptl.GL_EXTRACT_TIMESTAMP," +
	 		"ptl.DIRECT_PAY_PERMIT_FLAG, ptl.CALCULATE_IND, ptl.GL_DATE, ptl.GL_LINE_ITM_DIST_AMT, ptl.ALLOCATION_MATRIX_ID, ptl.ALLOCATION_SUBTRANS_ID,"+
			"ptl.TAX_ALLOC_MATRIX_ID,ptl.SPLIT_SUBTRANS_ID, ptl.MULTI_TRANS_CODE, ptl.TAXCODE_CODE, ptl.MANUAL_TAXCODE_IND, ptl.TAX_MATRIX_ID, ptl.COMMENTS,"+
	 		"ptl.PROCESSING_NOTES, ptl.TRANSACTION_STATE_CODE, ptl.AUTO_TRANSACTION_STATE_CODE, ptl.TRANSACTION_COUNTRY_CODE, ptl.AUTO_TRANSACTION_COUNTRY_CODE," +
			"ptl.TRANSACTION_IND, ptl.SUSPEND_IND, ptl.REJECT_FLAG, ptl.COUNTRY_TIER1_TAXABLE_AMT, ptl.COUNTRY_TIER2_TAXABLE_AMT, ptl.COUNTRY_TIER3_TAXABLE_AMT," +
	 		"ptl.STATE_TIER1_TAXABLE_AMT, ptl.STATE_TIER2_TAXABLE_AMT, ptl.STATE_TIER3_TAXABLE_AMT, ptl.COUNTY_TIER1_TAXABLE_AMT, ptl.COUNTY_TIER2_TAXABLE_AMT," +
			"ptl.COUNTY_TIER3_TAXABLE_AMT, ptl.CITY_TIER1_TAXABLE_AMT, ptl.CITY_TIER2_TAXABLE_AMT, ptl.CITY_TIER3_TAXABLE_AMT, ptl.STJ1_TAXABLE_AMT, ptl.STJ2_TAXABLE_AMT," +
	 		"ptl.STJ3_TAXABLE_AMT, ptl.STJ4_TAXABLE_AMT, ptl.STJ5_TAXABLE_AMT, ptl.STJ6_TAXABLE_AMT, ptl.STJ7_TAXABLE_AMT, ptl.STJ8_TAXABLE_AMT, ptl.STJ9_TAXABLE_AMT," +
			"ptl.STJ10_TAXABLE_AMT, ptl.B_COUNTRY_TIER2_TAX_AMT, ptl.B_COUNTRY_TIER3_TAX_AMT, ptl.B_STATE_TIER1_TAX_AMT, ptl.B_STATE_TIER2_TAX_AMT, ptl.B_STATE_TIER3_TAX_AMT," +
	 		"ptl.B_COUNTY_TIER1_TAX_AMT, ptl.B_COUNTY_TIER2_TAX_AMT, ptl.B_COUNTY_TIER3_TAX_AMT, ptl.B_CITY_TIER1_TAX_AMT, ptl.B_CITY_TIER2_TAX_AMT, ptl.B_CITY_TIER3_TAX_AMT,"+
			"ptl.B_STJ1_TAX_AMT, ptl.B_STJ2_TAX_AMT, ptl.B_STJ3_TAX_AMT, ptl.B_STJ4_TAX_AMT, ptl.B_STJ5_TAX_AMT, ptl.B_STJ6_TAX_AMT, ptl.B_STJ7_TAX_AMT,"+
	 		"ptl.B_STJ8_TAX_AMT, ptl.B_STJ9_TAX_AMT, ptl.B_STJ10_TAX_AMT, ptl.B_COUNTY_LOCAL_TAX_AMT, ptl.B_CITY_LOCAL_TAX_AMT, ptl.B_TB_CALC_TAX_AMT, ptl.A_COUNTRY_TIER1_TAX_AMT,"+
			"ptl.A_COUNTRY_TIER2_TAX_AMT, ptl.A_COUNTRY_TIER3_TAX_AMT, ptl.A_STATE_TIER1_TAX_AMT, ptl.A_STATE_TIER2_TAX_AMT, ptl.A_STATE_TIER3_TAX_AMT,"+
	 		"ptl.A_COUNTY_TIER1_TAX_AMT, ptl.A_COUNTY_TIER2_TAX_AMT, ptl.A_COUNTY_TIER3_TAX_AMT, ptl.A_CITY_TIER1_TAX_AMT, ptl.A_CITY_TIER2_TAX_AMT, ptl.A_CITY_TIER3_TAX_AMT,"+
			"ptl.A_STJ1_TAX_AMT, ptl.A_STJ2_TAX_AMT, ptl.A_STJ3_TAX_AMT, ptl.A_STJ4_TAX_AMT, ptl.A_STJ5_TAX_AMT, ptl.A_STJ6_TAX_AMT, ptl.A_STJ7_TAX_AMT,"+
	 		"ptl.A_STJ8_TAX_AMT, ptl.A_STJ9_TAX_AMT, ptl.A_STJ10_TAX_AMT, ptl.A_COUNTY_LOCAL_TAX_AMT, ptl.A_CITY_LOCAL_TAX_AMT, ptl.A_TB_CALC_TAX_AMT,"+
			"ptl.DISTR_01_AMT, ptl.DISTR_02_AMT, ptl.DISTR_03_AMT, ptl.DISTR_04_AMT, ptl.DISTR_05_AMT, ptl.DISTR_06_AMT, ptl.DISTR_07_AMT, ptl.DISTR_08_AMT,"+
	 		"ptl.DISTR_09_AMT, ptl.DISTR_10_AMT, ptl.COUNTRY_SITUS_MATRIX_ID, ptl.STATE_SITUS_MATRIX_ID, ptl.COUNTY_SITUS_MATRIX_ID, ptl.CITY_SITUS_MATRIX_ID,"+
			"ptl.STJ1_SITUS_MATRIX_ID, ptl.STJ2_SITUS_MATRIX_ID, ptl.STJ3_SITUS_MATRIX_ID, ptl.STJ4_SITUS_MATRIX_ID, ptl.STJ5_SITUS_MATRIX_ID, ptl.SHIPTO_LOCN_MATRIX_ID,"+
	 		"ptl.SHIPFROM_LOCN_MATRIX_ID, ptl.ORDRACPT_LOCN_MATRIX_ID, ptl.ORDRORGN_LOCN_MATRIX_ID, ptl.FIRSTUSE_LOCN_MATRIX_ID, ptl.BILLTO_LOCN_MATRIX_ID,"+
			"ptl.TTLXFR_LOCN_MATRIX_ID, ptl.SHIPTO_MANUAL_JUR_IND, ptl.COUNTRY_CODE, ptl.STATE_CODE, ptl.COUNTY_NAME, ptl.CITY_NAME, ptl.STJ1_NAME, ptl.STJ2_NAME,"+
	 		"ptl.STJ3_NAME, ptl.STJ4_NAME, ptl.STJ5_NAME, ptl.STJ6_NAME, ptl.STJ7_NAME, ptl.STJ8_NAME, ptl.STJ9_NAME, ptl.STJ10_NAME, ptl.COUNTRY_VENDOR_NEXUS_DTL_ID,"+
			"ptl.STATE_VENDOR_NEXUS_DTL_ID, ptl.COUNTY_VENDOR_NEXUS_DTL_ID, ptl.CITY_VENDOR_NEXUS_DTL_ID, ptl.COUNTRY_TAXCODE_DETAIL_ID, ptl.STATE_TAXCODE_DETAIL_ID,"+
	 		"ptl.COUNTY_TAXCODE_DETAIL_ID, ptl.CITY_TAXCODE_DETAIL_ID, ptl.STJ1_TAXCODE_DETAIL_ID, ptl.STJ2_TAXCODE_DETAIL_ID, ptl.STJ3_TAXCODE_DETAIL_ID,"+
			"ptl.STJ4_TAXCODE_DETAIL_ID, ptl.STJ5_TAXCODE_DETAIL_ID, ptl.STJ6_TAXCODE_DETAIL_ID, ptl.STJ7_TAXCODE_DETAIL_ID, ptl.STJ8_TAXCODE_DETAIL_ID, ptl.STJ9_TAXCODE_DETAIL_ID,"+
	 		"ptl.STJ10_TAXCODE_DETAIL_ID, ptl.COUNTRY_TAXTYPE_CODE, ptl.STATE_TAXTYPE_CODE, ptl.COUNTY_TAXTYPE_CODE, ptl.CITY_TAXTYPE_CODE, ptl.STJ1_TAXTYPE_CODE,"+
			"ptl.STJ2_TAXTYPE_CODE, ptl.STJ3_TAXTYPE_CODE, ptl.STJ4_TAXTYPE_CODE, ptl.STJ5_TAXTYPE_CODE, ptl.STJ6_TAXTYPE_CODE, ptl.STJ7_TAXTYPE_CODE, ptl.STJ8_TAXTYPE_CODE,"+
	 		"ptl.STJ9_TAXTYPE_CODE, ptl.STJ10_TAXTYPE_CODE, ptl.COUNTRY_TAXRATE_ID, ptl.STATE_TAXRATE_ID, ptl.COUNTY_TAXRATE_ID, ptl.CITY_TAXRATE_ID, ptl.STJ1_TAXRATE_ID,"+
			"ptl.STJ2_TAXRATE_ID, ptl.STJ3_TAXRATE_ID, ptl.STJ4_TAXRATE_ID, ptl.STJ5_TAXRATE_ID, ptl.STJ6_TAXRATE_ID, ptl.STJ7_TAXRATE_ID, ptl.STJ8_TAXRATE_ID,"+
	 		"ptl.STJ9_TAXRATE_ID, ptl.STJ10_TAXRATE_ID, ptl.COUNTRY_TAXCODE_OVER_FLAG, ptl.STATE_TAXCODE_OVER_FLAG, ptl.COUNTY_TAXCODE_OVER_FLAG, ptl.CITY_TAXCODE_OVER_FLAG,"+
			"ptl.STJ1_TAXCODE_OVER_FLAG, ptl.STJ2_TAXCODE_OVER_FLAG, ptl.STJ3_TAXCODE_OVER_FLAG, ptl.STJ4_TAXCODE_OVER_FLAG, ptl.STJ5_TAXCODE_OVER_FLAG, ptl.STJ6_TAXCODE_OVER_FLAG,"+
	 		"ptl.STJ7_TAXCODE_OVER_FLAG, ptl.STJ8_TAXCODE_OVER_FLAG, ptl.STJ9_TAXCODE_OVER_FLAG, ptl.STJ10_TAXCODE_OVER_FLAG, ptl.SHIPTO_JURISDICTION_ID,"+
			"ptl.SHIPFROM_JURISDICTION_ID, ptl.ORDRACPT_JURISDICTION_ID, ptl.ORDRORGN_JURISDICTION_ID, ptl.FIRSTUSE_JURISDICTION_ID, ptl.BILLTO_JURISDICTION_ID,"+
	 		"ptl.TTLXFR_JURISDICTION_ID, ptl.COUNTRY_TIER1_RATE, ptl.COUNTRY_TIER2_RATE, ptl.COUNTRY_TIER3_RATE, ptl.COUNTRY_TIER1_SETAMT, ptl.COUNTRY_TIER2_SETAMT,"+
			"ptl.COUNTRY_TIER3_SETAMT, ptl.COUNTRY_TIER1_MAX_AMT, ptl.COUNTRY_TIER2_MIN_AMT, ptl.COUNTRY_TIER2_MAX_AMT, ptl.COUNTRY_TIER2_ENTAM_FLAG, ptl.COUNTRY_TIER3_ENTAM_FLAG,"+
	 		"ptl.COUNTRY_MAXTAX_AMT, ptl.COUNTRY_TAXABLE_THRESHOLD_AMT, ptl.COUNTRY_MINIMUM_TAXABLE_AMT, ptl.COUNTRY_MAXIMUM_TAXABLE_AMT, ptl.COUNTRY_MAXIMUM_TAX_AMT,"+
			"ptl.COUNTRY_BASE_CHANGE_PCT, ptl.COUNTRY_SPECIAL_RATE, ptl.COUNTRY_SPECIAL_SETAMT, ptl.COUNTRY_TAX_PROCESS_TYPE_CODE, ptl.STATE_TIER1_RATE,"+
	 		"ptl.STATE_TIER2_RATE, ptl.STATE_TIER3_RATE, ptl.STATE_TIER1_SETAMT, ptl.STATE_TIER2_SETAMT, ptl.STATE_TIER3_SETAMT, ptl.STATE_TIER1_MAX_AMT,"+
			"ptl.STATE_TIER2_MIN_AMT, ptl.STATE_TIER2_MAX_AMT, ptl.STATE_TIER2_ENTAM_FLAG, ptl.STATE_TIER3_ENTAM_FLAG, ptl.STATE_MAXTAX_AMT, ptl.STATE_TAXABLE_THRESHOLD_AMT,"+
	 		"ptl.STATE_MINIMUM_TAXABLE_AMT, ptl.STATE_MAXIMUM_TAXABLE_AMT, ptl.STATE_MAXIMUM_TAX_AMT, ptl.STATE_BASE_CHANGE_PCT, ptl.STATE_SPECIAL_RATE, ptl.STATE_SPECIAL_SETAMT,"+
			"ptl.STATE_TAX_PROCESS_TYPE_CODE, ptl.COUNTY_TIER1_RATE, ptl.COUNTY_TIER2_RATE, ptl.COUNTY_TIER3_RATE, ptl.COUNTY_TIER1_SETAMT, ptl.COUNTY_TIER2_SETAMT,"+
	 		"ptl.COUNTY_TIER3_SETAMT, ptl.COUNTY_TIER1_MAX_AMT, ptl.COUNTY_TIER2_MIN_AMT, ptl.COUNTY_TIER2_MAX_AMT, ptl.COUNTY_TIER2_ENTAM_FLAG, ptl.COUNTY_TIER3_ENTAM_FLAG,"+
			"ptl.COUNTY_MAXTAX_AMT, ptl.COUNTY_TAXABLE_THRESHOLD_AMT, ptl.COUNTY_MINIMUM_TAXABLE_AMT, ptl.COUNTY_MAXIMUM_TAXABLE_AMT, ptl.COUNTY_MAXIMUM_TAX_AMT,"+
	 		"ptl.COUNTY_BASE_CHANGE_PCT, ptl.COUNTY_SPECIAL_RATE, ptl.COUNTY_SPECIAL_SETAMT, ptl.COUNTY_TAX_PROCESS_TYPE_CODE, ptl.CITY_TIER1_RATE, ptl.CITY_TIER2_RATE, "+
			"ptl.CITY_TIER3_RATE, ptl.CITY_TIER1_SETAMT, ptl.CITY_TIER2_SETAMT, ptl.CITY_TIER3_SETAMT, ptl.CITY_TIER1_MAX_AMT, ptl.CITY_TIER2_MIN_AMT, ptl.CITY_TIER2_MAX_AMT,"+
	 		"ptl.CITY_TIER2_ENTAM_FLAG, ptl.CITY_TIER3_ENTAM_FLAG, ptl.CITY_MAXTAX_AMT, ptl.CITY_TAXABLE_THRESHOLD_AMT, ptl.CITY_MINIMUM_TAXABLE_AMT, ptl.CITY_MAXIMUM_TAXABLE_AMT,"+
			"ptl.CITY_MAXIMUM_TAX_AMT, ptl.CITY_BASE_CHANGE_PCT, ptl.CITY_SPECIAL_RATE, ptl.CITY_SPECIAL_SETAMT, ptl.CITY_TAX_PROCESS_TYPE_CODE, ptl.STJ1_RATE, ptl.STJ1_SETAMT,"+
	 		"ptl.STJ2_RATE, ptl.STJ2_SETAMT, ptl.STJ3_RATE, ptl.STJ3_SETAMT, ptl.STJ4_RATE, ptl.STJ4_SETAMT, ptl.STJ5_RATE, ptl.STJ5_SETAMT, ptl.STJ6_RATE,"+
			"ptl.STJ6_SETAMT, ptl.STJ7_RATE, ptl.STJ7_SETAMT, ptl.STJ8_RATE, ptl.STJ8_SETAMT, ptl.STJ9_RATE, ptl.STJ9_SETAMT, ptl.STJ10_RATE, ptl.STJ10_SETAMT, ptl.COMBINED_RATE,"+
	 		"ptl.COUNTRY_TAXTYPE_USED_CODE, ptl.STATE_TAXTYPE_USED_CODE, ptl.COUNTY_TAXTYPE_USED_CODE, ptl.CITY_TAXTYPE_USED_CODE, ptl.STJ1_TAXTYPE_USED_CODE, "+
			"ptl.STJ2_TAXTYPE_USED_CODE, ptl.STJ3_TAXTYPE_USED_CODE, ptl.STJ4_TAXTYPE_USED_CODE, ptl.STJ5_TAXTYPE_USED_CODE, ptl.STJ6_TAXTYPE_USED_CODE,"+
	 		"ptl.STJ7_TAXTYPE_USED_CODE, ptl.STJ8_TAXTYPE_USED_CODE, ptl.STJ9_TAXTYPE_USED_CODE, ptl.STJ10_TAXTYPE_USED_CODE, ptl.MODIFY_USER_ID, ptl.MODIFY_TIMESTAMP" ;
	
	
	@SuppressWarnings("unchecked")
	public List<PurchaseTransactionLog> getAllGLTransactions(PurchaseTransactionLog exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds) throws DataAccessException {
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}
		
		String strOrderBy = null;
		String deltataxAmtOrder = null;
		FieldSortOrder fieldDeltaTaxAmt = null;
		if (orderBy != null){
            for (FieldSortOrder field : orderBy.getFields()){
            	if("TB_DELTA_TAX_AMT".equals(field.getName())) {
            		if(field.getAscending()) {
            			deltataxAmtOrder = "ASC";
            		}
            		else {
            			deltataxAmtOrder = "DESC";
            		}
            		orderBy = null;
            		fieldDeltaTaxAmt = field;
            	}
           	}
        }
		strOrderBy = getOrderByToken(orderBy);
		if (strOrderBy.length()== 0) {
			strOrderBy = "ptl.PURCHTRANS_ID ASC, ptl.UPDATE_TIMESTAMP DESC ";
		}
		String sqlQuery = 	"Select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " + "NVL(a_tb_calc_tax_amt,0)-NVL(b_tb_calc_tax_amt,0) AS tb_delta_tax_amt," + sqlColumn +
				" FROM tb_purchtrans_log ptl " ;
							
		if (pco != null && pco.equals("1")){
			//-- WITH PCO		
			sqlQuery = sqlQuery + " LEFT OUTER JOIN tb_purchtrans td on td.PURCHTRANS_ID = ptl.PURCHTRANS_ID ";		
			sqlQuery = sqlQuery + " WHERE td.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy') AND (td.transaction_ind = 'P') AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
		
		}
		else{	
			//-- NO PCO
			sqlQuery = sqlQuery + " LEFT OUTER JOIN tb_purchtrans td ON td.PURCHTRANS_ID = ptl.PURCHTRANS_ID ";
			sqlQuery = sqlQuery + " WHERE td.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy') AND (td.transaction_ind = 'P') AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ";
		}
		sqlQuery = sqlQuery + "  ) WHERE RowNumber BETWEEN :minrow AND :maxrow ";
		
		sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
		sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
		sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));
		if(fieldDeltaTaxAmt != null && "TB_DELTA_TAX_AMT".equals(fieldDeltaTaxAmt.getName())) {
			sqlQuery = sqlQuery + " order by TB_DELTA_TAX_AMT " + deltataxAmtOrder;
		}
		List<PurchaseTransactionLog> returnList=null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			returnList = parseTransactionDetailLogList(iter);
		}
		return returnList;	
	}


	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public Long getTransactionCount(PurchaseTransactionLog exampleInstance, 
			String sqlWhere, List<String> nullFileds) {
		
		String qry = "SELECT o.value FROM Option o WHERE o.codePK.optionCode = 'PCO' AND o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ";  
		String pco = "";
		List<String> alist = getJpaTemplate().find(qry);
		if ((alist != null) && (alist.size() > 0)) {
			pco = (String)alist.get(0);
		}

		StringBuilder sql = new StringBuilder();
		Long returnLong = 0L;
		if (pco != null && pco.equals("1")){
			//-- WITH PCO
			sql.append("Select count(*) as count FROM tb_purchtrans_log ptl ");		
			sql.append(" LEFT OUTER JOIN tb_purchtrans td on td.PURCHTRANS_ID = ptl.PURCHTRANS_ID ");		
			sql.append(" WHERE td.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy') AND (td.transaction_ind = 'P') AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ");
		}
		else{
			//-- NO PCO		
			sql.append("SELECT count(*) AS count FROM tb_purchtrans_log ptl ");
			sql.append(" LEFT OUTER JOIN tb_purchtrans td ON td.PURCHTRANS_ID = ptl.PURCHTRANS_ID ");
			sql.append(" WHERE td.gl_date <= to_date('"+ sqlWhere +"','mm/dd/yyyy') AND (td.transaction_ind = 'P') AND (ptl.gl_extract_batch_no IS NULL OR ptl.gl_extract_batch_no = 0) ");
		}	
		
		Query q = entityManager.createNativeQuery(sql.toString());
		try{
			returnLong = ((BigDecimal) q.getSingleResult()).longValue();
    	}catch(Exception e){}
		
        return returnLong;
	}
	
	@SuppressWarnings("unchecked")	
	public List<PurchaseTransactionLog> findPurchaseTransactionLogList(
				Long purchtransId) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria =  session.createCriteria(PurchaseTransactionLog.class);
		criteria.add(Restrictions.eq("purchtransId", new Long(purchtransId)));
		criteria.addOrder(Order.asc("updateTimestamp"));
		List<PurchaseTransactionLog> purchtrasList= criteria.list();
		closeLocalSession(session);
		session=null;
		
		fillLogListDescriptions(purchtrasList);
        return purchtrasList;
	}
	
	@Transactional
	public ListCodes findByPK(ListCodesPK lcpk) throws DataAccessException {
		return getJpaTemplate().find(ListCodes.class, lcpk);
	}
	
	private List<PurchaseTransactionLog> fillLogListDescriptions(List<PurchaseTransactionLog> purchTransLogList) {
		Session session = createLocalSession();
		for(final ListIterator<PurchaseTransactionLog> i = purchTransLogList.listIterator(); i.hasNext();) {
			final PurchaseTransactionLog purchTransLog = i.next();
			ListCodes suspendCode = null; 
			Criteria criteria = session.createCriteria(ListCodes.class);
			
			ListCodes transIndListCode = findByPK(new ListCodesPK("TRANSIND", purchTransLog.getTransactionInd()));
			
			criteria = session.createCriteria(ListCodes.class);
			criteria.add(Restrictions.eq("listCodesPK.codeTypeCode", "LOGSOURCE"));
			criteria.add(Restrictions.eq("message", purchTransLog.getLogSource()).ignoreCase());
			ListCodes logSourceDescCode = (ListCodes) criteria.uniqueResult();
			
			if(purchTransLog.getSuspendInd() != null) {
				suspendCode = findByPK(new ListCodesPK("SUSPENDIND", purchTransLog.getSuspendInd()));
			}
			
			if(transIndListCode != null)
				purchTransLog.setTransIndDesc(transIndListCode.getDescription());
			else
				purchTransLog.setTransIndDesc(purchTransLog.getTransactionInd());
			
			if(logSourceDescCode != null)
				purchTransLog.setLogSourceDesc(logSourceDescCode.getDescription());
			else
				purchTransLog.setLogSourceDesc(purchTransLog.getLogSource());
			
			if(purchTransLog.getSuspendInd() != null) {
				if(suspendCode != null)
					purchTransLog.setSuspendDesc(suspendCode.getDescription());
				else
					purchTransLog.setSuspendDesc(purchTransLog.getSuspendInd());
			}
		}
		
		return purchTransLogList;
	}
	 
	@Transactional(readOnly=true)
	public Long count(long purchtransId) {
		Session session = createLocalSession();
		Criteria criteria =  session.createCriteria(PurchaseTransactionLog.class);
		criteria.add(Restrictions.eq("purchtransId", new Long(purchtransId)));

		criteria.setProjection(Projections.rowCount()); 
		List test= criteria.list();
		Long ret = ((Number)criteria.list().get(0)).longValue();
		
		closeLocalSession(session);
		session=null;

        return ret;
	}
	
	
	private List<PurchaseTransactionLog> parseTransactionDetailLogList(Iterator<Object> iter){
		List<PurchaseTransactionLog> returnList = new ArrayList<PurchaseTransactionLog>();
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			PurchaseTransactionLog detailLog = new PurchaseTransactionLog();
			
			BigDecimal rownumber = (BigDecimal)objarray[0];
	        if (rownumber!=null) detailLog.setRownumber(rownumber.longValue());
	        
	        BigDecimal purchtransId = (BigDecimal) objarray[3];
	        if(purchtransId!=null) detailLog.setPurchtransId(purchtransId.longValue());
	        
	        String logsource =   (String)objarray[4];
	        if(logsource != null) detailLog.setLogSource(logsource);
	        
	        BigDecimal bTbCalcTaxAmt = (BigDecimal) objarray[73];
	        if(bTbCalcTaxAmt != null) detailLog.setbTbCalcTaxAmt(bTbCalcTaxAmt);
	        
	        BigDecimal aTbCalcTaxAmt = (BigDecimal) objarray[98];
	        if(aTbCalcTaxAmt != null) detailLog.setaTbCalcTaxAmt(aTbCalcTaxAmt);
	        
	        BigDecimal deltaTaxAmt = (BigDecimal) objarray[1];
	        if(deltaTaxAmt != null) detailLog.setDeltaTaxAmt(deltaTaxAmt);
	        
	        returnList.add(detailLog);                
		}
		 return returnList; 
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				if (field.getAscending()){
					orderByToken.append("ptl." + field.getName().toLowerCase() + " ASC");
				} 
				else {
					orderByToken.append("ptl." + field.getName().toLowerCase() + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
		
}
