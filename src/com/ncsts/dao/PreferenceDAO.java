package com.ncsts.dao;

import java.util.List;

import com.ncsts.domain.ListCodes;
import com.ncsts.domain.OptionCodePK;

public interface PreferenceDAO {

	public enum AdminPreference {
		ADMINDEFSCHEDTIME,
		ADMINSTATUSRECALCSECS,
		ADMINUSERCHGRECALCSECSFLAG,
		ADMINUSERCHGTIMEFLAG,
		ALLOCATIONSENABLED,
		TAXALLOCENABLED,
		ASP,
		DEBUGSEC,
		EXTENDSEC,
		GLEXTRACTENABLED,
		GLEXTRACTMARK,
		GLEXTRACTWIN,
		STSCORPDST,
		STSCORPTIMEZONE,
		TAXESTENABLED,
		TPEXTRACTENABLED,
		VERSION,
		PCO
	}
	
	public enum SystemPreference {
		AUTOPROCFLAG,
		BATCHAUTOCHECKFLAG,
		BATCHCOUNTMARKER,
		BATCHDEFSCHEDTIME,
		BATCHEOFMARKER,
		BATCHUSERCHGTIMEFLAG,
		COMPANYNAME,
		DEFAULTDIRCRYSTAL,
		DEFCCHGROUPCODE,
		DEFCCHTAXCATCODE,
		DEFUSERDST,
		DELETE0AMTFLAG,
		DRIVERREFVAL,
		ERRORROWS,
		ESTDEFTAXCODE,
		ESTDEFTAXTYPE,
		FILENAMEDUPRES,
		FILESIZEDUPRES,
		FULLTHRESHOLD,
		FULLTHRESHOLDPAGES,
		GLEXTRACTEXT,
		GLEXTRACTPREFIX,
		HOLDTRANSFLAG,
		IMPEXPFILEEXT,
		IMPEXPFILEPATH,
		IMPORTDATEMAP,
		KILLPROCFLAG,
		LASTTAXRATEDATEFULL,
		LASTTAXRATEDATEUPDATE,
		LASTTAXRATERELFULL,
		LASTTAXRATERELUPDATE,
		LIMITEDTHRESHOLD,
		LIMITEDTHRESHOLDPAGES,
		MINIMUMTAXDRIVERS,
		MINIMUMLOCATIONDRIVERS,
		MINIMUMALLOCATIONDRIVERS,
		MINIMUMEXTRACTDRIVERS,
		PWUSERCHG,
		REFDOCFILEPATH,
		SQLFILEEXT,
		SQLFILEPATH,
		SYSTEMDST,
		SYSTEMSTATUSRECALCSECS,
		TAXRATEUPDATEPATH,
		DONOTIMPORT,
		REPLACEWITH,
		MAPROWS
	}
	
	public enum UserPreference {
		DEFTAXCODETYPE,
		DEFTRANSIND,
		DISPLAYPOPUPS,
		PWLOCKFLAG,
		SAVEGRIDCHANGES,
		SOUNDS,
		SPLASH,
		USERDIRCRYSTAL,
		USERDST
	}
	
	public String getAdminPreference(AdminPreference preference, String defValue);
	public String getSystemPreference(SystemPreference preference, String defValue);
	public String getUserPreference(UserPreference preference, String defValue);
	
//	public CompanyDTO getCompany();
	public List<ListCodes> getTimeZoneList();
//	public BatchPreferenceDTO getBatchPreference();
//	public boolean getTaxPartnerClient();
	public List<ListCodes> getDefTransIndList();
	public List<ListCodes> getDefTaxCodeList();
	public List<ListCodes> getDefTaxTypeList();
	public List<ListCodes> getDefRateTypeList();
//	public MatrixDTO getMatrix();
//	public void modifyCompany(CompanyDTO cd);
	
}
