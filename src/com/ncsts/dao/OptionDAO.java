package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

/**
 * @author Paul Govindan
 *
 */

public interface OptionDAO {
	
    public abstract Option findByPK(OptionCodePK codePK) throws DataAccessException;	
    
    public abstract List<Option> findAllOptions() throws DataAccessException;
    
	public void remove(OptionCodePK codePK) throws DataAccessException;    
    
	public void saveOrUpdate (Option option) throws DataAccessException;     
    
	public String persist (Option option) throws DataAccessException; 
	
	public List<Option> getSystemPreferences() throws DataAccessException; 	
	
	public List<Option> getAdminPreferences() throws DataAccessException; 	
	
	public List<Option> getUserPreferences() throws DataAccessException; 
}
