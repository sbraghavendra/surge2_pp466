package com.ncsts.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.VendorNexus;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.view.bean.JurisdictionItem;

public class JPAVendorNexusDAO extends JPAGenericDAO<VendorNexus, Long> implements
		VendorNexusDAO{

	@Override
	protected void ryanExampleHandling(Criteria criteria, Example example,
			VendorNexus exampleInstance) {
		//overriding default ryanExampleHandling so that fields with value "*ALL" are not excluded from findByExample query
	}
	
	@Transactional
	public VendorNexus findOneByExample(VendorNexus exampleInstance) throws DataAccessException{
		VendorNexus vendorNexus = null;
		List<VendorNexus> list = new ArrayList<VendorNexus>();
		try {
			Session localSession = this.createLocalSession();
			Criteria criteria = localSession.createCriteria(VendorNexus.class);
			
			criteria.add(Restrictions.eq("vendorId",exampleInstance.getVendorId()));
			criteria.add(Restrictions.eq("nexusCountryCode",exampleInstance.getNexusCountryCode()));
			criteria.add(Restrictions.eq("nexusStateCode",exampleInstance.getNexusStateCode()));
			criteria.add(Restrictions.eq("nexusCounty",exampleInstance.getNexusCounty()));
			criteria.add(Restrictions.eq("nexusCity",exampleInstance.getNexusCity()));
			criteria.add(Restrictions.eq("nexusStj",exampleInstance.getNexusStj()));

        	list = criteria.list();
        	
        	if(list!=null && list.size()>0){
        		vendorNexus = (VendorNexus)list.get(0);
        	}
        
        	this.closeLocalSession(localSession);
        	localSession=null;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		
		return vendorNexus;
	}
	
	@Transactional
	public boolean removeVendorNexus(List<VendorNexus> vendorNexasArray) throws DataAccessException{
		boolean retBoolean = false;
		
		Session session = createLocalSession();
		Connection conn = null;
		PreparedStatement prep = null;

		String sql = "UPDATE TB_VENDOR_NEXUS_DTL SET ACTIVE_FLAG = '0' WHERE VENDOR_NEXUS_ID = ? ";
		
		try {
			conn = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			prep = conn.prepareStatement(sql); 

			List<VendorNexus> list = null;
			VendorNexus foundVendorNexus = null;
			VendorNexus workingVendorNexus = null;

			Criteria criteria = null;
			
			int count = 1;
			
			for (int i = 0; i < vendorNexasArray.size(); i++) {
				
				if (prep == null) {
					prep = conn.prepareStatement(sql);
				}
				
				workingVendorNexus = (VendorNexus) vendorNexasArray.get(i);
				criteria = session.createCriteria(VendorNexus.class);	
				criteria.add(Restrictions.eq("vendorId",workingVendorNexus.getVendorId()));
				criteria.add(Restrictions.eq("nexusCountryCode",workingVendorNexus.getNexusCountryCode()));
				criteria.add(Restrictions.eq("nexusStateCode",workingVendorNexus.getNexusStateCode()));
				criteria.add(Restrictions.eq("nexusCounty",workingVendorNexus.getNexusCounty()));
				criteria.add(Restrictions.eq("nexusCity",workingVendorNexus.getNexusCity()));
				criteria.add(Restrictions.eq("nexusStj",workingVendorNexus.getNexusStj()));

	        	list = criteria.list();
	        	
	        	foundVendorNexus = null;
	        	if(list!=null && list.size()>0){
	        		foundVendorNexus = (VendorNexus)list.get(0);
	        	}  
	        		        	
	        	if(foundVendorNexus==null){
	        		continue;
	        	}
	        	
	        	//PP-23 Don't need to actually delete any detail, keep original TB_VENDOR_NEXUS.NEXUS_FLAG 
	        	/*
	        	else if (!"0".equals(foundVendorNexus.getNexusFlag())) {
	        		//Update VendorNexus
    				foundVendorNexus.setNexusFlag("0");
    				
    				session.update(foundVendorNexus);		
    				session.flush();
    			}
				*/
				prep.setLong(1, foundVendorNexus.getVendorNexusId());  	
    			prep.addBatch(); 
    			
    			if ((count++ % 50) == 0) {
					prep.executeBatch();
					conn.commit();
					prep.clearBatch();
				}
			}
			
			if (prep != null) {
				prep.executeBatch();
				conn.commit();
				prep.clearBatch();

				try {
					prep.close();
				} catch (Exception ex) {
				}
			}
			retBoolean = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try {
					conn.close();
				} catch (SQLException ignore) {}
			}
			
			closeLocalSession(session);
			session=null;
		}
	
		return retBoolean;
	}
	
	private String independentCountySql =
	"SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.county_nexusind_code " +
	"      FROM tb_jurisdiction " +
	"      WHERE tb_jurisdiction.county_nexusind_code='I' " +
	"      AND :geocode " +
	"      AND :country " +
	"      AND :state " +
	"      AND :county " +
	"      AND :city " +
	"      AND :stj " +
	"      AND :zip " +
	"      AND :effectivedate " +
	"      AND :expirationdate ";
	
	@Transactional
	public boolean isIndependentCounty(Long entityId, String geocode, String country, String state, String county, String city, String zip, String stj) throws DataAccessException {
		boolean retBoolean = false;
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = independentCountySql;
		
		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		String geocodeWhere = "";
		String countryWhere = "";
		String zipWhere = "";
		String stateWhere = "";
		String countyWhere = "";
		String cityWhere = "";
		String stj1Where = "";
		String stj2Where = "";
		String stj3Where = "";
		String stj4Where = "";
		String stj5Where = "";
		String stjWhere = "";
		String effectivedateWhere = "tb_jurisdiction.effective_date <= to_date('" + now + "', 'mm/dd/yyyy')";
		String expirationdateWhere = "tb_jurisdiction.expiration_date >= to_date('" + now + "', 'mm/dd/yyyy')";
		
		//geocode
		if(geocode!=null && geocode.length()>0){
			startWild = geocode.startsWith("%");
			endWild = geocode.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				geocodeWhere = "tb_jurisdiction.geocode like '"+ geocode.toUpperCase() + "' ";
			}
			else{
				geocodeWhere = "tb_jurisdiction.geocode = '"+ geocode.toUpperCase() + "' ";
			}
		}
		else{
			geocodeWhere = "1=1 ";
		}
		
		//country
		if(country!=null && country.length()>0){
			countryWhere = "tb_jurisdiction.country = '"+ country.toUpperCase() + "' ";		
		}
		else{
			countryWhere = "1=1 ";
		}
		
		//zip
		if(zip!=null && zip.length()>0){
			zipWhere = "tb_jurisdiction.zip = '"+ zip.toUpperCase() + "' ";		
		}
		else{
			zipWhere = "1=1 ";
		}
		
		//state
		if(state!=null && state.length()>0){
			stateWhere = "tb_jurisdiction.state = '"+ state.toUpperCase() + "' ";		
		}
		else{
			stateWhere = "1=1 ";
		}
		
		//county
		if(county!=null && county.length()>0){
			startWild = county.startsWith("%");
			endWild = county.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				countyWhere = "tb_jurisdiction.county like '"+ county.toUpperCase() + "' ";
			}
			else{
				countyWhere = "tb_jurisdiction.county = '"+ county.toUpperCase() + "' ";
			}
		}
		else{
			countyWhere = "1=1 ";
		}
		
		//city
		if(city!=null && city.length()>0){
			startWild = city.startsWith("%");
			endWild = city.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				cityWhere = "tb_jurisdiction.city like '"+ city.toUpperCase() + "' ";
			}
			else{
				cityWhere = "tb_jurisdiction.city = '"+ city.toUpperCase() + "' ";
			}
		}
		else{
			cityWhere = "1=1 ";
		}
		
		//stj
		if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "tb_jurisdiction.stj1_name like '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name like '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name like '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name like '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "tb_jurisdiction.stj1_name = '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name = '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name = '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name = '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name = '"+ stj.toUpperCase() + "' ";
			}
		}
		else{
			stj1Where = "1=1 ";
			stj2Where = "1=1 ";
			stj3Where = "1=1 ";
			stj4Where = "1=1 ";
			stj5Where = "1=1 ";
		}
		
		stjWhere = "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		
		
		sql = sql.replaceAll(":selectedcountry", "'" + country + "'");
		sql = sql.replaceAll(":selectedstate", "'" + state + "'");
		
		
		sql = sql.replaceAll(":entityId", entityId.toString());
		sql = sql.replaceAll(":geocode", geocodeWhere);
		sql = sql.replaceAll(":country", countryWhere);
		sql = sql.replaceAll(":state", stateWhere);
		sql = sql.replaceAll(":county", countyWhere);
		sql = sql.replaceAll(":city", cityWhere);
		sql = sql.replaceAll(":stj", stjWhere);
		sql = sql.replaceAll(":zip", zipWhere);	
		sql = sql.replaceAll(":effectivedate", effectivedateWhere);
		sql = sql.replaceAll(":expirationdate", expirationdateWhere);
		
		
		List<Object[]> list = null;

		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);

		list = q.getResultList();
		
		if(list!=null && list.size()>0){
			retBoolean = true;
		}

		return retBoolean;
	}
	
	@Transactional
	public boolean addVendorNexus(List<VendorNexus> vendorNexasArray, List<VendorNexusDtl> vendorNexasDetailArray) throws DataAccessException{
		boolean retBoolean = false;
		
		Session session = createLocalSession();
		Connection conn = null;
		PreparedStatement prep = null;
		String sqlseq = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "sq_tb_vendor_nexus_dtl_id");
		
		String sql = "INSERT INTO TB_VENDOR_NEXUS_DTL (VENDOR_NEXUS_DTL_ID, VENDOR_NEXUS_ID, EFFECTIVE_DATE, EXPIRATION_DATE, NEXUS_TYPE_CODE, ACTIVE_FLAG) values (?, ?, ?, ?, ?, ?)"; 

		try {
			Query q = entityManager.createNativeQuery(sqlseq);
			
			conn = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			prep = conn.prepareStatement(sql); 
			
			List<VendorNexus> list = null;
			VendorNexus foundVendorNexus = null;
			VendorNexus workingVendorNexus = null;
			VendorNexusDtl workingVendorNexusDetail = null;
			
			Criteria criteria = null;
			
			int count = 1;
			
			for (int i = 0; i < vendorNexasArray.size(); i++) {
				
				if (prep == null) {
					prep = conn.prepareStatement(sql);
				}
				
				//Both should have the same order.
				workingVendorNexus = (VendorNexus) vendorNexasArray.get(i);
				workingVendorNexusDetail = (VendorNexusDtl) vendorNexasDetailArray.get(i);
				
				criteria = session.createCriteria(VendorNexus.class);	
				criteria.add(Restrictions.eq("vendorId",workingVendorNexus.getVendorId()));
				criteria.add(Restrictions.eq("nexusCountryCode",workingVendorNexus.getNexusCountryCode()));
				criteria.add(Restrictions.eq("nexusStateCode",workingVendorNexus.getNexusStateCode()));
				criteria.add(Restrictions.eq("nexusCounty",workingVendorNexus.getNexusCounty()));
				criteria.add(Restrictions.eq("nexusCity",workingVendorNexus.getNexusCity()));
				criteria.add(Restrictions.eq("nexusStj",workingVendorNexus.getNexusStj()));
	        	list = criteria.list();
	        	
	        	foundVendorNexus = null;
	        	if(list!=null && list.size()>0){
	        		foundVendorNexus = (VendorNexus)list.get(0);
	        	}        	
	        	
    			//Update VendorNexus
	        	Long vendorNexusId = null;
	        	if(foundVendorNexus==null){
	    			workingVendorNexus.setNexusFlag("1");
	    	
	    			session.save(workingVendorNexus);
	    			session.flush();
	    			
	    			vendorNexusId = workingVendorNexus.getVendorNexusId();
	
	    		} else {
	    			if (!"1".equals(foundVendorNexus.getNexusFlag())) {
	    				foundVendorNexus.setNexusFlag("1");
	    				
	    				session.update(foundVendorNexus);		
	    				session.flush();
	    			}
	    			
	    			vendorNexusId = foundVendorNexus.getVendorNexusId();
	    		}		
	        	
    			//Save new VendorNexusDtl
    			BigDecimal id = (BigDecimal)q.getSingleResult();
				prep.setLong(1, id.longValue());  	
    			prep.setLong(2, vendorNexusId);     
    			prep.setDate(3, new java.sql.Date(workingVendorNexusDetail.getEffectiveDate().getTime()));     
    			prep.setDate(4, new java.sql.Date(workingVendorNexusDetail.getExpirationDate().getTime()));  	
    			prep.setString(5, workingVendorNexusDetail.getNexusTypeCode());  
    			prep.setString(6, (workingVendorNexusDetail.getActiveBooleanFlag()) ? "1" : "0");  

    			prep.addBatch(); 
    			
    			if ((count++ % 50) == 0) {
					prep.executeBatch();
					conn.commit();
					prep.clearBatch();
				}
			}
			
			if (prep != null) {
				prep.executeBatch();
				conn.commit();
				prep.clearBatch();

				try {
					prep.close();
				} catch (Exception ex) {
				}
			}
			retBoolean = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try {
					conn.close();
				} catch (SQLException ignore) {}
			}
			
			closeLocalSession(session);
			session=null;
		}
	
		return retBoolean;
	}
	
	@Transactional
    public boolean isVendorNexusUsed(Long vendorNexusID) throws DataAccessException {
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	boolean isUsed = true;
    	
    	try {    		
    		//TODO: PP-23
    		String sql = "select vnd.vendor_nexus_dtl_id from tb_vendor_nexus vn " +
    				"left join tb_vendor_nexus_dtl vnd on vnd.vendor_nexus_id = vn.vendor_nexus_id " +
    				"where vn.vendor_nexus_id = :vendorNexusID ";
    		query = session.createSQLQuery(sql);
    		query.setParameter("vendorNexusID", vendorNexusID);
    		List<?> result = query.list();
    		if(result==null || result.size()==0){
    			isUsed = false;	
    		}
    		else{		
	    		String vendorNexusDtlIds = "";
	    		long detailId = 0;
				for(int i=0; i< result.size(); i++){
					detailId = ((BigDecimal)result.get(i)).longValue();			
					if(vendorNexusDtlIds.length()>0){
						vendorNexusDtlIds = vendorNexusDtlIds + ",";
					}
					vendorNexusDtlIds = vendorNexusDtlIds + detailId;
				}
	    		
	    		String sqlTrans = "SELECT COUNT(*) " +
	    				"FROM tb_purchtrans_nexsit " +
	    	    		"WHERE country_vendor_nexus_dtl_id in (:vendorNexusDtlIds) " +
	    	    		"OR state_vendor_nexus_dtl_id in (:vendorNexusDtlIds) " +
	    	    		"OR county_vendor_nexus_dtl_id in (:vendorNexusDtlIds) " +
	    	    		"OR city_vendor_nexus_dtl_id in (:vendorNexusDtlIds) ";
	
	    		query = session.createSQLQuery(sqlTrans);
	    		query.setParameter("vendorNexusDtlIds", vendorNexusDtlIds);
	    		List<?> resultTrans = query.list();
	    		long count = ((BigDecimal) resultTrans.get(0)).longValue();
	    		
	    		if(count == 0) {
	    			isUsed = false;	
	    		}
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return isUsed;
    }
	
	public boolean isVendorNexusDetailUsed(Long vendorNexusDetailID) throws DataAccessException {
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	boolean isUsed = true;
    	
    	try {	
    		//TODO: PP-23
    		String sql = "SELECT COUNT(*) " +
    		"FROM tb_purchtrans_nexsit " +
    		"WHERE country_vendor_nexus_dtl_id = :vendorNexusDtlId " +
    		"OR state_vendor_nexus_dtl_id = :vendorNexusDtlId " +
    		"OR county_vendor_nexus_dtl_id = :vendorNexusDtlId " +
    		"OR city_vendor_nexus_dtl_id = :vendorNexusDtlId ";

    		query = session.createSQLQuery(sql);
    		query.setParameter("vendorNexusDtlId", vendorNexusDetailID);
    		List<?> result = query.list();
    		long count = ((BigDecimal) result.get(0)).longValue();
    		
    		if(count == 0) {
    			isUsed = false;	
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return isUsed;
    }
}
