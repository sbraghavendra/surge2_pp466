package com.ncsts.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.TempTransactionDetail;
import com.ncsts.view.util.ArithmeticUtils;

public class TransactionUpdateImpl implements TransactionUpdate {

	private static String selectClause = "SELECT mat.tax_matrix_id, mat.relation_sign, mat.relation_amount, "
      + "mat.then_hold_code_flag, mat.else_hold_code_flag, "
      + "mat.then_taxcode_detail_id, mat.else_taxcode_detail_id, "
      + "mat.then_cch_taxcat_code, mat.then_cch_group_code, mat.then_cch_item_code, "
      + "mat.else_cch_taxcat_code, mat.else_cch_group_code, mat.else_cch_item_code, "
      + "thentaxcddtl.taxcode_state_code, thentaxcddtl.taxcode_type_code, thentaxcddtl.taxcode_code, thentaxcddtl.jurisdiction_id, thentaxcddtl.measure_type_code, thentaxcddtl.taxtype_code, "
      + "elsetaxcddtl.taxcode_state_code, elsetaxcddtl.taxcode_type_code, elsetaxcddtl.taxcode_code, elsetaxcddtl.jurisdiction_id, elsetaxcddtl.measure_type_code, elsetaxcddtl.taxtype_code "
    + "FROM tb_tmp_transaction_detail trn, "
      + "(tb_tax_matrix mat left outer join tb_taxcode_detail thentaxcddtl on mat.then_taxcode_detail_id = thentaxcddtl.taxcode_detail_id) "
       + "left outer join tb_taxcode_detail elsetaxcddtl on mat.else_taxcode_detail_id = elsetaxcddtl.taxcode_detail_id "
	+ "WHERE ( trn.transaction_detail_id = :v_transaction_detail_id ) "
	  + "AND ( mat.default_flag = \'0\' AND mat.binary_weight > 0 ) "
	  + "AND (( mat.matrix_state_code = \'*ALL\' ) OR ( mat.matrix_state_code = :v_transaction_state_code )) "
	  + "AND ( mat.effective_date <= trn.gl_date ) "
	  + "AND ( mat.expiration_date >= trn.gl_date ) ";

	private static String orderByClause = " ORDER BY mat.binary_weight DESC, mat.significant_digits DESC, mat.effective_date DESC";

	private static HashMap<String, String []> opMap = new HashMap<String, String []>();
	static {
		opMap.put("na", new String [] {"THEN", "THEN", "THEN"});
		opMap.put("=", new String [] {"ELSE", "THEN", "ELSE"});
		opMap.put("<", new String [] {"THEN", "ELSE", "ELSE"});
		opMap.put("<=", new String [] {"THEN", "THEN", "ELSE"});
		opMap.put(">", new String [] {"ELSE", "ELSE", "THEN"});
		opMap.put(">=", new String [] {"ELSE", "THEN", "THEN"});
		opMap.put("<>", new String [] {"THEN", "ELSE", "THEN"});
	};
	private static int [] thenCols = {4,6,8,  9,14,15,16,17,18,19};
	private static int [] elseCols = {5,7,11,12,20,21,22,23,24,25};
	
	String taxTypeCode;
	private LocationMatrix locationMatrix = null;
	private Long locationMatrixJurisdictionId;
	
	@Override
	public void processTransaction(Connection con, TempTransactionDetail trn)  throws Exception {
		CallableStatement cstatement = null;
	
		String suspendStatus = trn.getTransactionInd();
		if (suspendStatus == null || !"S".equals(suspendStatus)) {
			if (getTaxMatrix(con, trn)) {	
				cstatement = con.prepareCall("{ ? = call get_tax_matrix(?)}");
				
				//5058
				cstatement.registerOutParameter(1, Types.CLOB);						
				cstatement.setLong(2, trn.getTransactionDetailId());
				cstatement.execute();
				
				java.sql.Clob clob= cstatement.getClob(1);
				int strLength = (int) clob.length();
				String taxMatrixWhere = clob.getSubString(1, strLength);
				
				cstatement.close();
				if ((taxMatrixWhere != null) && !"".equals(taxMatrixWhere))
					updateFromTaxMatrix(con, taxMatrixWhere, trn);
			}
		}

		suspendStatus = trn.getTransactionInd();
		if (suspendStatus == null || !"S".equals(suspendStatus)) {
			
			//TODO: AC
			//if ("T".equals(trn.getTaxcodeTypeCode())) {
			if ("T".equals("void")) {
				initTransVals(trn);
				locationMatrix = new LocationMatrix();
				if (getLocationMatrix(con, trn)) {
					cstatement = con.prepareCall("{ ? = call f_get_location_matrix(?)}");
					
					cstatement.registerOutParameter(1, Types.VARCHAR);						
					cstatement.setLong(2, trn.getTransactionDetailId());
					cstatement.execute();
					String locationMatrixQuery = (String)cstatement.getObject(1);
					cstatement.close();
					if (locationMatrixQuery.length() > 0 ) {
						updateFromLocationMatrix(con, locationMatrixQuery, trn);
					}
					Long locId = locationMatrix.getLocationMatrixId();
					if ((locId != null) && !new Long(0).equals(locId)) {
						trn.setLocationMatrixId(locationMatrix.getLocationMatrixId());	
						trn.setJurisdictionId(locationMatrixJurisdictionId);							
					} else {
						trn.setTransactionInd("S");
						trn.setSuspendInd("L");
						
						//903 No calculateTaxes() needed if suspended
						suspendStatus = trn.getTransactionInd();
					}
				}
				
				//903 No calculateTaxes() needed if suspended				
				if (suspendStatus == null || !"S".equals(suspendStatus))
					calculateTaxes(con, trn, taxTypeCode);
			//TODO: AC
			//} else if ("E".equals(trn.getTaxcodeTypeCode())) {
			} else if ("E".equals("void")) {
				initTransVals(trn);
				trn.setJurisdictionTaxrateId(null);
				//TODO: AC
				//trn.setMeasureTypeCode("0");
				
				trn.setTransactionInd("P");
				
			//TODO: AC
			//} else if ("Q".equals(trn.getTaxcodeTypeCode())) {
			} else if ("Q".equals("void")) {
				trn.setTransactionInd("Q");
			} else {		
				trn.setTransactionInd("S");
				trn.setSuspendInd("?");
			}
		}
	
		suspendStatus = trn.getTransactionInd();
		if (suspendStatus == null || "P".equals(suspendStatus)) {
			
			//TODO: AC
			/*
			if(trn.getTaxMatrixId()!=null && trn.getTaxMatrixId()>0){
				//Do nothing.
			}
			else if ((trn.getTaxcodeDetailId() == null) || (trn.getJurisdictionId() == null)) {
				trn.setSuspendInd((trn.getTaxcodeDetailId() == null ? "T" : "L"));
				suspendStatus = "S";
			}
			*/
			
			trn.setTransactionInd(suspendStatus);
		}
	}

	private boolean getTaxMatrix(Connection con, TempTransactionDetail instance) throws SQLException {
		int rowCount = 0;
		taxTypeCode = null;
		
		//TODO: AC
		//Long detailId = instance.getTaxcodeDetailId();
		Long detailId = 0L;

		if ((detailId != null) && !new Long(0).equals(detailId)) {
			PreparedStatement p = con.prepareStatement("select taxtype_code from tb_taxcode "
					+ "where taxcode_code = ? and taxcode_type_code = ?");
			p.setString(1, instance.getTaxcodeCode());
			
			//TODO: AC
			//p.setString(2, instance.getTaxcodeTypeCode());
			p.setString(2, "");
			
			
			ResultSet rs = p.executeQuery();
			if (rs.next())
				taxTypeCode = rs.getString(1);
			p.close();
			con.commit();
			return false;
		} else {
			String tranStateCode = instance.getTransactionStateCode();
			if (tranStateCode != null) {
				PreparedStatement p = con.prepareStatement("select count(*) from tb_taxcode_state "
						+ "where taxcode_state_code = ?");
				p.setString(1, tranStateCode);
				ResultSet rs = p.executeQuery();
				if (rs.next())
					rowCount = ((Number)rs.getObject(1)).intValue();
				p.close();
				con.commit();
			}
			if (rowCount <= 0) {
				PreparedStatement p = con.prepareStatement("select state from tb_jurisdiction "
						+ "where jurisdiction_id = ?");
				p.setLong(1, instance.getJurisdictionId());
				ResultSet rs = p.executeQuery();
				if (rs.next())
					tranStateCode = rs.getString(1);
				p.close();
				con.commit();
				if (tranStateCode != null) {
					instance.setAutoTransactionStateCode((instance.getTransactionStateCode() == null ?
							"*NULL" : instance.getTransactionStateCode()));
					instance.setTransactionStateCode(tranStateCode);
				} else {
					instance.setTransactionInd("S");
					instance.setSuspendInd("L");
					return false;
				}
			}
		}
		return true;
	}
	
	private void updateFromTaxMatrix(Connection con, String where, TempTransactionDetail trans) throws Exception {
		String query = selectClause + where + orderByClause;
		boolean stateDriver = query.indexOf(":v_transaction_state_code") != query.lastIndexOf(":v_transaction_state_code");
		String stmt = query.replaceAll(":v_transaction_detail_id", "?");
		stmt = stmt.replaceAll(":v_transaction_state_code", "?");
		
		PreparedStatement p = con.prepareStatement(stmt);
		p.setLong(1, trans.getTransactionDetailId());	
		p.setString(2, trans.getTransactionStateCode());
		if (stateDriver){
			p.setString(3, trans.getTransactionStateCode());
		}
		
		ResultSet rs = p.executeQuery();
		if (rs.next()) {
			assignTaxMatrixValues(trans, rs);
			trans.setTaxMatrixId(rs.getLong("TAX_MATRIX_ID"));
		} else {
			trans.setTransactionInd("S");
			trans.setSuspendInd("T");
		}
	}

	private void assignTaxMatrixValues(TempTransactionDetail trans, ResultSet rs) throws Exception {
		Object [] lessVals = new Object [10];
		Object [] eqVals = new Object [10];
		Object [] gtVals = new Object [10];
		Object [] vals = new Object [] {lessVals, eqVals, gtVals};
		trans.setTaxMatrixId(rs.getLong("TAX_MATRIX_ID"));
		String relation = rs.getString("RELATION_SIGN");
		long relationAmount = "na".equals(relation) ? 0L : rs.getLong("RELATION_AMOUNT");
		String [] assignGroups = opMap.get(relation);
		for (String group : assignGroups) {
			for (Object v  : vals) {
				Object [] oa = (Object [])v;
				for (int i = 0; i < oa.length; i++)
					oa[i] = "THEN".equals(group) ? rs.getObject(thenCols[i]) : rs.getObject(elseCols[i]);
			}
		}
		BigDecimal sign = ArithmeticUtils.subtract(trans.getGlLineItmDistAmt(), ArithmeticUtils.toBigDecimal(relationAmount));
		Long overrideJurisId = null;
		taxTypeCode = null;
		switch (sign.compareTo(BigDecimal.ZERO)  < 0 ? -1 : (sign.compareTo(BigDecimal.ZERO) > 0 ? 1 : 0)) {
		case -1:
//AC		trans.setTaxcodeDetailId((lessVals[7] == null ? null : ((Number)lessVals[1]).longValue()));
//CCH		trans.setCchTaxcatCode((String)lessVals[2]);
//CCH		trans.setCchGroupCode((String)lessVals[3]);
//AC		trans.setTaxcodeStateCode((String)lessVals[4]);
//AC		trans.setTaxcodeTypeCode((String)lessVals[5]);
			trans.setTaxcodeCode((String)lessVals[6]);
			overrideJurisId = lessVals[7] == null ? null : ((Number)lessVals[7]).longValue();
//AC		trans.setMeasureTypeCode((String)lessVals[8]);
			taxTypeCode =(String)lessVals[9];
			break;
		case 0:
//AC		trans.setTaxcodeDetailId((eqVals[7] == null ? null : ((Number)eqVals[1]).longValue()));
//CCH		trans.setCchTaxcatCode((String)eqVals[2]);
//CCH		trans.setCchGroupCode((String)eqVals[3]);
//AC		trans.setTaxcodeStateCode((String)eqVals[4]);
//AC		trans.setTaxcodeTypeCode((String)eqVals[5]);
			trans.setTaxcodeCode((String)eqVals[6]);
			overrideJurisId = eqVals[7] == null ? null : ((Number)eqVals[7]).longValue();
//AC		trans.setMeasureTypeCode((String)eqVals[8]);
			taxTypeCode =(String)eqVals[9];
			break;
		case 1:
//AC		trans.setTaxcodeDetailId((gtVals[7] == null ? null : ((Number)gtVals[1]).longValue()));
//CCH		trans.setCchTaxcatCode((String)gtVals[2]);
//CCH		trans.setCchGroupCode((String)gtVals[3]);
//AC		trans.setTaxcodeStateCode((String)gtVals[4]);
//AC		trans.setTaxcodeTypeCode((String)gtVals[5]);
			trans.setTaxcodeCode((String)gtVals[6]);
			overrideJurisId = gtVals[7] == null ? null : ((Number)eqVals[7]).longValue();
//AC		trans.setMeasureTypeCode((String)gtVals[8]);
			taxTypeCode =(String)gtVals[9];
		}
		
		if ((overrideJurisId != null) && !new Long(0).equals(overrideJurisId)) {
			trans.setJurisdictionId(overrideJurisId);
			trans.setManualJurisdictionInd("AO");
		}
	}

	private boolean getLocationMatrix(Connection con, TempTransactionDetail trans) throws SQLException {
		locationMatrixJurisdictionId = trans.getJurisdictionId() == null ? new Long(0) : trans.getJurisdictionId();
		Long locId = trans.getLocationMatrixId() == null ? new Long(0) : trans.getLocationMatrixId();
		if ((locationMatrixJurisdictionId != null) && !new Long(0).equals(locationMatrixJurisdictionId)) {
			locationMatrix = new LocationMatrix();
			locationMatrix.setOverrideTaxtypeCode("*NO");
			locationMatrix.setStateFlag("1");
			locationMatrix.setCountyFlag("1");
			locationMatrix.setCityFlag("1");
			if ((locId != null) && !new Long(0).equals(locId)) {
				PreparedStatement p = con.prepareStatement("select location_matrix_id, override_taxtype_code, "
						+ "state_flag, county_flag, city_flag "
						+ "from tb_location_matrix where location_matrix_id = ?");
				p.setLong(1, locId);				
				ResultSet rs = p.executeQuery();	
				if (rs.next()) {
					locationMatrix.setOverrideTaxtypeCode(rs.getString("OVERRIDE_TAXTYPE_CODE"));
					locationMatrix.setStateFlag(rs.getString("STATE_FLAG"));
					locationMatrix.setCountyFlag(rs.getString("COUNTY_FLAG"));
					locationMatrix.setCityFlag(rs.getString("CITY_FLAG"));
					locationMatrix.setLocationMatrixId(rs.getLong("LOCATION_MATRIX_ID"));
				}
				p.close();
				con.commit();
			}
		}
		
		if (((locId == null) || new Long(0).equals(locId)) && (trans.getManualJurisdictionInd() == null))
			return true;
		return false;
	}
	
	private void updateFromLocationMatrix(Connection con, String query, TempTransactionDetail trans) throws Exception {
		String stmt = query.replaceAll(":v_transaction_detail_id", "?");
		PreparedStatement p = con.prepareStatement(stmt);
		p.setLong(1, trans.getTransactionDetailId());
		ResultSet rs = p.executeQuery();
		if (rs.next()) {
			locationMatrix = new LocationMatrix();
			locationMatrix.setOverrideTaxtypeCode(rs.getString("OVERRIDE_TAXTYPE_CODE"));
			locationMatrix.setStateFlag(rs.getString("STATE_FLAG"));
			locationMatrix.setCountyFlag(rs.getString("COUNTY_FLAG"));
			locationMatrix.setCityFlag(rs.getString("CITY_FLAG"));
			locationMatrix.setLocationMatrixId(rs.getLong("LOCATION_MATRIX_ID"));
			locationMatrixJurisdictionId = rs.getLong("JURISDICTION_ID");
		}
	}

	private void initTransVals(TempTransactionDetail instance) {
		BigDecimal glExtractAmt = instance.getGlExtractAmt();
		if ((glExtractAmt != null) && glExtractAmt.compareTo(BigDecimal.ZERO) != 0)   //glExtractAmt.compareTo(BigDecimal.ZERO) == 0
			instance.setGlLineItmDistAmt(glExtractAmt);

		instance.setGlExtractAmt(BigDecimal.ZERO);
		instance.setTbCalcTaxAmt(BigDecimal.ZERO);
		instance.setStateUseAmount(BigDecimal.ZERO);
		instance.setStateUseTier2Amount(BigDecimal.ZERO);
		instance.setStateUseTier3Amount(BigDecimal.ZERO);
		instance.setCountyUseAmount(BigDecimal.ZERO);
		instance.setCountyLocalUseAmount(BigDecimal.ZERO);
		instance.setCityUseAmount(BigDecimal.ZERO);
		instance.setCityLocalUseAmount(BigDecimal.ZERO);
		instance.setStateUseRate(BigDecimal.ZERO);
		instance.setStateUseTier2Rate(BigDecimal.ZERO);
		instance.setStateUseTier3Rate(BigDecimal.ZERO);
		instance.setStateSplitAmount(BigDecimal.ZERO);
		instance.setStateTier2MinAmount(BigDecimal.ZERO);
		instance.setStateTier2MaxAmount(BigDecimal.ZERO);
		instance.setStateMaxtaxAmount(BigDecimal.ZERO);
		instance.setCountyUseRate(BigDecimal.ZERO);
		instance.setCountyLocalUseRate(BigDecimal.ZERO);
		instance.setCountySplitAmount(BigDecimal.ZERO);
		instance.setCountyMaxtaxAmount(BigDecimal.ZERO);
		instance.setCountySingleFlag("0");
		instance.setCountyDefaultFlag("0");
		instance.setCityUseRate(BigDecimal.ZERO);
		instance.setCityLocalUseRate(BigDecimal.ZERO);
		instance.setCitySplitAmount(BigDecimal.ZERO);
		instance.setCitySplitUseRate(BigDecimal.ZERO);
		instance.setCitySingleFlag("0");
		instance.setCityDefaultFlag("0");
		instance.setCombinedUseRate(BigDecimal.ZERO);
		//5033
		//instance.setMeasureTypeCode("0");
	}
	
	private void calculateTaxes(Connection con, TempTransactionDetail trans, String taxTypeCode) throws Exception {
		CallableStatement cstatement = con.prepareCall(DatabaseUtil.getCalculateTaxesQuery(DatabaseUtil.getDatabaseProductName(con)));
		cstatement.registerOutParameter(1, Types.VARCHAR);						
		cstatement.setLong(2, trans.getJurisdictionId());
		cstatement.setDate(3, new java.sql.Date(trans.getGlDate().getTime()));
//AC	cstatement.setString(4, trans.getMeasureTypeCode());
		cstatement.setBigDecimal(5, trans.getGlLineItmDistAmt());
		cstatement.setString(6, taxTypeCode);
		cstatement.setString(7, locationMatrix.getOverrideTaxtypeCode());
		//Federal Tax
		cstatement.setString(8, locationMatrix.getCountryFlag());
		cstatement.setString(9, locationMatrix.getStateFlag());
		cstatement.setString(10, locationMatrix.getCountyFlag());
//ac		cstatement.setString(11, locationMatrix.getCountyLocalFlag());
		cstatement.setString(12, locationMatrix.getCityFlag());
//ac		cstatement.setString(13, locationMatrix.getCityLocalFlag());
		cstatement.setBigDecimal(14, trans.getInvoiceFreightAmt());
		cstatement.setBigDecimal(15, trans.getInvoiceDiscountAmt());
		//Federal Tax
		cstatement.setString(16, trans.getTransactionCountryCode());
		cstatement.setString(17, trans.getTransactionStateCode());
		cstatement.setString(18, "importdefn");
		cstatement.setString(19, trans.getTaxcodeCode());
		
		cstatement.execute();
		
		String taxAmounts = (String)cstatement.getObject(1);
		cstatement.close();

		String [] amts = taxAmounts.replace(",,", ",0,").replace(",,", ",0,").split(",");
		trans.setJurisdictionTaxrateId(new Long(amts[0]));
		//
		trans.setCountryUseAmount(ArithmeticUtils.toBigDecimal(amts[1]));
		
		trans.setStateUseAmount(ArithmeticUtils.toBigDecimal(amts[2]));
		trans.setStateUseTier2Amount(ArithmeticUtils.toBigDecimal(amts[3]));
		trans.setStateUseTier3Amount(ArithmeticUtils.toBigDecimal(amts[4]));
		trans.setCountyUseAmount(ArithmeticUtils.toBigDecimal(amts[5]));
		trans.setCountyLocalUseAmount(ArithmeticUtils.toBigDecimal(amts[6]));
		trans.setCityUseAmount(ArithmeticUtils.toBigDecimal(amts[7]));
		trans.setCityLocalUseAmount(ArithmeticUtils.toBigDecimal(amts[8]));
		//
		trans.setCountryUseRate(ArithmeticUtils.toBigDecimal(amts[9]));
		
		trans.setStateUseRate(ArithmeticUtils.toBigDecimal(amts[10]));
		trans.setStateUseTier2Rate(ArithmeticUtils.toBigDecimal(amts[11]));
		trans.setStateUseTier3Rate(ArithmeticUtils.toBigDecimal(amts[12]));
		trans.setStateSplitAmount(ArithmeticUtils.toBigDecimal(amts[13]));
		trans.setStateTier2MinAmount(ArithmeticUtils.toBigDecimal(amts[14]));
		trans.setStateTier2MaxAmount(ArithmeticUtils.toBigDecimal(amts[15]));
		trans.setStateMaxtaxAmount(ArithmeticUtils.toBigDecimal(amts[16]));
		trans.setCountyUseRate(ArithmeticUtils.toBigDecimal(amts[17]));
		trans.setCountyLocalUseRate(ArithmeticUtils.toBigDecimal(amts[18]));
		trans.setCountySplitAmount(ArithmeticUtils.toBigDecimal(amts[19]));
		trans.setCountyMaxtaxAmount(ArithmeticUtils.toBigDecimal(amts[20]));
		trans.setCountySingleFlag(amts[21]);
		trans.setCountyDefaultFlag(amts[22]);
		trans.setCityUseRate(ArithmeticUtils.toBigDecimal(amts[23]));
		trans.setCityLocalUseRate(ArithmeticUtils.toBigDecimal(amts[24]));
		trans.setCitySplitAmount(ArithmeticUtils.toBigDecimal(amts[25]));
		trans.setCitySplitUseRate(ArithmeticUtils.toBigDecimal(amts[26]));
		trans.setCitySingleFlag(amts[27]);
		trans.setCityDefaultFlag(amts[28]);
		trans.setCombinedUseRate(ArithmeticUtils.toBigDecimal(amts[29]));
		trans.setTbCalcTaxAmt(ArithmeticUtils.toBigDecimal(amts[30]));
		BigDecimal taxableAmt = (ArithmeticUtils.toBigDecimal(amts[31]));

		Long jurisTaxRtId = trans.getJurisdictionTaxrateId() == null ?
				new Long(0) : trans.getJurisdictionTaxrateId();
		if (jurisTaxRtId == 0) {
			trans.setTransactionInd("S");
			trans.setSuspendInd("R");
		} else {
			if (trans.getGlLineItmDistAmt().compareTo(taxableAmt) != 0) {
				trans.setGlExtractAmt(trans.getGlLineItmDistAmt());
				trans.setGlLineItmDistAmt(taxableAmt);
			}
		}
	}

}
