package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;

import com.ncsts.domain.Registration;


public interface RegistrationDAO extends GenericDAO<Registration,Long> {
	
	public List<Registration> getAllRecords(Registration exampleInstance, OrderBy orderBy, int firstRow, int maxResults);

	public Long count(Registration exampleInstance);
    
    public abstract List<Registration> findAllRegistrations() throws DataAccessException;
    
    public List<Registration> findAllRegistrationsByEntityId(Long entityId) throws DataAccessException;
    
    public Long persist(Registration registration)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(Registration registration) throws DataAccessException;

}