	package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.BCPConfigStagingText;

public class JPAConfigImportDAO extends JpaDaoSupport implements ConfigImportDAO{
	@SuppressWarnings("unused")
	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<BCPConfigStagingText> getImportedData(Long batchId){
		return getJpaTemplate().find("from BCPConfigStagingText configText where configText.id.batchId = ?",batchId);
		
	}
}
