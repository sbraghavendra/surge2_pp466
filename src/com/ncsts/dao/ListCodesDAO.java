package com.ncsts.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;


public interface ListCodesDAO {
	
	public abstract List<ListCodes> getListCodesByCodeTypeCode(String codeTypeCode) throws DataAccessException;
	public abstract List<ListCodes> getListCodesByCodeCode(String codeCode) throws DataAccessException;
	public abstract List<ListCodes> getAllListCodes() throws DataAccessException;
	public abstract ListCodes findByPK(ListCodesPK pk) throws DataAccessException;
	
	public abstract ListCodes addListCodeRecord(ListCodes instance) throws DataAccessException;
    public abstract void update(ListCodes listCodes) throws DataAccessException;
    public abstract void delete(ListCodesPK pk) throws DataAccessException;
    public List<ListCodes> getListCodesByTypeAndCode(String codeTypeCode, String code) throws DataAccessException;
	
    public ListCodes getListCode(String codeTypeCode, String codeCode, String description) throws DataAccessException;
	

	
}
