package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.Cust;

public class JPABCPCustLocnDAO extends JPAGenericDAO<BCPCustLocn, Long> implements BCPCustLocnDAO {

	@Override
	@Transactional
	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPCustLocn.class);
		criteria.add(Restrictions.eq("batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
		
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}

	@Transactional
	public List<BCPCustLocn> getPage(BatchMaintenance batch, int firstRow,
			int maxResults, boolean setOrder) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPCustLocn.class);
		criteria.add(Restrictions.eq("batchId", batch.getBatchId()));
		
		if(setOrder){
			criteria.addOrder( Order.asc("recordType") );
			criteria.addOrder( Order.asc("bcpCustLocnId") );
		}

		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		try {
			List<BCPCustLocn> returnBCPCustLocn = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPCustLocn;
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPCustLocn>();
		}
	}
	
	@Override
	@Transactional
	public List<BCPCustLocn> getPage(BatchMaintenance batch, int firstRow,
			int maxResults) throws DataAccessException {
		
		return getPage(batch, firstRow, maxResults, false);
	}
	
	@Transactional
	public List<BCPCustLocn> findByBatchId(Long batchId) throws DataAccessException {

		String sql = "select * from TB_BCP_CUST_LOCN where BATCH_ID=" + batchId.intValue() + " order by RECORD_TYPE asc, BCP_CUST_LOCN_ID asc ";
		
		Session session = createLocalSession();
		List<BCPCustLocn> returnBCPCustLocn =  new ArrayList<BCPCustLocn>();
		org.hibernate.Query query = null;
		try {
			query = session.createSQLQuery(sql);
			returnBCPCustLocn = query.list();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return returnBCPCustLocn;
	}
	
}
