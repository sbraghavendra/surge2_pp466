/**
 * 
 */
package com.ncsts.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.ReferenceDocument;
import com.seconddecimal.billing.domain.ListCode;
import com.seconddecimal.billing.query.SqlQuery;

/**
 * @author Muneer
 *
 */

public class JPAListCodesDAO extends JpaDaoSupport implements ListCodesDAO {

	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
	    logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
	    return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
	    	localSession.clear();
	    	localSession.close();
	    }
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getListCodesByCodeTypeCode(String codeTypeCode) throws DataAccessException {
		List<ListCodes> find = getJpaTemplate().find(" SELECT listCodes " +
				" FROM ListCodes listCodes " +
				" where listCodes.id.codeTypeCode = ?1 order by listCodes.description ", codeTypeCode );
		return find;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getListCodesByCodeCode(String codeCode) throws DataAccessException {
		logger.debug("Inside getListCodesByCodeCode code");
		List<ListCodes> find = getJpaTemplate().find(" SELECT listCodes " +
				" FROM ListCodes listCodes " +
				" where listCodes.id.codeCode = ?1 order by listCodes.description ", codeCode );
		return find;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getAllListCodes() throws DataAccessException{
		/* SELECT CODE_CODE, DESCRIPTION FROM TB_LIST_CODE WHERE CODE_TYPE_CODE = '*DEF' */
	
		List<ListCodes> find = getJpaTemplate().find(" select listCodes from ListCodes listCodes where listCodes.id.codeTypeCode = '*DEF' order by listCodes.id.codeCode " );	
		
		logger.debug(" from JPAListCodesDAO.getAllListCodes :::list size is ::" + ((find == null)? 0:find.size()));
		return find;		
	}

	@Transactional
	public ListCodes addListCodeRecord(ListCodes instance) throws DataAccessException{
		if (instance.getListCodesPK().getCodeTypeCode() == null) {
			//TODO Handle this case properly later to catch the exception and notify service layer
           logger.error(" Can not inser Null Values in PK colums");
           logger.info(" User Defined Error::prblem in JPAListCodesDAO.addListCodeRecord () ::Can not inser Null Values in PK colums" );
		}

		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		return instance;		
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(ListCodes listCodes) throws DataAccessException{
		Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.update(listCodes);
        t.commit();
        closeLocalSession(session);
        session=null;
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void delete(ListCodesPK pk) throws DataAccessException{
		ListCodes lc = findByPK(pk);
		if (lc != null) {
			// Update query - needs to be transactional!
			Session session = createLocalSession();
	    	Transaction t = session.beginTransaction();
	    	
	    	if (pk.getCodeTypeCode().equalsIgnoreCase("*DEF")) {
				org.hibernate.Query query = session.createSQLQuery("delete from TB_LIST_CODE where CODE_TYPE_CODE = :codeTypeCode");
				query.setParameter("codeTypeCode", pk.getCodeCode());
				int rows = query.executeUpdate();
				logger.debug(rows + " code code rows deleted");
				
				query = session.createSQLQuery("delete from TB_LIST_CODE where CODE_TYPE_CODE = :codeTypeCode and CODE_CODE = :codeCode");
				query.setParameter("codeTypeCode", "*DEF");
				query.setParameter("codeCode", pk.getCodeCode());
				rows = query.executeUpdate();
				logger.debug(rows + " definition rows deleted");
	    	}
	    	else{
	    		org.hibernate.Query query = session.createSQLQuery("delete from TB_LIST_CODE where CODE_TYPE_CODE = :codeTypeCode and CODE_CODE = :codeCode");
				query.setParameter("codeTypeCode", pk.getCodeTypeCode());
				query.setParameter("codeCode", pk.getCodeCode());
				int rows = query.executeUpdate();
				logger.debug(rows + " code code rows deleted");
	    	}
	    	
			// All done
			t.commit();
			
			closeLocalSession(session);
			session=null;
		} else {
			logger.warn("ListCodes Object not found for deletion");
		}	
	}
	
	@Transactional
	public ListCodes findByPK(ListCodesPK pk) throws DataAccessException {
		return getJpaTemplate().find(ListCodes.class, pk);
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ListCodes> getListCodesByTypeAndCode(String codeTypeCode, String code) throws DataAccessException {
		StringBuffer sb = new StringBuffer("SELECT listCodes FROM ListCodes listCodes ");
		
		Map<String, String> params = new HashMap<String, String>();
		
		String where = "WHERE ";
		if(StringUtils.hasText(codeTypeCode)) {
			sb.append(where).append("upper(listCodes.id.codeTypeCode) = :codeTypeCode ");
			where = "";
			params.put("codeTypeCode", codeTypeCode.toUpperCase());
		}
		
		if(StringUtils.hasText(code)) {
			if(where.length() > 0) {
				sb.append(where);
				where = "";
			}
			else {
				sb.append("AND ");
			}
			sb.append("upper(listCodes.id.codeCode) like :code ");
			params.put("code",code.toUpperCase());
		}
		sb.append("order by listCodes.description");
		
		return (List<ListCodes>) getJpaTemplate().findByNamedParams(sb.toString(), params);
	}
	
	@SuppressWarnings("unchecked")
	public ListCodes getListCode(String codeTypeCode, String codeCode, String description) throws DataAccessException {
		
		ListCodes listCode = null;
		List<ListCodes> find = getJpaTemplate().find(" SELECT listCodes FROM ListCodes listCodes " +
				" where upper(listCodes.id.codeTypeCode) = ?1 and upper(listCodes.id.codeCode) = ?2 order and upper(listCodes.description) ", 
				codeTypeCode.toUpperCase(), codeCode.toUpperCase(), description.toUpperCase());
		
		if(find!=null && find.size()>0) {
			listCode = find.get(0);
		}

		return listCode;
	}

}
