package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

public class JPAOptionDAO extends JpaDaoSupport implements OptionDAO {
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	  @SuppressWarnings("unchecked")	
	  @Transactional
	  public Option findByPK(OptionCodePK codePK) throws DataAccessException {
	    return getJpaTemplate().find(Option.class, codePK);
	  }
	  
	  
	  
	  @SuppressWarnings("unchecked")
	  @Transactional
	  public List<Option> findAllOptions() throws DataAccessException {
	      return getJpaTemplate().find(" select option from Option option");	  
	  }
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Option> getSystemPreferences() throws DataAccessException {
		  String qry = ("SELECT o FROM Option o WHERE " +
	      "o.codePK.optionTypeCode = 'SYSTEM' AND o.codePK.userCode = 'SYSTEM' ");
	      return getJpaTemplate().find(qry);		  
	  } 	  
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Option> getAdminPreferences() throws DataAccessException {
		  String qry = ("SELECT o FROM Option o WHERE " +
	      "o.codePK.optionTypeCode = 'ADMIN' AND o.codePK.userCode = 'ADMIN' ");
	      return getJpaTemplate().find(qry);		  
	  }  
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Option> getUserPreferences() throws DataAccessException {
		  String qry = ("SELECT o FROM Option o WHERE " +
	      "o.codePK.optionTypeCode = 'USER' AND o.codePK.userCode = 'STSCORP' ");
	      return getJpaTemplate().find(qry);		  
	  } 	  
	  
	  @Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	  public void remove(OptionCodePK codePK) throws DataAccessException {
	      Option option = getJpaTemplate().find(Option.class, codePK);
		  if( option != null){
		      getJpaTemplate().remove(option);
		  }else{
		      logger.info("Option Object not found for deletion");
		  }
	  } 
	  
	  public void saveOrUpdate (Option option) throws DataAccessException {	
		  Session session = createLocalSession();
	      Transaction t = session.beginTransaction();
	      t.begin();
	      session.saveOrUpdate(option);
	      t.commit();
	      closeLocalSession(session);
	      session=null;	
	  }	
		
	  @Transactional
	  public String persist (Option option) throws DataAccessException {
	      getJpaTemplate().persist(option);
		  String key = option.getCodePK().getOptionCode();
		  return key;
	  }
}


