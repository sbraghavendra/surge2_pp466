package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.TransactionHeader;

public interface DwColumnDAO {
	public List<TransactionHeader> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException;
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException;
	public List<TransactionHeader> getColumnsByDataWindowName(String windowName, String dataWindowName) throws DataAccessException;
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException;
	public void updateColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException;
	public void addColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException;
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException;
	public String getUserByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException;
	public List<TransactionHeader> getColumnsByUserandDataWindowName(String windowName, String dataWindowName, String userCode) throws DataAccessException;
	public void updateColumnsByUserAndFilterName(String screenName, String selectedColumnName, String selectedUserName, List<TransactionHeader> transactionHeaders) throws DataAccessException;
	public void addColumnsByUserAndFilterName(String windowName, String filterName, String selectedUserName,  List<TransactionHeader> transactionHeaders) throws DataAccessException;
	public void deleteColumnsByUserAndFilterName(String windowName, String filterName, String selectedUserName) throws DataAccessException;
}