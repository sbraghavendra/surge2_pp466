package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;

public class JPAImportConnectionDAO extends JPAGenericDAO<ImportConnection, String> implements ImportConnectionDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
	
    
    @Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)	
	public List<ImportConnection> getConnectionsByType(String type) {
		return getJpaTemplate().find("from ImportConnection conn where conn.importConnectionType = ?1", type);		
	}
    
    @SuppressWarnings("unchecked")
	@Transactional(readOnly=true)	
	public List<ImportConnection> getConnectionsByCode(String importConnectionCode) {
		return getJpaTemplate().find("from ImportConnection conn where conn.importConnectionCode = ?1", importConnectionCode);		
	}
    
/*	@Override
	@Transactional(readOnly=true)
	public void add(ImportConnection importConnection)
			throws DataAccessException {
		// TODO Auto-generated method stub
		
	}*/

    @Override
    @Transactional(readOnly=false)
    public void update(ImportConnection importConnection) throws DataAccessException {
    	Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.update(importConnection);
        t.commit();
        closeLocalSession(session);
        session=null;	
    }
    
    
	@Override
	@Transactional(readOnly=false)
	public void delete(String importConnectionCode) throws DataAccessException {
		// TODO Auto-generated method stub
		ImportConnection conn = findById(importConnectionCode);
		if (conn!=null) {
			getJpaTemplate().remove(conn);
			getJpaTemplate().flush();
		} else {
			System.out.println("Import Connection can't be deleted as it does not exist. ImportConnectionCode = "+importConnectionCode);
			logger.info("Import Connection can't be deleted as it does not exist. ImportConnectionCode = "+importConnectionCode);
		}
	}

	@Transactional(readOnly=true)
	public Long count(ImportConnection exampleInstance) {
		Long ret = 0L;	
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(ImportConnection.class);
			if(exampleInstance.getImportConnectionType()!=null && exampleInstance.getImportConnectionType().length()>0){
        		criteria.add(Restrictions.eq("importConnectionType", exampleInstance.getImportConnectionType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importConnectionCode", exampleInstance.getImportConnectionCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());
			
			criteria.setProjection(Projections.rowCount());
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
		} catch (Exception e) {
				e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<ImportConnection> getAllRecords(ImportConnection exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<ImportConnection> list =  null;
		Session session = createLocalSession();
		try { 	
			Criteria criteria = session.createCriteria(ImportConnection.class);
			if(exampleInstance.getImportConnectionType()!=null && exampleInstance.getImportConnectionType().length()>0){
        		criteria.add(Restrictions.eq("importConnectionType", exampleInstance.getImportConnectionType()).ignoreCase());
        	}
			addWildcardCriteria(criteria, "importConnectionCode", exampleInstance.getImportConnectionCode());
			addWildcardCriteria(criteria, "description", exampleInstance.getDescription());
			addWildcardCriteria(criteria, "comments", exampleInstance.getComments());

			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            else{
            	criteria.addOrder(Order.asc("importConnectionCode"));
            }
            
            list = criteria.list();
		} 
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}
}
