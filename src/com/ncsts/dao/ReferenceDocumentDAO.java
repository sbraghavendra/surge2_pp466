package com.ncsts.dao;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ReferenceDocument;

/**
 * @author Paul Govindan
 *
 */

public interface ReferenceDocumentDAO extends GenericDAO<ReferenceDocument,String> {
	public List<ReferenceDocument> search(ReferenceDocument refDoc,int firstRow, int maxResults,OrderBy orderBy);
	public List<ReferenceDocument> searchForRefDoc(ReferenceDocument refDoc);
	public Long count(ReferenceDocument exampleInstance);
}
