package com.ncsts.dao;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.exception.PurchaseTransactionProcessSuspendedException;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;

import java.util.Date;
import java.util.List;


public interface PurchaseTransactionDAO  extends GenericDAO<PurchaseTransaction, Long>{
    public LocationMatrix getLocationViaDrivers(String sql, Long entityId, Date effectiveDate, Date expirationDate);
    public TaxMatrix getTaxMatrixViaDrivers(String sql, Long entityId, Date effectiveDate, Date expirationDate);
    void save(PurchaseTransaction transaction);
    public PurchaseTransaction findById(Long purchtransId);
    public Long findLastPurchaseTransactionId();
    public ProcessSuspendedTransactionDocument findSuspendedTransactions(Long newSuspendRuleId, SuspendReason suspendReason) throws PurchaseTransactionProcessSuspendedException;
    public ListCodes retrieveErrorText(String error);
    public List<PurchaseTransaction> findAllTransactionsByMatrixId(Long matrixId, List<String> matrixFields);
}
