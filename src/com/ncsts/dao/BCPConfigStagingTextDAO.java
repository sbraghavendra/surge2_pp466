/*
 * Author: Jim M. Wilson
 * Created: Aug 23, 2008
 * $Date: 2008-08-29 16:28:03 -0500 (Fri, 29 Aug 2008) $: - $Revision: 2020 $: 
 */

package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPConfigStagingText;
import com.ncsts.domain.BCPConfigStagingTextPK;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchMaintenance;

/**
 *
 */
public interface BCPConfigStagingTextDAO 	extends GenericDAO<BCPConfigStagingText, BCPConfigStagingTextPK> {
	public Long count(BatchMaintenance batch);
	public List<BCPConfigStagingText> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
