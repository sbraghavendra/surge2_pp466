package com.ncsts.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import com.ncsts.common.LogMemory;
import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.seconddecimal.billing.domain.SaleTransaction;

public class JPATransactionDetailSaleDAO extends JPAGenericDAO<SaleTransaction,Long> implements
	TransactionDetailSaleDAO {
	
	@Transactional(readOnly=true)
	public Long count(SaleTransaction exampleInstance) {
		Long ret = 0L;	
		
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(SaleTransaction.class);
			Example example = Example.create(exampleInstance);
			exampleHandling(criteria, example, exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			extendExampleCriteria(criteria, exampleInstance);
			
			criteria.setProjection(Projections.rowCount()); // We just want a row count
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in JPATransactionDetailSaleDAO::()");
		} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error in finding the entities by example");
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}
	
	protected void exampleHandling(Criteria criteria, Example example, SaleTransaction exampleInstance) {
    	// Iterate the properties
    	logger.info("---- JPAGenericDAO.ryanExampleHandling enter -----");
    	for (Method m : getObjectClass().getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && m.getReturnType().equals(String.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					String value = (String) m.invoke(exampleInstance);
	    			if (value != null && value.trim().length()==0) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}
	    			else if (value != null && value.trim().length()>=0) {
	    				value = value.trim();
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			
		    			if(field.equalsIgnoreCase("calculateInd") || field.equalsIgnoreCase("creditInd") || field.equalsIgnoreCase("taxPaidToVendorFlag")  || field.equalsIgnoreCase("exemptInd") ){
		    				//1, y, Y, t, T
		    				//0, n, N, f, F, null
		    				if(value.equals("1")){
		    					criteria.add(Restrictions.or(Restrictions.or(Restrictions.ilike(field, "1", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "Y", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "y", MatchMode.START), Restrictions.ilike(field, "t", MatchMode.START)))), Restrictions.ilike(field, "T", MatchMode.START)));
		    					
	    						example.excludeProperty(field);
		    				}
		    				else if(value.equals("0")){
		    					criteria.add(Restrictions.or(Expression.isNull(field), Restrictions.not(Restrictions.or(Restrictions.or(Restrictions.ilike(field, "1", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "Y", MatchMode.START), Restrictions.or(Restrictions.ilike(field, "y", MatchMode.START), Restrictions.ilike(field, "t", MatchMode.START)))), Restrictions.ilike(field, "T", MatchMode.START)))));
	    						example.excludeProperty(field);
		    				}
		    			}
		    			else if(field.startsWith("shiptoStj") || field.startsWith("shipfromStj") || field.startsWith("ordrorgnStj")  || 
		    					field.startsWith("ordracptStj") || field.startsWith("firstuseStj") || field.startsWith("billtoStj") ){
		    				//For STJ 1 to 5 together, we will cover them below.
		    				example.excludeProperty(field);
		    			}
		    			else {
		    				boolean startWild = value.startsWith("%");
	    					boolean endWild = value.endsWith("%");
	    					if(startWild && endWild){ //Like %12345% or %123%45%
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
	    						example.excludeProperty(field);
	    					}
	    					else if(startWild && !endWild){ //Like $34567 or $34&567
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
	    						example.excludeProperty(field);
	    					}
	    					else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
	    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
	    						example.excludeProperty(field);
	    					}
		    			}
	    			}	
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Long.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Long value = (Long) m.invoke(exampleInstance);
	    			if (value != null && value.longValue()==0l) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}	
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Float.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Float value = (Float) m.invoke(exampleInstance);
	    			if (value != null && value.floatValue()==0.0) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			example.excludeProperty(field);
	    			}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else if (m.getName().startsWith("get") && m.getReturnType().equals(Double.class) && (m.getParameterTypes().length == 0)) {
				try {    			
					Double value = (Double) m.invoke(exampleInstance);
	    			if (value != null && value.doubleValue()==0d) {
		    			String field = Util.lowerCaseFirst(m.getName().substring(3));
		    			
		    			if(field.equalsIgnoreCase("invoiceTaxAmt")){
		    				int x = 0;
		    				x = 0;
		    			}
		    			
		    			example.excludeProperty(field);		    		 
	    			}	   			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    		else{	
    		}
    	}
  	
    	//Search STJ
    	//0003518: Selection Filter - Locations page
    	String stjWhere = "";
    	String stj1Where = "";
    	String stj2Where = "";
    	String stj3Where = "";
    	String stj4Where = "";
    	String stj5Where = "";
    	boolean startWild = false;
    	boolean endWild = false;
    	String stj = "";
    	
    	stj = exampleInstance.getShiptoStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(SHIPTO_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPTO_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPTO_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPTO_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPTO_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(SHIPTO_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPTO_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPTO_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPTO_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPTO_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	

    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getShipfromStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(SHIPFROM_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPFROM_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPFROM_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPFROM_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPFROM_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(SHIPFROM_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(SHIPFROM_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(SHIPFROM_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(SHIPFROM_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(SHIPFROM_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	

    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getOrdrorgnStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(ORDRORGN_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRORGN_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRORGN_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRORGN_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRORGN_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(ORDRORGN_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRORGN_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRORGN_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRORGN_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRORGN_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getOrdracptStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(ORDRACPT_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRACPT_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRACPT_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRACPT_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRACPT_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(ORDRACPT_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(ORDRACPT_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(ORDRACPT_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(ORDRACPT_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(ORDRACPT_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
		
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getFirstuseStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(FIRSTUSE_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(FIRSTUSE_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(FIRSTUSE_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(FIRSTUSE_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(FIRSTUSE_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(FIRSTUSE_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(FIRSTUSE_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(FIRSTUSE_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(FIRSTUSE_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(FIRSTUSE_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}
    	
    	stj1Where = "";
    	stj2Where = "";
    	stj3Where = "";
    	stj4Where = "";
    	stj5Where = "";
    	startWild = false;
    	endWild = false;
    	stj = exampleInstance.getBilltoStj1Name();
    	if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "UPPER(BILLTO_STJ1_NAME) like '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(BILLTO_STJ2_NAME) like '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(BILLTO_STJ3_NAME) like '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(BILLTO_STJ4_NAME) like '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(BILLTO_STJ5_NAME) like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "UPPER(BILLTO_STJ1_NAME) = '"+ stj.toUpperCase() + "' ";
				stj2Where = "UPPER(BILLTO_STJ2_NAME) = '"+ stj.toUpperCase() + "' ";
				stj3Where = "UPPER(BILLTO_STJ3_NAME) = '"+ stj.toUpperCase() + "' ";
				stj4Where = "UPPER(BILLTO_STJ4_NAME) = '"+ stj.toUpperCase() + "' ";
				stj5Where = "UPPER(BILLTO_STJ5_NAME) = '"+ stj.toUpperCase() + "' ";
			}
			
			if(stjWhere.length()>0){
	    		stjWhere = stjWhere + " and ";
	    	}
			stjWhere = stjWhere + "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		}


    	
    	// Add in glDate restrictions
    	String gldateFilter = "";
		if (exampleInstance.getGlDate() == null && exampleInstance.getGlToDate() == null) {
			example.excludeProperty("glDate");
		}
		else{
			example.excludeProperty("glDate");
			
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

			if(exampleInstance.getGlDate()!=null){		
				String effectiveDate = df.format(exampleInstance.getGlDate());
				if(gldateFilter!=null && gldateFilter.length()>0){
					gldateFilter = gldateFilter + " AND ";
				}
				gldateFilter = gldateFilter + " GL_DATE >= to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
			}
			
			if(exampleInstance.getGlToDate()!=null){		
				String effectiveDateThru = df.format(exampleInstance.getGlToDate());
				if(gldateFilter!=null && gldateFilter.length()>0){
					gldateFilter = gldateFilter + " AND ";
				}
				gldateFilter = gldateFilter + " GL_DATE <= to_date('"+effectiveDateThru+"', 'mm/dd/yyyy') ";
			}
		}
		
		String sqlTotal = "";
		if(gldateFilter!=null && gldateFilter.length()>0){	
			if(sqlTotal.length()>0){
				sqlTotal = sqlTotal + " and ";
	    	}
			sqlTotal = sqlTotal + gldateFilter;
		}
		
		if(stjWhere!=null && stjWhere.length()>0){	
			if(sqlTotal.length()>0){
				sqlTotal = sqlTotal + " and ";
	    	}
			sqlTotal = sqlTotal + stjWhere;
		}
		
		if(sqlTotal.length()>0){
			criteria.add(Restrictions.sqlRestriction(sqlTotal));
    	}
    }
	
	@Override
	protected void extendExampleCriteria(Criteria criteria, SaleTransaction exampleInstance) {
		// Hook to provide opportunity to adjust findByExample criteria
		Long id = (exampleInstance == null)? null:exampleInstance.getSaletransId(); 
		if ((id != null) && (id > 0L)) {
			criteria.add(Restrictions.eq("saletransId", id));
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<SaleTransaction> getAllRecords(SaleTransaction exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<SaleTransaction> transactionList =  new ArrayList<SaleTransaction>();

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(SaleTransaction.class);
			Example example = Example.create(exampleInstance);
			exampleHandling(criteria, example, exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			extendExampleCriteria(criteria, exampleInstance);
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            List<SaleTransaction> tList = criteria.list();
            for(SaleTransaction st : tList){
            	SaleTransaction ts = new SaleTransaction();
            	org.springframework.beans.BeanUtils.copyProperties(st, ts);
            	transactionList.add(ts);
            }
            
			LogMemory.executeGC();
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return transactionList;
	}

	public HashMap<String, HashMap<String, String>>  getNexusInfo(Long saleTransactionId) {
		Session localSession = createLocalSession();

		HashMap<String, HashMap<String, String>> result = new HashMap<String, HashMap<String, String>>();
		result.put("Country", new HashMap<String, String>());
		result.get("Country").put("NexusType", "None");
		result.get("Country").put("NexusInd", "None");
		result.put("State", new HashMap<String, String>());
		result.get("State").put("NexusType", "None");
		result.get("State").put("NexusInd", "None");
		result.put("County", new HashMap<String, String>());
		result.get("County").put("NexusType", "None");
		result.get("County").put("NexusInd", "None");
		result.put("City", new HashMap<String, String>());
		result.get("City").put("NexusType", "None");
		result.get("City").put("NexusInd", "None");
		result.put("STJ1", new HashMap<String, String>());
		result.get("STJ1").put("NexusType", "None");
		result.get("STJ1").put("NexusInd", "None");
		result.put("STJ2", new HashMap<String, String>());
		result.get("STJ2").put("NexusType", "None");
		result.get("STJ2").put("NexusInd", "None");
		result.put("STJ3", new HashMap<String, String>());
		result.get("STJ3").put("NexusType", "None");
		result.get("STJ3").put("NexusInd", "None");
		result.put("STJ4", new HashMap<String, String>());
		result.get("STJ4").put("NexusType", "None");
		result.get("STJ4").put("NexusInd", "None");
		result.put("STJ5", new HashMap<String, String>());
		result.get("STJ5").put("NexusType", "None");
		result.get("STJ5").put("NexusInd", "None");
		result.put("STJ6", new HashMap<String, String>());
		result.get("STJ6").put("NexusType", "None");
		result.get("STJ6").put("NexusInd", "None");
		result.put("STJ7", new HashMap<String, String>());
		result.get("STJ7").put("NexusType", "None");
		result.get("STJ7").put("NexusInd", "None");
		result.put("STJ8", new HashMap<String, String>());
		result.get("STJ8").put("NexusType", "None");
		result.get("STJ8").put("NexusInd", "None");
		result.put("STJ9", new HashMap<String, String>());
		result.get("STJ9").put("NexusType", "None");
		result.get("STJ9").put("NexusInd", "None");
		result.put("STJ10", new HashMap<String, String>());
		result.get("STJ10").put("NexusType", "None");
		result.get("STJ10").put("NexusInd", "None");



		try {
			//country
			List<Object[]> countryRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.country_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.country_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.country_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.country_nexusind_code " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (countryRecord!=null && countryRecord.size()>0) {
				if (countryRecord.get(0)[0]!=null)
					result.get("Country").put("NexusType", (String)countryRecord.get(0)[0]);

				if (countryRecord.get(0)[1]!=null)
					result.get("Country").put("NexusInd", (String)countryRecord.get(0)[1]);
			}

			//state
			List<Object[]> stateRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.state_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.state_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.state_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.state_nexusind_code " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stateRecord!=null && stateRecord.size()>0) {
				if (stateRecord.get(0)[0]!=null)
					result.get("State").put("NexusType", (String)stateRecord.get(0)[0]);

				if (stateRecord.get(0)[1]!=null)
					result.get("State").put("NexusInd", (String)stateRecord.get(0)[1]);
			}

			//county
			List<Object[]> countyRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.county_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.county_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.county_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.county_nexusind_code " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (countyRecord!=null && countyRecord.size()>0) {
				if (countyRecord.get(0)[0]!=null)
					result.get("County").put("NexusType", (String)countyRecord.get(0)[0]);

				if (countyRecord.get(0)[1]!=null)
					result.get("County").put("NexusInd", (String)countyRecord.get(0)[1]);
			}

			//city
			List<Object[]> cityRecord = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.city_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.city_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.city_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=j.city_nexusind_code " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (cityRecord!=null && cityRecord.size()>0) {
				if (cityRecord.get(0)[0]!=null)
					result.get("City").put("NexusType", (String)cityRecord.get(0)[0]);

				if (cityRecord.get(0)[1]!=null)
					result.get("City").put("NexusInd", (String)cityRecord.get(0)[1]);
			}

			//stj1
			List<Object[]> stj1Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj1_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj1_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj1_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code= " +
					" CASE st.stj1_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END" +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj1Record!=null && stj1Record.size()>0) {
				if (stj1Record.get(0)[0]!=null)
					result.get("STJ1").put("NexusType", (String)stj1Record.get(0)[0]);

				if (stj1Record.get(0)[1]!=null)
					result.get("STJ1").put("NexusInd", (String)stj1Record.get(0)[1]);
			}

			//stj2
			List<Object[]> stj2Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj2_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj2_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj2_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code= " +
					" CASE st.stj2_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj2Record!=null && stj2Record.size()>0) {
				if (stj2Record.get(0)[0]!=null)
					result.get("STJ2").put("NexusType", (String)stj2Record.get(0)[0]);

				if (stj2Record.get(0)[1]!=null)
					result.get("STJ2").put("NexusInd", (String)stj2Record.get(0)[1]);
			}

			//stj3
			List<Object[]> stj3Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj3_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj3_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj3_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj3_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj3Record!=null && stj3Record.size()>0) {
				if (stj3Record.get(0)[0]!=null)
					result.get("STJ3").put("NexusType", (String)stj3Record.get(0)[0]);

				if (stj3Record.get(0)[1]!=null)
					result.get("STJ3").put("NexusInd", (String)stj3Record.get(0)[1]);
			}

			//stj4
			List<Object[]> stj4Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj4_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj4_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj4_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj4_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj4Record!=null && stj4Record.size()>0) {
				if (stj4Record.get(0)[0]!=null)
					result.get("STJ4").put("NexusType", (String)stj4Record.get(0)[0]);

				if (stj4Record.get(0)[1]!=null)
					result.get("STJ4").put("NexusInd", (String)stj4Record.get(0)[1]);
			}

			//stj5
			List<Object[]> stj5Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj5_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj5_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj5_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj5_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj5Record!=null && stj5Record.size()>0) {
				if (stj5Record.get(0)[0]!=null)
					result.get("STJ5").put("NexusType", (String)stj5Record.get(0)[0]);

				if (stj5Record.get(0)[1]!=null)
					result.get("STJ5").put("NexusInd", (String)stj5Record.get(0)[1]);
			}


			//stj6
			List<Object[]> stj6Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj6_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj6_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj6_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code= " +
					" CASE st.stj6_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj6Record!=null && stj6Record.size()>0) {
				if (stj6Record.get(0)[0]!=null)
					result.get("STJ6").put("NexusType", (String)stj6Record.get(0)[0]);

				if (stj6Record.get(0)[1]!=null)
					result.get("STJ6").put("NexusInd", (String)stj6Record.get(0)[1]);
			}

			//stj7
			List<Object[]> stj7Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj7_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj7_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj7_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj7_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj7Record!=null && stj7Record.size()>0) {
				if (stj7Record.get(0)[0]!=null)
					result.get("STJ7").put("NexusType", (String)stj7Record.get(0)[0]);

				if (stj7Record.get(0)[1]!=null)
					result.get("STJ7").put("NexusInd", (String)stj7Record.get(0)[1]);
			}

			//stj8
			List<Object[]> stj8Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj8_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj8_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj8_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj8_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj8Record!=null && stj8Record.size()>0) {
				if (stj8Record.get(0)[0]!=null)
					result.get("STJ8").put("NexusType", (String)stj8Record.get(0)[0]);

				if (stj8Record.get(0)[1]!=null)
					result.get("STJ8").put("NexusInd", (String)stj8Record.get(0)[1]);
			}

			//stj9
			List<Object[]> stj9Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj9_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj9_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj9_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj9_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj9Record!=null && stj9Record.size()>0) {
				if (stj9Record.get(0)[0]!=null)
					result.get("STJ9").put("NexusType", (String)stj9Record.get(0)[0]);

				if (stj9Record.get(0)[1]!=null)
					result.get("STJ9").put("NexusInd", (String)stj9Record.get(0)[1]);
			}

			//stj10
			List<Object[]> stj10Record = localSession.createSQLQuery("SELECT  nexustype.description nexus_type, nexusind.description nexus_ind from tb_saletrans st " +
					" LEFT OUTER JOIN tb_nexus_def_detail ndd ON ndd.nexus_def_detail_id=st.stj10_nexus_def_detail_id AND ndd.active_flag='1' " +
					" LEFT OUTER JOIN tb_list_code taxtype ON taxtype.code_type_code='TAXTYPE' AND taxtype.code_code=st.stj10_taxtype_code " +
					" LEFT OUTER JOIN tb_list_code nexustype ON nexustype.code_type_code='NEXUSTYPE' AND nexustype.code_code=ndd.nexus_type_code " +
					" LEFT OUTER JOIN tb_jurisdiction j ON j.jurisdiction_id = st.stj10_situs_jur_id " +
					" LEFT OUTER JOIN tb_list_code nexusind ON nexusind.code_type_code='NEXUSIND' AND nexusind.code_code=" +
					" CASE st.stj10_name " +
					"     WHEN j.stj1_name THEN j.stj1_nexusind_code " +
					"     WHEN j.stj2_name THEN j.stj2_nexusind_code " +
					"     WHEN j.stj3_name THEN j.stj3_nexusind_code " +
					"     WHEN j.stj4_name THEN j.stj4_nexusind_code " +
					"     WHEN j.stj5_name THEN j.stj5_nexusind_code " +
					"   END " +
					" WHERE st.saletrans_Id IN (:saletransId)").setParameter("saletransId", saleTransactionId).list();
			if (stj10Record!=null && stj10Record.size()>0) {
				if (stj10Record.get(0)[0]!=null)
					result.get("STJ10").put("NexusType", (String)stj10Record.get(0)[0]);

				if (stj10Record.get(0)[1]!=null)
					result.get("STJ10").put("NexusInd", (String)stj10Record.get(0)[1]);
			}


			int i = 0;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		} finally {
			closeLocalSession(localSession);
		}
		return result;
	}

}