package com.ncsts.dao;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LogMemory;
import com.ncsts.common.SQLQuery;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.FisYear;
import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.view.bean.EditAction;
import com.ncsts.view.util.SqlHelper;



public class JPAFisYearDAO extends JPAGenericDAO<FisYear, String> implements FisYearDAO {

	@PersistenceContext 
	private EntityManager entityManager;
	
	public Session getCurrentSession(){
		return createLocalSession();
	}
	@SuppressWarnings("unchecked")
	public Long count(FisYear exampleInstance) {
		Session localSession = this.createLocalSession();
		Long returnLong = 0L;
		
		try {
			
             String sqlQuery = SQLQuery.TB_FISCAL_YEAR_COUNT;
			
			String extendWhere = getWhereClause(exampleInstance);
			sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}
	
	
	
	private String getWhereClause(FisYear exampleInstance) {
		
		if(exampleInstance==null){
			return "";
		}
		
		StringBuffer whereClauseExt = new StringBuffer();
		if(exampleInstance.getFiscalYear()!=null){
			appendANDToWhere(whereClauseExt, "FISCAL_YEAR >= " + exampleInstance.getFiscalYear().toString());	
		}
		if(exampleInstance.getCloseFlag()!=null && exampleInstance.getCloseFlag().length()>0){
			appendANDToWhere(whereClauseExt, "CLOSE_FLAG = " + exampleInstance.getCloseFlag().toString());	
		}
		
	return  whereClauseExt.toString();
	}
	
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<FisYear> find(FisYear exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {

		List<FisYear> result = null;
		Connection con = null;
		try {
			String whereClauseToken = getWhereClause(exampleInstance);
			String strOrderBy = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			if (con != null) {	
				String sqlQuery = SQLQuery.TB_FISCAL_YEAR_MATRIX_MASTER_LIST;
				
				if(strOrderBy==null || strOrderBy.length()==0){
					strOrderBy = "FISCAL_YEAR asc";
				}
				
				sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
				sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);
				sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
				sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));

				List<FisYear> returnList=null;
				Query q = entityManager.createNativeQuery(sqlQuery);

				List<Object> list= q.getResultList();
				Iterator<Object> iter=list.iterator();
				if(list!=null){
					returnList = getFiscalYearMatrixList(iter);
				}
				return returnList;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		LogMemory.executeGC();
		
		return result;
	}

	private List<FisYear> getFiscalYearMatrixList(Iterator<Object> iter){
		List<FisYear> returnList = new ArrayList<FisYear>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			FisYear fiscalYearMatrix=new FisYear();
            BigDecimal is = (BigDecimal)objarray[0];
            BigDecimal is2 = (BigDecimal)objarray[1];
            if (is2!=null){
            fiscalYearMatrix.setId(is.toString());
           	fiscalYearMatrix.setFiscalYear(is2.longValue());//Row number
           	if(getCloseFlag(is2.longValue()))
           		fiscalYearMatrix.setCheckCount(false);
           	else
           		fiscalYearMatrix.setCheckCount(true);
            }
          
             returnList.add(fiscalYearMatrix);                
		}
		
		return returnList;
	}
	
private Map<String,String> columnMapping = null;
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>(); 	
			columnMapping.put("fiscalYear".toLowerCase(), "FISCAL_YEAR"); 
		}
		
		String columnName = columnMapping.get(name.toLowerCase());
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}

	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			 }
		}
		
		return orderByToken.toString();
    }


	public Long count(Criteria criteria) {
		criteria.setProjection( Projections.distinct( Projections.projectionList()
	    .add(Projections.rowCount()).add(Projections.property("fiscalYear").as("fiscalYear"))));
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<FisYear> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<FisYear> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<FisYear> returnCust = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCust;
	}
	
	public FisYear find(FisYear exampleInstance) {
		List<FisYear> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}


    @SuppressWarnings("unchecked")
	@Transactional
	public List<FisYear> getAllYearPeriod(Long fisYearId) throws DataAccessException {	
		List<FisYear> returnList = new ArrayList<FisYear>();
		
		Session localSession = this.createLocalSession();
		try {
			Criteria criteria = localSession.createCriteria(FisYear.class);
			criteria.add(Restrictions.eq("fiscalYear",fisYearId));
			criteria.addOrder(Order.asc("yearPeriod") );
			
		    returnList = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return returnList;
	}
	
    @SuppressWarnings("unchecked")
   	@Transactional
   	public List<FisYear> getAllPeriodDesc(Long fisYear) throws DataAccessException {	
   		List<FisYear> returnList = new ArrayList<FisYear>();
   		
   		Session localSession = this.createLocalSession();
   		try {
   			Criteria criteria = localSession.createCriteria(FisYear.class);
   			
   			criteria.add(Restrictions.eq("fiscalYear",fisYear));
   			criteria.addOrder(Order.asc("yearPeriod") );
   		    returnList = criteria.list();
   		} catch (Exception e) {
   			e.printStackTrace();
   		} finally {
   			closeLocalSession(localSession);
   			localSession=null;
   		}

   		return returnList;
   	}
   	
    @SuppressWarnings("unchecked")
   	@Transactional
   	public List<FisYear> getSelectedPeriod(String fisYearId) throws DataAccessException {	
   		List<FisYear> returnList = new ArrayList<FisYear>();
   		
   		Session localSession = this.createLocalSession();
   		try {
   			Criteria criteria = localSession.createCriteria(FisYear.class);
   			
   			criteria.add(Restrictions.ilike("yearPeriod",fisYearId+"%"));
   			criteria.addOrder(Order.asc("yearPeriod") );
   			
   		    returnList = criteria.list();
   		} catch (Exception e) {
   			e.printStackTrace();
   		} finally {
   			closeLocalSession(localSession);
   			localSession=null;
   		}

   		return returnList;
   	}
    @SuppressWarnings("unchecked")
	public List<FisYear> getAllPeriod() throws DataAccessException {	
   		List<FisYear> returnList = new ArrayList<FisYear>();
   		
   		Session localSession = this.createLocalSession();
   		try {
   			Criteria criteria = localSession.createCriteria(FisYear.class);
   			
   			criteria.add(Restrictions.eq("closeFlag","0"));
   			criteria.addOrder(Order.asc("yearPeriod"));
   			returnList = criteria.list();
   		} catch (Exception e) {
   			e.printStackTrace();
   		} finally {
   			closeLocalSession(localSession);
   			localSession=null;
   		}

   		return returnList;
   	}
    
	@Transactional(readOnly=true)
	public boolean getCloseFlag(Long fisYearid) {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		String baseQuery = "Select count(*) from FisYear td where td.fiscalYear = :fiscalYear and td.closeFlag= :closeflag";
		
		Query query = entityManager.createQuery(baseQuery);
		query.setParameter("fiscalYear", fisYearid);
		query.setParameter("closeflag", "0");
		return ((Long) query.getSingleResult() > 0L);
	}

	@Transactional
	public void updateFisYear(Long fisYear, List<FisYear> fisYearList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.createQuery("delete FisYear where fiscalYear = :fiscalYear ").setParameter("fiscalYear", fisYear).executeUpdate();
			
			for (FisYear aFisYear : fisYearList) {
				session.save(aFisYear);
			}			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
}
