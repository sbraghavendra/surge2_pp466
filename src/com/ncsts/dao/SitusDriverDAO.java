package com.ncsts.dao;

import java.util.List;

import com.ncsts.dto.SitusDriverSitusDriverDetailDTO;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverDetail;

public interface SitusDriverDAO extends GenericDAO<SitusDriver, Long> {
	//public abstract TaxHoliday findByTaxHolidayCode(String taxHolidayCode) throws DataAccessException;
	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<SitusDriver> find(Criteria criteria);
	public abstract List<SitusDriver> find(DetachedCriteria criteria);
	public abstract SitusDriver find(SitusDriver exampleInstance);
	public abstract List<SitusDriver> find(SitusDriver exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	
	public Long count(SitusDriver exampleInstance);
	public List<SitusDriverDetail> findAllSitusDriverDetailBySitusDriverId(Long situsDriverId) throws DataAccessException;
	public void addSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException;
	public void updateSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException;
	public void deleteSitusDriver(SitusDriver situsDriver) throws DataAccessException;
	public List<String> getDistinctSitusDriverColumn(String distinctColumn, String whereClause) throws DataAccessException;
	List<SitusDriverSitusDriverDetailDTO> getSitusDriverDetailWithSitusInfo(String transactionTypeCode, String ratetypeCode, String methodDeliveryCode) throws DataAccessException;
	//public abstract List<TaxHoliday> findByTaxHoliday(TaxHoliday taxHoliday);
	//public Long count(TaxHoliday exampleInstance);
	//public abstract List<TaxHoliday> getAllTaxHolidayData() throws DataAccessException;   
	//public abstract void update(TaxHoliday taxHoliday) throws DataAccessException;
	//public abstract void remove(String taxHolidayCode) throws DataAccessException;

	//public TaxHoliday getTaxHolidayData(String taxHolidayCode) throws DataAccessException;//Method added by Krishna

  	//public abstract Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz);
  	
  	//public List<TaxHolidayDetail> findTaxHolidayDetailFromTaxCode(String country, String state, String taxCode, String description, String county, String city) throws DataAccessException;
  	//public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String taxCode, String county, String city) throws DataAccessException;
  	//public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String county, String city) throws DataAccessException;
	//public List<TaxHolidayDetail> findTaxHolidayDetailByTaxHolidayCode(String taxHolidayCode) throws DataAccessException;
	//public void executeTaxHolidayDetails(String taxHolidayCode) throws DataAccessException;
	//public void addTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList) throws DataAccessException;
	//public void updateTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList) throws DataAccessException;
	//public void updateTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;
	//public void deleteTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;

}
