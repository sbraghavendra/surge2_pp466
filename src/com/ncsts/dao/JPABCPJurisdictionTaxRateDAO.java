/*
 * Author: Jim M. Wilson
 * Created: Aug 23, 2008
 * $Date: 2008-08-28 12:48:23 -0500 (Thu, 28 Aug 2008) $: - $Revision: 1998 $: 
 */

package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRatePK;
import com.ncsts.domain.BatchMaintenance;

/**
 * 
 */
public class JPABCPJurisdictionTaxRateDAO extends JPAGenericDAO<BCPJurisdictionTaxRate, BCPJurisdictionTaxRatePK> 
implements BCPJurisdictionTaxRateDAO {

	@SuppressWarnings("unchecked")
	public List<BCPJurisdictionTaxRate> getPage(BatchMaintenance batch, int firstRow, int maxResults)
			throws DataAccessException {

		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPJurisdictionTaxRate.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
//		criteria.addOrder(Order.asc("id.geocode"));
//		criteria.addOrder(Order.asc("id.zip"));
		try {
//			List<BCPJurisdictionTaxRate> list = getJpaTemplate().find("select e from BCPJurisdictionTaxRate e where e.id.batchId = ?1 order by e.id.batchId desc",
//					batch.getBatchId());
			
			List<BCPJurisdictionTaxRate> returnBCPJurisdictionTaxRate = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPJurisdictionTaxRate;
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPJurisdictionTaxRate>();
		}
	}

	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPJurisdictionTaxRate.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
		
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}
}
