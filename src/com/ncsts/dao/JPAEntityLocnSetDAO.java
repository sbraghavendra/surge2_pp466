package com.ncsts.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.ProruleAudit;

public class JPAEntityLocnSetDAO extends JPAGenericDAO<EntityLocnSet,Long> implements EntityLocnSetDAO {
	  
	public Long count(Long entityId) throws DataAccessException {
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createQuery(" select count(*) from EntityLocnSet entityLocnSet where entityLocnSet.entityId = ?");
	  	query.setParameter(1,entityId );
	    Long count = (Long) query.getSingleResult();
		return count;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityLocnSet> findAllEntityLocnSets() throws DataAccessException {
	    return getJpaTemplate().find(" select entityLocnSet from EntityLocnSet entityLocnSet");	  
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityLocnSet> findAllEntityLocnSetsByEntityId(Long entityId) throws DataAccessException {
		return getJpaTemplate().find(" select entityLocnSet from EntityLocnSet entityLocnSet where entityLocnSet.entityId = ? order by entityLocnSet.locnSetCode", entityId);	  
	}
	  
	@SuppressWarnings("deprecation")
	@Transactional
	public Long persist(EntityLocnSet entityLocnSet) throws DataAccessException {
		getJpaTemplate().persist(entityLocnSet);
		getJpaTemplate().flush();
		Long entityLocnSetId = entityLocnSet.getEntityLocnSetId();
	
		return entityLocnSetId;
	}
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long id) throws DataAccessException {
		EntityLocnSet entityLocnSet = getJpaTemplate().find(EntityLocnSet.class, id);
		if( entityLocnSet != null){
			getJpaTemplate().remove(entityLocnSet);
		}else{
			logger.warn("EntityLocnSet Object not found for deletion");
		}
	} 
	  
	public void saveOrUpdate (EntityLocnSet entityLocnSet) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.update(entityLocnSet);
		t.commit();
		closeLocalSession(session);
		session=null;		
	}
	
	@Transactional
	public void deleteEntity(Long entityId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			//remove(TB_ENTITY);
			String hqlDelete = "DELETE TB_ENTITY_LOCN_SET WHERE ENTITY_ID = " + entityId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Delete all existing TB_NEXUS_DEF
			//hqlDelete = "DELETE FROM TB_NEXUS_DEF WHERE ENTITY_ID = " + entityId.intValue();
			//query = session.createSQLQuery(hqlDelete);
			//query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute deleteTEntity: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	  
	
	 public EntityLocnSet getEntityLocationforView(Long id){

		 String sqlQuery=
					"SELECT  tb_entity_locn_set.locn_set_code LocnSetCode, tb_entity_locn_set.name LocName, tb_entity_locn_set_dtl.description LocNameDesc  "+
	   "FROM TB_BILLTRANS "+
	   "LEFT OUTER JOIN tb_entity_locn_set_dtl ON tb_entity_locn_set_dtl.entity_locn_set_dtl_id = TB_BILLTRANS.ENTITY_LOCN_SET_DTL_ID "+
	   "LEFT OUTER JOIN tb_entity_locn_set ON tb_entity_locn_set.entity_locn_set_id = tb_entity_locn_set_dtl.entity_locn_set_id "+
	   "LEFT OUTER JOIN tb_list_code TRANSTYPE ON TRANSTYPE.code_type_code = 'TRANSTYPE' AND TRANSTYPE.code_code = TB_BILLTRANS.transaction_type_code "+
	   "WHERE TB_BILLTRANS.billtrans_id ="+ id +" ";
	   
			EntityLocnSet result = null;
			Query q = entityManager.createNativeQuery(sqlQuery);

			List<Object> list= q.getResultList();
			Iterator<Object> iter=list.iterator();
			if(list!=null){
				result = new EntityLocnSet();
			
				try{
					long pseudoId = 1;
					while(iter.hasNext()){
						Object[] objarray= (Object[])iter.next();
						
							
						String LocnSetCode=(String)objarray[0];
						result.setLocnSetCode(LocnSetCode);
						
						String LocName=(String)objarray[1];
						result.setLocName(LocName);
						
						String LocNameDesc = (String) objarray[2];//First column
						result.setLocNameDesc(LocNameDesc);
						
						
						
			                            
					}
				}catch(Exception e){
					
				}
			}
			
			
			return result;
		  
	  }
	
}


