package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.domain.UserMenuEx;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.UserDTO;

/**
 * @author Paul Govindan
 *
 */
public class JPAUserEntityDAO extends JpaDaoSupport implements UserEntityDAO {
    private static final transient Log log = LogFactory.getLog(JpaDaoSupport.class);

    protected Session createLocalSession() {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	Session localsession = (entityManager instanceof HibernateEntityManager) 
    	      ? ((HibernateEntityManager) entityManager).getSession()
    	      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
      
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
        
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }

	@SuppressWarnings("unchecked")
	public List<UserEntity> findByUserEntity(UserEntity userEntity, int count, String... excludeProperty) 
    throws DataAccessException {
		List<UserEntity> list = null;
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(UserEntity.class);
            if (count > 0) {
            	criteria.setMaxResults(count);
            }
            Example example = Example.create(userEntity);
            for (String exclude : excludeProperty) {
                example.excludeProperty(exclude);
            }
            criteria.add(example);
            list = criteria.list();
        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the entities by example");
        }
        
        closeLocalSession(session);
        session=null;
        
        return list; 		
	}
	
    @SuppressWarnings("unchecked")
    @Transactional
	public List<UserEntity> findAll() throws DataAccessException {
	    return getJpaTemplate().find(" select userEntity from UserEntity userEntity ");	    	
    }
    
    @SuppressWarnings("unchecked")
    @Transactional
	public List<UserEntity> findByUserCode(String userCode) throws DataAccessException {
	    return getJpaTemplate().find("select userEntity from UserEntity userEntity where userEntity.id.userCode = ?" , userCode);	    	
    }
    
    @SuppressWarnings("unchecked")
    @Transactional
	public List<Object[]> findByRoleCode(String roleCode) throws DataAccessException {
		logger.debug("inside the JPA     ++++");
    	EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createQuery("select user.userCode, user.userName, entity from User user , EntityItem entity , UserEntity userEntity where "+ 
											"user.userCode = userEntity.id.userCode and "+ 
											"entity.entityId = userEntity.id.entityId and "+
											"userEntity.roleCode = ?");
		q.setParameter(1, roleCode);
		return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> findForUserCode(String userCode) throws DataAccessException {
		logger.debug("inside the JPA     ++++");
    	EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createQuery("select user.userCode, user.userName, entity , entityLevel, role.roleCode, "+
											"role.roleName from Role role, EntityLevel entityLevel, "+
											"User user , EntityItem entity , UserEntity userEntity where " +
											"user.userCode = userEntity.id.userCode and "  +
											"entity.entityId = userEntity.id.entityId and " +
											"entityLevel.entityLevelId = entity.entityLevelId and "+
											"role.roleCode = userEntity.roleCode and "+
											"userEntity.id.userCode = ?");
		q.setParameter(1, userCode);
		return q.getResultList();
    }
    
    
    @Transactional(propagation = Propagation.NEVER, readOnly = false) 
	public void saveOrUpdate(UserEntity userEntity) throws DataAccessException {	
    	  Session session = createLocalSession();
	      Transaction t = session.beginTransaction();
	      t.begin();
	      session.saveOrUpdate(userEntity);
	      t.commit();

	      closeLocalSession(session);
	      session=null;
	  }	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserEntity userEntity) throws DataAccessException {
//		String sql = "select userEntity from UserEntity userEntity JOIN userEntity.userEntityPK pk WHERE pk.entityId = ?1 AND pk.userCode = ?2";
//		UserEntity userEntity2 = null;
		
//		//List<UserEntity> userEntities = getJpaTemplate().find(sql, userEntity.getUserEntityPK().getEntityId(), userEntity.getUserEntityPK().getUserCode());
//		if(!userEntities.isEmpty())
//		 userEntity2 = userEntities.get(0);
//		System.out.println(userEntity.getUserMenuEx());
//		System.out.println(userEntity.getUserEntityPK().getUserCode());
//		System.out.println(userEntity.getUserEntityPK().getEntityId());
		UserEntity userEntity2 = getJpaTemplate().find(UserEntity.class, userEntity.getUserEntityPK());
	    try {
	    	Set<UserMenuEx> test = userEntity2.getUserMenuEx();
	    	logger.info("Current Exceptions: " + test);
	    } catch (Exception e) {
	    	System.out.println("invalid user menu ex");
	    	userEntity2.setUserMenuEx(new HashSet<UserMenuEx>());
	    }
		  if( userEntity != null){
		      getJpaTemplate().remove(userEntity2);
		  }else{
		      logger.info("UserEntity Object not found for deletion");
		  }

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public EntityItemDTO getEntityItemDTO(UserDTO userDTO) throws DataAccessException {
		 Session session = createLocalSession();
		 // get latest entity in use by user
		 org.hibernate.Query query = session.createQuery("select u from User u where u.userCode = :user");
		 query.setParameter("user", userDTO.getUserCode());
		 User user = (User)query.uniqueResult();
	 
		 String sql = "select l.trans_dtl_column_name, e.entity_id, e.entity_code, e.entity_name from tb_entity e" +
		 " left join tb_entity_level l using (entity_level_id) " +
		 " left join tb_user_entity ue on (ue.entity_id = e.entity_id) " +
		 " where (ue.entity_id = :id) and (ue.user_code=:user)";
		 query = session.createSQLQuery(sql);
		 query.setParameter("id", user.getLastUsedEntityId());
		 query.setParameter("user", userDTO.getUserCode());
		 Object result[] = (Object[])query.uniqueResult();
		 if(result==null){
			 return null;
		 }
		 EntityItemDTO ei = new EntityItemDTO();
		 if(result!=null){
			 ei.setEntityColumn((String)result[0]);
			 ei.setEntityId(((BigDecimal)result[1]).longValue());
			 ei.setEntityCode((String)result[2]);
			 ei.setEntityName((String)result[3]);
		 }
		 
		 closeLocalSession(session);
		 session=null;
	
		 return ei;
	 }
    
	@Transactional
	public UserEntity find(Long id) throws DataAccessException {
		List<UserEntity> list = getJpaTemplate().find("select ue from UserEntity ue where ue.id.entityId = ?1",	id);
		if ((list == null) || list.size() < 1) { 
			return null;
		} else {
			return list.get(0);
		}
	}
	
	@Transactional
	public UserEntity findById(UserEntityPK userEntityPK) throws DataAccessException {
	    return getJpaTemplate().find(UserEntity.class, userEntityPK);
	  }
	
  /*
     * @see UserEntityDAO interface.
     * TODO: Add sorting
     /
    @SuppressWarnings("unchecked")
	public List<UserEntity> getPage(int startIndex, int endIndex){
        int maxResults = endIndex - startIndex;
        maxResults--; // Exclude last row (endIndex)
        Query query = getJpaTemplate().getEntityManagerFactory().createEntityManager().createQuery("from UserEntity entity")
            .setFirstResult(startIndex)
            .setMaxResults(maxResults); 
        List<UserEntity> results = query.getResultList();
        logger.debug("RETURNING results: " + results); 
        return results;
    }
    
    public UserEntity getById(UserEntityPK pk){
        return getJpaTemplate().find(UserEntity.class, pk);
    }
	
    public Long getTotalCount(){
        // JPA QL isn't working with count + composite key
//        Query query = getJpaTemplate().getEntityManagerFactory().createEntityManager()
//            .createQuery("select count(e) from UserEntity e");
//        Long count = (Long)query.getSingleResult();
        
        // Since the HQL count() doesn't seem to like composite keys, we use SQL
        BigDecimal count = (BigDecimal) getJpaTemplate().getEntityManagerFactory().createEntityManager()
            .createNativeQuery("select count(ent.user_code) from TB_USER_ENTITY ent").getSingleResult();
        logger.debug("RETURNING count: " + count); 
        return count.longValue();
    }
    
    public void persist(UserEntity entity){
        getJpaTemplate().persist(entity);
    }
    */

	@SuppressWarnings("unchecked")
	@Transactional
	public List<UserEntity> findByRole(String role) throws DataAccessException {
		List<UserEntity> list =getJpaTemplate().find("select userEntity from UserEntity userEntity where userEntity.roleCode = ?" , role);
		 return list;	    	
	}
}
