/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.FileReaderUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustEntityPK;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.UserEntity;
import com.ncsts.dto.BatchDTO;
import com.ncsts.view.util.HibernateUtils;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;
import com.seconddecimal.billing.query.SqlQuery;
import com.seconddecimal.billing.util.DateUtils;


public class JPACustDAO extends JPAGenericDAO<Cust, Long> implements CustDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	@SuppressWarnings("unused")
	private CustDAO custDAO;
	
	public Session getCurrentSession(){
		return createLocalSession();
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public Cust getCustByNumber(String custNbr) throws DataAccessException {
		List<Cust> list = getJpaTemplate().find("select cust from Cust cust where upper(cust.custNbr) = ?" , custNbr.toUpperCase());
		
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		
		return  null;
	}

	
	@Transactional
	public void deleteCustEntityList(Cust updateCust) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlDelete = "";
			
			int deleteCount = 0;
			
			//TB_CUST_CERT by CUST_LOCN_ID
			hqlDelete = "delete from TB_CUST_CERT where CUST_LOCN_ID in (select CUST_LOCN_ID from TB_CUST_LOCN where CUST_ID = "+updateCust.getCustId().intValue()+") ";
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			//TB_CUST_LOCN_EX by CUST_LOCN_ID
			hqlDelete = "delete from TB_CUST_LOCN_EX where CUST_LOCN_ID in (select CUST_LOCN_ID from TB_CUST_LOCN where CUST_ID = "+updateCust.getCustId().intValue()+") ";
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			// First delete all entities, TB_CUST_ENTITY by CUST_ID
			hqlDelete = "delete TB_CUST_ENTITY WHERE CUST_ID = "+updateCust.getCustId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			// First delete all locations, TB_CUST_LOCN by CUST_ID
			hqlDelete = "delete TB_CUST_LOCN WHERE CUST_ID = "+updateCust.getCustId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();


			Cust aCust = findById(updateCust.getId());
			if(aCust==null){
				return;
			}
			
			session.update(updateCust);
			session.delete(updateCust);
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			Cust aCust = findById(updateCust.getId());
			if(aCust==null){
				return;
			}
			
			aCust.setCustNbr(updateCust.getCustNbr());
			aCust.setCustName(updateCust.getCustName());
			aCust.setActiveFlag(updateCust.getActiveFlag());
			session.update(updateCust);
			
			// First delete all entity
			String hqlDelete = "delete TB_CUST_ENTITY WHERE CUST_ID = "+updateCust.getCustId().intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Now do an insert for each
			query = session.createSQLQuery("INSERT INTO TB_CUST_ENTITY (CUST_ID, ENTITY_ID) VALUES (:custId, :entityid)");
			for (CustEntity aCustEntity : updateCustEntityList) {
				query.setParameter("custId", updateCust.getCustId());
				query.setParameter("entityid", aCustEntity.getId().getEntityId());
				query.executeUpdate();
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(updateCust);
			session.flush();

			query = session.createSQLQuery("INSERT INTO TB_CUST_ENTITY (CUST_ID, ENTITY_ID) VALUES (:custId, :entityid)");
			for (CustEntity aCustEntity : updateCustEntityList) {
				query.setParameter("custId", updateCust.getCustId());
				query.setParameter("entityid", aCustEntity.getId().getEntityId());
				query.executeUpdate();
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void addCustEntity(Long custId, Long entityId) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			query = session.createSQLQuery("INSERT INTO TB_CUST_ENTITY (CUST_ID, ENTITY_ID) VALUES (:custId, :entityid)");
			query.setParameter("custId", custId);
			query.setParameter("entityid", entityId);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteCustEntity(Long custId, Long entityId) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			query = session.createSQLQuery("delete from TB_CUST_ENTITY where CUST_ID = :custId and ENTITY_ID = :entityid");
			query.setParameter("custId", custId);
			query.setParameter("entityid", entityId);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	public Long count(Cust exampleInstance) {
		Session localSession = this.createLocalSession();
		Long returnLong = 0L;
		try {
			Criteria criteria = localSession.createCriteria(Cust.class);
			criteria.add(Restrictions.sqlRestriction(getWhereClause(exampleInstance)));
	
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}

	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<Cust> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<Cust> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<Cust> returnCust = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCust;
	}
	
	public Cust find(Cust exampleInstance) {
		List<Cust> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException {	
		List<CustEntity> returnList = new ArrayList<CustEntity>();
		try{
			Query q = entityManager.createNativeQuery("select ENTITY_ID from TB_CUST_ENTITY where CUST_ID = " + custId.intValue());
			
			List<Object> list= q.getResultList();
			Iterator<Object> iter=list.iterator();
			if(list!=null){
				while(iter.hasNext()){
					long entityId = ((Number)iter.next()).longValue(); 
					CustEntity aCustEntity = new CustEntity();
		            CustEntityPK aKey = new CustEntityPK(custId, entityId);		     
		            aCustEntity.setId(aKey);
		           
		            returnList.add(aCustEntity);        
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return returnList;
	}
	
	@Transactional
	public CustEntity findCustEntity(Long custId, Long entityId) throws DataAccessException {	
		CustEntity returnCustEntity = null;
		try{
			Query q = entityManager.createNativeQuery("select ENTITY_ID from TB_CUST_ENTITY where CUST_ID = " + custId.intValue() + " and ENTITY_ID = " + entityId.intValue());     
			
			List<Object> list= q.getResultList();
			Iterator<Object> iter=list.iterator();
			if(list!=null){
				if(iter.hasNext()){
					long retEntityId = ((Number)iter.next()).longValue(); 
					CustEntity aCustEntity = new CustEntity();
		            CustEntityPK aKey = new CustEntityPK(custId, retEntityId);		     
		            aCustEntity.setId(aKey);
		            
		            returnCustEntity = aCustEntity;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return returnCustEntity;
	}
	
	@Transactional(readOnly = false)
	public boolean isAllowDeleteCustomer(String custNbr) throws DataAccessException {
	
		Long returnLong = 1L;
		try {
			String sqlQuery = "select count(*) count FROM tb_billtrans WHERE cust_nbr = '" + custNbr + "'";			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return (returnLong==0L);
	}
	
	@Transactional(readOnly = false)
	public void disableActiveCustomer(String custNbr) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			// Now do an insert for each user
			query = session.createSQLQuery("Update tb_cust SET active_flag = '0' WHERE cust_nbr = '" + custNbr + "'");
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cust> findAllCust(Cust exampleInstance) throws DataAccessException {
		Session localSession = this.createLocalSession();
		
		List<Cust> list = new ArrayList<Cust>();
		try { 
			Criteria criteria = localSession.createCriteria(Cust.class);
			criteria.add(Restrictions.sqlRestriction(getWhereClause(exampleInstance)));
			criteria.setFirstResult(0);
			criteria.setMaxResults(10000);
			criteria.addOrder( Order.asc("custNbr") );
		
            list = criteria.list();
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		} 
		finally {
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return new ArrayList<Cust>();
	}
	
	private String getWhereClause(Cust exampleInstance){
		//To include entity id criteria
		//Sample: select * from TB_CUST where CUST_ID in (select distinct TB_CUST.CUST_ID from TB_CUST inner join TB_CUST_ENTITY on  TB_CUST.CUST_ID = TB_CUST_ENTITY.CUST_ID where (1=1) and TB_CUST_ENTITY.ENTITY_ID = 51 )
		
		String whereClause = "(1=1) ";
		
		//For Cust
		if(exampleInstance.getEntityId() != null && exampleInstance.getEntityId().intValue()!=0) {
			whereClause = whereClause + " and TB_CUST_ENTITY.ENTITY_ID = " + exampleInstance.getEntityId().intValue();
		}
		if(exampleInstance.getCustNbr() != null && exampleInstance.getCustNbr().length()>0) {	
			if(exampleInstance.getLookupBooleanFlag()){
				//Lookup, always put wildcard
				whereClause = whereClause + " and upper(tb_cust.cust_nbr) like '"+ exampleInstance.getCustNbr().toUpperCase() + "%' ";
			}
			else{
				//Main search			
				if(exampleInstance.getCustNbr().substring(exampleInstance.getCustNbr().length()-1, exampleInstance.getCustNbr().length()).equals("%") ||
						exampleInstance.getCustNbr().substring(0, 1).equals("%")){
					whereClause = whereClause + " and upper(tb_cust.cust_nbr) like '"+ exampleInstance.getCustNbr().toUpperCase() + "' ";
				}
				else{
					whereClause = whereClause + " and upper(tb_cust.cust_nbr) = '"+ exampleInstance.getCustNbr().toUpperCase() + "' ";
				}
			}
		} 
		if(exampleInstance.getCustName() != null && exampleInstance.getCustName().length()>0) {
			if(exampleInstance.getLookupBooleanFlag()){
				//Lookup, always put wildcard
				whereClause = whereClause + " and upper(tb_cust.cust_name) like '"+ exampleInstance.getCustName().toUpperCase() + "%' ";
			}
			else{
				//Main search			
				if(exampleInstance.getCustName().substring(exampleInstance.getCustName().length()-1, exampleInstance.getCustName().length()).equals("%") ||
						exampleInstance.getCustName().substring(0, 1).equals("%")){
					whereClause = whereClause + " and upper(tb_cust.cust_name) like '"+ exampleInstance.getCustName().toUpperCase() + "' ";
				}
				else{
					whereClause = whereClause + " and upper(tb_cust.cust_name) = '"+ exampleInstance.getCustName().toUpperCase() + "' ";
				}
			}
		} 	
		if(exampleInstance.getActiveFlag()!=null && exampleInstance.getActiveFlag().length()>0){
			if(exampleInstance.getActiveFlag().equals("1")){	
				whereClause = whereClause + " and tb_cust.active_flag='1' ";

			}
			else{
				whereClause = whereClause + " and tb_cust.active_flag='0' ";
			}
		}
		
		//For CustLocn
		if(exampleInstance.getCustLocnCode() != null && exampleInstance.getCustLocnCode().length()>0) {
			if(exampleInstance.getLookupBooleanFlag()){
				//Lookup, always put wildcard
				whereClause = whereClause + " and upper(TB_CUST_LOCN.CUST_LOCN_CODE) like '"+ exampleInstance.getCustLocnCode().toUpperCase() + "%' ";
			}
			else{
				//Main search			
				if(exampleInstance.getCustLocnCode().substring(exampleInstance.getCustLocnCode().length()-1, exampleInstance.getCustLocnCode().length()).equals("%") ||
						exampleInstance.getCustLocnCode().substring(0, 1).equals("%")){
					whereClause = whereClause + " and upper(TB_CUST_LOCN.CUST_LOCN_CODE) like '"+ exampleInstance.getCustLocnCode().toUpperCase() + "' ";
				}
				else{
					whereClause = whereClause + " and upper(TB_CUST_LOCN.CUST_LOCN_CODE) = '"+ exampleInstance.getCustLocnCode().toUpperCase() + "' ";
				}
			}
		} 
		if(exampleInstance.getCustLocnName() != null && exampleInstance.getCustLocnName().length()>0) {
			if(exampleInstance.getLookupBooleanFlag()){
				//Lookup, always put wildcard
				whereClause = whereClause + " and upper(TB_CUST_LOCN.LOCATION_NAME) like '"+ exampleInstance.getCustLocnName().toUpperCase() + "%' ";
			}
			else{
				//Main search			
				if(exampleInstance.getCustLocnName().substring(exampleInstance.getCustLocnName().length()-1, exampleInstance.getCustLocnName().length()).equals("%") ||
						exampleInstance.getCustLocnName().substring(0, 1).equals("%")){
					whereClause = whereClause + " and upper(TB_CUST_LOCN.LOCATION_NAME) like '"+ exampleInstance.getCustLocnName().toUpperCase() + "' ";
				}
				else{
					whereClause = whereClause + " and upper(TB_CUST_LOCN.LOCATION_NAME) = '"+ exampleInstance.getCustLocnName().toUpperCase() + "' ";
				}
			}
		} 
		
		if(exampleInstance.getCustLocnActiveFlag()!=null && exampleInstance.getCustLocnActiveFlag().length()>0){
			if(exampleInstance.getCustLocnActiveFlag().equals("1")){	
				whereClause = whereClause + " and TB_CUST_LOCN.ACTIVE_FLAG='1' ";

			}
			else{
				whereClause = whereClause + " and TB_CUST_LOCN.ACTIVE_FLAG='0' ";
			}
		}
		if(exampleInstance.getCustLocnCountry() != null && exampleInstance.getCustLocnCountry().length()>0) {
			whereClause = whereClause + " and upper(TB_CUST_LOCN.COUNTRY) = '"+ exampleInstance.getCustLocnCountry().toUpperCase() + "' ";
		} 
		
		if(exampleInstance.getCustLocnState() != null && exampleInstance.getCustLocnState().length()>0) {
			whereClause = whereClause + " and upper(TB_CUST_LOCN.STATE) = '"+ exampleInstance.getCustLocnState().toUpperCase() + "' ";
		} 
		
		String advancedFilter = "CUST_ID in ( " +
				"select distinct TB_CUST.CUST_ID from TB_CUST " +
				"left join TB_CUST_ENTITY on  TB_CUST.CUST_ID = TB_CUST_ENTITY.CUST_ID " +
				"left join TB_CUST_LOCN on TB_CUST_LOCN.CUST_ID = TB_CUST.CUST_ID " +
				"where "+ whereClause + " )";
		
		return advancedFilter;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cust> find(Cust exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		logger.info("Entering the gatAllRecords");
		List<Cust> list = new ArrayList<Cust>();
		try {
			Session localSession = this.createLocalSession();
			  
			Criteria criteria = localSession.createCriteria(Cust.class);
			criteria.add(Restrictions.sqlRestriction(getWhereClause(exampleInstance)));

			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
		}
		return new ArrayList<Cust>();
	}
	
	private Criteria generateCriteriaBatch(Cust exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(Cust.class);
        
		if(exampleInstance.getCustNbr() != null) {
		   criteria.add(Restrictions.ilike("custNbr",exampleInstance.getCustNbr()+"%"));//Not case sensitive
		}    
		if(exampleInstance.getCustName() != null) {
			criteria.add(Restrictions.ilike("custName",exampleInstance.getCustName()+"%"));//Not case sensitive
		}  
		if(exampleInstance.getActiveFlag()!=null && exampleInstance.getActiveFlag().length()>0){
			if(exampleInstance.getActiveFlag().equals("1")){	
			
				criteria.add(Restrictions.eq("activeFlag","1"));
			}
			else{
				criteria.add(Restrictions.eq("activeFlag","0"));
			}
		}

        return criteria;
	}
	
	private String sqlCustCert = 
			"select * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " +
	
			"TB_CUST_CERT.CUST_CERT_ID, " +
			"TB_CUST_CERT.CERT_NBR, " +
			"TB_CUST_CERT.EXEMPT_REASON_CODE, " + 
			"TB_CUST_CERT.INVOICE_NBR, " +
			"TB_CUST_CERT.CERT_CUST_NAME, " +
			"TB_CUST_CERT.COUNTRY, " +
			"TB_CUST_CERT.CERT_STATE, " +
//			"TB_CUST_CERT.CERT_IMAGE, " +
			"TB_CUST_CERT.APPLY_TO_STATE_FLAG, " +
			"TB_CUST_CERT.BLANKET_FLAG, " +
			"TB_CUST_CERT.COUNTRY_FLAG, " +
			"TB_CUST_CERT.STATE_FLAG, " +
			"TB_CUST_CERT.COUNTY_FLAG, " +
			"TB_CUST_CERT.CITY_FLAG, " +
			"TB_CUST_CERT.STJ1_FLAG, " +
			"TB_CUST_CERT.STJ2_FLAG, " +
			"TB_CUST_CERT.STJ3_FLAG, " +
			"TB_CUST_CERT.STJ4_FLAG, " +
			"TB_CUST_CERT.STJ5_FLAG, " +
			"TB_CUST_CERT.ACTIVE_FLAG, " +	
			"TB_CUST_CERT.EFFECTIVE_DATE, " +
			"TB_CUST_CERT.EXPIRATION_DATE, " +
			
			"TB_CUST.CUST_NBR,  " +
			"TB_CUST.CUST_NAME,  " +
			
			"TB_CUST_LOCN.CUST_LOCN_CODE,  " +
			"TB_CUST_LOCN.LOCATION_NAME,  " +
			
			"TB_ENTITY.ENTITY_CODE, " + 
			"TB_ENTITY.ENTITY_NAME, " +
			"TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE " +
			
			"FROM TB_CUST, TB_CUST_LOCN, TB_CUST_LOCN_EX, TB_CUST_CERT, TB_ENTITY " +
			"WHERE TB_CUST.CUST_ID = TB_CUST_LOCN.CUST_ID AND " +
			"TB_CUST_LOCN.CUST_LOCN_ID =TB_CUST_CERT.CUST_LOCN_ID AND " +
			"TB_ENTITY.ENTITY_ID=TB_CUST_CERT.ENTITY_ID AND " +
	
	
			":extendWhere) WHERE RowNumber BETWEEN :minrow AND :maxrow ";
	
	@Transactional(readOnly = false)
	public long getNextAllocSubTransId() {
		String sql = DatabaseUtil.getSequenceQuery(DatabaseUtil.getDatabaseProductName(getJpaTemplate(), entityManager), "SQ_TB_ALLOCATION_SUBTRANS_ID", "NEXTID");
		Query q = entityManager.createNativeQuery(sql);
		BigDecimal id = (BigDecimal) q.getSingleResult();
		
		return id.longValue();
	}
	
	@Transactional(readOnly = false)
	public Long count(CustCert exampleInstance) {
	
		Long returnLong = 0L;
		try {
			String sqlQuery = "select count(*) count " +
								"FROM TB_CUST, TB_CUST_LOCN, TB_CUST_LOCN_EX, TB_CUST_CERT, TB_ENTITY " +
								"WHERE TB_CUST.CUST_ID = TB_CUST_LOCN.CUST_ID AND " +
								"TB_CUST_LOCN.CUST_LOCN_ID =TB_CUST_CERT.CUST_LOCN_ID AND " +
								"TB_ENTITY.ENTITY_ID=TB_CUST_CERT.ENTITY_ID AND " +
								":extendWhere " ;
			
			String extendWhere = getWhereClause(exampleInstance);
			sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return returnLong;
	}
	
	String getWhereClause(CustCert exampleInstance){
		String extendWhere = "(1=1) ";

		if(exampleInstance.getCustNbr()!=null && exampleInstance.getCustNbr().length()>0)
			extendWhere = extendWhere + "AND TB_CUST.CUST_NBR like '"+exampleInstance.getCustNbr()+"%' ";
		
		if(exampleInstance.getCustName()!=null && exampleInstance.getCustName().length()>0)
			extendWhere = extendWhere + "AND TB_CUST.CUST_NAME like '"+exampleInstance.getCustName()+"%' ";
		
		if(exampleInstance.getActiveFlag()!=null && exampleInstance.getCustActiveFlag().length()>0)
			extendWhere = extendWhere + "AND TB_CUST.ACTIVE_FLAG = '"+exampleInstance.getCustActiveFlag()+"' ";
		
		if(exampleInstance.getEntityId()!=null)
			extendWhere = extendWhere + "AND TB_CUST_LOCN_EX.ENTITY_ID = " + exampleInstance.getEntityId();
		
		if(exampleInstance.getEntityId()!=null)
			extendWhere = extendWhere + "AND TB_CUST_CERT.ENTITY_ID = " + exampleInstance.getEntityId();
		
		if(exampleInstance.getExemptionTypeCode()!=null && exampleInstance.getExemptionTypeCode().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN_EX.EXEMPTION_TYPE_CODE = '"+exampleInstance.getExemptionTypeCode()+"' ";
		
		if(exampleInstance.getCustLocnCode()!=null && exampleInstance.getCustLocnCode().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.CUST_LOCN_CODE like '"+exampleInstance.getCustLocnCode()+"%' ";
		
		if(exampleInstance.getCustLocnName()!=null && exampleInstance.getCustLocnName().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.LOCATION_NAME like '"+exampleInstance.getCustLocnName()+"%' ";
		
		if(exampleInstance.getCustLocnCountry()!=null && exampleInstance.getCustLocnCountry().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.COUNTRY = '"+exampleInstance.getCustLocnCountry()+"' ";
		
		if(exampleInstance.getCustLocnState()!=null && exampleInstance.getCustLocnState().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.STATE = '"+exampleInstance.getCustLocnState()+"' ";
		
		if(exampleInstance.getCustLocnCity()!=null && exampleInstance.getCustLocnCity().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.CITY like '"+exampleInstance.getCustLocnCity()+"%' ";
		
		if(exampleInstance.getCustLocnActiveFlag()!=null && exampleInstance.getCustLocnActiveFlag().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_LOCN.ACTIVE_FLAG = '"+exampleInstance.getCustLocnActiveFlag()+"' ";
		
		if(exampleInstance.getCountry()!=null && exampleInstance.getCountry().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_CERT.COUNTRY = '"+exampleInstance.getCountry()+"' ";
		
		if(exampleInstance.getState()!=null && exampleInstance.getState().length()>0)
			extendWhere = extendWhere + "AND TB_CUST_CERT.STATE = '"+exampleInstance.getState()+"' ";
			
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		if(exampleInstance.getEffectiveDate()!=null){		
			String effectiveDate = df.format(exampleInstance.getEffectiveDate());
			extendWhere = extendWhere + "AND TB_CUST_CERT.EFFECTIVE_DATE >= to_date('"+effectiveDate+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getEffectiveDateThru()!=null){		
			String effectiveDateThru = df.format(exampleInstance.getEffectiveDateThru());
			extendWhere = extendWhere + "AND TB_CUST_CERT.EFFECTIVE_DATE <= to_date('"+effectiveDateThru+"', 'mm/dd/yyyy') ";
		}
		
		if(exampleInstance.getExpirationDate()!=null){
			String expirationDate = df.format(exampleInstance.getExpirationDate());
			extendWhere = extendWhere + "AND TB_CUST_CERT.EXPIRATION_DATE <= to_date('"+expirationDate+"', 'mm/dd/yyyy') ";
		}
		
		return extendWhere;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<CustCert> find(CustCert exampleInstance,  OrderBy orderBy, int firstRow, int maxResults){
		String sqlQuery = sqlCustCert;
		
		String strOrderBy = "TB_CUST_CERT.CUST_CERT_ID ASC";
		String extendWhere = getWhereClause(exampleInstance);

		sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
		sqlQuery = sqlQuery.replaceAll(":extendWhere", extendWhere);
		sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
		sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));

		List<CustCert> returnList=null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			returnList = parseCustCertReturnList(iter);
		}
		return returnList;
		
	}
	
	private List<CustCert> parseCustCertReturnList(Iterator<Object> iter){
		List<CustCert> returnList = new ArrayList<CustCert>();
		
		while(iter.hasNext()){
			Object[] objarray= (Object[])iter.next();
			CustCert custCert=new CustCert();
			
            BigDecimal is = (BigDecimal)objarray[1];
            if (is!=null) custCert.setCustCertId(is.longValue());
            
            String certNbr = (String) objarray[2];
            custCert.setCertNbr(certNbr);
            
            String exemptReasonCode = (String) objarray[3];
            custCert.setExemptReasonCode(exemptReasonCode);
            
            String invoiceNbr = (String) objarray[4];
            custCert.setInvoiceNbr(invoiceNbr);
            
            String certCustName = (String) objarray[5];
            custCert.setCertCustName(certCustName);
            
            String country = (String) objarray[6];
            custCert.setCountry(country);
            
            String certState = (String) objarray[7];
            custCert.setCertState(certState);
            
            String applyToStateFlag = (String) objarray[8];
            custCert.setApplyToStateFlag(applyToStateFlag);
            
            String blanketFlag = (String) objarray[9];
            custCert.setBlanketFlag(blanketFlag);
            
            String countryFlag = (String) objarray[10];
            custCert.setCountryFlag(countryFlag);
            
            String stateFlag = (String) objarray[11];
            custCert.setStateFlag(stateFlag);
            
            String countyFlag = (String) objarray[12];
            custCert.setCountyFlag(countyFlag);
            
            String cityFlag = (String) objarray[13];
            custCert.setCityFlag(cityFlag);
            
            String stj1Flag = (String) objarray[14];
            custCert.setStj1Flag(stj1Flag);
            
            String stj2Flag = (String) objarray[15];
            custCert.setStj2Flag(stj2Flag);
            
            String stj3Flag = (String) objarray[16];
            custCert.setStj3Flag(stj3Flag);
            
            String stj4Flag = (String) objarray[17];
            custCert.setStj4Flag(stj4Flag);
            
            String stj5Flag = (String) objarray[18];
            custCert.setStj5Flag(stj5Flag);
            
            String activeFlag = (String) objarray[19];
            custCert.setActiveFlag(activeFlag);
            
            java.sql.Timestamp effectiveDate = (java.sql.Timestamp) objarray[20];
            if(effectiveDate!=null){
            	custCert.setEffectiveDate(new java.util.Date(effectiveDate.getTime()));
            }
            
            java.sql.Timestamp expirationDate = (java.sql.Timestamp) objarray[21];
            if(expirationDate!=null){
            	custCert.setExpirationDate(new java.util.Date(expirationDate.getTime()));
            }
            
            String custNbr = (String) objarray[22];
            custCert.setCustNbr(custNbr);
            
            String custName = (String) objarray[23];
            custCert.setCustName(custName);
            
            String custLocnCode = (String) objarray[24];
            custCert.setCustLocnCode(custLocnCode);
            
            String locationName = (String) objarray[25];
            custCert.setCustLocnName(locationName);
            
            String entityCode = (String) objarray[26];
            custCert.setEntityCode(entityCode);
            
            String entityName = (String) objarray[27];
            custCert.setEntityName(entityName);
            
            String exemptionTypeCode = (String) objarray[28];
            custCert.setExemptionTypeCode(exemptionTypeCode);
             
            returnList.add(custCert);                
		}
		
		return returnList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public Cust getCustByName(String custName) throws DataAccessException {
		List<Cust> list = getJpaTemplate().find("select cust from Cust cust where upper(cust.custName) = ?" , custName.toUpperCase());
		
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		
		return  null;
	}
	
	public int getCustCountWithReasonCode(String custNbr, String locnCode, String reasonCode, Date glDate) throws DataAccessException {
		java.sql.Date date = DateUtils.getSqlDate(glDate);
		int count = 0;
		Session localSession = this.createLocalSession();
		try {
			org.hibernate.Query q = localSession.createSQLQuery(CUST_REASON_LOCN_COUNT);
			q.setString(0, custNbr);
			q.setString(1, locnCode);
			q.setString(2, reasonCode);
			q.setDate(3, date);
			q.setDate(4, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return count;
	}
	
	public int getCustCountWithReasonCodeAndCertNbr(String custNbr, String locnCode, String certNbr, String reasonCode, Date glDate) throws DataAccessException {
		java.sql.Date date = DateUtils.getSqlDate(glDate);
		int count = 0;
		Session localSession = this.createLocalSession();
		try {
			org.hibernate.Query q = localSession.createSQLQuery(CUST_REASON_CERT_NBR_LOCN_COUNT);
			q.setString(0, custNbr);
			q.setString(1, locnCode);
			q.setString(2, certNbr);
			q.setString(3, reasonCode);
			q.setDate(4, date);
			q.setDate(5, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return count;
	}
	
	public int getCustCount(String custNbr, String locnCode, Date glDate) throws DataAccessException {
		java.sql.Date date = DateUtils.getSqlDate(glDate);
		int count = 0;
		Session localSession = this.createLocalSession();
		try {
			org.hibernate.Query q = localSession.createSQLQuery(CUST_LOCN_COUNT);	
			q.setString(1, custNbr);
			q.setString(2, locnCode);
			q.setDate(3, date);
			q.setDate(4, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public int getCustCountWithExAndState(String custNbr, String state, Date glDate) throws DataAccessException {
		java.sql.Date date = DateUtils.getSqlDate(glDate);
		int count = 0;
		Session localSession = this.createLocalSession();
		try {
			org.hibernate.Query q = localSession.createSQLQuery(CUST_COUNT_WITH_EX_AND_STATE);
			q.setString(0, custNbr);
			q.setString(1, state);
			q.setDate(2, date);
			q.setDate(3, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return count;
	}
	
	public int getCustCountWithCertNbr(String custNbr, String locnCode, String certNbr, Date glDate) throws DataAccessException {
		java.sql.Date date = DateUtils.getSqlDate(glDate);
		int count = 0;
		Session localSession = this.createLocalSession();
		try {
			org.hibernate.Query q = localSession.createSQLQuery(CUST_CERT_NBR_LOCN_COUNT);
			q.setString(0, custNbr);
			q.setString(1, locnCode);
			q.setString(2, certNbr);
			q.setDate(3, date);
			q.setDate(4, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return count;
	}
	
	public int getCustCountWithCertNbrWithoutLocnCode(String custNbr, String certNbr, Date glDate, String geocode, String addressLine1, String addressLine2,
			String city, String county, String state, String zip, String country) throws DataAccessException {
		
		int count = 0;
		Session localSession = this.createLocalSession();
	
		try{
			java.sql.Date date = DateUtils.getSqlDate(glDate);
			
			StringBuilder query = new StringBuilder(SqlQuery.CUST_NO_LOCN_CUST_NBR_COUNT);
			if(StringUtils.hasText(geocode)) {
				query.append(String.format("AND TB_CUST_LOCN.GEOCODE = '%s' ", geocode));
			}
			if(StringUtils.hasText(addressLine1)) {
				query.append(String.format("AND TB_CUST_LOCN.ADDRESS_LINE_1 = '%s' ", addressLine1));
			}
			if(StringUtils.hasText(addressLine2)) {
				query.append(String.format("AND TB_CUST_LOCN.ADDRESS_LINE_2 = '%s' ", addressLine2));
			}
			if(StringUtils.hasText(city)) {
				query.append(String.format("AND TB_CUST_LOCN.CITY = '%s' ", city));
			}
			if(StringUtils.hasText(county)) {
				query.append(String.format("AND TB_CUST_LOCN.COUNTY = '%s' ", county));
			}
			if(StringUtils.hasText(state)) {
				query.append(String.format("AND TB_CUST_LOCN.STATE = '%s' ", state));
			}
			if(StringUtils.hasText(zip)) {
				query.append(String.format("AND TB_CUST_LOCN.ZIP = '%s' ", zip));
			}
			if(StringUtils.hasText(country)) {
				query.append(String.format("AND TB_CUST_LOCN.COUNTRY = '%s'", country));
			}
			
			org.hibernate.Query q = localSession.createSQLQuery(query.toString());
			q.setString(0, custNbr);
			q.setString(1, certNbr);
			q.setDate(2, date);
			q.setDate(3, date);
			List<BigDecimal> result = q.list();	
			count = ((BigDecimal)result.get(0)).intValue();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return count;
	}
	
	public static final String CUST_REASON_LOCN_COUNT = "SELECT COUNT(*) FROM TB_CUST INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID " +
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN.CUST_LOCN_ID=TB_CUST_LOCN_EX.CUST_LOCN_ID WHERE TB_CUST.CUST_NBR = ? " +
			"AND TB_CUST_LOCN.CUST_LOCN_CODE = ? AND TB_CUST_LOCN_EX.EXEMPT_REASON_CODE = ? " +
			"AND trunc(TB_CUST_LOCN_EX.EFFECTIVE_DATE) <= ? AND trunc(TB_CUST_LOCN_EX.EXPIRATION_DATE) >= ?";
	
	public static final String CUST_REASON_CERT_NBR_LOCN_COUNT = "SELECT COUNT(*) FROM TB_CUST " +
			"INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID " +
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN.CUST_LOCN_ID=TB_CUST_LOCN_EX.CUST_LOCN_ID " +
			"INNER JOIN TB_CUST_CERT ON TB_CUST_LOCN_EX.CUST_LOCN_ID=TB_CUST_CERT.CUST_LOCN_ID " +
			"AND TB_CUST_LOCN_EX.CUST_CERT_ID=TB_CUST_CERT.CUST_CERT_ID WHERE TB_CUST.CUST_NBR = ? " +
			"AND TB_CUST_LOCN.CUST_LOCN_CODE = ? AND TB_CUST_CERT.CERT_NBR = ? " +
			"AND TB_CUST_CERT.EXEMPT_REASON_CODE = ? AND trunc(TB_CUST_CERT.EFFECTIVE_DATE) <= ? " +
			"AND trunc(TB_CUST_CERT.EXPIRATION_DATE) >= ?";
	
	public static final String CUST_LOCN_COUNT = "SELECT COUNT(*) FROM TB_CUST INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID " +
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN.CUST_LOCN_ID=TB_CUST_LOCN_EX.CUST_LOCN_ID WHERE TB_CUST.CUST_NBR = ? " +
			"AND TB_CUST_LOCN.CUST_LOCN_CODE = ? " +
			"AND trunc(TB_CUST_LOCN_EX.EFFECTIVE_DATE) <= ? AND trunc(TB_CUST_LOCN_EX.EXPIRATION_DATE) >= ?";
	
	public static String CUST_COUNT_WITH_EX_AND_STATE = "SELECT COUNT(*) " +
			"FROM TB_CUST "+
			"INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID "+
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN_EX.CUST_LOCN_ID=TB_CUST_LOCN.CUST_LOCN_ID "+
			"WHERE TB_CUST.CUST_NBR = ? "+
			"AND TB_CUST_LOCN.STATE = ? "+
			"AND trunc(TB_CUST_LOCN_EX.EFFECTIVE_DATE) <= ? "+
			"AND trunc(TB_CUST_LOCN_EX.EXPIRATION_DATE) >= ? ";
	
	public static final String CUST_CERT_NBR_LOCN_COUNT = "SELECT COUNT(*) FROM TB_CUST " +
			"INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID " +
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN.CUST_LOCN_ID=TB_CUST_LOCN_EX.CUST_LOCN_ID " +
			"INNER JOIN TB_CUST_CERT ON TB_CUST_LOCN_EX.CUST_LOCN_ID=TB_CUST_CERT.CUST_LOCN_ID " +
			"AND TB_CUST_LOCN_EX.CUST_CERT_ID=TB_CUST_CERT.CUST_CERT_ID WHERE TB_CUST.CUST_NBR = ? " +
			"AND TB_CUST_LOCN.CUST_LOCN_CODE = ? AND TB_CUST_CERT.CERT_NBR = ? " +
			"AND trunc(TB_CUST_CERT.EFFECTIVE_DATE) <= ? " +
			"AND trunc(TB_CUST_CERT.EXPIRATION_DATE) >= ?";
	
	public static final String CUST_NO_LOCN_CUST_NBR_COUNT = "SELECT COUNT(*) FROM TB_CUST INNER JOIN TB_CUST_LOCN ON TB_CUST.CUST_ID=TB_CUST_LOCN.CUST_ID " +
			"INNER JOIN TB_CUST_LOCN_EX ON TB_CUST_LOCN.CUST_LOCN_ID=TB_CUST_LOCN_EX.CUST_LOCN_ID " +
			"INNER JOIN TB_CUST_CERT ON TB_CUST_LOCN_EX.CUST_LOCN_ID=TB_CUST_CERT.CUST_LOCN_ID " +
			"AND TB_CUST_LOCN_EX.CUST_CERT_ID=TB_CUST_CERT.CUST_CERT_ID " +
			"WHERE TB_CUST.CUST_NBR = ? " +
			"AND TB_CUST_CERT.CERT_NBR = ? AND trunc(TB_CUST_CERT.EFFECTIVE_DATE) <= ? " +
			"AND trunc(TB_CUST_CERT.EXPIRATION_DATE) >= ? ";
}
