package com.ncsts.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Auditable;
import com.ncsts.domain.Idable;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.domain.NexusDefinitionDetail;
import com.seconddecimal.billing.domain.NexusDefDetail;
import com.seconddecimal.billing.query.SqlQuery;
import com.seconddecimal.billing.util.DateUtils;

public class JPAVendorNexusDetailDAO extends JPAGenericDAO<VendorNexusDtl, Long> implements VendorNexusDetailDAO {
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<VendorNexusDtl> findAllVendorNexusDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFag) 
		throws DataAccessException {
		
		StringBuffer sql =  new StringBuffer("Select ndd from VendorNexusDtl ndd where ndd.vendorNexusId in " +
				"(select vendorNexusId from VendorNexus where nexusCountryCode = ?1 and nexusStateCode = ?2 and " +
				"nexusCounty = ?3 and nexusCity = ?4 and nexusStj = ?5 and vendorId = ?6)");
		
		List<VendorNexusDtl> list = null;
		/* PP-23
		if(activeFag) {
			sql.append(" and activeFlag = ?7");
			list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId, "1");
		}
		else {
			list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId);
		}
		*/
		list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId);
		
		return list;
	}
	
	@Transactional
	public boolean isVendorNexusExist(Long vendorId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException {

		String sql = 	"Select count(*) " +
						"from TB_VENDOR_NEXUS nd join TB_VENDOR_NEXUS_DTL ndd on nd.VENDOR_NEXUS_ID=ndd.VENDOR_NEXUS_ID where (ndd.active_flag = '1') ";
		
		if(vendorId!=null){
			sql = sql + " and nd.VENDOR_ID = " + vendorId.intValue();
		}
		
		if(countryCode!=null){
			sql = sql + " and nd.NEXUS_COUNTRY_CODE = '" + countryCode + "'";
		}
		
		if(stateCode!=null){
			sql = sql + " and nd.NEXUS_STATE_CODE = '" + stateCode + "'";
		}
		
		if(county!=null){
			sql = sql + " and nd.NEXUS_COUNTY = '" + county + "'";
		}
		
		if(city!=null){
			sql = sql + " and nd.NEXUS_CITY = '" + city + "'";
		}
		
		if(stj!=null){
			sql = sql + " and nd.NEXUS_STJ = '" + stj + "'";
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(effectiveDate);
		
		if(type!=null && type.length()>0){ //check auto
			sql = sql + " and ndd.nexus_type_code = '" + type + "'";
			sql = sql + " and ndd.effective_date <= to_date('" + now + "', 'mm/dd/yyyy')"; 
		}
		else{
			sql = sql + " and ndd.effective_date = to_date('" + now + "', 'mm/dd/yyyy')"; 
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();

	    if(count!=null && count.intValue()>0){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	
	@Transactional
	public boolean isVendorNexusActive(Long vendorId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException {

		String sql = 	"Select count(*) " +
						"from TB_VENDOR_NEXUS nd join TB_VENDOR_NEXUS_DTL ndd on nd.VENDOR_NEXUS_ID=ndd.VENDOR_NEXUS_ID where (ndd.active_flag = '1') ";
		
		if(vendorId!=null){
			sql = sql + " and nd.VENDOR_ID = " + vendorId.intValue();
		}
		
		if(countryCode!=null){
			sql = sql + " and nd.NEXUS_COUNTRY_CODE = '" + countryCode + "'";
		}
		
		if(stateCode!=null){
			sql = sql + " and nd.NEXUS_STATE_CODE = '" + stateCode + "'";
		}
		
		if(county!=null){
			sql = sql + " and nd.NEXUS_COUNTY = '" + county + "'";
		}
		
		if(city!=null){
			sql = sql + " and nd.NEXUS_CITY = '" + city + "'";
		}
		
		if(stj!=null){
			sql = sql + " and nd.NEXUS_STJ = '" + stj + "'";
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();

	    if(count!=null && count.intValue()>0){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}

	@Override
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void bulkExpireVendorNexusDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		String sql =  "Update VendorNexusDtl set expirationDate = :expirationDate where vendorNexusId in " +
				"(select vendorNexusId from VendorNexus where nexusCountryCode = :countryCode and nexusStateCode = :stateCode) and expirationDate > :expirationDate";
		
		Session session = createLocalSession();
		try {
			org.hibernate.Query q = session.createQuery(sql);
			q.setParameter("countryCode", countryCode);
			q.setParameter("stateCode", stateCode);
			q.setParameter("expirationDate", expirationDate);
			q.executeUpdate();
		} 
		catch (Exception e) {	
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Override
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void bulkExpireRegistrationDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		String sql =  "Update RegistrationDetail set expirationDate = :expirationDate where registrationId in " +
				"(select registrationId from Registration where regCountryCode = :countryCode and regStateCode = :stateCode) and expirationDate > :expirationDate";

		Session session = createLocalSession();
		try {
			org.hibernate.Query q = session.createQuery(sql);
			q.setParameter("countryCode", countryCode);
			q.setParameter("stateCode", stateCode);
			q.setParameter("expirationDate", expirationDate);
			q.executeUpdate();
		} 
		catch (Exception e) {	
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException {
		boolean isExist = false;
		
		String sql = "";
		if(nexusTypeCode!=null && nexusTypeCode.length()>0){
			//check for auto jurisdiction
			sql = " select count(*) from VendorNexusDtl vendorNexusDtl where vendorNexusDtl.vendorNexusId = ? and  vendorNexusDtl.effectiveDate <= ? and nexusTypeCode = ? ";	
		}
		else{
			sql = " select count(*) from VendorNexusDtl vendorNexusDtl where vendorNexusDtl.vendorNexusId = ? and  vendorNexusDtl.effectiveDate = ? "; 
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	query.setParameter(1, nexusDefId );
	  	query.setParameter(2, effectiveDate );
	  		  	
	  	if(nexusTypeCode!=null && nexusTypeCode.length()>0){
			//check for auto jurisdiction
		  	query.setParameter(3, nexusTypeCode );
		}
		else{
		}
	  	
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();  
	    if(count!=null && count.intValue()>0){
	    	isExist = true;
	    }
		
        return isExist;
	}
	
	public List<VendorNexusDtl> getVendorNexusDtlList(Long vendorId, Date glDate, String whereClause, String orderBy) throws DataAccessException {
		
		List<VendorNexusDtl> list = new ArrayList<VendorNexusDtl>();
		
		String sqlState = 	
        		"SELECT tb_vendor_nexus_dtl.* " +
				"FROM tb_vendor_nexus, tb_vendor_nexus_dtl " +
				"WHERE tb_vendor_nexus.vendor_nexus_id = tb_vendor_nexus_dtl.vendor_nexus_id " +
				"AND tb_vendor_nexus.vendor_id = :vendorId " +
				"AND tb_vendor_nexus_dtl.effective_date <= :effectiveDate " +
			    "AND tb_vendor_nexus_dtl.expiration_date >= :expirationDate " +
				"AND tb_vendor_nexus_dtl.active_flag = '1' ";
		
		java.sql.Date glDateSql = DateUtils.getSqlDate(glDate);
		String sql = String.format("%s%s%s", sqlState, whereClause, orderBy);
		
		Session session = createLocalSession();
		try {	
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(VendorNexusDtl.class);

			query.setParameter("vendorId", vendorId);
			query.setParameter("effectiveDate", glDateSql);
			query.setParameter("expirationDate", glDateSql);
			
			List<VendorNexusDtl> returnList = query.list();
			
			VendorNexusDtl aVendorNexusDtl = null;
			for(int i=0; i<returnList.size(); i++){
				aVendorNexusDtl = returnList.get(i);
				VendorNexusDtl vendorNexusDtl = new VendorNexusDtl();
				vendorNexusDtl.setVendorNexusDtlId(aVendorNexusDtl.getVendorNexusDtlId());
				vendorNexusDtl.setNexusTypeCode(aVendorNexusDtl.getNexusTypeCode());
				
				list.add(vendorNexusDtl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}

		return list;
	}

	public List<NexusDefinitionDetail> getEntityNexusDtlList(Long entityId, Date glDate, String whereClause, String orderBy) throws DataAccessException {
		
		List<NexusDefinitionDetail> list = new ArrayList<NexusDefinitionDetail>();
		
		String sqlState = 	
        		"SELECT tb_nexus_def_detail.* " +
				"FROM tb_nexus_def, tb_nexus_def_detail " +
				"WHERE tb_nexus_def.nexus_def_id = tb_nexus_def_detail.nexus_def_id " +
				"AND tb_nexus_def.entity_id = :entityId " +
				"AND tb_nexus_def_detail.effective_date <= :effectiveDate " +
			    "AND tb_nexus_def_detail.expiration_date >= :expirationDate " +
				"AND tb_nexus_def_detail.active_flag = '1' ";
		
		java.sql.Date glDateSql = DateUtils.getSqlDate(glDate);
		String sql = String.format("%s%s%s", sqlState, whereClause, orderBy);
		
		Session session = createLocalSession();
		try {	
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(NexusDefinitionDetail.class);

			query.setParameter("entityId", entityId);
			query.setParameter("effectiveDate", glDateSql);
			query.setParameter("expirationDate", glDateSql);
			
			List<NexusDefinitionDetail> returnList = query.list();
			
			NexusDefinitionDetail aNexusDefinitionDetail = null;
			for(int i=0; i<returnList.size(); i++){
				aNexusDefinitionDetail = returnList.get(i);
				NexusDefinitionDetail nexusDefinitionDetail = new NexusDefinitionDetail();
				nexusDefinitionDetail.setNexusDefDetailId(aNexusDefinitionDetail.getNexusDefDetailId());
				nexusDefinitionDetail.setNexusTypeCode(aNexusDefinitionDetail.getNexusTypeCode());
				
				list.add(nexusDefinitionDetail);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}

		return list;
	}
}
