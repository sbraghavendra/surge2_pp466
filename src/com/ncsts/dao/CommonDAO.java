package com.ncsts.dao;

import java.util.List;

import com.ncsts.common.helper.OrderBy;

//import org.apache.commons.logging.*;

/**
 * <b>Created:</b> Mar 23, 2008<br>
 * <b>Title:</b> CommonDAO<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * A DAO that handles some commonly required persistence functions.
 */

public interface CommonDAO {
    //private static final transient Log log = LogFactory.getLog(com.ncsts.dao.CommonDAO.class);

    /**
     * Get's a page of the specified class from the DB, from startIndex to endIndex, non-inclusive of endIndex.
     * @param startIndex
     * @param endIndex
     * @return
     */
    List<Object> getPage(Class<?> clazz, int startIndex, int endIndex, OrderBy orderBy);
    List<Object> getPage(Class<?> clazz, int startIndex, int endIndex, OrderBy orderBy,
            Object exampleInstance, String[] excludeProperties);
    Object getById(Class<?> clazz, Object primaryKey);
    /**
     * Returns a count of all the specified entities in the DB.
     * @return
     */
    Long getTotalCount(Class<?> clazz);
    Long getTotalCount(Class<?> clazz, Object exampleInstance, String[] excludeProperties);
    void persist(Object entity);

}
