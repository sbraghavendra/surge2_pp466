package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Registration;

public class JPARegistrationDAO extends JPAGenericDAO<Registration,Long> implements RegistrationDAO {

	public Long count(Long entityId) throws DataAccessException {
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createQuery(" select count(*) from Registration registration where registration.entityId = ?");
	  	query.setParameter(1,entityId );
	    Long count = (Long) query.getSingleResult();
		return count;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Registration> findAllRegistrations() throws DataAccessException {
	    return getJpaTemplate().find(" select registration from Registration registration");	  
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Registration> findAllRegistrationsByEntityId(Long entityId) throws DataAccessException {
		return getJpaTemplate().find(" select registration from Registration registration where registration.entityId = ? order by registration.productCode", entityId);	  
	}
	  
	@SuppressWarnings("deprecation")
	@Transactional
	public Long persist(Registration registration) throws DataAccessException {
		getJpaTemplate().persist(registration);
		getJpaTemplate().flush();
		Long entityDefaultId = registration.getRegistrationId();
		
		return entityDefaultId;
	}	
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long id) throws DataAccessException {
		Registration registration = getJpaTemplate().find(Registration.class, id);
		if( registration != null){
			getJpaTemplate().remove(registration);
		}else{
			logger.warn("Registration Object not found for deletion");
		}
	} 
	  
	public void saveOrUpdate (Registration registration) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.update(registration);
		t.commit();
		closeLocalSession(session);
		session=null;		
	}
	
	@SuppressWarnings("unchecked")
	public List<Registration> getAllRecords(Registration exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		List<Registration> List = null;
		
		Session session = createLocalSession();
		try { 
			Criteria criteria = session.createCriteria(Registration.class);
			String advancedFilter = " ENTITY_ID = " + exampleInstance.getEntityId().intValue();
			//if(exampleInstance.getProductCode()!=null && exampleInstance.getProductCode().trim().length()>0){
			//	String productCode = exampleInstance.getProductCode().trim();
				
			//	if(productCode.endsWith("%")){
			//		advancedFilter = advancedFilter + " AND PRODUCT_CODE LIKE " + "'" + productCode + "'";
			//	}
			//	else{
			//		advancedFilter = advancedFilter + " AND ((PRODUCT_CODE =  '" + productCode + "') OR (PRODUCT_CODE >= '" + productCode + "' AND PRODUCT_CODE_THRU <=  '" + productCode + "'))";
			//	}
			//}
			criteria.add(Restrictions.sqlRestriction(advancedFilter));
			
			//if(exampleInstance.getTaxcodeCode()!=null && exampleInstance.getTaxcodeCode().trim().length()>0){
			//	String taxcodeCode = exampleInstance.getTaxcodeCode().trim();
			//	if(taxcodeCode.endsWith("%")){
			//		criteria.add(Restrictions.ilike("taxcodeCode", taxcodeCode, MatchMode.START));
			//	}else {
			//		criteria.add(Restrictions.eq("taxcodeCode", taxcodeCode));
			//	}
			//}

			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            List = criteria.list();
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding JPARegistrationDAO.getAllRecords() by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return List;
	}
	
	@Transactional(readOnly=true)
	public Long count(Registration exampleInstance) {
		Long ret = 0L;
		
		Session session = createLocalSession();
		try { 
			Criteria criteria = session.createCriteria(Registration.class);
			String advancedFilter = " ENTITY_ID = " + exampleInstance.getEntityId().intValue();
			//if(exampleInstance.getProductCode()!=null && exampleInstance.getProductCode().trim().length()>0){
			//	String productCode = exampleInstance.getProductCode().trim();
				
			//	if(productCode.endsWith("%")){
			//		advancedFilter = advancedFilter + " AND PRODUCT_CODE LIKE " + "'" + productCode + "'";
			//	}
			//	else{
			//		advancedFilter = advancedFilter + " AND ((PRODUCT_CODE =  '" + productCode + "') OR (PRODUCT_CODE >= '" + productCode + "' AND PRODUCT_CODE_THRU <=  '" + productCode + "'))";
			//	}
			//}
			criteria.add(Restrictions.sqlRestriction(advancedFilter));
			
			//if(exampleInstance.getTaxcodeCode()!=null && exampleInstance.getTaxcodeCode().trim().length()>0){
			//	String taxcodeCode = exampleInstance.getTaxcodeCode().trim();
			//	if(taxcodeCode.endsWith("%")){
			//		criteria.add(Restrictions.ilike("taxcodeCode", taxcodeCode, MatchMode.START));
			//	}else {
			//		criteria.add(Restrictions.eq("taxcodeCode", taxcodeCode));
			//	}
			//}
	 
			criteria.setProjection(Projections.rowCount()); // We just want a row count
			ret = ((Number)criteria.list().get(0)).longValue();
			logger.info("exit JPATransactionDetailDAO.count() = " + ret );	
			
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding JPARegistrationDAO.count() by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}

        return ret;
	}  
}
