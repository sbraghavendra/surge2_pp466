package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.User;
import com.ncsts.dto.ImportSpecDTO;


public interface ImportSpecDAO extends GenericDAO<ImportSpec, ImportSpecPK> {
	
	
	public abstract List<ImportSpec> getAllImportSpec();	
	public abstract List<ImportSpec> getImportSpecByDefCode(String defCode) throws DataAccessException;
	public abstract ImportSpec  findByPK(ImportSpecPK importSpecPK) throws DataAccessException;
	public abstract ImportSpec addImportSpecRecord(ImportSpec instance) throws DataAccessException;
	public abstract void update(ImportSpec importSpec) throws DataAccessException;
	public abstract void delete(ImportSpecPK importSpecPK) throws DataAccessException;
	
	public abstract List<ImportSpec> getAllForUserByType(String type) throws DataAccessException;
	
	// Import Spec User Details related Methods
	
	public abstract List<User> getImportSpecUserBySpec(ImportSpecPK specPK)throws DataAccessException;
	public void updateImportSpecUserBySpec(ImportSpec importSpec, List<String> userCodeList) throws DataAccessException;
	public List<ImportSpec> getAllRecords(ImportSpec exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults);
	public Long count(ImportSpec exampleInstance);
}
