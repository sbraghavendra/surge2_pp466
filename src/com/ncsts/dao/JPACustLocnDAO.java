/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.dto.BatchDTO;
import com.ncsts.view.util.HibernateUtils;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;
import com.seconddecimal.billing.query.SqlQuery;
import com.seconddecimal.billing.util.DateUtils;


public class JPACustLocnDAO extends JPAGenericDAO<CustLocn, Long> implements 
		CustLocnDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	@SuppressWarnings("unused")
	private CustLocnDAO custLocnDAO;
	
	public Session getCurrentSession(){
		return createLocalSession();
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public CustLocn getCustLocnByCustId(Long custId, String custLocnCode) throws DataAccessException {
		List<CustLocn> list = getJpaTemplate().find("select custLocn from CustLocn custLocn where upper(custLocn.custLocnCode) = ? and custLocn.custId = ?  " , custLocnCode.toUpperCase(), custId);
		
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		
		return  null;
	}
	
	@Transactional
	public CustLocn getCustLocnByCustLocnCode(String custLocnCode) throws DataAccessException {
		List<CustLocn> list = getJpaTemplate().find("select custLocn from CustLocn custLocn where upper(custLocn.custLocnCode) = ?  " , custLocnCode.toUpperCase());
		
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		
		return  null;
	}
	
	@Transactional(readOnly = false)
	public boolean isAllowDeleteCustLocn(String custLocnCode) throws DataAccessException {
	
		Long returnLong = 1L;
		try {
			String sqlQuery = "select count(*) count FROM tb_billtrans WHERE upper(cust_locn_code) = '" + custLocnCode.toUpperCase() + "'";			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return (returnLong==0L);
	}
	
	//CustLocn
	public Long count(CustLocn exampleInstance) {
		Session localSession = this.createLocalSession();
		
		Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
		criteria.setProjection(Projections.rowCount()); 
		List<?> result = criteria.list();
		Long returnLong = ((Number)result.get(0)).longValue();
		
		closeLocalSession(localSession);
		localSession=null;
		
		return returnLong;
	}

	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	@SuppressWarnings("unchecked")
	public List<CustLocn> find(Criteria criteria) {
		return criteria.list();
	}
	public List<CustLocn> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<CustLocn> returnCustLocn = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCustLocn;
	}
	
	public CustLocn find(CustLocn exampleInstance) {
		List<CustLocn> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CustLocn> findAllCustLocn(CustLocn exampleInstance) throws DataAccessException {
		return  find(exampleInstance, null, 0, 10000);
	}
	
	@SuppressWarnings("unchecked")
	public List<CustLocn> find(CustLocn exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults) {
		logger.info("Entering the gatAllRecords");
		List<CustLocn> list = new ArrayList<CustLocn>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
		}
		return new ArrayList<CustLocn>();
	}
	
	private Criteria generateCriteriaBatch(CustLocn exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(CustLocn.class);
		
		if(exampleInstance.getCustId() != null) {
			   criteria.add(Restrictions.eq("custId",exampleInstance.getCustId()));//Not case sensitive
		}
        
		if(exampleInstance.getCustLocnCode() != null) {
		   criteria.add(Restrictions.ilike("custLocnCode",exampleInstance.getCustLocnCode()+"%"));//Not case sensitive
		}    
		if(exampleInstance.getLocationName() != null) {
		   criteria.add(Restrictions.ilike("locationName",exampleInstance.getLocationName()+"%"));//Not case sensitive
		} 
		if(exampleInstance.getActiveFlag() != null) {
		   criteria.add(Restrictions.eq("activeFlag",exampleInstance.getActiveFlag())); 
		}
		if(exampleInstance.getCountry() != null) {
		   criteria.add(Restrictions.eq("country",exampleInstance.getCountry())); 
		}
		if(exampleInstance.getState() != null) {
		   criteria.add(Restrictions.eq("state",exampleInstance.getState())); 
		}

        return criteria;
	}

	@Transactional
	public Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz) {
		HibernateUtils hu = new HibernateUtils((HibernateEntityManagerFactory)getJpaTemplate().getEntityManagerFactory());
		return hu.getColumnToProperties(clazz);
	}
	
	@Transactional
	public boolean deleteBatch(Long batchId) {
		boolean success = false;
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlDelete = "";
			
			int deleteCount = 0;
			
			hqlDelete = "DELETE FROM tb_bcp_cust_locn WHERE batch_id = " + batchId;
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			logger.info(hqlDelete + ": " + deleteCount);
			
			hqlDelete = "DELETE FROM tb_bcp_cust_locn_text WHERE batch_id = " + batchId;
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			logger.info(hqlDelete + ": " + deleteCount);
			
			tx.commit();
			
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return success;
	}

	@Transactional
	public CustLocn getCustLocnByName(String locationName) throws DataAccessException {
		List<CustLocn> list = getJpaTemplate().find("select custLocn from CustLocn custLocn where upper(custLocn.locationName) = ?  " , locationName.toUpperCase());
		
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		
		return  null;
	}
	
	public void createTempExemptionWithReason(Long custLocnId, Long entityId, int exemptionGraceDays, String reasonCode, String user) throws DataAccessException {
		Calendar cal = Calendar.getInstance();
		java.sql.Date now = DateUtils.getSqlDate(cal.getTime());
		cal.add(Calendar.DAY_OF_YEAR, exemptionGraceDays);
		java.sql.Date expirationDate = DateUtils.getSqlDate(cal.getTime());
		
		Session localSession = this.createLocalSession();
		try {
			SQLQuery q = localSession.createSQLQuery(CREATE_TEMP_CERT_WITH_REASON);
			q.setLong(1, custLocnId);
			q.setLong(2, entityId);
			q.setDate(3, now);
			q.setDate(4, expirationDate);
			q.setString(5, reasonCode);
			q.setString(6, user);
			q.setDate(7, now);	
			q.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
	}
	
	public Long getNextCustLocnId() throws DataAccessException {
		Session session = createLocalSession();
		Long id = 0L;
		try {
			org.hibernate.Query query = session.createSQLQuery(NEXT_CUST_LOCN_ID);
			@SuppressWarnings("unchecked")
			List<BigDecimal> result = query.list();	
			id = ((BigDecimal)result.get(0)).longValue();
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
		}
		
		return id;
	}
	
	public void createCustLocn(Long custLocnId, Long custId, String custLocnCode, String locationName, String geocode, 
			String addressLine1, String addressLine2, String city, String county, String state, String zip, String zipplus4,
			String country, String user) throws DataAccessException {
		
		Session localSession = this.createLocalSession();
		try {
			SQLQuery q = localSession.createSQLQuery(CREATE_CUST_LOCN);
			q.setLong(1, custLocnId);
			q.setLong(2, custId);
			q.setString(3, custLocnCode);
			q.setString(4, locationName);
			q.setString(5, geocode);
			q.setString(6, addressLine1);
			q.setString(7, addressLine2);
			q.setString(8, city);
			q.setString(9, county);
			q.setString(10, state);
			q.setString(11, zip);
			q.setString(12, zipplus4);
			q.setString(13, country);
			q.setString(14, user);
			q.setDate(15, DateUtils.getSqlDate(new Date()));
			q.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
		finally {		
			closeLocalSession(localSession);
			localSession=null;
		}
	}

	public static final String CREATE_TEMP_CERT_WITH_REASON = "INSERT INTO TB_CUST_LOCN_EX (CUST_LOCN_EX_ID, CUST_LOCN_ID, ENTITY_ID, EXEMPTION_TYPE_CODE, EFFECTIVE_DATE, EXPIRATION_DATE, EXEMPT_REASON_CODE, CUST_CERT_ID, COUNTRY_FLAG, STATE_FLAG, COUNTY_FLAG, CITY_FLAG, STJ1_FLAG, STJ2_FLAG, STJ3_FLAG, STJ4_FLAG, STJ5_FLAG, ACTIVE_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) " +
			"VALUES (sq_tb_cust_locn_ex_id.nextval, ?, ?, '1', ?, ?, ?, NULL, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', ?, ?)";
	
	public static final String NEXT_CUST_LOCN_ID = "SELECT SQ_TB_CUST_LOCN_ID.NEXTVAL FROM DUAL";
	
	public static final String CREATE_CUST_LOCN = "INSERT INTO TB_CUST_LOCN (CUST_LOCN_ID, CUST_ID, CUST_LOCN_CODE, LOCATION_NAME, GEOCODE, ADDRESS_LINE_1, ADDRESS_LINE_2, CITY, COUNTY, STATE, ZIP, ZIPPLUS4, COUNTRY, ACTIVE_FLAG, UPDATE_USER_ID, UPDATE_TIMESTAMP) " +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '1', ?, ?)";
	
}
