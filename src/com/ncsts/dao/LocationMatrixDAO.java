package com.ncsts.dao;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;

/**
 * 
 * @author Anand
 * 
 *
 */
public interface LocationMatrixDAO extends MatrixDAO<LocationMatrix> {
	public List<LocationMatrix> getAllRecords(LocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			String defaultFlag, Long locationMatrixId,
			OrderBy orderBy, int firstRow, int maxResults);

	public Long count(LocationMatrix exampleInstance, 
			String geocode, String city, String county, String state, String country, String zip,
			String defaultFlag, Long locationMatrixId);
	
	public List<LocationMatrix> getDetailRecords(LocationMatrix locationMatrix, OrderBy orderBy);
	
	public List<LocationMatrix> getMatchingLocationMatrixLines(PurchaseTransaction exampleInstance);



}
