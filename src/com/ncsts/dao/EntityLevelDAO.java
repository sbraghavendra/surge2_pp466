package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.EntityLevel;
import com.ncsts.dto.EntityLevelDTO;

/**
 * @author Paul Govindan
 *
 */

public interface EntityLevelDAO {
	
    public abstract EntityLevel findById(Long entityId) throws DataAccessException;	
    
    public abstract List<EntityLevel> findAllEntityLevels() throws DataAccessException;
    
    public List<EntityLevel> findByTransDtlColName(String columnName) throws DataAccessException;
    public void save(EntityLevelDTO entityLevelDTO) throws DataAccessException;
    public void update (EntityLevelDTO entityLevelDTO) throws DataAccessException;
    public void remove (Long entityLevelId) throws DataAccessException;
    
    public abstract String findByBU() throws DataAccessException;
    
   }


