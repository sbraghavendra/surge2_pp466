package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;

public interface ImportDefinitionDAO extends GenericDAO<ImportDefinition, String> {
	public List<ImportDefinitionDetail> getAllRecords();
	public List<ImportDefinition> getAllImportDefinition();
	public List<ImportDefinition> getAllImportDefinition(String type);
	public List<ImportDefinition> getDefinitionsByConnection(String connectionCode);
	
	// New Methods written for Import Definitions 
	public ImportDefinition  findByPK(String importDefCode) throws DataAccessException;
	public abstract ImportDefinition addImportDefRecord(ImportDefinition instance) throws DataAccessException;
	public abstract void update(ImportDefinition importDefinition) throws DataAccessException;
	public abstract void delete(String importDefCode) throws DataAccessException;
	public List<ImportDefinitionDetail> getAllHeaders(String code);    
	public void replace(String importDefCode, List<ImportDefinitionDetail> list);    
	public abstract ImportDefinitionDetail add(ImportDefinitionDetail detail) throws DataAccessException;
    //public abstract List<ImportSpec> getImportSpecUsingDef(ImportSpecPK defPK)throws DataAccessException;
	public List<ImportDefinition> getAllRecords(ImportDefinition exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults);
	public Long count(ImportDefinition exampleInstance);
	public List<String> getImportDefinitionCodes(String tablename) throws DataAccessException;
	public List<ImportDefinitionDetail> getResetImpDefColHeadings(String code,String tablename) throws DataAccessException; 
	public void addImportDefinitionDetail(ImportDefinitionDetail importDefinitionDetail) throws DataAccessException; 
	
}
