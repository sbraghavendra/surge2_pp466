package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.dom4j.Document;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Auditable;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

public class JPAPreferenceDAO extends JpaDaoSupport implements PreferenceDAO {
	private Document stsDoc = null;
	
	public JPAPreferenceDAO() {
	}
	
	@Transactional
	public String getAdminPreference(AdminPreference preference, String defValue) {
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createNativeQuery("SELECT VALUE FROM TB_OPTION WHERE " +
				"UPPER(OPTION_TYPE_CODE) = 'ADMIN' AND UPPER(USER_CODE) = 'ADMIN' AND UPPER(OPTION_CODE) = ?1");
		q.setParameter(1, preference.toString().toUpperCase());
		String result = null;
		try {
			result = (String) q.getSingleResult();
		} catch (NoResultException e) {
			// Ignore these
			result = defValue;
		}
		return result;
	}
	
	@Transactional
	public String getSystemPreference(SystemPreference preference, String defValue) {
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createNativeQuery("SELECT VALUE FROM TB_OPTION WHERE " +
				"UPPER(OPTION_TYPE_CODE) = 'SYSTEM' AND UPPER(USER_CODE) = 'SYSTEM' AND UPPER(OPTION_CODE) = ?1");
		q.setParameter(1, preference.toString().toUpperCase());
		String result = null;
		try {
			result = (String) q.getSingleResult();
		} catch (NoResultException e) {
			// Ignore these
			result = defValue;
		}
		return result;
	}
	
	@Transactional
	public String getUserPreference(UserPreference preference, String defValue) {
		EntityManager entityManager=getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Query q = entityManager.createNativeQuery("SELECT VALUE FROM TB_OPTION WHERE " +
				"UPPER(OPTION_TYPE_CODE) = 'USER' AND UPPER(USER_CODE) = ?1 AND UPPER(OPTION_CODE) = ?2");
		q.setParameter(1, Auditable.currentUserCode().toUpperCase());
		q.setParameter(2, preference.toString().toUpperCase());
		String result = null;
		try {
			result = (String) q.getSingleResult();
		} catch (NoResultException e) {
			// Ignore these
			result = defValue;
		}
		return result;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getTimeZoneList() throws DataAccessException{
		return getJpaTemplate().find("SELECT lc FROM ListCodes lc WHERE lc.id.codeTypeCode = 'TIMEZONE' ORDER BY lc.id.codeCode ASC");
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getDefTransIndList() {
		return getJpaTemplate().find("SELECT lc FROM ListCodes lc WHERE lc.id.codeTypeCode in ('TRANSIND')");
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getDefTaxCodeList() {
		return getJpaTemplate().find("SELECT lc FROM ListCodes lc WHERE lc.id.codeTypeCode in ('TCTYPE')");
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getDefTaxTypeList() {
		return getJpaTemplate().find("SELECT lc FROM ListCodes lc WHERE lc.id.codeTypeCode in ('TAXTYPE')");
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ListCodes> getDefRateTypeList() {
		return getJpaTemplate().find("SELECT lc FROM ListCodes lc WHERE lc.id.codeTypeCode in ('RATETYPE')");
	}
}
