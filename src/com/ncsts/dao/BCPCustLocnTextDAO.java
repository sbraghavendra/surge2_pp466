package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPCustLocnText;
import com.ncsts.domain.BCPCustLocnTextPK;
import com.ncsts.domain.BatchMaintenance;

public interface BCPCustLocnTextDAO extends GenericDAO<BCPCustLocnText, BCPCustLocnTextPK> {
	public Long count(BatchMaintenance batch);
	public List<BCPCustLocnText> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
