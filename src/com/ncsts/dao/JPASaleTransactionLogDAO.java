package com.ncsts.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.seconddecimal.billing.domain.SaleTransactionLog;

public class JPASaleTransactionLogDAO extends JpaDaoSupport  implements SaleTransactionLogDAO {
	
	private static final Log logger = LogFactory
			.getLog(SaleTransactionLogDAO.class);

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Transactional(readOnly = false)
	public void save(SaleTransactionLog saleTransactionLog) {
		logger.debug("Saving saleTransactionLog log\n"+saleTransactionLog);
		entityManager.persist(saleTransactionLog);
	}

}
