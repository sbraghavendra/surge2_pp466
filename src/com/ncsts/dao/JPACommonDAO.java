package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;

/**
 * <b>Created:</b> Mar 23, 2008<br>
 * <b>Title:</b> JPACommonDAO<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * @todo add class overview here
 */

public class JPACommonDAO extends JpaDaoSupport implements CommonDAO{
	private Logger logger = LoggerFactory.getInstance().getLogger(JPACommonDAO.class);

    /**default empty constructor*/
    public JPACommonDAO() {
    }
    
    @SuppressWarnings("unchecked")
	public List<Object> getPage(Class<?> clazz, int startIndex, int endIndex, OrderBy orderBy,
                                    Object exampleInstance, String[] excludeProperties){
        logger.trace("BEGIN getPage(class, int, int, OrderBy, Object, String[]): " + clazz + " | " + startIndex + " | " + endIndex + " | " + orderBy + " | " + exampleInstance + " | " + excludeProperties);
        
        List<Object> returnList = new ArrayList<Object>();
        // TODO: Should probably move this to a util class
        Session session = createLocalSession();
        
        try {
            Criteria criteria = session.createCriteria(clazz);

            int maxResults = endIndex - startIndex;
            
            criteria.setFirstResult(startIndex);
            criteria.setMaxResults(maxResults);
            
            if (exampleInstance != null){
                Example example = Example.create(exampleInstance);
                
                if (excludeProperties != null){
                    for (String exclude : excludeProperties) {
                        example.excludeProperty(exclude);
                    }
                }
                
                criteria.add(example);
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            returnList = criteria.list();

        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the entities by example");
        }
        
        closeLocalSession(session);
        session=null;
        
        logger.trace("RETURNING returnList: " + returnList);
        return returnList;
    }
    
    /**
     * @see CommonDAO interface.
     */
    @SuppressWarnings("unchecked")
    @Transactional
	public List<Object> getPage(Class<?> clazz, int startIndex, int endIndex, OrderBy orderBy){
        logger.trace("BEGIN getPage()");
        if (startIndex < 0){
            logger.warn("startIndex < 0 - setting to 0"); 
            startIndex = 0;
        }
        if (endIndex < 0){
            logger.warn("endIndex < 0 - setting to 0");
            endIndex = 0;
        }
        String className = clazz.getCanonicalName();
        int maxResults = endIndex - startIndex;
        if (maxResults > 0){ // Only remove last row if the maxResults is greater than 0
            maxResults--; // Exclude last row (endIndex)
        }
        
        // Create the query string of the form
        // "from MyClass o order by MyClass.myField
        String queryString = "from " + className + " o ";
        
        Query query = getJpaTemplate().getEntityManagerFactory().createEntityManager()
            .createQuery(queryString + " " + orderBy.getHQLOrderByClause(className, "o"))
            .setFirstResult(startIndex)
            .setMaxResults(maxResults); // Use the query with the order by in place
        logger.trace("query: " + query);
        List<Object> results = query.getResultList();
        logger.trace("RETURNING results: " + results); 
        return results;
    }

    @Override
    @Transactional
    public Object getById(Class<?> clazz, Object primaryKey){
        return getJpaTemplate().find(clazz, primaryKey);
    }
    
    protected Session createLocalSession() {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
    
    public Long getTotalCount(Class<?> clazz, Object exampleInstance, String[] excludeProperties){
        logger.debug("BEGIN getTotalCount(Class, Object, String[]): " + clazz + " : " + exampleInstance + " | " + excludeProperties); 
       
        Session session = createLocalSession();
                
        Criteria crit = session.createCriteria(clazz);
        // Add the example and excludes
        if (exampleInstance != null){
            Example example = Example.create(exampleInstance);
            if (excludeProperties != null){
                for (String exclude : excludeProperties) {
                    example.excludeProperty(exclude);
                }
            }
            crit.add(example);
        }
        
        crit.setProjection(Projections.rowCount()); // We just want a row count
        
        Long returnLong = ((Number)crit.list().get(0)).longValue();

        closeLocalSession(session);
        session=null;

        return returnLong;
    }

    @Override
    @Transactional
    public Long getTotalCount(Class<?> clazz){
        String className = clazz.getName();
        Query query = getJpaTemplate().getEntityManagerFactory().createEntityManager()
            .createQuery("select count(*) from " + className); // Use * to avoid Hibernate error with count() on alias in HQL
        Long count = (Long)query.getSingleResult();
       
        return count.longValue();
    }
    
    @Transactional 
    public void persist(Object entity){
        getJpaTemplate().persist(entity);
    }




}
