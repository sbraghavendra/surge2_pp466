package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.DataDefinitionTable;

public interface DataDefinitionDAO {
	
	 public abstract List<DataDefinitionTable> getAllDataDefinitionTable() throws DataAccessException;
	 public abstract DataDefinitionTable findById(String tableName) throws DataAccessException;
	 public abstract DataDefinitionTable addNewDataDefinitionTable(DataDefinitionTable instance) throws DataAccessException;
	 public abstract void update(DataDefinitionTable dataDefinitionTable) throws DataAccessException;
	 public abstract void delete(String tableName) throws DataAccessException;
	 
	 public List<DataDefinitionColumn> getAllDataDefinitionColumn(DataDefinitionColumn exampleInstance, int count, String... excludeProperty) throws DataAccessException; 
	 public abstract List <DataDefinitionColumn> getAllDataDefinitionColumnByTable(String tableName) throws DataAccessException;
	 public abstract DataDefinitionColumn findByDataDefinitionColumnPK(DataDefinitionColumnPK dataDefinitionColumnPK) throws DataAccessException;
	 public List<DataDefinitionColumn> getAllDataDefinitionColumnByColumnName(String columnName); 
	 public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDesc(String columnName, String description, String dataType) throws DataAccessException;
	 public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDescDriver(String columnName, String description, String dataType,String driverCode) throws DataAccessException;
	 
	 public abstract DataDefinitionColumn addNewDataDefinitionColumn(DataDefinitionColumn instance) throws DataAccessException;
	 public abstract void update(DataDefinitionColumn dataDefinitionColumn) throws DataAccessException;
	 public abstract void delete(DataDefinitionColumnPK columnPK) throws DataAccessException;
	 
	 public abstract List<String> getColumnNameList(String tableName) throws DataAccessException;
	 public abstract List<String> getTableNameList() throws DataAccessException;
	 public abstract List<String> getDataTypeList() throws DataAccessException;
	 public abstract List<String> getDescriptionColumnList(String tableName) throws DataAccessException;
	 public abstract List<DataDefinitionTable> getExportDataDefinitionTables() throws DataAccessException;
	 public abstract DataDefinitionTable getDataDefinitionByConfigType(String configFileType) throws DataAccessException;

    List<DataDefinitionTable> getAllImportExportTypes() throws DataAccessException;

}