package com.ncsts.dao;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.DataStatistics;

public class JPADataStatisticsDAO extends JpaDaoSupport implements DataStatisticsDAO {
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	    Session localsession = (entityManager instanceof HibernateEntityManager) 
	                ? ((HibernateEntityManager) entityManager).getSession()
	                : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
    }
    
    protected void closeLocalSession(Session localSession) {
    	if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
    }
	
    @Transactional
	public DataStatistics findById(Long dataStatisticsId) throws DataAccessException {
	    return getJpaTemplate().find(DataStatistics.class, dataStatisticsId);
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DataStatistics> findAllDataStatisticsLines() throws DataAccessException {
	      return getJpaTemplate().find(" select dataStatistics from DataStatistics dataStatistics order by dataStatistics.dataStatisticsId ");	  
	}
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(DataStatistics dataStatistics) throws DataAccessException {
		//getJpaTemplate().merge(locationMatrix);
		Session session = createLocalSession();
        Transaction t = session.beginTransaction();
        t.begin();
        session.update(dataStatistics);
        t.commit();
 
        closeLocalSession(session);
        session=null;
	}	  

	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long dataStatisticsId)
			throws DataAccessException {
		DataStatistics ds = findById(dataStatisticsId);
		if(ds!=null){
			getJpaTemplate().remove(ds);
		}else{
			logger.info("Batch Maintenance Object not found for deletion");
		}
	}
	
	
	@Transactional
	public DataStatistics addNewStatisticsLine(DataStatistics instance) throws DataAccessException{
		
			if (instance.getDataStatisticsId() == null) {
				//TODO Handle this case properly later 
               logger.error(" Can not inser Null Values");
			}
			getJpaTemplate().persist(instance);	
			getJpaTemplate().flush();
			return instance;

	}
	
	public boolean isValidWhereClause(String whereClause){
		String whereClauseKeys[] = {"ALTER", "CREATE", "DROP", "GRANT", "PURGE", "RENAME", "REVOKE", "TRUNCATE", "DELETE",
						   "INSERT", "LOCK", "MERGE", "UPDATE" };
	    List<String> whereClauseList = Arrays.asList(whereClauseKeys);
	    for(String invalidkey : whereClauseList){
	    	if(whereClause.toLowerCase().contains(invalidkey.toLowerCase())) {
	    		return false;
	    	}
	    }
		String sql = "SELECT PURCHTRANS_ID FROM TB_PURCHTRANS WHERE (PURCHTRANS_ID = -1) AND " + whereClause;
		Session session = createLocalSession();
		try{
			org.hibernate.Query query = session.createSQLQuery(sql);
			query.list();
			return true;
		}
		catch(HibernateException e){
			return false;
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
	}
    public boolean isValidGroupByOnDrilldown(String groupByOnDrilldown){
    	String sql = "SELECT count(*) FROM TB_PURCHTRANS WHERE (PURCHTRANS_ID = -1) GROUP BY " + groupByOnDrilldown ;
  
    	Session session = createLocalSession();
		try{
			org.hibernate.Query query = session.createSQLQuery(sql);
			query.list();
			return true;
		}
		catch(HibernateException e){
			return false;
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
    }
}
