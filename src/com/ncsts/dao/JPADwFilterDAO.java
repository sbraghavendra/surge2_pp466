package com.ncsts.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.CustLocn;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.SitusMatrixAdmin;
import com.ncsts.domain.TransactionHeader;

public class JPADwFilterDAO extends JpaDaoSupport implements DwFilterDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DwFilter> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
		//return getJpaTemplate().find("select distinct df.dwFilterPK.filterName from DwFilter df where df.dwFilterPK.windowName=? and (df.dwFilterPK.userCode='*GLOBAL' or df.dwFilterPK.userCode=?) order by df.dwFilterPK.filterName", windowName, userCode);

		Session session = null;
		Transaction tx = null;
		org.hibernate.Query query = null;
		List<DwFilter> dwFilters = new ArrayList<DwFilter>();
		try {
			session = createLocalSession();
			query = session.createQuery("select df.dwFilterPK.windowName, df.dwFilterPK.userCode, df.dwFilterPK.filterName from DwFilter df where df.dwFilterPK.windowName=:windowName and (df.dwFilterPK.userCode='*GLOBAL' or df.dwFilterPK.userCode=:userCode) group by df.dwFilterPK.windowName, df.dwFilterPK.filterName, df.dwFilterPK.userCode order by df.dwFilterPK.userCode, df.dwFilterPK.filterName");
			query.setParameter("windowName",windowName);
			query.setParameter("userCode",userCode);

			DwFilter dwFilter = null;
			List<String[]> objArrayList = (List<String[]>) query.list();
			for(Iterator<?> pairs  = objArrayList.iterator(); pairs.hasNext();){
				Object[] pair = (Object[]) pairs.next();				
				dwFilter = new DwFilter();	
				dwFilter.setUserCode((String)pair[1].toString());
				dwFilter.setFilterName((String)pair[2].toString());
				dwFilters.add(dwFilter);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session = null;
		}
		
		return dwFilters;

	}
	
	//public List<String> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
	//	return getJpaTemplate().find("select distinct df.dwFilterPK.filterName from DwFilter df where df.dwFilterPK.windowName=? and (df.dwFilterPK.userCode='*GLOBAL' or df.dwFilterPK.userCode=?) order by df.dwFilterPK.filterName", windowName, userCode);
	//}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException {
		return getJpaTemplate().find("select distinct df.dwFilterPK.filterName from DwFilter df where df.dwFilterPK.windowName=? and df.dwFilterPK.userCode=? order by df.dwFilterPK.filterName", windowName, userCode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DwFilter> getColumnsByFilterNameandUser(String windowName, String userCode, String filterName) throws DataAccessException {
		return getJpaTemplate().find("select df from DwFilter df where df.dwFilterPK.windowName=? and df.dwFilterPK.filterName=? and df.dwFilterPK.userCode=?", windowName, filterName, userCode);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getUserByFilterNameandUserCode(String windowName, String filterName,  String userCode) throws DataAccessException {
		List<String> list = getJpaTemplate().find("select distinct df.dwFilterPK.userCode from DwFilter df where df.dwFilterPK.windowName=? and df.dwFilterPK.filterName=? and df.dwFilterPK.userCode=?", windowName, filterName, userCode);
		if(list!=null && list.size()>0){
			return (String)list.get(0);
		}
		else{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			String hql = "delete DwFilter df where df.dwFilterPK.windowName=:windowName and df.dwFilterPK.filterName=:filterName and df.dwFilterPK.userCode=:userCode";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("filterName",filterName);
	        query.setString("userCode",userCode);
	        int rowCount = query.executeUpdate();
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void updateColumnsByFilterNameandUser(String windowName, String filterName, String userCode, List<DwFilter> dwFilters) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			//Delete columns
			String hql = "delete DwFilter df where df.dwFilterPK.windowName=:windowName and df.dwFilterPK.filterName=:filterName and df.dwFilterPK.userCode=:userCode";
			org.hibernate.Query query = session.createQuery(hql);
	        query.setString("windowName",windowName);
	        query.setString("filterName",filterName);
	        query.setString("userCode",userCode);
	        int rowCount = query.executeUpdate();
	        
	        //Add columns
	        for(DwFilter dwFilter : dwFilters){
	        	session.saveOrUpdate(dwFilter);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void addColumnsByFilterName(String windowName, String filterName, List<DwFilter> dwFilters) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
	        //Add columns
	        for(DwFilter dwFilter : dwFilters){
	        	session.save(dwFilter);
	        }
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();		
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException {
		Session session = createLocalSession();
		String hql = "select count(*) from DwFilter df where df.dwFilterPK.windowName=:windowName and df.dwFilterPK.filterName=:filterName";
		org.hibernate.Query query = session.createQuery(hql);
        query.setString("windowName",windowName);
        query.setString("filterName",filterName);     
        Long count = ((Long)query.uniqueResult());
        closeLocalSession(session);
		session=null;
		
		if(count==0L){
			return false;
		}
		else{
			return true;
		}
	}
}
