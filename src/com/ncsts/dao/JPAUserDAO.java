package com.ncsts.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.User;

public class JPAUserDAO extends JpaDaoSupport implements UserDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}

	@Transactional
	public User findById(String userCode) throws DataAccessException {
	  return getJpaTemplate().find(User.class, userCode);
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<User> findAllUsers() throws DataAccessException {
	      return getJpaTemplate().find(" select user from User user");	  
	  }
	  
	@SuppressWarnings("deprecation")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void remove(String id) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			org.hibernate.Query query = session.createQuery("delete from UserEntity ue where ue.userEntityPK.userCode = '"+id+"'");
			query.executeUpdate();
			query = session.createQuery("delete from UserMenuEx ume where ume.userMenuExPk.userCode = '"+id+"'");
			query.executeUpdate();
			query = session.createQuery("delete from User u where u.userCode = '"+id+"'");
			int row = query.executeUpdate();

			if (row > 0) {
				org.hibernate.Query query1 = session.createQuery("delete from Option u where u.id.userCode = '"+id+"'");
				query1.executeUpdate();
				org.hibernate.Query query2 = session.createQuery("delete from TransactionHeader uh where uh.dwColumnPK.userCode = '"+id+"'");
				query2.executeUpdate();					
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		}
		
		closeLocalSession(session);
		session=null;
	}	
	  
	  public void saveOrUpdate (User user) throws DataAccessException {	
		  Session session = createLocalSession();
	      Transaction t = session.beginTransaction();
	      t.begin();
	      session.saveOrUpdate(user);
	      t.commit();
	      
	      closeLocalSession(session);
	      session=null;	
	  }	
		
	  @Transactional
	  public String persist (User user) throws DataAccessException {
	      getJpaTemplate().persist(user);
		  String key = user.getUserDTO().getUserCode();
		  return key;
	  }  
	  
	  @SuppressWarnings("deprecation")
	  public void updateUserLogoff(String userCode) throws DataAccessException{
		  Session session = createLocalSession();
		  Transaction t = session.beginTransaction();
		  
		  Timestamp logoffTime = new Timestamp(new Date().getTime());
		  logger.info(userCode + " logoff at " + logoffTime.toString());
		    
		  try {
			// PDB - method is deprecated, but no alternative has been provided yet
			  PreparedStatement stmt = session.connection().prepareStatement("UPDATE TB_USER SET last_logoff = ? WHERE user_code = ?");
			  stmt.setTimestamp(1, logoffTime);
			  stmt.setString(2, userCode.toUpperCase());
			  stmt.execute();
			  t.commit();
		  }
		  catch (SQLException e){}	
		  
		  closeLocalSession(session);
		  session=null;
	  }
	  
	 @SuppressWarnings("unchecked")
	 @Transactional(readOnly=true)
	 public String getUserLogoff(String userCode) throws DataAccessException{
		 String logoffTime = "";
		 String sql = "SELECT TO_CHAR(last_logoff, 'mm/dd/rr hh24/mi/ss') AS last_logoff FROM tb_user WHERE user_code = :userCode";
			
		 Session session = createLocalSession();
         org.hibernate.Query query = session.createSQLQuery(sql);
        	
       	 query.setParameter("userCode", userCode);
    	 List<String> timeList = query.list();
		
         if(timeList!=null && timeList.size()>0){
			logoffTime = (String)timeList.get(0);
         }
         
         closeLocalSession(session);
         session=null;
				
		 return logoffTime;
	} 
	 
	 public static String deleteUserInfo(Connection conn, String userCode){
			String errorMessage = "";
			PreparedStatement prep = null;
			try{  
				conn.setAutoCommit(false);
					
				//PP-263
				prep = conn.prepareStatement("DELETE TB_USER_MENU_EX WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate(); 
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_USER_ENTITY WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate(); 
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_IMPORT_SPEC_USER WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate(); 
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_DW_COLUMN WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate(); 
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_DW_FILTER WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate();
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_USER WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate();
				prep.close();
				
				prep = conn.prepareStatement("DELETE TB_OPTION WHERE UPPER(USER_CODE) = ?");
				prep.setString(1,userCode.toUpperCase());
				prep.executeUpdate();
				prep.close();
				
				conn.commit();
				conn.setAutoCommit(true);
			}
			catch(Exception e){
				try{
					conn.rollback();
				}
				catch(Exception ee){		
				}
				errorMessage = e.getMessage();
			    e.printStackTrace();
			}
			finally {
				if(prep!=null){try{prep.close();}catch(Exception ex){}}
			}	

			return errorMessage;
		}

}
