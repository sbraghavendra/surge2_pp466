package com.ncsts.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.EntityDefault;

public class JPAEntityDefaultDAO extends JPAGenericDAO<EntityDefault,Long> implements EntityDefaultDAO {

	public Long count(Long entityId) throws DataAccessException {
	 	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createQuery(" select count(*) from EntityDefault entityDefault where entityDefault.entityId = ?");
	  	query.setParameter(1, entityId );
	    Long count = (Long) query.getSingleResult();
		return count;
	}
  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityDefault> findAllEntityDefaults() throws DataAccessException {
	    return getJpaTemplate().find(" select entityDefault from EntityDefault entityDefault");	  
	}
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityDefault> findAllEntityDefaultsByEntityId(Long entityId) throws DataAccessException {
		return getJpaTemplate().find(" select entityDefault from EntityDefault entityDefault where entityDefault.entityId = ? order by entityDefault.defaultCode", entityId);	  
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<EntityDefault> findEntityDefaultsByDefaultCode(Long entityId, String defaultCode) throws DataAccessException {
		return getJpaTemplate().find(" select entityDefault from EntityDefault entityDefault where entityDefault.entityId = ? and entityDefault.defaultCode = ?", entityId, defaultCode);	  
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public void findAllEntityDefaultsByEntityIdAsMap(Long entityId, HashMap<String, String> entityDefaults) throws DataAccessException {
		List<EntityDefault> list = findAllEntityDefaultsByEntityId(entityId);
		//HashMap<String, String> result = null;
		if (list == null || list.size() == 0 || entityDefaults == null) {
			return;
		}
		for (EntityDefault ed: list) {
			if (ed != null && ed.getDefaultCode() != null)
				entityDefaults.put(ed.getDefaultCode().toUpperCase(), ed.getDefaultValue());
		}

	}


	@SuppressWarnings("deprecation")
	@Transactional
	public Long persist(EntityDefault entityDefault) throws DataAccessException {
		getJpaTemplate().persist(entityDefault);
		getJpaTemplate().flush();
		Long entityDefaultId = entityDefault.getEntityDefaultId();
		
		return entityDefaultId;
	}	
	  
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(Long id) throws DataAccessException {
		EntityDefault entityDefault = getJpaTemplate().find(EntityDefault.class, id);
		if( entityDefault != null){
			getJpaTemplate().remove(entityDefault);
		}else{
			logger.warn("EntityDefault Object not found for deletion");
		}
	} 
	  
	public void saveOrUpdate (EntityDefault entityDefault) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.update(entityDefault);
		t.commit();
		closeLocalSession(session);
		session=null;		
	}
	
	@Transactional
	public void deleteEntity(Long entityId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			//remove(TB_ENTITY);
			String hqlDelete = "DELETE TB_ENTITY_DEFAULT WHERE ENTITY_ID = " + entityId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			// Delete all existing TB_NEXUS_DEF
			//hqlDelete = "DELETE FROM TB_NEXUS_DEF WHERE ENTITY_ID = " + entityId.intValue();
			//query = session.createSQLQuery(hqlDelete);
			//query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to execute deleteTEntity: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	  
}


