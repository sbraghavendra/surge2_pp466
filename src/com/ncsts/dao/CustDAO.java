package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;

public interface CustDAO extends GenericDAO<Cust,Long> {

	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<Cust> find(Criteria criteria);
	public abstract List<Cust> find(DetachedCriteria criteria);
	public abstract Cust find(Cust exampleInstance);
	public abstract List<Cust> find(Cust exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public Long count(Cust exampleInstance);

	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
	public void deleteCustEntityList(Cust updateCust) throws DataAccessException;
	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException;
	public CustEntity findCustEntity(Long custId, Long entityId) throws DataAccessException;
	public Cust getCustByNumber(String custNbr) throws DataAccessException;
	public void addCustEntity(Long custId, Long entityId) throws DataAccessException;
	public void deleteCustEntity(Long custId, Long entityId) throws DataAccessException;
	
	
	public List<CustCert> find(CustCert exampleInstance,  OrderBy orderBy, int firstRow, int maxResults);
	public Long count(CustCert exampleInstance);
	
	public List<Cust> findAllCust(Cust exampleInstance) throws DataAccessException;
	public boolean isAllowDeleteCustomer(String custNbr) throws DataAccessException;
	public void disableActiveCustomer(String custNbr) throws DataAccessException;
	
	public Cust getCustByName(String custName) throws DataAccessException;
	public int getCustCountWithReasonCode(String custNbr, String locnCode, String reasonCode, Date glDate) throws DataAccessException;
  	public int getCustCountWithReasonCodeAndCertNbr(String custNbr, String locnCode, String certNbr, String reasonCode, Date glDate) throws DataAccessException;
  	public int getCustCount(String custNbr, String locnCode, Date glDate) throws DataAccessException;
  	public int getCustCountWithExAndState(String custNbr, String state, Date glDate) throws DataAccessException;
  	public int getCustCountWithCertNbr(String custNbr, String locnCode, String certNbr, Date glDate) throws DataAccessException;
  	public int getCustCountWithCertNbrWithoutLocnCode(String custNbr, String certNbr, Date glDate, String geocode, String addressLine1, String addressLine2,
			String city, String county, String state, String zip, String country) throws DataAccessException;
}
