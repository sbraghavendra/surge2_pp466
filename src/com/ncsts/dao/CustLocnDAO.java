package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;

public interface CustLocnDAO extends GenericDAO<CustLocn,Long> {

	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<CustLocn> find(Criteria criteria);
	public abstract List<CustLocn> find(DetachedCriteria criteria);
	public abstract CustLocn find(CustLocn exampleInstance);
	public abstract List<CustLocn> find(CustLocn exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public Long count(CustLocn exampleInstance);
	public List<CustLocn> findAllCustLocn(CustLocn exampleInstance) throws DataAccessException;
	public CustLocn getCustLocnByCustId(Long custId, String custLocnCode) throws DataAccessException;
	public CustLocn getCustLocnByCustLocnCode(String custLocnCode) throws DataAccessException;
	public boolean isAllowDeleteCustLocn(String custLocnCode) throws DataAccessException;

  	public abstract Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz);
  	
  	public boolean deleteBatch(Long batchId);
  	
	public CustLocn getCustLocnByName(String locationName) throws DataAccessException;
  	public void createTempExemptionWithReason(Long custLocnId, Long entityId, int exemptionGraceDays, String reasonCode, String user) throws DataAccessException;
  	public Long getNextCustLocnId() throws DataAccessException;
  	public void createCustLocn(Long custLocnId, Long custId, String custLocnCode, String locationName, String geocode, 
			String addressLine1, String addressLine2, String city, String county, String state, String zip, String zipplus4,
			String country, String user) throws DataAccessException;
}
