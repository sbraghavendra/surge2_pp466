package com.ncsts.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPTaxCodeRuleText;
import com.ncsts.domain.BCPTaxCodeRuleTextPK;
import com.ncsts.domain.BatchMaintenance;

public class JPABCPTaxCodeRuleTextDAO extends JPAGenericDAO<BCPTaxCodeRuleText, BCPTaxCodeRuleTextPK> implements BCPTaxCodeRuleTextDAO {

	@Override
	public Long count(BatchMaintenance batch) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPTaxCodeRuleText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setProjection(Projections.rowCount());
		List< ? > result = criteria.list();
			
		Long returnLong = ((Number) result.get(0)).longValue();
		closeLocalSession(session);
		session=null;
		
		return returnLong;
	}

	@Override
	public List<BCPTaxCodeRuleText> getPage(BatchMaintenance batch,
			int firstRow, int maxResults) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(BCPTaxCodeRuleText.class);
		criteria.add(Restrictions.eq("id.batchId", batch.getBatchId()));
		
		criteria.setFirstResult(firstRow);
		if (maxResults > 0) {
			criteria.setMaxResults(maxResults);
		}
		criteria.addOrder(Order.asc("id.line"));
		try {
			List<BCPTaxCodeRuleText> returnBCPTaxCodeRuleText = criteria.list();
			closeLocalSession(session);
			session=null;
			
			return returnBCPTaxCodeRuleText;	
		} 	catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
			return new ArrayList<BCPTaxCodeRuleText>();
		}
	}
}
