/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LogMemory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.dto.BatchDTO;
import com.ncsts.view.util.HibernateUtils;
import com.ncsts.view.util.ImportMapUploadParser;
import com.ncsts.view.util.TaxRateUploadParser;


public class JPATaxHolidayDAO extends JPAGenericDAO<TaxHoliday, String> implements 
		TaxHolidayDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	@SuppressWarnings("unused")
	private TaxHolidayDAO taxHolidayDAO;

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxHoliday> getAllTaxHolidayData()
	throws DataAccessException {
		// TODO Auto-generated method stub
		return getJpaTemplate().find("select bm from TaxHoliday bm order by bm.taxHolidayCode");
	}
	
	@Transactional
	public TaxHoliday findByTaxHolidayCode(String taxHolidayCode) throws DataAccessException{
		return getJpaTemplate().find(TaxHoliday.class, taxHolidayCode);
	}	
	
	public Session getCurrentSession(){
		return createLocalSession();
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TaxHoliday> findByTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException {
		List<TaxHoliday> list = null;
		
		logger.debug(" BatchManitenance Search from DAO ::::");
		logger.info("Entry stamp time "+ taxHoliday.getTaxHolidayCode());
		logger.info("Batch status code "+ taxHoliday.getTaxcodeCountryCode());
		logger.info("Batch type code "+ taxHoliday.getTaxcodeStateCode());
		logger.info("Batch type code "+ taxHoliday.getDescription());
		logger.info("Batch type code "+ taxHoliday.getEffectiveDate().toString());
		logger.info("Batch type code "+ taxHoliday.getExpirationDate().toString());

		if(taxHoliday.getTaxHolidayCode() == null || taxHoliday.getTaxHolidayCode().length()==0){
			
			logger.debug(" Search inside batchMaintenance.getBatchId() == null from DAO1 ::::");
		
			Session localSession = createLocalSession();
            try {
            	Criteria criteria = localSession.createCriteria(TaxHoliday.class) ;
            	if (taxHoliday.getEffectiveDate() != null) {
                    criteria.add(Expression.ge("effectiveDate",
                                               taxHoliday.getEffectiveDate()));
                } 
            	if (taxHoliday.getExpirationDate() != null) {
                    criteria.add(Expression.le("expirationDate",
                                               taxHoliday.getExpirationDate()));
                } 
                if (taxHoliday.getTaxcodeCountryCode() != null) {
                    criteria.add(Expression.eq("taxcodeCountryCode",
                                           taxHoliday.getTaxcodeCountryCode()));
                }                
                if (taxHoliday.getTaxcodeStateCode() != null) {
                     criteria.add(Expression.eq("taxcodeStateCode",taxHoliday.getTaxcodeStateCode()));
                }  
                
                
                if (taxHoliday.getTaxHolidayCode()!=null && taxHoliday.getTaxHolidayCode().length()>0) {
                    criteria.add(Expression.ilike("taxHolidayCode",taxHoliday.getTaxHolidayCode()));
                } 
                if (taxHoliday.getDescription()!=null && taxHoliday.getDescription().length()>0) {
                    criteria.add(Expression.ilike("description",taxHoliday.getDescription()));
                }          
                
                criteria.addOrder(Order.desc("taxHolidayCode"));
                list = criteria.list();
            }
            catch (HibernateException hbme) {
                 //String message = "Error in finding the entities by example";
            }
            
            closeLocalSession(localSession);
            localSession=null;
				
            LogMemory.executeGC();  
		}else{
			list = new ArrayList<TaxHoliday>();
			list.add(findByTaxHolidayCode(taxHoliday.getTaxHolidayCode()));
			
		}
        if( list != null)logger.debug(" TaxHoliday list size is :"+list.size());        
		return list; 		
	}

	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(TaxHoliday taxHoliday) throws DataAccessException {
		//getJpaTemplate().merge(locationMatrix);
		//List<String> find = listCodeCheck("BATCHSTAT");
		//List<String> findErr = listCodeCheck("ERRORSEV");
		//if((	find.contains(taxHoliday.getBatchStatusCode()) 
		//		&& ( (taxHoliday.getErrorSevCode()==null)||( taxHoliday.getErrorSevCode().length()==1)) )
		//		||taxHoliday.getErrorSevCode().length()==0 
		//		|| findErr.contains(taxHoliday.getErrorSevCode())){
			
			Session session = createLocalSession();
            Transaction t = session.beginTransaction();
            t.begin();
            session.update(taxHoliday);
            t.commit();
            
            //Prevent memory leak
            closeLocalSession(session);
            session=null;
		//}
		//else{
		//	logger.info("Batch Status Code/Error Seviority Code not in the tb_listCode");
		//}
	}
	
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(String taxHolidayCode)
			throws DataAccessException {
		TaxHoliday bm = findByTaxHolidayCode(taxHolidayCode);
		if(bm!=null){
			getJpaTemplate().remove(bm);
		}else{
			logger.info("TaxHoliday Object not found for deletion");
		}
	}
	
	/**
	 * @author Anand
	 */
	@Transactional
	public TaxHoliday saveTaxHoliday(TaxHoliday taxHoliday)
			throws DataAccessException {
		taxHoliday.setTaxHolidayCode(null); //new record
		getJpaTemplate().persist(taxHoliday);
		getJpaTemplate().flush();
		return taxHoliday;
	}
	
	@Transactional
	public TaxHoliday getTaxHolidayData(String taxHolidayCode) throws DataAccessException {
		// TODO Auto-generated method stub
		return getJpaTemplate().find(TaxHoliday.class, taxHolidayCode);
	}
	
	@Transactional
	public void executeTaxHolidayDetails(String taxHolidayCode) throws DataAccessException {

		TaxHoliday foundTaxHoliday = findByTaxHolidayCode(taxHolidayCode);

		//Not found or executed
		if (foundTaxHoliday == null || foundTaxHoliday.getTaxHolidayCode() == null || foundTaxHoliday.getTaxHolidayCode().length() == 0
				|| foundTaxHoliday.getExecuteBooleanFlag()) {
			return;
		}
		Session localSession = createLocalSession();


		List<TaxHolidayDetail> taxHolidayDetailList  = (List<TaxHolidayDetail>)localSession
				.createQuery(" from TaxHolidayDetail where id.taxHolidayCode=:taxHolidayCode")
				.setParameter("taxHolidayCode", taxHolidayCode)
				.list();

		//q.setParameter(1, taxHolidayCode);
		//List<TaxHolidayDetail> taxHolidayDetailList = (List<TaxHolidayDetail>) q.getResultList();

		String queryDetailStr = "SELECT td.TAXCODE_TYPE_CODE, td.OVERRIDE_TAXTYPE_CODE, td.RATETYPE_CODE, " +
				"td.TAXABLE_THRESHOLD_AMT, td.TAXABLE_LIMITATION_AMT, " +
				"td.BASE_CHANGE_PCT, td.SPECIAL_RATE, td.ACTIVE_FLAG, td.CUSTOM_FLAG, "+
				"   (CASE td.JUR_LEVEL WHEN '*STATE' THEN 1 " +
				"					WHEN '*COUNTY' THEN 2 " +
				"					WHEN '*CITY' THEN 3 " +
				"					WHEN '*STJ' THEN 4 END)  as jur_orderby " +
				"FROM TB_TAXCODE_DETAIL td " +
				"WHERE td.ACTIVE_FLAG ='1' " +
				"	AND td.TAXCODE_CODE =:taxcodeCode " +
				"	AND td.TAXCODE_COUNTRY_CODE =:taxcodeCountryCode " +
				"	AND (td.TAXCODE_STATE_CODE =:taxcodeStateCode OR td.TAXCODE_STATE_CODE ='*ALL') " +
				"	AND (td.TAXCODE_COUNTY =:taxCodeCounty OR td.TAXCODE_COUNTY = '*ALL') " +
				"	AND (td.TAXCODE_CITY =:taxCodeCity OR td.TAXCODE_CITY = '*ALL') " +
				"	AND (td.TAXCODE_STJ =:taxCodeStj OR td.TAXCODE_STJ = '*ALL') " +
				"	AND (td.JUR_LEVEL =:jurLevel OR td.JUR_LEVEL = '*STATE') " +
				"	AND td.EFFECTIVE_DATE <=:effectiveDate " +
				"	AND td.EXPIRATION_DATE >=:expirationDate " +
				"  AND td.MODIFIED_FLAG <>2 " +
				"ORDER BY jur_orderby DESC, td.SORT_NO DESC, td.EFFECTIVE_DATE DESC";
		//Query queryDetail = entityManager.createQuery(" select td, (CASE jurLevel WHEN '*STATE' THEN 1 WHEN '*COUNTY' THEN 2 WHEN '*CITY' THEN 3 WHEN '*STJ' THEN 4 END)  as jur_orderby from TaxCodeDetail td where  td.activeFlag = '1' and td.taxcodeCode =? and td.taxcodeCountryCode =? and (td.taxcodeStateCode =? or td.taxcodeStateCode ='*ALL' ) and td.effectiveDate <=?  and td.expirationDate >=? and td.taxCodeCounty='*ALL' and td.taxCodeCity='*ALL' and td.taxCodeStj='*ALL' ORDER BY jur_orderby DESC");

		Transaction tx = null;

		try {
			tx = localSession.beginTransaction();

			int fetchRows = 0;

			for (TaxHolidayDetail taxHolidayDetail : taxHolidayDetailList) {
				Object[] selectedTaxCodeDetail = null;
				if (taxHolidayDetail.getExceptInd() != null && taxHolidayDetail.getExceptInd().equals("1")) {
					fetchRows = 0;
					//Query queryDetail = entityManager.createQuery(queryDetailStr);
					org.hibernate.Query queryDetail = localSession.createSQLQuery(queryDetailStr);

					queryDetail.setParameter("taxcodeCode", taxHolidayDetail.getTaxcodeCode());
					queryDetail.setParameter("taxcodeCountryCode", foundTaxHoliday.getTaxcodeCountryCode());
					queryDetail.setParameter("taxcodeStateCode", foundTaxHoliday.getTaxcodeStateCode());
					queryDetail.setParameter("taxCodeCounty", taxHolidayDetail.getTaxcodeCounty());           // <--- Added this
					queryDetail.setParameter("taxCodeCity", taxHolidayDetail.getTaxcodeCity());             // <--- Added this
					queryDetail.setParameter("taxCodeStj", taxHolidayDetail.getTaxcodeStj());              // <--- Added this
					queryDetail.setParameter("jurLevel", taxHolidayDetail.getJurLevel());                // <--- Added this
					queryDetail.setParameter("effectiveDate", foundTaxHoliday.getEffectiveDate());            // <--- Moved this
					queryDetail.setParameter("expirationDate", foundTaxHoliday.getExpirationDate());           // <--- Moved this


					List<Object[]> taxCodeDetailList = (List<Object[]>) queryDetail.list();

					if (taxCodeDetailList != null && taxCodeDetailList.size() > 0) {
						fetchRows = taxCodeDetailList.size();
						selectedTaxCodeDetail = taxCodeDetailList.get(0);

					}

					if (fetchRows > 0) {
						//IF FOUND, WRITE TAXCODE DETAIL ROW
						//INSERT INTO tb_taxcode_detail
						TaxCodeDetail aTaxCodeDetail = new TaxCodeDetail();
						aTaxCodeDetail.setTaxcodeCode(taxHolidayDetail.getTaxcodeCode());
						aTaxCodeDetail.setTaxcodeCountryCode(foundTaxHoliday.getTaxcodeCountryCode());
						aTaxCodeDetail.setTaxcodeStateCode(foundTaxHoliday.getTaxcodeStateCode());
						aTaxCodeDetail.setTaxCodeCounty(taxHolidayDetail.getTaxcodeCounty());
						aTaxCodeDetail.setTaxCodeCity(taxHolidayDetail.getTaxcodeCity());
						aTaxCodeDetail.setTaxCodeStj(taxHolidayDetail.getTaxcodeStj());
						aTaxCodeDetail.setJurLevel(taxHolidayDetail.getJurLevel());
						aTaxCodeDetail.setEffectiveDate(foundTaxHoliday.getEffectiveDate());
						aTaxCodeDetail.setExpirationDate(foundTaxHoliday.getExpirationDate());

						aTaxCodeDetail.setTaxcodeTypeCode((String)selectedTaxCodeDetail[0]);
						aTaxCodeDetail.setTaxtypeCode((String)selectedTaxCodeDetail[1]);//OVERRIDE_TAXTYPE_CODE
						aTaxCodeDetail.setRateTypeCode((String)selectedTaxCodeDetail[2]);
						aTaxCodeDetail.setTaxableThresholdAmt((BigDecimal)selectedTaxCodeDetail[3]);
						aTaxCodeDetail.setBaseChangePct((BigDecimal)selectedTaxCodeDetail[4]);
						aTaxCodeDetail.setSpecialRate((BigDecimal)selectedTaxCodeDetail[5]);
						String acFlagStrValue =  (selectedTaxCodeDetail[6]!=null) ? ((Character)selectedTaxCodeDetail[6]).toString() : null;
						aTaxCodeDetail.setActiveFlag(acFlagStrValue);
						String cusFlagStrValue =  (selectedTaxCodeDetail[7]!=null) ? ((Character)selectedTaxCodeDetail[7]).toString() : null;
						aTaxCodeDetail.setCustomFlag(cusFlagStrValue);
						aTaxCodeDetail.setModifiedFlag("2");
						aTaxCodeDetail.setSortNo(taxHolidayDetail.getSortNo());
						localSession.save(aTaxCodeDetail);
						localSession.flush();
					}

				} else {
					//ELSE NOT AN EXCEMPTION, WRITE TAX HOLIDAY TAXCODE
					//INSERT INTO tb_taxcode_detail
					TaxCodeDetail aTaxCodeDetail = new TaxCodeDetail();
					aTaxCodeDetail.setTaxcodeCode(taxHolidayDetail.getTaxcodeCode());
					aTaxCodeDetail.setTaxcodeCountryCode(foundTaxHoliday.getTaxcodeCountryCode());
					aTaxCodeDetail.setTaxcodeStateCode(foundTaxHoliday.getTaxcodeStateCode());
					aTaxCodeDetail.setTaxCodeCounty(taxHolidayDetail.getTaxcodeCounty());
					aTaxCodeDetail.setTaxCodeCity(taxHolidayDetail.getTaxcodeCity());
					aTaxCodeDetail.setTaxCodeStj(taxHolidayDetail.getTaxcodeStj());
					aTaxCodeDetail.setJurLevel(taxHolidayDetail.getJurLevel());
					aTaxCodeDetail.setEffectiveDate(foundTaxHoliday.getEffectiveDate());
					aTaxCodeDetail.setExpirationDate(foundTaxHoliday.getExpirationDate());
					aTaxCodeDetail.setTaxcodeTypeCode(foundTaxHoliday.getTaxcodeTypeCode());
					//aTaxCodeDetail.setTaxtypeCode(foundTaxHoliday.getTaxcodeTypeCode());//OVERRIDE_TAXTYPE_CODE
					aTaxCodeDetail.setTaxtypeCode(foundTaxHoliday.getOverrideTaxtypeCode());
					aTaxCodeDetail.setRateTypeCode(foundTaxHoliday.getRatetypeCode());
					aTaxCodeDetail.setTaxableThresholdAmt(foundTaxHoliday.getTaxableThresholdAmt());
					aTaxCodeDetail.setBaseChangePct(foundTaxHoliday.getBaseChangePct());
					aTaxCodeDetail.setSpecialRate(foundTaxHoliday.getSpecialRate());
					aTaxCodeDetail.setActiveFlag("1");
					aTaxCodeDetail.setCustomFlag("0");
					aTaxCodeDetail.setModifiedFlag("2");
					aTaxCodeDetail.setSortNo(taxHolidayDetail.getSortNo());

					localSession.save(aTaxCodeDetail);
					localSession.flush();
				}
			}

			foundTaxHoliday.setExecuteFlag("1");
			localSession.update(foundTaxHoliday);
			tx.commit();
			
		} catch (Exception e) {
			logger.error("Failed to execute: " + e);
			e.printStackTrace();

			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(localSession);
		}
	}
	
	@Transactional
	public void addTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, boolean copyMapFlag) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.save(taxHoliday);
			session.flush();
			
			if(copyMapFlag){
				// Now do an insert for each user
				query = session.createSQLQuery("INSERT INTO TB_TAX_HOLIDAY_DETAIL (TAX_HOLIDAY_CODE, TAXCODE_CODE, TAXCODE_COUNTY, TAXCODE_CITY, TAXCODE_STJ, JUR_LEVEL, SORT_NO, EXCEPT_IND) VALUES (:holidayCode, :taxCode, :county, :city, :taxcodeStj, :jurLevel, :sortNo, :exceptInd)");
				for (TaxHolidayDetail taxHolidayDetail : taxHolidayDetailList) {
					query.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
					query.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
					query.setParameter("county", taxHolidayDetail.getTaxcodeCounty());
					query.setParameter("city", taxHolidayDetail.getTaxcodeCity());
					query.setParameter("taxcodeStj", taxHolidayDetail.getTaxcodeStj());
					query.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
					query.setParameter("sortNo", taxHolidayDetail.getSortNo());
					query.setParameter("exceptInd", taxHolidayDetail.getExceptInd());

					query.executeUpdate();
				}
			}
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_TAX_HOLIDAY_DETAIL: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(taxHoliday);
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_TAX_HOLIDAY_DETAIL: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateTaxHolidayExceptionDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, TaxHolidayDetail selectedTaxCodeDetail) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(taxHoliday);

			// First delete all existing taxholidaydetail
			String hqlDelete = "delete TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE = '"+taxHoliday.getTaxHolidayCode().toUpperCase() + "'";
			if(selectedTaxCodeDetail.getTaxcodeCode()!=null && selectedTaxCodeDetail.getTaxcodeCode().length()>0){
				hqlDelete = hqlDelete + " AND upper(TAXCODE_CODE) = '" + selectedTaxCodeDetail.getTaxcodeCode() + "'";
				hqlDelete = hqlDelete + " AND upper(JUR_LEVEL) = '" + selectedTaxCodeDetail.getJurLevel() + "'";
			}
			
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			tx.commit();
			
			tx = session.beginTransaction();		
			// Now do an insert for Exception 1
			query = session.createSQLQuery("INSERT INTO TB_TAX_HOLIDAY_DETAIL (TAX_HOLIDAY_CODE, TAXCODE_CODE, TAXCODE_COUNTY, TAXCODE_CITY, TAXCODE_STJ, JUR_LEVEL, SORT_NO, EXCEPT_IND) VALUES (:holidayCode, :taxCode, :county, :city, :stj, :jurLevel, :sortNo, :exceptInd)");
			for (TaxHolidayDetail taxHolidayDetail : taxHolidayDetailList) {
				query.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
				query.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
				query.setParameter("county", taxHolidayDetail.getTaxcodeCounty());
				query.setParameter("city", taxHolidayDetail.getTaxcodeCity());		
				query.setParameter("stj", taxHolidayDetail.getTaxcodeStj());
				query.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
				query.setParameter("sortNo", taxHolidayDetail.getSortNo());	
				query.setParameter("exceptInd", taxHolidayDetail.getExceptInd());

				query.executeUpdate();
			}
			
			//Add for Exception 0
			query.setParameter("holidayCode", taxHoliday.getTaxHolidayCode());
			query.setParameter("taxCode", selectedTaxCodeDetail.getTaxcodeCode());
			query.setParameter("county", selectedTaxCodeDetail.getTaxcodeCounty());
			query.setParameter("city", selectedTaxCodeDetail.getTaxcodeCity());	
			query.setParameter("stj", selectedTaxCodeDetail.getTaxcodeStj());
			query.setParameter("jurLevel", selectedTaxCodeDetail.getJurLevel());
			query.setParameter("sortNo", 1);	
			query.setParameter("exceptInd", "0");

			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_TAX_HOLIDAY_DETAIL: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateTaxHolidayMapDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, String taxCode, List<TaxHolidayDetail> unselectedList) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query queryDelete1 = null;
		org.hibernate.Query queryDelete0 = null;
		org.hibernate.Query queryInsert = null;
		org.hibernate.Query queryConut = null;
		try {
			tx = session.beginTransaction();
			session.update(taxHoliday);
			
			// Now do delete for unselected
			queryDelete0 = session.createSQLQuery("DELETE TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE=:holidayCode and upper(TAXCODE_CODE)=:taxCode AND upper(JUR_LEVEL) = :jurLevel AND EXCEPT_IND = :exceptInd");
			queryDelete1 = session.createSQLQuery("DELETE TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE=:holidayCode and upper(TAXCODE_CODE)=:taxCode AND EXCEPT_IND = :exceptInd");
			for (TaxHolidayDetail taxHolidayDetail : unselectedList) {
				queryDelete0.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
				queryDelete0.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
				queryDelete0.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
				queryDelete0.setParameter("exceptInd", "0");
				queryDelete0.executeUpdate();
				
				queryDelete1.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
				queryDelete1.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
				queryDelete1.setParameter("exceptInd", "1");
				queryDelete1.executeUpdate();
			}
				
			// Now do an insert for each user
			queryInsert = session.createSQLQuery("INSERT INTO TB_TAX_HOLIDAY_DETAIL (TAX_HOLIDAY_CODE, TAXCODE_CODE, TAXCODE_COUNTY, TAXCODE_CITY, TAXCODE_STJ, JUR_LEVEL, SORT_NO, EXCEPT_IND) VALUES (:holidayCode, :taxCode, :county, :city, :stj, :jurLevel, :sortNo, :exceptInd)");
			queryConut = session.createSQLQuery("SELECT COUNT(*) from TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE = :holidayCode AND upper(TAXCODE_CODE) = :taxCode AND upper(JUR_LEVEL) = :jurLevel AND EXCEPT_IND = :exceptInd");
			for (TaxHolidayDetail taxHolidayDetail : taxHolidayDetailList) {
				//Chekc if exception_ind = 0 existing for a specific taxcode_code
				queryConut.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
				queryConut.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
				queryConut.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
				queryConut.setParameter("exceptInd", "0");
				List<Number> countList = queryConut.list();
				if(((Number)countList.get(0)).intValue()==0){//need to create a map detail			
					queryInsert.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
					queryInsert.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
					queryInsert.setParameter("county", taxHolidayDetail.getTaxcodeCounty());
					queryInsert.setParameter("city", taxHolidayDetail.getTaxcodeCity());
					
					queryInsert.setParameter("stj", taxHolidayDetail.getTaxcodeStj());
					queryInsert.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
					queryInsert.setParameter("sortNo", taxHolidayDetail.getSortNo());	
					queryInsert.setParameter("exceptInd", taxHolidayDetail.getExceptInd());
					queryInsert.executeUpdate();
				}
			}
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_TAX_HOLIDAY_DETAIL: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			String hqlDelete ;
			tx = session.beginTransaction();
	
			// First delete all existing taxholidaydetail if exist
			hqlDelete = "delete TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE = '"+taxHoliday.getTaxHolidayCode().toUpperCase()+"'";
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			hqlDelete = "delete TB_TAX_HOLIDAY WHERE TAX_HOLIDAY_CODE = '"+taxHoliday.getTaxHolidayCode().toUpperCase()+"'";
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to remove TB_TAX_HOLIDAY_DETAIL: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void addTaxHolidayDetails(String taxHolidayCode, List<TaxHolidayDetail> taxHolidayDetailList) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			// First delete all existing specs
			String hqlDelete = "delete TB_TAX_HOLIDAY_DETAIL WHERE TAX_HOLIDAY_CODE = '"+taxHolidayCode.toUpperCase()+"'";
			org.hibernate.Query query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			// Now do an insert for each user
			
			query = session.createSQLQuery("INSERT INTO TB_TAX_HOLIDAY_DETAIL (TAX_HOLIDAY_CODE, TAXCODE_CODE, TAXCODE_COUNTY, TAXCODE_CITY, TAXCODE_STJ, JUR_LEVEL, SORT_NO, EXCEPT_IND) VALUES (:holidayCode, :taxCode, :county, :city, :stj, :jurLevel, :sortNo, :exceptInd)");
			for (TaxHolidayDetail taxHolidayDetail : taxHolidayDetailList) {
				//session.persist(taxHolidayDetail);
		
				query.setParameter("holidayCode", taxHolidayDetail.getTaxHolidayCode());
				query.setParameter("taxCode", taxHolidayDetail.getTaxcodeCode());
				query.setParameter("county", taxHolidayDetail.getTaxcodeCounty());
				query.setParameter("city", taxHolidayDetail.getTaxcodeCity());
				
				query.setParameter("stj", taxHolidayDetail.getTaxcodeStj());
				query.setParameter("jurLevel", taxHolidayDetail.getJurLevel());
				query.setParameter("sortNo", taxHolidayDetail.getSortNo());	
				query.setParameter("exceptInd", taxHolidayDetail.getExceptInd());

				query.executeUpdate();
			}
			
			tx.commit();
		} catch (Exception e) {
			logger.error("Failed to update TB_IMPORT_SPEC_UESR: " + e);
			e.printStackTrace();
			
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxHolidayDetail> findTaxHolidayDetailFromTaxCode(String country, String state, String taxCode, String description, String county, String city, String stj) throws DataAccessException {
		
		StringBuffer query = new StringBuffer();
		query.append("SELECT DISTINCT td.taxcode_code, td.taxcode_county, td.taxcode_city, td.taxcode_stj , tc.description, td.jur_level, td.sort_no  FROM TB_TAXCODE_DETAIL td inner join  TB_TAXCODE tc ON  (tc.taxcode_code = td.taxcode_code) ");
		query.append("WHERE td.taxcode_country_code = '"+ country + "' AND td.taxcode_state_code = '" + state + "' " );

		if ((taxCode != null) && !taxCode.equals("")) {
			query.append(" AND UPPER(td.taxcode_code) LIKE UPPER('"+taxCode+"%') ");
		}
		
		if ((description != null) && !description.equals("")) {
			query.append(" AND UPPER(tc.description) LIKE UPPER('"+description+"%') ");
		}
		if ((county != null) && !county.equals("")) {
			query.append(" AND UPPER(td.taxcode_county) = UPPER('"+county+"') ");
		}
		
		if ((city != null) && !city.equals("")) {
			query.append(" AND UPPER(td.taxcode_city) = UPPER('"+city+"') ");
		}
		
		if ((stj != null) && !stj.equals("")) {
			query.append(" AND UPPER(td.taxcode_stj) = UPPER('"+stj+"') ");
		}
		
		query.append(" and tc.active_flag = '1' and td.active_flag = '1'");
		query.append(" order by td.taxcode_code "); 
		
		Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(query.toString());
		
		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		List<TaxHolidayDetail> returnList = new ArrayList<TaxHolidayDetail>();
		if(list!=null){
			while(iter.hasNext()){
				Object[] objarray= (Object[])iter.next();
				TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();
				

	            String taxcodeCode = (String) objarray[0];
	            taxHolidayDetail.setTaxcodeCode(taxcodeCode);
	            
	            String taxcodeCounty = (String) objarray[1];
	            taxHolidayDetail.setTaxcodeCounty(taxcodeCounty);
	            
	            String taxcodeCity = (String) objarray[2];
	            taxHolidayDetail.setTaxcodeCity(taxcodeCity);
	            
	            String taxcodeStj = (String) objarray[3];
	            taxHolidayDetail.setTaxcodeStj(taxcodeStj);
	            
	            String taxcodeDescription = (String) objarray[4];
	            taxHolidayDetail.setDescription(taxcodeDescription);
	            
	            String jurLevel = (String) objarray[5];
	            taxHolidayDetail.setJurLevel(jurLevel);

	            BigDecimal sortNo = (BigDecimal) objarray[6];
	            if (sortNo!=null) taxHolidayDetail.setSortNo(new Long(sortNo.longValue()));
	            
	            returnList.add(taxHolidayDetail);                
			}
		}

		return returnList;
		
	}
	
	private String findTaxHolidayDetailFromJurisdictionSQL = 
						"SELECT alljurs.county, alljurs.city, alljurs.stj, alljurs.jur_level, alljurs.sort_no FROM ( " +
	
						"		   SELECT distinct " +
						"		      'A' AS sortby, " +
						"		      county AS county, " +
						"		      '*ALL' AS city, " +
						"		      '*ALL' AS stj, " +
						"		      '*COUNTY' AS jur_level, " +
						"		      1 AS sort_no " +
						"		   FROM tb_jurisdiction " +
						"		   WHERE country=:country " +
						"		   AND state=:state " +
						"		   AND county NOT IN ( " +
						"		      SELECT taxcode_county " +
						"		      FROM tb_taxcode_detail " +
						"		      WHERE taxcode_code=:taxcode " +
						"		      and active_flag='1') " +
						
						"		   UNION ALL " +
						
						"		   SELECT distinct " +
						"		      'B' AS sortby, " +
						"		      '*ALL' AS county, " +
						"		      city AS city, " +
						"		      '*ALL' AS stj, " +
						"		      '*CITY' AS jur_level, " +
						"		      1 AS sort_no " +
						"		   FROM tb_jurisdiction " +
						"		   WHERE country=:country " +
						"		   AND state=:state " +
						"		   AND city NOT IN ( " +
						"		      SELECT taxcode_city " +
						"		      FROM tb_taxcode_detail " +
						"		      WHERE taxcode_code=:taxcode " +
						"		      and active_flag='1') " +
						
						"		   UNION ALL " +
						
						"		   SELECT distinct sortby, county, city, stj, jur_level, sort_no FROM ( " +
						"		      SELECT " +
						"		         'C' AS sortby, " +
						"		         '*ALL' AS county, " +
						"		         '*ALL' AS city, " +
						"		         stj1_name AS stj, " +
						"		         '*STJ' AS jur_level, " +
						"		         1 AS sort_no " +
						"		      FROM tb_jurisdiction " +
						"		      WHERE country=:country " +
						"		      AND state=:state " +
						"		      AND stj1_name IS NOT NULL " +
						"		      AND stj1_name NOT IN ( " +
						"		         SELECT taxcode_stj " +
						"		         FROM tb_taxcode_detail " +
						"		         WHERE taxcode_code=:taxcode " +
						"		      and active_flag='1') " +
						
						"		      UNION ALL " +
						
						"		      SELECT " +
						"		         'C' AS sortby, " +
						"		         '*ALL' AS county, " +
						"		         '*ALL' AS city, " +
						"		         stj2_name AS stj, " +
						"		         '*STJ' AS jur_level, " +
						"		         1 AS sort_no " +
						"		      FROM tb_jurisdiction " +
						"		      WHERE country=:country " +
						"		      AND state=:state " +
						"		      AND stj2_name IS NOT NULL " +
						"		      AND stj2_name NOT IN ( " +
						"		         SELECT taxcode_stj " +
						"		         FROM tb_taxcode_detail " +
						"		         WHERE taxcode_code=:taxcode " +
						"		         and active_flag='1') " +
						
						"		      UNION ALL " +
						
						"		      SELECT " +
						"		         'C' AS sortby, " +
						"		         '*ALL' AS county, " +
						"		         '*ALL' AS city, " +
						"		         stj3_name AS stj, " +
						"		         '*STJ' AS jur_level, " +
						"		         1 AS sort_no " +
						"		      FROM tb_jurisdiction " +
						"		      WHERE country=:country " +
						"		      AND state=:state " +
						"		      AND stj3_name IS NOT NULL " +
						"		      AND stj3_name NOT IN ( " +
						"		         SELECT taxcode_stj " +
						"		         FROM tb_taxcode_detail " +
						"		         WHERE taxcode_code=:taxcode " +
						"		         and active_flag='1') " +
						
						"		      UNION ALL " +
						
						"		      SELECT " +
						"		         'C' AS sortby, " +
						"		         '*ALL' AS county, " +
						"		         '*ALL' AS city, " +
						"		         stj4_name AS stj, " +
						"		         '*STJ' AS jur_level, " +
						"		         1 AS sort_no " +
						"		      FROM tb_jurisdiction " +
						"		      WHERE country=:country " +
						"		      AND state=:state " +
						"		      AND stj4_name IS NOT NULL " +
						"		      AND stj4_name NOT IN ( " +
						"		         SELECT taxcode_stj " +
						"		         FROM tb_taxcode_detail " +
						"		         WHERE taxcode_code=:taxcode " +
						"		         and active_flag='1') " +
						
						"		      UNION ALL " +
						
						"		      SELECT " +
						"		         'C' AS sortby, " +
						"		         '*ALL' AS county, " +
						"		         '*ALL' AS city, " +
						"		         stj5_name AS stj, " +
						"		         '*STJ' AS jur_level, " +
						"		         1 AS sort_no " +
						"		      FROM tb_jurisdiction " +
						"		      WHERE country=:country " +
						"		      AND state=:state " +
						"		      AND stj5_name IS NOT NULL " +
						"		      AND stj5_name NOT IN ( " +
						"		         SELECT taxcode_stj " +
						"		         FROM tb_taxcode_detail " +
						"		         WHERE taxcode_code=:taxcode " +
						"		         and active_flag='1') " + 
						
						"		   ) stjs " + 
						"   ) alljurs " +
						"	WHERE :whereClausecounty " +
						"	ORDER BY alljurs.sortby, alljurs.county, alljurs.city, alljurs.stj ";	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String taxCode, String county, String city, String stj) throws DataAccessException {
		
		List<TaxHolidayDetail> returnList = new ArrayList<TaxHolidayDetail>();
		
		String sqlBuilder = this.findTaxHolidayDetailFromJurisdictionSQL;
		String whereClause = "1=1 ";
		
		if ((county != null) && !county.equals("")) {
			whereClause = whereClause + " AND UPPER(county) = UPPER('"+county+"') ";
		}
		
		if ((city != null) && !city.equals("")) {
			whereClause = whereClause + " AND UPPER(city) = UPPER('"+city+"') ";
		}
		
		if ((stj != null) && !stj.equals("")) {
			whereClause = whereClause + " AND UPPER(stj) = UPPER('"+stj+"') ";
		}
		
		sqlBuilder = sqlBuilder.replaceAll(":whereClausecounty", whereClause);
		sqlBuilder = sqlBuilder.replaceAll(":country", "'"+ country + "'");
		sqlBuilder = sqlBuilder.replaceAll(":state", "'"+ state + "'");
		sqlBuilder = sqlBuilder.replaceAll(":taxcode", "'"+ taxCode + "'");
		
		System.out.println("sqlBuilder : " + sqlBuilder);

		Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(sqlBuilder);
		
		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		
		String countyReturn = "";
		String cityReturn = "";
		String stjReturn = "";
		String jurLevel = "";
		BigDecimal sortNo = null;
				
		if(list!=null){
			while(iter.hasNext()){
				Object[] objarray= (Object[])iter.next();
				TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();		
	            taxHolidayDetail.setTaxcodeCode(taxCode); 
	            
	            countyReturn = (String) objarray[0];
	            cityReturn = (String) objarray[1];
	            stjReturn = (String) objarray[2];
	            
	            jurLevel = (String) objarray[3];
	            taxHolidayDetail.setJurLevel(jurLevel);

	            sortNo = (BigDecimal) objarray[4];
	            if (sortNo!=null) taxHolidayDetail.setSortNo(new Long(sortNo.longValue()));           
	       
	            taxHolidayDetail.setTaxcodeCode(taxCode); 
	            taxHolidayDetail.setTaxcodeCounty(countyReturn);	           
	            taxHolidayDetail.setTaxcodeCity(cityReturn);    
	            taxHolidayDetail.setTaxcodeStj(stjReturn);    
	            returnList.add(taxHolidayDetail);      
			}
		}

		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdictionBK(String country, String state, String taxCode, String county, String city, String stj) throws DataAccessException {
		
		List<TaxHolidayDetail> returnList = new ArrayList<TaxHolidayDetail>();
		
		//0001774: Tax Holiday screen issues - On the exceptions screen: 4. don't display the *ALL, *ALL row
		/*
		TaxHolidayDetail firstTaxHolidayDetail=new TaxHolidayDetail();
		firstTaxHolidayDetail.setTaxcodeCode(taxCode);
		firstTaxHolidayDetail.setTaxcodeCounty("*ALL");           
		firstTaxHolidayDetail.setTaxcodeCity("*ALL");
        returnList.add(firstTaxHolidayDetail);
        */   
        
		//County list
		StringBuffer query = new StringBuffer();
		query.append("SELECT DISTINCT COUNTY FROM TB_JURISDICTION WHERE STATE='"+state+"' AND country='"+country+"'");
		if ((county != null) && !county.equals("")) {
			query.append(" AND UPPER(county) = UPPER('"+county+"') ");
		}
		if ((city != null) && !city.equals("")) {
			query.append(" AND UPPER(city) = UPPER('"+city+"') ");
		}
		query.append(" order by county "); 
		
		System.out.println(query.toString());

		Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(query.toString());
		
		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		
		if(list!=null){
			while(iter.hasNext()){
				//Object[] objarray= (Object[])iter.next();
				//TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();		
	            //taxHolidayDetail.setTaxcodeCode(taxCode); 
	            //String taxcodeCounty = (String) objarray[0];
	            //taxHolidayDetail.setTaxcodeCounty(taxcodeCounty);	           
	            //taxHolidayDetail.setTaxcodeCity("*ALL");     
	            //returnList.add(taxHolidayDetail);       
				
				String taxcodeCounty= (String)iter.next();
				TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();		
	            taxHolidayDetail.setTaxcodeCode(taxCode); 
	      //      String taxcodeCounty = (String) objarray[0];
	            taxHolidayDetail.setTaxcodeCounty(taxcodeCounty);	           
	            taxHolidayDetail.setTaxcodeCity("*ALL");     
	            returnList.add(taxHolidayDetail);      
			}
		}
		
		//City list
		StringBuffer query1 = new StringBuffer();
		query1.append("SELECT DISTINCT CITY FROM TB_JURISDICTION WHERE STATE='"+state+"' AND country='"+country+"'");
		if ((county != null) && !county.equals("")) {
			query1.append(" AND UPPER(county) = UPPER('"+county+"') ");
		}
		if ((city != null) && !city.equals("")) {
			query1.append(" AND UPPER(city) = UPPER('"+city+"') ");
		}
		query1.append(" order by city "); 
		
		System.out.println(query1.toString());

		Query q1 = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(query1.toString());
		
		List<Object> list1= q1.getResultList();
		Iterator<Object> iter1=list1.iterator();
		if(list1!=null){
			while(iter1.hasNext()){
				//Object[] objarray= (Object[])iter.next();
				String taxcodeCity = (String)iter1.next();
				
				TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();
	            taxHolidayDetail.setTaxcodeCode(taxCode);
	            taxHolidayDetail.setTaxcodeCounty("*ALL"); 
	            
	            taxHolidayDetail.setTaxcodeCity(taxcodeCity);
	            returnList.add(taxHolidayDetail);                
			}
		}

		return returnList;
		
	}

	@SuppressWarnings("unchecked")
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String county, String city) throws DataAccessException {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxHolidayDetail> findTaxHolidayDetailByTaxHolidayCode(String taxHolidayCode, String exceptInd, String taxCode) throws DataAccessException {
		StringBuffer query = new StringBuffer();
		query.append("SELECT TAX_HOLIDAY_CODE, TAXCODE_CODE, TAXCODE_COUNTY, TAXCODE_CITY, TAXCODE_STJ, JUR_LEVEL, SORT_NO, EXCEPT_IND  from TB_TAX_HOLIDAY_DETAIL where TAX_HOLIDAY_CODE = '"+ taxHolidayCode.toUpperCase() + "' and EXCEPT_IND = '" + exceptInd + "' ");

		if(taxCode!=null && taxCode.length()>0){
			query.append(" and UPPER(TAXCODE_CODE) = '" + taxCode + "' "); 
		}
		
		query.append(" order by TAXCODE_CODE "); 
		
		System.out.println(query.toString());
		
		Query q = getJpaTemplate().getEntityManagerFactory().createEntityManager().createNativeQuery(query.toString());
		
		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		List<TaxHolidayDetail> returnList = new ArrayList<TaxHolidayDetail>();
		if(list!=null){
			while(iter.hasNext()){
				Object[] objarray= (Object[])iter.next();
				TaxHolidayDetail taxHolidayDetail=new TaxHolidayDetail();
				
				String taxHolidayCodeNew = (String) objarray[0];
	            taxHolidayDetail.setTaxHolidayCode(taxHolidayCodeNew);

	            String taxcodeCode = (String) objarray[1];
	            taxHolidayDetail.setTaxcodeCode(taxcodeCode);
	            
	            String taxcodeCounty = (String) objarray[2];
	            taxHolidayDetail.setTaxcodeCounty(taxcodeCounty);
	            
	            String taxcodeCity = (String) objarray[3];
	            taxHolidayDetail.setTaxcodeCity(taxcodeCity);
	            
	            String taxcodeStj = (String) objarray[4];
	            taxHolidayDetail.setTaxcodeStj(taxcodeStj);
	            
	            String jurLevel = (String) objarray[5];
	            taxHolidayDetail.setJurLevel(jurLevel);

	            BigDecimal sortNo = (BigDecimal) objarray[6];
	            if (sortNo!=null) taxHolidayDetail.setSortNo(new Long(sortNo.longValue()));   
	            
	            Character exceptIndStr = (Character) objarray[7];
	            taxHolidayDetail.setExceptInd(exceptIndStr.toString());
	            
	            taxHolidayDetail.setSelected(true);
	           
	            returnList.add(taxHolidayDetail);                
			}
		}
		
		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	public Long findTaxHolidayDetailCount(String taxHolidayCode) throws DataAccessException {
		Long returnLong = 0L;
		Session localSession = this.createLocalSession();
		Criteria criteria = localSession.createCriteria(TaxHolidayDetail.class);
		if(taxHolidayCode != null && taxHolidayCode.length()>0) {
			criteria.add(Restrictions.eq("id.taxHolidayCode",taxHolidayCode));
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		}    
		
		closeLocalSession(localSession);
		localSession=null;
		
		return returnLong;
	}
   	
	public Long count(TaxHoliday exampleInstance) {
		Session localSession = this.createLocalSession();
		
		Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
		criteria.setProjection(Projections.rowCount()); 
		List<?> result = criteria.list();
		Long returnLong = ((Number)result.get(0)).longValue();
		
		closeLocalSession(localSession);
		localSession=null;
		
		return returnLong;
	}

	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	@SuppressWarnings("unchecked")
	public List<TaxHoliday> find(Criteria criteria) {
		return criteria.list();
	}
	public List<TaxHoliday> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<TaxHoliday> returnTaxHoliday = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnTaxHoliday;
	}
	
	public TaxHoliday find(TaxHoliday exampleInstance) {
		List<TaxHoliday> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxHoliday> find(TaxHoliday exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults) {
		logger.info("Entering the gatAllRecords");
		List<TaxHoliday> list = new ArrayList<TaxHoliday>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
			logger.error("Hibernate Exception "+hbme.getMessage());
		}
		return new ArrayList<TaxHoliday>();
	}
	
	private Criteria generateCriteriaBatch(TaxHoliday exampleInstance, Session localSession) {

		Criteria criteria = localSession.createCriteria(TaxHoliday.class);
        
		if(exampleInstance.getTaxHolidayCode() != null && exampleInstance.getTaxHolidayCode().length()>0) {
			if(exampleInstance.getTaxHolidayCode().substring(exampleInstance.getTaxHolidayCode().length()-1, exampleInstance.getTaxHolidayCode().length()).equals("%") ||
					exampleInstance.getTaxHolidayCode().substring(0, 1).equals("%")){
				criteria.add(Restrictions.ilike("taxHolidayCode",exampleInstance.getTaxHolidayCode()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("taxHolidayCode",exampleInstance.getTaxHolidayCode()));
			}
		}    
		if(exampleInstance.getEffectiveDate() != null) {
		    criteria.add(Restrictions.ge("effectiveDate",exampleInstance.getEffectiveDate()));
		} 
		if(exampleInstance.getExpirationDate() != null) {
		    criteria.add(Restrictions.le("expirationDate",exampleInstance.getExpirationDate()));
		} 
		if(exampleInstance.getTaxcodeCountryCode() != null) {
		    criteria.add(Restrictions.ilike("taxcodeCountryCode",exampleInstance.getTaxcodeCountryCode()));
		}                
		if(exampleInstance.getTaxcodeStateCode() != null) {
		    criteria.add(Restrictions.ilike("taxcodeStateCode",exampleInstance.getTaxcodeStateCode()));
		}     
        if (exampleInstance.getDescription()!=null  && exampleInstance.getDescription().length()>0) {
        	if(exampleInstance.getDescription().substring(exampleInstance.getDescription().length()-1, exampleInstance.getDescription().length()).equals("%") ||
					exampleInstance.getDescription().substring(0, 1).equals("%")){
        		criteria.add(Restrictions.ilike("description",exampleInstance.getDescription()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("description",exampleInstance.getDescription()));
			}
        }          

        return criteria;
	}

	@Transactional
	public Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz) {
		HibernateUtils hu = new HibernateUtils((HibernateEntityManagerFactory)getJpaTemplate().getEntityManagerFactory());
		return hu.getColumnToProperties(clazz);
	}

}
