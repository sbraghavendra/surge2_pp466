package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BillTransactionLog;

public class JPABillTransactionLogDAO extends JPAGenericDAO<BillTransactionLog, Long> implements BillTransactionLogDAO {
}
