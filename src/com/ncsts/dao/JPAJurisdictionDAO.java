package com.ncsts.dao;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.VendorNexus;
import com.seconddecimal.billing.query.SqlQuery;
import com.seconddecimal.billing.util.DateUtils;

public class JPAJurisdictionDAO extends JPAGenericDAO<Jurisdiction,Long> implements JurisdictionDAO {

	private static Logger logger = LoggerFactory.getInstance().getLogger(
			  JPAJurisdictionDAO.class);
	public static final String ALIAS_PREFIX = "alias_";
	
	private Map<String,String> columnMapping = null;

	@SuppressWarnings("unchecked")
	public List<JurisdictionTaxrate> getAllJurisdictionTaxrate(JurisdictionTaxrate exampleInstance, int count, String... excludeProperty) throws DataAccessException{
		List<JurisdictionTaxrate> list=null;
		Session session = createLocalSession();

        try {
            Criteria criteria = session.createCriteria(JurisdictionTaxrate.class);
            if (count > 0) {
            	criteria.setMaxResults(count);
            }
            Example example = Example.create(exampleInstance);
            for (String exclude : excludeProperty) {
                example.excludeProperty(exclude);
            }
            criteria.add(example);
            list = criteria.list();
            
        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the JurisdictionTaxrate by example");
        }
        
        closeLocalSession(session);
        session=null;
        
        return list; 	
   }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeCounty(String countryCode, String stateCode) throws DataAccessException{
		List<String> listCounty = getJpaTemplate().find("select distinct upper(td.county)  from Jurisdiction td where td.state = ?1 and td.country = ?2 order by upper(td.county) ", stateCode, countryCode);
		return listCounty;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findTaxCodeCity(String countryCode, String stateCode) throws DataAccessException{
		List<String> listCity = getJpaTemplate().find("select distinct upper(td.city)  from Jurisdiction td where td.state = ?1 and td.country = ?2 order by upper(td.city) ", stateCode, countryCode);
		return listCity;
	}
	
	@Transactional(readOnly=true)
	public Long partialJurisdictionCount(Jurisdiction exampleInstance) {
		Session session = createLocalSession();
		
		Criteria criteria = session.createCriteria(Jurisdiction.class);
		if (exampleInstance.getGeocode()!=null && exampleInstance.getGeocode().length()>0) {
			criteria.add(Restrictions.eq("geocode", exampleInstance.getGeocode()));
		}	
		if (exampleInstance.getCity()!=null && exampleInstance.getCity().length()>0) {
			criteria.add(Restrictions.eq("city", exampleInstance.getCity()));
		}
		if (exampleInstance.getCounty()!=null && exampleInstance.getCounty().length()>0) {
			criteria.add(Restrictions.eq("county", exampleInstance.getCounty()));
		}	
		if (exampleInstance.getState()!=null && exampleInstance.getState().length()>0) {
			criteria.add(Restrictions.eq("state", exampleInstance.getState()));
		}	
		if (exampleInstance.getCountry()!=null && exampleInstance.getCountry().length()>0) {
			criteria.add(Restrictions.eq("country", exampleInstance.getCountry()));
		}	
		if (exampleInstance.getZip()!=null && exampleInstance.getZip().length()>0) {
			criteria.add(Restrictions.eq("zip", exampleInstance.getZip()));
		}
		if (exampleInstance.getZipplus4()!=null && exampleInstance.getZipplus4().length()>0) {
			criteria.add(Restrictions.eq("zipplus4", exampleInstance.getZipplus4()));
		}
		criteria.setProjection(Projections.rowCount()); // We just want a row count
		Long ret = ((Number)criteria.list().get(0)).longValue();
		logger.info("exit JPATransactionDetailDAO.partialJurisdictionCount() = " + ret );	
		
		closeLocalSession(session);
		session=null;

        return ret;
	}
	
	public Long count(Jurisdiction exampleInstance, JurisdictionTaxrate jurisdictionTaxrate) {
		long count = 0;
		
		Session session = createLocalSession();
		try {
			String hql = generateSQLQuery(exampleInstance, jurisdictionTaxrate, null, true, exampleInstance.getAutoWildSearch());
			org.hibernate.SQLQuery query = session.createSQLQuery(hql.toString());
			
			List<?> list = query.list();
	        if (list.size() > 0)
	        	count = ((Number)list.get(0)).longValue();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
        return count;
	}
	
	private String generateSQLQuery(Jurisdiction j, JurisdictionTaxrate t, OrderBy orderBy, boolean isCount, boolean autoWildSearch) {
		StringBuffer hql = new StringBuffer("");
		
		String rateWhereClause = " (1=1)";
		String juriWhereClause = " and (1=1)";
		String juriFlagWhereClause = " (1=1)";

		String customVal = j.getCustomFlag() != null ? t.getCustomFlag() : (t.getCustomFlag() != null ? t.getCustomFlag() : null);
		if (customVal != null && customVal.length()>0) {
			if(customVal.equals("1")){
				if(rateWhereClause.length()>0){
					rateWhereClause = rateWhereClause + " AND";
				}
				rateWhereClause = rateWhereClause + " (custom_Flag = '1')";
				
				if(juriFlagWhereClause.length()>0){
					juriFlagWhereClause = juriFlagWhereClause + " AND";
				}
				juriFlagWhereClause = juriFlagWhereClause + " (custom_Flag = '1')";
			}
			else{
				if(rateWhereClause.length()>0){
					rateWhereClause = rateWhereClause + " AND";
				}
				rateWhereClause = rateWhereClause + " (custom_Flag = '0' or trim(custom_Flag) is null)";
				
				if(juriFlagWhereClause.length()>0){
					juriFlagWhereClause = juriFlagWhereClause + " AND";
				}
				juriFlagWhereClause = juriFlagWhereClause + " (custom_Flag = '0' or trim(custom_Flag) is null)";
			}
		}
		
		String modifiedVal = t.getModifiedFlag() != null ? t.getModifiedFlag() : null;
		if (modifiedVal != null && modifiedVal.length()>0) {
			if(modifiedVal.equals("1")){
				if(rateWhereClause.length()>0){
					rateWhereClause = rateWhereClause + " AND";
				}
				rateWhereClause = rateWhereClause + " (modified_Flag = '1')";
				
				if(juriFlagWhereClause.length()>0){
					juriFlagWhereClause = juriFlagWhereClause + " AND";
				}
				juriFlagWhereClause = juriFlagWhereClause + " (modified_Flag = '1')";
			}
			else{
				if(rateWhereClause.length()>0){
					rateWhereClause = rateWhereClause + " AND";
				}
				rateWhereClause = rateWhereClause + " (modified_Flag = '0' or trim(modified_Flag) is null)";
				
				if(juriFlagWhereClause.length()>0){
					juriFlagWhereClause = juriFlagWhereClause + " AND";
				}
				juriFlagWhereClause = juriFlagWhereClause + " (modified_Flag = '0' or trim(modified_Flag) is null)";
			}
		}
		
		if (t.getEffectiveDate() != null) {
			if(rateWhereClause.length()>0){
				rateWhereClause = rateWhereClause + " AND";
			}
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDate = df.format(t.getEffectiveDate());
			
			rateWhereClause = rateWhereClause + " effective_date >= to_date('"+effectiveDate+"','mm/dd/yyyy') ";
		}
		
		if (t.getExpirationDate() != null) {
			if(rateWhereClause.length()>0){
				rateWhereClause = rateWhereClause + " AND";
			}
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDate = df.format(t.getExpirationDate());
			rateWhereClause = rateWhereClause + " effective_date <= to_date('"+effectiveDate+"','mm/dd/yyyy') ";
		}
		
		Long taxrateId = t.getId();
		if (taxrateId != null) {
			if(rateWhereClause.length()>0){
				rateWhereClause = rateWhereClause + " AND";
			}
			
			rateWhereClause = rateWhereClause + " (JURISDICTION_TAXRATE_ID = " + taxrateId.intValue() + ")";
		}
		
		String ratetypeCode = t.getRatetypeCode();
		if (ratetypeCode != null && ratetypeCode.length()>0) {
			if(rateWhereClause.length()>0){
				rateWhereClause = rateWhereClause + " AND";
			}
			
			rateWhereClause = rateWhereClause + " (RATETYPE_CODE = '" + ratetypeCode + "')";
		}
		
		if(rateWhereClause.length()>0){
			rateWhereClause = rateWhereClause + " AND";
		}
		rateWhereClause = rateWhereClause + " tbj.jurisdiction_id=jurisdiction_id";
		

		//Jurisdiction where clause
		String geocode = j.getGeocode() != null ? j.getGeocode().trim().toUpperCase() : null;
		if (geocode != null && geocode.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.GEOCODE) like '" + geocode + "%')";
			}
			else{			
				if(geocode.substring(geocode.length()-1, geocode.length()).equals("%") || geocode.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.GEOCODE) like '" + geocode + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.GEOCODE) = '" + geocode + "')";
				}
			}
		}
		
		String county = j.getCounty() != null ? j.getCounty().trim().toUpperCase() : null;
		if (county != null && county.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.county) like '" + county + "%')";
			}
			else{	
				if(county.substring(county.length()-1, county.length()).equals("%") || county.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.county) like '" + county + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.county) = '" + county + "')";
				}
			}
		}
		
		String city = j.getCity() != null ? j.getCity().trim().toUpperCase() : null;
		if (city != null && city.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.CITY) like '" + city + "%')";
			}
			else{	
				if(city.substring(city.length()-1, city.length()).equals("%") || city.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.CITY) like '" + city + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.CITY) = '" + city + "')";
				}
			}
		}
		
		String zip = j.getZip() != null ? j.getZip().trim().toUpperCase() : null;
		if (zip != null && zip.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.ZIP) like '" + zip + "%')";
			}
			else{	
				if(zip.substring(zip.length()-1, zip.length()).equals("%") || zip.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.ZIP) like '" + zip + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.ZIP) = '" + zip + "')";
				}
			}
		}
		
		String zipplus4 = j.getZipplus4() != null ? j.getZipplus4().trim().toUpperCase() : null;
		if (zipplus4 != null && zipplus4.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.ZIPPLUS4) like '" + zipplus4 + "%')";
			}
			else{	
				if(zipplus4.substring(zipplus4.length()-1, zipplus4.length()).equals("%") || zipplus4.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.ZIPPLUS4) like '" + zipplus4 + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.ZIPPLUS4) = '" + zipplus4 + "')";
				}
			}
		}
		
		String stj = j.getStj1Name() != null ? j.getStj1Name().trim().toUpperCase() : null;
		if (stj != null && stj.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.STJ1_NAME) like '" + stj + "%' or upper(tbj.STJ2_NAME) like '" + stj + "%' or upper(tbj.STJ3_NAME) like '" + stj + "%' or upper(tbj.STJ4_NAME) like '" + stj + "%' or upper(tbj.STJ5_NAME) like '" + stj + "%')"; 
			}
			else{	
				if(stj.substring(stj.length()-1, stj.length()).equals("%") || stj.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.STJ1_NAME) like '" + stj + "' or upper(tbj.STJ2_NAME) like '" + stj + "' or upper(tbj.STJ3_NAME) like '" + stj + "' or upper(tbj.STJ4_NAME) like '" + stj + "' or upper(tbj.STJ5_NAME) like '" + stj + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.STJ1_NAME) = '" + stj + "' or upper(tbj.STJ2_NAME) = '" + stj + "' or upper(tbj.STJ3_NAME) = '" + stj + "' or upper(tbj.STJ4_NAME) = '" + stj + "' or upper(tbj.STJ5_NAME) = '" + stj + "')"; 
				}
			}
		}
		
		String clientGeocode = j.getClientGeocode() != null ? j.getClientGeocode().trim().toUpperCase() : null;
		if (clientGeocode != null && clientGeocode.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(autoWildSearch){
				juriWhereClause = juriWhereClause + " (upper(tbj.CLIENT_GEOCODE) like '" + clientGeocode + "%')";
			}
			else{	
				if(clientGeocode.substring(clientGeocode.length()-1, clientGeocode.length()).equals("%") || clientGeocode.substring(0, 1).equals("%")){
					juriWhereClause = juriWhereClause + " (upper(tbj.CLIENT_GEOCODE) like '" + clientGeocode + "')";
				}
				else{
					juriWhereClause = juriWhereClause + " (upper(tbj.CLIENT_GEOCODE) = '" + clientGeocode + "')";
				}
			}
		}
		
		String compGeocode = j.getCompGeocode() != null ? j.getCompGeocode().trim().toUpperCase() : null;
		if (compGeocode != null && compGeocode.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			if(compGeocode.substring(compGeocode.length()-1, compGeocode.length()).equals("%") || compGeocode.substring(0, 1).equals("%")){
				juriWhereClause = juriWhereClause + " (upper(tbj.COMP_GEOCODE) like '" + compGeocode + "')";
			}
			else{
				juriWhereClause = juriWhereClause + " (upper(tbj.COMP_GEOCODE) = '" + compGeocode + "')";
			}
		}
		
		String country = j.getCountry() != null ? j.getCountry().trim().toUpperCase() : null;
		if (country != null && country.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			juriWhereClause = juriWhereClause + " (upper(tbj.COUNTRY) = '" + country + "')";
		}
		
		String state = j.getState() != null ? j.getState().trim().toUpperCase() : null;
		if (state != null && state.length()>0) {
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			juriWhereClause = juriWhereClause + " (upper(tbj.STATE) = '" + state + "')";
		}
		
		if(j.getJurisdictionId()!=null){
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			juriWhereClause = juriWhereClause + " (tbj.JURISDICTION_ID = " + j.getJurisdictionId().intValue() + ")";
        }	
		
		if(j.getDescription()!=null && j.getDescription().equalsIgnoreCase("INVALID")){
			if(juriWhereClause.length()>0){
				juriWhereClause = juriWhereClause + " AND";
			}
			
			juriWhereClause = juriWhereClause + " (upper(tbj.DESCRIPTION) not like '%INVALID%')";
        }

		//Build whole query
		if(isCount){
			hql.append("SELECT count(*) as count from tb_jurisdiction tbj ");
		}
		else{
			hql.append("SELECT tbj.* from tb_jurisdiction tbj ");
		}

		hql.append("WHERE ((tbj.jurisdiction_id IN ( ");
		hql.append("   SELECT jurisdiction_id ");
		hql.append("   FROM tb_jurisdiction ");
		hql.append("   WHERE  ");
		hql.append("   ( ");
		hql.append("      SELECT count(*) ");
		hql.append("      FROM tb_jurisdiction_taxrate ");
		hql.append("      WHERE ");
		
		hql.append("      " + rateWhereClause + " ");
		
		hql.append("   ) > 0)  )");
		
		if (taxrateId != null) {
			
			hql.append("   )");
			
		}else{
		
		hql.append("   OR (" + juriFlagWhereClause + ") ) ");

		hql.append(" " + juriWhereClause + " ");
		}
		
		if(!isCount){
			hql.append(getOrderByToken(orderBy));
		}

		return hql.toString();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Jurisdiction> getAllRecords(Jurisdiction exampleInstance, JurisdictionTaxrate jurisdictionTaxrate, OrderBy orderBy, int firstRow, int maxResults) {
		String sql = generateSQLQuery(exampleInstance, jurisdictionTaxrate, orderBy, false, exampleInstance.getAutoWildSearch());

		Session session = createLocalSession();
		List<Jurisdiction> list =  new ArrayList<Jurisdiction>();
		org.hibernate.SQLQuery query = null;
		try {
			query = session.createSQLQuery(sql);
			query.addEntity(Jurisdiction.class);
			
			query.setFirstResult(firstRow);
			if (maxResults > 0)
				query.setMaxResults(maxResults);

			list = query.list();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}

	
	private HashMap<String, Object> queryParams;
	private String getExampleParams(Class<?> cls, Object o, String alias, List<String> ignore,boolean autoWildSearch) {
		
		StringBuffer sb = new StringBuffer();
		String and = "";
		for (Field f : cls.getDeclaredFields()) {
			if (ignore.contains(f.getName()))
				continue;
			try {
				if (f.getAnnotation(Column.class) != null) {
					String methodName = "get" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1); 
					Method m = cls.getMethod(methodName, new Class<?> [] {});
					if (m != null) {
						Object val = m.invoke(o, new Object [] {});
						if (val != null) {
							//0004533
							boolean needWild = f.getName().equalsIgnoreCase("geocode") || f.getName().equalsIgnoreCase("county") || f.getName().equalsIgnoreCase("city") || 
									f.getName().equalsIgnoreCase("zip") || f.getName().equalsIgnoreCase("clientGeocode") || f.getName().equalsIgnoreCase("compGeocode");
							if(autoWildSearch){
								if(needWild){
									sb.append(and + " " + alias + "." + f.getName() + " like :" + f.getName());
								}else {
									sb.append(and + " " + alias + "." + f.getName() + " = :" + f.getName());
								}
						
							}
							else{
								//Fixed for ISSUE 0004549
								if((val.toString()).endsWith("%") && needWild){
									sb.append(and + " " + alias + "." + f.getName() + " like :" + f.getName());
								}else {
									sb.append(and + " " + alias + "." + f.getName() + " = :" + f.getName());
								}
							}
							
							if(autoWildSearch && needWild){
								queryParams.put(f.getName(), val + "%");
							}
							else{
								queryParams.put(f.getName(), val);
							}
							
							if ("".equals(and))
								and = " and";
						}
					}
				}
			} catch (Exception e) {}
		}
		return sb.toString();
	}
	
	private String makeWhere(Jurisdiction j, JurisdictionTaxrate t) {
		queryParams = new HashMap<String, Object>();
		StringBuffer sb = new StringBuffer();
		Class<?>jcls = j.getClass();
		Class<?>tcls = t == null ?
				null : t.getClass();
		
		List<String> ignore = new ArrayList<String>();
		ignore.add("customFlag");
		ignore.add("modifiedFlag");
		ignore.add("effectiveDate");
		ignore.add("expirationDate");
		
		ignore.add("geocode");
		ignore.add("county");
		ignore.add("city");
		ignore.add("zip");
		ignore.add("clientGeocode");
		ignore.add("compGeocode");
		String jExpr = getExampleParams(jcls, j, "j", ignore, j.getAutoWildSearch());
		String tExpr = getExampleParams(tcls, t, "t", ignore, false);
		//String jExpr = getExampleParams(jcls, j, "j", ignore);
		//String tExpr = getExampleParams(tcls, t, "t", ignore);
		String cExpr = new String();

		String customVal = j.getCustomFlag() != null ? t.getCustomFlag()
				: (t.getCustomFlag() != null ? t.getCustomFlag() : null);
		if (customVal != null && customVal.length()>0) {
			if(customVal.equals("1")){
				cExpr = " (j.customFlag = '1' or t.customFlag = '1')";
			}
			else{
				cExpr = " (j.customFlag = '0' or (trim(j.customFlag) is null) or t.customFlag = '0' or (trim(t.customFlag) is null) )";
			}
		}
		
		String modifiedVal = t.getModifiedFlag() != null ? t.getModifiedFlag() : null;
		if (modifiedVal != null && modifiedVal.length()>0) {
			if(modifiedVal.equals("1")){
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr + " (t.modifiedFlag = '1')";
			}
			else{
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr + " (t.modifiedFlag = '0' or (trim(t.modifiedFlag) is null))";
			}
		}
				
		if (t.getEffectiveDate() != null) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			cExpr = cExpr + " (t.effectiveDate >= :effectiveDate )";
			queryParams.put("effectiveDate", t.getEffectiveDate());	
		}
		
		if (t.getExpirationDate() != null) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			cExpr = cExpr + " (t.expirationDate <= :expirationDate )";
			queryParams.put("expirationDate", t.getExpirationDate());	
		}
		
		String geocode = j.getGeocode() != null ? j.getGeocode().trim().toUpperCase() : null;
		if (geocode != null && geocode.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(geocode.substring(geocode.length()-1, geocode.length()).equals("%") || geocode.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.geocode) like '" + geocode + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.geocode) = '" + geocode + "')";
			}
		}
		
		String county = j.getCounty() != null ? j.getCounty().trim().toUpperCase() : null;
		if (county != null && county.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(county.substring(county.length()-1, county.length()).equals("%") || county.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.county) like '" + county + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.county) = '" + county + "')";
			}
		}
		
		String city = j.getCity() != null ? j.getCity().trim().toUpperCase() : null;
		if (city != null && city.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(city.substring(city.length()-1, city.length()).equals("%") || city.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.city) like '" + city + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.city) = '" + city + "')";
			}
		}
		
		String zip = j.getZip() != null ? j.getZip().trim().toUpperCase() : null;
		if (zip != null && zip.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(zip.substring(zip.length()-1, zip.length()).equals("%") || zip.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.zip) like '" + zip + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.zip) = '" + zip + "')";
			}
		}
		
		String clientGeocode = j.getClientGeocode() != null ? j.getClientGeocode().trim().toUpperCase() : null;
		if (clientGeocode != null && clientGeocode.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(clientGeocode.substring(clientGeocode.length()-1, clientGeocode.length()).equals("%") || clientGeocode.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.clientGeocode) like '" + clientGeocode + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.clientGeocode) = '" + clientGeocode + "')";
			}
		}
		
		String compGeocode = j.getCompGeocode() != null ? j.getCompGeocode().trim().toUpperCase() : null;
		if (compGeocode != null && compGeocode.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			if(compGeocode.substring(compGeocode.length()-1, compGeocode.length()).equals("%") || compGeocode.substring(0, 1).equals("%")){
				cExpr = cExpr + " (upper(j.compGeocode) like '" + compGeocode + "')";
			}
			else{
				cExpr = cExpr + " (upper(j.compGeocode) = '" + compGeocode + "')";
			}
		}
		
		StringBuffer subClause = new StringBuffer(jExpr);
		if ((subClause.length() > 0) && (tExpr.length() > 0))
			subClause.append(" and");
		subClause.append(tExpr);
		if ((subClause.length() > 0) && (cExpr.length() > 0))
			subClause.append(" and");
		subClause.append(cExpr);
		
		if (subClause.length() > 0) {
			sb.append(" where");
			sb.append(subClause.toString());
		}
		return sb.toString();
	}
	
	private String generateQuery(Jurisdiction j, JurisdictionTaxrate t, OrderBy orderBy, boolean isCount) {
		StringBuffer hql = new StringBuffer("select " + (isCount ? "count(distinct j) " : "distinct j "));
		hql.append("from " + Jurisdiction.class.getCanonicalName() + " j left join j.jursidictionTaxrates t");
		hql.append(makeWhere(j, t));
		if (orderBy != null)
			hql.append(" " + orderBy.getHQLOrderByClause(Jurisdiction.class.getCanonicalName(), "j"));
		return hql.toString();
	}

	
	@SuppressWarnings("unchecked")
	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance, boolean exact) {
		boolean criteriaSet = false;
		List <Jurisdiction> list = null;

		Session session = createLocalSession();
        Criteria criteria = session.createCriteria(Jurisdiction.class);
		
		logger.info("Entering the Search Method "+ exampleInstance.getCity());
		criteriaSet |= buildCriteria(criteria, "country", exampleInstance.getCountry(), exact);
		criteriaSet |= buildCriteria(criteria, "geocode", exampleInstance.getGeocode(), exact);
		criteriaSet |= buildCriteria(criteria, "city", exampleInstance.getCity(), exact);
		criteriaSet |= buildCriteria(criteria, "county", exampleInstance.getCounty(), exact);
		criteriaSet |= buildCriteria(criteria, "state", exampleInstance.getState(), exact);
		criteriaSet |= buildCriteria(criteria, "zip", exampleInstance.getZip(), exact);
		criteriaSet |= buildCriteria(criteria, "zipplus4", exampleInstance.getZipplus4(), exact);	
		criteriaSet |= buildCriteria(criteria, "jurisdictionId", exampleInstance.getJurisdictionId());
		
		list = (criteriaSet)? ((List<Jurisdiction>) criteria.list()):(new ArrayList<Jurisdiction>());
		
		closeLocalSession(session);
		session=null;
	
		return list;
	}
	
	private boolean buildCriteria(Criteria criteria, String field, Long value) {
		if ((value != null) && value.longValue()!=0l) {
			logger.info("Entering the Build Criteria "+field);
			
			criteria.add(Restrictions.eq(field, value)); 
			return true;
		}
		
		return false;
	}
	
	private boolean buildCriteria(Criteria criteria, String field, String value, boolean exact) {
		if ((value != null) && !value.trim().equals("")) {
			logger.info("Entering the Build Criteria "+field);
			if(exact) {
				criteria.add(Restrictions.ilike(field, value.trim(), MatchMode.EXACT));
			}
			else {
				criteria.add(Restrictions.ilike(field, value.trim(), MatchMode.START));
			}
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings("unused")
	private Criteria buildTaxrateCriteria(Criteria criteria, Criteria taxrateCriteria, String field, Object value) {
		if ((value != null) &&
			((!(value instanceof String)) || (StringUtils.isNotBlank((String) value)))) {
			if (value instanceof String) {
				value = ((String) value).trim();
			}

			logger.debug("Restricting Tax Rate search on field: " + field);

			if (taxrateCriteria == null) {
				taxrateCriteria = criteria.createAlias("jursidictionTaxrates", "tx");
			}
			
			if ("customFlag".equals(field)) {
				taxrateCriteria.add(Restrictions.or(
					Restrictions.eq("tx." + field, value),
					Restrictions.eq(field, value)));
			} else {
				taxrateCriteria.add(Restrictions.eq("tx." + field, value));
			}
		}
		return taxrateCriteria;
	}

	static class MyAliasToBeanResultTransformer extends AliasToBeanResultTransformer {

		private static final long serialVersionUID = 1L;

		MyAliasToBeanResultTransformer(Class<?> resultClass) {
			super(resultClass);
		}

		@Override
		public Object transformTuple(Object[] tuple, String[] aliases) {
			// Clone the aliases array and drop the prefix from them
			String[] myAliases = (String[]) aliases.clone();
			for (int i = 0; i < myAliases.length; i++) {
				String alias = myAliases[i];
				if ((alias != null) && (alias.startsWith(ALIAS_PREFIX))) {
					myAliases[i] = alias.substring(ALIAS_PREFIX.length());
				}
			}
			
			return super.transformTuple(tuple, myAliases);
		}
	}

	@SuppressWarnings("unchecked")
	public List<JurisdictionTaxrate> findByIdAndDate(
			JurisdictionTaxrate jurisdictionTaxrate) throws DataAccessException {
		List <JurisdictionTaxrate> list = null;
		Session session = createLocalSession();
                
        Criteria criteria = session.createCriteria(JurisdictionTaxrate.class);
        if(jurisdictionTaxrate.getJurisdiction().getJurisdictionId()!=null){
        	criteria.add(Restrictions.eq("jurisdiction.jurisdictionId", jurisdictionTaxrate.getJurisdiction().getJurisdictionId())); 
        }
        
        //0003111
        if(jurisdictionTaxrate.getJurisdictionTaxrateId()!=null){
               	criteria.add(Restrictions.eq("jurisdictionTaxrateId", jurisdictionTaxrate.getJurisdictionTaxrateId()));
        }  
        
        String cExpr = new String();
		cExpr = cExpr + " (1=1) ";
		
        if (jurisdictionTaxrate.getEffectiveDate() != null) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDate = df.format(jurisdictionTaxrate.getEffectiveDate());
			
			cExpr = cExpr + " effective_date >= to_date('"+effectiveDate+"','mm/dd/yyyy') ";
		}
		
		if (jurisdictionTaxrate.getExpirationDate() != null) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDate = df.format(jurisdictionTaxrate.getExpirationDate());
			cExpr = cExpr + " effective_date <= to_date('"+effectiveDate+"','mm/dd/yyyy') ";
		}
        
        String customVal = jurisdictionTaxrate.getCustomFlag() != null ? jurisdictionTaxrate.getCustomFlag() : null;
		if (customVal != null && customVal.length()>0) {
			if(customVal.equals("1")){
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr + " (CUSTOM_FLAG = '1')";

			}
			else{
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr +  " (CUSTOM_FLAG = '0' or (trim(CUSTOM_FLAG) is null))";

			}
		}
		
		String modifiedVal = jurisdictionTaxrate.getModifiedFlag() != null ? jurisdictionTaxrate.getModifiedFlag() : null;
		if (modifiedVal != null && modifiedVal.length()>0) {
			if(modifiedVal.equals("1")){
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr +  " (MODIFIED_FLAG = '1')";
			}
			else{
				if(cExpr.length()>0){
					cExpr = cExpr + " AND";
				}
				cExpr = cExpr +  " (MODIFIED_FLAG = '0' or (trim(MODIFIED_FLAG) is null))";
			}
		}
		
		String ratetype = jurisdictionTaxrate.getRatetypeCode() != null ? jurisdictionTaxrate.getRatetypeCode() : null;
		if (ratetype != null && ratetype.length()>0) {
			if(cExpr.length()>0){
				cExpr = cExpr + " AND";
			}
			cExpr = cExpr +  " (RATETYPE_CODE = '" + ratetype + "')";
		}
        
		if(cExpr!=null && cExpr.length()>0){
			criteria.add(Restrictions.sqlRestriction(cExpr));
		}
          
        criteria.addOrder(Order.asc("effectiveDate"));
        list=criteria.list();
        
        closeLocalSession(session);
        session=null;
                
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<JurisdictionTaxrate> findByIdAndMeasureType(
			JurisdictionTaxrate jurisdictionTaxrate) throws DataAccessException {
		List <JurisdictionTaxrate> list = null;
		Session session = createLocalSession();
                
        Criteria criteria = session.createCriteria(JurisdictionTaxrate.class);
        
        if(jurisdictionTaxrate != null){
        	if(jurisdictionTaxrate.getJurisdiction() != null){
            	criteria.add(Restrictions.eq("jurisdiction.jurisdictionId", jurisdictionTaxrate.getJurisdiction().getJurisdictionId())); 
            }
        	//ac logger.debug("Inside JPA MeasureTypeCode " + jurisdictionTaxrate.getMeasureTypeCode());
        	//ac if(!"".equalsIgnoreCase(jurisdictionTaxrate.getMeasureTypeCode()) && jurisdictionTaxrate.getMeasureTypeCode()!=null )
        	//ac {
        	//ac criteria.add(Restrictions.eq("measureTypeCode", jurisdictionTaxrate.getMeasureTypeCode()));
        	//ac }
        }
        
        criteria.addOrder(Order.asc("jurisdiction.jurisdictionId"));
        list=criteria.list();
        
        closeLocalSession(session);
        session=null;
                
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Jurisdiction> findByTaxCodeStateCode(String stateCode)
			throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(Jurisdiction.class);
		criteria.add(Restrictions.eq("state", stateCode));
		criteria.setMaxResults(1);
		List<Jurisdiction> juris = criteria.list();
		closeLocalSession(session);
		session=null;
		return juris;
	}

	@Override
	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance) {
		return searchByExample(exampleInstance, false);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findCountryStateStj(String countryCode, String stateCode) throws DataAccessException {
		String sql = null;
		List<String> list = null;
		
		if(!(countryCode==null || countryCode.length()==0  || stateCode==null || stateCode.length()==0)){
			sql = "SELECT DISTINCT stj_name FROM ( " +
			"SELECT DISTINCT stj1_name AS stj_name FROM tb_jurisdiction WHERE country='" + countryCode+ "' AND state='" + stateCode+ "' AND stj1_name IS NOT NULL " +
			   "UNION ALL " +
			   "SELECT DISTINCT stj2_name AS stj_name FROM tb_jurisdiction WHERE country='" + countryCode+ "' AND state='" + stateCode+ "' AND stj2_name IS NOT NULL " +
			   "UNION ALL " +
			   "SELECT DISTINCT stj3_name AS stj_name FROM tb_jurisdiction WHERE country='" + countryCode+ "' AND state='" + stateCode+ "' AND stj3_name IS NOT NULL " +
			   "UNION ALL " +
			   "SELECT DISTINCT stj4_name AS stj_name FROM tb_jurisdiction WHERE country='" + countryCode+ "' AND state='" + stateCode+ "' AND stj4_name IS NOT NULL " +
			   "UNION ALL " +
			   "SELECT DISTINCT stj5_name AS stj_name FROM tb_jurisdiction WHERE country='" + countryCode+ "' AND state='" + stateCode+ "' AND stj5_name IS NOT NULL) order by stj_name ";
   		
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			list = q.getResultList();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findLocalCounty(String country, String state, String city, String stj) throws DataAccessException {
		String sql = null;
		List<String> list = null;
		
		String cityWhere = "";	
		if(city!=null && city.length()>0 && !city.equalsIgnoreCase("*ALL")){
			cityWhere = " AND city='" + city + "' ";
		}
		String stjWhere = "";	
		if(stj!=null && stj.length()>0 && !stj.equalsIgnoreCase("*ALL")){
			stjWhere = " AND (stj1_name='"+stj+"' OR stj2_name='"+stj+"' OR stj3_name='"+stj+"' OR stj4_name='"+stj+"' OR stj5_name='"+stj+"') ";
		}
		
		if(!(country==null || country.length()==0  || state==null || state.length()==0)){
			sql = "SELECT DISTINCT county FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state + "' " + 
					cityWhere + stjWhere + " AND county IS NOT NULL order by county ";
   		
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			list = q.getResultList();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findLocalCity(String country, String state, String county, String stj) throws DataAccessException {
		String sql = null;
		List<String> list = null;
		
		String countyWhere = "";	
		if(county!=null && county.length()>0 && !county.equalsIgnoreCase("*ALL")){
			countyWhere = " AND county='" + county + "' ";
		}
		String stjWhere = "";	
		if(stj!=null && stj.length()>0 && !stj.equalsIgnoreCase("*ALL")){
			stjWhere = " AND (stj1_name='"+stj+"' OR stj2_name='"+stj+"' OR stj3_name='"+stj+"' OR stj4_name='"+stj+"' OR stj5_name='"+stj+"') ";
		}
		
		if(!(country==null || country.length()==0  || state==null || state.length()==0)){
			sql = "SELECT DISTINCT city FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state + "' " + 
					countyWhere + stjWhere + " AND city IS NOT NULL order by city ";
   		
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			list = q.getResultList();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<String> findLocalStj(String country, String state, String county, String city) throws DataAccessException {
		String sql = null;
		List<String> list = null;
		
		String countyWhere = "";	
		if(county!=null && county.length()>0 && !county.equalsIgnoreCase("*ALL")){
			countyWhere = " AND county='" + county + "' ";
		}
		String cityWhere = "";	
		if(city!=null && city.length()>0 && !city.equalsIgnoreCase("*ALL")){
			cityWhere = " AND city='" + city + "' ";
		}
		
		if(!(country==null || country.length()==0  || state==null || state.length()==0)){
			sql = "SELECT DISTINCT stj_name FROM ( " +
				"SELECT DISTINCT stj1_name AS stj_name FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state+ "' AND stj1_name IS NOT NULL " + countyWhere + cityWhere +
					"UNION ALL " +
				"SELECT DISTINCT stj2_name AS stj_name FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state+ "' AND stj2_name IS NOT NULL " + countyWhere + cityWhere +
					"UNION ALL " +
				"SELECT DISTINCT stj3_name AS stj_name FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state+ "' AND stj3_name IS NOT NULL " + countyWhere + cityWhere +
					"UNION ALL " +
				"SELECT DISTINCT stj4_name AS stj_name FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state+ "' AND stj4_name IS NOT NULL " + countyWhere + cityWhere +
					"UNION ALL " +
				"SELECT DISTINCT stj5_name AS stj_name FROM tb_jurisdiction WHERE country='" + country+ "' AND state='" + state+ "' AND stj5_name IS NOT NULL " + countyWhere + cityWhere + 
				" ) order by stj_name ";
   		
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			list = q.getResultList();
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Object[]> findNexusJurisdiction(Long entityId, String countryCode,
			String stateCode, String nexusIndCode, String type) throws DataAccessException {
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = null;
		List<Object[]> list = null;
		if("level".equalsIgnoreCase(type)) {
			sql = "SELECT 'All State', " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_county = '*STATE' " +
							"AND nexus_city = '*STATE' " +
							"AND nexus_stj = '*STATE' AND entity_id = ?3), 1 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Counties', " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_county = '*ALL' " +
							"AND nexus_city = '*COUNTY' " +
							"AND nexus_stj = '*COUNTY' AND entity_id = ?3), 2 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Cities', " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_county = '*CITY' " +
							"AND nexus_city = '*ALL' " +
							"AND nexus_stj = '*CITY' AND entity_id = ?3), 3 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Stjs', " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_county = '*STJ' " +
							"AND nexus_city = '*STJ' " +
							"AND nexus_stj = '*ALL' AND entity_id = ?3), 4 " +
					"FROM dual " +
					"ORDER  BY 3";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, entityId);
			list = q.getResultList();
		}
		else if("county".equalsIgnoreCase(type)) {
			sql = "SELECT tds.county, " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_county = tds.county " +
							"AND nexus_city = '*COUNTY' " +
							"AND nexus_stj = '*COUNTY' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.county " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.county_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy')" +
							") tds " +
					"ORDER  BY tds.county";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}
		else if("city".equalsIgnoreCase(type)) {
			sql = "SELECT tds.city, " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_city = tds.city " +
							"AND nexus_county = '*CITY' " +
							"AND nexus_stj = '*CITY' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.city " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.city_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
								"AND td.reporting_city is null" +
							") tds " +
					"ORDER  BY tds.city";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}
		else if("stj".equalsIgnoreCase(type)) {
			sql = "SELECT tds.stj1_Name, " +
						"(SELECT nexus_flag " +
							"FROM   tb_nexus_def " +
							"WHERE  nexus_country_code = ?2 " +
							"AND nexus_state_code = ?1 " +
							"AND nexus_stj = tds.stj1_Name " +
							"AND nexus_county = '*STJ' " +
							"AND nexus_city = '*STJ' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.stj1_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
								"AND td.stj1_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj2_Name, " +
							"(SELECT nexus_flag " +
								"FROM   tb_nexus_def " +
								"WHERE  nexus_country_code = ?2 " +
								"AND nexus_state_code = ?1 " +
								"AND nexus_stj = tds.stj2_Name " +
								"AND nexus_county = '*STJ' " +
								"AND nexus_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj2_name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj2_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj2_name is not null) tds " +
					"UNION " +
					"SELECT tds.stj3_Name, " +
							"(SELECT nexus_flag " +
								"FROM   tb_nexus_def " +
								"WHERE  nexus_country_code = ?2 " +
								"AND nexus_state_code = ?1 " +
								"AND nexus_stj = tds.stj3_Name " +
								"AND nexus_county = '*STJ' " +
								"AND nexus_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj3_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj3_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj3_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj4_Name, " +
							"(SELECT nexus_flag " +
								"FROM   tb_nexus_def " +
								"WHERE  nexus_country_code = ?2 " +
								"AND nexus_state_code = ?1 " +
								"AND nexus_stj = tds.stj4_Name " +
								"AND nexus_county = '*STJ' " +
								"AND nexus_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj4_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj4_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj4_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj5_Name, " +
							"(SELECT nexus_flag " +
								"FROM   tb_nexus_def " +
								"WHERE  nexus_country_code = ?2 " +
								"AND nexus_state_code = ?1 " +
								"AND nexus_stj = tds.stj5_Name " +
								"AND nexus_county = '*STJ' " +
								"AND nexus_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj5_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj5_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj5_Name is not null) tds " +
						"ORDER  BY 1";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}		
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Object[]> findRegistrationJurisdiction(Long entityId, String countryCode,
			String stateCode, String nexusIndCode, String type) throws DataAccessException {
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = null;
		List<Object[]> list = null;
		if("level".equalsIgnoreCase(type)) {
			sql = "SELECT 'All State', " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_county = '*STATE' " +
							"AND reg_city = '*STATE' " +
							"AND reg_stj = '*STATE' AND entity_id = ?3), 1 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Counties', " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_county = '*ALL' " +
							"AND reg_city = '*COUNTY' " +
							"AND reg_stj = '*COUNTY' AND entity_id = ?3), 2 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Cities', " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_county = '*CITY' " +
							"AND reg_city = '*ALL' " +
							"AND reg_stj = '*CITY' AND entity_id = ?3), 3 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Stjs', " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_county = '*STJ' " +
							"AND reg_city = '*STJ' " +
							"AND reg_stj = '*ALL' AND entity_id = ?3), 4 " +
					"FROM dual " +
					"ORDER  BY 3";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, entityId);
			list = q.getResultList();
		}
		else if("county".equalsIgnoreCase(type)) {
			sql = "SELECT tds.county, " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_county = tds.county " +
							"AND reg_city = '*COUNTY' " +
							"AND reg_stj = '*COUNTY' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.county " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.county_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy')" +
							") tds " +
					"ORDER  BY tds.county";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}
		else if("city".equalsIgnoreCase(type)) {
			sql = "SELECT tds.city, " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_city = tds.city " +
							"AND reg_county = '*CITY' " +
							"AND reg_stj = '*CITY' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.city " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.city_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
								"AND td.reporting_city is null" +
							") tds " +
					"ORDER  BY tds.city";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}
		else if("stj".equalsIgnoreCase(type)) {
			sql = "SELECT tds.stj1_Name, " +
						"(SELECT reg_country_code " +
							"FROM   tb_registration " +
							"WHERE  reg_country_code = ?2 " +
							"AND reg_state_code = ?1 " +
							"AND reg_stj = tds.stj1_Name " +
							"AND reg_county = '*STJ' " +
							"AND reg_city = '*STJ' AND entity_id = ?6) " +
					"FROM   (SELECT DISTINCT td.stj1_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?3 " +
								"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
								"AND td.stj1_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj2_Name, " +
							"(SELECT reg_country_code " +
								"FROM   tb_registration " +
								"WHERE  reg_country_code = ?2 " +
								"AND reg_state_code = ?1 " +
								"AND reg_stj = tds.stj2_Name " +
								"AND reg_county = '*STJ' " +
								"AND reg_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj2_name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj2_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj2_name is not null) tds " +
					"UNION " +
					"SELECT tds.stj3_Name, " +
							"(SELECT reg_country_code " +
								"FROM   tb_registration " +
								"WHERE  reg_country_code = ?2 " +
								"AND reg_state_code = ?1 " +
								"AND reg_stj = tds.stj3_Name " +
								"AND reg_county = '*STJ' " +
								"AND reg_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj3_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj3_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj3_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj4_Name, " +
							"(SELECT reg_country_code " +
								"FROM   tb_registration " +
								"WHERE  reg_country_code = ?2 " +
								"AND reg_state_code = ?1 " +
								"AND reg_stj = tds.stj4_Name " +
								"AND reg_county = '*STJ' " +
								"AND reg_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj4_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj4_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj4_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj5_Name, " +
							"(SELECT reg_country_code " +
								"FROM   tb_registration " +
								"WHERE  reg_country_code = ?2 " +
								"AND reg_state_code = ?1 " +
								"AND reg_stj = tds.stj5_Name " +
								"AND reg_county = '*STJ' " +
								"AND reg_city = '*STJ' AND entity_id = ?6) " +
						"FROM   (SELECT DISTINCT td.stj5_Name " +
									"FROM   tb_jurisdiction td " +
									"WHERE  td.state = ?1 " +
									"AND td.country = ?2 " +
									"AND td.stj5_nexusind_code = ?3 " +
									"AND td.effective_date <= to_date(?4, 'mm/dd/yyyy') " +
									"AND td.expiration_date >= to_date(?5, 'mm/dd/yyyy') " +
									"AND td.stj5_Name is not null) tds " +
						"ORDER  BY 1";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, nexusIndCode);
			q.setParameter(4, now);
			q.setParameter(5, now);
			q.setParameter(6, entityId);
			list = q.getResultList();
		}		
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Object[]> findSitusJurisdiction(String countryCode, String stateCode, String transactionTypeCode, String ratetypeCode, 
			String methodDeliveryCode, String nexusIndCode, String type) throws DataAccessException {
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = null;
		List<Object[]> list = null;
		if("level".equalsIgnoreCase(type)) {
			sql = "SELECT 'All State', " +
							"(SELECT SITUS_COUNTRY_CODE " +
					 		"FROM TB_SITUS_MATRIX " +
					 		"WHERE  SITUS_COUNTRY_CODE  = ?1 " +
					 		"AND SITUS_STATE_CODE = ?2 " +
					 		"AND SITUS_COUNTY = '*STATE' " +
					 		"AND SITUS_CITY = '*STATE' " +
					 		"AND SITUS_STJ = '*STATE' AND TRANSACTION_TYPE_CODE=?3 " +
					 		"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 AND ROWNUM=1), 1 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Counties', " +
						"(SELECT SITUS_COUNTRY_CODE " +
					 		"FROM TB_SITUS_MATRIX " +
					 		"WHERE  SITUS_COUNTRY_CODE  = ?1 " +
					 		"AND SITUS_STATE_CODE = ?2 " +
					 		"AND SITUS_COUNTY = '*ALL' " +
					 		"AND SITUS_CITY = '*COUNTY' " +
					 		"AND SITUS_STJ = '*COUNTY' AND TRANSACTION_TYPE_CODE=?3 " +
					 		"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 AND ROWNUM=1), 2 " +
					"FROM dual " +
					"UNION " +
					"SELECT 'All Cities', " +
						"(SELECT SITUS_COUNTRY_CODE " +
					 		"FROM TB_SITUS_MATRIX " +
					 		"WHERE  SITUS_COUNTRY_CODE  = ?1 " +
					 		"AND SITUS_STATE_CODE = ?2 " +
					 		"AND SITUS_COUNTY = '*CITY' " +
					 		"AND SITUS_CITY = '*ALL' " +
					 		"AND SITUS_STJ = '*CITY' AND TRANSACTION_TYPE_CODE=?3 " +
					 		"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 AND ROWNUM=1), 3 " +
					"FROM dual " +
					"UNION " +	
					"SELECT 'All Stjs', " +
						"(SELECT SITUS_COUNTRY_CODE " +
					 		"FROM TB_SITUS_MATRIX " +
					 		"WHERE  SITUS_COUNTRY_CODE  = ?1 " +
					 		"AND SITUS_STATE_CODE = ?2 " +
					 		"AND SITUS_COUNTY = '*STJ' " +
					 		"AND SITUS_CITY = '*STJ' " +
					 		"AND SITUS_STJ = '*ALL' AND TRANSACTION_TYPE_CODE=?3 " +
					 		"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 AND ROWNUM=1), 4 " +
					"FROM dual " +
					"ORDER  BY 3 ";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, countryCode);
			q.setParameter(2, stateCode);
			q.setParameter(3, transactionTypeCode);
			q.setParameter(4, ratetypeCode);
			q.setParameter(5, methodDeliveryCode);
			list = q.getResultList();
		}
		else if("county".equalsIgnoreCase(type)) {
			sql = "SELECT tds.county, " +
						"(SELECT SITUS_COUNTRY_CODE " +
							"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = tds.county " +
							"AND SITUS_CITY = '*COUNTY' " +
							"AND SITUS_STJ = '*COUNTY' AND TRANSACTION_TYPE_CODE=?3 " +
					 		"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
					"FROM   (SELECT DISTINCT td.county " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.county_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy')" +
							") tds " +
					"ORDER  BY tds.county ";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, transactionTypeCode);
			q.setParameter(4, ratetypeCode);
			q.setParameter(5, methodDeliveryCode);
			q.setParameter(6, nexusIndCode);
			q.setParameter(7, now);
			q.setParameter(8, now);
			list = q.getResultList();
		}
		else if("city".equalsIgnoreCase(type)) {
			sql = "SELECT tds.city, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
						"WHERE  SITUS_COUNTRY_CODE = ?2 " +
						"AND SITUS_STATE_CODE = ?1 " +
						"AND SITUS_COUNTY = '*CITY' " +
						"AND SITUS_CITY = tds.city " +
						"AND SITUS_STJ = '*CITY' AND TRANSACTION_TYPE_CODE=?3 " +
						"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
					"FROM   (SELECT DISTINCT td.city " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.city_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.reporting_city is null " +
							") tds " +
					"ORDER  BY tds.city ";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, transactionTypeCode);
			q.setParameter(4, ratetypeCode);
			q.setParameter(5, methodDeliveryCode);
			q.setParameter(6, nexusIndCode);
			q.setParameter(7, now);
			q.setParameter(8, now);
			list = q.getResultList();
		}
		else if("stj".equalsIgnoreCase(type)) {
			sql = "SELECT tds.stj1_Name, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = '*STJ' " +
							"AND SITUS_CITY = '*STJ' " +
							"AND SITUS_STJ = tds.stj1_Name AND TRANSACTION_TYPE_CODE=?3 " +
							"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
							"FROM   (SELECT DISTINCT td.stj1_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.stj1_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj2_Name, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = '*STJ' " +
							"AND SITUS_CITY = '*STJ' " +
							"AND SITUS_STJ = tds.stj2_Name AND TRANSACTION_TYPE_CODE=?3 " +
							"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
							"FROM   (SELECT DISTINCT td.stj2_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.stj2_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj3_Name, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = '*STJ' " +
							"AND SITUS_CITY = '*STJ' " +
							"AND SITUS_STJ = tds.stj3_Name AND TRANSACTION_TYPE_CODE=?3 " +
							"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
							"FROM   (SELECT DISTINCT td.stj3_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.stj3_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj4_Name, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = '*STJ' " +
							"AND SITUS_CITY = '*STJ' " +
							"AND SITUS_STJ = tds.stj4_Name AND TRANSACTION_TYPE_CODE=?3 " +
							"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
							"FROM   (SELECT DISTINCT td.stj4_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.stj4_Name is not null) tds " +
					"UNION " +
					"SELECT tds.stj5_Name, " +
					"(SELECT SITUS_COUNTRY_CODE " +
						"FROM   TB_SITUS_MATRIX " +
							"WHERE  SITUS_COUNTRY_CODE = ?2 " +
							"AND SITUS_STATE_CODE = ?1 " +
							"AND SITUS_COUNTY = '*STJ' " +
							"AND SITUS_CITY = '*STJ' " +
							"AND SITUS_STJ = tds.stj5_Name AND TRANSACTION_TYPE_CODE=?3 " +
							"AND RATETYPE_CODE=?4 AND METHOD_DELIVERY_CODE=?5 ) " +
							"FROM   (SELECT DISTINCT td.stj5_Name " +
								"FROM   tb_jurisdiction td " +
								"WHERE  td.state = ?1 " +
								"AND td.country = ?2 " +
								"AND td.stj1_nexusind_code = ?6 " +
								"AND td.effective_date <= to_date(?7, 'mm/dd/yyyy') " +
								"AND td.expiration_date >= to_date(?8, 'mm/dd/yyyy') " +
								"AND td.stj5_Name is not null) tds " +
						"ORDER  BY 1";
			EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);
			q.setParameter(1, stateCode);
			q.setParameter(2, countryCode);
			q.setParameter(3, transactionTypeCode);
			q.setParameter(4, ratetypeCode);
			q.setParameter(5, methodDeliveryCode);
			q.setParameter(6, nexusIndCode);
			q.setParameter(7, now);
			q.setParameter(8, now);
			list = q.getResultList();
		}		
		
		return list;
	}
	
	public List<Object[]> findJurisdictionForCountyCityStj(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = sqlFindJurisdictionForCountyCityStj;
		
		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		//String geocode = "010036520801";
		//String country = "US";
		//String zip = "36567";
		//String state = "AL";
		//String county = "BALDWIN";
		//String city = "ROBERTSDALE";
		//String stj = "ROBERTSDALE%";
		
		String geocodeWhere = "";
		String countryWhere = "";
		String zipWhere = "";
		String stateWhere = "";
		String countyWhere = "";
		String cityWhere = "";
		String stj1Where = "";
		String stj2Where = "";
		String stj3Where = "";
		String stj4Where = "";
		String stj5Where = "";
		String stjWhere = "";
		String effectivedateWhere = "tb_jurisdiction.effective_date <= to_date('" + now + "', 'mm/dd/yyyy')";
		String expirationdateWhere = "tb_jurisdiction.expiration_date >= to_date('" + now + "', 'mm/dd/yyyy')";
		
		//geocode
		if(geocode!=null && geocode.length()>0){
			startWild = geocode.startsWith("%");
			endWild = geocode.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				geocodeWhere = "tb_jurisdiction.geocode like '"+ geocode.toUpperCase() + "' ";
			}
			else{
				geocodeWhere = "tb_jurisdiction.geocode = '"+ geocode.toUpperCase() + "' ";
			}
		}
		else{
			geocodeWhere = "1=1 ";
		}
		
		//country
		if(country!=null && country.length()>0){
			countryWhere = "tb_jurisdiction.country = '"+ country.toUpperCase() + "' ";		
		}
		else{
			countryWhere = "1=1 ";
		}
		
		//zip
		if(zip!=null && zip.length()>0){
			zipWhere = "tb_jurisdiction.zip = '"+ zip.toUpperCase() + "' ";		
		}
		else{
			zipWhere = "1=1 ";
		}
		
		//state
		if(state!=null && state.length()>0){
			stateWhere = "tb_jurisdiction.state = '"+ state.toUpperCase() + "' ";		
		}
		else{
			stateWhere = "1=1 ";
		}
		
		//county
		if(county!=null && county.length()>0){
			startWild = county.startsWith("%");
			endWild = county.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				countyWhere = "tb_jurisdiction.county like '"+ county.toUpperCase() + "' ";
			}
			else{
				countyWhere = "tb_jurisdiction.county = '"+ county.toUpperCase() + "' ";
			}
		}
		else{
			countyWhere = "1=1 ";
		}
		
		//city
		if(city!=null && city.length()>0){
			startWild = city.startsWith("%");
			endWild = city.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				cityWhere = "tb_jurisdiction.city like '"+ city.toUpperCase() + "' ";
			}
			else{
				cityWhere = "tb_jurisdiction.city = '"+ city.toUpperCase() + "' ";
			}
		}
		else{
			cityWhere = "1=1 ";
		}
		
		//stj
		if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "tb_jurisdiction.stj1_name like '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name like '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name like '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name like '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "tb_jurisdiction.stj1_name = '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name = '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name = '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name = '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name = '"+ stj.toUpperCase() + "' ";
			}
		}
		else{
			stj1Where = "1=1 ";
			stj2Where = "1=1 ";
			stj3Where = "1=1 ";
			stj4Where = "1=1 ";
			stj5Where = "1=1 ";
		}
		
		stjWhere = "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		
		
		sql = sql.replaceAll(":selectedcountry", "'" + country + "'");
		sql = sql.replaceAll(":selectedstate", "'" + state + "'");
		
		//Looking for "Entity" or "Vendor"
		if(area.equalsIgnoreCase("Entity")){
			sql = sql.replaceAll(":tablename", "TB_NEXUS_DEF");
			sql = sql.replaceAll(":idcolumn", "ENTITY_ID");
		}
		else if(area.equalsIgnoreCase("Vendor")){
			sql = sql.replaceAll(":tablename", "TB_VENDOR_NEXUS");
			sql = sql.replaceAll(":idcolumn", "VENDOR_ID");
		}
		else{
			return null;
		}
		
		sql = sql.replaceAll(":id", id.toString());
		sql = sql.replaceAll(":geocode", geocodeWhere);
		sql = sql.replaceAll(":country", countryWhere);
		sql = sql.replaceAll(":state", stateWhere);
		sql = sql.replaceAll(":county", countyWhere);
		sql = sql.replaceAll(":city", cityWhere);
		sql = sql.replaceAll(":stj", stjWhere);
		sql = sql.replaceAll(":zip", zipWhere);	
		sql = sql.replaceAll(":effectivedate", effectivedateWhere);
		sql = sql.replaceAll(":expirationdate", expirationdateWhere);
		
		
		List<Object[]> list = null;

		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);

		list = q.getResultList();

		return list;
	}
	
	private String sqlFindJurisdictionForCountyCityStj =
			"SELECT tds.country, tds.state, tds.county AS county, ' ' AS city, ' ' AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry " +
			"    AND nexus_state_code=:selectedstate " +
			"    AND nexus_county = tds.county " +
			"    AND nexus_city='*COUNTY' " +
			"    AND nexus_stj='*COUNTY' " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    3 as jur_level, " +
			"    county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.county_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, tds.city AS city, ' ' AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +			
			"    AND nexus_county='*CITY' " +
			"    AND nexus_city=tds.city " +
			"    AND nexus_stj='*CITY' " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    4 as jur_level, " +
			"    county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.city, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.city_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, '' AS city, tds.stj1_name AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +
			"    AND nexus_county='*STJ' " +
			"    AND nexus_city='*STJ' " +
			"    AND nexus_stj=tds.stj1_name " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    5 as jur_level, " +
			"    county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.stj1_name, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.stj1_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, '' AS city, tds.stj2_name AS stj, " +
			"  (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +
			"    AND nexus_county='*STJ' " +
			"    AND nexus_city='*STJ' " +
			"    AND nexus_stj=tds.stj2_name " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    5 as jur_level, " +
			"	 county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.stj2_name, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.stj2_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, '' AS city, tds.stj3_name AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +
			"    AND nexus_county='*STJ' " +
			"    AND nexus_city='*STJ' " +
			"    AND nexus_stj=tds.stj3_name " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    5 as jur_level, " +
			"    county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.stj3_name, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.stj3_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, '' AS city, tds.stj4_name AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +		
			"    AND nexus_county='*STJ' " +
			"    AND nexus_city='*STJ' " +
			"    AND nexus_stj=tds.stj4_name " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    5 as jur_level, " +
			"    county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.stj4_name, tb_jurisdiction.county_nexusind_code " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.stj4_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"UNION " +
			"SELECT tds.country, tds.state, tds.county AS county, '' AS city, tds.stj5_name AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=:selectedcountry  " +
			"    AND nexus_state_code=:selectedstate " +
			"    AND nexus_county='*STJ' " +
			"    AND nexus_city='*STJ' " +
			"    AND nexus_stj=tds.stj5_name " +			
			"    AND nexus_flag='1' " +
			"    AND :idcolumn = :id) AS nexus_flag, " +
			"    5 as jur_level, " +
			"	 county_nexusind_code AS county_nexusind " +
			"FROM (SELECT DISTINCT tb_jurisdiction.country, tb_jurisdiction.state, tb_jurisdiction.county, tb_jurisdiction.stj5_name, tb_jurisdiction.county_nexusind_code " +
			"     FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.stj5_nexusind_code='I' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"      ) tds " +
			"ORDER BY country, state, county, jur_level, city, stj ";
	
	public List<Object[]> findJurisdictionForCountryState(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(new Date());
		
		String sql = sqlFindJurisdictionForCountryState;
		
		boolean startWild = false;
		boolean endWild = false;
		String field = "";
		String value = "";
		
		//String geocode = "010036520801";
		//String country = "US";
		//String zip = "36567";
		//String state = "AL";
		//String county = "BALDWIN";
		//String city = "ROBERTSDALE";
		//String stj = "ROBERTSDALE%";
		
		String geocodeWhere = "";
		String countryWhere = "";
		String zipWhere = "";
		String stateWhere = "";
		String countyWhere = "";
		String cityWhere = "";
		String stj1Where = "";
		String stj2Where = "";
		String stj3Where = "";
		String stj4Where = "";
		String stj5Where = "";
		String stjWhere = "";
		String effectivedateWhere = "tb_jurisdiction.effective_date <= to_date('" + now + "', 'mm/dd/yyyy')";
		String expirationdateWhere = "tb_jurisdiction.expiration_date >= to_date('" + now + "', 'mm/dd/yyyy')";
		
		//geocode
		if(geocode!=null && geocode.length()>0){
			startWild = geocode.startsWith("%");
			endWild = geocode.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				geocodeWhere = "tb_jurisdiction.geocode like '"+ geocode.toUpperCase() + "' ";
			}
			else{
				geocodeWhere = "tb_jurisdiction.geocode = '"+ geocode.toUpperCase() + "' ";
			}
		}
		else{
			geocodeWhere = "1=1 ";
		}
		
		//country
		if(country!=null && country.length()>0){
			countryWhere = "tb_jurisdiction.country = '"+ country.toUpperCase() + "' ";		
		}
		else{
			countryWhere = "1=1 ";
		}
		
		//zip
		if(zip!=null && zip.length()>0){
			zipWhere = "tb_jurisdiction.zip = '"+ zip.toUpperCase() + "' ";		
		}
		else{
			zipWhere = "1=1 ";
		}
		
		//state
		if(state!=null && state.length()>0){
			stateWhere = "tb_jurisdiction.state = '"+ state.toUpperCase() + "' ";		
		}
		else{
			stateWhere = "1=1 ";
		}
		
		//county
		if(county!=null && county.length()>0){
			startWild = county.startsWith("%");
			endWild = county.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				countyWhere = "tb_jurisdiction.county like '"+ county.toUpperCase() + "' ";
			}
			else{
				countyWhere = "tb_jurisdiction.county = '"+ county.toUpperCase() + "' ";
			}
		}
		else{
			countyWhere = "1=1 ";
		}
		
		//city
		if(city!=null && city.length()>0){
			startWild = city.startsWith("%");
			endWild = city.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				cityWhere = "tb_jurisdiction.city like '"+ city.toUpperCase() + "' ";
			}
			else{
				cityWhere = "tb_jurisdiction.city = '"+ city.toUpperCase() + "' ";
			}
		}
		else{
			cityWhere = "1=1 ";
		}
		
		//stj
		if(stj!=null && stj.length()>0){
			startWild = stj.startsWith("%");
			endWild = stj.endsWith("%");
			if(startWild || endWild){ //Like %12345 or 12345%	 	
				stj1Where = "tb_jurisdiction.stj1_name like '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name like '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name like '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name like '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name like '"+ stj.toUpperCase() + "' ";
			}
			else{
				stj1Where = "tb_jurisdiction.stj1_name = '"+ stj.toUpperCase() + "' ";
				stj2Where = "tb_jurisdiction.stj2_name = '"+ stj.toUpperCase() + "' ";
				stj3Where = "tb_jurisdiction.stj3_name = '"+ stj.toUpperCase() + "' ";
				stj4Where = "tb_jurisdiction.stj4_name = '"+ stj.toUpperCase() + "' ";
				stj5Where = "tb_jurisdiction.stj5_name = '"+ stj.toUpperCase() + "' ";
			}
		}
		else{
			stj1Where = "1=1 ";
			stj2Where = "1=1 ";
			stj3Where = "1=1 ";
			stj4Where = "1=1 ";
			stj5Where = "1=1 ";
		}
		
		stjWhere = "(" + stj1Where + " or " + stj2Where + " or " + stj3Where + " or " + stj4Where + " or " + stj5Where + ")";
		
		//Looking for "Entity" or "Vendor"
		if(area.equalsIgnoreCase("Entity")){
			sql = sql.replaceAll(":tablename", "TB_NEXUS_DEF");
			sql = sql.replaceAll(":idcolumn", "ENTITY_ID");
		}
		else if(area.equalsIgnoreCase("Vendor")){
			sql = sql.replaceAll(":tablename", "TB_VENDOR_NEXUS");
			sql = sql.replaceAll(":idcolumn", "VENDOR_ID");
		}
		else{
			return null;
		}
		
		sql = sql.replaceAll(":id", id.toString());
		sql = sql.replaceAll(":geocode", geocodeWhere);
		sql = sql.replaceAll(":country", countryWhere);
		sql = sql.replaceAll(":state", stateWhere);
		sql = sql.replaceAll(":county", countyWhere);
		sql = sql.replaceAll(":city", cityWhere);
		sql = sql.replaceAll(":stj", stjWhere);
		sql = sql.replaceAll(":zip", zipWhere);	
		sql = sql.replaceAll(":effectivedate", effectivedateWhere);
		sql = sql.replaceAll(":expirationdate", expirationdateWhere);
		
		
		List<Object[]> list = null;

		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
			javax.persistence.Query q = entityManager.createNativeQuery(sql);

		list = q.getResultList();

		return list;
	}
	
	private String sqlFindJurisdictionForCountryState =
			
			"SELECT tds.country, tds.state as state, ' ' AS county, ' ' AS city, ' ' AS stj, " +
			"   (SELECT count(*) " +
			"    FROM :tablename " +
			"    WHERE nexus_country_code=tds.country " +
			"    AND nexus_state_code=tds.state " +
			"    AND nexus_county='*STATE' " +
			"    AND nexus_city='*STATE' " +
			"    AND nexus_stj='*STATE' " +
			"    AND nexus_flag='1' " +
			"    AND :idcolumn=:id ) AS nexus_flag, " +
			"    2 as jur_level, " +

			"  (SELECT count(*) FROM tb_jurisdiction WHERE country=tds.country AND state=tds.state " + 
	
			"   AND :geocode " +
			"   AND :country " +
			"   AND :state " +
			"   AND :county " +
			"   AND :city " +
			"   AND :stj " +
			"   AND :zip " +  
			"   AND :effectivedate " +
			"   AND :expirationdate " +

			"	AND (county_nexusind_code='I' OR city_nexusind_code='I' OR stj1_nexusind_code='I' OR stj2_nexusind_code='I' OR stj3_nexusind_code='I' OR stj4_nexusind_code='I' OR stj5_nexusind_code='I')) AS lower_count " +	
			
			"FROM (SELECT DISTINCT country, state " +
			"      FROM tb_jurisdiction " +
			"      WHERE tb_jurisdiction.state_nexusind_code='S' " +
			"      AND :geocode " +
			"      AND :country " +
			"      AND :state " +
			"      AND :county " +
			"      AND :city " +
			"      AND :stj " +
			"      AND :zip " +  
			"      AND :effectivedate " +
			"      AND :expirationdate " +
			"		) tds " +  
			"ORDER BY country, state ";
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				else{
					orderByToken.append(" ORDER BY ");
				}
				
				if (field.getAscending()){
					orderByToken.append("tbj." + getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append("tbj." + getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	public List<String> checkJurisdictionNotUsed(Long jurisdictionId) {
		String[] tablesToSearch = new String[] {"TB_LOCATION_MATRIX", "TB_PURCHTRANS_LOCN", "TB_ALLOCATION_MATRIX_DETAIL", "TB_JUR_ALLOC_MATRIX_DETAIL", "TB_ENTITY", "TB_PURCHTRANS_LOG", "TB_BILLTRANS_LOCN", "TB_BILLTRANS_LOG"};
		String[] locnIdsToSearch = new String [] {"BILLTO_JURISDICTION_ID","SHIPFROM_JURISDICTION_ID","TTLXFR_JURISDICTION_ID","ORDRACPT_JURISDICTION_ID","FIRSTUSE_JURISDICTION_ID","ORDRORGN_JURISDICTION_ID","SHIPTO_JURISDICTION_ID"};

//		String[] billTransLocToSearch = new String [] {"BILLTO_JURISDICTION_ID", "CUST_JURISDICTION_ID","SHIPFROM_JURISDICTION_ID","TTLXFR_JURISDICTION_ID","ORDRACPT_JURISDICTION_ID","FIRSTUSE_JURISDICTION_ID","ORDRORGN_JURISDICTION_ID","SHIPTO_JURISDICTION_ID","CITY_SITUS_JUR_ID","COUNTRY_SITUS_JUR_ID","COUNTY_SITUS_JUR_ID","STATE_SITUS_JUR_ID","STJ10_SITUS_JUR_ID","STJ1_SITUS_JUR_ID","STJ2_SITUS_JUR_ID","STJ3_SITUS_JUR_ID","STJ4_SITUS_JUR_ID","STJ5_SITUS_JUR_ID","STJ6_SITUS_JUR_ID","STJ7_SITUS_JUR_ID","STJ8_SITUS_JUR_ID","STJ9_SITUS_JUR_ID"};
//		String[] saleTransToSearch = new String[] {"TTLXFR_JURISDICTION_ID","SHIPTO_JURISDICTION_ID","BILLTO_JURISDICTION_ID","FIRSTUSE_JURISDICTION_ID","ORDRACPT_JURISDICTION_ID","SHIPFROM_JURISDICTION_ID","CUST_JURISDICTION_ID","ORDRORGN_JURISDICTION_ID","CITY_SITUS_JUR_ID","COUNTRY_SITUS_JUR_ID","COUNTY_SITUS_JUR_ID","STATE_SITUS_JUR_ID","STJ10_SITUS_JUR_ID","STJ1_SITUS_JUR_ID","STJ2_SITUS_JUR_ID","STJ3_SITUS_JUR_ID","STJ4_SITUS_JUR_ID","STJ5_SITUS_JUR_ID","STJ6_SITUS_JUR_ID","STJ7_SITUS_JUR_ID","STJ8_SITUS_JUR_ID","STJ9_SITUS_JUR_ID"};
//		String[] saleTransLogToSearch = new String[] {"A_CITY_SITUS_JUR_ID","A_COUNTRY_SITUS_JUR_ID","A_COUNTY_SITUS_JUR_ID","A_STATE_SITUS_JUR_ID","A_STJ10_SITUS_JUR_ID","A_STJ1_SITUS_JUR_ID","A_STJ2_SITUS_JUR_ID","A_STJ3_SITUS_JUR_ID","A_STJ4_SITUS_JUR_ID","A_STJ5_SITUS_JUR_ID","A_STJ6_SITUS_JUR_ID","A_STJ7_SITUS_JUR_ID","A_STJ8_SITUS_JUR_ID","A_STJ9_SITUS_JUR_ID","B_CITY_SITUS_JUR_ID","B_COUNTRY_SITUS_JUR_ID","B_COUNTY_SITUS_JUR_ID","B_STATE_SITUS_JUR_ID","B_STJ10_SITUS_JUR_ID","B_STJ1_SITUS_JUR_ID","B_STJ2_SITUS_JUR_ID","B_STJ3_SITUS_JUR_ID","B_STJ4_SITUS_JUR_ID","B_STJ5_SITUS_JUR_ID","B_STJ6_SITUS_JUR_ID","B_STJ7_SITUS_JUR_ID","B_STJ8_SITUS_JUR_ID","B_STJ9_SITUS_JUR_ID"};
		String[] jurisdictionIdToSearch = new String[] {"JURISDICTION_ID"};
		
		List<String> idSearchCriteria = Arrays.asList(jurisdictionIdToSearch);
		List<String> searchTablesList = Arrays.asList(tablesToSearch);
		
		Map<String, List<String>> searchMap = new HashMap<String, List<String>>();
		
		for(String table : tablesToSearch) {
			searchMap.put(table, idSearchCriteria);
		}
		
		searchMap.put("TB_PURCHTRANS_LOCN", Arrays.asList(locnIdsToSearch));
		searchMap.put("TB_PURCHTRANS_LOG", Arrays.asList(locnIdsToSearch));
		searchMap.put("TB_BILLTRANS_LOCN", Arrays.asList(locnIdsToSearch));
		searchMap.put("TB_BILLTRANS_LOG", Arrays.asList(locnIdsToSearch));
//		searchMap.put("TB_BCP_SALETRANS", Arrays.asList(bcpSaleTransToSearch));
//		searchMap.put("TB_SALETRANS", Arrays.asList(saleTransToSearch));
//		searchMap.put("TB_SALETRANS_LOG", Arrays.asList(saleTransLogToSearch));
//		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();

		List<String> usedLocations = new ArrayList<String>();
		List<String> tableDescriptions = new ArrayList<String>();
		
		//In this display version, we do not break from searching once we have found a table.
		//Instead, we find all tables where it was located and return all these values.
		for (String tableName : searchMap.keySet()) {
			
			
			javax.persistence.Query q = entityManager.createNativeQuery(buildJurisdictionLookupSql(tableName, searchMap));
			q.setParameter(1, jurisdictionId);
			
			if(((BigDecimal)q.getSingleResult()).intValue() > 0) {
				if(tableName.contains("PURCHTRANS") && !tableName.contains("LOG") && !usedLocations.contains(tableDescriptionLookup(entityManager, "TB_PURCHTRANS"))) {
					usedLocations.add(tableDescriptionLookup(entityManager, "TB_PURCHTRANS"));
				} else if (tableName.contains("BILLTRANS") && !tableName.contains("LOG") && !usedLocations.contains(tableDescriptionLookup(entityManager, "TB_BILLTRANS"))) {
					usedLocations.add(tableDescriptionLookup(entityManager, "TB_BILLTRANS"));
				}
				else {
					String tableDescription = tableDescriptionLookup(entityManager, tableName);
					if(tableDescription == null)
						usedLocations.add(tableName);
					else
						usedLocations.add(tableDescription);
				}
			}
		}
		
		
		return usedLocations;
	}


	private String buildJurisdictionLookupSql(String tableName, Map<String, List<String>> searchMap) {
		StringBuilder sqlStringBuilder = new StringBuilder();
		sqlStringBuilder.append("SELECT COUNT(*) FROM ");
		sqlStringBuilder.append(tableName);
		sqlStringBuilder.append(" WHERE ");
		List<String> searchValues = searchMap.get(tableName);
		for(int i = 0; i < searchValues.size(); i++) {
			String columnName = searchValues.get(i);
			if(i > 0) sqlStringBuilder.append(" OR ");
			sqlStringBuilder.append(columnName);
			sqlStringBuilder.append(" = ?1 ");
		}
		
		return sqlStringBuilder.toString();
	}
	
	private String tableDescriptionLookup(EntityManager entityManager, String tableName) {
		
		String sqlString = "SELECT DESCRIPTION FROM TB_DATA_DEF_TABLE WHERE TABLE_NAME = ?1 ";
		javax.persistence.Query q = entityManager.createNativeQuery(sqlString);
		
		q.setParameter(1, tableName);
		return (String) q.getSingleResult();
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			
			columnMapping.put("jurisdictionId".toLowerCase(), "JURISDICTION_ID");
			columnMapping.put("geocode".toLowerCase(), "GEOCODE");
			columnMapping.put("state".toLowerCase(), "STATE");
			columnMapping.put("county".toLowerCase(), "COUNTY");
			columnMapping.put("city".toLowerCase(), "CITY");
			columnMapping.put("zip".toLowerCase(), "ZIP");
			columnMapping.put("zipplus4".toLowerCase(), "ZIPPLUS4");
			columnMapping.put("inOut".toLowerCase(), "IN_OUT");
			columnMapping.put("customFlag".toLowerCase(), "CUSTOM_FLAG");
			columnMapping.put("description".toLowerCase(), "DESCRIPTION");
			columnMapping.put("clientGeocode".toLowerCase(), "CLIENT_GEOCODE");
			columnMapping.put("compGeocode".toLowerCase(), "COMP_GEOCODE");
			columnMapping.put("country".toLowerCase(), "COUNTRY");
			columnMapping.put("reportingCity".toLowerCase(), "REPORTING_CITY");
			columnMapping.put("reportingCityFips".toLowerCase(), "REPORTING_CITY_FIPS");
			columnMapping.put("stj1Name".toLowerCase(), "STJ1_NAME");
			columnMapping.put("stj2Name".toLowerCase(), "STJ2_NAME");
			columnMapping.put("stj3Name".toLowerCase(), "STJ3_NAME");
			columnMapping.put("stj4Name".toLowerCase(), "STJ4_NAME");
			columnMapping.put("stj5Name".toLowerCase(), "STJ5_NAME");
			columnMapping.put("effectiveDate".toLowerCase(), "EFFECTIVE_DATE");
			columnMapping.put("expirationDate".toLowerCase(), "EXPIRATION_DATE");
			columnMapping.put("modifiedFlag".toLowerCase(), "MODIFIED_FLAG");
			columnMapping.put("countryNexusindCode".toLowerCase(), "COUNTRY_NEXUSIND_CODE");
			columnMapping.put("stateNexusindCode".toLowerCase(), "STATE_NEXUSIND_CODE");
			columnMapping.put("countyNexusindCode".toLowerCase(), "COUNTY_NEXUSIND_CODE");
			columnMapping.put("cityNexusindCode".toLowerCase(), "CITY_NEXUSIND_CODE");
			columnMapping.put("stj1NexusindCode".toLowerCase(), "STJ1_NEXUSIND_CODE");
			columnMapping.put("stj2NexusindCode".toLowerCase(), "STJ2_NEXUSIND_CODE");
			columnMapping.put("stj3NexusindCode".toLowerCase(), "STJ3_NEXUSIND_CODE");
			columnMapping.put("stj4NexusindCode".toLowerCase(), "STJ4_NEXUSIND_CODE");
			columnMapping.put("stj5NexusindCode".toLowerCase(), "STJ5_NEXUSIND_CODE");
			columnMapping.put("stj1LocalCode".toLowerCase(), "STJ1_LOCAL_CODE");
			columnMapping.put("stj2LocalCode".toLowerCase(), "STJ2_LOCAL_CODE");
			columnMapping.put("stj3LocalCode".toLowerCase(), "STJ3_LOCAL_CODE");
			columnMapping.put("stj4LocalCode".toLowerCase(), "STJ4_LOCAL_CODE");
			columnMapping.put("stj5LocalCode".toLowerCase(), "STJ5_LOCAL_CODE");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public Jurisdiction getJurisdictionFromAPIxxx(String geocode, String country, String state, String county,
								String city, String zip, String zipplus4, int georecon) throws DataAccessException {
		// 1	Highest Rate
		// 2	Lowest Rate
		// 3	Highest Jurisdictional Level
		// 4	Lowest Jurisdictional Level
		// 5	Highest Rate/Highest Jurisdictional Level
		// 6	Highest Rate/Lowest Jurisdictional Level
		// 7	Lowest Rate/Highest Jurisdictional Level
		// 8	Lowest Rate/Lowest Jurisdictional Level
		// 9	Highest Jurisdictional Level/Highest Rate
		// 10	Highest Jurisdictional Level/Lowest Rate
		// 11	Lowest Jurisdictional Level/Highest Rate
		// 12	Lowest Jurisdictional Level/Lowest Rate
		
		boolean exact = false;
		boolean criteriaSet = false;
		List <Jurisdiction> list = null;

		Session session = createLocalSession();
        Criteria criteria = session.createCriteria(Jurisdiction.class);
		
        criteriaSet |= buildCriteria(criteria, "geocode", geocode, exact);
		criteriaSet |= buildCriteria(criteria, "country", country, exact);
		criteriaSet |= buildCriteria(criteria, "state", state, exact);
		criteriaSet |= buildCriteria(criteria, "county", county, exact);
		criteriaSet |= buildCriteria(criteria, "city", city, exact);
		criteriaSet |= buildCriteria(criteria, "zip", zip, exact);
		criteriaSet |= buildCriteria(criteria, "zipplus4", zipplus4, exact);			
		list = (criteriaSet)? ((List<Jurisdiction>) criteria.list()):(new ArrayList<Jurisdiction>());		
		closeLocalSession(session);
		session=null;
		
		Jurisdiction jurisdiction = null;
		
		if(list!=null && list.size()>0){
			jurisdiction = (Jurisdiction) list.get(0);
		}
		
		return jurisdiction;
	}
	
	@SuppressWarnings("unchecked")
	public Jurisdiction getJurisdictionFromAPIQQQQQQQQQQ(String geocode, String country, String state, String county,
								String city, String zip, String zipplus4, int georecon) throws DataAccessException {
		// 1	Highest Rate
		// 2	Lowest Rate
		// 3	Highest Jurisdictional Level
		// 4	Lowest Jurisdictional Level
		// 5	Highest Rate/Highest Jurisdictional Level
		// 6	Highest Rate/Lowest Jurisdictional Level
		// 7	Lowest Rate/Highest Jurisdictional Level
		// 8	Lowest Rate/Lowest Jurisdictional Level
		// 9	Highest Jurisdictional Level/Highest Rate
		// 10	Highest Jurisdictional Level/Lowest Rate
		// 11	Lowest Jurisdictional Level/Highest Rate
		// 12	Lowest Jurisdictional Level/Lowest Rate
		

		
		//select j.* from tb_jurisdiction j inner join tb_jurisdiction_taxrate tr on j.jurisdiction_id = tr.jurisdiction_id
		//where j.city = 'WILDCAT LAKE' and j.county = 'KITSAP' 
		//order by j.state desc , tr.combined_use_rate desc 
		
		StringBuffer query = new StringBuffer();
		query.append("select j.* from tb_jurisdiction j inner join tb_jurisdiction_taxrate tr on j.jurisdiction_id = tr.jurisdiction_id ");
		query.append("where j.city = 'WILDCAT LAKE' and j.county = 'KITSAP' ");
		query.append("order by j.state desc , tr.combined_use_rate desc");

		Jurisdiction jurisdiction = null;
		
		Session session = createLocalSession();
		org.hibernate.SQLQuery q = null;
		try {
			q = session.createSQLQuery(query.toString());
			q.setFirstResult(0);
			q.setMaxResults(1);
			q.addEntity(Jurisdiction.class);
			
			List<Jurisdiction> list = q.list();
			if(list!=null && list.size()>0){
				jurisdiction = (Jurisdiction) list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return jurisdiction;
	}
	
	@SuppressWarnings("unchecked")
	public Jurisdiction getJurisdictionFromAPI(String geocode, String country, String state, String county,
								String city, String zip, String zipplus4, String geoReconCode) throws SQLException {
		
		com.seconddecimal.billing.domain.Jurisdiction jurisdiction = null;
		Date glDate = null;	
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			glDate = dateFormat.parse(dateFormat.format(new Date()));
		}
		catch(Exception e){		
		}
		
		Session session = createLocalSession();
		Connection conn = null;
		try {
			//conn = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			conn = session.connection();
			if (conn != null) {
				jurisdiction = (new com.seconddecimal.billing.dao.JurisdictionDao()).getJurisdiction(conn, glDate, geoReconCode, geocode, city,
						county, state, zip, zipplus4, country);
			}
		//} catch (SQLException se) {
		//	se.printStackTrace();
		}
		finally{
			if (conn != null) {
				try { conn.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
			closeLocalSession(session);
			session=null;
		}
		
		//Convert com.seconddecimal.billing.domain.Jurisdiction jurisdiction to com.ncsts.domain.Jurisdiction
		Jurisdiction jurReturn = new Jurisdiction();
		BeanUtils.copyProperties(jurisdiction, jurReturn);
			
		return jurReturn;
	
		// 1	Highest Rate
		// 2	Lowest Rate
		// 3	Highest Jurisdictional Level
		// 4	Lowest Jurisdictional Level
		// 5	Highest Rate/Highest Jurisdictional Level
		// 6	Highest Rate/Lowest Jurisdictional Level
		// 7	Lowest Rate/Highest Jurisdictional Level
		// 8	Lowest Rate/Lowest Jurisdictional Level
		// 9	Highest Jurisdictional Level/Highest Rate
		// 10	Highest Jurisdictional Level/Lowest Rate
		// 11	Lowest Jurisdictional Level/Highest Rate
		// 12	Lowest Jurisdictional Level/Lowest Rate
	/*	
		Jurisdiction jurisdiction = null;
		Date glDate = null;	
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			glDate = dateFormat.parse(dateFormat.format(new Date()));
		}
		catch(Exception e){		
		}
		
		java.sql.Date glDateSql = DateUtils.getSqlDate(glDate);
		
		if(!org.springframework.util.StringUtils.hasText(zip) && !org.springframework.util.StringUtils.hasText(geocode)) {
			//No zip or geocode
			jurisdiction = new Jurisdiction();
			jurisdiction.setJurisdictionId(0L);
			return jurisdiction;
		}
		
		StringBuilder sql = new StringBuilder(SqlQuery.JURISDICTION_SELECT);
		List<Object> params = new ArrayList<Object>();
		params.add(glDateSql);
		params.add(glDateSql);
		params.add(glDateSql);
		params.add(glDateSql);
		
		//Build WHERE clause
		StringBuilder whereClause = new StringBuilder();
		if(org.springframework.util.StringUtils.hasText(geocode)) {
			whereClause.append("AND tb_jurisdiction.geocode = ? ");
			params.add(geocode);
		}
		
		if(org.springframework.util.StringUtils.hasText(city)) {
			whereClause.append("AND tb_jurisdiction.city = ? ");
			params.add(city.toUpperCase());
		}
		
		if(org.springframework.util.StringUtils.hasText(county)) {
			whereClause.append("AND tb_jurisdiction.county = ? ");
			params.add(county.toUpperCase());
		}
		
		if(org.springframework.util.StringUtils.hasText(state)) {
			whereClause.append("AND tb_jurisdiction.state = ? ");
			params.add(state.toUpperCase());
		}
		
		if(org.springframework.util.StringUtils.hasText(zip)) {
			whereClause.append("AND tb_jurisdiction.zip = ? ");
			params.add(zip);
		}
		
		if(org.springframework.util.StringUtils.hasText(country)) {
			whereClause.append("AND tb_jurisdiction.country = ? ");
			params.add(country.toUpperCase());
		}
		
		//Build ORDER BY clause
		StringBuilder orderBy = new StringBuilder("ORDER BY tb_jurisdiction_taxrate.effective_date DESC ");
		if("1".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("2".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		else if("3".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("4".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("5".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("6".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("7".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("8".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("9".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("10".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		else if("11".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("12".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		
		sql.append(whereClause).append(orderBy);
		
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		for(int i=0; i< params.size(); i++) {
			Object o = params.get(i);
			if(o instanceof java.sql.Date) {
				ps.setDate(i+1, (java.sql.Date) o);
			}
			else {
				ps.setString(i+1, (String) o);
			}
		}
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			jurisdiction = buildJurisdiction(jurisdiction, rs);
		}
		
		ps.close();
		rs.close();
		
		if(jurisdiction == null) {
			jurisdiction = new Jurisdiction();
			jurisdiction.setJurisdictionId(0L);
		}
		
		return jurisdiction;
		*/
	}
	
	public Jurisdiction getJurisdictionByGeoRecon(Date glDate, String geoReconCode, String geocode, String city,
			String county, String state, String zip, String zipplus4, String country) throws SQLException {
	
		//check if all geocode, city,county, state, zip, (zipplus4), country are null or empty , return null;
		if(StringUtils.isNotBlank(geocode) || StringUtils.isNotBlank(city) || StringUtils.isNotBlank(county) ||
				StringUtils.isNotBlank(state) || StringUtils.isNotBlank(zip) || StringUtils.isNotBlank(country)) {
		}
		else {
			return null;
		}
		
		//if glDate is null, make it today. <glDate>04/26/2019</glDate>
		java.sql.Date glDateSql = DateUtils.getSqlDate((glDate==null) ? new Date() : glDate);
		
		StringBuilder sql = new StringBuilder(JURISDICTION_SELECT);
		List<Object> params = new ArrayList<Object>();
		params.add(glDateSql);
		params.add(glDateSql);
		params.add(glDateSql);
		params.add(glDateSql);
		
		//Build WHERE clause
		StringBuilder whereClause = new StringBuilder();
		if(StringUtils.isNotBlank(geocode)) {
			whereClause.append("AND tb_jurisdiction.geocode = ? ");
			params.add(geocode);
		}
		
		if(StringUtils.isNotBlank(city)) {
			whereClause.append("AND tb_jurisdiction.city = ? ");
			params.add(city.toUpperCase());
		}
		
		if(StringUtils.isNotBlank(county)) {
			whereClause.append("AND tb_jurisdiction.county = ? ");
			params.add(county.toUpperCase());
		}
		
		if(StringUtils.isNotBlank(state)) {
			whereClause.append("AND tb_jurisdiction.state = ? ");
			params.add(state.toUpperCase());
		}
		
		if(StringUtils.isNotBlank(zip)) {
			whereClause.append("AND tb_jurisdiction.zip = ? ");
			params.add(zip);
		}
		
		/*
		if(StringUtils.isNotBlank(zipplus4)) {
			whereClause.append("AND tb_jurisdiction.zipplus4 = ? ");
			params.add(zipplus4);
		}
		*/
		
		if(StringUtils.isNotBlank(country)) {
			whereClause.append("AND tb_jurisdiction.country = ? ");
			params.add(country.toUpperCase());
		}
		
		//Build ORDER BY clause
		StringBuilder orderBy = new StringBuilder("ORDER BY tb_jurisdiction_taxrate.effective_date DESC ");
		if("1".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("2".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		else if("3".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("4".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("5".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("6".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate DESC, binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("7".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, binary_weight DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("8".equals(geoReconCode)) {
			orderBy.append(", tb_jurisdiction_taxrate.combined_use_rate, binary_weight, tb_jurisdiction.jurisdiction_id ");
		}
		else if("9".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("10".equals(geoReconCode)) {
			orderBy.append(", binary_weight DESC, tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		else if("11".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction_taxrate.combined_use_rate DESC, tb_jurisdiction.jurisdiction_id ");
		}
		else if("12".equals(geoReconCode)) {
			orderBy.append(", binary_weight, tb_jurisdiction_taxrate.combined_use_rate, tb_jurisdiction.jurisdiction_id ");
		}
		
		sql.append(whereClause).append(orderBy);
		
		Session session = createLocalSession();
		Jurisdiction jurisdiction = null;
		Connection conn = null;
		PreparedStatement prep = null;
		try {
			conn = session.connection();
			PreparedStatement ps = conn.prepareStatement(sql.toString());
			for(int i=0; i< params.size(); i++) {
				Object o = params.get(i);
				if(o instanceof java.sql.Date) {
					ps.setDate(i+1, (java.sql.Date) o);
				}
				else {
					ps.setString(i+1, (String) o);
				}
			}
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				jurisdiction = buildJurisdiction(jurisdiction, rs);
			}
			
			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try {
					conn.close();
				} catch (SQLException ignore) {}
			}
			closeLocalSession(session);
			session=null;
		}

		return jurisdiction;
	}
	
	private Jurisdiction buildJurisdiction(Jurisdiction jurisdiction, ResultSet rs) throws SQLException {
		if(jurisdiction == null) {
			jurisdiction = new Jurisdiction();
		}
		
		jurisdiction.setJurisdictionId(rs.getLong("JURISDICTION_ID"));
		jurisdiction.setGeocode(rs.getString("GEOCODE"));
		jurisdiction.setCity(rs.getString("CITY"));
		jurisdiction.setCounty(rs.getString("COUNTY"));
		jurisdiction.setState(rs.getString("STATE"));
		jurisdiction.setZip(rs.getString("ZIP"));
		jurisdiction.setZipplus4(rs.getString("ZIPPLUS4"));
		jurisdiction.setCountry(rs.getString("COUNTRY"));
		jurisdiction.setStj1Name(rs.getString("STJ1_NAME"));
		jurisdiction.setStj2Name(rs.getString("STJ2_NAME"));
		jurisdiction.setStj3Name(rs.getString("STJ3_NAME"));
		jurisdiction.setStj4Name(rs.getString("STJ4_NAME"));
		jurisdiction.setStj5Name(rs.getString("STJ5_NAME"));
		jurisdiction.setCountryNexusindCode(rs.getString("COUNTRY_NEXUSIND_CODE"));
		jurisdiction.setStateNexusindCode(rs.getString("STATE_NEXUSIND_CODE"));
		jurisdiction.setCountyNexusindCode(rs.getString("COUNTY_NEXUSIND_CODE"));
		jurisdiction.setCityNexusindCode(rs.getString("CITY_NEXUSIND_CODE"));
		jurisdiction.setStj1NexusindCode(rs.getString("STJ1_NEXUSIND_CODE"));
		jurisdiction.setStj2NexusindCode(rs.getString("STJ2_NEXUSIND_CODE"));
		jurisdiction.setStj3NexusindCode(rs.getString("STJ3_NEXUSIND_CODE"));
		jurisdiction.setStj4NexusindCode(rs.getString("STJ4_NEXUSIND_CODE"));
		jurisdiction.setStj5NexusindCode(rs.getString("STJ5_NEXUSIND_CODE"));	
		jurisdiction.setStj1LocalCode(rs.getString("STJ1_LOCAL_CODE"));
		jurisdiction.setStj2LocalCode(rs.getString("STJ2_LOCAL_CODE"));
		jurisdiction.setStj3LocalCode(rs.getString("STJ3_LOCAL_CODE"));
		jurisdiction.setStj4LocalCode(rs.getString("STJ4_LOCAL_CODE"));
		jurisdiction.setStj5LocalCode(rs.getString("STJ5_LOCAL_CODE"));	
//		jurisdiction.setBinaryWeight(rs.getLong("BINARY_WEIGHT"));
		
		return jurisdiction;
	}
	
	public static final String JURISDICTION_SELECT = "SELECT tb_jurisdiction.jurisdiction_id, tb_jurisdiction.geocode, tb_jurisdiction.city, " +
			"tb_jurisdiction.county, tb_jurisdiction.state, tb_jurisdiction.zip, tb_jurisdiction.zipplus4, tb_jurisdiction.country, "
			+ "STJ1_NAME, STJ2_NAME, STJ3_NAME, STJ4_NAME, STJ5_NAME, "
			+ "COUNTRY_NEXUSIND_CODE, STATE_NEXUSIND_CODE, COUNTY_NEXUSIND_CODE, CITY_NEXUSIND_CODE, "
			+ "STJ1_NEXUSIND_CODE, STJ2_NEXUSIND_CODE, STJ3_NEXUSIND_CODE, STJ4_NEXUSIND_CODE, STJ5_NEXUSIND_CODE, "		
			+ "STJ1_LOCAL_CODE, STJ2_LOCAL_CODE, STJ3_LOCAL_CODE, STJ4_LOCAL_CODE, STJ5_LOCAL_CODE, "	
			+ "DECODE(country_use_tier1_rate,NULL,0,0,0,16) + DECODE(state_use_tier1_rate,NULL,0,0,0,8) + DECODE(county_use_tier1_rate,NULL,0,0,0,4) + DECODE(city_use_tier1_rate,NULL,0,0,0,2) + "
			+ "DECODE((DECODE(stj1_use_rate,NULL,0,0,0,.01) + DECODE(stj2_use_rate,NULL,0,0,0,.01) + DECODE(stj3_use_rate,NULL,0,0,0,.01) + DECODE(stj4_use_rate,NULL,0,0,0,.01) + DECODE(stj5_use_rate,NULL,0,0,0,.01)),NULL,0,0,0,1) as binary_weight "			
			+ "FROM tb_jurisdiction, tb_jurisdiction_taxrate "
			+ "WHERE tb_jurisdiction.jurisdiction_id=tb_jurisdiction_taxrate.jurisdiction_id "
			+ "AND tb_jurisdiction.reporting_city IS NULL "
			+ "AND trunc(tb_jurisdiction.effective_date) <= ? "
			+ "AND trunc(tb_jurisdiction.expiration_date) >= ? "
			+ "AND trunc(tb_jurisdiction_taxrate.effective_date) <= ? "
			+ "AND trunc(tb_jurisdiction_taxrate.expiration_date) >= ? ";
	
	public static void main(String[] arg){
		
		java.util.Date date = new Date();
		System.out.println("date : " + date.getTime() +  date.toString());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		//String todayString = dateFormat.format(date);
		
		
		
		try{
		//java.util.Date dateToday =  dateFormat.parse(todayString);
		//System.out.println("dateToday : " + dateToday.getTime()  +  dateToday.toString());
		
		Date dateWithoutTime = dateFormat.parse(dateFormat.format(date));
		
		System.out.println("datdateWithoutTimeeToday : " + dateWithoutTime.getTime()  +  dateWithoutTime.toString());
		
		
		}
		catch(Exception e){
			
		}
	}
}
