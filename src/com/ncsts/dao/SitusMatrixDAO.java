package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.SitusMatrix;

public interface SitusMatrixDAO extends GenericDAO<SitusMatrix, Long> {
	//public abstract TaxHoliday findByTaxHolidayCode(String taxHolidayCode) throws DataAccessException;
	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
//	public abstract Long count(Criteria criteria);
//	public abstract List<SitusMatrix> find(Criteria criteria);
//	public abstract List<SitusMatrix> find(DetachedCriteria criteria);
	public abstract SitusMatrix find(SitusMatrix exampleInstance);
	public List<SitusMatrix> findByExample(SitusMatrix exampleInstance);
	
	public List<SitusMatrix> getAllRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public List<SitusMatrix> getAllDetailRecords(SitusMatrix exampleInstance, String display) throws DataAccessException;
	
	public Long count(SitusMatrix exampleInstance);
	public List<SitusMatrix> findAllSitusMatrixDetailBySitusMatrixId(Long situsMatrixId) throws DataAccessException;
	public void addSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void updateSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void inactiveSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void deleteSitusMatrix(Long situsMatrixId) throws DataAccessException;
	
	public List<SitusMatrix> getMasterRulesRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	public List<SitusMatrix> getMasterRulesDetailRecords(SitusMatrix situsMatrix);
	public Long masterRulesCount(SitusMatrix exampleInstance);
	List<SitusMatrix> getSitusMatrix(Date glDate, String transactionTypeCode,
									 String ratetypeCode, String methodDeliveryCode, String situsCountryCode,
									 String situsStateCode, String situsJurLevel,
									 String driver01, String driver02, String driver03, String driver04, String driver05, String driver06, String driver07) throws DataAccessException;
	
}
