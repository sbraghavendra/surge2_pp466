package com.ncsts.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.BillTransaction;

public interface TransactionDetailBillDAO extends GenericDAO<BillTransaction, Long> {

	public Long count(BillTransaction exampleInstance);
	
	public List<BillTransaction> getAllRecords(BillTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults);

	HashMap<String, HashMap<String, String>> getNexusInfo(Long saleTransactionId);
	public HashMap<String, String> getTaxRateInfo(Long billtransId);
}
