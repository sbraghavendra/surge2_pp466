package com.ncsts.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.Role;

public class JPARoleDAO extends JpaDaoSupport implements RoleDAO {
	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) 
		      ? ((HibernateEntityManager) entityManager).getSession()
		      : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
	  
    	logger.info("===== createLocalSession() Hibernate Session Entity Count = " + localsession.getStatistics().getEntityCount() + "====" );    	
    	return localsession;
	}
	    
	protected void closeLocalSession(Session localSession) {
		if(localSession!=null && localSession.isOpen()){
    		localSession.clear();
    		localSession.close();
    	}
	}

	  @Transactional
	  public Role findById(String roleCode) throws DataAccessException {
	    return getJpaTemplate().find(Role.class, roleCode);
	  }
	  
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Role> findAllRoles() throws DataAccessException {
	      return getJpaTemplate().find(" select role from Role role");	  
	  }
	  
	  @Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	  public void remove(String id) throws DataAccessException {
	      Role role = getJpaTemplate().find(Role.class, id);
		  if( role != null){
		      getJpaTemplate().remove(role);
		  }else{
		      logger.info("Role Object not found for deletion");
		  }
	  } 
	  
	  public void saveOrUpdate (Role role) throws DataAccessException {	
		  Session session = createLocalSession();
	      Transaction t = session.beginTransaction();
	      t.begin();
	      session.saveOrUpdate(role);
	      t.commit();
	      closeLocalSession(session);
	      session=null;
	  }	
		
	  @Transactional
	  public String persist (Role role) throws DataAccessException {
	      getJpaTemplate().persist(role);
		  String key = role.getRoleDTO().getRoleCode();
		  return key;
	  }  		
	  
}

