/*
 * Author: Jim M. Wilson
 * Created: Aug 23, 2008
 * $Date: 2008-08-28 12:48:23 -0500 (Thu, 28 Aug 2008) $: - $Revision: 1998 $: 
 */

package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRatePK;
import com.ncsts.domain.BatchMaintenance;

/**
 *
 */
public interface BCPJurisdictionTaxRateDAO 	extends GenericDAO<BCPJurisdictionTaxRate, BCPJurisdictionTaxRatePK> {
	public Long count(BatchMaintenance batch);
	public List<BCPJurisdictionTaxRate> getPage(BatchMaintenance batch, int firstRow, int maxResults) throws DataAccessException;
}
