/**
 * 
 */
package com.ncsts.dao;

import java.util.List;
import java.util.Map;

import com.ncsts.domain.DataDefinitionColumnDrivers;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;;

/**
 * @author Anand
 * TODO : Document this interface
 */
public interface DriverReferenceDAO {

	public List<DriverReference> findDriverReference(String transDtlColName, String driverValue, String driverDesc, String userValue, int count) throws DataAccessException;

	public List<DriverReference> findByTransactionDetailColumnName(String columnName, int count) throws DataAccessException ;
	
	public List<DriverReference> findByDriverName(String drvValue, String columnName ,int count) throws DataAccessException; 
	
	public List<String> getDriverNamesByDrvCodeMatrixCol (String drvCode, String mtrxCol) throws DataAccessException;
	
	public DriverReference getDriverReference(String transDtlColName, String driverValue);
	public Long countDriverReference(String transDtlColName, String driverValue);

	/**
	 * @return A distinct list of driver category codes from the TB_DRIVER_NAMES table.
	 * @throws DataAccessException
	 */
	public List<String> getDriverCategories() throws DataAccessException;
	
	public List<DriverNames> getDriverNamesByCode(String code) throws DataAccessException;
	
	
	
	public DriverNames findDriverForTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException;
	
	public DriverNames findTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException;
	
	public void saveOrUpdate(DriverReference driverRef) throws DataAccessException;
	
	public void delete(DriverReference driverRef) throws DataAccessException;
	
	public Long getDriverNamesId(String drvCode) throws DataAccessException;
	
	public void saveOrUpdateNames(DriverNames driverName) throws DataAccessException;
	
	public void deleteNames(DriverNames driverName) throws DataAccessException;
	
	public boolean getTransactionDetailUpdate(String category,String driverId) throws DataAccessException;//4356
	
	public String findCurrentTrColumnName(String category,String driverId )throws DataAccessException;
	
	public boolean allowColChange(String code, String matrixColName) throws DataAccessException;

	Map<String, DataDefinitionColumnDrivers> getValidDrivers(String driverNamesCode, String tableName);
}
