/**
 * 
 */
package com.ncsts.dao;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;

public class JPAProctableDAO extends JPAGenericDAO<Proctable, Long> implements ProctableDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	public Session getCurrentSession(){
		return createLocalSession();
	}
	
	@Transactional
	public void addProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			session.save(updateProctable);
			session.flush();

			for (ProctableDetail aProctableDetail : updateProctableEntityList) {
				aProctableDetail.setProctableId(updateProctable.getProctableId());
				session.save(aProctableDetail);
			}
					
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteProctable(Proctable updateProctable) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlDelete = "";
			
			int deleteCount = 0;

			// First delete all TB_PROCTABLE_DETAIL, Proctable_ID by ProctableId
			hqlDelete = "delete TB_Proctable_DETAIL WHERE Proctable_ID = " + updateProctable.getProctableId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			// Second delete TB_Proctable, Proctable_ID by ProctableId
			hqlDelete = "delete TB_Proctable WHERE Proctable_ID = " + updateProctable.getProctableId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteProctableDetail(ProctableDetail aProctableDetail) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlDelete = "";
			hqlDelete = "delete from TB_Proctable_DETAIL WHERE PROCTABLE_DETAIL_ID = " + aProctableDetail.getProctableDetailId().intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			Proctable aProctable = findById(updateProctable.getId());
			if(aProctable==null){
				return;
			}

			aProctable.setActiveFlag(updateProctable.getActiveFlag());

			session.update(aProctable);
			
			// First delete all TB_Proctable_DETAIL, Proctable_ID by ProctableId
			String hqlDelete = "delete TB_Proctable_DETAIL WHERE Proctable_ID = " + updateProctable.getProctableId().intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			for (ProctableDetail aProctableDetail : updateProctableEntityList) {
				aProctableDetail.setProctableId(updateProctable.getProctableId());
				session.save(aProctableDetail);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	public Long count(Proctable exampleInstance) {
		Session localSession = this.createLocalSession();
		Long returnLong = 0L;
		
		try {
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProctableDetail> findProctableDetail(Long proctableId) throws DataAccessException {	
		List<ProctableDetail> returnList = new ArrayList<ProctableDetail>();
		
		Session localSession = this.createLocalSession();
		try {
			Criteria criteria = localSession.createCriteria(ProctableDetail.class);
			criteria.add(Restrictions.eq("proctableId",proctableId));
	
			returnList = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return returnList;
	}
	
	static String createRemoveSql(String operatorStr, String numberStr, String proctableId){
		String sql = "UPDATE TB_PROCTABLE_DETAIL SET ";
		
		int columnId = Integer.parseInt(numberStr);
		columnId++;
		
		NumberFormat formatter = new DecimalFormat("00");
		
		if(operatorStr.equalsIgnoreCase("IN")){
			for(int i=columnId; i<20; i++){
				sql = sql + "IN_COLUMN_" + formatter.format(i) + "=IN_COLUMN_" + formatter.format(i+1) + ", ";
			
			}
			sql = sql + "IN_COLUMN_20=NULL ";
		}
		else if(operatorStr.equalsIgnoreCase("OUT")){
			for(int i=columnId; i<10; i++){
				sql = sql + "OUT_COLUMN_NAME_" + formatter.format(i) + "=OUT_COLUMN_NAME_" + formatter.format(i+1) + ", ";
				sql = sql + "OUT_COLUMN_TYPE_" + formatter.format(i) + "=OUT_COLUMN_TYPE_" + formatter.format(i+1) + ", ";
			
			}
			sql = sql + "OUT_COLUMN_NAME_10=NULL, ";
			sql = sql + "OUT_COLUMN_TYPE_10=NULL ";
		}
		
		
		sql = sql + "WHERE PROCTABLE_ID=" + proctableId;

		return sql;
	}
	
	static String createChangeSql(String operatorStr, String numberStr, String proctableId){
		String sql = "UPDATE TB_PROCTABLE_DETAIL SET ";
		
		int columnId = Integer.parseInt(numberStr);
		columnId++;
		
		NumberFormat formatter = new DecimalFormat("00");
		
		if(operatorStr.equalsIgnoreCase("IN")){
			sql = sql + "IN_COLUMN_" + formatter.format(columnId) + "=NULL ";
		}
		else if(operatorStr.equalsIgnoreCase("OUT")){
			sql = sql + "OUT_COLUMN_NAME_" + formatter.format(columnId) + "=NULL, ";
			sql = sql + "OUT_COLUMN_TYPE_" + formatter.format(columnId) + "=NULL ";
		}
		
		sql = sql + "WHERE PROCTABLE_ID=" + proctableId;

		return sql;
	}
	
	public static void main(String[] args) {

		ArrayList changedArrayList = new ArrayList<ArrayList>();
		changedArrayList.add(new String[]{"ADD", "IN", "3"});
		changedArrayList.add(new String[]{"REMOVE", "IN", "4"});
		changedArrayList.add(new String[]{"ADD", "OUT", "6"});
		changedArrayList.add(new String[]{"REMOVE", "OUT", "6"});
		changedArrayList.add(new String[]{"CHANGE", "IN", "4"});
		changedArrayList.add(new String[]{"CHANGE", "OUT", "3"});
		changedArrayList.add(new String[]{"REMOVE", "IN", "3"});
		changedArrayList.add(new String[]{"REMOVE", "OUT", "4"});
		
		if(changedArrayList!=null && changedArrayList.size()>0){
			for(int i=0; i< changedArrayList.size(); i++){
				String[] aChangedItems = (String[]) changedArrayList.get(i);
				
				if(aChangedItems[0].equalsIgnoreCase("REMOVE")){
					String removeSql = createRemoveSql(aChangedItems[1], aChangedItems[2], "12345");
					
					System.out.println("SQL " + i + " : ");
					System.out.println(removeSql);
				
				}
				else if(aChangedItems[0].equalsIgnoreCase("CHANGE")){
					String changeSql = createChangeSql(aChangedItems[1], aChangedItems[2], "12345");
					
					System.out.println("SQL " + i + " : ");
					System.out.println(changeSql);
					
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void updateProctableDetail(Proctable aProctable, ArrayList changedArrayList) throws DataAccessException {			
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			session.update(aProctable);
			if(changedArrayList!=null && changedArrayList.size()>0){
				for(int i=0; i< changedArrayList.size(); i++){
					String[] aChangedItems = (String[]) changedArrayList.get(i);
					
					if(aChangedItems[0].equalsIgnoreCase("REMOVE")){
						String removeSql = createRemoveSql(aChangedItems[1], aChangedItems[2], aProctable.getProctableId().toString());
						query = session.createSQLQuery(removeSql);
						query.executeUpdate();
					}
					else if(aChangedItems[0].equalsIgnoreCase("CHANGE")){
						String changeSql = createChangeSql(aChangedItems[1], aChangedItems[2], aProctable.getProctableId().toString());
						query = session.createSQLQuery(changeSql);
						query.executeUpdate();
					}
				}
			}

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	@Transactional
	public void populateProcruleDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableDetailList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			//When active flag changed
			session.update(updateProctable);
			
			// First delete all TB_PROCTABLE_DETAIL, PROCRULE_ID by ProcruleId
			String hqlDelete = "delete TB_PROCTABLE_DETAIL WHERE PROCTABLE_ID = " + updateProctable.getProctableId().intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();
			
			for (ProctableDetail aProctableDetail : updateProctableDetailList) {
				aProctableDetail.setProctableId(updateProctable.getProctableId());
				session.save(aProctableDetail);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void reorderProctableList(List<Proctable> updateProctableList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			for (Proctable aProctable : updateProctableList) {
				session.update(aProctable);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Proctable> find(Proctable exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {

		List<Proctable> list = new ArrayList<Proctable>();
		
		Session localSession = this.createLocalSession();
		try {  
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Proctable> getAllProctable() throws DataAccessException {

		List<Proctable> list = new ArrayList<Proctable>();
		
		Session localSession = this.createLocalSession();
		try {  

			Criteria criteria = localSession.createCriteria(Proctable.class);
			
            criteria.addOrder( Order.asc("orderNo") );
   
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return list;
	}

	private Criteria generateCriteriaBatch(Proctable exampleInstance, Session localSession) {
		Criteria criteria = localSession.createCriteria(Proctable.class);
		
		if(exampleInstance.getModuleCode() != null && exampleInstance.getModuleCode().length()>0) {
			criteria.add(Restrictions.eq("moduleCode",exampleInstance.getModuleCode()));
		}    
        
		if(exampleInstance.getProctableName() != null && exampleInstance.getProctableName().length()>0) {
			if(exampleInstance.getProctableName().substring(exampleInstance.getProctableName().length()-1, exampleInstance.getProctableName().length()).equals("%") ||
					exampleInstance.getProctableName().substring(0, 1).equals("%")){
				criteria.add(Restrictions.ilike("proctableName",exampleInstance.getProctableName()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("proctableName",exampleInstance.getProctableName()));
			}
		}    
	
        if (exampleInstance.getDescription()!=null  && exampleInstance.getDescription().length()>0) {
        	if(exampleInstance.getDescription().substring(exampleInstance.getDescription().length()-1, exampleInstance.getDescription().length()).equals("%") ||
					exampleInstance.getDescription().substring(0, 1).equals("%")){
        		criteria.add(Restrictions.ilike("description",exampleInstance.getDescription()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("description",exampleInstance.getDescription()));
			}
        }          

        if(exampleInstance.getActiveFlag()!=null && exampleInstance.getActiveFlag().length()>0){
        	criteria.add(Restrictions.eq("activeFlag",exampleInstance.getActiveFlag()));
		}
        
        return criteria;
	}
	
	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<Proctable> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<Proctable> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<Proctable> returnCust = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCust;
	}
	
	public Proctable find(Proctable exampleInstance) {
		List<Proctable> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ProctableDetail findProctableDetailById(Long proctableDetailId) throws DataAccessException {
		Session localSession = this.createLocalSession();
		Criteria criteria = localSession.createCriteria(ProctableDetail.class);
		if(proctableDetailId != null) {
			criteria.add(Restrictions.eq("proctableDetailId",proctableDetailId));
			return  (ProctableDetail)criteria.uniqueResult();
		}    
		
		closeLocalSession(localSession);
		localSession=null;
		return null;
	}
	
	@Transactional
	public void addProctableDetail(Proctable updateProctable, ProctableDetail aPoctableDetail) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			session.saveOrUpdate(updateProctable);
			session.flush();

			aPoctableDetail.setProctableId(updateProctable.getProctableId());
			session.saveOrUpdate(aPoctableDetail);
					
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
}
