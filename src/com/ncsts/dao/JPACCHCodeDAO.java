package com.ncsts.dao;

import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHCodePK;

/**
 * @author Paul Govindan
 *
 */

public class JPACCHCodeDAO extends JPAGenericDAO<CCHCode,CCHCodePK> implements CCHCodeDAO {
}


