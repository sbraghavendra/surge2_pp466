package com.ncsts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.FisYear;

public interface FisYearDAO  extends GenericDAO<FisYear,String>  {
		public List<FisYear> getSelectedPeriod(String ID) throws DataAccessException;
		public abstract Criteria createCriteria();
		public abstract Criteria create(DetachedCriteria c);
		public abstract Long count(Criteria criteria);
		public abstract List<FisYear> find(Criteria criteria);
		public abstract List<FisYear> find(DetachedCriteria criteria);
		public abstract FisYear find(FisYear exampleInstance);
		public abstract List<FisYear> find(FisYear exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
		public Long count(FisYear exampleInstance);
		public List<FisYear> getAllPeriodDesc(Long fisYear) throws DataAccessException;
		public List<FisYear> getAllYearPeriod(Long fisYearId) throws DataAccessException;
		public List<FisYear> getAllPeriod() throws DataAccessException;
		public boolean getCloseFlag(Long fisYearId);
		public void updateFisYear(Long fisYear, List<FisYear> fisYearList) throws DataAccessException;
}
