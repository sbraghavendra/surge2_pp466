package com.ncsts.dao;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.domain.LocationMatrix;

public class JPAGlExtractMapDAO extends JPAMatrixDAO<GlExtractMap> implements GlExtractMapDAO {
	@Transactional
	public GlExtractMap findByGlExtractMapId(Long glExtractMapId)
			throws DataAccessException {
		return getJpaTemplate().find(GlExtractMap.class, glExtractMapId);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<GlExtractMap> findByGLExtractMap(GlExtractMap glExtractMap, String... excludeProperty) throws DataAccessException {
		List<GlExtractMap> list = null;
		if(glExtractMap.getGlExtractMapId() == null){
			Session session = createLocalSession();
            try {
                Criteria criteria = session.createCriteria(GlExtractMap.class);
                Example example = Example.create(glExtractMap);
                for (String exclude : excludeProperty) {
                    example.excludeProperty(exclude);
                }
                criteria.add(example);
                list = criteria.list();
                
            } catch (HibernateException hbme) {
            	hbme.printStackTrace();
                logger.error("Error in finding the entities by example");
            }
            
            closeLocalSession(session);
            session=null;
		}else{
			list = new ArrayList<GlExtractMap>();
			list.add(findByGlExtractMapId(glExtractMap.getGlExtractMapId()));
		}
	   return list;
	}
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void remove(long glExtractMapId) throws DataAccessException {
		GlExtractMap gl = findByGlExtractMapId(glExtractMapId);
		if(gl!=null){
		getJpaTemplate().remove(gl);
		}else{
			logger.info("G/LExtarct Map object does not exist.");
		}
	}
	@Transactional
	public GlExtractMap saveGl(GlExtractMap glExtractMap)
			throws DataAccessException {
		List<String> find = listCodeCheck("JURISTYPE");
		if(find.contains(glExtractMap.getTaxJurisTypeCode())){
		getJpaTemplate().persist(glExtractMap);
		getJpaTemplate().flush();
		}else{
			logger.info("Tax Juris Type Code not in the tb_listCode");
		}
		return glExtractMap;
	}
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void update(GlExtractMap glExtractMap) throws DataAccessException {
		List<String> find = listCodeCheck("JURISTYPE");
		if(find.contains(glExtractMap.getTaxJurisTypeCode())){
			Session session = createLocalSession();
            Transaction t = session.beginTransaction();
            t.begin();
            session.update(glExtractMap);
            t.commit();
            closeLocalSession(session);
            session=null;
		}else{
			logger.info("Tax Juris Type Code not in the tb_listCode");
		}
	}
	
	// 02-05-2009 Method Added for exact search and also wild card search
	// BUG# 0003810
	private Criteria createCriteria(Criteria criteria, GlExtractMap glExtractMap){
		if(!"".equals(glExtractMap.getTaxCodeStateCode()) &&  glExtractMap.getTaxCodeStateCode()!=null){
				criteria.add(Restrictions.eq("taxCodeStateCode", glExtractMap.getTaxCodeStateCode()));
		}
		if(!"".equals(glExtractMap.getTaxcodeCountryCode()) &&  glExtractMap.getTaxcodeCountryCode()!=null){
			criteria.add(Restrictions.eq("taxcodeCountryCode", glExtractMap.getTaxcodeCountryCode()));
		}
		if(!"".equals(glExtractMap.getTaxCodeTypeCode()) &&  glExtractMap.getTaxCodeTypeCode()!=null){
				criteria.add(Restrictions.eq("taxCodeTypeCode", glExtractMap.getTaxCodeTypeCode()));
		}
		if(!"".equals(glExtractMap.getTaxCodeCode()) && glExtractMap.getTaxCodeCode()!=null){
			if(glExtractMap.getTaxCodeCode().endsWith("%")){
				criteria.add(Restrictions.ilike("taxCodeCode", glExtractMap.getTaxCodeCode(), MatchMode.START));
			}else {
				criteria.add(Restrictions.eq("taxCodeCode", glExtractMap.getTaxCodeCode()));
			}
		}
		if(!"".equals(glExtractMap.getTaxJurisTypeCode()) && glExtractMap.getTaxJurisTypeCode()!=null){
				criteria.add(Restrictions.eq("taxJurisTypeCode", glExtractMap.getTaxJurisTypeCode()));
		}
		return criteria;
	}
	
	// 02-05-2009 Method Modified for exact search and also wild card search
	// BUG# 0003810
	@SuppressWarnings("unchecked")
	public List<GlExtractMap> find(GlExtractMap exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults, String... excludeProperty) {
		logger.info("Entering the gatAllRecords");
		List<GlExtractMap> result = null;
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(GlExtractMap.class);
            
                      
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 10; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(exampleInstance, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		if(value.endsWith("%")){
    	        			criteria.add(Restrictions.ilike(name.toLowerCase(), value, MatchMode.START));
    	        		}else {
    	        			criteria.add(Restrictions.eq(name.toLowerCase(), value));
						}
    	        	}
    	        }
    	        criteria = createCriteria(criteria, exampleInstance);
            }
            catch (Exception ex) {
            }
            
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            result = criteria.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
        }
        finally{
        	closeLocalSession(session);
        	session=null;
        }
        
		return result;
	}

	public List<GlExtractMap> findByStateCode(String state) {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(GlExtractMap.class);
		criteria.add(Restrictions.eq("taxCodeStateCode", state));
		criteria.setMaxResults(1);
		
		List<GlExtractMap> returnGlExtractMap = criteria.list();
		closeLocalSession(session);
		session=null;
		
		return returnGlExtractMap;
	}
}
