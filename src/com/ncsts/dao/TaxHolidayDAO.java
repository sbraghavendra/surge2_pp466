package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;

public interface TaxHolidayDAO extends GenericDAO<TaxHoliday,String> {

	public abstract TaxHoliday findByTaxHolidayCode(String taxHolidayCode) throws DataAccessException;
	public abstract Criteria createCriteria();
	public abstract Criteria create(DetachedCriteria c);
	
	public abstract Long count(Criteria criteria);
	public abstract List<TaxHoliday> find(Criteria criteria);
	public abstract List<TaxHoliday> find(DetachedCriteria criteria);
	public abstract TaxHoliday find(TaxHoliday exampleInstance);
	public abstract List<TaxHoliday> find(TaxHoliday exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	
	public abstract List<TaxHoliday> findByTaxHoliday(TaxHoliday taxHoliday);
	public Long count(TaxHoliday exampleInstance);
	public abstract List<TaxHoliday> getAllTaxHolidayData() throws DataAccessException;   
	public abstract void update(TaxHoliday taxHoliday) throws DataAccessException;
	public abstract void remove(String taxHolidayCode) throws DataAccessException;

	public TaxHoliday getTaxHolidayData(String taxHolidayCode) throws DataAccessException;//Method added by Krishna

  	public abstract Map<String, PropertyDescriptor> getColumnProperties(Class<?> clazz);
  	
  	public List<TaxHolidayDetail> findTaxHolidayDetailFromTaxCode(String country, String state, String taxCode, String description, String county, String city, String stj) throws DataAccessException;
  	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String taxCode, String county, String city, String stj) throws DataAccessException;
  	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String county, String city) throws DataAccessException;
	public List<TaxHolidayDetail> findTaxHolidayDetailByTaxHolidayCode(String taxHolidayCode, String exceptInd, String taxCode) throws DataAccessException;
	public Long findTaxHolidayDetailCount(String taxHolidayCode) throws DataAccessException;
	public void executeTaxHolidayDetails(String taxHolidayCode) throws DataAccessException;
	public void addTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, boolean copyMapFlag) throws DataAccessException;
	public void updateTaxHolidayExceptionDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, TaxHolidayDetail selectedTaxCodeDetail) throws DataAccessException;
	public void updateTaxHolidayMapDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, String taxCode, List<TaxHolidayDetail> unselectedList) throws DataAccessException;
	public void updateTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;
	public void deleteTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;
	
}
