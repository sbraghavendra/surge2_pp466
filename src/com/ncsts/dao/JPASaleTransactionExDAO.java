package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ncsts.services.SaleTransactionLogService;
import com.seconddecimal.billing.domain.SaleTransactionLog;
import com.seconddecimal.billing.dto.SaleTransactionLogUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.BillTransactionLog;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.services.OptionService;
import com.seconddecimal.billing.domain.SaleTransaction;

public class JPASaleTransactionExDAO extends JpaDaoSupport implements SaleTransactionExDAO {
	private static final Log logger = LogFactory
			.getLog(SaleTransactionExDAO.class);

	@PersistenceContext
	protected EntityManager entityManager;

	private Class<BillTransaction> persistentClass;
	//private Session session;

	@Autowired
	private OptionService optionService;

	@Autowired
	private SaleTransactionLogService saleTransactionLogService;
	
	public Class<BillTransaction> getObjectClass() {
		return persistentClass;
	}

	public JPASaleTransactionExDAO() {
		this.persistentClass = BillTransaction.class;
	}

	@SuppressWarnings("unchecked")
	public List<BillTransaction> findByExample(
			BillTransaction exampleInstance, String sortField,
			boolean ascending, int offset, int count) {
		List<BillTransaction> list = null;
		Session session = createLocalSession();
		try {

			Criteria criteria = session.createCriteria(persistentClass);

			if (offset >= 0) {
				criteria.setFirstResult(offset);
			}

			if (count > 0) {
				criteria.setMaxResults(count);
			}

			Example example = Example.create(exampleInstance);
			example.ignoreCase();

			criteria.add(example);

			if (sortField != null)
				criteria.addOrder((ascending) ? Order.asc(sortField) : Order
						.desc(sortField));

			list = criteria.list();

			//session = null;
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally {
			closeLocalSession(session);
		}

		return list;
	}

	public Long count(BillTransaction exampleInstance) {
		Long returnLong = 0L;

		try {
			Session session = createLocalSession();
			Criteria criteria = session.createCriteria(persistentClass);

			Example example = Example.create(exampleInstance);
			example.ignoreCase();

			criteria.add(example);

			criteria.setProjection(Projections.rowCount());

			returnLong = ((Number) criteria.list().get(0)).longValue();

			closeLocalSession(session);
			session = null;
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}

		return returnLong;
	}
	
	@Transactional(readOnly = false)
	public void save(BillTransaction instance) {
		if (instance.getBilltransId() != null && instance.getBilltransId() > 0L) {
			entityManager.merge(instance);
		} else {
			boolean isPCO = false;
			Option pco = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN") );
			if (pco != null && "1".equals(pco.getValue()))
				isPCO = true;
			
			if (isPCO) {
				Transaction tx = null;
				Session session = null;
				try {
					Query query = null;
					session = createLocalSession();
					tx = session.beginTransaction();
				
					query = session.createSQLQuery("select SQ_TB_TRANSACTION_DETAIL_ID.nextval from dual");
					
					List<BigDecimal> result = query.list();
					
					Long pTransId = ((BigDecimal)result.get(0)).longValue();
					
					instance.setBilltransId(pTransId);
					entityManager.merge(instance);
					tx.commit();
					
					
				}catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					if (tx!=null) 
						tx.rollback();
				}
				finally {
					closeLocalSession(session);
				}
			} else {
				instance.setBilltransId(null);
				entityManager.persist(instance);
			}
		}
	}
	@Transactional(readOnly = false)
	public boolean deleteBatch(Long batchId) {
		boolean success = false;
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			EntityManager entityManager = getJpaTemplate()
					.getEntityManagerFactory().createEntityManager();
			javax.persistence.Query selectQuery = entityManager.createQuery("SELECT b.billtransId FROM BillTransaction b where b.processBatchNo = :batchId");
			Query deleteQuery = session.createQuery("DELETE FROM BillTransaction b where b.processBatchNo = :batchId");
			int deleteCount = 0;
			
			List<Long> billtransIds = selectQuery.setParameter("batchId",  batchId).getResultList();
			for(Long billtransId : billtransIds) {
				Query deleteFromLogQuery = session.createQuery("DELETE FROM BillTransactionLog bl where bl.billtransId = :billtransId");
				int deletedLogs = deleteFromLogQuery.setParameter("billtransId", billtransId).executeUpdate();
			}
			
			deleteCount = deleteQuery.setParameter("batchId", batchId).executeUpdate();
			System.out.println(deleteCount);
			logger.info("Deleted: " + batchId + "-" + deleteCount);
			
			String hqlDelete = "";
//			
//			int deleteCount = 0;
//			
//			hqlDelete = "delete from TB_BILLTRANS where PROCESS_BATCH_NO = " + batchId;
//			query = session.createSQLQuery(hqlDelete);
//			deleteCount = query.executeUpdate();
//			
//			logger.info(hqlDelete + ": " + deleteCount);
			
			hqlDelete = "delete from TB_BCP_BILLTRANS where PROCESS_BATCH_NO = " + batchId;
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			logger.info(hqlDelete + ": " + deleteCount);
			
			tx.commit();
			
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
		
		return success;
	}

	public void delete(BillTransaction instance) {
		if (instance == null) return;
		Session session = createLocalSession();
		try {
			session.delete(instance);
		} catch (HibernateException e) {
			logger.error("Error while deleting SaleTransaction id="+instance);
			e.printStackTrace();
		} finally {
			closeLocalSession(session);
		}
	}

	@Transactional
	public void cleanUpRelatedTransactions(BillTransaction trans) {

		String currentUser = "";
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			currentUser  = authentication.getName();



		if (trans.getRelatedSubtransId() == null || trans.getRelatedSubtransId() == 0L) return;

		Session localSession = createLocalSession();
//		try {
//
//			List<BillTransaction> removalCandidates = localSession.createCriteria(SaleTransaction.class)
//					.add(Restrictions.eq("relatedSubtransId", trans.getRelatedSubtransId()))
//					.add(Restrictions.ne("saletransId", trans.getBilltransId()))
//					.list();
//
//
//			BillTransaction afterLogSource = new BillTransaction();
//			for (BillTransaction candidate : removalCandidates) {
//				BillTransactionLog candidateLog = new BillTransactionLog();
//				SaleTransactionLogUtil.copyState(candidate, candidateLog, SaleTransactionLogUtil.BEFORE_STATE);
//				SaleTransactionLogUtil.copyState(afterLogSource, candidateLog, SaleTransactionLogUtil.AFTER_STATE);
//				candidateLog.setLogSource("Sale Transaction Process API");
//				candidateLog.setSaletransId(candidate.getSaletransId());
//
//				localSession.delete(candidate);
//				candidateLog.setUpdateTimestamp(new Date());
//				candidateLog.setUpdateUserId(currentUser);
//				saleTransactionLogService.save(candidateLog);
//			}
//			localSession.flush();
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error("Error while cleaning related transactions! " + e);
//		} finally {
//			closeLocalSession(localSession);
//		}
	}


	protected Session createLocalSession() {
		EntityManager entityManager = getJpaTemplate()
				.getEntityManagerFactory().createEntityManager();
		Session localsession = (entityManager instanceof HibernateEntityManager) ? ((HibernateEntityManager) entityManager)
				.getSession() : ((HibernateEntityManager) entityManager
				.getDelegate()).getSession();

		logger.info("===== createLocalSession() Hibernate Session Entity Count = "
				+ localsession.getStatistics().getEntityCount() + "====");
		return localsession;
	}

	protected void closeLocalSession(Session localSession) {
		if (localSession != null && localSession.isOpen()) {
			localSession.clear();
			localSession.close();
		}
	}
}
