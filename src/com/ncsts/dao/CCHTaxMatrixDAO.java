package com.ncsts.dao;

import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CCHTaxMatrixPK;

/**
 * @author Paul Govindan
 *
 */

public interface CCHTaxMatrixDAO extends GenericDAO<CCHTaxMatrix,CCHTaxMatrixPK> {
}
