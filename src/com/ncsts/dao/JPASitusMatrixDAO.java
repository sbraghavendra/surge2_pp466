package com.ncsts.dao;

import java.io.FileWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.sql.Types;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import oracle.jdbc.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.LogMemory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.domain.SitusMatrixAdmin;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.SitusMatrixDTO;

public class JPASitusMatrixDAO extends JPAGenericDAO<SitusMatrix, Long> implements SitusMatrixDAO {

	private Map<String,String> columnMapping = null;

/*	public Session getCurrentSession(){
		return createLocalSession();
	}*/
	
	@Transactional
	public Long count(SitusMatrix exampleInstance, int x) {
		Session localSession = this.createLocalSession();
		Long returnLong = new Long(0);
		try{
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		}
		catch(Exception e){	
			e.printStackTrace();
		}
		finally{
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}
	
	@Transactional
	public SitusMatrix find(SitusMatrix exampleInstance) {
		List<SitusMatrix> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@Transactional
	public List<SitusMatrix> findByExample(SitusMatrix exampleInstance) {
		List<SitusMatrix> list = new ArrayList<SitusMatrix>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateExampleCriteria(exampleInstance, localSession);
			criteria.setFirstResult(0);
			criteria.setMaxResults(100); //Just in case

            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		return new ArrayList<SitusMatrix>();
	}
	
	private String getDriverColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 30; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}

			columnMapping.put("transactiontypecode", "TRANSACTION_TYPE_CODE");
			columnMapping.put("ratetypecode", "RATETYPE_CODE");
			columnMapping.put("methoddeliverycode", "METHOD_DELIVERY_CODE");
			columnMapping.put("binaryweight", "BINARY_WEIGHT");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		
		// Build sorting expression
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if(orderByToken.length()>0){
					orderByToken.append(" , ");
				}
				else{
					orderByToken.append(" ORDER BY ");
				}
				
				if (field.getAscending()){
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getDriverColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }

		 return orderByToken.toString();
	}
	
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	private String getWhereClauseToken(SitusMatrix exampleInstance){
		
		StringBuffer whereClauseExt = new StringBuffer();

	    if (exampleInstance.getTransactionTypeCode()!=null && !exampleInstance.getTransactionTypeCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "TRANSACTION_TYPE_CODE='" + exampleInstance.getTransactionTypeCode() +"'");
	    }
	    
	    if (exampleInstance.getRatetypeCode()!=null && !exampleInstance.getRatetypeCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "RATETYPE_CODE='" + exampleInstance.getRatetypeCode() +"'");
	    }
	    
	    if (exampleInstance.getMethodDeliveryCode()!=null && !exampleInstance.getMethodDeliveryCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "METHOD_DELIVERY_CODE='" + exampleInstance.getMethodDeliveryCode() +"'");
	    }
	    
	    //0003320
	    if (exampleInstance.getSitusCountryCode()!=null && !exampleInstance.getSitusCountryCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "SITUS_COUNTRY_CODE='" + exampleInstance.getSitusCountryCode() +"'");
	    }
	    
	    if (exampleInstance.getSitusStateCode()!=null && !exampleInstance.getSitusStateCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "SITUS_STATE_CODE='" + exampleInstance.getSitusStateCode() +"'");
	    }
	    
	    if (exampleInstance.getJurLevel()!=null && !exampleInstance.getJurLevel().equals("")) {
	    	appendANDToWhere(whereClauseExt, "JUR_LEVEL='" + exampleInstance.getJurLevel() +"'");
	    }
		
	    return whereClauseExt.toString();
	}
	
	private String getMasterRulesWhereClauseToken(SitusMatrix exampleInstance){
		StringBuffer whereClauseExt = new StringBuffer();

	    if (exampleInstance.getTransactionTypeCode()!=null && !exampleInstance.getTransactionTypeCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "TRANSACTION_TYPE_CODE='" + exampleInstance.getTransactionTypeCode() +"'");
	    }
	    
	    if (exampleInstance.getRatetypeCode()!=null && !exampleInstance.getRatetypeCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "RATETYPE_CODE='" + exampleInstance.getRatetypeCode() +"'");
	    }
	    
	    if (exampleInstance.getMethodDeliveryCode()!=null && !exampleInstance.getMethodDeliveryCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "METHOD_DELIVERY_CODE='" + exampleInstance.getMethodDeliveryCode() +"'");
	    }

	    if (exampleInstance.getSitusCountryCode()!=null && !exampleInstance.getSitusCountryCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "SITUS_COUNTRY_CODE='" + exampleInstance.getSitusCountryCode() +"'");
	    }
	    
	    if (exampleInstance.getSitusStateCode()!=null && !exampleInstance.getSitusStateCode().equals("")) {
	    	appendANDToWhere(whereClauseExt, "SITUS_STATE_CODE='" + exampleInstance.getSitusStateCode() +"'");
	    }
	    
	    if (exampleInstance.getJurLevel()!=null && !exampleInstance.getJurLevel().equals("")) {
	    	appendANDToWhere(whereClauseExt, "JUR_LEVEL='" + exampleInstance.getJurLevel() +"'");
	    }
		
	    return whereClauseExt.toString();
	}
	
	protected int getCursor(java.sql.Connection con) throws SQLException {
		int cursor = 0;		
		if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")) {
			cursor =  OracleTypes.CURSOR;					
		} else if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("PostgreSQL")) {
			cursor = Types.OTHER;					
		}			
		return cursor;
	}
	
	@Transactional
	public List<SitusMatrix> getAllRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		List<SitusMatrix> result = null;
		Connection con = null;
		try {
			String whereClauseToken = getWhereClauseToken(exampleInstance);
			String orderByToken = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			if (con != null) {		
				System.out.println("\t Begin stored procedure call SP_TB_SITUS_MTRX_MASTER_LIST \n");				
				CallableStatement cstatement = con.prepareCall("{ ? = call SP_TB_SITUS_MTRX_MASTER_LIST(?,?,?,?)}");		
		
				cstatement.registerOutParameter(1, getCursor(con));	
				cstatement.setString(2, whereClauseToken);
				cstatement.setString(3, orderByToken);
				cstatement.setInt(4, firstRow);
				cstatement.setInt(5, maxResults);
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);					
				result = getSitusMatrixList(rs);
				System.out.println("\t *** Executing Store procedure, took " + result.size() );
				cstatement.close();					
				rs.close();
				cstatement.close();									
				con.commit();
				con.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		return result;
	}
	
	/*
	@Transactional
	public List<SitusMatrix> getAllDetailRecords(SitusMatrix exampleInstance, String display) {
		List<SitusMatrix> result = null;
		Connection con = null;
		try {
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(
					entityManager, true).getConnection();
			if (con != null) {	
				
				long begintime = System.currentTimeMillis();
				
				System.out.println("\t Begin stored procedure call sp_situs_matrix_list \n");				
				CallableStatement cstatement = con.prepareCall("{ ? = call sp_situs_matrix_list(?,?,?,?,?,?)}");		
		
				cstatement.registerOutParameter(1, getCursor(con));	
				cstatement.setString(2, exampleInstance.getTransactionTypeCode());
				cstatement.setString(3, exampleInstance.getRatetypeCode());
				cstatement.setString(4, exampleInstance.getMethodDeliveryCode());
				cstatement.setString(5, exampleInstance.getSitusCountryCode());
				cstatement.setString(6, exampleInstance.getSitusStateCode());
				cstatement.setString(7, exampleInstance.getJurLevel());
		
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);		
							
				result = getSitusMatrixDetailList(rs);
				
				String time = Long.toString(System.currentTimeMillis() - begintime);
				System.out.println("\t *** Executing sp_situs_matrix_list," + time + "," + result.size());
				
				cstatement.close();					
				rs.close();
				cstatement.close();									
				con.commit();
				con.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		return result;
	}
	*/
	
	@Transactional
	public List<SitusMatrix> getAllDetailRecords(SitusMatrix exampleInstance, String display) {
		long begintime = System.currentTimeMillis();
		
		String sqlQuery = 		
		"SELECT DISTINCT sm.situs_country_code       situs_country_code, " +
		"        lc.description              situs_country_name, " +
		"        sm.situs_state_code         situs_state_code, " +
		"        tcs.name                    situs_state_name, " +
		"        sm.jur_level                situs_jur_level " +
		"FROM tb_situs_matrix sm " +
		"LEFT JOIN tb_list_code lc " +
		"ON lc.code_type_code = 'COUNTRY' " +
		"AND lc.code_code = sm.situs_country_code " +
		"LEFT JOIN tb_taxcode_state tcs " +
		"ON tcs.taxcode_country_code = sm.situs_country_code " +
		"AND tcs.taxcode_state_code = sm.situs_state_code " +
		"WHERE sm.transaction_type_code = '"+exampleInstance.getTransactionTypeCode()+"' " +
		"AND sm.ratetype_code = '"+ exampleInstance.getRatetypeCode() + "' " +
		"AND sm.method_delivery_code = '" + exampleInstance.getMethodDeliveryCode() + "' ";
		
		if(exampleInstance.getSitusCountryCode()!=null && exampleInstance.getSitusCountryCode().length()>0){
			sqlQuery += "AND sm.situs_country_code = '"+ exampleInstance.getSitusCountryCode() + "' ";
		}
		
		if(exampleInstance.getSitusStateCode()!=null && exampleInstance.getSitusStateCode().length()>0){
			sqlQuery += "AND sm.situs_state_code = '"+ exampleInstance.getSitusStateCode() + "' ";
		}
		
		if(exampleInstance.getJurLevel()!=null && exampleInstance.getJurLevel().length()>0){
			sqlQuery += "AND sm.jur_level = '"+ exampleInstance.getJurLevel() + "' ";
		}
		
		
		sqlQuery += "ORDER BY situs_country_code, situs_state_code, situs_jur_level ";
		
		List<SitusMatrix> result = null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			result = new ArrayList<SitusMatrix>();
		
			try{
				long pseudoId = 1;
				while(iter.hasNext()){
					Object[] objarray= (Object[])iter.next();

					SitusMatrix situsMatrix = new SitusMatrix();
				
					situsMatrix.setId(new Long(pseudoId++));
					String countryCode = (String) objarray[0];//First column
					situsMatrix.setSitusCountryCode(countryCode);
					
					String stateCode = (String) objarray[2];
					situsMatrix.setSitusStateCode(stateCode);
					
					String jurLevel = (String) objarray[4];
					situsMatrix.setJurLevel(jurLevel);
					
		            result.add(situsMatrix);                
				}
			}catch(Exception e){
				
			}
		}
		
		String time = Long.toString(System.currentTimeMillis() - begintime);
		System.out.println("\t *** Executing Java, " + time + " miliseconds,  " + result.size() + " rows");

		return result;
	}

	@Transactional
	public Long count(SitusMatrix exampleInstance) {	
		Long count = 0l;
		Connection con = null;
		try {
			String whereClauseToken = getWhereClauseToken(exampleInstance);
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
					CallableStatement cstatement = con
							.prepareCall("{? = call SP_TB_SITUS_MTRX_MASTER_COUNT(?)}");
								
					cstatement.registerOutParameter(1, getCursor(con));	
					cstatement.setString(2, whereClauseToken);
					System.out.println("\t Begin stored procedure call SP_TB_SITUS_MTRX_MASTER_COUNT on " + con.getMetaData().getURL());
					cstatement.execute();
					ResultSet rs = (ResultSet) cstatement.getObject(1);
					while(rs.next()){
						count = rs.getLong(1);
						System.out.println("count " + count);
					}
					System.out.println("\t *** Executing Store procedure, took ");
					cstatement.close();
					con.commit();
					con.close();
				
			} 
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		return count;
	}
	
	@Transactional
	public List<SitusMatrix> getMasterRulesRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		
		List<SitusMatrix> result = null;
		Connection con = null;
		try {
			String whereClauseToken = getMasterRulesWhereClauseToken(exampleInstance);
			String orderByToken = getOrderByToken(orderBy);
			
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {		
				System.out.println("\t Begin stored procedure call SP_TB_SITUS_RULES_MASTER_LIST \n");				
				CallableStatement cstatement = con.prepareCall("{ ? = call SP_TB_SITUS_RULES_MASTER_LIST(?,?,?,?)}");		
		
				cstatement.registerOutParameter(1, getCursor(con));	
				cstatement.setString(2, whereClauseToken);
				cstatement.setString(3, orderByToken);
				cstatement.setInt(4, firstRow);
				cstatement.setInt(5, maxResults);
				cstatement.execute();
				ResultSet rs = (ResultSet) cstatement.getObject(1);					
				result = getSitusMatrixRulesList(rs);
				System.out.println("\t *** Executing Store procedure, took " + result.size() );
				cstatement.close();					
				rs.close();
				cstatement.close();									
				con.commit();
				con.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		
		return result;
	}
	
	private List<SitusMatrix> getSitusMatrixRulesList(ResultSet rs){
		List<SitusMatrix> result = new ArrayList<SitusMatrix>();
		String defaultFlag = null;
		String driverGlobalFlag = null;
		try{
			long pseudoId = 1;
			while(rs.next()){
				SitusMatrix situsMatrix = new SitusMatrix();
			
				situsMatrix.setId(new Long(pseudoId++));
				
	          //  defaultFlag =  rs.getString(1);;
	           //if (defaultFlag != null)situsMatrix.setDefaultFlag(defaultFlag);  
	            
	          //  driverGlobalFlag = rs.getString(2);
	            //if (driverGlobalFlag != null)situsMatrix.setDriverGlobalFlag(driverGlobalFlag);      
	            
	            
	            try{
	            	NumberFormat formatter = new DecimalFormat("00");
	            	Method theMethod = null;
	            	String name = "";
	            	String value = "";
	            	for (int i = 1; i <= 30; i++) {
	            		name = "setDriver" + formatter.format(i);
	            		value = rs.getString(i);
	            		theMethod = situsMatrix.getClass().getDeclaredMethod(name, new Class[] {value.getClass()});
	            		theMethod.setAccessible(true);
		        	
	            		theMethod.invoke(situsMatrix, new Object[]{value});
	            	}
	            }
	            catch (Exception ex) {
	            	logger.info("ex = " + ex.getMessage());
	            }
	          
	            
	            BigDecimal binaryWeight = rs.getBigDecimal(31);
	            if (binaryWeight!=null) situsMatrix.setBinaryWeight(binaryWeight.longValue());
	            
	            //BigDecimal defaultBinaryWeight = rs.getBigDecimal(34);
	            //if (defaultBinaryWeight!=null) situsMatrix.setDefaultBinaryWeight(defaultBinaryWeight.longValue());
	
	            result.add(situsMatrix);                
			}
		}catch(Exception e){
			
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SitusMatrix> getMasterRulesDetailRecords(SitusMatrix situsMatrix) {
		logger.debug("List<SitusMatrix> getMasterRulesDetailRecords called");
		
		List<SitusMatrix> result = null;
				
		Session session = createLocalSession();
        try {
            Criteria criteria = session.createCriteria(SitusMatrix.class);
            
      	    //Original filter criteria
      	    if (situsMatrix.getTransactionTypeCode()!=null && !situsMatrix.getTransactionTypeCode().equals("")) {
    	    	criteria.add(Restrictions.eq("transactionTypeCode", situsMatrix.getTransactionTypeCode()));
    	    }
      	  
      	    if (situsMatrix.getRatetypeCode()!=null && !situsMatrix.getRatetypeCode().equals("")) {
      	    	criteria.add(Restrictions.eq("ratetypeCode", situsMatrix.getRatetypeCode()));
      	    }
      	    
      	    if (situsMatrix.getMethodDeliveryCode()!=null && !situsMatrix.getMethodDeliveryCode().equals("")) {
    	    	criteria.add(Restrictions.eq("methodDeliveryCode", situsMatrix.getMethodDeliveryCode()));
    	    }
			
			if (situsMatrix.getSitusCountryCode()!=null && !situsMatrix.getSitusCountryCode().equals("")) {
    	    	criteria.add(Restrictions.eq("situsCountryCode", situsMatrix.getSitusCountryCode()));
    	    }
			
			if (situsMatrix.getSitusStateCode()!=null && !situsMatrix.getSitusStateCode().equals("")) {
    	    	criteria.add(Restrictions.eq("situsStateCode", situsMatrix.getSitusStateCode()));
    	    }
			
			if (situsMatrix.getJurLevel()!=null && !situsMatrix.getJurLevel().equals("")) {
    	    	criteria.add(Restrictions.eq("jurLevel", situsMatrix.getJurLevel()));
    	    }

      	    //From row selected
      	    if (situsMatrix.getBinaryWeight()!=null && !situsMatrix.getBinaryWeight().equals("")) {
    	    	criteria.add(Restrictions.eq("binaryWeight", situsMatrix.getBinaryWeight()));
    	    }
                  
            try {
    	    	NumberFormat formatter = new DecimalFormat("00");
    	        Method theMethod = null;
    	        String name = "";
    	        String value = "";
    	        for (int i = 1; i <= 30; i++) {
    	        	name = "Driver" + formatter.format(i);
    	        	theMethod = situsMatrix.getClass().getDeclaredMethod("get" + name, new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	value = (String)theMethod.invoke(situsMatrix, new Object[]{});
    	        	if(value!=null && value.length()>0){
    	        		criteria.add(Restrictions.eq(name.toLowerCase(), value));
    	        	}
    	        }
            }
            catch (Exception ex) {
            }
            
            result = criteria.list();
        } 
        catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding List<SitusMatrix> getMasterRulesDetailRecords");
        }
        
        closeLocalSession(session);
        session=null;
			
		return result;
	}
	
	@Transactional
	public Long masterRulesCount(SitusMatrix exampleInstance) {	
		
		Long count = 0l;
		Connection con = null;
		try {
			String whereClauseToken = getMasterRulesWhereClauseToken(exampleInstance);
			con = getJpaTemplate().getJpaDialect().getJdbcConnection(entityManager, true).getConnection();
			if (con != null) {
					CallableStatement cstatement = con.prepareCall("{? = call SP_TB_SITUS_RULES_MASTER_COUNT(?)}");
								
					cstatement.registerOutParameter(1, getCursor(con));	
					cstatement.setString(2, whereClauseToken);
					System.out.println("\t Begin stored procedure call SP_TB_SITUS_RULES_MASTER_COUNT on " + con.getMetaData().getURL());
					cstatement.execute();
					ResultSet rs = (ResultSet) cstatement.getObject(1);
					while(rs.next()){
						count = rs.getLong(1);
						System.out.println("count " + count);
					}
					System.out.println("\t *** Executing Store procedure, took ");
					cstatement.close();
					con.commit();
					con.close();
				
			} 
		} catch (SQLException se) {
			se.printStackTrace();
		}
		finally{
			if (con != null) {
				try { con.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
		return count;
	}

	@Override
	public List<SitusMatrix> getSitusMatrix(Date glDate, String transactionTypeCode, String ratetypeCode, String methodDeliveryCode, String situsCountryCode, String situsStateCode, String situsJurLevel, String driver01, String driver02, String driver03, String driver04, String driver05, String driver06, String driver07) throws DataAccessException {
		Session session = createLocalSession();
		List<SitusMatrix> result = new ArrayList<SitusMatrix>();
		try {
			/*SELECT tb_situs_matrix.situs_matrix_id,
            tb_situs_matrix.primary_situs_code,
            tb_situs_matrix.primary_taxtype_code,
            tb_situs_matrix.secondary_situs_code,
            tb_situs_matrix.secondary_taxtype_code,
            tb_situs_matrix.all_levels_flag,
            tb_situs_matrix.method_delivery_code
            FROM tb_situs_matrix
            WHERE trunc(tb_situs_matrix.effective_date) <= ?
            AND trunc(tb_situs_matrix.expiration_date) >= ?
            AND tb_situs_matrix.active_flag = '1'
            AND (tb_situs_matrix.transaction_type_code = ? OR tb_situs_matrix.transaction_type_code = '*ALL')
            AND (tb_situs_matrix.ratetype_code = ? OR tb_situs_matrix.ratetype_code = '*ALL')
            AND (tb_situs_matrix.method_delivery_code = ? OR method_delivery_code = '*ALL')
            AND tb_situs_matrix.situs_country_code = ?
            AND tb_situs_matrix.situs_state_code = ?
            AND tb_situs_matrix.jur_level = ?
            AND (tb_situs_matrix.driver_01 = '*ALL' OR tb_situs_matrix.driver_01 = ?)
            AND (tb_situs_matrix.driver_02 = '*ALL' OR tb_situs_matrix.driver_02 = ?)
            AND (tb_situs_matrix.driver_03 = '*ALL' OR tb_situs_matrix.driver_03 = ?)
            AND (tb_situs_matrix.driver_04 = '*ALL' OR tb_situs_matrix.driver_04 = ?)
            AND (tb_situs_matrix.driver_05 = '*ALL' OR tb_situs_matrix.driver_05 = ?)
            AND (tb_situs_matrix.driver_06 = '*ALL' OR tb_situs_matrix.driver_06 = ?)
            AND (tb_situs_matrix.driver_07 = '*ALL' OR tb_situs_matrix.driver_07 = ?)
            ORDER BY binary_weight DESC, custom_flag DESC, transaction_type_code DESC, ratetype_code DESC, method_delivery_code DESC*/

			String lookupQuery = "from SitusMatrix where " +
					"effectiveDate <= :effectiveDate AND " +
					"expirationDate >= :expirationDate AND " +
					"activeFlag = '1' AND " +
					"(transactionTypeCode = :transactionTypeCode or transactionTypeCode='*ALL') AND " +
					"(ratetypeCode = :rateTypeCode or ratetypeCode='*ALL' ) AND " +
					"(methodDeliveryCode = :methodDeliveryCode or methodDeliveryCode='*ALL') AND " +
					"situsCountryCode = :situsCountryCode AND " +
					"situsStateCode = :situsStateCode AND " +
					"jurLevel = :jurLevel AND " +
					"(driver01='*ALL' or driver01 = :driver01) AND "+
					"(driver02='*ALL' or driver02 = :driver02) AND "+
					"(driver03='*ALL' or driver03 = :driver03) AND "+
					"(driver04='*ALL' or driver04 = :driver04) AND "+
					"(driver05='*ALL' or driver05 = :driver05) AND "+
					"(driver06='*ALL' or driver06 = :driver06) AND "+
					"(driver07='*ALL' or driver07 = :driver07) " +
					" order by binaryWeight desc, customFlag desc, transactionTypeCode desc, ratetypeCode desc, methodDeliveryCode desc ";


			result = session.createQuery(lookupQuery)
					.setParameter("effectiveDate", glDate)
					.setParameter("expirationDate", glDate)
					.setParameter("transactionTypeCode", transactionTypeCode)
					.setParameter("rateTypeCode", ratetypeCode)
					.setParameter("methodDeliveryCode", methodDeliveryCode)
					.setParameter("situsCountryCode", situsCountryCode)
					.setParameter("situsStateCode", situsStateCode)
					.setParameter("jurLevel", situsJurLevel)
					.setParameter("driver01", driver01)
					.setParameter("driver02", driver02)
					.setParameter("driver03", driver03)
					.setParameter("driver04", driver04)
					.setParameter("driver05", driver05)
					.setParameter("driver06", driver06)
					.setParameter("driver07", driver07)
					.list();
		}
		catch (HibernateException hbe) {
			logger.error("Error while listing SitusMatrix "+hbe.getMessage());
			hbe.printStackTrace();
		}
		finally {
			closeLocalSession(session);
		}
		return result;
	}

	private List<SitusMatrix> getSitusMatrixList(ResultSet rs){
		List<SitusMatrix> result = new ArrayList<SitusMatrix>();
		String transactionTypeCod = null;
		String ratetypeCode = null;
		String methodDeliveryCode = null;
		try{
			long pseudoId = 1;
			while(rs.next()){
				SitusMatrix situsMatrix = new SitusMatrix();
			
				situsMatrix.setId(new Long(pseudoId++));
				
				transactionTypeCod =  rs.getString(1);;
	            if (transactionTypeCod != null) situsMatrix.setTransactionTypeCode(transactionTypeCod);  	          
	            
	            ratetypeCode = rs.getString(2);
	            if (ratetypeCode != null) situsMatrix.setRatetypeCode(ratetypeCode);      
	            
	            methodDeliveryCode =  rs.getString(3);;
	            if (methodDeliveryCode != null) situsMatrix.setMethodDeliveryCode(methodDeliveryCode);  

	            result.add(situsMatrix);                
			}
		}catch(Exception e){
		}
		return result;
	}
	
	private List<SitusMatrix> getSitusMatrixDetailList(ResultSet rs){
		List<SitusMatrix> result = new ArrayList<SitusMatrix>();
		
		String tempNumber = "";

		try{
			long pseudoId = 1;
			while(rs.next()){
				SitusMatrix situsMatrix = new SitusMatrix();
			
				situsMatrix.setId(new Long(pseudoId++));
				situsMatrix.setSitusCountryCode(rs.getString(1));
				situsMatrix.setSitusStateCode(rs.getString(3));
				situsMatrix.setJurLevel(rs.getString(5));
				
	            result.add(situsMatrix);                
			}
		}catch(Exception e){
			
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<SitusMatrix> find(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {
		List<SitusMatrix> list = new ArrayList<SitusMatrix>();
		try {
			Session localSession = this.createLocalSession();
			
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			logger.debug("maxResults: " + maxResults);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		}
		catch (HibernateException hbme) {
			hbme.printStackTrace();
		}
		return new ArrayList<SitusMatrix>();
	}
	
	private Criteria generateExampleCriteria(SitusMatrix exampleInstance, Session localSession) {
		Criteria criteria = localSession.createCriteria(SitusMatrix.class);
        
		if(exampleInstance.getTransactionTypeCode() != null && exampleInstance.getTransactionTypeCode().length()>0) {
		   criteria.add(Restrictions.eq("transactionTypeCode",exampleInstance.getTransactionTypeCode()));
		}    
		if(exampleInstance.getRatetypeCode() != null && exampleInstance.getRatetypeCode().length()>0) {
			criteria.add(Restrictions.eq("ratetypeCode",exampleInstance.getRatetypeCode()));
		} 
		if(exampleInstance.getMethodDeliveryCode() != null && exampleInstance.getMethodDeliveryCode().length()>0) {
			criteria.add(Restrictions.eq("methodDeliveryCode",exampleInstance.getMethodDeliveryCode()));
		} 
		
		if(exampleInstance.getSitusCountryCode() != null && exampleInstance.getSitusCountryCode().length()>0) {
			criteria.add(Restrictions.eq("situsCountryCode",exampleInstance.getSitusCountryCode()));
		}
		
		if(exampleInstance.getSitusStateCode() != null && exampleInstance.getSitusStateCode().length()>0) {
			criteria.add(Restrictions.eq("situsStateCode",exampleInstance.getSitusStateCode()));
		}
		
		if(exampleInstance.getJurLevel() != null && exampleInstance.getJurLevel().length()>0) {
			criteria.add(Restrictions.eq("jurLevel",exampleInstance.getJurLevel()));
		}
		
		if(exampleInstance.getCustomFlag() != null && exampleInstance.getCustomFlag().equals("1")) {
			criteria.add(Restrictions.eq("customFlag","1"));
		}
		else{
			criteria.add(Restrictions.or(Restrictions.eq("customFlag", "0"), Restrictions.isNull("customFlag")));
		}
		
		try {
	    	NumberFormat formatter = new DecimalFormat("00");
	        Method theMethod = null;
	        String name = "";
	        String value = null;
	        for (int i = 1; i <= 30; i++) {
	        	name = "Driver" + formatter.format(i);
	        	theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name, new Class[]{});
	        	theMethod.setAccessible(true);
	        	value = (String)theMethod.invoke(exampleInstance, new Object[]{});
	        	if(value!=null && value.length()>0){
	        		criteria.add(Restrictions.eq(name.toLowerCase(),value));
	        	}
	        }
        }
        catch (Exception ex) {
        }
		          
        return criteria;
	}
	
	private Criteria generateCriteriaBatch(SitusMatrix exampleInstance, Session localSession) {
		Criteria criteria = localSession.createCriteria(SitusMatrix.class);
        
		if(exampleInstance.getTransactionTypeCode() != null && exampleInstance.getTransactionTypeCode().length()>0) {
		   criteria.add(Restrictions.ilike("transactionTypeCode",exampleInstance.getTransactionTypeCode()+"%"));//Not case sensitive
		}    
		if(exampleInstance.getRatetypeCode() != null && exampleInstance.getRatetypeCode().length()>0) {
			criteria.add(Restrictions.ilike("ratetypeCode",exampleInstance.getRatetypeCode()+"%"));//Not case sensitive
		} 
		if(exampleInstance.getMethodDeliveryCode() != null && exampleInstance.getMethodDeliveryCode().length()>0) {
			criteria.add(Restrictions.ilike("methodDeliveryCode",exampleInstance.getMethodDeliveryCode()+"%"));//Not case sensitive
		} 
		
		if(exampleInstance.getCustomFlag() != null && exampleInstance.getCustomFlag().length()>0) {
			criteria.add(Restrictions.ilike("customFlag",exampleInstance.getCustomFlag()+"%"));//Not case sensitive
		} 
		          
        return criteria;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SitusMatrix> findAllSitusMatrixDetailBySitusMatrixId(Long situsMatrixId) throws DataAccessException {
		return getJpaTemplate().find(" select situsMatrix from SitusMatrix situsMatrix where situsMatrix.situsMatrixId = ? order by situsMatrix.situsMatrixId", situsMatrixId);	  
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void addSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			if(situsMatrix.getCustomBooleanFlag()){
				session.save(situsMatrix);
			}
			else{
				SitusMatrixAdmin situsMatrixAdmin = new SitusMatrixAdmin();
				BeanUtils.copyProperties(situsMatrix, situsMatrixAdmin);
				session.save(situsMatrixAdmin);
			}
		
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(situsMatrix);

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void inactiveSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException {
		String sql =  "UPDATE SitusMatrix SET activeFlag='0' WHERE  transactionTypeCode = :TransactionTypeCode AND ratetypeCode = :RatetypeCode AND methodDeliveryCode = :MethodDeliveryCode AND situsCountryCode = :SitusCountryCode AND situsStateCode = :SitusStateCode";
		
		Session session = createLocalSession();
		try {
			org.hibernate.Query q = session.createQuery(sql);
			q.setParameter("TransactionTypeCode", situsMatrix.getTransactionTypeCode());
			q.setParameter("RatetypeCode", situsMatrix.getRatetypeCode());
			q.setParameter("MethodDeliveryCode", situsMatrix.getMethodDeliveryCode());
			q.setParameter("SitusCountryCode", situsMatrix.getSitusCountryCode());
			q.setParameter("SitusStateCode", situsMatrix.getSitusStateCode());
			q.executeUpdate();
		} 
		catch (Exception e) {	
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteSitusMatrix(Long situsMatrixId) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			String hqlDelete = "delete TB_SITUS_MATRIX WHERE SITUS_MATRIX_ID = " + situsMatrixId.intValue();
			query = session.createSQLQuery(hqlDelete);
			query.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
}
