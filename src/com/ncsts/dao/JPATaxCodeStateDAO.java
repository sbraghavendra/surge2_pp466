package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.TaxCodeState;
import com.ncsts.domain.TaxCodeStatePK;

public class JPATaxCodeStateDAO extends JPAGenericDAO<TaxCodeState,TaxCodeStatePK> implements TaxCodeStateDAO {

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCodeState> getAllTaxCodeState() throws DataAccessException {
		return getJpaTemplate().find("select tcs from TaxCodeState tcs order by tcs.name");
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TaxCodeState> getActiveTaxCodeState() throws DataAccessException {
		return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.activeFlag = '1' order by tcs.name");
	}

	@SuppressWarnings("unchecked")
	public List<TaxCodeState> findByTaxCode(String taxCodeType, String taxcodeCode) throws DataAccessException {
		//return getJpaTemplate().find("select ts from TaxCodeState ts where ts.taxCodeStatePK.taxcodeStateCode in (select td.taxcodeStateCode from TaxCodeDetail td" 
		//						+" where td.taxcodeTypeCode = ?1 and taxcodeCode = ?2) order by ts.name" , taxCodeType,taxcodeCode);
	//	return getJpaTemplate().find("select ts from TaxCodeState ts inner join TaxCodeDetail td on ts.taxCodeStatePK.taxcodeStateCode = td.taxcodeStateCode and ts.taxCodeStatePK.country = td.taxcodeCountryCode " 
		//		+" where td.taxcodeTypeCode = ?1 and td.taxcodeCode = ?2 order by ts.name" , taxCodeType,taxcodeCode);
		
		StringBuffer query = new StringBuffer();
	
//		query.append("select tc.* from tb_taxcode tc inner join  tb_taxcode_detail td on ");
//		query.append("((tc.taxcode_code = td.taxcode_code) and (tc.taxcode_type_code=td.taxcode_type_code))");
//		query.append(" where ");

		query.append("SELECT ts.* FROM  TB_TAXCODE_STATE ts inner join TB_TAXCODE_DETAIL td ON ");
		query.append("ts.taxcode_State_Code =td.taxcode_State_Code  AND ts.country =td.taxcode_country_Code ");
		query.append("WHERE td.taxcode_Type_Code = :taxcodeTypeCode AND TD.taxcode_Code = :taxcodeCode ");  
		query.append("ORDER BY ts.name ");
		
		Session session = createLocalSession();
		org.hibernate.SQLQuery q = session.createSQLQuery(query.toString());
		q.addEntity(TaxCodeState.class);

		q.setParameter("taxcodeTypeCode", taxCodeType);
		q.setParameter("taxcodeCode", taxcodeCode);

		List<TaxCodeState> returnTaxCodeState = q.list();
		
		closeLocalSession(session);
		session=null;
		
		return returnTaxCodeState;

	}

	@SuppressWarnings("unchecked")
	public List<TaxCodeState> findByStateName(String stateName) {
		//return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.name = ?1",stateName);
		List<TaxCodeState> list = new ArrayList<TaxCodeState>();
		// TODO Auto-generated method stub
		//return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.taxCodeState = ?1",stateCode);
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(TaxCodeState.class);
		if(stateName!=null){
			criteria.add(Restrictions.ilike("name", stateName, MatchMode.EXACT));
		}
		list = criteria.list();
		
		closeLocalSession(session);
		session=null;
			
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<TaxCodeState> findByStateCode(String stateCode) {
		List<TaxCodeState> list = new ArrayList<TaxCodeState>();
		// TODO Auto-generated method stub
		//return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.taxCodeState = ?1",stateCode);
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(TaxCodeState.class);
		if(stateCode!=null){
			criteria.add(Restrictions.ilike("taxCodeState", stateCode, MatchMode.EXACT));
		}
		list = criteria.list();
		
		closeLocalSession(session);
		session=null;
			
		return list;
	}
	
	@Transactional
	public TaxCodeState add(TaxCodeState instance) throws DataAccessException{
		if (instance.getTaxCodeStatePK().getCountry() == null) {
			//TODO Handle this case properly later to catch the exception and notify service layer
           logger.error(" Can not inser Null Values in PK colums");
           logger.info(" User Defined Error::prblem in JPATaxCodeStateDAO.add () ::Can not inser Null Values in PK colums" );
		}
		
		if (instance.getTaxCodeStatePK().getTaxcodeStateCode() == null) {
			//TODO Handle this case properly later to catch the exception and notify service layer
           logger.error(" Can not inser Null Values in PK colums");
           logger.info(" User Defined Error::prblem in JPATaxCodeStateDAO.add () ::Can not inser Null Values in PK colums" );
		}

		getJpaTemplate().persist(instance);	
		getJpaTemplate().flush();
		return instance;		
	}
	
	//Midtier project code modified - january 2009
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(TaxCodeState instance) throws DataAccessException {
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			session.merge(instance);
			session.flush();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void delete(TaxCodeStatePK pk) throws DataAccessException{
		TaxCodeState lc = findByPK(pk);
		if (lc != null) {
			// Update query - needs to be transactional!
			Session session = createLocalSession();
	    	Transaction t = session.beginTransaction();

	    	org.hibernate.Query query = session.createSQLQuery("delete from TB_TAXCODE_STATE where TAXCODE_COUNTRY_CODE = :country and TAXCODE_STATE_CODE = :taxcodeStateCode");
			query.setParameter("country", pk.getCountry());
			query.setParameter("taxcodeStateCode", pk.getTaxcodeStateCode());
			int rows = query.executeUpdate();
			logger.debug(rows + " state rows deleted");

			t.commit();
			
			closeLocalSession(session);
			session=null;
		} else {
			logger.warn("ListCodes Object not found for deletion");
		}	
	}
	
	@Transactional
	public void batchQuickUpdate(List<TaxCodeState> quickUpdateList) throws DataAccessException {
		
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			for(TaxCodeState taxCode : quickUpdateList) {	
				TaxCodeStatePK pk = new TaxCodeStatePK();
				pk.setCountry(taxCode.getCountry());
				pk.setTaxcodeStateCode(taxCode.getTaxCodeState());
				
				TaxCodeState code = getJpaTemplate().find(TaxCodeState.class, pk);
				if(code!=null){
					code.setActiveFlag(taxCode.getActiveFlag());
					session.merge(code);
					session.flush();
				}
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
	}
	
	@Transactional
	public TaxCodeState findByPK(TaxCodeStatePK pk) throws DataAccessException {
		return getJpaTemplate().find(TaxCodeState.class, pk);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<TaxCodeState> findByExampleSorted(String country, String state,
			String activeFlag) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuffer qb = new StringBuffer("select distinct tcs from TaxCodeState tcs, Jurisdiction j where tcs.taxCodeStatePK.country = j.country ")
				.append("AND tcs.taxCodeStatePK.taxcodeStateCode = j.state ");
		int pcount = 0;
		if(country != null) {
			qb.append("AND tcs.taxCodeStatePK.country = ?").append(++pcount).append(" ");
			params.add(country);
		}
		if(state != null) {
			qb.append("AND tcs.taxCodeStatePK.taxcodeStateCode = ?").append(++pcount).append(" ");
			params.add(state);
		}
		if(activeFlag != null) {
			qb.append("AND tcs.activeFlag = ?").append(++pcount).append(" ");
			params.add(activeFlag);
			
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date now = cal.getTime();
		
		qb.append("AND j.effectiveDate <= ?").append(++pcount).append(" AND j.expirationDate >= ?").append(++pcount).append(" ");
		params.add(now);
		params.add(now);
		
		qb.append("order by tcs.taxCodeStatePK.country, tcs.name");
		
		return getJpaTemplate().find(qb.toString(), params.toArray(new Object[]{}));
	}

	@Override
	public boolean isCountryUsed(String countryName) {
		// TODO Auto-generated method stub
		Session session = createLocalSession();
    	org.hibernate.Query query;
    	boolean isUsed = true;
    	
    	try {
    		query = session.createSQLQuery("SELECT COUNT(*) FROM TB_TAXCODE_STATE WHERE (TAXCODE_STATE_CODE <> '*COUNTRY') AND TAXCODE_COUNTRY_CODE = :taxcodeCountry");
    		query.setParameter("taxcodeCountry", countryName.toUpperCase());
    		List<?> result = query.list();
    		long count = ((BigDecimal) result.get(0)).longValue();
    		
    		if(count == 0) {
    			isUsed = false;	
    		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		
		closeLocalSession(session);
		session=null;
		
		return isUsed;
	}

	@Override
	public void deleteCountry(TaxCodeState instance) {
		// TODO Auto-generated method stub
		Session session = createLocalSession();
    	Transaction t = session.beginTransaction();

    	org.hibernate.Query query = session.createSQLQuery("delete from TB_TAXCODE_STATE where TAXCODE_COUNTRY_CODE = :country and TAXCODE_STATE_CODE = :taxcodeStateCode");
		query.setParameter("country", instance.getCountry());
		query.setParameter("taxcodeStateCode", "*COUNTRY");
		int rows = query.executeUpdate();
		logger.debug(rows + " state rows deleted");

		t.commit();
		
		closeLocalSession(session);
		session=null;
	}

	@Override
	public void updateCountry(TaxCodeState instance) {
		// TODO Auto-generated method stub
		List<TaxCodeState> list = findByCountryName(instance.getCountry());	
		if(list==null || list.size()==0){
			return;
		}
		
		TaxCodeState aCountry = (TaxCodeState)list.get(0);	
		aCountry.setName(instance.getName());
		aCountry.setActiveFlag(instance.getActiveFlag());	
		aCountry.setUpdateUserId(instance.getUpdateUserId());
		aCountry.setUpdateTimestamp(instance.getUpdateTimestamp());
		
		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			session.merge(aCountry);
			session.flush();

			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				logger.debug("Rolling back");
				tx.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
	}

	@Override
	public List<TaxCodeState> findByCountryName(String countryName) {
		// TODO Auto-generated method stub
		if(countryName==null || countryName.trim().length()==0){
			return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.taxCodeStatePK.taxcodeStateCode = '*COUNTRY' order by tcs.name");
		}
		else{
			return getJpaTemplate().find("select tcs from TaxCodeState tcs where tcs.taxCodeStatePK.taxcodeStateCode = '*COUNTRY' and UPPER(tcs.taxCodeStatePK.country) like '" + countryName.trim().toUpperCase() + "%' order by tcs.name");
		}
	}	

}
