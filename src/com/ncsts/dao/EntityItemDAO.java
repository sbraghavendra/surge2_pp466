package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.EntityItem;

/**
 * @author Paul Govindan
 *
 */

public interface EntityItemDAO extends GenericDAO<EntityItem,Long> {
	
    public abstract EntityItem findById(Long entityId) throws DataAccessException;
    
    public abstract Long count(Long entityId) throws DataAccessException;
    
    public EntityItem findByCode(String entityCode) throws DataAccessException;
    public EntityItem findByName(String entityName) throws DataAccessException;
    
    public EntityItem findByCode(String entityCode, Long parentId) throws DataAccessException;
    public EntityItem findByName(String entityName, Long parentId) throws DataAccessException;
    
    public abstract List<EntityItem> findAllEntityItems() throws DataAccessException;
    public abstract List<EntityItem> findAllEntityItemsBylockflag() throws DataAccessException;
    public List<EntityItem> findAllEntityItemsWithFilter(Long level, String entityCode, String entityName) throws DataAccessException;
    
    public List<EntityItem> findAllEntityItemsByLevel(Long level) throws DataAccessException;
    
    public List<EntityItem> findAllEntityItemsByParent(Long parentId) throws DataAccessException;
    public List<EntityItem> findAllEntityItemsByLevelAndParent(Long level, Long parentId) throws DataAccessException;
    
    public Long persist(EntityItem entityItem)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate (EntityItem entityItem) throws DataAccessException;
    
    public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException;
    public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException;
    public void deleteEntity(Long entityId) throws DataAccessException;
    public void setSubEntityInactive(Long entityId) throws DataAccessException;
    
    public List<EntityItem> findAllEntityItemsWithCustomer(Long custid) throws DataAccessException;      
    
    public abstract List<EntityItem> getAllRecords(EntityItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause) throws DataAccessException;
	public Long count(EntityItem exampleInstance, String whereClause);
    public List<Object[]> findByJurisdictionId(Long jurisdictionId) throws DataAccessException;
}

