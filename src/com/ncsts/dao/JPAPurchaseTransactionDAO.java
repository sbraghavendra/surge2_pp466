package com.ncsts.dao;

import com.ncsts.common.*;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.Matrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.exception.AuditableIdNotFoundException;
import com.ncsts.exception.InvalidSuspensionTypeException;
import com.ncsts.exception.PurchaseTransactionProcessSuspendedException;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;

import org.hibernate.*;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import org.springframework.orm.jpa.support.JpaDaoSupport;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE;

/**
 * Created by RC05200 on 7/14/2016.
 */
public class JPAPurchaseTransactionDAO extends JPAGenericDAO<PurchaseTransaction,Long> implements PurchaseTransactionDAO {
    @Override
    public LocationMatrix getLocationViaDrivers(String sql, Long entityId, Date effectiveDate, Date expirationDate) {
        if (entityId == null || effectiveDate == null || expirationDate==null)
            return null;

        LocationMatrix result = null;
        Session session = null;
        try {
            session = createLocalSession();

            SQLQuery locationMatrixQuery = session.createSQLQuery(sql);
            locationMatrixQuery.setLong(0, entityId);
            locationMatrixQuery.setDate(1, effectiveDate);
            locationMatrixQuery.setDate(2, expirationDate);
            locationMatrixQuery.addEntity(LocationMatrix.class);

            List<LocationMatrix> matrices =  locationMatrixQuery.list();

            if (matrices!=null && matrices.size()>0)
                return matrices.get(0);

        } catch (HibernateException sqle) {
            logger.error(sqle.getMessage());
            sqle.printStackTrace();
        } finally {
            closeLocalSession(session);
        }

        return result;
    }

    @Override
    public TaxMatrix getTaxMatrixViaDrivers(String sql, Long entityId, Date effectiveDate, Date expirationDate) {
        if (entityId == null || effectiveDate == null || expirationDate==null)
            return null;
        TaxMatrix result = null;
        Session session = null;
        Transaction tx =  null;
        try {
            session = createLocalSession();

            SQLQuery taxMatrixQuery = session.createSQLQuery(sql);
            taxMatrixQuery.setLong(0, entityId);
            taxMatrixQuery.setDate(1, effectiveDate);
            taxMatrixQuery.setDate(2, expirationDate);
            taxMatrixQuery.addEntity(TaxMatrix.class);

            List<TaxMatrix> matrices =  taxMatrixQuery.list();



            if (matrices!=null && matrices.size()>0)
                return matrices.get(0);

        } catch (HibernateException hbe) {
            //if (tx!=null) tx.rollback();
            logger.error(hbe.getMessage() +":"+hbe.getCause().getMessage());
            hbe.printStackTrace();
        } finally {
            closeLocalSession(session);
        }

        return result;
    }

    @Override
    public void save(PurchaseTransaction transaction) {
        Session session = createLocalSession();

        Transaction tx = null;
        try {

            tx = session.beginTransaction();

            if (transaction.getPurchtransId()==null) {
                session.persist(transaction);
            } else {

              Long count =  (Long)session.createCriteria(PurchaseTransaction.class)
                        .add(Restrictions.eq("purchtransId", transaction.getPurchtransId()))
                        .setProjection(Projections.rowCount()).uniqueResult();

               // Query query = session.createQuery("select count(*) as count from PurchaseTransaction where  purchtransId in (9991960, 9011960)");
                        //.setLong("purchtransid", transaction.getPurchtransId());


                //List list = query.list();

               // Long count = ((Number)list.get(0)).longValue();;

                if (count!=null && count>0L) {
                    session.merge(transaction);
                } else {
                    session.save(transaction);
                }
            }
            tx.commit();

        } catch (Exception hbe) {
            logger.error("Error while persisting PurchaseTransaction!" + hbe.getMessage());
            hbe.printStackTrace();

            transaction.setErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
            transaction.setFatalErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
            transaction.setErrorText("Failed to write PurchaseTransaction! \n"+hbe.getMessage());

            if (tx!=null)
                tx.rollback();



        } finally {
            closeLocalSession(session);
        }
    }
    
    public PurchaseTransaction findById(Long purchtransId) {
        return getJpaTemplate().find(PurchaseTransaction.class, purchtransId);
    }

    protected Session createLocalSession() {
        EntityManager entityManager = getJpaTemplate()
                .getEntityManagerFactory().createEntityManager();
        Session localsession = (entityManager instanceof HibernateEntityManager) ? ((HibernateEntityManager) entityManager)
                .getSession() : ((HibernateEntityManager) entityManager
                .getDelegate()).getSession();

        logger.info("===== createLocalSession() Hibernate Session Entity Count = "
                + localsession.getStatistics().getEntityCount() + "====");
        return localsession;
    }

    protected void closeLocalSession(Session localSession) {
        if (localSession != null && localSession.isOpen()) {
            localSession.clear();
            localSession.close();
        }
    }
    
    public Long findLastPurchaseTransactionId(){
    	Session localsession = null;
    	Long pTransId = null;
	    try {
			Query query = null;
			localsession = createLocalSession();
			query = localsession.createSQLQuery("SELECT last_number FROM user_sequences WHERE sequence_name = 'SQ_TB_PROCESS_COUNT'");			
			List<BigDecimal> result = query.list();	
			pTransId = ((BigDecimal)result.get(0)).longValue();
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		finally {
			closeLocalSession(localsession);
		}
	    
	    return pTransId;
    }
    private String extractFieldName(String getter) {
    
    	String fieldName = getter.substring(3);
    	fieldName = fieldName.substring(0,1).toLowerCase() + fieldName.substring(1);
    	
    	return fieldName;
    }
    
    private String getDriverColumnName(String name){
		Map<String, String> columnMapping = null;
    	if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 10; i++) {
				columnMapping.put("driver"+formatter.format(i), "DRIVER_" + formatter.format(i));
			}
			
			columnMapping.put("defaultflag", "DEFAULT_FLAG");
			columnMapping.put("binarywt", "BINARY_WEIGHT");
			columnMapping.put("defltbinarywt", "DEFAULT_BINARY_WEIGHT");
			columnMapping.put("entityid", "ENTITY_ID");
		}
		
		String columnName = columnMapping.get(name);
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
		
    }
    
    private ProcessSuspendedTransactionDocument findMatchingTransactionsJurisdictionTaxrate(ProcessSuspendedTransactionDocument document, Long id, Auditable dummyInstance, String suspendIndicator) throws Exception {
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	JurisdictionTaxrate exampleInstance = (JurisdictionTaxrate) getJpaTemplate().find(dummyInstance.getClass(), id);
    	
    	String sql = "select tb_purchtrans FROM PurchaseTransaction tb_purchtrans"
    			+ " where tb_purchtrans.transactionInd = 'S' and tb_purchtrans.suspendInd = ?1"
    			+ " and (tb_purchtrans.glDate BETWEEN to_date(?2, 'yyyy-mm-dd') AND to_date(?3, 'yyyy-mm-dd'))"
    			+ " and tb_purchtrans.ratetypeCode = ?4"
    			+ " and tb_purchtrans.shiptoJurisdictionId = ?5";
    	
    	TypedQuery<PurchaseTransaction> query = entityManager.createQuery(sql, PurchaseTransaction.class);
    	query.setParameter(1, suspendIndicator);
    	query.setParameter(2, exampleInstance.getEffectiveDate().toString().substring(0,10));
    	query.setParameter(3, exampleInstance.getExpirationDate().toString().substring(0,10));
    	query.setParameter(4, exampleInstance.getRatetypeCode());
    	query.setParameter(5, exampleInstance.getJurisdiction().getId());
    	
		document.setLocationTransSql(sql);

    	List<PurchaseTransaction> transactions = query.getResultList();
		
		document.setPurchaseTransaction(transactions);
		document.setProcessedTransactions(transactions.size());
		
		if(transactions.isEmpty())
			throw new Exception(sql);
    	
    	return document;
    }
    
    private List<Long> findJurisdictionIds(String city, String country, String county, String state){
    	String sql = "select j.jurisdictionId from Jurisdiction j where j.country = ?1 and j.state = ?2";
    	if(!county.equals("*ALL")) sql+=" and j.county = '" + county + "'";
    	if(!city.equals("*ALL")) sql+=" and j.city = '" + city + "'";
    	List<Long> jurisdictionIds =  getJpaTemplate().find(sql, country, state);
    	
    	return jurisdictionIds;
    }
    
    private ProcessSuspendedTransactionDocument findMatchingTransactionsTaxCodeDetail(ProcessSuspendedTransactionDocument document, Long id, Auditable dummyInstance, String suspendIndicator) throws Exception{
    	
    	
    	TaxCodeDetail exampleInstance = (TaxCodeDetail) getJpaTemplate().find(dummyInstance.getClass(), id);
    	
    	String country = exampleInstance.getTaxcodeCountryCode();
    	String state = exampleInstance.getTaxcodeStateCode();
    	String county = exampleInstance.getTaxCodeCounty(); 
    	String city = exampleInstance.getTaxCodeCity();
    	
    	//List<Long> jurisdictionIds =  findJurisdictionIds(country, state, county, city);
    	
//    	Session session = createLocalSession();
//    	TypedQuery<Jurisdiction> query = EntityManager.createQuery("SELECT j FROM Jurisdiction j WHERE j.country = " )
    	
    	//Jurisdiction exampleJurisdiction = (Jurisdiction) getJpaTemplate().executeFind(Jurisdiction.class, )
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	String sql = "SELECT tb_purchtrans FROM PurchaseTransaction tb_purchtrans "
    			+ "WHERE tb_purchtrans.taxcodeCode = ?1 AND (tb_purchtrans.transactionInd = 'S' AND tb_purchtrans.suspendInd = ?2) "
    			+ " AND (tb_purchtrans.glDate BETWEEN to_date(?3, 'yyyy-mm-dd') AND to_date(?4, 'yyyy-mm-dd'))"
    			+ " AND EXISTS( SELECT 1"
    			+ " FROM Jurisdiction tb_jurisdiction"
    			+ " WHERE tb_jurisdiction.jurisdictionId = tb_purchtrans.shiptoJurisdictionId"
    			+ " AND tb_jurisdiction.country = ?5";
    	
    	if(!state.equals("*ALL")) sql+=" and tb_jurisdiction.state = '" + state + "'";
    	if(!county.equals("*ALL")) sql+=" and tb_jurisdiction.county = '" + county + "'";
    	if(!city.equals("*ALL")) sql+=" and tb_jurisdiction.city = '" + city + "'";
    	sql+=")";
    	
    	/*List<List<Long>> batchJurisdictionIds = new ArrayList<List<Long>>();
    	//tb_purchtrans.shiptoJurisdictionId IN :jurisdictions 
    	final AtomicInteger counter = new AtomicInteger();
    	final int chunkSize = 1000;
    	for(Long jurisdictionId : jurisdictionIds) {
    		if (counter.getAndIncrement() % chunkSize == 0) {
    			batchJurisdictionIds.add(new ArrayList<Long>());
    		}
    		batchJurisdictionIds.get(batchJurisdictionIds.size() - 1).add(jurisdictionId);
    	}
    	
    	for(int i = 0; i < batchJurisdictionIds.size(); i++) {
    		if(i>0) sql+= " or ";
    		sql+= "tb_purchtrans.shiptoJurisdictionId IN (:jurisdictions" + i + ")";
    	}
    	sql+=")"; */
    	TypedQuery<PurchaseTransaction> query = entityManager.createQuery(sql, PurchaseTransaction.class);
    	query.setParameter(1, exampleInstance.getTaxcodeCode());
    	query.setParameter(2, suspendIndicator);
    	query.setParameter(3, exampleInstance.getEffectiveDate().toString().substring(0,10));
    	query.setParameter(4, exampleInstance.getExpirationDate().toString().substring(0,10));
    	query.setParameter(5, country);
    	/*
    	for(int i = 0; i < batchJurisdictionIds.size(); i++) {
    		query.setParameter("jurisdictions" + i, batchJurisdictionIds.get(i));
    	}
    	*/
    	
		String sqlQuery = query.unwrap(org.hibernate.Query.class).getQueryString();
		document.setLocationTransSql(sqlQuery);
    	
    	List<PurchaseTransaction> transactions = query.getResultList();
		
		document.setPurchaseTransaction(transactions);
		document.setProcessedTransactions(transactions.size());
		
		if(transactions.isEmpty())
			throw new Exception(sqlQuery);
		
    	return document;
    }
    
    private Map<String, String> findMatrixValuesForJPA(Long id, Auditable dummyInstance, Long driverCount, Long entityId) throws Exception {
    	Matrix exampleInstance = (Matrix) getJpaTemplate().find(dummyInstance.getClass(), id);
    	Map<String, String> propertiesMap = new HashMap<String, String>();
    	String name = "";
    	NumberFormat formatter = new DecimalFormat("00");
    	Method theMethod = null;
    	
    	for(int i = 1 ; i <= driverCount; i++ ) {
    		name = "Driver" + formatter.format(i);
    		theMethod = exampleInstance.getClass().getDeclaredMethod("get" + name,  new Class[] {});
    		theMethod.setAccessible(true);
    		propertiesMap.put(name, (String)theMethod.invoke(exampleInstance, new Object[] {}));
    	}
    	
    	propertiesMap.put("effectiveDate", exampleInstance.getEffectiveDate().toString().substring(0,10));
    	propertiesMap.put("expirationDate", exampleInstance.getExpirationDate().toString().substring(0,10));
    	propertiesMap.put("entityId", entityId.toString());
    	return propertiesMap;
    }
    
    private <T extends Auditable> Auditable findSpecificAuditable(Long id, Class<T> auditableType) throws AuditableIdNotFoundException {
    	
    	Auditable exampleInstance = getJpaTemplate().find(auditableType, id);
    	if(exampleInstance == null)  throw new AuditableIdNotFoundException("Auditable Id " + id + " not found.");
    	return exampleInstance;
    }
    
    public ListCodes retrieveErrorText(String error) {
    	//ListCodesPK searchByCode = new ListCodesPK(errorType, errorCode);
    	EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
    	
    	String jpql = "SELECT lc FROM ListCodes lc WHERE lc.listCodesPK.codeCode = '" + error + "'" ;
		TypedQuery<ListCodes> tq = entityManager.createQuery(jpql, ListCodes.class);
		try {
			ListCodes errorCode = tq.getSingleResult();
			return errorCode;
		}
		catch(Exception e){
			return null;
		}
    }
    
    private ProcessSuspendedTransactionDocument findMatchingTransactionsMatricies(ProcessSuspendedTransactionDocument document, Map<String, String> driversToSearch, String suspendIndicator, String columnHeader) throws Exception{
    	
    	List<PurchaseTransaction> transactions;
		String effectiveDate = driversToSearch.remove("effectiveDate");
		String expirationDate = driversToSearch.remove("expirationDate");
		String tableName = "TB_PURCHTRANS";
		Long entityId = Long.valueOf(driversToSearch.remove("entityId"));
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		
		String jpql = "SELECT tb_purchtrans FROM PurchaseTransaction tb_purchtrans WHERE (tb_purchtrans.transactionInd = 'S' AND tb_purchtrans.suspendInd = ?1) AND tb_purchtrans.glDate "
		+ "BETWEEN to_date(?2, 'yyyy-mm-dd') AND to_date(?3, 'yyyy-mm-dd') "
		+ "AND tb_purchtrans.entityId = ?4";
		
		for(Map.Entry<String, String> entry : driversToSearch.entrySet()) {
			String columnName = tableName + "." + columnHeader + entry.getKey();
			String columnValue = entry.getValue();
			
			if(!columnValue.equals("*ALL")) {
				if(!columnValue.equals("*NULL")) {
					String sqlToAdd = "( " + columnName + "='" + columnValue + "') ";
					jpql+= " AND " + sqlToAdd;
				} else if(columnValue.equals("*NULL")) {
					String sqlToAdd = "( " + columnName + " IS NULL)";
					jpql+= " AND " + sqlToAdd;
				}
			
			}
			
		}
		
		TypedQuery<PurchaseTransaction> tq = entityManager.createQuery(jpql, PurchaseTransaction.class);
		tq.setParameter(1, suspendIndicator);
		tq.setParameter(2, effectiveDate);
		tq.setParameter(3, expirationDate);
		tq.setParameter(4, entityId);
		String sqlQuery = tq.unwrap(org.hibernate.Query.class).getQueryString();
		
		transactions = tq.getResultList();
		document.setLocationTransSql(sqlQuery);
		
		
		document.setPurchaseTransaction(transactions);
		document.setProcessedTransactions(transactions.size());
		if(transactions.isEmpty())
			throw new Exception(sqlQuery);
		
		return document;
    }

	@Override
	public ProcessSuspendedTransactionDocument findSuspendedTransactions(Long newSuspendRuleId, SuspendReason suspendReason) throws PurchaseTransactionProcessSuspendedException {
		// TODO Auto-generated method stub
		ProcessSuspendedTransactionDocument document = new ProcessSuspendedTransactionDocument();

		try {
		//SuspendReasons: SUSPEND_LOCN, SUSPEND_GS, SUSPEND_TAXCODE_DTL, SUSPEND_RATE
		String columnHeader = null;
		String suspendIndicator = null;
		Auditable exampleInstance = null;
		Long entityId = null;
		
		long driverCount = 0;
		if(suspendReason == null) {
			throw new InvalidSuspensionTypeException("Invalid Suspend Type");			
		}
		switch(suspendReason)
		{
		case SUSPEND_GS:
			columnHeader="tax";
			suspendIndicator=PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_GSB;
			exampleInstance = (TaxMatrix) findSpecificAuditable(newSuspendRuleId, TaxMatrix.class);
			driverCount = ((TaxMatrix) exampleInstance).getDriverCount();
			entityId = ((TaxMatrix) exampleInstance).getEntityId();
			break;
		case SUSPEND_LOCN:
			columnHeader="location";
			suspendIndicator=PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_LOCATION;
			exampleInstance = (LocationMatrix) findSpecificAuditable(newSuspendRuleId, LocationMatrix.class);
			driverCount = ((LocationMatrix) exampleInstance).getDriverCount();
			entityId = ((LocationMatrix) exampleInstance).getEntityId();
			break;
		case SUSPEND_RATE:
			columnHeader="com.ncsts.domain.JurisdictionTaxrate.class";
			suspendIndicator=PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_RATE;
			exampleInstance = (JurisdictionTaxrate) findSpecificAuditable(newSuspendRuleId, JurisdictionTaxrate.class);
			break;
		case SUSPEND_TAXCODE_DTL:
			columnHeader = "taxcodedetail";
			suspendIndicator=PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_TAXCODE_DTL;
			exampleInstance = (TaxCodeDetail) findSpecificAuditable(newSuspendRuleId, TaxCodeDetail.class);
			break;
		}
		Map<String, String> driversToSearch;
		
		if(exampleInstance instanceof Matrix) {
			driversToSearch = findMatrixValuesForJPA(newSuspendRuleId, exampleInstance, driverCount, entityId);
			document = findMatchingTransactionsMatricies(document, driversToSearch, suspendIndicator, columnHeader);
		} else if (exampleInstance instanceof TaxCodeDetail) {
			//TODO: Will evaluate what to do for TaxCodeDetail and JurisdictionTaxrate
			document = findMatchingTransactionsTaxCodeDetail(document, newSuspendRuleId, exampleInstance, suspendIndicator);
		} else if (exampleInstance instanceof JurisdictionTaxrate) {
			document = findMatchingTransactionsJurisdictionTaxrate(document, newSuspendRuleId, exampleInstance, suspendIndicator);

		}
		  else {
			throw new AuditableIdNotFoundException("Invalid Id for Auditable");
		}
		

		
	
		} 	catch(AuditableIdNotFoundException e) {
			e.printStackTrace();
			throw new PurchaseTransactionProcessSuspendedException(retrieveErrorText("P32"));
		} catch (InvalidSuspensionTypeException e) {
			e.printStackTrace();
			throw new PurchaseTransactionProcessSuspendedException(retrieveErrorText("P33"));
		}
			catch (Exception e) {
			e.printStackTrace();
			throw new PurchaseTransactionProcessSuspendedException(e.getMessage(), retrieveErrorText("P34"));
		}

		return document;
	}
	
	public List<PurchaseTransaction> findAllTransactionsByMatrixId(Long matrixId, List<String> matrixFields){
		
		String jpql = "select purchtrans FROM PurchaseTransaction purchtrans WHERE ";
		
		for(int i = 0; i < matrixFields.size(); i++) {
			if(i != 0)
				jpql+=" or ";
			jpql+= "purchtrans." + matrixFields.get(i) + " = :matrixId";
		}

		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
		TypedQuery<PurchaseTransaction> tq = entityManager.createQuery(jpql, PurchaseTransaction.class);
		tq.setParameter("matrixId", matrixId);
		
		return tq.getResultList();
	}
}
