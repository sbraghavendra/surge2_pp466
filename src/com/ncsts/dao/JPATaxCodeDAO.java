package com.ncsts.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;

/**
 * @author Paul Govindan
 *
 */

public class JPATaxCodeDAO extends JPAGenericDAO<TaxCode,TaxCodePK> implements TaxCodeDAO {
	
	private Map<String,String> columnMapping = null;
	
	@SuppressWarnings("deprecation")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(TaxCode instance) throws DataAccessException {

		Session session = createLocalSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();			
			session.merge(instance);
			session.flush();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				logger.debug("Rolling back");
				logger.debug("Exception:"+e);
				tx.rollback();
			}
		} 
		
		closeLocalSession(session);
		session=null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaxCode> getTaxCodeList(TaxCode exampleInstance,int firstRow,int maxResults,OrderBy orderBy) {
		
		List<TaxCode> taxCodeList= new ArrayList<TaxCode>();
		String whereClauseToken = getWhereClauseToken(exampleInstance);	
		try{
			String strOrderBy = getOrderByToken(orderBy);	
			String sqlQuery="SELECT * from (Select ROW_NUMBER() OVER (ORDER BY :orderby) AS RowNumber, " +
					        "tc.taxcode_code, tc.description,tc.comments,tc.active_flag, tcrule.rulecount FROM tb_taxcode tc LEFT JOIN " +
					        " (SELECT tcd.taxcode_code, count(*) AS rulecount FROM tb_taxcode_detail tcd GROUP BY tcd.taxcode_code) tcrule ON "    +
					        " tcrule.taxcode_code = tc.taxcode_code  WHERE 1=1 :extendWhere";
			sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);
			sqlQuery = sqlQuery + ")" +  " WHERE RowNumBER BETWEEN :minrow AND :maxrow ";
			sqlQuery = sqlQuery.replaceAll(":minrow", "" + (firstRow + 1)); //1 to 50, 51 to 100
			sqlQuery = sqlQuery.replaceAll(":maxrow", "" + (firstRow + maxResults));

			sqlQuery = sqlQuery.replaceAll(":orderby", strOrderBy);
			Query q = entityManager.createNativeQuery(sqlQuery);
			List<Object> list =  q.getResultList();
			Iterator<Object> iter=list.iterator();
			while(iter.hasNext()){
				TaxCode taxcode = new TaxCode();
				Object[] objarray= (Object[])iter.next();
				String taxCodecode = (String) objarray[1]; 
				TaxCodePK taxcodePK = new TaxCodePK();
				taxcodePK.setTaxcodeCode(taxCodecode);
				taxcode.setTaxCodePK(taxcodePK);
				String description = (String) objarray[2];
				taxcode.setDescription(description);
				String comments = (String)objarray[3];
				taxcode.setComments(comments);
				Character activeFlag = (Character)objarray[4];
				taxcode.setActiveFlag(activeFlag.toString());
				BigDecimal refdocCount = (BigDecimal)objarray[5];
				taxcode.setRefdoccount(refdocCount);
				taxCodeList.add(taxcode);
		  }
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return taxCodeList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaxCode> findTaxCodeList(
			String taxcodeCode, String desc, String activeFlag) throws DataAccessException {
		Session session = createLocalSession();
		Criteria criteria = session.createCriteria(TaxCode.class);
		addSqlRestriction(criteria, "TAXCODE_CODE", taxcodeCode);
		addSqlRestriction(criteria, "DESCRIPTION", desc);
		
		if(StringUtils.hasText(activeFlag)) {
			if("1".equals(activeFlag)) {
				criteria.add(Restrictions.eq("activeFlag", "1"));
			}
			else if("0".equals(activeFlag)) {
				criteria.add(Restrictions.or(Restrictions.or(Restrictions.eq("activeFlag", "0"), Restrictions.isNull("activeFlag")), Restrictions.eq("activeFlag", "")));
			}
		}
		
		criteria.addOrder(Order.asc("taxCodePK.taxcodeCode"));
		
		List<TaxCode> list = criteria.list();
		closeLocalSession(session);
		session = null;
		
		return list;
	}	

	@Transactional
	public Long count(TaxCode exampleInstance) {
		Long count = 0L;
		String whereClauseToken = getWhereClauseToken(exampleInstance);	
		try{
		    EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
				String sqlQuery="SELECT COUNT(*) FROM tb_taxcode tc LEFT JOIN " +
						 " (SELECT tcd.taxcode_code, count(*) AS rulecount FROM tb_taxcode_detail tcd GROUP BY tcd.taxcode_code) tcrule ON "    +
						  " tcrule.taxcode_code = tc.taxcode_code  WHERE 1=1 :extendWhere";
				sqlQuery = sqlQuery.replaceAll(":extendWhere", whereClauseToken);
				Query q = entityManager.createNativeQuery(sqlQuery);
				BigDecimal id = (BigDecimal) q.getSingleResult();
				count = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
        return count;
	}
	
	private String getWhereClauseToken(TaxCode taxcode) {
	        StringBuffer whereClauseExt = new StringBuffer();
		
		    if (taxcode.getTaxcodeCode()!=null && !taxcode.getTaxcodeCode().equals("")) {
		    	appendANDToWhere(whereClauseExt, "tc.taxcode_code like '" + taxcode.getTaxcodeCode() +"'");
		    }
		    if (taxcode.getDescription()!=null && !taxcode.getDescription().equals("")) {
		    	appendANDToWhere(whereClauseExt, "tc.description like '" + taxcode.getDescription() +"'");
		    }
		    if (taxcode.getActiveFlag()!=null) {
		    	if( "-1".equals(taxcode.getActiveFlag())) {
		    		appendANDToWhere(whereClauseExt, "(tc.active_flag = '" + "1" +"'" + " OR " + "tc.active_flag = '" + 0 +"')");
		        }
		    	else{
		    		appendANDToWhere(whereClauseExt, "tc.active_flag = '" + taxcode.getActiveFlag() + "'");
		    	}
		    }
	    
	return whereClauseExt.toString();
	}
	private void appendANDToWhere(StringBuffer where, String andClause){
		where.append(" AND " + andClause);
	}
	
	private String getOrderByToken(OrderBy orderBy){
		StringBuffer orderByToken = new StringBuffer();
		if (orderBy != null){
			for (FieldSortOrder field : orderBy.getFields()){
				if (field.getAscending()){
					orderByToken.append(getTaxCodeColumnName(field.getName().toLowerCase()) + " ASC");
				} 
				else {
					orderByToken.append(getTaxCodeColumnName(field.getName().toLowerCase()) + " DESC");
				}
			}
         }
		
		//Default order
		if(orderByToken.length()==0){
			orderByToken.append("tc.taxcode_code");
		}
		 return orderByToken.toString();
	}
	

	private String getTaxCodeColumnName(String name){
		if(columnMapping==null){
			columnMapping = new HashMap<String,String>();
			columnMapping.put("taxcodeCode".toLowerCase(), "tc.taxcode_code"); 
			columnMapping.put("description".toLowerCase(), "tc.description"); 
			columnMapping.put("comments".toLowerCase(), "tc.comments"); 
			columnMapping.put("activeFlag".toLowerCase(), "active_flag"); 
			columnMapping.put("refdoccount".toLowerCase(), "rulecount"); 
		}
		
		String columnName = columnMapping.get(name.toLowerCase());
		if(columnName!=null && columnName.length()>0){
			return columnName;
		}
		else{
			return "";
		}
	}
	
}


