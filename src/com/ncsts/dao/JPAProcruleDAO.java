/**
 * 
 */
package com.ncsts.dao;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.ProruleAudit;
import com.ncsts.domain.SitusMatrix;


public class JPAProcruleDAO extends JPAGenericDAO<Procrule, Long> implements ProcruleDAO {
	@PersistenceContext 
	private EntityManager entityManager;
	
	public Session getCurrentSession(){
		return createLocalSession();
	}
	
	@Transactional
	public void addProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(Procrule.class);
			criteria.setProjection(Projections.max("orderNo")); 
			List<?> result = criteria.list();
			if(result==null || result.get(0)==null){
				updateProcrule.setOrderNo(new Long(1));
			}
			else{
				Long returnLong = ((Long)result.get(0)).longValue();
				if(returnLong!=null){
					updateProcrule.setOrderNo(new Long(returnLong.longValue()+1));
				}
				else{
					updateProcrule.setOrderNo(new Long(1));
				}
			}
			
			session.save(updateProcrule);
			session.flush();

			for (ProcruleDetail aProcruleDetail : updateProcruleEntityList) {
				aProcruleDetail.setProcruleId(updateProcrule.getProcruleId());
				session.save(aProcruleDetail);
			}
					
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteProcruleDetail(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			// First delete all TB_PROCRULE_DETAIL, PROCRULE_ID by ProcruleId, EffectiveDate
			Date effectiveDate = ((ProcruleDetail)updateProcruleEntityList.get(0)).getEffectiveDate();
			session.createQuery("delete ProcruleDetail where procruleId = :procruleId and effectiveDate = :effectiveDate")
			.setParameter("procruleId", updateProcrule.getId()).setParameter("effectiveDate", effectiveDate)
			.executeUpdate();
	
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void deleteProcrule(Procrule updateProcrule) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			String hqlDelete = "";
			
			int deleteCount = 0;

			// First delete all TB_PROCRULE_DETAIL, PROCRULE_ID by ProcruleId
			hqlDelete = "delete TB_PROCRULE_DETAIL WHERE PROCRULE_ID = " + updateProcrule.getProcruleId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			// Second delete TB_PROCRULE, PROCRULE_ID by ProcruleId
			hqlDelete = "delete TB_PROCRULE WHERE PROCRULE_ID = " + updateProcrule.getProcruleId().intValue();
			query = session.createSQLQuery(hqlDelete);
			deleteCount = query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public void updateProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();
			
			Procrule aProcrule = findById(updateProcrule.getId());
			if(aProcrule==null){
				return;
			}
			
			//aProcrule.setProcruleName(updateProcrule.getProcruleName());
			aProcrule.setOrderNo(updateProcrule.getOrderNo());
			aProcrule.setDescription(updateProcrule.getDescription());
			aProcrule.setRuletypeCode(updateProcrule.getRuletypeCode());
			aProcrule.setActiveFlag(updateProcrule.getActiveFlag());		
			aProcrule.setAlwaysFlag(updateProcrule.getAlwaysFlag());
			aProcrule.setReprocessCode(updateProcrule.getReprocessCode());

			session.update(aProcrule);
			
			// First delete all TB_PROCRULE_DETAIL, PROCRULE_ID by ProcruleId, EffectiveDate
			Date effectiveDate = ((ProcruleDetail)updateProcruleEntityList.get(0)).getEffectiveDate();
			session.createQuery("delete ProcruleDetail where procruleId = :procruleId and effectiveDate = :effectiveDate")
			.setParameter("procruleId", updateProcrule.getId()).setParameter("effectiveDate", effectiveDate)
			.executeUpdate();
			
			for (ProcruleDetail aProcruleDetail : updateProcruleEntityList) {
				aProcruleDetail.setProcruleId(updateProcrule.getProcruleId());
				session.save(aProcruleDetail);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	public Long count(Procrule exampleInstance) {
		Session localSession = this.createLocalSession();
		Long returnLong = 0L;
		
		try {
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setProjection(Projections.rowCount()); 
			List<?> result = criteria.list();
			returnLong = ((Number)result.get(0)).longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}
		
		return returnLong;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProcruleDetail> findProcruleDetail(Long procruleId, Date effectiveDate) throws DataAccessException {	
		List<ProcruleDetail> returnList = new ArrayList<ProcruleDetail>();
		
		Session localSession = this.createLocalSession();
		try {
			Criteria criteria = localSession.createCriteria(ProcruleDetail.class);
			criteria.add(Restrictions.eq("procruleId",procruleId));
			criteria.add(Restrictions.eq("effectiveDate",effectiveDate));
			criteria.addOrder( Order.asc("lineNo") );
			
			returnList = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return returnList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findProcruleDetailDistinctDate(Long procruleId) throws DataAccessException {
		List<String> result =  null;	
		String sqlQuery = 	"SELECT DISTINCT TO_CHAR(EFFECTIVE_DATE, 'mm/dd/yyyy') AS EFFECTIVE_DATE FROM TB_PROCRULE_DETAIL " +
							"WHERE EFFECTIVE_DATE IS NOT NULL AND PROCRULE_ID = " + procruleId + " ORDER BY EFFECTIVE_DATE DESC ";
		
		Query q = entityManager.createNativeQuery(sqlQuery);
		return q.getResultList();
	}
	
	@Transactional
	public void reorderProcruleList(List<Procrule> updateProcruleList) throws DataAccessException {	
		Session session = createLocalSession();
		Transaction tx = null;
		org.hibernate.Query query = null;
		try {
			tx = session.beginTransaction();

			for (Procrule aProcrule : updateProcruleList) {
				session.update(aProcrule);
			}
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			closeLocalSession(session);
			session=null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Procrule> find(Procrule exampleInstance, OrderBy orderBy, int firstRow, int maxResults) {

		List<Procrule> list = new ArrayList<Procrule>();
		
		Session localSession = this.createLocalSession();
		try {  
			Criteria criteria = generateCriteriaBatch(exampleInstance, localSession);
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
            if (orderBy != null && orderBy.getFields().size()>0){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            else{
            	//Default by orderNo asc
            	criteria.addOrder( Order.asc("orderNo") );
            }
            
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Procrule> getProcrule(String moduleCode, String ruletypeCode) throws DataAccessException {

		List<Procrule> list = new ArrayList<Procrule>();
		
		Session localSession = this.createLocalSession();
		try {  

			Criteria criteria = localSession.createCriteria(Procrule.class);
			
			if(moduleCode!=null && moduleCode.length()>0){
				criteria.add(Restrictions.eq("moduleCode", moduleCode));
			}
			
			if(ruletypeCode!=null && ruletypeCode.length()>0){
				criteria.add(Restrictions.eq("ruletypeCode", ruletypeCode));
			}
					
            criteria.addOrder( Order.asc("orderNo") );
   
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Procrule> getAllProcrule() throws DataAccessException {

		List<Procrule> list = new ArrayList<Procrule>();
		
		Session localSession = this.createLocalSession();
		try {  

			Criteria criteria = localSession.createCriteria(Procrule.class);
			
            criteria.addOrder( Order.asc("orderNo") );
   
            list = criteria.list();
            
            this.closeLocalSession(localSession);
            localSession=null;
            
            return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProruleAudit> getAllProcAuditRecords(String sessionId)
			throws DataAccessException {
		long begintime = System.currentTimeMillis();

		String sqlQuery = "SELECT  lc.description, "
				+ "pr.description description1, "
				+ "CASE "
				+ "WHEN ddc.description IS NULL THEN au.column_name "
				+ "ELSE ddc.description "
				+ "END column_name_desc, "
				+ "au.before_value, "
				+ "au.after_value "
				+ "FROM tb_procrule_audit au "
				+ "LEFT OUTER JOIN tb_procrule pr "
				+ "ON pr.procrule_id = au.procrule_id "
				+ "LEFT OUTER JOIN tb_list_code lc ON lc.code_type_code = 'RULETYPE' "
				+ "AND lc.code_code = pr.ruletype_code "
				+ "LEFT OUTER JOIN tb_data_def_column ddc ON ddc.table_name = 'TB_SALETRANS' "
				+ "AND ddc.column_name = au.column_name "
				+ "WHERE au.session_id ='" + sessionId + "' " +
				// "WHERE au.transaction_id ="+ id +" "+
				"ORDER BY lc.SEVERITY_LEVEL, pr.order_no ";

		//System.out.println("JPAProcruleDAO->getAllProcAuditRecords"+sqlQuery);	
		List<ProruleAudit> result = null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list = q.getResultList();
		Iterator<Object> iter = list.iterator();
		if (list != null) {
			result = new ArrayList<ProruleAudit>();

			try {
				long pseudoId = 1;
				while (iter.hasNext()) {
					Object[] objarray = (Object[]) iter.next();

					ProruleAudit proAudit = new ProruleAudit();

					proAudit.setId(new Long(pseudoId++));

					String listcodeDesc = (String) objarray[0];
					proAudit.setListcodedescription(listcodeDesc);

					String proruleDesc = (String) objarray[1];
					proAudit.setProruledescription(proruleDesc);

					String datadefdescription = (String) objarray[2];// First
																		// column
					proAudit.setDatadefdescription(datadefdescription);

					String beforeValue = (String) objarray[3];
					proAudit.setBeforeValue(beforeValue);

					String afterValue = (String) objarray[4];
					proAudit.setAfterValue(afterValue);

					result.add(proAudit);
				}
			} catch (Exception e) {

			}
		}

		String time = Long.toString(System.currentTimeMillis() - begintime);
		System.out.println("\t *** Executing Java, " + time + " miliseconds,  "
				+ result.size() + " rows");

		return result;
	}
	

	private Criteria generateCriteriaBatch(Procrule exampleInstance, Session localSession) {
		Criteria criteria = localSession.createCriteria(Procrule.class);
		
		if(exampleInstance.getModuleCode() != null && exampleInstance.getModuleCode().length()>0) {
			criteria.add(Restrictions.eq("moduleCode",exampleInstance.getModuleCode()));
		}    
        
		if(exampleInstance.getProcruleName() != null && exampleInstance.getProcruleName().length()>0) {
			if(exampleInstance.getProcruleName().substring(exampleInstance.getProcruleName().length()-1, exampleInstance.getProcruleName().length()).equals("%") ||
					exampleInstance.getProcruleName().substring(0, 1).equals("%")){
				criteria.add(Restrictions.ilike("procruleName",exampleInstance.getProcruleName()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("procruleName",exampleInstance.getProcruleName()));
			}
		}    
	
        if (exampleInstance.getDescription()!=null  && exampleInstance.getDescription().length()>0) {
        	if(exampleInstance.getDescription().substring(exampleInstance.getDescription().length()-1, exampleInstance.getDescription().length()).equals("%") ||
					exampleInstance.getDescription().substring(0, 1).equals("%")){
        		criteria.add(Restrictions.ilike("description",exampleInstance.getDescription()));//Not case sensitive
			}
			else{
				criteria.add(Restrictions.eq("description",exampleInstance.getDescription()));
			}
        }          

        if(exampleInstance.getActiveFlag()!=null && exampleInstance.getActiveFlag().length()>0){
        	criteria.add(Restrictions.eq("activeFlag",exampleInstance.getActiveFlag()));
		}
        
        if(exampleInstance.getRuletypeCode()!=null && exampleInstance.getRuletypeCode().length()>0){
        	criteria.add(Restrictions.eq("ruletypeCode",exampleInstance.getRuletypeCode()));
        }
        
        return criteria;
	}
	
	public Long count(Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		List<?> result = criteria.list();
		return ((Number)result.get(0)).longValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<Procrule> find(Criteria criteria) {
		return criteria.list();
	}
	
	public List<Procrule> find(DetachedCriteria criteria) {
		Session localSession = createLocalSession();
		List<Procrule> returnCust = find(criteria.getExecutableCriteria(localSession));
		closeLocalSession(localSession);
		localSession=null;
		
		return returnCust;
	}

	public Procrule find(Procrule exampleInstance) {
		List<Procrule> list = find(exampleInstance, null, 0, 1);
		if ((list != null) && (list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean getIsRuleNameExist(String procruleName, String moduleCode) throws DataAccessException {
		boolean exist = true;
		List<Procrule> list = new ArrayList<Procrule>();
		
		Session localSession = this.createLocalSession();
		try {  
			Criteria criteria = localSession.createCriteria(Procrule.class);
			criteria.add(Restrictions.ilike("procruleName",procruleName));//Not case sensitive
			criteria.add(Restrictions.eq("moduleCode", moduleCode));
            list = criteria.list();
            
            if(list==null || list.size()==0){
            	exist = false;
            }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return exist;
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProruleAudit> getProcRuleRecords(String sessionId)throws DataAccessException  {
		long begintime = System.currentTimeMillis();
		
		SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String sqlQuery=
				"SELECT  lc.description, "+
				"pr.procrule_name, "+
				"pr.description description1, "+
				"CASE "+
				"WHEN ddc.description IS NULL THEN au.column_name "+
				"ELSE ddc.description "+
				"END column_name_desc, "+
				"au.before_value, "+
				"au.after_value, "+ 
				"to_char(au.process_timestamp,'MM/dd/yyyy hh24:mi:ss') as processTimestamp " +
				"FROM tb_procrule_audit au "+
				"LEFT OUTER JOIN tb_procrule pr "+
				"ON pr.procrule_id = au.procrule_id "+
				"LEFT OUTER JOIN tb_list_code lc ON lc.code_type_code = 'RULETYPE' "+
				"AND lc.code_code = pr.ruletype_code "+
				"LEFT OUTER JOIN tb_data_def_column ddc ON ddc.table_name = 'TB_SALETRANS' "+
				"AND ddc.column_name = au.column_name "+
				"WHERE au.session_id ='"+ sessionId +"' "+
				"ORDER BY au.process_timestamp desc ";
		
		List<ProruleAudit> result = null;
		Query q = entityManager.createNativeQuery(sqlQuery);

		List<Object> list= q.getResultList();
		Iterator<Object> iter=list.iterator();
		if(list!=null){
			result = new ArrayList<ProruleAudit>();
		
			try{
				long pseudoId = 1;
				while(iter.hasNext()){
					Object[] objarray= (Object[])iter.next();

					ProruleAudit proAudit = new ProruleAudit();
				
					proAudit.setId(new Long(pseudoId++));
					
					String listcodeDesc=(String)objarray[0];
					proAudit.setListcodedescription(listcodeDesc);
					
					String name = (String) objarray[1];
					proAudit.setProcruleName(name);
					
					String proruleDesc=(String)objarray[2];
					proAudit.setProruledescription(proruleDesc);
					
					String datadefdescription = (String) objarray[3];//First column
					proAudit.setDatadefdescription(datadefdescription);
					
					
					String beforeValue = (String) objarray[4];
					proAudit.setBeforeValue(beforeValue);
					
					String afterValue = (String) objarray[5];
					proAudit.setAfterValue(afterValue);
					
					String processTimestamp = (String) objarray[6];
					Date d = f.parse(processTimestamp);
					proAudit.setProcessTimestamp(new Timestamp(d.getTime())); 

		            result.add(proAudit);                
				}
			}catch(Exception e){
				
			}
		}
		
		String time = Long.toString(System.currentTimeMillis() - begintime);
		System.out.println("\t *** Executing Java, " + time + " miliseconds,  " + result.size() + " rows");

		return result;
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean getIsRuleNameDateExist(String procruleName, String moduleCode, Date effectiveDate) throws DataAccessException {
		boolean exist = true;
		List<Procrule> list = new ArrayList<Procrule>();
		
		Session localSession = this.createLocalSession();
		try {  
			Criteria criteria = localSession.createCriteria(Procrule.class);
			criteria.add(Restrictions.ilike("procruleName",procruleName));//Not case sensitive
			criteria.add(Restrictions.eq("moduleCode", moduleCode));
            list = criteria.list();
            
            if(list==null || list.size()==0){
            	exist = false;
            }
            else{           	
            	Long procruleId = ((Procrule) list.get(0)).getProcruleId();
            	criteria = localSession.createCriteria(ProcruleDetail.class);
            	criteria.add(Restrictions.eq("procruleId", procruleId));
    			criteria.add(Restrictions.eq("effectiveDate", effectiveDate));
                list = criteria.list();
                
                if(list==null || list.size()==0){
                	exist = false;
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeLocalSession(localSession);
			localSession=null;
		}

		return exist;
	}
	@Transactional(readOnly = false)
	public boolean isAllowDeleteRules(Long procruleId) throws DataAccessException {
	
		Long returnLong = 1L;
		try {
			String sqlQuery = "SELECT count(*) FROM tb_procrule_audit WHERE procrule_id = " + procruleId;			
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return (returnLong==0L);
	}
	
	@Transactional(readOnly = false)
	public boolean isAllowDeleteDefinitions(Long procruleId, Date effectiveDate) throws DataAccessException {
	
		Long returnLong = 1L;
		
		try{
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveDateStr = df.format(effectiveDate);

			String sqlQuery = "SELECT count(*) FROM tb_procrule_audit WHERE procrule_id = " + procruleId + 
					" AND procrule_detail_id IN (SELECT procrule_detail_id FROM tb_procrule_detail WHERE procrule_id = " + procruleId + 
					" AND effective_date = to_date('"+ effectiveDateStr +"','mm/dd/yyyy')) ";
			Query q = entityManager.createNativeQuery(sqlQuery);
			BigDecimal id = (BigDecimal) q.getSingleResult();
			
			returnLong = id.longValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		
		return (returnLong==0L);
	}
	
}
