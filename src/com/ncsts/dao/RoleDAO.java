package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Role;

/**
 * @author Paul Govindan
 *
 */

public interface RoleDAO {
	
    public abstract Role findById(String roleCode) throws DataAccessException;	
    
    public abstract List<Role> findAllRoles() throws DataAccessException;
    
	public void remove(String id) throws DataAccessException;    
    
	public void saveOrUpdate (Role role) throws DataAccessException;     
    
	public String persist (Role role) throws DataAccessException;    
      
}

