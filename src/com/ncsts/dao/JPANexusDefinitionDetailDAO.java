package com.ncsts.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.NexusDefinitionDetail;

public class JPANexusDefinitionDetailDAO extends JPAGenericDAO<NexusDefinitionDetail, Long> implements NexusDefinitionDetailDAO {
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NexusDefinitionDetail> findAllNexusDefinitionDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFag) 
		throws DataAccessException {
		
		StringBuffer sql =  new StringBuffer("Select ndd from NexusDefinitionDetail ndd where ndd.nexusDefId in " +
				"(select nexusDefId from NexusDefinition where nexusCountryCode = ?1 and nexusStateCode = ?2 and " +
				"nexusCounty = ?3 and nexusCity = ?4 and nexusStj = ?5 and entityId = ?6)");
		
		List<NexusDefinitionDetail> list = null;
		if(activeFag) {
			sql.append(" and activeFlag = ?7");
			list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId, "1");
		}
		else {
			list = getJpaTemplate().find(sql.toString(), countryCode, stateCode, county, city, stj, entityId);
		}
		
		return list;
	}
	
	@Transactional
	public boolean isNexusDefinitionExist(Long entityId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException {

		String sql = 	"Select count(*) " +
						"from TB_NEXUS_DEF nd join TB_NEXUS_DEF_DETAIL ndd on nd.NEXUS_DEF_ID=ndd.NEXUS_DEF_ID where (ndd.active_flag = '1') ";
		
		if(entityId!=null){
			sql = sql + " and nd.ENTITY_ID = " + entityId.intValue();
		}
		
		if(countryCode!=null){
			sql = sql + " and nd.NEXUS_COUNTRY_CODE = '" + countryCode + "'";
		}
		
		if(stateCode!=null){
			sql = sql + " and nd.NEXUS_STATE_CODE = '" + stateCode + "'";
		}
		
		if(county!=null){
			sql = sql + " and nd.NEXUS_COUNTY = '" + county + "'";
		}
		
		if(city!=null){
			sql = sql + " and nd.NEXUS_CITY = '" + city + "'";
		}
		
		if(stj!=null){
			sql = sql + " and nd.NEXUS_STJ = '" + stj + "'";
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String now = df.format(effectiveDate);
		
		if(type!=null && type.length()>0){ //check auto
			sql = sql + " and ndd.nexus_type_code = '" + type + "'";
			sql = sql + " and ndd.effective_date <= to_date('" + now + "', 'mm/dd/yyyy')"; 
		}
		else{
			sql = sql + " and ndd.effective_date = to_date('" + now + "', 'mm/dd/yyyy')"; 
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();

	    if(count!=null && count.intValue()>0){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	
	@Transactional
	public boolean isNexusDefinitionActive(Long entityId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException {

		String sql = 	"Select count(*) " +
						"from TB_NEXUS_DEF nd join TB_NEXUS_DEF_DETAIL ndd on nd.NEXUS_DEF_ID=ndd.NEXUS_DEF_ID where (ndd.active_flag = '1') ";
		
		if(entityId!=null){
			sql = sql + " and nd.ENTITY_ID = " + entityId.intValue();
		}
		
		if(countryCode!=null){
			sql = sql + " and nd.NEXUS_COUNTRY_CODE = '" + countryCode + "'";
		}
		
		if(stateCode!=null){
			sql = sql + " and nd.NEXUS_STATE_CODE = '" + stateCode + "'";
		}
		
		if(county!=null){
			sql = sql + " and nd.NEXUS_COUNTY = '" + county + "'";
		}
		
		if(city!=null){
			sql = sql + " and nd.NEXUS_CITY = '" + city + "'";
		}
		
		if(stj!=null){
			sql = sql + " and nd.NEXUS_STJ = '" + stj + "'";
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();

	    if(count!=null && count.intValue()>0){
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}

	@Override
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void bulkExpireNexusDefinitionDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		String sql =  "Update NexusDefinitionDetail set expirationDate = :expirationDate where nexusDefId in " +
				"(select nexusDefId from NexusDefinition where nexusCountryCode = :countryCode and nexusStateCode = :stateCode) and expirationDate > :expirationDate";
		
		Session session = createLocalSession();
		try {
			org.hibernate.Query q = session.createQuery(sql);
			q.setParameter("countryCode", countryCode);
			q.setParameter("stateCode", stateCode);
			q.setParameter("expirationDate", expirationDate);
			q.executeUpdate();
		} 
		catch (Exception e) {	
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Override
	@Transactional( propagation = Propagation.REQUIRED, readOnly = false )
	public void bulkExpireRegistrationDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		String sql =  "Update RegistrationDetail set expirationDate = :expirationDate where registrationId in " +
				"(select registrationId from Registration where regCountryCode = :countryCode and regStateCode = :stateCode) and expirationDate > :expirationDate";

		Session session = createLocalSession();
		try {
			org.hibernate.Query q = session.createQuery(sql);
			q.setParameter("countryCode", countryCode);
			q.setParameter("stateCode", stateCode);
			q.setParameter("expirationDate", expirationDate);
			q.executeUpdate();
		} 
		catch (Exception e) {	
			e.printStackTrace();
		}
		finally {
			closeLocalSession(session);
			session=null;
		}
	}
	
	@Transactional
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException {
		boolean isExist = false;
		
		String sql = "";
		if(nexusTypeCode!=null && nexusTypeCode.length()>0){
			//check for auto jurisdiction
			sql = " select count(*) from NexusDefinitionDetail nexusDefinitionDetail where nexusDefinitionDetail.nexusDefId = ? and  nexusDefinitionDetail.effectiveDate <= ? and nexusTypeCode = ? ";	
		}
		else{
			sql = " select count(*) from NexusDefinitionDetail nexusDefinitionDetail where nexusDefinitionDetail.nexusDefId = ? and  nexusDefinitionDetail.effectiveDate = ? "; 
		}
		
		EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
	  	Query query = entityManager.createNativeQuery(sql);
	  	query.setParameter(1, nexusDefId );
	  	query.setParameter(2, effectiveDate );
	  		  	
	  	if(nexusTypeCode!=null && nexusTypeCode.length()>0){
			//check for auto jurisdiction
		  	query.setParameter(3, nexusTypeCode );
		}
		else{
		}
	  	
	  	Long count  = ((BigDecimal)query.getSingleResult()).longValue();  
	    if(count!=null && count.intValue()>0){
	    	isExist = true;
	    }
		
        return isExist;
	}
}
