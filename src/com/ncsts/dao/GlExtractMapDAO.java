package com.ncsts.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.GlExtractMap;

public interface GlExtractMapDAO extends MatrixDAO<GlExtractMap>{
	public abstract GlExtractMap findByGlExtractMapId(Long glExtractMapId) throws DataAccessException;
	public abstract List<GlExtractMap> findByGLExtractMap(GlExtractMap glExtractMap, String... excludeProperty) throws DataAccessException;
	public abstract GlExtractMap saveGl(GlExtractMap glExtractMap) throws DataAccessException;
	public abstract void update(GlExtractMap glExtractMap) throws DataAccessException;
	public abstract void remove(long glExtractMapId) throws DataAccessException;
	public List<GlExtractMap> find(GlExtractMap exampleInstance, 
			  OrderBy orderBy, int firstRow, int maxResults, String... excludeProperty);
	public List<GlExtractMap> findByStateCode(String state);
}
