package com.ncsts.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.BillScenario;
import com.ncsts.domain.PurchaseScenario;

public class JPAScenarioDAO extends JPAGenericDAO<BillScenario,Long> implements
	ScenarioDAO {
	
	@Transactional(readOnly=true)
	public Long count(BillScenario exampleInstance) {
		Long ret = 0L;	
		
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(BillScenario.class);
			Example example = Example.create(exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			
			criteria.setProjection(Projections.rowCount()); // We just want a row count
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in JPATransactionDetailSaleDAO::()");
		} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error in finding the entities by example");
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<BillScenario> getAllRecords(BillScenario exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<BillScenario> list =  null;

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(BillScenario.class);
			Example example = Example.create(exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            list = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getScenarioNames() {
		List<String> list =  null;

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(BillScenario.class);
			criteria.setProjection(Projections.distinct(Projections.property("billscenarioName")));
			
            list = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}
	
	public int deleteByName(String name) {
		int count = 0;
		Session session = createLocalSession();
		Transaction tx = session.beginTransaction();
		try { 
			count = session.createQuery("delete BillScenario where billscenarioName = :scenarioName").setString("scenarioName", name).executeUpdate();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		finally{
			tx.commit();

			closeLocalSession(session);
			session=null;
		}
		
		return count;
	}
	
	
	@Transactional(readOnly=true)
	public Long count(PurchaseScenario exampleInstance) {
		Long ret = 0L;	
		
		Session session = createLocalSession();	
		try {
			Criteria criteria = session.createCriteria(PurchaseScenario.class);
			Example example = Example.create(exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			
			criteria.setProjection(Projections.rowCount()); // We just want a row count
			List< ? > result = criteria.list();	
			ret = ((Number) result.get(0)).longValue();

		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in JPATransactionDetailSaleDAO::()");
		} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error in finding the entities by example");
		} finally {
			closeLocalSession(session);
			session=null;
		}
        return ret;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<PurchaseScenario> getAllRecords(PurchaseScenario exampleInstance,
			OrderBy orderBy, int firstRow, int maxResults) {
		List<PurchaseScenario> list =  null;

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(PurchaseScenario.class);
			Example example = Example.create(exampleInstance);

			example.ignoreCase();
			criteria.add(example);
			
			criteria.setFirstResult(firstRow);
			if (maxResults > 0) {
				criteria.setMaxResults(maxResults);
			}
			
            if (orderBy != null){
                for (FieldSortOrder field : orderBy.getFields()){
                    if (field.getAscending()){
                        criteria.addOrder( Order.asc(field.getName()) );
                    } else {
                        criteria.addOrder( Order.desc(field.getName()) );
                    }
                }
            }
            
            list = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding the entities by example");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getPurchaseScenarioNames() {
		List<String> list =  null;

		Session session = createLocalSession();
		try { 
			
			Criteria criteria = session.createCriteria(PurchaseScenario.class);
			criteria.setProjection(Projections.distinct(Projections.property("purchscenarioName")));
			
            list = criteria.list();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		finally{
			closeLocalSession(session);
			session=null;
		}
		
		return list;
	}

	@Override
	public int deleteByNamePurchase(String name) {
		int count = 0;
		Session session = createLocalSession();
		Transaction tx = session.beginTransaction();
		try { 
			count = session.createQuery("delete PurchaseScenario where purchscenarioName = :scenarioName").setString("scenarioName", name).executeUpdate();
		} catch (HibernateException hbme) {
			hbme.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in finding scenario names");
		}
		finally{
			tx.commit();

			closeLocalSession(session);
			session=null;
		}
		
		return count;
	}
	
	@Transactional(readOnly = false)
	public void savePurchaseScenario(PurchaseScenario instance) {
		entityManager.persist(instance);
	}

	@Override
	public List<PurchaseScenario> findByExample(PurchaseScenario exampleInstance, int count,
			String... excludeProperty) {
		return findByExampleSorted(exampleInstance, count, exampleInstance.getIdPropertyName(), true, excludeProperty);
	}

	@Override
	public List<PurchaseScenario> findByExampleSorted(PurchaseScenario exampleInstance, int count, String sortField,
			boolean ascending, String... excludeProperty) {
		// TODO Auto-generated method stub
		List<PurchaseScenario> list = null;
        
        try {
        	Session session = createLocalSession();
			Criteria criteria = session.createCriteria(PurchaseScenario.class);
        	
            if (count > 0) {
            	criteria.setMaxResults(count);
            }
            
            Example example = Example.create(exampleInstance);
            example.ignoreCase();
            if (excludeProperty != null) {
	            for (String exclude : excludeProperty) {
	                example.excludeProperty(exclude);
	            }
            }
            
            // SPECIAL PROCESSING FOR RYAN BUSINESS LOGIC
            ryanExampleHandling(criteria, example, exampleInstance);
            
            criteria.add(example);
            
            // Return results using predictable ordering
           	criteria.addOrder((ascending)? Order.asc(sortField):Order.desc(sortField));
            
            list = criteria.list();
            
            closeLocalSession(session);
            session=null;
            
        } catch (HibernateException hbme) {
        	hbme.printStackTrace();
            logger.error("Error in finding the entities by example");
        }
        
        return list; 
	}
	
    protected void ryanExampleHandling(Criteria criteria, Example example, PurchaseScenario exampleInstance) {
    	// Iterate the properties
    	logger.info("---- JPAGenericDAO.ryanExampleHandling enter -----");
    	for (Method m : PurchaseScenario.class.getMethods()) {
    		// Look for all string getters
    		if (m.getName().startsWith("get") && 
    			m.getReturnType().equals(String.class) &&
    			(m.getParameterTypes().length == 0)) {
				try {
	    			
					String value = (String) m.invoke(exampleInstance);
	    			if (value != null) {
	    				if (value.equalsIgnoreCase("*NULL") || value.equalsIgnoreCase("*ALL") || value.contains("%")) {
		    				String field = Util.lowerCaseFirst(m.getName().substring(3));
		    				logger.debug("Special handling for field: " + field + " - " + value);
		    				// Exclude from the example, since we are handling it
		    				example.excludeProperty(field);
		    				
		    				if (value.equalsIgnoreCase("*ALL")) {
		    					// Nothing more to do than exclude it
		    				} else if (value.equalsIgnoreCase("*NULL")) {
		    					criteria.add(Restrictions.isNull(field));
		    				} else {
		    					//0001650: Wildcards with What if		    				
		    					boolean startWild = value.startsWith("%");
		    					boolean endWild = value.endsWith("%");
		    					//String subValue = value;
		    					if(startWild && endWild){ //Like %12345% or %123%45%
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.ANYWHERE));
		    					}
		    					else if(startWild && !endWild){ //Like $34567 or $34&567
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.END));
		    					}
		    					else if(!startWild && endWild && (value.length() >= 2)){ //Like 12345% or 12%345%
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		    					}
		    					else{ //Like 12%345 or else
		    						criteria.add(Restrictions.ilike(field, value.replaceAll("%", ""), MatchMode.START));
		    					}
		    				}
	    				}
	    				
	    				if (m.getName().endsWith("Flag") && value.equals("0")) {
		    				String field = Util.lowerCaseFirst(m.getName().substring(3));
		    				logger.debug("Special handling for flag: " + field + " - " + value);
		    				// Exclude from the example, since we are handling it
		    				example.excludeProperty(field);
	    					criteria.add(Restrictions.or(
	    							Restrictions.isNull(field),
	    							Restrictions.eq(field, value)));
	    				}
	    			}
	    			
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
    		}
    	}
    	logger.info("---- JPAGenericDAO.ryanExampleHandling exit -----");
    }

}