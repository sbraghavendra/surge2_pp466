package com.ncsts.services;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;

public interface TaxMatrixService extends MatrixService<TaxMatrix> {
	public List<TaxMatrix> getDetailRecords(TaxMatrix taxMatrix, OrderBy orderBy,String moduleCode);
	public List<TaxMatrix> getMatchingTaxMatrixLines(PurchaseTransaction exampleInstance);
}
