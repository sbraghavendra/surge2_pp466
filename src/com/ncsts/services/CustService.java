package com.ncsts.services;

import java.util.Hashtable;
import java.util.List;

import com.ncsts.common.helper.OrderBy;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.dto.ErrorLogDTO;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public interface CustService extends GenericService<Cust, Long> {

	public void deleteCustEntityList(Cust updateCust) throws DataAccessException;
	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException;
	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException;
	public Cust getCustByNumber(String custNbr) throws DataAccessException;
	public List<Cust> findAllCust(Cust exampleInstance) throws DataAccessException;
	public boolean isAllowDeleteCustomer(String custNbr) throws DataAccessException;
	public void disableActiveCustomer(String custNbr) throws DataAccessException;
}
