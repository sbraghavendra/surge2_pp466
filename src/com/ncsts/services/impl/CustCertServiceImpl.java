package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.common.helper.OrderBy;

import com.ncsts.dao.CustCertDAO;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.services.CustCertService;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public class CustCertServiceImpl extends GenericServiceImpl<CustCertDAO, CustCert, Long> implements CustCertService {
	
	public void addCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException{
		getDAO().addCustLocnEx(updateCustLocnEx);
	}
	public void updateCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException{
		getDAO().updateCustLocnEx(updateCustLocnEx);
	}
	public void deleteCustLocnEx(CustLocnEx updateCustLocnEx) throws DataAccessException{
		getDAO().deleteCustLocnEx(updateCustLocnEx);	
	}
	
	public void addCustCert(CustCert updateCustCert) throws DataAccessException{
		getDAO().addCustCert(updateCustCert);
	}
	public void updateCustCert(CustCert updateCustCert) throws DataAccessException{
		getDAO().updateCustCert(updateCustCert);
	}
	public void deleteCustCert(CustCert updateCustCert) throws DataAccessException{
		getDAO().deleteCustCert(updateCustCert);	
	}
	
	public CustLocnEx findCustLocnEx(Long custLocnExId) throws DataAccessException {
		return getDAO().findCustLocnEx(custLocnExId);	
	}
	
	public void addCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException {
		getDAO().addCustCertExemp(updateCustCert, custLocnEx);	
	}
	
	public void updateCustCertExemp(CustCert updateCustCert, CustLocnEx updateCustLocnEx) throws DataAccessException {
		getDAO().updateCustCertExemp(updateCustCert, updateCustLocnEx);	
	}
	
	public void validateCustCertExemp(CustCert updateCustCert, CustLocnEx custLocnEx) throws DataAccessException {
		getDAO().validateCustCertExemp(updateCustCert, custLocnEx);	
	}
	
	public boolean validateUniqueCertificate(CustCert updateCustCert) throws DataAccessException {
		return getDAO().validateUniqueCertificate(updateCustCert);	
	}
	
	public boolean validateUniqueExemption(CustCert aCustCert) throws DataAccessException {
		return getDAO().validateUniqueExemption(aCustCert);	
	}

//	public void deleteCustEntityList(Cust updateCust) throws DataAccessException {
//		getDAO().deleteCustEntityList(updateCust);
//	}
	
//	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException  {
//		getDAO().addCustEntityList(updateCust, updateCustEntityList);
//	}
	
//	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException  {
//		getDAO().updateCustEntityList(updateCust, updateCustEntityList);
//	}
	
//	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException {
//		return getDAO().findCustEntity(custId);
//	}
	
//	public Cust getCustByNumber(String custNbr) throws DataAccessException {
//		return getDAO().getCustByNumber(custNbr);
//	}
	
}
