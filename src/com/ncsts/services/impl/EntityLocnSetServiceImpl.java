package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.EntityLocnSetDAO;
import com.ncsts.services.EntityLocnSetService;
import com.ncsts.domain.EntityLocnSet;


public class EntityLocnSetServiceImpl extends GenericServiceImpl<EntityLocnSetDAO, EntityLocnSet, Long> implements EntityLocnSetService {
	
    public Long count(Long entityId) throws DataAccessException{
    	 return getDAO().count(entityId);
    }
    
    public List<EntityLocnSet> findAllEntityLocnSets() throws DataAccessException {
    	return getDAO().findAllEntityLocnSets();
    }
    
    public List<EntityLocnSet> findAllEntityLocnSetsByEntityId(Long entityId) throws DataAccessException{
    	return getDAO().findAllEntityLocnSetsByEntityId(entityId);
    }
    
    public Long persist(EntityLocnSet entityLocnSet)throws DataAccessException {
    	return getDAO().persist(entityLocnSet);
    }
    
    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate(EntityLocnSet entityLocnSet) throws DataAccessException{
    	getDAO().saveOrUpdate(entityLocnSet);
    }
    
    public void deleteEntity(Long entityId) throws DataAccessException {
    	getDAO().deleteEntity(entityId);
    }
    public EntityLocnSet getEntityLocationforView(Long transId) throws DataAccessException{
    	return getDAO().getEntityLocationforView(transId);
    }
}


