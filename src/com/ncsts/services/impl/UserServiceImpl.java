package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.UserDAO;
import com.ncsts.domain.User;
import com.ncsts.services.UserService;

/**
 * @author Paul Govindan
 *
 */

public class UserServiceImpl implements UserService {
	
	private UserDAO userDAO;	
	
	public void setUserDAO (UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	public UserDAO getUserDAO() {
		return this.userDAO;
	}
	
	public boolean validateUser(String username, String password) {
		if (username == null || "".equalsIgnoreCase(username)) return false;
		if (password == null || "".equalsIgnoreCase(password)) return false;		
		User user = userDAO.findById(password);
		if (user != null && username.equalsIgnoreCase(user.getUserDTO().getUserName())) return true;
		return false;
	}
	
	public User findById(String userCode) {
		return userDAO.findById(userCode);
	}
	
    public List<User> findAllUsers() throws DataAccessException {
    	return userDAO.findAllUsers();
    }
    
	public void remove(String id) throws DataAccessException {
		userDAO.remove(id);
	}
    
	public void saveOrUpdate (User user) throws DataAccessException {
		userDAO.saveOrUpdate(user);
	}
    
	public String persist (User user) throws DataAccessException {
		return userDAO.persist(user);
	}    
	
	public void updateUserLogoff(String userCode) throws DataAccessException {
		userDAO.updateUserLogoff(userCode);
	}
	
	public String getUserLogoff(String userCode) throws DataAccessException {
		return userDAO.getUserLogoff(userCode);
	}
}

