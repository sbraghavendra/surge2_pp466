package com.ncsts.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.VendorNexusDetailDAO;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.services.VendorNexusDetailService;

public class VendorNexusDetailServiceImpl extends GenericServiceImpl<VendorNexusDetailDAO, VendorNexusDtl, Long> implements VendorNexusDetailService {

	@Override
	public List<VendorNexusDtl> findAllVendorNexusDetails(Long entityId, 
			String countryCode, String stateCode, String county, String city,
			String stj, boolean activeFlag) throws DataAccessException {
		return getDAO().findAllVendorNexusDetails(entityId, countryCode, stateCode, county, city, stj, activeFlag);
	}

	@Override
	public void bulkExpireVendorNexusDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		getDAO().bulkExpireVendorNexusDetail(countryCode, stateCode, expirationDate);
	}
	
	@Override
	public void bulkExpireRegistrationDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		getDAO().bulkExpireRegistrationDetail(countryCode, stateCode, expirationDate);
	}
	
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException {
		return getDAO().isRegistrationDetailExist(nexusDefId, nexusTypeCode, effectiveDate);
	}
	
	public boolean isVendorNexusExist(Long entityId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException{
		return getDAO().isVendorNexusExist(entityId, countryCode, stateCode, county, city, stj, effectiveDate, type);
	}
	
	public boolean isVendorNexusActive(Long entityId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException {
		return getDAO().isVendorNexusActive(entityId, countryCode, stateCode, county, city, stj);
	}
	
	public List<VendorNexusDtl> getVendorNexusDtlList(Long vendorId, Date glDate, String whereClause, String orderBy) throws DataAccessException  {
		return getDAO().getVendorNexusDtlList(vendorId, glDate, whereClause, orderBy);
	}
	
	public List<NexusDefinitionDetail> getEntityNexusDtlList(Long entityId, Date glDate, String whereClause, String orderBy) throws DataAccessException {
		return getDAO().getEntityNexusDtlList(entityId, glDate, whereClause, orderBy);
	}
}
