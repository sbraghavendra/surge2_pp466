package com.ncsts.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.TaxCodeDetailDAO;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.services.TaxCodeDetailService;

public class TaxCodeDetailServiceImpl extends GenericServiceImpl<TaxCodeDetailDAO, TaxCodeDetail, Long> implements TaxCodeDetailService {

	public List<TaxCodeDetail> findTaxCodeCode(String stateCode, String taxCodeType) {
		return getDAO().findTaxCodeCode(stateCode, taxCodeType);
	}

	public List<TaxCodeDetail> findTaxCodeDetailList(String stateCoountry, String stateCode,
			String taxCodeType, String taxcodeCode) {
		return getDAO().findTaxCodeDetailList(stateCoountry, stateCode, taxCodeType, taxcodeCode);
	}
	
	public List<TaxCodeDetail> findTaxCodeDetailListByTaxCode(String taxcodeCode) {
		return getDAO().findTaxCodeDetailListByTaxCode(taxcodeCode);
	}
	
	public List<TaxCode> findTaxCodeListByTaxCode(String taxcodeCode) {
		return getDAO().findTaxCodeListByTaxCode(taxcodeCode);
	}
	
	public List<String> findTaxCodeTypeForState(String stateCode) {
		return getDAO().findTaxCodeTypeForState(stateCode);
	}
	
    public List<String> findTaxCodeCounty(String countryCode, String stateCode){
    	return getDAO().findTaxCodeCounty(countryCode, stateCode);
    }
    public List<String> findTaxCodeCity(String countryCode, String stateCode){
    	return getDAO().findTaxCodeCity(countryCode, stateCode);
    }
    public List<String> findTaxCodeStj(String countryCode, String stateCode){
    	return getDAO().findTaxCodeStj(countryCode, stateCode);
    }
    
    public List<TaxCodeDetail> findTaxCodeDetailListByCountrCity(String countryCode, String stateCode, String taxcodeCode, 
    		String taxcodeCounty, String taxcodeCity, String description){
    	return getDAO().findTaxCodeDetailListByCountrCity(countryCode, stateCode, taxcodeCode, 
        		taxcodeCounty, taxcodeCity, description);
    }

	public List<String[]> findTaxCodes() {
		return getDAO().findTaxCodes();
	}

	public TaxCode findTaxCode(TaxCodePK id) {
		return getDAO().findTaxCode(id);
	}

	public List<TaxCode> findTaxCodeList(String stateCode, String taxCodeType, String pattern) {
		return getDAO().findTaxCodeList(stateCode, taxCodeType, pattern);
	}
	
	public List<TaxCode> findTaxCodeListByCountry(String countryCode, String stateCode, String taxCodeType, String pattern) {
		return getDAO().findTaxCodeListByCountry(countryCode, stateCode, taxCodeType, pattern);
	}

	public List<TaxCode> findByTaxCodeCode(String taxCodeTypeCode) {
		return getDAO().findByTaxCodeCode(taxCodeTypeCode);
	}

	public List<TaxCode> findTaxCodeListByStateCode(String stateCode, String taxCodeType) {
		return getDAO().findTaxCodeListByStateCode(stateCode, taxCodeType);
	}

	public List<TaxCode> getAllTaxCode() {
		return getDAO().getAllTaxCode();
	}

    public void removeTaxCode(TaxCode taxCode) throws IllegalAccessException {
    	getDAO().removeTaxCode(taxCode);
    }
 
    public void updateBackdate(TaxCodeDetail taxCodeDetail) throws DataAccessException {
    	getDAO().updateBackdate(taxCodeDetail);
    }
    
    public void updateDetail(TaxCodeDetail taxCodeDetail) throws DataAccessException {
    	getDAO().updateDetail(taxCodeDetail);
    }
    
    public boolean isTaxCodeUsed(TaxCode taxCode) throws DataAccessException{
    	return getDAO().isTaxCodeUsed(taxCode);
    }
    public void updateDetail(TaxCode taxCode, Set<String> states, Map<String,Long> stateJurisdictionMap) {
    	getDAO().updateDetail(taxCode, states, stateJurisdictionMap);
    }

	public List<ListCodes> findListCodeForTaxCode(String taxCode) {
		return getDAO().findListCodeForTaxCode(taxCode);
	}

	public List<ListCodes> findListCodeForState(String stateCode) {
		return getDAO().findListCodeForState(stateCode);
	}
	
	public void update(TaxCodeDetail taxCodeDetail)throws DataAccessException {
		getDAO().update(taxCodeDetail);
	}

	public List<ReferenceDetail> getDocumentReferences(String taxCodeType, String taxCode, String taxStateCode) {
		return getDAO().getDocumentReferences(taxCodeType, taxCode, taxStateCode);
	}

	public List<ReferenceDocument> getAllDocumentReferences() {
		return getDAO().getAllDocumentReferences();
	}

        public List<ReferenceDocument> getDocumentReferencesByTaxCodeId(Long id) {
                return getDAO().getDocumentReferencesByTaxCodeId(id);
        }

    public List<ReferenceDocument> getDocumentReferencesByTypeAndCode(String refType, String code) {
    	return getDAO().getDocumentReferencesByTypeAndCode(refType, code);
    }

    public void addDocumentReference(ReferenceDetail detail) {
    	getDAO().addDocumentReference(detail);
    }

    public void removeDocumentReference(ReferenceDetail detail) {
    	getDAO().removeDocumentReference(detail);
    }

	public List<CCHCode> getAllCCHCode() {
		return getDAO().getAllCCHCode();
	}

	public List<CCHTaxMatrix> getAllCCHGroup() {
		return getDAO().getAllCCHGroup();
	}

	public List<CCHTaxMatrix> getAllCCHGroupItems(String groupCode) {
		return getDAO().getAllCCHGroupItems(groupCode);
	}

/*	public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId){
		List<TaxCodeDetailDTO> taxCodeDetailDTO= new ArrayList<TaxCodeDetailDTO>();
		long begintime = System.currentTimeMillis();
		List<Object> olist = getDAO().getTaxcodeDetails(taxCodeState, taxCodeType, taxCode, description, detailId);
		for(Object obj : olist){
			Object[] objarray= (Object[])obj;
			TaxCodeDetailDTO taxCodeDetail = new TaxCodeDetailDTO();
			taxCodeDetail.setTaxcodeCode((String)objarray[0]);
			taxCodeDetail.setName((String)objarray[1]);
			taxCodeDetail.setTaxcodeTypeDesc((String)objarray[4]);
			taxCodeDetail.setDescription((String)objarray[5]);
			taxCodeDetail.setComments((String)objarray[6]);
			taxCodeDetail.setTaxtypeCode((String)objarray[7]);
			taxCodeDetail.setErpTaxcode((String)objarray[8]);
			taxCodeDetail.setTaxtypeDesc((String)objarray[9]);
			BigDecimal taxcodeDetailId = (BigDecimal)objarray[10];
			if(taxcodeDetailId!= null)taxCodeDetail.setTaxcodeDetailId(taxcodeDetailId.longValue());
			Jurisdiction jurisdiction = new Jurisdiction();
			BigDecimal jurisdictionId = (BigDecimal)objarray[11];
			if(jurisdictionId!= null)jurisdiction.setJurisdictionId(jurisdictionId.longValue());
			jurisdiction.setGeocode((String)objarray[12]);
			jurisdiction.setCity((String)objarray[13]);
			jurisdiction.setCounty((String)objarray[14]);
			jurisdiction.setState((String)objarray[15]);
			jurisdiction.setZip((String)objarray[16]);
			taxCodeDetail.setJurisdiction(jurisdiction);

			taxCodeDetailDTO.add(taxCodeDetail);
		}
		String time = Long.toString(System.currentTimeMillis()
						- begintime);
				System.out.println("\t *** Executing Store procedure, took "
						+ time + " milliseconds. \n");
		return taxCodeDetailDTO;
	}*/
	
	public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId){
		return getDAO().getTaxcodeDetails(taxCodeState, taxCodeType, taxCode, description, detailId);
	}
	
	public List<TaxCodeDetailDTO> getTaxcodeDetailsByCountry(String taxCodeCountry, String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId){
		return getDAO().getTaxcodeDetailsByCountry(taxCodeCountry, taxCodeState, taxCodeType, taxCode, description, detailId);
	}
	
	public List<TaxCodeDetailDTO> getTaxcodeDetailsJurisList(String taxcodeCode, String taxCodeCountry, String taxCodeState, String taxCodeCounty, String taxCodeCity, String taxCodeStj) throws DataAccessException{
		return getDAO().getTaxcodeDetailsJurisList(taxcodeCode, taxCodeCountry, taxCodeState, taxCodeCounty, taxCodeCity, taxCodeStj);
	}

	@Override
	public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxcodeCode, String taxCodeCountry,
			String taxCodeState, String taxCodeCounty, String taxCodeCity, boolean showLocalTaxability, boolean activeFlag, boolean definedFlag) {
		
		return getDAO().getTaxcodeDetails(taxcodeCode, taxCodeCountry, taxCodeState, taxCodeCounty, taxCodeCity, showLocalTaxability, activeFlag, definedFlag);
	}

	@Override
	public List<TaxCodeDetailDTO> getTaxcodeRules(String taxcodeCode,
			String taxcodeCountryCode, String taxcodeStateCode,
			String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel) {
		return getDAO().getTaxcodeRules(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, taxcodeStj, jurLevel);
	}
	
	public long getRulesusedCount(Long ruleId){
		return getDAO().getRulesusedCount(ruleId);
	}

	@Override
	public long getTaxCodeRulesCount(String taxcodeCode,
			String taxcodeCountryCode, String taxcodeStateCode,
			String taxcodeCounty, String taxcodeCity, String stj, Date effectiveDate, String jurLevel) {
		return getDAO().getTaxCodeRulesCount(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, stj, effectiveDate, jurLevel);
	}
	
		public long getTaxCodeJurisdInfoCount(String taxcodeCode,
			String taxcodeCountryCode, String taxcodeStateCode,
			String taxcodeCounty, String taxcodeCity, String stj,String jurLevel) {
		return getDAO().getTaxCodeJurisdInfoCount(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, stj,jurLevel);
	}
	
	public long getAllocBucketCount(Long taxcodeDetailId, String allocBucket) {
		return getDAO().getAllocBucketCount(taxcodeDetailId, allocBucket);
	}
	
	public Date minEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel){
		return getDAO().minEffectiveDate(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, taxcodeStj, jurLevel);
	}
	
	public Long matrixEffectiveDateCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel, Date backEffectiveDate){
		return getDAO().matrixEffectiveDateCount(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, taxcodeStj, jurLevel, backEffectiveDate);
	}

	@Override
	public List<ReferenceDetail> getReferenceDetails(
			Long taxcodeDetailId) {
		return getDAO().getReferenceDetails(taxcodeDetailId);
	}
	
	@Override
	public Date getRuleEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String transInd, String suspendInd) {
		return getDAO().getRuleEffectiveDate(taxcodeCode, taxcodeCountryCode, taxcodeStateCode, taxcodeCounty, taxcodeCity, transInd, suspendInd);
	}
	
	@Override
	public Long[] addRulesFromTransaction(TaxCodeDetailDTO stateRule, TaxCodeDetailDTO countyRule, TaxCodeDetailDTO cityRule) throws Exception {
		return getDAO().addRulesFromTransaction(stateRule, countyRule, cityRule);
		//getDAO().processTransactions(ruleIds);
	}

}
