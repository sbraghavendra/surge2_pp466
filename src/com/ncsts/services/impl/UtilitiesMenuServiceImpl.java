package com.ncsts.services.impl;

/**
 * @author AChen
 * 
 */

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.UtilitiesMenuDAO;
import com.ncsts.services.UtilitiesMenuService;

public class UtilitiesMenuServiceImpl implements UtilitiesMenuService {

	private Logger logger = LoggerFactory.getInstance().getLogger(UtilitiesMenuServiceImpl.class);
	
	private UtilitiesMenuDAO utilitiesMenuDAO;

    public UtilitiesMenuDAO getUtilitiesMenuDAO() {
		return utilitiesMenuDAO;
	}

	public void setUtilitiesMenuDAO(UtilitiesMenuDAO utilitiesMenuDAO) {
		this.utilitiesMenuDAO = utilitiesMenuDAO;
	}
	
	public String runMasterControlProgram() throws DataAccessException {
		return utilitiesMenuDAO.runMasterControlProgram();
	}
	
	public String resumeBusService(Long jobNum, boolean resume) throws DataAccessException {
		return utilitiesMenuDAO.resumeBusService(jobNum, resume);
	}
	
	public String createBusService() throws DataAccessException {
		return utilitiesMenuDAO.createBusService();
	}
	
	public Map<String, String> findBusProperty() throws DataAccessException {
		return utilitiesMenuDAO.findBusProperty();
	}
	
	public String resetDatawindowSyntax(String userCode) throws DataAccessException {
		return utilitiesMenuDAO.resetDatawindowSyntax(userCode);
	}
	
	public String buildIndices(String indexParam) throws DataAccessException {
		return utilitiesMenuDAO.buildIndices(indexParam);
	}	
	
	public String reweightMatrices() throws DataAccessException {
		return utilitiesMenuDAO.reweightMatrices();
	}
	
	public String resetAllSequences() throws DataAccessException {
		return utilitiesMenuDAO.resetAllSequences();
	}
	
	public String rebuildDriverReferences() throws DataAccessException {
		return utilitiesMenuDAO.rebuildDriverReferences();
	}
	
	public String truncateBCPTables() throws DataAccessException {
		return utilitiesMenuDAO.truncateBCPTables();
	}
}

