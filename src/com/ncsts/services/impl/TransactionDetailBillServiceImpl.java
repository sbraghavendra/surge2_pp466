package com.ncsts.services.impl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ncsts.dao.SaleTransactionExDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailBillDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.services.TransactionDetailBillService;
import com.ncsts.domain.BillTransaction;
import com.seconddecimal.billing.domain.SaleTransactionLog;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;

public class TransactionDetailBillServiceImpl extends GenericServiceImpl<TransactionDetailBillDAO, BillTransaction, Long> implements TransactionDetailBillService {
	
	private Logger logger = Logger.getLogger(TransactionDetailBillService.class);

	public Long count(BillTransaction exampleInstance) {
		return getDAO().count(exampleInstance);
	}
	
	public List<BillTransaction> getAllRecords(BillTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults){
		return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
	}
	
	public HashMap<String, HashMap<String, String>> getNexusInfo(Long billTransactionId) {
		return getDAO().getNexusInfo(billTransactionId);
	}
	
	public HashMap<String, String> getTaxRateInfo(Long billtransId) {
		return getDAO().getTaxRateInfo(billtransId);
	}

}
