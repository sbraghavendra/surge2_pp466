package com.ncsts.services.impl;
import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.GSBMatrixDAO;
import com.ncsts.domain.GSBMatrix;
import com.ncsts.services.GSBMatrixService;


	public class GSBMatrixServiceImpl extends MatrixServiceImpl<GSBMatrixDAO, GSBMatrix> implements GSBMatrixService {
		
		public List<GSBMatrix> getDetailRecords(GSBMatrix taxMatrix, OrderBy orderBy,String modulecode){
			return getDAO().getDetailRecords(taxMatrix, orderBy,modulecode);
		}
		
		public void save(GSBMatrix taxMatrix) {
			getDAO().save(taxMatrix);
		}
	}

