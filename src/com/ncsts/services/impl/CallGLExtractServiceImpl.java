package com.ncsts.services.impl;


import java.math.BigDecimal;
import java.util.Date;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.CallGLExtractDao;
import com.ncsts.services.CallGLExtractService;

public class CallGLExtractServiceImpl implements CallGLExtractService {
	
    private CallGLExtractDao callGLExtractDao;

	public CallGLExtractDao getCallGLExtractDao() {
		return callGLExtractDao;
	}
	public void setCallGLExtractDao(CallGLExtractDao callGLExtractDao) {
		this.callGLExtractDao = callGLExtractDao;
	}
	public String callGlExtractProc(String where_clause, String file_name,
			int execution_mode, int return_code) {
		return callGLExtractDao.callGlExtractProc(where_clause, file_name, execution_mode, return_code);
	}
	
	public BigDecimal getTransactionCount(String GLdate) throws DataAccessException {
	
		return callGLExtractDao.getTransactionCount(GLdate);
	}
	 public BigDecimal getRecordCount(String GLdate) throws DataAccessException{
		 return callGLExtractDao.getRecordCount(GLdate);
	 }
	
	public BigDecimal getTaxAmountCount(String GLdate) throws DataAccessException {
		 return callGLExtractDao.getTaxAmountCount(GLdate);
	}
}
