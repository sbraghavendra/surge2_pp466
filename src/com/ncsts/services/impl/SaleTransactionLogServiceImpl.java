package com.ncsts.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.beanutils.BeanUtils;

import com.ncsts.dao.SaleTransactionLogDAO;
import com.ncsts.services.SaleTransactionLogService;
import com.seconddecimal.billing.domain.SaleTransaction;
import com.seconddecimal.billing.domain.SaleTransactionLog;



public class SaleTransactionLogServiceImpl implements SaleTransactionLogService {

	private SaleTransactionLogDAO saleTransacitonLogDAO;
	
	
	public SaleTransactionLogDAO getDAO() {
		return saleTransacitonLogDAO;
	}
	public void setDAO(SaleTransactionLogDAO dao) {
		this.saleTransacitonLogDAO = dao;
	}
	public void save(SaleTransactionLog saleTransactionLog) {
		saleTransacitonLogDAO.save(saleTransactionLog);
	}
	
}
