package com.ncsts.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.NexusDefinitionDetailDAO;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.services.NexusDefinitionDetailService;

public class NexusDefinitionDetailServiceImpl extends GenericServiceImpl<NexusDefinitionDetailDAO, NexusDefinitionDetail, Long> implements NexusDefinitionDetailService {

	@Override
	public List<NexusDefinitionDetail> findAllNexusDefinitionDetails(Long entityId, 
			String countryCode, String stateCode, String county, String city,
			String stj, boolean activeFlag) throws DataAccessException {
		return getDAO().findAllNexusDefinitionDetails(entityId, countryCode, stateCode, county, city, stj, activeFlag);
	}

	@Override
	public void bulkExpireNexusDefinitionDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		getDAO().bulkExpireNexusDefinitionDetail(countryCode, stateCode, expirationDate);
	}
	
	@Override
	public void bulkExpireRegistrationDetail(String countryCode,
			String stateCode, Date expirationDate) throws DataAccessException {
		getDAO().bulkExpireRegistrationDetail(countryCode, stateCode, expirationDate);
	}
	
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException {
		return getDAO().isRegistrationDetailExist(nexusDefId, nexusTypeCode, effectiveDate);
	}
	
	public boolean isNexusDefinitionExist(Long entityId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException{
		return getDAO().isNexusDefinitionExist(entityId, countryCode, stateCode, county, city, stj, effectiveDate, type);
	}
	
	public boolean isNexusDefinitionActive(Long entityId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException {
		return getDAO().isNexusDefinitionActive(entityId, countryCode, stateCode, county, city, stj);
	}
}
