/**
 * 
 */
package com.ncsts.services.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.ListCodesDAO;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.services.ListCodesService;

/**
 * @author Muneer
 *
 */
public class ListCodesServiceImpl implements ListCodesService {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(ListCodesServiceImpl.class);
	
	private ListCodesDAO listCodesDAO;
	
	public ListCodesDAO getListCodesDAO() {
		return listCodesDAO;
	}
	public void setListCodesDAO(ListCodesDAO listCodesDAO) {
		this.listCodesDAO = listCodesDAO;
	}

	public  List<ListCodes> getListCodesByCodeTypeCode(String codeTypeCode){
		return listCodesDAO.getListCodesByCodeTypeCode(codeTypeCode);
	}

	public  List<ListCodesDTO> getListCodesByCodeCode(String codeCode){
		List<ListCodesDTO> listListCodesDTO = new ArrayList<ListCodesDTO>();
		List<ListCodes> listListCodes=listCodesDAO.getListCodesByCodeCode(codeCode);
		if(listListCodes!=null){
			for(ListCodes listCodes : listListCodes)
				listListCodesDTO.add(listCodes.getListCodesDTO());
		}
		
		logger.debug("in Serice Impl ::getListCodesByCodeTypeCodeAsDTO::listListCodesDTO size is :"+listListCodesDTO.size());
		return listListCodesDTO;
	}
	
	public  List<ListCodes> getAllListCodes() throws DataAccessException{
		logger.debug(" its inside ListCodesServiceImpl:::getAllListCodes::");
		return listCodesDAO.getAllListCodes();
	}

	public  List<ListCodesDTO> getListCodesByCodeTypeCodeAsDTO(String codeTypeCode){
		
		List<ListCodesDTO> listListCodesDTO = new ArrayList<ListCodesDTO>();		
		List<ListCodes> listListCodes=listCodesDAO.getListCodesByCodeTypeCode(codeTypeCode);		
		if(listListCodes!=null){
			for(ListCodes listCodes : listListCodes)
				listListCodesDTO.add(listCodes.getListCodesDTO());
		}
		
		logger.debug("in Serice Impl ::getListCodesByCodeTypeCodeAsDTO::listListCodesDTO size is :"+listListCodesDTO.size());
		return listListCodesDTO;
	}
	

	public ListCodes findByPK(ListCodesPK pk) throws DataAccessException{
		ListCodes lc = listCodesDAO.findByPK(pk);
		return lc;
	}
	
	public ListCodesDTO addListCodeRecord(ListCodesDTO instanceDTO) throws DataAccessException{
		
		logger.info("Start point in ListCodesServiceImpl.addListCodeRecord");
				
		ListCodes listCodesObject = getListCodesObject(instanceDTO);
		return (listCodesDAO.addListCodeRecord(listCodesObject)).getListCodesDTO();		
	}
	
	public void update(ListCodesDTO listCodesDTO) throws DataAccessException{
		logger.info(" Start of update for ListCodeServiceImpl.update");
		ListCodes listCodesObject = getListCodesObject(listCodesDTO);
		listCodesDAO.update(listCodesObject);
		logger.info(" End of update for ListCodeServiceImpl.update");
	}
	
	public void delete(ListCodesPK pk) throws DataAccessException{
		listCodesDAO.delete(pk);
	}
	private ListCodes getListCodesObject(ListCodesDTO listCodesDTO) throws DataAccessException{

		ListCodes listCodes= new ListCodes(listCodesDTO.getListCodesPK());
		   BeanUtils.copyProperties(listCodesDTO,listCodes);
		   return listCodes;
	}
	public List<ListCodes> getListCodesByTypeAndCode(String codeTypeCode, String code) throws DataAccessException{
		return listCodesDAO.getListCodesByTypeAndCode(codeTypeCode, code);
	}
	
	public ListCodes getListCode(String codeTypeCode, String codeCode, String description) throws SQLException {
		return listCodesDAO.getListCode(codeTypeCode, codeCode, description);
	}
}
