package com.ncsts.services.impl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ncsts.dao.SaleTransactionExDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailSaleDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.services.SaleTransactionLogService;
import com.ncsts.services.TransactionDetailSaleService;
import com.seconddecimal.billing.domain.SaleTransaction;
import com.seconddecimal.billing.domain.SaleTransactionLog;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;
import com.seconddecimal.billing.service.SaleTransactionProcess;

public class TransactionDetailSaleServiceImpl extends GenericServiceImpl<TransactionDetailSaleDAO, SaleTransaction, Long> implements TransactionDetailSaleService {
	
	private Logger logger = Logger.getLogger(TransactionDetailSaleService.class);
	
	@Autowired
	private SaleTransactionProcess saleTransactionProcess;
	
	@Autowired

	private TransactionDetailSaleDAO saleTransactionDAO;

	@Autowired
	private  SaleTransactionExDAO saleTransactionExDAO;
	
	@Autowired 
	private SaleTransactionLogService saleTransactionLogService;

	public Long count(SaleTransaction exampleInstance) {
		return getDAO().count(exampleInstance);
	}
	
	public List<SaleTransaction> getAllRecords(SaleTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults){
		return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
	}

	@Override
	public boolean suspendAndLog(Long saleTransactionId) {
			
		SaleTransaction transToSuspend = saleTransactionDAO.findById(saleTransactionId);
		if (transToSuspend == null) 
			return false;


		
		try {

			if("CR_ORIG".equals(transToSuspend.getReprocessCode())) {
				//saleTransactionExDAO.cleanUpRelatedTransactions(transToSuspend);
				transToSuspend.setReprocessCode(null);
				transToSuspend.setRelatedSubtransId(null);
			}
			saleTransactionProcess.suspend(transToSuspend);

		} catch (ProcessAbortedException e) {			
			logger.error("Could not suspend SaleTransaction. ID="+saleTransactionId +"\n"+e.getMessage());
			e.printStackTrace();
			return false;
		}
		
		SaleTransactionLog logEntry = transToSuspend.getTransactionLog();
		logEntry.setSaletransId(saleTransactionId);
		logEntry.setLogSource("Sale Transaction Service: Suspend");
		
		saleTransactionLogService.save(logEntry);
		
		return true;
	}

	public HashMap<String, HashMap<String, String>> getNexusInfo(Long saleTransactionId) {
		return saleTransactionDAO.getNexusInfo(saleTransactionId);
	}
}
