package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.FileReaderUtil;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.SQLQuery;
import com.ncsts.dao.BatchDAO;
import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.BatchDTO;
import com.ncsts.services.ImportMapService;

public class ImportMapServiceImpl implements ImportMapService{

	private Logger logger = LoggerFactory.getInstance().getLogger(ImportMapServiceImpl.class);
	
	private BatchDAO batchDAO;
	private ImportDefinitionDAO importDefinitionDAO;
	private TransactionDetailDAO transactionDetailDAO;
	
	private HashMap<String,String>  bcpColumnFileHeaderMap;

	public List<BatchDTO> getImportMapData(String batchTypeCode){
		List<BatchDTO> list = batchDAO.getAllBatchRecords(SQLQuery.IMPORT_MAP_PROCESS_1,batchTypeCode);
		return list;
	}
	
	public List<BatchDTO> getAOLBatchRecords(String entryTimestamp, String statusCode){
		List<BatchDTO> list = batchDAO.getAOLBatchRecords(SQLQuery.IMPORT_MAP_PROCESS_AOL, entryTimestamp, statusCode);
		return list;
	}
	
	public void uploadFiletoServer(){
		
	}
	
	/**
	 * 
	 */
	public List<String> importFileData(String path){
		List<String> errorMssgs=new ArrayList<String>();
		FileReaderUtil fileReader=new FileReaderUtil();
		fileReader.readFile(path, false);
		HashMap<Integer,String>  mapHeader=fileReader.getMapHeader();
		//ArrayList<String> listHeader =(ArrayList<String>)mapHeader.values();
		Collection<String> listHeader =mapHeader.values();
		boolean doesFileMatch=this.doesTheFileMatchWithCurrentDatabase(listHeader);
		//if true , continue with reading rest of file
		if(doesFileMatch)
		{
			fileReader.readFile(path, true);
			List<HashMap<Integer,String>>  listBCPData=fileReader.getListBCPData();
			List<BCPPurchaseTransaction> listBCP=prepareDataForInsert(listBCPData,mapHeader,listHeader.size());
			//insert each row one by one
			for(BCPPurchaseTransaction bcpInstance :listBCP){
				try{
				
						transactionDetailDAO.saveBcpData(bcpInstance);
					
				
					}catch(Exception e){
						e.printStackTrace();
						String error="Error "+e.getMessage()+" for id ";
						errorMssgs.add(error);
			   }	
			}
		}	
		
		return errorMssgs;
	}
	
	
	/**
	 * This method will verify if the header information of the file
	 * has matching records in database(TB_IMPORT_DEFINITION_DETAIL)
	 * It will check for first 10 fields
	 * If not then returns false 
	 * If it matches then we craete the mapping of name from import file with the collumn names
	 * @return
	 */
	private boolean doesTheFileMatchWithCurrentDatabase(Collection<String> headerList){
		boolean doesMatch=false;
		bcpColumnFileHeaderMap=new HashMap<String,String>(); 
		List<ImportDefinitionDetail> list=importDefinitionDAO.getAllRecords();
		
		int counter=0;
		for(ImportDefinitionDetail impDetail: list){
			String colName=impDetail.getImportColumnName();
			//logger.debug("colName from database "+colName);
			if(headerList.contains(colName))
			{
				doesMatch=true;
				bcpColumnFileHeaderMap.put(colName, impDetail.getImportDefinitionDetailPK().getTransDtlColumnName());
			}
			else if(counter>10){
				//break;
			}
			counter++;
			
			
		}
		return doesMatch;
	}
	/**
	 * This will create the domain objects for insertion
	 * listSize is the maximum no of columns this list has
	 * @param mapBCPData
	 * @param mapHeader
	 */
	private List<BCPPurchaseTransaction> prepareDataForInsert(List<HashMap<Integer,String>> listBCPData,HashMap<Integer,String>  mapHeader,int listSize){
		List<BCPPurchaseTransaction> listBCP=new ArrayList<BCPPurchaseTransaction>();
		long tempCounter=0;
		logger.debug(" Lizt size"+listSize +" listBCPData size"+listBCPData.size());
		for(HashMap<Integer,String> map : listBCPData){
			tempCounter++;
			int count =0;		
			BCPPurchaseTransaction bcpTransaction=new BCPPurchaseTransaction();
			//get corresponding column name
			while(count <listSize && tempCounter<listSize){
				String colNameTemp=mapHeader.get(count);
				String colName=bcpColumnFileHeaderMap.get(colNameTemp);
				logger.debug(" COl name  "+colName);				
				String colValue=map.get(count);

				bcpTransaction.setBcpPurchtransId(tempCounter);
				if(colName!=null){				
					if(colName.equals("AFE_CONTRACT_STRUCTURE"))
						bcpTransaction.setAfeContractStructure(colValue);
					else if (colName.equals("AFE_CONTRACT_TYPE"))
						bcpTransaction.setAfeCategoryNbr(colValue);
					else if (colName.equals("AFE_PROJECT_NAME"))
						bcpTransaction.setAfeProjectName(colValue);
					else if (colName.equals("AFE_PROJECT_NBR"))
						bcpTransaction.setPoDate(getDate(colValue));
					else if (colName.equals("ENTERED_DATE"))
						bcpTransaction.setEnteredDate(getDate(colValue));
					else if (colName.equals("GL_CC_NBR_DEPT_ID"))
						bcpTransaction.setGlCcNbrDeptId(colValue);
					else if (colName.equals("GL_CC_NBR_DEPT_NAME"))
						bcpTransaction.setGlCcNbrDeptName(colValue);
					else if (colName.equals("GL_COMPANY_NAME"))
						bcpTransaction.setGlCompanyName(colValue);
					else if (colName.equals("GL_COMPANY_NBR"))
						bcpTransaction.setGlCompanyNbr(colValue);
					else if (colName.equals("GL_DATE"))
						bcpTransaction.setGlDate(getDate(colValue));
					else if (colName.equals("GL_DIVISION_NAME"))
						bcpTransaction.setGlDivisionName(colValue);
					else if (colName.equals("GL_DIVISION_NBR"))
						bcpTransaction.setGlDivisionNbr(colValue);
					else if (colName.equals("GL_LINE_ITM_DIST_AMT"))
						bcpTransaction.setGlDivisionNbr(colValue);
					else if (colName.equals("GL_LOCAL_ACCT_NAME"))
						bcpTransaction.setGlLocalAcctName(colValue);
					else if (colName.equals("GL_LOCAL_ACCT_NBR"))
						bcpTransaction.setGlLocalAcctNbr(colValue);
					else if (colName.equals("INVENTORY_CLASS"))
						bcpTransaction.setInventoryClass(colValue);
					else if (colName.equals("INVENTORY_CLASS_NAME"))
						bcpTransaction.setInventoryClassName(colValue);
					else if (colName.equals("INVENTORY_NAME"))
						bcpTransaction.setInventoryName(colValue);
					else if (colName.equals("INVENTORY_NBR"))
						bcpTransaction.setInventoryNbr(colValue);
					else if (colName.equals("INVOICE_DATE"))
						bcpTransaction.setInvoiceDate(getDate(colValue));
					else if (colName.equals("INVOICE_DESC"))
						bcpTransaction.setInvoiceDesc(colValue);
					else if (colName.equals("INVOICE_LINE_NAME"))
						bcpTransaction.setInvoiceLineName(colValue);
					else if (colName.equals("INVOICE_LINE_NBR"))
						bcpTransaction.setInvoiceLineNbr(colValue);
					else if (colName.equals("INVOICE_LINE_TYPE"))
						bcpTransaction.setInvoiceLineType(colValue);
				
				}
				count++;

			}
			//logger.debug(" Count "+tempCounter);			
			listBCP.add(bcpTransaction);
		}
		
		return listBCP;
		
	}
	
	public ImportDefinitionDAO getImportDefinitionDAO() {
		return importDefinitionDAO;
	}

	public void setImportDefinitionDAO(ImportDefinitionDAO importDefinitionDAO) {
		this.importDefinitionDAO = importDefinitionDAO;
	}

	public BatchDAO getBatchDAO() {
		return batchDAO;
	}

	public void setBatchDAO(BatchDAO batchDAO) {
		this.batchDAO = batchDAO;
	}

	public List<String> saveFileData(List<TransactionDetail> list){
		return null;
	}

	public TransactionDetailDAO getTransactionDetailDAO() {
		return transactionDetailDAO;
	}

	public void setTransactionDetailDAO(TransactionDetailDAO transactionDetailDAO) {
		this.transactionDetailDAO = transactionDetailDAO;
	}
	
	private Date getDate(String dateFormat){
		return new java.util.Date();
	}
}
