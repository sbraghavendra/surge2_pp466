package com.ncsts.services.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.dao.GenericDAO;
import com.ncsts.domain.Idable;
import com.ncsts.services.GenericServiceDTO;

// NOTE:  This class implements both the T based interface as well as the DTO based version

public class GenericServiceDTOImpl<DAO extends GenericDAO<T,ID>, DTO, T extends Idable<ID>, ID extends Serializable>
	extends GenericServiceImpl<DAO, T, ID>
	implements GenericServiceDTO<DTO, T,ID> {

	private Class<DTO> dtoClass;
	private Class<T> objectClass;
	
	@SuppressWarnings("unchecked")
	public GenericServiceDTOImpl() {
        this.dtoClass = (Class<DTO>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
        this.objectClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];
	}
	
	public Long count(DTO exampleInstance, String... excludeProperty) {
		return getDAO().count(dtoToObject(exampleInstance), excludeProperty);
	}

	public DTO findDTOById(ID id) throws DataAccessException {
		return objectToDTO(getDAO().findById(id));
	}
	
	public List<DTO> findDTOById(Collection<ID> ids) {
		return convertList(getDAO().findById(ids));
	}
	
	public List<DTO> findAllDTO() {
		return convertList(getDAO().findAll());
	}
	
	public List<DTO> findAllSortedDTO(String sortField, boolean ascending) {
		return convertList(getDAO().findAllSorted(sortField, ascending));
	}
	
	public List<DTO> findByExample(DTO exampleInstance, int count, String... excludeProperty) {
		return convertList(getDAO().findByExample(dtoToObject(exampleInstance), count, excludeProperty));
	}
	
	public List<DTO> findByExampleSorted(DTO exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty) {
		return convertList(getDAO().findByExampleSorted(dtoToObject(exampleInstance), count, 
				sortField, ascending, excludeProperty));
	}
	
	public void save(DTO instance) {
		getDAO().save(dtoToObject(instance));
	}
	
	public void update(DTO instance) throws DataAccessException {
		getDAO().update(dtoToObject(instance));
	}
	
	public void remove(DTO instance)throws DataAccessException {
		getDAO().remove(dtoToObject(instance));
	}
	
	protected T dtoToObject(DTO dto) {
		T object = null;
		if (dto != null) {
			try {
				object = objectClass.newInstance();
				BeanUtils.copyProperties(dto, object);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return object;
	}
	
	protected DTO objectToDTO(T object) {
		DTO dto = null;
		if (object != null) {
			try {
				dto = dtoClass.newInstance();
				BeanUtils.copyProperties(object, dto);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dto;
	}
	
	protected List<DTO> convertList(List<T> items) {
		List<DTO> result = new ArrayList<DTO>();	
		for (T item : items) {
			result.add(objectToDTO(item));
		}
		return result;
	}
}
