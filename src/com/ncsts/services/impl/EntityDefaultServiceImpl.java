package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.EntityDefaultDAO;
import com.ncsts.services.EntityDefaultService;
import com.ncsts.domain.EntityDefault;


public class EntityDefaultServiceImpl extends GenericServiceImpl<EntityDefaultDAO, EntityDefault, Long> implements EntityDefaultService {
	
    public Long count(Long entityId) throws DataAccessException{
    	 return getDAO().count(entityId);
    }
    
    public List<EntityDefault> findAllEntityDefaults() throws DataAccessException {
    	return getDAO().findAllEntityDefaults();
    }
    
    public List<EntityDefault> findAllEntityDefaultsByEntityId(Long entityId) throws DataAccessException{
    	return getDAO().findAllEntityDefaultsByEntityId(entityId);
    }
    
    public List<EntityDefault> findEntityDefaultsByDefaultCode(Long entityId, String defaultCode) throws DataAccessException {
    	return getDAO().findEntityDefaultsByDefaultCode(entityId, defaultCode);
    }
    
    public Long persist(EntityDefault entityDefault)throws DataAccessException {
    	return getDAO().persist(entityDefault);
    }
    
    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate(EntityDefault entityDefault) throws DataAccessException{
    	getDAO().saveOrUpdate(entityDefault);
    }
    
    public void deleteEntity(Long entityId) throws DataAccessException {
    	getDAO().deleteEntity(entityId);
    }

}


