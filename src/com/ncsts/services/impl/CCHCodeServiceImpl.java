package com.ncsts.services.impl;


import com.ncsts.dao.CCHCodeDAO;
import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHCodePK;
import com.ncsts.services.CCHCodeService;

public class CCHCodeServiceImpl extends GenericServiceImpl<CCHCodeDAO, CCHCode, CCHCodePK> implements CCHCodeService {
}
