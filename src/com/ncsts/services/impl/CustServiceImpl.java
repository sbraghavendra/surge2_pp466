package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.common.helper.OrderBy;

import com.ncsts.dao.CustDAO;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.Cust;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.services.CustService;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public class CustServiceImpl extends GenericServiceImpl<CustDAO, Cust, Long> implements CustService {

	public void deleteCustEntityList(Cust updateCust) throws DataAccessException {
		getDAO().deleteCustEntityList(updateCust);
	}
	
	public void addCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException  {
		getDAO().addCustEntityList(updateCust, updateCustEntityList);
	}
	
	public void updateCustEntityList(Cust updateCust, List<CustEntity> updateCustEntityList) throws DataAccessException  {
		getDAO().updateCustEntityList(updateCust, updateCustEntityList);
	}
	
	public List<CustEntity> findCustEntity(Long custId) throws DataAccessException {
		return getDAO().findCustEntity(custId);
	}
	
	public Cust getCustByNumber(String custNbr) throws DataAccessException {
		return getDAO().getCustByNumber(custNbr);
	}
	
	public List<Cust> findAllCust(Cust exampleInstance) throws DataAccessException {
		return getDAO().findAllCust(exampleInstance);
	}
	
	public boolean isAllowDeleteCustomer(String custNbr) throws DataAccessException {
		return getDAO().isAllowDeleteCustomer(custNbr);
	}
	
	public void disableActiveCustomer(String custNbr) throws DataAccessException{
		getDAO().disableActiveCustomer(custNbr);
	}
	
}
