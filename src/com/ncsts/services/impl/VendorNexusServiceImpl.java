package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.VendorNexusDAO;
import com.ncsts.domain.VendorNexus;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.services.VendorNexusService;
import com.ncsts.view.bean.JurisdictionItem;

public class VendorNexusServiceImpl extends GenericServiceImpl<VendorNexusDAO, VendorNexus, Long> implements VendorNexusService {
	public VendorNexus findOneByExample(VendorNexus exampleInstance) throws DataAccessException{
		return this.getDAO().findOneByExample(exampleInstance);
	}
	
	public boolean removeVendorNexus(List<VendorNexus> vendorNexasArray) throws DataAccessException{
		return this.getDAO().removeVendorNexus(vendorNexasArray);
	}
	
	public boolean addVendorNexus(List<VendorNexus> vendorNexasArray, List<VendorNexusDtl> vendorNexasDetailArray) throws DataAccessException{
		return this.getDAO().addVendorNexus(vendorNexasArray, vendorNexasDetailArray);
	}
	
	public boolean isIndependentCounty(Long entityId, String geocode, String country, String state, String county, String city, String zip, String stj) throws DataAccessException{
		return this.getDAO().isIndependentCounty(entityId, geocode, country, state, county, city, zip, stj);
	}
	
	public boolean isVendorNexusUsed(Long vendorNexusID) throws DataAccessException {
		return getDAO().isVendorNexusUsed(vendorNexusID);
	}
	
	public boolean isVendorNexusDetailUsed(Long vendorNexusDetailID) throws DataAccessException {
		return getDAO().isVendorNexusDetailUsed(vendorNexusDetailID);
	}
}
