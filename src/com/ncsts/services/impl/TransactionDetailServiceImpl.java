package com.ncsts.services.impl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.TransactionDetailService;

public class TransactionDetailServiceImpl extends GenericServiceImpl<TransactionDetailDAO, PurchaseTransaction, Long> implements TransactionDetailService {
	
	public boolean getTaxMatrixInUse(Long taxMatrixId) {
		return getDAO().getTaxMatrixInUse(taxMatrixId);
	}
	
    public ProcessedTransactionDetail getSuspendedTaxMatrixTransactionDetails(Long taxMatrixId, Date asOfDate) {
		return getDAO().getSuspendedTaxMatrixTransactionCounts(taxMatrixId, asOfDate);
    }

    public void suspendTaxMatrixTransactions(PurchaseTransactionService service, Long taxMatrixId, Date asOfDate, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine) {
    	getDAO().suspendTaxMatrixTransactions(service, taxMatrixId, asOfDate, suspendExtracted, suspendNonExtracted, deleteMatrixLine);
    }
    
    public boolean getLocationMatrixInUse(Long locationMatrixId) {
    	return getDAO().getLocationMatrixInUse(locationMatrixId);
    }
    
    public ProcessedTransactionDetail getSuspendedLocationMatrixTransactionDetails(Long locationMatrixId) {
		return getDAO().getSuspendedLocationMatrixTransactionCounts(locationMatrixId);
    }

    public void suspendLocationMatrixTransactions(PurchaseTransactionService service, Long locationMatrixId, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine) {
    	getDAO().suspendLocationMatrixTransactions(service, locationMatrixId, suspendExtracted, suspendNonExtracted, deleteMatrixLine);
    }
    
    public void suspendTransaction(PurchaseTransactionService purchaseTransactionService, Long id, SuspendReason suspendReason) {
    	getDAO().suspendTransaction(purchaseTransactionService, id, suspendReason);
    }
    
    public void acceptHeldTransaction(Long id){
    	getDAO().acceptHeldTransaction(id);
    }
    
    public void appendComment(Long id, String comment) {
    	getDAO().appendComment(id, comment);
    }
    
    public void appendComment(Collection<Long> ids, String comment) {
    	getDAO().appendComment(ids, comment);
    }

    private static LinkedHashMap<String, String> saveFields = new LinkedHashMap<String, String>();
    static {
    	saveFields.put("process_status","processStatus");
    	saveFields.put("PURCHTRANS_ID","purchtransId");
    	saveFields.put("source_transaction_id","sourceTransactionId");
    	saveFields.put("process_batch_no","processBatchNo");
    	saveFields.put("gl_extract_batch_no","glExtractBatchNo");
    	saveFields.put("archive_batch_no","archiveBatchNo");
    	saveFields.put("entered_date","allocationMatrixId");
    	saveFields.put("transaction_status","allocationSubtransId");
    	saveFields.put("gl_date","glDate");
    	saveFields.put("gl_company_nbr","glCompanyNbr");
    	saveFields.put("gl_company_name","glCompanyName");
    	saveFields.put("gl_division_nbr","glDivisionNbr");
    	saveFields.put("gl_division_name","glDivisionName");
    	saveFields.put("gl_cc_nbr_dept_id","glCcNbrDeptId");
    	saveFields.put("gl_cc_nbr_dept_name","glCcNbrDeptName");
    	saveFields.put("gl_local_acct_nbr","glLocalAcctNbr");
    	saveFields.put("gl_local_acct_name","glLocalAcctName");
    	saveFields.put("gl_local_sub_acct_nbr","glLocalSubAcctNbr");
    	saveFields.put("gl_local_sub_acct_name","glLocalSubAcctName");
    	saveFields.put("gl_full_acct_nbr","glFullAcctNbr");
    	saveFields.put("gl_full_acct_name","glFullAcctName");
    	saveFields.put("gl_line_itm_dist_amt","glLineItmDistAmt");
    	saveFields.put("vendor_nbr","vendorNbr");
    	saveFields.put("vendor_name","vendorName");
    	saveFields.put("vendor_address_line_1","vendorAddressLine1");
    	saveFields.put("vendor_address_line_2","vendorAddressLine2");
    	saveFields.put("vendor_address_line_3","vendorAddressLine3");
    	saveFields.put("vendor_address_line_4","vendorAddressLine4");
    	saveFields.put("vendor_address_city","vendorAddressCity");
    	saveFields.put("vendor_address_county","vendorAddressCounty");
    	saveFields.put("vendor_address_state","vendorAddressState");
    	saveFields.put("vendor_address_zip","vendorAddressZip");
    	saveFields.put("vendor_address_country","vendorAddressCountry");
    	saveFields.put("vendor_type","vendorType");
    	saveFields.put("vendor_type_name","vendorTypeName");
    	saveFields.put("invoice_nbr","invoiceNbr");
    	saveFields.put("invoice_desc","invoiceDesc");
    	saveFields.put("invoice_date","invoiceDate");
    	saveFields.put("invoice_freight_amt","invoiceFreightAmt");
    	saveFields.put("invoice_discount_amt","invoiceDiscountAmt");
    	saveFields.put("invoice_tax_amt","invoiceTaxAmt");
    	saveFields.put("invoice_total_amt","invoiceTotalAmt");
    	saveFields.put("invoice_tax_flg","invoiceTaxFlg");
    	saveFields.put("invoice_line_nbr","invoiceLineNbr");
    	saveFields.put("invoice_line_name","invoiceLineName");
    	saveFields.put("invoice_line_type","invoiceLineType");
    	saveFields.put("invoice_line_type_name","invoiceLineTypeName");
    	saveFields.put("invoice_line_amt","invoiceLineAmt");
    	saveFields.put("invoice_line_tax","invoiceLineTax");
    	saveFields.put("afe_project_nbr","afeProjectNbr");
    	saveFields.put("afe_project_name","afeProjectName");
    	saveFields.put("afe_category_nbr","afeCategoryNbr");
    	saveFields.put("afe_category_name","afeCategoryName");
    	saveFields.put("afe_sub_cat_nbr","afeSubCatNbr");
    	saveFields.put("afe_sub_cat_name","afeSubCatName");
    	saveFields.put("afe_use","afeUse");
    	saveFields.put("afe_contract_type","afeContractType");
    	saveFields.put("afe_contract_structure","afeContractStructure");
    	saveFields.put("afe_property_cat","afePropertyCat");
    	saveFields.put("inventory_nbr","inventoryNbr");
    	saveFields.put("inventory_name","inventoryName");
    	saveFields.put("inventory_class","inventoryClass");
    	saveFields.put("inventory_class_name","inventoryClassName");
    	saveFields.put("po_nbr","poNbr");
    	saveFields.put("po_name","poName");
    	saveFields.put("po_date","poDate");
    	saveFields.put("po_line_number","poLineNbr");
    	saveFields.put("po_line_name","poLineName");
    	saveFields.put("po_line_type","poLineType");
    	saveFields.put("po_line_type_name","poLineTypeName");
    	saveFields.put("ship_to_location","shipToLocation");
    	saveFields.put("ship_to_location_name","shipToLocationName");
    	saveFields.put("ship_to_address_line_1","shipToAddressLine1");
    	saveFields.put("ship_to_address_line_2","shipToAddressLine2");
    	saveFields.put("ship_to_address_line_3","shipToAddressLine3");
    	saveFields.put("ship_to_address_line_4","shipToAddressLine4");
    	saveFields.put("ship_to_address_city","shipToAddressCity");
    	saveFields.put("ship_to_address_county","shipToAddressCounty");
    	saveFields.put("ship_to_address_state","shipToAddressState");
    	saveFields.put("ship_to_address_zip","shipToAddressZip");
    	saveFields.put("ship_to_address_country","shipToAddressCountry");
    	saveFields.put("wo_nbr","woNbr");
    	saveFields.put("wo_name","woName");
    	saveFields.put("wo_date","woDate");
    	saveFields.put("wo_type","woType");
    	saveFields.put("wo_type_desc","woTypeDesc");
    	saveFields.put("wo_class","woClass");
    	saveFields.put("wo_class_desc","woClassDesc");
    	saveFields.put("wo_entity","woEntity");
    	saveFields.put("wo_entity_desc","woEntityDesc");
    	saveFields.put("wo_line_nbr","woLineNbr");
    	saveFields.put("wo_line_name","woLineName");
    	saveFields.put("wo_line_type","woLineType");
    	saveFields.put("wo_line_type_desc","woLineTypeDesc");
    	saveFields.put("wo_shut_down_cd","woShutDownCd");
    	saveFields.put("wo_shut_down_cd_desc","woShutDownCdDesc");
    	saveFields.put("voucher_id","voucherId");
    	saveFields.put("voucher_name","voucherName");
    	saveFields.put("voucher_date","voucherDate");
    	saveFields.put("voucher_line_nbr","voucherLineNbr");
    	saveFields.put("voucher_line_desc","voucherLineDesc");
    	saveFields.put("check_nbr","checkNbr");
    	saveFields.put("check_no","checkNo");
    	saveFields.put("check_date","checkDate");
    	saveFields.put("check_amt","checkAmt");
    	saveFields.put("check_desc","checkDesc");
    	saveFields.put("user_text_01","userText01");
    	saveFields.put("user_text_02","userText02");
    	saveFields.put("user_text_03","userText03");
    	saveFields.put("user_text_04","userText04");
    	saveFields.put("user_text_05","userText05");
    	saveFields.put("user_text_06","userText06");
    	saveFields.put("user_text_07","userText07");
    	saveFields.put("user_text_08","userText08");
    	saveFields.put("user_text_09","userText09");
    	saveFields.put("user_text_10","userText10");
    	saveFields.put("user_text_11","userText11");
    	saveFields.put("user_text_12","userText12");
    	saveFields.put("user_text_13","userText13");
    	saveFields.put("user_text_14","userText14");
    	saveFields.put("user_text_15","userText15");
    	saveFields.put("user_text_16","userText16");
    	saveFields.put("user_text_17","userText17");
    	saveFields.put("user_text_18","userText18");
    	saveFields.put("user_text_19","userText19");
    	saveFields.put("user_text_20","userText20");
    	saveFields.put("user_text_21","userText21");
    	saveFields.put("user_text_22","userText22");
    	saveFields.put("user_text_23","userText23");
    	saveFields.put("user_text_24","userText24");
    	saveFields.put("user_text_25","userText25");
    	saveFields.put("user_text_26","userText26");
    	saveFields.put("user_text_27","userText27");
    	saveFields.put("user_text_28","userText28");
    	saveFields.put("user_text_29","userText29");
    	saveFields.put("user_text_30","userText30");
    	saveFields.put("user_text_31","userText31");
    	saveFields.put("user_text_32","userText32");
    	saveFields.put("user_text_33","userText33");
    	saveFields.put("user_text_34","userText34");
    	saveFields.put("user_text_35","userText35");
    	saveFields.put("user_text_36","userText36");
    	saveFields.put("user_text_37","userText37");
    	saveFields.put("user_text_38","userText38");
    	saveFields.put("user_text_39","userText39");
    	saveFields.put("user_text_40","userText40");
    	saveFields.put("user_text_41","userText41");
    	saveFields.put("user_text_42","userText42");
    	saveFields.put("user_text_43","userText43");
    	saveFields.put("user_text_44","userText44");
    	saveFields.put("user_text_45","userText45");
    	saveFields.put("user_text_46","userText46");
    	saveFields.put("user_text_47","userText47");
    	saveFields.put("user_text_48","userText48");
    	saveFields.put("user_text_49","userText49");
    	saveFields.put("user_text_50","userText50");
    	saveFields.put("user_text_51","userText51");
    	saveFields.put("user_text_52","userText52");
    	saveFields.put("user_text_53","userText53");
    	saveFields.put("user_text_54","userText54");
    	saveFields.put("user_text_55","userText55");
    	saveFields.put("user_text_56","userText56");
    	saveFields.put("user_text_57","userText57");
    	saveFields.put("user_text_58","userText58");
    	saveFields.put("user_text_59","userText59");
    	saveFields.put("user_text_60","userText60");
    	saveFields.put("user_text_61","userText61");
    	saveFields.put("user_text_62","userText62");
    	saveFields.put("user_text_63","userText63");
    	saveFields.put("user_text_64","userText64");
    	saveFields.put("user_text_65","userText65");
    	saveFields.put("user_text_66","userText66");
    	saveFields.put("user_text_67","userText67");
    	saveFields.put("user_text_68","userText68");
    	saveFields.put("user_text_69","userText69");
    	saveFields.put("user_text_70","userText70");
    	saveFields.put("user_text_71","userText71");
    	saveFields.put("user_text_72","userText72");
    	saveFields.put("user_text_73","userText73");
    	saveFields.put("user_text_74","userText74");
    	saveFields.put("user_text_75","userText75");
    	saveFields.put("user_text_76","userText76");
    	saveFields.put("user_text_77","userText77");
    	saveFields.put("user_text_78","userText78");
    	saveFields.put("user_text_79","userText79");
    	saveFields.put("user_text_80","userText80");
    	saveFields.put("user_text_81","userText81");
    	saveFields.put("user_text_82","userText82");
    	saveFields.put("user_text_83","userText83");
    	saveFields.put("user_text_84","userText84");
    	saveFields.put("user_text_85","userText85");
    	saveFields.put("user_text_86","userText86");
    	saveFields.put("user_text_87","userText87");
    	saveFields.put("user_text_88","userText88");
    	saveFields.put("user_text_89","userText89");
    	saveFields.put("user_text_90","userText90");
    	saveFields.put("user_text_91","userText91");
    	saveFields.put("user_text_92","userText92");
    	saveFields.put("user_text_93","userText93");
    	saveFields.put("user_text_94","userText94");
    	saveFields.put("user_text_95","userText95");
    	saveFields.put("user_text_96","userText96");
    	saveFields.put("user_text_97","userText97");
    	saveFields.put("user_text_98","userText98");
    	saveFields.put("user_text_99","userText99");
    	saveFields.put("user_text_100","userText100");
    	saveFields.put("user_number_01","userNumber01");
    	saveFields.put("user_number_02","userNumber02");
    	saveFields.put("user_number_03","userNumber03");
    	saveFields.put("user_number_04","userNumber04");
    	saveFields.put("user_number_05","userNumber05");
    	saveFields.put("user_number_06","userNumber06");
    	saveFields.put("user_number_07","userNumber07");
    	saveFields.put("user_number_08","userNumber08");
    	saveFields.put("user_number_09","userNumber09");
    	saveFields.put("user_number_10","userNumber10");
    	saveFields.put("user_number_11","userNumber11");
    	saveFields.put("user_number_12","userNumber12");
    	saveFields.put("user_number_13","userNumber13");
    	saveFields.put("user_number_14","userNumber14");
    	saveFields.put("user_number_15","userNumber15");
    	saveFields.put("user_number_16","userNumber16");
    	saveFields.put("user_number_17","userNumber17");
    	saveFields.put("user_number_18","userNumber18");
    	saveFields.put("user_number_19","userNumber19");
    	saveFields.put("user_number_20","userNumber20");
    	saveFields.put("user_number_21","userNumber21");
    	saveFields.put("user_number_22","userNumber22");
    	saveFields.put("user_number_23","userNumber23");
    	saveFields.put("user_number_24","userNumber24");
    	saveFields.put("user_number_25","userNumber25");
    	saveFields.put("user_date_01","userDate01");
    	saveFields.put("user_date_02","userDate02");
    	saveFields.put("user_date_03","userDate03");
    	saveFields.put("user_date_04","userDate04");
    	saveFields.put("user_date_05","userDate05");
    	saveFields.put("user_date_06","userDate06");
    	saveFields.put("user_date_07","userDate07");
    	saveFields.put("user_date_08","userDate08");
    	saveFields.put("user_date_09","userDate09");
    	saveFields.put("user_date_10","userDate10");
    	saveFields.put("user_date_11","userDate11");
    	saveFields.put("user_date_12","userDate12");
    	saveFields.put("user_date_13","userDate13");
    	saveFields.put("user_date_14","userDate14");
    	saveFields.put("user_date_15","userDate15");
    	saveFields.put("user_date_16","userDate16");
    	saveFields.put("user_date_17","userDate17");
    	saveFields.put("user_date_18","userDate18");
    	saveFields.put("user_date_19","userDate19");
    	saveFields.put("user_date_20","userDate20");
    	saveFields.put("user_date_21","userDate21");
    	saveFields.put("user_date_22","userDate22");
    	saveFields.put("user_date_23","userDate23");
    	saveFields.put("user_date_24","userDate24");
    	saveFields.put("user_date_25","userDate25");
    	saveFields.put("comments","comments");
    	saveFields.put("tb_calc_tax_amt","tbCalcTaxAmt");
    	saveFields.put("state_use_amount","stateUseAmount");
    	saveFields.put("state_use_tier2_amount","stateUseTier2Amount");
    	saveFields.put("state_use_tier3_amount","stateUseTier3Amount");
    	saveFields.put("county_use_amount","countyUseAmount");
    	saveFields.put("county_local_use_amount","countyLocalUseAmount");
    	saveFields.put("city_use_amount","cityUseAmount");
    	saveFields.put("city_local_use_amount","cityLocalUseAmount");
    	saveFields.put("transaction_state_code","transactionStateCode");
    	saveFields.put("auto_transaction_state_code","autoTransactionStateCode");
    	saveFields.put("transaction_ind","transactionInd");
    	saveFields.put("suspend_ind","suspendInd");
//AC    saveFields.put("taxcode_detail_id","taxcodeDetailId");
//AC    saveFields.put("taxcode_state_code","taxcodeStateCode");
//AC    saveFields.put("taxcode_type_code","taxcodeTypeCode");
    	saveFields.put("taxcode_code","taxcodeCode");
    	saveFields.put("manual_taxcode_ind","manualTaxcodeInd");
    	saveFields.put("tax_matrix_id","taxMatrixId");
    	//saveFields.put("location_matrix_id","locationMatrixId");
    	saveFields.put("jurisdiction_id","jurisdictionId");
    	saveFields.put("jurisdiction_taxrate_id","jurisdictionTaxrateId");
    	saveFields.put("manual_jurisdiction_ind","manualJurisdictionInd");
    	
    	saveFields.put("country_use_amount","countryUseAmount");
    	saveFields.put("country_use_rate","countryUseRate");
    	saveFields.put("transaction_country_code","transactionCountryCode");
    	saveFields.put("auto_transaction_country_code","autoTransactionCountryCode");
    	saveFields.put("taxcode_country_code","taxcodeCountryCode");
    	
    	saveFields.put("state_use_rate","stateUseRate");
    	saveFields.put("county_use_rate","countyUseRate");
    	saveFields.put("county_split_amount","countySplitAmount");
    	saveFields.put("county_maxtax_amount","countyMaxtaxAmount");
    	saveFields.put("county_single_flag","countySingleFlag");
    	saveFields.put("county_default_flag","countyDefaultFlag");
    	saveFields.put("city_use_rate","cityUseRate");
    	saveFields.put("city_local_use_rate","cityLocalUseRate");
    	saveFields.put("city_split_amount","citySplitAmount");
    	saveFields.put("city_split_use_rate","citySplitUseRate");
    	saveFields.put("city_single_flag","citySingleFlag");
    	saveFields.put("city_default_flag","cityDefaultFlag");
    	saveFields.put("load_timestamp","loadTimestamp");
    	saveFields.put("gl_extract_flag","glExtractFlag");
    	saveFields.put("gl_extract_amt","glExtractAmt");
    	saveFields.put("gl_extract_updater","glExtractUpdater");
    	saveFields.put("gl_extract_timestamp","glExtractTimestamp");
    	saveFields.put("gl_log_flag","glLogFlag");
    	saveFields.put("audit_flag","auditFlag");
    	saveFields.put("reject_flag","rejectFlag");
    	saveFields.put("audit_user_id","auditUserId");
    	saveFields.put("audit_user_id","auditUserId");
    	saveFields.put("modify_user_id","modifyUserId");
    	saveFields.put("modify_timestamp","modifyTimestamp");
    	saveFields.put("update_timestamp","updateTimestamp");
    	saveFields.put("update_user_id","updateUserId");
    	saveFields.put("comments","comments");
    	saveFields.put("allocation_matrix_id","allocationMatrixId");
    	saveFields.put("allocation_subtrans_id","allocationSubtransId");
//AC    saveFields.put("measure_type_code","measureTypeCode");
    	saveFields.put("state_use_tier2_rate","stateUseTier2Rate");
    	saveFields.put("state_use_tier3_rate","stateUseTier3Rate");
    	saveFields.put("state_split_amount","stateSplitAmount");
    	saveFields.put("state_tier2_min_amount","stateTier2MinAmount");
    	saveFields.put("state_tier2_max_amount","stateTier2MaxAmount");
    	saveFields.put("state_maxtax_amount","stateMaxtaxAmount");
    	saveFields.put("combined_use_rate","combinedUseRate");
    	saveFields.put("audit_timestamp","auditTimestamp");
    	saveFields.put("orig_gl_line_itm_dist_amt","origGlLineItmDistAmt");
    	saveFields.put("entity_code","entityCode");
        };
    
    public StringBuffer saveAs(List<PurchaseTransaction> trList) throws Exception{
		Class<?> cls = PurchaseTransaction.class;
		StringBuffer fos = new StringBuffer();
		String tab = "";
		for (String s : saveFields.keySet()) {
			fos.append(tab + s);
			if ("".equals(tab))
				tab = "\t";
		}

		for (PurchaseTransaction td : trList) {
			fos.append("\n");
			tab = "";
			String transactionInd = td.getTransactionInd();  
			String suspendInd = td.getSuspendInd();
			String glExtractFlag = td.getGlExtractFlag();

			String processStatus;
	        if("P".equalsIgnoreCase(transactionInd)){
	            processStatus = "Processed/";
	            if(glExtractFlag !=null && glExtractFlag.equals("1"))
	            	processStatus = processStatus + "Closed";
	            else
	            	processStatus = processStatus + "OPEN";
	        }else if("S".equalsIgnoreCase(transactionInd)){
	        	processStatus = "Suspended/";
	            if("T".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Tax Matrix";
	            }else if("L".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Location Matrix";
	            }else if("R".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Tax Rate";
	            }else if("?".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "Unknown Suspend Error";
	            }else{
	            	processStatus = processStatus + "Suspend Ind. Error";
	            }
	        }else if("Q".equalsIgnoreCase(transactionInd)){
	        	processStatus = "Question";
	        }else if("H".equalsIgnoreCase(transactionInd)){
	        	processStatus = "Held";
	        }else{
	        	processStatus = "TRANSACTION Ind. Error";
	        }
	        fos.append(processStatus);
			for (String s : saveFields.keySet()) {
				if ("process_status".equals(s))
					continue;
		        fos.append("\t");
				String fieldName = saveFields.get(s);
				String methodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1); 
				Method m = cls.getMethod(methodName, new Class<?> [] {});
				Object o = m.invoke(td, new Object [] {});
				
				if ("comments_flag".equals(s)) {
					// 3943 change from 1 or 0
					fos.append((Boolean)o ? "yes":" ");
				} else {
					if (o instanceof String) {
						String val = (String)o; //for 3943, lets get rid of \r and \ns....
						val = val.replaceAll("\r", "");
						val = val.replaceAll("\n", " ");
						fos.append(val);
					}else if(o instanceof Date){
						Date newdate = (Date)o; 
						fos.append(newdate.toString());
					}else if(o instanceof Number){
						Number number = (Number)o; 
						fos.append(number.toString());
					}else if(o instanceof Character){
						Character ch = (Character)o; 
						fos.append(ch.toString());
					}
				}
			}
		}
		return fos;
	}
    
	public List<TransactionDetailDTO> getTransactionDetailRecordsWithDynamicWhere(String whereClause) {
		List<TransactionDetailDTO> list  = new ArrayList<TransactionDetailDTO>();
		List<Object> olist = getDAO().getTransactionDetailRecordsWithDynamicWhere(whereClause);
		for(Object obj : olist){
			Object[] objarray= (Object[])obj;
			PurchaseTransaction trDetail = new PurchaseTransaction();
			
            BigDecimal trId = (BigDecimal)objarray[0];
            if (trId!=null) trDetail.setPurchtransId(trId.longValue());
            
            
            BigDecimal pbn = (BigDecimal)objarray[2];
            if (pbn!=null) trDetail.setProcessBatchNo(pbn.longValue());
            BigDecimal glebn = (BigDecimal)objarray[3];
            if (glebn!=null) trDetail.setGlExtractBatchNo(glebn.longValue());
            
            trDetail.setEnteredDate((Date) objarray[7]);
            trDetail.setTransactionStatus((String) objarray[8]);
            trDetail.setGlDate((Date) objarray[9]);
            trDetail.setGlCompanyNbr((String) objarray[10]);
            trDetail.setGlCompanyName((String) objarray[11]);
            trDetail.setGlDivisionNbr((String) objarray[12]);
            trDetail.setGlDivisionName((String) objarray[13]);
            trDetail.setGlCcNbrDeptId((String) objarray[14]);
            trDetail.setGlCcNbrDeptName((String) objarray[15]);
            trDetail.setGlLocalAcctNbr((String) objarray[16]);
            trDetail.setGlLocalAcctName((String) objarray[17]);
            trDetail.setGlLocalSubAcctNbr((String) objarray[18]);
            trDetail.setGlLocalSubAcctName((String) objarray[19]);
            trDetail.setGlFullAcctNbr((String) objarray[20]);
            trDetail.setGlFullAcctName((String) objarray[21]);
            
            BigDecimal glAmt = (BigDecimal)objarray[22];
            if (glAmt!=null) trDetail.setGlLineItmDistAmt(glAmt);
  //          trDetail.setVendorNbr((String) objarray[24]);
// rev 17673
            
            /*
            trDetail.setVendorNbr((String) objarray[24]);
//            trDetail.setVendorNbr((String) objarray[24]);
			trDetail.setVendorId((Long) objarray[24]);
            trDetail.setVendorName((String) objarray[25]);
            trDetail.setVendorAddressLine1((String) objarray[26]);
            trDetail.setVendorAddressLine2((String) objarray[27]);
// rev 17673
//            trDetail.setVendorAddressLine3((String) objarray[28]);
//            trDetail.setVendorAddressLine4((String) objarray[29]);
            trDetail.setVendorAddressCity((String) objarray[30]);
            trDetail.setVendorAddressCounty((String) objarray[31]);
            trDetail.setVendorAddressState((String) objarray[32]);
            trDetail.setVendorAddressZip((String) objarray[33]);
            */
            BigDecimal vendorId = (BigDecimal)objarray[24];
            if (vendorId!=null) trDetail.setVendorId(vendorId.longValue());

            trDetail.setVendorCode((String) objarray[25]);
            trDetail.setVendorName((String) objarray[26]);
            trDetail.setVendorAddressLine1((String) objarray[27]);
            trDetail.setVendorAddressLine2((String) objarray[28]);
            trDetail.setVendorAddressCity((String) objarray[29]);
            trDetail.setVendorAddressCounty((String) objarray[30]);
            trDetail.setVendorAddressState((String) objarray[31]);
            trDetail.setVendorAddressZip((String) objarray[32]);
            trDetail.setVendorAddressZipplus4((String) objarray[33]);
                
            trDetail.setVendorAddressCountry((String) objarray[34]);
            trDetail.setVendorType((String) objarray[35]);
            trDetail.setVendorTypeName((String) objarray[36]);
            
            trDetail.setInvoiceNbr((String) objarray[37]);
            trDetail.setInvoiceDesc((String) objarray[38]);
            trDetail.setInvoiceDate((Date) objarray[39]);
            
            BigDecimal invoiceFreightAmt = (BigDecimal)objarray[40];
            if (invoiceFreightAmt!=null) trDetail.setInvoiceFreightAmt(invoiceFreightAmt);
            BigDecimal invoiceDiscountAmt = (BigDecimal)objarray[41];
            if (invoiceDiscountAmt!=null) trDetail.setInvoiceDiscountAmt(invoiceDiscountAmt);
            BigDecimal invoiceTaxAmt = (BigDecimal)objarray[42];
            if (invoiceTaxAmt!=null) trDetail.setInvoiceTaxAmt(invoiceTaxAmt);
            BigDecimal invoiceTotalAmt = (BigDecimal)objarray[43];
            if (invoiceTotalAmt!=null) trDetail.setInvoiceTotalAmt(invoiceTotalAmt);
            
            trDetail.setInvoiceTaxFlg((String) objarray[44]);
            trDetail.setInvoiceLineNbr((String) objarray[45]);
            trDetail.setInvoiceLineName((String) objarray[46]);
            trDetail.setInvoiceLineType((String) objarray[47]);
            trDetail.setInvoiceLineTypeName((String) objarray[48]);
            BigDecimal invoiceLineAmt = (BigDecimal)objarray[49];
            if (invoiceLineAmt!=null)trDetail.setInvoiceLineAmt(invoiceLineAmt);
            BigDecimal invoiceLineTax = (BigDecimal)objarray[50];
            if (invoiceLineTax!=null)trDetail.setInvoiceLineTax(invoiceLineTax);
            
            trDetail.setAfeProjectNbr((String) objarray[51]);
            trDetail.setAfeProjectName((String) objarray[52]);
            trDetail.setAfeCategoryNbr((String) objarray[53]);
            trDetail.setAfeCategoryName((String) objarray[54]);
            trDetail.setAfeSubCatNbr((String) objarray[55]);
            trDetail.setAfeSubCatName((String) objarray[56]);
            trDetail.setAfeUse((String) objarray[57]);
            trDetail.setAfeContractType((String) objarray[58]);
            trDetail.setAfeContractStructure((String) objarray[59]);
            trDetail.setAfePropertyCat((String) objarray[60]);            
            trDetail.setInventoryNbr((String) objarray[61]);
            trDetail.setInventoryName((String) objarray[62]);
            trDetail.setInventoryClass((String) objarray[63]);
            trDetail.setInventoryClassName((String) objarray[64]);
            trDetail.setPoNbr((String) objarray[65]);
            trDetail.setPoName((String) objarray[66]);
            trDetail.setPoDate((Date) objarray[67]);
            trDetail.setPoLineNbr((String) objarray[68]);
            trDetail.setPoLineName((String) objarray[69]);
            trDetail.setPoLineType((String) objarray[70]);
            trDetail.setPoLineTypeName((String) objarray[71]);
            
            trDetail.setShiptoLocation((String) objarray[72]);
            trDetail.setShiptoLocationName((String) objarray[73]);
            trDetail.setShiptoAddressLine1((String) objarray[74]);
            trDetail.setShiptoAddressLine2((String) objarray[75]);
            trDetail.setShiptoCity((String) objarray[78]);
            trDetail.setShiptoCounty((String) objarray[79]);
            trDetail.setShiptoStateCode((String) objarray[80]);
            trDetail.setShiptoZip((String) objarray[81]);
            trDetail.setShiptoCountryCode((String) objarray[82]);
            trDetail.setWoNbr((String) objarray[83]);
            trDetail.setWoName((String) objarray[84]);
            trDetail.setWoDate((Date) objarray[85]);
            trDetail.setWoType((String) objarray[86]);
            trDetail.setWoTypeDesc((String) objarray[87]);
            trDetail.setWoClass((String) objarray[88]);
            trDetail.setWoClassDesc((String) objarray[89]);
            trDetail.setWoEntity((String) objarray[90]);
            trDetail.setWoEntityDesc((String) objarray[91]);
            trDetail.setWoLineNbr((String) objarray[92]);
            trDetail.setWoLineName((String) objarray[93]);
            trDetail.setWoLineType((String) objarray[94]);
            trDetail.setWoLineTypeDesc((String) objarray[95]);
            trDetail.setWoShutDownCd((String) objarray[96]);
            trDetail.setWoShutDownCdDesc((String) objarray[97]);
            trDetail.setVoucherId((String) objarray[98]);
            trDetail.setVoucherName((String) objarray[99]);
            trDetail.setVoucherDate((Date) objarray[100]);
            trDetail.setVoucherLineNbr((String) objarray[101]);
            trDetail.setVoucherLineDesc((String) objarray[102]);
            trDetail.setCheckNbr((String) objarray[103]);
            BigDecimal checkNo = (BigDecimal)objarray[104];
            if (checkNo !=null)trDetail.setCheckNo(checkNo.longValue());
            trDetail.setCheckDate((Date) objarray[105]);
            BigDecimal checkAmt = (BigDecimal)objarray[106];
            if (checkAmt!=null)trDetail.setCheckAmt(checkAmt);            
            trDetail.setCheckDesc((String) objarray[107]);
            
            trDetail.setUserText01((String) objarray[108]);
            trDetail.setUserText02((String) objarray[109]);
            trDetail.setUserText03((String) objarray[110]);
            trDetail.setUserText04((String) objarray[111]);
            trDetail.setUserText05((String) objarray[112]);
            trDetail.setUserText06((String) objarray[113]);
            trDetail.setUserText07((String) objarray[114]);
            trDetail.setUserText08((String) objarray[115]);
            trDetail.setUserText09((String) objarray[116]);
            trDetail.setUserText10((String) objarray[117]);
            trDetail.setUserText11((String) objarray[118]);
            trDetail.setUserText12((String) objarray[119]);
            trDetail.setUserText13((String) objarray[120]);
            trDetail.setUserText14((String) objarray[121]);
            trDetail.setUserText15((String) objarray[122]);
            trDetail.setUserText16((String) objarray[123]);
            trDetail.setUserText17((String) objarray[124]);
            trDetail.setUserText18((String) objarray[125]);
            trDetail.setUserText19((String) objarray[126]);
            trDetail.setUserText20((String) objarray[127]);
            trDetail.setUserText21((String) objarray[128]);
            trDetail.setUserText22((String) objarray[129]);
            trDetail.setUserText23((String) objarray[130]);
            trDetail.setUserText24((String) objarray[131]);
            trDetail.setUserText25((String) objarray[132]);
            trDetail.setUserText26((String) objarray[133]);
            trDetail.setUserText27((String) objarray[134]);
            trDetail.setUserText28((String) objarray[135]);
            trDetail.setUserText29((String) objarray[136]);
            trDetail.setUserText30((String) objarray[137]);
            
            BigDecimal userNumber01 = (BigDecimal)objarray[138];
            if (userNumber01 !=null)trDetail.setUserNumber01(userNumber01);
            BigDecimal userNumber02 = (BigDecimal)objarray[139];
            if (userNumber02 !=null)trDetail.setUserNumber02(userNumber02);
            BigDecimal userNumber03 = (BigDecimal)objarray[140];
            if (userNumber03 !=null)trDetail.setUserNumber03(userNumber03);
            BigDecimal userNumber04 = (BigDecimal)objarray[141];
            if (userNumber04 !=null)trDetail.setUserNumber04(userNumber04);
            BigDecimal userNumber05 = (BigDecimal)objarray[142];
            if (userNumber05 !=null)trDetail.setUserNumber05(userNumber05);
            BigDecimal userNumber06 = (BigDecimal)objarray[143];
            if (userNumber06 !=null)trDetail.setUserNumber06(userNumber06);
            BigDecimal userNumber07 = (BigDecimal)objarray[144];
            if (userNumber07 !=null)trDetail.setUserNumber07(userNumber07);
            BigDecimal userNumber08 = (BigDecimal)objarray[145];
            if (userNumber08 !=null)trDetail.setUserNumber08(userNumber08);
            BigDecimal userNumber09 = (BigDecimal)objarray[146];
            if (userNumber09 !=null)trDetail.setUserNumber09(userNumber09);
            BigDecimal userNumber10 = (BigDecimal)objarray[147];
            if (userNumber10 !=null)trDetail.setUserNumber10(userNumber10);
            
            trDetail.setUserDate01((Date) objarray[148]);
            trDetail.setUserDate02((Date) objarray[149]);
            trDetail.setUserDate03((Date) objarray[150]);
            trDetail.setUserDate04((Date) objarray[151]);
            trDetail.setUserDate05((Date) objarray[152]);
            trDetail.setUserDate06((Date) objarray[153]);
            trDetail.setUserDate07((Date) objarray[154]);
            trDetail.setUserDate08((Date) objarray[155]);
            trDetail.setUserDate09((Date) objarray[156]);
            trDetail.setUserDate10((Date) objarray[157]);
            String comments = (String) objarray[169];
            if (comments !=null)trDetail.setComments(comments);
            BigDecimal tbCalcTaxAmt = (BigDecimal)objarray[159];
            if (tbCalcTaxAmt !=null) trDetail.setTbCalcTaxAmt(tbCalcTaxAmt);
            BigDecimal stateUseAmount = (BigDecimal)objarray[160];
            if (stateUseAmount !=null)trDetail.setStateTier1TaxAmt(stateUseAmount);
            BigDecimal countyUseAmount = (BigDecimal)objarray[163];
            if (countyUseAmount !=null)trDetail.setCountyTier1TaxAmt(countyUseAmount);
            BigDecimal countyLocalUseAmount = (BigDecimal)objarray[164];
            if (countyLocalUseAmount !=null)trDetail.setCountyLocalTaxAmt(countyLocalUseAmount);
            BigDecimal cityUseAmount = (BigDecimal)objarray[165];
            if (cityUseAmount !=null)trDetail.setCityTier1TaxAmt(cityUseAmount);
            BigDecimal cityLocalUseAmount = (BigDecimal)objarray[166];
            if (cityLocalUseAmount !=null)trDetail.setCityLocalTaxAmt(cityLocalUseAmount);
            trDetail.setTransactionStateCode((String) objarray[167]);
            trDetail.setAutoTransactionStateCode((String) objarray[168]);
            String transactionInd = (String) objarray[169];
            if (transactionInd !=null)trDetail.setTransactionInd(transactionInd);
            String suspendInd = (String) objarray[170];
            if (suspendInd !=null)trDetail.setSuspendInd(suspendInd);
            
//AC        BigDecimal taxcodeDetailId = (BigDecimal)objarray[171];
//AC        if (taxcodeDetailId !=null)trDetail.setTaxcodeDetailId(taxcodeDetailId.longValue());
//AC        trDetail.setTaxcodeStateCode((String) objarray[172]);
//AC        trDetail.setTaxcodeTypeCode((String) objarray[173]);
            
            trDetail.setTaxcodeCode((String) objarray[174]);
            
            trDetail.setManualTaxcodeInd((String) objarray[178]);
            BigDecimal taxMatrixId = (BigDecimal)objarray[179];
            if (taxMatrixId !=null)trDetail.setTaxMatrixId(taxMatrixId.longValue());
            BigDecimal locationMatrixId = (BigDecimal)objarray[180];
            if (locationMatrixId !=null)trDetail.setShiptoLocnMatrixId(locationMatrixId.longValue());
            BigDecimal jurisdictionId = (BigDecimal)objarray[181];
            if (jurisdictionId !=null)trDetail.setShiptoJurisdictionId(jurisdictionId.longValue());
            BigDecimal jurisdictionTaxrateId = (BigDecimal)objarray[182];
            if (jurisdictionTaxrateId !=null)trDetail.setStateTaxrateId(jurisdictionTaxrateId.longValue());
            trDetail.setShiptoManualJurInd((String) objarray[183]);
            
            BigDecimal stateUseRate = (BigDecimal)objarray[185];
            if (stateUseRate !=null)trDetail.setStateTier1Rate(stateUseRate);
            
            BigDecimal countyUseRate = (BigDecimal)objarray[192];
            if (countyUseRate !=null)trDetail.setCountyTier1Rate(countyUseRate);
            BigDecimal countySplitAmount = (BigDecimal)objarray[194];
            if (countySplitAmount !=null)trDetail.setCountyTier2MinAmt(countySplitAmount);
            BigDecimal countyMaxtaxAmount = (BigDecimal)objarray[195];
            if (countyMaxtaxAmount !=null)trDetail.setCountyMaxtaxAmt(countyMaxtaxAmount);
            BigDecimal cityUseRate = (BigDecimal)objarray[198];
            if (cityUseRate !=null)trDetail.setCityTier1Rate(cityUseRate);
            BigDecimal citySplitAmount = (BigDecimal)objarray[200];
            if (citySplitAmount !=null)trDetail.setCityTier2MinAmt(citySplitAmount);
            BigDecimal citySplitUseRate = (BigDecimal)objarray[201];
            if (citySplitUseRate !=null)trDetail.setCityTier2Rate(citySplitUseRate);
            
            trDetail.setLoadTimestamp((Date) objarray[205]);
            
            String glExtractFlag = (String)objarray[208];
            if (glExtractFlag !=null)trDetail.setGlExtractFlag(glExtractFlag);
            
            trDetail.setUpdateUserId((String) objarray[216]);
            trDetail.setUpdateTimestamp((Date) objarray[217]);
            
            String processStatus = null;
            if("P".equalsIgnoreCase(transactionInd)){
	            processStatus = "Processed/";
	            if(glExtractFlag !=null && glExtractFlag.equals("1"))
	            	processStatus = processStatus + "Closed";
	            else
	            	processStatus = processStatus + "OPEN";
            }else if("S".equalsIgnoreCase(transactionInd)){
            	processStatus = "Suspended/";
	            if("T".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Tax Matrix";
	            }else if("L".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Location Matrix";
	            }else if("R".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "No Tax Rate";
	            }else if("?".equalsIgnoreCase(suspendInd)){
	            	processStatus = processStatus + "Unknown Suspend Error";
	            }else{
	            	processStatus = processStatus + "Suspend Ind. Error";
	            }
            }else if("Q".equalsIgnoreCase(transactionInd)){
            	processStatus = "Question";
            }else if("H".equalsIgnoreCase(transactionInd)){
            	processStatus = "Held";
            }else{
            	processStatus = "TRANSACTION Ind. Error";
            }
            TransactionDetailDTO trDetailDTO = trDetail.getTransactionDetailDTO();
            if(processStatus != null)trDetailDTO.setProcessStatus(processStatus);
            
            trDetailDTO.setHasComments(comments != null);
            
            list.add(trDetailDTO);
		}		
		 return list;
	}

	public void update(PurchaseTransaction detail) {
		detail.setModifyTimestamp(new Date());
		detail.setModifyUserId(Auditable.currentUserCode());
		super.update(detail);
	}
	// Bug 4184
	public List<PurchaseTransaction> saveRecords(
			PurchaseTransaction exampleInstance, Date effectiveDate,
			Date expirationDate, Boolean includeUnprocessed,
			Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults) {
		return getDAO().getAllRecords(exampleInstance, effectiveDate, 
				expirationDate, includeUnprocessed, includeProcessed, 
				includeSuspended, glExtractFlagIsNull, advancedFilter, 
				orderBy, firstRow, maxResults);
	}
	
	
	public String getProcessSQLStatement(
			PurchaseTransaction exampleInstance, Date effectiveDate,
			Date expirationDate, Boolean includeUnprocessed,
			Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults) {
		return getDAO().getProcessSQLStatement(exampleInstance, effectiveDate, 
				expirationDate, includeUnprocessed, includeProcessed, 
				includeSuspended, glExtractFlagIsNull, advancedFilter, 
				orderBy, firstRow, maxResults);
	}
	
	public String getProcessSQLCriteria(
			PurchaseTransaction exampleInstance, Date effectiveDate,
			Date expirationDate, Boolean includeUnprocessed,
			Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults) {
		return getDAO().getProcessSQLCriteria(exampleInstance, effectiveDate, 
				expirationDate, includeUnprocessed, includeProcessed, 
				includeSuspended, glExtractFlagIsNull, advancedFilter, 
				orderBy, firstRow, maxResults);
	}
	
	public String getStatisticsSQLStatement(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds){
		
		return getDAO().getStatisticsSQLStatement(exampleInstance, 
				sqlWhere, orderBy, firstRow, maxResults,nullFileds);
	}

	//3987
	public void saveColumn(TransactionHeader transactionHeader) {
		 getDAO().saveColumn(transactionHeader);
	}

	@Override
	public List<String> getColumnHeaders(String userCode, String columnType, String windowName, String dataWindowName) {
		return getDAO().getColumnHeaders(userCode, columnType, windowName, dataWindowName);
	}

	@Override
	public void removeColumns(String userCode, String columnType, String windowName, String dataWindowName) {
		getDAO().removeColumns(userCode, columnType, windowName, dataWindowName);
		
	}

	@Override
	public long getTransactionCountByTaxcodeDetail(Long taxcodeDetailId) {
		return getDAO().getTransactionCountByTaxcodeDetail(taxcodeDetailId);
	}

	@Override
	public void saveColumns(String userCode, String columnType,
			String windowName, String dataWindowName,
			List<TransactionHeader> transactionHeaders) {
		getDAO().saveColumns(userCode, columnType, windowName, dataWindowName, transactionHeaders);
	}

	@Override
	public boolean getGSBMatrixInUse(Long taxMatrixId) {
		return getDAO().getGSBMatrixInUse(taxMatrixId);
	}

	@Override
	public void updateaActiveFlagMatrixInUse(Long taxMatrixId) {
		getDAO().updateaActiveFlagMatrixInUse(taxMatrixId);
		
	}
	
	public void updateTransaction(PurchaseTransaction updatedTransaction) {
		updatedTransaction.setModifyTimestamp(new Date());
		updatedTransaction.setModifyUserId(Auditable.currentUserCode());
		getDAO().updateTransaction(updatedTransaction);
	}

	public void updatetoHoldTransaction(PurchaseTransaction instance) {
		getDAO().updatetoHoldTransaction(instance);
	}
	  
	
	/**
	 * @author Anand
	 * @param locationMatrixId - Long type
	 * this service method suspends a location matrix line.
	 * location matrix already extracted, can't be suspended. 
	 * 
	public void suspendLM(Long locationMatrixId) {
		getDAO().suspendLM(locationMatrixId);	
	}
	 */
	
}
