package com.ncsts.services.impl;


import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.services.RatepointService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RatepointServiceImpl implements RatepointService {

    private static final Log logger = LogFactory.getLog(RatepointService.class);

    @Override
    public Jurisdiction getRateforLatLon(String serverUrl, String latLonUrl, String userId, String password, String latitude, String longitude) {
        if (!StringUtils.hasText(serverUrl) || !StringUtils.hasText(latLonUrl)) {
            logger.error("Failed to start Ratepoint lookup getRateforLatLon because of configuration parameters missing.\nserverUrl="+serverUrl+", latLonUrl="+latLonUrl);
            return new Jurisdiction();
        }

        StringBuilder request = new StringBuilder("<request>");
        //request.append(String.format("<username>%s</username><password>%s</password>", username, password));
        request.append(String.format("<username>%s</username><password>%s</password>",
                userId, password));

        boolean hasInput = false;
        if(StringUtils.hasText(latitude)) {
            request.append(String.format("<latitude>%s</latitude>", latitude));
            hasInput = true;
        }

        if(StringUtils.hasText(longitude)) {
            request.append(String.format("<longitude>%s</longitude>", longitude));
            hasInput = true;
        }

        request.append("<taxType>USE</taxType>");
        request.append("</request>");

        Jurisdiction juris = null;
        if(hasInput) {
            String response = post(serverUrl, latLonUrl, request.toString());
            juris = buildJurisdiction(response, "rate");
        }
        else {
            juris = new Jurisdiction();
            logger.error("Failed to start Ratepoint lookup getRateforLatLon because of request parameters missing.\nLatitude:"+latitude+"\nLongitude"+longitude);
        }
        return juris;
    }

    @Override
    public List<Jurisdiction> getJurGeneric(String serverUrl, String genericJurUrl, String userId, String password, String horl, String city, String county, String state, String zip, String country) {
    	List<Jurisdiction> jurisArray = new ArrayList<Jurisdiction>();
    	if(!StringUtils.hasText(horl)) {
            horl = "0";
        }

        if (!"0".equalsIgnoreCase(horl) && !"H".equalsIgnoreCase(horl) && !"L".equalsIgnoreCase(horl) && !"A".equalsIgnoreCase(horl))
            horl = "H";

        if (!StringUtils.hasText(serverUrl) || !StringUtils.hasText(genericJurUrl)) {
            logger.error("Failed to start Ratepoint lookup getJurGeneric because of configuration parameters missing.\nserverUrl="+serverUrl+", genericJurUrl="+genericJurUrl);
            jurisArray.add(new Jurisdiction());
            return jurisArray;
        }

        StringBuilder request = new StringBuilder("<request>");
        //request.append(String.format("<username>%s</username><password>%s</password>", username, password));
        request.append(String.format("<username>%s</username><password>%s</password>",
                userId, password));

        boolean hasInput = false;

        if(StringUtils.hasText(horl)) {
            request.append(String.format("<horL>%s</horL>", horl));
            hasInput = true;
        }

        if(StringUtils.hasText(city)) {
            request.append(String.format("<city>%s</city>", city));
            hasInput = true;
        }

        if(StringUtils.hasText(county)) {
            request.append(String.format("<county>%s</county>", county));
            hasInput = true;
        }

        if(StringUtils.hasText(state)) {
            request.append(String.format("<state>%s</state>", state));
            hasInput = true;
        }

        if(StringUtils.hasText(zip)) {
            request.append(String.format("<zip>%s</zip>", zip));
            hasInput = true;
        }

        if(StringUtils.hasText(country)) {
            request.append(String.format("<country>%s</country>", country));
            hasInput = true;
        }


        request.append("</request>");

        Jurisdiction juris = null;
        if(hasInput) {
            String response = post(serverUrl, genericJurUrl, request.toString());
            jurisArray = buildJurisdiction(response, "geocodeRecord", true);
        }
        else {
            logger.error("Failed to start Ratepoint lookup getJurGeneric! Not enough data to complete request! horl="+horl+", " +
                    "city="+city+", county= "+county+",\n" +
                    "state="+state+", zip="+zip+", country="+country);
            jurisArray.add(new Jurisdiction());
        }

        return jurisArray;
    }

    @Override
    public Jurisdiction getStreetLevelRate(String serverUrl, String streetLevelRateUrl, String userId, String password, String streetAddress1, String streetAddress2, String city, String county, String state, String zip, String country, Date glDate) {
        if(!StringUtils.hasText(streetAddress1)) {
            logger.error("Failed to start Ratepoint lookup getStreetLevelRate! StreetAddress1 missing");
            return new Jurisdiction();
        }

        if(!StringUtils.hasText(zip)) {
            logger.error("Failed to start Ratepoint lookup getStreetLevelRate! Zip missing");
            return new Jurisdiction();
        }


        if (!StringUtils.hasText(serverUrl) || !StringUtils.hasText(streetLevelRateUrl)) {
            logger.error("Failed to start Ratepoint lookup getStreetLevelRate because of configuration parameters missing.\n serverUrl="+serverUrl+", streetLevelRateUrl="+streetLevelRateUrl);
            return new Jurisdiction();
        }

        StringBuilder request = new StringBuilder("<request>");
        //request.append(String.format("<username>%s</username><password>%s</password>", username, password));
        request.append(String.format("<username>%s</username><password>%s</password>",
                userId, password));

        boolean hasInput = false;
        if(StringUtils.hasText(streetAddress1)) {
            request.append(String.format("<streetAddress1>%s</streetAddress1>", streetAddress1));
            hasInput = true;
        }

        if(StringUtils.hasText(streetAddress2)) {
            request.append(String.format("<streetAddress2>%s</streetAddress2>", streetAddress2));
            hasInput = true;
        }

        if(StringUtils.hasText(city)) {
            request.append(String.format("<city>%s</city>", city));
            hasInput = true;
        }

        if(StringUtils.hasText(county)) {
            request.append(String.format("<county>%s</county>", county));
            hasInput = true;
        }

        if(StringUtils.hasText(state)) {
            request.append(String.format("<state>%s</state>", state));
            hasInput = true;
        }

        if(StringUtils.hasText(zip)) {
            request.append(String.format("<zip>%s</zip>", zip));
            hasInput = true;
        }

        if(StringUtils.hasText(country)) {
            request.append(String.format("<country>%s</country>", country));
            hasInput = true;
        }
        
        String transactionDate = null;
        if(glDate!=null) {
        	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try{
				transactionDate = dateFormat.format(glDate);
			}
	    	catch(Exception e) {
	    	}
        }
        if(StringUtils.hasText(transactionDate)) {
            request.append(String.format("<transactionDate>%s</transactionDate>", transactionDate));
            hasInput = true;
        }

        request.append("<taxType>USE</taxType>");
        request.append("</request>");

        Jurisdiction juris = null;
        if(hasInput) {
            String response = post(serverUrl, streetLevelRateUrl, request.toString());
            juris = buildJurisdiction(response, "rate");
        }
        else {
            logger.error("Failed to start Ratepoint lookup getStreetLevelRate! Not enough data to complete request! streetAddress1="+streetAddress1+", " +
                    "streetAddress2="+streetAddress2+", city="+city+", county= "+county+",\n" +
                    "state="+state+", zip="+zip+", country="+country);
            juris = new Jurisdiction();
        }

        return juris;
    }

    private Jurisdiction buildJurisdiction(String response, String rootNode) {
    	List<Jurisdiction> jurisArray = buildJurisdiction(response, rootNode, false);
    	if(jurisArray.size()>0) {
    		return jurisArray.get(0);
        }
    	else {
    		return new Jurisdiction();
    	}
    }

    private List<Jurisdiction> buildJurisdiction(String response, String rootNode, boolean returnAll) {
        List<Jurisdiction> jurisArray = new ArrayList<Jurisdiction>();

        if(StringUtils.hasText(response)) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(new StringReader(response)));
                Element root = doc.getDocumentElement();
                
                NodeList nl = root.getElementsByTagName(rootNode);
                if(nl != null && nl.getLength() > 0) {
                	for(int i=0; i<nl.getLength(); i++) {
	                    Element el = (Element) nl.item(i);
	                    if(el != null) {
	                    	Jurisdiction juris = new Jurisdiction();
	                    	
	                        String geocode = getTextValue(el, "geocode");
	                        String sdCode = getTextValue(el, "sdCode");
	
	                        if(StringUtils.hasText(geocode) && StringUtils.hasText(sdCode)) {
	                            juris.setGeocode(geocode + sdCode);
	                        }
	                        else if(StringUtils.hasText(geocode)) {
	                            juris.setGeocode(geocode);
	                        }
	
	                    /*    if (StringUtils.hasText(sdCode)) {
	                            juris.setSdCode(sdCode);
	                        }*/
	
	                        juris.setCity(getTextValue(el, "cityName"));
	                        juris.setCounty(getTextValue(el, "countyName"));
	                        juris.setState(getTextValue(el, "stateName"));
	                        String zip = getTextValue(el, "zip");
	                        if(zip != null && zip.length() > 6) {
	                            juris.setZip(zip.substring(0, 5));
	                            juris.setZipplus4(zip.substring(6));
	                        }
	                        else {
	                            juris.setZip(zip);
	                        }
	                        juris.setStj1Name(getTextValue(el, "stj1Name"));
	                        juris.setStj2Name(getTextValue(el, "stj2Name"));
	                        juris.setStj3Name(getTextValue(el, "stj3Name"));
	                        juris.setStj4Name(getTextValue(el, "stj4Name"));
	                        juris.setStj5Name(getTextValue(el, "stj5Name"));
	
	                        NodeList dnl = root.getElementsByTagName("address");
	                        Element del = null;
	                        if(dnl != null && dnl.getLength() > 0) {
	                            del = (Element) dnl.item(0);
	                            juris.setCountry(getTextValue(del, "country", "US"));
	                            if(juris.getZipplus4() == null) {
	                                zip = getTextValue(del, "zip");
	                                if(zip != null && zip.length() > 6) {
	                                    juris.setZipplus4(zip.substring(6));
	                                }
	                            }
	                        }
	                        else {
	                            juris.setCountry("US");
	                        }
	                        
	                        jurisArray.add(juris);
	                    }
	                    if(!returnAll) {
	                    	break;
	                    }
                	}
                }
            }
            catch(Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        
        if(jurisArray.size()==0) {
        	jurisArray.add(new Jurisdiction());
        }
      
        return jurisArray;
    }

    private String getTextValue(Element ele, String tagName, String defaultValue) {
        String textVal = getTextValue(ele, tagName);

        return  StringUtils.hasText(textVal)  ? textVal : defaultValue;
    }

    private String getTextValue(Element ele, String tagName) {
        String textVal = null;

        try {
            NodeList nl = ele.getElementsByTagName(tagName);
            if(nl != null && nl.getLength() > 0) {
                Element el = (Element)nl.item(0);
                if(el.getFirstChild() != null) {
                    textVal = el.getFirstChild().getNodeValue();
                }
            }
        }
        catch(Exception e) {
            logger.debug(e.getMessage(), e);
        }

        return textVal;
    }

    private String post(String url, String path, String request) {
        String responseBody = null;
        HttpClient httpclient = new DefaultHttpClient();
        try {

            HttpGet httpget = new HttpGet(String.format("%s%s?request=%s", url, path, URLEncoder.encode(request, "UTF-8")));

            if(logger.isDebugEnabled()) {
                logger.debug("executing request " + httpget.getURI());
            }
            logger.debug("executing request " + httpget.getURI());
            System.out.println("executing request " + httpget.getURI());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();

            responseBody = httpclient.execute(httpget, responseHandler);

            if(logger.isDebugEnabled()) {
                logger.debug(responseBody);
            }
            logger.debug(responseBody);
            System.out.println(responseBody);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        } finally {
            httpclient.getConnectionManager().shutdown();
        }

        return responseBody;
    }



}
