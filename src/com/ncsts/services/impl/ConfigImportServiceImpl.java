package com.ncsts.services.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlTypeValue;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.ConfigImportDAO;
import com.ncsts.domain.BCPConfigStagingText;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.dto.DataDefinitionTableDTO;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.ConfigImportService;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.OptionService;
import com.ncsts.view.bean.ChangeContextHolder;
import com.ncsts.view.util.ImportConfigFileUploadParser;

public class ConfigImportServiceImpl implements ConfigImportService {

	
	
	
	public final static String ERRCODE_INCORR_COL_NUM = "IP1";
	public final static String ERRCODE_COL_TRANS_ERR = "IP2";
	public final static String ERRCODE_MAXIMUM_ERR = "IP3";
	public final static String LISTCODE_TYPE_ERR = "ERRORCODE";
	
	
	

	
	private Logger logger = LoggerFactory.getInstance().getLogger(ConfigImportServiceImpl.class);
	private ConfigImportDAO configImportDAO;
	private DbPropertiesBean dbPropertiesBean;
	private DbPropertiesDTO dbPropertiesDTO = null;
	protected DataDefinitionService dataDefinitionService;
	private OptionService optionService;
	private CacheManager cacheManager;
	private boolean isValidRow;
	
	public static Map<String, String> delimeterMap = new HashMap<String, String>(){};
	static {
		delimeterMap.put("~t", "\t");
		delimeterMap.put("~v", "\u000B");
		delimeterMap.put("~r", "\r");
		delimeterMap.put("~f", "\f");
		delimeterMap.put("~b", "\b");
		delimeterMap.put("~n", "\n");		
	}

	
	
	public boolean isValidRow() {
		return isValidRow;
	}

	public void setValidRow(boolean isValidRow) {
		this.isValidRow = isValidRow;
	}

	@Override
	public List<BCPConfigStagingText> getImportedData(Long batchId) {		
		return configImportDAO.getImportedData(batchId);
	}
	
	public ConfigImportDAO getConfigImportDAO() {
		return configImportDAO;
	}
	
	public void setConfigImportDAO(ConfigImportDAO configImportDAO) {
		this.configImportDAO = configImportDAO;
	}
	
	public DbPropertiesBean getDbPropertiesBean() {
		return dbPropertiesBean;
	}
	
	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}
	
	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}
	
	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}
	
	public OptionService getOptionService() {
		return optionService;
	}
	
	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}
	
	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public BatchMaintenance addBatchUsingJDBC(
			BatchMaintenance batchMaintenance) {
		// Get next batch Id
		long nextId = -1;
		Connection conn;
		Statement stat = null;
		try {
			if(dbPropertiesDTO==null){
				String selectedDb = ChangeContextHolder.getDataBaseType();
				dbPropertiesDTO=dbPropertiesBean.getDbPropertiesDTO(selectedDb);
			}
			conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
					dbPropertiesDTO.getUserName(),
					dbPropertiesDTO.getPassword());
			conn.setAutoCommit(false);
			stat = conn.createStatement();

			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_batch_id", "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ addBatchUsingJDBC() failed: "
					+ e.toString());
			logger.error("Can't get nextid sequence\n"+e.getMessage());
			return null;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// Set new batch id
		batchMaintenance.setBatchId(nextId);
		//this.batchNumber = nextId;
		PreparedStatement prep = null;
		try {
			String sql = batchMaintenance.getRawAddStatement();
			prep = conn.prepareStatement(sql);
			batchMaintenance.populateAddPreparedStatement(conn, prep);
			prep.addBatch();
			prep.executeBatch();
			conn.commit();
		} catch (Exception e) {
			logger.error("addBatchUsingJDBC failed:\n"+e.getMessage());
			return null;
		} finally {
			if (prep != null) {
				try {
					prep.close();
					conn.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}

		return batchMaintenance;
	}

	public void createTemplate(String tableName){
		// FileWriter fw=null;
		ResultSet rs = null;
		Connection conn = null;
		Statement stat = null;
		String delimeter = "BROKEN_DELIMETER";
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			HttpServletResponse response = (HttpServletResponse) context
					.getExternalContext().getResponse();

			response.setContentType("text/plain");
			response.setHeader("Content-Language", "en");
			response.setHeader("Content-disposition", "attachment; filename=\""
					+ tableName + "_template.txt" + "\"");
			PrintWriter fw = response.getWriter();
			DataDefinitionTableDTO dataDefDto=dataDefinitionService.findById(tableName);
			if (dataDefDto!=null) {
				String fileType = dataDefDto.getConfigFileTypeCode();
				fw.append("ConfigBatch,   ");
				fw.append("Type: " + fileType);
				fw.append(",  ");
				fw.append("Date: ");
				String currentDate = new SimpleDateFormat("MM/dd/yyyy")
						.format(new Date());
				fw.append(currentDate);
				
				fw.append(", Append: A");
				
				fw.append(System.getProperty("line.separator"));
			}
			Option option = optionService.findByPK(new OptionCodePK(
					"EXPORTDELIMITER", "SYSTEM", "SYSTEM"));
			
			String delimeterFromDb = null; 
			if (option!=null && option.getValue()!=null)
				delimeterFromDb = option.getValue().trim();

			
			if (delimeterFromDb != null) {
				delimeter = delimeterMap.get(delimeterFromDb);
				if (delimeter == null)
					delimeter = delimeterFromDb;
			}
			String selectedDb = ChangeContextHolder.getDataBaseType();
			dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);
			conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
					dbPropertiesDTO.getUserName(),
					dbPropertiesDTO.getPassword());
			conn.setAutoCommit(false);
			stat = conn.createStatement();
			rs = stat.executeQuery("select * from" + " "
					+ tableName);
			
			StringBuilder header = new StringBuilder();
			if (rs != null) {
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				for (int i = 1; i <= columnCount; i++) {
					
					if (rsmd.getColumnType(i) != java.sql.Types.LONGVARCHAR ) {					
						header.append(rsmd.getColumnName(i));
						if (i != columnCount)
							header.append(delimeter);
					} else if (i == columnCount) {
						header.deleteCharAt(header.length()-1);
					}
					
				}
				fw.append(header);
				fw.append(System.getProperty("line.separator"));
				fw.flush();
				fw.close();
				rs.close();
				context.responseComplete();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} finally {

			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}
		}
		//return null;
	
	}
	public void exportTableToFile(String tableName) {
		ResultSet rs = null;
		Connection conn = null;
		Statement stat = null;
		try {
			String delimeter = " ";
			String filePath = null;
			Option option = optionService.findByPK(new OptionCodePK(
					"DOWNLOADPATH", "SYSTEM", "SYSTEM"));
			filePath = option.getValue();
			if (filePath == null) {
				filePath = "C:/";
			}

			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context
					.getExternalContext().getResponse();

			response.setContentType("text/plain");
		
			response.setHeader("Content-Language", "en");
			response.setHeader("Content-disposition", "attachment; filename=\""
					+ tableName + ".txt" + "\"");
			PrintWriter fw = response.getWriter();
			option = optionService.findByPK(new OptionCodePK(
					"EXPORTDELIMITER", "SYSTEM", "SYSTEM"));
			String delimeterFromDb = option.getValue().trim();
			if (delimeterFromDb != null) {
				delimeter = delimeterMap.get(delimeterFromDb);
				if (delimeter == null)
					delimeter = delimeterFromDb;
			}
			DataDefinitionTableDTO dataDefDto=dataDefinitionService.findById(tableName);
			if (dataDefDto!=null) {
				String fileType = dataDefDto.getConfigFileTypeCode();
				fw.append("ConfigBatch,   ");
				fw.append("Type: " + fileType);
				fw.append(",  ");
				fw.append("Date: ");
				String currentDate = new SimpleDateFormat("MM/dd/yyyy")
						.format(new Date());
				fw.append(currentDate);
				
				fw.append(", Append: A");
				
				fw.append(System.getProperty("line.separator"));
			}
			String selectedDb = ChangeContextHolder.getDataBaseType();
			dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);
			conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
					dbPropertiesDTO.getUserName(),
					dbPropertiesDTO.getPassword());
			conn.setAutoCommit(false);
			stat = conn.createStatement();
			rs = stat.executeQuery("select * from" + " "
					+ tableName);
			// rs=getDataFromTable(selectedExportTableName);
			StringBuilder header = new StringBuilder();  
			if (rs != null) {
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				for (int i = 1; i <= columnCount; i++) {
					if (rsmd.getColumnType(i) != java.sql.Types.LONGVARCHAR ) { 
						header.append(rsmd.getColumnName(i));
						if (i != columnCount)
							header.append(delimeter);
					} else if (i == columnCount) {
						header.deleteCharAt(header.length()-1);
					}
				}
				fw.append(header);
				fw.append(System.getProperty("line.separator"));
				
				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
				while (rs.next()) {
					StringBuilder row = new StringBuilder();
					for (int i = 1; i <= columnCount; i++) {
						int columnType = rsmd.getColumnType(i); 
						if (columnType != Types.LONGVARCHAR ) {
							if (rs.getObject(i) != null) {
								
								if (columnType == Types.TIMESTAMP) {
									Date cd = rs.getTimestamp(i);
									String data = dateFormatter.format(cd);
									row.append(data);
								} else if (columnType == Types.TIME) {
									Date cd = rs.getTime(i);
									String data = dateFormatter.format(cd);
									row.append(data);
								} else if (columnType == Types.DATE) {
									Date cd = rs.getDate(i);
									String data = dateFormatter.format(cd);
									row.append(data);
								} else {								
									String data = rs.getObject(i).toString();
									row.append(data);
								}
							} else {
								
								if(columnType == Types.BIGINT   ||
									columnType == Types.DECIMAL ||
									columnType == Types.NUMERIC ||
									columnType == Types.DOUBLE  ||
									columnType == Types.FLOAT   ||
									columnType == Types.INTEGER ||
									columnType == Types.SMALLINT) {
									
									String data = "0";
									row.append(data);
								} else if  (columnType == Types.TIMESTAMP || columnType == Types.TIME || columnType == Types.DATE) {
									System.out.println("Date is null");
									System.out.println("cname:"+rsmd.getColumnName(i));
									
									
								} else  {
									
									
									
								/*	String data = "null";
									row.append(data);*/
								}
							}
							if (i != columnCount)
								row.append(delimeter);
						} else if (i == columnCount) {
							row.deleteCharAt(row.length()-1);							
						}
						
					}
					row.append(System.getProperty("line.separator"));
					fw.append(row);
					
				}
				
				
				
				fw.flush();
				fw.close();
				rs.close();
				context.responseComplete();

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exportTableToFile failed"+e.getMessage());
		} finally {

			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}

		}
		
		//return null;
	}

	
	public BatchMaintenance importConfigFile(
			BatchMaintenance batchMaintenance,
			ImportConfigFileUploadParser parser, InputStream inputStream)
			throws Exception {
		Connection conn=null;
		BufferedReader br = null;
		
		HashMap<String, ListCodes> errMap = new HashMap<String, ListCodes>(); 
		List<ListCodes> errCodes = cacheManager.getListCodesByType(LISTCODE_TYPE_ERR);
		
		for (ListCodes code: errCodes) {
			String codeCode = code.getCodeCode(); 
			if (ERRCODE_COL_TRANS_ERR.equalsIgnoreCase(codeCode) || ERRCODE_INCORR_COL_NUM.equals(codeCode) || ERRCODE_MAXIMUM_ERR.equals(codeCode)) 
				errMap.put(codeCode, code);
		}
		
		int maximumAttempts = -1;
		
		Option maxAttemptsOption = optionService.findByPK(new OptionCodePK("ERRORROWS", "SYSTEM", "SYSTEM"));
		if (maxAttemptsOption!=null && maxAttemptsOption.getValue()!=null) {
			try {
				maximumAttempts = Integer.parseInt(maxAttemptsOption.getValue()); 				
			}catch (NumberFormatException e) {
				logger.error("Cant contvert value from TB_OPTION /ERRORROWS/SYSTEM/SYSTEM to int.");
			}
		}
		System.out.println("maximumAttempts="+maximumAttempts);
		
		
		parser.setKnownErrors(errMap);
		parser.setErrorTreshold(maximumAttempts);
		
		PreparedStatement prep = null;
		PreparedStatement prepText = null;
		
		try {
			if (inputStream != null) {
				br = new BufferedReader(new InputStreamReader(inputStream));
			}
			String sql = null;
			String sqlText = null;

			sql = getImportedQuery(parser.getFileType());
			
			System.out.println("prepSQL"+sql);
			
			if (sql==null) throw new Exception("Could not find insert statement for code "+parser.getFileType());
			//headerLine
			while(br.readLine().trim().equals(""));
			
			
			
			String headerColumns;
			while((headerColumns = br.readLine()).trim().equals(""));
			
			
			///String headerColumns = br.readLine();
			String importTableColumnHeaders = getImportTableColumnHeaders(sql);
			Option option = optionService.findByPK(new OptionCodePK(
					"EXPORTDELIMITER", "SYSTEM", "SYSTEM"));
			String delimeter=null;
			String delimeterFromDb = null;
			
			if (option!=null && option.getValue()!=null)
				delimeterFromDb=option.getValue().trim();
			
            if (delimeterFromDb != null) {
				delimeter = delimeterMap.get(delimeterFromDb);
				if (delimeter == null)
					delimeter = delimeterFromDb;
			}
            
            
            
			sql = prepareSqlStatement(sql, headerColumns, delimeter);
			String importTableColumn = getImportTableColumnsString(sql);
			
			sqlText = "INSERT INTO tb_bcp_config_staging_text (batch_id,line,text) VALUES (?,?,?)";
			if (dbPropertiesDTO == null) {
				String selectedDb = ChangeContextHolder.getDataBaseType();
				dbPropertiesDTO = dbPropertiesBean
						.getDbPropertiesDTO(selectedDb);
			}
			conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
					dbPropertiesDTO.getUserName(),
					dbPropertiesDTO.getPassword());
			conn.setAutoCommit(false);
			prep = conn.prepareStatement(sql);
			prepText = conn.prepareStatement(sqlText);
			int i = 0;
			while (br.ready()) {
				i++;
				String nextRow = br.readLine();
				
				if (!nextRow.trim().isEmpty()) {

					

					
					Map<String,String> nextRowMap = setRowValuesAsMap(nextRow, headerColumns, delimeter, parser);
					long nextId = getNextId("sq_tb_bcp_config_staging_id", conn);
					this.isValidRow = true;
					prep = setPrepareStatementValues(prep, nextRow, nextRowMap,
							importTableColumn, importTableColumnHeaders, nextId,
							batchMaintenance.getbatchId(), i, parser, delimeter);
					
					
					
					if (parser.isErrorTresholdReached()) {
						flushBatch(prep, prepText, conn, parser, i);
						parser.addError(i, nextRow, ERRCODE_MAXIMUM_ERR);
						flushErrorLog(conn, batchMaintenance, parser);
						return batchMaintenance;
					}
					
					prepText.setLong(1, batchMaintenance.getbatchId());
					prepText.setLong(2, i);
					prepText.setString(3, nextRow);
	

					if (isValidRow) {
						prep.addBatch();
					}
					prepText.addBatch();
	

					
					if ((i % 50) == 0) {
						flushBatch(prep, prepText, conn, parser, i);
/*						logger.debug("Begin Flush: " + i);
						try {
							prep.executeBatch();
							prepText.executeBatch();
							conn.commit();
							prep.clearBatch();
							prepText.clearBatch();
							logger.debug("End Flush");
							parser.setLinesWritten(i);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("Importing Config File Failed: "+e.getMessage());
						}*/
					}
					parser.setLinesWritten(i);
					if (parser.getBatchErrorLogs().size() >= 50) {
						// flush every 50+ errors
						flushErrorLog(conn, batchMaintenance, parser);
					}
					
				}
				
			}

			if (prep != null) {
				try {
					prep.executeBatch();
					prepText.executeBatch();
					conn.commit();
					prep.clearBatch();
					prepText.clearBatch();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			flushErrorLog(conn, batchMaintenance,parser);
			parser.setLinesWritten(i);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("importConfigFile failed"+e.getMessage());
		} finally {
			
			try {
				prep.close();
			} catch (Exception ex) {}
			
			try {
				prepText.close();
			} catch (Exception ex) {}
			
			try {					
				conn.close();
			} catch (SQLException ignore) {}
		}
		
		return batchMaintenance;
	}

	private void flushBatch(PreparedStatement prep, PreparedStatement prepText, Connection conn, 
			ImportConfigFileUploadParser parser, int lineNumber) {
		logger.debug("Begin Flush: " + lineNumber);
		try {
			prep.executeBatch();
			prepText.executeBatch();
			conn.commit();
			prep.clearBatch();
			prepText.clearBatch();
			logger.debug("End Flush");
			parser.setLinesWritten(lineNumber);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Importing Config File Failed: "+e.getMessage());
		}
	}
	
	public void updateBatchUsingJDBC(BatchMaintenance batchMaintenance) throws Exception {
		Connection conn = null;
		PreparedStatement prep=null;
		try{
			if (null == dbPropertiesDTO) {
				String selectedDb = ChangeContextHolder.getDataBaseType();
				dbPropertiesDTO = dbPropertiesBean
						.getDbPropertiesDTO(selectedDb);
			}
		conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
				dbPropertiesDTO.getUserName(),
				dbPropertiesDTO.getPassword());
		conn.setAutoCommit(false);
		String sql = batchMaintenance.getRawUpdateStatement();
		 prep = conn.prepareStatement(sql);
		batchMaintenance.populateUpdatePreparedStatement(conn, prep);
		prep.addBatch();

		prep.executeBatch();
		conn.commit();
		prep.clearBatch();
		}catch (Exception e) {
			logger.error("updateBatchUsingJDBC failed"+e.getMessage());
		}finally{
			if (prep != null) {
				try {
					prep.close();
					conn.close();
				} catch (Exception ex) {
					logger.error("updateBatchUsingJDBC failed"+ex.getMessage());
				}
			}
		}
		return;
	}
	private String prepareSqlStatement(String importSql,
			String headerColumnString,String delimeter) {
		String[] headerColumns = headerColumnString.split(Pattern.quote(delimeter), headerColumnString.length());
		int startingIndex = importSql.indexOf("(");
		int endIndex = importSql.indexOf(")");
		String importColumnsString = importSql.substring(startingIndex + 1,
				endIndex);
		String[] importColumns = importColumnsString.split(",");
		if (headerColumns.length + 3 != importColumns.length) {
			return importSql;
		}
		importSql = importSql.substring(0, importSql.lastIndexOf("("));
		importSql = importSql + "(";
		for (String column : importColumns) {
			importSql = importSql + "?,";
		}
		importSql = importSql.substring(0, importSql.length() - 1);
		importSql = importSql + ")";

		return importSql;
	}

	private String getImportTableColumnsString(String importSql) {
		int startIndex = importSql.indexOf("(");
		int endIndex = importSql.indexOf(")");
		return importSql.substring(startIndex + 1, endIndex);
	}

	private long getNextId(String idType, Connection conn) {
		long nextId = -1;
		Statement stat = null;
		try {
			/*conn = DriverManager.getConnection(dbPropertiesDTO.getUrl(),
					dbPropertiesDTO.getUserName(),
					dbPropertiesDTO.getPassword());
			conn.setAutoCommit(false);*/
			stat = conn.createStatement();

			String sql = DatabaseUtil
					.getSequenceQuery(
							DatabaseUtil.getDatabaseProductName(stat), idType,
							"NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ addBatchUsingJDBC() failed: "
					+ e.toString());
			return nextId;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return nextId;
	}

	public void flushErrorLog(Connection conn, BatchMaintenance batchMaintenance, ImportConfigFileUploadParser parser)
			throws Exception {
		String sql = null;
		PreparedStatement prep = null;
		Statement statErrorNext = null;

		int ie = 1;
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			if (prep == null) {
				sql = error.getRawInsertStatement();
				prep = conn.prepareStatement(sql);
				if (error.getErrorDefCode().equalsIgnoreCase("CI12"))
					parser.setAbort(true);
				statErrorNext = conn.createStatement();
			}

			error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
			error.setProcessType("I");
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			error.populatePreparedStatement(conn, prep);
			prep.addBatch();

			if ((ie++ % 50) == 0) {
				prep.executeBatch();
				conn.commit();
				prep.clearBatch();
			}
		}

		if (prep != null) {
			prep.executeBatch();
			conn.commit();
			prep.clearBatch();

			try {
				prep.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (statErrorNext != null) {
			try {
				statErrorNext.close();
			} catch (Exception ex) {
			   ex.printStackTrace();
			}
		}

		parser.resetBatchErrorLogs();
	}
	private long getNextBatchErrorLogId(Statement statErrorNext) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(statErrorNext),
					"sq_tb_batch_error_log_id", "NEXTID");

			ResultSet rs = statErrorNext.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBatchErrorLogId() failed: "
					+ e.toString());
		} finally {
		}

		return nextId;
	}
	
	private PreparedStatement setPrepareStatementValues(
			PreparedStatement pstmt, String row, Map<String,String> nextRowMap, String importTableColumn, String importTableColumnHeaders,
			long nextId, long batchId, int lineNumber, ImportConfigFileUploadParser parser, String delimeter) {
		
		String[] importTableColumns = importTableColumn.split(",");
		String[] importColumnHeaders = importTableColumnHeaders.split(",");
		
		
		String[] tokens = row.split(Pattern.quote(delimeter), row.length());
		int columnsLength = importTableColumns.length;
		String errorCode=null;
		this.isValidRow = true;
		if ((tokens.length + 3) != columnsLength) {
			parser.addError(lineNumber, row, ERRCODE_INCORR_COL_NUM);
			//this.isValidRow = false;
			errorCode = ERRCODE_INCORR_COL_NUM;
			//return pstmt;
		}
		String column = null;
		String columnValue = null;
		String columnType=null;
		
		int i = 3;
		try {

			pstmt.setLong(1, nextId);
			pstmt.setLong(2, batchId);
			pstmt.setString(3, parser.getFileType());
			//int j = 0;

			for (i = 3; i < columnsLength; i++) {
				try {
					column = importTableColumns[i];
					columnType=importColumnHeaders[i];
					columnValue = nextRowMap.get(columnType.toUpperCase().trim());
					if(columnValue==null || columnValue.equals("")){
						pstmt.setObject(i+1, setNull());
					}else if (column.contains("vc")) {
						pstmt.setString(i + 1, columnValue);
					} else if (column.contains("nu")) {
						if (!"null".equalsIgnoreCase(columnValue)) {
							//pstmt.setLong(i + 1, new Long(columnValue));
							pstmt.setBigDecimal(i + 1, new BigDecimal(columnValue));
						} else {
							pstmt.setLong(i + 1, new Long(0l));
						}
						
					} else if (column.contains("ts")) {
						
						if (!"null".equalsIgnoreCase(columnValue)) {
							Date dateFormat = new Date(columnValue);
							pstmt.setDate(i + 1,
									new java.sql.Date(dateFormat.getTime()));
						} else {
							pstmt.setObject(i+1, setNull());
						}
						
					} else if (column.contains("update_timestamp")) {
						
						if (!"null".equalsIgnoreCase(columnValue)) {
							Date dateFormat = new Date(columnValue);
							pstmt.setDate(i + 1,
									new java.sql.Date(dateFormat.getTime()));
						} else {
							pstmt.setObject(i+1, setNull());
						}
						
					} else if (column.contains("import_table_id")) {
						pstmt.setLong(i + 1, new Long(columnValue));
					} else if (column.contains("ch")) {
						String value =columnValue.substring(0, 1);
						pstmt.setString(i + 1, value);
					} else {
						pstmt.setString(i + 1, columnValue);
					}
				} catch (Exception e) {
					e.printStackTrace();
					parser.addError(lineNumber, i, columnType, columnValue, row,
							ERRCODE_COL_TRANS_ERR);
					this.isValidRow = false;
					pstmt.setObject(i+1, setNull());
					//pstmt.setNull(i+1, SqlTypeValue.TYPE_UNKNOWN);
					if(errorCode==null){
						errorCode=ERRCODE_COL_TRANS_ERR;
					}
				}
			//	j++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			parser.addError(lineNumber, i, columnType, columnValue, row, ERRCODE_COL_TRANS_ERR);
			this.isValidRow = false;
		}
		if (errorCode!=null && parser.getBatchErrorLogs() != null && parser.getBatchErrorLogs().size()>0) {
			List<ListCodes> listCodes = cacheManager.getListCodesByType("ERRORCODE");
			
			for (ListCodes listCode : listCodes) {
				if (listCode.getCodeCode().equals(errorCode)
						&& listCode.getWriteImportLineFlag().equals("1")) {
					this.isValidRow = true;
					break;
				}
			}
		}
		return pstmt;
	}

	private String getImportedQuery(String fileType) {
		DataDefinitionTableDTO dataDefDto=dataDefinitionService.getDataDefinitionByConfigType(fileType);
		return dataDefDto.getImportInsertSql();
	}
    

	
    private Map<String,String> setRowValuesAsMap(String nextRow, String headerColumns, String delimeter, ImportConfigFileUploadParser parser) {
    	String [] headerColumnsArray = headerColumns.split(Pattern.quote(delimeter), headerColumns.length());
    	String [] nextRowValues = nextRow.split(Pattern.quote(delimeter), nextRow.length());
    	int columnsLength = headerColumnsArray.length;
    	Map<String, String> map = new HashMap<String, String>();
    	
    	try {
	    	for(int i = 0; i < columnsLength; i++) {
	    		String headerColumn = headerColumnsArray[i].trim().toUpperCase();
	    		headerColumn = headerColumn.replaceAll("\\p{C}\\p{Z}", "");
//	    		if (i<nextRowValues.length-1) {
	    			map.put(headerColumn , nextRowValues[i]);
//	    		} else {
	    			
//	    			return map;
//	    		}
	    			
	    	}
    	} catch (Exception e) {
			e.printStackTrace();
		}
    	return map;
    }
    
    private String getImportTableColumnHeaders(String importSql) {
		int startIndex = importSql.lastIndexOf("(");
		int endIndex = importSql.lastIndexOf(")");
		return importSql.substring(startIndex + 1, endIndex);
	}
    
    
    private Object setNull(){
    	return null;
    }
    

    
}
