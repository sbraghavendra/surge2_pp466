package com.ncsts.services.impl;


import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.DRIVER_NAMES_TYPE_GOODSNSERVICES;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.DRIVER_NAMES_TYPE_LOCATION;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.DRIVER_NAMES_TYPE_TAX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.ENTITY_CODE_ALL;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.ENTITY_DEFAULT_DISINCITEM;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.ENTITY_DEFAULT_FRTINCITEM;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.GOOD_SERVICE_TYPE_CODE_GOOD;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.GOOD_SERVICE_TYPE_CODE_SERVICE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.LOCATIONPREFERENCE_LOCATIONMATRIX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.LOCATIONPREFERENCE_RATEPOINT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MANUAL_IND_OVERRIDE_ENTERED_AUTOPROC;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MANUAL_IND_OVERRIDE_ENTERED_JURISDICTION;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MANUAL_IND_OVERRIDE_ENTERED_LOCATION;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MANUAL_IND_OVERRIDE_ENTERED_TAXCODE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MANUAL_IND_OVERRIDE_ENTERED_TAXMATRIX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MODULE_CODE_PURCHASE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.MODULE_CODE_SALE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_CITY_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_COUNTRY_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_COUNTY_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_ENTITY_NOT_FOUND_A;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_MISSING_OR_INVALID_OPTION;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_NO_LOCATION_DRIVERS;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_NO_TAX_DRIVERS;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P21;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P22;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P23;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P24;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P26_TAX_MATRIX_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P30;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_P31;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_PS0;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STATE_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STJ1_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STJ2_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STJ3_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STJ4_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.PROCESSING_ERROR_STJ5_TAX_RATE_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.SITUS_TYPE_CODE_SHIPTO;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXCODE_TYPE_CODE_DISTRIBUTE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXCODE_TYPE_CODE_TAXABLE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_GST_INPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_GST_OUTPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_HST_INPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_HST_OUTPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_PST_INPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_PST_OUTPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_QST_INPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_QST_OUTPUT;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_SALE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_USE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TAXTYPE_CODE_VENDOR_USE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANSACTION_TYPE_CODE_BILL;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANSACTION_TYPE_CODE_PURCHASE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_ERROR_PURCHTRANSID_NOT_FOUND;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_ERROR_SUSPENDFLAG_NOT_SET;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_ERROR;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_HELD;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_ORIGINAL;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_PROCESSED;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_PUSH_ALL_TAX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_PUSH_HARD_TAX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_PUSH_PREFERENCE_TAX;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_IND_SUSPENDED;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_GSB;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_LOCATION;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_RATE;
import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.TRANS_SUSPEND_IND_TAXCODE_DTL;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.CITY;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.COUNTY;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STATE;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ1;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ10;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ2;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ3;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ4;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ5;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ6;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ7;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ8;
import static com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference.STJ9;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.util.FieldUtils;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.common.comparator.StringDateComparator;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.JurisdictionLevel;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LocationType;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.ProcessStep;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.RatepointCallType;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.dao.BCPPurchaseTransactionDAO;
import com.ncsts.dao.BCPSaleTransactionDAO;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.dao.BillTransactionDAO;
import com.ncsts.dao.BillTransactionLogDAO;
import com.ncsts.dao.CustDAO;
import com.ncsts.dao.CustLocnDAO;
import com.ncsts.dao.EntityDefaultDAO;
import com.ncsts.dao.EntityItemDAO;
import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.dao.JurisdictionTaxrateDAO;
import com.ncsts.dao.ListCodesDAO;
import com.ncsts.dao.LocationMatrixDAO;
import com.ncsts.dao.PurchaseTransactionDAO;
import com.ncsts.dao.PurchaseTransactionLogDAO;
import com.ncsts.dao.SitusDriverDAO;
import com.ncsts.dao.SitusMatrixDAO;
import com.ncsts.dao.TaxCodeDAO;
import com.ncsts.dao.TaxCodeDetailDAO;
import com.ncsts.dao.TaxCodeStateDAO;
import com.ncsts.dao.TaxMatrixDAO;
import com.ncsts.dao.VendorItemDAO;
import com.ncsts.dao.utils.DAOUtils;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BCPPurchaseTransaction;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.BillTransactionLog;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.DataDefinitionColumnDrivers;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxCodeState;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.TransactionLog;
import com.ncsts.domain.VendorItem;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.dto.JurisdictionFilterDTO;
import com.ncsts.dto.PurchaseTransactionOptions;
import com.ncsts.dto.SitusDriverSitusDriverDetailDTO;
import com.ncsts.dto.SitusInfoDTO;
import com.ncsts.dto.TaxCodeRuleGroupBy;
import com.ncsts.dto.TransientMicroApiTransactionDocument;
import com.ncsts.dto.TransientPurchaseTransaction;
import com.ncsts.dto.TransientPurchaseTransactionDocument;
import com.ncsts.exception.PurchaseTranasactionSoftAbortProcessingException;
import com.ncsts.exception.PurchaseTransactionAbortProcessingException;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.exception.PurchaseTransactionSuspendedException;
import com.ncsts.services.OptionService;
import com.ncsts.services.PurchaseTransactionLogService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.RatepointService;
import com.ncsts.services.VendorNexusDetailService;
import com.ncsts.view.util.ArithmeticUtils;
import com.ncsts.ws.message.BillTransactionDocument;
import com.ncsts.ws.message.MicroApiTransactionDocument;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;
import com.ncsts.ws.message.PurchaseTransactionDocument;

public class PurchaseTransactionServiceImpl implements PurchaseTransactionService {


    @Autowired
    private EntityItemDAO entityItemDAO;

    @Autowired
    private BatchMaintenanceDAO batchMaintenanceDAO;

    @Autowired
    private JurisdictionDAO jurisdictionDAO;

    @Autowired
    private LocationMatrixDAO locationMatrixDAO;

    @Autowired
    private TaxCodeDAO taxCodeDAO;

    @Autowired
    private CacheManager cacheManager;
    
    @Autowired
    private ListCodesDAO listCodesDAO;

    @Autowired
    private PurchaseTransactionDAO purchaseTransactionDAO;

	@Autowired
    private BillTransactionDAO billTransactionDAO;

    @Autowired
    private BCPPurchaseTransactionDAO bcpPurchaseTransactionDAO;
    
    @Autowired
    private BCPSaleTransactionDAO bcpSaleTransactionDAO;

    @Autowired
    private TaxMatrixDAO taxMatrixDAO;

    @Autowired
    private TaxCodeDetailDAO taxCodeDetailDAO;


    @Autowired
    private PurchaseTransactionLogDAO purchaseTransactionLogDAO;
    
    @Autowired
    private BillTransactionLogDAO billTransactionLogDAO;
    
    @Autowired
    private VendorItemDAO vendorItemDAO;

    @Autowired
    @Qualifier("jurisdictionTaxrateDAO")
    private JurisdictionTaxrateDAO jurisdictionTaxrateDao;


    @Autowired
    private EntityDefaultDAO entityDefaultDAO;

    @Autowired
    private SitusDriverDAO situsDriverDAO;

    @Autowired
    private SitusMatrixDAO situsMatrixDAO;

    @Autowired
    private TaxCodeStateDAO taxCodeStateDAO;

    @Autowired
    @Qualifier("purchaseTransactionlLogService")
    private PurchaseTransactionLogService transactionLogService;

    @Autowired
    private OptionService optionService;
    
    @Autowired
    private VendorNexusDetailService vendorNexusDetailService;
    
    @Autowired
	private CustDAO custDAO;
    
    @Autowired
    private CustLocnDAO custLocnDAO;

    private RatepointService ratepointLookupService;

    Logger logger = LoggerFactory.getInstance().getLogger(PurchaseTransactionService.class);
    
    @Override
    public void getTransFullExemption(MicroApiTransaction trans)  throws PurchaseTransactionProcessingException {
    	if(trans.getUnlockEntityId()==null || trans.getUnlockEntityId()==0L) {
    		trans.setUnlockEntityId(0L);
    	}
    	EntityItem unlockEntity = entityItemDAO.findById(trans.getUnlockEntityId());

		// initialize
		trans.setExemptCode(null);
		trans.setExemptAmt(BigDecimal.ZERO);
		trans.setCountryExemptAmt(BigDecimal.ZERO);
		trans.setStateExemptAmt(BigDecimal.ZERO);
		trans.setCountyExemptAmt(BigDecimal.ZERO);
		trans.setCityExemptAmt(BigDecimal.ZERO);
		trans.setStj1ExemptAmt(BigDecimal.ZERO);
		trans.setStj2ExemptAmt(BigDecimal.ZERO);
		trans.setStj3ExemptAmt(BigDecimal.ZERO);
		trans.setStj4ExemptAmt(BigDecimal.ZERO);
		trans.setStj5ExemptAmt(BigDecimal.ZERO);
		trans.setStj6ExemptAmt(BigDecimal.ZERO);
		trans.setStj7ExemptAmt(BigDecimal.ZERO);
		trans.setStj8ExemptAmt(BigDecimal.ZERO);
		trans.setStj9ExemptAmt(BigDecimal.ZERO);
		trans.setStj10ExemptAmt(BigDecimal.ZERO); 

		// Exempt Indicator
		if ((trans.getExemptInd() != null && (trans.getExemptInd()
				.toUpperCase().startsWith("T") || trans.getExemptInd()
				.toUpperCase().startsWith("Y"))) || "1".equals(trans.getExemptInd())) {
			trans.setExemptCode("FI"); // Full Exemption for Indicator

			trans.setExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCountryExemptAmt(trans.getGlLineItmDistAmt()); 
			trans.setStateExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCountyExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCityExemptAmt(trans.getGlLineItmDistAmt());	
			if(trans.getStj1Name() != null) {
				trans.setStj1ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj2Name() != null) {
				trans.setStj2ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj3Name() != null) {
				trans.setStj3ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj4Name() != null) {
				trans.setStj4ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj5Name() != null) {
				trans.setStj5ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj6Name() != null) {
				trans.setStj6ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj7Name() != null) {
				trans.setStj7ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj8Name() != null) {
				trans.setStj8ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj9Name() != null) {
				trans.setStj9ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj10Name() != null) {
				trans.setStj10ExemptAmt(trans.getGlLineItmDistAmt());
			}
			
			trans.setTransactionInd(TRANS_IND_PROCESSED);
			trans.setSuspendInd(null);
			throw new PurchaseTranasactionSoftAbortProcessingException("");
		}
		
		//Customer Exemption Certificate
		boolean isCustmerExempt = false;
		Cust cust = null;
		CustLocn custLocn = null;		
		
		if(StringUtils.hasText(trans.getCustNbr())) {
			cust = custDAO.getCustByNumber(trans.getCustNbr());
			if(cust == null) {
				logger.info(String.format("Cust not found for custNbr: %s", trans.getCustNbr()));
			}
		}
		
		if(cust==null && StringUtils.hasText(trans.getCustName())) {
			cust = custDAO.getCustByName(trans.getCustName());
			if(cust != null && StringUtils.hasText(cust.getCustNbr())) {
				trans.setCustNbr(cust.getCustNbr());
			}
			else {
				logger.info("CustNbr not found for CustName = " + trans.getCustName());
			}
		}
		
		if(cust != null && StringUtils.hasText(trans.getCustNbr())) {
			String exemptReasonCode = null;
			if(StringUtils.hasText(trans.getExemptReason())) {
				ListCodes listCode = listCodesDAO.getListCode("EXREASON", trans.getExemptReason(), trans.getExemptReason());
				if(listCode != null) {
					exemptReasonCode = listCode.getCodeCode();
					trans.setExemptReason(listCode.getDescription());
				}
				else {
					exemptReasonCode = trans.getExemptReason();
				}
			}
			
			if(StringUtils.hasText(trans.getCustLocnCode())) {
				custLocn = custLocnDAO.getCustLocnByCustLocnCode(trans.getCustLocnCode());
				
				if(custLocn == null) {
					logger.info(String.format("CustLocn not found for custLocnCode: %s", trans.getCustLocnCode()));
				}
			}
			else if(StringUtils.hasText(trans.getLocationName())) {
				custLocn = custLocnDAO.getCustLocnByName(trans.getLocationName());
				if(custLocn != null && StringUtils.hasText(custLocn.getCustLocnCode())) {
					trans.setCustLocnCode(custLocn.getCustLocnCode());
				}
			}
			
			if(!StringUtils.hasText(trans.getCertNbr())) {
				if(custLocn != null && StringUtils.hasText(trans.getCustLocnCode())) {
					if(StringUtils.hasText(exemptReasonCode)) {
						//exemptReason is passed
						int count = custDAO.getCustCountWithReasonCode(trans.getCustNbr(), trans.getCustLocnCode(), exemptReasonCode, trans.getGlDate());
						if(count > 0) {
							isCustmerExempt = true;
						}
						else {
							logger.info(String.format("CustNbr: %s is not exempt", trans.getCustNbr()));
							//IF grace days > 0 THEN create a temporary exemption with reason
							if(unlockEntity != null && unlockEntity.getExemptGraceDays() != null && unlockEntity.getExemptGraceDays() > 0) {
								custLocnDAO.createTempExemptionWithReason(custLocn.getCustLocnId(), unlockEntity.getEntityId(), unlockEntity.getExemptGraceDays().intValue(), 
										exemptReasonCode, trans.getUpdateUserId());
								isCustmerExempt = true;
							}
						}
					}
					else {
						//exemptReason is NOT passed
						int count = custDAO.getCustCount(trans.getCustNbr(), trans.getCustLocnCode(), trans.getGlDate());
						if(count > 0) {
							isCustmerExempt = true;
						}
						else {
							logger.info(String.format("CustNbr: %s is not exempt", trans.getCustNbr()));
							//IF grace days > 0 THEN create a temporary exemption reason code TEMPORARY
							if(unlockEntity != null && unlockEntity.getExemptGraceDays() != null && unlockEntity.getExemptGraceDays() > 0) {
								custLocnDAO.createTempExemptionWithReason(custLocn.getCustLocnId(), unlockEntity.getEntityId(), unlockEntity.getExemptGraceDays().intValue(), 
										"TEMPORARY", trans.getUpdateUserId());
								isCustmerExempt = true;
								trans.setExemptReason("TEMPORARY");
							}
						}
					}
				}
				else {
					//Use ST situs values to get customer location
					if (StringUtils.hasText(trans.getShiptoStateCode())) {
						int count = custDAO.getCustCountWithExAndState(trans.getCustNbr(), trans.getShiptoStateCode(), trans.getGlDate());
						if (count > 0) {
							isCustmerExempt = true;
						}
					}
				}
			}
			else {
				//Exception cert is included
				if(custLocn != null && StringUtils.hasText(trans.getCustLocnCode())) {
					if(StringUtils.hasText(exemptReasonCode)) {
						//exemptReason is passed
						int count = custDAO.getCustCountWithReasonCodeAndCertNbr(trans.getCustNbr(), trans.getCustLocnCode(), trans.getCertNbr(), exemptReasonCode, trans.getGlDate());
						if(count > 0) {
							isCustmerExempt = true;
						}
						else {
							logger.info(String.format("CustNbr: %s is not except", trans.getCustNbr()));
							//IF grace days > 0 THEN create a temporary exemption with reason
							if(unlockEntity != null && unlockEntity.getExemptGraceDays() != null && unlockEntity.getExemptGraceDays() > 0) {
								custLocnDAO.createTempExemptionWithReason(custLocn.getCustLocnId(), unlockEntity.getEntityId(), unlockEntity.getExemptGraceDays().intValue(), 
										exemptReasonCode, trans.getUpdateUserId());
								isCustmerExempt = true;
							}
						}
					}
					else {
						//exemptReason is NOT passed
						int count = custDAO.getCustCountWithCertNbr(trans.getCustNbr(), trans.getCustLocnCode(), trans.getCertNbr(), trans.getGlDate());
						if(count > 0) {
							isCustmerExempt = true;
						}
						else {
							logger.info(String.format("CustNbr: %s is not except", trans.getCustNbr()));
							//IF grace days > 0 THEN create a temporary exemption reason code TEMPORARY
							if(unlockEntity != null && unlockEntity.getExemptGraceDays() != null && unlockEntity.getExemptGraceDays() > 0) {
								custLocnDAO.createTempExemptionWithReason(custLocn.getCustLocnId(), unlockEntity.getEntityId(), unlockEntity.getExemptGraceDays().intValue(), 
										"TEMPORARY", trans.getUpdateUserId());
								isCustmerExempt = true;
								trans.setExemptReason("TEMPORARY");
							}
						}
					}
				}
				else {
					if(trans.getShiptoGeocode() != null) {
						int count = custDAO.getCustCountWithCertNbrWithoutLocnCode(trans.getCustNbr(), trans.getCertNbr(), trans.getGlDate(), 
								trans.getShiptoGeocode(), trans.getShiptoAddressLine1(), trans.getShiptoAddressLine2(), trans.getShiptoCity(), 
								trans.getShiptoCounty(), trans.getShiptoStateCode(), trans.getShiptoZip(), trans.getShiptoCountryCode());
						if(count > 0) {
							isCustmerExempt = true;
						}
						else {
							logger.info(String.format("CustNbr: %s, CertNbr: %s is not exempt", trans.getCustNbr(), trans.getCertNbr()));					
							//IF grace days > 0 THEN create a temporary cert with customer location code = the primary situs state and exempt reason code=�TEMPORARY�
							if(unlockEntity != null && unlockEntity.getExemptGraceDays() != null && unlockEntity.getExemptGraceDays() > 0) {
								Long custLocnId = custLocnDAO.getNextCustLocnId();
								custLocnDAO.createCustLocn(custLocnId, cust.getCustId(), custLocnId + "", trans.getShiptoStateCode(), trans.getShiptoGeocode(), 
									trans.getShiptoAddressLine1(), trans.getShiptoAddressLine2(), trans.getShiptoCity(), trans.getShipfromCounty(), 
									trans.getShiptoStateCode(), trans.getShiptoZip(), trans.getShiptoZipplus4(), trans.getShiptoCountryCode(), trans.getUpdateUserId());
								custLocnDAO.createTempExemptionWithReason(custLocnId, unlockEntity.getEntityId(), unlockEntity.getExemptGraceDays().intValue(), "TEMPORARY", trans.getUpdateUserId());
								isCustmerExempt = true;
								trans.setExemptReason("TEMPORARY");
							}
						}
					}
				}
			}
		}
		
		if(isCustmerExempt) {
			trans.setExemptCode("FR"); // Full Exemption for Reason
			trans.setExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCountryExemptAmt(trans.getGlLineItmDistAmt());
			trans.setStateExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCountyExemptAmt(trans.getGlLineItmDistAmt());
			trans.setCityExemptAmt(trans.getGlLineItmDistAmt());
			if(trans.getStj1Name() != null) {
				trans.setStj1ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj2Name() != null) {
				trans.setStj2ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj3Name() != null) {
				trans.setStj3ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj4Name() != null) {
				trans.setStj4ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj5Name() != null) {
				trans.setStj5ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj6Name() != null) {
				trans.setStj6ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj7Name() != null) {
				trans.setStj7ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj8Name() != null) {
				trans.setStj8ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj9Name() != null) {
				trans.setStj9ExemptAmt(trans.getGlLineItmDistAmt());
			}
			if(trans.getStj10Name() != null) {
				trans.setStj10ExemptAmt(trans.getGlLineItmDistAmt());
			}

			trans.setTransactionInd(TRANS_IND_PROCESSED);
			trans.setSuspendInd(null);
			throw new PurchaseTranasactionSoftAbortProcessingException("");
		}
	}
    
    public void billTransactionProcess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	billTransactionProcess(document, writeFlag, LogSourceEnum.BillTransactionProcess);
    }

    public void billTransactionProcess(MicroApiTransactionDocument document, Boolean writeFlag,LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
        List<MicroApiTransaction> originals = document.getMicroApiTransaction();
        LinkedList<MicroApiTransaction> workQueue = new LinkedList<>();
        for (MicroApiTransaction trans : originals)
            workQueue.add(trans);
        originals.clear();

        while (!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();

            if (transientPurchaseTransaction.getPurchasingOptions() == null)
                transientPurchaseTransaction.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientPurchaseTransaction.getRefreshCacheFlag()));
            Map<OptionCodePK, String> purchasingOptions = transientPurchaseTransaction.getPurchasingOptions();
            setDelete0Amount(transientPurchaseTransaction);
            if (transientPurchaseTransaction.getDelete0Amount() &&
                    (purchaseTransaction.getInvoiceLineGrossAmt() == null || purchaseTransaction.getGlLineItmDistAmt().compareTo(BigDecimal.ZERO) != 0)) {
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                throw new PurchaseTranasactionSoftAbortProcessingException("");
            }

            if (!checkCalcIndicator(purchaseTransaction.getCalculateInd())){
            	originals.add(purchaseTransaction);
            	continue; //Save trans with calculateInd = 0
            }


            enableAllCalcFlags(transientPurchaseTransaction);

            //boolean localWrite = true;
            TransactionLog log = null;
                log = new TransactionLog();
//                if(purchaseTransaction.isBillSaleModuleCode()) {
//                	log.setLogSource(ProcessStep.billTransactionProcess.name());
//                }
//                else {
//                	log.setLogSource(ProcessStep.purchaseTransactionProcess.name());
//                }  raghu
            log.setLogSource(callingFromMethod.name());  //raghu enum
            
            transactionLogService.fillBeforeEntries(log, purchaseTransaction);//Need to check for Bill
            transientPurchaseTransaction.setTransactionLog(log);
            zeroBigDecimalsIfNull(purchaseTransaction);

            /*
            * TRANS.multi_trans_code like "O%"
            **/
            if (purchaseTransaction.getMultiTransCode() != null && purchaseTransaction.getMultiTransCode().length() > 0
                    && "o".equalsIgnoreCase(purchaseTransaction.getMultiTransCode().substring(0, 1))) {
                /*
                * TRANS.transaction_ind = "O"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_ORIGINAL);
                purchaseTransaction.setSuspendInd(null);
                throw new PurchaseTranasactionSoftAbortProcessingException(TRANS_IND_ORIGINAL);
            }

            /*
            * TRANS.multi_trans_code = "FS"
            * */
            if (purchaseTransaction.getMultiTransCode() != null
                    && "FS".equalsIgnoreCase(purchaseTransaction.getMultiTransCode())) {
                /*
                * TRANS.transaction_ind = "H"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_HELD);
                purchaseTransaction.setSuspendInd(null);
                throw new PurchaseTranasactionSoftAbortProcessingException(TRANS_IND_HELD);
            }
            
			//PARM.reprocessFlag = "1"
            if(purchaseTransaction.getReprocessFlag()!=null && "1".equals(purchaseTransaction.getReprocessFlag())){
            	// call transReprocess
            	getTransReprocess(purchaseTransaction);
    		}

            // TODO REVERSE FLAG?
/*
            if (true) {
                getTransReverse(purchaseTransaction);
                continue;
            }
*/
            setProcruleFlag(transientPurchaseTransaction);

            if (transientPurchaseTransaction.getProcRuleEnabled() &&
                    (transientPurchaseTransaction.getPreProcessFlag() != null && transientPurchaseTransaction.getPreProcessFlag())) {
                /// run PRE processing rules!
            }
            
            if (purchaseTransaction.getGlDate() == null)
                purchaseTransaction.setGlDate(new Date());


            if (purchaseTransaction.getAutoTransactionCountryCode() == null) {
                if (purchaseTransaction.getTransactionCountryCode() == null) {
                    purchaseTransaction.setAutoTransactionCountryCode("*NULL");
                } else {
                    purchaseTransaction.setAutoTransactionCountryCode(purchaseTransaction.getTransactionCountryCode());
                }
            }

            if (purchaseTransaction.getAutoTransactionStateCode() == null) {
                if (purchaseTransaction.getTransactionStateCode() == null) {
                    purchaseTransaction.setAutoTransactionStateCode("*NULL");
                } else {
                    purchaseTransaction.setAutoTransactionStateCode(purchaseTransaction.getTransactionStateCode());
                }
            }

            try {
                getTransEntity(purchaseTransaction);
                getTransLocation(purchaseTransaction, LocationType.SHIPTO, "0");
                getTransLocation(purchaseTransaction, LocationType.SHIPFROM, "0");
                getTransLocation(purchaseTransaction, LocationType.BILLTO, "0");
                getTransLocation(purchaseTransaction, LocationType.FIRSTUSE, "0");
                getTransLocation(purchaseTransaction, LocationType.ORDERORIGIN, "0");
                getTransLocation(purchaseTransaction, LocationType.ORDERACCEPT, "0");
                getTransLocation(purchaseTransaction, LocationType.TITLETRANSFER, "0");
                getSitus(purchaseTransaction);
                getTransEntityNexus(purchaseTransaction);//Change from getTransVendorNexus to getTransEntityNexus
                getTransFullExemption(purchaseTransaction);
                
                //Initialize TaxcodeTypeCode fields
				purchaseTransaction.setCountryTaxcodeTypeCode("T");
				purchaseTransaction.setStateTaxcodeTypeCode("T");
				purchaseTransaction.setCountyTaxcodeTypeCode("T");
				purchaseTransaction.setCityTaxcodeTypeCode("T");
				if(purchaseTransaction.getStj1Name() != null) {
					purchaseTransaction.setStj1TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj2Name() != null) {
					purchaseTransaction.setStj2TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj3Name() != null) {
					purchaseTransaction.setStj3TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj4Name() != null) {
					purchaseTransaction.setStj4TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj5Name() != null) {
					purchaseTransaction.setStj5TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj6Name() != null) {
					purchaseTransaction.setStj6TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj7Name() != null) {
					purchaseTransaction.setStj7TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj8Name() != null) {
					purchaseTransaction.setStj8TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj9Name() != null) {
					purchaseTransaction.setStj9TaxcodeTypeCode("T");
				}
				if(purchaseTransaction.getStj10Name() != null) {
					purchaseTransaction.setStj10TaxcodeTypeCode("T");
				} 
				
                getTransGoodSvc(purchaseTransaction);
                
                if(purchaseTransaction.getTaxcodeCode()!=null && purchaseTransaction.getTaxcodeCode().length()>0) {
                	getTransTaxCodeDtl(purchaseTransaction, document);     
                }

                resetRatesSetFlags(transientPurchaseTransaction);
                getTransTaxRates(purchaseTransaction);
            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                //localWrite = false;
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } finally {
                originals.add(purchaseTransaction);
            }
        }
        /*pass 1 end*/

        /*pass 2 start  distbuckets */
        for (MicroApiTransaction transaction : originals) {
            if (transaction.getTransientTransaction().getStopProcessing() == Boolean.TRUE)
                continue;

            if (TAXCODE_TYPE_CODE_DISTRIBUTE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())) {
                calcDistBuckets(transaction, document);
            }
        }
        /*pass 2 end*/

        /*pass 3 start*/
        /* bypassing  "Group by" */
        /*pass 3 end*/

        /* pass 4 start - calc all taxes */
        for (MicroApiTransaction trans: originals)
            workQueue.add(trans);

        originals.clear();


        while(!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();

            try {
            	if (checkCalcIndicator(purchaseTransaction.getCalculateInd())) {
	                if (transientPurchaseTransaction.getStopProcessing() == Boolean.TRUE)
	                    continue;
	
	                	getTransCalcAllTaxes(purchaseTransaction, document);
	
	                if(purchaseTransaction.getInvoiceVerificationFlag()!=null && purchaseTransaction.getInvoiceVerificationFlag()){
	                    this.getTransVariance(purchaseTransaction);
	                }
            	}
            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            }
            finally {
                originals.add(purchaseTransaction);
            }
        }

        try {
            calcMaxTax(document);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        /* pass 4 end */

        /* Push taxes */
               /*
                * public static final String TRANS_IND_PUSH_ALL_TAX = "P1";
                  public static final String TRANS_IND_PUSH_PREFERENCE_TAX = "P2";
                  public static final String TRANS_IND_PUSH_HARD_TAX = "P3";
                * */

        int placeholder = 0;
        if (TRANS_IND_PUSH_ALL_TAX.equals(document.getPushTaxInd())) {
            pushAllTaxes(document);
        } else if (TRANS_IND_PUSH_PREFERENCE_TAX.equals(document.getPushTaxInd())) {
            pushPreferenceTax(document);
        } else if (TRANS_IND_PUSH_HARD_TAX.equals(document.getPushTaxInd())) {
            pushHardTax(document);
        }

        /*Pass 5*/
        for (MicroApiTransaction trans: originals)
            workQueue.add(trans);

        originals.clear();

        while(!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();
                      
            try {
                if (purchaseTransaction.getTransientTransaction().getStopProcessing() == Boolean.TRUE)
                    continue;

                if (purchaseTransaction.getInvoiceVerificationFlag() != null && purchaseTransaction.getInvoiceVerificationFlag()) {
                    this.getTransVariance(purchaseTransaction);
                }

                getTransStatus(purchaseTransaction);

                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {

            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
            } finally {
                originals.add(purchaseTransaction);
            }
        }

        for (MicroApiTransaction purchaseTransaction: originals) {
            if (writeFlag && purchaseTransaction.getTransientTransaction().getLocalWrite() &&
                    purchaseTransaction.getTransientTransaction().getTransactionLog() != null) {


                TransactionLog log = purchaseTransaction.getTransientTransaction().getTransactionLog();
                transactionLogService.fillAfterEntries(log, purchaseTransaction);

                if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                    purchaseTransaction.setPurchtransId(null);

                switch (purchaseTransaction.getModuleCode()) {
	    			case PURCHUSE:
	    				break;
	    	
	    			case BILLSALE: //billTransactionProcess(MicroApiTransactionDocument)
	    				BillTransaction updatedBillTransaction = new BillTransaction();
	    				purchaseTransaction.updateBillTransaction(updatedBillTransaction);
	    				billTransactionDAO.save(updatedBillTransaction);
	    				purchaseTransaction.setBilltransId(updatedBillTransaction.getBilltransId());    				
	    				log.setBilltransId(purchaseTransaction.getBilltransId());
	                    try {
	                    	BillTransactionLog billTransactionLog = new BillTransactionLog();
	                		BeanUtils.copyProperties(log, billTransactionLog);
	                		billTransactionLogDAO.save(billTransactionLog);
	                		log.setBilltransLogId(billTransactionLog.getBilltransLogId());
	                    } catch (DataAccessException dte) {
	                        purchaseTransaction.setErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setFatalErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setErrorText("Failed to write PurchaseTransaction log! \n"+dte.getMessage());
	                    }
	                    
	    				break;
	    	
	    			default:
	    				break;
                }
            }
        }
    }

    public void billTransactionLogSave(MicroApiTransactionDocument doc) {
    	
    	for(MicroApiTransaction purchaseTransaction : doc.getMicroApiTransaction()) {
    		
    		if(purchaseTransaction.getTransientTransaction().getTransactionLog() != null) {
            TransactionLog log = purchaseTransaction.getTransientTransaction().getTransactionLog();
            transactionLogService.fillAfterEntries(log, purchaseTransaction);
    		
    		
    		 if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                 purchaseTransaction.setPurchtransId(null);

             switch (purchaseTransaction.getModuleCode()) {
	    			case PURCHUSE:
	    				break;
	    	
	    			case BILLSALE: //billTransactionProcess(MicroApiTransactionDocument)
	                    try {
	                    	BillTransactionLog billTransactionLog = new BillTransactionLog();
	                		BeanUtils.copyProperties(log, billTransactionLog);
	                		billTransactionLogDAO.save(billTransactionLog);
	                		log.setBilltransLogId(billTransactionLog.getBilltransLogId());
	                    } catch (DataAccessException dte) {
	                        purchaseTransaction.setErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setFatalErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setErrorText("Failed to write PurchaseTransaction log! \n"+dte.getMessage());
	                    }
	                    
	    				break;
	    	
	    			default:
	    				break;
             }
             }
         }
     }
    
    
    //raghu
    public void taxEstimate(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	taxEstimate(document, writeFlag, LogSourceEnum.TaxEstimate);
    }

    
    @Override
    public void taxEstimate(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
        List<MicroApiTransaction> originals = document.getMicroApiTransaction();
        LinkedList<MicroApiTransaction> workQueue = new LinkedList<>();

        for (MicroApiTransaction trans : originals)
            workQueue.add(trans);
        originals.clear();

        while (!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();

            if (transientPurchaseTransaction.getPurchasingOptions() == null)
                transientPurchaseTransaction.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientPurchaseTransaction.getRefreshCacheFlag()));
            Map<OptionCodePK, String> purchasingOptions = transientPurchaseTransaction.getPurchasingOptions();
            setDelete0Amount(transientPurchaseTransaction);
            if (transientPurchaseTransaction.getDelete0Amount() &&
                    (purchaseTransaction.getGlLineItmDistAmt() == null || purchaseTransaction.getGlLineItmDistAmt().compareTo(BigDecimal.ZERO) != 0)) {
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                throw new PurchaseTranasactionSoftAbortProcessingException("");
            }

            if (!checkCalcIndicator(purchaseTransaction.getCalculateInd())) {
            	originals.add(purchaseTransaction);
            	continue; //Save trans with calculateInd = 0
            }            

            enableAllCalcFlags(transientPurchaseTransaction);

            //boolean localWrite = true;
            TransactionLog log = null;
            if (writeFlag) {
                log = new TransactionLog();
//                if(purchaseTransaction.isBillSaleModuleCode()) {
//                	log.setLogSource(ProcessStep.billTransactionProcess.name());
//                }
//                else {
//                	log.setLogSource(ProcessStep.purchaseTransactionProcess.name());
//                }  raghu
                
                  log.setLogSource(callingFromMethod.name());  //raghu enu
              
                transactionLogService.fillBeforeEntries(log, purchaseTransaction);//Need to check for Bill
                transientPurchaseTransaction.setTransactionLog(log);
            }
            zeroBigDecimalsIfNull(purchaseTransaction);

            /*
            * TRANS.multi_trans_code like "O%"
            **/
            if (purchaseTransaction.getMultiTransCode() != null && purchaseTransaction.getMultiTransCode().length() > 0
                    && "o".equalsIgnoreCase(purchaseTransaction.getMultiTransCode().substring(0, 1))) {
                /*
                * TRANS.transaction_ind = "O"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_ORIGINAL);
                purchaseTransaction.setSuspendInd(null);
                throw new PurchaseTranasactionSoftAbortProcessingException(TRANS_IND_ORIGINAL);
            }

            /*
            * TRANS.multi_trans_code = "FS"
            * */
            if (purchaseTransaction.getMultiTransCode() != null
                    && "FS".equalsIgnoreCase(purchaseTransaction.getMultiTransCode())) {
                /*
                * TRANS.transaction_ind = "H"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_HELD);
                purchaseTransaction.setSuspendInd(null);
                throw new PurchaseTranasactionSoftAbortProcessingException(TRANS_IND_HELD);
            }
            
			//PARM.reprocessFlag = "1"
            if(purchaseTransaction.getReprocessFlag()!=null && "1".equals(purchaseTransaction.getReprocessFlag())){
            	// call transReprocess
            	getTransReprocess(purchaseTransaction);
    		}

            // TODO REVERSE FLAG?
/*
            if (true) {
                getTransReverse(purchaseTransaction);
                continue;
            }
*/
            setProcruleFlag(transientPurchaseTransaction);

            if (transientPurchaseTransaction.getProcRuleEnabled() &&
                    (transientPurchaseTransaction.getPreProcessFlag() != null && transientPurchaseTransaction.getPreProcessFlag())) {
                /// run PRE processing rules!
            }

            if (purchaseTransaction.getGlDate() == null)
                purchaseTransaction.setGlDate(new Date());


            if (purchaseTransaction.getAutoTransactionCountryCode() == null) {
                if (purchaseTransaction.getTransactionCountryCode() == null) {
                    purchaseTransaction.setAutoTransactionCountryCode("*NULL");
                } else {
                    purchaseTransaction.setAutoTransactionCountryCode(purchaseTransaction.getTransactionCountryCode());
                }
            }

            if (purchaseTransaction.getAutoTransactionStateCode() == null) {
                if (purchaseTransaction.getTransactionStateCode() == null) {
                    purchaseTransaction.setAutoTransactionStateCode("*NULL");
                } else {
                    purchaseTransaction.setAutoTransactionStateCode(purchaseTransaction.getTransactionStateCode());
                }
            }

            boolean directPayPermitFlag = (purchaseTransaction.getDirectPayPermitFlag() != null && (purchaseTransaction.getDirectPayPermitFlag().equalsIgnoreCase("y")
                    || purchaseTransaction.getDirectPayPermitFlag().equalsIgnoreCase("t")
                    || purchaseTransaction.getDirectPayPermitFlag().equalsIgnoreCase("1"))) ? true : false;


            try {
                getTransEntity(purchaseTransaction);

                if (directPayPermitFlag) {
                    getTransLocation(purchaseTransaction, LocationType.SHIPTO, "1");
                } else {
                    getTransLocation(purchaseTransaction, LocationType.SHIPTO, "0");
                    getTransLocation(purchaseTransaction, LocationType.SHIPFROM, "0");
                    getTransLocation(purchaseTransaction, LocationType.BILLTO, "0");
                    getTransLocation(purchaseTransaction, LocationType.FIRSTUSE, "0");
                    getTransLocation(purchaseTransaction, LocationType.ORDERORIGIN, "0");
                    getTransLocation(purchaseTransaction, LocationType.ORDERACCEPT, "0");
                    getTransLocation(purchaseTransaction, LocationType.TITLETRANSFER, "0");
                    getSitus(purchaseTransaction);
                }

                getTransVendorNexus(purchaseTransaction, directPayPermitFlag);


                getTransGoodSvc(purchaseTransaction);
                getTransTaxCodeDtl(purchaseTransaction, document);
                // if (directPayPermitFlag)


                resetRatesSetFlags(transientPurchaseTransaction);

                getTransTaxRates(purchaseTransaction);




            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                //localWrite = false;
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                //purchaseTransaction.setSuspendInd(TRANS_SUSPEND_IND_UNKNOWN);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } finally {
                originals.add(purchaseTransaction);
            }
        }
        /*pass 1 end*/

        /*pass 2 start  distbuckets */
        for (MicroApiTransaction transaction : originals) {
            if (transaction.getTransientTransaction().getStopProcessing() == Boolean.TRUE)
                continue;

            if (TAXCODE_TYPE_CODE_DISTRIBUTE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())) {
                calcDistBuckets(transaction, document);
            }
        }
        /*pass 2 end*/

        /*pass 3 start*/
        /* bypassing  "Group by" */
        /*pass 3 end*/

        /* pass 4 start - calc all taxes */
        for (MicroApiTransaction trans: originals)
            workQueue.add(trans);

        originals.clear();


        while(!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();

            try {     	
            	if (checkCalcIndicator(purchaseTransaction.getCalculateInd())) {
	                if (transientPurchaseTransaction.getStopProcessing() == Boolean.TRUE)
	                    continue;
	
	                	getTransCalcAllTaxes(purchaseTransaction, document);
	
	                if(purchaseTransaction.getInvoiceVerificationFlag()!=null && purchaseTransaction.getInvoiceVerificationFlag()){
	                    this.getTransVariance(purchaseTransaction);
	                }
            	}
            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                //purchaseTransaction.setSuspendInd(TRANS_SUSPEND_IND_UNKNOWN);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
            }
            finally {
                originals.add(purchaseTransaction);
            }
        }

        try {
            calcMaxTax(document);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        /* pass 4 end */

        /* Push taxes */
               /*
                * public static final String TRANS_IND_PUSH_ALL_TAX = "P1";
                  public static final String TRANS_IND_PUSH_PREFERENCE_TAX = "P2";
                  public static final String TRANS_IND_PUSH_HARD_TAX = "P3";
                * */

        int placeholder = 0;
        if (TRANS_IND_PUSH_ALL_TAX.equals(document.getPushTaxInd())) {
            pushAllTaxes(document);
        } else if (TRANS_IND_PUSH_PREFERENCE_TAX.equals(document.getPushTaxInd())) {
            pushPreferenceTax(document);
        } else if (TRANS_IND_PUSH_HARD_TAX.equals(document.getPushTaxInd())) {
            pushHardTax(document);
        }

        /*Pass 5*/
        for (MicroApiTransaction trans: originals)
            workQueue.add(trans);

        originals.clear();

        while(!workQueue.isEmpty()) {

        	MicroApiTransaction purchaseTransaction = workQueue.poll();
            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();
                      
            try {
                if (purchaseTransaction.getTransientTransaction().getStopProcessing() == Boolean.TRUE)
                    continue;

                if (purchaseTransaction.getInvoiceVerificationFlag() != null && purchaseTransaction.getInvoiceVerificationFlag()) {
                    this.getTransVariance(purchaseTransaction);
                }

                getTransStatus(purchaseTransaction);

                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
            } catch (PurchaseTranasactionSoftAbortProcessingException ste) {

            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                transientPurchaseTransaction.setLocalWrite(Boolean.FALSE);
                transientPurchaseTransaction.setStopProcessing(Boolean.TRUE);
                purchaseTransaction.setErrorText(pte.getMessage());
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                //purchaseTransaction.setSuspendInd(TRANS_SUSPEND_IND_UNKNOWN);
            } finally {
                originals.add(purchaseTransaction);
            }
        }

        for (MicroApiTransaction purchaseTransaction: originals) {
            if (writeFlag && purchaseTransaction.getTransientTransaction().getLocalWrite() &&
                    purchaseTransaction.getTransientTransaction().getTransactionLog() != null) {


                TransactionLog log = purchaseTransaction.getTransientTransaction().getTransactionLog();
                transactionLogService.fillAfterEntries(log, purchaseTransaction);

                if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                    purchaseTransaction.setPurchtransId(null);

                switch (purchaseTransaction.getModuleCode()) {
	    			case PURCHUSE: //taxEstimate(MicroApiTransactionDocument)
	    				PurchaseTransaction updatedPurchaseTransaction = new PurchaseTransaction();
	    				purchaseTransaction.updatePurchaseTransaction(updatedPurchaseTransaction);
	    				purchaseTransactionDAO.save(updatedPurchaseTransaction);
	    				purchaseTransaction.setPurchtransId(updatedPurchaseTransaction.getPurchtransId());
	    				
	    				log.setPurchtransId(purchaseTransaction.getPurchtransId());
	                    try {
	                        PurchaseTransactionLog purchaseTransactionLog = new PurchaseTransactionLog();
		    	    		BeanUtils.copyProperties(log, purchaseTransactionLog);
		    	    		purchaseTransactionLogDAO.save(purchaseTransactionLog);
		    	    		log.setPurchtransLogId(purchaseTransactionLog.getPurchtransLogId());
	                    } catch (DataAccessException dte) {
	                        purchaseTransaction.setErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setFatalErrorCode(BAD_RETURN_ON_TRANSACTION_DETAIL_WRITE);
	                        purchaseTransaction.setErrorText("Failed to write PurchaseTransaction log! \n"+dte.getMessage());
	                    }
	                    
	    				break;
	    	
	    			case BILLSALE:
	    				//To DO:
	    				break;
	    	
	    			default:
	    				break;
                }
            }
        }

    }
    
    public void normalProcess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	normalProcess(document, writeFlag, LogSourceEnum.normalProcess);
    }

    @Override
    public void normalProcess(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
        List<MicroApiTransaction> transactions = document.getMicroApiTransaction();

        for (MicroApiTransaction purchaseTransaction: transactions) {

            TransientPurchaseTransaction transientPurchaseTransaction = purchaseTransaction.getTransientTransaction();

            if (transientPurchaseTransaction.getPurchasingOptions() == null)
                transientPurchaseTransaction.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientPurchaseTransaction.getRefreshCacheFlag()));
            Map<OptionCodePK, String> purchasingOptions = transientPurchaseTransaction.getPurchasingOptions();

            setDelete0Amount(transientPurchaseTransaction);

            if (transientPurchaseTransaction.getDelete0Amount())
                if (purchaseTransaction.getGlLineItmDistAmt() != null && BigDecimal.ZERO.compareTo(purchaseTransaction.getGlLineItmDistAmt()) != 0)
                    continue;


            String calcInd = purchaseTransaction.getCalculateInd();

            if (!checkCalcIndicator(calcInd))
                continue;



            enableAllCalcFlags(transientPurchaseTransaction);


            boolean localWrite = true;
            TransactionLog log = null;
            if (writeFlag) {
                log = new TransactionLog();
//                if(purchaseTransaction.isBillSaleModuleCode()) {
//                	log.setLogSource(ProcessStep.billTransactionProcess.name());
//                }
//                else {
//                	log.setLogSource(ProcessStep.purchaseTransactionProcess.name());
//                }  raghu
            	
            	log.setLogSource(callingFromMethod.name());  //raghu enum
                transactionLogService.fillBeforeEntries(log, purchaseTransaction);
            }

            /*
            * TRANS.multi_trans_code like "O%"
            **/
            if (purchaseTransaction.getMultiTransCode() != null && purchaseTransaction.getMultiTransCode().length()>0
                    && "o".equalsIgnoreCase(purchaseTransaction.getMultiTransCode().substring(0, 1))) {
                /*
                * TRANS.transaction_ind = "O"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_ORIGINAL);
                purchaseTransaction.setSuspendInd(null);
                continue;
            }

            /*
            * TRANS.multi_trans_code = "FS"
            * */
            if (purchaseTransaction.getMultiTransCode() != null
                    && "FS".equalsIgnoreCase(purchaseTransaction.getMultiTransCode())) {
                /*
                * TRANS.transaction_ind = "H"
                  TRANS.suspend_ind = NULL
                * */
                purchaseTransaction.setTransactionInd(TRANS_IND_HELD);
                purchaseTransaction.setSuspendInd(null);
                continue;
            }
            
            if(purchaseTransaction.getReprocessFlag()!=null && "1".equals(purchaseTransaction.getReprocessFlag())){
            	// call transReprocess
            	getTransReprocess(purchaseTransaction);
    		}

            setProcruleFlag(transientPurchaseTransaction);

            if (transientPurchaseTransaction.getProcRuleEnabled() &&
                    (transientPurchaseTransaction.getPreProcessFlag() != null && transientPurchaseTransaction.getPreProcessFlag())) {
                /// run PRE processing rules!
            }

            if (purchaseTransaction.getGlDate() == null)
                purchaseTransaction.setGlDate(new Date());

            if (purchaseTransaction.getAutoTransactionCountryCode() == null) {
                if (purchaseTransaction.getTransactionCountryCode() == null) {
                    purchaseTransaction.setAutoTransactionCountryCode("*NULL");
                }
                else {
                    purchaseTransaction.setAutoTransactionCountryCode(purchaseTransaction.getTransactionCountryCode());
                }
            }

            if (purchaseTransaction.getAutoTransactionStateCode() == null) {
                if (purchaseTransaction.getTransactionStateCode() == null) {
                    purchaseTransaction.setAutoTransactionStateCode("*NULL");
                } else {
                    purchaseTransaction.setAutoTransactionStateCode(purchaseTransaction.getTransactionStateCode());
                }
            }



            try {
                getTransEntity(purchaseTransaction);
                getTransLocation(purchaseTransaction, LocationType.SHIPTO, "1");
                getTransGoodSvc(purchaseTransaction);
                getTransTaxCodeDtl(purchaseTransaction, document);
                getTransTaxRates(purchaseTransaction);
                getTransCalcAllTaxes(purchaseTransaction, document);
                getTransStatus(purchaseTransaction);

                transientPurchaseTransaction.setProcessingLog(transientPurchaseTransaction.getProcessingLog() +" -"+ ProcessStep.purchaseTransactionProcess.name());

                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
            } catch (PurchaseTransactionAbortProcessingException pte) {
                pte.printStackTrace();
                localWrite = false;
            } catch (PurchaseTransactionSuspendedException ste) {
                // do nothing yet
                //purchaseTransaction.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
                purchaseTransaction.setTransactionInd(TRANS_IND_SUSPENDED);
                //purchaseTransaction.setSuspendInd(TRANS_SUSPEND_IND_UNKNOWN);
            }

            if (writeFlag && localWrite) {
                transactionLogService.fillAfterEntries(log, purchaseTransaction);

                if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                    purchaseTransaction.setPurchtransId(null);

                switch (purchaseTransaction.getModuleCode()) {
	    			case PURCHUSE:
	    				PurchaseTransaction updatedPurchaseTransaction = new PurchaseTransaction();
	    				purchaseTransaction.updatePurchaseTransaction(updatedPurchaseTransaction);
	    				purchaseTransactionDAO.save(updatedPurchaseTransaction);
	    				purchaseTransaction.setPurchtransId(updatedPurchaseTransaction.getPurchtransId());
	    				
	    				log.setPurchtransId(purchaseTransaction.getPurchtransId());
	    				
	    				PurchaseTransactionLog purchaseTransactionLog = new PurchaseTransactionLog();
	    	    		BeanUtils.copyProperties(log, purchaseTransactionLog);
	    	    		purchaseTransactionLogDAO.save(purchaseTransactionLog);
	    	    		log.setPurchtransLogId(purchaseTransactionLog.getPurchtransLogId());
  
	    				break;
	    	
	    			case BILLSALE:
	    				//To DO:
	    				break;
	    	
	    			default:
	    				break;
	            }

            }
        }
    }

    @Override
    public void getTransEntity(MicroApiTransaction purchaseTransaction) throws PurchaseTransactionProcessingException {
        logger.debug("Entering getTransEntity");

        if (purchaseTransaction == null) throw new PurchaseTransactionAbortProcessingException(PROCESSING_ERROR_PS0);
        try {
            TransientPurchaseTransaction transientDetails = purchaseTransaction.getTransientTransaction();
            if (transientDetails.getPurchasingOptions() == null)
                transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
            Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();

            EntityItem entityItem = null;
            if (purchaseTransaction.getEntityId() != null) {
                entityItem = entityItemDAO.findById(purchaseTransaction.getEntityId());
            }

            if (entityItem == null)
                entityItem = entityItemDAO.findByCode(purchaseTransaction.getEntityCode());

            if (entityItem == null) {
                entityItem = entityItemDAO.findById(0L);
                if (entityItem == null) {
                    purchaseTransaction.setErrorCode(PROCESSING_ERROR_ENTITY_NOT_FOUND_A);
                    purchaseTransaction.setAbortFlag(true);
                    throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_ENTITY_NOT_FOUND_A);
                }
                purchaseTransaction.setEntityCode(ENTITY_CODE_ALL);
            }

            purchaseTransaction.setEntityId(entityItem.getEntityId());
            purchaseTransaction.setEntityCode(entityItem.getEntityCode());

            /// Getting an unlocked entity
            EntityItem unlockedEntity = null;

            if (entityItem.getLockBooleanFlag()) {
                Long parentEntityId = entityItem.getParentEntityId();
                Boolean lockFlag = entityItem.getLockBooleanFlag();
                while (lockFlag) {
                    if (parentEntityId == null) {
                        purchaseTransaction.setErrorCode(PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND);
                        purchaseTransaction.setAbortFlag(true);
                        logger.debug("Parent entity id not found " + parentEntityId);
                        throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND);
                    }

                    unlockedEntity = entityItemDAO.findById(parentEntityId);

                    if (unlockedEntity == null) {
                        purchaseTransaction.setErrorCode(PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND);
                        purchaseTransaction.setAbortFlag(true);
                        logger.debug("Error while finding unlocked entity. Entity id not found " + parentEntityId);
                        throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PARENT_ENTITY_NOT_FOUND);
                    }
                    parentEntityId = unlockedEntity.getParentEntityId();
                    lockFlag = unlockedEntity.getLockBooleanFlag();
                }

            } else {
                unlockedEntity = entityItem;
            }
            logger.debug("Unlocked entity = " + unlockedEntity);

            purchaseTransaction.setUnlockEntityId(unlockedEntity.getEntityId());

            if (purchaseTransaction.getCpFilingEntityCode() == null)
                purchaseTransaction.setCpFilingEntityCode(purchaseTransaction.getEntityCode());

            entityDefaultDAO.findAllEntityDefaultsByEntityIdAsMap(unlockedEntity.getEntityId(), transientDetails.getEntityDefaults());

	        switch (purchaseTransaction.getModuleCode()) {
				case PURCHUSE:
					break;
		
				case BILLSALE:
					if (purchaseTransaction.getTransactionTypeCode() == null) {
		                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_TRANSTYPE) != null) {
		                    purchaseTransaction.setTransactionTypeCode(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_TRANSTYPE));
		                } else {
		                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTTRANSTYPE) != null ) {
		                        purchaseTransaction.setTransactionTypeCode(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTTRANSTYPE));
		                    } else {
		                        purchaseTransaction.setTransactionTypeCode("SALE");
		                    }
		                }
		            }
					break;
		
				default:
					break;
		    }
            
            if (purchaseTransaction.getRatetypeCode() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_RATETYPE) != null) {
                    purchaseTransaction.setRatetypeCode(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_RATETYPE));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTRATETYPE) != null ) {
                        purchaseTransaction.setRatetypeCode(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTRATETYPE));
                    } else {
                        purchaseTransaction.setRatetypeCode("0");
                    }
                }
            }
            
            if (purchaseTransaction.getMethodDeliveryCode() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_MOD) != null) {
                    purchaseTransaction.setMethodDeliveryCode(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_MOD));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTMOD) != null) {
                        purchaseTransaction.setMethodDeliveryCode(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTMOD));
                    } else {
                        purchaseTransaction.setMethodDeliveryCode("1");
                    }
                }
            }

            if (purchaseTransaction.getGeoReconCode() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_GEORECON) != null) {
                    purchaseTransaction.setGeoReconCode(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_GEORECON));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTGEORECON) != null) {
                        purchaseTransaction.setGeoReconCode(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTGEORECON));
                    } else {
                        purchaseTransaction.setGeoReconCode("1");
                    }
                }
            }


            if (transientDetails.getFrtIncItem() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_FRTINCITEM) != null) {
                    transientDetails.setFrtIncItem(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_FRTINCITEM));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTFRTINCITEM) != null) {
                        transientDetails.setFrtIncItem(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTFRTINCITEM));
                    } else {
                        transientDetails.setFrtIncItem("0");
                    }
                }
            }

            if (transientDetails.getDiscIncItem() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_DISINCITEM) != null) {
                    transientDetails.setDiscIncItem(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_DISINCITEM));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTDISCINCITEM) != null) {
                        transientDetails.setDiscIncItem(purchasingOptions.get(PurchaseTransactionOptions.PK_DEFAULTDISCINCITEM));
                    } else {
                        transientDetails.setDiscIncItem("0");
                    }
                }
            }

            if (transientDetails.getLocationPreference() == null) {
                if (transientDetails.getEntityDefaults() != null && transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_LOCNPREF) != null) {
                    transientDetails.setLocationPreference(transientDetails.getEntityDefaults().get(TransientPurchaseTransaction.KEY_LOCNPREF));
                } else {
                    if (purchasingOptions.get(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE) != null) {
                        transientDetails.setLocationPreference(purchasingOptions.get(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE));
                    } else {
                        transientDetails.setLocationPreference("RATEPOINT");
                    }
                }
            }
            
            if(transientDetails.getLocationPreference()==null || (!transientDetails.getLocationPreference().equalsIgnoreCase("LOCNMATRIX") && 
            		!transientDetails.getLocationPreference().equalsIgnoreCase("RATEPOINT"))) {
            	transientDetails.setLocationPreference("RATEPOINT");
            }

            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transEntity.name());

            logger.debug("Exiting getTransEntity" + purchaseTransaction);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Aborting processing!");
            logger.error(e);
            if (purchaseTransaction.getErrorCode() == null)
                purchaseTransaction.setErrorCode(e.getMessage());
            writeError(purchaseTransaction);
            throw new PurchaseTransactionAbortProcessingException(e.getMessage());
        }

    }
    
    private void writeError(MicroApiTransaction purchaseTransaction) {
        String errorCode = purchaseTransaction.getErrorCode();
        ListCodes errorListCode = null;
        if (errorCode!=null) {
            Map<String, ListCodes> errorCodes = cacheManager.getErrorListCodes();
            errorListCode = errorCodes.get(errorCode);
        }

        if (errorListCode == null) {
            errorListCode = new ListCodes();
            errorListCode.setDescription("Unknown Error");
            errorListCode.setMessage(null);
            errorListCode.setExplanation(null);
            errorListCode.setSeverityLevel("0");
            errorListCode.setAbortImportFlag("0");
            errorListCode.setWriteImportLineFlag("1");
        }
        purchaseTransaction.setErrorText(errorListCode.getDescription());

        if (purchaseTransaction.getAbortFlag() != null  && !purchaseTransaction.getAbortFlag() )
            purchaseTransaction.setAbortFlag(errorListCode.isAbortEnabled());

        /*
        if (purchaseTransaction.getProcessBatchNo()!=null) {
            BatchErrorLog batchErrorLog = new BatchErrorLog();
            batchErrorLog.setBatchId(purchaseTransaction.getProcessBatchNo());
            batchErrorLog.setErrorDefCode(errorCode);
            batchErrorLog.setProcessType("P");
            batchErrorLog.setRowNo(0L);
            batchErrorLog.setColumnNo(0L);
            if (purchaseTransaction.getLoadTimestamp() == null) {
                batchErrorLog.setProcessTimestamp(new Date());
            } else {
                batchErrorLog.setProcessTimestamp(purchaseTransaction.getLoadTimestamp());
            }

            batchMaintenanceDAO.save(batchErrorLog);
        }
        */
    }

    private void populateCalcFlags(TransientPurchaseTransaction transientDetails, LocationMatrix locationMatrix ) throws PurchaseTransactionProcessingException {
        if (locationMatrix == null) {
            logger.error("Location matrix is null.");
            throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        }
        transientDetails.setCountryCalcFlag(locationMatrix.getCountryBooleanFlag());
        transientDetails.setStateCalcFlag(locationMatrix.getStateBooleanFlag());
        transientDetails.setCountyCalcFlag(locationMatrix.getCountyBooleanFlag());
        transientDetails.setCityCalcFlag(locationMatrix.getCityBooleanFlag());
        transientDetails.setStj1CalcFlag(locationMatrix.getStj1BooleanFlag());
        transientDetails.setStj2CalcFlag(locationMatrix.getStj2BooleanFlag());
        transientDetails.setStj3CalcFlag(locationMatrix.getStj3BooleanFlag());
        transientDetails.setStj4CalcFlag(locationMatrix.getStj4BooleanFlag());
        transientDetails.setStj5CalcFlag(locationMatrix.getStj5BooleanFlag());
    }
    
    public String getLocationMethodName(LocationType locnType){
    	String locationMethod = "Shipto";
        switch (locnType) {
			case BILLTO:
				locationMethod = "Billto";
				break;
	
			case  SHIPTO:
				locationMethod = "Shipto";
				break;
	
			case  TITLETRANSFER:
				locationMethod = "Ttlxfr";
				break;
	
			case  FIRSTUSE:
				locationMethod = "Firstuse";
				break;
	
			case  SHIPFROM:
				locationMethod = "Shipfrom";
				break;
	
			case  ORDERORIGIN:
				locationMethod = "Ordrorgn";
				break;
	
			case  ORDERACCEPT:
				locationMethod = "Ordracpt";
				break;
	
			default:
				break;
		}
        
        return locationMethod; 	
    }
    
    public boolean isLocationEmpty(MicroApiTransaction trans, LocationType locnType) {
    	String locationMethod = getLocationMethodName(locnType);
        Method theMethod = null;
        try {
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine1", new Class[]{});
            theMethod.setAccessible(true);
            String addressLine1 = (String) theMethod.invoke(trans, new Object[]{});     
            if(addressLine1!=null && addressLine1.length()>0) {
            	return false;
            }

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine2", new Class[]{});
            theMethod.setAccessible(true);
            String addressLine2 = (String) theMethod.invoke(trans, new Object[]{});
            if(addressLine2!=null && addressLine2.length()>0) {
            	return false;
            }

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "City", new Class[]{});
            theMethod.setAccessible(true);
            String city = (String) theMethod.invoke(trans, new Object[]{});
            if(city!=null && city.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "CountryCode", new Class[]{});
            theMethod.setAccessible(true);
            String countryCode = (String) theMethod.invoke(trans, new Object[]{});
            if(countryCode!=null && countryCode.length()>0) {
            	return false;
            }

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "County", new Class[]{});
            theMethod.setAccessible(true);
            String county = (String) theMethod.invoke(trans, new Object[]{});
            if(county!=null && county.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "EntityCode", new Class[]{});
            theMethod.setAccessible(true);
            String entityCode = (String) theMethod.invoke(trans, new Object[]{});
            if(entityCode!=null && entityCode.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "EntityId", new Class[]{});
            theMethod.setAccessible(true);
            Long entityId = (Long) theMethod.invoke(trans, new Object[]{});
            if(entityId!=null && entityId.longValue()>0L) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Geocode", new Class[]{});
            theMethod.setAccessible(true);
            String geocode = (String) theMethod.invoke(trans, new Object[]{});
            if(geocode!=null && geocode.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "JurisdictionId", new Class[]{});
            theMethod.setAccessible(true);
            Long jurisdictionId = (Long) theMethod.invoke(trans, new Object[]{});
            if(jurisdictionId!=null && jurisdictionId.longValue()>0L) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Lat", new Class[]{});
            theMethod.setAccessible(true);
            String lat = (String) theMethod.invoke(trans, new Object[]{});
            if(lat!=null && lat.length()>0) {
            	return false;
            }
           /* 
            if (locnType.equals(LocationType.SHIPTO)) {
	            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Location", new Class[]{});
	            theMethod.setAccessible(true);
	            String location = (String) theMethod.invoke(trans, new Object[]{});
	            if(location!=null && location.length()>0) {
	            	return false;
	            }
	            
	            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocationName", new Class[]{});
	            theMethod.setAccessible(true);
	            String locationName = (String) theMethod.invoke(trans, new Object[]{});
	            if(locationName!=null && locationName.length()>0) {
	            	return false;
	            }
            }
            */
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
            theMethod.setAccessible(true);
            Long locnMatrixId = (Long) theMethod.invoke(trans, new Object[]{});
            if(locnMatrixId!=null && locnMatrixId.longValue()>0L) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Long", new Class[]{});
            theMethod.setAccessible(true);
            String loclong = (String) theMethod.invoke(trans, new Object[]{});
            if(loclong!=null && loclong.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "StateCode", new Class[]{});
            theMethod.setAccessible(true);
            String stateCode = (String) theMethod.invoke(trans, new Object[]{});
            if(stateCode!=null && stateCode.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj1Name", new Class[]{});
            theMethod.setAccessible(true);
            String stj1Name = (String) theMethod.invoke(trans, new Object[]{});
            if(stj1Name!=null && stj1Name.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj2Name", new Class[]{});
            theMethod.setAccessible(true);
            String stj2Name = (String) theMethod.invoke(trans, new Object[]{});
            if(stj2Name!=null && stj2Name.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj3Name", new Class[]{});
            theMethod.setAccessible(true);
            String stj3Name = (String) theMethod.invoke(trans, new Object[]{});
            if(stj3Name!=null && stj3Name.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj4Name", new Class[]{});
            theMethod.setAccessible(true);
            String stj4Name = (String) theMethod.invoke(trans, new Object[]{});
            if(stj4Name!=null && stj4Name.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj5Name", new Class[]{});
            theMethod.setAccessible(true);
            String stj5Name = (String) theMethod.invoke(trans, new Object[]{});
            if(stj5Name!=null && stj5Name.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zip", new Class[]{});
            theMethod.setAccessible(true);
            String zip = (String) theMethod.invoke(trans, new Object[]{});
            if(zip!=null && zip.length()>0) {
            	return false;
            }
            
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zipplus4", new Class[]{});
            theMethod.setAccessible(true);
            String zipplus4 = (String) theMethod.invoke(trans, new Object[]{});
            if(zipplus4!=null && zipplus4.length()>0) {
            	return false;
            }
        } catch (NoSuchMethodException ne) {
            logger.error("Invalid method name!" + ne.getMessage());
        } catch (IllegalAccessException ie) {
            logger.error("Insufficient permissions to call a getter!" + ie.getMessage());
        }  catch (InvocationTargetException ie) {
            logger.error("Invocation target exception while calling a getter!" + ie.getCause().getMessage());
        }
    	
    	return true;
    }
    
    public void getTransLocation(MicroApiTransaction trans, LocationType locnType, String overrideFlag) throws PurchaseTransactionProcessingException {

        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (transientDetails.getPurchasingOptions() == null)
            transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
        Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();
        
        String locationMethod = getLocationMethodName(locnType);  
        populateDrivers(trans, DRIVER_NAMES_TYPE_LOCATION);

        try {
            boolean jurFound = false;           
            Method theMethod = null;
            
            Long jurisdictionId = null;
            Long locnMatrixId = null;
  
	        theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "JurisdictionId", new Class[]{});
	        theMethod.setAccessible(true);	        	
	        jurisdictionId = (Long)theMethod.invoke(trans, new Object[]{});
               
            Jurisdiction jurisdiction = null;
            if (jurisdictionId != null && jurisdictionId > 0L) {
                try {
                    jurisdiction = jurisdictionDAO.findById(jurisdictionId);
                } catch (DataAccessException dte) {
                    logger.debug("Jurisdiction not found based on id="+jurisdictionId);
                }
                if (jurisdiction != null) {
                    jurFound = true;
                    
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
        	        theMethod.setAccessible(true);	        	
        	        locnMatrixId = (Long)theMethod.invoke(trans, new Object[]{});
        	        
                    if (locnMatrixId != null && locnMatrixId > 0L) {
                        LocationMatrix locationMatrix = locationMatrixDAO.findById(locnMatrixId);
                        if (locationMatrix != null) {
                            populateCalcFlags(transientDetails, locationMatrix);
                        } else {                         
                        	theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "LocnMatrixId", new Class[] {Long.class});
    	            		theMethod.setAccessible(true);
    	            		theMethod.invoke(trans, new Object[]{0L});               	
                            enableJurFlags(trans.getTransientTransaction());
                        }

                    } else {
                        enableJurFlags(trans.getTransientTransaction());
                    }
                } else {
	                trans.setErrorCode(PROCESSING_ERROR_P30);
	                writeError(trans);
   
                	theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "JurisdictionId", new Class[] {Long.class});
            		theMethod.setAccessible(true);
            		theMethod.invoke(trans, new Object[]{0L});       
            		
            		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "ManualJurInd", new Class[] {String.class});
            		theMethod.setAccessible(true);
            		theMethod.invoke(trans, new Object[]{MANUAL_IND_OVERRIDE_ENTERED_JURISDICTION});
                }
            }

            if (!jurFound) {
            	
            	theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
    	        theMethod.setAccessible(true);	        	
    	        locnMatrixId = (Long)theMethod.invoke(trans, new Object[]{});
                if (locnMatrixId != null && locnMatrixId > 0L) {
                    LocationMatrix locationMatrix = null;
                    try {
                        locationMatrix = locationMatrixDAO.findById(locnMatrixId);
                    } catch (DataAccessException dte) {
                        logger.debug("Did not find location based on id =" + locnMatrixId);
                    }
                    if (locationMatrix == null) {
                    	trans.setErrorCode(PROCESSING_ERROR_P31);
    	                writeError(trans);
    	                
                    	theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "LocnMatrixId", new Class[] {Long.class});
                		theMethod.setAccessible(true);
                		theMethod.invoke(trans, new Object[]{0L});       
                		
                		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "ManualJurInd", new Class[] {String.class});
                		theMethod.setAccessible(true);
                		theMethod.invoke(trans, new Object[]{MANUAL_IND_OVERRIDE_ENTERED_LOCATION});
                    } else {
                        try {
                            jurisdiction = jurisdictionDAO.findById(locationMatrix.getJurisdiction().getJurisdictionId());
                            
  
	                          Date currGlDate = trans.getGlDate();
	                      	  if(currGlDate == null)
	                      		  currGlDate = new Date();
	                      	  
	                      	  StringBuilder sb = new StringBuilder();
	                            if (trans.getProcessingNotes() != null)
	                                sb.append(trans.getProcessingNotes());
	                            
	                    		  if(jurisdiction.getEffectiveDate().compareTo(currGlDate) <= 0 && jurisdiction.getExpirationDate().compareTo(currGlDate) > 0 ) {
		          	                populateCalcFlags(transientDetails, locationMatrix);
		                            jurFound = true;
	                    		  } else {
	                    			  sb.append("-").append("locnMatrix *Jurisdiction Not Effective*");
	          	                  trans.setProcessingNotes(sb.toString());
	                    		  }
                            
                        }catch (DataAccessException dte) {
                            logger.debug("Jurisdiction not found based on id="+jurisdictionId);

                        }
                        if (jurisdiction == null) {
                        	theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "LocnMatrixId", new Class[] {Long.class});
                    		theMethod.setAccessible(true);
                    		theMethod.invoke(trans, new Object[]{0L}); 
                    		
                    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "JurisdictionId", new Class[] {Long.class});
                    		theMethod.setAccessible(true);
                    		theMethod.invoke(trans, new Object[]{0L}); 
                        }
                    }
                }
            }

            String locationPreferences = "";
            if (!jurFound) {

                Object ratepointEnabled = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED);
                if (ratepointEnabled == null)
                    ratepointEnabled = "0";

                locationPreferences = transientDetails.getLocationPreference();
                if (locationPreferences == null)
                    locationPreferences = purchasingOptions.get(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE);

                if ("1".equals(ratepointEnabled)) {
                    if (locationPreferences == null)
                        locationPreferences = LOCATIONPREFERENCE_RATEPOINT;

                    if (locationPreferences.equalsIgnoreCase(LOCATIONPREFERENCE_RATEPOINT)) {
                    	
                        jurisdiction = getJurViaRatepoint(trans, locnType);
                        if (jurisdiction == null) {
                            jurisdiction = getJurViaLocationMatrix(trans, locnType);
                            
                            //If LocationType.LocnMatrixId found in getJurViaLocationMatrix
                            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
                	        theMethod.setAccessible(true);	        	
                	        Long localId = (Long)theMethod.invoke(trans, new Object[]{});
                            if (localId != null && localId > 0L) {
                                LocationMatrix localMatrix = locationMatrixDAO.findById(localId);
                                if (localMatrix != null) {
                                    populateCalcFlags(transientDetails, localMatrix);
                                    jurFound = true;
                                }
                            }
                            
                            //if (jurisdiction == null)
                            //    throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P21);
                        } else {
                            enableJurFlags(trans.getTransientTransaction());
                            jurFound = true;
                        }
                    } else if (locationPreferences.equalsIgnoreCase(LOCATIONPREFERENCE_LOCATIONMATRIX)) {
                        jurisdiction = getJurViaLocationMatrix(trans, locnType);
                        
                        //If LocationType.LocnMatrixId found in getJurViaLocationMatrix
                        theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
            	        theMethod.setAccessible(true);	        	
            	        Long localId = (Long)theMethod.invoke(trans, new Object[]{});
                        if (localId != null && localId > 0L) {
                            LocationMatrix localMatrix = locationMatrixDAO.findById(localId);
                            if (localMatrix != null) {
                                populateCalcFlags(transientDetails, localMatrix);
                                jurFound = true;
                            }
                        }
                        
                        if (jurisdiction == null) {
                            jurisdiction = getJurViaRatepoint(trans, locnType);
                            if (jurisdiction == null) {
                                //throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P22);
                            } else {
                                enableJurFlags(trans.getTransientTransaction());
                                jurFound = true;
                            }
                        }
                    }

                } else {
                    jurisdiction = getJurViaLocationMatrix(trans, locnType);
                    
                    //If LocationType.LocnMatrixId found in getJurViaLocationMatrix
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "LocnMatrixId", new Class[]{});
        	        theMethod.setAccessible(true);	        	
        	        Long localId = (Long)theMethod.invoke(trans, new Object[]{});
                    if (localId != null && localId > 0L) {
                        LocationMatrix localMatrix = locationMatrixDAO.findById(localId);
                        if (localMatrix != null) {
                            populateCalcFlags(transientDetails, localMatrix);
                            jurFound = true;
                        }
                    }
                    
                    //if (jurisdiction == null)
                    //    throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P21);
                }
            }
            
            //TODO: if (jurisdiction == null), try to get jurisdiction from PinPoint Jurisdiction table - like old sale call, JurisdictionDao.getJurisdiction()         
            if (jurisdiction == null) {
                try {    
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "City", new Class[]{});
                    theMethod.setAccessible(true);
                    String city = (String) theMethod.invoke(trans, new Object[]{});
                    
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "CountryCode", new Class[]{});
                    theMethod.setAccessible(true);
                    String countryCode = (String) theMethod.invoke(trans, new Object[]{});

                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "County", new Class[]{});
                    theMethod.setAccessible(true);
                    String county = (String) theMethod.invoke(trans, new Object[]{});
                                      
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Geocode", new Class[]{});
                    theMethod.setAccessible(true);
                    String geocode = (String) theMethod.invoke(trans, new Object[]{});                            

                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "StateCode", new Class[]{});
                    theMethod.setAccessible(true);
                    String stateCode = (String) theMethod.invoke(trans, new Object[]{});           
                    
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zip", new Class[]{});
                    theMethod.setAccessible(true);
                    String zip = (String) theMethod.invoke(trans, new Object[]{});
                    
                    theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zipplus4", new Class[]{});
                    theMethod.setAccessible(true);
                    String zipplus4 = (String) theMethod.invoke(trans, new Object[]{});
                    
                    jurisdiction = jurisdictionDAO.getJurisdictionByGeoRecon(trans.getGlDate(), trans.getGeoReconCode(), geocode,
                    		city, county, stateCode, zip,
                    		zipplus4, countryCode);
             
                    if(jurisdiction!=null) {
                        StringBuilder sb = new StringBuilder();
                        if (trans.getProcessingNotes() != null)
                            sb.append(trans.getProcessingNotes());
                        sb.append("-").append(locnType.toString().toLowerCase()).append("=").append("ppJurLookup");
                        trans.setProcessingNotes(sb.toString());
                        
                        enableJurFlags(trans.getTransientTransaction());
                        jurFound = true;
                    }                
                } catch (NoSuchMethodException ne) {
                    logger.error("Invalid method name!" + ne.getMessage());
                } catch (IllegalAccessException ie) {
                    logger.error("Insufficient permissions to call a getter!" + ie.getMessage());
                }  catch (InvocationTargetException ie) {
                    logger.error("Invocation target exception while calling a getter!" + ie.getCause().getMessage());
                }

            }
            
            if (jurisdiction == null) {
            	if(locnType==LocationType.SHIPTO) {
            		if(trans.getErrorCode()==null || trans.getErrorCode().length()==0){           		
	            		if (locationPreferences!=null && locationPreferences.equalsIgnoreCase(LOCATIONPREFERENCE_RATEPOINT)) {
	            			trans.setErrorCode(PROCESSING_ERROR_P22);
	                        writeError(trans);
	                        throw new PurchaseTransactionSuspendedException(trans.getErrorCode());
	            		}
	            		else if (locationPreferences!=null && locationPreferences.equalsIgnoreCase(LOCATIONPREFERENCE_LOCATIONMATRIX)) {
	            			trans.setErrorCode(PROCESSING_ERROR_P21);
	                        writeError(trans);
	                        throw new PurchaseTransactionSuspendedException(trans.getErrorCode());
	            		}
	            		else {
	            			throw new PurchaseTransactionSuspendedException("Can't find jurisdiction!");
	            		}
            		}
            		else {
            			throw new PurchaseTransactionSuspendedException(trans.getErrorCode());
            		}
        	    }
            	else {
            		return;
            	}
            }

            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "JurisdictionId", new Class[] {Long.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getJurisdictionId()});
    		
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Geocode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getGeocode()});   
            
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "City", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCity()});   
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "County", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCounty()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "StateCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getState()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Zip", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getZip()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Zipplus4", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getZipplus4()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "CountryCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCountry()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj1Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStj1Name()});
            
            theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj2Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStj2Name()});
            
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj3Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStj3Name()});
            
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj4Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStj4Name()});
            
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj5Name", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStj5Name()});
    		
    		//Populate Trans.
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "CountryNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCountryNexusindCode()});
    		
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "StateNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getStateNexusindCode()});
    		
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "CountyNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCountyNexusindCode()});
    		
    		theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "CityNexusindCode", new Class[] {String.class});
    		theMethod.setAccessible(true);
    		theMethod.invoke(trans, new Object[]{jurisdiction.getCityNexusindCode()});
    		
    		//Populate TEMP, TRAN 1 to 5
    		String stjName = null;
    		theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj1Name", new Class[]{});
        	theMethod.setAccessible(true);
        	stjName = (String)theMethod.invoke(trans, new Object[]{});    		
    		if (stjName!=null && stjName.length()>0) {
    			theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj1NexusindCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(trans, new Object[]{jurisdiction.getStj1NexusindCode()});

                theMethod = transientDetails.getClass().getDeclaredMethod("set" + locationMethod + "Stj1LocalCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(transientDetails, new Object[]{jurisdiction.getStj1LocalCode()});
            }
    		
    		theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj2Name", new Class[]{});
        	theMethod.setAccessible(true);
        	stjName = (String)theMethod.invoke(trans, new Object[]{});    		
    		if (stjName!=null && stjName.length()>0) {
    			theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj2NexusindCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(trans, new Object[]{jurisdiction.getStj2NexusindCode()});

                theMethod = transientDetails.getClass().getDeclaredMethod("set" + locationMethod + "Stj2LocalCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(transientDetails, new Object[]{jurisdiction.getStj2LocalCode()});
            }
    		
    		theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj3Name", new Class[]{});
        	theMethod.setAccessible(true);
        	stjName = (String)theMethod.invoke(trans, new Object[]{});    		
    		if (stjName!=null && stjName.length()>0) {
    			theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj3NexusindCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(trans, new Object[]{jurisdiction.getStj3NexusindCode()});

                theMethod = transientDetails.getClass().getDeclaredMethod("set" + locationMethod + "Stj3LocalCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(transientDetails, new Object[]{jurisdiction.getStj3LocalCode()});
            }
    		
    		theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj4Name", new Class[]{});
        	theMethod.setAccessible(true);
        	stjName = (String)theMethod.invoke(trans, new Object[]{});    		
    		if (stjName!=null && stjName.length()>0) {
    			theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj4NexusindCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(trans, new Object[]{jurisdiction.getStj4NexusindCode()});

                theMethod = transientDetails.getClass().getDeclaredMethod("set" + locationMethod + "Stj4LocalCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(transientDetails, new Object[]{jurisdiction.getStj4LocalCode()});
            }
    		
    		theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Stj5Name", new Class[]{});
        	theMethod.setAccessible(true);
        	stjName = (String)theMethod.invoke(trans, new Object[]{});    		
    		if (stjName!=null && stjName.length()>0) {
    			theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "Stj5NexusindCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(trans, new Object[]{jurisdiction.getStj5NexusindCode()});

                theMethod = transientDetails.getClass().getDeclaredMethod("set" + locationMethod + "Stj5LocalCode", new Class[] {String.class});
        		theMethod.setAccessible(true);
        		theMethod.invoke(transientDetails, new Object[]{jurisdiction.getStj5LocalCode()});
            }
    		
    		if(overrideFlag!=null && overrideFlag.equals("1")){
    			//populate trans 
    			theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "CountryCode");
    			theMethod.setAccessible(true);
    			trans.setTransactionCountryCode(theMethod.invoke(trans).toString());
    			theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "StateCode");
    			theMethod.setAccessible(true);
    			trans.setTransactionStateCode(theMethod.invoke(trans).toString());
    			
	            //situs info
	            trans.setCountryCode(jurisdiction.getCountry());
	            trans.setStateCode(jurisdiction.getState());
	            trans.setCountyName(jurisdiction.getCounty());
	            trans.setCityName(jurisdiction.getCity());
	            trans.setStj1Name(jurisdiction.getStj1Name());
	            trans.setStj2Name(jurisdiction.getStj2Name());
	            trans.setStj3Name(jurisdiction.getStj3Name());
	            trans.setStj4Name(jurisdiction.getStj4Name());
	            trans.setStj5Name(jurisdiction.getStj5Name());
	            
	            trans.setCountryNexusindCode(jurisdiction.getCountryNexusindCode());
	            trans.setStateNexusindCode(jurisdiction.getStateNexusindCode());
	            trans.setCountyNexusindCode(jurisdiction.getCountyNexusindCode());
	            trans.setCityNexusindCode(jurisdiction.getCityNexusindCode());

	            /* Hardcoding situs codes to 'ST' for now */
	            trans.setCountryPrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	            trans.setStatePrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	            trans.setCountyPrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	            trans.setCityPrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);

	            trans.setCountrySitusJurId(jurisdiction.getJurisdictionId());
	            trans.setStateSitusJurId(jurisdiction.getJurisdictionId());
	            trans.setCountySitusJurId(jurisdiction.getJurisdictionId());
	            trans.setCitySitusJurId(jurisdiction.getJurisdictionId());
	            
	            //Populate TEMP.
	            if (trans.getStj1Name() != null) {
	                trans.setStj1PrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	                trans.setStj1SitusJurId(jurisdiction.getJurisdictionId());                
	                transientDetails.setStj1LocalCode(jurisdiction.getStj1LocalCode());
	                trans.setStj1NexusindCode(jurisdiction.getStj1NexusindCode());
	            }
	            if (trans.getStj2Name() != null) {
	                trans.setStj2PrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	                trans.setStj2SitusJurId(jurisdiction.getJurisdictionId());
	                transientDetails.setStj2LocalCode(jurisdiction.getStj2LocalCode());
	                trans.setStj2NexusindCode(jurisdiction.getStj2NexusindCode());
	            }
	            if (trans.getStj3Name() != null) {
	                trans.setStj3PrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	                trans.setStj3SitusJurId(jurisdiction.getJurisdictionId());
	                transientDetails.setStj3LocalCode(jurisdiction.getStj3LocalCode());
	                trans.setStj3NexusindCode(jurisdiction.getStj3NexusindCode());
	            }
	            if (trans.getStj4Name() != null) {
	                trans.setStj4PrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	                trans.setStj4SitusJurId(jurisdiction.getJurisdictionId());
	                transientDetails.setStj4LocalCode(jurisdiction.getStj4LocalCode());
	                trans.setStj4NexusindCode(jurisdiction.getStj4NexusindCode());
	            }
	            if (trans.getStj5Name() != null) {
	                trans.setStj5PrimarySitusCode(SITUS_TYPE_CODE_SHIPTO);
	                trans.setStj5SitusJurId(jurisdiction.getJurisdictionId());
	                transientDetails.setStj5LocalCode(jurisdiction.getStj5LocalCode());
	                trans.setStj5NexusindCode(jurisdiction.getStj5NexusindCode());
	            }
	            //transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transLocation.name());//Moved to below
    		}
    		transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transLocation.name() + "(" + locnType.toString() + ")");
        } catch (PurchaseTransactionSuspendedException suspEx) {
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transLocation.name() + "(" + locnType.toString() + ")");
            
            switch (trans.getModuleCode()) {
				case PURCHUSE:
					suspend(trans, SuspendReason.SUSPEND_LOCN);
		            suspEx.printStackTrace();
		            logger.error(suspEx);
		            trans.setErrorCode(suspEx.getMessage());
		            writeError(trans);
		            throw new PurchaseTransactionSuspendedException(suspEx.getMessage());
		
				case BILLSALE:
					if(locnType==LocationType.SHIPTO ) {
						trans.setTransactionInd(TRANS_IND_ERROR);
						trans.setSuspendInd(null);
						trans.setFatalErrorCode("PS20");
						throw new PurchaseTranasactionSoftAbortProcessingException("");
		            }
					
					break;
		
				default:
					break;
	        }
        } catch (Exception e) { // we should never get here
            e.printStackTrace();
            logger.error(e);
            trans.setErrorCode(e.getMessage());
            writeError(trans);
            throw new PurchaseTransactionAbortProcessingException(e.getMessage());
        }
    }

    private void enableJurFlags(TransientPurchaseTransaction transientDetails) {
        transientDetails.setCountryCalcFlag(true);
        transientDetails.setStateCalcFlag(true);
        transientDetails.setCountyCalcFlag(true);
        transientDetails.setCityCalcFlag(true);
        transientDetails.setStj1CalcFlag(true);
        transientDetails.setStj2CalcFlag(true);
        transientDetails.setStj3CalcFlag(true);
        transientDetails.setStj4CalcFlag(true);
        transientDetails.setStj5CalcFlag(true);
    }

    @Override
    public void getTransGoodSvc(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (transientDetails.getPurchasingOptions() == null)
            transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
        Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();
        
        switch (trans.getModuleCode()) {
			case PURCHUSE:
				populateDrivers(trans, DRIVER_NAMES_TYPE_TAX);
				break;
	
			case BILLSALE:
				populateDrivers(trans, DRIVER_NAMES_TYPE_GOODSNSERVICES);
				break;
	
			default:
				break;
	    }

        try {
            TaxCode taxCode = null;

            if (trans.getTaxcodeCode() != null) {
                TaxCodePK taxCodePk = new TaxCodePK();
                taxCodePk.setTaxcodeCode(trans.getTaxcodeCode());
                taxCode = taxCodeDAO.findById(taxCodePk);

                if (taxCode == null) {
                    trans.setTaxcodeCode(null);
                    trans.setManualTaxcodeInd(MANUAL_IND_OVERRIDE_ENTERED_TAXCODE);
                } else {
                    trans.setTaxcodeCode(taxCode.getTaxcodeCode());
                    return;
                }
            }

            TaxMatrix taxMatrix = null;
            if (trans.getTaxMatrixId() != null && trans.getTaxMatrixId() != 0L) {
                try {
                    taxMatrix = taxMatrixDAO.findById(trans.getTaxMatrixId());
                } catch (DataAccessException dte) {
                    logger.debug("Cant find TaxMatrix by id=" + trans.getTaxMatrixId());
                }
                
                if (taxMatrix == null  || !taxMatrix.getModuleCode().contentEquals(trans.getModuleCode().toString())) {
                    trans.setTaxMatrixId(0L);
                    trans.setManualTaxcodeInd(MANUAL_IND_OVERRIDE_ENTERED_TAXMATRIX);
                    taxMatrix = null;
                }
            }

            if (taxMatrix == null) {
            	String sql = ""; //String sql = buildSql(trans, DRIVER_NAMES_TYPE_TAX);
            	switch (trans.getModuleCode()) {
	    			case PURCHUSE:
	    				sql = buildSql(trans, DRIVER_NAMES_TYPE_TAX);
	    				break;
	    	
	    			case BILLSALE:
	    				sql = buildSql(trans, DRIVER_NAMES_TYPE_GOODSNSERVICES);
	    				break;
	    	
	    			default:
	    				break;
	            }

                if (sql == null) {
                    trans.setErrorCode(PROCESSING_ERROR_NO_TAX_DRIVERS);
                    writeError(trans);
                    trans.setAbortFlag(true);
                    throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_NO_TAX_DRIVERS);
                }
                trans.getTransientTransaction().setTaxMatrixSQL(sql);

                taxMatrix = purchaseTransactionDAO.getTaxMatrixViaDrivers(sql, trans.getEntityId(), trans.getGlDate(), trans.getGlDate());

                if (taxMatrix == null) {

                    if (transientDetails.getAutoProcFlag() == null) {
                        String autoProcFlag = purchasingOptions.get(PurchaseTransactionOptions.PK_AUTOPROCFLAG);
                        if (autoProcFlag == null) {
                            transientDetails.setAutoProcFlag(false);
                            //throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_MISSING_OR_INVALID_OPTION);
                        }
                        transientDetails.setAutoProcFlag("1".equals(autoProcFlag) ? Boolean.TRUE : Boolean.FALSE);
                    }
                    
                    if(!transientDetails.getAutoProcFlag()) {
                    	throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P26_TAX_MATRIX_NOT_FOUND);
                    	//return;
                    }

                    if (transientDetails.getAutoProcAmt() == null) {
                        String autoProcAmtStr = purchasingOptions.get(PurchaseTransactionOptions.PK_AUTOPROCAMT);
                        BigDecimal autoProcAmt = null;
                        try {
                            if (autoProcAmtStr!=null)
                                autoProcAmt = new BigDecimal(autoProcAmtStr);
                        } catch (NumberFormatException nfe) {
                            logger.debug("Can't convert "+PurchaseTransactionOptions.PK_AUTOPROCAMT+ " to BigDecimal. "+nfe.getMessage());
                        }
                        if (autoProcAmt == null) {
                            transientDetails.setAutoProcFlag(false);
                            throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_MISSING_OR_INVALID_OPTION);
                        }
                        transientDetails.setAutoProcAmt(autoProcAmt);
                    }

                    if (transientDetails.getAutoProcId() == null) {
                        String autoProcAmtStr = purchasingOptions.get(PurchaseTransactionOptions.PK_AUTOPROCID);
                        if (autoProcAmtStr == null)
                            throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_MISSING_OR_INVALID_OPTION);

                        transientDetails.setAutoProcId(autoProcAmtStr);
                    }

                    TaxCode autoProcTaxCode = null;
                    try {
                        autoProcTaxCode = taxCodeDAO.findById(new TaxCodePK(transientDetails.getAutoProcId()));
                    } catch (DataAccessException dte) {
                        logger.debug("Unable to find taxcode via id="+transientDetails.getAutoProcId());
                    }

                    if (autoProcTaxCode == null)
                        throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_MISSING_OR_INVALID_OPTION);


                    if ( trans.getGlLineItmDistAmt()==null || trans.getGlLineItmDistAmt().compareTo(transientDetails.getAutoProcAmt())!=-1) {
                        throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P26_TAX_MATRIX_NOT_FOUND);
                    }

                    trans.setTaxcodeCode(transientDetails.getAutoProcId());
                    trans.setManualTaxcodeInd(MANUAL_IND_OVERRIDE_ENTERED_AUTOPROC);


                }
            }

            if (taxMatrix != null) {
                calcIfThenElse(taxMatrix, trans);
                trans.setTaxMatrixId(taxMatrix.getTaxMatrixId());
                if(trans.getTransientTransaction().getSuspendCodeFlag() == true) {
                	trans.setSuspendInd(TRANS_SUSPEND_IND_GSB);
                	trans.setTransactionInd(TRANS_IND_SUSPENDED);
                }

            }
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transGoodSvc.name());

        } catch (PurchaseTransactionSuspendedException suspEx) {
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transGoodSvc.name());
            
            switch (trans.getModuleCode()) {
				case PURCHUSE:
					suspend(trans, SuspendReason.SUSPEND_GS);
					suspEx.printStackTrace();
		            logger.error(suspEx);

		            // TODO Fix error writing more gracefully
		            if (!PROCESSING_ERROR_MISSING_OR_INVALID_OPTION.equals(suspEx.getMessage()) && !PROCESSING_ERROR_P26_TAX_MATRIX_NOT_FOUND.equals(suspEx.getMessage())) {
			            	trans.setErrorCode(suspEx.getMessage());
			            	writeError(trans);
		                }
		            throw new PurchaseTransactionSuspendedException(suspEx.getMessage());
		
				case BILLSALE:
					trans.setTaxMatrixId(-1L);
					break;
		
				default:
					break;
	        }
  
        } catch (Exception e) { // we should never get here
            e.printStackTrace();
            logger.error(e);
//            if (trans.getErrorCode() == null)
            trans.setErrorCode(e.getMessage());
            writeError(trans);
            throw new PurchaseTransactionAbortProcessingException(e.getMessage());
        }


    }
    
    private void resetJurIds(MicroApiTransaction trans) {
        trans.setStateTaxcodeDetailId(0L);
        trans.setCountyTaxcodeDetailId(0L);
        trans.setCityTaxcodeDetailId(0L);
        trans.setStj1TaxcodeDetailId(0L);
        trans.setStj2TaxcodeDetailId(0L);
        trans.setStj3TaxcodeDetailId(0L);
        trans.setStj4TaxcodeDetailId(0L);
        trans.setStj5TaxcodeDetailId(0L);
        trans.setStj6TaxcodeDetailId(0L);
        trans.setStj7TaxcodeDetailId(0L);
        trans.setStj8TaxcodeDetailId(0L);
        trans.setStj9TaxcodeDetailId(0L);
        trans.setStj10TaxcodeDetailId(0L);
    }
    
    @Override
    public void getTransTaxCodeDtl(MicroApiTransaction trans, MicroApiTransactionDocument document) throws PurchaseTransactionProcessingException {
        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
        try {
            TaxCodeDetail countryTaxCodeDtl = null;
            if (trans.getCountryTaxcodeDetailId() != null && trans.getCountryTaxcodeDetailId() > 0L) {
                countryTaxCodeDtl = taxCodeDetailDAO.findById(trans.getCountryTaxcodeDetailId());
                if (countryTaxCodeDtl == null)
                    trans.setCountryTaxcodeDetailId(0L);

            } else {
                trans.setCountryTaxcodeDetailId(0L);
            }

            if (countryTaxCodeDtl == null)
                countryTaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*COUNTRY", trans.getCountryCode(),
                        trans.getStateCode(), trans.getCountyName(), trans.getCityName(), null, trans.getGlDate());

            if (countryTaxCodeDtl!=null) {
                trans.setCountryTaxcodeDetailId(countryTaxCodeDtl.getTaxcodeDetailId());

                trans.setCountryTaxcodeTypeCode(countryTaxCodeDtl.getTaxcodeTypeCode());
                trans.setCountryTaxtypeCode(countryTaxCodeDtl.getTaxtypeCode());

                if (trans.getCountryTaxtypeUsedCode() == null) {
                    if (!"1".equals(trans.getDirectPayPermitFlag())) {
                        trans.setCountryTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                    } else
                        trans.setCountryTaxtypeUsedCode(countryTaxCodeDtl.getTaxtypeCode());
                }

                trans.setCountryRatetypeCode(countryTaxCodeDtl.getRateTypeCode());
                trans.setCountryGoodSvcTypeCode(countryTaxCodeDtl.getGoodSvcTypeCode());
                trans.setCountryCitationRef(countryTaxCodeDtl.getCitationRef());
                trans.setCountryExemptReason(countryTaxCodeDtl.getExemptReason());

                trans.setCountryTaxProcessTypeCode(countryTaxCodeDtl.getTaxProcessTypeCode());
                trans.setCountryGroupByDriver(countryTaxCodeDtl.getGroupByDriver());
                trans.setCountryAllocBucket(countryTaxCodeDtl.getAllocBucket());
                trans.setCountryAllocProrateByCode(countryTaxCodeDtl.getAllocProrateByCode());
                trans.setCountryAllocConditionCode(countryTaxCodeDtl.getAllocConditionCode());

                /*
                * TRANS.country* = countryTCDTL.
                taxable_threshold_amt +
                minimum_taxable_amt +
                maximum_taxable_amt +
                maximum_tax_amt +
                base_change_pct
                special_rate +
                special_setamt +*/

                transientDetails.setCountryTcdtlSpecialRate(countryTaxCodeDtl.getSpecialRate());
                transientDetails.setCountryTcdtlSpecialSetAmt(countryTaxCodeDtl.getSpecialSetAmt());
                transientDetails.setCountryTcdtlTaxableThresholdAmt(countryTaxCodeDtl.getTaxableThresholdAmt());
                transientDetails.setCountryTcdtlMinimumTaxableAmt(countryTaxCodeDtl.getMinimumTaxableAmt());
                transientDetails.setCountryTcdtlMaximumTaxableAmt(countryTaxCodeDtl.getMaximumTaxableAmt());
                transientDetails.setCountryTcdtlMaximumTaxAmt(countryTaxCodeDtl.getMaximumTaxAmt());

                trans.setCountryTaxableThresholdAmt(countryTaxCodeDtl.getTaxableThresholdAmt());
                trans.setCountryMinimumTaxableAmt(countryTaxCodeDtl.getMinimumTaxableAmt());
                trans.setCountryMaximumTaxableAmt(countryTaxCodeDtl.getMaximumTaxableAmt());
                trans.setCountryMaximumTaxAmt(countryTaxCodeDtl.getMaximumTaxAmt());
                trans.setCountryBaseChangePct(countryTaxCodeDtl.getBaseChangePct());

            }

            TaxCodeDetail stateTaxCodeDtl = null;
            if (trans.getStateTaxcodeDetailId()!=null && trans.getStateTaxcodeDetailId()>0L) {
                stateTaxCodeDtl = taxCodeDetailDAO.findById(trans.getStateTaxcodeDetailId());
                if (stateTaxCodeDtl == null)
                    resetJurIds(trans);
            } else {
                resetJurIds(trans);
            }

            if (stateTaxCodeDtl == null)
                stateTaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STATE", trans.getCountryCode(),
                        trans.getStateCode(), trans.getCountyName(), trans.getCityName(), null, trans.getGlDate());

            if (stateTaxCodeDtl==null)
                throw new PurchaseTransactionSuspendedException(TRANS_SUSPEND_IND_TAXCODE_DTL);


            if ("D".equals(stateTaxCodeDtl.getTaxcodeTypeCode()))
                transientDetails.setDistributedFlag(Boolean.TRUE);

            if ("2".equals(stateTaxCodeDtl.getTaxProcessTypeCode())) {
                transientDetails.setDocumentFlag(Boolean.TRUE);

                if (!transientDetails.getMarkedForGroupby() && document!=null) {
                    addToGroupedAndMark(document, trans, stateTaxCodeDtl);
                }
            }



            trans.setStateTaxcodeDetailId(stateTaxCodeDtl.getTaxcodeDetailId());
            trans.setStateTaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
            trans.setStateTaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

            if (trans.getStateTaxtypeUsedCode() == null) {
                if (!"1".equals(trans.getDirectPayPermitFlag())) {
                    trans.setStateTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                } else
                    trans.setStateTaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
            }

            trans.setStateRatetypeCode(stateTaxCodeDtl.getRateTypeCode());
            trans.setStateGoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
            trans.setStateCitationRef(stateTaxCodeDtl.getCitationRef());
            trans.setStateExemptReason(stateTaxCodeDtl.getExemptReason());

            trans.setStateTaxProcessTypeCode(stateTaxCodeDtl.getTaxProcessTypeCode());
            trans.setStateGroupByDriver(stateTaxCodeDtl.getGroupByDriver());
            trans.setStateAllocBucket(stateTaxCodeDtl.getAllocBucket());
            trans.setStateAllocProrateByCode(stateTaxCodeDtl.getAllocProrateByCode());
            trans.setStateAllocConditionCode(stateTaxCodeDtl.getAllocConditionCode());

            transientDetails.setStateTcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
            transientDetails.setStateTcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
            transientDetails.setStateTcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
            transientDetails.setStateTcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
            transientDetails.setStateTcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
            transientDetails.setStateTcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());

            trans.setStateTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
            trans.setStateMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
            trans.setStateMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
            trans.setStateMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());

            trans.setStateBaseChangePct(stateTaxCodeDtl.getBaseChangePct());


            /*COUNTY*/
            TaxCodeDetail countyTaxCodeDtl = null;
            if (trans.getCountyTaxcodeDetailId()!=null && trans.getCountyTaxcodeDetailId() > 0L) {
                countyTaxCodeDtl = taxCodeDetailDAO.findById(trans.getCountyTaxcodeDetailId());
                if (countyTaxCodeDtl == null)
                    trans.setCountyTaxcodeDetailId(0L);
            } else {
                trans.setCountyTaxcodeDetailId(0L);
            }

            if (countyTaxCodeDtl == null)
                countyTaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*COUNTY", trans.getCountryCode(),
                        trans.getStateCode(), trans.getCountyName(), trans.getCityName(), null, trans.getGlDate());

            if (countyTaxCodeDtl != null) {
                if ("D".equals(countyTaxCodeDtl.getTaxcodeTypeCode()))
                    transientDetails.setDistributedFlag(Boolean.TRUE);

                if ("2".equals(countyTaxCodeDtl.getTaxProcessTypeCode())) {
                    transientDetails.setDocumentFlag(Boolean.TRUE);

                    if (!transientDetails.getMarkedForGroupby() && document!=null) {
                        addToGroupedAndMark(document, trans, countyTaxCodeDtl);
                    }
                }

                trans.setCountyTaxcodeDetailId(countyTaxCodeDtl.getTaxcodeDetailId());
                trans.setCountyTaxcodeTypeCode(countyTaxCodeDtl.getTaxcodeTypeCode());

                trans.setCountyTaxtypeCode(countyTaxCodeDtl.getTaxtypeCode());

                if (trans.getCountyTaxtypeUsedCode() == null) {
                    if (!"1".equals(trans.getDirectPayPermitFlag())) {
                        trans.setCountyTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                    } else
                        trans.setCountyTaxtypeUsedCode(countyTaxCodeDtl.getTaxtypeCode());
                }

                trans.setCountyRatetypeCode(countyTaxCodeDtl.getRateTypeCode());
                trans.setCountyGoodSvcTypeCode(countyTaxCodeDtl.getGoodSvcTypeCode());
                trans.setCountyCitationRef(countyTaxCodeDtl.getCitationRef());
                trans.setCountyExemptReason(countyTaxCodeDtl.getExemptReason());

                trans.setCountyTaxProcessTypeCode(countyTaxCodeDtl.getTaxProcessTypeCode());
                trans.setCountyGroupByDriver(countyTaxCodeDtl.getGroupByDriver());
                trans.setCountyAllocBucket(countyTaxCodeDtl.getAllocBucket());
                trans.setCountyAllocProrateByCode(countyTaxCodeDtl.getAllocProrateByCode());
                trans.setCountyAllocConditionCode(countyTaxCodeDtl.getAllocConditionCode());


                transientDetails.setCountyTcdtlSpecialRate(countyTaxCodeDtl.getSpecialRate());
                transientDetails.setCountyTcdtlSpecialSetAmt(countyTaxCodeDtl.getSpecialSetAmt());
                transientDetails.setCountyTcdtlTaxableThresholdAmt(countyTaxCodeDtl.getTaxableThresholdAmt());
                transientDetails.setCountyTcdtlMinimumTaxableAmt(countyTaxCodeDtl.getMinimumTaxableAmt());
                transientDetails.setCountyTcdtlMaximumTaxableAmt(countyTaxCodeDtl.getMaximumTaxableAmt());
                transientDetails.setCountyTcdtlMaximumTaxAmt(countyTaxCodeDtl.getMaximumTaxAmt());

                trans.setCountyTaxableThresholdAmt(countyTaxCodeDtl.getTaxableThresholdAmt());
                trans.setCountyMinimumTaxableAmt(countyTaxCodeDtl.getMinimumTaxableAmt());
                trans.setCountyMaximumTaxableAmt(countyTaxCodeDtl.getMaximumTaxableAmt());
                trans.setCountyMaximumTaxAmt(countyTaxCodeDtl.getMaximumTaxAmt());
                trans.setCountyBaseChangePct(countyTaxCodeDtl.getBaseChangePct());

            } else {

                trans.setCountyTaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                trans.setCountyTaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                if (trans.getCountyTaxtypeUsedCode() == null) {
                    if (!"1".equals(trans.getDirectPayPermitFlag())) {
                        trans.setCountyTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                    } else
                        trans.setCountyTaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                }

                trans.setCountyRatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                trans.setCountyGoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                trans.setCountyCitationRef(stateTaxCodeDtl.getCitationRef());
                trans.setCountyExemptReason(stateTaxCodeDtl.getExemptReason());

                trans.setCountyTaxProcessTypeCode("0"); // ADDED by MBF 5/22/2019
              /*  trans.setCountyTaxProcessTypeCode(stateTaxCodeDtl.getTaxProcessTypeCode());
                trans.setCountyGroupByDriver(stateTaxCodeDtl.getGroupByDriver());      TAKEN OFF THE CHART BY MBF 03/09/2017 */
                trans.setCountyAllocBucket(stateTaxCodeDtl.getAllocBucket());
                trans.setCountyAllocProrateByCode(stateTaxCodeDtl.getAllocProrateByCode());
                trans.setCountyAllocConditionCode(stateTaxCodeDtl.getAllocConditionCode());

              /*transientDetails.setCountyTcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                transientDetails.setCountyTcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                transientDetails.setCountyTcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                transientDetails.setCountyTcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                transientDetails.setCountyTcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                transientDetails.setCountyTcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
            }

            TaxCodeDetail cityTaxCodeDtl = null;
            if (trans.getCityTaxcodeDetailId()!=null && trans.getCityTaxcodeDetailId()>0L) {
                cityTaxCodeDtl = taxCodeDetailDAO.findById(trans.getCityTaxcodeDetailId());
                if (cityTaxCodeDtl == null)
                    trans.setCityTaxcodeDetailId(0L);
            } else {
                trans.setCityTaxcodeDetailId(0L);
            }

            if (cityTaxCodeDtl == null)
                cityTaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*CITY", trans.getCountryCode(),
                        trans.getStateCode(), trans.getCountyName(), trans.getCityName(), null, trans.getGlDate());

            if (cityTaxCodeDtl != null) {
                if ("D".equals(cityTaxCodeDtl.getTaxcodeTypeCode()))
                    transientDetails.setDistributedFlag(Boolean.TRUE);

                if ("2".equals(cityTaxCodeDtl.getTaxProcessTypeCode())) {
                    transientDetails.setDocumentFlag(Boolean.TRUE);

                    if (!transientDetails.getMarkedForGroupby() && document!=null) {
                        addToGroupedAndMark(document, trans, cityTaxCodeDtl);
                    }
                }

                trans.setCityTaxcodeDetailId(cityTaxCodeDtl.getTaxcodeDetailId());
                trans.setCityTaxcodeTypeCode(cityTaxCodeDtl.getTaxcodeTypeCode());
                trans.setCityTaxtypeCode(cityTaxCodeDtl.getTaxtypeCode());

                if (trans.getCityTaxtypeUsedCode() == null) {
                    if (!"1".equals(trans.getDirectPayPermitFlag())) {
                        trans.setCityTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                    } else
                        trans.setCityTaxtypeUsedCode(cityTaxCodeDtl.getTaxtypeCode());
                }
                trans.setCityRatetypeCode(cityTaxCodeDtl.getRateTypeCode());
                trans.setCityGoodSvcTypeCode(cityTaxCodeDtl.getGoodSvcTypeCode());
                trans.setCityCitationRef(cityTaxCodeDtl.getCitationRef());
                trans.setCityExemptReason(cityTaxCodeDtl.getExemptReason());

                trans.setCityTaxProcessTypeCode(cityTaxCodeDtl.getTaxProcessTypeCode());
                trans.setCityGroupByDriver(cityTaxCodeDtl.getGroupByDriver());
                trans.setCityAllocBucket(cityTaxCodeDtl.getAllocBucket());
                trans.setCityAllocProrateByCode(cityTaxCodeDtl.getAllocProrateByCode());
                trans.setCityAllocConditionCode(cityTaxCodeDtl.getAllocConditionCode());

                transientDetails.setCityTcdtlSpecialRate(cityTaxCodeDtl.getSpecialRate());
                transientDetails.setCityTcdtlSpecialSetAmt(cityTaxCodeDtl.getSpecialSetAmt());
                transientDetails.setCityTcdtlTaxableThresholdAmt(cityTaxCodeDtl.getTaxableThresholdAmt());
                transientDetails.setCityTcdtlMinimumTaxableAmt(cityTaxCodeDtl.getMinimumTaxableAmt());
                transientDetails.setCityTcdtlMaximumTaxableAmt(cityTaxCodeDtl.getMaximumTaxableAmt());
                transientDetails.setCityTcdtlMaximumTaxAmt(cityTaxCodeDtl.getMaximumTaxAmt());

                trans.setCityTaxableThresholdAmt(cityTaxCodeDtl.getTaxableThresholdAmt());
                trans.setCityMinimumTaxableAmt(cityTaxCodeDtl.getMinimumTaxableAmt());
                trans.setCityMaximumTaxableAmt(cityTaxCodeDtl.getMaximumTaxableAmt());
                trans.setCityMaximumTaxAmt(cityTaxCodeDtl.getMaximumTaxAmt());
                trans.setCityBaseChangePct(cityTaxCodeDtl.getBaseChangePct());

            } else {

                trans.setCityTaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                trans.setCityTaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                if (trans.getCityTaxtypeUsedCode() == null) {
                    if (!"1".equals(trans.getDirectPayPermitFlag())) {
                        trans.setCityTaxtypeUsedCode(TAXTYPE_CODE_SALE);
                    } else
                        trans.setCityTaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                }

                trans.setCityRatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                trans.setCityGoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                trans.setCityCitationRef(stateTaxCodeDtl.getCitationRef());
                trans.setCityExemptReason(stateTaxCodeDtl.getExemptReason());

                trans.setCityTaxProcessTypeCode("0"); // ADDED by MBF 5/22/2019
               /* trans.setCityTaxProcessTypeCode(stateTaxCodeDtl.getTaxProcessTypeCode());
                trans.setCityGroupByDriver(stateTaxCodeDtl.getGroupByDriver());     TAKEN OFF THE CHART BY MBF 03/09/2017  */
                trans.setCityAllocBucket(stateTaxCodeDtl.getAllocBucket());
                trans.setCityAllocProrateByCode(stateTaxCodeDtl.getAllocProrateByCode());
                trans.setCityAllocConditionCode(stateTaxCodeDtl.getAllocConditionCode());

/*                transientDetails.setCityTcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                transientDetails.setCityTcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                transientDetails.setCityTcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                transientDetails.setCityTcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                transientDetails.setCityTcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                transientDetails.setCityTcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
            }

            /*STJ1*/
            if (trans.getStj1Name()!=null) {
                TaxCodeDetail stj1TaxCodeDtl = null;
                if (trans.getStj1TaxcodeDetailId() != null && trans.getStj1TaxcodeDetailId() > 0L) {
                    stj1TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj1TaxcodeDetailId());
                    if (stj1TaxCodeDtl == null)
                        trans.setStj1TaxcodeDetailId(0L);
                } else {
                    trans.setStj1TaxcodeDetailId(0L);
                }

                if (stj1TaxCodeDtl == null)
                    stj1TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj1Name(), trans.getGlDate());

                if (stj1TaxCodeDtl != null) {
                    if ("D".equals(stj1TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj1TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);
                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj1TaxCodeDtl);
                        }
                    }

                    trans.setStj1TaxcodeDetailId(stj1TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj1TaxcodeTypeCode(stj1TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj1TaxtypeCode(stj1TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj1TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj1TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj1TaxtypeUsedCode(stj1TaxCodeDtl.getTaxtypeCode());
                    }
                    trans.setStj1RatetypeCode(stj1TaxCodeDtl.getRateTypeCode());
                    trans.setStj1GoodSvcTypeCode(stj1TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj1CitationRef(stj1TaxCodeDtl.getCitationRef());
                    trans.setStj1ExemptReason(stj1TaxCodeDtl.getExemptReason());

                    transientDetails.setStj1TcdtlSpecialRate(stj1TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj1TcdtlSpecialSetAmt(stj1TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj1TcdtlTaxableThresholdAmt(stj1TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj1TcdtlMinimumTaxableAmt(stj1TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj1TcdtlMaximumTaxableAmt(stj1TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj1TcdtlMaximumTaxAmt(stj1TaxCodeDtl.getMaximumTaxAmt());

                } else {

                    trans.setStj1TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());
                    trans.setStj1TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());

                    if (trans.getStj1TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj1TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj1TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj1RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj1GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj1CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj1ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj1TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj1TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj1TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj1TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj1TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj1TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ2*/
            if (trans.getStj2Name()!=null) {
                TaxCodeDetail stj2TaxCodeDtl = null;
                if (trans.getStj2TaxcodeDetailId() != null && trans.getStj2TaxcodeDetailId() > 0L) {
                    stj2TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj2TaxcodeDetailId());
                    if (stj2TaxCodeDtl == null)
                        trans.setStj2TaxcodeDetailId(0L);
                } else {
                    trans.setStj2TaxcodeDetailId(0L);
                }

                if (stj2TaxCodeDtl == null)
                    stj2TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj2Name(), trans.getGlDate());

                if (stj2TaxCodeDtl != null) {
                    if ("D".equals(stj2TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj2TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj2TaxCodeDtl);
                        }
                    }

                    trans.setStj2TaxcodeDetailId(stj2TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj2TaxcodeTypeCode(stj2TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj2TaxtypeCode(stj2TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj2TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj2TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj2TaxtypeUsedCode(stj2TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj2RatetypeCode(stj2TaxCodeDtl.getRateTypeCode());
                    trans.setStj2GoodSvcTypeCode(stj2TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj2CitationRef(stj2TaxCodeDtl.getCitationRef());
                    trans.setStj2ExemptReason(stj2TaxCodeDtl.getExemptReason());

                    transientDetails.setStj2TcdtlSpecialRate(stj2TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj2TcdtlSpecialSetAmt(stj2TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj2TcdtlTaxableThresholdAmt(stj2TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj2TcdtlMinimumTaxableAmt(stj2TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj2TcdtlMaximumTaxableAmt(stj2TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj2TcdtlMaximumTaxAmt(stj2TaxCodeDtl.getMaximumTaxAmt());
                } else {

                    trans.setStj2TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj2TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj2TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj2TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj2TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj2RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj2GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj2CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj2ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj2TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj2TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj2TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj2TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj2TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj2TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ3*/
            if (trans.getStj3Name()!=null) {
                TaxCodeDetail stj3TaxCodeDtl = null;
                if (trans.getStj3TaxcodeDetailId() != null && trans.getStj3TaxcodeDetailId() > 0L) {
                    stj3TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj3TaxcodeDetailId());
                    if (stj3TaxCodeDtl == null)
                        trans.setStj3TaxcodeDetailId(0L);
                } else {
                    trans.setStj3TaxcodeDetailId(0L);
                }

                if (stj3TaxCodeDtl == null)
                    stj3TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj3Name(), trans.getGlDate());

                if (stj3TaxCodeDtl != null) {
                    if ("D".equals(stj3TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj3TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj3TaxCodeDtl);
                        }
                    }

                    trans.setStj3TaxcodeDetailId(stj3TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj3TaxcodeTypeCode(stj3TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj3TaxtypeCode(stj3TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj3TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj3TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj3TaxtypeUsedCode(stj3TaxCodeDtl.getTaxtypeCode());
                    }
                    trans.setStj3RatetypeCode(stj3TaxCodeDtl.getRateTypeCode());
                    trans.setStj3GoodSvcTypeCode(stj3TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj3CitationRef(stj3TaxCodeDtl.getCitationRef());
                    trans.setStj3ExemptReason(stj3TaxCodeDtl.getExemptReason());

                    transientDetails.setStj3TcdtlSpecialRate(stj3TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj3TcdtlSpecialSetAmt(stj3TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj3TcdtlTaxableThresholdAmt(stj3TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj3TcdtlMinimumTaxableAmt(stj3TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj3TcdtlMaximumTaxableAmt(stj3TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj3TcdtlMaximumTaxAmt(stj3TaxCodeDtl.getMaximumTaxAmt());

                } else {

                    trans.setStj3TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj3TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj3TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj3TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj3TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj3RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj3GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj3CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj3ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj3TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj3TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj3TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj3TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj3TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj3TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ4*/

            if (trans.getStj4Name()!=null) {
                TaxCodeDetail stj4TaxCodeDtl = null;
                if (trans.getStj4TaxcodeDetailId() != null && trans.getStj4TaxcodeDetailId() > 0L) {
                    stj4TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj4TaxcodeDetailId());
                    if (stj4TaxCodeDtl == null)
                        trans.setStj4TaxcodeDetailId(0L);
                } else {
                    trans.setStj4TaxcodeDetailId(0L);
                }

                if (stj4TaxCodeDtl == null)
                    stj4TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj4Name(), trans.getGlDate());

                if (stj4TaxCodeDtl != null) {
                    if ("D".equals(stj4TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj4TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj4TaxCodeDtl);
                        }
                    }

                    trans.setStj4TaxcodeDetailId(stj4TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj4TaxcodeTypeCode(stj4TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj4TaxtypeCode(stj4TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj4TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj4TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj4TaxtypeUsedCode(stj4TaxCodeDtl.getTaxtypeCode());
                    }
                    trans.setStj4RatetypeCode(stj4TaxCodeDtl.getRateTypeCode());
                    trans.setStj4GoodSvcTypeCode(stj4TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj4CitationRef(stj4TaxCodeDtl.getCitationRef());
                    trans.setStj4ExemptReason(stj4TaxCodeDtl.getExemptReason());

                    transientDetails.setStj4TcdtlSpecialRate(stj4TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj4TcdtlSpecialSetAmt(stj4TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj4TcdtlTaxableThresholdAmt(stj4TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj4TcdtlMinimumTaxableAmt(stj4TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj4TcdtlMaximumTaxableAmt(stj4TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj4TcdtlMaximumTaxAmt(stj4TaxCodeDtl.getMaximumTaxAmt());
                } else {
                    //trans.setStj4TaxcodeDetailId(stateTaxCodeDtl.getTaxcodeDetailId());

                    trans.setStj4TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj4TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());


                    if (trans.getStj4TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj4TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj4TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj4RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj4GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj4CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj4ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj4TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj4TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj4TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj4TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj4TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj4TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ5*/
            if (trans.getStj5Name()!=null) {
                TaxCodeDetail stj5TaxCodeDtl = null;
                if (trans.getStj5TaxcodeDetailId() != null && trans.getStj5TaxcodeDetailId() > 0L) {
                    stj5TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj5TaxcodeDetailId());
                    if (stj5TaxCodeDtl == null)
                        trans.setStj5TaxcodeDetailId(0L);
                } else {
                    trans.setStj5TaxcodeDetailId(0L);
                }

                if (stj5TaxCodeDtl == null)
                    stj5TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj5Name(), trans.getGlDate());

                if (stj5TaxCodeDtl != null) {
                    if ("D".equals(stj5TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj5TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj5TaxCodeDtl);
                        }
                    }

                    trans.setStj5TaxcodeDetailId(stj5TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj5TaxcodeTypeCode(stj5TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj5TaxtypeCode(stj5TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj5TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj5TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj5TaxtypeUsedCode(stj5TaxCodeDtl.getTaxtypeCode());
                    }
                    trans.setStj5RatetypeCode(stj5TaxCodeDtl.getRateTypeCode());
                    trans.setStj5GoodSvcTypeCode(stj5TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj5CitationRef(stj5TaxCodeDtl.getCitationRef());
                    trans.setStj5ExemptReason(stj5TaxCodeDtl.getExemptReason());

                    transientDetails.setStj5TcdtlSpecialRate(stj5TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj5TcdtlSpecialSetAmt(stj5TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj5TcdtlTaxableThresholdAmt(stj5TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj5TcdtlMinimumTaxableAmt(stj5TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj5TcdtlMaximumTaxableAmt(stj5TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj5TcdtlMaximumTaxAmt(stj5TaxCodeDtl.getMaximumTaxAmt());
                } else {

                    trans.setStj5TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj5TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj5TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj5TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj5TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }
                    trans.setStj5RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj5GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj5CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj5ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj5TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj5TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj5TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj5TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj5TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj5TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ6*/
            if (trans.getStj6Name()!=null) {
                TaxCodeDetail stj6TaxCodeDtl = null;
                if (trans.getStj6TaxcodeDetailId() != null && trans.getStj6TaxcodeDetailId() > 0L) {
                    stj6TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj6TaxcodeDetailId());
                    if (stj6TaxCodeDtl == null)
                        trans.setStj6TaxcodeDetailId(0L);
                } else {
                    trans.setStj6TaxcodeDetailId(0L);
                }

                if (stj6TaxCodeDtl == null)
                    stj6TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj6Name(), trans.getGlDate());

                if (stj6TaxCodeDtl != null) {
                    if ("D".equals(stj6TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj6TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj6TaxCodeDtl);
                        }
                    }

                    trans.setStj6TaxcodeDetailId(stj6TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj6TaxcodeTypeCode(stj6TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj6TaxtypeCode(stj6TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj6TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj6TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj6TaxtypeUsedCode(stj6TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj6RatetypeCode(stj6TaxCodeDtl.getRateTypeCode());
                    trans.setStj6GoodSvcTypeCode(stj6TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj6CitationRef(stj6TaxCodeDtl.getCitationRef());
                    trans.setStj6ExemptReason(stj6TaxCodeDtl.getExemptReason());

                    transientDetails.setStj6TcdtlSpecialRate(stj6TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj6TcdtlSpecialSetAmt(stj6TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj6TcdtlTaxableThresholdAmt(stj6TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj6TcdtlMinimumTaxableAmt(stj6TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj6TcdtlMaximumTaxableAmt(stj6TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj6TcdtlMaximumTaxAmt(stj6TaxCodeDtl.getMaximumTaxAmt());
                } else {

                    trans.setStj6TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj6TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj6TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj6TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj6TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj6RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj6GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj6CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj6ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj6TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj6TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj6TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj6TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj6TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj6TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ7*/
            if (trans.getStj7Name()!=null) {
                TaxCodeDetail stj7TaxCodeDtl = null;
                if (trans.getStj7TaxcodeDetailId() != null && trans.getStj7TaxcodeDetailId() > 0L) {
                    stj7TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj7TaxcodeDetailId());
                    if (stj7TaxCodeDtl == null)
                        trans.setStj7TaxcodeDetailId(0L);
                } else {
                    trans.setStj7TaxcodeDetailId(0L);
                }

                if (stj7TaxCodeDtl == null)
                    stj7TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj7Name(), trans.getGlDate());

                if (stj7TaxCodeDtl != null) {
                    if ("D".equals(stj7TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj7TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj7TaxCodeDtl);
                        }
                    }

                    trans.setStj7TaxcodeDetailId(stj7TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj7TaxcodeTypeCode(stj7TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj7TaxtypeCode(stj7TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj7TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj7TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj7TaxtypeUsedCode(stj7TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj7RatetypeCode(stj7TaxCodeDtl.getRateTypeCode());
                    trans.setStj7GoodSvcTypeCode(stj7TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj7CitationRef(stj7TaxCodeDtl.getCitationRef());
                    trans.setStj7ExemptReason(stj7TaxCodeDtl.getExemptReason());

                    transientDetails.setStj7TcdtlSpecialRate(stj7TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj7TcdtlSpecialSetAmt(stj7TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj7TcdtlTaxableThresholdAmt(stj7TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj7TcdtlMinimumTaxableAmt(stj7TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj7TcdtlMaximumTaxableAmt(stj7TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj7TcdtlMaximumTaxAmt(stj7TaxCodeDtl.getMaximumTaxAmt());
                } else {

                    trans.setStj7TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj7TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj7TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj7TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj7TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj7TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj7RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj7GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj7CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj7ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj7TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj7TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj7TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj7TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj7TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj7TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ8*/
            if (trans.getStj8Name()!=null) {
                TaxCodeDetail stj8TaxCodeDtl = null;
                if (trans.getStj8TaxcodeDetailId() != null && trans.getStj8TaxcodeDetailId() > 0L) {
                    stj8TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj8TaxcodeDetailId());
                    if (stj8TaxCodeDtl == null)
                        trans.setStj8TaxcodeDetailId(0L);
                } else {
                    trans.setStj8TaxcodeDetailId(0L);
                }

                if (stj8TaxCodeDtl == null)
                    stj8TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj8Name(), trans.getGlDate());

                if (stj8TaxCodeDtl != null) {
                    if ("D".equals(stj8TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj8TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj8TaxCodeDtl);
                        }
                    }

                    trans.setStj8TaxcodeDetailId(stj8TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj8TaxcodeTypeCode(stj8TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj8TaxtypeCode(stj8TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj8TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj8TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj8TaxtypeUsedCode(stj8TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj8RatetypeCode(stj8TaxCodeDtl.getRateTypeCode());
                    trans.setStj8GoodSvcTypeCode(stj8TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj8CitationRef(stj8TaxCodeDtl.getCitationRef());
                    trans.setStj8ExemptReason(stj8TaxCodeDtl.getExemptReason());

                    transientDetails.setStj8TcdtlSpecialRate(stj8TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj8TcdtlSpecialSetAmt(stj8TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj8TcdtlTaxableThresholdAmt(stj8TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj8TcdtlMinimumTaxableAmt(stj8TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj8TcdtlMaximumTaxableAmt(stj8TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj8TcdtlMaximumTaxAmt(stj8TaxCodeDtl.getMaximumTaxAmt());
                } else {
                    trans.setStj8TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj8TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj8TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj8TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj8TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj8RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj8GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj8CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj8ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj8TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj8TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj8TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj8TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj8TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj8TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ9*/
            if (trans.getStj9Name()!=null) {
                TaxCodeDetail stj9TaxCodeDtl = null;
                if (trans.getStj9TaxcodeDetailId() != null && trans.getStj9TaxcodeDetailId() > 0L) {
                    stj9TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj9TaxcodeDetailId());
                    if (stj9TaxCodeDtl == null)
                        trans.setStj9TaxcodeDetailId(0L);
                } else {
                    trans.setStj9TaxcodeDetailId(0L);
                }

                if (stj9TaxCodeDtl == null)
                    stj9TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj9Name(), trans.getGlDate());

                if (stj9TaxCodeDtl != null) {
                    if ("D".equals(stj9TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj9TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj9TaxCodeDtl);
                        }
                    }

                    trans.setStj9TaxcodeDetailId(stj9TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj9TaxcodeTypeCode(stj9TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj9TaxtypeCode(stj9TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj9TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj9TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj9TaxtypeUsedCode(stj9TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj9RatetypeCode(stj9TaxCodeDtl.getRateTypeCode());
                    trans.setStj9GoodSvcTypeCode(stj9TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj9CitationRef(stj9TaxCodeDtl.getCitationRef());
                    trans.setStj9ExemptReason(stj9TaxCodeDtl.getExemptReason());

                    transientDetails.setStj9TcdtlSpecialRate(stj9TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj9TcdtlSpecialSetAmt(stj9TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj9TcdtlTaxableThresholdAmt(stj9TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj9TcdtlMinimumTaxableAmt(stj9TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj9TcdtlMaximumTaxableAmt(stj9TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj9TcdtlMaximumTaxAmt(stj9TaxCodeDtl.getMaximumTaxAmt());

                } else {

                    trans.setStj9TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj9TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj9TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj9TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj9TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj9RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj9GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj9CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj9ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj9TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj9TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj9TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj9TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj9TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj9TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }

            /*STJ10*/
            if (trans.getStj10Name()!=null) {
                TaxCodeDetail stj10TaxCodeDtl = null;
                if (trans.getStj10TaxcodeDetailId() != null && trans.getStj10TaxcodeDetailId() > 0L) {
                    stj10TaxCodeDtl = taxCodeDetailDAO.findById(trans.getStj10TaxcodeDetailId());
                    if (stj10TaxCodeDtl == null)
                        trans.setStj10TaxcodeDetailId(0L);
                } else {
                    trans.setStj10TaxcodeDetailId(0L);
                }

                if (stj10TaxCodeDtl == null)
                    stj10TaxCodeDtl = taxCodeDetailDAO.findByJurLevelBroadSearch(trans.getTaxcodeCode(), "*STJ", trans.getCountryCode(),
                            trans.getStateCode(), trans.getCountyName(), trans.getCityName(), trans.getStj10Name(), trans.getGlDate());

                if (stj10TaxCodeDtl != null) {
                    if ("D".equals(stj10TaxCodeDtl.getTaxcodeTypeCode()))
                        transientDetails.setDistributedFlag(Boolean.TRUE);

                    if ("2".equals(stj10TaxCodeDtl.getTaxProcessTypeCode())) {
                        transientDetails.setDocumentFlag(Boolean.TRUE);

                        if (!transientDetails.getMarkedForGroupby() && document!=null) {
                            addToGroupedAndMark(document, trans, stj10TaxCodeDtl);
                        }
                    }

                    trans.setStj10TaxcodeDetailId(stj10TaxCodeDtl.getTaxcodeDetailId());
                    trans.setStj10TaxcodeTypeCode(stj10TaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj10TaxtypeCode(stj10TaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj10TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj10TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj10TaxtypeUsedCode(stj10TaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj10RatetypeCode(stj10TaxCodeDtl.getRateTypeCode());
                    trans.setStj10GoodSvcTypeCode(stj10TaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj10CitationRef(stj10TaxCodeDtl.getCitationRef());
                    trans.setStj10ExemptReason(stj10TaxCodeDtl.getExemptReason());

                    transientDetails.setStj10TcdtlSpecialRate(stj10TaxCodeDtl.getSpecialRate());
                    transientDetails.setStj10TcdtlSpecialSetAmt(stj10TaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj10TcdtlTaxableThresholdAmt(stj10TaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj10TcdtlMinimumTaxableAmt(stj10TaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj10TcdtlMaximumTaxableAmt(stj10TaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj10TcdtlMaximumTaxAmt(stj10TaxCodeDtl.getMaximumTaxAmt());
                } else {
                    trans.setStj10TaxcodeTypeCode(stateTaxCodeDtl.getTaxcodeTypeCode());
                    trans.setStj10TaxtypeCode(stateTaxCodeDtl.getTaxtypeCode());

                    if (trans.getStj10TaxtypeUsedCode() == null) {
                        if (!"1".equals(trans.getDirectPayPermitFlag())) {
                            trans.setStj10TaxtypeUsedCode(TAXTYPE_CODE_SALE);
                        } else
                            trans.setStj10TaxtypeUsedCode(stateTaxCodeDtl.getTaxtypeCode());
                    }

                    trans.setStj10RatetypeCode(stateTaxCodeDtl.getRateTypeCode());
                    trans.setStj10GoodSvcTypeCode(stateTaxCodeDtl.getGoodSvcTypeCode());
                    trans.setStj10CitationRef(stateTaxCodeDtl.getCitationRef());
                    trans.setStj10ExemptReason(stateTaxCodeDtl.getExemptReason());

/*                    transientDetails.setStj10TcdtlSpecialRate(stateTaxCodeDtl.getSpecialRate());
                    transientDetails.setStj10TcdtlSpecialSetAmt(stateTaxCodeDtl.getSpecialSetAmt());
                    transientDetails.setStj10TcdtlTaxableThresholdAmt(stateTaxCodeDtl.getTaxableThresholdAmt());
                    transientDetails.setStj10TcdtlMinimumTaxableAmt(stateTaxCodeDtl.getMinimumTaxableAmt());
                    transientDetails.setStj10TcdtlMaximumTaxableAmt(stateTaxCodeDtl.getMaximumTaxableAmt());
                    transientDetails.setStj10TcdtlMaximumTaxAmt(stateTaxCodeDtl.getMaximumTaxAmt());*/
                }
            }
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transTaxCodeDtl.name());

            switch (trans.getModuleCode()) {
				case PURCHUSE:
					break;
		
				case BILLSALE:
					// Is Product fully exempt?
					if ("E".equals(trans.getStateTaxcodeTypeCode())
							&& "E".equals(trans.getCountyTaxcodeTypeCode())
							&& "E".equals(trans.getCityTaxcodeTypeCode())
							&& ("E".equals(trans.getStj1TaxcodeTypeCode()) || trans.getStj1TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj2TaxcodeTypeCode()) || trans.getStj2TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj3TaxcodeTypeCode()) || trans.getStj3TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj4TaxcodeTypeCode()) || trans.getStj4TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj5TaxcodeTypeCode()) || trans.getStj5TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj6TaxcodeTypeCode()) || trans.getStj6TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj7TaxcodeTypeCode()) || trans.getStj7TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj8TaxcodeTypeCode()) || trans.getStj8TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj9TaxcodeTypeCode()) || trans.getStj9TaxcodeTypeCode()==null)
							&& ("E".equals(trans.getStj10TaxcodeTypeCode()) || trans.getStj10TaxcodeTypeCode()==null)) {
						trans.setExemptCode("FP"); // Full Exemption for Product
						trans.setExemptAmt(trans.getGlLineItmDistAmt());
						trans.setCountryExemptAmt(trans.getGlLineItmDistAmt());
						trans.setStateExemptAmt(trans.getGlLineItmDistAmt());
						trans.setCountyExemptAmt(trans.getGlLineItmDistAmt());
						trans.setCityExemptAmt(trans.getGlLineItmDistAmt());
						if(trans.getStj1Name() != null) {
							trans.setStj1ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj2Name() != null) {
							trans.setStj2ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj3Name() != null) {
							trans.setStj3ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj4Name() != null) {
							trans.setStj4ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj5Name() != null) {
							trans.setStj5ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj6Name() != null) {
							trans.setStj6ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj7Name() != null) {
							trans.setStj7ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj8Name() != null) {
							trans.setStj8ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj9Name() != null) {
							trans.setStj9ExemptAmt(trans.getGlLineItmDistAmt());
						}
						if(trans.getStj10Name() != null) {
							trans.setStj10ExemptAmt(trans.getGlLineItmDistAmt());
						}
						
						trans.setTransactionInd(TRANS_IND_PROCESSED);
		    			trans.setSuspendInd(null);
		    			throw new PurchaseTranasactionSoftAbortProcessingException("");
					} 
					else if ("E".equals(trans.getStateTaxcodeTypeCode())
							|| "E".equals(trans.getCountyTaxcodeTypeCode())
							|| "E".equals(trans.getCityTaxcodeTypeCode())
							|| "E".equals(trans.getStj1TaxcodeTypeCode())
							|| "E".equals(trans.getStj2TaxcodeTypeCode())
							|| "E".equals(trans.getStj3TaxcodeTypeCode())
							|| "E".equals(trans.getStj4TaxcodeTypeCode())
							|| "E".equals(trans.getStj5TaxcodeTypeCode())
							|| "E".equals(trans.getStj6TaxcodeTypeCode())
							|| "E".equals(trans.getStj7TaxcodeTypeCode())
							|| "E".equals(trans.getStj8TaxcodeTypeCode())
							|| "E".equals(trans.getStj9TaxcodeTypeCode())
							|| "E".equals(trans.getStj10TaxcodeTypeCode())) {
						trans.setExemptCode("PP"); // Partial Exemption for Product
					}
					break;
		
				default:
					break;
	        }


        } catch(PurchaseTranasactionSoftAbortProcessingException saEx) { 	
        	throw saEx;	
        } catch (PurchaseTransactionSuspendedException suspEx) {
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transTaxCodeDtl.name());
            
            switch (trans.getModuleCode()) {
				case PURCHUSE:
					suspend(trans, SuspendReason.SUSPEND_TAXCODE_DTL);
		            suspEx.printStackTrace();
		            logger.error(suspEx);
		            throw suspEx;
		
				case BILLSALE:
					trans.setCountryTaxcodeDetailId(-1L);
				    trans.setStateTaxcodeDetailId(-1L);
				    trans.setCountyTaxcodeDetailId(-1L);
				    trans.setCityTaxcodeDetailId(-1L);

		    		if(trans.getStj1Name() != null) {
		    			trans.setStj1TaxcodeDetailId(-1L);
					}
					if(trans.getStj2Name() != null) {
						trans.setStj2TaxcodeDetailId(-1L);
					}
					if(trans.getStj3Name() != null) {
						trans.setStj3TaxcodeDetailId(-1L);
					}
					if(trans.getStj4Name() != null) {
						trans.setStj4TaxcodeDetailId(-1L);
					}
					if(trans.getStj5Name() != null) {
						trans.setStj5TaxcodeDetailId(-1L);
					}
					if(trans.getStj6Name() != null) {
						trans.setStj6TaxcodeDetailId(-1L);
					}
					if(trans.getStj7Name() != null) {
						trans.setStj7TaxcodeDetailId(-1L);
					}
					if(trans.getStj8Name() != null) {
						trans.setStj8TaxcodeDetailId(-1L);
					}
					if(trans.getStj9Name() != null) {
						trans.setStj9TaxcodeDetailId(-1L);
					}
					if(trans.getStj10Name() != null) {
						trans.setStj10TaxcodeDetailId(-1L);
					}			
					break;
		
				default:
					break;
	        }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
//            if (trans.getErrorCode() == null)
            trans.setErrorCode(e.getMessage());
            writeError(trans);
        }
    }
    
    private MicroApiTransaction fetchTaxRatesGeneric(MicroApiTransaction trans,  TransientPurchaseTransaction transientDetails,
										            HashMap<JurisdictionLevel, Long> situsIds,
										            HashMap<JurisdictionLevel, String> taxTypeCodes) throws PurchaseTransactionSuspendedException {

    	MicroApiTransaction copy = new MicroApiTransaction();
		
		Long countrySitusJurId = situsIds.get(JurisdictionLevel.COUNTRY);
		Long stateSitusJurId = situsIds.get(JurisdictionLevel.STATE);
		Long countySitusJurId = situsIds.get(JurisdictionLevel.COUNTY);
		Long citySitusJurId = situsIds.get(JurisdictionLevel.CITY);
		Long stj1SitusJurId = situsIds.get(JurisdictionLevel.STJ1);
		Long stj2SitusJurId = situsIds.get(JurisdictionLevel.STJ2);
		Long stj3SitusJurId = situsIds.get(JurisdictionLevel.STJ3);
		Long stj4SitusJurId = situsIds.get(JurisdictionLevel.STJ4);
		Long stj5SitusJurId = situsIds.get(JurisdictionLevel.STJ5);
		
		String countryTaxTypeCode = taxTypeCodes.get(JurisdictionLevel.COUNTRY);
		String stateTaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STATE);
		String countyTaxTypeCode = taxTypeCodes.get(JurisdictionLevel.COUNTY);
		String cityTaxTypeCode = taxTypeCodes.get(JurisdictionLevel.CITY);
		String stj1TaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STJ1);
		String stj2TaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STJ2);
		String stj3TaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STJ3);
		String stj4TaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STJ4);
		String stj5TaxTypeCode = taxTypeCodes.get(JurisdictionLevel.STJ5);

		/* keep it if needed
		if(trans.getCountryRatetypeCode()==null) {
			trans.setCountryRatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStateRatetypeCode()==null) {
			trans.setStateRatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getCountyRatetypeCode()==null) {
			trans.setCountyRatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getCityRatetypeCode()==null) {
			trans.setCityRatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStj1RatetypeCode()==null) {
			trans.setStj1RatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStj2RatetypeCode()==null) {
			trans.setStj2RatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStj3RatetypeCode()==null) {
			trans.setStj3RatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStj4RatetypeCode()==null) {
			trans.setStj4RatetypeCode(trans.getRatetypeCode());	
		}
		if(trans.getStj5RatetypeCode()==null) {
			trans.setStj5RatetypeCode(trans.getRatetypeCode());	
		}
		*/
		
		List<JurisdictionTaxrate> aList = jurisdictionDAO.getAllJurisdictionTaxrate(new JurisdictionTaxrate(), 1); 
		
		if (stateSitusJurId != null && stateSitusJurId > 0L
		&& (transientDetails.getStateRateSetFlag() == null || !transientDetails.getStateRateSetFlag())) {
		JurisdictionTaxrate stateTaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stateSitusJurId,
		(trans.getStateRatetypeCode() != null)? trans.getStateRatetypeCode():"0",
		trans.getGlDate());
		if (stateTaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STATE_TAX_RATE_NOT_FOUND);
		transientDetails.setStateRateSetFlag(Boolean.TRUE);
		copy.setStateTaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stateTaxTypeCode)) {
		populateStateUseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stateTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stateTaxTypeCode)) {
		populateStateSaleTaxRates(copy, stateTaxRate);
		}
		
		/*  COUNTRY?    */
		if (stateSitusJurId.equals(countrySitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getCountryRatetypeCode()))) {
		transientDetails.setCountryRateSetFlag(Boolean.TRUE);
		copy.setCountryTaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		if (TAXTYPE_CODE_VENDOR_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countryTaxTypeCode)) {
		populateCountryUseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countryTaxTypeCode)) {
		populateCountrySaleTaxRates(copy, stateTaxRate);
		}
		}
		
		/*    COUNTY?    */
		if (stateSitusJurId.equals(countySitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getCountyRatetypeCode()))) {
		transientDetails.setCountyRateSetFlag(Boolean.TRUE);
		copy.setCountyTaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countyTaxTypeCode)) {
		populateCountyUseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countyTaxTypeCode)) {
		populateCountySaleTaxRates(copy, stateTaxRate);
		}
		}
		/*    CITY?    */
		if (stateSitusJurId.equals(citySitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getCityRatetypeCode()))) {
		transientDetails.setCityRateSetFlag(Boolean.TRUE);
		copy.setCityTaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(cityTaxTypeCode)) {
		populateCityUseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(cityTaxTypeCode)) {
		populateCitySaleTaxRates(copy, stateTaxRate);
		}
		}
		/*    STJ1?    */
		if (stateSitusJurId.equals(stj1SitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getStj1RatetypeCode()))) {
		transientDetails.setStj1RateSetFlag(Boolean.TRUE);
		copy.setStj1TaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj1TaxTypeCode)) {
		populateStj1UseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj1TaxTypeCode)) {
		populateStj1SaleTaxRates(copy, stateTaxRate);
		}
		transientDetails.setStj1EffectiveDate(stateTaxRate.getEffectiveDate());
		}
		/*    STJ2?    */
		if (stateSitusJurId.equals(stj2SitusJurId) &&
		(trans.getCountryRatetypeCode() != null && trans.getCountryRatetypeCode().equals(trans.getStj2RatetypeCode()))) {
		transientDetails.setStj2RateSetFlag(Boolean.TRUE);
		copy.setStj2TaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj2TaxTypeCode)) {
		populateStj2UseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj2TaxTypeCode)) {
		populateStj2SaleTaxRates(copy, stateTaxRate);
		}
		transientDetails.setStj2EffectiveDate(stateTaxRate.getEffectiveDate());
		}
		/*    STJ3?    */
		if (stateSitusJurId.equals(stj3SitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getStj3RatetypeCode()))) {
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)) {
		populateStj3SaleTaxRates(copy, stateTaxRate);
		}
		transientDetails.setStj3EffectiveDate(stateTaxRate.getEffectiveDate());
		}
		/*    STJ4?    */
		if (stateSitusJurId.equals(stj4SitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, stateTaxRate);
		}
		transientDetails.setStj4EffectiveDate(stateTaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (stateSitusJurId.equals(stj5SitusJurId) &&
		(trans.getStateRatetypeCode() != null && trans.getStateRatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stateTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stateTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, stateTaxRate);
		}
		transientDetails.setStj5EffectiveDate(stateTaxRate.getEffectiveDate());
		}
		}
		
		if (countySitusJurId != null && countySitusJurId > 0L
		&& (transientDetails.getCountyRateSetFlag() == null || !transientDetails.getCountyRateSetFlag())) {
		JurisdictionTaxrate countyTaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(countySitusJurId,
		(trans.getCountyRatetypeCode() != null)? trans.getCountyRatetypeCode():"0",
		trans.getGlDate());
		if (countyTaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_COUNTY_TAX_RATE_NOT_FOUND);
		transientDetails.setCountyRateSetFlag(Boolean.TRUE);
		copy.setCountyTaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(countyTaxTypeCode )
		|| TAXTYPE_CODE_USE.equals(countyTaxTypeCode )
		|| TAXTYPE_CODE_GST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countyTaxTypeCode)) {
		populateCountyUseTaxRates(copy, countyTaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(countyTaxTypeCode )
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countyTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countyTaxTypeCode)) {
		populateCountySaleTaxRates(copy, countyTaxRate);
		}
		
		
		/*  COUNTRY?    */
		if (countySitusJurId.equals(countrySitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getCountryRatetypeCode()))) {
		transientDetails.setCountryRateSetFlag(Boolean.TRUE);
		copy.setCountryTaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		if (TAXTYPE_CODE_VENDOR_USE.equals(countryTaxTypeCode )
		|| TAXTYPE_CODE_USE.equals(countryTaxTypeCode )
		|| TAXTYPE_CODE_GST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countryTaxTypeCode)) {
		populateCountryUseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countryTaxTypeCode)) {
		populateCountrySaleTaxRates(copy, countyTaxRate);
		}
		}
		
		/*    CITY?    */
		if (countySitusJurId.equals(citySitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getCityRatetypeCode()))) {
		transientDetails.setCityRateSetFlag(Boolean.TRUE);
		copy.setCityTaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(cityTaxTypeCode)) {
		populateCityUseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(cityTaxTypeCode)) {
		populateCitySaleTaxRates(copy, countyTaxRate);
		}
		}
		/*    STJ1?    */
		if (countySitusJurId.equals(stj1SitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getStj1RatetypeCode()))) {
		transientDetails.setStj1RateSetFlag(Boolean.TRUE);
		copy.setStj1TaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj1TaxTypeCode)) {
		populateStj1UseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj1TaxTypeCode)) {
		populateStj1SaleTaxRates(copy, countyTaxRate);
		}
		transientDetails.setStj1EffectiveDate(countyTaxRate.getEffectiveDate());
		}
		/*    STJ2?    */
		if (countySitusJurId.equals(stj2SitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getStj2RatetypeCode()))) {
		transientDetails.setStj2RateSetFlag(Boolean.TRUE);
		copy.setStj2TaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj2TaxTypeCode)) {
		populateStj2UseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj2TaxTypeCode)) {
		populateStj2SaleTaxRates(copy, countyTaxRate);
		}
		transientDetails.setStj2EffectiveDate(countyTaxRate.getEffectiveDate());
		}
		/*    STJ3?    */
		if (countySitusJurId.equals(stj3SitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getStj3RatetypeCode()))) {
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)) {
		populateStj3SaleTaxRates(copy, countyTaxRate);
		}
		transientDetails.setStj3EffectiveDate(countyTaxRate.getEffectiveDate());
		}
		/*    STJ4?    */
		if (countySitusJurId.equals(stj4SitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, countyTaxRate);
		}
		transientDetails.setStj4EffectiveDate(countyTaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (countySitusJurId.equals(stj5SitusJurId) &&
		(trans.getCountyRatetypeCode() != null && trans.getCountyRatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(countyTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, countyTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, countyTaxRate);
		}
		transientDetails.setStj5EffectiveDate(countyTaxRate.getEffectiveDate());
		}
		}
		
		if (citySitusJurId != null && citySitusJurId > 0L
		&& (transientDetails.getCityRateSetFlag() == null || !transientDetails.getCityRateSetFlag())) {
		JurisdictionTaxrate cityTaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(citySitusJurId,
		(trans.getCityRatetypeCode() != null)? trans.getCityRatetypeCode():"0",
		trans.getGlDate());
		if (cityTaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_CITY_TAX_RATE_NOT_FOUND);
		transientDetails.setCityRateSetFlag(Boolean.TRUE);
		copy.setCityTaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(cityTaxTypeCode)) {
		populateCityUseTaxRates(copy, cityTaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(cityTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(cityTaxTypeCode)) {
		populateCitySaleTaxRates(copy, cityTaxRate);
		}
		
		
		/*  COUNTRY?    */
		if (citySitusJurId.equals(countrySitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getCountryRatetypeCode()))) {
		transientDetails.setCountryRateSetFlag(Boolean.TRUE);
		copy.setCountryTaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		if (TAXTYPE_CODE_VENDOR_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countryTaxTypeCode)) {
		populateCountryUseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countryTaxTypeCode)) {
		populateCountrySaleTaxRates(copy, cityTaxRate);
		}
		}
		
		/*    STJ1?    */
		if (citySitusJurId.equals(stj1SitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getStj1RatetypeCode()))) {
		transientDetails.setStj1RateSetFlag(Boolean.TRUE);
		copy.setStj1TaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj1TaxTypeCode)) {
		populateStj1UseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj1TaxTypeCode)) {
		populateStj1SaleTaxRates(copy, cityTaxRate);
		}
		transientDetails.setStj1EffectiveDate(cityTaxRate.getEffectiveDate());
		}
		/*    STJ2?    */
		if (citySitusJurId.equals(stj2SitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getStj2RatetypeCode()))) {
		transientDetails.setStj2RateSetFlag(Boolean.TRUE);
		copy.setStj2TaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj2TaxTypeCode)) {
		populateStj2UseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj2TaxTypeCode)) {
		populateStj2SaleTaxRates(copy, cityTaxRate);
		}
		transientDetails.setStj2EffectiveDate(cityTaxRate.getEffectiveDate());
		}
		/*    STJ3?    */
		if (citySitusJurId.equals(stj3SitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getStj3RatetypeCode()))) {
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)) {
		populateStj3SaleTaxRates(copy, cityTaxRate);
		}
		transientDetails.setStj3EffectiveDate(cityTaxRate.getEffectiveDate());
		}
		/*    STJ4?    */
		if (citySitusJurId.equals(stj4SitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, cityTaxRate);
		}
		transientDetails.setStj4EffectiveDate(cityTaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (citySitusJurId.equals(stj5SitusJurId) &&
		(trans.getCityRatetypeCode() != null && trans.getCityRatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(cityTaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, cityTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, cityTaxRate);
		}
		transientDetails.setStj5EffectiveDate(cityTaxRate.getEffectiveDate());
		}
		}
		
		if (stj1SitusJurId != null && stj1SitusJurId > 0L
		&& (transientDetails.getStj1RateSetFlag() == null || !transientDetails.getStj1RateSetFlag())) {
		JurisdictionTaxrate stj1TaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stj1SitusJurId,
		(trans.getStj1RatetypeCode() != null)? trans.getStj1RatetypeCode():"0",
		trans.getGlDate());
		if (stj1TaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STJ1_TAX_RATE_NOT_FOUND);
		transientDetails.setStj1RateSetFlag(Boolean.TRUE);
		copy.setStj1TaxrateId(stj1TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj1TaxTypeCode)) {
		populateStj1UseTaxRates(copy, stj1TaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj1TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj1TaxTypeCode)){
		populateStj1SaleTaxRates(copy, stj1TaxRate);
		}
		transientDetails.setStj1EffectiveDate(stj1TaxRate.getEffectiveDate());
		
		/*    STJ2?    */
		if (stj1SitusJurId.equals(stj2SitusJurId) &&
		(trans.getStj1RatetypeCode() != null && trans.getStj1RatetypeCode().equals(trans.getStj2RatetypeCode()))) {
		transientDetails.setStj2RateSetFlag(Boolean.TRUE);
		copy.setStj2TaxrateId(stj1TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj2TaxTypeCode)) {
		populateStj2UseTaxRates(copy, stj1TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj2TaxTypeCode)) {
		populateStj2SaleTaxRates(copy, stj1TaxRate);
		}
		transientDetails.setStj2EffectiveDate(stj1TaxRate.getEffectiveDate());
		}
		/*    STJ3?    */
		if (stj1SitusJurId.equals(stj3SitusJurId) &&
		(trans.getStj1RatetypeCode() != null && trans.getStj1RatetypeCode().equals(trans.getStj3RatetypeCode()))) {
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(stj1TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, stj1TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)) {
		populateStj3SaleTaxRates(copy, stj1TaxRate);
		}
		transientDetails.setStj3EffectiveDate(stj1TaxRate.getEffectiveDate());
		}
		/*    STJ4?    */
		if (stj1SitusJurId.equals(stj4SitusJurId) &&
		(trans.getStj1RatetypeCode() != null && trans.getStj1RatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(stj1TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, stj1TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, stj1TaxRate);
		}
		transientDetails.setStj4EffectiveDate(stj1TaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (stj1SitusJurId.equals(stj5SitusJurId) &&
		(trans.getStj1RatetypeCode() != null && trans.getStj1RatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stj1TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stj1TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, stj1TaxRate);
		}
		transientDetails.setStj5EffectiveDate(stj1TaxRate.getEffectiveDate());
		}
		}
		
		if (stj2SitusJurId != null && stj2SitusJurId > 0L
		&& (transientDetails.getStj2RateSetFlag() == null || !transientDetails.getStj2RateSetFlag())) {
		JurisdictionTaxrate stj2TaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stj2SitusJurId,
		(trans.getStj2RatetypeCode() != null)? trans.getStj2RatetypeCode():"0",
		trans.getGlDate());
		if (stj2TaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STJ2_TAX_RATE_NOT_FOUND);
		transientDetails.setStj2RateSetFlag(Boolean.TRUE);
		copy.setStj2TaxrateId(stj2TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj2TaxTypeCode)) {
		populateStj2UseTaxRates(copy, stj2TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj2TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj2TaxTypeCode)) {
		populateStj2SaleTaxRates(copy, stj2TaxRate);
		}
		transientDetails.setStj2EffectiveDate(stj2TaxRate.getEffectiveDate());
		
		/*    STJ3?    */
		if (stj2SitusJurId.equals(stj3SitusJurId) &&
		(trans.getStj2RatetypeCode() != null && trans.getStj2RatetypeCode().equals(trans.getStj3RatetypeCode()))) {
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(stj2TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, stj2TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)) {
		populateStj3SaleTaxRates(copy, stj2TaxRate);
		}
		transientDetails.setStj3EffectiveDate(stj2TaxRate.getEffectiveDate());
		}
		/*    STJ4?    */
		if (stj2SitusJurId.equals(stj4SitusJurId) &&
		(trans.getStj2RatetypeCode() != null && trans.getStj2RatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(stj2TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, stj2TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, stj2TaxRate);
		}
		transientDetails.setStj4EffectiveDate(stj2TaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (stj2SitusJurId.equals(stj5SitusJurId) &&
		(trans.getStj2RatetypeCode() != null && trans.getStj2RatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stj2TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stj2TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, stj2TaxRate);
		}
		transientDetails.setStj5EffectiveDate(stj2TaxRate.getEffectiveDate());
		}
		}
		
		if (stj3SitusJurId != null && stj3SitusJurId > 0L
		&& (transientDetails.getStj3RateSetFlag() == null || !transientDetails.getStj3RateSetFlag())) {
		JurisdictionTaxrate stj3TaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stj3SitusJurId,
		(trans.getStj3RatetypeCode() != null)? trans.getStj3RatetypeCode():"0",
		trans.getGlDate());
		if (stj3TaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STJ3_TAX_RATE_NOT_FOUND);
		transientDetails.setStj3RateSetFlag(Boolean.TRUE);
		copy.setStj3TaxrateId(stj3TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj3TaxTypeCode)) {
		populateStj3UseTaxRates(copy, stj3TaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj3TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj3TaxTypeCode)){
		populateStj3SaleTaxRates(copy, stj3TaxRate);
		}
		transientDetails.setStj3EffectiveDate(stj3TaxRate.getEffectiveDate());
		/*    STJ4?    */
		if (stj3SitusJurId.equals(stj4SitusJurId) &&
		(trans.getStj3RatetypeCode() != null && trans.getStj3RatetypeCode().equals(trans.getStj4RatetypeCode()))) {
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(stj3TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, stj3TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)) {
		populateStj4SaleTaxRates(copy, stj3TaxRate);
		}
		transientDetails.setStj4EffectiveDate(stj3TaxRate.getEffectiveDate());
		}
		/*    STJ5?    */
		if (stj3SitusJurId.equals(stj5SitusJurId) &&
		(trans.getStj3RatetypeCode() != null && trans.getStj3RatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stj3TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stj3TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, stj3TaxRate);
		}
		transientDetails.setStj5EffectiveDate(stj3TaxRate.getEffectiveDate());
		}
		}
		
		if (stj4SitusJurId != null && stj4SitusJurId > 0L
		&& (transientDetails.getStj4RateSetFlag() == null || !transientDetails.getStj4RateSetFlag())) {
		JurisdictionTaxrate stj4TaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stj4SitusJurId,
		(trans.getStj4RatetypeCode() != null)? trans.getStj4RatetypeCode():"0",
		trans.getGlDate());
		if (stj4TaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STJ4_TAX_RATE_NOT_FOUND);
		transientDetails.setStj4RateSetFlag(Boolean.TRUE);
		copy.setStj4TaxrateId(stj4TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj4TaxTypeCode)) {
		populateStj4UseTaxRates(copy, stj4TaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj4TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj4TaxTypeCode)){
		populateStj4SaleTaxRates(copy, stj4TaxRate);
		}
		transientDetails.setStj4EffectiveDate(stj4TaxRate.getEffectiveDate());
		
		/*    STJ5?    */
		if (stj4SitusJurId.equals(stj5SitusJurId) &&
		(trans.getStj4RatetypeCode() != null && trans.getStj4RatetypeCode().equals(trans.getStj5RatetypeCode()))) {
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stj4TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stj4TaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)) {
		populateStj5SaleTaxRates(copy, stj4TaxRate);
		}
		transientDetails.setStj5EffectiveDate(stj4TaxRate.getEffectiveDate());
		}
		}
		if (stj5SitusJurId != null && stj5SitusJurId > 0L
		&& (transientDetails.getStj5RateSetFlag() == null || !transientDetails.getStj5RateSetFlag())) {
		JurisdictionTaxrate stj5TaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(stj5SitusJurId,
		(trans.getStj5RatetypeCode() != null)? trans.getStj5RatetypeCode():"0",
		trans.getGlDate());
		if (stj5TaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_STJ5_TAX_RATE_NOT_FOUND);
		
		transientDetails.setStj5RateSetFlag(Boolean.TRUE);
		copy.setStj5TaxrateId(stj5TaxRate.getJurisdictionTaxrateId());
		//TRANS.jur_taxtype_used_code IN ('U', 'V')?
		if (TAXTYPE_CODE_VENDOR_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(stj5TaxTypeCode)) {
		populateStj5UseTaxRates(copy, stj5TaxRate);
		} else if(TAXTYPE_CODE_SALE.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(stj5TaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(stj5TaxTypeCode)){
		populateStj5SaleTaxRates(copy, stj5TaxRate);
		}
		transientDetails.setStj5EffectiveDate(stj5TaxRate.getEffectiveDate());
		}
		
		if (countrySitusJurId != null && countrySitusJurId > 0L &&
		trans.getCountryTaxcodeDetailId()!=null && trans.getCountryTaxcodeDetailId() > 0L &&
		(transientDetails.getCountryRateSetFlag() == null || !transientDetails.getCountryRateSetFlag())) {
		JurisdictionTaxrate countryTaxRate = jurisdictionTaxrateDao.findByIdRateTypeCode(countrySitusJurId,
		(trans.getCountryRatetypeCode() != null)? trans.getCountryRatetypeCode():"0",
		trans.getGlDate());
		if (countryTaxRate == null)
		throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_COUNTRY_TAX_RATE_NOT_FOUND);
		
		copy.setCountryTaxrateId(countryTaxRate.getJurisdictionTaxrateId());
		if (TAXTYPE_CODE_VENDOR_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_USE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_INPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_INPUT.equals(countryTaxTypeCode)) {
		populateCountryUseTaxRates(copy, countryTaxRate);
		} else if (TAXTYPE_CODE_SALE.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_HST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_PST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_GST_OUTPUT.equals(countryTaxTypeCode)
		|| TAXTYPE_CODE_QST_OUTPUT.equals(countryTaxTypeCode)) {
		populateCountrySaleTaxRates(copy, countryTaxRate);
		}
		}
		
		if(copy.getCountryTier1MaxAmt()==null || copy.getCountryTier1MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCountryTier1MaxAmt(BigDecimal.valueOf(999999));
        }
        if(copy.getCountryTier2MaxAmt()==null || copy.getCountryTier2MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCountryTier2MaxAmt(BigDecimal.valueOf(999999));
        }
        
        if(copy.getStateTier1MaxAmt()==null || copy.getStateTier1MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setStateTier1MaxAmt(BigDecimal.valueOf(999999));
        }
        if(copy.getStateTier2MaxAmt()==null || copy.getStateTier2MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setStateTier2MaxAmt(BigDecimal.valueOf(999999));
        }
        
        if(copy.getCountyTier1MaxAmt()==null || copy.getCountyTier1MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCountyTier1MaxAmt(BigDecimal.valueOf(999999));
        }
        if(copy.getCountyTier2MaxAmt()==null || copy.getCountyTier2MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCountyTier2MaxAmt(BigDecimal.valueOf(999999));
        }
        
        if(copy.getCityTier1MaxAmt()==null || copy.getCityTier1MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCityTier1MaxAmt(BigDecimal.valueOf(999999));
        }
        if(copy.getCityTier2MaxAmt()==null || copy.getCityTier2MaxAmt().compareTo(BigDecimal.ZERO) ==0){
        	copy.setCityTier2MaxAmt(BigDecimal.valueOf(999999));
        }
        
		return copy;
	}
    
    public void getTransTaxRates(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        try {
            HashMap<JurisdictionLevel, Long> situsIds = new HashMap<>();

            situsIds.put(JurisdictionLevel.COUNTRY, trans.getCountrySitusJurId());
            situsIds.put(JurisdictionLevel.STATE, trans.getStateSitusJurId());
            situsIds.put(JurisdictionLevel.COUNTY, trans.getCountySitusJurId());
            situsIds.put(JurisdictionLevel.CITY, trans.getCitySitusJurId());
            situsIds.put(JurisdictionLevel.STJ1, trans.getStj1SitusJurId());
            situsIds.put(JurisdictionLevel.STJ2, trans.getStj2SitusJurId());
            situsIds.put(JurisdictionLevel.STJ3, trans.getStj3SitusJurId());
            situsIds.put(JurisdictionLevel.STJ4, trans.getStj4SitusJurId());
            situsIds.put(JurisdictionLevel.STJ5, trans.getStj5SitusJurId());

            HashMap<JurisdictionLevel, String> taxTypeCodes = new HashMap<>();

            taxTypeCodes.put(JurisdictionLevel.COUNTRY, trans.getCountryTaxcodeTypeCode());
            taxTypeCodes.put(JurisdictionLevel.STATE, trans.getStateTaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.COUNTY, trans.getCountyTaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.CITY, trans.getCityTaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.STJ1, trans.getStj1TaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.STJ2, trans.getStj2TaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.STJ3, trans.getStj3TaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.STJ4, trans.getStj4TaxtypeUsedCode());
            taxTypeCodes.put(JurisdictionLevel.STJ5, trans.getStj5TaxtypeUsedCode());

            MicroApiTransaction copy = fetchTaxRatesGeneric(trans, transientDetails, situsIds, taxTypeCodes);

            //copy rates back to an original transaction
            copyRates(copy, trans);

            if (trans.getCountryTaxcodeDetailId() != null && trans.getCountryTaxcodeDetailId() > 0L) {

                if ((transientDetails.getCountryTcdtlSpecialRate() != null && !transientDetails.getCountryTcdtlSpecialRate().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountryTcdtlSpecialSetAmt() != null && !transientDetails.getCountryTcdtlSpecialSetAmt().equals(BigDecimal.ZERO))) {
                    trans.setCountryTier2Rate(BigDecimal.ZERO);
                    trans.setCountryTier3Rate(BigDecimal.ZERO);
                    trans.setCountryTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCountryTier2MinAmt(BigDecimal.ZERO);
                    trans.setCountryTier2MaxAmt(BigDecimal.ZERO);
                    trans.setCountryMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCountryTaxcodeOverFlag("1");

                }
                if (transientDetails.getCountryTcdtlSpecialRate() != null && !transientDetails.getCountryTcdtlSpecialRate().equals(BigDecimal.ZERO)) {
                    trans.setCountryTier1Rate(transientDetails.getCountryTcdtlSpecialRate());
                    trans.setCountryTier1Setamt(BigDecimal.ZERO);
                    trans.setCountryTier2Setamt(BigDecimal.ZERO);
                    trans.setCountryTier3Setamt(BigDecimal.ZERO);
                } else if (transientDetails.getCountryTcdtlSpecialSetAmt() != null && !transientDetails.getCountryTcdtlSpecialSetAmt().equals(BigDecimal.ZERO)) {
                    trans.setCountryTier1Setamt(transientDetails.getCountryTcdtlSpecialSetAmt());
                    trans.setCountryTier1Rate(BigDecimal.ZERO);
                    trans.setCountryTier2Setamt(BigDecimal.ZERO);
                    trans.setCountryTier3Setamt(BigDecimal.ZERO);
                }

                if ((transientDetails.getCountryTcdtlTaxableThresholdAmt() != null && !transientDetails.getCountryTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountryTcdtlMinimumTaxableAmt() != null && !transientDetails.getCountryTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountryTcdtlMaximumTaxableAmt() != null && !transientDetails.getCountryTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountryTcdtlMaximumTaxAmt() != null && !transientDetails.getCountryTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)))  {

                    if (transientDetails.getCountryTcdtlTaxableThresholdAmt() != null && !transientDetails.getCountryTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountryTaxableThresholdAmt(transientDetails.getCountryTcdtlTaxableThresholdAmt());
                    } else if (transientDetails.getCountryTcdtlMinimumTaxableAmt() != null && !transientDetails.getCountryTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountryMinimumTaxableAmt(transientDetails.getCountryTcdtlMinimumTaxableAmt());
                    } else if (transientDetails.getCountryTcdtlMaximumTaxableAmt() != null && !transientDetails.getCountryTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountryMaximumTaxableAmt(transientDetails.getCountryTcdtlMaximumTaxableAmt());
                    } else if (transientDetails.getCountryTcdtlMaximumTaxAmt() != null && !transientDetails.getCountryTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountryMaxtaxAmt(transientDetails.getCountryTcdtlMaximumTaxAmt());
                    }


                    trans.setCountryTier2Rate(BigDecimal.ZERO);
                    trans.setCountryTier3Rate(BigDecimal.ZERO);
                    trans.setCountryTier2Setamt(BigDecimal.ZERO);
                    trans.setCountryTier3Setamt(BigDecimal.ZERO);
                    trans.setCountryTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCountryTier2MinAmt(BigDecimal.ZERO);
                    trans.setCountryTier2MaxAmt(BigDecimal.ZERO);
                    //trans.setCountryTier3MinAmt(BigDecimal.ZERO);
                    trans.setCountryMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCountryTaxcodeOverFlag("1");
                }

                if (transientDetails.getCountryTcdtlMaximumTaxAmt()!=null && !transientDetails.getCountryTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                    trans.setCountryMaximumTaxAmt(transientDetails.getCountryTcdtlMaximumTaxAmt());
                    trans.setCountryTaxcodeOverFlag("1");
                }

                //TRANS.country_base_change_pct <> 0?
                //TRANS.country_taxcode_over_flag = "1"
                if (trans.getCountryBaseChangePct()!=null && !trans.getCountryBaseChangePct().equals(BigDecimal.ZERO))
                    trans.setCountryTaxcodeOverFlag("1");

            } if (trans.getStateTaxcodeDetailId() != null && trans.getStateTaxcodeDetailId() > 0L) {

                if ((transientDetails.getStateTcdtlSpecialRate() != null && !transientDetails.getStateTcdtlSpecialRate().equals(BigDecimal.ZERO))
                        || (transientDetails.getStateTcdtlSpecialSetAmt() != null && !transientDetails.getStateTcdtlSpecialSetAmt().equals(BigDecimal.ZERO))) {
                    trans.setStateTier2Rate(BigDecimal.ZERO);
                    trans.setStateTier3Rate(BigDecimal.ZERO);
                    trans.setStateTier1MaxAmt(BigDecimal.ZERO);
                    trans.setStateTier2MinAmt(BigDecimal.ZERO);
                    trans.setStateTier2MaxAmt(BigDecimal.ZERO);
                    trans.setStateMaxtaxAmt(BigDecimal.ZERO);

                    trans.setStateTaxcodeOverFlag("1");
                }

                if (transientDetails.getStateTcdtlSpecialRate() != null && !transientDetails.getStateTcdtlSpecialRate().equals(BigDecimal.ZERO)) {
                    trans.setStateTier1Rate(transientDetails.getStateTcdtlSpecialRate());
                    trans.setStateTier1Setamt(BigDecimal.ZERO);
                    trans.setStateTier2Setamt(BigDecimal.ZERO);
                    trans.setStateTier3Setamt(BigDecimal.ZERO);
                } else if (transientDetails.getStateTcdtlSpecialSetAmt() != null && !transientDetails.getStateTcdtlSpecialSetAmt().equals(BigDecimal.ZERO)) {
                    trans.setStateTier1Setamt(transientDetails.getStateTcdtlSpecialSetAmt());
                    trans.setStateTier1Rate(BigDecimal.ZERO);
                    trans.setStateTier2Setamt(BigDecimal.ZERO);
                    trans.setStateTier3Setamt(BigDecimal.ZERO);
                }

                if ((transientDetails.getStateTcdtlTaxableThresholdAmt() != null && !transientDetails.getStateTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getStateTcdtlMinimumTaxableAmt() != null && !transientDetails.getStateTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getStateTcdtlMaximumTaxableAmt() != null && !transientDetails.getStateTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getStateTcdtlMaximumTaxAmt() != null && !transientDetails.getStateTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)))  {

                    if (transientDetails.getStateTcdtlTaxableThresholdAmt() != null && !transientDetails.getStateTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO)) {
                        trans.setStateTaxableThresholdAmt(transientDetails.getStateTcdtlTaxableThresholdAmt());
                    } else if (transientDetails.getStateTcdtlMinimumTaxableAmt() != null && !transientDetails.getStateTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setStateMinimumTaxableAmt(transientDetails.getStateTcdtlMinimumTaxableAmt());
                    } else if (transientDetails.getStateTcdtlMaximumTaxableAmt() != null && !transientDetails.getStateTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setStateMaximumTaxableAmt(transientDetails.getStateTcdtlMaximumTaxableAmt());
                    } else if (transientDetails.getStateTcdtlMaximumTaxAmt() != null && !transientDetails.getStateTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                        trans.setStateMaxtaxAmt(transientDetails.getStateTcdtlMaximumTaxAmt());
                    }

                    trans.setStateTier2Rate(BigDecimal.ZERO);
                    trans.setStateTier3Rate(BigDecimal.ZERO);
                    trans.setStateTier2Setamt(BigDecimal.ZERO);
                    trans.setStateTier3Setamt(BigDecimal.ZERO);
                    trans.setStateTier1MaxAmt(BigDecimal.ZERO);
                    trans.setStateTier2MinAmt(BigDecimal.ZERO);
                    trans.setStateTier2MaxAmt(BigDecimal.ZERO);
                    //trans.setCountryTier3MinAmt(BigDecimal.ZERO);
                    trans.setStateMaxtaxAmt(BigDecimal.ZERO);

                    trans.setStateTaxcodeOverFlag("1");
                }

                if (transientDetails.getStateTcdtlMaximumTaxAmt()!=null && !transientDetails.getStateTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                    trans.setStateMaximumTaxAmt(transientDetails.getStateTcdtlMaximumTaxAmt());
                    trans.setStateTaxcodeOverFlag("1");
                }

                if (trans.getStateBaseChangePct()!=null && !trans.getStateBaseChangePct().equals(BigDecimal.ZERO))
                    trans.setStateTaxcodeOverFlag("1");

            }
            if (trans.getCountyTaxcodeDetailId() != null && trans.getCountyTaxcodeDetailId() > 0L) {
                if ((transientDetails.getCountyTcdtlSpecialRate() != null && !transientDetails.getCountyTcdtlSpecialRate().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountyTcdtlSpecialSetAmt() != null && !transientDetails.getCountyTcdtlSpecialSetAmt().equals(BigDecimal.ZERO))) {
                    trans.setCountyTier2Rate(BigDecimal.ZERO);
                    trans.setCountyTier3Rate(BigDecimal.ZERO);
                    trans.setCountyTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCountyTier2MinAmt(BigDecimal.ZERO);
                    trans.setCountyTier2MaxAmt(BigDecimal.ZERO);
                    trans.setCountyMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCountyTaxcodeOverFlag("1");

                }
                if (transientDetails.getCountyTcdtlSpecialRate() != null && !transientDetails.getCountyTcdtlSpecialRate().equals(BigDecimal.ZERO)) {
                    trans.setCountyTier1Rate(transientDetails.getCountyTcdtlSpecialRate());
                    trans.setCountyTier1Setamt(BigDecimal.ZERO);
                    trans.setCountyTier2Setamt(BigDecimal.ZERO);
                    trans.setCountyTier3Setamt(BigDecimal.ZERO);
                } else if (transientDetails.getCountyTcdtlSpecialSetAmt() != null && !transientDetails.getCountyTcdtlSpecialSetAmt().equals(BigDecimal.ZERO)) {
                    trans.setCountyTier1Setamt(transientDetails.getCountyTcdtlSpecialSetAmt());
                    trans.setCountyTier1Rate(BigDecimal.ZERO);
                    trans.setCountyTier2Setamt(BigDecimal.ZERO);
                    trans.setCountyTier3Setamt(BigDecimal.ZERO);
                }

                if ((transientDetails.getCountyTcdtlTaxableThresholdAmt() != null && !transientDetails.getCountyTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountyTcdtlMinimumTaxableAmt() != null && !transientDetails.getCountyTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountyTcdtlMaximumTaxableAmt() != null && !transientDetails.getCountyTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCountyTcdtlMaximumTaxAmt() != null && !transientDetails.getCountyTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)))  {

                    if (transientDetails.getCountyTcdtlTaxableThresholdAmt() != null && !transientDetails.getCountyTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountyTaxableThresholdAmt(transientDetails.getCountyTcdtlTaxableThresholdAmt());
                    } else if (transientDetails.getCountyTcdtlMinimumTaxableAmt() != null && !transientDetails.getCountyTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountyMinimumTaxableAmt(transientDetails.getCountyTcdtlMinimumTaxableAmt());
                    } else if (transientDetails.getCountyTcdtlMaximumTaxableAmt() != null && !transientDetails.getCountyTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountyMaximumTaxableAmt(transientDetails.getCountyTcdtlMaximumTaxableAmt());
                    } else if (transientDetails.getCountyTcdtlMaximumTaxAmt() != null && !transientDetails.getCountyTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                        trans.setCountyMaxtaxAmt(transientDetails.getCountyTcdtlMaximumTaxAmt());
                    }

                    trans.setCountyTier2Rate(BigDecimal.ZERO);
                    trans.setCountyTier3Rate(BigDecimal.ZERO);
                    trans.setCountyTier2Setamt(BigDecimal.ZERO);
                    trans.setCountyTier3Setamt(BigDecimal.ZERO);
                    trans.setCountyTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCountyTier2MinAmt(BigDecimal.ZERO);
                    trans.setCountyTier2MaxAmt(BigDecimal.ZERO);
                    //trans.setCountyTier3MinAmt(BigDecimal.ZERO);
                    trans.setCountyMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCountyTaxcodeOverFlag("1");
                }

                if (transientDetails.getCountyTcdtlMaximumTaxAmt()!=null && !transientDetails.getCountyTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                    trans.setCountyMaximumTaxAmt(transientDetails.getCountyTcdtlMaximumTaxAmt());
                    trans.setCountyTaxcodeOverFlag("1");
                }

                if (trans.getCountyBaseChangePct()!=null && !trans.getCountyBaseChangePct().equals(BigDecimal.ZERO))
                    trans.setCountyTaxcodeOverFlag("1");
            }
            if (trans.getCityTaxcodeDetailId() != null && trans.getCityTaxcodeDetailId() > 0L) {
                if ((transientDetails.getCityTcdtlSpecialRate() != null && !transientDetails.getCityTcdtlSpecialRate().equals(BigDecimal.ZERO))
                        || (transientDetails.getCityTcdtlSpecialSetAmt() != null && !transientDetails.getCityTcdtlSpecialSetAmt().equals(BigDecimal.ZERO))) {
                    trans.setCityTier2Rate(BigDecimal.ZERO);
                    trans.setCityTier3Rate(BigDecimal.ZERO);
                    trans.setCityTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCityTier2MinAmt(BigDecimal.ZERO);
                    trans.setCityTier2MaxAmt(BigDecimal.ZERO);
                    trans.setCityMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCityTaxcodeOverFlag("1");

                }
                if (transientDetails.getCityTcdtlSpecialRate() != null && !transientDetails.getCityTcdtlSpecialRate().equals(BigDecimal.ZERO)) {
                    trans.setCityTier1Rate(transientDetails.getCityTcdtlSpecialRate());
                    trans.setCityTier1Setamt(BigDecimal.ZERO);
                    trans.setCityTier2Setamt(BigDecimal.ZERO);
                    trans.setCityTier3Setamt(BigDecimal.ZERO);
                } else if (transientDetails.getCityTcdtlSpecialSetAmt() != null && !transientDetails.getCityTcdtlSpecialSetAmt().equals(BigDecimal.ZERO)) {
                    trans.setCityTier1Setamt(transientDetails.getCityTcdtlSpecialSetAmt());
                    trans.setCityTier1Rate(BigDecimal.ZERO);
                    trans.setCityTier2Setamt(BigDecimal.ZERO);
                    trans.setCityTier3Setamt(BigDecimal.ZERO);
                }

                if ((transientDetails.getCityTcdtlTaxableThresholdAmt() != null && !transientDetails.getCityTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCityTcdtlMinimumTaxableAmt() != null && !transientDetails.getCityTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCityTcdtlMaximumTaxableAmt() != null && !transientDetails.getCityTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO))
                        || (transientDetails.getCityTcdtlMaximumTaxAmt() != null && !transientDetails.getCityTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)))  {

                    if (transientDetails.getCityTcdtlTaxableThresholdAmt() != null && !transientDetails.getCityTcdtlTaxableThresholdAmt().equals(BigDecimal.ZERO)) {
                        trans.setCityTaxableThresholdAmt(transientDetails.getCityTcdtlTaxableThresholdAmt());
                    } else if (transientDetails.getCityTcdtlMinimumTaxableAmt() != null && !transientDetails.getCityTcdtlMinimumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCityMinimumTaxableAmt(transientDetails.getCityTcdtlMinimumTaxableAmt());
                    } else if (transientDetails.getCityTcdtlMaximumTaxableAmt() != null && !transientDetails.getCityTcdtlMaximumTaxableAmt().equals(BigDecimal.ZERO)) {
                        trans.setCityMaximumTaxableAmt(transientDetails.getCityTcdtlMaximumTaxableAmt());
                    } else if (transientDetails.getCityTcdtlMaximumTaxAmt() != null && !transientDetails.getCityTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                        trans.setCityMaxtaxAmt(transientDetails.getCityTcdtlMaximumTaxAmt());
                    }

                    trans.setCityTier2Rate(BigDecimal.ZERO);
                    trans.setCityTier3Rate(BigDecimal.ZERO);
                    trans.setCityTier2Setamt(BigDecimal.ZERO);
                    trans.setCityTier3Setamt(BigDecimal.ZERO);
                    trans.setCityTier1MaxAmt(BigDecimal.ZERO);
                    trans.setCityTier2MinAmt(BigDecimal.ZERO);
                    trans.setCityTier2MaxAmt(BigDecimal.ZERO);
                    //trans.setCityTier3MinAmt(BigDecimal.ZERO);
                    trans.setCityMaxtaxAmt(BigDecimal.ZERO);

                    trans.setCityTaxcodeOverFlag("1");
                }

                if (transientDetails.getCityTcdtlMaximumTaxAmt()!=null && !transientDetails.getCityTcdtlMaximumTaxAmt().equals(BigDecimal.ZERO)) {
                    trans.setCityMaximumTaxAmt(transientDetails.getCityTcdtlMaximumTaxAmt());
                    trans.setCityTaxcodeOverFlag("1");
                }

                if (trans.getCityBaseChangePct()!=null && !trans.getCityBaseChangePct().equals(BigDecimal.ZERO))
                    trans.setCityTaxcodeOverFlag("1");

            }
            if (trans.getStj1TaxcodeDetailId() != null && trans.getStj1TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj2TaxcodeDetailId() != null && trans.getStj2TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj3TaxcodeDetailId() != null && trans.getStj3TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj4TaxcodeDetailId() != null && trans.getStj4TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj5TaxcodeDetailId() != null && trans.getStj5TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj6TaxcodeDetailId() != null && trans.getStj6TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj7TaxcodeDetailId() != null && trans.getStj7TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj8TaxcodeDetailId() != null && trans.getStj8TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj9TaxcodeDetailId() != null && trans.getStj9TaxcodeDetailId() > 0L) {

            }
            if (trans.getStj10TaxcodeDetailId() != null && trans.getStj10TaxcodeDetailId() > 0L) {

            }

            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transTaxRates.name());

        } catch (PurchaseTransactionSuspendedException suspEx) {
            transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transTaxRates.name());
            suspend(trans, SuspendReason.SUSPEND_RATE);
            suspEx.printStackTrace();
            logger.error(suspEx);
            if (trans.getErrorCode() == null) {
                trans.setErrorCode(suspEx.getMessage());
                writeError(trans);
            }
            throw new PurchaseTransactionSuspendedException(suspEx.getMessage());

        } catch (PurchaseTransactionProcessingException ptpex) {
            ptpex.printStackTrace();
            logger.error(ptpex);
            if (trans.getErrorCode() == null)
                trans.setErrorCode(ptpex.getMessage());
            writeError(trans);
            throw new PurchaseTransactionAbortProcessingException(ptpex.getMessage());

        }
    }
    
    public void getTransCalcAllTaxes(MicroApiTransaction trans, MicroApiTransactionDocument document) throws PurchaseTransactionProcessingException {
        //TODO remove this (or make conditional )
        //zeroBigDecimalsIfNull(trans);
       /* if (document!=null && document.getGroupedDocument() == null) {
            for(PurchaseTransaction line : document.getPurchaseTransaction()) {
                //line.getTax
            }
        }*/

        //TRANS.country_taxcode_type_code = "T"
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();


        if (trans == null) throw new PurchaseTransactionAbortProcessingException(PROCESSING_ERROR_PS0);

        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getCountryTaxcodeTypeCode()) &&
                (transientDetails.getCountryCalcFlag()!=null && transientDetails.getCountryCalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.COUNTRY);

        if ((TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStateTaxcodeTypeCode()) )&&
                (transientDetails.getStateCalcFlag()!=null && transientDetails.getStateCalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STATE);

        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getCountyTaxcodeTypeCode()) &&
                (transientDetails.getCountyCalcFlag()!=null &&  transientDetails.getCountyCalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.COUNTY);

        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getCityTaxcodeTypeCode()) &&
                (transientDetails.getCityCalcFlag()!=null && transientDetails.getCityCalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.CITY);

        if (trans.getStj1Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj1TaxcodeTypeCode()) &&
                (transientDetails.getStj1CalcFlag()!=null && transientDetails.getStj1CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ1);

        if (trans.getStj2Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj2TaxcodeTypeCode()) &&
                (transientDetails.getStj2CalcFlag()!=null && transientDetails.getStj2CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ2);

        if (trans.getStj3Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj3TaxcodeTypeCode()) &&
                (transientDetails.getStj3CalcFlag()!=null && transientDetails.getStj3CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ3);

        if (trans.getStj4Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj4TaxcodeTypeCode()) &&
                (transientDetails.getStj4CalcFlag()!=null && transientDetails.getStj4CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ4);

        if (trans.getStj5Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj5TaxcodeTypeCode()) &&
                (transientDetails.getStj5CalcFlag()!=null && transientDetails.getStj5CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ5);

        if (trans.getStj6Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj6TaxcodeTypeCode()) &&
                (transientDetails.getStj6CalcFlag()!=null && transientDetails.getStj6CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ6);

        if (trans.getStj7Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj7TaxcodeTypeCode()) &&
                (transientDetails.getStj7CalcFlag()!=null && transientDetails.getStj7CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ7);

        if (trans.getStj8Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj8TaxcodeTypeCode()) &&
                (transientDetails.getStj8CalcFlag()!=null && transientDetails.getStj8CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ8);

        if (trans.getStj9Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj9TaxcodeTypeCode()) &&
                (transientDetails.getStj9CalcFlag()!=null && transientDetails.getStj9CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ9);

        if (trans.getStj10Name()!=null && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(trans.getStj10TaxcodeTypeCode()) &&
                (transientDetails.getStj10CalcFlag()!=null && transientDetails.getStj10CalcFlag()))
            calcTaxAmounts(trans, document, JurisdictionLevel.STJ10);




        trans.setTbCalcTaxAmt(nullToZero(trans.getCountryTier1TaxAmt())
                .add(nullToZero(trans.getCountryTier2TaxAmt()))
                .add(nullToZero(trans.getCountryTier3TaxAmt()))
                .add(nullToZero(trans.getStateTier1TaxAmt()))
                .add(nullToZero(trans.getStateTier2TaxAmt()))
                .add(nullToZero(trans.getStateTier3TaxAmt()))
                .add(nullToZero(trans.getCountyTier1TaxAmt()))
                .add(nullToZero(trans.getCountyTier2TaxAmt()))
                .add(nullToZero(trans.getCountyTier3TaxAmt()))
                .add(nullToZero(trans.getCityTier1TaxAmt()))
                .add(nullToZero(trans.getCityTier2TaxAmt()))
                .add(nullToZero(trans.getCityTier3TaxAmt()))
                .add(nullToZero(trans.getStj1TaxAmt()))
                .add(nullToZero(trans.getStj2TaxAmt()))
                .add(nullToZero(trans.getStj3TaxAmt()))
                .add(nullToZero(trans.getStj4TaxAmt()))
                .add(nullToZero(trans.getStj5TaxAmt()))
                .add(nullToZero(trans.getStj6TaxAmt()))
                .add(nullToZero(trans.getStj7TaxAmt()))
                .add(nullToZero(trans.getStj8TaxAmt()))
                .add(nullToZero(trans.getStj9TaxAmt()))
                .add(nullToZero(trans.getStj10TaxAmt()))
            );
        transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transCalcAllTaxes.name());
    }
    
    public void getPurchaseTransactionInvoiceVerification(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	getPurchaseTransactionInvoiceVerification(document, writeFlag, LogSourceEnum.InvoiceVerification);
    }
    
    public void getPurchaseTransactionInvoiceVerification(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
    	
    	for(MicroApiTransaction transaction: document.getMicroApiTransaction()){
			transaction.setInvoiceVerificationFlag(Boolean.TRUE);
		}

    	taxEstimate(document, writeFlag, callingFromMethod);
    }
    
    public void getPurchaseTransactionReverse(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();

        for (MicroApiTransaction purchaseTransaction: transactions) {      	
        	processReverse(purchaseTransaction, writeFlag, LogSourceEnum.Reverse);
        }
    }

    public void getPurchaseTransactionLoadAndReverse(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	//Check purchaseId 
        	boolean idExist = false;
        	if(purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()>0L){
        		idExist = true;
        	}
        	
        	if(idExist){
        		System.out.println("purchaseTransaction.getPurchtransId() : " + purchaseTransaction.getPurchtransId());
        		PurchaseTransaction tempTrans = purchaseTransactionDAO.findById(purchaseTransaction.getPurchtransId());
        		if(tempTrans==null){
        			idExist = false;
        		}
        		else{
        			BeanUtils.copyProperties(tempTrans, purchaseTransaction);
        			getTransTaxCodeDtl(purchaseTransaction, null);
        			getTransTaxRates(purchaseTransaction);
        		}
        	}
        	
        	if(!idExist){	
        		purchaseTransaction.setErrorCode(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
                writeError(purchaseTransaction);        		
        		continue;
        	}

        	processReverse(purchaseTransaction, writeFlag,LogSourceEnum.LoadAndReverse);
        }
    }
    
    public void getPurchaseTransactionLoadAndReprocess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	getPurchaseTransactionLoadAndReprocess(document, writeFlag, LogSourceEnum.LoadAndReprocess);
    }
    
    public void getPurchaseTransactionLoadAndReprocess(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	//Check purchaseId 
        	boolean idExist = false;
        	if(purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()>0L){
        		idExist = true;
        	}
        	
        	if(idExist){
        		System.out.println("purchaseTransaction.getPurchtransId() : " + purchaseTransaction.getPurchtransId());
        		PurchaseTransaction tempTrans = purchaseTransactionDAO.findById(purchaseTransaction.getPurchtransId());
        		if(tempTrans==null){
        			idExist = false;
        		}
        		else{
        			BeanUtils.copyProperties(tempTrans, purchaseTransaction);
        			getTransTaxCodeDtl(purchaseTransaction, null);
        			getTransTaxRates(purchaseTransaction);
        		}
        	}
        	
        	purchaseTransaction.setReprocessFlag("1");
        	
        	if(!idExist){	
        		purchaseTransaction.setErrorCode(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
                writeError(purchaseTransaction);        		
        		continue;
        	}
        }
        
        taxEstimate(document, writeFlag, callingFromMethod);
    }
    
    public void getPurchaseTransactionEvaluateForAccrual(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	getPurchaseTransactionEvaluateForAccrual(document, writeFlag,  LogSourceEnum.EvaluateForAccrual);
    }
  
    public void getPurchaseTransactionEvaluateForAccrual(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
    	for(MicroApiTransaction transaction: document.getMicroApiTransaction()){
			transaction.setDirectPayPermitFlag("1");
		}

    	taxEstimate(document, writeFlag, callingFromMethod);
    }
    
    public void processReverse(PurchaseTransaction purchaseTransaction, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	processReverse(purchaseTransaction, writeFlag, LogSourceEnum.processReverse);
    }
    
    public void processReverse(PurchaseTransaction purchaseTransaction, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
        TransactionLog log = null;
        if (writeFlag) {
            log = new TransactionLog();
            log.setLogSource(callingFromMethod.name());
            transactionLogService.fillBeforeEntries(log, purchaseTransaction);
        }
        
        getTransReverse(purchaseTransaction);
            
        if (writeFlag) {
            transactionLogService.fillAfterEntries(log, purchaseTransaction);

            if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                purchaseTransaction.setPurchtransId(null);

            purchaseTransactionDAO.save(purchaseTransaction);

            log.setPurchtransId(purchaseTransaction.getPurchtransId());

            PurchaseTransactionLog purchaseTransactionLog = new PurchaseTransactionLog();
    		BeanUtils.copyProperties(log, purchaseTransactionLog);
    		purchaseTransactionLogDAO.save(purchaseTransactionLog);
    		log.setPurchtransLogId(purchaseTransactionLog.getPurchtransLogId());
        }
    }
    
    public void processReverse(MicroApiTransaction purchaseTransaction, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	processReverse(purchaseTransaction, writeFlag, LogSourceEnum.processReverse);
    }
    
    public void processReverse(MicroApiTransaction purchaseTransaction, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {
        TransactionLog log = null;
        if (writeFlag) {
            log = new TransactionLog();
//            if(purchaseTransaction.isBillSaleModuleCode()) {
//            	log.setLogSource(ProcessStep.billTransactionProcess.name());
//            }
//            else {
//            	log.setLogSource(ProcessStep.purchaseTransactionProcess.name());
//            } raghu enum
            log.setLogSource(callingFromMethod.name());  //raghu enum
            transactionLogService.fillBeforeEntries(log, purchaseTransaction);
        }
        
        getTransReverse(purchaseTransaction);
            
        if (writeFlag) {
            transactionLogService.fillAfterEntries(log, purchaseTransaction);

            if (purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()==0L)
                purchaseTransaction.setPurchtransId(null);

            switch (purchaseTransaction.getModuleCode()) {
				case PURCHUSE:
					PurchaseTransaction updatedPurchaseTransaction = new PurchaseTransaction();
					purchaseTransaction.updatePurchaseTransaction(updatedPurchaseTransaction);
					purchaseTransactionDAO.save(updatedPurchaseTransaction);
					purchaseTransaction.setPurchtransId(updatedPurchaseTransaction.getPurchtransId());
					
					log.setPurchtransId(purchaseTransaction.getPurchtransId());
	
					PurchaseTransactionLog purchaseTransactionLog = new PurchaseTransactionLog();
		    		BeanUtils.copyProperties(log, purchaseTransactionLog);
		    		purchaseTransactionLogDAO.save(purchaseTransactionLog);
		    		log.setPurchtransLogId(purchaseTransactionLog.getPurchtransLogId());
	                
					break;
		
				case BILLSALE:
					//To DO:
					break;
		
				default:
					break;
	        }
        }
    }

    public ProcessSuspendedTransactionDocument processSuspendedTransactions(Long newSuspendRuleId, SuspendReason suspendReason, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	System.out.println("************** newSuspendRuleId : " + newSuspendRuleId + ", suspendReason : " + suspendReason);
    	
    	ProcessSuspendedTransactionDocument  procSusDoc = purchaseTransactionDAO.findSuspendedTransactions(newSuspendRuleId, suspendReason);
    	List<PurchaseTransaction> purchaseTransactions = procSusDoc.getPurchaseTransaction();
    	
    	//Purchase Transaction DAO is null currently.
    	PurchaseTransactionDocument documents = new PurchaseTransactionDocument();
    	documents.setPurchaseTransaction(purchaseTransactions);
    
    	MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(documents);
    	getPurchaseTransactionEvaluateForAccrual(microApiTransactionDocument, writeFlag, LogSourceEnum.processSuspendedTransactions);
    	List<MicroApiTransaction> processedTransactions = microApiTransactionDocument.getMicroApiTransaction();
    	purchaseTransactions = new ArrayList<>();
    	for(MicroApiTransaction trans : processedTransactions) {
    		
    		PurchaseTransaction updatedTransaction = new PurchaseTransaction();
    		trans.updatePurchaseTransaction(updatedTransaction);
    		purchaseTransactions.add(updatedTransaction);
    	}
    	
    	
    	procSusDoc.setPurchaseTransaction(purchaseTransactions);
    	
        return procSusDoc;
    }

    private void calcMaxTax(PurchaseTransactionDocument document) {
        Map<TaxCodeRuleGroupBy, List<PurchaseTransaction>> groups = document.getTransientPurchaseTransactionDocument().getGroupedDocument();
        if (groups == null) return;
        for (List<PurchaseTransaction> siblings: groups.values()) {
            splitSubTotalTaxAmounts(siblings);
            prorateGroupForMaxTaxAmt(siblings);
        }
    }
    
    private void calcMaxTax(MicroApiTransactionDocument document) {
        Map<TaxCodeRuleGroupBy, List<MicroApiTransaction>> groups = document.getTransientMicroApiTransactionDocument().getGroupedDocument();
        if (groups == null) return;
        for (List<MicroApiTransaction> siblings: groups.values()) {
            splitSubTotalTaxAmountsM(siblings);
            prorateGroupForMaxTaxAmtM(siblings);
        }
    }

    private void calcTaxAmounts(MicroApiTransaction trans, MicroApiTransactionDocument document ,JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        try {
            HashMap<String, String> entityDefaults = transientDetails.getEntityDefaults();

            BigDecimal vTier1Rate = null;
            BigDecimal vTier2Rate = null;
            BigDecimal vTier3Rate = null;
            BigDecimal vTier1Setamt = null;
            BigDecimal vTier2Setamt = null;
            BigDecimal vTier3Setamt = null;

            BigDecimal vTier1TaxAmt = null;
            BigDecimal vTier1MaxAmt = null;
            BigDecimal vTier2MinAmt = null;
            BigDecimal vTier2MaxAmt = null;
            BigDecimal vTier3MinAmt = null;
            Boolean vTier2EntireAmtFlag = null;
            Boolean  vTier3EntireAmtFlag = null;
            BigDecimal vMaxtaxAmt = null;

            BigDecimal vBaseChangePercent = null;
            //vjurLevel_taxable_threshold_amt
            BigDecimal vTaxableThresholdAmt = null;
            BigDecimal vMinimumTaxableAmt = null;
            BigDecimal vMaximumTaxableAmt = null;
            BigDecimal vMaximumTaxAmt = null;

            //TRANS.jurLevel_tax_process_type_code ?
            String vTaxProcessTypeCode = null;

            switch (jurLevel) {
                case COUNTRY:

                    vTier1Rate = trans.getCountryTier1Rate();
                    vTier2Rate = trans.getCountryTier2Rate();
                    vTier3Rate = trans.getCountryTier3Rate();
                    vTier1Setamt = trans.getCountryTier1Setamt();
                    vTier2Setamt = trans.getCountryTier2Setamt();
                    vTier3Setamt = trans.getCountryTier3Setamt();
                    vTier1MaxAmt = trans.getCountryTier1MaxAmt();
                    vTier2MinAmt = trans.getCountryTier2MinAmt();
                    vTier2MaxAmt = trans.getCountryTier2MaxAmt();
                    vTier1TaxAmt = trans.getCountryTier1TaxAmt();
                    //vTier3MinAmt = trans.getCountry3;
                    vTier2EntireAmtFlag = trans.getCountryTier2EntamBooleanFlag();
                    vTier3EntireAmtFlag = trans.getCountryTier3EntamBooleanFlag();
                    vMaxtaxAmt = trans.getCountryMaxtaxAmt();
                    vMaximumTaxAmt = trans.getCountryMaximumTaxAmt();

                    vBaseChangePercent = trans.getCountryBaseChangePct();

                    vTaxableThresholdAmt = trans.getCountryTaxableThresholdAmt();
                    vMinimumTaxableAmt = trans.getCountryMinimumTaxableAmt();
                    vMaximumTaxableAmt = trans.getCountryMaximumTaxableAmt();

                    vTaxProcessTypeCode = trans.getCountryTaxProcessTypeCode();

                    break;
                case STATE:
                    vTier1Rate = trans.getStateTier1Rate();
                    vTier2Rate = trans.getStateTier2Rate();
                    vTier3Rate = trans.getStateTier3Rate();
                    vTier1Setamt = trans.getStateTier1Setamt();
                    vTier2Setamt = trans.getStateTier2Setamt();
                    vTier3Setamt = trans.getStateTier3Setamt();
                    vTier1MaxAmt = trans.getStateTier1MaxAmt();
                    vTier2MinAmt = trans.getStateTier2MinAmt();
                    vTier2MaxAmt = trans.getStateTier2MaxAmt();;
                    vTier1TaxAmt = trans.getStateTier1TaxAmt();
                    //vTier3MinAmt = trans.getCountry3;
                    vTier2EntireAmtFlag = trans.getStateTier2EntamBooleanFlag();
                    vTier3EntireAmtFlag = trans.getStateTier3EntamBooleanFlag();
                    vMaxtaxAmt = trans.getStateMaxtaxAmt();
                    vMaximumTaxAmt = trans.getStateMaximumTaxAmt();

                    vBaseChangePercent = trans.getStateBaseChangePct();

                    vTaxableThresholdAmt = trans.getStateTaxableThresholdAmt();
                    vMinimumTaxableAmt = trans.getStateMinimumTaxableAmt();
                    vMaximumTaxableAmt = trans.getStateMaximumTaxableAmt();

                    vTaxProcessTypeCode = trans.getStateTaxProcessTypeCode();
                    break;
                case COUNTY:

                    vTier1Rate = trans.getCountyTier1Rate();
                    vTier2Rate = trans.getCountyTier2Rate();
                    vTier3Rate = trans.getCountyTier3Rate();
                    vTier1Setamt = trans.getCountyTier1Setamt();
                    vTier2Setamt = trans.getCountyTier2Setamt();
                    vTier3Setamt = trans.getCountyTier3Setamt();
                    vTier1MaxAmt = trans.getCountyTier1MaxAmt();
                    vTier2MinAmt = trans.getCountyTier2MinAmt();
                    vTier2MaxAmt = trans.getCountyTier2MaxAmt();
                    vTier1TaxAmt = trans.getCountyTier1TaxAmt();
                    //vTier3MinAmt = trans.getCountry3;
                    vTier2EntireAmtFlag = trans.getCountyTier2EntamBooleanFlag();
                    vTier3EntireAmtFlag = trans.getCountyTier3EntamBooleanFlag();
                    vMaxtaxAmt = trans.getCountyMaxtaxAmt();
                    vMaximumTaxAmt = trans.getCountyMaximumTaxAmt();

                    vBaseChangePercent = trans.getCountyBaseChangePct();
                    vTaxableThresholdAmt = trans.getCountyTaxableThresholdAmt();
                    vMinimumTaxableAmt = trans.getCountyMinimumTaxableAmt();
                    vMaximumTaxableAmt = trans.getCountyMaximumTaxableAmt();
                    vTaxProcessTypeCode = trans.getCountyTaxProcessTypeCode();
                    break;
                case CITY:

                    vTier1Rate = trans.getCityTier1Rate();
                    vTier2Rate = trans.getCityTier2Rate();
                    vTier3Rate = trans.getCityTier3Rate();
                    vTier1Setamt = trans.getCityTier1Setamt();
                    vTier2Setamt = trans.getCityTier2Setamt();
                    vTier3Setamt = trans.getCityTier3Setamt();
                    vTier1MaxAmt = trans.getCityTier1MaxAmt();
                    vTier2MinAmt = trans.getCityTier2MinAmt();
                    vTier2MaxAmt = trans.getCityTier2MaxAmt();
                    vTier1TaxAmt = trans.getCityTier1TaxAmt();
                    //vTier3MinAmt = trans.getCountry3;
                    vTier2EntireAmtFlag = trans.getCityTier2EntamBooleanFlag();
                    vTier3EntireAmtFlag = trans.getCityTier3EntamBooleanFlag();
                    vMaxtaxAmt = trans.getCityMaxtaxAmt();
                    vMaximumTaxAmt = trans.getCityMaximumTaxAmt();

                    vBaseChangePercent = trans.getCityBaseChangePct();
                    vTaxableThresholdAmt = trans.getCityTaxableThresholdAmt();
                    vMinimumTaxableAmt = trans.getCityMinimumTaxableAmt();
                    vMaximumTaxableAmt = trans.getCityMaximumTaxableAmt();

                    vTaxProcessTypeCode = trans.getCityTaxProcessTypeCode();
                    break;
                case STJ1:


                    //vTier1Rate = trans.getStj;
        /*            vTier2Rate = trans.getCityTier2Rate();
                    vTier3Rate = trans.getCityTier3Rate();
                    vTier1Setamt = trans.getCityTier1Setamt();
                    vTier2Setamt = trans.getCityTier2Setamt();
                    vTier3Setamt = trans.getCityTier3Setamt();
                    vTier1MaxAmt = trans.getCityTier1MaxAmt();
                    vTier2MinAmt = trans.getCityTier2MinAmt();
                    vTier2MaxAmt = trans.getCityTier2MaxAmt();;
                    //vTier3MinAmt = trans.getCountry3;
                    vTier2EntireAmtFlag = trans.getCityTier2EntamBooleanFlag();
                    vTier3EntireAmtFlag = trans.getCityTier3EntamBooleanFlag();
                    vMaxtaxAmt = trans.getCityMaxtaxAmt();*/


                    //vBaseChangePercent = trans.getStj1BaseChangePct(); no such thing on STJs

                    break;
                case STJ2:
                    break;
                case STJ3:
                    break;
                case STJ4:
                    break;
                case STJ5:
                    break;
                case STJ6:
                    break;
                case STJ7:
                    break;
                case STJ8:
                    break;
                case STJ9:
                    break;
                case STJ10:
                    break;
            }

            if (vTaxableThresholdAmt == null)
                vTaxableThresholdAmt = BigDecimal.ZERO;

            if (vMinimumTaxableAmt == null)
                vMinimumTaxableAmt = BigDecimal.ZERO;

            if (vMaximumTaxableAmt == null)
                vMaximumTaxableAmt = BigDecimal.ZERO;

            if (vMaximumTaxAmt == null)
                vMaximumTaxAmt = BigDecimal.ZERO;

            if (vTier1Rate == null)
            	vTier1Rate = BigDecimal.ZERO;
            
            if (vTier2Rate == null)
            	vTier2Rate = BigDecimal.ZERO;
            
            if (vTier3Rate == null)
            	vTier3Rate = BigDecimal.ZERO;
            
            if (vTier1Setamt == null)
            	vTier1Setamt = BigDecimal.ZERO;
            
            if (vTier2Setamt == null)
            	vTier2Setamt = BigDecimal.ZERO;
            
            if (vTier3Setamt == null)
            	vTier3Setamt = BigDecimal.ZERO;
            
            if (vTier1MaxAmt == null)
            	vTier1MaxAmt = BigDecimal.ZERO;
            
            if (vTier2MinAmt == null)
            	vTier2MinAmt = BigDecimal.ZERO;
            
            if (vTier2MaxAmt == null)
            	vTier2MaxAmt = BigDecimal.ZERO;
            
            if (vTier3MinAmt == null)
            	vTier3MinAmt = BigDecimal.ZERO;
            
            if (vMaxtaxAmt == null)
            	vMaxtaxAmt = BigDecimal.ZERO;
            
            if (vBaseChangePercent == null)
            	vBaseChangePercent = BigDecimal.ZERO;

            if (vTier1TaxAmt == null)
                vTier1TaxAmt = BigDecimal.ZERO;
            
            if (getTier1TaxAmt(trans, jurLevel) == null)
                setTier1TaxAmt(trans, jurLevel, BigDecimal.ZERO);

            /*TRANS.taxable_amt = TRANS.gl_line_itm_dist_amt*/
            /*TRANS.taxable_amt = TRANS.taxable_amt + Distr_01_amt thru Distr_10_amt*/
           // trans.getGlLineItmDistAmt()

    		setJurLevelTier1TaxableAmt(trans, jurLevel, nullToZero(trans.getGlLineItmDistAmt())
    				.add(nullToZero(trans.getDistr01Amt()))
    				.add(nullToZero(trans.getDistr02Amt()))
    				.add(nullToZero(trans.getDistr03Amt()))
    				.add(nullToZero(trans.getDistr04Amt()))
    				.add(nullToZero(trans.getDistr05Amt()))
    				.add(nullToZero(trans.getDistr06Amt()))
    				.add(nullToZero(trans.getDistr07Amt()))
    				.add(nullToZero(trans.getDistr08Amt()))
    				.add(nullToZero(trans.getDistr09Amt()))
    				.add(nullToZero(trans.getDistr10Amt()))
    		);

            //TRANS.taxable_amt = TRANS.taxable_amt - TRANS.invoice_line_freight_amt
            if (entityDefaults != null && "1".equals(entityDefaults.get(ENTITY_DEFAULT_FRTINCITEM))) {
                if (trans.getInvoiceLineFreightAmt() == null)
                    trans.setInvoiceLineFreightAmt(BigDecimal.ZERO);
                setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                        .subtract(trans.getInvoiceLineFreightAmt()));
            }


            //TRANS.taxable_amt = TRANS.taxable_amt - ABS(TRANS.invoice_line_disc_amt
            if (entityDefaults != null && "1".equals(entityDefaults.get(ENTITY_DEFAULT_DISINCITEM))) {
                if (trans.getInvoiceLineDiscAmt() == null)
                    trans.setInvoiceLineDiscAmt(BigDecimal.ZERO);
                setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                        .subtract(trans.getInvoiceLineDiscAmt().abs()));
            }

            //TRANS.LineGrossAmt = LineItemAmt + LineFreightAmt + ABS(LineDiscount Amt)
            // GREEN BOX



            if (vBaseChangePercent != null && vBaseChangePercent.compareTo(BigDecimal.ZERO) == 1) {
                //TRANS.exempt_amt = taxable_amt – (taxable_amt * jurLevel_base_change_pct)
                ///GREEN BOX

                //TRANS.taxable_amt = taxable_amt * jurLevel_base_change_pct
                setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                        .multiply(vBaseChangePercent));
            }

            if (jurLevel != JurisdictionLevel.COUNTRY &&
                    jurLevel != JurisdictionLevel.STATE &&
                    jurLevel != JurisdictionLevel.COUNTY &&
                    jurLevel != JurisdictionLevel.CITY) {
                calcTaxAmountsCalcStj(trans, jurLevel);
                return;
            }

            ///TRANS.jurLevel_tier2_rate > 0? OR setamt > 0
            //PP-401 if (!(vTier2Rate.compareTo(BigDecimal.ZERO) == 1) && !(vTier2Setamt.compareTo(BigDecimal.ZERO) == 1)) {

                //TRANS.jurLevel_taxable_threshold_amt > 0?
                if (vTaxableThresholdAmt.compareTo(BigDecimal.ZERO) == 1) {

                    ///A
                    //TRANS.jurLevel_tax_process_type_code = "0"?
                    if ("0".equals(vTaxProcessTypeCode)) {
                        //TRANS.taxable_amt < jurLevel_taxable_threshhold_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel).compareTo(vTaxableThresholdAmt) == -1) {
                            //TRANS.taxable_amt = 0
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }


                        //TRANS.jurLevel_tax_process_type_code = "1"?
                    } else if ("1".equals(vTaxProcessTypeCode)) {
                        //TRANS.invoice_line_item_count > 0?
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = taxable_amt / line_Item_count
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getInvoiceLineCount(), 7, RoundingMode.HALF_EVEN));
                        }
                        //TRANS.taxable_amt < jurLevel_taxable_threshhold_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel).compareTo(vTaxableThresholdAmt) == -1) {
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }

                        calcTaxAmountsCalc(trans, jurLevel);

                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            setTier1TaxAmt(trans, jurLevel, getTier1TaxAmt(trans, jurLevel)
                                    .multiply(trans.getInvoiceLineCount()));
//                            vTier1TaxAmt = vTier1TaxAmt.multiply(BigDecimal.valueOf(trans.getInvoiceLineCount()));
//                            setTier1TaxAmt(trans, jurLevel, vTier1TaxAmt);
                        }
                        return;


                    } else if ("2".equals(vTaxProcessTypeCode)) {
                        //Pass 3

                        TaxCodeRuleGroupBy groupByKey = transientDetails.getTaxCodeRuleGroupByKey();
                        if (groupByKey!=null && (trans.getTransientTransaction().getSubtotalGrossDistributed() == null
                                || trans.getTransientTransaction().getSubtotalGrossDistributed() == Boolean.FALSE)) {
                            splitSubTotalGrossAmounts(document, groupByKey);
                        }

                        ///Get Subtotals by group
                        ///Pass 4
                        //TRANS.total_gross_amt < jurLevel_taxable_threshhold_amt?

                        if (trans.getTransientTransaction().getSubTotalGrossAmt() != null) {
                            if (trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(vTaxableThresholdAmt) == -1) {
                                setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                            }
                        }

                    } else {
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vTaxableThresholdAmt) == -1) {
                            //TRANS.taxable_amt = 0
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }
                    }
                    calcTaxAmountsCalc(trans, jurLevel);
                    return;
                    //TRANS.jurLevel.minimum_taxable_amt > 0?
                } else if (vMinimumTaxableAmt.compareTo(BigDecimal.ZERO) == 1) {
                    if ("0".equals(vTaxProcessTypeCode)) {
                        //TRANS.taxable_amt > jurLevel_minimum_taxable_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel).compareTo(vMinimumTaxableAmt) == 1) {
                            //TRANS.taxable_amt = taxable_amt – jurLevel_minimum_taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .subtract(vMinimumTaxableAmt));
                        } else {
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }
                    } else if ("1".equals(vTaxProcessTypeCode)) {
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = taxable_amt / line_Item_count
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getInvoiceLineCount(), 7, RoundingMode.HALF_EVEN));
                        }
                        //TRANS.taxable_amt > jurLevel_minimum_taxable_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vMinimumTaxableAmt) == 1) {
                            //TRANS.taxable_amt = taxable_amt – jurLevel_minimum_taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .subtract(vMinimumTaxableAmt));
                        } else {
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }

                        calcTaxAmountsCalc(trans, jurLevel);

                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            setTier1TaxAmt(trans, jurLevel, getTier1TaxAmt(trans, jurLevel)
                                .multiply(trans.getInvoiceLineCount()));
                            //setTier1TaxAmt(trans, jurLevel, vTier1TaxAmt);
                        }
                        return;
                    } else if ("2".equals(vTaxProcessTypeCode)) {
                        ////Pass 3
                        ///Get Subtotals by group
                        TaxCodeRuleGroupBy groupByKey = transientDetails.getTaxCodeRuleGroupByKey();
                        if (groupByKey!=null && (trans.getTransientTransaction().getSubtotalGrossDistributed() == null
                                || trans.getTransientTransaction().getSubtotalGrossDistributed() == Boolean.FALSE)) {
                            splitSubTotalGrossAmounts(document, groupByKey);
                        }

                        ///TRANS.invoice_total_amt > jurLevel_minimum_taxable_amt?
                        if (trans.getTransientTransaction().getSubTotalGrossAmt() != null
                                && trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(vMinimumTaxableAmt) == 1
                                && trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = (taxable_amt/invoice_total_amt) * (invoice_total_amt – jurLevel_minimum_taxable_amt)
                            //setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                            BigDecimal taxableMultiplicand = getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getTransientTransaction().getSubTotalGrossAmt(), 7, RoundingMode.HALF_EVEN);
                            BigDecimal taxableMultiplier = trans.getTransientTransaction().getSubTotalGrossAmt().subtract(vMinimumTaxableAmt);
                            setJurLevelTier1TaxableAmt(trans, jurLevel, taxableMultiplicand.multiply(taxableMultiplier));
                        } else
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);

                    } else {
                        ///B
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vMinimumTaxableAmt) == 1) {
                            //TRANS.taxable_amt = taxable_amt – jurLevel_minimum_taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .subtract(vMinimumTaxableAmt));
                        } else {
                            setJurLevelTier1TaxableAmt(trans, jurLevel, BigDecimal.ZERO);
                        }
                    }
                    calcTaxAmountsCalc(trans, jurLevel);
                    return;

                    //TRANS.jurLevel.maximum_taxable_amt > 0?
                } else if (vMaximumTaxableAmt.compareTo(BigDecimal.ZERO) == 1) {
                    if ("0".equals(vTaxProcessTypeCode)) {
                        //TRANS.taxable_amt. > jurLevel_maximum_taxable_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vMaximumTaxableAmt) == 1) {
                            //TRANS.taxable_amt = jurLevel_maximum_taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, vMaximumTaxableAmt);
                        }

                    } else if ("1".equals(vTaxProcessTypeCode)) {
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = taxable_amt / line_Item_count
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getInvoiceLineCount(), 7, RoundingMode.HALF_EVEN));
                        }
                        //TRANS.taxable_amt. > jurLevel_maximum_taxable_amt?
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vMaximumTaxableAmt) == 1) {
                            setJurLevelTier1TaxableAmt(trans, jurLevel, vMaximumTaxableAmt);
                        }

                        calcTaxAmountsCalc(trans, jurLevel);

                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            setTier1TaxAmt(trans, jurLevel, getTier1TaxAmt(trans, jurLevel)
                                    .multiply(trans.getInvoiceLineCount()));
                        }
                        return;
                    } else if ("2".equals(vTaxProcessTypeCode)) {
                        TaxCodeRuleGroupBy groupByKey = transientDetails.getTaxCodeRuleGroupByKey();
                        if (groupByKey!=null && (trans.getTransientTransaction().getSubtotalGrossDistributed() == null
                                || trans.getTransientTransaction().getSubtotalGrossDistributed() == Boolean.FALSE)) {
                            splitSubTotalGrossAmounts(document, groupByKey);
                        }

                        //TRANS.invoice_total_amt. > jurLevel_maximum_taxable_amt?
                        if(trans.getTransientTransaction().getSubTotalGrossAmt() != null
                                && trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(vMaximumTaxableAmt) == 1
                                && trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = (jurLevel_maximum_taxable_amt / invoice_total_amt) * taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, vMaximumTaxableAmt
                                    .divide(trans.getTransientTransaction().getSubTotalGrossAmt(), 7, RoundingMode.HALF_EVEN)
                                    .multiply(getJurlLevelTaxableAmt(trans, jurLevel)));
                        }
                    } else {
                        if (getJurlLevelTaxableAmt(trans, jurLevel)
                                .compareTo(vMaximumTaxableAmt) == 1) {
                            //TRANS.taxable_amt = jurLevel_maximum_taxable_amt
                            setJurLevelTier1TaxableAmt(trans, jurLevel, vMaximumTaxableAmt);
                        }
                    }
                    calcTaxAmountsCalc(trans, jurLevel);
                    return;
                    ///TRANS.jurLevel.maximum_tax_amt > 0?
                } else if (vMaximumTaxAmt.compareTo(BigDecimal.ZERO) == 1) {
                    if ("0".equals(vTaxProcessTypeCode)) {
                        calcTaxAmountsCalc(trans, jurLevel);
                        if (getTier1TaxAmt(trans, jurLevel).compareTo(vMaximumTaxAmt) == 1) {
                            //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                            setTier1TaxAmt(trans, jurLevel, vMaximumTaxAmt);
                        }
                        return;
                    } else if ("1".equals(vTaxProcessTypeCode)) {
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = taxable_amt / line_Item_count
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getInvoiceLineCount(), 7, RoundingMode.HALF_EVEN));
                        }
                        calcTaxAmountsCalc(trans, jurLevel);
                        //TRANS.jurLevel_tier1_tax_amt. > jurLevel_maximum_tax_amt?
                        if (getTier1TaxAmt(trans, jurLevel).compareTo(vMaximumTaxAmt) == 1) {
                            //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                            setTier1TaxAmt(trans, jurLevel, vMaximumTaxAmt);
                        }
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.jurlevel_tier1_tax_amt = jurlevel_tier1_tax_amt * line_item_count
                            setTier1TaxAmt(trans,jurLevel,getTier1TaxAmt(trans, jurLevel)
                                    .multiply(trans.getInvoiceLineCount()));
                        }
                        return;
                    } else if ("2".equals(vTaxProcessTypeCode)) {
                        calcTaxAmountsCalc(trans, jurLevel);

                        //TRANS.jurLevel_tier1_tax_amt. > jurLevel_maximum_tax_amt?
/*                        if (getTier1TaxAmt(trans, jurLevel).compareTo(vMaximumTaxAmt) == 1
                                && trans.getTransientTransaction().getSubTotalGrossAmt() != null
                                && trans.getTransientTransaction().getSubTotalGrossAmt().compareTo(BigDecimal.ZERO) != 0) {
                            //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                            //setTier1TaxAmt(trans, jurLevel, vMaximumTaxAmt);
                            setTier1TaxAmt(trans, jurLevel, vMaximumTaxAmt
                                    .divide(trans.getTransientTransaction().getSubTotalGrossAmt(), 7, RoundingMode.HALF_EVEN)
                                    .multiply(getJurlLevelTaxableAmt(trans, jurLevel)));
                        }*/
                        return;
                    } else {
                        //TRANS.jurLevel_tier1_tax_amt. > jurLevel_maximum_tax_amt?
                        if (getTier1TaxAmt(trans, jurLevel).compareTo(vMaximumTaxAmt) == 1) {
                            //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                            setTier1TaxAmt(trans, jurLevel, vMaximumTaxAmt);
                        }
                        return;
                    }
                } else {
                    //PP-401	
                	if ("0".equals(vTaxProcessTypeCode)) {
                		calcTaxTiers(trans, jurLevel);        		
                    } 
                	else if ("1".equals(vTaxProcessTypeCode)) {
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                            //TRANS.taxable_amt = taxable_amt / line_Item_count
                            setJurLevelTier1TaxableAmt(trans, jurLevel, getJurlLevelTaxableAmt(trans, jurLevel)
                                    .divide(trans.getInvoiceLineCount(), 7, RoundingMode.HALF_EVEN));
                        }
                        
                        calcTaxTiers(trans, jurLevel);
            
                        if (trans.getInvoiceLineCount() != null && trans.getInvoiceLineCount().compareTo(BigDecimal.ZERO) == 1) {
                        	if(getTier1TaxAmt(trans, jurLevel)!=null) {
	                            //TRANS.jurlevel_tier1_tax_amt = jurlevel_tier1_tax_amt * line_item_count
	                            setTier1TaxAmt(trans,jurLevel,getTier1TaxAmt(trans, jurLevel)
	                                    .multiply(trans.getInvoiceLineCount()));
                        	}
                        	if(getTier2TaxAmt(trans, jurLevel)!=null) {
	                            //TRANS.jurlevel_tier2_tax_amt = jurlevel_tier2_tax_amt * line_item_count
	                            setTier2TaxAmt(trans,jurLevel,getTier2TaxAmt(trans, jurLevel)
	                                    .multiply(trans.getInvoiceLineCount()));
                        	}
                        	if(getTier3TaxAmt(trans, jurLevel)!=null) {
	                            //TRANS.jurlevel_tier3_tax_amt = jurlevel_tier3_tax_amt * line_item_count
	                            setTier3TaxAmt(trans,jurLevel,getTier3TaxAmt(trans, jurLevel)
	                                    .multiply(trans.getInvoiceLineCount()));
                        	}
                        }
                    } 
                	else if ("2".equals(vTaxProcessTypeCode)) {
                    	calcTaxTiers(trans, jurLevel);
                    } 
                	else {
                		calcTaxTiers(trans, jurLevel);
                    }
                	
                	if(((vTier2Rate==null || vTier2Rate.compareTo(BigDecimal.ZERO)==0)) && ((vTier2Setamt==null || vTier2Setamt.compareTo(BigDecimal.ZERO)==0))) {
                		if(vMaxtaxAmt!=null && vMaxtaxAmt.compareTo(BigDecimal.ZERO)==1){

                			switch (jurLevel) {
	                            case COUNTRY:
	                            	vTier1TaxAmt = trans.getCountryTier1TaxAmt();
	                                break;
	                            case STATE:
	                            	vTier1TaxAmt = trans.getStateTier1TaxAmt();
	                                break;
	                            case COUNTY:
	                            	vTier1TaxAmt = trans.getCountyTier1TaxAmt();
	                                break;
	                            case CITY:
	                            	vTier1TaxAmt = trans.getCityTier1TaxAmt();
	                                break;
	                            default:
	                            	vTier1TaxAmt = BigDecimal.ZERO;
	                                break;
	                        }
                			
                			if(vTier1TaxAmt.compareTo(vMaxtaxAmt)==1){
                				setTier1TaxAmt(trans,jurLevel,vMaxtaxAmt);
                    		}		
                		}	
                	}
                }
            //} else {
            //  calcTaxTiers(trans, jurLevel);
            //}


        } /*catch (PurchaseTransactionSuspendedException suspEx) {
        suspend(trans, SuspendReason.SUSPEND_RATE);
        suspEx.printStackTrace();
        logger.error(suspEx);
        if (trans.getErrorCode() == null) {
            trans.setErrorCode(suspEx.getMessage());
            writeError(trans);
        }

    } catch (PurchaseTransactionProcessingException ptpex) {
        ptpex.printStackTrace();
        logger.error(ptpex);
        if (trans.getErrorCode() == null)
            trans.setErrorCode(ptpex.getMessage());
        writeError(trans);


        }*/ catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            //            if (trans.getErrorCode() == null)
	        trans.setErrorCode(e.getMessage());
	        writeError(trans);
        }
    }
    
    private void calcTaxAmountsCalc(PurchaseTransaction trans, JurisdictionLevel jurLevel) {

        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (trans.getTaxableAmt() == null)
            trans.setTaxableAmt(BigDecimal.ZERO);

        switch (jurLevel) {
            case COUNTRY:
                if (trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCountryTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.COUNTRY)
                            .multiply(trans.getCountryTier1Rate()));
                } else if (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCountryTier1TaxAmt(trans.getCountryTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCountryMaxtaxAmt() != null && trans.getCountryMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCountryTier1TaxAmt() != null && trans.getCountryTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCountryTier1TaxAmt(trans.getCountryMaxtaxAmt());
                    }
                    //TRANS.jurLevel_maximum_tax_amt > 0?
                } else if (trans.getCountryMaximumTaxAmt() != null && trans.getCountryMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCountryTier1TaxAmt() != null && trans.getCountryTier1TaxAmt().compareTo(trans.getCountryMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCountryTier1TaxAmt(trans.getCountryMaximumTaxAmt());
                    }
                }
                break;
            case STATE:
                if (trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStateTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.STATE)
                            .multiply(trans.getStateTier1Rate()));
                } else if (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStateTier1TaxAmt(trans.getStateTier1Setamt());
                }
                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getStateMaxtaxAmt() != null && trans.getStateMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getStateTier1TaxAmt() != null && trans.getStateTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setStateTier1TaxAmt(trans.getStateMaxtaxAmt());
                    }
                } else if (trans.getStateMaximumTaxAmt() != null && trans.getStateMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getStateTier1TaxAmt() != null && trans.getStateTier1TaxAmt().compareTo(trans.getStateMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setStateTier1TaxAmt(trans.getStateMaximumTaxAmt());
                    }
                }
                break;

            case COUNTY:
                if (trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCountyTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.COUNTY)
                            .multiply(trans.getCountyTier1Rate()));
                } else if (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCountyTier1TaxAmt(trans.getCountyTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCountyMaxtaxAmt() != null && trans.getCountyMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCountyTier1TaxAmt() != null && trans.getCountyTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCountyTier1TaxAmt(trans.getCountyMaxtaxAmt());
                    }
                } else if (trans.getCountyMaximumTaxAmt() != null && trans.getCountyMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCountyTier1TaxAmt() != null && trans.getCountyTier1TaxAmt().compareTo(trans.getCountyMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCountyTier1TaxAmt(trans.getCountyMaximumTaxAmt());
                    }
                }

                break;
            case CITY:
                if (trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCityTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.CITY)
                            .multiply(trans.getCityTier1Rate()));
                } else if (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCityTier1TaxAmt(trans.getCityTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCityMaxtaxAmt() != null && trans.getCityMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCityTier1TaxAmt() != null && trans.getCityTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCityTier1TaxAmt(trans.getCityMaxtaxAmt());
                    }
                } else if (trans.getCityMaximumTaxAmt() != null && trans.getCityMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCityTier1TaxAmt() != null && trans.getCityTier1TaxAmt().compareTo(trans.getCityMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCityTier1TaxAmt(trans.getCityMaximumTaxAmt());
                    }
                }
                break;
        }
    }
    
    private void calcTaxAmountsCalc(MicroApiTransaction trans, JurisdictionLevel jurLevel) {

        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (trans.getTaxableAmt() == null)
            trans.setTaxableAmt(BigDecimal.ZERO);

        switch (jurLevel) {
            case COUNTRY:
                if (trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCountryTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.COUNTRY)
                            .multiply(trans.getCountryTier1Rate()));
                } else if (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCountryTier1TaxAmt(trans.getCountryTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCountryMaxtaxAmt() != null && trans.getCountryMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCountryTier1TaxAmt() != null && trans.getCountryTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCountryTier1TaxAmt(trans.getCountryMaxtaxAmt());
                    }
                    //TRANS.jurLevel_maximum_tax_amt > 0?
                } else if (trans.getCountryMaximumTaxAmt() != null && trans.getCountryMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCountryTier1TaxAmt() != null && trans.getCountryTier1TaxAmt().compareTo(trans.getCountryMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCountryTier1TaxAmt(trans.getCountryMaximumTaxAmt());
                    }
                }
                break;
            case STATE:
                if (trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStateTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.STATE)
                            .multiply(trans.getStateTier1Rate()));
                } else if (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStateTier1TaxAmt(trans.getStateTier1Setamt());
                }
                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getStateMaxtaxAmt() != null && trans.getStateMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getStateTier1TaxAmt() != null && trans.getStateTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setStateTier1TaxAmt(trans.getStateMaxtaxAmt());
                    }
                } else if (trans.getStateMaximumTaxAmt() != null && trans.getStateMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getStateTier1TaxAmt() != null && trans.getStateTier1TaxAmt().compareTo(trans.getStateMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setStateTier1TaxAmt(trans.getStateMaximumTaxAmt());
                    }
                }
                break;

            case COUNTY:
                if (trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCountyTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.COUNTY)
                            .multiply(trans.getCountyTier1Rate()));
                } else if (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCountyTier1TaxAmt(trans.getCountyTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCountyMaxtaxAmt() != null && trans.getCountyMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCountyTier1TaxAmt() != null && trans.getCountyTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCountyTier1TaxAmt(trans.getCountyMaxtaxAmt());
                    }
                } else if (trans.getCountyMaximumTaxAmt() != null && trans.getCountyMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCountyTier1TaxAmt() != null && trans.getCountyTier1TaxAmt().compareTo(trans.getCountyMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCountyTier1TaxAmt(trans.getCountyMaximumTaxAmt());
                    }
                }

                break;
            case CITY:
                if (trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setCityTier1TaxAmt(getJurlLevelTaxableAmt(trans, JurisdictionLevel.CITY)
                            .multiply(trans.getCityTier1Rate()));
                } else if (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setCityTier1TaxAmt(trans.getCityTier1Setamt());
                }

                //TRANS.jurLevel_maxtax_amt > 0?
                if (trans.getCityMaxtaxAmt() != null && trans.getCityMaxtaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maxtax_amt?
                    if (trans.getCityTier1TaxAmt() != null && trans.getCityTier1TaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maxtax_amt
                        trans.setCityTier1TaxAmt(trans.getCityMaxtaxAmt());
                    }
                } else if (trans.getCityMaximumTaxAmt() != null && trans.getCityMaximumTaxAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt > jurLevel_maximum_tax_amt?
                    if (trans.getCityTier1TaxAmt() != null && trans.getCityTier1TaxAmt().compareTo(trans.getCityMaximumTaxAmt()) == 1) {
                        //TRANS.jurLevel_tier1_tax_amt = jurLevel_maximum_tax_amt
                        trans.setCityTier1TaxAmt(trans.getCityMaximumTaxAmt());
                    }
                }
                break;
        }
    }

    private void calcTaxAmountsCalcStj(PurchaseTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
        switch (jurLevel) {
            case STJ1:

                if (trans.getStj1Rate() != null && trans.getStj1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj1TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj1Rate()));
                } else if (trans.getStj1Setamt() != null && trans.getStj1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj1TaxAmt(trans.getStj1Setamt());
                }
                ///Jur.jurlevel_local_code = "COUNTY"?
                if ("COUNTY".equals(transientDetails.getStj1LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj1TaxAmt() == null)
                        trans.setStj1TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj1TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj1LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj1TaxAmt() == null)
                        trans.setStj1TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add(nullToZero(trans.getStj1TaxAmt())));
                }



                break;
            case STJ2:
                if (trans.getStj2Rate() != null && trans.getStj2Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj2TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj2Rate()));
                } else if (trans.getStj2Setamt() != null && trans.getStj2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj2TaxAmt(trans.getStj2Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj2LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj2TaxAmt() == null)
                        trans.setStj2TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj2TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj2LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj2TaxAmt() == null)
                        trans.setStj2TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add(nullToZero(trans.getStj2TaxAmt())));
                }

                break;
            case STJ3:
                if (trans.getStj3Rate() != null && trans.getStj3Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj3TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj3Rate()));
                } else if (trans.getStj3Setamt() != null && trans.getStj3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj3TaxAmt(trans.getStj3Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj3LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj3TaxAmt() == null)
                        trans.setStj3TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj3TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj3LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj3TaxAmt() == null)
                        trans.setStj3TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add((trans.getStj3TaxAmt())));
                }


                break;
            case STJ4:
                if (trans.getStj4Rate() != null && trans.getStj4Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj4TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj4Rate()));
                } else if (trans.getStj4Setamt() != null && trans.getStj4Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj4TaxAmt(trans.getStj4Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj4LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj4TaxAmt() == null)
                        trans.setStj4TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt().add(trans.getStj4TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj4LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj4TaxAmt() == null)
                        trans.setStj4TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt().add(trans.getStj4TaxAmt())));
                }

                break;
            case STJ5:

                if (trans.getStj5Rate() != null && trans.getStj5Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj5TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj5Rate()));
                } else if (trans.getStj5Setamt() != null && trans.getStj5Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj5TaxAmt(trans.getStj5Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj5LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj5TaxAmt() == null)
                        trans.setStj5TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt().add(trans.getStj5TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj5LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj5TaxAmt() == null)
                        trans.setStj5TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt().add(trans.getStj5TaxAmt())));
                }


                break;
            case STJ6:
                if (trans.getStj6Rate() != null && trans.getStj6Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj6TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj6Rate()));
                } else if (trans.getStj6Setamt() != null && trans.getStj6Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj6TaxAmt(trans.getStj6Setamt());
                }
                break;
            case STJ7:
                if (trans.getStj7Rate() != null && trans.getStj7Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj7TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj7Rate()));
                } else if (trans.getStj7Setamt() != null && trans.getStj7Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj7TaxAmt(trans.getStj7Setamt());
                }
                break;
            case STJ8:
                if (trans.getStj8Rate() != null && trans.getStj8Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj8TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj8Rate()));
                } else if (trans.getStj8Setamt() != null && trans.getStj8Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj8TaxAmt(trans.getStj8Setamt());
                }
                break;
            case STJ9:
                if (trans.getStj9Rate() != null && trans.getStj9Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj9TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj9Rate()));
                } else if (trans.getStj9Setamt() != null && trans.getStj9Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj9TaxAmt(trans.getStj9Setamt());
                }
                break;
            case STJ10:
                if (trans.getStj10Rate() != null && trans.getStj10Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj10TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj10Rate()));
                } else if (trans.getStj10Setamt() != null && trans.getStj10Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj10TaxAmt(trans.getStj10Setamt());
                }
                break;


        }
    }

    private void calcTaxAmountsCalcStj(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
        switch (jurLevel) {
            case STJ1:

                if (trans.getStj1Rate() != null && trans.getStj1Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj1TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj1Rate()));
                } else if (trans.getStj1Setamt() != null && trans.getStj1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj1TaxAmt(trans.getStj1Setamt());
                }
                ///Jur.jurlevel_local_code = "COUNTY"?
                if ("COUNTY".equals(transientDetails.getStj1LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj1TaxAmt() == null)
                        trans.setStj1TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj1TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj1LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj1TaxAmt() == null)
                        trans.setStj1TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add(nullToZero(trans.getStj1TaxAmt())));
                }



                break;
            case STJ2:
                if (trans.getStj2Rate() != null && trans.getStj2Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj2TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj2Rate()));
                } else if (trans.getStj2Setamt() != null && trans.getStj2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj2TaxAmt(trans.getStj2Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj2LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj2TaxAmt() == null)
                        trans.setStj2TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj2TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj2LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj2TaxAmt() == null)
                        trans.setStj2TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add(nullToZero(trans.getStj2TaxAmt())));
                }

                break;
            case STJ3:
                if (trans.getStj3Rate() != null && trans.getStj3Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj3TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj3Rate()));
                } else if (trans.getStj3Setamt() != null && trans.getStj3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj3TaxAmt(trans.getStj3Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj3LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj3TaxAmt() == null)
                        trans.setStj3TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt()).add(nullToZero(trans.getStj3TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj3LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj3TaxAmt() == null)
                        trans.setStj3TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt()).add((trans.getStj3TaxAmt())));
                }


                break;
            case STJ4:
                if (trans.getStj4Rate() != null && trans.getStj4Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj4TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj4Rate()));
                } else if (trans.getStj4Setamt() != null && trans.getStj4Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj4TaxAmt(trans.getStj4Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj4LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj4TaxAmt() == null)
                        trans.setStj4TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt().add(trans.getStj4TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj4LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj4TaxAmt() == null)
                        trans.setStj4TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt().add(trans.getStj4TaxAmt())));
                }

                break;
            case STJ5:

                if (trans.getStj5Rate() != null && trans.getStj5Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj5TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj5Rate()));
                } else if (trans.getStj5Setamt() != null && trans.getStj5Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj5TaxAmt(trans.getStj5Setamt());
                }

                if ("COUNTY".equals(transientDetails.getStj5LocalCode())) {
                    //TRANS.county_local_tax_amt += TRANS.jurLevel_tax_amt
                    if (trans.getCountyLocalTaxAmt() == null)
                        trans.setCountyLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj5TaxAmt() == null)
                        trans.setStj5TaxAmt(BigDecimal.ZERO);
                    trans.setCountyLocalTaxAmt(nullToZero(trans.getCountyLocalTaxAmt().add(trans.getStj5TaxAmt())));
                } else if ("CITY".equals(transientDetails.getStj5LocalCode())) {
                    if (trans.getCityLocalTaxAmt() == null)
                        trans.setCityLocalTaxAmt(BigDecimal.ZERO);
                    if (trans.getStj5TaxAmt() == null)
                        trans.setStj5TaxAmt(BigDecimal.ZERO);
                    trans.setCityLocalTaxAmt(nullToZero(trans.getCityLocalTaxAmt().add(trans.getStj5TaxAmt())));
                }


                break;
            case STJ6:
                if (trans.getStj6Rate() != null && trans.getStj6Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj6TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj6Rate()));
                } else if (trans.getStj6Setamt() != null && trans.getStj6Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj6TaxAmt(trans.getStj6Setamt());
                }
                break;
            case STJ7:
                if (trans.getStj7Rate() != null && trans.getStj7Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj7TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj7Rate()));
                } else if (trans.getStj7Setamt() != null && trans.getStj7Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj7TaxAmt(trans.getStj7Setamt());
                }
                break;
            case STJ8:
                if (trans.getStj8Rate() != null && trans.getStj8Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj8TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj8Rate()));
                } else if (trans.getStj8Setamt() != null && trans.getStj8Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj8TaxAmt(trans.getStj8Setamt());
                }
                break;
            case STJ9:
                if (trans.getStj9Rate() != null && trans.getStj9Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj9TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj9Rate()));
                } else if (trans.getStj9Setamt() != null && trans.getStj9Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj9TaxAmt(trans.getStj9Setamt());
                }
                break;
            case STJ10:
                if (trans.getStj10Rate() != null && trans.getStj10Rate().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt = taxable_amt * jurLevel_tier1Rate
                    trans.setStj10TaxAmt(getJurlLevelTaxableAmt(trans, jurLevel)
                            .multiply(trans.getStj10Rate()));
                } else if (trans.getStj10Setamt() != null && trans.getStj10Setamt().compareTo(BigDecimal.ZERO) == 1) {
                    //TRANS.jurLevel_tier1_tax_amt =  jurLevel_tier1Setamt
                    trans.setStj10TaxAmt(trans.getStj10Setamt());
                }
                break;


        }
    }

    private void calcTaxTiers(PurchaseTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        BigDecimal tmpTier1TaxableAmt = null;
        BigDecimal vTier2RangeAmt;
        BigDecimal vTier3MinAmt;
        BigDecimal vTier3MaxAmt;
        vTier3MaxAmt = BigDecimal.valueOf(999999);
        BigDecimal vTier3RangeAmt;

        switch (jurLevel) {
            case COUNTRY:
                tmpTier1TaxableAmt = trans.getCountryTier1TaxableAmt();
//                Calculate Tier Taxable Amts
/*
* TRANS.
jurLevel_tier1_taxable_amt = 0
jurLevel_tier2_taxable_amt = 0
jurLevel_tier3_taxable_amt = 0*/
                trans.setCountryTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCountryTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCountryTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCountryTier2MaxAmt() == null)
                    trans.setCountryTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCountryTier2MinAmt() == null)
                    trans.setCountryTier2MinAmt(BigDecimal.ZERO);

                vTier2RangeAmt = trans.getCountryTier2MaxAmt().subtract(trans.getCountryTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCountryTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCountryTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCountryTier1TaxableAmt() != null && trans.getCountryTier1MaxAmt() != null
                        && trans.getCountryTier1TaxableAmt().compareTo(trans.getCountryTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCountryTier1TaxableAmt(trans.getCountryTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCountryTier2Rate() != null && trans.getCountryTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCountryTier2Setamt() != null && trans.getCountryTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCountryTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCountryTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCountryTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCountryTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCountryTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCountryTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCountryTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCountryTier3Rate() != null && trans.getCountryTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCountryTier3Setamt()!= null && trans.getCountryTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCountryTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCountryTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCountryTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCountryTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCountryTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCountryTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCountryTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCountryTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCountryTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }

                //Calculate Tier Tax Amounts


                trans.setCountryTier1TaxAmt(BigDecimal.ZERO);
                trans.setCountryTier2TaxAmt(BigDecimal.ZERO);
                trans.setCountryTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCountryTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCountryTier1TaxAmt(trans.getCountryTier1TaxableAmt().multiply(trans.getCountryTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCountryTier1TaxAmt(trans.getCountryTier1Setamt());
                    }
                }

                if (trans.getCountryTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountryTier2Rate() != null && trans.getCountryTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier2TaxAmt(trans.getCountryTier2TaxableAmt().multiply(trans.getCountryTier2Rate()));
                    } else if (trans.getCountryTier2Setamt() != null && trans.getCountryTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier2TaxAmt(trans.getCountryTier2Setamt());
                    }
                }

                if (trans.getCountryTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountryTier3Rate() != null && trans.getCountryTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier3TaxAmt(trans.getCountryTier3TaxableAmt().multiply(trans.getCountryTier3Rate()));
                    } else if (trans.getCountryTier3Setamt() != null && trans.getCountryTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier3TaxAmt(trans.getCountryTier3Setamt());
                    }
                }



                //trans.setStateTaxAm
                /*TRANS.tax_amt = jurLevel_tier1_tax_amt + jurLevel_tier2_tax_amt + jurLevel_tier3_tax_amt*/

                //trans.setCountryTaxAmt

                break;
            case STATE:

                tmpTier1TaxableAmt = trans.getStateTier1TaxableAmt();

                trans.setStateTier1TaxableAmt(BigDecimal.ZERO);
                trans.setStateTier2TaxableAmt(BigDecimal.ZERO);
                trans.setStateTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getStateTier2MaxAmt() == null)
                    trans.setStateTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getStateTier2MinAmt() == null)
                    trans.setStateTier2MinAmt(BigDecimal.ZERO);

                vTier2RangeAmt = trans.getStateTier2MaxAmt().subtract(trans.getStateTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getStateTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setStateTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getStateTier1TaxableAmt() != null && trans.getStateTier1MaxAmt() != null
                        && trans.getStateTier1TaxableAmt().compareTo(trans.getStateTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setStateTier1TaxableAmt(trans.getStateTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getStateTier2Rate() != null && trans.getStateTier2Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getStateTier2Setamt() != null && trans.getStateTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setStateTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getStateTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getStateTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getStateTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setStateTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getStateTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setStateTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getStateTier3Rate() != null && trans.getStateTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getStateTier3Setamt()!= null && trans.getStateTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setStateTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getStateTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getStateTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setStateTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getStateTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setStateTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getStateTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setStateTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setStateTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setStateTier1TaxAmt(BigDecimal.ZERO);
                trans.setStateTier2TaxAmt(BigDecimal.ZERO);
                trans.setStateTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getStateTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setStateTier1TaxAmt(trans.getStateTier1TaxableAmt().multiply(trans.getStateTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setStateTier1TaxAmt(trans.getStateTier1Setamt());
                    }
                }

                if (trans.getStateTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getStateTier2Rate() != null && trans.getStateTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier2TaxAmt(trans.getStateTier2TaxableAmt().multiply(trans.getStateTier2Rate()));
                    } else if (trans.getStateTier2Setamt() != null && trans.getStateTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier2TaxAmt(trans.getStateTier2Setamt());
                    }
                }

                if (trans.getStateTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getStateTier3Rate() != null && trans.getStateTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier3TaxAmt(trans.getStateTier3TaxableAmt().multiply(trans.getStateTier3Rate()));
                    } else if (trans.getStateTier3Setamt() != null && trans.getStateTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier3TaxAmt(trans.getStateTier3Setamt());
                    }
                }



                break;
            case COUNTY:

                tmpTier1TaxableAmt = trans.getCountyTier1TaxableAmt();

                trans.setCountyTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCountyTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCountyTier3TaxableAmt(BigDecimal.ZERO);
                /*
                * v_tier2_range_amt =
                jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCountyTier2MaxAmt() == null)
                    trans.setCountyTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCountyTier2MinAmt() == null)
                    trans.setCountyTier2MinAmt(BigDecimal.ZERO);

                vTier2RangeAmt = trans.getCountyTier2MaxAmt().subtract(trans.getCountyTier2MinAmt());
                /*v_tier3_min_amt = jurLevel_tier2_max_amt
                v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCountyTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCountyTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCountyTier1TaxableAmt() != null && trans.getCountyTier1MaxAmt() != null
                        && trans.getCountyTier1TaxableAmt().compareTo(trans.getCountyTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCountyTier1TaxableAmt(trans.getCountyTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCountyTier2Rate() != null && trans.getCountyTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCountyTier2Setamt() != null && trans.getCountyTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCountyTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCountyTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCountyTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCountyTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCountyTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCountyTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCountyTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCountyTier3Rate() != null && trans.getCountyTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCountyTier3Setamt()!= null && trans.getCountyTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCountyTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCountyTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCountyTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCountyTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCountyTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCountyTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCountyTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCountyTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCountyTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setCountyTier1TaxAmt(BigDecimal.ZERO);
                trans.setCountyTier2TaxAmt(BigDecimal.ZERO);
                trans.setCountyTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCountyTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCountyTier1TaxAmt(trans.getCountyTier1TaxableAmt().multiply(trans.getCountyTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCountyTier1TaxAmt(trans.getCountyTier1Setamt());
                    }
                }

                if (trans.getCountyTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountyTier2Rate() != null && trans.getCountyTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier2TaxAmt(trans.getCountyTier2TaxableAmt().multiply(trans.getCountyTier2Rate()));
                    } else if (trans.getCountyTier2Setamt() != null && trans.getCountyTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier2TaxAmt(trans.getCountyTier2Setamt());
                    }
                }

                if (trans.getCountyTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountyTier3Rate() != null && trans.getCountyTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier3TaxAmt(trans.getCountyTier3TaxableAmt().multiply(trans.getCountyTier3Rate()));
                    } else if (trans.getCountyTier3Setamt() != null && trans.getCountyTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier3TaxAmt(trans.getCountyTier3Setamt());
                    }
                }


                break;
            case CITY:
                tmpTier1TaxableAmt = trans.getCityTier1TaxableAmt();

                trans.setCityTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCityTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCityTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCityTier2MaxAmt() == null)
                    trans.setCityTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCityTier2MinAmt() == null)
                    trans.setCityTier2MinAmt(BigDecimal.ZERO);

                vTier2RangeAmt = trans.getCityTier2MaxAmt().subtract(trans.getCityTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCityTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCityTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCityTier1TaxableAmt() != null && trans.getCityTier1MaxAmt() != null
                        && trans.getCityTier1TaxableAmt().compareTo(trans.getCityTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCityTier1TaxableAmt(trans.getCityTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCityTier2Rate() != null && trans.getCityTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCityTier2Setamt() != null && trans.getCityTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCityTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCityTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCityTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCityTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCityTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCityTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCityTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCityTier3Rate() != null && trans.getCityTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCityTier3Setamt()!= null && trans.getCityTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCityTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCityTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCityTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCityTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCityTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCityTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCityTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCityTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCityTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setCityTier1TaxAmt(BigDecimal.ZERO);
                trans.setCityTier2TaxAmt(BigDecimal.ZERO);
                trans.setCityTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCityTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCityTier1TaxAmt(trans.getCityTier1TaxableAmt().multiply(trans.getCityTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCityTier1TaxAmt(trans.getCityTier1Setamt());
                    }
                }

                if (trans.getCityTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCityTier2Rate() != null && trans.getCityTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier2TaxAmt(trans.getCityTier2TaxableAmt().multiply(trans.getCityTier2Rate()));
                    } else if (trans.getCityTier2Setamt() != null && trans.getCityTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier2TaxAmt(trans.getCityTier2Setamt());
                    }
                }

                if (trans.getCityTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCityTier3Rate() != null && trans.getCityTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier3TaxAmt(trans.getCityTier3TaxableAmt().multiply(trans.getCityTier3Rate()));
                    } else if (trans.getCityTier3Setamt() != null && trans.getCityTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier3TaxAmt(trans.getCityTier3Setamt());
                    }
                }
                break;
/*            case STJ1:
                break;
            case STJ2:
                break;
            case STJ3:
                break;
            case STJ4:
                break;
            case STJ5:
                break;
            case STJ6:
                break;
            case STJ7:
                break;
            case STJ8:
                break;
            case STJ9:
                break;
            case STJ10:
                break;*/
        }
    }
    
    private void calcTaxTiers(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        BigDecimal tmpTier1TaxableAmt = null;
        BigDecimal vTier2RangeAmt;
        BigDecimal vTier3MinAmt;
        BigDecimal vTier3MaxAmt;
        vTier3MaxAmt = BigDecimal.valueOf(999999);
        BigDecimal vTier3RangeAmt;  
        BigDecimal signFlag = BigDecimal.ONE;

        switch (jurLevel) {
            case COUNTRY:
            	signFlag = (trans.getCountryTier1TaxableAmt().compareTo(BigDecimal.ZERO) >= 0)? signFlag: signFlag.negate(); 
                tmpTier1TaxableAmt = trans.getCountryTier1TaxableAmt().multiply(signFlag);
                
                
//                Calculate Tier Taxable Amts
/*
* TRANS.
jurLevel_tier1_taxable_amt = 0
jurLevel_tier2_taxable_amt = 0
jurLevel_tier3_taxable_amt = 0*/
                trans.setCountryTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCountryTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCountryTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCountryTier2MaxAmt() == null)
                    trans.setCountryTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCountryTier2MinAmt() == null)
                    trans.setCountryTier2MinAmt(BigDecimal.ZERO);
                

                vTier2RangeAmt = trans.getCountryTier2MaxAmt().subtract(trans.getCountryTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCountryTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCountryTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCountryTier1TaxableAmt() != null && trans.getCountryTier1MaxAmt() != null
                        && trans.getCountryTier1TaxableAmt().compareTo(trans.getCountryTier1MaxAmt()) == 1 ) { 
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCountryTier1TaxableAmt(trans.getCountryTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCountryTier2Rate() != null && trans.getCountryTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCountryTier2Setamt() != null && trans.getCountryTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCountryTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCountryTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCountryTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCountryTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCountryTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCountryTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCountryTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCountryTier3Rate() != null && trans.getCountryTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCountryTier3Setamt()!= null && trans.getCountryTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCountryTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCountryTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCountryTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCountryTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCountryTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCountryTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCountryTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCountryTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCountryTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }

                //Calculate Tier Tax Amounts


                trans.setCountryTier1TaxAmt(BigDecimal.ZERO);
                trans.setCountryTier2TaxAmt(BigDecimal.ZERO);
                trans.setCountryTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCountryTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCountryTier1Rate() != null && trans.getCountryTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCountryTier1TaxAmt(trans.getCountryTier1TaxableAmt().multiply(trans.getCountryTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCountryTier1Setamt() != null && trans.getCountryTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCountryTier1TaxAmt(trans.getCountryTier1Setamt());
                    }
                }

                if (trans.getCountryTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountryTier2Rate() != null && trans.getCountryTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier2TaxAmt(trans.getCountryTier2TaxableAmt().multiply(trans.getCountryTier2Rate()));
                    } else if (trans.getCountryTier2Setamt() != null && trans.getCountryTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier2TaxAmt(trans.getCountryTier2Setamt());
                    }
                }

                if (trans.getCountryTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountryTier3Rate() != null && trans.getCountryTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier3TaxAmt(trans.getCountryTier3TaxableAmt().multiply(trans.getCountryTier3Rate()));
                    } else if (trans.getCountryTier3Setamt() != null && trans.getCountryTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountryTier3TaxAmt(trans.getCountryTier3Setamt());
                    }
                }

                if(signFlag.compareTo(BigDecimal.ZERO) == -1) {
	                trans.setCountryTier1TaxAmt(trans.getCountryTier1TaxAmt().multiply(signFlag));
	                trans.setCountryTier1TaxableAmt(trans.getCountryTier1TaxableAmt().multiply(signFlag));
	                
	                trans.setCountryTier2TaxAmt(trans.getCountryTier2TaxAmt().multiply(signFlag));
	                trans.setCountryTier2TaxableAmt(trans.getCountryTier2TaxableAmt().multiply(signFlag));
	                
	                trans.setCountryTier3TaxAmt(trans.getCountryTier3TaxAmt().multiply(signFlag));
	                trans.setCountryTier3TaxableAmt(trans.getCountryTier3TaxableAmt().multiply(signFlag));
                }
                

                //trans.setStateTaxAm
                /*TRANS.tax_amt = jurLevel_tier1_tax_amt + jurLevel_tier2_tax_amt + jurLevel_tier3_tax_amt*/

                //trans.setCountryTaxAmt

                break;
                
            case STATE:
            	signFlag = (trans.getStateTier1TaxableAmt().compareTo(BigDecimal.ZERO) >= 0)? signFlag: signFlag.negate(); 
                tmpTier1TaxableAmt = trans.getStateTier1TaxableAmt().multiply(signFlag);

                trans.setStateTier1TaxableAmt(BigDecimal.ZERO);
                trans.setStateTier2TaxableAmt(BigDecimal.ZERO);
                trans.setStateTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getStateTier2MaxAmt() == null)
                    trans.setStateTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getStateTier2MinAmt() == null)
                    trans.setStateTier2MinAmt(BigDecimal.ZERO);
                

                vTier2RangeAmt = trans.getStateTier2MaxAmt().subtract(trans.getStateTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getStateTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setStateTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getStateTier1TaxableAmt() != null && (trans.getStateTier1MaxAmt()!=null && trans.getStateTier1MaxAmt().compareTo(BigDecimal.ZERO) == 1)
                        && trans.getStateTier1TaxableAmt().compareTo(trans.getStateTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setStateTier1TaxableAmt(trans.getStateTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getStateTier2Rate() != null && trans.getStateTier2Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getStateTier2Setamt() != null && trans.getStateTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setStateTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getStateTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getStateTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getStateTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setStateTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getStateTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setStateTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getStateTier3Rate() != null && trans.getStateTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getStateTier3Setamt()!= null && trans.getStateTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setStateTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getStateTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getStateTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setStateTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getStateTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setStateTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getStateTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setStateTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setStateTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setStateTier1TaxAmt(BigDecimal.ZERO);
                trans.setStateTier2TaxAmt(BigDecimal.ZERO);
                trans.setStateTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getStateTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getStateTier1Rate() != null && trans.getStateTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setStateTier1TaxAmt(trans.getStateTier1TaxableAmt().multiply(trans.getStateTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getStateTier1Setamt() != null && trans.getStateTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setStateTier1TaxAmt(trans.getStateTier1Setamt());
                    }
                }

                if (trans.getStateTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getStateTier2Rate() != null && trans.getStateTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier2TaxAmt(trans.getStateTier2TaxableAmt().multiply(trans.getStateTier2Rate()));
                    } else if (trans.getStateTier2Setamt() != null && trans.getStateTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier2TaxAmt(trans.getStateTier2Setamt());
                    }
                }

                if (trans.getStateTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getStateTier3Rate() != null && trans.getStateTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier3TaxAmt(trans.getStateTier3TaxableAmt().multiply(trans.getStateTier3Rate()));
                    } else if (trans.getStateTier3Setamt() != null && trans.getStateTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setStateTier3TaxAmt(trans.getStateTier3Setamt());
                    }
                }

                if(signFlag.compareTo(BigDecimal.ZERO) == -1) {
	                trans.setStateTier1TaxAmt(trans.getStateTier1TaxAmt().multiply(signFlag));
	                trans.setStateTier1TaxableAmt(trans.getStateTier1TaxableAmt().multiply(signFlag));
	                
	                trans.setStateTier2TaxAmt(trans.getStateTier2TaxAmt().multiply(signFlag));
	                trans.setStateTier2TaxableAmt(trans.getStateTier2TaxableAmt().multiply(signFlag));
	                
	                trans.setStateTier3TaxAmt(trans.getStateTier3TaxAmt().multiply(signFlag));
	                trans.setStateTier3TaxableAmt(trans.getStateTier3TaxableAmt().multiply(signFlag));
                }

                break;
            case COUNTY:
            	signFlag = (trans.getCountyTier1TaxableAmt().compareTo(BigDecimal.ZERO) >= 0)? signFlag: signFlag.negate(); 
                tmpTier1TaxableAmt = trans.getCountyTier1TaxableAmt().multiply(signFlag);

                trans.setCountyTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCountyTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCountyTier3TaxableAmt(BigDecimal.ZERO);
                /*
                * v_tier2_range_amt =
                jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCountyTier2MaxAmt() == null)
                    trans.setCountyTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCountyTier2MinAmt() == null)
                    trans.setCountyTier2MinAmt(BigDecimal.ZERO);
                
                vTier2RangeAmt = trans.getCountyTier2MaxAmt().subtract(trans.getCountyTier2MinAmt());
                /*v_tier3_min_amt = jurLevel_tier2_max_amt
                v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCountyTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCountyTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCountyTier1TaxableAmt() != null && trans.getCountyTier1MaxAmt() != null
                        && trans.getCountyTier1TaxableAmt().compareTo(trans.getCountyTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCountyTier1TaxableAmt(trans.getCountyTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCountyTier2Rate() != null && trans.getCountyTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCountyTier2Setamt() != null && trans.getCountyTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCountyTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCountyTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCountyTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCountyTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCountyTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCountyTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCountyTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCountyTier3Rate() != null && trans.getCountyTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCountyTier3Setamt()!= null && trans.getCountyTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCountyTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCountyTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCountyTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCountyTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCountyTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCountyTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCountyTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCountyTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCountyTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setCountyTier1TaxAmt(BigDecimal.ZERO);
                trans.setCountyTier2TaxAmt(BigDecimal.ZERO);
                trans.setCountyTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCountyTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCountyTier1Rate() != null && trans.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCountyTier1TaxAmt(trans.getCountyTier1TaxableAmt().multiply(trans.getCountyTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCountyTier1Setamt() != null && trans.getCountyTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCountyTier1TaxAmt(trans.getCountyTier1Setamt());
                    }
                }

                if (trans.getCountyTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountyTier2Rate() != null && trans.getCountyTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier2TaxAmt(trans.getCountyTier2TaxableAmt().multiply(trans.getCountyTier2Rate()));
                    } else if (trans.getCountyTier2Setamt() != null && trans.getCountyTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier2TaxAmt(trans.getCountyTier2Setamt());
                    }
                }

                if (trans.getCountyTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCountyTier3Rate() != null && trans.getCountyTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier3TaxAmt(trans.getCountyTier3TaxableAmt().multiply(trans.getCountyTier3Rate()));
                    } else if (trans.getCountyTier3Setamt() != null && trans.getCountyTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCountyTier3TaxAmt(trans.getCountyTier3Setamt());
                    }
                }

                if(signFlag.compareTo(BigDecimal.ZERO) == -1) {
	                trans.setCountyTier1TaxAmt(trans.getCountyTier1TaxAmt().multiply(signFlag));
	                trans.setCountyTier1TaxableAmt(trans.getCountyTier1TaxableAmt().multiply(signFlag));
	                
	                trans.setCountyTier2TaxAmt(trans.getCountyTier2TaxAmt().multiply(signFlag));
	                trans.setCountyTier2TaxableAmt(trans.getCountyTier2TaxableAmt().multiply(signFlag));
	                
	                trans.setCountyTier3TaxAmt(trans.getCountyTier3TaxAmt().multiply(signFlag));
	                trans.setCountyTier3TaxableAmt(trans.getCountyTier3TaxableAmt().multiply(signFlag));
                }

                break;
            case CITY:
            	signFlag = (trans.getCityTier1TaxableAmt().compareTo(BigDecimal.ZERO) >= 0)? signFlag: signFlag.negate(); 
                tmpTier1TaxableAmt = trans.getCityTier1TaxableAmt().multiply(signFlag);

                trans.setCityTier1TaxableAmt(BigDecimal.ZERO);
                trans.setCityTier2TaxableAmt(BigDecimal.ZERO);
                trans.setCityTier3TaxableAmt(BigDecimal.ZERO);
/*
* v_tier2_range_amt =
jurLevel_tier2_max_amt – jurLevel_tier2_min_amt*/
                if (trans.getCityTier2MaxAmt() == null)
                    trans.setCityTier2MaxAmt(BigDecimal.ZERO);
                if (trans.getCityTier2MinAmt() == null)
                    trans.setCityTier2MinAmt(BigDecimal.ZERO);
                vTier2RangeAmt = trans.getCityTier2MaxAmt().subtract(trans.getCityTier2MinAmt());
/*v_tier3_min_amt = jurLevel_tier2_max_amt
v_tier3_max_amt = 999999*/
                vTier3MinAmt = trans.getCityTier2MaxAmt();

                //v_tier3_range_amt = v_tier3_max_amt – v_tier3_min_amt

                vTier3RangeAmt = vTier3MaxAmt.subtract(vTier3MinAmt);


                //jurLevel_tier1_rate > 0?
                //jurLevel_tier1_setamt > 0?
                if ((trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) ||
                        (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                    //jurLevel_tier1_taxable_amt = TRANS.taxable_amt
                    trans.setCityTier1TaxableAmt(tmpTier1TaxableAmt);
                } else
                    return;


                //jurLevel_tier1_taxable_amt > jurLevel_tier1_max_amt?

                if (trans.getCityTier1TaxableAmt() != null && trans.getCityTier1MaxAmt() != null
                        && trans.getCityTier1TaxableAmt().compareTo(trans.getCityTier1MaxAmt()) == 1 ) {
                    //jurLevel_tier1_taxable_amt. = jurLevel_tier1_max_amt
                    trans.setCityTier1TaxableAmt(trans.getCityTier1MaxAmt());
                }

                //jurLevel_tier2_rate > 0?
                //jurLevel_tier2_setamt > 0?
                if ((trans.getCityTier2Rate() != null && trans.getCityTier2Rate().compareTo(BigDecimal.ZERO) ==1) ||
                        (trans.getCityTier2Setamt() != null && trans.getCityTier2Setamt().compareTo(BigDecimal.ZERO) == 1) ) {
                    //jurLevel_tier2_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_min_amt
                    trans.setCityTier2TaxableAmt(tmpTier1TaxableAmt
                            .subtract(trans.getCityTier2MinAmt()));

                    //jurLevel_tier2_taxable_amt < 0?
                    if (trans.getCityTier2TaxableAmt().compareTo(BigDecimal.ZERO) >= 0) {
                        //jurLevel_tier2_tax_entam_flag = ‘1’?
                        if (trans.getCityTier2EntamBooleanFlag()) {
                            //jurLevel_tier2_taxable_amt = TRANS.taxable_amt
                            trans.setCityTier2TaxableAmt(tmpTier1TaxableAmt);
                            //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                        } else if (trans.getCityTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1){
                            //jurLevel_tier2_taxable_amt = v_tier2_range_amt
                            trans.setCityTier2TaxableAmt(vTier2RangeAmt);
                        }

                        //jurLevel_tier3_rate > 0?
                        //jurLevel_tier3_setamt > 0?
                        if ((trans.getCityTier3Rate() != null && trans.getCityTier3Rate().compareTo(BigDecimal.ZERO) == 1 ) ||
                                (trans.getCityTier3Setamt()!= null && trans.getCityTier3Setamt().compareTo(BigDecimal.ZERO) == 1)) {
                            //jurLevel_tier3_taxable_amt = TRANS.taxable_amt – jurLevel_tier2_max_amt
                            trans.setCityTier3TaxableAmt(tmpTier1TaxableAmt
                                    .subtract(trans.getCityTier2MaxAmt()));

                            //jurLevel__tier3_taxable_amt < 0?
                            if (trans.getCityTier3TaxableAmt().compareTo(BigDecimal.ZERO) == -1) {
                                //jurLevel_tier3_taxable_amt = 0
                                trans.setCityTier3TaxableAmt(BigDecimal.ZERO);

                                //jurLevel_tier3_tax_entam_flag = ‘1’?
                            } else if(trans.getCityTier3EntamBooleanFlag()) {
                                //jurLevel_tier3_taxable_amt = TRANS.taxable_amt
                                trans.setCityTier3TaxableAmt(tmpTier1TaxableAmt);

                                //jurLevel_tier2_taxable_amt > v_tier2_range_amt?
                            } else if(trans.getCityTier2TaxableAmt().compareTo(vTier2RangeAmt) == 1) {
                                //jurLevel_tier3_taxable_amt = v_tier3_range_amt
                                trans.setCityTier3TaxableAmt(vTier3RangeAmt);
                            }

                        }

                    } else {
                        trans.setCityTier2TaxableAmt(BigDecimal.ZERO);
                    }

                }


                trans.setCityTier1TaxAmt(BigDecimal.ZERO);
                trans.setCityTier2TaxAmt(BigDecimal.ZERO);
                trans.setCityTier3TaxAmt(BigDecimal.ZERO);


                //jurLevel_tier1_taxable_amt > 0?
                if (trans.getCityTier1TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    //jurLevel_tier1_rate > 0?
                    if (trans.getCityTier1Rate() != null && trans.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_taxable_amt * jurLevel_tier1_rate
                        trans.setCityTier1TaxAmt(trans.getCityTier1TaxableAmt().multiply(trans.getCityTier1Rate()));

                        //jurLevel_tier1_setamt > 0?
                    } else if (trans.getCityTier1Setamt() != null && trans.getCityTier1Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        //jurLevel_tier1_tax_amt = jurLevel_tier1_setamt
                        trans.setCityTier1TaxAmt(trans.getCityTier1Setamt());
                    }
                }

                if (trans.getCityTier2TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCityTier2Rate() != null && trans.getCityTier2Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier2TaxAmt(trans.getCityTier2TaxableAmt().multiply(trans.getCityTier2Rate()));
                    } else if (trans.getCityTier2Setamt() != null && trans.getCityTier2Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier2TaxAmt(trans.getCityTier2Setamt());
                    }
                }

                if (trans.getCityTier3TaxableAmt().compareTo(BigDecimal.ZERO) == 1) {
                    if (trans.getCityTier3Rate() != null && trans.getCityTier3Rate().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier3TaxAmt(trans.getCityTier3TaxableAmt().multiply(trans.getCityTier3Rate()));
                    } else if (trans.getCityTier3Setamt() != null && trans.getCityTier3Setamt().compareTo(BigDecimal.ZERO) == 1) {
                        trans.setCityTier3TaxAmt(trans.getCityTier3Setamt());
                    }
                }
                
                if(signFlag.compareTo(BigDecimal.ZERO) == -1) {
	                trans.setCityTier1TaxAmt(trans.getCityTier1TaxAmt().multiply(signFlag));
	                trans.setCityTier1TaxableAmt(trans.getCityTier1TaxableAmt().multiply(signFlag));
	                
	                trans.setCityTier2TaxAmt(trans.getCityTier2TaxAmt().multiply(signFlag));
	                trans.setCityTier2TaxableAmt(trans.getCityTier2TaxableAmt().multiply(signFlag));
	                
	                trans.setCityTier3TaxAmt(trans.getCityTier3TaxAmt().multiply(signFlag));
	                trans.setCityTier3TaxableAmt(trans.getCityTier3TaxableAmt().multiply(signFlag));
                }
                
                break;
/*            case STJ1:
                break;
            case STJ2:
                break;
            case STJ3:
                break;
            case STJ4:
                break;
            case STJ5:
                break;
            case STJ6:
                break;
            case STJ7:
                break;
            case STJ8:
                break;
            case STJ9:
                break;
            case STJ10:
                break;*/
        }
    }

    private void calcIfThenElse(TaxMatrix matrix, PurchaseTransaction trans) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        String lessCode;
        String equalCode;
        String greaterCode;

        String thenTaxcodeCode = matrix.getThenTaxcodeCode();
        String elseTaxcodeCode = matrix.getElseTaxcodeCode();

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals("<")
                || matrix.getRelationSign().equals("<=")) {
            lessCode = "then";
        } else
            lessCode = "else";

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals("=")
                || matrix.getRelationSign().equals("<=")) {
            equalCode = "then";
        } else
            equalCode = "else";

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals(">")
                || matrix.getRelationSign().equals(">=")) {
            greaterCode = "then";
        } else
            greaterCode = "else";

        BigDecimal relAmt = (matrix.getRelationAmount() !=null) ? BigDecimal.valueOf(matrix.getRelationAmount()) : BigDecimal.ZERO;
        BigDecimal disAmt = (trans.getGlLineItmDistAmt()!=null) ? trans.getGlLineItmDistAmt() : BigDecimal.ZERO;

        BigDecimal amount = disAmt.subtract(relAmt);

        String relationCode;

        if( amount.compareTo(BigDecimal.ZERO) > 0) {
            relationCode = greaterCode;
        } else if( amount.compareTo(BigDecimal.ZERO) < 0 ) {
            relationCode = lessCode;
        } else
            relationCode = equalCode;

        if( relationCode == "then" ) {
            transientDetails.setHoldCodeFlag(matrix.getThenHoldCodeFlag());
            transientDetails.setSuspendCodeFlag(matrix.getThenSuspendCodeFlag());
            trans.setTaxcodeCode(thenTaxcodeCode);
        } else {
            transientDetails.setHoldCodeFlag(matrix.getElseHoldCodeFlag());
            transientDetails.setSuspendCodeFlag(matrix.getElseSuspendCodeFlag());
            trans.setTaxcodeCode(elseTaxcodeCode);
        }

    }
    
    private void calcIfThenElse(TaxMatrix matrix, MicroApiTransaction trans) {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        String lessCode;
        String equalCode;
        String greaterCode;

        String thenTaxcodeCode = matrix.getThenTaxcodeCode();
        String elseTaxcodeCode = matrix.getElseTaxcodeCode();

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals("<")
                || matrix.getRelationSign().equals("<=")) {
            lessCode = "then";
        } else
            lessCode = "else";

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals("=")
                || matrix.getRelationSign().equals("<=")) {
            equalCode = "then";
        } else
            equalCode = "else";

        if(matrix.getRelationSign().equalsIgnoreCase("na")
                || matrix.getRelationSign().equals(">")
                || matrix.getRelationSign().equals(">=")) {
            greaterCode = "then";
        } else
            greaterCode = "else";

        BigDecimal relAmt = (matrix.getRelationAmount() !=null) ? BigDecimal.valueOf(matrix.getRelationAmount()) : BigDecimal.ZERO;
        BigDecimal disAmt = (trans.getGlLineItmDistAmt()!=null) ? trans.getGlLineItmDistAmt() : BigDecimal.ZERO;

        BigDecimal amount = disAmt.subtract(relAmt);

        String relationCode;

        if( amount.compareTo(BigDecimal.ZERO) > 0) {
            relationCode = greaterCode;
        } else if( amount.compareTo(BigDecimal.ZERO) < 0 ) {
            relationCode = lessCode;
        } else
            relationCode = equalCode;

        if( relationCode == "then" ) {
            transientDetails.setHoldCodeFlag(matrix.getThenHoldCodeFlag());
            transientDetails.setSuspendCodeFlag(matrix.getThenSuspendCodeFlag());
            trans.setTaxcodeCode(thenTaxcodeCode);
        } else {
            transientDetails.setHoldCodeFlag(matrix.getElseHoldCodeFlag());
            transientDetails.setSuspendCodeFlag(matrix.getElseSuspendCodeFlag());
            trans.setTaxcodeCode(elseTaxcodeCode);
        }
    }

    private void calcDistBuckets(PurchaseTransaction trans, PurchaseTransactionDocument document) {
        LinkedList<PurchaseTransaction> distributees = new LinkedList<>();
        TransientPurchaseTransaction transientPurchaseTransaction = trans.getTransientTransaction();

        transientPurchaseTransaction.setInvoiceGoodAmt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceAmt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceGoodCnt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceCnt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceGoodWgt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceWgt(BigDecimal.ZERO);

        for(PurchaseTransaction candidate : document.getPurchaseTransaction()) {
            if(!TAXCODE_TYPE_CODE_DISTRIBUTE.equalsIgnoreCase(candidate.getStateTaxcodeTypeCode())
                    && trans.getStateGoodSvcTypeCode() != null && trans.getStateGoodSvcTypeCode().equalsIgnoreCase(candidate.getStateGoodSvcTypeCode())) {
                if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_GOOD)) {
                    if (candidate.getGlLineItmDistAmt() != null)
                        transientPurchaseTransaction.setInvoiceGoodAmt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodAmt())
                                        .add(nullToZero(candidate.getGlLineItmDistAmt())));
                    if (candidate.getInvoiceLineCount() != null)
                        transientPurchaseTransaction.setInvoiceGoodCnt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodCnt())
                                        .add(nullToZero(candidate.getInvoiceLineCount())));
                    if (candidate.getInvoiceLineWeight() != null)
                        transientPurchaseTransaction.setInvoiceGoodWgt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodWgt())
                                        .add(nullToZero(candidate.getInvoiceLineWeight())));


                } else if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_SERVICE)) {
                    if (candidate.getGlLineItmDistAmt() != null)
                        transientPurchaseTransaction.setInvoiceServiceAmt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceAmt())
                                        .add(nullToZero(candidate.getGlLineItmDistAmt())));
                    if (candidate.getInvoiceLineCount() != null)
                        transientPurchaseTransaction.setInvoiceServiceCnt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceCnt())
                                    .add(nullToZero(candidate.getInvoiceLineCount())));
                    if (candidate.getInvoiceLineWeight() != null)
                        transientPurchaseTransaction.setInvoiceServiceWgt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceWgt())
                                        .add(nullToZero(candidate.getInvoiceLineWeight())));
                }
                distributees.add(candidate);
            }
        }
        BigDecimal totalDist = BigDecimal.ZERO;
        for (PurchaseTransaction distributee : distributees) {
            BigDecimal distAmt = null;
            if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_GOOD)) {
                //TRANS.distr_XX_amt = (N.gl_line_item_dist_amt / PARM.invoiceGoodAmt ) * D.gl_line_item_dist_amt

                if (transientPurchaseTransaction.getInvoiceGoodAmt().compareTo(BigDecimal.ZERO) != 0) {
                    if (distributee.getGlLineItmDistAmt() != null && trans.getGlLineItmDistAmt() != null) {
                        distAmt = distributee.getGlLineItmDistAmt()
                                .divide(transientPurchaseTransaction.getInvoiceGoodAmt(), 7, RoundingMode.HALF_EVEN)
                                .multiply(trans.getGlLineItmDistAmt());
                    }
                }
            } else if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_SERVICE)) {
                //TRANS.distr_XX_amt = (N.gl_line_item_dist_amt / PARM.invoiceSvcAmt ) * D.gl_line_item_dist_amt
                if (transientPurchaseTransaction.getInvoiceServiceAmt().compareTo(BigDecimal.ZERO) != 0) {
                    if (distributee.getGlLineItmDistAmt() != null && trans.getGlLineItmDistAmt() != null) {
                        distAmt = distributee.getGlLineItmDistAmt()
                                .divide(transientPurchaseTransaction.getInvoiceServiceAmt(), 7, RoundingMode.HALF_EVEN)
                                .multiply(trans.getGlLineItmDistAmt());
                    }
                }
            }
            totalDist = totalDist.add(distAmt);

            if (trans.getStateAllocBucket() != null && trans.getStateAllocBucket().length() > 0) {
                String bucketNameFirstCapital = DAOUtils.convSqlToBeanName((trans.getStateAllocBucket()));
                String bucketNameFirstLC = bucketNameFirstCapital.substring(0, 1).toLowerCase() + bucketNameFirstCapital.substring(1, bucketNameFirstCapital.length());

                FieldUtils.setProtectedFieldValue(bucketNameFirstLC, distributee, distAmt);
            }
        }
        if (trans.getGlLineItmDistAmt().compareTo(totalDist) != 0) {
            PurchaseTransaction addTo = distributees.getLast();
            BigDecimal diffAmt = totalDist.subtract(trans.getGlLineItmDistAmt());

            String bucketNameFirstCapital = DAOUtils.convSqlToBeanName((trans.getStateAllocBucket()));
            String bucketNameFirstLC = bucketNameFirstCapital.substring(0, 1).toLowerCase() + bucketNameFirstCapital.substring(1, bucketNameFirstCapital.length());

            try {
                BigDecimal currVal = (BigDecimal) FieldUtils.getFieldValue(addTo, bucketNameFirstLC);
                if (currVal != null) {
                    BigDecimal newVal = currVal.add(diffAmt);
                    FieldUtils.setProtectedFieldValue(bucketNameFirstLC, addTo, newVal);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private void calcDistBuckets(MicroApiTransaction trans, MicroApiTransactionDocument document) {
        LinkedList<MicroApiTransaction> distributees = new LinkedList<>();
        TransientPurchaseTransaction transientPurchaseTransaction = trans.getTransientTransaction();

        transientPurchaseTransaction.setInvoiceGoodAmt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceAmt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceGoodCnt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceCnt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceGoodWgt(BigDecimal.ZERO);
        transientPurchaseTransaction.setInvoiceServiceWgt(BigDecimal.ZERO);

        for(MicroApiTransaction candidate : document.getMicroApiTransaction()) {
            if(!TAXCODE_TYPE_CODE_DISTRIBUTE.equalsIgnoreCase(candidate.getStateTaxcodeTypeCode())
                    && trans.getStateGoodSvcTypeCode() != null && trans.getStateGoodSvcTypeCode().equalsIgnoreCase(candidate.getStateGoodSvcTypeCode())) {
                if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_GOOD)) {
                    if (candidate.getGlLineItmDistAmt() != null)
                        transientPurchaseTransaction.setInvoiceGoodAmt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodAmt())
                                        .add(nullToZero(candidate.getGlLineItmDistAmt())));
                    if (candidate.getInvoiceLineCount() != null)
                        transientPurchaseTransaction.setInvoiceGoodCnt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodCnt())
                                        .add(nullToZero(candidate.getInvoiceLineCount())));
                    if (candidate.getInvoiceLineWeight() != null)
                        transientPurchaseTransaction.setInvoiceGoodWgt(
                                nullToZero(transientPurchaseTransaction.getInvoiceGoodWgt())
                                        .add(nullToZero(candidate.getInvoiceLineWeight())));


                } else if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_SERVICE)) {
                    if (candidate.getGlLineItmDistAmt() != null)
                        transientPurchaseTransaction.setInvoiceServiceAmt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceAmt())
                                        .add(nullToZero(candidate.getGlLineItmDistAmt())));
                    if (candidate.getInvoiceLineCount() != null)
                        transientPurchaseTransaction.setInvoiceServiceCnt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceCnt())
                                    .add(nullToZero(candidate.getInvoiceLineCount())));
                    if (candidate.getInvoiceLineWeight() != null)
                        transientPurchaseTransaction.setInvoiceServiceWgt(
                                nullToZero(transientPurchaseTransaction.getInvoiceServiceWgt())
                                        .add(nullToZero(candidate.getInvoiceLineWeight())));
                }
                distributees.add(candidate);
            }
        }
        BigDecimal totalDist = BigDecimal.ZERO;
        for (MicroApiTransaction distributee : distributees) {
            BigDecimal distAmt = null;
            if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_GOOD)) {
                //TRANS.distr_XX_amt = (N.gl_line_item_dist_amt / PARM.invoiceGoodAmt ) * D.gl_line_item_dist_amt

                if (transientPurchaseTransaction.getInvoiceGoodAmt().compareTo(BigDecimal.ZERO) != 0) {
                    if (distributee.getGlLineItmDistAmt() != null && trans.getGlLineItmDistAmt() != null) {
                        distAmt = distributee.getGlLineItmDistAmt()
                                .divide(transientPurchaseTransaction.getInvoiceGoodAmt(), 7, RoundingMode.HALF_EVEN)
                                .multiply(trans.getGlLineItmDistAmt());
                    }
                }
            } else if (trans.getStateGoodSvcTypeCode().equalsIgnoreCase(GOOD_SERVICE_TYPE_CODE_SERVICE)) {
                //TRANS.distr_XX_amt = (N.gl_line_item_dist_amt / PARM.invoiceSvcAmt ) * D.gl_line_item_dist_amt
                if (transientPurchaseTransaction.getInvoiceServiceAmt().compareTo(BigDecimal.ZERO) != 0) {
                    if (distributee.getGlLineItmDistAmt() != null && trans.getGlLineItmDistAmt() != null) {
                        distAmt = distributee.getGlLineItmDistAmt()
                                .divide(transientPurchaseTransaction.getInvoiceServiceAmt(), 7, RoundingMode.HALF_EVEN)
                                .multiply(trans.getGlLineItmDistAmt());
                    }
                }
            }
            totalDist = totalDist.add(distAmt);

            if (trans.getStateAllocBucket() != null && trans.getStateAllocBucket().length() > 0) {
                String bucketNameFirstCapital = DAOUtils.convSqlToBeanName((trans.getStateAllocBucket()));
                String bucketNameFirstLC = bucketNameFirstCapital.substring(0, 1).toLowerCase() + bucketNameFirstCapital.substring(1, bucketNameFirstCapital.length());

                FieldUtils.setProtectedFieldValue(bucketNameFirstLC, distributee, distAmt);
            }
        }
        if (trans.getGlLineItmDistAmt().compareTo(totalDist) != 0) {
        	MicroApiTransaction addTo = distributees.getLast();
            BigDecimal diffAmt = totalDist.subtract(trans.getGlLineItmDistAmt());

            String bucketNameFirstCapital = DAOUtils.convSqlToBeanName((trans.getStateAllocBucket()));
            String bucketNameFirstLC = bucketNameFirstCapital.substring(0, 1).toLowerCase() + bucketNameFirstCapital.substring(1, bucketNameFirstCapital.length());

            try {
                BigDecimal currVal = (BigDecimal) FieldUtils.getFieldValue(addTo, bucketNameFirstLC);
                if (currVal != null) {
                    BigDecimal newVal = currVal.add(diffAmt);
                    FieldUtils.setProtectedFieldValue(bucketNameFirstLC, addTo, newVal);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Jurisdiction getJurViaRatepoint(PurchaseTransaction trans, LocationType locationType) throws PurchaseTransactionSuspendedException {
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (transientDetails.getPurchasingOptions() == null)
            transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
        Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();

        String ratepointEnabled =  purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED);
        if (ratepointEnabled == null || !"1".equals(ratepointEnabled)){
            logger.debug("Ratepoint is not enabled. ratepointEnabled = "+ratepointEnabled);
            return null;
        }
        String ratepointServerURL = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_SERVER);
        if (ratepointServerURL == null || "".equals(ratepointServerURL)){
            logger.debug("Ratepoint server URL is not set. ratepointServerURL = "+ratepointServerURL);
            trans.getTransientTransaction().getPurchasingOptions().put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointUserId = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USER_ID);
        if (ratepointUserId == null || "".equals(ratepointUserId)){
            logger.debug("Ratepoint user id is not set. ratepointUserId = "+ratepointUserId);
            trans.getTransientTransaction().getPurchasingOptions().put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointPassword = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_PASSWORD);
        if (ratepointPassword == null){
            logger.debug("Ratepoint password is not set. Setting password to a blank value.");
            ratepointPassword = "";

        }
        String latLonLookup = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USELATLONLOOKUP);
        if (latLonLookup == null || !"1".equals(latLonLookup)) {
            latLonLookup = "0";
            logger.debug("Ratepoint uselatlon lookup parameter is null or not equal 1. Setting the value to 0.");
        }
        String latLonLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_LATLONLOOKUP_URL);
        if ("".equals(latLonLookupUrl))
            latLonLookupUrl = null;

        String genericJurAPI = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USEGENERICJURAPI);
        if (genericJurAPI == null || "".equals(genericJurAPI)) {
            genericJurAPI = "0";
            logger.debug("Ratepoint use JurGeneric lookup parameter is null or not equal 1. Setting the value to 0.");
        } else if (!"0".equals(genericJurAPI) && !"H".equalsIgnoreCase(genericJurAPI) && !"L".equalsIgnoreCase(genericJurAPI)) {
            genericJurAPI = "H";
        }
        String genericApiUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_GENERICJURAPI_URL);
        if ("".equals(genericApiUrl))
            genericApiUrl = null;

        String streetLevelLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_STREETLEVELRATE_URL);
        if ("".equals(streetLevelLookupUrl))
            streetLevelLookupUrl = null;

		String locationMethod = getLocationMethodName(locationType);


        Method theMethod = null;

        String lat = null;
        String lon = null;
        String addressLine1 = null;
        String addressLine2 = null;
        String city = null;
        String county = null;
        String stateCode = null;
        String zip = null;
        String countryCode = null;
        Date glDate = null;

        try {
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Lat", new Class[]{});
            theMethod.setAccessible(true);
            lat = (String)theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Long", new Class[]{});
            theMethod.setAccessible(true);
            lon = (String)theMethod.invoke(trans, new Object[]{});
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine1", new Class[]{});
            theMethod.setAccessible(true);
            addressLine1 = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine2", new Class[]{});
            theMethod.setAccessible(true);
            addressLine2 = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "City", new Class[]{});
            theMethod.setAccessible(true);
            city = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "County", new Class[]{});
            theMethod.setAccessible(true);
            county = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "StateCode", new Class[]{});
            theMethod.setAccessible(true);
            stateCode = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zip", new Class[]{});
            theMethod.setAccessible(true);
            zip = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "CountryCode", new Class[]{});
            theMethod.setAccessible(true);
            countryCode = (String) theMethod.invoke(trans, new Object[]{});
            
            glDate = trans.getGlDate();
        } catch (NoSuchMethodException ne) {
            logger.error("Invalid method name!" + ne.getMessage());
        } catch (IllegalAccessException ie) {
            logger.error("Insufficient permissions to call a getter!" + ie.getMessage());
        }  catch (InvocationTargetException ie) {
            logger.error("Invocation target exception while calling a getter!" + ie.getCause().getMessage());
        }


        Jurisdiction ratePointJur = new Jurisdiction();
        if (latLonLookup.equals("1") && lat != null && !lat.equals("") &&
                lon != null && !lon.equals("") && latLonLookupUrl != null) {

            ratePointJur = ratepointLookupService.getRateforLatLon(ratepointServerURL, latLonLookupUrl, ratepointUserId, ratepointPassword, lat, lon);
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.latlon.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur != null && ratePointJur.getGeocode() == null && streetLevelLookupUrl != null
                && !"".equals(streetLevelLookupUrl) && addressLine1 != null && !"".equals(addressLine1) &&
                zip != null && !"".equals(zip)) {

            ratePointJur = ratepointLookupService.getStreetLevelRate(ratepointServerURL, streetLevelLookupUrl, ratepointUserId, ratepointPassword,
                    addressLine1, addressLine2, city, county, stateCode, zip, countryCode, glDate);
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.street.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur != null && ratePointJur.getGeocode() == null
                && ("H".equalsIgnoreCase(genericJurAPI) || "L".equalsIgnoreCase(genericJurAPI) || "A".equalsIgnoreCase(genericJurAPI))
                && genericApiUrl != null
                && (stateCode !=null || zip != null)) {
        	List<Jurisdiction> jurisArray = ratepointLookupService.getJurGeneric(ratepointServerURL, genericApiUrl, ratepointUserId,
                    ratepointPassword, genericJurAPI, city, county, stateCode, zip, countryCode);
            if(jurisArray.size()>0) {
            	ratePointJur = jurisArray.get(0);
            }
        	else {
        		ratePointJur = new Jurisdiction();
        	}
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.generic.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur!=null && ratePointJur.getGeocode()!=null && !"".equals(ratePointJur.getGeocode())) {
            Jurisdiction example = new Jurisdiction();
            example.setGeocode(ratePointJur.getGeocode());
            example.setCity(ratePointJur.getCity());
            example.setZip(ratePointJur.getZip());
            List<Jurisdiction> jurs = jurisdictionDAO.findByExample(example, 2, "");

            if (jurs == null || jurs.size() == 0) {
                trans.setShiptoJurisdictionId(-1l);
                throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P23);
//            } else if (jurs.size()>1) {
//                trans.setShiptoJurisdictionId(-2l);
//                throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P24);
            } else {
            	
            	//Test Code
            	Date currGlDate = trans.getGlDate();
            	
            	if(currGlDate == null)
           		  currGlDate = new Date();
            	
            	List<Jurisdiction> validJurs = new ArrayList<Jurisdiction>();
            	for(Jurisdiction jur : jurs) {
            		if(jur.getEffectiveDate().compareTo(currGlDate) <= 0 && jur.getExpirationDate().compareTo(currGlDate) > 0 ) {
            			validJurs.add(jur);
            		}
            	}
            	
            	if(validJurs.size() > 1) {
            		 trans.setShiptoJurisdictionId(-2l);
            		 throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P24);
            	} else if (validJurs.size() != 0)
            	{
            		Jurisdiction validJurisdiction = validJurs.get(0);
            		trans.setShiptoJurisdictionId(validJurisdiction.getJurisdictionId());
            		return validJurisdiction;
            	}
            	
            	StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.generic.toString().toLowerCase()).append("Jursidiction Not Effective");
                trans.setProcessingNotes(sb.toString());
            	//End Test Code
//                trans.setShiptoJurisdictionId(jurs.get(0).getJurisdictionId());
//                return jurs.get(0);
            }
        }
        return null;

    }
    
    public Jurisdiction getJurViaRatepoint(MicroApiTransaction trans, LocationType locationType) throws PurchaseTransactionSuspendedException {
    	
    	//if values for LocationType are all null or zero, return
        if (!locationType.equals(LocationType.SHIPTO)) {
	        if(isLocationEmpty(trans, locationType)) {
	        	return null;
	        }
	    }
    	
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

        if (transientDetails.getPurchasingOptions() == null)
            transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
        Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();

        String ratepointEnabled =  purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED);
        if (ratepointEnabled == null || !"1".equals(ratepointEnabled)){
            logger.debug("Ratepoint is not enabled. ratepointEnabled = "+ratepointEnabled);
            return null;
        }
        String ratepointServerURL = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_SERVER);
        if (ratepointServerURL == null || "".equals(ratepointServerURL)){
            logger.debug("Ratepoint server URL is not set. ratepointServerURL = "+ratepointServerURL);
            trans.getTransientTransaction().getPurchasingOptions().put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointUserId = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USER_ID);
        if (ratepointUserId == null || "".equals(ratepointUserId)){
            logger.debug("Ratepoint user id is not set. ratepointUserId = "+ratepointUserId);
            trans.getTransientTransaction().getPurchasingOptions().put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointPassword = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_PASSWORD);
        if (ratepointPassword == null){
            logger.debug("Ratepoint password is not set. Setting password to a blank value.");
            ratepointPassword = "";

        }
        String latLonLookup = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USELATLONLOOKUP);
        if (latLonLookup == null || !"1".equals(latLonLookup)) {
            latLonLookup = "0";
            logger.debug("Ratepoint uselatlon lookup parameter is null or not equal 1. Setting the value to 0.");
        }
        String latLonLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_LATLONLOOKUP_URL);
        if ("".equals(latLonLookupUrl))
            latLonLookupUrl = null;

        String genericJurAPI = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USEGENERICJURAPI);
        if (genericJurAPI == null || "".equals(genericJurAPI)) {
            genericJurAPI = "0";
            logger.debug("Ratepoint use JurGeneric lookup parameter is null or not equal 1. Setting the value to 0.");
        } else if (!"0".equals(genericJurAPI) && !"H".equalsIgnoreCase(genericJurAPI) && !"L".equalsIgnoreCase(genericJurAPI)) {
            genericJurAPI = "H";
        }
        String genericApiUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_GENERICJURAPI_URL);
        if ("".equals(genericApiUrl))
            genericApiUrl = null;

        String streetLevelLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_STREETLEVELRATE_URL);
        if ("".equals(streetLevelLookupUrl))
            streetLevelLookupUrl = null;

		String locationMethod = getLocationMethodName(locationType);


        Method theMethod = null;

        String lat = null;
        String lon = null;
        String addressLine1 = null;
        String addressLine2 = null;
        String city = null;
        String county = null;
        String stateCode = null;
        String zip = null;
        String countryCode = null;
        Date glDate  = null;

        try {
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Lat", new Class[]{});
            theMethod.setAccessible(true);
            lat = (String)theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Long", new Class[]{});
            theMethod.setAccessible(true);
            lon = (String)theMethod.invoke(trans, new Object[]{});
            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine1", new Class[]{});
            theMethod.setAccessible(true);
            addressLine1 = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "AddressLine2", new Class[]{});
            theMethod.setAccessible(true);
            addressLine2 = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "City", new Class[]{});
            theMethod.setAccessible(true);
            city = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "County", new Class[]{});
            theMethod.setAccessible(true);
            county = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "StateCode", new Class[]{});
            theMethod.setAccessible(true);
            stateCode = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "Zip", new Class[]{});
            theMethod.setAccessible(true);
            zip = (String) theMethod.invoke(trans, new Object[]{});

            theMethod = trans.getClass().getDeclaredMethod("get" + locationMethod + "CountryCode", new Class[]{});
            theMethod.setAccessible(true);
            countryCode = (String) theMethod.invoke(trans, new Object[]{});
            
            glDate = trans.getGlDate();
        } catch (NoSuchMethodException ne) {
            logger.error("Invalid method name!" + ne.getMessage());
        } catch (IllegalAccessException ie) {
            logger.error("Insufficient permissions to call a getter!" + ie.getMessage());
        }  catch (InvocationTargetException ie) {
            logger.error("Invocation target exception while calling a getter!" + ie.getCause().getMessage());
        }


        Jurisdiction ratePointJur = new Jurisdiction();
        if (latLonLookup.equals("1") && lat != null && !lat.equals("") &&
                lon != null && !lon.equals("") && latLonLookupUrl != null) {

            ratePointJur = ratepointLookupService.getRateforLatLon(ratepointServerURL, latLonLookupUrl, ratepointUserId, ratepointPassword, lat, lon);
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.latlon.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur != null && ratePointJur.getGeocode() == null && streetLevelLookupUrl != null
                && !"".equals(streetLevelLookupUrl) && addressLine1 != null && !"".equals(addressLine1) &&
                zip != null && !"".equals(zip)) {

            ratePointJur = ratepointLookupService.getStreetLevelRate(ratepointServerURL, streetLevelLookupUrl, ratepointUserId, ratepointPassword,
                    addressLine1, addressLine2, city, county, stateCode, zip, countryCode, glDate);
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.street.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur != null && ratePointJur.getGeocode() == null
                && ("H".equalsIgnoreCase(genericJurAPI) || "L".equalsIgnoreCase(genericJurAPI))
                && genericApiUrl != null
                && (stateCode !=null || zip != null)) {
        	List<Jurisdiction> jurisArray = ratepointLookupService.getJurGeneric(ratepointServerURL, genericApiUrl, ratepointUserId,
                    ratepointPassword, genericJurAPI, city, county, stateCode, zip, countryCode);
            if(jurisArray.size()>0) {
            	ratePointJur = jurisArray.get(0);
            }
        	else {
        		ratePointJur = new Jurisdiction();
        	}
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
                StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.generic.toString().toLowerCase());
                trans.setProcessingNotes(sb.toString());
            }
        }

        if (ratePointJur!=null && ratePointJur.getGeocode()!=null && !"".equals(ratePointJur.getGeocode())) {
            Jurisdiction example = new Jurisdiction();
            example.setGeocode(ratePointJur.getGeocode());
            example.setCity(ratePointJur.getCity());
            example.setZip(ratePointJur.getZip());
            List<Jurisdiction> jurs = jurisdictionDAO.findByExample(example, 2, "");

            if (jurs == null || jurs.size() == 0) {
                trans.setShiptoJurisdictionId(-1l);
                throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P23);
//            } else if (jurs.size()>1) {
//                trans.setShiptoJurisdictionId(-2l);
//                throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P24);
            } else {
            	
            	//Test Code
            	Date currGlDate = trans.getGlDate();
            	
            	if(currGlDate == null)
           		  currGlDate = new Date();
            	
            	List<Jurisdiction> validJurs = new ArrayList<Jurisdiction>();
            	for(Jurisdiction jur : jurs) {
            		System.out.println(jur.getJurisdictionId());
            		if(jur.getEffectiveDate().compareTo(currGlDate) <= 0 && jur.getExpirationDate().compareTo(currGlDate) > 0 ) {
            			validJurs.add(jur);
            		}
            	}
            	
            	if(validJurs.size() > 1) {
            		 trans.setShiptoJurisdictionId(-2l);
            		 throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P24);
            	} else if(validJurs.size() != 0)
            	{
            		Jurisdiction validJurisdiction = validJurs.get(0);
            		trans.setShiptoJurisdictionId(validJurisdiction.getJurisdictionId());
            		return validJurisdiction;
            	}
            	//End Test Code
//                trans.setShiptoJurisdictionId(jurs.get(0).getJurisdictionId());
//                return jurs.get(0);
            	StringBuilder sb = new StringBuilder();
                if (trans.getProcessingNotes() != null)
                    sb.append(trans.getProcessingNotes());
                sb.append("-").append(locationType.toString().toLowerCase()).append("=").append(RatepointCallType.generic.toString().toLowerCase()).append("Jursidiction Not Effective");
                trans.setProcessingNotes(sb.toString());
            }
        }
        return null;

    }
    
    public Jurisdiction getJurViaLocationMatrix(MicroApiTransaction trans, LocationType locationType) throws PurchaseTransactionSuspendedException {

        /*  TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

          if (transientDetails.getPurchasingOptions() == null)
              transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
          Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();
          */
          String sql = buildSql(trans, DRIVER_NAMES_TYPE_LOCATION);
          if (sql == null) {
              Map<OptionCodePK, String> purchasingOptions = trans.getTransientTransaction().getPurchasingOptions();
              String locationPreferences = purchasingOptions.get(PurchaseTransactionOptions.PK_LOCATIONPREFERENCE);
              if (!locationPreferences.equalsIgnoreCase(LOCATIONPREFERENCE_LOCATIONMATRIX)) {

                  trans.setErrorCode(PROCESSING_ERROR_NO_LOCATION_DRIVERS);
                  trans.setAbortFlag(true);
                  //throw new PurchaseTransactionSuspendedException(PROCESSING_ERROR_NO_LOCATION_DRIVERS);
                  return null;
              }
          }
          trans.getTransientTransaction().setLocationMatrixSql(sql);

          LocationMatrix locationMatrix = purchaseTransactionDAO.getLocationViaDrivers(sql, trans.getEntityId(), trans.getGlDate(), trans.getGlDate());
          if (locationMatrix == null)
              return null;

          String locationMethod = getLocationMethodName(locationType);
          Method theMethod = null;
  		try{
          	theMethod = trans.getClass().getDeclaredMethod("set" + locationMethod + "LocnMatrixId", new Class[] {Long.class});
      		theMethod.setAccessible(true);
      		theMethod.invoke(trans, new Object[]{locationMatrix.getLocationMatrixId()});
  		}
  		catch(Exception e){
  			e.printStackTrace();
  		}	
  		
      	try {
              Jurisdiction jur = jurisdictionDAO.findById(locationMatrix.getJurisdiction().getJurisdictionId());
              if (jur!=null) {
            	  Date currGlDate = trans.getGlDate();
            	  if(currGlDate == null)
            		  currGlDate = new Date();
            	  
            	  StringBuilder sb = new StringBuilder();
                  if (trans.getProcessingNotes() != null)
                      sb.append(trans.getProcessingNotes());
                  
          		  if(jur.getEffectiveDate().compareTo(currGlDate) <= 0 && jur.getExpirationDate().compareTo(currGlDate) > 0 ) {
	                  sb.append("-").append(locationType.toString().toLowerCase()).append("=").append("locnMatrix");
	                  trans.setProcessingNotes(sb.toString());
	                  return jur;
          		  } else {
          			  sb.append("-").append(locationType.toString().toLowerCase()).append("=").append("locnMatrix *Jurisdiction Not Effective*");
	                  trans.setProcessingNotes(sb.toString());
          			  return null;	
          		  }
                  
                  //else 
                  //sp.append(locationType.toString().toLowerCase()).append("=").append("locnMatrix *Jurisdiction Not Effective*")
                  //return null
              }


          } catch (DataAccessException dte) {
              logger.debug("Cant find jurisdiction!" + dte.getMessage());
          }

          return null;
      }

    
    private String buildSql(MicroApiTransaction trans, String driverType) {

        DecimalFormat df = new DecimalFormat("00");
        final BeanWrapper transWrapper = new BeanWrapperImpl(trans);
        StringBuilder sql = new StringBuilder();
        StringBuilder whereClause = new StringBuilder(" WHERE ");

        String driverTypeField = null;
        String matrixTable = null;


        /*
        * (1):
                v_matrixSelect =
                SELECT *
                FROM :v_matrixTable
                WHERE (module_code = :v_moduleCode)
                AND (default_flag = '0' AND binary_weight > 0)
                AND (entity_id = :entity_id)
                AND (effective_date <= :gl_date AND expiration_date >= :gl_date)
                AND active_flag='1'
*/

        sql.append("SELECT * FROM ");
        if (DRIVER_NAMES_TYPE_LOCATION.equals(driverType)) {
            matrixTable = "tb_location_matrix";
            sql.append(" tb_location_matrix ");
            /// L = NONE
            /// GSB = BILLSALE
            /// T = PURCHUSE
            //whereClause.append(" module_code=").append("'").append(MODULE_CODE_SALE).append("'");
            whereClause.append(" 1=1 ");
            driverTypeField = "locationDriver";
        } else if (DRIVER_NAMES_TYPE_TAX.equals(driverType) || DRIVER_NAMES_TYPE_GOODSNSERVICES.equals(driverType)) {
            matrixTable = "tb_tax_matrix";
            sql.append(" tb_tax_matrix ");
            String modCode = (DRIVER_NAMES_TYPE_TAX.equals(driverType))? MODULE_CODE_PURCHASE: MODULE_CODE_SALE;
            whereClause.append(" module_code=").append("'").append(modCode).append("'");
            driverTypeField = "taxDriver";
        } else return null;

        if((DRIVER_NAMES_TYPE_TAX.equals(driverType) || DRIVER_NAMES_TYPE_LOCATION.equals(driverType)) && (trans.getDefaultMatrixFlag()!=null && "1".equals(trans.getDefaultMatrixFlag()))){
        }
        else{
        	whereClause.append(" AND default_flag='0' AND binary_weight > 0");
        }

        whereClause.append(" AND entity_id  = ?")
           .append(" AND effective_date<=? AND expiration_date >= ? ");
        
        whereClause.append(" AND active_flag='1' ");
        
        if(matrixTable.equals("tb_tax_matrix")) {
        	whereClause.append(" AND (tb_tax_matrix.driver_country_code = '*ALL' OR tb_tax_matrix.driver_country_code = '" + trans.getTransactionCountryCode() + "') ");
        	whereClause.append(" AND (tb_tax_matrix.driver_state_code = '*ALL' OR tb_tax_matrix.driver_state_code = '" + trans.getTransactionStateCode() + "') ");
        }

        Map<String, DataDefinitionColumnDrivers> availableDrivers = cacheManager.getValidDriversMap(driverType, "TB_PURCHTRANS");

        if (availableDrivers == null || availableDrivers.size() == 0)
            return null;

        for (DataDefinitionColumnDrivers driver: availableDrivers.values()) {
            if (driver!=null && "1".equals(driver.getActiveFlag()) && ( driver.getDataType().equalsIgnoreCase("CHAR") ||
                                                                        driver.getDataType().equalsIgnoreCase("VARCHAR") ||
                                                                        driver.getDataType().equalsIgnoreCase("VARCHAR2")) ) {

                String driverValue = (String)transWrapper.getPropertyValue(driverTypeField + df.format(driver.getDriverId()));
                driverValue = (driverValue == null || "null".equalsIgnoreCase(driverValue)) ? "NULL" : "'"+driverValue+"'";
                String matrixColName  = matrixTable+"."+driver.getMatrixColName();
                if ("1".equals(driver.getNullDriverFlag())) {
                    if ("1".equals(driver.getWildcardFlag())) {
/*
* (2a):
                        v_matrixWhere = v_matrixWhere +
                        AND (((:transdriverValue(tax/location_driver_xx) IS NULL AND (:v_matrixTable.driver_xx = '*ALL' OR :v_matrixTable.driver_xx = '*NULL' ))
                        OR (:transdriverValue(tax/location_driver_xx) IS NOT NULL AND (:v_matrixTable.driver_xx = '*ALL'
                        OR :transdriverValue(tax/location_driver_xx) LIKE :v_matrixTable.driver_xx))))
* */
                        whereClause.append(" AND ( ( "+driverValue+" IS NULL AND  ("+matrixColName+"='*ALL' OR "+matrixColName+"='*NULL' ) OR  ")
                                .append(" ("+driverValue+" IS NOT NULL AND  "+matrixColName+"='*ALL'  ) OR ")
                                .append(" ("+driverValue+" LIKE "+matrixColName+") ")
                                .append(" ) ) ");
                    } else {
/*(2b):
                        v_matrixWhere = v_matrixWhere +
                        AND (((:transdriverValue(tax/location_driver_xx) IS NULL AND (:v_matrixTable.driver_xx = '*ALL' OR t:v_matrixTable.driver_xx = '*NULL' ))
                        OR (:transdriverValue(tax/location_driver_xx) IS NOT NULL AND (:v_matrixTable.driver_xx = '*ALL'
                        OR :transdriverValue(tax/location_driver_xx) = :v_matrixTable.driver_xx))))*/

                        whereClause.append(" AND ( ( "+driverValue+" IS NULL AND  ("+matrixColName+"='*ALL' OR "+matrixColName+"='*NULL' ) OR  ")
                                .append(" ("+driverValue+" IS NOT NULL AND  "+matrixColName+"='*ALL'  ) OR ")
                                .append(" ("+driverValue+"="+matrixColName+") ")
                                .append(" ) ) ");
                    }
                } else {
                    if ("1".equals(driver.getWildcardFlag())) {
/*
 (2c):
                        v_matrixWhere = v_matrixWhere +
                        AND ((:v_matrixTable.driver_xx = '*ALL' OR :transdriverValue(tax/location_driver_xx) LIKE :v_matrixTable.driver_xx))

* */
                        whereClause.append("AND ( "+matrixColName+"='*ALL'  OR  "+driverValue+" LIKE "+matrixColName+" ) ");
                    } else {
/*
 (2d):
                        v_matrixWhere = v_matrixWhere +
                        AND ((:v_matrixTable.driver_xx = '*ALL' OR :transdriverValue(tax/location_driver_xx) = :v_matrixTable.driver_xx))
*/
                        whereClause.append("AND ( "+matrixColName+"='*ALL'  OR  "+driverValue+" = "+matrixColName+" ) ");
                    }
                }
            }
/*
 (3):
                v_matrixOrderBy =
                ORDER BY binary_weight DESC, significant_digits DESC, effective_date DESC
*/
        }
        whereClause.append(" ORDER BY binary_weight DESC, significant_digits DESC, effective_date DESC");
        sql.append(whereClause);
        return sql.toString();
    }

    private void populateDrivers(MicroApiTransaction trans, String driverType) {
    	String tablename = "";
    	if (DRIVER_NAMES_TYPE_LOCATION.equals(driverType) || DRIVER_NAMES_TYPE_TAX.equals(driverType)) {
    		tablename = "TB_PURCHTRANS";
        } else {
        	tablename = "TB_BILLTRANS";
        }
        Map<String, DataDefinitionColumnDrivers> availableDrivers = cacheManager.getValidDriversMap(driverType, tablename);

        BeanWrapper src = new BeanWrapperImpl(trans);
        BeanWrapper trg = new BeanWrapperImpl(trans);
        DecimalFormat df = new DecimalFormat("00");

        for(final String propertyName : availableDrivers.keySet()){
            DataDefinitionColumnDrivers driver = availableDrivers.get(propertyName);

            if (driver!=null && driver.getDriverId()!=null && "1".equals(driver.getActiveFlag())) {

                StringBuilder targetPropertyName = new StringBuilder();

                if (DRIVER_NAMES_TYPE_LOCATION.equals(driverType)) {
                    targetPropertyName.append("locationDriver");
                } else {
                    targetPropertyName.append("taxDriver");
                }
                targetPropertyName.append(df.format(driver.getDriverId()));

                String sourcePropertyName = Util.columnToProperty(driver.getTransDtlColName());
                //String sourcePropertyName = TransientPurchaseTransaction.transProperties.get(driver.getTransDtlColName());
                if (sourcePropertyName != null) {
                    String value = (String) src.getPropertyValue(sourcePropertyName);
                    if (value!=null)
                        value = value.trim().toUpperCase();

                    trg.setPropertyValue(
                            targetPropertyName.toString(), value);
                }
            }
        }
    }

    @Override
    public void getTransStatus(MicroApiTransaction transaction) {
        TransientPurchaseTransaction transientDetails = transaction.getTransientTransaction();


        transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transStatus.name());

        if (transientDetails.getHoldCodeFlag() == null || (transientDetails.getHoldCodeFlag()!=null && (!"1".equals(transientDetails.getHoldCodeFlag())
                && !"y".equalsIgnoreCase(transientDetails.getHoldCodeFlag())
                && !"t".equalsIgnoreCase(transientDetails.getHoldCodeFlag())))) {
            if (transientDetails.getPurchasingOptions() == null)
                transientDetails.setPurchasingOptions(cacheManager.getPurchaseEngineOptions(transientDetails.getRefreshCacheFlag()));
            Map<OptionCodePK, String> purchasingOptions = transientDetails.getPurchasingOptions();

            String holdTransFlagStr = purchasingOptions.get(PurchaseTransactionOptions.PK_HOLDTRANSFLAG);
            boolean holdTransFlag = false;
            BigDecimal holdTransAmt = BigDecimal.ZERO;
            if (holdTransFlagStr!=null && "1".equals(holdTransFlagStr))
                holdTransFlag = true;

            if (!holdTransFlag) {
                transaction.setTransactionInd(TRANS_IND_PROCESSED);
                transaction.setSuspendInd(null);
                return;
            }

            String holdtransamtStr  = purchasingOptions.get(PurchaseTransactionOptions.PK_HOLDTRANSAMT);
            try {
                if (holdtransamtStr!=null) {
                    holdTransAmt = new BigDecimal(holdtransamtStr);
                } else {
                    holdTransFlag = false;
                }
            } catch (NumberFormatException nfe) {
                logger.error("Cant convert '"+holdtransamtStr+"' to BigDecimal!");
                nfe.printStackTrace();
            }

            if (!holdTransFlag) {
                transaction.setTransactionInd(TRANS_IND_PROCESSED);
                transaction.setSuspendInd(null);
                return;
            }


            if (transaction.getGlLineItmDistAmt()!=null && transaction.getGlLineItmDistAmt().compareTo(holdTransAmt) == 1) {
                transaction.setTransactionInd(TRANS_IND_HELD);
                transaction.setSuspendInd(null);
            } else {
                transaction.setTransactionInd(TRANS_IND_PROCESSED);
                transaction.setSuspendInd(null);
            }
        } else {
            transaction.setTransactionInd(TRANS_IND_HELD);
            transaction.setSuspendInd(null);
        }
    }
    
    private void suspend(MicroApiTransaction trans, SuspendReason reason) {
    	suspend(trans, reason, null);
    }
    
    private void suspend(MicroApiTransaction trans, SuspendReason reason, Long matrixId) {
        trans.setTransactionInd(TRANS_IND_SUSPENDED);
        if (SuspendReason.SUSPEND_LOCN == reason) {
            if(trans.getShiptoManualJurInd() == null)
            	wipeLocation(trans, matrixId);
            trans.setSuspendInd(TRANS_SUSPEND_IND_LOCATION);
        } else if (SuspendReason.SUSPEND_GS == reason) {
            wipeTaxCode(trans);
            trans.setSuspendInd(TRANS_SUSPEND_IND_GSB);
        } else if (SuspendReason.SUSPEND_TAXCODE_DTL == reason) {
            wipeTaxCodeDetails(trans);
            trans.setSuspendInd(TRANS_SUSPEND_IND_TAXCODE_DTL);
        } else if (SuspendReason.SUSPEND_RATE == reason) {
            wipeTaxRate(trans);
            trans.setSuspendInd(TRANS_SUSPEND_IND_RATE);
        }
    }
    
    public void getSitus(MicroApiTransaction transaction) throws PurchaseTransactionProcessingException {
        TransientPurchaseTransaction transientDetails = transaction.getTransientTransaction();
        //TODO change getTransactionStateCode to a global level TransactionCode, change hardcoded methodDeliveryCode

        String typeCode = "";
    	switch (transaction.getModuleCode()) {
			case PURCHUSE:
				typeCode = TRANSACTION_TYPE_CODE_PURCHASE;        
				break;
	
			case BILLSALE:
				typeCode = TRANSACTION_TYPE_CODE_BILL;
				break;
	
			default:
				break;
        }
        
        List<SitusDriverSitusDriverDetailDTO> driverList = situsDriverDAO.getSitusDriverDetailWithSitusInfo(typeCode,
                (transaction.getStateRatetypeCode() != null)? transaction.getStateRatetypeCode():"0",
                (transaction.getMethodDeliveryCode() != null)? transaction.getMethodDeliveryCode() :"1");

        if (driverList == null || driverList.size() == 0)
            throw new PurchaseTransactionAbortProcessingException("getSitus - no drivers available.");

        String transactionTypeCode = null;
        String ratetypeCode = null;
        String methodDeliveryCode = null;

        List<SitusInfoDTO> situsInfoList = new ArrayList<>();

        for (SitusDriverSitusDriverDetailDTO sd: driverList) {
            if( (transactionTypeCode != null && !transactionTypeCode.equals(sd.getTransactionTypeCode())) ||
                    (ratetypeCode != null && !ratetypeCode.equals(sd.getRatetypeCode())) ||
                    (methodDeliveryCode != null && !methodDeliveryCode.equals(sd.getMethodDeliveryCode())) ) {
                break;
            }


            transactionTypeCode = sd.getTransactionTypeCode();
            ratetypeCode = sd.getRatetypeCode();
            methodDeliveryCode = sd.getMethodDeliveryCode();
            String locationType = sd.getLocationTypeCode();
            if (locationType == null) throw new PurchaseTransactionAbortProcessingException("Location type is null!");

            SitusInfoDTO driverInfo = new SitusInfoDTO();
            driverInfo.setSitusDriver(locationType);


            if (locationType.equals(LocationType.SHIPFROM.shortCode())) {

                driverInfo.setCountryJur(transaction.getShipfromCountryCode());
                driverInfo.setStateJur(transaction.getShipfromStateCode());
                driverInfo.setCountyJur(transaction.getShipfromCounty());
                driverInfo.setCityJur(transaction.getShipfromCity());
                driverInfo.setStj1Jur(transaction.getShipfromStj1Name());
                driverInfo.setStj2Jur(transaction.getShipfromStj2Name());
                driverInfo.setStj3Jur(transaction.getShipfromStj3Name());
                driverInfo.setStj4Jur(transaction.getShipfromStj4Name());
                driverInfo.setStj5Jur(transaction.getShipfromStj5Name());

            } else if (locationType.equals(LocationType.SHIPTO.shortCode())) {

                driverInfo.setCountryJur(transaction.getShiptoCountryCode());
                driverInfo.setStateJur(transaction.getShiptoStateCode());
                driverInfo.setCountyJur(transaction.getShiptoCounty());
                driverInfo.setCityJur(transaction.getShiptoCity());
                driverInfo.setStj1Jur(transaction.getShiptoStj1Name());
                driverInfo.setStj2Jur(transaction.getShiptoStj2Name());
                driverInfo.setStj3Jur(transaction.getShiptoStj3Name());
                driverInfo.setStj4Jur(transaction.getShiptoStj4Name());
                driverInfo.setStj5Jur(transaction.getShiptoStj5Name());

            } else if (locationType.equals(LocationType.ORDERORIGIN.shortCode())) {

                driverInfo.setCountryJur(transaction.getOrdrorgnCountryCode());
                driverInfo.setStateJur(transaction.getOrdrorgnStateCode());
                driverInfo.setCountyJur(transaction.getOrdrorgnCounty());
                driverInfo.setCityJur(transaction.getOrdrorgnCity());
                driverInfo.setStj1Jur(transaction.getOrdrorgnStj1Name());
                driverInfo.setStj2Jur(transaction.getOrdrorgnStj2Name());
                driverInfo.setStj3Jur(transaction.getOrdrorgnStj3Name());
                driverInfo.setStj4Jur(transaction.getOrdrorgnStj4Name());
                driverInfo.setStj5Jur(transaction.getOrdrorgnStj5Name());

            } else if (locationType.equals(LocationType.ORDERACCEPT.shortCode())) {

                driverInfo.setCountryJur(transaction.getOrdracptCountryCode());
                driverInfo.setStateJur(transaction.getOrdracptStateCode());
                driverInfo.setCountyJur(transaction.getOrdracptCounty());
                driverInfo.setCityJur(transaction.getOrdracptCity());
                driverInfo.setStj1Jur(transaction.getOrdracptStj1Name());
                driverInfo.setStj2Jur(transaction.getOrdracptStj2Name());
                driverInfo.setStj3Jur(transaction.getOrdracptStj3Name());
                driverInfo.setStj4Jur(transaction.getOrdracptStj4Name());
                driverInfo.setStj5Jur(transaction.getOrdracptStj5Name());

            } else if (locationType.equals(LocationType.FIRSTUSE.shortCode())) {

                driverInfo.setCountryJur(transaction.getFirstuseCountryCode());
                driverInfo.setStateJur(transaction.getFirstuseStateCode());
                driverInfo.setCountyJur(transaction.getFirstuseCounty());
                driverInfo.setCityJur(transaction.getFirstuseCity());
                driverInfo.setStj1Jur(transaction.getFirstuseStj1Name());
                driverInfo.setStj2Jur(transaction.getFirstuseStj2Name());
                driverInfo.setStj3Jur(transaction.getFirstuseStj3Name());
                driverInfo.setStj4Jur(transaction.getFirstuseStj4Name());
                driverInfo.setStj5Jur(transaction.getFirstuseStj5Name());

            } else if (locationType.equals(LocationType.BILLTO.shortCode())) {

                driverInfo.setCountryJur(transaction.getBilltoCountryCode());
                driverInfo.setStateJur(transaction.getBilltoStateCode());
                driverInfo.setCountyJur(transaction.getBilltoCounty());
                driverInfo.setCityJur(transaction.getBilltoCity());
                driverInfo.setStj1Jur(transaction.getBilltoStj1Name());
                driverInfo.setStj2Jur(transaction.getBilltoStj2Name());
                driverInfo.setStj3Jur(transaction.getBilltoStj3Name());
                driverInfo.setStj4Jur(transaction.getBilltoStj4Name());
                driverInfo.setStj5Jur(transaction.getBilltoStj5Name());

            } else if (locationType.equals(LocationType.TITLETRANSFER.shortCode())) {

                driverInfo.setCountryJur(transaction.getTtlxfrCountryCode());
                driverInfo.setStateJur(transaction.getTtlxfrStateCode());
                driverInfo.setCountyJur(transaction.getTtlxfrCounty());
                driverInfo.setCityJur(transaction.getTtlxfrCity());
                driverInfo.setStj1Jur(transaction.getTtlxfrStj1Name());
                driverInfo.setStj2Jur(transaction.getTtlxfrStj2Name());
                driverInfo.setStj3Jur(transaction.getTtlxfrStj3Name());
                driverInfo.setStj4Jur(transaction.getTtlxfrStj4Name());
                driverInfo.setStj5Jur(transaction.getTtlxfrStj5Name());
            }
            situsInfoList.add(driverInfo);
        }

        transientDetails.setSitusInfoList(situsInfoList);


        String[] driverValues = new String[9];
        SitusInfoDTO baseSitusInfo = situsInfoList.get(0); // first one is the base
        int driverCount = situsInfoList.size();


        StringBuilder debugSitusMatrix = new StringBuilder();
        debugSitusMatrix.append("\n");
        debugSitusMatrix.append("\t|");

        for (SitusInfoDTO sti: situsInfoList)
            debugSitusMatrix.append(sti.getSitusDriver()).append("\t|");
        debugSitusMatrix.append("\n");



        String[] jurLevelsStr = {"Country","State", "County", "City", "STJ1", "STJ2", "STJ3", "STJ4", "STJ5"};
        for(int jurIdx = 1; jurIdx<=9; jurIdx++) { //country, state, county, city, stj1-stj5
            debugSitusMatrix.append(jurLevelsStr[jurIdx-1]).append("\t|");
            for(int locnIdx=0; locnIdx<driverCount; locnIdx++) {
                SitusInfoDTO situsInfo = situsInfoList.get(locnIdx);

                switch(jurIdx) {
                    case 1: //jurIdx 1 = Country
                        if (!StringUtils.hasText(situsInfo.getCountryJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if (situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) {
                            driverValues[locnIdx] = "IB";
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }

                        break;
                    case 2: //jurIdx 2 = State
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            driverValues[locnIdx] = "IB";
                        } else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 3: //jurIdx 3 = County
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if (situsInfo.getCountyJur()!=null && situsInfo.getCountyJur().equals(baseSitusInfo.getCountyJur())) {
                                driverValues[locnIdx] = "IB";
                            }
                            else {
                                driverValues[locnIdx] = "OB";
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 4: //jurIdx 4 = City
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if (situsInfo.getCityJur()!=null && situsInfo.getCityJur().equals(baseSitusInfo.getCityJur())) {
                                driverValues[locnIdx] = "IB";
                            }
                            else {
                                driverValues[locnIdx] = "OB";
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 5: //jurIdx 5 = Stj1

                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if(locnIdx == 0 && !StringUtils.hasText(baseSitusInfo.getStj1Jur())) {
                                if (!StringUtils.hasText(situsInfoList.get(1).getStj1Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(2).getStj1Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(3).getStj1Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(4).getStj1Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(5).getStj1Jur())) {
                                    driverValues[locnIdx] = "xx";
                                }
                                else {
                                    driverValues[locnIdx] = "IB";
                                }
                            } else {
                                if(situsInfo.getStj1Jur()!=null && situsInfo.getStj1Jur().equals(baseSitusInfo.getStj1Jur())) {
                                    driverValues[locnIdx] = "IB";
                                }
                                else {
                                    driverValues[locnIdx] = "OB";
                                }
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 6: //jurIdx 5 = Stj2
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if(locnIdx == 0 && !StringUtils.hasText(baseSitusInfo.getStj2Jur())) {
                                if (!StringUtils.hasText(situsInfoList.get(1).getStj2Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(2).getStj2Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(3).getStj2Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(4).getStj2Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(5).getStj2Jur())) {
                                    driverValues[locnIdx] = "xx";
                                }
                                else {
                                    driverValues[locnIdx] = "IB";
                                }
                            } else {
                                if(situsInfo.getStj2Jur()!=null && situsInfo.getStj2Jur().equals(baseSitusInfo.getStj2Jur())) {
                                    driverValues[locnIdx] = "IB";
                                }
                                else {
                                    driverValues[locnIdx] = "OB";
                                }
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 7: //jurIdx 5 = Stj3
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if(locnIdx == 0 && !StringUtils.hasText(baseSitusInfo.getStj3Jur())) {
                                if (!StringUtils.hasText(situsInfoList.get(1).getStj3Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(2).getStj3Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(3).getStj3Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(4).getStj3Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(5).getStj3Jur())) {
                                    driverValues[locnIdx] = "xx";
                                }
                                else {
                                    driverValues[locnIdx] = "IB";
                                }
                            } else {
                                if(situsInfo.getStj3Jur()!=null && situsInfo.getStj3Jur().equals(baseSitusInfo.getStj3Jur())) {
                                    driverValues[locnIdx] = "IB";
                                }
                                else {
                                    driverValues[locnIdx] = "OB";
                                }
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 8: //jurIdx 5 = Stj4
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if(locnIdx == 0 && !StringUtils.hasText(baseSitusInfo.getStj4Jur())) {
                                if (!StringUtils.hasText(situsInfoList.get(1).getStj4Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(2).getStj4Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(3).getStj4Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(4).getStj4Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(5).getStj4Jur())) {
                                    driverValues[locnIdx] = "xx";
                                }
                                else {
                                    driverValues[locnIdx] = "IB";
                                }
                            } else {
                                if(situsInfo.getStj4Jur()!=null && situsInfo.getStj4Jur().equals(baseSitusInfo.getStj4Jur())) {
                                    driverValues[locnIdx] = "IB";
                                }
                                else {
                                    driverValues[locnIdx] = "OB";
                                }
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;
                    case 9: //jurIdx 5 = Stj5
                        if (!StringUtils.hasText(situsInfo.getCountryJur()) || !StringUtils.hasText(situsInfo.getStateJur())) {
                            driverValues[locnIdx] = "xx";
                        }
                        else if ((situsInfo.getCountryJur()!=null && situsInfo.getCountryJur().equals(baseSitusInfo.getCountryJur())) &&
                                (situsInfo.getStateJur()!=null && situsInfo.getStateJur().equals(baseSitusInfo.getStateJur()))) {
                            if(locnIdx == 0 && !StringUtils.hasText(baseSitusInfo.getStj5Jur())) {
                                if (!StringUtils.hasText(situsInfoList.get(1).getStj5Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(2).getStj5Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(3).getStj5Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(4).getStj5Jur()) &&
                                        !StringUtils.hasText(situsInfoList.get(5).getStj5Jur())) {
                                    driverValues[locnIdx] = "xx";
                                }
                                else {
                                    driverValues[locnIdx] = "IB";
                                }
                            } else {
                                if(situsInfo.getStj5Jur()!=null && situsInfo.getStj5Jur().equals(baseSitusInfo.getStj5Jur())) {
                                    driverValues[locnIdx] = "IB";
                                }
                                else {
                                    driverValues[locnIdx] = "OB";
                                }
                            }
                        }
                        else {
                            driverValues[locnIdx] = "OS";
                        }
                        break;

                }
                debugSitusMatrix.append(driverValues[locnIdx]).append("\t|");

            }
            debugSitusMatrix.append("\n");


            JurisdictionFilterDTO jurisFilter = new JurisdictionFilterDTO();

            switch (jurIdx) {
                case 1:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState("*COUNTRY");
                    jurisFilter.setJurLevel("*COUNTRY");
                    break;
                case 2:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    jurisFilter.setJurLevel("*STATE");
                    break;
                case 3:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    jurisFilter.setJurLevel("*COUNTY");
                    break;
                case 4:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    jurisFilter.setJurLevel("*CITY");
                    break;
                case 5:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    if(StringUtils.hasText(situsInfoList.get(0).getStj1Jur()) ||
                            StringUtils.hasText(situsInfoList.get(1).getStj1Jur()) ||
                            StringUtils.hasText(situsInfoList.get(2).getStj1Jur()) ||
                            StringUtils.hasText(situsInfoList.get(3).getStj1Jur()) ||
                            StringUtils.hasText(situsInfoList.get(4).getStj1Jur()) ||
                            StringUtils.hasText(situsInfoList.get(5).getStj1Jur())) {
                        jurisFilter.setJurLevel("*STJ");
                    }
                    break;
                case 6:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    if(StringUtils.hasText(situsInfoList.get(0).getStj2Jur()) ||
                            StringUtils.hasText(situsInfoList.get(1).getStj2Jur()) ||
                            StringUtils.hasText(situsInfoList.get(2).getStj2Jur()) ||
                            StringUtils.hasText(situsInfoList.get(3).getStj2Jur()) ||
                            StringUtils.hasText(situsInfoList.get(4).getStj2Jur()) ||
                            StringUtils.hasText(situsInfoList.get(5).getStj2Jur())) {
                        jurisFilter.setJurLevel("*STJ");
                    }
                    break;
                case 7:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    if(StringUtils.hasText(situsInfoList.get(0).getStj3Jur()) ||
                            StringUtils.hasText(situsInfoList.get(1).getStj3Jur()) ||
                            StringUtils.hasText(situsInfoList.get(2).getStj3Jur()) ||
                            StringUtils.hasText(situsInfoList.get(3).getStj3Jur()) ||
                            StringUtils.hasText(situsInfoList.get(4).getStj3Jur()) ||
                            StringUtils.hasText(situsInfoList.get(5).getStj3Jur())) {
                        jurisFilter.setJurLevel("*STJ");
                    }
                    break;
                case 8:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    if(StringUtils.hasText(situsInfoList.get(0).getStj4Jur()) ||
                            StringUtils.hasText(situsInfoList.get(1).getStj4Jur()) ||
                            StringUtils.hasText(situsInfoList.get(2).getStj4Jur()) ||
                            StringUtils.hasText(situsInfoList.get(3).getStj4Jur()) ||
                            StringUtils.hasText(situsInfoList.get(4).getStj4Jur()) ||
                            StringUtils.hasText(situsInfoList.get(5).getStj4Jur())) {
                        jurisFilter.setJurLevel("*STJ");
                    }
                    break;
                case 9:
                    jurisFilter.setCountry(baseSitusInfo.getCountryJur());
                    jurisFilter.setState(baseSitusInfo.getStateJur());
                    if(StringUtils.hasText(situsInfoList.get(0).getStj5Jur()) ||
                            StringUtils.hasText(situsInfoList.get(1).getStj5Jur()) ||
                            StringUtils.hasText(situsInfoList.get(2).getStj5Jur()) ||
                            StringUtils.hasText(situsInfoList.get(3).getStj5Jur()) ||
                            StringUtils.hasText(situsInfoList.get(4).getStj5Jur()) ||
                            StringUtils.hasText(situsInfoList.get(5).getStj5Jur())) {
                        jurisFilter.setJurLevel("*STJ");
                    }
                    break;
            }

            /*
            List<SitusMatrix> situsMatrixList = null;
			if(jurisFilter.getJurLevel() != null) {
				situsMatrixList = situsMatrixDao.getSitusMatrixList(conn, trans.getGlDate(), transTypeCode,
						trans.getRatetypeCode(), trans.getMethodDeliveryCode(), jurisFilter.getCountry(), jurisFilter.getState(),
						jurisFilter.getJurLevel(),
						driverValues[0], driverValues[1], driverValues[2], driverValues[3], driverValues[4], driverValues[5], driverValues[6]);
			}
            * */
            List<SitusMatrix> situsMatrixList = null;
            if(jurisFilter.getJurLevel() != null) {
                situsMatrixList = situsMatrixDAO.getSitusMatrix(
                        transaction.getGlDate(), transactionTypeCode,
                        ratetypeCode, methodDeliveryCode,
                        jurisFilter.getCountry(), jurisFilter.getState(), jurisFilter.getJurLevel(),
                        driverValues[0], driverValues[1], driverValues[2],
                        driverValues[3], driverValues[4], driverValues[5], driverValues[6]);
            }

            boolean breakLoop = false;
            if(situsMatrixList != null && situsMatrixList.size() > 0) {


                //Matrix line found
                SitusMatrix situsMatrix = situsMatrixList.get(0);
                switch (jurIdx) {
                    case 1:  // Country?
                        transaction.setCountrySitusMatrixId(situsMatrix.getSitusMatrixId());
                        transaction.setCountryPrimarySitusCode(situsMatrix.getPrimarySitusCode());
                        transaction.setCountryPrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
                        transaction.setCountrySecondarySitusCode(situsMatrix.getSecondarySitusCode());
                        transaction.setCountrySecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
                        transaction.setCountryAllLevelsFlag(situsMatrix.getAllLevelsFlag());

                        populateStateSitusMatrix(transaction, situsMatrix);
                        populateCountySitusMatrix(transaction, situsMatrix);
                        populateCitySitusMatrix(transaction, situsMatrix);
                        populateStj1SitusMatrix(transaction, situsMatrix);
                        populateStj2SitusMatrix(transaction, situsMatrix);
                        populateStj3SitusMatrix(transaction, situsMatrix);
                        populateStj4SitusMatrix(transaction, situsMatrix);
                        populateStj5SitusMatrix(transaction, situsMatrix);

                        break;
                    case 2: //State?
                        populateStateSitusMatrix(transaction, situsMatrix);
                        populateCountySitusMatrix(transaction, situsMatrix);
                        populateCitySitusMatrix(transaction, situsMatrix);
                        populateStj1SitusMatrix(transaction, situsMatrix);
                        populateStj2SitusMatrix(transaction, situsMatrix);
                        populateStj3SitusMatrix(transaction, situsMatrix);
                        populateStj4SitusMatrix(transaction, situsMatrix);
                        populateStj5SitusMatrix(transaction, situsMatrix);

                        if("1".equals(situsMatrix.getAllLevelsFlag())) {
                            breakLoop = true;
                        }

                        break;
                    case 3:  //County?
                        populateCountySitusMatrix(transaction, situsMatrix);
                        break;
                    case 4: // City
                        populateCitySitusMatrix(transaction, situsMatrix);
                        break;
                    case 5: // Stj1
                        populateStj1SitusMatrix(transaction, situsMatrix);
                        break;
                    case 6: // Stj2
                        populateStj2SitusMatrix(transaction, situsMatrix);
                        break;
                    case 7:
                        populateStj3SitusMatrix(transaction, situsMatrix);
                        break;
                    case 8:
                        populateStj4SitusMatrix(transaction, situsMatrix);
                        break;
                    case 9:
                        populateStj5SitusMatrix(transaction, situsMatrix);
                        break;
                }
                if(breakLoop) {
                    break;
                }
            }
        }

        transientDetails.setSitusDebugMatrix(debugSitusMatrix.toString());


        /****************************************************************************

         Populate Primary values based on Primary Situs Type

         ****************************************************************************/
        // Country
        if (LocationType.SHIPTO.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getShiptoCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getShiptoCountryNexusindCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getShipfromCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getShipfromCountryNexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getOrdrorgnCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getOrdrorgnCountryNexusindCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getOrdracptCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getOrdracptCountryNexusindCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getFirstuseCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getFirstuseCountryNexusindCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getBilltoCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getBilltoCountryNexusindCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getCountryPrimarySitusCode())) {
            transientDetails.setCountryPrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setCountryPrimaryCode(transaction.getTtlxfrCountryCode());
            transientDetails.setCountryPrimaryNexusindCode(transaction.getTtlxfrCountryNexusindCode());
        }
        else {
            transientDetails.setCountryPrimarySitusJurId(null);
            transientDetails.setCountryPrimaryCode(null);
            transientDetails.setCountryPrimaryNexusindCode(null);
        }

        // State
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getShiptoStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getShiptoStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getShiptoCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getShiptoStateCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getShipfromStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getShipfromStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getShipfromCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getShipfromStateCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getOrdrorgnStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getOrdrorgnStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getOrdrorgnCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getOrdrorgnStateCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getOrdracptStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getOrdracptStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getOrdracptCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getOrdracptStateCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getFirstuseStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getFirstuseStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getFirstuseCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getFirstuseStateCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getBilltoStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getBilltoStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getBilltoCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getBilltoStateCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStatePrimarySitusCode())) {
            transientDetails.setStatePrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStatePrimaryCode(transaction.getTtlxfrStateCode());
            transientDetails.setStatePrimaryNexusindCode(transaction.getTtlxfrStateNexusindCode());
            transientDetails.setLookupMaxCountryCode(transaction.getTtlxfrCountryCode());
            transientDetails.setLookupMaxStateCode(transaction.getTtlxfrStateCode());
        }
        else {
            transientDetails.setStatePrimarySitusJurId(null);
            transientDetails.setStatePrimaryCode(null);
            transientDetails.setStatePrimaryNexusindCode(null);

            transientDetails.setLookupMaxCountryCode(null);
            transientDetails.setLookupMaxStateCode(null);
        }

        // County
        if (LocationType.SHIPTO.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getShiptoCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getShiptoCountyNexusindCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getShipfromCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getShipfromCountyNexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getOrdrorgnCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getOrdrorgnCountyNexusindCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getOrdracptCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getOrdracptCountyNexusindCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getFirstuseCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getFirstuseCountyNexusindCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getBilltoCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getBilltoCountyNexusindCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getCountyPrimarySitusCode())) {
            transientDetails.setCountyPrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setCountyPrimaryName(transaction.getTtlxfrCounty());
            transientDetails.setCountyPrimaryNexusindCode(transaction.getTtlxfrCountyNexusindCode());
        }
        else {
            transientDetails.setCountyPrimarySitusJurId(null);
            transientDetails.setCountyPrimaryName(null);
            transientDetails.setCountyPrimaryNexusindCode(null);
        }

        // City
        if (LocationType.SHIPTO.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getShiptoCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getShiptoCityNexusindCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getShipfromCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getShipfromCityNexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getOrdrorgnCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getOrdrorgnCityNexusindCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getOrdracptCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getOrdracptCityNexusindCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getFirstuseCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getFirstuseCityNexusindCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getBilltoCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getBilltoCityNexusindCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getCityPrimarySitusCode())) {
            transientDetails.setCityPrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setCityPrimaryName(transaction.getTtlxfrCity());
            transientDetails.setCityPrimaryNexusindCode(transaction.getTtlxfrCityNexusindCode());
        }
        else {
            transientDetails.setCityPrimarySitusJurId(null);
            transientDetails.setCityPrimaryName(null);
            transientDetails.setCityPrimaryNexusindCode(null);
        }



        // Stj1
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getShiptoStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getShiptoStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getShiptoStj1LocalCode());

        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getShipfromStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getShipfromStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getShipfromStj1LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getOrdrorgnStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getOrdrorgnStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getOrdrorgnStj1LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getOrdracptStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getOrdracptStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getOrdracptStj1LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getFirstuseStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getFirstuseStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getFirstuseStj1LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getBilltoStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getBilltoStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getBilltoStj1LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj1PrimarySitusCode())) {
            transientDetails.setStj1PrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj1PrimaryName(transaction.getTtlxfrStj1Name());
            transientDetails.setStj1PrimaryNexusindCode(transaction.getTtlxfrStj1NexusindCode());
            transientDetails.setStj1PrimaryLocalCode(transientDetails.getTtlxfrStj1LocalCode());
        }
        else {
            transientDetails.setStj1PrimarySitusJurId(null);
            transientDetails.setStj1PrimaryName(null);
            transientDetails.setStj1PrimaryNexusindCode(null);
        }

        // Stj2
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getShiptoStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getShiptoStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getShiptoStj2LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getShipfromStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getShipfromStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getShipfromStj2LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getOrdrorgnStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getOrdrorgnStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getOrdrorgnStj2LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getOrdracptStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getOrdracptStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getOrdracptStj2LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getFirstuseStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getFirstuseStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getFirstuseStj2LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getBilltoStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getBilltoStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getBilltoStj2LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj2PrimarySitusCode())) {
            transientDetails.setStj2PrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj2PrimaryName(transaction.getTtlxfrStj2Name());
            transientDetails.setStj2PrimaryNexusindCode(transaction.getTtlxfrStj2NexusindCode());
            transientDetails.setStj2PrimaryLocalCode(transientDetails.getTtlxfrStj2LocalCode());
        }
        else {
            transientDetails.setStj2PrimarySitusJurId(null);
            transientDetails.setStj2PrimaryName(null);
            transientDetails.setStj2PrimaryNexusindCode(null);
        }

        // Stj3
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getShiptoStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getShiptoStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getShiptoStj3LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getShipfromStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getShipfromStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getShipfromStj3LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getOrdrorgnStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getOrdrorgnStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getOrdrorgnStj3LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getOrdracptStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getOrdracptStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getOrdracptStj3LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getFirstuseStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getFirstuseStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getFirstuseStj3LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getBilltoStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getBilltoStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getBilltoStj3LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj3PrimarySitusCode())) {
            transientDetails.setStj3PrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj3PrimaryName(transaction.getTtlxfrStj3Name());
            transientDetails.setStj3PrimaryNexusindCode(transaction.getTtlxfrStj3NexusindCode());
            transientDetails.setStj3PrimaryLocalCode(transientDetails.getTtlxfrStj3LocalCode());
        }
        else {
            transientDetails.setStj3PrimarySitusJurId(null);
            transientDetails.setStj3PrimaryName(null);
            transientDetails.setStj3PrimaryNexusindCode(null);
        }

        // Stj4
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getShiptoStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getShiptoStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getShiptoStj4LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getShipfromStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getShipfromStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getShipfromStj4LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getOrdrorgnStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getOrdrorgnStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getOrdrorgnStj4LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getOrdracptStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getOrdracptStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getOrdracptStj4LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getFirstuseStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getFirstuseStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getFirstuseStj4LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getBilltoStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getBilltoStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getBilltoStj4LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj4PrimarySitusCode())) {
            transientDetails.setStj4PrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj4PrimaryName(transaction.getTtlxfrStj4Name());
            transientDetails.setStj4PrimaryNexusindCode(transaction.getTtlxfrStj4NexusindCode());
            transientDetails.setStj4PrimaryLocalCode(transientDetails.getTtlxfrStj4LocalCode());
        }
        else {
            transientDetails.setStj4PrimarySitusJurId(null);
            transientDetails.setStj4PrimaryName(null);
            transientDetails.setStj4PrimaryNexusindCode(null);
        }

        // Stj5
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getShiptoStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getShiptoStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getShiptoStj5LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getShipfromStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getShipfromStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getShipfromStj5LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getOrdrorgnStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getOrdrorgnStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getOrdrorgnStj5LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getOrdracptStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getOrdracptStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getOrdracptStj5LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getFirstuseStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getFirstuseStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getFirstuseStj5LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getBilltoStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getBilltoStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getBilltoStj5LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj5PrimarySitusCode())) {
            transientDetails.setStj5PrimarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj5PrimaryName(transaction.getTtlxfrStj5Name());
            transientDetails.setStj5PrimaryNexusindCode(transaction.getTtlxfrStj5NexusindCode());
            transientDetails.setStj5PrimaryLocalCode(transientDetails.getTtlxfrStj5LocalCode());
        }
        else {
            transientDetails.setStj5PrimarySitusJurId(null);
            transientDetails.setStj5PrimaryName(null);
            transientDetails.setStj5PrimaryNexusindCode(null);
        }

        // City - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getShiptoCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getShiptoCityNexusindCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getShipfromCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getShipfromCityNexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getOrdrorgnCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getOrdrorgnCityNexusindCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getOrdracptCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getOrdracptCityNexusindCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getFirstuseCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getFirstuseCityNexusindCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getBilltoCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getBilltoCityNexusindCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getCitySecondarySitusCode())) {
            transientDetails.setCitySecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setCitySecondaryName(transaction.getTtlxfrCity());
            transientDetails.setCitySecondaryNexusindCode(transaction.getTtlxfrCityNexusindCode());
        }
        else {
            transientDetails.setCitySecondarySitusJurId(null);
            transientDetails.setCitySecondaryName(null);
            transientDetails.setCitySecondaryNexusindCode(null);
        }

        // County - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getShiptoCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getShiptoCountyNexusindCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getShipfromCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getShipfromCountyNexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getOrdrorgnCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getOrdrorgnCountyNexusindCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getOrdracptCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getOrdracptCountyNexusindCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getFirstuseCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getFirstuseCountyNexusindCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getBilltoCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getBilltoCountyNexusindCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getCountySecondarySitusCode())) {
            transientDetails.setCountySecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setCountySecondaryName(transaction.getTtlxfrCounty());
            transientDetails.setCountySecondaryNexusindCode(transaction.getTtlxfrCountyNexusindCode());
        }
        else {
            transientDetails.setCountySecondarySitusJurId(null);
            transientDetails.setCountySecondaryName(null);
            transientDetails.setCountySecondaryNexusindCode(null);
        }

        // Stj1 - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getShiptoStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getShiptoStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getShiptoStj1LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getShipfromStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getShipfromStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getShipfromStj1LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getOrdrorgnStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getOrdrorgnStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getOrdrorgnStj1LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getOrdracptStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getOrdracptStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getOrdracptStj1LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getFirstuseStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getFirstuseStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getFirstuseStj1LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getBilltoStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getBilltoStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getBilltoStj1LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj1SecondarySitusCode())) {
            transientDetails.setStj1SecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj1SecondaryName(transaction.getTtlxfrStj1Name());
            transientDetails.setStj1SecondaryNexusindCode(transaction.getTtlxfrStj1NexusindCode());
            transientDetails.setStj1SecondaryLocalCode(transientDetails.getTtlxfrStj1LocalCode());
        }
        else {
            transientDetails.setStj1SecondarySitusJurId(null);
            transientDetails.setStj1SecondaryName(null);
            transientDetails.setStj1SecondaryNexusindCode(null);
        }

        // Stj2 - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getShiptoStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getShiptoStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getShiptoStj2LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getShipfromStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getShipfromStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getShipfromStj2LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getOrdrorgnStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getOrdrorgnStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getOrdrorgnStj2LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getOrdracptStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getOrdracptStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getOrdracptStj2LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getFirstuseStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getFirstuseStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getFirstuseStj2LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getBilltoStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getBilltoStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getBilltoStj2LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj2SecondarySitusCode())) {
            transientDetails.setStj2SecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj2SecondaryName(transaction.getTtlxfrStj2Name());
            transientDetails.setStj2SecondaryNexusindCode(transaction.getTtlxfrStj2NexusindCode());
            transientDetails.setStj2SecondaryLocalCode(transientDetails.getTtlxfrStj2LocalCode());
        }
        else {
            transientDetails.setStj2SecondarySitusJurId(null);
            transientDetails.setStj2SecondaryName(null);
            transientDetails.setStj2SecondaryNexusindCode(null);
        }

        // Stj3 - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getShiptoStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getShiptoStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getShiptoStj3LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getShipfromStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getShipfromStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getShipfromStj3LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getOrdrorgnStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getOrdrorgnStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getOrdrorgnStj3LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getOrdracptStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getOrdracptStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getOrdracptStj3LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getFirstuseStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getFirstuseStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getFirstuseStj3LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getBilltoStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getBilltoStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getBilltoStj3LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj3SecondarySitusCode())) {
            transientDetails.setStj3SecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj3SecondaryName(transaction.getTtlxfrStj3Name());
            transientDetails.setStj3SecondaryNexusindCode(transaction.getTtlxfrStj3NexusindCode());
            transientDetails.setStj3SecondaryLocalCode(transientDetails.getTtlxfrStj3LocalCode());
        }
        else {
            transientDetails.setStj3SecondarySitusJurId(null);
            transientDetails.setStj3SecondaryName(null);
            transientDetails.setStj3SecondaryNexusindCode(null);
        }

        // Stj4 - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getShiptoStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getShiptoStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getShiptoStj4LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getShipfromStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getShipfromStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getShipfromStj4LocalCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getOrdrorgnStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getOrdrorgnStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getOrdrorgnStj4LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getOrdracptStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getOrdracptStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getOrdracptStj4LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getFirstuseStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getFirstuseStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getFirstuseStj4LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getBilltoStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getBilltoStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getBilltoStj4LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj4SecondarySitusCode())) {
            transientDetails.setStj4SecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj4SecondaryName(transaction.getTtlxfrStj4Name());
            transientDetails.setStj4SecondaryNexusindCode(transaction.getTtlxfrStj4NexusindCode());
            transientDetails.setStj4SecondaryLocalCode(transientDetails.getTtlxfrStj4LocalCode());
        }
        else {
            transientDetails.setStj4SecondarySitusJurId(null);
            transientDetails.setStj4SecondaryName(null);
            transientDetails.setStj4SecondaryNexusindCode(null);
        }

        // Stj5 - Secondary
        if (LocationType.SHIPTO.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getShiptoJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getShiptoStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getShiptoStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getShiptoStj5LocalCode());
        }
        else if (LocationType.SHIPFROM.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getShipfromJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getShipfromStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getShipfromStj5NexusindCode());
        }
        else if (LocationType.ORDERORIGIN.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getOrdrorgnJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getOrdrorgnStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getOrdrorgnStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getOrdrorgnStj5LocalCode());
        }
        else if (LocationType.ORDERACCEPT.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getOrdracptJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getOrdracptStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getOrdracptStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getOrdracptStj5LocalCode());
        }
        else if (LocationType.FIRSTUSE.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getFirstuseJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getFirstuseStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getFirstuseStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getFirstuseStj5LocalCode());
        }
        else if (LocationType.BILLTO.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getBilltoJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getBilltoStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getBilltoStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getBilltoStj5LocalCode());
        }
        else if (LocationType.TITLETRANSFER.shortCode().equals(transaction.getStj5SecondarySitusCode())) {
            transientDetails.setStj5SecondarySitusJurId(transaction.getTtlxfrJurisdictionId());
            transientDetails.setStj5SecondaryName(transaction.getTtlxfrStj5Name());
            transientDetails.setStj5SecondaryNexusindCode(transaction.getTtlxfrStj5NexusindCode());
            transientDetails.setStj5SecondaryLocalCode(transientDetails.getTtlxfrStj5LocalCode());
        }
        else {
            transientDetails.setStj5SecondarySitusJurId(null);
            transientDetails.setStj5SecondaryName(null);
            transientDetails.setStj5SecondaryNexusindCode(null);
        }


        /*****************************************************************
         *
         *                       Get Maximum Local Tax Rate
         *
         * ****************************************************************/

        TaxCodeState taxCodeState = taxCodeStateDAO.findByPK(new TaxCodeStatePK(transientDetails.getLookupMaxCountryCode(), transientDetails.getLookupMaxStateCode()));
        if (taxCodeState != null && taxCodeState.getMaxLocalRate() != null) {
            transientDetails.setMaxLocalRate(taxCodeState.getMaxLocalRate());
        } else {
            transientDetails.setMaxLocalRate(BigDecimal.ZERO);
        }

        /*****************************************************************
         *
         *                Determine STJ Names & Types (MTA or SPD)
         *
         * ****************************************************************/

        if (transientDetails.getStj1PrimaryName() != null) {
            if (transientDetails.getStj1PrimaryName().contains("MTA") ||
                    transientDetails.getStj1PrimaryName().contains("CTA") ||
                    transientDetails.getStj1PrimaryName().contains("CTD") ||
                    transientDetails.getStj1PrimaryName().contains("ATD")) {
                transientDetails.setStj1PrimaryType("MTA");
                transientDetails.setPrimaryMtaFlag(Boolean.TRUE);
            }
            else {
                transientDetails.setStj1PrimaryType("SPD");
                transientDetails.setPrimarySpdFlag(Boolean.TRUE);
            }
        }
        if (transientDetails.getStj2PrimaryName() != null) {
            if (transientDetails.getStj2PrimaryName().contains("MTA") ||
                    transientDetails.getStj2PrimaryName().contains("CTA") ||
                    transientDetails.getStj2PrimaryName().contains("CTD") ||
                    transientDetails.getStj2PrimaryName().contains("ATD")) {
                transientDetails.setStj2PrimaryType("MTA");
                transientDetails.setPrimaryMtaFlag(Boolean.TRUE);
            }
            else {
                transientDetails.setStj2PrimaryType("SPD");
                transientDetails.setPrimarySpdFlag(Boolean.TRUE);
            }
        }
        if (transientDetails.getStj3PrimaryName() != null) {
            if (transientDetails.getStj3PrimaryName().contains("MTA") ||
                    transientDetails.getStj3PrimaryName().contains("CTA") ||
                    transientDetails.getStj3PrimaryName().contains("CTD") ||
                    transientDetails.getStj3PrimaryName().contains("ATD")) {
                transientDetails.setStj3PrimaryType("MTA");
                transientDetails.setPrimaryMtaFlag(Boolean.TRUE);
            }
            else {
                transientDetails.setStj3PrimaryType("SPD");
                transientDetails.setPrimarySpdFlag(Boolean.TRUE);
            }
        }
        if (transientDetails.getStj4PrimaryName() != null) {
            if (transientDetails.getStj4PrimaryName().contains("MTA") ||
                    transientDetails.getStj4PrimaryName().contains("CTA") ||
                    transientDetails.getStj4PrimaryName().contains("CTD") ||
                    transientDetails.getStj4PrimaryName().contains("ATD")) {
                transientDetails.setStj4PrimaryType("MTA");
                transientDetails.setPrimaryMtaFlag(Boolean.TRUE);
            }
            else {
                transientDetails.setStj4PrimaryType("SPD");
                transientDetails.setPrimarySpdFlag(Boolean.TRUE);
            }
        }
        if (transientDetails.getStj5PrimaryName() != null) {
            if (transientDetails.getStj5PrimaryName().contains("MTA") ||
                    transientDetails.getStj5PrimaryName().contains("CTA") ||
                    transientDetails.getStj5PrimaryName().contains("CTD") ||
                    transientDetails.getStj5PrimaryName().contains("ATD")) {
                transientDetails.setStj5PrimaryType("MTA");
                transientDetails.setPrimaryMtaFlag(Boolean.TRUE);
            }
            else {
                transientDetails.setStj5PrimaryType("SPD");
                transientDetails.setPrimarySpdFlag(Boolean.TRUE);
            }
        }


        if (transientDetails.getStj1SecondaryName() != null) {
            if (transientDetails.getStj1SecondaryName().contains("MTA") ||
                    transientDetails.getStj1SecondaryName().contains("CTA") ||
                    transientDetails.getStj1SecondaryName().contains("CTD") ||
                    transientDetails.getStj1SecondaryName().contains("ATD")) {
                transientDetails.setStj1SecondaryType("MTA");
            }
            else {
                transientDetails.setStj1SecondaryType("SPD");
            }
        }
        if (transientDetails.getStj2SecondaryName() != null) {
            if (transientDetails.getStj2SecondaryName().contains("MTA") ||
                    transientDetails.getStj2SecondaryName().contains("CTA") ||
                    transientDetails.getStj2SecondaryName().contains("CTD") ||
                    transientDetails.getStj2SecondaryName().contains("ATD")) {
                transientDetails.setStj2SecondaryType("MTA");
            }
            else {
                transientDetails.setStj2SecondaryType("SPD");

            }
        }
        if (transientDetails.getStj3SecondaryName() != null) {
            if (transientDetails.getStj3SecondaryName().contains("MTA") ||
                    transientDetails.getStj3SecondaryName().contains("CTA") ||
                    transientDetails.getStj3SecondaryName().contains("CTD") ||
                    transientDetails.getStj3SecondaryName().contains("ATD")) {
                transientDetails.setStj3SecondaryType("MTA");
            }
            else {
                transientDetails.setStj3SecondaryType("SPD");
            }
        }
        if (transientDetails.getStj4SecondaryName() != null) {
            if (transientDetails.getStj4SecondaryName().contains("MTA") ||
                    transientDetails.getStj4SecondaryName().contains("CTA") ||
                    transientDetails.getStj4SecondaryName().contains("CTD") ||
                    transientDetails.getStj4SecondaryName().contains("ATD")) {
                transientDetails.setStj4SecondaryType("MTA");
            }
            else {
                transientDetails.setStj4SecondaryType("SPD");
            }
        }
        if (transientDetails.getStj5SecondaryName() != null) {
            if (transientDetails.getStj5SecondaryName().contains("MTA") ||
                    transientDetails.getStj5SecondaryName().contains("CTA") ||
                    transientDetails.getStj5SecondaryName().contains("CTD") ||
                    transientDetails.getStj5SecondaryName().contains("ATD")) {
                transientDetails.setStj5SecondaryType("MTA");
            }
            else {
                transientDetails.setStj5SecondaryType("SPD");
            }
        }

        int finalIndex = -1;


        String[] stjFinalSitusCode = new String[10];
        String[] stjFinalTxtypCd = new String[10];
        Long[] stjFinalJurId = new Long[10];
        BigDecimal[] stjFinalRate = new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO};
        String[] stjFinalName = new String[10];
        String[] stjFinalNexusindCode = new String[10];
        String [] stjFinalLocalCode = new String[10];
        Long[] stjFinalTaxrateId = new Long[10];

        /*****************************************************************
         *
         *                Determine all Primary tax rates
         *
         *****************************************************************/

        HashMap<JurisdictionLevel, Long> primarySitusIds = new HashMap<>();
        primarySitusIds.put(JurisdictionLevel.COUNTRY, transientDetails.getCountryPrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STATE, transientDetails.getStatePrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.COUNTY, transientDetails.getCountyPrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.CITY, transientDetails.getCityPrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STJ1, transientDetails.getStj1PrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STJ2, transientDetails.getStj2PrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STJ3, transientDetails.getStj3PrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STJ4, transientDetails.getStj4PrimarySitusJurId());
        primarySitusIds.put(JurisdictionLevel.STJ5, transientDetails.getStj5PrimarySitusJurId());


        HashMap<JurisdictionLevel, String> primaryTaxTypeCodes = new HashMap<>();

        primaryTaxTypeCodes.put(JurisdictionLevel.COUNTRY, transaction.getCountryPrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STATE, transaction.getStatePrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.COUNTY, transaction.getCountyPrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.CITY, transaction.getCityPrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STJ1, transaction.getStj1PrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STJ2, transaction.getStj2PrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STJ3, transaction.getStj3PrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STJ4, transaction.getStj4PrimaryTaxtypeCode());
        primaryTaxTypeCodes.put(JurisdictionLevel.STJ5, transaction.getStj5PrimaryTaxtypeCode());


        MicroApiTransaction transWithPrimaryRates = fetchTaxRatesGeneric(transaction, transientDetails, primarySitusIds, primaryTaxTypeCodes);


        /*****************************************************************
         *
         *          Determine Final Situs as Primary or Secondary
         *
         ******************************************************************/
        
        /* 10/04/2019 MF & AC, transaction is wrong!
        BigDecimal primaryCityRate = (transaction.getCityTier1Rate() == null) ? BigDecimal.ZERO : transWithPrimaryRates.getCityTier1Rate();
        BigDecimal primaryCountyRate = (transaction.getCountyTier1Rate() == null) ? BigDecimal.ZERO : transWithPrimaryRates.getCountyTier1Rate();
        BigDecimal primaryStj1Rate = (transaction.getStj1Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj1Rate();
        BigDecimal primaryStj2Rate = (transaction.getStj2Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj2Rate();
        BigDecimal primaryStj3Rate = (transaction.getStj3Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj3Rate();
        BigDecimal primaryStj4Rate = (transaction.getStj4Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj4Rate();
        BigDecimal primaryStj5Rate = (transaction.getStj5Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj5Rate();
		*/
		
        BigDecimal primaryCityRate = (transWithPrimaryRates.getCityTier1Rate() == null) ? BigDecimal.ZERO : transWithPrimaryRates.getCityTier1Rate();
        BigDecimal primaryCountyRate = (transWithPrimaryRates.getCountyTier1Rate() == null) ? BigDecimal.ZERO : transWithPrimaryRates.getCountyTier1Rate();
        BigDecimal primaryStj1Rate = (transWithPrimaryRates.getStj1Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj1Rate();
        BigDecimal primaryStj2Rate = (transWithPrimaryRates.getStj2Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj2Rate();
        BigDecimal primaryStj3Rate = (transWithPrimaryRates.getStj3Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj3Rate();
        BigDecimal primaryStj4Rate = (transWithPrimaryRates.getStj4Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj4Rate();
        BigDecimal primaryStj5Rate = (transWithPrimaryRates.getStj5Rate() == null ) ? BigDecimal.ZERO : transWithPrimaryRates.getStj5Rate();
    
        transientDetails.setCombinedLocalRate(primaryCityRate.add(primaryCountyRate)
                .add(primaryStj1Rate)
                .add(primaryStj2Rate)
                .add(primaryStj3Rate)
                .add(primaryStj4Rate)
                .add(primaryStj5Rate));


        //10/07/2019 MF & AC
        //if (transientDetails.getMaxLocalRate().compareTo(BigDecimal.ZERO) == 0
        //        && transientDetails.getCombinedLocalRate().compareTo(transientDetails.getMaxLocalRate()) != 1) {
        if (transientDetails.getMaxLocalRate().compareTo(BigDecimal.ZERO) == 0
                || transientDetails.getCombinedLocalRate().compareTo(transientDetails.getMaxLocalRate()) >= 0) {

            transaction.setCitySitusCode(transaction.getCityPrimarySitusCode());
            transaction.setCityTaxtypeUsedCode(transaction.getCityPrimaryTaxtypeCode());
            transaction.setCitySitusJurId(transientDetails.getCityPrimarySitusJurId());
            transaction.setCityName(transientDetails.getCityPrimaryName());
            transaction.setCityNexusindCode(transientDetails.getCityPrimaryNexusindCode());
            transaction.setCityTaxrateId(transWithPrimaryRates.getCityTaxrateId());

            transaction.setCountySitusCode(transaction.getCountyPrimarySitusCode());
            transaction.setCountyTaxtypeUsedCode(transaction.getCountyPrimaryTaxtypeCode());
            transaction.setCountySitusJurId(transientDetails.getCountyPrimarySitusJurId());
            transaction.setCountyName(transientDetails.getCountyPrimaryName());
            transaction.setCountyNexusindCode(transientDetails.getCountyPrimaryNexusindCode());
            transaction.setCountryTaxrateId(transWithPrimaryRates.getCountyTaxrateId());

            if (transientDetails.getStj1PrimaryType() != null && transWithPrimaryRates.getStj1Rate() != null && (transWithPrimaryRates.getStj1Rate().compareTo(BigDecimal.ZERO) == 1) ) {
                /*
                * TRANS.stj1-5_situs_code = vTRANS.stj1-5_primary_situs_code
                    TRANS.stj1-5_taxtype_used_code = vTRANS.stj1-5_primary_taxtype_code
                    TRANS.stj1-5_situs_jur_id = TEMP.stj1-5_primary_situs_jur_id
                    TRANS.stj1-5_name = TEMP.stj1-5_primary_name
                    TRANS.stj1-5_nexusind_code = TEMP.stj1-5_primary_nexusind_code
                    TEMP.stj1-5_local_code = TEMP.stj1-5_primary_local_code
                    TRANS.stj1-5_taxrate_id = PRIMRATE.stj1-5_taxrate_id */
                transaction.setStj1SitusCode(transaction.getStj1PrimarySitusCode());
                transaction.setStj1TaxtypeUsedCode(transaction.getStj1PrimaryTaxtypeCode());
                transaction.setStj1SitusJurId(transientDetails.getStj1PrimarySitusJurId());
                transaction.setStj1Name(transientDetails.getStj1PrimaryName());
                transaction.setStj1NexusindCode(transientDetails.getStj1PrimaryNexusindCode());
                transientDetails.setStj1LocalCode(transientDetails.getStj1PrimaryLocalCode());
                transaction.setStj1TaxrateId(transWithPrimaryRates.getStj1TaxrateId());

            /*    finalIndex++;
                stjFinalSitusCode[finalIndex] = transaction.getStj1PrimarySitusCode();
                stjFinalTxtypCd[finalIndex] = transaction.getStj1PrimaryTaxtypeCode();
                stjFinalJurId[finalIndex] = transientDetails.getStj1PrimarySitusJurId();
                stjFinalRate[finalIndex] = transWithPrimaryRates.getStj1Rate();
                stjFinalName[finalIndex] = transientDetails.getStj1PrimaryName();
                stjFinalNexusindCode[finalIndex] = transientDetails.getStj1NexusIndCode();
                stjFinalLocalCode[finalIndex] = transientDetails.getStj1PrimaryLocalCode();
                stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj1TaxrateId();*/

            }
            if (transientDetails.getStj2PrimaryType() != null && transWithPrimaryRates.getStj2Rate() != null && (transWithPrimaryRates.getStj2Rate().compareTo(BigDecimal.ZERO) == 1) ) {
           /*     finalIndex++;
                stjFinalSitusCode[finalIndex] = transaction.getStj2PrimarySitusCode();
                stjFinalTxtypCd[finalIndex] = transaction.getStj2PrimaryTaxtypeCode();
                stjFinalJurId[finalIndex] = transientDetails.getStj2PrimarySitusJurId();
                stjFinalRate[finalIndex] = transWithPrimaryRates.getStj2Rate();
                stjFinalName[finalIndex] = transientDetails.getStj2PrimaryName();
                stjFinalNexusindCode[finalIndex] = transientDetails.getStj2NexusIndCode();
                stjFinalLocalCode[finalIndex] = transientDetails.getStj2PrimaryLocalCode();
                stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj2TaxrateId();*/
                transaction.setStj2SitusCode(transaction.getStj2PrimarySitusCode());
                transaction.setStj2TaxtypeUsedCode(transaction.getStj2PrimaryTaxtypeCode());
                transaction.setStj2SitusJurId(transientDetails.getStj2PrimarySitusJurId());
                transaction.setStj2Name(transientDetails.getStj2PrimaryName());
                transaction.setStj2NexusindCode(transientDetails.getStj2PrimaryNexusindCode());
                transientDetails.setStj2LocalCode(transientDetails.getStj2PrimaryLocalCode());
                transaction.setStj2TaxrateId(transWithPrimaryRates.getStj2TaxrateId());

            }
            if (transientDetails.getStj3PrimaryType() != null && transWithPrimaryRates.getStj3Rate() != null && (transWithPrimaryRates.getStj3Rate().compareTo(BigDecimal.ZERO) == 1) ) {
           /*     finalIndex++;
                stjFinalSitusCode[finalIndex] = transaction.getStj3PrimarySitusCode();
                stjFinalTxtypCd[finalIndex] = transaction.getStj3PrimaryTaxtypeCode();
                stjFinalJurId[finalIndex] = transientDetails.getStj3PrimarySitusJurId();
                stjFinalRate[finalIndex] = transWithPrimaryRates.getStj3Rate();
                stjFinalName[finalIndex] = transientDetails.getStj3PrimaryName();
                stjFinalNexusindCode[finalIndex] = transientDetails.getStj3NexusIndCode();
                stjFinalLocalCode[finalIndex] = transientDetails.getStj3PrimaryLocalCode();
                stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj3TaxrateId();*/
                transaction.setStj3SitusCode(transaction.getStj3PrimarySitusCode());
                transaction.setStj3TaxtypeUsedCode(transaction.getStj3PrimaryTaxtypeCode());
                transaction.setStj3SitusJurId(transientDetails.getStj3PrimarySitusJurId());
                transaction.setStj3Name(transientDetails.getStj3PrimaryName());
                transaction.setStj3NexusindCode(transientDetails.getStj3PrimaryNexusindCode());
                transientDetails.setStj3LocalCode(transientDetails.getStj3PrimaryLocalCode());
                transaction.setStj3TaxrateId(transWithPrimaryRates.getStj3TaxrateId());
            }
            if (transientDetails.getStj4PrimaryType() != null && transWithPrimaryRates.getStj4Rate() != null && (transWithPrimaryRates.getStj4Rate().compareTo(BigDecimal.ZERO) == 1) ) {
            /*    finalIndex++;
                stjFinalSitusCode[finalIndex] = transaction.getStj4PrimarySitusCode();
                stjFinalTxtypCd[finalIndex] = transaction.getStj4PrimaryTaxtypeCode();
                stjFinalJurId[finalIndex] = transientDetails.getStj4PrimarySitusJurId();
                stjFinalRate[finalIndex] = transWithPrimaryRates.getStj4Rate();
                stjFinalName[finalIndex] = transientDetails.getStj4PrimaryName();
                stjFinalNexusindCode[finalIndex] = transientDetails.getStj4NexusIndCode();
                stjFinalLocalCode[finalIndex] = transientDetails.getStj4PrimaryLocalCode();
                stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj4TaxrateId();*/
                transaction.setStj4SitusCode(transaction.getStj4PrimarySitusCode());
                transaction.setStj4TaxtypeUsedCode(transaction.getStj4PrimaryTaxtypeCode());
                transaction.setStj4SitusJurId(transientDetails.getStj4PrimarySitusJurId());
                transaction.setStj4Name(transientDetails.getStj4PrimaryName());
                transaction.setStj4NexusindCode(transientDetails.getStj4PrimaryNexusindCode());
                transientDetails.setStj4LocalCode(transientDetails.getStj4PrimaryLocalCode());
                transaction.setStj4TaxrateId(transWithPrimaryRates.getStj4TaxrateId());
            }
            if (transientDetails.getStj5PrimaryType() != null && transWithPrimaryRates.getStj5Rate() != null && (transWithPrimaryRates.getStj5Rate().compareTo(BigDecimal.ZERO) == 1) ) {
           /*     finalIndex++;
                stjFinalSitusCode[finalIndex] = transaction.getStj5PrimarySitusCode();
                stjFinalTxtypCd[finalIndex] = transaction.getStj5PrimaryTaxtypeCode();
                stjFinalJurId[finalIndex] = transientDetails.getStj5PrimarySitusJurId();
                stjFinalRate[finalIndex] = transWithPrimaryRates.getStj5Rate();
                stjFinalName[finalIndex] = transientDetails.getStj5PrimaryName();
                stjFinalNexusindCode[finalIndex] = transientDetails.getStj5NexusIndCode();
                stjFinalLocalCode[finalIndex] = transientDetails.getStj5PrimaryLocalCode();
                stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj5TaxrateId();*/
                transaction.setStj5SitusCode(transaction.getStj5PrimarySitusCode());
                transaction.setStj5TaxtypeUsedCode(transaction.getStj5PrimaryTaxtypeCode());
                transaction.setStj5SitusJurId(transientDetails.getStj5PrimarySitusJurId());
                transaction.setStj5Name(transientDetails.getStj5PrimaryName());
                transaction.setStj5NexusindCode(transientDetails.getStj5PrimaryNexusindCode());
                transientDetails.setStj5LocalCode(transientDetails.getStj5PrimaryLocalCode());
                transaction.setStj5TaxrateId(transWithPrimaryRates.getStj5TaxrateId());
            }

        }
        else {

            /*****************************************************************
             *
             *                    Determine Final Situs
             *
             ******************************************************************/



            transientDetails.setCountyRateSetFlag(Boolean.FALSE);
         //   transientDetails.setStateRateSetFlag(Boolean.FALSE);
            transientDetails.setCityRateSetFlag(Boolean.FALSE);
            transientDetails.setStj1RateSetFlag(Boolean.FALSE);
            transientDetails.setStj2RateSetFlag(Boolean.FALSE);
            transientDetails.setStj3RateSetFlag(Boolean.FALSE);
            transientDetails.setStj4RateSetFlag(Boolean.FALSE);
            transientDetails.setStj5RateSetFlag(Boolean.FALSE);

            HashMap<JurisdictionLevel, Long> secondarySitusIds = new HashMap<>();

            secondarySitusIds.put(JurisdictionLevel.COUNTY, transientDetails.getCountySecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.CITY, transientDetails.getCitySecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.STJ1, transientDetails.getStj1SecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.STJ2, transientDetails.getStj2SecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.STJ3, transientDetails.getStj3SecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.STJ4, transientDetails.getStj4SecondarySitusJurId());
            secondarySitusIds.put(JurisdictionLevel.STJ5, transientDetails.getStj5SecondarySitusJurId());


            HashMap<JurisdictionLevel, String> secondaryTaxTypeCodes = new HashMap<>();

            secondaryTaxTypeCodes.put(JurisdictionLevel.COUNTRY, transaction.getCountrySecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STATE, transaction.getStateSecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.COUNTY, transaction.getCountySecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.CITY, transaction.getCitySecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STJ1, transaction.getStj1SecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STJ2, transaction.getStj2SecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STJ3, transaction.getStj3SecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STJ4, transaction.getStj4SecondaryTaxtypeCode());
            secondaryTaxTypeCodes.put(JurisdictionLevel.STJ5, transaction.getStj5SecondaryTaxtypeCode());

            MicroApiTransaction transWithSecondaryRates  = fetchTaxRatesGeneric(transaction, transientDetails, secondarySitusIds, secondaryTaxTypeCodes);






            BigDecimal combinedLRPlusCitySec = transientDetails.getCombinedLocalRate()
                    .add((transWithSecondaryRates.getCityTier1Rate() != null )? transWithSecondaryRates.getCityTier1Rate() : BigDecimal.ZERO);

            if (transaction.getCitySecondarySitusCode() != null && transWithPrimaryRates.getCityTier1Rate()!=null
                    && (transWithPrimaryRates.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 0)
                    && (transWithSecondaryRates.getCityTier1Rate() != null && transWithSecondaryRates.getCityTier1Rate().compareTo(BigDecimal.ZERO) == 1)
                    && (combinedLRPlusCitySec.compareTo(transientDetails.getMaxLocalRate()) == -1 || combinedLRPlusCitySec.compareTo(transientDetails.getMaxLocalRate()) == 0) ) {

                transientDetails.setCombinedLocalRate(combinedLRPlusCitySec);

                transaction.setCitySitusCode(transaction.getCitySecondarySitusCode());
                transaction.setCityTaxtypeUsedCode(transaction.getCitySecondaryTaxtypeCode());
                transaction.setCitySitusJurId(transientDetails.getCitySecondarySitusJurId());
                transaction.setCityName(transientDetails.getCitySecondaryName());
                transaction.setCityNexusindCode(transientDetails.getCitySecondaryNexusindCode());
                transaction.setCityTaxrateId(transWithSecondaryRates.getCityTaxrateId());

            } else {
                transaction.setCitySitusCode(transaction.getCityPrimarySitusCode());
                transaction.setCityTaxtypeUsedCode(transaction.getCityPrimaryTaxtypeCode());
                transaction.setCitySitusJurId(transientDetails.getCityPrimarySitusJurId());
                transaction.setCityName(transientDetails.getCityPrimaryName());
                transaction.setCityNexusindCode(transientDetails.getCityPrimaryNexusindCode());
                transaction.setCityTaxrateId(transWithPrimaryRates.getCityTaxrateId());
            }

            BigDecimal combinedLRPlusCountySec = transientDetails.getCombinedLocalRate()
                    .add((transWithSecondaryRates.getCountyTier1Rate() != null )? transWithSecondaryRates.getCountyTier1Rate() : BigDecimal.ZERO);


            if (transaction.getCountySecondarySitusCode() != null && transWithPrimaryRates.getCountyTier1Rate() != null
                    && (transWithPrimaryRates.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 0)
                    && (transWithSecondaryRates.getCountyTier1Rate() != null && transWithSecondaryRates.getCountyTier1Rate().compareTo(BigDecimal.ZERO) == 1)
                    && (combinedLRPlusCountySec.compareTo(transientDetails.getMaxLocalRate()) == -1 || combinedLRPlusCountySec.compareTo(transientDetails.getMaxLocalRate()) == 0) ) {

                transientDetails.setCombinedLocalRate(combinedLRPlusCountySec);

                transaction.setCountySitusCode(transaction.getCountySecondarySitusCode());
                transaction.setCountyTaxtypeUsedCode(transaction.getCountySecondaryTaxtypeCode());
                transaction.setCountySitusJurId(transientDetails.getCountySecondarySitusJurId());
                transaction.setCountyName(transientDetails.getCountySecondaryName());
                transaction.setCountyNexusindCode(transientDetails.getCountySecondaryNexusindCode());
                transaction.setCountyTaxrateId(transWithSecondaryRates.getCountyTaxrateId());

            } else {

                transaction.setCountySitusCode(transaction.getCountyPrimarySitusCode());
                transaction.setCountyTaxtypeUsedCode(transaction.getCountyPrimaryTaxtypeCode());
                transaction.setCountySitusJurId(transientDetails.getCountyPrimarySitusJurId());
                transaction.setCountyName(transientDetails.getCountyPrimaryName());
                transaction.setCountyNexusindCode(transientDetails.getCountyPrimaryNexusindCode());
                transaction.setCountyTaxrateId(transWithPrimaryRates.getCountyTaxrateId());
            }



            String[] stjType = null;
            BigDecimal [] stjRate = null;
            String[] stjSitusCode = null;
            String[] stjTxtypCd = null;
            Long[] stjJurId = null;
            String[] stjName = null;
            String[] stjNexusindCode = null;
            String [] stjLocalCode = null;
            Long[] stjTaxrateId = null;

            finalIndex = -1;

            if ((transientDetails.getPrimaryMtaFlag() == null || !transientDetails.getPrimaryMtaFlag()) ||
                    (transientDetails.getPrimarySpdFlag() == null || !transientDetails.getPrimarySpdFlag())) {
                // Fill these arrays with all STJs by Effective Date

                stjType = new String[5];
                stjRate = new BigDecimal[] {BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                        BigDecimal.ZERO, BigDecimal.ZERO };
                stjSitusCode = new String[5];
                stjTxtypCd = new String[5];
                stjJurId = new Long[5];
                stjName = new String[5];
                stjNexusindCode = new String[5];
                stjLocalCode = new String[5];
                stjTaxrateId = new Long[5];

                Hashtable<String, Date> hash = new Hashtable<String, Date>();

                Date nullDate = new GregorianCalendar(1901, Calendar.JANUARY, 1).getTime();

                if(transWithSecondaryRates.getTransientTransaction().getStj1EffectiveDate() != null) {
                    hash.put("stj1", transWithSecondaryRates.getTransientTransaction().getStj1EffectiveDate());
                } else {
                    hash.put("stj1", nullDate);
                }
                if(transWithSecondaryRates.getTransientTransaction().getStj2EffectiveDate() != null) {
                    hash.put("stj2", transWithSecondaryRates.getTransientTransaction().getStj2EffectiveDate());
                } else {
                    hash.put("stj2", nullDate);
                }
                if(transWithSecondaryRates.getTransientTransaction().getStj3EffectiveDate() != null) {
                    hash.put("stj3", transWithSecondaryRates.getTransientTransaction().getStj3EffectiveDate());
                } else {
                    hash.put("stj3", nullDate);
                }
                if(transWithSecondaryRates.getTransientTransaction().getStj4EffectiveDate() != null) {
                    hash.put("stj4", transWithSecondaryRates.getTransientTransaction().getStj4EffectiveDate());
                }else {
                    hash.put("stj4", nullDate);
                }

                if(transWithSecondaryRates.getTransientTransaction().getStj5EffectiveDate() != null) {
                    hash.put("stj5", transWithSecondaryRates.getTransientTransaction().getStj5EffectiveDate());
                }else {
                    hash.put("stj5", nullDate);
                }

                List sortedList = new ArrayList(hash.entrySet());
                Collections.sort(sortedList, new StringDateComparator());
                Iterator itr = sortedList.iterator();
                int i = 0;
                while(itr.hasNext()) {
                    Map.Entry e = (Map.Entry) itr.next();

                    if("stj1".equals(e.getKey())) {
/*                        stjType[i] = td.getStj1SecdType();
                        stjRate[i] = td.getStj1SecdRate();
                        stjSitusCode[i] = td.getStj1SecdSitusCode();
                        stjTxtypCd[i] = td.getStj1SecdSitusTxtypCd();
                        stjJurId[i] = td.getStj1SecdJurId();
                        stjName[i] = td.getStj1SecdName();
                        stjNexusindCode[i] = td.getStj1SecdNexusindCode();
                        stjTaxrateId[i] = td.getStj1SecdTaxrateId();*/

                        stjType[i] = transientDetails.getStj1SecondaryType();
                        stjRate[i] = transWithSecondaryRates.getStj1Rate();
                        stjSitusCode[i] = transaction.getStj1SecondarySitusCode();
                        stjTxtypCd[i] = transaction.getStj1SecondaryTaxtypeCode();
                        stjJurId[i] = transWithSecondaryRates.getStj1SitusJurId();
                        stjName[i] = transientDetails.getStj1SecondaryName();
                        stjNexusindCode[i] = transientDetails.getStj1SecondaryNexusindCode();
                        stjLocalCode[i] = transientDetails.getStj1SecondaryLocalCode();
                        stjTaxrateId[i] = transWithSecondaryRates.getStj1TaxrateId();


                    }
                    else if("stj2".equals(e.getKey())) {
                        stjType[i] = transientDetails.getStj2SecondaryType();
                        stjRate[i] = transWithSecondaryRates.getStj2Rate();
                        stjSitusCode[i] = transaction.getStj2SecondarySitusCode();
                        stjTxtypCd[i] = transaction.getStj2SecondaryTaxtypeCode();
                        stjJurId[i] = transWithSecondaryRates.getStj2SitusJurId();
                        stjName[i] = transientDetails.getStj2SecondaryName();
                        stjNexusindCode[i] = transientDetails.getStj2SecondaryNexusindCode();
                        stjLocalCode[i] = transientDetails.getStj2SecondaryLocalCode();
                        stjTaxrateId[i] = transWithSecondaryRates.getStj2TaxrateId();
                    }
                    else if("stj3".equals(e.getKey())) {
                        stjType[i] = transientDetails.getStj3SecondaryType();
                        stjRate[i] = transWithSecondaryRates.getStj3Rate();
                        stjSitusCode[i] = transaction.getStj3SecondarySitusCode();
                        stjTxtypCd[i] = transaction.getStj3SecondaryTaxtypeCode();
                        stjJurId[i] = transWithSecondaryRates.getStj3SitusJurId();
                        stjName[i] = transientDetails.getStj3SecondaryName();
                        stjNexusindCode[i] = transientDetails.getStj3SecondaryNexusindCode();
                        stjLocalCode[i] = transientDetails.getStj3SecondaryLocalCode();
                        stjTaxrateId[i] = transWithSecondaryRates.getStj3TaxrateId();
                    }
                    else if("stj4".equals(e.getKey())) {
                        stjType[i] = transientDetails.getStj4SecondaryType();
                        stjRate[i] = transWithSecondaryRates.getStj4Rate();
                        stjSitusCode[i] = transaction.getStj4SecondarySitusCode();
                        stjTxtypCd[i] = transaction.getStj4SecondaryTaxtypeCode();
                        stjJurId[i] = transWithSecondaryRates.getStj4SitusJurId();
                        stjName[i] = transientDetails.getStj4SecondaryName();
                        stjNexusindCode[i] = transientDetails.getStj4SecondaryNexusindCode();
                        stjLocalCode[i] = transientDetails.getStj4SecondaryLocalCode();
                        stjTaxrateId[i] = transWithSecondaryRates.getStj4TaxrateId();
                    }
                    else if("stj5".equals(e.getKey())) {
                        stjType[i] = transientDetails.getStj5SecondaryType();
                        stjRate[i] = transWithSecondaryRates.getStj5Rate();
                        stjSitusCode[i] = transaction.getStj5SecondarySitusCode();
                        stjTxtypCd[i] = transaction.getStj5SecondaryTaxtypeCode();
                        stjJurId[i] = transWithSecondaryRates.getStj5SitusJurId();
                        stjName[i] = transientDetails.getStj5SecondaryName();
                        stjNexusindCode[i] = transientDetails.getStj5SecondaryNexusindCode();
                        stjLocalCode[i] = transientDetails.getStj5SecondaryLocalCode();
                        stjTaxrateId[i] = transWithSecondaryRates.getStj5TaxrateId();
                    }
                    i++;
                }
            }

            if (transientDetails.getPrimarySpdFlag() != null && transientDetails.getPrimarySpdFlag()) {
                //if ("SPD".equals(transientDetails.getStj1PrimaryType()) && transWithSecondaryRates.getStj1Rate() !=null
                //        && (transWithSecondaryRates.getStj1Rate().compareTo(BigDecimal.ZERO) != 0)) {
                //10/08/2019 for all 5 stj. Change transWithSecondaryRates to transWithPrimaryRates
            	if ("SPD".equals(transientDetails.getStj1PrimaryType()) && transWithPrimaryRates.getStj1Rate() !=null
                        && (transWithPrimaryRates.getStj1Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj1PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj1PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj1PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj1Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj1PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj1NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj1PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj1TaxrateId();

                }
                if ("SPD".equals(transientDetails.getStj2PrimaryType()) && transWithPrimaryRates.getStj2Rate() !=null
                        && (transWithPrimaryRates.getStj2Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj2PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj2PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj2PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj2Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj2PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj2NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj2PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj2TaxrateId();

                }
                if ("SPD".equals(transientDetails.getStj3PrimaryType()) && transWithPrimaryRates.getStj3Rate() !=null
                        && (transWithPrimaryRates.getStj3Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj3PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj3PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj3PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj3Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj3PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj3NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj3PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj3TaxrateId();

                }
                if ("SPD".equals(transientDetails.getStj4PrimaryType()) && transWithPrimaryRates.getStj4Rate() !=null
                        && (transWithPrimaryRates.getStj4Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj4PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj4PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj4PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj4Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj4PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj4NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj4PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj4TaxrateId();

                }
                if ("SPD".equals(transientDetails.getStj5PrimaryType()) && transWithPrimaryRates.getStj5Rate() !=null
                        && (transWithPrimaryRates.getStj5Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj5PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj5PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj5PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj5Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj5PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj5NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj5PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj5TaxrateId();
                }
            }
            else { //If the Primary does NOT have any SPD's
                for (int i = 0; i < 5; i++) {
                    if ("SPD".equals(stjType[i]) && stjRate[i] !=null && stjRate[i].compareTo(BigDecimal.ZERO) > 0
                            && transientDetails.getCombinedLocalRate().add(nullToZero(stjRate[i])).compareTo(transientDetails.getMaxLocalRate()) <= 0) {
                        finalIndex++;
                        transientDetails.setCombinedLocalRate(transientDetails.getCombinedLocalRate().add(nullToZero(stjRate[i])));
                        stjFinalSitusCode[finalIndex] = stjSitusCode[i];
                        stjFinalTxtypCd[finalIndex] = stjTxtypCd[i];
                        stjFinalJurId[finalIndex] = stjJurId[i];
                        stjFinalRate[finalIndex] = stjRate[i];
                        stjFinalName[finalIndex] = stjName[i];
                        stjFinalNexusindCode[finalIndex] = stjNexusindCode[i];
                        stjFinalLocalCode[finalIndex] = stjLocalCode[i];
                        stjFinalTaxrateId[finalIndex] = stjTaxrateId[i];

                    }
                }
            }

            //If the Primary has any MTAs then move them to Final
            if (transientDetails.getPrimaryMtaFlag() != null && transientDetails.getPrimaryMtaFlag()) {
                //if ("MTA".equals(transientDetails.getStj1PrimaryType()) && transWithSecondaryRates.getStj1Rate() !=null
                //        && (transWithSecondaryRates.getStj1Rate().compareTo(BigDecimal.ZERO) != 0)) {
            	//10/07/2019 for all 5 stj. Change transWithSecondaryRates to transWithPrimaryRates
            	if ("MTA".equals(transientDetails.getStj1PrimaryType()) && transWithPrimaryRates.getStj1Rate() !=null
                        && (transWithPrimaryRates.getStj1Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj1PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj1PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj1PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj1Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj1PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj1NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj1PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj1TaxrateId();
                }
                if ("MTA".equals(transientDetails.getStj2PrimaryType()) && transWithPrimaryRates.getStj2Rate() !=null
                        && (transWithPrimaryRates.getStj2Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj2PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj2PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj2PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj2Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj2PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj2NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj2PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj2TaxrateId();
                }
                if ("MTA".equals(transientDetails.getStj3PrimaryType()) && transWithPrimaryRates.getStj3Rate() !=null
                        && (transWithPrimaryRates.getStj3Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj3PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj3PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj3PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj3Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj3PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj3NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj3PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj3TaxrateId();
                }
                if ("MTA".equals(transientDetails.getStj4PrimaryType()) && transWithPrimaryRates.getStj4Rate() !=null
                        && (transWithPrimaryRates.getStj4Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj4PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj4PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj4PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj4Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj4PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj4NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj4PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj4TaxrateId();
                }

                if ("MTA".equals(transientDetails.getStj5PrimaryType()) && transWithPrimaryRates.getStj5Rate() !=null
                        && (transWithPrimaryRates.getStj5Rate().compareTo(BigDecimal.ZERO) != 0)) {
                    finalIndex++;
                    stjFinalSitusCode[finalIndex] = transaction.getStj5PrimarySitusCode();
                    stjFinalTxtypCd[finalIndex] = transaction.getStj5PrimaryTaxtypeCode();
                    stjFinalJurId[finalIndex] = transientDetails.getStj5PrimarySitusJurId();
                    stjFinalRate[finalIndex] = transWithPrimaryRates.getStj5Rate();
                    stjFinalName[finalIndex] = transientDetails.getStj5PrimaryName();
                    stjFinalNexusindCode[finalIndex] = transientDetails.getStj5NexusIndCode();
                    stjFinalLocalCode[finalIndex] = transientDetails.getStj5PrimaryLocalCode();
                    stjFinalTaxrateId[finalIndex] = transWithPrimaryRates.getStj5TaxrateId();
                }
            }
            else { //If the Primary does NOT have any MTA's
                for (int i = 0; i < 5; i++) {
                    if ("MTA".equals(stjType[i]) && stjRate[i] !=null && stjRate[i].compareTo(BigDecimal.ZERO) > 0
                            && transientDetails.getCombinedLocalRate().add(nullToZero(stjRate[i])).compareTo(transientDetails.getMaxLocalRate()) <= 0) {
                        finalIndex++;
                        transientDetails.setCombinedLocalRate(transientDetails.getCombinedLocalRate().add(nullToZero(stjRate[i])));
                        stjFinalSitusCode[finalIndex] = stjSitusCode[i];
                        stjFinalTxtypCd[finalIndex] = stjTxtypCd[i];
                        stjFinalJurId[finalIndex] = stjJurId[i];
                        stjFinalRate[finalIndex] = stjRate[i];
                        stjFinalName[finalIndex] = stjName[i];
                        stjFinalNexusindCode[finalIndex] = stjNexusindCode[i];
                        stjFinalLocalCode[finalIndex] = stjLocalCode[i];
                        stjFinalTaxrateId[finalIndex] = stjTaxrateId[i];
                    }
                }
            }



            transientDetails.setSitusStjFinalSitusCode(stjFinalSitusCode);
            transientDetails.setSitusStjFinalTxtypCd(stjFinalTxtypCd);
            transientDetails.setSitusStjFinalJurId(stjFinalJurId);
            transientDetails.setSitusStjFinalRate(stjFinalRate);
            transientDetails.setSitusStjFinalName(stjFinalName);
            transientDetails.setSitusStjFinalNexusindCode(stjFinalNexusindCode);
            transientDetails.setSitusStjFinalRate(stjFinalRate);

            //SAVE STJ NAMES AND RATES TO TABLE

            transaction.setStj1SitusCode(stjFinalSitusCode[0]);
            transaction.setStj2SitusCode(stjFinalSitusCode[1]);
            transaction.setStj3SitusCode(stjFinalSitusCode[2]);
            transaction.setStj4SitusCode(stjFinalSitusCode[3]);
            transaction.setStj5SitusCode(stjFinalSitusCode[4]);
            transaction.setStj6SitusCode(stjFinalSitusCode[5]);
            transaction.setStj7SitusCode(stjFinalSitusCode[6]);
            transaction.setStj8SitusCode(stjFinalSitusCode[7]);
            transaction.setStj9SitusCode(stjFinalSitusCode[8]);
            transaction.setStj10SitusCode(stjFinalSitusCode[9]);

            transaction.setStj1TaxtypeUsedCode(stjFinalTxtypCd[0]);
            transaction.setStj2TaxtypeUsedCode(stjFinalTxtypCd[1]);
            transaction.setStj3TaxtypeUsedCode(stjFinalTxtypCd[2]);
            transaction.setStj4TaxtypeUsedCode(stjFinalTxtypCd[3]);
            transaction.setStj5TaxtypeUsedCode(stjFinalTxtypCd[4]);
            transaction.setStj6TaxtypeUsedCode(stjFinalTxtypCd[5]);
            transaction.setStj7TaxtypeUsedCode(stjFinalTxtypCd[6]);
            transaction.setStj8TaxtypeUsedCode(stjFinalTxtypCd[7]);
            transaction.setStj9TaxtypeUsedCode(stjFinalTxtypCd[8]);
            transaction.setStj10TaxtypeUsedCode(stjFinalTxtypCd[9]);

            transaction.setStj1SitusJurId(stjFinalJurId[0]);
            transaction.setStj2SitusJurId(stjFinalJurId[1]);
            transaction.setStj3SitusJurId(stjFinalJurId[2]);
            transaction.setStj4SitusJurId(stjFinalJurId[3]);
            transaction.setStj5SitusJurId(stjFinalJurId[4]);
            transaction.setStj6SitusJurId(stjFinalJurId[5]);
            transaction.setStj7SitusJurId(stjFinalJurId[6]);
            transaction.setStj8SitusJurId(stjFinalJurId[7]);
            transaction.setStj9SitusJurId(stjFinalJurId[8]);
            transaction.setStj10SitusJurId(stjFinalJurId[9]);

            transaction.setStj1Name(stjFinalName[0]);
            transaction.setStj2Name(stjFinalName[1]);
            transaction.setStj3Name(stjFinalName[2]);
            transaction.setStj4Name(stjFinalName[3]);
            transaction.setStj5Name(stjFinalName[4]);
            transaction.setStj6Name(stjFinalName[5]);
            transaction.setStj7Name(stjFinalName[6]);
            transaction.setStj8Name(stjFinalName[7]);
            transaction.setStj9Name(stjFinalName[8]);
            transaction.setStj10Name(stjFinalName[9]);

            transaction.setStj1NexusindCode(stjFinalNexusindCode[0]);
            transaction.setStj2NexusindCode(stjFinalNexusindCode[1]);
            transaction.setStj3NexusindCode(stjFinalNexusindCode[2]);
            transaction.setStj4NexusindCode(stjFinalNexusindCode[3]);
            transaction.setStj5NexusindCode(stjFinalNexusindCode[4]);
            transaction.setStj6NexusindCode(stjFinalNexusindCode[5]);
            transaction.setStj7NexusindCode(stjFinalNexusindCode[6]);
            transaction.setStj8NexusindCode(stjFinalNexusindCode[7]);
            transaction.setStj9NexusindCode(stjFinalNexusindCode[8]);
            transaction.setStj10NexusindCode(stjFinalNexusindCode[9]);

            transientDetails.setStj1LocalCode(stjFinalLocalCode[0]);
            transientDetails.setStj2LocalCode(stjFinalLocalCode[1]);
            transientDetails.setStj3LocalCode(stjFinalLocalCode[2]);
            transientDetails.setStj4LocalCode(stjFinalLocalCode[3]);
            transientDetails.setStj5LocalCode(stjFinalLocalCode[4]);
            transientDetails.setStj6LocalCode(stjFinalLocalCode[5]);
            transientDetails.setStj7LocalCode(stjFinalLocalCode[6]);
            transientDetails.setStj8LocalCode(stjFinalLocalCode[7]);
            transientDetails.setStj9LocalCode(stjFinalLocalCode[8]);
            transientDetails.setStj10LocalCode(stjFinalLocalCode[9]);

            transaction.setStj1TaxrateId(stjFinalTaxrateId[0]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[1]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[2]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[3]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[4]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[5]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[6]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[7]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[8]);
            transaction.setStj1TaxrateId(stjFinalTaxrateId[9]);

        }





/*TRANS.country_situs_code = TEMP.country_primary_situs_code
TRANS.country_taxtype_used_code = TRANS.country_primary_taxtype_code
TRANS.country_situs_jur_id = TEMP.country_primary_situs_jur_id
TRANS.country_code = TEMP.country_primary_code
TRANS.country_nexusind_code = TEMP.coutry_primary_nexusind_code
TRANS.country_taxrate_id = TEMP.country_primary_taxrate_id

TRANS.state_situs_code = TEMP.state_primary_situs_code
TRANS.state_taxtype_used_code = TRANS.state_primary_taxtype_code
TRANS.state_situs_jur_id = TEMP.state_primary_situs_jur_id
TRANS.state_code = TEMP.state_primary_code
TRANS.state_nexusind_code = TEMP.state_primary_nexusind_code
TRANS.state_taxrate_id = TEMP.state_primary_taxrate_id*/

        transaction.setCountrySitusCode(transaction.getCountryPrimarySitusCode());
        transaction.setCountryTaxtypeUsedCode(transaction.getCountryPrimaryTaxtypeCode());
        transaction.setCountrySitusJurId(transientDetails.getCountryPrimarySitusJurId());
        transaction.setCountryCode(transientDetails.getCountryPrimaryCode());
        transaction.setCountryNexusindCode(transientDetails.getCountryPrimaryNexusindCode());
        transaction.setCountryTaxrateId(transWithPrimaryRates.getCountryTaxrateId());

        transaction.setStateSitusCode(transaction.getStatePrimarySitusCode());
        transaction.setStateTaxtypeUsedCode(transaction.getStatePrimaryTaxtypeCode());
        transaction.setStateSitusJurId(transientDetails.getStatePrimarySitusJurId());
        transaction.setStateCode(transientDetails.getStatePrimaryCode());
        transaction.setStateNexusindCode(transientDetails.getStatePrimaryNexusindCode());
        transaction.setStateTaxrateId(transWithPrimaryRates.getStateTaxrateId());
        
        transaction.setTransactionCountryCode(transaction.getCountryCode());
        transaction.setTransactionStateCode(transaction.getStateCode());
        
        transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transSitus.name());
    }
    
    private void populateStateSitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStateSitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStatePrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStatePrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStateSecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStateSecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStateAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCountySitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setCountySitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setCountyPrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setCountyPrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setCountySecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setCountySecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setCountyAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCitySitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setCitySitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setCityPrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setCityPrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setCitySecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setCitySecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setCityAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj1SitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj1SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj1PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj1PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj1SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj1SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj1AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj2SitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj2SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj2PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj2PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj2SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj2SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj2AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj3SitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj3SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj3PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj3PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj3SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj3SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj3AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj4SitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj4SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj4PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj4PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj4SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj4SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj4AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj5SitusMatrix(PurchaseTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj5SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj5PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj5PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj5SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj5SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj5AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCountryUseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountryTier1Rate(taxrate.getCountryUseTier1Rate());
        trans.setCountryTier2Rate(taxrate.getCountryUseTier2Rate());
        trans.setCountryTier3Rate(taxrate.getCountryUseTier3Rate());

        trans.setCountryTier1Setamt(taxrate.getCountryUseTier1Setamt());
        trans.setCountryTier2Setamt(taxrate.getCountryUseTier2Setamt());
        trans.setCountryTier3Setamt(taxrate.getCountryUseTier3Setamt());

        trans.setCountryTier1MaxAmt(taxrate.getCountryUseTier1MaxAmt());
        trans.setCountryTier2MinAmt(taxrate.getCountryUseTier2MinAmt());
        trans.setCountryTier2MaxAmt(taxrate.getCountryUseTier2MaxAmt());
        trans.setCountryMaxtaxAmt(taxrate.getCountryUseMaxtaxAmt());
    }

    private void populateCountrySaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountryTier1Rate(taxrate.getCountrySalesTier1Rate());
        trans.setCountryTier2Rate(taxrate.getCountrySalesTier2Rate());
        trans.setCountryTier3Rate(taxrate.getCountrySalesTier3Rate());

        trans.setCountryTier1Setamt(taxrate.getCountrySalesTier1Setamt());
        trans.setCountryTier2Setamt(taxrate.getCountrySalesTier2Setamt());
        trans.setCountryTier3Setamt(taxrate.getCountrySalesTier3Setamt());

        trans.setCountryTier1MaxAmt(taxrate.getCountrySalesTier1MaxAmt());
        trans.setCountryTier2MinAmt(taxrate.getCountrySalesTier2MinAmt());
        trans.setCountryTier2MaxAmt(taxrate.getCountrySalesTier2MaxAmt());
        trans.setCountryMaxtaxAmt(taxrate.getCountrySalesMaxtaxAmt());
    }

    private void populateStateUseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStateTier1Rate(taxrate.getStateUseTier1Rate());
        trans.setStateTier2Rate(taxrate.getStateUseTier2Rate());
        trans.setStateTier3Rate(taxrate.getStateUseTier3Rate());

        trans.setStateTier1Setamt(taxrate.getStateUseTier1Setamt());
        trans.setStateTier2Setamt(taxrate.getStateUseTier2Setamt());
        trans.setStateTier3Setamt(taxrate.getStateUseTier3Setamt());

        trans.setStateTier1MaxAmt(taxrate.getStateUseTier1MaxAmt());
        trans.setStateTier2MinAmt(taxrate.getStateUseTier2MinAmt());
        trans.setStateTier2MaxAmt(taxrate.getStateUseTier2MaxAmt());
        trans.setStateMaxtaxAmt(taxrate.getStateUseMaxtaxAmt());
    }

    private void populateStateSaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStateTier1Rate(taxrate.getStateSalesTier1Rate());
        trans.setStateTier2Rate(taxrate.getStateSalesTier2Rate());
        trans.setStateTier3Rate(taxrate.getStateSalesTier3Rate());

        trans.setStateTier1Setamt(taxrate.getStateSalesTier1Setamt());
        trans.setStateTier2Setamt(taxrate.getStateSalesTier2Setamt());
        trans.setStateTier3Setamt(taxrate.getStateSalesTier3Setamt());

        trans.setStateTier1MaxAmt(taxrate.getStateSalesTier1MaxAmt());
        trans.setStateTier2MinAmt(taxrate.getStateSalesTier2MinAmt());
        trans.setStateTier2MaxAmt(taxrate.getStateSalesTier2MaxAmt());
        trans.setStateMaxtaxAmt(taxrate.getStateSalesMaxtaxAmt());
    }

    private void populateCountyUseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountyTier1Rate(taxrate.getCountyUseTier1Rate());
        trans.setCountyTier2Rate(taxrate.getCountyUseTier2Rate());
        trans.setCountyTier3Rate(taxrate.getCountyUseTier3Rate());

        trans.setCountyTier1Setamt(taxrate.getCountyUseTier1Setamt());
        trans.setCountyTier2Setamt(taxrate.getCountyUseTier2Setamt());
        trans.setCountyTier3Setamt(taxrate.getCountyUseTier3Setamt());

        trans.setCountyTier1MaxAmt(taxrate.getCountyUseTier1MaxAmt());
        trans.setCountyTier2MinAmt(taxrate.getCountyUseTier2MinAmt());
        trans.setCountyTier2MaxAmt(taxrate.getCountyUseTier2MaxAmt());
        trans.setCountyMaxtaxAmt(taxrate.getCountyUseMaxtaxAmt());
    }

    private void populateCountySaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountyTier1Rate(taxrate.getCountySalesTier1Rate());
        trans.setCountyTier2Rate(taxrate.getCountySalesTier2Rate());
        trans.setCountyTier3Rate(taxrate.getCountySalesTier3Rate());

        trans.setCountyTier1Setamt(taxrate.getCountySalesTier1Setamt());
        trans.setCountyTier2Setamt(taxrate.getCountySalesTier2Setamt());
        trans.setCountyTier3Setamt(taxrate.getCountySalesTier3Setamt());

        trans.setCountyTier1MaxAmt(taxrate.getCountySalesTier1MaxAmt());
        trans.setCountyTier2MinAmt(taxrate.getCountySalesTier2MinAmt());
        trans.setCountyTier2MaxAmt(taxrate.getCountySalesTier2MaxAmt());
        trans.setCountyMaxtaxAmt(taxrate.getCountySalesMaxtaxAmt());
    }

    private void populateCityUseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCityTier1Rate(taxrate.getCityUseTier1Rate());
        trans.setCityTier2Rate(taxrate.getCityUseTier2Rate());
        trans.setCityTier3Rate(taxrate.getCityUseTier3Rate());

        trans.setCityTier1Setamt(taxrate.getCityUseTier1Setamt());
        trans.setCityTier2Setamt(taxrate.getCityUseTier2Setamt());
        trans.setCityTier3Setamt(taxrate.getCityUseTier3Setamt());

        trans.setCityTier1MaxAmt(taxrate.getCityUseTier1MaxAmt());
        trans.setCityTier2MinAmt(taxrate.getCityUseTier2MinAmt());
        trans.setCityTier2MaxAmt(taxrate.getCityUseTier2MaxAmt());
        trans.setCityMaxtaxAmt(taxrate.getCityUseMaxtaxAmt());
    }

    private void populateCitySaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCityTier1Rate(taxrate.getCitySalesTier1Rate());
        trans.setCityTier2Rate(taxrate.getCitySalesTier2Rate());
        trans.setCityTier3Rate(taxrate.getCitySalesTier3Rate());

        trans.setCityTier1Setamt(taxrate.getCitySalesTier1Setamt());
        trans.setCityTier2Setamt(taxrate.getCitySalesTier2Setamt());
        trans.setCityTier3Setamt(taxrate.getCitySalesTier3Setamt());

        trans.setCityTier1MaxAmt(taxrate.getCitySalesTier1MaxAmt());
        trans.setCityTier2MinAmt(taxrate.getCitySalesTier2MinAmt());
        trans.setCityTier2MaxAmt(taxrate.getCitySalesTier2MaxAmt());
        trans.setCityMaxtaxAmt(taxrate.getCitySalesMaxtaxAmt());
    }

    private void populateStj1UseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj1Rate(taxrate.getStj1UseRate());
        trans.setStj1Setamt(taxrate.getStj1UseSetamt());
    }

    private void populateStj1SaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj1Rate(taxrate.getStj1SalesRate());
        trans.setStj1Setamt(taxrate.getStj1SalesSetamt());
    }

    private void populateStj2UseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj2Rate(taxrate.getStj2UseRate());
        trans.setStj2Setamt(taxrate.getStj2UseSetamt());
    }

    private void populateStj2SaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj2Rate(taxrate.getStj2SalesRate());
        trans.setStj2Setamt(taxrate.getStj2SalesSetamt());
    }

    private void populateStj3UseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj3Rate(taxrate.getStj3UseRate());
        trans.setStj3Setamt(taxrate.getStj3UseSetamt());
    }

    private void populateStj3SaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj3Rate(taxrate.getStj3SalesRate());
        trans.setStj3Setamt(taxrate.getStj3SalesSetamt());
    }

    private void populateStj4UseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj4Rate(taxrate.getStj4UseRate());
        trans.setStj4Setamt(taxrate.getStj4UseSetamt());
    }

    private void populateStj4SaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj4Rate(taxrate.getStj4SalesRate());
        trans.setStj4Setamt(taxrate.getStj4SalesSetamt());
    }

    private void populateStj5UseTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj5Rate(taxrate.getStj5UseRate());
        trans.setStj5Setamt(taxrate.getStj5UseSetamt());
    }

    private void populateStj5SaleTaxRates(PurchaseTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj5Rate(taxrate.getStj5SalesRate());
        trans.setStj5Setamt(taxrate.getStj5SalesSetamt());
    }

    private void populateStateSitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStateSitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStatePrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStatePrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStateSecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStateSecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStateAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCountySitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setCountySitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setCountyPrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setCountyPrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setCountySecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setCountySecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setCountyAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCitySitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setCitySitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setCityPrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setCityPrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setCitySecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setCitySecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setCityAllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj1SitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj1SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj1PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj1PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj1SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj1SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj1AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj2SitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj2SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj2PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj2PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj2SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj2SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj2AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj3SitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj3SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj3PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj3PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj3SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj3SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj3AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj4SitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj4SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj4PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj4PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj4SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj4SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj4AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateStj5SitusMatrix(MicroApiTransaction transaction, SitusMatrix situsMatrix ) {
        transaction.setStj5SitusMatrixId(situsMatrix.getSitusMatrixId());
        transaction.setStj5PrimarySitusCode(situsMatrix.getPrimarySitusCode());
        transaction.setStj5PrimaryTaxtypeCode(situsMatrix.getPrimaryTaxtypeCode());
        transaction.setStj5SecondarySitusCode(situsMatrix.getSecondarySitusCode());
        transaction.setStj5SecondaryTaxtypeCode(situsMatrix.getSecondaryTaxtypeCode());
        transaction.setStj5AllLevelsFlag(situsMatrix.getAllLevelsFlag());
    }

    private void populateCountryUseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountryTier1Rate(taxrate.getCountryUseTier1Rate());
        trans.setCountryTier2Rate(taxrate.getCountryUseTier2Rate());
        trans.setCountryTier3Rate(taxrate.getCountryUseTier3Rate());

        trans.setCountryTier1Setamt(taxrate.getCountryUseTier1Setamt());
        trans.setCountryTier2Setamt(taxrate.getCountryUseTier2Setamt());
        trans.setCountryTier3Setamt(taxrate.getCountryUseTier3Setamt());

        trans.setCountryTier1MaxAmt(taxrate.getCountryUseTier1MaxAmt());
        trans.setCountryTier2MinAmt(taxrate.getCountryUseTier2MinAmt());
        trans.setCountryTier2MaxAmt(taxrate.getCountryUseTier2MaxAmt());
        trans.setCountryMaxtaxAmt(taxrate.getCountryUseMaxtaxAmt());
    }

    private void populateCountrySaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountryTier1Rate(taxrate.getCountrySalesTier1Rate());
        trans.setCountryTier2Rate(taxrate.getCountrySalesTier2Rate());
        trans.setCountryTier3Rate(taxrate.getCountrySalesTier3Rate());

        trans.setCountryTier1Setamt(taxrate.getCountrySalesTier1Setamt());
        trans.setCountryTier2Setamt(taxrate.getCountrySalesTier2Setamt());
        trans.setCountryTier3Setamt(taxrate.getCountrySalesTier3Setamt());

        trans.setCountryTier1MaxAmt(taxrate.getCountrySalesTier1MaxAmt());
        trans.setCountryTier2MinAmt(taxrate.getCountrySalesTier2MinAmt());
        trans.setCountryTier2MaxAmt(taxrate.getCountrySalesTier2MaxAmt());
        trans.setCountryMaxtaxAmt(taxrate.getCountrySalesMaxtaxAmt());
    }

    private void populateStateUseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStateTier1Rate(taxrate.getStateUseTier1Rate());
        trans.setStateTier2Rate(taxrate.getStateUseTier2Rate());
        trans.setStateTier3Rate(taxrate.getStateUseTier3Rate());

        trans.setStateTier1Setamt(taxrate.getStateUseTier1Setamt());
        trans.setStateTier2Setamt(taxrate.getStateUseTier2Setamt());
        trans.setStateTier3Setamt(taxrate.getStateUseTier3Setamt());

        trans.setStateTier1MaxAmt(taxrate.getStateUseTier1MaxAmt());
        trans.setStateTier2MinAmt(taxrate.getStateUseTier2MinAmt());
        trans.setStateTier2MaxAmt(taxrate.getStateUseTier2MaxAmt());
        trans.setStateMaxtaxAmt(taxrate.getStateUseMaxtaxAmt());
    }

    private void populateStateSaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStateTier1Rate(taxrate.getStateSalesTier1Rate());
        trans.setStateTier2Rate(taxrate.getStateSalesTier2Rate());
        trans.setStateTier3Rate(taxrate.getStateSalesTier3Rate());

        trans.setStateTier1Setamt(taxrate.getStateSalesTier1Setamt());
        trans.setStateTier2Setamt(taxrate.getStateSalesTier2Setamt());
        trans.setStateTier3Setamt(taxrate.getStateSalesTier3Setamt());

        trans.setStateTier1MaxAmt(taxrate.getStateSalesTier1MaxAmt());
        trans.setStateTier2MinAmt(taxrate.getStateSalesTier2MinAmt());
        trans.setStateTier2MaxAmt(taxrate.getStateSalesTier2MaxAmt());
        trans.setStateMaxtaxAmt(taxrate.getStateSalesMaxtaxAmt());
    }

    private void populateCountyUseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountyTier1Rate(taxrate.getCountyUseTier1Rate());
        trans.setCountyTier2Rate(taxrate.getCountyUseTier2Rate());
        trans.setCountyTier3Rate(taxrate.getCountyUseTier3Rate());

        trans.setCountyTier1Setamt(taxrate.getCountyUseTier1Setamt());
        trans.setCountyTier2Setamt(taxrate.getCountyUseTier2Setamt());
        trans.setCountyTier3Setamt(taxrate.getCountyUseTier3Setamt());

        trans.setCountyTier1MaxAmt(taxrate.getCountyUseTier1MaxAmt());
        trans.setCountyTier2MinAmt(taxrate.getCountyUseTier2MinAmt());
        trans.setCountyTier2MaxAmt(taxrate.getCountyUseTier2MaxAmt());
        trans.setCountyMaxtaxAmt(taxrate.getCountyUseMaxtaxAmt());
    }

    private void populateCountySaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCountyTier1Rate(taxrate.getCountySalesTier1Rate());
        trans.setCountyTier2Rate(taxrate.getCountySalesTier2Rate());
        trans.setCountyTier3Rate(taxrate.getCountySalesTier3Rate());

        trans.setCountyTier1Setamt(taxrate.getCountySalesTier1Setamt());
        trans.setCountyTier2Setamt(taxrate.getCountySalesTier2Setamt());
        trans.setCountyTier3Setamt(taxrate.getCountySalesTier3Setamt());

        trans.setCountyTier1MaxAmt(taxrate.getCountySalesTier1MaxAmt());
        trans.setCountyTier2MinAmt(taxrate.getCountySalesTier2MinAmt());
        trans.setCountyTier2MaxAmt(taxrate.getCountySalesTier2MaxAmt());
        trans.setCountyMaxtaxAmt(taxrate.getCountySalesMaxtaxAmt());
    }

    private void populateCityUseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCityTier1Rate(taxrate.getCityUseTier1Rate());
        trans.setCityTier2Rate(taxrate.getCityUseTier2Rate());
        trans.setCityTier3Rate(taxrate.getCityUseTier3Rate());

        trans.setCityTier1Setamt(taxrate.getCityUseTier1Setamt());
        trans.setCityTier2Setamt(taxrate.getCityUseTier2Setamt());
        trans.setCityTier3Setamt(taxrate.getCityUseTier3Setamt());

        trans.setCityTier1MaxAmt(taxrate.getCityUseTier1MaxAmt());
        trans.setCityTier2MinAmt(taxrate.getCityUseTier2MinAmt());
        trans.setCityTier2MaxAmt(taxrate.getCityUseTier2MaxAmt());
        trans.setCityMaxtaxAmt(taxrate.getCityUseMaxtaxAmt());
    }

    private void populateCitySaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setCityTier1Rate(taxrate.getCitySalesTier1Rate());
        trans.setCityTier2Rate(taxrate.getCitySalesTier2Rate());
        trans.setCityTier3Rate(taxrate.getCitySalesTier3Rate());

        trans.setCityTier1Setamt(taxrate.getCitySalesTier1Setamt());
        trans.setCityTier2Setamt(taxrate.getCitySalesTier2Setamt());
        trans.setCityTier3Setamt(taxrate.getCitySalesTier3Setamt());

        trans.setCityTier1MaxAmt(taxrate.getCitySalesTier1MaxAmt());
        trans.setCityTier2MinAmt(taxrate.getCitySalesTier2MinAmt());
        trans.setCityTier2MaxAmt(taxrate.getCitySalesTier2MaxAmt());
        trans.setCityMaxtaxAmt(taxrate.getCitySalesMaxtaxAmt());
    }

    private void populateStj1UseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj1Rate(taxrate.getStj1UseRate());
        trans.setStj1Setamt(taxrate.getStj1UseSetamt());
    }

    private void populateStj1SaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj1Rate(taxrate.getStj1SalesRate());
        trans.setStj1Setamt(taxrate.getStj1SalesSetamt());
    }

    private void populateStj2UseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj2Rate(taxrate.getStj2UseRate());
        trans.setStj2Setamt(taxrate.getStj2UseSetamt());
    }

    private void populateStj2SaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj2Rate(taxrate.getStj2SalesRate());
        trans.setStj2Setamt(taxrate.getStj2SalesSetamt());
    }

    private void populateStj3UseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj3Rate(taxrate.getStj3UseRate());
        trans.setStj3Setamt(taxrate.getStj3UseSetamt());
    }

    private void populateStj3SaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj3Rate(taxrate.getStj3SalesRate());
        trans.setStj3Setamt(taxrate.getStj3SalesSetamt());
    }

    private void populateStj4UseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj4Rate(taxrate.getStj4UseRate());
        trans.setStj4Setamt(taxrate.getStj4UseSetamt());
    }

    private void populateStj4SaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj4Rate(taxrate.getStj4SalesRate());
        trans.setStj4Setamt(taxrate.getStj4SalesSetamt());
    }

    private void populateStj5UseTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj5Rate(taxrate.getStj5UseRate());
        trans.setStj5Setamt(taxrate.getStj5UseSetamt());
    }

    private void populateStj5SaleTaxRates(MicroApiTransaction trans, JurisdictionTaxrate taxrate) {
        trans.setStj5Rate(taxrate.getStj5SalesRate());
        trans.setStj5Setamt(taxrate.getStj5SalesSetamt());
    }


    //Uhh...
    //TODO Implement these
    
    //WipeLocation. How does it correspond to shipTo and shipFrom? 
    //Using a matrix Id to make a check and know which one to delete.
    private void wipeLocation(MicroApiTransaction trans, Long matrixId) {
    	
    	boolean shipFrom = false;
    	boolean shipTo = false;
    	boolean ordracpt = false;
    	boolean ordrorgn = false;
    	boolean firstUse = false;
    	boolean billTo = false;
    	boolean ttlxfr = false;
    	
    	if(matrixId == null) {
    		shipFrom = true;
    		shipTo = true;
    		ordracpt = true;
    		ordrorgn = true;
    		firstUse = true;
    		billTo = true;
    		ttlxfr = true;
    	} else {
	    	if(trans.getShipfromLocnMatrixId() == matrixId)
	    		shipFrom = true;
	    	if(trans.getShiptoLocnMatrixId() == matrixId)
	    		shipTo = true;
	    	if(trans.getOrdracptLocnMatrixId() == matrixId)
	    		ordracpt = true;
	    	if(trans.getOrdrorgnLocnMatrixId() == matrixId)
	    		ordrorgn = true;
	    	if(trans.getFirstuseLocnMatrixId() == matrixId)
	    		firstUse = true;
	    	if(trans.getBilltoLocnMatrixId() == matrixId)
	    		billTo = true;
	    	if(trans.getTtlxfrLocnMatrixId() == matrixId)
	    		ttlxfr = true;
    	}
    	
    	if(shipFrom) {
    		trans.setShipfromLocnMatrixId(null);
    		trans.setShipfromJurisdictionId(null);
    	}
    	
    	if(shipTo) {
    		trans.setShiptoLocnMatrixId(null);
    		trans.setShiptoJurisdictionId(null);
    	}
    	if(ordracpt) {
    		trans.setOrdracptLocnMatrixId(null);
    		trans.setOrdracptJurisdictionId(null);
    	}
    	if(ordrorgn) {
    		trans.setOrdrorgnLocnMatrixId(null);
    		trans.setOrdrorgnJurisdictionId(null);
    	}
    	if(firstUse) {
    		trans.setFirstuseLocnMatrixId(null);
    		trans.setFirstuseJurisdictionId(null);
    	}
    	if(billTo) {
    		trans.setBilltoLocnMatrixId(null);
    		trans.setBilltoJurisdictionId(null);
    	}
    	if(ttlxfr) {
    		trans.setTtlxfrLocnMatrixId(null);
    		trans.setTtlxfrJurisdictionId(null);
    	}
    	
    }

    private void wipeTaxCode(PurchaseTransaction trans) {

    }

    private void wipeTaxCodeDetails(PurchaseTransaction trans) {

    }

    private void wipeTaxRate(PurchaseTransaction trans) {

    }
    
    private void wipeLocation(MicroApiTransaction trans) {

    }

    private void wipeTaxCode(MicroApiTransaction trans) {
    	trans.setTaxcodeCode(null);
    	trans.setTaxMatrixId(null);
    }

    private void wipeTaxCodeDetails(MicroApiTransaction trans) {
    	//trans.set
    }

    private void wipeTaxRate(MicroApiTransaction trans) {
    	trans.setRatetypeCode(null);
    }

    public RatepointService getRatepointLookupService() {
        return ratepointLookupService;
    }

    public void setRatepointLookupService(RatepointService ratepointLookupService) {
        this.ratepointLookupService = ratepointLookupService;
    }


    private TaxCodeRuleGroupBy generateGoupbyKey(PurchaseTransaction trans, String taxCode, String state, String groupByFieldName) {

        if (trans == null || taxCode == null || state == null || groupByFieldName == null)
            return null;

        Object groupByFieldValue = null;
        String groupByFieldStrValue = null;

        try {
            groupByFieldValue = FieldUtils.getFieldValue(trans, groupByFieldName);
        } catch (IllegalAccessException e) {
            logger.warn("Field "+groupByFieldName+" cannot be accessed on PurchaseTransaction. "+e.getMessage());
            return null;
        }

        if (groupByFieldValue == null)
            return null;

        groupByFieldStrValue = String.valueOf(groupByFieldValue).trim();
        return new TaxCodeRuleGroupBy(state, taxCode, groupByFieldName, groupByFieldStrValue);
    }
    
    private TaxCodeRuleGroupBy generateGoupbyKey(MicroApiTransaction trans, String taxCode, String state, String groupByFieldName) {

        if (trans == null || taxCode == null || state == null || groupByFieldName == null)
            return null;

        Object groupByFieldValue = null;
        String groupByFieldStrValue = null;

        try {
            groupByFieldValue = FieldUtils.getFieldValue(trans, groupByFieldName);
        } catch (IllegalAccessException e) {
            logger.warn("Field "+groupByFieldName+" cannot be accessed on PurchaseTransaction. "+e.getMessage());
            return null;
        }

        if (groupByFieldValue == null)
            return null;

        groupByFieldStrValue = String.valueOf(groupByFieldValue).trim();
        return new TaxCodeRuleGroupBy(state, taxCode, groupByFieldName, groupByFieldStrValue);
    }

    private void addToGroupedAndMark(PurchaseTransactionDocument document, PurchaseTransaction trans, TaxCodeDetail taxCodeDetail) {
        ///cutoff
        TransientPurchaseTransactionDocument transientDocument = document.getTransientPurchaseTransactionDocument();

        String taxCode = taxCodeDetail.getTaxcodeCode();
        String state = trans.getShiptoStateCode();
        String groupByFieldName = taxCodeDetail.getGroupByDriver();

        TaxCodeRuleGroupBy taxCodeRuleGroupBy = generateGoupbyKey(trans, taxCode, state, groupByFieldName);

        if (taxCodeRuleGroupBy != null) {
            trans.getTransientTransaction().setTaxCodeRuleGroupByKey(taxCodeRuleGroupBy);

            if (!transientDocument.getGroupedDocument().containsKey(taxCodeRuleGroupBy)) {
                ArrayList<PurchaseTransaction> grouped = new ArrayList<PurchaseTransaction>();
                grouped.add(trans);
                transientDocument.getGroupedDocument().put(taxCodeRuleGroupBy, grouped);
            } else {
                transientDocument.getGroupedDocument().get(taxCodeRuleGroupBy).add(trans);

            }
            trans.getTransientTransaction().setMarkedForGroupby(Boolean.TRUE);
        }
    }
    
    private void addToGroupedAndMark(MicroApiTransactionDocument document, MicroApiTransaction trans, TaxCodeDetail taxCodeDetail) {
        ///cutoff
        TransientMicroApiTransactionDocument transientDocument = document.getTransientMicroApiTransactionDocument();

        String taxCode = taxCodeDetail.getTaxcodeCode();
        String state = trans.getShiptoStateCode();
        String groupByFieldName = taxCodeDetail.getGroupByDriver();

        TaxCodeRuleGroupBy taxCodeRuleGroupBy = generateGoupbyKey(trans, taxCode, state, groupByFieldName);

        if (taxCodeRuleGroupBy != null) {
            trans.getTransientTransaction().setTaxCodeRuleGroupByKey(taxCodeRuleGroupBy);

            if (!transientDocument.getGroupedDocument().containsKey(taxCodeRuleGroupBy)) {
                ArrayList<MicroApiTransaction> grouped = new ArrayList<MicroApiTransaction>();
                grouped.add(trans);
                transientDocument.getGroupedDocument().put(taxCodeRuleGroupBy, grouped);
            } else {
                transientDocument.getGroupedDocument().get(taxCodeRuleGroupBy).add(trans);

            }
            trans.getTransientTransaction().setMarkedForGroupby(Boolean.TRUE);
        }
    }

    private BigDecimal addDistributionBucket(PurchaseTransaction trans) {
        return nullToZero(trans.getGlLineItmDistAmt())
                .add(nullToZero(trans.getDistr01Amt())
                .add(nullToZero(trans.getDistr02Amt())
                .add(nullToZero(trans.getDistr03Amt())
                .add(nullToZero(trans.getDistr04Amt())
                .add(nullToZero(trans.getDistr05Amt())
                .add(nullToZero(trans.getDistr06Amt())
                .add(nullToZero(trans.getDistr07Amt())
                .add(nullToZero(trans.getDistr08Amt())
                .add(nullToZero(trans.getDistr09Amt())
                .add(nullToZero(trans.getDistr10Amt())))))))))));
    }
    
    private BigDecimal addDistributionBucket(MicroApiTransaction trans) {
        return nullToZero(trans.getGlLineItmDistAmt())
                .add(nullToZero(trans.getDistr01Amt())
                .add(nullToZero(trans.getDistr02Amt())
                .add(nullToZero(trans.getDistr03Amt())
                .add(nullToZero(trans.getDistr04Amt())
                .add(nullToZero(trans.getDistr05Amt())
                .add(nullToZero(trans.getDistr06Amt())
                .add(nullToZero(trans.getDistr07Amt())
                .add(nullToZero(trans.getDistr08Amt())
                .add(nullToZero(trans.getDistr09Amt())
                .add(nullToZero(trans.getDistr10Amt())))))))))));
    }

    private void splitSubTotalGrossAmounts(PurchaseTransactionDocument document, TaxCodeRuleGroupBy groupByKey) {
        List<PurchaseTransaction> siblings = document.getTransientPurchaseTransactionDocument()
                .getGroupedDocument()
                .get(groupByKey);
        if (siblings != null && siblings.size() > 0) {
            BigDecimal subTotal = BigDecimal.ZERO;
            for (PurchaseTransaction pTrans : siblings) {
                subTotal = subTotal.add(addDistributionBucket(pTrans));
            }
            //BigDecimal subTotalGrossAmt = subTotal.divide(new BigDecimal(siblings.size()), 7, RoundingMode.HALF_EVEN);
            for (PurchaseTransaction pTrans : siblings) {
                //pTrans.getTransientTransaction().setSubTotalGrossAmt(subTotalGrossAmt);
                pTrans.getTransientTransaction().setSubTotalGrossAmt(subTotal);

                pTrans.getTransientTransaction().setSubtotalGrossDistributed(Boolean.TRUE);
            }
        }
    }
    
    private void splitSubTotalGrossAmounts(MicroApiTransactionDocument document, TaxCodeRuleGroupBy groupByKey) {
        List<MicroApiTransaction> siblings = document.getTransientMicroApiTransactionDocument()
                .getGroupedDocument()
                .get(groupByKey);
        if (siblings != null && siblings.size() > 0) {
            BigDecimal subTotal = BigDecimal.ZERO;
            for (MicroApiTransaction pTrans : siblings) {
                subTotal = subTotal.add(addDistributionBucket(pTrans));
            }
            //BigDecimal subTotalGrossAmt = subTotal.divide(new BigDecimal(siblings.size()), 7, RoundingMode.HALF_EVEN);
            for (MicroApiTransaction pTrans : siblings) {
                //pTrans.getTransientTransaction().setSubTotalGrossAmt(subTotalGrossAmt);
                pTrans.getTransientTransaction().setSubTotalGrossAmt(subTotal);

                pTrans.getTransientTransaction().setSubtotalGrossDistributed(Boolean.TRUE);
            }
        }
    }

    private void splitSubTotalTaxAmounts(List<PurchaseTransaction> siblings) {
        if (siblings!=null && siblings.size() > 0) {
            BigDecimal countrySubTotal = BigDecimal.ZERO;
            BigDecimal stateSubTotal = BigDecimal.ZERO;
            BigDecimal countySubTotal = BigDecimal.ZERO;
            BigDecimal citySubTotal = BigDecimal.ZERO;
            BigDecimal stj1SubTotal = BigDecimal.ZERO;
            BigDecimal stj2SubTotal = BigDecimal.ZERO;
            BigDecimal stj3SubTotal = BigDecimal.ZERO;
            BigDecimal stj4SubTotal = BigDecimal.ZERO;
            BigDecimal stj5SubTotal = BigDecimal.ZERO;
            BigDecimal stj6SubTotal = BigDecimal.ZERO;
            BigDecimal stj7SubTotal = BigDecimal.ZERO;
            BigDecimal stj8SubTotal = BigDecimal.ZERO;
            BigDecimal stj9SubTotal = BigDecimal.ZERO;
            BigDecimal stj10SubTotal = BigDecimal.ZERO;
            for (PurchaseTransaction trans: siblings) {
                countrySubTotal = countrySubTotal.add((trans.getCountryTier1TaxAmt() != null)? trans.getCountryTier1TaxAmt() : BigDecimal.ZERO);
                stateSubTotal = stateSubTotal.add((trans.getStateTier1TaxAmt() != null)? trans.getStateTier1TaxAmt() : BigDecimal.ZERO);
                countySubTotal = countySubTotal.add((trans.getCountyTier1TaxAmt() != null)? trans.getCountyTier1TaxAmt() : BigDecimal.ZERO);
                citySubTotal = citySubTotal.add((trans.getCityTier1TaxAmt() != null)? trans.getCityTier1TaxAmt() : BigDecimal.ZERO);
                stj1SubTotal = stj1SubTotal.add((trans.getStj1TaxAmt() != null)? trans.getStj1TaxAmt() : BigDecimal.ZERO);
                stj2SubTotal = stj2SubTotal.add((trans.getStj2TaxAmt() != null)? trans.getStj2TaxAmt() : BigDecimal.ZERO);
                stj3SubTotal = stj3SubTotal.add((trans.getStj3TaxAmt() != null)? trans.getStj3TaxAmt() : BigDecimal.ZERO);
                stj4SubTotal = stj4SubTotal.add((trans.getStj4TaxAmt() != null)? trans.getStj4TaxAmt() : BigDecimal.ZERO);
                stj5SubTotal = stj5SubTotal.add((trans.getStj5TaxAmt() != null)? trans.getStj5TaxAmt() : BigDecimal.ZERO);
                stj6SubTotal = stj6SubTotal.add((trans.getStj6TaxAmt() != null)? trans.getStj6TaxAmt() : BigDecimal.ZERO);
                stj7SubTotal = stj7SubTotal.add((trans.getStj7TaxAmt() != null)? trans.getStj7TaxAmt() : BigDecimal.ZERO);
                stj8SubTotal = stj8SubTotal.add((trans.getStj8TaxAmt() != null)? trans.getStj8TaxAmt() : BigDecimal.ZERO);
                stj9SubTotal = stj9SubTotal.add((trans.getStj9TaxAmt() != null)? trans.getStj9TaxAmt() : BigDecimal.ZERO);
                stj10SubTotal = stj10SubTotal.add((trans.getStj10TaxAmt() != null)? trans.getStj10TaxAmt() : BigDecimal.ZERO);
            }

            for (PurchaseTransaction trans: siblings) {
                TransientPurchaseTransaction transientTrans = trans.getTransientTransaction();
                transientTrans.setCountrySubTotalTaxAmt(countrySubTotal);
                transientTrans.setStateSubTotalTaxAmt(stateSubTotal);
                transientTrans.setCountySubTotalTaxAmt(countySubTotal);
                transientTrans.setCitySubTotalTaxAmt(citySubTotal);
                transientTrans.setStj1SubTotalTaxAmt(stj1SubTotal);
                transientTrans.setStj2SubTotalTaxAmt(stj2SubTotal);
                transientTrans.setStj3SubTotalTaxAmt(stj3SubTotal);
                transientTrans.setStj4SubTotalTaxAmt(stj4SubTotal);
                transientTrans.setStj5SubTotalTaxAmt(stj5SubTotal);
                transientTrans.setStj6SubTotalTaxAmt(stj6SubTotal);
                transientTrans.setStj7SubTotalTaxAmt(stj7SubTotal);
                transientTrans.setStj8SubTotalTaxAmt(stj8SubTotal);
                transientTrans.setStj9SubTotalTaxAmt(stj9SubTotal);
                transientTrans.setStj10SubTotalTaxAmt(stj10SubTotal);
            }
        }
    }
    
    private void splitSubTotalTaxAmountsM(List<MicroApiTransaction> siblings) {
        if (siblings!=null && siblings.size() > 0) {
            BigDecimal countrySubTotal = BigDecimal.ZERO;
            BigDecimal stateSubTotal = BigDecimal.ZERO;
            BigDecimal countySubTotal = BigDecimal.ZERO;
            BigDecimal citySubTotal = BigDecimal.ZERO;
            BigDecimal stj1SubTotal = BigDecimal.ZERO;
            BigDecimal stj2SubTotal = BigDecimal.ZERO;
            BigDecimal stj3SubTotal = BigDecimal.ZERO;
            BigDecimal stj4SubTotal = BigDecimal.ZERO;
            BigDecimal stj5SubTotal = BigDecimal.ZERO;
            BigDecimal stj6SubTotal = BigDecimal.ZERO;
            BigDecimal stj7SubTotal = BigDecimal.ZERO;
            BigDecimal stj8SubTotal = BigDecimal.ZERO;
            BigDecimal stj9SubTotal = BigDecimal.ZERO;
            BigDecimal stj10SubTotal = BigDecimal.ZERO;
            for (MicroApiTransaction trans: siblings) {
                countrySubTotal = countrySubTotal.add((trans.getCountryTier1TaxAmt() != null)? trans.getCountryTier1TaxAmt() : BigDecimal.ZERO);
                stateSubTotal = stateSubTotal.add((trans.getStateTier1TaxAmt() != null)? trans.getStateTier1TaxAmt() : BigDecimal.ZERO);
                countySubTotal = countySubTotal.add((trans.getCountyTier1TaxAmt() != null)? trans.getCountyTier1TaxAmt() : BigDecimal.ZERO);
                citySubTotal = citySubTotal.add((trans.getCityTier1TaxAmt() != null)? trans.getCityTier1TaxAmt() : BigDecimal.ZERO);
                stj1SubTotal = stj1SubTotal.add((trans.getStj1TaxAmt() != null)? trans.getStj1TaxAmt() : BigDecimal.ZERO);
                stj2SubTotal = stj2SubTotal.add((trans.getStj2TaxAmt() != null)? trans.getStj2TaxAmt() : BigDecimal.ZERO);
                stj3SubTotal = stj3SubTotal.add((trans.getStj3TaxAmt() != null)? trans.getStj3TaxAmt() : BigDecimal.ZERO);
                stj4SubTotal = stj4SubTotal.add((trans.getStj4TaxAmt() != null)? trans.getStj4TaxAmt() : BigDecimal.ZERO);
                stj5SubTotal = stj5SubTotal.add((trans.getStj5TaxAmt() != null)? trans.getStj5TaxAmt() : BigDecimal.ZERO);
                stj6SubTotal = stj6SubTotal.add((trans.getStj6TaxAmt() != null)? trans.getStj6TaxAmt() : BigDecimal.ZERO);
                stj7SubTotal = stj7SubTotal.add((trans.getStj7TaxAmt() != null)? trans.getStj7TaxAmt() : BigDecimal.ZERO);
                stj8SubTotal = stj8SubTotal.add((trans.getStj8TaxAmt() != null)? trans.getStj8TaxAmt() : BigDecimal.ZERO);
                stj9SubTotal = stj9SubTotal.add((trans.getStj9TaxAmt() != null)? trans.getStj9TaxAmt() : BigDecimal.ZERO);
                stj10SubTotal = stj10SubTotal.add((trans.getStj10TaxAmt() != null)? trans.getStj10TaxAmt() : BigDecimal.ZERO);
            }

            for (MicroApiTransaction trans: siblings) {
                TransientPurchaseTransaction transientTrans = trans.getTransientTransaction();
                transientTrans.setCountrySubTotalTaxAmt(countrySubTotal);
                transientTrans.setStateSubTotalTaxAmt(stateSubTotal);
                transientTrans.setCountySubTotalTaxAmt(countySubTotal);
                transientTrans.setCitySubTotalTaxAmt(citySubTotal);
                transientTrans.setStj1SubTotalTaxAmt(stj1SubTotal);
                transientTrans.setStj2SubTotalTaxAmt(stj2SubTotal);
                transientTrans.setStj3SubTotalTaxAmt(stj3SubTotal);
                transientTrans.setStj4SubTotalTaxAmt(stj4SubTotal);
                transientTrans.setStj5SubTotalTaxAmt(stj5SubTotal);
                transientTrans.setStj6SubTotalTaxAmt(stj6SubTotal);
                transientTrans.setStj7SubTotalTaxAmt(stj7SubTotal);
                transientTrans.setStj8SubTotalTaxAmt(stj8SubTotal);
                transientTrans.setStj9SubTotalTaxAmt(stj9SubTotal);
                transientTrans.setStj10SubTotalTaxAmt(stj10SubTotal);
            }
        }
    }
    
    private void prorateGroupForMaxTaxAmt(List<PurchaseTransaction> siblings) {
        for (PurchaseTransaction trans : siblings) {
            TransientPurchaseTransaction transientPurchaseTransaction  = trans.getTransientTransaction();
            if ("2".equals(trans.getCountryTaxProcessTypeCode())
                    && trans.getCountryMaximumTaxAmt() != null
                    && trans.getCountryMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getCountrySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCountrySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.COUNTRY);
            }
            if ("2".equals(trans.getStateTaxProcessTypeCode())
                    && trans.getStateMaximumTaxAmt() != null
                    && trans.getStateMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getStateSubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getStateSubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.STATE);
            }
            if ("2".equals(trans.getCountyTaxProcessTypeCode())
                    && trans.getCountyMaximumTaxAmt() != null
                    && trans.getCountyMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getCountySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCountySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.COUNTY);
            }
            if ("2".equals(trans.getCityTaxProcessTypeCode())
                    && trans.getCityMaximumTaxAmt() != null
                    && trans.getCityMaximumTaxAmt().compareTo(BigDecimal.ZERO) > 0
                    && transientPurchaseTransaction.getCitySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCitySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.CITY);
            }
            trans.setTbCalcTaxAmt(recalcTbCalcTaxAmt(trans));
        }
    }
    
    private void prorateGroupForMaxTaxAmtM(List<MicroApiTransaction> siblings) {
        for (MicroApiTransaction trans : siblings) {
            TransientPurchaseTransaction transientPurchaseTransaction  = trans.getTransientTransaction();
            if ("2".equals(trans.getCountryTaxProcessTypeCode())
                    && trans.getCountryMaximumTaxAmt() != null
                    && trans.getCountryMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getCountrySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCountrySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.COUNTRY);
            }
            if ("2".equals(trans.getStateTaxProcessTypeCode())
                    && trans.getStateMaximumTaxAmt() != null
                    && trans.getStateMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getStateSubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getStateSubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.STATE);
            }
            if ("2".equals(trans.getCountyTaxProcessTypeCode())
                    && trans.getCountyMaximumTaxAmt() != null
                    && trans.getCountyMaximumTaxAmt().compareTo(BigDecimal.ZERO) != 0
                    && transientPurchaseTransaction.getCountySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCountySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.COUNTY);
            }
            if ("2".equals(trans.getCityTaxProcessTypeCode())
                    && trans.getCityMaximumTaxAmt() != null
                    && trans.getCityMaximumTaxAmt().compareTo(BigDecimal.ZERO) > 0
                    && transientPurchaseTransaction.getCitySubTotalTaxAmt() !=null
                    && transientPurchaseTransaction.getCitySubTotalTaxAmt().compareTo(BigDecimal.ZERO) != 0) {
                prorateTransForMaxTaxAmt(trans, JurisdictionLevel.CITY);
            }
            trans.setTbCalcTaxAmt(recalcTbCalcTaxAmt(trans));
        }
    }

    private void prorateTransForMaxTaxAmt(PurchaseTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientPurchaseTransaction  = trans.getTransientTransaction();
        String vTaxProcessTypeCode = null;
        BigDecimal vMaximumTaxAmt = null;
        BigDecimal subtotalTaxAmt = null;
        switch(jurLevel) {
            case COUNTRY:
                vMaximumTaxAmt = trans.getCountryMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCountrySubTotalTaxAmt();
                break;
            case STATE:
                vMaximumTaxAmt = trans.getStateMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getStateSubTotalTaxAmt();
                break;
            case COUNTY:
                vMaximumTaxAmt = trans.getCountyMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCountySubTotalTaxAmt();
                break;
            case CITY:
                vMaximumTaxAmt = trans.getCityMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCitySubTotalTaxAmt();
                break;
            default:
                return;
        }

        if (subtotalTaxAmt != null && vMaximumTaxAmt != null
                && getTier1TaxAmt(trans, jurLevel) != null
                && subtotalTaxAmt.compareTo(vMaximumTaxAmt) > 0) {
            //TRANS.jurLevel_tier1_tax_amt = (jurLevel_maximum_tax_amt / subtotal_tax_amt) * jurLevel_tier1_taxable_amt
            BigDecimal newTaxAmt =  vMaximumTaxAmt
                    .divide(subtotalTaxAmt, 7, RoundingMode.HALF_EVEN)
                    .multiply(getTier1TaxAmt(trans, jurLevel)).setScale(2, RoundingMode.HALF_UP);
            setTier1TaxAmt(trans, jurLevel, newTaxAmt);
        }
    }
    
    private void prorateTransForMaxTaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        TransientPurchaseTransaction transientPurchaseTransaction  = trans.getTransientTransaction();
        String vTaxProcessTypeCode = null;
        BigDecimal vMaximumTaxAmt = null;
        BigDecimal subtotalTaxAmt = null;
        switch(jurLevel) {
            case COUNTRY:
                vMaximumTaxAmt = trans.getCountryMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCountrySubTotalTaxAmt();
                break;
            case STATE:
                vMaximumTaxAmt = trans.getStateMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getStateSubTotalTaxAmt();
                break;
            case COUNTY:
                vMaximumTaxAmt = trans.getCountyMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCountySubTotalTaxAmt();
                break;
            case CITY:
                vMaximumTaxAmt = trans.getCityMaximumTaxAmt();
                subtotalTaxAmt = transientPurchaseTransaction.getCitySubTotalTaxAmt();
                break;
            default:
                return;
        }

        if (subtotalTaxAmt != null && vMaximumTaxAmt != null
                && getTier1TaxAmt(trans, jurLevel) != null
                && subtotalTaxAmt.compareTo(vMaximumTaxAmt) > 0) {
            //TRANS.jurLevel_tier1_tax_amt = (jurLevel_maximum_tax_amt / subtotal_tax_amt) * jurLevel_tier1_taxable_amt
            BigDecimal newTaxAmt =  vMaximumTaxAmt
                    .divide(subtotalTaxAmt, 7, RoundingMode.HALF_EVEN)
                    .multiply(getTier1TaxAmt(trans, jurLevel)).setScale(2, RoundingMode.HALF_UP);
            setTier1TaxAmt(trans, jurLevel, newTaxAmt);
        }
    }

    private void setTier1TaxAmt(PurchaseTransaction trans, JurisdictionLevel jurLevel, BigDecimal tier1TaxAmt) {
        //trans.getCityTier1TaxAmt()
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier1TaxAmt(tier1TaxAmt);
                break;
            case STATE:
                trans.setStateTier1TaxAmt(tier1TaxAmt);
                break;
            case COUNTY:
                trans.setCountyTier1TaxAmt(tier1TaxAmt);
                break;
            case CITY:
                trans.setCityTier1TaxAmt(tier1TaxAmt);
                break;
            case STJ1:
                //nothing - there is no trans.getStj1Tier1TaxAmt()
                break;
        }
    }

    private void setTier1TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel, BigDecimal tier1TaxAmt) {
        //trans.getCityTier1TaxAmt()
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier1TaxAmt(tier1TaxAmt);
                break;
            case STATE:
                trans.setStateTier1TaxAmt(tier1TaxAmt);
                break;
            case COUNTY:
                trans.setCountyTier1TaxAmt(tier1TaxAmt);
                break;
            case CITY:
                trans.setCityTier1TaxAmt(tier1TaxAmt);
                break;
            case STJ1:
                //nothing - there is no trans.getStj1Tier1TaxAmt()
                break;
        }
    }
    
    private void setTier2TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel, BigDecimal tier2TaxAmt) {
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier2TaxAmt(tier2TaxAmt);
                break;
            case STATE:
                trans.setStateTier2TaxAmt(tier2TaxAmt);
                break;
            case COUNTY:
                trans.setCountyTier2TaxAmt(tier2TaxAmt);
                break;
            case CITY:
                trans.setCityTier2TaxAmt(tier2TaxAmt);
                break;
            case STJ1:
                //nothing - there is no trans.getStj1Tier1TaxAmt()
                break;
        }
    }
    
    private void setTier3TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel, BigDecimal tier3TaxAmt) {
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier3TaxAmt(tier3TaxAmt);
                break;
            case STATE:
                trans.setStateTier3TaxAmt(tier3TaxAmt);
                break;
            case COUNTY:
                trans.setCountyTier3TaxAmt(tier3TaxAmt);
                break;
            case CITY:
                trans.setCityTier3TaxAmt(tier3TaxAmt);
                break;
            case STJ1:
                //nothing - there is no trans.getStj1Tier1TaxAmt()
                break;
        }
    }
    
    private BigDecimal getTier1TaxAmt(PurchaseTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier1TaxAmt();
            case STATE:
                return trans.getStateTier1TaxAmt();
            case COUNTY:
                return trans.getCountyTier1TaxAmt();
            case CITY:
                return trans.getCityTier1TaxAmt();
            default:
                return null;
        }
    }
    
    private BigDecimal getTier1TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier1TaxAmt();
            case STATE:
                return trans.getStateTier1TaxAmt();
            case COUNTY:
                return trans.getCountyTier1TaxAmt();
            case CITY:
                return trans.getCityTier1TaxAmt();
            default:
                return null;
        }
    }
    
    private BigDecimal getTier2TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier2TaxAmt();
            case STATE:
                return trans.getStateTier2TaxAmt();
            case COUNTY:
                return trans.getCountyTier2TaxAmt();
            case CITY:
                return trans.getCityTier2TaxAmt();
            default:
                return null;
        }
    }
    
    private BigDecimal getTier3TaxAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier3TaxAmt();
            case STATE:
                return trans.getStateTier3TaxAmt();
            case COUNTY:
                return trans.getCountyTier3TaxAmt();
            case CITY:
                return trans.getCityTier3TaxAmt();
            default:
                return null;
        }
    }

    private void setJurLevelTier1TaxableAmt(PurchaseTransaction trans, JurisdictionLevel jurLevel, BigDecimal taxableAmt) {
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier1TaxableAmt(taxableAmt);
                break;
            case STATE:
                trans.setStateTier1TaxableAmt(taxableAmt);
                break;
            case COUNTY:
                trans.setCountyTier1TaxableAmt(taxableAmt);
                break;
            case CITY:
                trans.setCityTier1TaxableAmt(taxableAmt);
                break;
            case STJ1:
                trans.setStj1TaxableAmt(taxableAmt);
                break;
            case STJ2:
                trans.setStj2TaxableAmt(taxableAmt);
                break;
            case STJ3:
                trans.setStj3TaxableAmt(taxableAmt);
                break;
            case STJ4:
                trans.setStj4TaxableAmt(taxableAmt);
                break;
            case STJ5:
                trans.setStj5TaxableAmt(taxableAmt);
                break;
            case STJ6:
                trans.setStj6TaxableAmt(taxableAmt);
                break;
            case STJ7:
                trans.setStj7TaxableAmt(taxableAmt);
                break;
            case STJ8:
                trans.setStj8TaxableAmt(taxableAmt);
                break;
            case STJ9:
                trans.setStj9TaxableAmt(taxableAmt);
                break;
            case STJ10:
                trans.setStj10TaxableAmt(taxableAmt);
                break;
        }
    }
    
    private void setJurLevelTier1TaxableAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel, BigDecimal taxableAmt) {
        switch (jurLevel) {
            case COUNTRY:
                trans.setCountryTier1TaxableAmt(taxableAmt);
                break;
            case STATE:
                trans.setStateTier1TaxableAmt(taxableAmt);
                break;
            case COUNTY:
                trans.setCountyTier1TaxableAmt(taxableAmt);
                break;
            case CITY:
                trans.setCityTier1TaxableAmt(taxableAmt);
                break;
            case STJ1:
                trans.setStj1TaxableAmt(taxableAmt);
                break;
            case STJ2:
                trans.setStj2TaxableAmt(taxableAmt);
                break;
            case STJ3:
                trans.setStj3TaxableAmt(taxableAmt);
                break;
            case STJ4:
                trans.setStj4TaxableAmt(taxableAmt);
                break;
            case STJ5:
                trans.setStj5TaxableAmt(taxableAmt);
                break;
            case STJ6:
                trans.setStj6TaxableAmt(taxableAmt);
                break;
            case STJ7:
                trans.setStj7TaxableAmt(taxableAmt);
                break;
            case STJ8:
                trans.setStj8TaxableAmt(taxableAmt);
                break;
            case STJ9:
                trans.setStj9TaxableAmt(taxableAmt);
                break;
            case STJ10:
                trans.setStj10TaxableAmt(taxableAmt);
                break;
        }
    }

    private BigDecimal getJurlLevelTaxableAmt(PurchaseTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier1TaxableAmt();
            case STATE:
                return trans.getStateTier1TaxableAmt();
            case COUNTY:
                return trans.getCountyTier1TaxableAmt();
            case CITY:
                return trans.getCityTier1TaxableAmt();
            case STJ1:
                return trans.getStj1TaxableAmt();
            case STJ2:
                return trans.getStj2TaxableAmt();
            case STJ3:
                return trans.getStj3TaxableAmt();
            case STJ4:
                return trans.getStj4TaxableAmt();
            case STJ5:
                return trans.getStj5TaxableAmt();
            case STJ6:
                return trans.getStj6TaxableAmt();
            case STJ7:
                return trans.getStj7TaxableAmt();
            case STJ8:
                return trans.getStj8TaxableAmt();
            case STJ9:
                return trans.getStj9TaxableAmt();
            case STJ10:
                return trans.getStj10TaxableAmt();
            default:
                return null;
        }
    }
    
    private BigDecimal getJurlLevelTaxableAmt(MicroApiTransaction trans, JurisdictionLevel jurLevel) {
        switch (jurLevel) {
            case COUNTRY:
                return trans.getCountryTier1TaxableAmt();
            case STATE:
                return trans.getStateTier1TaxableAmt();
            case COUNTY:
                return trans.getCountyTier1TaxableAmt();
            case CITY:
                return trans.getCityTier1TaxableAmt();
            case STJ1:
                return trans.getStj1TaxableAmt();
            case STJ2:
                return trans.getStj2TaxableAmt();
            case STJ3:
                return trans.getStj3TaxableAmt();
            case STJ4:
                return trans.getStj4TaxableAmt();
            case STJ5:
                return trans.getStj5TaxableAmt();
            case STJ6:
                return trans.getStj6TaxableAmt();
            case STJ7:
                return trans.getStj7TaxableAmt();
            case STJ8:
                return trans.getStj8TaxableAmt();
            case STJ9:
                return trans.getStj9TaxableAmt();
            case STJ10:
                return trans.getStj10TaxableAmt();
            default:
                return null;
        }
    }

    private void zeroBigDecimalsIfNull(PurchaseTransaction trans) {
        if (trans.getInvoiceTotalAmt() == null)
            trans.setInvoiceTotalAmt(BigDecimal.ZERO);

        if (trans.getDistr01Amt() == null)
            trans.setDistr01Amt(BigDecimal.ZERO);
        if (trans.getDistr02Amt() == null)
            trans.setDistr02Amt(BigDecimal.ZERO);
        if (trans.getDistr03Amt() == null)
            trans.setDistr03Amt(BigDecimal.ZERO);
        if (trans.getDistr04Amt() == null)
            trans.setDistr04Amt(BigDecimal.ZERO);
        if (trans.getDistr05Amt() == null)
            trans.setDistr05Amt(BigDecimal.ZERO);
        if (trans.getDistr06Amt() == null)
            trans.setDistr06Amt(BigDecimal.ZERO);
        if (trans.getDistr07Amt() == null)
            trans.setDistr07Amt(BigDecimal.ZERO);
        if (trans.getDistr08Amt() == null)
            trans.setDistr08Amt(BigDecimal.ZERO);
        if (trans.getDistr09Amt() == null)
            trans.setDistr09Amt(BigDecimal.ZERO);
        if (trans.getDistr10Amt() == null)
            trans.setDistr10Amt(BigDecimal.ZERO);

        if (trans.getInvoiceLineAmt() == null)
            trans.setInvoiceLineAmt(BigDecimal.ZERO);

        if (trans.getTbCalcTaxAmt() == null)
            trans.setTbCalcTaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier1TaxAmt() == null)
            trans.setCountryTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier2TaxAmt() == null)
            trans.setCountryTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier3TaxAmt() == null)
            trans.setCountryTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier1TaxAmt() == null)
            trans.setStateTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier2TaxAmt() == null)
            trans.setStateTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier3TaxAmt() == null)
            trans.setStateTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier1TaxAmt() == null)
            trans.setCountyTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier2TaxAmt() == null)
            trans.setCountyTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier3TaxAmt() == null)
            trans.setCountyTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier1TaxAmt() == null)
            trans.setCityTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier2TaxAmt() == null)
            trans.setCityTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier3TaxAmt() == null)
            trans.setCityTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getStj1TaxAmt() == null)
            trans.setStj1TaxAmt(BigDecimal.ZERO);

        if (trans.getStj2TaxAmt() == null)
            trans.setStj2TaxAmt(BigDecimal.ZERO);

        if (trans.getStj3TaxAmt() == null)
            trans.setStj3TaxAmt(BigDecimal.ZERO);

        if (trans.getStj4TaxAmt() == null)
            trans.setStj4TaxAmt(BigDecimal.ZERO);

        if (trans.getStj5TaxAmt() == null)
            trans.setStj5TaxAmt(BigDecimal.ZERO);

        if (trans.getStj6TaxAmt() == null)
            trans.setStj6TaxAmt(BigDecimal.ZERO);

        if (trans.getStj7TaxAmt() == null)
            trans.setStj7TaxAmt(BigDecimal.ZERO);

        if (trans.getStj8TaxAmt() == null)
            trans.setStj8TaxAmt(BigDecimal.ZERO);

        if (trans.getStj9TaxAmt() == null)
            trans.setStj9TaxAmt(BigDecimal.ZERO);

        if (trans.getStj10TaxAmt() == null)
            trans.setStj10TaxAmt(BigDecimal.ZERO);

        if (trans.getGlLineItmDistAmt() == null)
            trans.setGlLineItmDistAmt(BigDecimal.ZERO);

        if (trans.getTaxableAmt() == null)
            trans.setTaxableAmt(BigDecimal.ZERO);
        if (trans.getCountryTier1TaxableAmt() == null)
            trans.setCountryTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStateTier1TaxableAmt() == null)
            trans.setStateTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getCountyTier1TaxableAmt() == null)
            trans.setCountyTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getCityTier1TaxableAmt() == null)
            trans.setCityTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj1TaxableAmt() == null)
            trans.setStj1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj2TaxableAmt() == null)
            trans.setStj2TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj3TaxableAmt() == null)
            trans.setStj3TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj4TaxableAmt() == null)
            trans.setStj4TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj5TaxableAmt() == null)
            trans.setStj5TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj6TaxableAmt() == null)
            trans.setStj6TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj7TaxableAmt() == null)
            trans.setStj7TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj8TaxableAmt() == null)
            trans.setStj8TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj9TaxableAmt() == null)
            trans.setStj9TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj10TaxableAmt() == null)
            trans.setStj10TaxableAmt(BigDecimal.ZERO);
    }
    
    private void zeroBigDecimalsIfNull(MicroApiTransaction trans) {
        if (trans.getInvoiceTotalAmt() == null)
            trans.setInvoiceTotalAmt(BigDecimal.ZERO);

        if (trans.getDistr01Amt() == null)
            trans.setDistr01Amt(BigDecimal.ZERO);
        if (trans.getDistr02Amt() == null)
            trans.setDistr02Amt(BigDecimal.ZERO);
        if (trans.getDistr03Amt() == null)
            trans.setDistr03Amt(BigDecimal.ZERO);
        if (trans.getDistr04Amt() == null)
            trans.setDistr04Amt(BigDecimal.ZERO);
        if (trans.getDistr05Amt() == null)
            trans.setDistr05Amt(BigDecimal.ZERO);
        if (trans.getDistr06Amt() == null)
            trans.setDistr06Amt(BigDecimal.ZERO);
        if (trans.getDistr07Amt() == null)
            trans.setDistr07Amt(BigDecimal.ZERO);
        if (trans.getDistr08Amt() == null)
            trans.setDistr08Amt(BigDecimal.ZERO);
        if (trans.getDistr09Amt() == null)
            trans.setDistr09Amt(BigDecimal.ZERO);
        if (trans.getDistr10Amt() == null)
            trans.setDistr10Amt(BigDecimal.ZERO);

        if (trans.getInvoiceLineAmt() == null)
            trans.setInvoiceLineAmt(BigDecimal.ZERO);

        if (trans.getTbCalcTaxAmt() == null)
            trans.setTbCalcTaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier1TaxAmt() == null)
            trans.setCountryTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier2TaxAmt() == null)
            trans.setCountryTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCountryTier3TaxAmt() == null)
            trans.setCountryTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier1TaxAmt() == null)
            trans.setStateTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier2TaxAmt() == null)
            trans.setStateTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getStateTier3TaxAmt() == null)
            trans.setStateTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier1TaxAmt() == null)
            trans.setCountyTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier2TaxAmt() == null)
            trans.setCountyTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCountyTier3TaxAmt() == null)
            trans.setCountyTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier1TaxAmt() == null)
            trans.setCityTier1TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier2TaxAmt() == null)
            trans.setCityTier2TaxAmt(BigDecimal.ZERO);

        if (trans.getCityTier3TaxAmt() == null)
            trans.setCityTier3TaxAmt(BigDecimal.ZERO);

        if (trans.getStj1TaxAmt() == null)
            trans.setStj1TaxAmt(BigDecimal.ZERO);

        if (trans.getStj2TaxAmt() == null)
            trans.setStj2TaxAmt(BigDecimal.ZERO);

        if (trans.getStj3TaxAmt() == null)
            trans.setStj3TaxAmt(BigDecimal.ZERO);

        if (trans.getStj4TaxAmt() == null)
            trans.setStj4TaxAmt(BigDecimal.ZERO);

        if (trans.getStj5TaxAmt() == null)
            trans.setStj5TaxAmt(BigDecimal.ZERO);

        if (trans.getStj6TaxAmt() == null)
            trans.setStj6TaxAmt(BigDecimal.ZERO);

        if (trans.getStj7TaxAmt() == null)
            trans.setStj7TaxAmt(BigDecimal.ZERO);

        if (trans.getStj8TaxAmt() == null)
            trans.setStj8TaxAmt(BigDecimal.ZERO);

        if (trans.getStj9TaxAmt() == null)
            trans.setStj9TaxAmt(BigDecimal.ZERO);

        if (trans.getStj10TaxAmt() == null)
            trans.setStj10TaxAmt(BigDecimal.ZERO);

        if (trans.getGlLineItmDistAmt() == null)
            trans.setGlLineItmDistAmt(BigDecimal.ZERO);

        if (trans.getTaxableAmt() == null)
            trans.setTaxableAmt(BigDecimal.ZERO);
        if (trans.getCountryTier1TaxableAmt() == null)
            trans.setCountryTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStateTier1TaxableAmt() == null)
            trans.setStateTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getCountyTier1TaxableAmt() == null)
            trans.setCountyTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getCityTier1TaxableAmt() == null)
            trans.setCityTier1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj1TaxableAmt() == null)
            trans.setStj1TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj2TaxableAmt() == null)
            trans.setStj2TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj3TaxableAmt() == null)
            trans.setStj3TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj4TaxableAmt() == null)
            trans.setStj4TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj5TaxableAmt() == null)
            trans.setStj5TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj6TaxableAmt() == null)
            trans.setStj6TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj7TaxableAmt() == null)
            trans.setStj7TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj8TaxableAmt() == null)
            trans.setStj8TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj9TaxableAmt() == null)
            trans.setStj9TaxableAmt(BigDecimal.ZERO);
        if (trans.getStj10TaxableAmt() == null)
            trans.setStj10TaxableAmt(BigDecimal.ZERO);
    }
    
    public List<Jurisdiction> getJurisdictionViaRatepoint(String country, String state, String county, String city, String zip,    		
    		String streetAddress1, String streetAddress2, String latitude, String longitude, Date glDate) 
    				throws PurchaseTransactionSuspendedException {
        
        Map<OptionCodePK, String> purchasingOptions = cacheManager.getPurchaseEngineOptions(true);

        String ratepointEnabled =  purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED);
        if (ratepointEnabled == null || !"1".equals(ratepointEnabled)){
            logger.debug("Ratepoint is not enabled. ratepointEnabled = "+ratepointEnabled);
            return null;
        }
        String ratepointServerURL = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_SERVER);
        if (ratepointServerURL == null || "".equals(ratepointServerURL)){
            logger.debug("Ratepoint server URL is not set. ratepointServerURL = "+ratepointServerURL);
            purchasingOptions.put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointUserId = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USER_ID);
        if (ratepointUserId == null || "".equals(ratepointUserId)){
            logger.debug("Ratepoint user id is not set. ratepointUserId = "+ratepointUserId);
            purchasingOptions.put(PurchaseTransactionOptions.PK_RATEPOINT_ENABLED,"0");
            return null;
        }
        String ratepointPassword = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_PASSWORD);
        if (ratepointPassword == null){
            logger.debug("Ratepoint password is not set. Setting password to a blank value.");
            ratepointPassword = "";

        }
        String latLonLookup = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USELATLONLOOKUP);
        if (latLonLookup == null || !"1".equals(latLonLookup)) {
            latLonLookup = "0";
            logger.debug("Ratepoint uselatlon lookup parameter is null or not equal 1. Setting the value to 0.");
        }
        String latLonLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_LATLONLOOKUP_URL);
        if ("".equals(latLonLookupUrl))
            latLonLookupUrl = null;

        //default "A"
        String genericJurAPI = "A";
        /*
        String genericJurAPI = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_USEGENERICJURAPI);
        if (genericJurAPI == null || "".equals(genericJurAPI)) {
            genericJurAPI = "0";
            logger.debug("Ratepoint use JurGeneric lookup parameter is null or not equal 1. Setting the value to 0.");
        } else if (!"0".equals(genericJurAPI) && !"H".equalsIgnoreCase(genericJurAPI) && !"L".equalsIgnoreCase(genericJurAPI)) {
            genericJurAPI = "H";
        }
        */
        
        String genericApiUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_GENERICJURAPI_URL);
        if ("".equals(genericApiUrl))
            genericApiUrl = null;

        String streetLevelLookupUrl = purchasingOptions.get(PurchaseTransactionOptions.PK_RATEPOINT_STREETLEVELRATE_URL);
        if ("".equals(streetLevelLookupUrl))
            streetLevelLookupUrl = null;

        List<Jurisdiction> jurisArray = null;
        String notes = "";
        Jurisdiction ratePointJur = new Jurisdiction();
        if (latLonLookup.equals("1")  && latitude != null && !latitude.equals("")  &&
        		longitude!=null && !longitude.equals("") &&
                latLonLookupUrl!=null) {
            ratePointJur = ratepointLookupService.getRateforLatLon(ratepointServerURL, latLonLookupUrl, ratepointUserId, ratepointPassword, latitude, longitude);
            if (ratePointJur!=null && ratePointJur.getGeocode()!=null) {
            	notes = "Jurisdiction Via RatePoint-Lat/Long";
            	jurisArray = new ArrayList<Jurisdiction>();
            	jurisArray.add(ratePointJur);
            }
        }

        if ((ratePointJur==null || (ratePointJur != null && ratePointJur.getGeocode()==null)) && streetLevelLookupUrl!=null && !"".equals(streetLevelLookupUrl) && streetAddress1!=null && !"".equals(streetAddress1) &&
        		zip!=null && !"".equals(zip) ) {
            ratePointJur = ratepointLookupService.getStreetLevelRate(ratepointServerURL, streetLevelLookupUrl, ratepointUserId, ratepointPassword,
            		streetAddress1, streetAddress2, city, county, state, zip, country, glDate);
            if (ratePointJur!=null && ratePointJur.getGeocode()!=null) {
            	notes = "Jurisdiction Via RatePoint-Street Level";
            	jurisArray = new ArrayList<Jurisdiction>();
            	jurisArray.add(ratePointJur);
            }
        }

        if ((ratePointJur == null || (ratePointJur != null && ratePointJur.getGeocode() == null))
                    && genericApiUrl != null && (state!= null || zip != null)) {
        	jurisArray = ratepointLookupService.getJurGeneric(ratepointServerURL, genericApiUrl, ratepointUserId, ratepointPassword, genericJurAPI,  
            		city, county, state, zip, country);
            if(jurisArray.size()>0) {
            	ratePointJur = jurisArray.get(0);
            }
            if (ratePointJur != null && ratePointJur.getGeocode() != null) {
            	notes = "Jurisdiction Via RatePoint-Generic=" + genericJurAPI.toUpperCase();
            }
        }

        //if (ratePointJur!=null && ratePointJur.getGeocode()!=null && !"".equals(ratePointJur.getGeocode())) {
        if (jurisArray!=null && jurisArray.size()>0) {
        	List<Jurisdiction> jurisReturn = new ArrayList<Jurisdiction>();
        	for(Jurisdiction jurisdiction : jurisArray) {
        		if (jurisdiction!=null && jurisdiction.getGeocode()!=null && !"".equals(jurisdiction.getGeocode())) {
		            Jurisdiction example = new Jurisdiction();
		            example.setGeocode(jurisdiction.getGeocode());
		            example.setCity(jurisdiction.getCity());
		            example.setZip(jurisdiction.getZip());
		            List<Jurisdiction> jurs = jurisdictionDAO.findByExample(example, 2, "");
		
		            if (jurs == null || jurs.size() == 0) {
		                //throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P23);
		            } else if (jurs.size() > 1) {
		                //throw  new PurchaseTransactionSuspendedException(PROCESSING_ERROR_P24);
		            } else {
		            	if(glDate == null)
		            		glDate = new Date();
		            	
		            	Jurisdiction aJurisdiction = (Jurisdiction)jurs.get(0);
		            	if(aJurisdiction.getEffectiveDate().compareTo(glDate) <= 0 && aJurisdiction.getExpirationDate().compareTo(glDate) > 0 ) {
		            		aJurisdiction.setNotes(notes);        	
			            	jurisReturn.add(aJurisdiction);	  
			            } else
			            	return null;
		            	
		            }
        		}
        	}
        	return jurisReturn;
        }
        return null;
    }

    private void enableAllCalcFlags(TransientPurchaseTransaction transientTrans) {
        transientTrans.setCountryCalcFlag(Boolean.TRUE);
        transientTrans.setStateCalcFlag(Boolean.TRUE);
        transientTrans.setCountyCalcFlag(Boolean.TRUE);
        transientTrans.setCityCalcFlag(Boolean.TRUE);
        transientTrans.setStj1CalcFlag(Boolean.TRUE);
        transientTrans.setStj2CalcFlag(Boolean.TRUE);
        transientTrans.setStj3CalcFlag(Boolean.TRUE);
        transientTrans.setStj4CalcFlag(Boolean.TRUE);
        transientTrans.setStj5CalcFlag(Boolean.TRUE);
        transientTrans.setStj6CalcFlag(Boolean.TRUE);
        transientTrans.setStj7CalcFlag(Boolean.TRUE);
        transientTrans.setStj8CalcFlag(Boolean.TRUE);
        transientTrans.setStj9CalcFlag(Boolean.TRUE);
        transientTrans.setStj10CalcFlag(Boolean.TRUE);
    }
    
    private void suspendGS(PurchaseTransaction trans){
		trans.setManualTaxcodeInd(null);
		trans.setTaxMatrixId(null);
		trans.setTaxcodeCode(null);
    }
    
    private void suspendGS(MicroApiTransaction trans){
		trans.setManualTaxcodeInd(null);
		trans.setTaxMatrixId(null);
		//trans.setTaxcodeCode(null);
    }
    
    private void suspendTaxCodeDtl(PurchaseTransaction trans){
	
		//Need to clear *_taxcode_detail_id, *_taxtype_code, *_taxcode_over_flag
    	trans.setCountryTaxcodeDetailId(null);
    	trans.setStateTaxcodeDetailId(null);
    	trans.setCountyTaxcodeDetailId(null);
    	trans.setCityTaxcodeDetailId(null);
    	trans.setStj1TaxcodeDetailId(null);
    	trans.setStj2TaxcodeDetailId(null);
    	trans.setStj3TaxcodeDetailId(null);
    	trans.setStj4TaxcodeDetailId(null);
    	trans.setStj5TaxcodeDetailId(null);
    	trans.setStj6TaxcodeDetailId(null);
    	trans.setStj7TaxcodeDetailId(null);
    	trans.setStj8TaxcodeDetailId(null);
    	trans.setStj9TaxcodeDetailId(null);
    	trans.setStj10TaxcodeDetailId(null);
    	
		trans.setCountryTaxcodeOverFlag(null);
		trans.setStateTaxcodeOverFlag(null);
		trans.setCountyTaxcodeOverFlag(null);
		trans.setCityTaxcodeOverFlag(null);
		trans.setStj1TaxcodeOverFlag(null);
		trans.setStj2TaxcodeOverFlag(null);
		trans.setStj3TaxcodeOverFlag(null);
		trans.setStj4TaxcodeOverFlag(null);
		trans.setStj5TaxcodeOverFlag(null);
		trans.setStj6TaxcodeOverFlag(null);
		trans.setStj7TaxcodeOverFlag(null);
		trans.setStj8TaxcodeOverFlag(null);
		trans.setStj9TaxcodeOverFlag(null);
		trans.setStj10TaxcodeOverFlag(null);
		trans.setCountryTaxtypeUsedCode(null);
		trans.setStateTaxtypeUsedCode(null);
		trans.setCountyTaxtypeUsedCode(null);
		trans.setCityTaxtypeUsedCode(null);
		trans.setStj1TaxtypeUsedCode(null);
		trans.setStj2TaxtypeUsedCode(null);
		trans.setStj3TaxtypeUsedCode(null);
		trans.setStj4TaxtypeUsedCode(null);
		trans.setStj5TaxtypeUsedCode(null);
		trans.setStj6TaxtypeUsedCode(null);
		trans.setStj7TaxtypeUsedCode(null);
		trans.setStj8TaxtypeUsedCode(null);
		trans.setStj9TaxtypeUsedCode(null);
		trans.setStj10TaxtypeUsedCode(null);
		trans.setCountryTaxcodeTypeCode(null);
		trans.setCountryTaxtypeCode(null);
		trans.setCountryRatetypeCode(null);
		trans.setCountryGoodSvcTypeCode(null);
		trans.setCountryCitationRef(null);
		trans.setCountryExemptReason(null);
		trans.setCountryGroupByDriver(null);
		trans.setCountryAllocBucket(null);
		trans.setCountryAllocProrateByCode(null);
		trans.setCountryAllocConditionCode(null);
		trans.setStateTaxcodeTypeCode(null);
		trans.setStateTaxtypeCode(null);
		trans.setStateRatetypeCode(null);
		trans.setStateGoodSvcTypeCode(null);
		trans.setStateCitationRef(null);
		trans.setStateExemptReason(null);
		trans.setStateGroupByDriver(null);
		trans.setStateAllocBucket(null);
		trans.setStateAllocProrateByCode(null);
		trans.setStateAllocConditionCode(null);
		trans.setCountyTaxcodeTypeCode(null);
		trans.setCountyTaxtypeCode(null);
		trans.setCountyRatetypeCode(null);
		trans.setCountyGoodSvcTypeCode(null);
		trans.setCountyCitationRef(null);
		trans.setCountyExemptReason(null);
		trans.setCountyGroupByDriver(null);
		trans.setCountyAllocBucket(null);
		trans.setCountyAllocProrateByCode(null);
		trans.setCountyAllocConditionCode(null);
		trans.setCityTaxcodeTypeCode(null);
		trans.setCityTaxtypeCode(null);
		trans.setCityRatetypeCode(null);
		trans.setCityGoodSvcTypeCode(null);
		trans.setCityCitationRef(null);
		trans.setCityExemptReason(null);
		trans.setCityGroupByDriver(null);
		trans.setCityAllocBucket(null);
		trans.setCityAllocProrateByCode(null);
		trans.setCityAllocConditionCode(null);
		trans.setStj1TaxcodeTypeCode(null);
		trans.setStj1TaxtypeCode(null);
		trans.setStj1RatetypeCode(null);
		trans.setStj1GoodSvcTypeCode(null);
		trans.setStj1CitationRef(null);
		trans.setStj1ExemptReason(null);
		trans.setStj1GroupByDriver(null);
		trans.setStj1AllocBucket(null);
		trans.setStj1AllocProrateByCode(null);
		trans.setStj1AllocConditionCode(null);
		trans.setStj2TaxcodeTypeCode(null);
		trans.setStj2TaxtypeCode(null);
		trans.setStj2RatetypeCode(null);
		trans.setStj2GoodSvcTypeCode(null);
		trans.setStj2CitationRef(null);
		trans.setStj2ExemptReason(null);
		trans.setStj2GroupByDriver(null);
		trans.setStj2AllocBucket(null);
		trans.setStj2AllocProrateByCode(null);
		trans.setStj2AllocConditionCode(null);
		trans.setStj3TaxcodeTypeCode(null);
		trans.setStj3TaxtypeCode(null);
		trans.setStj3RatetypeCode(null);
		trans.setStj3GoodSvcTypeCode(null);
		trans.setStj3CitationRef(null);
		trans.setStj3ExemptReason(null);
		trans.setStj3GroupByDriver(null);
		trans.setStj3AllocBucket(null);
		trans.setStj3AllocProrateByCode(null);
		trans.setStj3AllocConditionCode(null);
		trans.setStj4TaxcodeTypeCode(null);
		trans.setStj4TaxtypeCode(null);
		trans.setStj4RatetypeCode(null);
		trans.setStj4GoodSvcTypeCode(null);
		trans.setStj4CitationRef(null);
		trans.setStj4ExemptReason(null);
		trans.setStj4GroupByDriver(null);
		trans.setStj4AllocBucket(null);
		trans.setStj4AllocProrateByCode(null);
		trans.setStj4AllocConditionCode(null);
		trans.setStj5TaxcodeTypeCode(null);
		trans.setStj5TaxtypeCode(null);
		trans.setStj5RatetypeCode(null);
		trans.setStj5GoodSvcTypeCode(null);
		trans.setStj5CitationRef(null);
		trans.setStj5ExemptReason(null);
		trans.setStj5GroupByDriver(null);
		trans.setStj5AllocBucket(null);
		trans.setStj5AllocProrateByCode(null);
		trans.setStj5AllocConditionCode(null);
		trans.setStj6TaxcodeTypeCode(null);
		trans.setStj6TaxtypeCode(null);
		trans.setStj6RatetypeCode(null);
		trans.setStj6GoodSvcTypeCode(null);
		trans.setStj6CitationRef(null);
		trans.setStj6ExemptReason(null);
		trans.setStj6GroupByDriver(null);
		trans.setStj6AllocBucket(null);
		trans.setStj6AllocProrateByCode(null);
		trans.setStj6AllocConditionCode(null);
		trans.setStj7TaxcodeTypeCode(null);
		trans.setStj7TaxtypeCode(null);
		trans.setStj7RatetypeCode(null);
		trans.setStj7GoodSvcTypeCode(null);
		trans.setStj7CitationRef(null);
		trans.setStj7ExemptReason(null);
		trans.setStj7GroupByDriver(null);
		trans.setStj7AllocBucket(null);
		trans.setStj7AllocProrateByCode(null);
		trans.setStj7AllocConditionCode(null);
		trans.setStj8TaxcodeTypeCode(null);
		trans.setStj8TaxtypeCode(null);
		trans.setStj8RatetypeCode(null);
		trans.setStj8GoodSvcTypeCode(null);
		trans.setStj8CitationRef(null);
		trans.setStj8ExemptReason(null);
		trans.setStj8GroupByDriver(null);
		trans.setStj8AllocBucket(null);
		trans.setStj8AllocProrateByCode(null);
		trans.setStj8AllocConditionCode(null);
		trans.setStj9TaxcodeTypeCode(null);
		trans.setStj9TaxtypeCode(null);
		trans.setStj9RatetypeCode(null);
		trans.setStj9GoodSvcTypeCode(null);
		trans.setStj9CitationRef(null);
		trans.setStj9ExemptReason(null);
		trans.setStj9GroupByDriver(null);
		trans.setStj9AllocBucket(null);
		trans.setStj9AllocProrateByCode(null);
		trans.setStj9AllocConditionCode(null);
		trans.setStj10TaxcodeTypeCode(null);
		trans.setStj10TaxtypeCode(null);
		trans.setStj10RatetypeCode(null);
		trans.setStj10GoodSvcTypeCode(null);
		trans.setStj10CitationRef(null);
		trans.setStj10ExemptReason(null);
		trans.setStj10GroupByDriver(null);
		trans.setStj10AllocBucket(null);
		trans.setStj10AllocProrateByCode(null);
		trans.setStj10AllocConditionCode(null);
	}
    
    private void suspendTaxCodeDtl(MicroApiTransaction trans){
    	
		//Need to clear *_taxcode_detail_id, *_taxtype_code, *_taxcode_over_flag
    	trans.setCountryTaxcodeDetailId(null);
    	trans.setStateTaxcodeDetailId(null);
    	trans.setCountyTaxcodeDetailId(null);
    	trans.setCityTaxcodeDetailId(null);
    	trans.setStj1TaxcodeDetailId(null);
    	trans.setStj2TaxcodeDetailId(null);
    	trans.setStj3TaxcodeDetailId(null);
    	trans.setStj4TaxcodeDetailId(null);
    	trans.setStj5TaxcodeDetailId(null);
    	trans.setStj6TaxcodeDetailId(null);
    	trans.setStj7TaxcodeDetailId(null);
    	trans.setStj8TaxcodeDetailId(null);
    	trans.setStj9TaxcodeDetailId(null);
    	trans.setStj10TaxcodeDetailId(null);
    	
		trans.setCountryTaxcodeOverFlag(null);
		trans.setStateTaxcodeOverFlag(null);
		trans.setCountyTaxcodeOverFlag(null);
		trans.setCityTaxcodeOverFlag(null);
		trans.setStj1TaxcodeOverFlag(null);
		trans.setStj2TaxcodeOverFlag(null);
		trans.setStj3TaxcodeOverFlag(null);
		trans.setStj4TaxcodeOverFlag(null);
		trans.setStj5TaxcodeOverFlag(null);
		trans.setStj6TaxcodeOverFlag(null);
		trans.setStj7TaxcodeOverFlag(null);
		trans.setStj8TaxcodeOverFlag(null);
		trans.setStj9TaxcodeOverFlag(null);
		trans.setStj10TaxcodeOverFlag(null);
		trans.setCountryTaxtypeUsedCode(null);
		trans.setStateTaxtypeUsedCode(null);
		trans.setCountyTaxtypeUsedCode(null);
		trans.setCityTaxtypeUsedCode(null);
		trans.setStj1TaxtypeUsedCode(null);
		trans.setStj2TaxtypeUsedCode(null);
		trans.setStj3TaxtypeUsedCode(null);
		trans.setStj4TaxtypeUsedCode(null);
		trans.setStj5TaxtypeUsedCode(null);
		trans.setStj6TaxtypeUsedCode(null);
		trans.setStj7TaxtypeUsedCode(null);
		trans.setStj8TaxtypeUsedCode(null);
		trans.setStj9TaxtypeUsedCode(null);
		trans.setStj10TaxtypeUsedCode(null);
		trans.setCountryTaxcodeTypeCode(null);
		trans.setCountryTaxtypeCode(null);
		trans.setCountryRatetypeCode(null);
		trans.setCountryGoodSvcTypeCode(null);
		trans.setCountryCitationRef(null);
		trans.setCountryExemptReason(null);
		trans.setCountryGroupByDriver(null);
		trans.setCountryAllocBucket(null);
		trans.setCountryAllocProrateByCode(null);
		trans.setCountryAllocConditionCode(null);
		trans.setStateTaxcodeTypeCode(null);
		trans.setStateTaxtypeCode(null);
		trans.setStateRatetypeCode(null);
		trans.setStateGoodSvcTypeCode(null);
		trans.setStateCitationRef(null);
		trans.setStateExemptReason(null);
		trans.setStateGroupByDriver(null);
		trans.setStateAllocBucket(null);
		trans.setStateAllocProrateByCode(null);
		trans.setStateAllocConditionCode(null);
		trans.setCountyTaxcodeTypeCode(null);
		trans.setCountyTaxtypeCode(null);
		trans.setCountyRatetypeCode(null);
		trans.setCountyGoodSvcTypeCode(null);
		trans.setCountyCitationRef(null);
		trans.setCountyExemptReason(null);
		trans.setCountyGroupByDriver(null);
		trans.setCountyAllocBucket(null);
		trans.setCountyAllocProrateByCode(null);
		trans.setCountyAllocConditionCode(null);
		trans.setCityTaxcodeTypeCode(null);
		trans.setCityTaxtypeCode(null);
		trans.setCityRatetypeCode(null);
		trans.setCityGoodSvcTypeCode(null);
		trans.setCityCitationRef(null);
		trans.setCityExemptReason(null);
		trans.setCityGroupByDriver(null);
		trans.setCityAllocBucket(null);
		trans.setCityAllocProrateByCode(null);
		trans.setCityAllocConditionCode(null);
		trans.setStj1TaxcodeTypeCode(null);
		trans.setStj1TaxtypeCode(null);
		trans.setStj1RatetypeCode(null);
		trans.setStj1GoodSvcTypeCode(null);
		trans.setStj1CitationRef(null);
		trans.setStj1ExemptReason(null);
		trans.setStj1GroupByDriver(null);
		trans.setStj1AllocBucket(null);
		trans.setStj1AllocProrateByCode(null);
		trans.setStj1AllocConditionCode(null);
		trans.setStj2TaxcodeTypeCode(null);
		trans.setStj2TaxtypeCode(null);
		trans.setStj2RatetypeCode(null);
		trans.setStj2GoodSvcTypeCode(null);
		trans.setStj2CitationRef(null);
		trans.setStj2ExemptReason(null);
		trans.setStj2GroupByDriver(null);
		trans.setStj2AllocBucket(null);
		trans.setStj2AllocProrateByCode(null);
		trans.setStj2AllocConditionCode(null);
		trans.setStj3TaxcodeTypeCode(null);
		trans.setStj3TaxtypeCode(null);
		trans.setStj3RatetypeCode(null);
		trans.setStj3GoodSvcTypeCode(null);
		trans.setStj3CitationRef(null);
		trans.setStj3ExemptReason(null);
		trans.setStj3GroupByDriver(null);
		trans.setStj3AllocBucket(null);
		trans.setStj3AllocProrateByCode(null);
		trans.setStj3AllocConditionCode(null);
		trans.setStj4TaxcodeTypeCode(null);
		trans.setStj4TaxtypeCode(null);
		trans.setStj4RatetypeCode(null);
		trans.setStj4GoodSvcTypeCode(null);
		trans.setStj4CitationRef(null);
		trans.setStj4ExemptReason(null);
		trans.setStj4GroupByDriver(null);
		trans.setStj4AllocBucket(null);
		trans.setStj4AllocProrateByCode(null);
		trans.setStj4AllocConditionCode(null);
		trans.setStj5TaxcodeTypeCode(null);
		trans.setStj5TaxtypeCode(null);
		trans.setStj5RatetypeCode(null);
		trans.setStj5GoodSvcTypeCode(null);
		trans.setStj5CitationRef(null);
		trans.setStj5ExemptReason(null);
		trans.setStj5GroupByDriver(null);
		trans.setStj5AllocBucket(null);
		trans.setStj5AllocProrateByCode(null);
		trans.setStj5AllocConditionCode(null);
		trans.setStj6TaxcodeTypeCode(null);
		trans.setStj6TaxtypeCode(null);
		trans.setStj6RatetypeCode(null);
		trans.setStj6GoodSvcTypeCode(null);
		trans.setStj6CitationRef(null);
		trans.setStj6ExemptReason(null);
		trans.setStj6GroupByDriver(null);
		trans.setStj6AllocBucket(null);
		trans.setStj6AllocProrateByCode(null);
		trans.setStj6AllocConditionCode(null);
		trans.setStj7TaxcodeTypeCode(null);
		trans.setStj7TaxtypeCode(null);
		trans.setStj7RatetypeCode(null);
		trans.setStj7GoodSvcTypeCode(null);
		trans.setStj7CitationRef(null);
		trans.setStj7ExemptReason(null);
		trans.setStj7GroupByDriver(null);
		trans.setStj7AllocBucket(null);
		trans.setStj7AllocProrateByCode(null);
		trans.setStj7AllocConditionCode(null);
		trans.setStj8TaxcodeTypeCode(null);
		trans.setStj8TaxtypeCode(null);
		trans.setStj8RatetypeCode(null);
		trans.setStj8GoodSvcTypeCode(null);
		trans.setStj8CitationRef(null);
		trans.setStj8ExemptReason(null);
		trans.setStj8GroupByDriver(null);
		trans.setStj8AllocBucket(null);
		trans.setStj8AllocProrateByCode(null);
		trans.setStj8AllocConditionCode(null);
		trans.setStj9TaxcodeTypeCode(null);
		trans.setStj9TaxtypeCode(null);
		trans.setStj9RatetypeCode(null);
		trans.setStj9GoodSvcTypeCode(null);
		trans.setStj9CitationRef(null);
		trans.setStj9ExemptReason(null);
		trans.setStj9GroupByDriver(null);
		trans.setStj9AllocBucket(null);
		trans.setStj9AllocProrateByCode(null);
		trans.setStj9AllocConditionCode(null);
		trans.setStj10TaxcodeTypeCode(null);
		trans.setStj10TaxtypeCode(null);
		trans.setStj10RatetypeCode(null);
		trans.setStj10GoodSvcTypeCode(null);
		trans.setStj10CitationRef(null);
		trans.setStj10ExemptReason(null);
		trans.setStj10GroupByDriver(null);
		trans.setStj10AllocBucket(null);
		trans.setStj10AllocProrateByCode(null);
		trans.setStj10AllocConditionCode(null);
	}
    
    private void suspendRateFlag(PurchaseTransaction trans){
	
		//Need to clear *_taxrate_id, tb_purchtrans_rate.*
		trans.setCountryTaxrateId(null);
		trans.setStateTaxrateId(null);
		trans.setCountyTaxrateId(null);
		trans.setCityTaxrateId(null);
		trans.setStj1TaxrateId(null);
		trans.setStj2TaxrateId(null);
		trans.setStj3TaxrateId(null);
		trans.setStj4TaxrateId(null);
		trans.setStj5TaxrateId(null);
		trans.setStj6TaxrateId(null);
		trans.setStj7TaxrateId(null);
		trans.setStj8TaxrateId(null);
		trans.setStj9TaxrateId(null);
		trans.setStj10TaxrateId(null);
		trans.setCountryTier1Rate(null);
		trans.setCountryTier2Rate(null);
		trans.setCountryTier3Rate(null);
		trans.setCountryTier1Setamt(null);
		trans.setCountryTier2Setamt(null);
		trans.setCountryTier3Setamt(null);
		trans.setCountryTier1MaxAmt(null);
		trans.setCountryTier2MinAmt(null);
		trans.setCountryTier2MaxAmt(null);
		trans.setCountryTier2EntamFlag(null);
		trans.setCountryTier3EntamFlag(null);
		trans.setCountrySpecialSetamt(null);
		trans.setCountrySpecialRate(null);
		trans.setCountryMaxtaxAmt(null);
		trans.setCountryTaxableThresholdAmt(null);
		trans.setCountryMinimumTaxableAmt(null);
		trans.setCountryMaximumTaxableAmt(null);
		trans.setCountryMaximumTaxAmt(null);
		trans.setCountryBaseChangePct(null);
		trans.setCountryTaxProcessTypeCode(null);
		trans.setStateTier1Rate(null);
		trans.setStateTier2Rate(null);
		trans.setStateTier3Rate(null);
		trans.setStateTier1Setamt(null);
		trans.setStateTier2Setamt(null);
		trans.setStateTier3Setamt(null);
		trans.setStateTier1MaxAmt(null);
		trans.setStateTier2MinAmt(null);
		trans.setStateTier2MaxAmt(null);
		trans.setStateTier2EntamFlag(null);
		trans.setStateTier3EntamFlag(null);
		trans.setStateSpecialSetamt(null);
		trans.setStateSpecialRate(null);
		trans.setStateMaxtaxAmt(null);
		trans.setStateTaxableThresholdAmt(null);
		trans.setStateMinimumTaxableAmt(null);
		trans.setStateMaximumTaxableAmt(null);
		trans.setStateMaximumTaxAmt(null);
		trans.setStateBaseChangePct(null);
		trans.setStateTaxProcessTypeCode(null);
		trans.setCountyTier1Rate(null);
		trans.setCountyTier2Rate(null);
		trans.setCountyTier3Rate(null);
		trans.setCountyTier1Setamt(null);
		trans.setCountyTier2Setamt(null);
		trans.setCountyTier3Setamt(null);
		trans.setCountyTier1MaxAmt(null);
		trans.setCountyTier2MinAmt(null);
		trans.setCountyTier2MaxAmt(null);
		trans.setCountyTier2EntamFlag(null);
		trans.setCountyTier3EntamFlag(null);
		trans.setCountySpecialSetamt(null);
		trans.setCountySpecialRate(null);
		trans.setCountyMaxtaxAmt(null);
		trans.setCountyTaxableThresholdAmt(null);
		trans.setCountyMinimumTaxableAmt(null);
		trans.setCountyMaximumTaxableAmt(null);
		trans.setCountyMaximumTaxAmt(null);
		trans.setCountyBaseChangePct(null);
		trans.setCountyTaxProcessTypeCode(null);
		trans.setCityTier1Rate(null);
		trans.setCityTier2Rate(null);
		trans.setCityTier3Rate(null);
		trans.setCityTier1Setamt(null);
		trans.setCityTier2Setamt(null);
		trans.setCityTier3Setamt(null);
		trans.setCityTier1MaxAmt(null);
		trans.setCityTier2MinAmt(null);
		trans.setCityTier2MaxAmt(null);
		trans.setCityTier2EntamFlag(null);
		trans.setCityTier3EntamFlag(null);
		trans.setCitySpecialSetamt(null);
		trans.setCitySpecialRate(null);
		trans.setCityMaxtaxAmt(null);
		trans.setCityTaxableThresholdAmt(null);
		trans.setCityMinimumTaxableAmt(null);
		trans.setCityMaximumTaxableAmt(null);
		trans.setCityMaximumTaxAmt(null);
		trans.setCityBaseChangePct(null);
		trans.setCityTaxProcessTypeCode(null);
		trans.setStj1Rate(null);
		trans.setStj1Setamt(null);
		trans.setStj2Rate(null);
		trans.setStj2Setamt(null);
		trans.setStj3Rate(null);
		trans.setStj3Setamt(null);
		trans.setStj4Rate(null);
		trans.setStj4Setamt(null);
		trans.setStj5Rate(null);
		trans.setStj5Setamt(null);
		trans.setStj6Rate(null);
		trans.setStj6Setamt(null);
		trans.setStj7Rate(null);
		trans.setStj7Setamt(null);
		trans.setStj8Rate(null);
		trans.setStj8Setamt(null);
		trans.setStj9Rate(null);
		trans.setStj9Setamt(null);
		trans.setStj10Rate(null);
		trans.setStj10Setamt(null);
		trans.setCombinedRate(null);
	}
    
    private void suspendRateFlag(MicroApiTransaction trans){
    	
		//Need to clear *_taxrate_id, tb_purchtrans_rate.*
		trans.setCountryTaxrateId(null);
		trans.setStateTaxrateId(null);
		trans.setCountyTaxrateId(null);
		trans.setCityTaxrateId(null);
		trans.setStj1TaxrateId(null);
		trans.setStj2TaxrateId(null);
		trans.setStj3TaxrateId(null);
		trans.setStj4TaxrateId(null);
		trans.setStj5TaxrateId(null);
		trans.setStj6TaxrateId(null);
		trans.setStj7TaxrateId(null);
		trans.setStj8TaxrateId(null);
		trans.setStj9TaxrateId(null);
		trans.setStj10TaxrateId(null);
		trans.setCountryTier1Rate(null);
		trans.setCountryTier2Rate(null);
		trans.setCountryTier3Rate(null);
		trans.setCountryTier1Setamt(null);
		trans.setCountryTier2Setamt(null);
		trans.setCountryTier3Setamt(null);
		trans.setCountryTier1MaxAmt(null);
		trans.setCountryTier2MinAmt(null);
		trans.setCountryTier2MaxAmt(null);
		trans.setCountryTier2EntamFlag(null);
		trans.setCountryTier3EntamFlag(null);
		trans.setCountrySpecialSetamt(null);
		trans.setCountrySpecialRate(null);
		trans.setCountryMaxtaxAmt(null);
		trans.setCountryTaxableThresholdAmt(null);
		trans.setCountryMinimumTaxableAmt(null);
		trans.setCountryMaximumTaxableAmt(null);
		trans.setCountryMaximumTaxAmt(null);
		trans.setCountryBaseChangePct(null);
		trans.setCountryTaxProcessTypeCode(null);
		trans.setStateTier1Rate(null);
		trans.setStateTier2Rate(null);
		trans.setStateTier3Rate(null);
		trans.setStateTier1Setamt(null);
		trans.setStateTier2Setamt(null);
		trans.setStateTier3Setamt(null);
		trans.setStateTier1MaxAmt(null);
		trans.setStateTier2MinAmt(null);
		trans.setStateTier2MaxAmt(null);
		trans.setStateTier2EntamFlag(null);
		trans.setStateTier3EntamFlag(null);
		trans.setStateSpecialSetamt(null);
		trans.setStateSpecialRate(null);
		trans.setStateMaxtaxAmt(null);
		trans.setStateTaxableThresholdAmt(null);
		trans.setStateMinimumTaxableAmt(null);
		trans.setStateMaximumTaxableAmt(null);
		trans.setStateMaximumTaxAmt(null);
		trans.setStateBaseChangePct(null);
		trans.setStateTaxProcessTypeCode(null);
		trans.setCountyTier1Rate(null);
		trans.setCountyTier2Rate(null);
		trans.setCountyTier3Rate(null);
		trans.setCountyTier1Setamt(null);
		trans.setCountyTier2Setamt(null);
		trans.setCountyTier3Setamt(null);
		trans.setCountyTier1MaxAmt(null);
		trans.setCountyTier2MinAmt(null);
		trans.setCountyTier2MaxAmt(null);
		trans.setCountyTier2EntamFlag(null);
		trans.setCountyTier3EntamFlag(null);
		trans.setCountySpecialSetamt(null);
		trans.setCountySpecialRate(null);
		trans.setCountyMaxtaxAmt(null);
		trans.setCountyTaxableThresholdAmt(null);
		trans.setCountyMinimumTaxableAmt(null);
		trans.setCountyMaximumTaxableAmt(null);
		trans.setCountyMaximumTaxAmt(null);
		trans.setCountyBaseChangePct(null);
		trans.setCountyTaxProcessTypeCode(null);
		trans.setCityTier1Rate(null);
		trans.setCityTier2Rate(null);
		trans.setCityTier3Rate(null);
		trans.setCityTier1Setamt(null);
		trans.setCityTier2Setamt(null);
		trans.setCityTier3Setamt(null);
		trans.setCityTier1MaxAmt(null);
		trans.setCityTier2MinAmt(null);
		trans.setCityTier2MaxAmt(null);
		trans.setCityTier2EntamFlag(null);
		trans.setCityTier3EntamFlag(null);
		trans.setCitySpecialSetamt(null);
		trans.setCitySpecialRate(null);
		trans.setCityMaxtaxAmt(null);
		trans.setCityTaxableThresholdAmt(null);
		trans.setCityMinimumTaxableAmt(null);
		trans.setCityMaximumTaxableAmt(null);
		trans.setCityMaximumTaxAmt(null);
		trans.setCityBaseChangePct(null);
		trans.setCityTaxProcessTypeCode(null);
		trans.setStj1Rate(null);
		trans.setStj1Setamt(null);
		trans.setStj2Rate(null);
		trans.setStj2Setamt(null);
		trans.setStj3Rate(null);
		trans.setStj3Setamt(null);
		trans.setStj4Rate(null);
		trans.setStj4Setamt(null);
		trans.setStj5Rate(null);
		trans.setStj5Setamt(null);
		trans.setStj6Rate(null);
		trans.setStj6Setamt(null);
		trans.setStj7Rate(null);
		trans.setStj7Setamt(null);
		trans.setStj8Rate(null);
		trans.setStj8Setamt(null);
		trans.setStj9Rate(null);
		trans.setStj9Setamt(null);
		trans.setStj10Rate(null);
		trans.setStj10Setamt(null);
		trans.setCombinedRate(null);
	}
    
    private BigDecimal nullOrReverseValue(BigDecimal value,  boolean reverse){
    	if(!reverse || value==null){
    		return null;
    	}
    	
    	return ArithmeticUtils.multiply(value, ArithmeticUtils.toBigDecimal(-1.0f));		
    }
    
    public void suspAmts(PurchaseTransaction trans,  boolean reverse){
		//Need to clear tb_calc_tax_amt		
    	trans.setCountryTier1TaxableAmt(nullOrReverseValue(trans.getCountryTier1TaxableAmt(),reverse));
    	trans.setCountryTier2TaxableAmt(nullOrReverseValue(trans.getCountryTier2TaxableAmt(),reverse));
    	trans.setCountryTier3TaxableAmt(nullOrReverseValue(trans.getCountryTier3TaxableAmt(),reverse));
    	trans.setStateTier1TaxableAmt(nullOrReverseValue(trans.getStateTier1TaxableAmt(),reverse));
    	trans.setStateTier2TaxableAmt(nullOrReverseValue(trans.getStateTier2TaxableAmt(),reverse));
    	trans.setStateTier3TaxableAmt(nullOrReverseValue(trans.getStateTier3TaxableAmt(),reverse));
    	trans.setCountyTier1TaxableAmt(nullOrReverseValue(trans.getCountyTier1TaxableAmt(),reverse));
    	trans.setCountyTier2TaxableAmt(nullOrReverseValue(trans.getCountyTier2TaxableAmt(),reverse));
    	trans.setCountyTier3TaxableAmt(nullOrReverseValue(trans.getCountyTier3TaxableAmt(),reverse));
    	trans.setCityTier1TaxableAmt(nullOrReverseValue(trans.getCityTier1TaxableAmt(),reverse));
    	trans.setCityTier2TaxableAmt(nullOrReverseValue(trans.getCityTier2TaxableAmt(),reverse));
    	trans.setCityTier3TaxableAmt(nullOrReverseValue(trans.getCityTier3TaxableAmt(),reverse));
    	trans.setStj1TaxableAmt(nullOrReverseValue(trans.getStj1TaxableAmt(),reverse));
    	trans.setStj2TaxableAmt(nullOrReverseValue(trans.getStj2TaxableAmt(),reverse));
    	trans.setStj3TaxableAmt(nullOrReverseValue(trans.getStj3TaxableAmt(),reverse));
    	trans.setStj4TaxableAmt(nullOrReverseValue(trans.getStj4TaxableAmt(),reverse));
    	trans.setStj5TaxableAmt(nullOrReverseValue(trans.getStj5TaxableAmt(),reverse));
    	trans.setStj6TaxableAmt(nullOrReverseValue(trans.getStj6TaxableAmt(),reverse));
    	trans.setStj7TaxableAmt(nullOrReverseValue(trans.getStj7TaxableAmt(),reverse));
    	trans.setStj8TaxableAmt(nullOrReverseValue(trans.getStj8TaxableAmt(),reverse));
    	trans.setStj9TaxableAmt(nullOrReverseValue(trans.getStj9TaxableAmt(),reverse));
    	trans.setStj10TaxableAmt(nullOrReverseValue(trans.getStj10TaxableAmt(),reverse));
    	trans.setTbCalcTaxAmt(nullOrReverseValue(trans.getTbCalcTaxAmt(),reverse));
    	trans.setCountryTier1TaxAmt(nullOrReverseValue(trans.getCountryTier1TaxAmt(),reverse));
    	trans.setCountryTier2TaxAmt(nullOrReverseValue(trans.getCountryTier2TaxAmt(),reverse));
    	trans.setCountryTier3TaxAmt(nullOrReverseValue(trans.getCountryTier3TaxAmt(),reverse));
    	trans.setStateTier1TaxAmt(nullOrReverseValue(trans.getStateTier1TaxAmt(),reverse));
    	trans.setStateTier2TaxAmt(nullOrReverseValue(trans.getStateTier2TaxAmt(),reverse));
    	trans.setStateTier3TaxAmt(nullOrReverseValue(trans.getStateTier3TaxAmt(),reverse));
    	trans.setCountyTier1TaxAmt(nullOrReverseValue(trans.getCountyTier1TaxAmt(),reverse));
    	trans.setCountyTier2TaxAmt(nullOrReverseValue(trans.getCountyTier2TaxAmt(),reverse));
    	trans.setCountyTier3TaxAmt(nullOrReverseValue(trans.getCountyTier3TaxAmt(),reverse));
    	trans.setCityTier1TaxAmt(nullOrReverseValue(trans.getCityTier1TaxAmt(),reverse));
    	trans.setCityTier2TaxAmt(nullOrReverseValue(trans.getCityTier2TaxAmt(),reverse));
    	trans.setCityTier3TaxAmt(nullOrReverseValue(trans.getCityTier3TaxAmt(),reverse));
    	trans.setStj1TaxAmt(nullOrReverseValue(trans.getStj1TaxAmt(),reverse));
    	trans.setStj2TaxAmt(nullOrReverseValue(trans.getStj2TaxAmt(),reverse));
    	trans.setStj3TaxAmt(nullOrReverseValue(trans.getStj3TaxAmt(),reverse));
    	trans.setStj4TaxAmt(nullOrReverseValue(trans.getStj4TaxAmt(),reverse));
    	trans.setStj5TaxAmt(nullOrReverseValue(trans.getStj5TaxAmt(),reverse));
    	trans.setStj6TaxAmt(nullOrReverseValue(trans.getStj6TaxAmt(),reverse));
    	trans.setStj7TaxAmt(nullOrReverseValue(trans.getStj7TaxAmt(),reverse));
    	trans.setStj8TaxAmt(nullOrReverseValue(trans.getStj8TaxAmt(),reverse));
    	trans.setStj9TaxAmt(nullOrReverseValue(trans.getStj9TaxAmt(),reverse));
    	trans.setStj10TaxAmt(nullOrReverseValue(trans.getStj10TaxAmt(),reverse));
    	trans.setCountyLocalTaxAmt(nullOrReverseValue(trans.getCountyLocalTaxAmt(),reverse));
    	trans.setCityLocalTaxAmt(nullOrReverseValue(trans.getCityLocalTaxAmt(),reverse));
    	trans.setDistr01Amt(nullOrReverseValue(trans.getDistr01Amt(),reverse));
    	trans.setDistr02Amt(nullOrReverseValue(trans.getDistr02Amt(),reverse));
    	trans.setDistr03Amt(nullOrReverseValue(trans.getDistr03Amt(),reverse));
    	trans.setDistr04Amt(nullOrReverseValue(trans.getDistr04Amt(),reverse));
    	trans.setDistr05Amt(nullOrReverseValue(trans.getDistr05Amt(),reverse));
    	trans.setDistr06Amt(nullOrReverseValue(trans.getDistr06Amt(),reverse));
    	trans.setDistr07Amt(nullOrReverseValue(trans.getDistr07Amt(),reverse));
    	trans.setDistr08Amt(nullOrReverseValue(trans.getDistr08Amt(),reverse));
    	trans.setDistr09Amt(nullOrReverseValue(trans.getDistr09Amt(),reverse));
    	trans.setDistr10Amt(nullOrReverseValue(trans.getDistr10Amt(),reverse));
		
    	trans.setExemptAmt(nullOrReverseValue(trans.getExemptAmt(),reverse));
    	trans.setCountryExemptAmt(nullOrReverseValue(trans.getCountryExemptAmt(),reverse));
    	trans.setStateExemptAmt(nullOrReverseValue(trans.getStateExemptAmt(),reverse));
    	trans.setCountyExemptAmt(nullOrReverseValue(trans.getCountyExemptAmt(),reverse));
    	trans.setCityExemptAmt(nullOrReverseValue(trans.getCityExemptAmt(),reverse));
    	trans.setStj1ExemptAmt(nullOrReverseValue(trans.getStj1ExemptAmt(),reverse));
    	trans.setStj2ExemptAmt(nullOrReverseValue(trans.getStj2ExemptAmt(),reverse));
    	trans.setStj3ExemptAmt(nullOrReverseValue(trans.getStj3ExemptAmt(),reverse));
    	trans.setStj4ExemptAmt(nullOrReverseValue(trans.getStj4ExemptAmt(),reverse));
    	trans.setStj5ExemptAmt(nullOrReverseValue(trans.getStj5ExemptAmt(),reverse));
    	trans.setStj6ExemptAmt(nullOrReverseValue(trans.getStj6ExemptAmt(),reverse));
    	trans.setStj7ExemptAmt(nullOrReverseValue(trans.getStj7ExemptAmt(),reverse));
    	trans.setStj8ExemptAmt(nullOrReverseValue(trans.getStj8ExemptAmt(),reverse));
    	trans.setStj9ExemptAmt(nullOrReverseValue(trans.getStj9ExemptAmt(),reverse));
    	trans.setStj10ExemptAmt(nullOrReverseValue(trans.getStj10ExemptAmt(),reverse));
    	trans.setExclusionAmt(nullOrReverseValue(trans.getExclusionAmt(),reverse));
    	trans.setTaxableAmt(nullOrReverseValue(trans.getTaxableAmt(), reverse));
	}
    
    public void suspAmts(MicroApiTransaction trans,  boolean reverse){
		//Need to clear tb_calc_tax_amt		
    	trans.setCountryTier1TaxableAmt(nullOrReverseValue(trans.getCountryTier1TaxableAmt(),reverse));
    	trans.setCountryTier2TaxableAmt(nullOrReverseValue(trans.getCountryTier2TaxableAmt(),reverse));
    	trans.setCountryTier3TaxableAmt(nullOrReverseValue(trans.getCountryTier3TaxableAmt(),reverse));
    	trans.setStateTier1TaxableAmt(nullOrReverseValue(trans.getStateTier1TaxableAmt(),reverse));
    	trans.setStateTier2TaxableAmt(nullOrReverseValue(trans.getStateTier2TaxableAmt(),reverse));
    	trans.setStateTier3TaxableAmt(nullOrReverseValue(trans.getStateTier3TaxableAmt(),reverse));
    	trans.setCountyTier1TaxableAmt(nullOrReverseValue(trans.getCountyTier1TaxableAmt(),reverse));
    	trans.setCountyTier2TaxableAmt(nullOrReverseValue(trans.getCountyTier2TaxableAmt(),reverse));
    	trans.setCountyTier3TaxableAmt(nullOrReverseValue(trans.getCountyTier3TaxableAmt(),reverse));
    	trans.setCityTier1TaxableAmt(nullOrReverseValue(trans.getCityTier1TaxableAmt(),reverse));
    	trans.setCityTier2TaxableAmt(nullOrReverseValue(trans.getCityTier2TaxableAmt(),reverse));
    	trans.setCityTier3TaxableAmt(nullOrReverseValue(trans.getCityTier3TaxableAmt(),reverse));
    	trans.setStj1TaxableAmt(nullOrReverseValue(trans.getStj1TaxableAmt(),reverse));
    	trans.setStj2TaxableAmt(nullOrReverseValue(trans.getStj2TaxableAmt(),reverse));
    	trans.setStj3TaxableAmt(nullOrReverseValue(trans.getStj3TaxableAmt(),reverse));
    	trans.setStj4TaxableAmt(nullOrReverseValue(trans.getStj4TaxableAmt(),reverse));
    	trans.setStj5TaxableAmt(nullOrReverseValue(trans.getStj5TaxableAmt(),reverse));
    	trans.setStj6TaxableAmt(nullOrReverseValue(trans.getStj6TaxableAmt(),reverse));
    	trans.setStj7TaxableAmt(nullOrReverseValue(trans.getStj7TaxableAmt(),reverse));
    	trans.setStj8TaxableAmt(nullOrReverseValue(trans.getStj8TaxableAmt(),reverse));
    	trans.setStj9TaxableAmt(nullOrReverseValue(trans.getStj9TaxableAmt(),reverse));
    	trans.setStj10TaxableAmt(nullOrReverseValue(trans.getStj10TaxableAmt(),reverse));
    	trans.setTbCalcTaxAmt(nullOrReverseValue(trans.getTbCalcTaxAmt(),reverse));
    	trans.setCountryTier1TaxAmt(nullOrReverseValue(trans.getCountryTier1TaxAmt(),reverse));
    	trans.setCountryTier2TaxAmt(nullOrReverseValue(trans.getCountryTier2TaxAmt(),reverse));
    	trans.setCountryTier3TaxAmt(nullOrReverseValue(trans.getCountryTier3TaxAmt(),reverse));
    	trans.setStateTier1TaxAmt(nullOrReverseValue(trans.getStateTier1TaxAmt(),reverse));
    	trans.setStateTier2TaxAmt(nullOrReverseValue(trans.getStateTier2TaxAmt(),reverse));
    	trans.setStateTier3TaxAmt(nullOrReverseValue(trans.getStateTier3TaxAmt(),reverse));
    	trans.setCountyTier1TaxAmt(nullOrReverseValue(trans.getCountyTier1TaxAmt(),reverse));
    	trans.setCountyTier2TaxAmt(nullOrReverseValue(trans.getCountyTier2TaxAmt(),reverse));
    	trans.setCountyTier3TaxAmt(nullOrReverseValue(trans.getCountyTier3TaxAmt(),reverse));
    	trans.setCityTier1TaxAmt(nullOrReverseValue(trans.getCityTier1TaxAmt(),reverse));
    	trans.setCityTier2TaxAmt(nullOrReverseValue(trans.getCityTier2TaxAmt(),reverse));
    	trans.setCityTier3TaxAmt(nullOrReverseValue(trans.getCityTier3TaxAmt(),reverse));
    	trans.setStj1TaxAmt(nullOrReverseValue(trans.getStj1TaxAmt(),reverse));
    	trans.setStj2TaxAmt(nullOrReverseValue(trans.getStj2TaxAmt(),reverse));
    	trans.setStj3TaxAmt(nullOrReverseValue(trans.getStj3TaxAmt(),reverse));
    	trans.setStj4TaxAmt(nullOrReverseValue(trans.getStj4TaxAmt(),reverse));
    	trans.setStj5TaxAmt(nullOrReverseValue(trans.getStj5TaxAmt(),reverse));
    	trans.setStj6TaxAmt(nullOrReverseValue(trans.getStj6TaxAmt(),reverse));
    	trans.setStj7TaxAmt(nullOrReverseValue(trans.getStj7TaxAmt(),reverse));
    	trans.setStj8TaxAmt(nullOrReverseValue(trans.getStj8TaxAmt(),reverse));
    	trans.setStj9TaxAmt(nullOrReverseValue(trans.getStj9TaxAmt(),reverse));
    	trans.setStj10TaxAmt(nullOrReverseValue(trans.getStj10TaxAmt(),reverse));
    	trans.setCountyLocalTaxAmt(nullOrReverseValue(trans.getCountyLocalTaxAmt(),reverse));
    	trans.setCityLocalTaxAmt(nullOrReverseValue(trans.getCityLocalTaxAmt(),reverse));
    	trans.setDistr01Amt(nullOrReverseValue(trans.getDistr01Amt(),reverse));
    	trans.setDistr02Amt(nullOrReverseValue(trans.getDistr02Amt(),reverse));
    	trans.setDistr03Amt(nullOrReverseValue(trans.getDistr03Amt(),reverse));
    	trans.setDistr04Amt(nullOrReverseValue(trans.getDistr04Amt(),reverse));
    	trans.setDistr05Amt(nullOrReverseValue(trans.getDistr05Amt(),reverse));
    	trans.setDistr06Amt(nullOrReverseValue(trans.getDistr06Amt(),reverse));
    	trans.setDistr07Amt(nullOrReverseValue(trans.getDistr07Amt(),reverse));
    	trans.setDistr08Amt(nullOrReverseValue(trans.getDistr08Amt(),reverse));
    	trans.setDistr09Amt(nullOrReverseValue(trans.getDistr09Amt(),reverse));
    	trans.setDistr10Amt(nullOrReverseValue(trans.getDistr10Amt(),reverse));
		
    	trans.setExemptAmt(nullOrReverseValue(trans.getExemptAmt(),reverse));
    	trans.setCountryExemptAmt(nullOrReverseValue(trans.getCountryExemptAmt(),reverse));
    	trans.setStateExemptAmt(nullOrReverseValue(trans.getStateExemptAmt(),reverse));
    	trans.setCountyExemptAmt(nullOrReverseValue(trans.getCountyExemptAmt(),reverse));
    	trans.setCityExemptAmt(nullOrReverseValue(trans.getCityExemptAmt(),reverse));
    	trans.setStj1ExemptAmt(nullOrReverseValue(trans.getStj1ExemptAmt(),reverse));
    	trans.setStj2ExemptAmt(nullOrReverseValue(trans.getStj2ExemptAmt(),reverse));
    	trans.setStj3ExemptAmt(nullOrReverseValue(trans.getStj3ExemptAmt(),reverse));
    	trans.setStj4ExemptAmt(nullOrReverseValue(trans.getStj4ExemptAmt(),reverse));
    	trans.setStj5ExemptAmt(nullOrReverseValue(trans.getStj5ExemptAmt(),reverse));
    	trans.setStj6ExemptAmt(nullOrReverseValue(trans.getStj6ExemptAmt(),reverse));
    	trans.setStj7ExemptAmt(nullOrReverseValue(trans.getStj7ExemptAmt(),reverse));
    	trans.setStj8ExemptAmt(nullOrReverseValue(trans.getStj8ExemptAmt(),reverse));
    	trans.setStj9ExemptAmt(nullOrReverseValue(trans.getStj9ExemptAmt(),reverse));
    	trans.setStj10ExemptAmt(nullOrReverseValue(trans.getStj10ExemptAmt(),reverse));
    	trans.setExclusionAmt(nullOrReverseValue(trans.getExclusionAmt(),reverse));
    	trans.setTaxableAmt(nullOrReverseValue(trans.getTaxableAmt(), reverse));

	}
    
    // Copy from rom public void getTransVendorNexus(MicroApiTransaction trans)
    public void getTransEntityNexus(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {

    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
        transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transEntityNexus.name());

    	trans.setCountryNexusTypeCode("*NO");
    	trans.setStateNexusTypeCode("*NO");
    	trans.setCountyNexusTypeCode("*NO");
    	trans.setCityNexusTypeCode("*NO");
    	trans.setStj1NexusTypeCode("*NO");
		trans.setStj2NexusTypeCode("*NO");
		trans.setStj3NexusTypeCode("*NO");
		trans.setStj4NexusTypeCode("*NO");
		trans.setStj5NexusTypeCode("*NO");
		trans.setStj6NexusTypeCode("*NO");
		trans.setStj7NexusTypeCode("*NO");
		trans.setStj8NexusTypeCode("*NO");
		trans.setStj9NexusTypeCode("*NO");
		trans.setStj10NexusTypeCode("*NO");

    	if(trans.getUnlockEntityId()==null || trans.getUnlockEntityId()<0L){
    		return;
    	}
    	//else{
    	//	if(trans.getEntityCode()==null || trans.getEntityCode().length()==0){
    	//		return;
    	//	}
    	//}

		String vendorNexusWhere = "";
		String vendorNexusOrderby = "";
		
		String nexusindCode = "";

		String primaryCountryCode = trans.getCountryCode();
		String primaryStateCode = trans.getStateCode();
		String primaryCounty = trans.getCountyName();
		String primaryCity = trans.getCityName();
		
		//Check each Primary Nexus Jurisdictions
		//Loop through Jurisdictions in FORWARD order
		for(int jurIdx = 2; jurIdx <= 14; jurIdx++) {
			switch(jurIdx) {
			case 1: //jurIdx 1 = Country
				nexusindCode = "I";
				vendorNexusWhere =	" AND nexus_country_code = '" + primaryCountryCode + "'" +
                              		" AND nexus_state_code='*COUNTRY' " +
                              		" AND nexus_county='*COUNTRY' " +
                              		" AND nexus_city='*COUNTRY' " +
                              		" AND nexus_stj='*COUNTRY' ";
				vendorNexusOrderby = 	" ORDER BY tb_nexus_def_detail.effective_date DESC ";
				break;
			case 2: //jurIdx 2 = State
				nexusindCode = "I";
				vendorNexusWhere =	" AND nexus_country_code = '" + primaryCountryCode + "'" +
                              		" AND nexus_state_code = '" + primaryStateCode + "'" +
                              		" AND nexus_county='*STATE' " +
                              		" AND nexus_city='*STATE' " +
                              		" AND nexus_stj='*STATE' ";
				vendorNexusOrderby = 	" ORDER BY tb_nexus_def_detail.effective_date DESC ";
				break;
			case 3: //jurIdx 3 = County
				if(trans.getCountyNexusindCode()!=null && trans.getCountyNexusindCode().equalsIgnoreCase("S")){
	               nexusindCode = "S";             
	               trans.setCountyVendorNexusDtlId(trans.getStateVendorNexusDtlId());	               
	               trans.setCountyNexusTypeCode(trans.getStateNexusTypeCode());
	               continue; //no query
				}
				else{
	               nexusindCode = "I";
	               vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
	                                " AND nexus_state_code = '" + primaryStateCode + "'" +
	                                " AND (nexus_county = '" +  primaryCounty + "'" +
	                                " OR  nexus_county='*ALL')" +
	                                " AND nexus_city='*COUNTY'" +
	                                " AND nexus_stj='*COUNTY'";
	               vendorNexusOrderby = " ORDER BY tb_nexus_def.nexus_county DESC, tb_nexus_def_detail.effective_date DESC";
				}
				break;
			case 4: //jurIdx 4 = City
				if(trans.getCityNexusindCode()!=null && trans.getCityNexusindCode().equalsIgnoreCase("S")){
	               nexusindCode = "S";
	               trans.setCityVendorNexusDtlId(trans.getStateVendorNexusDtlId());
	               trans.setCityNexusTypeCode(trans.getStateNexusTypeCode());
	               continue; //no query
				}
				else{
	               nexusindCode = "I";
	               vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
	                                " AND nexus_state_code = '" + primaryStateCode + "'" +
	                                " AND nexus_county='*CITY'" +
	                                " AND (nexus_city = '" + primaryCity + "'" +
	                                " OR  nexus_city='*ALL')" +
	                                " AND nexus_stj='*CITY'";
	               vendorNexusOrderby = " ORDER BY tb_nexus_def.nexus_city DESC, tb_nexus_def_detail.effective_date DESC";
				}
				break;
			default: //jurIdx 5-14, Stj1 - Stj10
				int stj = jurIdx-4; 
				String stjNexusindCode = null;
				String stjFinalName = null;
				Method theMethod = null;
				try{
            		theMethod = trans.getClass().getDeclaredMethod("getStj" + stj + "Name", new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	stjFinalName = (String)theMethod.invoke(trans, new Object[]{});
				}
				catch(Exception e){
					e.printStackTrace();
				}	
				
				if(stjFinalName!=null && stjFinalName.length()>0){
					try{
	            		theMethod = trans.getClass().getDeclaredMethod("getStj" + stj + "NexusindCode", new Class[]{});
	    	        	theMethod.setAccessible(true);	        	
	    	        	stjNexusindCode = (String)theMethod.invoke(trans, new Object[]{});
					}
					catch(Exception e){
						e.printStackTrace();
					}	
					
					if(stjNexusindCode!=null && stjNexusindCode.equalsIgnoreCase("S")){
						nexusindCode = "S";
						
						try{
							theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusTypeCode", new Class[] {String.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{trans.getStateNexusTypeCode()}); 
	                		
	                		theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusDefDetailId", new Class[] {Long.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{trans.getStateVendorNexusDtlId()}); 
						}
						catch(Exception e){
							e.printStackTrace();
						}
						continue; //no query
					}
					else{
						nexusindCode = "I";
						vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
                                		" AND nexus_state_code = '" + primaryStateCode + "'" +
                                		" AND nexus_county='*STJ'" +
                                		" AND nexus_city='*STJ'" +
                                		" AND (nexus_stj='" + stjFinalName + "'" +
                                		" OR  nexus_stj='*ALL')";
						vendorNexusOrderby = " ORDER BY tb_nexus_def.nexus_stj DESC, tb_nexus_def_detail.effective_date DESC";
					}
				}
				else{
					nexusindCode = "X";
					continue;
				}
				
	            break;		
			}
			
			//Search for Nexus when Indicator = 'I'
			if("I".equals(nexusindCode)) {
	         // Search for Nexus
				List<NexusDefinitionDetail> nexusDefinitionDetails = vendorNexusDetailService.getEntityNexusDtlList(trans.getUnlockEntityId(), trans.getGlDate(), vendorNexusWhere, vendorNexusOrderby);
				
				//Nexus line found
				if(nexusDefinitionDetails != null && nexusDefinitionDetails.size() > 0) {
					NexusDefinitionDetail nexusDefinitionDetail = nexusDefinitionDetails.get(0);
					
					switch(jurIdx) {
					case 1:   //jur_idx 1 = Country
						trans.setCountryNexusDefDetailId(nexusDefinitionDetail.getNexusDefDetailId());
						trans.setCountryNexusTypeCode(nexusDefinitionDetail.getNexusTypeCode());
						
						break;
					case 2:   //jur_idx 2 = State
						trans.setStateNexusDefDetailId(nexusDefinitionDetail.getNexusDefDetailId());
						trans.setStateNexusTypeCode(nexusDefinitionDetail.getNexusTypeCode());						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						
	                  	break;
					case 3:   //jur_idx 3 = County
						trans.setCountyNexusDefDetailId(nexusDefinitionDetail.getNexusDefDetailId());
						trans.setCountyNexusTypeCode(nexusDefinitionDetail.getNexusTypeCode());	
						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setStateNexusTypeCode("*YES");  
						}
						
						break;		
						
					case 4:   //jur_idx 4 = City
						trans.setCityNexusDefDetailId(nexusDefinitionDetail.getNexusDefDetailId());
						trans.setCityNexusTypeCode(nexusDefinitionDetail.getNexusTypeCode());						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setStateNexusTypeCode("*YES");  
						}
						if(trans.getCountyNexusTypeCode()!=null && trans.getCountyNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountyNexusTypeCode("*YES");  
						}

						break;
								
					default: //jurIdx 5-14, Stj1 - Stj10
						
						//trans.setStj1NexusDefDetailId(nexusDefinitionDetail.getNexusDefDetailId());
						//trans.setStj1NexusTypeCode(nexusDefinitionDetail.getNexusTypeCode());
						
						int stj = jurIdx-4; 
						try{
							Method theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusDefDetailId", new Class[] {Long.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{nexusDefinitionDetail.getNexusDefDetailId()}); 
	                		
	                		theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusTypeCode", new Class[] {String.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{nexusDefinitionDetail.getNexusTypeCode()}); 
						}
						catch(Exception e){
							e.printStackTrace();
						}
		
			            break;
					}
				}			
				else {
					// Nexus not found
					
					boolean breakLoop = false;
					
					switch(jurIdx) {
					case 1: //jur_idx 1 = Country
						breakLoop = true;
						break;
					case 2: //jur_idx 2 = State
						breakLoop = true;
						break;
					default:
	                   //no nexus for County, City, or STJ1-STJ5 when not found
					}
					
					if(breakLoop) {
						break;
					}
				}
									
				//Check STJs after all 10 have been looked up
				if(jurIdx == 14) {
					if(		!"*NO".equals(trans.getStj1NexusTypeCode()) ||
							!"*NO".equals(trans.getStj2NexusTypeCode()) ||
							!"*NO".equals(trans.getStj3NexusTypeCode()) ||
							!"*NO".equals(trans.getStj4NexusTypeCode()) ||
							!"*NO".equals(trans.getStj5NexusTypeCode()) ||
							!"*NO".equals(trans.getStj6NexusTypeCode()) ||
							!"*NO".equals(trans.getStj7NexusTypeCode()) ||
							!"*NO".equals(trans.getStj8NexusTypeCode()) ||
							!"*NO".equals(trans.getStj9NexusTypeCode()) ||
							!"*NO".equals(trans.getStj10NexusTypeCode())) {
						if(trans.getCityNexusTypeCode()!=null && trans.getCityNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCityNexusTypeCode("*YES");  
						}
						if(trans.getCountyNexusTypeCode()!=null && trans.getCountyNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCountyNexusTypeCode("*YES");  
						}
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setStateNexusTypeCode("*YES");  
						}
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCountryNexusTypeCode("*YES");  
						}
						break;
					}
				}
			}//End of query	
		}

		//Check for full non-nexus
		if("*NO".equals(trans.getCountryNexusTypeCode()) && "*NO".equals(trans.getStateNexusTypeCode()) &&
			"*NO".equals(trans.getCountyNexusTypeCode()) && "*NO".equals(trans.getCityNexusTypeCode())) {
				trans.setExemptCode("FN");
				trans.setTransactionInd(TRANS_IND_PROCESSED);
				trans.setSuspendInd(null);
				//trans.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
				throw new PurchaseTranasactionSoftAbortProcessingException("");
		}

    }
    
    private void suspendLocnFlag(PurchaseTransaction trans, boolean alwaysNull){
    	boolean locationmatrixIdFound = false;
		if((trans.getShiptoLocnMatrixId()!=null && trans.getShiptoLocnMatrixId()>0L) || alwaysNull){
			//Shipto	
			locationmatrixIdFound = true;
			trans.setShiptoLocation(null);
			trans.setShiptoLocationName(null);		
			trans.setShiptoManualJurInd(null);
			
			trans.setShiptoEntityId(null);
			trans.setShiptoEntityCode(null);
			trans.setShiptoLat(null);
			trans.setShiptoLong(null);			
			trans.setShiptoLocnMatrixId(0L);
			trans.setShiptoJurisdictionId(null);		
			trans.setShiptoGeocode(null);
			trans.setShiptoAddressLine1(null);
			trans.setShiptoAddressLine2(null);
			trans.setShiptoCity(null);
			trans.setShiptoCounty(null);
			trans.setShiptoStateCode(null);
			trans.setShiptoZip(null);
			trans.setShiptoZipplus4(null);
			trans.setShiptoCountryCode(null);
			trans.setShiptoStj1Name(null);
			trans.setShiptoStj2Name(null);
			trans.setShiptoStj3Name(null);
			trans.setShiptoStj4Name(null);
			trans.setShiptoStj5Name(null);			
			trans.setShiptoCountryNexusindCode(null);
			trans.setShiptoStateNexusindCode(null);
			trans.setShiptoCountyNexusindCode(null);
			trans.setShiptoCityNexusindCode(null);
			trans.setShiptoStj1NexusindCode(null);
			trans.setShiptoStj2NexusindCode(null);
			trans.setShiptoStj3NexusindCode(null);
			trans.setShiptoStj4NexusindCode(null);
			trans.setShiptoStj5NexusindCode(null);
		}
		
		if((trans.getShipfromLocnMatrixId()!=null && trans.getShipfromLocnMatrixId()>0L) || alwaysNull){
			//Shipfrom	
			locationmatrixIdFound = true;
			trans.setShipfromEntityId(null);
			trans.setShipfromEntityCode(null);
			trans.setShipfromLat(null);
			trans.setShipfromLong(null);			
			trans.setShipfromLocnMatrixId(0L);
			trans.setShipfromJurisdictionId(null);		
			trans.setShipfromGeocode(null);
			trans.setShipfromAddressLine1(null);
			trans.setShipfromAddressLine2(null);
			trans.setShipfromCity(null);
			trans.setShipfromCounty(null);
			trans.setShipfromStateCode(null);
			trans.setShipfromZip(null);
			trans.setShipfromZipplus4(null);
			trans.setShipfromCountryCode(null);
			trans.setShipfromStj1Name(null);
			trans.setShipfromStj2Name(null);
			trans.setShipfromStj3Name(null);
			trans.setShipfromStj4Name(null);
			trans.setShipfromStj5Name(null);			
			trans.setShipfromCountryNexusindCode(null);
			trans.setShipfromStateNexusindCode(null);
			trans.setShipfromCountyNexusindCode(null);
			trans.setShipfromCityNexusindCode(null);
			trans.setShipfromStj1NexusindCode(null);
			trans.setShipfromStj2NexusindCode(null);
			trans.setShipfromStj3NexusindCode(null);
			trans.setShipfromStj4NexusindCode(null);
			trans.setShipfromStj5NexusindCode(null);
		}
		
		if((trans.getOrdracptLocnMatrixId()!=null && trans.getOrdracptLocnMatrixId()>0L) || alwaysNull){
			//Ordracpt
			locationmatrixIdFound = true;
			trans.setOrdracptEntityId(null);
			trans.setOrdracptEntityCode(null);
			trans.setOrdracptLat(null);
			trans.setOrdracptLong(null);			
			trans.setOrdracptLocnMatrixId(0L);
			trans.setOrdracptJurisdictionId(null);		
			trans.setOrdracptGeocode(null);
			trans.setOrdracptAddressLine1(null);
			trans.setOrdracptAddressLine2(null);
			trans.setOrdracptCity(null);
			trans.setOrdracptCounty(null);
			trans.setOrdracptStateCode(null);
			trans.setOrdracptZip(null);
			trans.setOrdracptZipplus4(null);
			trans.setOrdracptCountryCode(null);
			trans.setOrdracptStj1Name(null);
			trans.setOrdracptStj2Name(null);
			trans.setOrdracptStj3Name(null);
			trans.setOrdracptStj4Name(null);
			trans.setOrdracptStj5Name(null);			
			trans.setOrdracptCountryNexusindCode(null);
			trans.setOrdracptStateNexusindCode(null);
			trans.setOrdracptCountyNexusindCode(null);
			trans.setOrdracptCityNexusindCode(null);
			trans.setOrdracptStj1NexusindCode(null);
			trans.setOrdracptStj2NexusindCode(null);
			trans.setOrdracptStj3NexusindCode(null);
			trans.setOrdracptStj4NexusindCode(null);
			trans.setOrdracptStj5NexusindCode(null);
		}
		
		if((trans.getOrdrorgnLocnMatrixId()!=null && trans.getOrdrorgnLocnMatrixId()>0L) || alwaysNull){
			//Ordrorgn
			locationmatrixIdFound = true;
			trans.setOrdrorgnEntityId(null);
			trans.setOrdrorgnEntityCode(null);
			trans.setOrdrorgnLat(null);
			trans.setOrdrorgnLong(null);			
			trans.setOrdrorgnLocnMatrixId(0L);
			trans.setOrdrorgnJurisdictionId(null);		
			trans.setOrdrorgnGeocode(null);
			trans.setOrdrorgnAddressLine1(null);
			trans.setOrdrorgnAddressLine2(null);
			trans.setOrdrorgnCity(null);
			trans.setOrdrorgnCounty(null);
			trans.setOrdrorgnStateCode(null);
			trans.setOrdrorgnZip(null);
			trans.setOrdrorgnZipplus4(null);
			trans.setOrdrorgnCountryCode(null);
			trans.setOrdrorgnStj1Name(null);
			trans.setOrdrorgnStj2Name(null);
			trans.setOrdrorgnStj3Name(null);
			trans.setOrdrorgnStj4Name(null);
			trans.setOrdrorgnStj5Name(null);			
			trans.setOrdrorgnCountryNexusindCode(null);
			trans.setOrdrorgnStateNexusindCode(null);
			trans.setOrdrorgnCountyNexusindCode(null);
			trans.setOrdrorgnCityNexusindCode(null);
			trans.setOrdrorgnStj1NexusindCode(null);
			trans.setOrdrorgnStj2NexusindCode(null);
			trans.setOrdrorgnStj3NexusindCode(null);
			trans.setOrdrorgnStj4NexusindCode(null);
			trans.setOrdrorgnStj5NexusindCode(null);
		}
		
		if((trans.getFirstuseLocnMatrixId()!=null && trans.getFirstuseLocnMatrixId()>0L) || alwaysNull){
			//Firstuse
			locationmatrixIdFound = true;
			trans.setFirstuseEntityId(null);
			trans.setFirstuseEntityCode(null);
			trans.setFirstuseLat(null);
			trans.setFirstuseLong(null);			
			trans.setFirstuseLocnMatrixId(0L);
			trans.setFirstuseJurisdictionId(null);		
			trans.setFirstuseGeocode(null);
			trans.setFirstuseAddressLine1(null);
			trans.setFirstuseAddressLine2(null);
			trans.setFirstuseCity(null);
			trans.setFirstuseCounty(null);
			trans.setFirstuseStateCode(null);
			trans.setFirstuseZip(null);
			trans.setFirstuseZipplus4(null);
			trans.setFirstuseCountryCode(null);
			trans.setFirstuseStj1Name(null);
			trans.setFirstuseStj2Name(null);
			trans.setFirstuseStj3Name(null);
			trans.setFirstuseStj4Name(null);
			trans.setFirstuseStj5Name(null);			
			trans.setFirstuseCountryNexusindCode(null);
			trans.setFirstuseStateNexusindCode(null);
			trans.setFirstuseCountyNexusindCode(null);
			trans.setFirstuseCityNexusindCode(null);
			trans.setFirstuseStj1NexusindCode(null);
			trans.setFirstuseStj2NexusindCode(null);
			trans.setFirstuseStj3NexusindCode(null);
			trans.setFirstuseStj4NexusindCode(null);
			trans.setFirstuseStj5NexusindCode(null);
		}
		
		if((trans.getBilltoLocnMatrixId()!=null && trans.getBilltoLocnMatrixId()>0L) || alwaysNull){
			//Billto
			locationmatrixIdFound = true;
			trans.setBilltoEntityId(null);
			trans.setBilltoEntityCode(null);
			trans.setBilltoLat(null);
			trans.setBilltoLong(null);			
			trans.setBilltoLocnMatrixId(0L);
			trans.setBilltoJurisdictionId(null);		
			trans.setBilltoGeocode(null);
			trans.setBilltoAddressLine1(null);
			trans.setBilltoAddressLine2(null);
			trans.setBilltoCity(null);
			trans.setBilltoCounty(null);
			trans.setBilltoStateCode(null);
			trans.setBilltoZip(null);
			trans.setBilltoZipplus4(null);
			trans.setBilltoCountryCode(null);
			trans.setBilltoStj1Name(null);
			trans.setBilltoStj2Name(null);
			trans.setBilltoStj3Name(null);
			trans.setBilltoStj4Name(null);
			trans.setBilltoStj5Name(null);			
			trans.setBilltoCountryNexusindCode(null);
			trans.setBilltoStateNexusindCode(null);
			trans.setBilltoCountyNexusindCode(null);
			trans.setBilltoCityNexusindCode(null);
			trans.setBilltoStj1NexusindCode(null);
			trans.setBilltoStj2NexusindCode(null);
			trans.setBilltoStj3NexusindCode(null);
			trans.setBilltoStj4NexusindCode(null);
			trans.setBilltoStj5NexusindCode(null);
		}
		
		if((trans.getTtlxfrLocnMatrixId()!=null && trans.getTtlxfrLocnMatrixId()>0L) || alwaysNull){
			//Ttlxfr
			locationmatrixIdFound = true;
			trans.setTtlxfrEntityId(null);
			trans.setTtlxfrEntityCode(null);
			trans.setTtlxfrLat(null);
			trans.setTtlxfrLong(null);			
			trans.setTtlxfrLocnMatrixId(0L);
			trans.setTtlxfrJurisdictionId(null);		
			trans.setTtlxfrGeocode(null);
			trans.setTtlxfrAddressLine1(null);
			trans.setTtlxfrAddressLine2(null);
			trans.setTtlxfrCity(null);
			trans.setTtlxfrCounty(null);
			trans.setTtlxfrStateCode(null);
			trans.setTtlxfrZip(null);
			trans.setTtlxfrZipplus4(null);
			trans.setTtlxfrCountryCode(null);
			trans.setTtlxfrStj1Name(null);
			trans.setTtlxfrStj2Name(null);
			trans.setTtlxfrStj3Name(null);
			trans.setTtlxfrStj4Name(null);
			trans.setTtlxfrStj5Name(null);			
			trans.setTtlxfrCountryNexusindCode(null);
			trans.setTtlxfrStateNexusindCode(null);
			trans.setTtlxfrCountyNexusindCode(null);
			trans.setTtlxfrCityNexusindCode(null);
			trans.setTtlxfrStj1NexusindCode(null);
			trans.setTtlxfrStj2NexusindCode(null);
			trans.setTtlxfrStj3NexusindCode(null);
			trans.setTtlxfrStj4NexusindCode(null);
			trans.setTtlxfrStj5NexusindCode(null);
		}
		
		//Need to clear *_situs_matrix_id & *_vendor_nexus_detail_id	
		trans.setAutoTransactionCountryCode(null);
		trans.setAutoTransactionStateCode(null);
	
		trans.setCountryVendorNexusDtlId(null);
		trans.setStateVendorNexusDtlId(null);
		trans.setCountyVendorNexusDtlId(null);
		trans.setCityVendorNexusDtlId(null);
		trans.setStj1VendorNexusDtlId(null);
		trans.setStj2VendorNexusDtlId(null);
		trans.setStj3VendorNexusDtlId(null);
		trans.setStj4VendorNexusDtlId(null);
		trans.setStj5VendorNexusDtlId(null);
		trans.setStj6VendorNexusDtlId(null);
		trans.setStj7VendorNexusDtlId(null);
		trans.setStj8VendorNexusDtlId(null);
		trans.setStj9VendorNexusDtlId(null);
		trans.setStj10VendorNexusDtlId(null);

		trans.setCountrySitusMatrixId(0L);
		trans.setStateSitusMatrixId(0L);
		trans.setCountySitusMatrixId(0L);
		trans.setCitySitusMatrixId(0L);
		trans.setStj1SitusMatrixId(0L);
		trans.setStj2SitusMatrixId(0L);
		trans.setStj3SitusMatrixId(0L);
		trans.setStj4SitusMatrixId(0L);
		trans.setStj5SitusMatrixId(0L);
		trans.setCountrySitusCode(null);
		trans.setStateSitusCode(null);
		trans.setCountySitusCode(null);
		trans.setCitySitusCode(null);
		trans.setStj1SitusCode(null);
		trans.setStj2SitusCode(null);
		trans.setStj3SitusCode(null);
		trans.setStj4SitusCode(null);
		trans.setStj5SitusCode(null);
		trans.setStj6SitusCode(null);
		trans.setStj7SitusCode(null);
		trans.setStj8SitusCode(null);
		trans.setStj9SitusCode(null);
		trans.setStj10SitusCode(null);
		trans.setCountryCode(null);
		trans.setStateCode(null);
		trans.setCountyName(null);
		trans.setCityName(null);
	    trans.setStj1Name(null);
	    trans.setStj2Name(null);
	    trans.setStj3Name(null);
	    trans.setStj4Name(null);
	    trans.setStj5Name(null);
	    trans.setStj6Name(null);
	    trans.setStj7Name(null);
	    trans.setStj8Name(null);
	    trans.setStj9Name(null);
	    trans.setStj10Name(null);
	    trans.setCountrySitusJurId(null);
	    trans.setStateSitusJurId(null);
	    trans.setCountySitusJurId(null);
	    trans.setCitySitusJurId(null);
	    trans.setStj1SitusJurId(null);
	    trans.setStj2SitusJurId(null);
	    trans.setStj3SitusJurId(null);
	    trans.setStj4SitusJurId(null);
	    trans.setStj5SitusJurId(null);
	    trans.setStj6SitusJurId(null);
	    trans.setStj7SitusJurId(null);
	    trans.setStj8SitusJurId(null);
	    trans.setStj9SitusJurId(null);
	    trans.setStj10SitusJurId(null);
	    trans.setCountryNexusindCode(null);
	    trans.setStateNexusindCode(null);
	    trans.setCountyNexusindCode(null);
	    trans.setCityNexusindCode(null);
	    trans.setStj1NexusindCode(null);
	    trans.setStj2NexusindCode(null);
	    trans.setStj3NexusindCode(null);
	    trans.setStj4NexusindCode(null);
	    trans.setStj5NexusindCode(null);
	    trans.setStj6NexusindCode(null);
	    trans.setStj7NexusindCode(null);
	    trans.setStj8NexusindCode(null);
	    trans.setStj9NexusindCode(null);
	    trans.setStj10NexusindCode(null);
	    trans.setCountryVendorNexusDtlId(null);
	    trans.setStateVendorNexusDtlId(null);
	    trans.setCountyVendorNexusDtlId(null);
	    trans.setCityVendorNexusDtlId(null);
	    trans.setCountryNexusTypeCode(null);
	    trans.setStateNexusTypeCode(null);
	    trans.setCountyNexusTypeCode(null);
	    trans.setCityNexusTypeCode(null);
	    trans.setCountryPrimarySitusCode(null);
	    trans.setCountryPrimaryTaxtypeCode(null);
	    trans.setCountrySecondarySitusCode(null);
	    trans.setCountrySecondaryTaxtypeCode(null);
	    trans.setCountryAllLevelsFlag(null);
	    trans.setStatePrimarySitusCode(null);
	    trans.setStatePrimaryTaxtypeCode(null);
	    trans.setStateSecondarySitusCode(null);
	    trans.setStateSecondaryTaxtypeCode(null);
	    trans.setStateAllLevelsFlag(null);
	    trans.setCountyPrimarySitusCode(null);
	    trans.setCountyPrimaryTaxtypeCode(null);
	    trans.setCountySecondarySitusCode(null);
	    trans.setCountySecondaryTaxtypeCode(null);
	    trans.setCountyAllLevelsFlag(null);
	    trans.setCityPrimarySitusCode(null);
	    trans.setCityPrimaryTaxtypeCode(null);
	    trans.setCitySecondarySitusCode(null);
	    trans.setCitySecondaryTaxtypeCode(null);
	    trans.setCityAllLevelsFlag(null);
	    trans.setStj1PrimarySitusCode(null);
	    trans.setStj1PrimaryTaxtypeCode(null);
	    trans.setStj1SecondarySitusCode(null);
	    trans.setStj1SecondaryTaxtypeCode(null);
	    trans.setStj1AllLevelsFlag(null);
	    trans.setStj2PrimarySitusCode(null);
	    trans.setStj2PrimaryTaxtypeCode(null);
	    trans.setStj2SecondarySitusCode(null);
	    trans.setStj2SecondaryTaxtypeCode(null);
	    trans.setStj2AllLevelsFlag(null);
	    trans.setStj3PrimarySitusCode(null);
	    trans.setStj3PrimaryTaxtypeCode(null);
	    trans.setStj3SecondarySitusCode(null);
	    trans.setStj3SecondaryTaxtypeCode(null);
	    trans.setStj3AllLevelsFlag(null);
	    trans.setStj4PrimarySitusCode(null);
	    trans.setStj4PrimaryTaxtypeCode(null);
	    trans.setStj4SecondarySitusCode(null);
	    trans.setStj4SecondaryTaxtypeCode(null);
	    trans.setStj4AllLevelsFlag(null);
	    trans.setStj5PrimarySitusCode(null);
	    trans.setStj5PrimaryTaxtypeCode(null);
	    trans.setStj5SecondarySitusCode(null);
	    trans.setStj5SecondaryTaxtypeCode(null);
	    trans.setStj5AllLevelsFlag(null);
	    trans.setStj6PrimarySitusCode(null);
	    trans.setStj6PrimaryTaxtypeCode(null);
	    trans.setStj6SecondarySitusCode(null);
	    trans.setStj6SecondaryTaxtypeCode(null);
	    trans.setStj6AllLevelsFlag(null);
	    trans.setStj7PrimarySitusCode(null);
	    trans.setStj7PrimaryTaxtypeCode(null);
	    trans.setStj7SecondarySitusCode(null);
	    trans.setStj7SecondaryTaxtypeCode(null);
	    trans.setStj7AllLevelsFlag(null);
	    trans.setStj8PrimarySitusCode(null);
	    trans.setStj8PrimaryTaxtypeCode(null);
	    trans.setStj8SecondarySitusCode(null);
	    trans.setStj8SecondaryTaxtypeCode(null);
	    trans.setStj8AllLevelsFlag(null);
	    trans.setStj9PrimarySitusCode(null);
	    trans.setStj9PrimaryTaxtypeCode(null);
	    trans.setStj9SecondarySitusCode(null);
	    trans.setStj9SecondaryTaxtypeCode(null);
	    trans.setStj9AllLevelsFlag(null);
	    trans.setStj10PrimarySitusCode(null);
	    trans.setStj10PrimaryTaxtypeCode(null);
	    trans.setStj10SecondarySitusCode(null);
	    trans.setStj10SecondaryTaxtypeCode(null);
	    trans.setStj10AllLevelsFlag(null);
	}
    
    private void suspendLocnFlag(MicroApiTransaction trans, boolean alwaysNull){
    	boolean locationmatrixIdFound = false;
//		if((trans.getShiptoLocnMatrixId()!=null && trans.getShiptoLocnMatrixId()>0L) || alwaysNull){
//			//Shipto	
//			locationmatrixIdFound = true;
//			trans.setShiptoLocation(null);
//			trans.setShiptoLocationName(null);		
//			trans.setShiptoManualJurInd(null);
//			
//			trans.setShiptoEntityId(null);
//			trans.setShiptoEntityCode(null);
//			trans.setShiptoLat(null);
//			trans.setShiptoLong(null);			
//			trans.setShiptoLocnMatrixId(0L);
//			trans.setShiptoJurisdictionId(null);		
//			trans.setShiptoGeocode(null);
//			trans.setShiptoAddressLine1(null);
//			trans.setShiptoAddressLine2(null);
//			trans.setShiptoCity(null);
//			trans.setShiptoCounty(null);
//			trans.setShiptoStateCode(null);
//			trans.setShiptoZip(null);
//			trans.setShiptoZipplus4(null);
//			trans.setShiptoCountryCode(null);
//			trans.setShiptoStj1Name(null);
//			trans.setShiptoStj2Name(null);
//			trans.setShiptoStj3Name(null);
//			trans.setShiptoStj4Name(null);
//			trans.setShiptoStj5Name(null);			
//			trans.setShiptoCountryNexusindCode(null);
//			trans.setShiptoStateNexusindCode(null);
//			trans.setShiptoCountyNexusindCode(null);
//			trans.setShiptoCityNexusindCode(null);
//			trans.setShiptoStj1NexusindCode(null);
//			trans.setShiptoStj2NexusindCode(null);
//			trans.setShiptoStj3NexusindCode(null);
//			trans.setShiptoStj4NexusindCode(null);
//			trans.setShiptoStj5NexusindCode(null);
//		}
//		
//		if((trans.getShipfromLocnMatrixId()!=null && trans.getShipfromLocnMatrixId()>0L) || alwaysNull){
//			//Shipfrom	
//			locationmatrixIdFound = true;
//			trans.setShipfromEntityId(null);
//			trans.setShipfromEntityCode(null);
//			trans.setShipfromLat(null);
//			trans.setShipfromLong(null);			
//			trans.setShipfromLocnMatrixId(0L);
//			trans.setShipfromJurisdictionId(null);		
//			trans.setShipfromGeocode(null);
//			trans.setShipfromAddressLine1(null);
//			trans.setShipfromAddressLine2(null);
//			trans.setShipfromCity(null);
//			trans.setShipfromCounty(null);
//			trans.setShipfromStateCode(null);
//			trans.setShipfromZip(null);
//			trans.setShipfromZipplus4(null);
//			trans.setShipfromCountryCode(null);
//			trans.setShipfromStj1Name(null);
//			trans.setShipfromStj2Name(null);
//			trans.setShipfromStj3Name(null);
//			trans.setShipfromStj4Name(null);
//			trans.setShipfromStj5Name(null);			
//			trans.setShipfromCountryNexusindCode(null);
//			trans.setShipfromStateNexusindCode(null);
//			trans.setShipfromCountyNexusindCode(null);
//			trans.setShipfromCityNexusindCode(null);
//			trans.setShipfromStj1NexusindCode(null);
//			trans.setShipfromStj2NexusindCode(null);
//			trans.setShipfromStj3NexusindCode(null);
//			trans.setShipfromStj4NexusindCode(null);
//			trans.setShipfromStj5NexusindCode(null);
//		}
//		
//		if((trans.getOrdracptLocnMatrixId()!=null && trans.getOrdracptLocnMatrixId()>0L) || alwaysNull){
//			//Ordracpt
//			locationmatrixIdFound = true;
//			trans.setOrdracptEntityId(null);
//			trans.setOrdracptEntityCode(null);
//			trans.setOrdracptLat(null);
//			trans.setOrdracptLong(null);			
//			trans.setOrdracptLocnMatrixId(0L);
//			trans.setOrdracptJurisdictionId(null);		
//			trans.setOrdracptGeocode(null);
//			trans.setOrdracptAddressLine1(null);
//			trans.setOrdracptAddressLine2(null);
//			trans.setOrdracptCity(null);
//			trans.setOrdracptCounty(null);
//			trans.setOrdracptStateCode(null);
//			trans.setOrdracptZip(null);
//			trans.setOrdracptZipplus4(null);
//			trans.setOrdracptCountryCode(null);
//			trans.setOrdracptStj1Name(null);
//			trans.setOrdracptStj2Name(null);
//			trans.setOrdracptStj3Name(null);
//			trans.setOrdracptStj4Name(null);
//			trans.setOrdracptStj5Name(null);			
//			trans.setOrdracptCountryNexusindCode(null);
//			trans.setOrdracptStateNexusindCode(null);
//			trans.setOrdracptCountyNexusindCode(null);
//			trans.setOrdracptCityNexusindCode(null);
//			trans.setOrdracptStj1NexusindCode(null);
//			trans.setOrdracptStj2NexusindCode(null);
//			trans.setOrdracptStj3NexusindCode(null);
//			trans.setOrdracptStj4NexusindCode(null);
//			trans.setOrdracptStj5NexusindCode(null);
//		}
//		
//		if((trans.getOrdrorgnLocnMatrixId()!=null && trans.getOrdrorgnLocnMatrixId()>0L) || alwaysNull){
//			//Ordrorgn
//			locationmatrixIdFound = true;
//			trans.setOrdrorgnEntityId(null);
//			trans.setOrdrorgnEntityCode(null);
//			trans.setOrdrorgnLat(null);
//			trans.setOrdrorgnLong(null);			
//			trans.setOrdrorgnLocnMatrixId(0L);
//			trans.setOrdrorgnJurisdictionId(null);		
//			trans.setOrdrorgnGeocode(null);
//			trans.setOrdrorgnAddressLine1(null);
//			trans.setOrdrorgnAddressLine2(null);
//			trans.setOrdrorgnCity(null);
//			trans.setOrdrorgnCounty(null);
//			trans.setOrdrorgnStateCode(null);
//			trans.setOrdrorgnZip(null);
//			trans.setOrdrorgnZipplus4(null);
//			trans.setOrdrorgnCountryCode(null);
//			trans.setOrdrorgnStj1Name(null);
//			trans.setOrdrorgnStj2Name(null);
//			trans.setOrdrorgnStj3Name(null);
//			trans.setOrdrorgnStj4Name(null);
//			trans.setOrdrorgnStj5Name(null);			
//			trans.setOrdrorgnCountryNexusindCode(null);
//			trans.setOrdrorgnStateNexusindCode(null);
//			trans.setOrdrorgnCountyNexusindCode(null);
//			trans.setOrdrorgnCityNexusindCode(null);
//			trans.setOrdrorgnStj1NexusindCode(null);
//			trans.setOrdrorgnStj2NexusindCode(null);
//			trans.setOrdrorgnStj3NexusindCode(null);
//			trans.setOrdrorgnStj4NexusindCode(null);
//			trans.setOrdrorgnStj5NexusindCode(null);
//		}
//		
//		if((trans.getFirstuseLocnMatrixId()!=null && trans.getFirstuseLocnMatrixId()>0L) || alwaysNull){
//			//Firstuse
//			locationmatrixIdFound = true;
//			trans.setFirstuseEntityId(null);
//			trans.setFirstuseEntityCode(null);
//			trans.setFirstuseLat(null);
//			trans.setFirstuseLong(null);			
//			trans.setFirstuseLocnMatrixId(0L);
//			trans.setFirstuseJurisdictionId(null);		
//			trans.setFirstuseGeocode(null);
//			trans.setFirstuseAddressLine1(null);
//			trans.setFirstuseAddressLine2(null);
//			trans.setFirstuseCity(null);
//			trans.setFirstuseCounty(null);
//			trans.setFirstuseStateCode(null);
//			trans.setFirstuseZip(null);
//			trans.setFirstuseZipplus4(null);
//			trans.setFirstuseCountryCode(null);
//			trans.setFirstuseStj1Name(null);
//			trans.setFirstuseStj2Name(null);
//			trans.setFirstuseStj3Name(null);
//			trans.setFirstuseStj4Name(null);
//			trans.setFirstuseStj5Name(null);			
//			trans.setFirstuseCountryNexusindCode(null);
//			trans.setFirstuseStateNexusindCode(null);
//			trans.setFirstuseCountyNexusindCode(null);
//			trans.setFirstuseCityNexusindCode(null);
//			trans.setFirstuseStj1NexusindCode(null);
//			trans.setFirstuseStj2NexusindCode(null);
//			trans.setFirstuseStj3NexusindCode(null);
//			trans.setFirstuseStj4NexusindCode(null);
//			trans.setFirstuseStj5NexusindCode(null);
//		}
//		
//		if((trans.getBilltoLocnMatrixId()!=null && trans.getBilltoLocnMatrixId()>0L) || alwaysNull){
//			//Billto
//			locationmatrixIdFound = true;
//			trans.setBilltoEntityId(null);
//			trans.setBilltoEntityCode(null);
//			trans.setBilltoLat(null);
//			trans.setBilltoLong(null);			
//			trans.setBilltoLocnMatrixId(0L);
//			trans.setBilltoJurisdictionId(null);		
//			trans.setBilltoGeocode(null);
//			trans.setBilltoAddressLine1(null);
//			trans.setBilltoAddressLine2(null);
//			trans.setBilltoCity(null);
//			trans.setBilltoCounty(null);
//			trans.setBilltoStateCode(null);
//			trans.setBilltoZip(null);
//			trans.setBilltoZipplus4(null);
//			trans.setBilltoCountryCode(null);
//			trans.setBilltoStj1Name(null);
//			trans.setBilltoStj2Name(null);
//			trans.setBilltoStj3Name(null);
//			trans.setBilltoStj4Name(null);
//			trans.setBilltoStj5Name(null);			
//			trans.setBilltoCountryNexusindCode(null);
//			trans.setBilltoStateNexusindCode(null);
//			trans.setBilltoCountyNexusindCode(null);
//			trans.setBilltoCityNexusindCode(null);
//			trans.setBilltoStj1NexusindCode(null);
//			trans.setBilltoStj2NexusindCode(null);
//			trans.setBilltoStj3NexusindCode(null);
//			trans.setBilltoStj4NexusindCode(null);
//			trans.setBilltoStj5NexusindCode(null);
//		}
//		
//		if((trans.getTtlxfrLocnMatrixId()!=null && trans.getTtlxfrLocnMatrixId()>0L) || alwaysNull){
//			//Ttlxfr
//			locationmatrixIdFound = true;
//			trans.setTtlxfrEntityId(null);
//			trans.setTtlxfrEntityCode(null);
//			trans.setTtlxfrLat(null);
//			trans.setTtlxfrLong(null);			
//			trans.setTtlxfrLocnMatrixId(0L);
//			trans.setTtlxfrJurisdictionId(null);		
//			trans.setTtlxfrGeocode(null);
//			trans.setTtlxfrAddressLine1(null);
//			trans.setTtlxfrAddressLine2(null);
//			trans.setTtlxfrCity(null);
//			trans.setTtlxfrCounty(null);
//			trans.setTtlxfrStateCode(null);
//			trans.setTtlxfrZip(null);
//			trans.setTtlxfrZipplus4(null);
//			trans.setTtlxfrCountryCode(null);
//			trans.setTtlxfrStj1Name(null);
//			trans.setTtlxfrStj2Name(null);
//			trans.setTtlxfrStj3Name(null);
//			trans.setTtlxfrStj4Name(null);
//			trans.setTtlxfrStj5Name(null);			
//			trans.setTtlxfrCountryNexusindCode(null);
//			trans.setTtlxfrStateNexusindCode(null);
//			trans.setTtlxfrCountyNexusindCode(null);
//			trans.setTtlxfrCityNexusindCode(null);
//			trans.setTtlxfrStj1NexusindCode(null);
//			trans.setTtlxfrStj2NexusindCode(null);
//			trans.setTtlxfrStj3NexusindCode(null);
//			trans.setTtlxfrStj4NexusindCode(null);
//			trans.setTtlxfrStj5NexusindCode(null);
//		}
		
		//Clear out Matrix Ids
		trans.setShiptoLocnMatrixId(0L);
		trans.setShipfromLocnMatrixId(0L);
		trans.setBilltoLocnMatrixId(0L);
		trans.setFirstuseLocnMatrixId(0L);
		trans.setOrdrorgnLocnMatrixId(0L);
		trans.setOrdracptLocnMatrixId(0L);

		//Need to clear *_situs_matrix_id & *_vendor_nexus_detail_id	
		trans.setAutoTransactionCountryCode(null);
		trans.setAutoTransactionStateCode(null);
	
		trans.setCountryVendorNexusDtlId(null);
		trans.setStateVendorNexusDtlId(null);
		trans.setCountyVendorNexusDtlId(null);
		trans.setCityVendorNexusDtlId(null);
		trans.setStj1VendorNexusDtlId(null);
		trans.setStj2VendorNexusDtlId(null);
		trans.setStj3VendorNexusDtlId(null);
		trans.setStj4VendorNexusDtlId(null);
		trans.setStj5VendorNexusDtlId(null);
		trans.setStj6VendorNexusDtlId(null);
		trans.setStj7VendorNexusDtlId(null);
		trans.setStj8VendorNexusDtlId(null);
		trans.setStj9VendorNexusDtlId(null);
		trans.setStj10VendorNexusDtlId(null);

		trans.setCountrySitusMatrixId(0L);
		trans.setStateSitusMatrixId(0L);
		trans.setCountySitusMatrixId(0L);
		trans.setCitySitusMatrixId(0L);
		trans.setStj1SitusMatrixId(0L);
		trans.setStj2SitusMatrixId(0L);
		trans.setStj3SitusMatrixId(0L);
		trans.setStj4SitusMatrixId(0L);
		trans.setStj5SitusMatrixId(0L);
		trans.setCountrySitusCode(null);
		trans.setStateSitusCode(null);
		trans.setCountySitusCode(null);
		trans.setCitySitusCode(null);
		trans.setStj1SitusCode(null);
		trans.setStj2SitusCode(null);
		trans.setStj3SitusCode(null);
		trans.setStj4SitusCode(null);
		trans.setStj5SitusCode(null);
		trans.setStj6SitusCode(null);
		trans.setStj7SitusCode(null);
		trans.setStj8SitusCode(null);
		trans.setStj9SitusCode(null);
		trans.setStj10SitusCode(null);
		trans.setCountryCode(null);
		trans.setStateCode(null);
		trans.setCountyName(null);
		trans.setCityName(null);
	    trans.setStj1Name(null);
	    trans.setStj2Name(null);
	    trans.setStj3Name(null);
	    trans.setStj4Name(null);
	    trans.setStj5Name(null);
	    trans.setStj6Name(null);
	    trans.setStj7Name(null);
	    trans.setStj8Name(null);
	    trans.setStj9Name(null);
	    trans.setStj10Name(null);
	    trans.setCountrySitusJurId(null);
	    trans.setStateSitusJurId(null);
	    trans.setCountySitusJurId(null);
	    trans.setCitySitusJurId(null);
	    trans.setStj1SitusJurId(null);
	    trans.setStj2SitusJurId(null);
	    trans.setStj3SitusJurId(null);
	    trans.setStj4SitusJurId(null);
	    trans.setStj5SitusJurId(null);
	    trans.setStj6SitusJurId(null);
	    trans.setStj7SitusJurId(null);
	    trans.setStj8SitusJurId(null);
	    trans.setStj9SitusJurId(null);
	    trans.setStj10SitusJurId(null);
	    trans.setCountryNexusindCode(null);
	    trans.setStateNexusindCode(null);
	    trans.setCountyNexusindCode(null);
	    trans.setCityNexusindCode(null);
	    trans.setStj1NexusindCode(null);
	    trans.setStj2NexusindCode(null);
	    trans.setStj3NexusindCode(null);
	    trans.setStj4NexusindCode(null);
	    trans.setStj5NexusindCode(null);
	    trans.setStj6NexusindCode(null);
	    trans.setStj7NexusindCode(null);
	    trans.setStj8NexusindCode(null);
	    trans.setStj9NexusindCode(null);
	    trans.setStj10NexusindCode(null);
	    trans.setCountryVendorNexusDtlId(null);
	    trans.setStateVendorNexusDtlId(null);
	    trans.setCountyVendorNexusDtlId(null);
	    trans.setCityVendorNexusDtlId(null);
	    trans.setCountryNexusTypeCode(null);
	    trans.setStateNexusTypeCode(null);
	    trans.setCountyNexusTypeCode(null);
	    trans.setCityNexusTypeCode(null);
	    trans.setCountryPrimarySitusCode(null);
	    trans.setCountryPrimaryTaxtypeCode(null);
	    trans.setCountrySecondarySitusCode(null);
	    trans.setCountrySecondaryTaxtypeCode(null);
	    trans.setCountryAllLevelsFlag(null);
	    trans.setStatePrimarySitusCode(null);
	    trans.setStatePrimaryTaxtypeCode(null);
	    trans.setStateSecondarySitusCode(null);
	    trans.setStateSecondaryTaxtypeCode(null);
	    trans.setStateAllLevelsFlag(null);
	    trans.setCountyPrimarySitusCode(null);
	    trans.setCountyPrimaryTaxtypeCode(null);
	    trans.setCountySecondarySitusCode(null);
	    trans.setCountySecondaryTaxtypeCode(null);
	    trans.setCountyAllLevelsFlag(null);
	    trans.setCityPrimarySitusCode(null);
	    trans.setCityPrimaryTaxtypeCode(null);
	    trans.setCitySecondarySitusCode(null);
	    trans.setCitySecondaryTaxtypeCode(null);
	    trans.setCityAllLevelsFlag(null);
	    trans.setStj1PrimarySitusCode(null);
	    trans.setStj1PrimaryTaxtypeCode(null);
	    trans.setStj1SecondarySitusCode(null);
	    trans.setStj1SecondaryTaxtypeCode(null);
	    trans.setStj1AllLevelsFlag(null);
	    trans.setStj2PrimarySitusCode(null);
	    trans.setStj2PrimaryTaxtypeCode(null);
	    trans.setStj2SecondarySitusCode(null);
	    trans.setStj2SecondaryTaxtypeCode(null);
	    trans.setStj2AllLevelsFlag(null);
	    trans.setStj3PrimarySitusCode(null);
	    trans.setStj3PrimaryTaxtypeCode(null);
	    trans.setStj3SecondarySitusCode(null);
	    trans.setStj3SecondaryTaxtypeCode(null);
	    trans.setStj3AllLevelsFlag(null);
	    trans.setStj4PrimarySitusCode(null);
	    trans.setStj4PrimaryTaxtypeCode(null);
	    trans.setStj4SecondarySitusCode(null);
	    trans.setStj4SecondaryTaxtypeCode(null);
	    trans.setStj4AllLevelsFlag(null);
	    trans.setStj5PrimarySitusCode(null);
	    trans.setStj5PrimaryTaxtypeCode(null);
	    trans.setStj5SecondarySitusCode(null);
	    trans.setStj5SecondaryTaxtypeCode(null);
	    trans.setStj5AllLevelsFlag(null);
	    trans.setStj6PrimarySitusCode(null);
	    trans.setStj6PrimaryTaxtypeCode(null);
	    trans.setStj6SecondarySitusCode(null);
	    trans.setStj6SecondaryTaxtypeCode(null);
	    trans.setStj6AllLevelsFlag(null);
	    trans.setStj7PrimarySitusCode(null);
	    trans.setStj7PrimaryTaxtypeCode(null);
	    trans.setStj7SecondarySitusCode(null);
	    trans.setStj7SecondaryTaxtypeCode(null);
	    trans.setStj7AllLevelsFlag(null);
	    trans.setStj8PrimarySitusCode(null);
	    trans.setStj8PrimaryTaxtypeCode(null);
	    trans.setStj8SecondarySitusCode(null);
	    trans.setStj8SecondaryTaxtypeCode(null);
	    trans.setStj8AllLevelsFlag(null);
	    trans.setStj9PrimarySitusCode(null);
	    trans.setStj9PrimaryTaxtypeCode(null);
	    trans.setStj9SecondarySitusCode(null);
	    trans.setStj9SecondaryTaxtypeCode(null);
	    trans.setStj9AllLevelsFlag(null);
	    trans.setStj10PrimarySitusCode(null);
	    trans.setStj10PrimaryTaxtypeCode(null);
	    trans.setStj10SecondarySitusCode(null);
	    trans.setStj10SecondaryTaxtypeCode(null);
	    trans.setStj10AllLevelsFlag(null);
	}
    
    private Boolean locationMatrixIdPresent(MicroApiTransaction trans) {
    	Boolean locationmatrixIdFound = false;
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode())){
			
			if(trans.getShiptoLocnMatrixId()!=null && trans.getShiptoLocnMatrixId()>0L){
				//Shipto
				locationmatrixIdFound = true;
			}		
			else if(trans.getShipfromLocnMatrixId()!=null && trans.getShipfromLocnMatrixId()>0L){
				//Shipfrom
				locationmatrixIdFound = true;
			}		
			else if(trans.getOrdracptLocnMatrixId()!=null && trans.getOrdracptLocnMatrixId()>0L){
				//Ordracpt
				locationmatrixIdFound = true;
			}	
			else if(trans.getOrdrorgnLocnMatrixId()!=null && trans.getOrdrorgnLocnMatrixId()>0L){
				//Ordrorgn
				locationmatrixIdFound = true;
			}	
			else if(trans.getFirstuseLocnMatrixId()!=null && trans.getFirstuseLocnMatrixId()>0L){
				//Firstuse
				locationmatrixIdFound = true;
			}	
			else if(trans.getBilltoLocnMatrixId()!=null && trans.getBilltoLocnMatrixId()>0L){
				//Billto
				locationmatrixIdFound = true;
			}	
			else if(trans.getTtlxfrLocnMatrixId()!=null && trans.getTtlxfrLocnMatrixId()>0L){
				//Ttlxfr
				locationmatrixIdFound = true;
			}
		}
			
			return locationmatrixIdFound;
    }
	    
    public void getTransSuspend(PurchaseTransaction trans) throws PurchaseTransactionProcessingException {

        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
       
        
        if(!("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
        		"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_RATE".equalsIgnoreCase(trans.getSuspendCode()))){
        	//Suspend flag not found
        	trans.setErrorCode(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        	trans.setAbortFlag(true);
			throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        }
			
        String transactionInd = null;
        String suspendInd = null;	
		
        //SuspendLocnFlag
		boolean locationmatrixIdFound = false;
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode())){
			
			if(trans.getShiptoLocnMatrixId()!=null && trans.getShiptoLocnMatrixId()>0L){
				//Shipto
				locationmatrixIdFound = true;
			}		
			else if(trans.getShipfromLocnMatrixId()!=null && trans.getShipfromLocnMatrixId()>0L){
				//Shipfrom
				locationmatrixIdFound = true;
			}		
			else if(trans.getOrdracptLocnMatrixId()!=null && trans.getOrdracptLocnMatrixId()>0L){
				//Ordracpt
				locationmatrixIdFound = true;
			}	
			else if(trans.getOrdrorgnLocnMatrixId()!=null && trans.getOrdrorgnLocnMatrixId()>0L){
				//Ordrorgn
				locationmatrixIdFound = true;
			}	
			else if(trans.getFirstuseLocnMatrixId()!=null && trans.getFirstuseLocnMatrixId()>0L){
				//Firstuse
				locationmatrixIdFound = true;
			}	
			else if(trans.getBilltoLocnMatrixId()!=null && trans.getBilltoLocnMatrixId()>0L){
				//Billto
				locationmatrixIdFound = true;
			}	
			else if(trans.getTtlxfrLocnMatrixId()!=null && trans.getTtlxfrLocnMatrixId()>0L){
				//Ttlxfr
				locationmatrixIdFound = true;
			}
			
			//SuspendLocn
			if(transactionInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("L");
				
				transactionInd = "S";	
				suspendInd = "L";
			}
			
			suspendLocnFlag(trans, false);
		}
		
		//SuspendGSFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendGS
			if(transactionInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("T");
				
				transactionInd = "S";					
				suspendInd = "T";
			}
			
			if(trans.getTaxMatrixId()!=null && trans.getTaxMatrixId()>0L){
				suspendGS(trans);
			}
		}
			
		//SuspendTaxCodeDtlFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
				"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode())){   //if("SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendTaxCodeDtl
			if(suspendInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("D");
				
				transactionInd = "S";					
				suspendInd = "D";
			}
			
			suspendTaxCodeDtl(trans);
		}
		
		//SuspendRateFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
				"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_RATE".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendRate
			if(suspendInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("R");
				
				transactionInd = "S";					
				suspendInd = "R";
			}
			
			suspendRateFlag(trans);
		}
		
		if(suspendInd !=null){
			suspAmts(trans, false);
		}
    }
    
    public void getTransSuspend(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);

    	if(!(trans.getSuspendCode()!=null && trans.getSuspendCode().length()>0)){
        	//Suspend flag not found
        	trans.setErrorCode(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        	trans.setAbortFlag(true);
			throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        }
        
        
        if(!("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
        		"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_RATE".equalsIgnoreCase(trans.getSuspendCode()))){
        	//Suspend flag not found
        	trans.setErrorCode(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        	trans.setAbortFlag(true);
			throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        }
			
        String transactionInd = null;
        String suspendInd = null;	
		
        //SuspendLocnFlag
		boolean locationmatrixIdFound = false;
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode())){
			
			if(trans.getShiptoLocnMatrixId()!=null && trans.getShiptoLocnMatrixId()>0L){
				//Shipto
				locationmatrixIdFound = true;
			}		
			else if(trans.getShipfromLocnMatrixId()!=null && trans.getShipfromLocnMatrixId()>0L){
				//Shipfrom
				locationmatrixIdFound = true;
			}		
			else if(trans.getOrdracptLocnMatrixId()!=null && trans.getOrdracptLocnMatrixId()>0L){
				//Ordracpt
				locationmatrixIdFound = true;
			}	
			else if(trans.getOrdrorgnLocnMatrixId()!=null && trans.getOrdrorgnLocnMatrixId()>0L){
				locationmatrixIdFound = true;
			}	
			else if(trans.getFirstuseLocnMatrixId()!=null && trans.getFirstuseLocnMatrixId()>0L){
				//Firstuse
				locationmatrixIdFound = true;
			}	
			else if(trans.getBilltoLocnMatrixId()!=null && trans.getBilltoLocnMatrixId()>0L){
				//Billto
				locationmatrixIdFound = true;
			}	
			else if(trans.getTtlxfrLocnMatrixId()!=null && trans.getTtlxfrLocnMatrixId()>0L){
				//Ttlxfr
				locationmatrixIdFound = true;
			}
			
			//SuspendLocn
			if(transactionInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("L");
				
				transactionInd = "S";	
				suspendInd = "L";
			}
			
			suspendLocnFlag(trans, false);
		}
		
		//SuspendGSFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendGS
			if(transactionInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("T");
				
				transactionInd = "S";					
				suspendInd = "T";
			}
			
			if(trans.getTaxMatrixId()!=null && trans.getTaxMatrixId()>0L){
				suspendGS(trans);
			}
		}
			
		//SuspendTaxCodeDtlFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
				"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode())){   //if("SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendTaxCodeDtl
			if(suspendInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("D");
				
				transactionInd = "S";					
				suspendInd = "D";
			}
			
			suspendTaxCodeDtl(trans);
		}
		
		//SuspendRateFlag
		if("SUSPEND_LOCN".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_GS".equalsIgnoreCase(trans.getSuspendCode()) ||
				"SUSPEND_TAXCODE_DTL".equalsIgnoreCase(trans.getSuspendCode()) || "SUSPEND_RATE".equalsIgnoreCase(trans.getSuspendCode())){
			//SuspendRate
			if(suspendInd==null){
				trans.setTransactionInd("S");	
				trans.setSuspendInd("R");
				
				transactionInd = "S";					
				suspendInd = "R";
			}
			
			suspendRateFlag(trans);
		}
		
		if(suspendInd !=null){
			suspAmts(trans, false);
		}
		
    }

    public void getTransReprocess(PurchaseTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
			
		suspendGS(trans);
		suspendTaxCodeDtl(trans);
		suspendLocnFlag(trans, true);
		
		suspendRateFlag(trans);
		suspAmts(trans, false);
		
		trans.setTransactionInd(null);
		trans.setSuspendInd(null);
    }
    
    public void getTransReprocess(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
			
		suspendGS(trans);
		suspendTaxCodeDtl(trans);
		suspendLocnFlag(trans, true);
		
		suspendRateFlag(trans);
		suspAmts(trans, false);
		
		trans.setTransactionInd(null);
		trans.setSuspendInd(null);
    }
    
    public void getPurchaseTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag) throws PurchaseTransactionProcessingException {

    	String errorCode = null;
    	
    	Long processBatchNo = responseBatch.getBatchId();
    	if(processBatchNo==null || processBatchNo.equals(0L)){
            throw new PurchaseTransactionProcessingException("P8");
    	}
    	
    	BatchMaintenance batch = batchMaintenanceDAO.findByBatchId(processBatchNo);
    	if(batch==null){
            throw new PurchaseTransactionProcessingException("P8");
    	}
    	
    	if((batch.getBatchStatusCode()==null || !batch.getBatchStatusCode().equalsIgnoreCase("FP")) || (batch.getHeldFlag()!=null && batch.getHeldFlag().equalsIgnoreCase("1"))){
            throw new PurchaseTransactionProcessingException("P9");
    	}
    	
    	int totalCount = 0;
    	totalCount = bcpPurchaseTransactionDAO.findBCPCountByBatchId(processBatchNo);
    	
    	//Update Batch as Processing
    	responseBatch.setBatchStatusCode("XP");
    	responseBatch.setTotalRows(new Long(totalCount));
		
    	batch.setBatchStatusCode("XP");
//		batch.setSchedStartTimestamp(new Date());
		batch.setUpdateTimestamp(new Date());
		batch.setStatusUpdateTimestamp(new Date());	
		try {
			batch.setStatusUpdateUserId(SecurityContextHolder.getContext().getAuthentication().getName());
		}
		catch(Exception e) {
			batch.setStatusUpdateUserId("STSCORP");
		}
		batch.setActualStartTimestamp(new Date());	
		
		Long lastId = purchaseTransactionDAO.findLastPurchaseTransactionId();
		if(lastId!=null){
			batch.setStartRow(lastId);//
		} 
		
		batchMaintenanceDAO.update(batch);
		
		//Loop Trans In Batch
		int firstRow = 0;

		boolean newStart = true;
		String vendorCode = "";
		String invoiceNbr = "";
		Date invoiceDate = null;
		
		int rowsPerRequest = 1000;
		int numberRequest = 0;
		int totalCountActual = 0;

		numberRequest = (int)(totalCount/rowsPerRequest);
		if(totalCount%rowsPerRequest > 0){
			numberRequest = numberRequest + 1;
		}

		System.out.println("   ... processBatchNo: " + processBatchNo);
		System.out.println("   ... rowsPerRequest: " + rowsPerRequest);
		System.out.println("   ... totalCount: " + totalCount);
		System.out.println("   ... numberRequest: " + numberRequest);

		List<PurchaseTransaction> transactionsForDocument = null;

		boolean sameVendorCode = true;
		boolean sameInvoiceNbr = true;
		boolean sameInvoiceDate = true;
		boolean oneNull = true;

		for(int i=0; i<numberRequest; i++){

			if(errorCode!=null){
				break; //Abort
			}

			firstRow = (i*rowsPerRequest);
			List<BCPPurchaseTransaction> transactions = bcpPurchaseTransactionDAO.findBCPPurchaseTransactionsByBatchId(processBatchNo, firstRow, rowsPerRequest);

			for(BCPPurchaseTransaction bcpPurchaseTransaction: transactions) {
				totalCountActual++;
				//Increment Counter Here
				//Chnge from Purchtrans ID to tb_process_count.NEXTVAL
				batchMaintenanceDAO.incProcessCount();
				if(newStart){
					transactionsForDocument = new LinkedList<PurchaseTransaction>();
					vendorCode = bcpPurchaseTransaction.getVendorCode();
					invoiceNbr = bcpPurchaseTransaction.getInvoiceNbr();
					invoiceDate = bcpPurchaseTransaction.getInvoiceDate();
				}

				if((vendorCode==null || vendorCode.length()==0) && (bcpPurchaseTransaction.getVendorCode()==null || bcpPurchaseTransaction.getVendorCode().length()==0)){
					sameVendorCode = true;
				}
				else if((vendorCode!=null && bcpPurchaseTransaction.getVendorCode()!=null) && vendorCode.equalsIgnoreCase(bcpPurchaseTransaction.getVendorCode())){
					sameVendorCode = true;
				}
				else{
					sameVendorCode = false;
				}

				if((invoiceNbr==null || invoiceNbr.length()==0) && (bcpPurchaseTransaction.getInvoiceNbr()==null || bcpPurchaseTransaction.getInvoiceNbr().length()==0)){
					sameInvoiceNbr = true;
				}
				else if((invoiceNbr!=null && bcpPurchaseTransaction.getInvoiceNbr()!=null) && invoiceNbr.equalsIgnoreCase(bcpPurchaseTransaction.getInvoiceNbr())){
					sameInvoiceNbr = true;
				}
				else{
					sameInvoiceNbr = false;
				}

				if(invoiceDate==null && bcpPurchaseTransaction.getInvoiceDate()==null){
					sameInvoiceDate = true;
				}
				else if((invoiceDate!=null && bcpPurchaseTransaction.getInvoiceDate()!=null) && invoiceDate.getTime()==bcpPurchaseTransaction.getInvoiceDate().getTime()){
					sameInvoiceDate = true;
				}
				else{
					sameInvoiceDate = false;
				}

				oneNull = false;
				if(sameVendorCode && sameInvoiceNbr && sameInvoiceDate){
					if((bcpPurchaseTransaction.getVendorCode()==null || bcpPurchaseTransaction.getVendorCode().length()==0) ||
						(bcpPurchaseTransaction.getInvoiceNbr()==null || bcpPurchaseTransaction.getInvoiceNbr().length()==0) ||
						  bcpPurchaseTransaction.getInvoiceDate()==null){
						oneNull = true;
					}
				}

				if(!(sameVendorCode && sameInvoiceNbr && sameInvoiceDate) || (oneNull && !newStart)){
					PurchaseTransactionDocument document = new PurchaseTransactionDocument();
					document.setPurchaseTransaction(transactionsForDocument);
					
					try {					
						MicroApiTransactionDocument microApiTransactionDocument =  new MicroApiTransactionDocument(document);
						getPurchaseTransactionEvaluateForAccrual(microApiTransactionDocument, true, LogSourceEnum.getPurchaseTransactionProcessBatch);
						microApiTransactionDocument.updatePurchaseTransactionDocument(document);					
					} catch (Exception e) {
						e.printStackTrace();
						errorCode = e.getMessage();
					}

					transactionsForDocument = new LinkedList<PurchaseTransaction>();
					vendorCode = bcpPurchaseTransaction.getVendorCode();
					invoiceNbr = bcpPurchaseTransaction.getInvoiceNbr();
					invoiceDate = bcpPurchaseTransaction.getInvoiceDate();
				}

				newStart = false;

				PurchaseTransaction purchaseTransaction = new PurchaseTransaction();
				try {
					//Copy property values from the origin bean to the destination bean for all cases where the property names are the same.
					BeanUtils.copyProperties(bcpPurchaseTransaction, purchaseTransaction);

					if(defaultFlag!=null && "1".equals(defaultFlag)){
						purchaseTransaction.setDefaultMatrixFlag("1");
					}
				} catch (Exception e) {
					e.printStackTrace();
					errorCode = e.getMessage();
				}

				transactionsForDocument.add(purchaseTransaction);
			}
		}
		
		//Final flush
		if(errorCode==null && transactionsForDocument!=null && transactionsForDocument.size()>0){
			PurchaseTransactionDocument document = new PurchaseTransactionDocument();
			document.setPurchaseTransaction(transactionsForDocument);

			try {	
				MicroApiTransactionDocument microApiTransactionDocument =  new MicroApiTransactionDocument(document);
				getPurchaseTransactionEvaluateForAccrual(microApiTransactionDocument, true);
				microApiTransactionDocument.updatePurchaseTransactionDocument(document);	
			} catch (Exception e) {
				e.printStackTrace();
				errorCode = e.getMessage();
			}

		}

		batch.setProcessedRows(new Long(totalCountActual));
		if(errorCode!=null){
			batch.setBatchStatusCode("ABP");
			batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
						
			responseBatch.setBatchStatusCode("ABP");
	    	responseBatch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
		}
		else{
			batch.setBatchStatusCode("P");
			batch.setProcessedRows(new Long(totalCountActual));
			
			responseBatch.setBatchStatusCode("P");
		}
//		batch.setSchedStartTimestamp(new Date());
		batch.setUpdateTimestamp(new Date());
		batch.setStatusUpdateTimestamp(new Date());
		batch.setActualEndTimestamp(new Date());
		try {
			batch.setStatusUpdateUserId(SecurityContextHolder.getContext().getAuthentication().getName());
		}
		catch(Exception e) {
			batch.setStatusUpdateUserId("STSCORP");
		}
		batchMaintenanceDAO.update(batch);

		System.out.println("   ... totalCountActual: " + totalCountActual);

		//Delete Batch from Staging
		bcpPurchaseTransactionDAO.deleteBCPPurchaseTransactionsByBatchId(processBatchNo);

		if(errorCode!=null){
			throw new PurchaseTransactionProcessingException(errorCode);
		}
    }
    
    public void getBillTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag) throws PurchaseTransactionProcessingException {
    	getBillTransactionProcessBatch(responseBatch, defaultFlag,LogSourceEnum.BillTransactionProcessBatch);
    
    }
    
    public void getBillTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException {

    	String errorCode = null;
    	
    	Long processBatchNo = responseBatch.getBatchId();
    	if(processBatchNo==null || processBatchNo.equals(0L)){
            throw new PurchaseTransactionProcessingException("P8");
    	}
    	
    	BatchMaintenance batch = batchMaintenanceDAO.findByBatchId(processBatchNo);
    	if(batch==null){
            throw new PurchaseTransactionProcessingException("P8");
    	}
    	
    	if((batch.getBatchStatusCode()==null || !batch.getBatchStatusCode().equalsIgnoreCase("FP")) || (batch.getHeldFlag()!=null && batch.getHeldFlag().equalsIgnoreCase("1"))){
            throw new PurchaseTransactionProcessingException("P9");
    	}
    	
    	int totalCount = 0;
    	totalCount = bcpSaleTransactionDAO.findBCPCountByBatchId(processBatchNo);
    	
    	//Update Batch as Processing
    	responseBatch.setBatchStatusCode("XP");
    	responseBatch.setTotalRows(new Long(totalCount));
		
    	batch.setBatchStatusCode("XP");
		batch.setSchedStartTimestamp(new Date());
		batch.setUpdateTimestamp(new Date());
		batch.setStatusUpdateTimestamp(new Date());	
		try {
			batch.setStatusUpdateUserId(SecurityContextHolder.getContext().getAuthentication().getName());
		}
		catch(Exception e) {
			batch.setStatusUpdateUserId("STSCORP");
		}
		batch.setActualStartTimestamp(new Date());	
		
		Long lastId =billTransactionDAO.findLastBillTransactionId();
		if(lastId!=null){
			batch.setStartRow(lastId);//
		} 
		
		batchMaintenanceDAO.update(batch);
		
		//Loop Trans In Batch
		int firstRow = 0;

		boolean newStart = true;
		String customerNumber = "";
		String invoiceNbr = "";
		Date invoiceDate = null;
		
		int rowsPerRequest = 1000;
		int numberRequest = 0;
		int totalCountActual = 0;

		numberRequest = (int)(totalCount/rowsPerRequest);
		if(totalCount%rowsPerRequest > 0){
			numberRequest = numberRequest + 1;
		}

		System.out.println("   ... processBatchNo: " + processBatchNo);
		System.out.println("   ... rowsPerRequest: " + rowsPerRequest);
		System.out.println("   ... totalCount: " + totalCount);
		System.out.println("   ... numberRequest: " + numberRequest);

		List<BillTransaction> transactionsForDocument = null;

		boolean sameCustomerNumber = true;
		boolean sameInvoiceNbr = true;
		boolean sameInvoiceDate = true;
		boolean oneNull = true;

		for(int i=0; i<numberRequest; i++){

			if(errorCode!=null){
				break; //Abort
			}

			firstRow = (i*rowsPerRequest);
			List<BCPBillTransaction> transactions = bcpSaleTransactionDAO.findBCPBillTransactionsByBatchId(processBatchNo, firstRow, rowsPerRequest);

			for(BCPBillTransaction bcpBillTransaction: transactions) {
				totalCountActual++;

				if(newStart){
					transactionsForDocument = new LinkedList<BillTransaction>();
					customerNumber = bcpBillTransaction.getCustNbr();
					invoiceNbr = bcpBillTransaction.getInvoiceNbr();
					invoiceDate = bcpBillTransaction.getInvoiceDate();
				}

				if((customerNumber==null || customerNumber.length()==0) && (bcpBillTransaction.getCustNbr()==null || bcpBillTransaction.getCustNbr().length()==0)){
					sameCustomerNumber = true;
				}
				else if((customerNumber!=null && bcpBillTransaction.getCustNbr()!=null) && customerNumber.equalsIgnoreCase(bcpBillTransaction.getCustNbr())){
					sameCustomerNumber = true;
				}
				else{
					sameCustomerNumber = false;
				}

				if((invoiceNbr==null || invoiceNbr.length()==0) && (bcpBillTransaction.getInvoiceNbr()==null || bcpBillTransaction.getInvoiceNbr().length()==0)){
					sameInvoiceNbr = true;
				}
				else if((invoiceNbr!=null && bcpBillTransaction.getInvoiceNbr()!=null) && invoiceNbr.equalsIgnoreCase(bcpBillTransaction.getInvoiceNbr())){
					sameInvoiceNbr = true;
				}
				else{
					sameInvoiceNbr = false;
				}

				if(invoiceDate==null && bcpBillTransaction.getInvoiceDate()==null){
					sameInvoiceDate = true;
				}
				else if((invoiceDate!=null && bcpBillTransaction.getInvoiceDate()!=null) && invoiceDate.getTime()==bcpBillTransaction.getInvoiceDate().getTime()){
					sameInvoiceDate = true;
				}
				else{
					sameInvoiceDate = false;
				}

				oneNull = false;
				if(sameCustomerNumber && sameInvoiceNbr && sameInvoiceDate){
					if((bcpBillTransaction.getCustNbr()==null || bcpBillTransaction.getCustNbr().length()==0) ||
						(bcpBillTransaction.getInvoiceNbr()==null || bcpBillTransaction.getInvoiceNbr().length()==0) ||
						  bcpBillTransaction.getInvoiceDate()==null){
						oneNull = true;
					}
				}

				if(!(sameCustomerNumber && sameInvoiceNbr && sameInvoiceDate) || (oneNull && !newStart)){
					BillTransactionDocument document = new BillTransactionDocument();
					document.setBillTransaction(transactionsForDocument);

					try {					
						MicroApiTransactionDocument microApiTransactionDocument =  new MicroApiTransactionDocument(document);
						billTransactionProcess(microApiTransactionDocument, true, callingFromMethod);  
						microApiTransactionDocument.updateBillTransactionDocument(document);					
					} catch (Exception e) {
						e.printStackTrace();
						errorCode = e.getMessage();
					}

					transactionsForDocument = new LinkedList<BillTransaction>();
					customerNumber = bcpBillTransaction.getCustNbr();
					invoiceNbr = bcpBillTransaction.getInvoiceNbr();
					invoiceDate = bcpBillTransaction.getInvoiceDate();
				}

				newStart = false;

				BillTransaction billTransaction = new BillTransaction();
				try {
					//Copy property values from the origin bean to the destination bean for all cases where the property names are the same.
					BeanUtils.copyProperties(bcpBillTransaction, billTransaction);

					if(defaultFlag!=null && "1".equals(defaultFlag)){
						billTransaction.setDefaultMatrixFlag("1");
					}
				} catch (Exception e) {
					e.printStackTrace();
					errorCode = e.getMessage();
				}

				transactionsForDocument.add(billTransaction);
			}
		}
		
		//Final flush
		if(errorCode==null && transactionsForDocument!=null && transactionsForDocument.size()>0){
			BillTransactionDocument document = new BillTransactionDocument();
			document.setBillTransaction(transactionsForDocument);

			try {	
				MicroApiTransactionDocument microApiTransactionDocument =  new MicroApiTransactionDocument(document);
				billTransactionProcess(microApiTransactionDocument, true, LogSourceEnum.BillTransactionProcessBatch);
				microApiTransactionDocument.updateBillTransactionDocument(document);	
			} catch (Exception e) {
				e.printStackTrace();
				errorCode = e.getMessage();
			}

		}

		batch.setProcessedRows(new Long(totalCountActual));
		if(errorCode!=null){
			batch.setBatchStatusCode("ABP");
			batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
						
			responseBatch.setBatchStatusCode("ABP");
	    	responseBatch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
		}
		else{
			batch.setBatchStatusCode("P");
			batch.setProcessedRows(new Long(totalCountActual));
			
			responseBatch.setBatchStatusCode("P");
		}
		batch.setSchedStartTimestamp(new Date());
		batch.setUpdateTimestamp(new Date());
		batch.setStatusUpdateTimestamp(new Date());
		batch.setActualEndTimestamp(new Date());
		try {
			batch.setStatusUpdateUserId(SecurityContextHolder.getContext().getAuthentication().getName());
		}
		catch(Exception e) {
			batch.setStatusUpdateUserId("STSCORP");
		}
		batchMaintenanceDAO.update(batch);

		System.out.println("   ... totalCountActual: " + totalCountActual);

		//Delete Batch from Staging
		bcpSaleTransactionDAO.deleteBCPBillTransactionsByBatchId(processBatchNo);

		if(errorCode!=null){
			throw new PurchaseTransactionProcessingException(errorCode);
		}
    }
    
    public void getTransVariance(PurchaseTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
    	
    	transientDetails.setCountryTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCountryTier1TaxAmt(), trans.getCountryTier2TaxAmt()), trans.getCountryTier3TaxAmt()));
    	transientDetails.setStateTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getStateTier1TaxAmt(), trans.getStateTier2TaxAmt()), trans.getStateTier3TaxAmt()));
    	transientDetails.setCountyTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCountyTier1TaxAmt(), trans.getCountyTier2TaxAmt()), trans.getCountyTier3TaxAmt()));
    	transientDetails.setCityTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCityTier1TaxAmt(), trans.getCityTier2TaxAmt()), trans.getCityTier3TaxAmt()));
    	trans.setDiffTaxAmt(ArithmeticUtils.subtract(trans.getTbCalcTaxAmt(), trans.getCompTaxAmt()));
    	
    	trans.setDiffCountryTaxAmt(ArithmeticUtils.subtract(transientDetails.getCountryTaxAmt(), trans.getCompCountryTaxAmt()));
    	trans.setDiffCountryTier1TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier1TaxAmt(), trans.getCompCountryTier1TaxAmt()));
    	trans.setDiffCountryTier2TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier2TaxAmt(), trans.getCompCountryTier2TaxAmt()));
    	trans.setDiffCountryTier3TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier3TaxAmt(), trans.getCompCountryTier3TaxAmt()));
    	
    	trans.setDiffStateTaxAmt(ArithmeticUtils.subtract(transientDetails.getStateTaxAmt(), trans.getCompStateTaxAmt()));
    	trans.setDiffStateTier1TaxAmt(ArithmeticUtils.subtract(trans.getStateTier1TaxAmt(), trans.getCompStateTier1TaxAmt()));
    	trans.setDiffStateTier2TaxAmt(ArithmeticUtils.subtract(trans.getStateTier2TaxAmt(), trans.getCompStateTier2TaxAmt()));
    	trans.setDiffStateTier3TaxAmt(ArithmeticUtils.subtract(trans.getStateTier3TaxAmt(), trans.getCompStateTier3TaxAmt()));
    	
    	trans.setDiffCountyTaxAmt(ArithmeticUtils.subtract(transientDetails.getCountyTaxAmt(), trans.getCompCountyTaxAmt()));
    	trans.setDiffCountyTier1TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier1TaxAmt(), trans.getCompCountyTier1TaxAmt()));
    	trans.setDiffCountyTier2TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier2TaxAmt(), trans.getCompCountyTier2TaxAmt()));
    	trans.setDiffCountyTier3TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier3TaxAmt(), trans.getCompCountyTier3TaxAmt()));
    	
    	trans.setDiffCityTaxAmt(ArithmeticUtils.subtract(transientDetails.getCityTaxAmt(), trans.getCompCityTaxAmt()));
    	trans.setDiffCityTier1TaxAmt(ArithmeticUtils.subtract(trans.getCityTier1TaxAmt(), trans.getCompCityTier1TaxAmt()));
    	trans.setDiffCityTier2TaxAmt(ArithmeticUtils.subtract(trans.getCityTier2TaxAmt(), trans.getCompCityTier2TaxAmt()));
    	trans.setDiffCityTier3TaxAmt(ArithmeticUtils.subtract(trans.getCityTier3TaxAmt(), trans.getCompCityTier3TaxAmt()));
    	
    	trans.setDiffStj1TaxAmt(ArithmeticUtils.subtract(trans.getStj1TaxAmt(), trans.getCompStj1TaxAmt()));
    	trans.setDiffStj2TaxAmt(ArithmeticUtils.subtract(trans.getStj2TaxAmt(), trans.getCompStj2TaxAmt()));
    	trans.setDiffStj3TaxAmt(ArithmeticUtils.subtract(trans.getStj3TaxAmt(), trans.getCompStj3TaxAmt()));
    	trans.setDiffStj4TaxAmt(ArithmeticUtils.subtract(trans.getStj4TaxAmt(), trans.getCompStj4TaxAmt()));
    	trans.setDiffStj5TaxAmt(ArithmeticUtils.subtract(trans.getStj5TaxAmt(), trans.getCompStj5TaxAmt()));
    	trans.setDiffStj6TaxAmt(ArithmeticUtils.subtract(trans.getStj6TaxAmt(), trans.getCompStj6TaxAmt()));
    	trans.setDiffStj7TaxAmt(ArithmeticUtils.subtract(trans.getStj7TaxAmt(), trans.getCompStj7TaxAmt()));
    	trans.setDiffStj8TaxAmt(ArithmeticUtils.subtract(trans.getStj8TaxAmt(), trans.getCompStj8TaxAmt()));
    	trans.setDiffStj9TaxAmt(ArithmeticUtils.subtract(trans.getStj9TaxAmt(), trans.getCompStj9TaxAmt()));
    	trans.setDiffStj10TaxAmt(ArithmeticUtils.subtract(trans.getStj10TaxAmt(), trans.getCompStj10TaxAmt()));
    }
    
public void getTransVariance(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();
    	
    	transientDetails.setCountryTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCountryTier1TaxAmt(), trans.getCountryTier2TaxAmt()), trans.getCountryTier3TaxAmt()));
    	transientDetails.setStateTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getStateTier1TaxAmt(), trans.getStateTier2TaxAmt()), trans.getStateTier3TaxAmt()));
    	transientDetails.setCountyTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCountyTier1TaxAmt(), trans.getCountyTier2TaxAmt()), trans.getCountyTier3TaxAmt()));
    	transientDetails.setCityTaxAmt(ArithmeticUtils.add(ArithmeticUtils.add(trans.getCityTier1TaxAmt(), trans.getCityTier2TaxAmt()), trans.getCityTier3TaxAmt()));
    	trans.setDiffTaxAmt(ArithmeticUtils.subtract(trans.getTbCalcTaxAmt(), trans.getCompTaxAmt()));
    	
    	trans.setDiffCountryTaxAmt(ArithmeticUtils.subtract(transientDetails.getCountryTaxAmt(), trans.getCompCountryTaxAmt()));
    	trans.setDiffCountryTier1TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier1TaxAmt(), trans.getCompCountryTier1TaxAmt()));
    	trans.setDiffCountryTier2TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier2TaxAmt(), trans.getCompCountryTier2TaxAmt()));
    	trans.setDiffCountryTier3TaxAmt(ArithmeticUtils.subtract(trans.getCountryTier3TaxAmt(), trans.getCompCountryTier3TaxAmt()));
    	
    	trans.setDiffStateTaxAmt(ArithmeticUtils.subtract(transientDetails.getStateTaxAmt(), trans.getCompStateTaxAmt()));
    	trans.setDiffStateTier1TaxAmt(ArithmeticUtils.subtract(trans.getStateTier1TaxAmt(), trans.getCompStateTier1TaxAmt()));
    	trans.setDiffStateTier2TaxAmt(ArithmeticUtils.subtract(trans.getStateTier2TaxAmt(), trans.getCompStateTier2TaxAmt()));
    	trans.setDiffStateTier3TaxAmt(ArithmeticUtils.subtract(trans.getStateTier3TaxAmt(), trans.getCompStateTier3TaxAmt()));
    	
    	trans.setDiffCountyTaxAmt(ArithmeticUtils.subtract(transientDetails.getCountyTaxAmt(), trans.getCompCountyTaxAmt()));
    	trans.setDiffCountyTier1TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier1TaxAmt(), trans.getCompCountyTier1TaxAmt()));
    	trans.setDiffCountyTier2TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier2TaxAmt(), trans.getCompCountyTier2TaxAmt()));
    	trans.setDiffCountyTier3TaxAmt(ArithmeticUtils.subtract(trans.getCountyTier3TaxAmt(), trans.getCompCountyTier3TaxAmt()));
    	
    	trans.setDiffCityTaxAmt(ArithmeticUtils.subtract(transientDetails.getCityTaxAmt(), trans.getCompCityTaxAmt()));
    	trans.setDiffCityTier1TaxAmt(ArithmeticUtils.subtract(trans.getCityTier1TaxAmt(), trans.getCompCityTier1TaxAmt()));
    	trans.setDiffCityTier2TaxAmt(ArithmeticUtils.subtract(trans.getCityTier2TaxAmt(), trans.getCompCityTier2TaxAmt()));
    	trans.setDiffCityTier3TaxAmt(ArithmeticUtils.subtract(trans.getCityTier3TaxAmt(), trans.getCompCityTier3TaxAmt()));
    	
    	trans.setDiffStj1TaxAmt(ArithmeticUtils.subtract(trans.getStj1TaxAmt(), trans.getCompStj1TaxAmt()));
    	trans.setDiffStj2TaxAmt(ArithmeticUtils.subtract(trans.getStj2TaxAmt(), trans.getCompStj2TaxAmt()));
    	trans.setDiffStj3TaxAmt(ArithmeticUtils.subtract(trans.getStj3TaxAmt(), trans.getCompStj3TaxAmt()));
    	trans.setDiffStj4TaxAmt(ArithmeticUtils.subtract(trans.getStj4TaxAmt(), trans.getCompStj4TaxAmt()));
    	trans.setDiffStj5TaxAmt(ArithmeticUtils.subtract(trans.getStj5TaxAmt(), trans.getCompStj5TaxAmt()));
    	trans.setDiffStj6TaxAmt(ArithmeticUtils.subtract(trans.getStj6TaxAmt(), trans.getCompStj6TaxAmt()));
    	trans.setDiffStj7TaxAmt(ArithmeticUtils.subtract(trans.getStj7TaxAmt(), trans.getCompStj7TaxAmt()));
    	trans.setDiffStj8TaxAmt(ArithmeticUtils.subtract(trans.getStj8TaxAmt(), trans.getCompStj8TaxAmt()));
    	trans.setDiffStj9TaxAmt(ArithmeticUtils.subtract(trans.getStj9TaxAmt(), trans.getCompStj9TaxAmt()));
    	trans.setDiffStj10TaxAmt(ArithmeticUtils.subtract(trans.getStj10TaxAmt(), trans.getCompStj10TaxAmt()));
    }
    
    public void getTransRelease(PurchaseTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	if(trans.getTransactionInd()==null || !trans.getTransactionInd().equalsIgnoreCase("H")){
    		trans.setErrorCode(TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET);
        	trans.setAbortFlag(true);
    		throw new PurchaseTransactionProcessingException(TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET);
		}	

    	trans.setTransactionInd(TRANS_IND_PROCESSED);
		trans.setSuspendInd(null);
		trans.setManualTaxcodeInd("MR");
    }
    
    public void getTransRelease(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	if(trans.getTransactionInd()==null || !trans.getTransactionInd().equalsIgnoreCase("H")){
    		trans.setErrorCode(TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET);
        	trans.setAbortFlag(true);
    		throw new PurchaseTransactionProcessingException(TRANS_ERROR_TRANSACTIONIND_HELD_NOT_SET);
		}	

    	trans.setTransactionInd(TRANS_IND_PROCESSED);
		trans.setSuspendInd(null);
		trans.setManualTaxcodeInd("MR");
    }
    
    public void getTransReverse(PurchaseTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	suspAmts(trans, true);
    }
    
    public void getTransReverse(MicroApiTransaction trans) throws PurchaseTransactionProcessingException {
        
    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
    	
    	suspAmts(trans, true);
    }
    
    public void getTransVendorNexus(MicroApiTransaction trans, Boolean directPayPermitBoolean) throws PurchaseTransactionProcessingException {

    	if (trans == null) throw new PurchaseTransactionProcessingException(PROCESSING_ERROR_PS0);
        TransientPurchaseTransaction transientDetails = trans.getTransientTransaction();

    	trans.setCountryNexusTypeCode("*NO");
    	trans.setStateNexusTypeCode("*NO");
    	trans.setCountyNexusTypeCode("*NO");
    	trans.setCityNexusTypeCode("*NO");

    	if(trans.getStj1Name() != null) {
			trans.setStj1NexusTypeCode("*NO");
		}
		if(trans.getStj2Name() != null) {
			trans.setStj2NexusTypeCode("*NO");
		}
		if(trans.getStj3Name() != null) {
			trans.setStj3NexusTypeCode("*NO");
		}
		if(trans.getStj4Name() != null) {
			trans.setStj4NexusTypeCode("*NO");
		}
		if(trans.getStj5Name() != null) {
			trans.setStj5NexusTypeCode("*NO");
		}
		if(trans.getStj6Name() != null) {
			trans.setStj6NexusTypeCode("*NO");
		}
		if(trans.getStj7Name() != null) {
			trans.setStj7NexusTypeCode("*NO");
		}
		if(trans.getStj8Name() != null) {
			trans.setStj8NexusTypeCode("*NO");
		}
		if(trans.getStj9Name() != null) {
			trans.setStj9NexusTypeCode("*NO");
		}
		if(trans.getStj10Name() != null) {
			trans.setStj10NexusTypeCode("*NO");
		}
    	
    	if(!directPayPermitBoolean && (trans.getVendorNexusFlag()==null || trans.getVendorNexusFlag().length()==0)){
    		Option option = optionService.findByPK(new OptionCodePK ("VENDORNEXUSFLAG", "SYSTEM", "SYSTEM"));   
            if(option.getValue()!=null && option.getValue().length()>0){      
            	trans.setVendorNexusFlag(option.getValue());
            }
            else{
            	trans.setVendorNexusFlag("0");
            }
    	}
    	else{		
    	}
    	if(trans.getVendorNexusFlag()!=null && "1".equals(trans.getVendorNexusFlag())){
    		
    	}
    	else{
    		trans.setCountryVendorNexusDtlId(-1L);
    		trans.setStateVendorNexusDtlId(-1L);
    		trans.setCountyVendorNexusDtlId(-1L);
    		trans.setCityVendorNexusDtlId(-1L);
    		
    		trans.setCountryNexusTypeCode("*YES");
    		trans.setStateNexusTypeCode("*YES");
    		trans.setCountyNexusTypeCode("*YES");
    		trans.setCityNexusTypeCode("*YES");
    	
    		if(trans.getStj1Name() != null) {
    			trans.setStj1VendorNexusDtlId(-1L);
				trans.setStj1NexusTypeCode("*YES");
			}
			if(trans.getStj2Name() != null) {
				trans.setStj2VendorNexusDtlId(-1L);
				trans.setStj2NexusTypeCode("*YES");
			}
			if(trans.getStj3Name() != null) {
				trans.setStj3VendorNexusDtlId(-1L);
				trans.setStj3NexusTypeCode("*YES");
			}
			if(trans.getStj4Name() != null) {
				trans.setStj4VendorNexusDtlId(-1L);
				trans.setStj4NexusTypeCode("*YES");
			}
			if(trans.getStj5Name() != null) {
				trans.setStj5VendorNexusDtlId(-1L);
				trans.setStj5NexusTypeCode("*YES");
			}
			if(trans.getStj6Name() != null) {
				trans.setStj6VendorNexusDtlId(-1L);
				trans.setStj6NexusTypeCode("*YES");
			}
			if(trans.getStj7Name() != null) {
				trans.setStj7VendorNexusDtlId(-1L);
				trans.setStj7NexusTypeCode("*YES");
			}
			if(trans.getStj8Name() != null) {
				trans.setStj8VendorNexusDtlId(-1L);
				trans.setStj8NexusTypeCode("*YES");
			}
			if(trans.getStj9Name() != null) {
				trans.setStj9VendorNexusDtlId(-1L);
				trans.setStj9NexusTypeCode("*YES");
			}
			if(trans.getStj10Name() != null) {
				trans.setStj10VendorNexusDtlId(-1L);
				trans.setStj10NexusTypeCode("*YES");
			}
    		return;
    	}
    	
    	//Moved from the start of the method in order to not write out when Nexus is false.
        transientDetails.setProcessingLog(transientDetails.getProcessingLog() +" -"+ ProcessStep.transVendorNexus.name());

    	if(trans.getVendorId()!=null && trans.getVendorId()>0L){
    		//lookup tb_vendor by vendorId
    		VendorItem vendorItem = vendorItemDAO.findById(trans.getVendorId());
    		if(vendorItem==null) {
    			trans.setVendorId(0L);
    		}
    		else {
    			trans.setVendorCode(vendorItem.getVendorCode());
    		}
    	}
    	
    	if((trans.getVendorId()==null || trans.getVendorId()==0L) && (trans.getVendorCode()!=null && trans.getVendorCode().length()>0)) {
    		//lookup tb_vendor by vendorCode
    		VendorItem vendorItem = vendorItemDAO.findByCode(trans.getVendorCode());
    		if(vendorItem==null) {
    			trans.setVendorId(0L);
    			trans.setVendorCode(null);
    		}
    		else {
    			trans.setVendorId(vendorItem.getVendorId());
    		}
    	}
    	
    	if(trans.getVendorId()==null || trans.getVendorId()==0L) {
    		return;
    	}

		String vendorNexusWhere = "";
		String vendorNexusOrderby = "";
		
		String nexusindCode = "";

		String primaryCountryCode = trans.getCountryCode();
		String primaryStateCode = trans.getStateCode();
		String primaryCounty = trans.getCountyName();
		String primaryCity = trans.getCityName();
		
		//Check each Primary Nexus Jurisdictions
		//Loop through Jurisdictions in FORWARD order
		for(int jurIdx = 2; jurIdx <= 14; jurIdx++) {
			switch(jurIdx) {
			case 1: //jurIdx 1 = Country
				nexusindCode = "I";
				vendorNexusWhere =	" AND nexus_country_code = '" + primaryCountryCode + "'" +
                              		" AND nexus_state_code='*COUNTRY' " +
                              		" AND nexus_county='*COUNTRY' " +
                              		" AND nexus_city='*COUNTRY' " +
                              		" AND nexus_stj='*COUNTRY' ";
				vendorNexusOrderby = 	" ORDER BY tb_vendor_nexus_dtl.effective_date DESC ";
				break;
			case 2: //jurIdx 2 = State
				nexusindCode = "I";
				vendorNexusWhere =	" AND nexus_country_code = '" + primaryCountryCode + "'" +
                              		" AND nexus_state_code = '" + primaryStateCode + "'" +
                              		" AND nexus_county='*STATE' " +
                              		" AND nexus_city='*STATE' " +
                              		" AND nexus_stj='*STATE' ";
				vendorNexusOrderby = 	" ORDER BY tb_vendor_nexus_dtl.effective_date DESC ";
				break;
			case 3: //jurIdx 3 = County
				if(trans.getCountyNexusindCode()!=null && trans.getCountyNexusindCode().equalsIgnoreCase("S")){
	               nexusindCode = "S";             
	               trans.setCountyVendorNexusDtlId(trans.getStateVendorNexusDtlId());	               
	               trans.setCountyNexusTypeCode(trans.getStateNexusTypeCode());
	               continue; //no query
				}
				else{
	               nexusindCode = "I";
	               vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
	                                " AND nexus_state_code = '" + primaryStateCode + "'" +
	                                " AND (nexus_county = '" +  primaryCounty + "'" +
	                                " OR  nexus_county='*ALL')" +
	                                " AND nexus_city='*COUNTY'" +
	                                " AND nexus_stj='*COUNTY'";
	               vendorNexusOrderby = " ORDER BY tb_vendor_nexus.nexus_county DESC, tb_vendor_nexus_dtl.effective_date DESC";
				}
				break;
			case 4: //jurIdx 4 = City
				if(trans.getCityNexusindCode()!=null && trans.getCityNexusindCode().equalsIgnoreCase("S")){
	               nexusindCode = "S";
	               trans.setCityVendorNexusDtlId(trans.getStateVendorNexusDtlId());
	               trans.setCityNexusTypeCode(trans.getStateNexusTypeCode());
	               continue; //no query
				}
				else{
	               nexusindCode = "I";
	               vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
	                                " AND nexus_state_code = '" + primaryStateCode + "'" +
	                                " AND nexus_county='*CITY'" +
	                                " AND (nexus_city = '" + primaryCity + "'" +
	                                " OR  nexus_city='*ALL')" +
	                                " AND nexus_stj='*CITY'";
	               vendorNexusOrderby = " ORDER BY tb_vendor_nexus.nexus_city DESC, tb_vendor_nexus_dtl.effective_date DESC";
				}
				break;
			default: //jurIdx 5-14, Stj1 - Stj10
				int stj = jurIdx-4; 
				String stjNexusindCode = null;
				String stjFinalName = null;
				Method theMethod = null;
				try{
            		theMethod = trans.getClass().getDeclaredMethod("getStj" + stj + "Name", new Class[]{});
    	        	theMethod.setAccessible(true);	        	
    	        	stjFinalName = (String)theMethod.invoke(trans, new Object[]{});
				}
				catch(Exception e){
					e.printStackTrace();
				}	
				
				if(stjFinalName!=null && stjFinalName.length()>0){
					try{
	            		theMethod = trans.getClass().getDeclaredMethod("getStj" + stj + "NexusindCode", new Class[]{});
	    	        	theMethod.setAccessible(true);	        	
	    	        	stjNexusindCode = (String)theMethod.invoke(trans, new Object[]{});
					}
					catch(Exception e){
						e.printStackTrace();
					}	
					
					if(stjNexusindCode!=null && stjNexusindCode.equalsIgnoreCase("S")){
						nexusindCode = "S";
						
						try{
							theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusTypeCode", new Class[] {String.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{trans.getStateNexusTypeCode()}); 
	                		
	                		theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "VendorNexusDtlId", new Class[] {Long.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{trans.getStateVendorNexusDtlId()}); 
						}
						catch(Exception e){
							e.printStackTrace();
						}
						continue; //no query
					}
					else{
						nexusindCode = "I";
						vendorNexusWhere = " AND nexus_country_code = '" + primaryCountryCode + "'" +
                                		" AND nexus_state_code = '" + primaryStateCode + "'" +
                                		" AND nexus_county='*STJ'" +
                                		" AND nexus_city='*STJ'" +
                                		" AND (nexus_stj='" + stjFinalName + "'" +
                                		" OR  nexus_stj='*ALL')";
						vendorNexusOrderby = " ORDER BY tb_vendor_nexus.nexus_stj DESC, tb_vendor_nexus_dtl.effective_date DESC";
					}
				}
				else{
					nexusindCode = "X";
					continue;
				}
				
	            break;		
			}
			
			//Search for VendorNexus when Indicator = 'I'
			if("I".equals(nexusindCode)) {
	         // Search for Nexus
				List<VendorNexusDtl> VendorNexusDtls = vendorNexusDetailService.getVendorNexusDtlList(trans.getVendorId(), trans.getGlDate(), vendorNexusWhere, vendorNexusOrderby);
				
				// VendorNexus line found
				if(VendorNexusDtls != null && VendorNexusDtls.size() > 0) {
					VendorNexusDtl vendorNexusDtl = VendorNexusDtls.get(0);
					
					switch(jurIdx) {
					case 1:   //jur_idx 1 = Country
						trans.setCountryVendorNexusDtlId(vendorNexusDtl.getVendorNexusDtlId());
						trans.setCountryNexusTypeCode(vendorNexusDtl.getNexusTypeCode());
						
						break;
					case 2:   //jur_idx 2 = State
						trans.setStateVendorNexusDtlId(vendorNexusDtl.getVendorNexusDtlId());
						trans.setStateNexusTypeCode(vendorNexusDtl.getNexusTypeCode());						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						
	                  	break;
					case 3:   //jur_idx 3 = County
						trans.setCountyVendorNexusDtlId(vendorNexusDtl.getVendorNexusDtlId());
						trans.setCountyNexusTypeCode(vendorNexusDtl.getNexusTypeCode());	
						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setStateNexusTypeCode("*YES");  
						}
						
						break;		
						
					case 4:   //jur_idx 4 = City
						trans.setCityVendorNexusDtlId(vendorNexusDtl.getVendorNexusDtlId());
						trans.setCityNexusTypeCode(vendorNexusDtl.getNexusTypeCode());						
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountryNexusTypeCode("*YES");  
						}
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setStateNexusTypeCode("*YES");  
						}
						if(trans.getCountyNexusTypeCode()!=null && trans.getCountyNexusTypeCode().equalsIgnoreCase("*NO")){
							trans.setCountyNexusTypeCode("*YES");  
						}

						break;
								
					default: //jurIdx 5-14, Stj1 - Stj10
						
						trans.setStj1VendorNexusDtlId(vendorNexusDtl.getVendorNexusDtlId());
						trans.setStj1NexusTypeCode(vendorNexusDtl.getNexusTypeCode());
						
						int stj = jurIdx-4; 
						try{
							Method theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "VendorNexusDtlId", new Class[] {Long.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{vendorNexusDtl.getVendorNexusDtlId()}); 
	                		
	                		theMethod = trans.getClass().getDeclaredMethod("setStj" + stj + "NexusTypeCode", new Class[] {String.class});
	                		theMethod.setAccessible(true);
	                		theMethod.invoke(trans, new Object[]{vendorNexusDtl.getNexusTypeCode()}); 
						}
						catch(Exception e){
							e.printStackTrace();
						}
		
			            break;
					}
				}
				
				else {
					// Nexus not found
					
					boolean breakLoop = false;
					
					switch(jurIdx) {
					case 1: //jur_idx 1 = Country
						breakLoop = true;
						break;
					case 2: //jur_idx 2 = State
						breakLoop = true;
						break;
					default:
	                   //no nexus for County, City, or STJ1-STJ5 when not found
					}
					
					if(breakLoop) {
						break;
					}
				}
									
				//Check STJs after all 10 have been looked up
				if(jurIdx == 14) {
					if(		!"*NO".equals(trans.getStj1NexusTypeCode()) ||
							!"*NO".equals(trans.getStj2NexusTypeCode()) ||
							!"*NO".equals(trans.getStj3NexusTypeCode()) ||
							!"*NO".equals(trans.getStj4NexusTypeCode()) ||
							!"*NO".equals(trans.getStj5NexusTypeCode()) ||
							!"*NO".equals(trans.getStj6NexusTypeCode()) ||
							!"*NO".equals(trans.getStj7NexusTypeCode()) ||
							!"*NO".equals(trans.getStj8NexusTypeCode()) ||
							!"*NO".equals(trans.getStj9NexusTypeCode()) ||
							!"*NO".equals(trans.getStj10NexusTypeCode())) {
						if(trans.getCityNexusTypeCode()!=null && trans.getCityNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCityNexusTypeCode("*YES");  
						}
						if(trans.getCountyNexusTypeCode()!=null && trans.getCountyNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCountyNexusTypeCode("*YES");  
						}
						if(trans.getStateNexusTypeCode()!=null && trans.getStateNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setStateNexusTypeCode("*YES");  
						}
						if(trans.getCountryNexusTypeCode()!=null && trans.getCountryNexusTypeCode().equalsIgnoreCase("*NO")){//E, V, YES, NO
							trans.setCountryNexusTypeCode("*YES");  
						}
						break;
					}
				}
			}//End of query	
		}

		//Check for full non-nexus
		if("*NO".equals(trans.getCountryNexusTypeCode()) && "*NO".equals(trans.getStateNexusTypeCode()) &&
			"*NO".equals(trans.getCountyNexusTypeCode()) && "*NO".equals(trans.getCityNexusTypeCode())) {
				trans.setExemptCode("FN");
				trans.setTransactionInd(TRANS_IND_PROCESSED);
				trans.setSuspendInd(null);
				//trans.setProcesstypeCode(TRANSACTION_STAGE_PURCHASE_PROCESSED);
				throw new PurchaseTranasactionSoftAbortProcessingException("");
		}

    }


    private void setDelete0Amount(TransientPurchaseTransaction transientPurchaseTransaction) {
        if (transientPurchaseTransaction.getDelete0Amount() == null) {
            String delete0AmtParmStr = transientPurchaseTransaction.getPurchasingOptions().get(PurchaseTransactionOptions.PK_DELETE0AMT);
            if (delete0AmtParmStr != null && "1".equals(delete0AmtParmStr) ) {
                transientPurchaseTransaction.setDelete0Amount(Boolean.TRUE);
            }
            else {
                transientPurchaseTransaction.setDelete0Amount(Boolean.FALSE);
            }
        }
    }

    private boolean checkCalcIndicator(String calcInd) {
        if (calcInd !=null && calcInd.length()>0 && !"1".equals(calcInd.substring(0,1)) &&
                !"t".equalsIgnoreCase(calcInd.substring(0,1)) &&
                !"y".equalsIgnoreCase(calcInd.substring(0,1)))
            return false;
        return true;
    }
    
    private void copyRates(MicroApiTransaction from, MicroApiTransaction to) {
        to.setCountryTaxrateId(from.getCountryTaxrateId());
        to.setCountryTier1Rate(from.getCountryTier1Rate());
        to.setCountryTier2Rate(from.getCountryTier2Rate());
        to.setCountryTier3Rate(from.getCountryTier3Rate());

        to.setCountryTier1Setamt(from.getCountryTier1Setamt());
        to.setCountryTier2Setamt(from.getCountryTier2Setamt());
        to.setCountryTier3Setamt(from.getCountryTier3Setamt());

        to.setCountryTier1MaxAmt(from.getCountryTier1MaxAmt());
        to.setCountryTier2MinAmt(from.getCountryTier2MinAmt());
        to.setCountryTier2MaxAmt(from.getCountryTier2MaxAmt());
        to.setCountryMaxtaxAmt(from.getCountryMaxtaxAmt());


        to.setStateTaxrateId(from.getStateTaxrateId());
        to.setStateTier1Rate(from.getStateTier1Rate());
        to.setStateTier2Rate(from.getStateTier2Rate());
        to.setStateTier3Rate(from.getStateTier3Rate());

        to.setStateTier1Setamt(from.getStateTier1Setamt());
        to.setStateTier2Setamt(from.getStateTier2Setamt());
        to.setStateTier3Setamt(from.getStateTier3Setamt());

        to.setStateTier1MaxAmt(from.getStateTier1MaxAmt());
        to.setStateTier2MinAmt(from.getStateTier2MinAmt());
        to.setStateTier2MaxAmt(from.getStateTier2MaxAmt());
        to.setStateMaxtaxAmt(from.getStateMaxtaxAmt());


        to.setCountyTaxrateId(from.getCountyTaxrateId());
        to.setCountyTier1Rate(from.getCountyTier1Rate());
        to.setCountyTier2Rate(from.getCountyTier2Rate());
        to.setCountyTier3Rate(from.getCountyTier3Rate());

        to.setCountyTier1Setamt(from.getCountyTier1Setamt());
        to.setCountyTier2Setamt(from.getCountyTier2Setamt());
        to.setCountyTier3Setamt(from.getCountyTier3Setamt());

        to.setCountyTier1MaxAmt(from.getCountyTier1MaxAmt());
        to.setCountyTier2MinAmt(from.getCountyTier2MinAmt());
        to.setCountyTier2MaxAmt(from.getCountyTier2MaxAmt());
        to.setCountyMaxtaxAmt(from.getCountyMaxtaxAmt());


        to.setCityTaxrateId(from.getCityTaxrateId());
        to.setCityTier1Rate(from.getCityTier1Rate());
        to.setCityTier2Rate(from.getCityTier2Rate());
        to.setCityTier3Rate(from.getCityTier3Rate());

        to.setCityTier1Setamt(from.getCityTier1Setamt());
        to.setCityTier2Setamt(from.getCityTier2Setamt());
        to.setCityTier3Setamt(from.getCityTier3Setamt());

        to.setCityTier1MaxAmt(from.getCityTier1MaxAmt());
        to.setCityTier2MinAmt(from.getCityTier2MinAmt());
        to.setCityTier2MaxAmt(from.getCityTier2MaxAmt());
        to.setCityMaxtaxAmt(from.getCityMaxtaxAmt());

        to.setStj1TaxrateId(from.getStj1TaxrateId());
        to.setStj1Rate(from.getStj1Rate());
        to.setStj1Setamt(from.getStj1Setamt());

        to.setStj2TaxrateId(from.getStj2TaxrateId());
        to.setStj2Rate(from.getStj2Rate());
        to.setStj2Setamt(from.getStj2Setamt());

        to.setStj3TaxrateId(from.getStj3TaxrateId());
        to.setStj3Rate(from.getStj3Rate());
        to.setStj3Setamt(from.getStj3Setamt());

        to.setStj4TaxrateId(from.getStj4TaxrateId());
        to.setStj4Rate(from.getStj4Rate());
        to.setStj4Setamt(from.getStj4Setamt());

        to.setStj5TaxrateId(from.getStj5TaxrateId());
        to.setStj5Rate(from.getStj5Rate());
        to.setStj5Setamt(from.getStj5Setamt());
    }

    private void setProcruleFlag(TransientPurchaseTransaction transientPurchaseTransaction) {
        if (transientPurchaseTransaction.getProcRuleEnabled() == null) {
            String procRuleEnabledStr = null;
            if (transientPurchaseTransaction.getPurchasingOptions() != null)
                procRuleEnabledStr = transientPurchaseTransaction.getPurchasingOptions().get(PurchaseTransactionOptions.PK_PROCRULEENABLED);
            if (procRuleEnabledStr != null && "1".equals(procRuleEnabledStr)) {
                transientPurchaseTransaction.setProcRuleEnabled(Boolean.TRUE);
            }
            else {
                transientPurchaseTransaction.setProcRuleEnabled(Boolean.FALSE);
            }
        }
    }

    private void resetRatesSetFlags(TransientPurchaseTransaction transientPurchaseTransaction) {
        transientPurchaseTransaction.setCountryRateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStateRateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setCountyRateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setCityRateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj1RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj2RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj3RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj4RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj5RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj6RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj7RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj8RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj9RateSetFlag(Boolean.FALSE);
        transientPurchaseTransaction.setStj10RateSetFlag(Boolean.FALSE);
    }
    
    private void pushAllTaxes(MicroApiTransactionDocument document) {

        BigDecimal documentProrateAmt = getTotalProrateAmt(document);
        BigDecimal accumulatedDocumentPushAmt = BigDecimal.ZERO;

        if (documentProrateAmt == null
                || documentProrateAmt.equals(BigDecimal.ZERO)
                || document.getPushTaxAmt() == null
                || document.getMicroApiTransaction() == null
                || document.getMicroApiTransaction().size() == 0)
            return;

        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                    || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())) {
                transaction.setProrateAmt(transaction.getTbCalcTaxAmt());
                if (transaction.getProrateAmt() != null)
                    transaction.setPushToTrans((transaction.getTbCalcTaxAmt()
                            .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                            .multiply(document.getPushTaxAmt())).setScale(2, RoundingMode.HALF_UP));
            }

            if (transaction.getPushToTrans() != null) {
                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                        && transaction.getStateTier1TaxAmt() != null) {
                    BigDecimal pushStateAmt = (transaction.getStateTier1TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStateTier1TaxAmt(pushStateAmt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStateAmt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                        && transaction.getCountyTier1TaxAmt() != null) {
                    BigDecimal pushCountyAmt = (transaction.getCountyTier1TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setCountyTier1TaxAmt(pushCountyAmt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushCountyAmt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                        && transaction.getCityTier1TaxAmt() != null) {
                    BigDecimal pushCityAmt = (transaction.getCityTier1TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setCityTier1TaxAmt(pushCityAmt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushCityAmt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                        && transaction.getStj1TaxAmt() != null) {
                    BigDecimal pushStj1Amt = (transaction.getStj1TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj1TaxAmt(pushStj1Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj1Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                        && transaction.getStj2TaxAmt() != null) {
                    BigDecimal pushStj2Amt = (transaction.getStj2TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj2TaxAmt(pushStj2Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj2Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                        && transaction.getStj3TaxAmt() != null) {
                    BigDecimal pushStj3Amt = (transaction.getStj3TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj3TaxAmt(pushStj3Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj3Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                        && transaction.getStj4TaxAmt() != null) {
                    BigDecimal pushStj4Amt = (transaction.getStj4TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj4TaxAmt(pushStj4Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj4Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                        && transaction.getStj5TaxAmt() != null) {
                    BigDecimal pushStj5Amt = (transaction.getStj5TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj5TaxAmt(pushStj5Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj5Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                        && transaction.getStj6TaxAmt() != null) {
                    BigDecimal pushStj6Amt = (transaction.getStj6TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj6TaxAmt(pushStj6Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj6Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                        && transaction.getStj7TaxAmt() != null) {
                    BigDecimal pushStj7Amt = (transaction.getStj7TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj7TaxAmt(pushStj7Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj7Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                        && transaction.getStj8TaxAmt() != null) {
                    BigDecimal pushStj8Amt = (transaction.getStj8TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj8TaxAmt(pushStj8Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj8Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                        && transaction.getStj9TaxAmt() != null) {
                    BigDecimal pushStj9Amt = (transaction.getStj9TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj9TaxAmt(pushStj9Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj9Amt);
                }

                if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                        && transaction.getStj10TaxAmt() != null) {
                    BigDecimal pushStj10Amt = (transaction.getStj10TaxAmt()
                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                            .multiply(transaction.getPushToTrans()))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setStj10TaxAmt(pushStj10Amt);
                    accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(pushStj10Amt);
                }

                transaction.setTbCalcTaxAmt(recalcTbCalcTaxAmt(transaction));
            }
        }
        BigDecimal deltaAmt = document.getPushTaxAmt().subtract(accumulatedDocumentPushAmt);
        document.setPushDelta(deltaAmt);
        addDeltaAmt(document);
        recalcLocalAmounts(document);
    }

    private void pushPreferenceTax(MicroApiTransactionDocument document) {

        if (document.getPushTaxPreference() == null
                || document.getPushTaxAmt() == null
                || ((document.getPushTaxPreference() != STATE
                && document.getPushTaxPreference() != COUNTY
                && document.getPushTaxPreference() != CITY
                && document.getPushTaxPreference() != STJ1
                && document.getPushTaxPreference() != STJ2
                && document.getPushTaxPreference() != STJ3
                && document.getPushTaxPreference() != STJ4
                && document.getPushTaxPreference() != STJ5
                && document.getPushTaxPreference() != STJ6
                && document.getPushTaxPreference() != STJ7
                && document.getPushTaxPreference() != STJ8
                && document.getPushTaxPreference() != STJ9
                && document.getPushTaxPreference() != STJ10)))
            return;


        BigDecimal documentProrateAmt = BigDecimal.ZERO;

        BigDecimal preferenceValue = getPreferenceAmount(document);


        BigDecimal accumulatedDocumentPushAmt = BigDecimal.ZERO;

        boolean totalPreferenceLessThanPush = (preferenceValue.compareTo(document.getPushTaxAmt()) < 0);

        if (!totalPreferenceLessThanPush) {
            pushHardTax(document);
            return;
        }
        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {

            BigDecimal transactionProrateAmt = BigDecimal.ZERO;
//            if (totalPreferenceLessThanPush) {
                if (document.getPushTaxPreference() != STATE
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                        && transaction.getStateTier1TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStateTier1TaxAmt());
                }
                if (document.getPushTaxPreference() != COUNTY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                        && transaction.getCountyTier1TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getCountyTier1TaxAmt());
                }
                if (document.getPushTaxPreference() != CITY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                        && transaction.getCityTier1TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getCityTier1TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ1
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                        && transaction.getStj1TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj1TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ2
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                        && transaction.getStj2TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj2TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ3
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                        && transaction.getStj3TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj3TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ4
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                        && transaction.getStj4TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj4TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ5
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                        && transaction.getStj5TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj5TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ6
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                        && transaction.getStj6TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj6TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ7
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                        && transaction.getStj7TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj7TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ8
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                        && transaction.getStj8TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj8TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ9
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                        && transaction.getStj9TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj9TaxAmt());
                }
                if (document.getPushTaxPreference() != STJ10
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                        && transaction.getStj10TaxAmt() != null) {
                    transactionProrateAmt = transactionProrateAmt.add(transaction.getStj10TaxAmt());
                }


            /*else {
                if (document.getPushTaxPreference() == STATE
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                        && transaction.getStateTier1TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStateTier1TaxAmt();
                } else if (document.getPushTaxPreference() == COUNTY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                        && transaction.getCountyTier1TaxAmt() != null) {
                    transactionProrateAmt = transaction.getCountyTier1TaxAmt();
                } else if (document.getPushTaxPreference() == CITY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                        && transaction.getCityTier1TaxAmt() != null) {
                    transactionProrateAmt = transaction.getCityTier1TaxAmt();
                } else if (document.getPushTaxPreference() == STJ1
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                        && transaction.getStj1TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj1TaxAmt();
                } else if (document.getPushTaxPreference() == STJ2
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                        && transaction.getStj2TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj2TaxAmt();
                } else if (document.getPushTaxPreference() == STJ3
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                        && transaction.getStj3TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj3TaxAmt();
                } else if (document.getPushTaxPreference() == STJ4
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                        && transaction.getStj4TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj4TaxAmt();
                } else if (document.getPushTaxPreference() == STJ5
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                        && transaction.getStj5TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj5TaxAmt();
                } else if (document.getPushTaxPreference() == STJ6
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                        && transaction.getStj6TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj6TaxAmt();
                } else if (document.getPushTaxPreference() == STJ7
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                        && transaction.getStj7TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj7TaxAmt();
                } else if (document.getPushTaxPreference() == STJ8
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                        && transaction.getStj8TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj8TaxAmt();
                } else if (document.getPushTaxPreference() == STJ9
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                        && transaction.getStj9TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj9TaxAmt();
                } else if (document.getPushTaxPreference() == STJ10
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                        && transaction.getStj10TaxAmt() != null) {
                    transactionProrateAmt = transaction.getStj10TaxAmt();
                }

            }*/
            transaction.setProrateAmt(transactionProrateAmt);
            documentProrateAmt = documentProrateAmt.add(nullToZero(transactionProrateAmt));
        }


        BigDecimal pushDocumentTotal = BigDecimal.ZERO;
        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {

            if ((TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                        || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                        || TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode()))
                    && transaction.getProrateAmt() != null
                    && documentProrateAmt != null
                    && documentProrateAmt.compareTo(BigDecimal.ZERO) != 0
                    && document.getPushTaxAmt().compareTo(BigDecimal.ZERO) != 0) {

                //BigDecimal accumulatedPerTrans = BigDecimal.ZERO;

  //              if (totalPreferenceLessThanPush) {

                    BigDecimal pushToTrans = (transaction.getProrateAmt()
                            .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                            .multiply(document.getPushTaxAmt().subtract(preferenceValue)))
                            .setScale(2, RoundingMode.HALF_UP);
                    transaction.setPushToTrans(pushToTrans);


                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                            && transaction.getStateTier1TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STATE) {
                            transaction.setPushToState(
                                    transaction.getStateTier1TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()));

                            transaction.setStateTier1TaxAmt(transaction.getPushToState());
                        } else if (document.getPushTaxPreference() == STATE) {
                            transaction.setPushToState(transaction.getStateTier1TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToState()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                            && transaction.getCountyTier1TaxAmt() != null) {
                        if (document.getPushTaxPreference() != COUNTY) {
                            transaction.setPushToCounty(
                                    (transaction.getCountyTier1TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                    .setScale(2, RoundingMode.HALF_UP));
                            transaction.setCountyTier1TaxAmt(transaction.getPushToCounty());
                        } else if (document.getPushTaxPreference() == COUNTY) {
                            transaction.setPushToCounty(transaction.getCountyTier1TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToCounty()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                            && transaction.getCityTier1TaxAmt() != null) {
                        if (document.getPushTaxPreference() != CITY) {
                            transaction.setPushToCity(
                                    (transaction.getCityTier1TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                    .setScale(2, RoundingMode.HALF_UP));
                            transaction.setCityTier1TaxAmt(transaction.getPushToCity());
                        } else if (document.getPushTaxPreference() == CITY) {
                            transaction.setPushToCity(transaction.getCityTier1TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToCity()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                            && transaction.getStj1TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ1) {
                            transaction.setPushToStj1(
                                    (transaction.getStj1TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj1TaxAmt(transaction.getPushToStj1());
                        } else if (document.getPushTaxPreference() == STJ1) {
                            transaction.setPushToStj1(transaction.getStj1TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj1()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                            && transaction.getStj2TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ2) {
                            transaction.setPushToStj2(
                                    (transaction.getStj2TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj2TaxAmt(transaction.getPushToStj2());
                        } else if (document.getPushTaxPreference() == STJ2) {
                            transaction.setPushToStj2(transaction.getStj2TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj2()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                            && transaction.getStj3TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ3) {
                            transaction.setPushToStj3(
                                    (transaction.getStj3TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj3TaxAmt(transaction.getPushToStj3());
                        } else if (document.getPushTaxPreference() == STJ3) {
                            transaction.setPushToStj3(transaction.getStj3TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj3()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                            && transaction.getStj4TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ4) {
                            transaction.setPushToStj4(
                                    (transaction.getStj4TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj4TaxAmt(transaction.getPushToStj4());
                        } else if (document.getPushTaxPreference() == STJ4) {
                            transaction.setPushToStj4(transaction.getStj4TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj4()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                            && transaction.getStj5TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ5) {
                            transaction.setPushToStj5(
                                    (transaction.getStj5TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj5TaxAmt(transaction.getPushToStj5());
                        } else if (document.getPushTaxPreference() == STJ5) {
                            transaction.setPushToStj5(transaction.getStj5TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj5()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                            && transaction.getStj6TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ6) {
                            transaction.setPushToStj6(
                                    (transaction.getStj6TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj6TaxAmt(transaction.getPushToStj6());
                        } else if (document.getPushTaxPreference() == STJ6) {
                            transaction.setPushToStj6(transaction.getStj6TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj6()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                            && transaction.getStj7TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ7) {
                            transaction.setPushToStj7(
                                    (transaction.getStj7TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj7TaxAmt(transaction.getPushToStj7());
                        } else if (document.getPushTaxPreference() == STJ7) {
                            transaction.setPushToStj7(transaction.getStj7TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj7()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                            && transaction.getStj8TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ8) {
                            transaction.setPushToStj8(
                                    (transaction.getStj8TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj8TaxAmt(transaction.getPushToStj8());
                        } else if (document.getPushTaxPreference() == STJ8) {
                            transaction.setPushToStj8(transaction.getStj8TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj8()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                            && transaction.getStj9TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ9) {
                            transaction.setPushToStj9(
                                    (transaction.getStj9TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));

                            transaction.setStj9TaxAmt(transaction.getPushToStj9());
                        } else if (document.getPushTaxPreference() == STJ9) {
                            transaction.setPushToStj9(transaction.getStj9TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj9()));
                    }
                    if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                            && transaction.getStj10TaxAmt() != null) {
                        if (document.getPushTaxPreference() != STJ10) {
                            transaction.setPushToStj10(
                                    (transaction.getStj10TaxAmt()
                                            .divide(transaction.getProrateAmt(), 7, RoundingMode.HALF_EVEN)
                                            .multiply(transaction.getPushToTrans()))
                                            .setScale(2, RoundingMode.HALF_UP));
                            transaction.setStj10TaxAmt(transaction.getPushToStj10());
                        } else if (document.getPushTaxPreference() == STJ10) {
                            transaction.setPushToStj10(transaction.getStj10TaxAmt());
                        }
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(nullToZero(transaction.getPushToStj10()));
                    }
                /*} else {
                    if (document.getPushTaxPreference() == STATE
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                            && transaction.getStateTier1TaxAmt() != null) {
                        transaction.setPushToState(
                                (transaction.getStateTier1TaxAmt()
                                    .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                    .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStateTier1TaxAmt(transaction.getPushToState());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToState());
                    }
                    if (document.getPushTaxPreference() == COUNTY
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                            && transaction.getCountyTier1TaxAmt() != null) {
                        transaction.setPushToCounty((transaction.getCountyTier1TaxAmt()
                                    .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                    .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setCountyTier1TaxAmt(transaction.getPushToCounty());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToCounty());
                    }
                    if (document.getPushTaxPreference() == CITY
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                            && transaction.getCityTier1TaxAmt() != null) {
                        transaction.setPushToCity((transaction.getCityTier1TaxAmt()
                                    .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                    .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setCityTier1TaxAmt(transaction.getPushToCity());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToCity());
                    }
                    if (document.getPushTaxPreference() == STJ1
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                            && transaction.getStj1TaxAmt() != null) {
                        transaction.setPushToStj1((transaction.getStj1TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj1TaxAmt(transaction.getPushToStj1());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj1());
                    }
                    if (document.getPushTaxPreference() == STJ2
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                            && transaction.getStj2TaxAmt() != null) {
                        transaction.setPushToStj2((transaction.getStj2TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj2TaxAmt(transaction.getPushToStj2());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj2());
                    }
                    if (document.getPushTaxPreference() == STJ3
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                            && transaction.getStj3TaxAmt() != null) {
                        transaction.setPushToStj3((transaction.getStj3TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj3TaxAmt(transaction.getPushToStj3());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj3());
                    }
                    if (document.getPushTaxPreference() == STJ4
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                            && transaction.getStj4TaxAmt() != null) {
                        transaction.setPushToStj4((transaction.getStj4TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj4TaxAmt(transaction.getPushToStj4());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj4());
                    }
                    if (document.getPushTaxPreference() == STJ5
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                            && transaction.getStj5TaxAmt() != null) {
                        transaction.setPushToStj5((transaction.getStj5TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj5TaxAmt(transaction.getPushToStj5());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj5());
                    }
                    if (document.getPushTaxPreference() == STJ6
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                            && transaction.getStj6TaxAmt() != null) {
                        transaction.setPushToStj6((transaction.getStj6TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj6TaxAmt(transaction.getPushToStj6());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj6());
                    }
                    if (document.getPushTaxPreference() == STJ7
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                            && transaction.getStj7TaxAmt() != null) {
                        transaction.setPushToStj7((transaction.getStj7TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj7TaxAmt(transaction.getPushToStj7());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj7());
                    }
                    if (document.getPushTaxPreference() == STJ8
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                            && transaction.getStj8TaxAmt() != null) {
                        transaction.setPushToStj8((transaction.getStj8TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj8TaxAmt(transaction.getPushToStj8());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj8());
                    }
                    if (document.getPushTaxPreference() == STJ9
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                            && transaction.getStj9TaxAmt() != null) {
                        transaction.setPushToStj9((transaction.getStj9TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj9TaxAmt(transaction.getPushToStj9());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj9());
                    }
                    if (document.getPushTaxPreference() == STJ10
                            && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                            && transaction.getStj10TaxAmt() != null) {
                        transaction.setPushToStj10((transaction.getStj10TaxAmt()
                                .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                                .multiply(document.getPushTaxAmt()))
                                .setScale(2, RoundingMode.HALF_UP));
                        transaction.setStj10TaxAmt(transaction.getPushToStj10());
                        accumulatedDocumentPushAmt = accumulatedDocumentPushAmt.add(transaction.getPushToStj10());
                    }
                }*/
                BigDecimal newTbCalcTaxAmt = recalcTbCalcTaxAmt(transaction);
                transaction.setTbCalcTaxAmt(newTbCalcTaxAmt);
                //pushDocumentTotal = pushDocumentTotal.add(newTbCalcTaxAmt);
            }
        }
        document.setPushDelta(document.getPushTaxAmt().subtract(accumulatedDocumentPushAmt));
        addDeltaAmt(document);
        recalcLocalAmounts(document);
    }
    
    private void pushHardTax(MicroApiTransactionDocument document) {
        if (document.getPushTaxAmt() == null
                || (document.getPushTaxPreference() != STATE
                && document.getPushTaxPreference() != COUNTY
                && document.getPushTaxPreference() != CITY))
            return;

        //BigDecimal preferenceAmt = getPreferenceAmount(document);
        BigDecimal documentProrateAmt = BigDecimal.ZERO;
        BigDecimal transactionProrateAmt = BigDecimal.ZERO;
        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            if (document.getPushTaxPreference() == STATE
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())
                    && transaction.getStateTier1TaxAmt() != null) {
                transactionProrateAmt = transaction.getStateTier1TaxAmt();
            }
            if (document.getPushTaxPreference() == COUNTY
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())
                    && transaction.getCountyTier1TaxAmt() != null) {
                transactionProrateAmt = transaction.getCountyTier1TaxAmt();
            }
            if (document.getPushTaxPreference() == CITY
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())
                    && transaction.getCityTier1TaxAmt() != null) {
                transactionProrateAmt = transaction.getCityTier1TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ1
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())
                    && transaction.getStj1TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj1TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ2
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())
                    && transaction.getStj2TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj2TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ3
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())
                    && transaction.getStj3TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj3TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ4
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())
                    && transaction.getStj4TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj4TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ5
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())
                    && transaction.getStj5TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj5TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ6
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())
                    && transaction.getStj6TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj6TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ7
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())
                    && transaction.getStj7TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj7TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ8
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())
                    && transaction.getStj8TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj8TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ9
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())
                    && transaction.getStj9TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj9TaxAmt();
            }
            if (document.getPushTaxPreference() == STJ10
                    && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())
                    && transaction.getStj10TaxAmt() != null) {
                transactionProrateAmt = transaction.getStj10TaxAmt();
            }
            transaction.setProrateAmt(transactionProrateAmt);
            documentProrateAmt = documentProrateAmt.add(nullToZero(transactionProrateAmt));
        }

        BigDecimal pushDocumentTotal = BigDecimal.ZERO;
        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            if (documentProrateAmt.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal pushAmt = (transaction.getProrateAmt()
                            .divide(documentProrateAmt, 7, RoundingMode.HALF_EVEN)
                            .multiply(document.getPushTaxAmt()))
                        .setScale(2, RoundingMode.HALF_UP);
                if (document.getPushTaxPreference() == STATE
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode())) {
                    transaction.setPushToState(pushAmt);
                    transaction.setStateTier1TaxAmt(pushAmt);
                } else if (document.getPushTaxPreference() != STATE) {
                    transaction.setStateTier1TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == COUNTY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode())) {
                    transaction.setPushToCounty(pushAmt);
                    transaction.setCountyTier1TaxAmt(pushAmt);
                } else {
                    transaction.setCountyTier1TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == CITY
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode())) {
                    transaction.setPushToCity(pushAmt);
                    transaction.setCityTier1TaxAmt(pushAmt);
                } else {
                    transaction.setCityTier1TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ1
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode())) {
                    transaction.setPushToStj1(pushAmt);
                    transaction.setStj1TaxAmt(pushAmt);
                } else {
                    transaction.setStj1TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ2
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode())) {
                    transaction.setPushToStj2(pushAmt);
                    transaction.setStj2TaxAmt(pushAmt);
                } else {
                    transaction.setStj2TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ3
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode())) {
                    transaction.setPushToStj3(pushAmt);
                    transaction.setStj3TaxAmt(pushAmt);
                } else {
                    transaction.setStj3TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ4
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode())) {
                    transaction.setPushToStj4(pushAmt);
                    transaction.setStj4TaxAmt(pushAmt);
                } else {
                    transaction.setStj4TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ5
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode())) {
                    transaction.setPushToStj5(pushAmt);
                    transaction.setStj5TaxAmt(pushAmt);
                } else {
                    transaction.setStj5TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ6
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode())) {
                    transaction.setPushToStj6(pushAmt);
                    transaction.setStj6TaxAmt(pushAmt);
                } else {
                    transaction.setStj6TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ7
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode())) {
                    transaction.setPushToStj7(pushAmt);
                    transaction.setStj7TaxAmt(pushAmt);
                } else {
                    transaction.setStj7TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ8
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode())) {
                    transaction.setPushToStj8(pushAmt);
                    transaction.setStj8TaxAmt(pushAmt);
                } else {
                    transaction.setStj8TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ9
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode())) {
                    transaction.setPushToStj9(pushAmt);
                    transaction.setStj9TaxAmt(pushAmt);
                } else {
                    transaction.setStj9TaxAmt(BigDecimal.ZERO);
                }
                if (document.getPushTaxPreference() == STJ10
                        && TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode())) {
                    transaction.setPushToStj10(pushAmt);
                    transaction.setStj10TaxAmt(pushAmt);
                } else {
                    transaction.setStj10TaxAmt(BigDecimal.ZERO);
                }

                transaction.setTbCalcTaxAmt(pushAmt);
                pushDocumentTotal = pushDocumentTotal.add(nullToZero(pushAmt));
            }
        }
        document.setPushDelta(document.getPushTaxAmt().subtract(pushDocumentTotal));
        addDeltaAmt(document);
        recalcLocalAmounts(document);

    }

    private BigDecimal getTotalProrateAmt(PurchaseTransactionDocument document) {
        BigDecimal result = BigDecimal.ZERO;

        for (PurchaseTransaction transaction : document.getPurchaseTransaction()) {
            if (transaction != null && transaction.getTbCalcTaxAmt() !=null)
                result = result.add(transaction.getTbCalcTaxAmt());
        }

        return result;
    }
    
    private BigDecimal getTotalProrateAmt(MicroApiTransactionDocument document) {
        BigDecimal result = BigDecimal.ZERO;

        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            if (transaction != null && transaction.getTbCalcTaxAmt() !=null)
                result = result.add(transaction.getTbCalcTaxAmt());
        }

        return result;
    }

    private BigDecimal getPreferenceAmount(PurchaseTransactionDocument document) {

        BigDecimal totalPreferenceState = BigDecimal.ZERO;
        BigDecimal totalPreferenceCounty = BigDecimal.ZERO;
        BigDecimal totalPreferenceCity = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj1 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj2 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj3 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj4 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj5 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj6 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj7 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj8 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj9 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj10 = BigDecimal.ZERO;


        for (PurchaseTransaction transaction : document.getPurchaseTransaction()) {
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode()) && transaction.getStateTier1TaxAmt() != null) {
                totalPreferenceState = totalPreferenceState.add(transaction.getStateTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode()) && transaction.getCountyTier1TaxAmt() != null) {
                totalPreferenceCounty = totalPreferenceCounty.add(transaction.getCountyTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode()) && transaction.getCityTier1TaxAmt() != null) {
                totalPreferenceCity = totalPreferenceCity.add(transaction.getCityTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode()) && transaction.getStj1TaxAmt() != null) {
                totalPreferenceStj1 = totalPreferenceStj1.add(transaction.getStj1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode()) && transaction.getStj2TaxAmt() != null) {
                totalPreferenceStj2 = totalPreferenceStj2.add(transaction.getStj2TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode()) && transaction.getStj3TaxAmt() != null) {
                totalPreferenceStj3 = totalPreferenceStj3.add(transaction.getStj3TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode()) && transaction.getStj4TaxAmt() != null) {
                totalPreferenceStj4 = totalPreferenceStj4.add(transaction.getStj4TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode()) && transaction.getStj5TaxAmt() != null) {
                totalPreferenceStj5 = totalPreferenceStj5.add(transaction.getStj5TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode()) && transaction.getStj6TaxAmt() != null) {
                totalPreferenceStj6 = totalPreferenceStj6.add(transaction.getStj6TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode()) && transaction.getStj7TaxAmt() != null) {
                totalPreferenceStj7 = totalPreferenceStj7.add(transaction.getStj7TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode()) && transaction.getStj8TaxAmt() != null) {
                totalPreferenceStj8 = totalPreferenceStj8.add(transaction.getStj8TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode()) && transaction.getStj9TaxAmt() != null) {
                totalPreferenceStj9 = totalPreferenceStj9.add(transaction.getStj9TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode()) && transaction.getStj10TaxAmt() != null) {
                totalPreferenceStj10 = totalPreferenceStj10.add(transaction.getStj10TaxAmt());
            }
        }

        BigDecimal preferenceValue = BigDecimal.ZERO;
        switch (document.getPushTaxPreference()) {
            case STATE:
                preferenceValue = totalPreferenceState;
                break;

            case COUNTY:
                preferenceValue = totalPreferenceCounty;
                break;

            case CITY:
                preferenceValue = totalPreferenceCity;
                break;

            case STJ1:
                preferenceValue = totalPreferenceStj1;
                break;

            case STJ2:
                preferenceValue = totalPreferenceStj2;
                break;

            case STJ3:
                preferenceValue = totalPreferenceStj3;
                break;

            case STJ4:
                preferenceValue = totalPreferenceStj4;
                break;

            case STJ5:
                preferenceValue = totalPreferenceStj5;
                break;

            case STJ6:
                preferenceValue = totalPreferenceStj6;
                break;

            case STJ7:
                preferenceValue = totalPreferenceStj7;
                break;

            case STJ8:
                preferenceValue = totalPreferenceStj8;
                break;

            case STJ9:
                preferenceValue = totalPreferenceStj9;
                break;

            case STJ10:
                preferenceValue = totalPreferenceStj10;
                break;
        }
        return preferenceValue;
    }
    
    private BigDecimal getPreferenceAmount(MicroApiTransactionDocument document) {

        BigDecimal totalPreferenceState = BigDecimal.ZERO;
        BigDecimal totalPreferenceCounty = BigDecimal.ZERO;
        BigDecimal totalPreferenceCity = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj1 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj2 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj3 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj4 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj5 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj6 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj7 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj8 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj9 = BigDecimal.ZERO;
        BigDecimal totalPreferenceStj10 = BigDecimal.ZERO;


        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode()) && transaction.getStateTier1TaxAmt() != null) {
                totalPreferenceState = totalPreferenceState.add(transaction.getStateTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode()) && transaction.getCountyTier1TaxAmt() != null) {
                totalPreferenceCounty = totalPreferenceCounty.add(transaction.getCountyTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode()) && transaction.getCityTier1TaxAmt() != null) {
                totalPreferenceCity = totalPreferenceCity.add(transaction.getCityTier1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode()) && transaction.getStj1TaxAmt() != null) {
                totalPreferenceStj1 = totalPreferenceStj1.add(transaction.getStj1TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode()) && transaction.getStj2TaxAmt() != null) {
                totalPreferenceStj2 = totalPreferenceStj2.add(transaction.getStj2TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode()) && transaction.getStj3TaxAmt() != null) {
                totalPreferenceStj3 = totalPreferenceStj3.add(transaction.getStj3TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode()) && transaction.getStj4TaxAmt() != null) {
                totalPreferenceStj4 = totalPreferenceStj4.add(transaction.getStj4TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode()) && transaction.getStj5TaxAmt() != null) {
                totalPreferenceStj5 = totalPreferenceStj5.add(transaction.getStj5TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode()) && transaction.getStj6TaxAmt() != null) {
                totalPreferenceStj6 = totalPreferenceStj6.add(transaction.getStj6TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode()) && transaction.getStj7TaxAmt() != null) {
                totalPreferenceStj7 = totalPreferenceStj7.add(transaction.getStj7TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode()) && transaction.getStj8TaxAmt() != null) {
                totalPreferenceStj8 = totalPreferenceStj8.add(transaction.getStj8TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode()) && transaction.getStj9TaxAmt() != null) {
                totalPreferenceStj9 = totalPreferenceStj9.add(transaction.getStj9TaxAmt());
            }
            if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode()) && transaction.getStj10TaxAmt() != null) {
                totalPreferenceStj10 = totalPreferenceStj10.add(transaction.getStj10TaxAmt());
            }
        }

        BigDecimal preferenceValue = BigDecimal.ZERO;
        switch (document.getPushTaxPreference()) {
            case STATE:
                preferenceValue = totalPreferenceState;
                break;

            case COUNTY:
                preferenceValue = totalPreferenceCounty;
                break;

            case CITY:
                preferenceValue = totalPreferenceCity;
                break;

            case STJ1:
                preferenceValue = totalPreferenceStj1;
                break;

            case STJ2:
                preferenceValue = totalPreferenceStj2;
                break;

            case STJ3:
                preferenceValue = totalPreferenceStj3;
                break;

            case STJ4:
                preferenceValue = totalPreferenceStj4;
                break;

            case STJ5:
                preferenceValue = totalPreferenceStj5;
                break;

            case STJ6:
                preferenceValue = totalPreferenceStj6;
                break;

            case STJ7:
                preferenceValue = totalPreferenceStj7;
                break;

            case STJ8:
                preferenceValue = totalPreferenceStj8;
                break;

            case STJ9:
                preferenceValue = totalPreferenceStj9;
                break;

            case STJ10:
                preferenceValue = totalPreferenceStj10;
                break;
        }
        return preferenceValue;
    }

    private BigDecimal recalcTbCalcTaxAmt(PurchaseTransaction transaction) {
        BigDecimal result = BigDecimal.ZERO;
        if (transaction.getStateTier1TaxAmt() != null)
            result = result.add(transaction.getStateTier1TaxAmt());

        if (transaction.getCountyTier1TaxAmt() != null)
            result = result.add(transaction.getCountyTier1TaxAmt());

        if (transaction.getCityTier1TaxAmt() != null)
            result = result.add(transaction.getCityTier1TaxAmt());

        if (transaction.getStj1TaxAmt() != null)
            result = result.add(transaction.getStj1TaxAmt());

        if (transaction.getStj2TaxAmt() != null)
            result = result.add(transaction.getStj2TaxAmt());

        if (transaction.getStj3TaxAmt() != null)
            result = result.add(transaction.getStj3TaxAmt());

        if (transaction.getStj4TaxAmt() != null)
            result = result.add(transaction.getStj4TaxAmt());

        if (transaction.getStj5TaxAmt() != null)
            result = result.add(transaction.getStj5TaxAmt());

        if (transaction.getStj6TaxAmt() != null)
            result = result.add(transaction.getStj6TaxAmt());

        if (transaction.getStj7TaxAmt() != null)
            result = result.add(transaction.getStj7TaxAmt());

        if (transaction.getStj8TaxAmt() != null)
            result = result.add(transaction.getStj8TaxAmt());

        if (transaction.getStj9TaxAmt() != null)
            result = result.add(transaction.getStj9TaxAmt());

        if (transaction.getStj10TaxAmt() != null)
            result = result.add(transaction.getStj10TaxAmt());

        return result;
    }
    
    private BigDecimal recalcTbCalcTaxAmt(MicroApiTransaction transaction) {
        BigDecimal result = BigDecimal.ZERO;
        if (transaction.getStateTier1TaxAmt() != null)
            result = result.add(transaction.getStateTier1TaxAmt());

        if (transaction.getCountyTier1TaxAmt() != null)
            result = result.add(transaction.getCountyTier1TaxAmt());

        if (transaction.getCityTier1TaxAmt() != null)
            result = result.add(transaction.getCityTier1TaxAmt());

        if (transaction.getStj1TaxAmt() != null)
            result = result.add(transaction.getStj1TaxAmt());

        if (transaction.getStj2TaxAmt() != null)
            result = result.add(transaction.getStj2TaxAmt());

        if (transaction.getStj3TaxAmt() != null)
            result = result.add(transaction.getStj3TaxAmt());

        if (transaction.getStj4TaxAmt() != null)
            result = result.add(transaction.getStj4TaxAmt());

        if (transaction.getStj5TaxAmt() != null)
            result = result.add(transaction.getStj5TaxAmt());

        if (transaction.getStj6TaxAmt() != null)
            result = result.add(transaction.getStj6TaxAmt());

        if (transaction.getStj7TaxAmt() != null)
            result = result.add(transaction.getStj7TaxAmt());

        if (transaction.getStj8TaxAmt() != null)
            result = result.add(transaction.getStj8TaxAmt());

        if (transaction.getStj9TaxAmt() != null)
            result = result.add(transaction.getStj9TaxAmt());

        if (transaction.getStj10TaxAmt() != null)
            result = result.add(transaction.getStj10TaxAmt());

        return result;
    }

    private JurisdictionLevel getFirstTaxableJurisdictionLevel(PurchaseTransaction transaction) {
        JurisdictionLevel result = null;

        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode()))
            return JurisdictionLevel.STATE;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode()))
            return JurisdictionLevel.COUNTY;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode()))
            return JurisdictionLevel.CITY;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode()))
            return JurisdictionLevel.STJ1;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode()))
            return JurisdictionLevel.STJ2;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode()))
            return JurisdictionLevel.STJ3;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode()))
            return JurisdictionLevel.STJ4;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode()))
            return JurisdictionLevel.STJ5;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode()))
            return JurisdictionLevel.STJ6;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode()))
            return JurisdictionLevel.STJ7;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode()))
            return JurisdictionLevel.STJ8;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode()))
            return JurisdictionLevel.STJ9;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode()))
            return JurisdictionLevel.STJ10;


        return result;
    }
    
    private JurisdictionLevel getFirstTaxableJurisdictionLevel(MicroApiTransaction transaction) {
        JurisdictionLevel result = null;

        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStateTaxcodeTypeCode()))
            return JurisdictionLevel.STATE;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCountyTaxcodeTypeCode()))
            return JurisdictionLevel.COUNTY;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getCityTaxcodeTypeCode()))
            return JurisdictionLevel.CITY;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj1TaxcodeTypeCode()))
            return JurisdictionLevel.STJ1;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj2TaxcodeTypeCode()))
            return JurisdictionLevel.STJ2;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj3TaxcodeTypeCode()))
            return JurisdictionLevel.STJ3;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj4TaxcodeTypeCode()))
            return JurisdictionLevel.STJ4;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj5TaxcodeTypeCode()))
            return JurisdictionLevel.STJ5;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj6TaxcodeTypeCode()))
            return JurisdictionLevel.STJ6;
        if (TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj7TaxcodeTypeCode()))
            return JurisdictionLevel.STJ7;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj8TaxcodeTypeCode()))
            return JurisdictionLevel.STJ8;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj9TaxcodeTypeCode()))
            return JurisdictionLevel.STJ9;
        if(TAXCODE_TYPE_CODE_TAXABLE.equalsIgnoreCase(transaction.getStj10TaxcodeTypeCode()))
            return JurisdictionLevel.STJ10;


        return result;
    }

    private void addDeltaAmt(PurchaseTransactionDocument document) {
        BigDecimal deltaAmt = document.getPushDelta();
        if (deltaAmt.compareTo(BigDecimal.ZERO) != 0) {
            for (PurchaseTransaction deltaTarget : document.getPurchaseTransaction()) {
                JurisdictionLevel availableJurlevel = getFirstTaxableJurisdictionLevel(deltaTarget);
                if (availableJurlevel != null) {
                    BigDecimal currentAmt = null;
                    switch (availableJurlevel) {
                        case STATE:
                            currentAmt = (deltaTarget.getStateTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStateTier1TaxAmt();
                            deltaTarget.setStateTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case COUNTY:
                            currentAmt = (deltaTarget.getCountyTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getCountyTier1TaxAmt();
                            deltaTarget.setCountyTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case CITY:
                            currentAmt = (deltaTarget.getCityTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getCountyTier1TaxAmt();
                            deltaTarget.setCityTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ1:
                            currentAmt = (deltaTarget.getStj1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj1TaxAmt();
                            deltaTarget.setStj1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ2:
                            currentAmt = (deltaTarget.getStj2TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj2TaxAmt();
                            deltaTarget.setStj2TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ3:
                            currentAmt = (deltaTarget.getStj3TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj3TaxAmt();
                            deltaTarget.setStj3TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ4:
                            currentAmt = (deltaTarget.getStj4TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj4TaxAmt();
                            deltaTarget.setStj4TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ5:
                            currentAmt = (deltaTarget.getStj5TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj5TaxAmt();
                            deltaTarget.setStj5TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ6:
                            currentAmt = (deltaTarget.getStj6TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj6TaxAmt();
                            deltaTarget.setStj6TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ7:
                            currentAmt = (deltaTarget.getStj7TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj7TaxAmt();
                            deltaTarget.setStj7TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ8:
                            currentAmt = (deltaTarget.getStj8TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj8TaxAmt();
                            deltaTarget.setStj8TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ9:
                            currentAmt = (deltaTarget.getStj9TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj9TaxAmt();
                            deltaTarget.setStj9TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ10:
                            currentAmt = (deltaTarget.getStj10TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj10TaxAmt();
                            deltaTarget.setStj10TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                    }
                    deltaTarget.setTbCalcTaxAmt(recalcTbCalcTaxAmt(deltaTarget));
                    return;
                }
            }
        }
    }

    private void addDeltaAmt(MicroApiTransactionDocument document) {
        BigDecimal deltaAmt = document.getPushDelta();
        if (deltaAmt.compareTo(BigDecimal.ZERO) != 0) {
            for (MicroApiTransaction deltaTarget : document.getMicroApiTransaction()) {
                JurisdictionLevel availableJurlevel = getFirstTaxableJurisdictionLevel(deltaTarget);
                if (availableJurlevel != null) {
                    BigDecimal currentAmt = null;
                    switch (availableJurlevel) {
                        case STATE:
                            currentAmt = (deltaTarget.getStateTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStateTier1TaxAmt();
                            deltaTarget.setStateTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case COUNTY:
                            currentAmt = (deltaTarget.getCountyTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getCountyTier1TaxAmt();
                            deltaTarget.setCountyTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case CITY:
                            currentAmt = (deltaTarget.getCityTier1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getCountyTier1TaxAmt();
                            deltaTarget.setCityTier1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ1:
                            currentAmt = (deltaTarget.getStj1TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj1TaxAmt();
                            deltaTarget.setStj1TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ2:
                            currentAmt = (deltaTarget.getStj2TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj2TaxAmt();
                            deltaTarget.setStj2TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ3:
                            currentAmt = (deltaTarget.getStj3TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj3TaxAmt();
                            deltaTarget.setStj3TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ4:
                            currentAmt = (deltaTarget.getStj4TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj4TaxAmt();
                            deltaTarget.setStj4TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ5:
                            currentAmt = (deltaTarget.getStj5TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj5TaxAmt();
                            deltaTarget.setStj5TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ6:
                            currentAmt = (deltaTarget.getStj6TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj6TaxAmt();
                            deltaTarget.setStj6TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ7:
                            currentAmt = (deltaTarget.getStj7TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj7TaxAmt();
                            deltaTarget.setStj7TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ8:
                            currentAmt = (deltaTarget.getStj8TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj8TaxAmt();
                            deltaTarget.setStj8TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ9:
                            currentAmt = (deltaTarget.getStj9TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj9TaxAmt();
                            deltaTarget.setStj9TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                        case STJ10:
                            currentAmt = (deltaTarget.getStj10TaxAmt() == null)? BigDecimal.ZERO : deltaTarget.getStj10TaxAmt();
                            deltaTarget.setStj10TaxAmt(currentAmt.add(nullToZero(deltaAmt)));
                            break;
                    }
                    deltaTarget.setTbCalcTaxAmt(recalcTbCalcTaxAmt(deltaTarget));
                    return;
                }
            }
        }
    }
    
    public void recalcLocalAmounts(MicroApiTransactionDocument document) {
        for (MicroApiTransaction transaction : document.getMicroApiTransaction()) {
            TransientPurchaseTransaction transientDetails = transaction.getTransientTransaction();
            transientDetails.setLocalCountyAmtSet(Boolean.FALSE);
            transientDetails.setLocalCityAmtSet(Boolean.FALSE);
            if ("COUNTY".equals(transientDetails.getStj1LocalCode()) || "CITY".equals(transientDetails.getStj1LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj1LocalCode(), transaction.getStj1TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj2LocalCode()) || "CITY".equals(transientDetails.getStj2LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj2LocalCode(), transaction.getStj2TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj3LocalCode()) || "CITY".equals(transientDetails.getStj3LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj3LocalCode(), transaction.getStj3TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj4LocalCode()) || "CITY".equals(transientDetails.getStj4LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj4LocalCode(), transaction.getStj4TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj5LocalCode()) || "CITY".equals(transientDetails.getStj5LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj5LocalCode(), transaction.getStj5TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj6LocalCode()) || "CITY".equals(transientDetails.getStj6LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj6LocalCode(), transaction.getStj6TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj7LocalCode()) || "CITY".equals(transientDetails.getStj7LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj7LocalCode(), transaction.getStj7TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj8LocalCode()) || "CITY".equals(transientDetails.getStj8LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj8LocalCode(), transaction.getStj8TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj9LocalCode()) || "CITY".equals(transientDetails.getStj9LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj9LocalCode(), transaction.getStj9TaxAmt());
            }
            if ("COUNTY".equals(transientDetails.getStj10LocalCode()) || "CITY".equals(transientDetails.getStj10LocalCode())) {
                recalcStjLocalAmt(transaction, transientDetails.getStj10LocalCode(), transaction.getStj10TaxAmt());
            }
        }
    }
    
    private PurchaseTransaction extractPurchaseTransaction(MicroApiTransaction transaction) {
    	PurchaseTransaction updatedPurchaseTransaction = new PurchaseTransaction();
		transaction.updatePurchaseTransaction(updatedPurchaseTransaction);
		
		return updatedPurchaseTransaction;
    }
    
    private void recalcStjLocalAmt(MicroApiTransaction transaction, String localCode, BigDecimal stjAmt) {
        TransientPurchaseTransaction transientPurchaseTransaction = transaction.getTransientTransaction();
        if (stjAmt == null)
            stjAmt = BigDecimal.ZERO;
        if ("COUNTY".equals(localCode)) {
            if (!transientPurchaseTransaction.getLocalCountyAmtSet() || transaction.getCountyLocalTaxAmt() == null) {
                transaction.setCountyLocalTaxAmt(BigDecimal.ZERO);
                transientPurchaseTransaction.setLocalCountyAmtSet(Boolean.TRUE);
            }
            transaction.setCountyLocalTaxAmt(transaction.getCountyLocalTaxAmt().add(nullToZero(stjAmt)));
        } else if ("CITY".equals(localCode)) {
            if (!transientPurchaseTransaction.getLocalCityAmtSet() || transaction.getCityLocalTaxAmt() == null) {
                transaction.setCityLocalTaxAmt(BigDecimal.ZERO);
                transientPurchaseTransaction.setLocalCityAmtSet(Boolean.TRUE);
            }
            transaction.setCityLocalTaxAmt(transaction.getCityLocalTaxAmt().add(nullToZero(stjAmt)));
        }

    }
    
    public void getPurchaseTransactionReprocess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {  	
        	purchaseTransaction.setReprocessFlag("1");
        }
        
        getPurchaseTransactionEvaluateForAccrual(document, writeFlag, LogSourceEnum.getPurchaseTransactionReprocess);
    }
    
    public void getPurchaseTransactionRelease(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	getTransRelease(purchaseTransaction);
        }    
    }
    
    public void getPurchaseTransactionLoadAndRelease(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	//Check purchaseId 
        	boolean idExist = false;
        	if(purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()>0L){
        		idExist = true;
        	}
        	
        	if(idExist){
        		PurchaseTransaction tempTrans = purchaseTransactionDAO.findById(purchaseTransaction.getPurchtransId());
        		if(tempTrans==null){
        			idExist = false;
        		}
        		else{
        			BeanUtils.copyProperties(tempTrans, purchaseTransaction);
        			getTransTaxCodeDtl(purchaseTransaction, null);
        			getTransTaxRates(purchaseTransaction);
        		}
        	}

        	if(!idExist){	
        		purchaseTransaction.setErrorCode(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
                writeError(purchaseTransaction);        		
        		continue;
        	}
        	
        	getTransRelease(purchaseTransaction);
        }
    }
    
    public void getPurchaseTransactionSuspend(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	//purchaseTransaction.setSuspendCode("SUSPENDCODE");
        	try {
        		SuspendReason.valueOf(purchaseTransaction.getSuspendCode());	
            } catch(IllegalArgumentException e) {
            	purchaseTransaction.setErrorCode(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        		purchaseTransaction.setAbortFlag(true);
        		throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        	}
            
        	getTransSuspend(purchaseTransaction);
        	if(writeFlag) {
				purchaseTransactionDAO.save(extractPurchaseTransaction(purchaseTransaction));
        	}
        }
    }
    
    public void getPurchaseTransactionLoadAndSuspend(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	String suspendCode = purchaseTransaction.getSuspendCode();
        	try {
        		SuspendReason.valueOf(suspendCode);	
            } catch(IllegalArgumentException e) {
        		throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
        	}
            
        	//Check purchaseId 
        	boolean idExist = false;
        	if(purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()>0L){
        		idExist = true;
        	}
        	
        	if(idExist){
        		PurchaseTransaction tempTrans = purchaseTransactionDAO.findById(purchaseTransaction.getPurchtransId());
        		if(tempTrans==null || tempTrans.getTransactionInd().equals("S")){
        			idExist = false;
        		}
        		else{
        			try {
	        			BeanUtils.copyProperties(tempTrans, purchaseTransaction);
//	        			getTransTaxCodeDtl(purchaseTransaction, null);
//	        			getTransTaxRates(purchaseTransaction);
        			} catch(Exception e) {
        				throw new PurchaseTransactionProcessingException(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
        			}
        		}
        	}
        	if(!idExist) {
        		
        		purchaseTransaction.setErrorCode(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
                writeError(purchaseTransaction);        		
        		continue;
        	}
        	
        	purchaseTransaction.setSuspendCode(suspendCode);
        	getTransSuspend(purchaseTransaction);
        	if(writeFlag) {
				purchaseTransactionDAO.save(extractPurchaseTransaction(purchaseTransaction));
        	}
        }
    }
    
    public void getPurchaseTransactionLoad(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
    	List<MicroApiTransaction> transactions = document.getMicroApiTransaction();
    	
        for (MicroApiTransaction purchaseTransaction: transactions) {
        	//Check purchaseId 
        	boolean idExist = false;
        	if(purchaseTransaction.getPurchtransId()!=null && purchaseTransaction.getPurchtransId()>0L){
        		idExist = true;
        	}
        	
        	if(idExist){
        		PurchaseTransaction tempTrans = purchaseTransactionDAO.findById(purchaseTransaction.getPurchtransId());
        		if(tempTrans==null){
        			idExist = false;
        		}
        		else{
        			BeanUtils.copyProperties(tempTrans, purchaseTransaction);
        			getTransTaxCodeDtl(purchaseTransaction, null);
        			getTransTaxRates(purchaseTransaction);
        		}
        	}
        	
        	if(!idExist){	
        		purchaseTransaction.setErrorCode(TRANS_ERROR_PURCHTRANSID_NOT_FOUND);
                writeError(purchaseTransaction);        		
        		continue;
        	}
        }
    }
    
    private BigDecimal nullToZero(BigDecimal value){
    	return (value != null)? value : BigDecimal.ZERO;
    }
    
    public PurchaseTransactionDAO getPurchaseTransactionDAO() {
		return purchaseTransactionDAO;
	}

	public void setPurchaseTransactionDAO(PurchaseTransactionDAO purchaseTransactionDAO) {
		this.purchaseTransactionDAO = purchaseTransactionDAO;
	}

	@Override
	public boolean suspendAndLog(Long transactionId, SuspendReason suspendReason, Long matrixId, Boolean suspendExtracted) {
		PurchaseTransaction transToSuspend = purchaseTransactionDAO.findById(transactionId);
		boolean transactionSuspended = true;
//		PurchaseTransactionDocument document = new PurchaseTransactionDocument();
//		document.setPurchaseTransaction(transToSuspend);
		
		if (transToSuspend == null) 
			transactionSuspended = false;
		else if( (matrixId == null) || ((suspendReason.equals(SuspendReason.SUSPEND_LOCN) && transToSuspend.getShiptoManualJurInd() == null) || (suspendReason.equals(SuspendReason.SUSPEND_GS) && transToSuspend.getManualTaxcodeInd() == null)) && (suspendExtracted || (transToSuspend.getGlExtractFlag() == null || !transToSuspend.getGlExtractFlag().equals("1")))) {
			
			try {
				String suspendInd;
				switch(suspendReason) {
					case SUSPEND_LOCN:
						suspendInd = TRANS_SUSPEND_IND_LOCATION;
						break;
					case SUSPEND_GS:
						suspendInd = TRANS_SUSPEND_IND_GSB;
						break;
					case SUSPEND_TAXCODE_DTL:
						suspendInd = TRANS_SUSPEND_IND_TAXCODE_DTL;
						break;
					case SUSPEND_RATE:
						suspendInd = TRANS_SUSPEND_IND_RATE;
						break;
					default:
						transToSuspend.setErrorCode("P41");
						throw new PurchaseTransactionProcessingException("P41");
				}
				transToSuspend.setSuspendCode(suspendReason.toString());
				//Not sure we even want to use getTransSuspend because it's suspending a lot more than we anticipated.
				getTransSuspend(transToSuspend);
				purchaseTransactionDAO.save(transToSuspend);
			} catch (PurchaseTransactionProcessingException e) {
				transactionSuspended = false;
			}

			
//			PurchaseTransactionDocument document = new PurchaseTransactionDocument();
//			document.setPurchaseTransaction(transToSuspend);
//
//			MicroApiTransactionDocument transactionDoc = new MicroApiTransactionDocument(document);
//			
//			for(MicroApiTransaction transaction : transactionDoc.getMicroApiTransaction()) {
//				suspend(transaction, suspendReason, matrixId);
//				
//				PurchaseTransaction updatedPurchaseTransaction = new PurchaseTransaction();
//				transaction.updatePurchaseTransaction(updatedPurchaseTransaction);
//				
//				//For some reason the copyProperties utility is not working for purchase transaction and micro api document.
//				//So we lose the Id of the bean. I don't quite understand what whoever did this was thinking
//				
//				
//				//We lose the ProcessTransactionId on the updatedpurchasetransaction when it comes back through
//				//, so, I'm readding it in.
//				updatedPurchaseTransaction.setId(transactionId);
//				purchaseTransactionDAO.save(updatedPurchaseTransaction);
		} else 
			transactionSuspended = false;
		return transactionSuspended;
		
	}

	@Override
	public void suspendByMatrixId(Long matrixId, MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException {
		// TODO Implement
		MicroApiTransaction purchaseTransaction = document.getMicroApiTransaction().get(0);
		//Determine the type of matrix
		SuspendReason suspendReason = SuspendReason.valueOf(purchaseTransaction.getSuspendCode());
		
		List<PurchaseTransaction> transactionsToSuspend;
		List<MicroApiTransaction> suspendedTransactions =new ArrayList<MicroApiTransaction>();
		List<String> matrixTypes = new ArrayList<String>();

		switch(suspendReason){
			case SUSPEND_LOCN:
				matrixTypes.add("shiptoLocnMatrixId");
				matrixTypes.add("shipfromLocnMatrixId");
				matrixTypes.add("ordracptLocnMatrixId");
				matrixTypes.add("ordrorgnLocnMatrixId");
				matrixTypes.add("firstuseLocnMatrixId");
				matrixTypes.add("billtoLocnMatrixId");
				matrixTypes.add("ttlxfrLocnMatrixId");
				break;
			case SUSPEND_GS:
				matrixTypes.add("taxMatrixId");
				break;
			default:
				throw new PurchaseTransactionProcessingException(TRANS_ERROR_SUSPENDFLAG_NOT_SET);
		}
		//Find all transactions associated with that matrix

		transactionsToSuspend = purchaseTransactionDAO.findAllTransactionsByMatrixId(matrixId, matrixTypes);
		
		//Suspend each transaction.
		if(!transactionsToSuspend.isEmpty()) {
			for(PurchaseTransaction transaction: transactionsToSuspend) {
				if((transaction.getShiptoManualJurInd() == null || !transaction.getShiptoLocnMatrixId().equals(matrixId)) && (transaction.getManualTaxcodeInd() == null || !transaction.getTaxMatrixId().equals(matrixId))) {
					transaction.setSuspendCode(purchaseTransaction.getSuspendCode());

					MicroApiTransaction microApiTransaction = new MicroApiTransaction(transaction);
					getTransSuspend(microApiTransaction);
					suspendedTransactions.add(microApiTransaction);
					microApiTransaction.updatePurchaseTransaction(transaction);
					if(writeFlag)
						purchaseTransactionDAO.save(transaction);
				}
			}
		} else
			throw new PurchaseTransactionProcessingException("P34");
		document.setMicroApiTransaction(suspendedTransactions);
	}
	
	@Override
	public MicroApiTransactionDocument purchTransToMicroApiTransactionDocument(PurchaseTransaction purchTrans) {
		PurchaseTransactionDocument document = new PurchaseTransactionDocument();
		document.setPurchaseTransaction(purchTrans);
		return new MicroApiTransactionDocument(document);
	}
}

