package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.dao.TaxCodeDAO;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.services.TaxCodeService;

public class TaxCodeServiceImpl extends GenericServiceImpl<TaxCodeDAO, TaxCode, TaxCodePK> implements TaxCodeService {
	@Override
	public List<TaxCode> findTaxCodeList(String taxcodeCode, String desc, String activeFlag) {
		return getDAO().findTaxCodeList(taxcodeCode, desc, activeFlag);
	}
}
