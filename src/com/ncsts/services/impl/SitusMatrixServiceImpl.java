package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.SitusMatrixDAO;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.services.SitusMatrixService;

public class SitusMatrixServiceImpl extends GenericServiceImpl<SitusMatrixDAO, SitusMatrix, Long> implements SitusMatrixService {

	public Long count(SitusMatrix exampleInstance){
		return getDAO().count(exampleInstance);
	}
	
	public SitusMatrix find(SitusMatrix exampleInstance){
		return getDAO().find(exampleInstance);
	}
	
	public List<SitusMatrix> findByExample(SitusMatrix exampleInstance) {
		return getDAO().findByExample(exampleInstance);
	}
	
	public List<SitusMatrix> findAllSitusMatrixDetailBySitusMatrixId(Long situsMatrixId) throws DataAccessException{
   	 	return getDAO().findAllSitusMatrixDetailBySitusMatrixId(situsMatrixId);
	}
	
	public void addSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException{
   	 	getDAO().addSitusMatrix(situsMatrix);
	}
	
	public void updateSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException{
   	 	getDAO().updateSitusMatrix(situsMatrix);
	}
	
	public void inactiveSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException{
   	 	getDAO().inactiveSitusMatrix(situsMatrix);
	}
	
	public void deleteSitusMatrix(Long situsMatrixId) throws DataAccessException{
   	 	getDAO().deleteSitusMatrix(situsMatrixId);
	}
	
	public List<SitusMatrix> getAllRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException{
		return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
	}
	
	public List<SitusMatrix> getAllDetailRecords(SitusMatrix exampleInstance, String display) throws DataAccessException{
		return getDAO().getAllDetailRecords(exampleInstance, display);
	}
	
	public List<SitusMatrix> getMasterRulesRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults){
		return getDAO().getMasterRulesRecords(exampleInstance, orderBy, firstRow, maxResults);
	}
	
	public List<SitusMatrix> getMasterRulesDetailRecords(SitusMatrix situsMatrix){
		return getDAO().getMasterRulesDetailRecords(situsMatrix);
	}
	
	public Long masterRulesCount(SitusMatrix exampleInstance){
		return getDAO().masterRulesCount(exampleInstance);
	}
	

}
