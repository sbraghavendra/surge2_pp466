package com.ncsts.services.impl;


import java.util.ArrayList;
import java.util.List;

import com.ncsts.common.helper.OrderBy;

import com.ncsts.dao.ProctableDAO;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;
import com.ncsts.services.ProctableService;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public class ProctableServiceImpl extends GenericServiceImpl<ProctableDAO, Proctable, Long> implements ProctableService {
	
	public void addProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException{
		getDAO().addProctableDetailList(updateProctable, updateProctableEntityList);
	}
	
	public void updateProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException{
		getDAO().updateProctableDetailList(updateProctable, updateProctableEntityList);
	}
	
	public void deleteProctable(Proctable updateProctable) throws DataAccessException{
		getDAO().deleteProctable(updateProctable);
	}
	
	public void deleteProctableDetail(ProctableDetail aProctableDetail) throws DataAccessException{
		getDAO().deleteProctableDetail(aProctableDetail);
	}


	public List<ProctableDetail> findProctableDetail(Long proctableId) throws DataAccessException{
		return getDAO().findProctableDetail(proctableId);
	}
	
	public List<Proctable> getAllProctable() throws DataAccessException{
		return getDAO().getAllProctable();
	}
	
	public void reorderProctableList(List<Proctable> updateProctableList) throws DataAccessException{
		getDAO().reorderProctableList(updateProctableList);
	}
	
	public void populateProcruleDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableDetailList) throws DataAccessException{
		getDAO().populateProcruleDetailList(updateProctable, updateProctableDetailList);
	}
	
	public ProctableDetail findProctableDetailById(Long proctableId) throws DataAccessException {
		return getDAO().findProctableDetailById(proctableId);
	}
	
	public void updateProctableDetail(Proctable aProctable, ArrayList changedArrayList) throws DataAccessException {
		getDAO().updateProctableDetail(aProctable, changedArrayList);
	}
	
	public void addProctableDetail(Proctable updateProctable, ProctableDetail proctableDetail) throws DataAccessException{
		getDAO().addProctableDetail(updateProctable, proctableDetail);
	}

}
