package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.RoleUserMenuDAO;
import com.ncsts.domain.RoleMenu;
import com.ncsts.services.RoleUserMenuService;

public class RoleUserMenuServiceImpl implements RoleUserMenuService{
	
	private RoleUserMenuDAO roleUserMenuDAO;	
	
	public RoleUserMenuDAO getRoleUserMenuDAO() {
		return roleUserMenuDAO;
	}

	public void setRoleUserMenuDAO(RoleUserMenuDAO roleUserMenuDAO) {
		this.roleUserMenuDAO = roleUserMenuDAO;
	}

	public List<RoleMenu> findRoleMenuByRoleCode(String roleCode) throws DataAccessException{
		return roleUserMenuDAO.findRoleMenuByRoleCode(roleCode);
	}
	
	public void saveOrUpdate(RoleMenu roleMenu) throws DataAccessException{
		roleUserMenuDAO.saveOrUpdate(roleMenu);
	}
	
	public void deleteRoleMenu(RoleMenu roleMenu) throws DataAccessException{
		roleUserMenuDAO.deleteRoleMenu(roleMenu);
	}
}
