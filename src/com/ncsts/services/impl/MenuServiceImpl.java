package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.MenuDAO;
import com.ncsts.services.MenuService;
import com.ncsts.domain.Menu;

/**
 * @author Paul Govindan
 *
 */

public class MenuServiceImpl implements MenuService {
	
	private MenuDAO menuDAO;	
	
	public void setMenuDAO (MenuDAO menuDAO) {
		this.menuDAO = menuDAO;
	}
	
	public MenuDAO getMenuDAO() {
		return this.menuDAO;
	}
	
    public Menu findById(String menuCode) throws DataAccessException {
        return menuDAO.findById(menuCode);
    }	
	
    public List<Menu> findMenusByRole(String roleCode) throws DataAccessException{
    	return menuDAO.findMenusByRole(roleCode);
    }
    
    public List<Menu> findAllMenus() throws DataAccessException {
    	return menuDAO.findAllMenus();
    }
    
    public List<String> findAllMainMenuCodes() throws DataAccessException {
    	return menuDAO.findAllMainMenuCodes();
    }
    
	public void remove(String id) throws DataAccessException {
		menuDAO.remove(id);
	}
    
	public void saveOrUpdate (Menu menu) throws DataAccessException {
		menuDAO.saveOrUpdate(menu);
	}
    
	public String persist (Menu menu) throws DataAccessException {
		return menuDAO.persist(menu);
	}    

}



