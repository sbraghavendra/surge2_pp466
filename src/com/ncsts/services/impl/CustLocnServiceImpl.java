package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.common.helper.OrderBy;

import com.ncsts.dao.CustLocnDAO;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.Cust;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.services.CustLocnService;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public class CustLocnServiceImpl extends GenericServiceImpl<CustLocnDAO, CustLocn, Long> implements CustLocnService {

	public List<CustLocn> findAllCustLocn(CustLocn exampleInstance) throws DataAccessException {
		return getDAO().findAllCustLocn(exampleInstance);
	}
	
	public CustLocn getCustLocnByCustId(Long custId, String custLocnCode) throws DataAccessException {
		return  getDAO().getCustLocnByCustId(custId, custLocnCode);
	}
}
