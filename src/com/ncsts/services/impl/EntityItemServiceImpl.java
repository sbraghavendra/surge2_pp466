package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.EntityItemDAO;
import com.ncsts.domain.EntityItem;
import com.ncsts.services.EntityItemService;

/**
 * @author Paul Govindan
 *
 */

public class EntityItemServiceImpl extends GenericServiceImpl<EntityItemDAO, EntityItem, Long> implements EntityItemService {
    public EntityItem findById(Long entityId) throws DataAccessException {
        return getDAO().findById(entityId);
    }	
    
    public Long count(Long entityId) throws DataAccessException{
    	 return getDAO().count(entityId);
    }
    
    public EntityItem findByCode(String entityCode) throws DataAccessException {
        return getDAO().findByCode(entityCode);
    }
    
    public EntityItem findByName(String entityName) throws DataAccessException {
        return getDAO().findByName(entityName);
    }
    
    public EntityItem findByCode(String entityCode, Long parentId) throws DataAccessException {
        return getDAO().findByCode(entityCode,parentId);
    }
    
    public EntityItem findByName(String entityName, Long parentId) throws DataAccessException {
        return getDAO().findByName(entityName,parentId);
    }
    
    public List<EntityItem> findAllEntityItems() throws DataAccessException {
    	return getDAO().findAllEntityItems();
    }
    
    public List<EntityItem> findAllEntityItemsBylockflag() throws DataAccessException {
    	return getDAO().findAllEntityItemsBylockflag();
    }
    public List<EntityItem> findAllEntityItemsWithFilter(Long level, String entityCode, String entityName) throws DataAccessException {
    	return getDAO().findAllEntityItemsWithFilter(level, entityCode, entityName);
    }
    
    public List<EntityItem> findAllEntityItemsByLevel(Long level) throws DataAccessException{
    	return getDAO().findAllEntityItemsByLevel(level);
    }
    
    public List<EntityItem> findAllEntityItemsByParent(Long parentId) throws DataAccessException{
    	return getDAO().findAllEntityItemsByParent(parentId);
    }
    
    public List<EntityItem> findAllEntityItemsByLevelAndParent(Long level, Long parentId) throws DataAccessException{
    	return getDAO().findAllEntityItemsByLevelAndParent(level, parentId) ;
    }
    
    public Long persist(EntityItem entityItem)throws DataAccessException {
    	return getDAO().persist(entityItem);
    }
    
    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate (EntityItem entityItem) throws DataAccessException{
    	getDAO().saveOrUpdate(entityItem);
    }
    
    public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException {
    	return getDAO().isAllowToDeleteEntity(entityId);
    }
    
    public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException {
    	getDAO().copyEntityComponents(fromEntityId, toEntityId, selectedComponents);
    }
    public void deleteEntity(Long entityId) throws DataAccessException {
    	getDAO().deleteEntity(entityId);
    }
    
    public void setSubEntityInactive(Long entityId) throws DataAccessException{
    	getDAO().setSubEntityInactive(entityId);
    }
    
    public List<EntityItem> findAllEntityItemsWithCustomer(Long custId) throws DataAccessException {
    	return getDAO().findAllEntityItemsWithCustomer(custId);
    }
    
    public List<EntityItem> getAllRecords(EntityItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause) throws DataAccessException{
    	return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults, whereClause);
    }
    
	public Long count(EntityItem exampleInstance, String whereClause){
		return getDAO().count(exampleInstance, whereClause);
	}
	
	public List<Object[]> findByJurisdictionId(Long jurisdictionId) {
		return getDAO().findByJurisdictionId(jurisdictionId);
	}

}


