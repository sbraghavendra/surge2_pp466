package com.ncsts.services.impl;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.ScenarioDAO;
import com.ncsts.domain.BillScenario;
import com.ncsts.domain.PurchaseScenario;
import com.ncsts.services.ScenarioService;

public class ScenarioServiceImpl extends GenericServiceImpl<ScenarioDAO, BillScenario, Long> implements ScenarioService {
	
	public Long count(BillScenario exampleInstance) {
		return getDAO().count(exampleInstance);
	}
	
	public List<BillScenario> getAllRecords(BillScenario exampleInstance, OrderBy orderBy, int firstRow, int maxResults){
		return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
	}

	@Override
	public List<String> getScenarioNames() {
		return getDAO().getScenarioNames();
	}

	@Override
	public int deleteByName(String name) {
		return getDAO().deleteByName(name);
	}

	@Override
	public Long count(PurchaseScenario exampleInstance) {
		return getDAO().count(exampleInstance);
	}

	@Override
	public List<PurchaseScenario> getAllRecords(PurchaseScenario exampleInstance, OrderBy orderBy, int firstRow,
			int maxResults) {
		return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
	}

	@Override
	public List<String> getPurchaseScenarioNames() {
		return getDAO().getPurchaseScenarioNames();
	}

	@Override
	public int deleteByNamePurchase(String name) {
		return getDAO().deleteByNamePurchase(name);
	}

	@Override
	public void save(PurchaseScenario instance) {
		getDAO().savePurchaseScenario(instance);
	}

	public List<PurchaseScenario> findByExample(PurchaseScenario exampleInstance, int count, String... excludeProperty) {
		return getDAO().findByExample(exampleInstance, count, excludeProperty);
	}
	
	public List<PurchaseScenario> findByExampleSorted(PurchaseScenario exampleInstance, int count,
			String sortField, boolean ascending, String... excludeProperty) {
		return getDAO().findByExampleSorted(exampleInstance, count, sortField, ascending, excludeProperty);
	}


	
	
}
