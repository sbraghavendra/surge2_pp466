package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.RoleDAO;
import com.ncsts.services.RoleService;
import com.ncsts.domain.Role;

/**
 * @author Paul Govindan
 *
 */

public class RoleServiceImpl implements RoleService {
	
	private RoleDAO roleDAO;	
	
	public void setRoleDAO (RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}
	
	public RoleDAO getRoleDAO() {
		return this.roleDAO;
	}
	
    public Role findById(String roleCode) throws DataAccessException {
        return roleDAO.findById(roleCode);
    }	
	
    public List<Role> findAllRoles() throws DataAccessException {
    	return roleDAO.findAllRoles();
    }
    
	public void remove(String id) throws DataAccessException {
		roleDAO.remove(id);
	}
    
	public void saveOrUpdate (Role role) throws DataAccessException {
		roleDAO.saveOrUpdate(role);
	}
    
	public String persist (Role role) throws DataAccessException {
		return roleDAO.persist(role);
	}

}


