package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.dao.TaxCodeStateDAO;
import com.ncsts.domain.TaxCodeState;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.TaxCodeStateService;

public class TaxCodeStateServiceImpl implements TaxCodeStateService {
	
	private TaxCodeStateDAO taxCodeStateDAO;
	
	protected List<TaxCodeStateDTO> convertList(List<TaxCodeState> items) {
		List<TaxCodeStateDTO> result = new ArrayList<TaxCodeStateDTO>();	
		for (TaxCodeState item : items) {
			TaxCodeStateDTO dto = new TaxCodeStateDTO();
			BeanUtils.copyProperties(item, dto);
			
			dto.setCountry(item.getCountry());
			dto.setTaxCodeState(item.getTaxcodeStateCode());
		//	item.setTaxCodeStatePK(new TaxCodeStatePK(dto.getCountry(), dto.getTaxCodeState()));
			result.add(dto);
		}
		return result;
	}
	
	public List<TaxCodeStateDTO> getAllTaxCodeStateDTO() {
		return convertList(taxCodeStateDAO.getAllTaxCodeState());
	}
	
	public TaxCodeStateDAO getTaxCodeStateDAO() {
		return taxCodeStateDAO;
	}
	public void setTaxCodeStateDAO(TaxCodeStateDAO taxCodeStateDAO) {
		this.taxCodeStateDAO = taxCodeStateDAO;
	}

	public List<TaxCodeStateDTO> getActiveTaxCodeState() {
		return convertList(taxCodeStateDAO.getActiveTaxCodeState());
	}
	
	public List<TaxCodeStateDTO> findByTaxCode(String taxCodeType, String taxcodeCode) {
		return convertList(taxCodeStateDAO.findByTaxCode(taxCodeType, taxcodeCode));
	}

	public List<TaxCodeStateDTO> findByStateName(String stateName) {
		return convertList(taxCodeStateDAO.findByStateName(stateName));
	}

	public List<TaxCodeStateDTO> findByStateCode(String stateCode) {
		return convertList(taxCodeStateDAO.findByStateCode(stateCode));
	}
	
	
	public TaxCodeStateDTO save(TaxCodeStateDTO instanceDTO) throws DataAccessException{		
		TaxCodeState taxCodeStateObject = getTaxCodeStateObject(instanceDTO);
		return taxCodeStateDAO.add(taxCodeStateObject).getTaxCodeStateDTO();		
	}
	
	public void update(TaxCodeStateDTO instanceDTO) throws DataAccessException{
		TaxCodeState taxCodeStateObject = getTaxCodeStateObject(instanceDTO);
		taxCodeStateDAO.update(taxCodeStateObject);
	}
	
	public void remove(TaxCodeStatePK pk) throws DataAccessException{
		taxCodeStateDAO.delete(pk);
	}
	
	private TaxCodeState getTaxCodeStateObject(TaxCodeStateDTO instanceDTO) throws DataAccessException{
		TaxCodeState taxCodeState= new TaxCodeState(new TaxCodeStatePK(instanceDTO.getCountry(), instanceDTO.getTaxCodeState()));
		BeanUtils.copyProperties(instanceDTO,taxCodeState);
		return taxCodeState;
	}
	
	public TaxCodeStateDTO findByPK(TaxCodeStatePK pk) throws DataAccessException{
		TaxCodeState aTaxCodeState = taxCodeStateDAO.findByPK(pk);
		if(aTaxCodeState==null){
			return null;
		}
		return taxCodeStateDAO.findByPK(pk).getTaxCodeStateDTO();
	}

	public void batchQuickUpdate(List<TaxCodeStateDTO> quickUpdateList) {	
		List<TaxCodeState> result = new ArrayList<TaxCodeState>();		
		for(TaxCodeStateDTO instanceDTO : quickUpdateList) {	
			TaxCodeState state = getTaxCodeStateObject(instanceDTO);
			result.add(state);
		}
		
		taxCodeStateDAO.batchQuickUpdate(result);
	}
	
	@Override
	public List<TaxCodeStateDTO> findByCountryStateActive(String country, String state, String activeFlag) {
		List<TaxCodeState> list = taxCodeStateDAO.findByExampleSorted(country, state, activeFlag);
		return list == null ? null : convertList(list);
	}

	@Override
	public boolean isCountryUsed(String countryName) {
		return taxCodeStateDAO.isCountryUsed(countryName);
	}

	@Override
	public void deleteCountry(TaxCodeStateDTO instance) {
		taxCodeStateDAO.deleteCountry(getTaxCodeStateObject(instance));
		
	}

	@Override
	public void updateCountry(TaxCodeStateDTO instance) {
		taxCodeStateDAO.updateCountry(getTaxCodeStateObject(instance));
	}

	@Override
	public List<TaxCodeStateDTO> findByCountryName(String countryName) {
		// TODO Auto-generated method stub
		return convertList(taxCodeStateDAO.findByCountryName(countryName));
	}
}
