package com.ncsts.services.impl;

import java.util.List;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.RegistrationDAO;
import com.ncsts.services.RegistrationService;
import com.ncsts.domain.Registration;

public class RegistrationServiceImpl extends GenericServiceImpl<RegistrationDAO, Registration, Long> implements RegistrationService {
	
	public List<Registration> getAllRecords(Registration exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException {
   	 	return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults);
    }
	public Long count(Registration exampleInstance) throws DataAccessException{
   	 	return getDAO().count(exampleInstance);
    }
    
    public List<Registration> findAllRegistrations() throws DataAccessException {
    	return getDAO().findAllRegistrations();
    }
    
    public List<Registration> findAllRegistrationsByEntityId(Long entityId) throws DataAccessException{
    	return getDAO().findAllRegistrationsByEntityId(entityId);
    }
    
    public Long persist(Registration registration)throws DataAccessException {
    	return getDAO().persist(registration);
    }
    
    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate(Registration registration) throws DataAccessException{
    	getDAO().saveOrUpdate(registration);
    }
}