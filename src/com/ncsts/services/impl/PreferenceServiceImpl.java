package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.ncsts.dao.PreferenceDAO;
import com.ncsts.dao.PreferenceDAO.AdminPreference;
import com.ncsts.dao.PreferenceDAO.SystemPreference;
import com.ncsts.dao.PreferenceDAO.UserPreference;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.services.PreferenceService;

public class PreferenceServiceImpl implements PreferenceService {
	private PreferenceDAO preferenceDAO;

	public String getAdminPreference(AdminPreference preference, String defValue) {
		return preferenceDAO.getAdminPreference(preference, defValue);
	}
	
	public Long getAdminPreferenceAsLong(AdminPreference preference, Long defValue) {
		String value = preferenceDAO.getAdminPreference(preference, null);
		Long result = defValue;
		if (value != null) {
			try {
				result = Long.parseLong(value.trim());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public String getSystemPreference(SystemPreference preference, String defValue) {
		return preferenceDAO.getSystemPreference(preference, defValue);
	}
	
	public Long getSystemPreferenceAsLong(SystemPreference preference, Long defValue) {
		String value = preferenceDAO.getSystemPreference(preference, null);
		Long result = defValue;
		if (value != null) {
			try {
				result = Long.parseLong(value.trim());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public String getUserPreference(UserPreference preference, String defValue) {
		return preferenceDAO.getUserPreference(preference, defValue);
	}
	
	public Long getUserPreferenceAsLong(UserPreference preference, Long defValue) {
		String value = preferenceDAO.getUserPreference(preference, null);
		Long result = defValue;
		if (value != null) {
			try {
				result = Long.parseLong(value.trim());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
//	public CompanyDTO getCompany() {
//		return preferenceDAO.getCompany();
//	}
	
	public List<ListCodesDTO> getTimeZoneList() {
		List<ListCodesDTO> list = new ArrayList<ListCodesDTO>();
		List<ListCodes> listTimeZone = preferenceDAO.getTimeZoneList();
		for(ListCodes lc : listTimeZone){
			list.add(lc.getListCodesDTO());
		}
		return list;
	}
	
	public List<ListCodesDTO> getDefTaxCodeList() {
		List<ListCodesDTO> list = new ArrayList<ListCodesDTO>();
		List<ListCodes> listTaxCode = preferenceDAO.getDefTaxCodeList();
		for(ListCodes lc : listTaxCode){
			list.add(lc.getListCodesDTO());
		}
		return list;
	}

	public List<ListCodesDTO> getDefTransIndList() {
		List<ListCodesDTO> list = new ArrayList<ListCodesDTO>();
		List<ListCodes> listTransInd = preferenceDAO.getDefTransIndList();
		for(ListCodes lc : listTransInd){
			list.add(lc.getListCodesDTO());
		}
		return list;
	}

	public PreferenceDAO getPreferenceDAO() {
		return preferenceDAO;
	}
	public void setPreferenceDAO(PreferenceDAO preferenceDAO) {
		this.preferenceDAO = preferenceDAO;
	}

	@Override
	public List<ListCodesDTO> getDefTaxTypeList() {
		List<ListCodesDTO> list = new ArrayList<ListCodesDTO>();
		List<ListCodes> listTaxCode = preferenceDAO.getDefTaxTypeList();
		for(ListCodes lc : listTaxCode){
			list.add(lc.getListCodesDTO());
		}
		return list;
	}

	@Override
	public List<ListCodesDTO> getDefRateTypeList() {
		List<ListCodesDTO> list = new ArrayList<ListCodesDTO>();
		List<ListCodes> listTaxCode = preferenceDAO.getDefRateTypeList();
		for(ListCodes lc : listTaxCode){
			list.add(lc.getListCodesDTO());
		}
		return list;
	}


}
