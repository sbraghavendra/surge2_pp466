package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.UserMenuExDAO;
import com.ncsts.domain.UserMenuEx;
import com.ncsts.services.UserMenuExService;

public class UserMenuExImpl implements UserMenuExService {
	
	private UserMenuExDAO userMenuExDAO;
	
	public UserMenuExDAO getUserMenuExDAO() {
		return userMenuExDAO;
	}

	public void setUserMenuExDAO(UserMenuExDAO userMenuExDAO) {
		this.userMenuExDAO = userMenuExDAO;
	}
	
	 public List<UserMenuEx> findByUserEntity(String userCode,Long entityId) throws DataAccessException{
		 return this.userMenuExDAO.findByUserEntity(userCode, entityId);
	 }
	 
	 public List<UserMenuEx> findByUserCode(String userCode) throws DataAccessException{
		 return this.userMenuExDAO.findByUserCode(userCode);
	 }

	public void saveOrUpdate (UserMenuEx userMenuEx) throws DataAccessException{
		userMenuExDAO.saveOrUpdate(userMenuEx);
	}
	
	public void delete(UserMenuEx userMenuEx) throws DataAccessException{
		userMenuExDAO.delete(userMenuEx);
	}

}
