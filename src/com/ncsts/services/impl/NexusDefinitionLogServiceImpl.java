package com.ncsts.services.impl;

import com.ncsts.dao.NexusDefinitionLogDAO;
import com.ncsts.domain.NexusDefinitionLog;
import com.ncsts.services.NexusDefinitionLogService;

public class NexusDefinitionLogServiceImpl extends GenericServiceImpl<NexusDefinitionLogDAO, NexusDefinitionLog, Long> implements NexusDefinitionLogService {

}
