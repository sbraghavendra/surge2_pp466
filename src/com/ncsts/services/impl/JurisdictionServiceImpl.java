/**
 * 
 */
package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.services.JurisdictionService;

/**
 * @author Anand
 *
 */
public class JurisdictionServiceImpl extends GenericServiceImpl<JurisdictionDAO, Jurisdiction, Long> implements JurisdictionService {
	
	public List<String> findTaxCodeCounty(String countryCode, String stateCode) throws DataAccessException{
		return getDAO().findTaxCodeCounty(countryCode, stateCode);
	}

	public List<String> findTaxCodeCity(String countryCode, String stateCode) throws DataAccessException{
		return getDAO().findTaxCodeCity(countryCode, stateCode);
	}
	
	public List<JurisdictionTaxrate> getAllJurisdictionTaxrate(
			JurisdictionTaxrate jurisdictionTaxrate, int count){
		return getDAO().getAllJurisdictionTaxrate(jurisdictionTaxrate, count);
	}
	
	public Jurisdiction copyToNew(Long jurisdictionId) {
		Jurisdiction jurisdiction = findById(jurisdictionId);
		
		if (jurisdiction != null) {
			jurisdiction.setJurisdictionId(null);
			jurisdiction.setUpdateTimestamp(null);
			jurisdiction.setUpdateUserId(null);
		} else {
			jurisdiction = new Jurisdiction();
		}
		
		return jurisdiction;
	}
	
	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance) {
		return getDAO().searchByExample(exampleInstance);
	}

	public List<JurisdictionTaxrate> findByIdAndDate(
			JurisdictionTaxrate jurisdictionTaxrate) throws DataAccessException {
		// TODO Auto-generated method stub
		return getDAO().findByIdAndDate(jurisdictionTaxrate);
	}
	
	public List<JurisdictionTaxrate> findByIdAndMeasureType(JurisdictionTaxrate jurisdictionTaxrate) throws DataAccessException{
		return getDAO().findByIdAndMeasureType(jurisdictionTaxrate);
	}

	public List<Jurisdiction> findByTaxCodeStateCode(String stateCode)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return getDAO().findByTaxCodeStateCode(stateCode);
	}

	@Override
	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance,
			boolean exact) {
		return getDAO().searchByExample(exampleInstance, exact);
	}

	@Override
	public List<Object[]> findNexusJurisdiction(Long entityId, String countryCode,
			String stateCode, String nexusIndCode, String type) throws DataAccessException {
		return getDAO().findNexusJurisdiction(entityId, countryCode, stateCode, nexusIndCode, type);
	}
	
	public List<String> findCountryStateStj(String countryCode, String stateCode) throws DataAccessException {
		return getDAO().findCountryStateStj(countryCode, stateCode);
	}
	
	@Override
	public List<Object[]> findRegistrationJurisdiction(Long entityId, String countryCode,
			String stateCode, String nexusIndCode, String type) throws DataAccessException {
		return getDAO().findRegistrationJurisdiction(entityId, countryCode, stateCode, nexusIndCode, type);
	}
	
	public Long partialJurisdictionCount(Jurisdiction exampleInstance){
		return getDAO().partialJurisdictionCount(exampleInstance);
	}
	
	public List<Object[]> findSitusJurisdiction(String countryCode,
			String stateCode, String transactionTypeCode, String ratetypeCode, String methodDeliveryCode, String nexusIndCode, String type){
		return getDAO().findSitusJurisdiction(countryCode,stateCode,transactionTypeCode,ratetypeCode,methodDeliveryCode, nexusIndCode, type);
	}
	
	public List<Object[]> findJurisdictionForCountryState(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException {
		return getDAO().findJurisdictionForCountryState(id, geocode, country, state, county, city, zip, stj, area);
	}
	
	public List<Object[]> findJurisdictionForCountyCityStj(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException {
		return getDAO().findJurisdictionForCountyCityStj(id, geocode, country, state, county, city, zip, stj, area);
	}
	
	public List<String> findLocalCounty(String country, String state, String city, String stj) throws DataAccessException {
		return getDAO().findLocalCounty(country, state, city, stj);
	}
	
	public List<String> findLocalCity(String country, String state, String county, String stj) throws DataAccessException {
		return getDAO().findLocalCity(country, state, county, stj);
	}
	
	public List<String> findLocalStj(String country, String state, String county, String city) throws DataAccessException {
		return getDAO().findLocalStj(country, state, county, city);
	}
	
	public List<String> checkJurisdictionNotUsed(Long jurisdictionId) {
		return getDAO().checkJurisdictionNotUsed(jurisdictionId);
	}
}
