package com.ncsts.services.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DwColumnDAO;
import com.ncsts.domain.DwFilter;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.services.DwColumnService;

public class DwColumnServiceImpl implements DwColumnService {

	private Logger logger = LoggerFactory.getInstance().getLogger(DwColumnServiceImpl.class);
	
	private DwColumnDAO dwColumnDAO;

    public DwColumnDAO getDwColumnDAO() {
		return dwColumnDAO;
	}

	public void setDwColumnDAO(DwColumnDAO dwColumnDAO) {
		this.dwColumnDAO = dwColumnDAO;
	}
	
	public List<TransactionHeader> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
		return dwColumnDAO.getFilterNamesByUserCode(windowName, userCode);
	}
	
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException {
		return dwColumnDAO.getFilterNamesPerUserCode(windowName, userCode);
	}
	
	public List<TransactionHeader> getColumnsByDataWindowName(String windowName, String dataWindowName) throws DataAccessException {
		return dwColumnDAO.getColumnsByDataWindowName(windowName, dataWindowName);
	}
	
	public List<TransactionHeader> getColumnsByUserandDataWindowName(String windowName, String dataWindowName,String username) throws DataAccessException {
		return dwColumnDAO.getColumnsByUserandDataWindowName(windowName, dataWindowName,username);
	}
	
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		dwColumnDAO.deleteColumnsByFilterNameandUserCode(windowName, filterName, userCode);
	}
	
	public void updateColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		dwColumnDAO.updateColumnsByFilterName(windowName, filterName, transactionHeaders);
	}
	
	public void addColumnsByFilterName(String windowName, String filterName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		dwColumnDAO.addColumnsByFilterName(windowName, filterName, transactionHeaders);
	}
	
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException {
		return dwColumnDAO.isFilterNameExist(windowName, filterName);
	}
	
	public String getUserByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		return dwColumnDAO.getUserByFilterNameandUserCode(windowName, filterName, userCode);
	}
	
	public void updateColumnsByUserAndFilterName(String screenName, String selectedColumnName, String selectedUserName, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		dwColumnDAO.updateColumnsByUserAndFilterName(screenName, selectedColumnName, selectedUserName,  transactionHeaders);
	}
	public void addColumnsByUserAndFilterName(String windowName, String filterName, String username, List<TransactionHeader> transactionHeaders) throws DataAccessException {
		dwColumnDAO.addColumnsByUserAndFilterName(windowName, filterName, username, transactionHeaders);
	}
	public void deleteColumnsByUserAndFilterName(String windowName, String filterName, String username) throws DataAccessException {
		dwColumnDAO.deleteColumnsByUserAndFilterName(windowName, filterName, username);
	}
}

