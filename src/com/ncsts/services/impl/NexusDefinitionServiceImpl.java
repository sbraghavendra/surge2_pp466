package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.NexusDefinitionDAO;
import com.ncsts.domain.NexusDefinition;
import com.ncsts.domain.NexusDefinitionDetail;
import com.ncsts.services.NexusDefinitionService;
import com.ncsts.view.bean.JurisdictionItem;

public class NexusDefinitionServiceImpl extends GenericServiceImpl<NexusDefinitionDAO, NexusDefinition, Long> implements NexusDefinitionService {
	public NexusDefinition findOneByExample(NexusDefinition exampleInstance) throws DataAccessException{
		return this.getDAO().findOneByExample(exampleInstance);
	}
	
	public boolean removeNexusDefinition(List<NexusDefinition> nexusDefinitionArray) throws DataAccessException{
		return this.getDAO().removeNexusDefinition(nexusDefinitionArray);
	}
	
	public boolean addNexusDefinition(List<NexusDefinition> nexusDefinitionArray, List<NexusDefinitionDetail> nexusDefinitionDetailArray) throws DataAccessException{
		return this.getDAO().addNexusDefinition(nexusDefinitionArray, nexusDefinitionDetailArray);
	}
	
	public boolean isIndependentCounty(Long entityId, String geocode, String country, String state, String county, String city, String zip, String stj) throws DataAccessException{
		return this.getDAO().isIndependentCounty(entityId, geocode, country, state, county, city, zip, stj);
	}
}
