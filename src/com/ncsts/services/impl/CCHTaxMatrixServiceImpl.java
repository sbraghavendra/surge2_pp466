package com.ncsts.services.impl;


import com.ncsts.dao.CCHTaxMatrixDAO;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CCHTaxMatrixPK;
import com.ncsts.services.CCHTaxMatrixService;

public class CCHTaxMatrixServiceImpl extends GenericServiceImpl<CCHTaxMatrixDAO, CCHTaxMatrix, CCHTaxMatrixPK> implements CCHTaxMatrixService {
}
