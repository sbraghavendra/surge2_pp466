/**
 * 
 */
package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DataDefinitionDAO;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.DataDefinitionTable;
import com.ncsts.dto.DataDefinitionColumnDTO;
import com.ncsts.dto.DataDefinitionTableDTO;
import com.ncsts.services.DataDefinitionService;

/**
 * @author Muneer Basha
 *
 */
public class DataDefinitionServiceImpl implements DataDefinitionService {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(DataDefinitionServiceImpl.class);
	
	DataDefinitionDAO dataDefinitionDAO;

	public DataDefinitionTableDTO findById(String tableName) {
		DataDefinitionTable table = dataDefinitionDAO.findById(tableName); 
		return (table == null)? null:table.getDataDefinitionTableDTO();
	}

	public List<DataDefinitionTableDTO> getAllDataDefinitionTable() throws DataAccessException{
		List<DataDefinitionTable> dataDefinitionTableList = dataDefinitionDAO.getAllDataDefinitionTable();
		List<DataDefinitionTableDTO> dataDefinitionTableDTOList = new ArrayList<DataDefinitionTableDTO>();
		for(DataDefinitionTable dataDefinitionTable : dataDefinitionTableList){
			dataDefinitionTableDTOList.add(dataDefinitionTable.getDataDefinitionTableDTO());
		}
		return dataDefinitionTableDTOList;
	}

	public DataDefinitionDAO getDataDefinitionDAO() {
		return dataDefinitionDAO;
	}

	public void setDataDefinitionDAO(DataDefinitionDAO dataDefinitionDAO) {
		this.dataDefinitionDAO = dataDefinitionDAO;
	}

	public List<DataDefinitionColumn> getAllDataDefinitionColumnByTable(String tableName) {
		return dataDefinitionDAO.getAllDataDefinitionColumnByTable(tableName);
	}
	
	public DataDefinitionColumn getDataDefinitionColumnByTable(String tableName , String columnName) {
		DataDefinitionColumnPK columnPK = new DataDefinitionColumnPK(tableName , columnName);
		return dataDefinitionDAO.findByDataDefinitionColumnPK(columnPK);
	}
	
	public List<DataDefinitionColumn> getAllDataDefinitionColumnByColumnName(String columnName){
		String likeColumn = concat(columnName, "%");
		return dataDefinitionDAO.getAllDataDefinitionColumnByColumnName(likeColumn);
	}
	
	public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDesc(String columnName, String description, String dataType) throws DataAccessException{
		logger.info("Inside Service Impl DataDefinitionServiceImpl");
		String likeColumn = concat(columnName, "%");
		String likeDescription = concat(description, "%");
		String likeDataType = concat(dataType, "%");
		
		return dataDefinitionDAO.getAllDataDefinitionByColumnNameAndDesc(likeColumn, likeDescription, likeDataType);
		
	}
	
	public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDescDriver(String columnName, String description, String dataType,String driverCode) throws DataAccessException{
		logger.info("Inside Service Impl DataDefinitionServiceImpl");
		String likeColumn = concat(columnName, "%");
		String likeDescription = concat(description, "%");
		String likeDataType = concat(dataType, "%");
		
		return dataDefinitionDAO.getAllDataDefinitionByColumnNameAndDescDriver(likeColumn, likeDescription, likeDataType,driverCode);
	}

	public List<DataDefinitionColumnDTO> getAllDataDefinitionColumn(DataDefinitionColumnDTO dataDefinitionColumnDTO)
											throws DataAccessException{
		List<DataDefinitionColumnDTO> list=new ArrayList<DataDefinitionColumnDTO>();
		//List<DataDefinitionColumn> tempList=dataDefinitionDAO.getAllDataDefinitionColumn(getDataDefinitionColumn(dataDefinitionColumnDTO));
		
		logger.debug( "In service :::dataDefinitionColumnDTO:::"+dataDefinitionColumnDTO);
		logger.debug( "In service :::dataDefinitionColumnDTO.getTableName:::"+dataDefinitionColumnDTO.getTableName());
		String tempTableName = dataDefinitionColumnDTO.getTableName();
		//List<DataDefinitionColumn> tempList=dataDefinitionDAO.getAllDataDefinitionColumnByTable(getDataDefinitionColumn(dataDefinitionColumnDTO));
		List<DataDefinitionColumn> tempList=dataDefinitionDAO.getAllDataDefinitionColumnByTable(tempTableName);
		if(tempList!=null){
			for(DataDefinitionColumn dataDefinitionColumn : tempList){
				list.add(dataDefinitionColumn.getDataDefinitionColumnDTO());
			}
		}
		return list;
	}
	
	public DataDefinitionTableDTO addNewDataDefinitionTable(DataDefinitionTableDTO dataDefinitionTableDTO) 
								  throws DataAccessException{
		DataDefinitionTable dataDefinitionTableObject = getDataDefinitionTable(dataDefinitionTableDTO);		
		logger.debug("we are before calling addNewDataDefinitionTable method in service::"+dataDefinitionTableObject);
		logger.debug(" dataDefinitionTableObject values are ::"+dataDefinitionTableObject.getTableName());
		return (dataDefinitionDAO.addNewDataDefinitionTable(dataDefinitionTableObject)).getDataDefinitionTableDTO();
	}
	
	private DataDefinitionTable getDataDefinitionTable(DataDefinitionTableDTO dataDefinitionTableDTO)
	throws DataAccessException{

		DataDefinitionTable dataDefinitionTable= new DataDefinitionTable();
		logger.debug(" getDataDefinitionTable in Service Method :"); 
		logger.debug(" inside getDataDefinitionTable from Service method the Table Name is :"+dataDefinitionTableDTO.toString());
		BeanUtils.copyProperties(dataDefinitionTableDTO,dataDefinitionTable);
		return dataDefinitionTable;
	}	
	
	private DataDefinitionColumn getDataDefinitionColumn(DataDefinitionColumnDTO dataDefinitionColumnDTO) throws DataAccessException{
		DataDefinitionColumn dataDefinitionColumn= new DataDefinitionColumn();
		BeanUtils.copyProperties(dataDefinitionColumnDTO,dataDefinitionColumn);
		return dataDefinitionColumn;
	}
	
	public void update(DataDefinitionColumnDTO dataDefinitionColumnDTO) throws DataAccessException{
		DataDefinitionColumn dataDefinitionColumn= getDataDefinitionColumn(dataDefinitionColumnDTO);
		dataDefinitionDAO.update(dataDefinitionColumn);
	}
	
    public void delete(String tableName , String columnName) throws DataAccessException{
    	DataDefinitionColumnPK columnPK = new DataDefinitionColumnPK(tableName , columnName);
    	dataDefinitionDAO.delete(columnPK);
    }
    
    public DataDefinitionColumnDTO addNewDataDefinitionColumn(DataDefinitionColumnDTO dataDefinitionColumnDTO) 
	  throws DataAccessException{
    	DataDefinitionColumn dataDefinitionColumnObject = getDataDefinitionColumn(dataDefinitionColumnDTO);	
    	return (dataDefinitionDAO.addNewDataDefinitionColumn(dataDefinitionColumnObject)).getDataDefinitionColumnDTO();
    }
    
    public List<String> getColumnNameList(String tableName) {
		return dataDefinitionDAO.getColumnNameList(tableName);
	}
    
    public List<String> getTableNameList() {
		return dataDefinitionDAO.getTableNameList();
	}
    
    public List<String> getDataTypeList() {
		return dataDefinitionDAO.getDataTypeList();
	}
    
    public List<String> getDescriptionColumnList(String tableName) {
		return dataDefinitionDAO.getDescriptionColumnList(tableName);
	}
    
    public void update(DataDefinitionTableDTO dataDefinitionTableDTO) throws DataAccessException{
		DataDefinitionTable dataDefinitionTable = getDataDefinitionTable(dataDefinitionTableDTO);
		dataDefinitionDAO.update(dataDefinitionTable);
	}
	
    public void delete(String tableName) throws DataAccessException{
    	dataDefinitionDAO.delete(tableName);
    }
    
    public List<DataDefinitionTableDTO> getExportDataDefinitionTables()
            throws DataAccessException
        {
            List<DataDefinitionTable> dataDefinitionTableList = dataDefinitionDAO.getExportDataDefinitionTables();
            List<DataDefinitionTableDTO> dataDefinitionTableDTOList = new ArrayList();
            DataDefinitionTable dataDefinitionTable;
            for(Iterator ir = dataDefinitionTableList.iterator(); ir.hasNext(); dataDefinitionTableDTOList.add(dataDefinitionTable.getDataDefinitionTableDTO()))
                dataDefinitionTable = (DataDefinitionTable)ir.next();

            return dataDefinitionTableDTOList;
        }

    public DataDefinitionTableDTO getDataDefinitionByConfigType(String configFileType) throws DataAccessException{
 		DataDefinitionTable dataDefTable = dataDefinitionDAO.getDataDefinitionByConfigType(configFileType);
 		return (dataDefTable != null)? dataDefTable.getDataDefinitionTableDTO() : null;
 	}

	@Override
	public List<DataDefinitionTable> getAllImportExportTypes() {
		return dataDefinitionDAO.getAllImportExportTypes();
	}



	/**
     *  concat two strings and handle case where they could be null
     * @param a
     * @param b
     * @return
     */
    private String concat(String a, String b) {
      String s = "";
      if (a != null) s = s + a;
      if (b != null) s = s + b;
      return s;
    }
    public void update(DataDefinitionColumn dataDefinitionColumn) throws DataAccessException{
		dataDefinitionDAO.update(dataDefinitionColumn);
	}
	
}
