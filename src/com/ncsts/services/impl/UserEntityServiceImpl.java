package com.ncsts.services.impl;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.UserEntityDAO;
import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.UserEntityService;

public class UserEntityServiceImpl implements UserEntityService {
	
	private UserEntityDAO userEntityDAO;
	
	public void setUserEntityDAO (UserEntityDAO userEntityDAO) {
		this.userEntityDAO = userEntityDAO;
	}
	
	public UserEntityDAO getUserEntityDAO () {
		return this.userEntityDAO;
	}
	
	public List<UserEntity> findByUserEntity(UserEntity userEntity) throws DataAccessException {
		List<UserEntity> userEntityList= userEntityDAO.findByUserEntity(userEntity, 0); // 0 = all matches		
		return userEntityList;		
	}
	
    public List<UserEntity> findAll() throws DataAccessException {
	    return userEntityDAO.findAll();	    	
    }
    
    public UserEntity findById(UserEntityPK userEntityPK) throws DataAccessException{
    	return userEntityDAO.findById(userEntityPK);
    }
    
    public List<UserEntity> findByUserCode(String userCode) throws DataAccessException{
    	return userEntityDAO.findByUserCode(userCode);
    }
    
    public List<Object[]> findByRoleCode(String roleCode) throws DataAccessException{
    	return userEntityDAO.findByRoleCode(roleCode);
    }
    
    public List<Object[]> findForUserCode(String userCode) throws DataAccessException{
    	return userEntityDAO.findForUserCode(userCode);
    }
    
    public void saveOrUpdate(UserEntity userEntity) throws DataAccessException{
    	userEntityDAO.saveOrUpdate(userEntity);
    }
    
    public void delete(UserEntity userEntity) throws DataAccessException{
    	userEntityDAO.delete(userEntity);
    }
  	
  	public EntityItemDTO getEntityItemDTO() throws DataAccessException {
    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	return getEntityItemDTO((UserDTO)session.getAttribute("user"));
  	}
  	public EntityItemDTO getEntityItemDTO(UserDTO user) throws DataAccessException {
  		return userEntityDAO.getEntityItemDTO(user);
  	}
  	
  	public UserEntity find(Long id) throws DataAccessException{
  		return userEntityDAO.find(id);
  	}

	@Override
	public List<UserEntity> findByRole(String role) {
		return userEntityDAO.findByRole(role);
	}
}
