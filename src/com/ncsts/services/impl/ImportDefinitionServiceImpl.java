package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.services.ImportDefinitionService;

public class ImportDefinitionServiceImpl implements ImportDefinitionService{
private ImportDefinitionDAO importDefinitionDAO;
	
	public ImportDefinitionDAO getImportDefinitionDAO() {
		return importDefinitionDAO;
	}

	public void setImportDefinitionDAO(ImportDefinitionDAO importDefinitionDAO) {
		this.importDefinitionDAO = importDefinitionDAO;
	}

	public List<ImportDefDTO> getAllImportDefinition(){
		
		List<ImportDefDTO> listImportDefDTO=new ArrayList<ImportDefDTO>();
		List<ImportDefinition> listImportDefList=importDefinitionDAO.getAllImportDefinition();
		if(listImportDefList!=null){
			for(ImportDefinition importDef1 : listImportDefList)
				listImportDefDTO.add(importDef1.getImportDefDTO());
		}
		
		return listImportDefDTO;
		
	}
	
	public ImportDefDTO  findByPK(String importDefCode) throws DataAccessException {
		ImportDefinition importDefinition = importDefinitionDAO.findByPK(importDefCode);
		return importDefinition.getImportDefDTO();
	}
	
	public List<ImportDefinitionDetail> getAllHeaders(String code) {
		return importDefinitionDAO.getAllHeaders(code);
	}
	public List<String> getImportDefinitionCodes(String tablename) {
		return importDefinitionDAO.getImportDefinitionCodes(tablename);
	}
	public List<ImportDefinitionDetail> getResetImpDefColHeadings(String code, String tablename) {
		return importDefinitionDAO.getResetImpDefColHeadings(code,tablename);
		
	}
	
	public void addImportDefinitionDetail(ImportDefinitionDetail importDefinitionDetail) {
		importDefinitionDAO.addImportDefinitionDetail(importDefinitionDetail);
	}
	

}