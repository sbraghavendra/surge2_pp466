package com.ncsts.services.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DwFilterDAO;
import com.ncsts.domain.DwFilter;
import com.ncsts.services.DwFilterService;

public class DwFilterServiceImpl implements DwFilterService {

	private Logger logger = LoggerFactory.getInstance().getLogger(DwFilterServiceImpl.class);
	
	private DwFilterDAO dwFilterDAO;

    public DwFilterDAO getDwFilterDAO() {
		return dwFilterDAO;
	}

	public void setDwFilterDAO(DwFilterDAO dwFilterDAO) {
		this.dwFilterDAO = dwFilterDAO;
	}
	
	public List<DwFilter> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException {
		return dwFilterDAO.getFilterNamesByUserCode(windowName, userCode);
	}
	
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException {
		return dwFilterDAO.getFilterNamesPerUserCode(windowName, userCode);
	}
	
	public List<DwFilter> getColumnsByFilterNameandUser(String windowName, String userCode, String filterName) throws DataAccessException{
		return dwFilterDAO.getColumnsByFilterNameandUser(windowName, userCode, filterName);  
	}
	
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		dwFilterDAO.deleteColumnsByFilterNameandUserCode(windowName, filterName, userCode);
	}
	public void updateColumnsByFilterNameandUser(String windowName, String filterName, String userCode, List<DwFilter> dwFilters) throws DataAccessException {
		dwFilterDAO.updateColumnsByFilterNameandUser(windowName, filterName, userCode, dwFilters);
	}
	
	public void addColumnsByFilterName(String windowName, String filterName, List<DwFilter> dwFilters) throws DataAccessException {
		dwFilterDAO.addColumnsByFilterName(windowName, filterName, dwFilters);
	}
	
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException {
		return dwFilterDAO.isFilterNameExist(windowName, filterName);
	}
	
	public String getUserByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException {
		return dwFilterDAO.getUserByFilterNameandUserCode(windowName, filterName, userCode);
	}
}

