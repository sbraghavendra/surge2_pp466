package com.ncsts.services.impl;

/**
 * @author AChen
 * 
 */

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DatabaseSetupDAO;
import com.ncsts.services.DatabaseSetupService;

public class DatabaseSetupServiceImpl implements DatabaseSetupService {

	private Logger logger = LoggerFactory.getInstance().getLogger(DatabaseSetupServiceImpl.class);
	
	private DatabaseSetupDAO databaseSetupDAO;

    public DatabaseSetupDAO getDatabaseSetupDAO() {
		return databaseSetupDAO;
	}

	public void setDatabaseSetupDAO(DatabaseSetupDAO databaseSetupDAO) {
		this.databaseSetupDAO = databaseSetupDAO;
	}
	
	public boolean isEnterImportDefinitionSpecDone() throws DataAccessException {
		return databaseSetupDAO.isEnterImportDefinitionSpecDone();
	}
	
	public boolean isEnterDriversDone() throws DataAccessException {
		return databaseSetupDAO.isEnterDriversDone();
	}
	
	public boolean isEnterEntityLevelsDone() throws DataAccessException {
		return databaseSetupDAO.isEnterEntityLevelsDone();
	}
	
	public boolean isEnterEntitiesDone() throws DataAccessException {
		return databaseSetupDAO.isEnterEntitiesDone();
	}	
}