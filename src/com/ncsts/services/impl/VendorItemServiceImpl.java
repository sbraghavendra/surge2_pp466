package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.VendorItemDAO;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.VendorItem;
import com.ncsts.services.VendorItemService;

public class VendorItemServiceImpl extends GenericServiceImpl<VendorItemDAO, VendorItem, Long> implements VendorItemService {
    public VendorItem findById(Long vendorId) throws DataAccessException {
        return getDAO().findById(vendorId);
    }	
    
    public Long count(Long entityId) throws DataAccessException{
    	 return getDAO().count(entityId);
    }
    
    public VendorItem findByCode(String vendorCode) throws DataAccessException {
        return getDAO().findByCode(vendorCode);
    }
    
    public VendorItem findByName(String vendorName) throws DataAccessException {
        return getDAO().findByName(vendorName);
    }

    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate(VendorItem vendorItem) throws DataAccessException{
    	getDAO().saveOrUpdate(vendorItem);
    }

    public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException {
    	return getDAO().isAllowToDeleteEntity(entityId);
    }
    
    public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException {
    	getDAO().copyEntityComponents(fromEntityId, toEntityId, selectedComponents);
    }
    public void deleteVendor(Long vendorId) throws DataAccessException {
    	getDAO().deleteVendor(vendorId);
    }
    
    public void disableVendor(Long vendorId) throws DataAccessException {
    	getDAO().disableVendor(vendorId);
    }

    public boolean isVendorUsed(String vendorCode, String vendorName) throws DataAccessException {
    	return getDAO().isVendorUsed(vendorCode, vendorName);
    }

    public List<VendorItem> getAllRecords(VendorItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause) throws DataAccessException{
    	return getDAO().getAllRecords(exampleInstance, orderBy, firstRow, maxResults, whereClause);
    }
    
	public Long count(VendorItem exampleInstance, String whereClause){
		return getDAO().count(exampleInstance, whereClause);
	}
	
	public boolean isNexusDefinitionsUsed(Long vendorID) throws DataAccessException {
		return getDAO().isNexusDefinitionsUsed(vendorID);
	}
}


