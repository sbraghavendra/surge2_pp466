package com.ncsts.services.impl;

import java.util.Date;
import java.util.List;

import com.ncsts.dao.MatrixDAO;
import com.ncsts.domain.Matrix;
import com.ncsts.domain.MatrixDriver;
import com.ncsts.services.MatrixService;

public class MatrixServiceImpl<DAO extends MatrixDAO<M>, M extends Matrix> extends GenericServiceImpl<DAO, M, Long> implements MatrixService<M> {

	public boolean matrixExists(M matrix, boolean checkDate) {
		return getDAO().matrixExists(matrix, checkDate);
	}
	
	public List<MatrixDriver> getUniqueDrivers(M filter) {
		return getDAO().getUniqueDrivers(filter);
	}
	
	public Date minEffectiveDate(M matrix) {
		return getDAO().minEffectiveDate(matrix);
	}
	
	public Long matrixEffectiveDateCount(M matrix) {
		return getDAO().matrixEffectiveDateCount(matrix);
	}
	
}

