package com.ncsts.services.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.GenericDAO;
import com.ncsts.domain.Idable;
import com.ncsts.services.GenericService;

public class GenericServiceImpl<DAO extends GenericDAO<T,ID>, T extends Idable<ID>, ID extends Serializable> implements GenericService<T,ID> {

	private DAO dao;
	
	public DAO getDAO() {
		return dao;
	}

	public void setDAO(DAO dao) {
		this.dao = dao;
	}
	
	public Long count() {
		return dao.count();
	}

	public Long count(T exampleInstance, String... excludeProperty) {
		return dao.count(exampleInstance, excludeProperty);
	}

	public T findById(ID id) throws DataAccessException {
		return dao.findById(id);
	}
	
	public List<T> findById(Collection<ID> ids) {
		return dao.findById(ids);
	}
	
	public List<T> findAll() {
		return dao.findAll();
	}
	
	public List<T> findAllSorted(String sortField, boolean ascending) {
		return dao.findAllSorted(sortField, ascending);
	}
	
	public List<T> findByExample(T exampleInstance, int count, String... excludeProperty) {
		return dao.findByExample(exampleInstance, count, excludeProperty);
	}
	
	public List<T> findByExampleSorted(T exampleInstance, int count,
			String sortField, boolean ascending, String... excludeProperty) {
		return dao.findByExampleSorted(exampleInstance, count, sortField, ascending, excludeProperty);
	}
	
	public void save(T instance) {
		dao.save(instance);
	}
	
	public void update(T instance) throws DataAccessException {
		dao.update(instance);
	}
	
	public void remove(T instance)throws DataAccessException {
		dao.remove(instance);
	}
}
