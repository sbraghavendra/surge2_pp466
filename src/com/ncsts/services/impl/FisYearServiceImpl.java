package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.FisYearDAO;
import com.ncsts.domain.FisYear;
import com.ncsts.services.FisYearService;

public class FisYearServiceImpl extends GenericServiceImpl<FisYearDAO, FisYear, String>  implements FisYearService{


	public List<FisYear> getSelectedPeriod(String ID) throws DataAccessException {
	
		return getDAO().getSelectedPeriod(ID);
	}

	public List<FisYear> getAllPeriodDesc(Long fisYear)
			throws DataAccessException {
	
		return getDAO().getAllPeriodDesc(fisYear);
	}

	
	public List<FisYear> getAllYearPeriod(Long fisYearId)
			throws DataAccessException {
		
		return getDAO().getAllYearPeriod(fisYearId);
	}


	public List<FisYear> getAllPeriod() throws DataAccessException {
		
		return getDAO().getAllPeriod();
	}

	public boolean getCloseFlag(Long fisYearId) {
		
		return getDAO().getCloseFlag(fisYearId);
	}
	
	public void updateFisYear(Long fisYear, List<FisYear> fisYearList) throws DataAccessException  {	
		getDAO().updateFisYear(fisYear, fisYearList);
	}
}
