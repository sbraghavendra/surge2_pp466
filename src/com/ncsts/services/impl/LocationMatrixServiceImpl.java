/**
 * 
 */
package com.ncsts.services.impl;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.LocationMatrixDAO;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.services.LocationMatrixService;

/**
 * @author Anand
 *
 */
public class LocationMatrixServiceImpl extends MatrixServiceImpl<LocationMatrixDAO, LocationMatrix> implements LocationMatrixService {
	public List<LocationMatrix> getDetailRecords(LocationMatrix locationMatrix, OrderBy orderBy){
		return getDAO().getDetailRecords(locationMatrix, orderBy);
	}
	
	public List<LocationMatrix> getMatchingLocationMatrixLines(PurchaseTransaction exampleInstance){
		return getDAO().getMatchingLocationMatrixLines(exampleInstance);
	}
}
