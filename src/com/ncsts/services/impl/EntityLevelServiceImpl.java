package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.EntityLevelDAO;
import com.ncsts.services.EntityLevelService;
import com.ncsts.domain.EntityLevel;
import com.ncsts.dto.EntityLevelDTO;

/**
 * @author Paul Govindan
 *
 */

public class EntityLevelServiceImpl implements EntityLevelService {
	
	private EntityLevelDAO entityLevelDAO;	
	
	public void setEntityLevelDAO (EntityLevelDAO entityLevelDAO) {
		this.entityLevelDAO = entityLevelDAO;
	}
	
	public EntityLevelDAO getEntityLevelDAO() {
		return this.entityLevelDAO;
	}
	
    public EntityLevel findById(Long entityId) throws DataAccessException {
        return entityLevelDAO.findById(entityId);
    }	
	
    public List<EntityLevel> findAllEntityLevels() throws DataAccessException {
    	return entityLevelDAO.findAllEntityLevels();
    }

	public List<EntityLevel> findByTransDtlColName(String columnName) throws DataAccessException {
		// TODO Auto-generated method stub
		return entityLevelDAO.findByTransDtlColName(columnName);
	}

	public void save (EntityLevelDTO entityLevelDTO)throws DataAccessException{
		entityLevelDAO.save(entityLevelDTO);
	}

	public void update (EntityLevelDTO entityLevelDTO) throws DataAccessException{
		entityLevelDAO.update(entityLevelDTO);
	}

	public void remove (Long entityLevelId) throws DataAccessException{
		entityLevelDAO.remove(entityLevelId);
	}

	public String findByBU() throws DataAccessException{
    	return entityLevelDAO.findByBU();
	}
}