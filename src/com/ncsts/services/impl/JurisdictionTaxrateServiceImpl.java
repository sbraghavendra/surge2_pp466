package com.ncsts.services.impl;



import java.util.Date;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.JurisdictionTaxrateDAO;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.services.JurisdictionTaxrateService;

/**
 * @author Jay Jungalwala
 * 
 */

public class JurisdictionTaxrateServiceImpl extends GenericServiceImpl<JurisdictionTaxrateDAO, JurisdictionTaxrate, Long> implements JurisdictionTaxrateService {

	public Date findByDate(Long jurisdictionId) throws DataAccessException {
		return getDAO().findByDate(jurisdictionId);
	}

	@Override
	public JurisdictionTaxrate findByIdInitialized(Long id)
			throws DataAccessException {
		return getDAO().findByIdInitialized(id);
	}
	
	public long getTaxrateUsedCount(Long taxrateId) throws DataAccessException {
		return getDAO().getTaxrateUsedCount(taxrateId);
	}
}
