package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.SitusDriverDAO;
import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverDetail;
import com.ncsts.services.SitusDriverService;

public class SitusDriverServiceImpl extends GenericServiceImpl<SitusDriverDAO, SitusDriver, Long> implements SitusDriverService {

	public Long count(SitusDriver exampleInstance){
		return getDAO().count(exampleInstance);
	}
	
	public List<SitusDriverDetail> findAllSitusDriverDetailBySitusDriverId(Long situsDriverId) throws DataAccessException{
   	 	return getDAO().findAllSitusDriverDetailBySitusDriverId(situsDriverId);
	}
	
	public void addSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException{
   	 	getDAO().addSitusDriver(situsDriver, situsDriverDetailList);
	}
	
	public void updateSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException{
   	 	getDAO().updateSitusDriver(situsDriver, situsDriverDetailList);
	}
	
	public void deleteSitusDriver(SitusDriver situsDriver) throws DataAccessException{
   	 	getDAO().deleteSitusDriver(situsDriver);
	}
	
	public SitusDriver find(SitusDriver exampleInstance){
		return getDAO().find(exampleInstance);
	}
	
	public List<String> getDistinctSitusDriverColumn(String distinctColumn, String whereClause) throws DataAccessException {
		return getDAO().getDistinctSitusDriverColumn(distinctColumn, whereClause);
	}
}
