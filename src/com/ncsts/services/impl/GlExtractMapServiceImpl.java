package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.GlExtractMapDAO;
import com.ncsts.domain.GlExtractMap;
import com.ncsts.dto.GlExtractMapDTO;
import com.ncsts.services.GlExtractMapService;

public class GlExtractMapServiceImpl extends MatrixServiceImpl<GlExtractMapDAO, GlExtractMap> implements GlExtractMapService {
	private Logger logger = LoggerFactory.getInstance().getLogger(GlExtractMapServiceImpl.class);
	
	public GlExtractMapDTO findByGlExtractMapId(Long glExtractMapId) {
		// TODO Auto-generated method stub
		GlExtractMap gl =  getDAO().findByGlExtractMapId(glExtractMapId);
		return gl.getGlExtractMapDTO();
	}

	public List<GlExtractMapDTO> getGlExtractMapData(GlExtractMapDTO glExtractMapDTO) {
		GlExtractMap gl = new GlExtractMap();
		BeanUtils.copyProperties(glExtractMapDTO, gl);
		List<GlExtractMapDTO> listDTO = new ArrayList<GlExtractMapDTO>();
		List<GlExtractMap> list = getDAO().findByGLExtractMap(gl);
		for(GlExtractMap gle : list){
			listDTO.add(gle.getGlExtractMapDTO());
		}
		return listDTO;
	}

	public void remove(Long glExtractMapId) {
		getDAO().remove(glExtractMapId);

	}

	public GlExtractMapDTO save(GlExtractMapDTO glExtractMapDTO) {
		logger.info("Inside Service Impl");
		GlExtractMap gl = new GlExtractMap();
		BeanUtils.copyProperties(glExtractMapDTO, gl);
		return getDAO().saveGl(gl).getGlExtractMapDTO();
	}

	public void update(GlExtractMapDTO glExtractMapDTO) {
		GlExtractMap gl = new GlExtractMap();
		BeanUtils.copyProperties(glExtractMapDTO, gl);
		getDAO().update(gl);
	}

	public List<GlExtractMap> findByStateCode(String state) {
		// TODO Auto-generated method stub
		return getDAO().findByStateCode(state);
	}
}
