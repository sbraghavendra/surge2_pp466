package com.ncsts.services.impl;

import java.util.List;
import com.ncsts.dao.AllocationMatrixDAO;
import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.services.AllocationMatrixService;

/**
 * @author Paul Govindan
 *
 */
public class AllocationMatrixServiceImpl extends MatrixServiceImpl<AllocationMatrixDAO, AllocationMatrix> implements AllocationMatrixService {
	
	public AllocationMatrix findByIdWithDetails(Long id) {
		return getDAO().findByIdWithDetails(id);
	}
	
	public List<AllocationMatrix> getDetailRecords(AllocationMatrix allocationMatrix, OrderBy orderBy){
		return getDAO().getDetailRecords(allocationMatrix, orderBy);
	}
	
}
