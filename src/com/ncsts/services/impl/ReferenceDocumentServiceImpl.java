package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.dao.ReferenceDocumentDAO;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.services.ReferenceDocumentService;

public class ReferenceDocumentServiceImpl extends GenericServiceImpl<ReferenceDocumentDAO, ReferenceDocument, String> implements ReferenceDocumentService {
	public List<ReferenceDocument> searchForRefDoc(ReferenceDocument refDoc) {
		return getDAO().searchForRefDoc(refDoc);
	}
	
	public List<ReferenceDocument> findAll() {
		return getDAO().findAll();
	}
	
	public void update(ReferenceDocument referenceDocument) {
		getDAO().update(referenceDocument);
	}	
}
