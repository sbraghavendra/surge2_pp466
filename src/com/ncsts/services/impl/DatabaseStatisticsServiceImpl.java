package com.ncsts.services.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.DatabaseTransactionDAO;
import com.ncsts.domain.TempStatistics;
import com.ncsts.dto.DatabaseTransactionStatisticsDTO;
import com.ncsts.dto.LocationMatrixTransactionStatiticsDTO;
import com.ncsts.dto.TaxMatrixTransactionStatiticsDTO;
import com.ncsts.services.DatabaseStatisticsService;

@RunWith(TestClassRunner.class)
public class DatabaseStatisticsServiceImpl implements DatabaseStatisticsService{
	
	private DatabaseTransactionDAO databaseTransactionDAO;
	
	public DatabaseTransactionDAO getDatabaseTransactionDAO() {
		return databaseTransactionDAO;
	}

	public void setDatabaseTransactionDAO(
			DatabaseTransactionDAO databaseTransactionDAO) {
		this.databaseTransactionDAO = databaseTransactionDAO;
	}

	/**
	 * Will return all the records for database statistics Transactions Tab
	 * @return
	 */
	public List<DatabaseTransactionStatisticsDTO> getTransactionsStatistics(String whereClauseforSearch)throws SQLException{
		//TempStatistics tempStatistics=getDatabaseStatisticsObject(databaseTransactionStatisticsDTO);
		List<DatabaseTransactionStatisticsDTO> listDBStatisticsDTO=new ArrayList<DatabaseTransactionStatisticsDTO>();
		List<TempStatistics> listMatrixList=databaseTransactionDAO.getTransactionsStatistics(whereClauseforSearch);
		if(listMatrixList!=null){
			for(TempStatistics tmp:listMatrixList)
				listDBStatisticsDTO.add(tmp.getDatabaseTransactionStatisticsDTO());
	}
		return listDBStatisticsDTO;
		
	}
	
	public StringBuffer saveStatisticsOnGroupBy(String groupBy, String whereClause){
		StringBuffer sql = new StringBuffer();
	    sql.append("SELECT ");
	    sql.append( groupBy + ", ");
	    sql.append("COUNT (*) AS COMPUTE_COUNT, SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, ");
	    sql.append("SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, ");
	    sql.append("CASE SUM(GL_LINE_ITM_DIST_AMT) WHEN 0 THEN 0");
	    sql.append(" ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) END AS  COMPUTE_EFFTAXRATE, ");
	    sql.append("MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT, AVG (GL_LINE_ITM_DIST_AMT)  AS COMPUTE_AVGDISTAMT ");	    
	    sql.append(" FROM TB_PURCHTRANS_LOCN LEFT JOIN TB_JURISDICTION ON (TB_PURCHTRANS_LOCN.SHIPTO_JURISDICTION_ID = TB_JURISDICTION.jurisdiction_id) ");
	    sql.append(" LEFT JOIN TB_PURCHTRANS on (TB_PURCHTRANS_LOCN.PURCHTRANS_LOCN_ID = TB_PURCHTRANS.PURCHTRANS_ID)");
	    if ((whereClause != null) && !whereClause.equals("")) {
	    	sql.append(whereClause + " ");
	    }
	    sql.append("GROUP BY ");
	    sql.append(groupBy);
	    String header = groupBy.replace(",", " ") + " Count Dist.Amt TaxAmt. Eff.TaxRate Max.Dist.Amt Avg.Dist.Amt";
	    StringBuffer fos = new StringBuffer();
	    String tokens[] = null;
    	tokens = header.split(" ");
    	for (int i = 0; i < tokens.length; i++) {
    		fos.append(tokens[i]);
    		fos.append("\t");
    	}
		fos.append("\r");
		fos.append("\n");
	    List<Object[]> olist = databaseTransactionDAO.getListOfObjectFromNativeQuery(sql.toString());
		for(Object obj : olist){
			Object[] objarray= (Object[])obj;
			for(int i=0; i < objarray.length ; i++){
					if (objarray[i] instanceof String) {
						fos.append((String)objarray[i]);
					}else if(objarray[i] instanceof Date){
						Date newdate = (Date)objarray[i]; 
						fos.append(newdate.toString());
					}else if(objarray[i] instanceof BigDecimal){
						BigDecimal number = (BigDecimal)objarray[i]; 
						fos.append(number.toString());
					}else if(objarray[i] instanceof Character){
						Character number = (Character)objarray[i]; 
						fos.append(number.toString());
					}
					fos.append("\t");
			}
			fos.append("\r");
			fos.append("\n");
		}
		return fos;
	}
	
	public List<DatabaseTransactionStatisticsDTO> getStatisticsOnGroupBy(String groupBy, String whereClause){
		StringBuffer sql = new StringBuffer();
	    sql.append("SELECT ");
	    sql.append("COUNT (*) AS COMPUTE_COUNT, SUM (GL_LINE_ITM_DIST_AMT) AS COMPUTE_DISTAMT, ");
	    sql.append("SUM (TB_CALC_TAX_AMT) AS COMPUTE_TAXAMT, ");
	    sql.append("CASE SUM(GL_LINE_ITM_DIST_AMT) WHEN 0 THEN 0 ");
	    sql.append(" ELSE (SUM (TB_CALC_TAX_AMT) / SUM (GL_LINE_ITM_DIST_AMT)) END AS  COMPUTE_EFFTAXRATE, ");
	    sql.append("MAX (GL_LINE_ITM_DIST_AMT) AS COMPUTE_MAXDISTAMT, AVG (GL_LINE_ITM_DIST_AMT) AS  COMPUTE_AVGDISTAMT ");
	    sql.append( ", " + "TB_PURCHTRANS." + groupBy );
	    sql.append(" FROM TB_PURCHTRANS_LOCN LEFT JOIN TB_JURISDICTION ON (TB_PURCHTRANS_LOCN.SHIPTO_JURISDICTION_ID = TB_JURISDICTION.jurisdiction_id) ");
	    sql.append(" LEFT JOIN TB_PURCHTRANS on (TB_PURCHTRANS_LOCN.PURCHTRANS_LOCN_ID = TB_PURCHTRANS.PURCHTRANS_ID)");
	    if ((whereClause != null) && !whereClause.equals("")) {
	    	sql.append(whereClause + " ");
	    }
	    sql.append("GROUP BY ");
	    sql.append(groupBy);
	    String[] dynamicCols = groupBy.split(",");
	    
	    List<DatabaseTransactionStatisticsDTO> drillDownList = new ArrayList<DatabaseTransactionStatisticsDTO>();
	    List<Object[]> list = databaseTransactionDAO.getListOfObjectFromNativeQuery(sql.toString());
	    for(Object[] objArray : list){
	    	DatabaseTransactionStatisticsDTO drilldown = new DatabaseTransactionStatisticsDTO();
	    	//N.B: Oracle returns BigDecimal's, but Postgres will return a BigInteger... What fun...
	    	Number distCount = (Number)objArray[0];
	    	if (distCount != null) drilldown.setDistCount(distCount.longValue());
	    	BigDecimal distAmt = (BigDecimal) objArray[1];
	    	if(distAmt != null)drilldown.setDistAmt(distAmt.doubleValue());
	    	BigDecimal taxAmt = (BigDecimal) objArray[2];
	    	if(taxAmt != null)drilldown.setTaxAmt(taxAmt.doubleValue());
	    	BigDecimal effTaxRate = (BigDecimal) objArray[3];
	    	if(effTaxRate != null)drilldown.setEffTaxRate(effTaxRate.doubleValue());
	    	BigDecimal maxDistAmt = (BigDecimal) objArray[4];
	    	if(maxDistAmt != null)drilldown.setMaxDistAmt(maxDistAmt.doubleValue());
	    	BigDecimal avgDistAmt = (BigDecimal) objArray[5];
	    	if(avgDistAmt != null)drilldown.setAvgDistAmt(avgDistAmt.doubleValue());
	    	String[] dynamicText = new String[dynamicCols.length];
	    	int j = 0;
	    	for(int i=6; i < 6 + dynamicCols.length ; i++){
					if (objArray[i] instanceof String) {
						dynamicText[j] = (String)objArray[i];
					}else if(objArray[i] instanceof Date){
						Date newdate = (Date)objArray[i]; 
						dynamicText[j] = newdate.toString();
					}else if(objArray[i] instanceof BigDecimal){
						BigDecimal number = (BigDecimal)objArray[i]; 
						dynamicText[j] = number.toString();
					}else if(objArray[i] instanceof Character){
						Character number = (Character)objArray[i]; 
						dynamicText[j] = number.toString();
					}
					j++;
			}
	    	drilldown.setDynamicCols(Arrays.asList(dynamicText));
	    	drillDownList.add(drilldown);
	    }
	   return drillDownList; 
	}
		
	public List<DatabaseTransactionStatisticsDTO> getTransactionsStatistics(){
		List<DatabaseTransactionStatisticsDTO> listDBStatisticsDTO=new ArrayList<DatabaseTransactionStatisticsDTO>();
		List<TempStatistics> listMatrixList=databaseTransactionDAO.getTransactionsStatistics();
		if(listMatrixList!=null){
			for(TempStatistics tmp:listMatrixList)
				listDBStatisticsDTO.add(tmp.getDatabaseTransactionStatisticsDTO());
	}
		return listDBStatisticsDTO;
	}
	
	
	
	 public StringBuffer saveAs(List<TempStatistics> listMatrixList) throws IOException{
	    	String header = "title	count	count_pct	sum	sum_pct	tax_amt	effTaxRate	max	avg	where_clause	group_by_on_drilldown";
			StringBuffer fos = new StringBuffer();
			fos.append(header);
			fos.append("\n");
			
			for(TempStatistics trdetail : listMatrixList){
				fos.append(trdetail.toString());
				fos.append("\n");
			}
			return fos;
	    }
		
	
	/**
	 * Will return all the records for database statistics TaxMatrix tab
	 * @return
	 */
	public TaxMatrixTransactionStatiticsDTO getTaxMatrixStatistics(String andClauseforSearch){
		TaxMatrixTransactionStatiticsDTO taxMatrixStatitics=new TaxMatrixTransactionStatiticsDTO();
		
		//Midtier project code modified - january 2009
		taxMatrixStatitics.setActive_matrix_lines(databaseTransactionDAO.activeCount("SP_ACTIVE_MATRIX_LINES",andClauseforSearch).toString());
		
		taxMatrixStatitics.setTotal_matrix_lines(databaseTransactionDAO.count("SP_TAX_MTRX_TOTAL_MTRX_LINES", andClauseforSearch).toString());
		
		taxMatrixStatitics.setDistinct_matrix_lines(databaseTransactionDAO.count("sp_tax_mtrx_dstnct_mtrx_lines", andClauseforSearch).toString());
		
		taxMatrixStatitics.setTempTaxMatrixCount(databaseTransactionDAO.getTaxMatrixStatistics(andClauseforSearch));
		
		return taxMatrixStatitics;
		
		
	}
	
	/**
	 * Will return all the records for database statistics LocationMatrix tab
	 * @return
	 */
	public LocationMatrixTransactionStatiticsDTO getLocationMatrixStatistics(String andClauseforSearch){
		LocationMatrixTransactionStatiticsDTO locationDTO=new LocationMatrixTransactionStatiticsDTO();
		
		//Midtier project code modified - january 2009
		locationDTO.setActive_location_matrix_lines(databaseTransactionDAO.activeCount("SP_ACTIVE_LOCATION_MTRX_LINES",andClauseforSearch).toString());
		
		locationDTO.setTotal_location_matrix_lines(databaseTransactionDAO.count("sp_loc_mtrx_tot_mtrx_lines ", andClauseforSearch).toString());
		
		locationDTO.setDistinct_location_matrix_lines(databaseTransactionDAO.count("sp_distinct_loc_mtrx_lines ", andClauseforSearch).toString());
		
		locationDTO.setTempLocationMatrixCount(databaseTransactionDAO.getLocationMatrixStatistics(andClauseforSearch));
		
		return locationDTO;
	}
}
