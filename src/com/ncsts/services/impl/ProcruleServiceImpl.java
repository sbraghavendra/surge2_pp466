package com.ncsts.services.impl;


import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.ProcruleDAO;
import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.ProruleAudit;
import com.ncsts.services.ProcruleService;

public class ProcruleServiceImpl extends GenericServiceImpl<ProcruleDAO, Procrule, Long> implements ProcruleService {
	
	public void addProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException{
		getDAO().addProcruleDetailList(updateProcrule, updateProcruleEntityList);
	}
	
	public void updateProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException{
		getDAO().updateProcruleDetailList(updateProcrule, updateProcruleEntityList);
	}
	
	public void deleteProcrule(Procrule updateProcrule) throws DataAccessException{
		getDAO().deleteProcrule(updateProcrule);
	}
	
	public void deleteProcruleDetail(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList)  throws DataAccessException{
		getDAO().deleteProcruleDetail(updateProcrule, updateProcruleEntityList);
	}
	
	public List<ProcruleDetail> findProcruleDetail(Long procruleId, Date effectiveDate) throws DataAccessException{
		return getDAO().findProcruleDetail(procruleId, effectiveDate);
	}
	
	public List<String> findProcruleDetailDistinctDate(Long procruleId) throws DataAccessException{
		return getDAO().findProcruleDetailDistinctDate(procruleId);
	}
	
	public List<Procrule> getAllProcrule() throws DataAccessException{
		return getDAO().getAllProcrule();
	}
	public List<ProruleAudit> getAllProcAuditRecords(String sessionId)throws DataAccessException{
		return getDAO().getAllProcAuditRecords(sessionId);
	}
	
	public List<Procrule> getProcrule(String moduleCode, String ruletypeCode) throws DataAccessException{
		return getDAO().getProcrule(moduleCode, ruletypeCode);
	}
	
	public void reorderProcruleList(List<Procrule> updateProcruleList) throws DataAccessException{
		getDAO().reorderProcruleList(updateProcruleList);
	}
	
	public boolean getIsRuleNameExist(String procruleName, String moduleCode) throws DataAccessException {
		return getDAO().getIsRuleNameExist(procruleName, moduleCode);
	}
	public List<ProruleAudit> getProcRuleRecords(String sessionId) throws DataAccessException{
		return getDAO().getProcRuleRecords(sessionId);
	}
	public boolean getIsRuleNameDateExist(String procruleName, String moduleCode, Date effectiveDate) throws DataAccessException {
		return getDAO().getIsRuleNameDateExist(procruleName, moduleCode, effectiveDate);
	}
	public boolean isAllowDeleteRules(Long procruleId) throws DataAccessException {
		return getDAO().isAllowDeleteRules(procruleId);
	}
	
	public boolean isAllowDeleteDefinitions(Long procruleId, Date effectiveDate) throws DataAccessException {
		return getDAO().isAllowDeleteDefinitions(procruleId, effectiveDate);
	}
}
