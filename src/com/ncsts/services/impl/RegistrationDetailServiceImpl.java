package com.ncsts.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.RegistrationDetailDAO;
import com.ncsts.domain.RegistrationDetail;
import com.ncsts.services.RegistrationDetailService;

public class RegistrationDetailServiceImpl extends GenericServiceImpl<RegistrationDetailDAO, RegistrationDetail, Long> implements RegistrationDetailService {

	@Override
	public List<RegistrationDetail> findAllRegistrationDetails(Long entityId, 
			String countryCode, String stateCode, String county, String city,
			String stj, boolean activeFlag) throws DataAccessException {
		return getDAO().findAllRegistrationDetails(entityId, countryCode, stateCode, county, city, stj, activeFlag);
	}
}
