package com.ncsts.services.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.dao.EntityLocnSetDtlDAO;
import com.ncsts.services.EntityLocnSetDtlService;
import com.ncsts.domain.EntityLocnSetDtl;


public class EntityLocnSetDtlServiceImpl extends GenericServiceImpl<EntityLocnSetDtlDAO, EntityLocnSetDtl, Long> implements EntityLocnSetDtlService {
	
    public Long count(Long entityLocnSetId) throws DataAccessException{
    	 return getDAO().count(entityLocnSetId);
    }
    
    public List<EntityLocnSetDtl> findAllEntityLocnSetDtls() throws DataAccessException {
    	return getDAO().findAllEntityLocnSetDtls();
    }
    
    public List<EntityLocnSetDtl> findAllEntityLocnSetDtlsByEntityLocnSetId(Long entityLocnSetId) throws DataAccessException{
    	return getDAO().findAllEntityLocnSetDtlsByEntityLocnSetId(entityLocnSetId);
    }
    
    public Long persist(EntityLocnSetDtl entityLocnSetDtl)throws DataAccessException {
    	return getDAO().persist(entityLocnSetDtl);
    }
    
    public void remove(Long id) throws DataAccessException{
    	getDAO().remove(id);
    }
    
    public void saveOrUpdate(EntityLocnSetDtl entityLocnSetDtl) throws DataAccessException{
    	getDAO().saveOrUpdate(entityLocnSetDtl);
    }
    
    public void deleteEntityLocnSet(Long entityLocnSetId) throws DataAccessException {
    	getDAO().deleteEntityLocnSet(entityLocnSetId);
    }

}


