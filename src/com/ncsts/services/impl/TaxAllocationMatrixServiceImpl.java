package com.ncsts.services.impl;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TaxAllocationMatrixDAO;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.services.TaxAllocationMatrixService;

public class TaxAllocationMatrixServiceImpl extends MatrixServiceImpl<TaxAllocationMatrixDAO, TaxAllocationMatrix> implements TaxAllocationMatrixService {
	
	public List<TaxAllocationMatrix> getDetailRecords(TaxAllocationMatrix taxAllocationMatrix, OrderBy orderBy){
		return getDAO().getDetailRecords(taxAllocationMatrix, orderBy);
	}
	
	public void save(TaxAllocationMatrix taxAllocationMatrix) {
		getDAO().save(taxAllocationMatrix);
	}

	@Override
	public TaxAllocationMatrix findByIdWithDetails(Long id) {
		return getDAO().findByIdWithDetails(id);
	}
	
	public boolean isTaxAllocationUsedinTransaction(Long taxAllocationMatrixId) {
		return getDAO().isTaxAllocationUsedinTransaction(taxAllocationMatrixId);
	}
}

