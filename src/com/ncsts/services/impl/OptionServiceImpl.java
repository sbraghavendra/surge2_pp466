package com.ncsts.services.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.OptionDAO;
import com.ncsts.services.OptionService;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

/**
 * @author Paul Govindan
 *
 */

public class OptionServiceImpl implements OptionService {
	
	static private Logger logger = LoggerFactory.getInstance().getLogger(OptionServiceImpl.class);

	private OptionDAO optionDAO;	
	
	public void setOptionDAO (OptionDAO optionDAO) {
		this.optionDAO = optionDAO;
	}
	
	public OptionDAO getOptionDAO() {
		return this.optionDAO;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ncsts.services.OptionService#findByPK(com.ncsts.domain.OptionCodePK)
	 * Careful - This method returns a new Option object if the requested object is not found in the DB
	 */
    public Option findByPK(OptionCodePK codePK) throws DataAccessException {
		logger.debug("enter findByPK"); 	
    	Option option = optionDAO.findByPK(codePK);
    	if (option == null) {
    		logger.debug("option is null, create new option");
    		option = new Option(codePK);
    	}
		logger.debug("exit findByPK");     	
        return option;
    }

	@Override
	public String findByPKSafe(OptionCodePK codePK) throws DataAccessException {
		Option option = optionDAO.findByPK(codePK);
		if (option != null) {
			return option.getValue();
		}
		return null;
	}

	public List<Option> findAllOptions() throws DataAccessException {
    	return optionDAO.findAllOptions();
    }
    
	public void remove(OptionCodePK codePK) throws DataAccessException {
		optionDAO.remove(codePK);
	}
    
	public void saveOrUpdate (Option option) throws DataAccessException {
		optionDAO.saveOrUpdate(option);
	}
    
	public String persist (Option option) throws DataAccessException {
		return optionDAO.persist(option);
	}
	
	public List<Option> getSystemPreferences() throws DataAccessException {
		return optionDAO.getSystemPreferences();
	}	
	
	public List<Option> getAdminPreferences() throws DataAccessException {
		return optionDAO.getAdminPreferences();
	}

	public List<Option> getUserPreferences() throws DataAccessException {
		return optionDAO.getUserPreferences();
	}		

	public String getUserOption(String user, String name) throws DataAccessException {
		return findByPK(new OptionCodePK (name,  "USER", user)).getValue();
	}
	
	public boolean isAolEnabled(String user) {
		return "1".equalsIgnoreCase(getUserOption(user, "AOLENABLED"));   
	}
	
}



