package com.ncsts.services.impl;


import com.ncsts.dao.OptionDAO;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.view.bean.loginBean;
import com.ncsts.ws.message.StatusResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DiagnosticService {
    private String pinpointBuildProps;

    private String salesEngineBuildProps;

    @Autowired
    private OptionDAO optionDAO;

    public StatusResponse getCurrentStatus() {
        StatusResponse response =new StatusResponse();
        response.setApplicationVersion("PinPoint " + getPinpointAppVersion());
        response.setSalesModuleVersion(getSalesEnginepVersion());
        response.setDatabaseVersion(getDbVersion());
        response.setUptime(uptime());
        return response;
    }

    public String getPinpointAppVersion() {
        InputStream input = null;
        try {
            Properties buildMap = new Properties();

            String filename = getPinpointBuildProps();
            input = loginBean.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("############ Sorry, unable to find " + filename);
            }
            else{
                buildMap.load(input);
                String appVersion = buildMap.getProperty("appversion");
                String buildNo = buildMap.getProperty("build_number");
                String buildTimeStamp = buildMap.getProperty("build_timestamp");
                return "v"+appVersion +" build "+buildNo+" / "+buildTimeStamp;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (input!=null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String getSalesEnginepVersion() {
        InputStream input = null;
        try {
            Properties buildMap = new Properties();

            String filename = getSalesEngineBuildProps();
            input = loginBean.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("############ Sorry, unable to find " + filename);
            }
            else{
                buildMap.load(input);
                String appVersion = buildMap.getProperty("application.version");

                return appVersion;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (input!=null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public String getDbVersion() {
        //String optionCode, String optionTypeCode, String userCod
        Option option = optionDAO.findByPK(new OptionCodePK("VERSION", "ADMIN", "ADMIN"));
        if (option == null) {
            return null;
        } else {
            return option.getValue();
        }
    }

    public String uptime() {
        return org.apache.commons.lang.time.DurationFormatUtils.formatDurationWords(
                java.lang.management.ManagementFactory.getRuntimeMXBean().getUptime(), true, true);
    }



    public String getPinpointBuildProps() {
        return pinpointBuildProps;
    }

    public void setPinpointBuildProps(String pinpoinBuildProps) {
        this.pinpointBuildProps = pinpoinBuildProps;
    }

    public String getSalesEngineBuildProps() {
        return salesEngineBuildProps;
    }

    public void setSalesEngineBuildProps(String salesEngineBuildProps) {
        this.salesEngineBuildProps = salesEngineBuildProps;
    }
}
