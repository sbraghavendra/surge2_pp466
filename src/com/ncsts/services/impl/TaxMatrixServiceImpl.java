package com.ncsts.services.impl;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TaxMatrixDAO;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.services.TaxMatrixService;

public class TaxMatrixServiceImpl extends MatrixServiceImpl<TaxMatrixDAO, TaxMatrix> implements TaxMatrixService {
	
	public List<TaxMatrix> getDetailRecords(TaxMatrix taxMatrix, OrderBy orderBy,String modulecode){
		return getDAO().getDetailRecords(taxMatrix, orderBy,modulecode);
	}
	
	public List<TaxMatrix> getMatchingTaxMatrixLines(PurchaseTransaction exampleInstance){
		return getDAO().getMatchingTaxMatrixLines(exampleInstance);
	}
	
	public void save(TaxMatrix taxMatrix) {
		getDAO().save(taxMatrix);
	}
}

