package com.ncsts.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DataStatisticsDAO;
import com.ncsts.domain.DataStatistics;
import com.ncsts.dto.DataStatisticsDTO;
import com.ncsts.services.DataStatisticsService;

/**
 * @author Muneer Basha
 *
 */

public class DataStatisticsServiceImpl implements DataStatisticsService {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(DataStatisticsServiceImpl.class);
	
	private DataStatisticsDAO dataStatisticsDAO;

    public DataStatisticsDAO getDataStatisticsDAO() {
		return dataStatisticsDAO;
	}

	public void setDataStatisticsDAO(DataStatisticsDAO dataStatisticsDAO) {
		this.dataStatisticsDAO = dataStatisticsDAO;
	}

	public DataStatistics findById(Long dataStatisticsId) throws DataAccessException {
        return dataStatisticsDAO.findById(dataStatisticsId);
    }	
	
    public List<DataStatisticsDTO> findAllDataStatisticsLines() throws DataAccessException {
    	//return dBStatisticsDAO.findAllDBStatisticsLines();
    	
		logger.debug(" you are inside findAllDataStatisticsLines from Service IMPL ::");
		
		List<DataStatisticsDTO> listDataStatisticsDTO = new ArrayList<DataStatisticsDTO>();		
		List<DataStatistics> listDataStatistics=dataStatisticsDAO.findAllDataStatisticsLines();		
		if(listDataStatistics!=null){
			for(DataStatistics dataStatistics : listDataStatistics)
				listDataStatisticsDTO.add(dataStatistics.getDataStatisticsDTO());
		}
		
		return listDataStatisticsDTO;		
    }
    
	private DataStatistics getDataStatisticsObject(DataStatisticsDTO dataStatisticsDTO) throws DataAccessException{
		
		DataStatistics dataStatistics= new DataStatistics();
		   BeanUtils.copyProperties(dataStatisticsDTO,dataStatistics);
		   return dataStatistics;
	} 
	
	public void update(DataStatisticsDTO dataStatisticsDTO) throws DataAccessException {
		DataStatistics dataStatisticsObject = getDataStatisticsObject(dataStatisticsDTO);		
		dataStatisticsDAO.update(dataStatisticsObject);
		
	}	
	public void remove(Long dataStatisticsId) throws DataAccessException{
		dataStatisticsDAO.remove(dataStatisticsId);
	}
	
	public DataStatisticsDTO addNewStatisticsLine(DataStatisticsDTO dataStatisticsDTO) throws DataAccessException{
		
		DataStatistics dataStatisticsObject = getDataStatisticsObject(dataStatisticsDTO);		
		logger.debug("we are before calling DataStatistics dao addNewStatisticsLine method");
		return (dataStatisticsDAO.addNewStatisticsLine(dataStatisticsObject)).getDataStatisticsDTO();
		
	}
	
	public boolean isValidWhereClause(String whereClause){
    	return dataStatisticsDAO.isValidWhereClause(whereClause);
    }
    
	public boolean isValidGroupByOnDrilldown(String groupByOnDrilldown){
    	return dataStatisticsDAO.isValidGroupByOnDrilldown(groupByOnDrilldown);	
    }
	
}


