package com.ncsts.services.impl;
/**
 * @author Muneer Basha
 */
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.ImportConnectionDAO;
import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.dao.ImportSpecDAO;
import com.ncsts.dao.ListCodesDAO;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.view.bean.loginBean;

public class ImportDefAndSpecsServiceImpl implements ImportDefAndSpecsService{

	private Logger logger = LoggerFactory.getInstance().getLogger(ImportDefAndSpecsServiceImpl.class);
	
	private ImportDefinitionDAO importDefinitionDAO;
	
	private ImportSpecDAO importSpecDAO;
	
	private ListCodesDAO listCodesDAO;
	
	private ImportConnectionDAO importConnectionDAO;
	
   /**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/
          // Import Definition Related methods
	/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/
   public ImportDefinitionDAO getImportDefinitionDAO() {
		return importDefinitionDAO;
	}

	public void setImportDefinitionDAO(ImportDefinitionDAO importDefinitionDAO) {
		this.importDefinitionDAO = importDefinitionDAO;
	}

	public List<ImportDefDTO> getAllImportDefinition(){
		
		List<ImportDefDTO> listImportDefDTO=new ArrayList<ImportDefDTO>();
		List<ImportDefinition> listImportDefList=importDefinitionDAO.getAllImportDefinition();
		if(listImportDefList!=null){
			for(ImportDefinition importDef1 : listImportDefList)
				listImportDefDTO.add(importDef1.getImportDefDTO());
		}
		
		return listImportDefDTO;
		
	}
	
	public List<ImportDefDTO> getAllImportDefinition(String type) {
		
		List<ImportDefDTO> listImportDefDTO=new ArrayList<ImportDefDTO>();
		List<ImportDefinition> listImportDefList=importDefinitionDAO.getAllImportDefinition(type);
		if(listImportDefList!=null){
			for(ImportDefinition importDef1 : listImportDefList)
				listImportDefDTO.add(importDef1.getImportDefDTO());
		}
		
		return listImportDefDTO;
		
	}	
	
	public ImportDefDTO addImportDefRecord(ImportDefDTO instanceDTO) throws DataAccessException{
		
		logger.info("Starting point in ImportDefAndSpecsServiceImpl.addImportDefRecord");
		ImportDefinition importDefinitionObject = getImportDefObject(instanceDTO);
		return (importDefinitionDAO.addImportDefRecord(importDefinitionObject)).getImportDefDTO();		
	}
	
    public void update(ImportDefDTO instanceDTO) throws DataAccessException{
		logger.info(" Start of update for ImportDefAndSpecsServiceImpl.update");
		ImportDefinition importDefinitionObject = getImportDefObject(instanceDTO);
		importDefinitionDAO.update(importDefinitionObject);
		logger.info(" End of update for ImportDefAndSpecsServiceImpl.update");   	
    }
    
    public void delete(String importDefCode) throws DataAccessException{
    	logger.info(" Start of delete for ImportDefAndSpecsServiceImpl.delete");
    	importDefinitionDAO.delete(importDefCode);
    }
	
    public ImportDefinition getImportDefObject(ImportDefDTO instanceDTO) {
    	ImportDefinition importDefinition= new ImportDefinition();
    	BeanUtils.copyProperties(instanceDTO,importDefinition);
    	return importDefinition;
    }
    
    public ImportDefinition find(String importDefCode) {
    	return importDefinitionDAO.findByPK(importDefCode);    	
    }
	
    //////////////////////////////////////////////////////////////////////////////////////
    //**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/                      /
    //		// Import Specs Related methods                                              /
	//**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**/                      /
    //////////////////////////////////////////////////////////////////////////////////////
    
	public ImportSpecDAO getImportSpecDAO() {
		return importSpecDAO;
	}

	public void setImportSpecDAO(ImportSpecDAO importSpecDAO) {
		this.importSpecDAO = importSpecDAO;
	}
	
	public List<ImportSpecDTO> getAllImportSpec() throws DataAccessException{
		
		List<ImportSpecDTO> listImportSpecDTO=new ArrayList<ImportSpecDTO>();
		List<ImportSpec> listImportSpecList=importSpecDAO.getAllImportSpec();
		if(listImportSpecList!=null){
			for(ImportSpec importSpec1 : listImportSpecList)
				listImportSpecDTO.add(importSpec1.getImportSpecDTO());
		}
		logger.debug(" It is inside getAllImportSpec :::listImportSpecDTO.size::"+listImportSpecDTO.size());
		return listImportSpecDTO;		
	}

	public List<ImportSpecDTO> getImportSpecByDefCode(String defCode) throws DataAccessException{
		
		List<ImportSpecDTO> listImportSpecDTO=new ArrayList<ImportSpecDTO>();
		List<ImportSpec> listImportSpecList=importSpecDAO.getImportSpecByDefCode(defCode);
		if(listImportSpecList!=null){
			for(ImportSpec importSpec1 : listImportSpecList)
				listImportSpecDTO.add(importSpec1.getImportSpecDTO());
		}
		logger.debug(" It is inside getImportSpecByDefCode :::listImportSpecDTO.size::"+listImportSpecDTO.size());
		return listImportSpecDTO;		
	}

	public ImportSpecDTO addImportSpecRecord(ImportSpecDTO instanceDTO) throws DataAccessException{
		logger.info("Starting point in ImportDefAndSpecsServiceImpl.addImportSpecRecord");
		ImportSpec importSpecObject = getImportSpecObject(instanceDTO);
		return (importSpecDAO.addImportSpecRecord(importSpecObject)).getImportSpecDTO();		
		
	}
	public void updateImportSpec(ImportSpecDTO importSpecDTO) throws DataAccessException{
		logger.info(" Start of update for ImportDefAndSpecsServiceImpl.update");
		ImportSpec importSpecObject = getImportSpecObject(importSpecDTO);
		importSpecDAO.update(importSpecObject);
		logger.info(" End of update for ImportDefAndSpecsServiceImpl.update"); 		
	}
    public void deleteImportSpec(ImportSpecPK pk) throws DataAccessException{
    	logger.info(" Start of delete for ImportDefAndSpecsServiceImpl.deleteSpec");
    	importSpecDAO.delete(pk);
    }
    public ImportSpec getImportSpec(ImportSpecPK pk) throws DataAccessException{
    	logger.info(" Start of get for ImportDefAndSpecsServiceImpl.getSpec");
    	return importSpecDAO.findByPK(pk);
    }
    
    public ImportSpec getImportSpecObject(ImportSpecDTO instanceDTO) {
    	
		   ImportSpecPK lpk = new ImportSpecPK();
			lpk.setImportSpecType(instanceDTO.getImportSpecType());
			lpk.setImportSpecCode(instanceDTO.getImportSpecCode());
		   
    	ImportSpec importSpec= new ImportSpec(lpk);
    	BeanUtils.copyProperties(instanceDTO,importSpec);
    	return importSpec;
    }	
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////  Methods Related to Import Spec Details   /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public List<UserDTO> getImportSpecUserBySpec(ImportSpecDTO instanceDTO) throws DataAccessException{
    
		ImportSpecPK specPK  = new ImportSpecPK();
		specPK.setImportSpecType(instanceDTO.getImportSpecType());
		specPK.setImportSpecCode(instanceDTO.getImportSpecCode());
		return getImportSpecUserBySpecPK(specPK);
    }
    
    public List<UserDTO> getImportSpecUserBySpecPK(ImportSpecPK specPK) throws DataAccessException{
        
		List<UserDTO> listUserDTO= new ArrayList<UserDTO>();
		
		List<User> listImportSpecUserList=importSpecDAO.getImportSpecUserBySpec(specPK);
		if(listImportSpecUserList!=null){
			for(User user1 : listImportSpecUserList)
				listUserDTO.add(user1.getUserDTO());
		}
		
		return listUserDTO;    	
    }
    
    public void updateImportSpecUserBySpec(ImportSpecDTO importSpecDTO, List<String> userCodeList) throws DataAccessException {
		ImportSpec importSpec = getImportSpecObject(importSpecDTO);
		importSpecDAO.updateImportSpecUserBySpec(importSpec, userCodeList);
    }
    
    public List<ImportSpec> getAllForUserByType(String type) throws DataAccessException {
    	return importSpecDAO.getAllForUserByType(type);
    }

	@Override
	public List<ImportDefDTO> getDefinitionsByConnection(
			String connectionCode) throws DataAccessException {	
		List <ImportDefinition> defsByConn = importDefinitionDAO.getDefinitionsByConnection(connectionCode);
		List <ImportDefDTO> defsDTOByConn = new ArrayList<ImportDefDTO>();
		
		for (ImportDefinition impDef: defsByConn) {
			defsDTOByConn.add(impDef.getImportDefDTO());
		}
		return defsDTOByConn;
	}
	
	@Override
	public List<ListCodes> getConnectionTypeCodes() {		
		return listCodesDAO.getListCodesByCodeTypeCode("DBTYPE");
	}

	public ListCodesDAO getListCodesDAO() {
		return listCodesDAO;
	}

	public void setListCodesDAO(ListCodesDAO listCodesDAO) {
		this.listCodesDAO = listCodesDAO;
	}

	@Override
	public void addConnection(ImportConnection importConnection)
			throws DataAccessException {

		importConnectionDAO.save(importConnection);
		
		
	}

	@Override
	public void updateConnection(ImportConnection importConnection)
			throws DataAccessException {
		
		
		importConnectionDAO.update(importConnection);
		
		
	}

	@Override
	public void deleteConnection(String importConnectionCode)
			throws DataAccessException {
		importConnectionDAO.delete(importConnectionCode);
		
		
	}

	public ImportConnectionDAO getImportConnectionDAO() {
		return importConnectionDAO;
	}

	public void setImportConnectionDAO(ImportConnectionDAO importConnectionDAO) {
		this.importConnectionDAO = importConnectionDAO;
	}

	public List<ImportConnection> getAllImportConnection() throws DataAccessException{
		List<ImportConnection> listImportConnection = importConnectionDAO.findAll();
		return listImportConnection;		
	}
	
	public List<ImportConnection> getConnectionsByCode(String importConnectionCode) throws DataAccessException{
    	return importConnectionDAO.getConnectionsByCode(importConnectionCode);    	
    }
	
	  public List<String> getImportDefinitionCodes(String tablename) throws DataAccessException{
    	return importDefinitionDAO.getImportDefinitionCodes(tablename);    	
    }
}
