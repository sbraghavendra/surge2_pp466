package com.ncsts.services.impl;


import java.util.List;

import com.ncsts.dao.TaxHolidayDAO;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.services.TaxHolidayService;
import org.springframework.dao.DataAccessException;

public class TaxHolidayServiceImpl extends GenericServiceImpl<TaxHolidayDAO, TaxHoliday, String> implements TaxHolidayService {

	
	public void update(TaxHoliday taxHoliday){
		getDAO().update(taxHoliday);
	}
	public void remove(String taxHolidayCode){
		getDAO().remove(taxHolidayCode);
	}	
	public List<TaxHoliday> getAllTaxHolidayData(){
		return getDAO().getAllTaxHolidayData();
	}

	public TaxHoliday getTaxHolidayData(String taxHolidayCode) throws DataAccessException{
		return getDAO().getTaxHolidayData(taxHolidayCode);
	}
	
	public List<TaxHolidayDetail> findTaxHolidayDetailFromTaxCode(String country, String state, String taxCode, String description, String county, String city, String stj) throws DataAccessException {
		return getDAO().findTaxHolidayDetailFromTaxCode(country, state, taxCode, description, county, city, stj);
	}
	
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String taxCode, String county, String city, String stj) throws DataAccessException {
		return getDAO().findTaxHolidayDetailFromJurisdiction(country, state, taxCode, county, city, stj);
	}

	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String county, String city) throws DataAccessException {
		return getDAO().findTaxHolidayDetailFromJurisdiction(country, state, county, city);
	}

	public List<TaxHolidayDetail> findTaxHolidayDetailByTaxHolidayCode(String taxHolidayCode, String exceptInd, String taxCode) throws DataAccessException {
		return getDAO().findTaxHolidayDetailByTaxHolidayCode(taxHolidayCode, exceptInd, taxCode);
	}
	
	public Long findTaxHolidayDetailCount(String taxHolidayCode) throws DataAccessException {
		return getDAO().findTaxHolidayDetailCount(taxHolidayCode);
	}

	public void executeTaxHolidayDetails(String taxHolidayCode) throws DataAccessException {
		getDAO().executeTaxHolidayDetails(taxHolidayCode);
	}
	
	public void addTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, boolean copyMapFlag) throws DataAccessException {
		getDAO().addTaxHolidayDetails(taxHoliday, taxHolidayDetailList, copyMapFlag);
	}
	
	public void updateTaxHolidayExceptionDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, TaxHolidayDetail selectedTaxCodeDetail) throws DataAccessException {
		getDAO().updateTaxHolidayExceptionDetails(taxHoliday, taxHolidayDetailList, exceptInd, selectedTaxCodeDetail);
	}
	
	public void updateTaxHolidayMapDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, String taxCode, List<TaxHolidayDetail> unselectedList) throws DataAccessException {
		getDAO().updateTaxHolidayMapDetails(taxHoliday, taxHolidayDetailList, exceptInd, taxCode, unselectedList);
	}
	
	public void updateTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException {
		getDAO().updateTaxHoliday(taxHoliday);
	}
	
	public void deleteTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException {
		getDAO().deleteTaxHoliday(taxHoliday);
	}
}
