/**
 * 
 */
package com.ncsts.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.ErrorLogDTO;
import com.ncsts.services.BatchMaintenanceService;

/**
 * @author Muneer 
 *
 */
public class BatchMaintenanceServiceImpl extends GenericServiceImpl<BatchMaintenanceDAO, BatchMaintenance, Long> implements BatchMaintenanceService {
	
	static private Logger logger = LoggerFactory.getInstance().getLogger(BatchMaintenanceServiceImpl.class);

	public BatchMaintenance findByBatchId(Long batchId) {
		return getDAO().findByBatchId(batchId);
		}



	public List<BatchMaintenanceDTO> getBatchMaintenanceData(
															BatchMaintenanceDTO batchMaintenanceDTO,
															Hashtable<String,String> batchTypeDtl,
															Hashtable<String,String> batchStatusDtl
															) {
		
		logger.debug(" you are inside getBatchMaintenanceData from Service IMPL ::");
		
		BatchMaintenance batchMaintenanceObject = getBatchMaintenanceObject(batchMaintenanceDTO);
				
		List<BatchMaintenanceDTO> listBatchMaintenanceDTO = new ArrayList<BatchMaintenanceDTO>();
		List<BatchMaintenance> listBatchList=getDAO().findByBatchMaintenance(batchMaintenanceObject);
		
		
		if(listBatchList!=null){
						
			for(BatchMaintenance batchMaintenance : listBatchList){
				
				BatchMaintenanceDTO tempDTO = batchMaintenance.getBatchMaintenanceDTO();
				logger.info("BATCH ITEM"+ listBatchList.get(0));
				if(batchTypeDtl != null){
					String tmpBatchTypeDesc = (tempDTO.getBatchTypeCode() != null)?((String)batchTypeDtl.get( tempDTO.getBatchTypeCode())):"";
					tempDTO.setBatchTypeDesc(tmpBatchTypeDesc);
					logger.info("FROM IMPL "+tmpBatchTypeDesc);
				}
				
				if(batchStatusDtl != null){
					String tmpBatchStatusDesc = (tempDTO.getBatchStatusCode() != null)?((String)batchStatusDtl.get( tempDTO.getBatchStatusCode())):"";
					tempDTO.setBatchStatusDesc(tmpBatchStatusDesc);
				}
				
				listBatchMaintenanceDTO.add(tempDTO);
				logger.info("listBatchMaintenanceDTO size "+listBatchMaintenanceDTO.size());
			}
		}		
		return listBatchMaintenanceDTO;

	}
	
  // Correct working method
/*	public List<BatchMaintenanceDTO> getBatchMaintenanceData(
			BatchMaintenanceDTO batchMaintenanceDTO) {
		
		logger.debug(" MB you are inside getBatchMaintenanceData from Service IMPL ::");
		
		BatchMaintenance batchMaintenanceObject = getBatchMaintenanceObject(batchMaintenanceDTO);
		logger.debug(" MB In service IMPL1 :: input batchMaintenanceObject::"+batchMaintenanceObject);
		if(batchMaintenanceObject != null){
		    logger.debug(" MB In service IMPL2 ::"+batchMaintenanceObject.getBatchId());
		    logger.debug(" MB In service IMPL3 ::"+batchMaintenanceObject.getBatchStatusCode());
		    
		    logger.debug(" MB In service IMPL4 ::"+batchMaintenanceObject.getBatchTypeCode());
		}
				
		List<BatchMaintenanceDTO> listBatchMaintenanceDTO = new ArrayList<BatchMaintenanceDTO>();
		List<BatchMaintenance> listBatchList=getDAO().findByBatchMaintenance(batchMaintenanceObject);
		logger.debug(" MB In service IMPL5 ::"+listBatchList);
		if(listBatchList!=null){
			logger.debug(" MB In service IMPL6 ::"+listBatchList.size());
			for(BatchMaintenance batchMaintenance : listBatchList)
				listBatchMaintenanceDTO.add(batchMaintenance.getBatchMaintenanceDTO());
		}
		logger.debug(" MB In service IMPL7 ::"+listBatchMaintenanceDTO.size());
		return listBatchMaintenanceDTO;
	}*/
	
	
	private BatchMaintenance getBatchMaintenanceObject(BatchMaintenanceDTO batchMaintenanceDTO){
		
		BatchMaintenance batchMaintenance= new BatchMaintenance();
		   BeanUtils.copyProperties(batchMaintenanceDTO,batchMaintenance);
		   return batchMaintenance;
	}
	
	public void update(BatchMaintenance batchMaintenance) {
		//BatchMaintenance batchMaintenanceObject = getBatchMaintenanceObject(batchMaintenanceDTO);		
		getDAO().update(batchMaintenance);
		 
	}	
	public void remove(Long batchId) {
		getDAO().remove(batchId);
	}

	public List<BatchMaintenance> findByStatusUpdateUser(String batchTypeCode, String batchStatusCode, String statusUpdateUserId) throws DataAccessException{
		return getDAO(). findByStatusUpdateUser(batchTypeCode, batchStatusCode, statusUpdateUserId);
	}
	
	public  List<BatchMaintenanceDTO> getAllBatchMaintenanceData(){
		
		List<BatchMaintenanceDTO> listBatchMaintenanceDTO = new ArrayList<BatchMaintenanceDTO>();		
		List<BatchMaintenance> listBatchMaintenance=getDAO().getAllBatchMaintenanceData();		
		if(listBatchMaintenance!=null){
			for(BatchMaintenance batchMaintenance : listBatchMaintenance)
				listBatchMaintenanceDTO.add(batchMaintenance.getBatchMaintenanceDTO());
		}
		
		return listBatchMaintenanceDTO;
	}
	public  List<BatchMaintenance> getBatchMaintenanceByBatchStatusCode(String batchStatusCode){
		return getDAO().getBatchMaintenanceByBatchStatusCode(batchStatusCode);
	}
	public  List<BatchMaintenance> getBatchMaintenanceByBatchTypeCode(String batchTypeCode){
		return getDAO().getBatchMaintenanceByBatchTypeCode(batchTypeCode);
	}

	/** this method added by anand **/
	public List<BatchMaintenanceDTO> getAllBatchesByBatchTypeCode(String batchTypeCode) {
		// TODO Auto-generated method stub
		List<BatchMaintenanceDTO> batchList = new ArrayList<BatchMaintenanceDTO>();
		List<BatchMaintenance> list = getDAO().getAllBatchesByBatchTypeCode(batchTypeCode);
		for(BatchMaintenance bm : list){
			batchList.add(bm.getBatchMaintenanceDTO());
		}
		return batchList;
	}

	public BatchMaintenanceDTO save(BatchMaintenanceDTO batchMaintenanceDTO) {
		BatchMaintenance bm = new BatchMaintenance();
		BeanUtils.copyProperties(batchMaintenanceDTO, bm);
		return getDAO().saveBatch(bm).getBatchMaintenanceDTO();
	}
	public void createNewBatchGL(BatchMaintenance batchMaintenance,String glDate){
		
	    getDAO().createNewBatchGL(batchMaintenance,glDate);
	}

	/**
	 * @author Anand
	 * Obsolete - JMW 08/28/08
	 */
//	public void processTaxRateUpdateBatch(Long batchId) {
//		logger.debug("process starts for batch id == " + batchId);
//		getDAO().processTaxRateUpdateBatch(batchId);
//		logger.debug("process ends for batch id == " + batchId);
//	}*/


	/**
	 * @author Charishma
	 */
	public List<ErrorLogDTO> getBatchProcesses(Long batchId){
		List<Object[]> list = getDAO().getBatchProcesses(batchId);
//		List<Object[]> list = batchMaintenanceDAO.getErrorLog(SQLQuery.TB_BATCH_ERROR_PROCESSES, batchId);
		List<ErrorLogDTO> processes = new ArrayList<ErrorLogDTO>();
		for (Object[] o : list) {
			ErrorLogDTO b = new ErrorLogDTO();
			int i = -1;
			b.setProcessTimestamp((Timestamp)o[++i]);
			b.setProcessType((String)o[++i]);
			b.setBatchId((Long)o[++i]);
			processes.add(b);
		}
		return processes;
	}		

	public List<ErrorLogDTO> getBatchErrors(ErrorLogDTO batch){
		BatchErrorLog b = new BatchErrorLog();
		BeanUtils.copyProperties(batch, b);
		List<BatchErrorLog> list = getDAO().getBatchErrors(b);
		List<ErrorLogDTO> details = new ArrayList<ErrorLogDTO>();
		for (BatchErrorLog e : list) {
			ErrorLogDTO dto = new ErrorLogDTO();
			BeanUtils.copyProperties(e, dto);
			details.add(dto);
		}
		return details;
	}

	@Override
	public void saveBatchMetadata(BatchMetadata batchMetadata) {
		getDAO().saveBatchMetadata(batchMetadata);
	}

	public BatchMaintenance getBatchMaintananceData(long batchId) throws DataAccessException {
		// TODO Auto-generated method stub
		return getDAO().getBatchMaintananceData(batchId);
	}

	public List<BatchMaintenanceDTO> getAllBatchesByBatchTypeCode(String codeTypeCode,
			List<String> batchtypeCodeCode, List<String> batchstatusCodeCode, List<String> errorsevCodeCode) {
		List<BatchMaintenanceDTO> batchList = new ArrayList<BatchMaintenanceDTO>();
		List<BatchMaintenance> list = getDAO().getAllBatchesByBatchTypeCode(codeTypeCode, batchtypeCodeCode, batchstatusCodeCode, errorsevCodeCode);
		for (BatchMaintenance bm : list) {
			batchList.add(bm.getBatchMaintenanceDTO());
		}
		return batchList;
	}
      
	public BatchMetadata findBatchMetadataById(String id) throws DataAccessException {
		return getDAO().findBatchMetadataById(id);
	}
	
	public BatchErrorLog save(BatchErrorLog error) throws DataAccessException{
		return getDAO().save(error);
	}

	public List<BatchMetadata> loadValidMetadata(boolean isPurchasingSelected) {
		return getDAO().loadValidMetadata(isPurchasingSelected);
	}
	
	public boolean hasExtractedTransactions(List<Long> ids){
		return getDAO().hasExtractedTransactions(ids);
	}
	
	public List<BatchMaintenance> findFlaggedProcessTaxRateBatches() throws DataAccessException {
		return getDAO().findFlaggedProcessTaxRateBatches();
	}
	
	public boolean checkRunningBatches() {
		return getDAO().checkRunningBatches();
	}
      
      //    public BatchErrorLog getBatchErrorDetail(Long batchErrorLogId){
//      List<BatchErrorLog> details = batchMaintenanceDAO.getErrorLogDetails(batchErrorLogId);
//      List<ErrorLogDTO> detail = new ArrayList<ErrorLogDTO>();
//      for (BatchErrorLog e : details) {
//              BatchErrorLog dto = new BatchErrorLog();
//              BeanUtils.copyProperties(e, dto);
//              details.add(dto);
//      }
//      return details.get(0);
//}
//      List<Object[]> list = batchMaintenanceDAO.getErrorLog(SQLQuery.TB_BATCH_ERROR_LOG_DETAIL_ERRORS , batchErrorLogId);
//      List<ErrorLogDTO> detail = new ArrayList<ErrorLogDTO>();
//      Iterator<Object[]> itr = list.iterator();
//      itr = list.iterator();
//      while(itr.hasNext()){
//        Object[] objArray = itr.next();
//        detail.add(getErrorDetailDTO(objArray));
//      }
//      return detail;
//	}
	/**
	 * @author Charishma
	 */
	/*private ErrorLogDTO getErrorDTO(Object[] objArray) {
		
		ErrorLogDTO errorDTO = new ErrorLogDTO();
		int i = 0;
		if(objArray[i] != null){
			errorDTO.setProcessTypeDesc((objArray[i].toString()));
		}
		if(objArray[++i] != null){
			errorDTO.setProcessTimestamp((Date)objArray[i]);
		}		
        if(objArray[++i] != null){
          errorDTO.setProcessType((String)objArray[i]);
        }       
        if(objArray[++i] != null){
          errorDTO.setBatchId(objArray[i].toString());
        }       
        if(objArray[++i] != null){
          errorDTO.setBatchErrorLogId(objArray[i].toString());
        }       
      return errorDTO;
	}
	*/
/*	private ErrorLogDTO getErrorDetailDTO(Object[] objArray) {
		int i = 0;
		ErrorLogDTO errorDTO = new ErrorLogDTO();
		if(objArray[i] != null){
			errorDTO.setBatchErrorLogId(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setBatchId(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setProcessType(objArray[i].toString());
		}
        if(objArray[++i] != null){
          errorDTO.setProcessTimestamp((Date)objArray[i]);
      }   
			
		if(objArray[++i] != null){
			errorDTO.setRowNumber(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setColumnNumber(objArray[i].toString());
		}	
		if(objArray[++i] != null){
			errorDTO.setErrorDefCode(objArray[i].toString());
		}	
		if(objArray[++i] != null){
			errorDTO.setErrorDefDesc(objArray[i].toString());
		}	
		if(objArray[++i] != null){
			errorDTO.setImportHeaderColumn(objArray[i].toString());
		}	
		if(objArray[++i] != null){
			errorDTO.setImportColumnValue(objArray[i].toString());
		}	
		if(objArray[++i] != null){
			errorDTO.setTransDtlColumnName(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setTransDtlColumnDesc(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setTransDtlDatatype(objArray[i].toString());
		}
		if(objArray[++i] != null){
			errorDTO.setImportRow(objArray[i].toString());
		}
		if(objArray.length > i){
			if(objArray[++i] != null){
				errorDTO.setProcessTypeDesc(objArray[i].toString());
			}
			if(objArray[++i] != null){
				errorDTO.setErrorDefExplanation(objArray[i].toString());
			}
			if(objArray[++i] != null){
				errorDTO.setSevLevel(objArray[i].toString());
			}
			if(objArray[++i] != null){
				errorDTO.setSevLevelDesc(objArray[i].toString());
			}
		}
		return errorDTO;
	}
*/	
/*	public ErrorLogDTO batchErrorsDetails(String batchErrorLogId){
		List<Object[]> list = getDAO().getErrorLog(SQLQuery.TB_BATCH_ERROR_LOG , Long.valueOf(batchErrorLogId));
		ErrorLogDTO errorObj = new ErrorLogDTO();
		if(list != null){
			logger.debug("Error details size   " + list.size());
			Object[] objArray = list.get(0);
			logger.debug("Error objArray size   " + objArray.length);
			errorObj = getErrorDetailDTO(objArray);
			
		}
		return errorObj;
<<<<<<< .mine
	}*/
	
//	public BigDecimal getlastNumber(String sequenceName) throws DataAccessException {
//		return getDAO().getLastNumber(sequenceName);
//	}
}
