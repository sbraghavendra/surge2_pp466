package com.ncsts.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import com.ncsts.domain.DataDefinitionColumnDrivers;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.DriverReferenceDAO;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.dto.DriverNamesDTO;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.view.bean.MatrixDriverBean;

public class DriverReferenceServiceImpl implements DriverReferenceService {
	 private Logger logger = LoggerFactory.getInstance().getLogger(DriverReferenceServiceImpl.class);
	
	DriverReferenceDAO driverReferenceDAO;
	
	public DriverReferenceDAO getDriverReferenceDAO() {
		return driverReferenceDAO;
	}

	public void setDriverReferenceDAO(DriverReferenceDAO driverReferenceDAO) {
		this.driverReferenceDAO = driverReferenceDAO;
	}

	public List<DriverReference> findDriverReference(String transDtlColName, String driverValue, String driverDesc, String userValue, int count) {
		return driverReferenceDAO.findDriverReference(transDtlColName, driverValue, driverDesc, userValue, count);
	}
	
	public List<DriverReference> findByTransactionDetailColumnName(String columnName, int count) throws DataAccessException {
		return driverReferenceDAO.findByTransactionDetailColumnName(columnName, count);
	}	
	
	public List<DriverReference> findByDriverName(String drvValue, String columnName ,int count) throws DataAccessException{
		return driverReferenceDAO.findByDriverName(drvValue, columnName, count);
	}
	
	public DriverReference getDriverReference(String transDtlColName, String driverValue) {
		return driverReferenceDAO.getDriverReference(transDtlColName, driverValue);
	}
	
	public Long countDriverReference(String transDtlColName, String driverValue) {
		return driverReferenceDAO.countDriverReference(transDtlColName, driverValue);
	}
	
	public List<String> getDriverNamesByDrvCodeMatrixCol (String drvCode, String mtrxCol) throws DataAccessException {
		return driverReferenceDAO.getDriverNamesByDrvCodeMatrixCol(drvCode, mtrxCol);		
	}

	public List<DriverNames> getDriverNamesByCode(String driverCode) throws DataAccessException {
		return driverReferenceDAO.getDriverNamesByCode(driverCode);
	}
	
	public void delete(DriverReference driverRef) throws DataAccessException {
		driverReferenceDAO.delete(driverRef);
	}
	
	public void saveOrUpdate(DriverReference driverRef) throws DataAccessException  {
		driverReferenceDAO.saveOrUpdate(driverRef);
	}
	
	public Long getDriverNamesId(String drvCode)throws DataAccessException{
		return driverReferenceDAO.getDriverNamesId(drvCode);
	}
	
	public void saveOrUpdateNames(DriverNamesDTO driverNameDTO) throws DataAccessException{
		DriverNames driverName = new DriverNames();
		try {
			BeanUtils.copyProperties(driverName, driverNameDTO);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driverReferenceDAO.saveOrUpdateNames(driverName);
	}
	
	public void deleteNames(DriverNamesDTO driverNameDTO) throws DataAccessException{
		DriverNames driverName = new DriverNames();
		try {
			BeanUtils.copyProperties(driverName, driverNameDTO);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driverReferenceDAO.deleteNames(driverName);
	}
	
	public DriverNames findDriverForTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException{
		return driverReferenceDAO.findDriverForTransactionDetailColumnName(columnName, code, matrixColName);
	}
	
	public DriverNames findTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException{
		return driverReferenceDAO.findTransactionDetailColumnName(columnName, code, matrixColName);
	}
	//4356
	public boolean getTransactionDetailUpdate(String category,String driverId) throws DataAccessException{ 
		logger.info("------------------>>>Service Impl >>>--------------------------------------------------- ");
		return driverReferenceDAO.getTransactionDetailUpdate(category, driverId);
	}

	
	public String findCurrentTrColumnName(String category, String driverId) throws DataAccessException {
		return driverReferenceDAO.findCurrentTrColumnName(category,driverId);
	}

	public boolean allowColChange(String code, String matrixColName) throws DataAccessException {
		return driverReferenceDAO.allowColChange(code, matrixColName);
	}

	@Override
	public Map<String, DataDefinitionColumnDrivers> getValidDrivers(String driverNamesCode, String tableName) {
		return driverReferenceDAO.getValidDrivers(driverNamesCode, tableName);
	}
}
