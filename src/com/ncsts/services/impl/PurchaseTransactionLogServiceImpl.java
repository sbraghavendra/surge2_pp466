package com.ncsts.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.ncsts.dao.PurchaseTransactionLogDAO;
import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.TransactionLog;
import com.ncsts.services.PurchaseTransactionLogService;
import org.apache.commons.beanutils.BeanUtils;


public class PurchaseTransactionLogServiceImpl extends GenericServiceImpl<PurchaseTransactionLogDAO,PurchaseTransactionLog, Long> implements PurchaseTransactionLogService {


	Class<?> purchaseTransactionClass = PurchaseTransaction.class;

	HashSet<String> itemsWithPrefix = new HashSet<String>(Arrays.asList(new String[]   {"CountryTier1TaxAmt", "CountryTier2TaxAmt", "CountryTier3TaxAmt", "StateTier1TaxAmt",
																					"StateTier2TaxAmt", "StateTier3TaxAmt", "CountyTier1TaxAmt", "CountyTier2TaxAmt",
																					"CountyTier3TaxAmt", "CityTier1TaxAmt", "CityTier2TaxAmt", "CityTier3TaxAmt", "Stj1TaxAmt",
																					"Stj2TaxAmt", "Stj3TaxAmt", "Stj4TaxAmt", "Stj5TaxAmt", "Stj6TaxAmt", "Stj7TaxAmt",
																					"Stj8TaxAmt", "Stj9TaxAmt", "Stj10TaxAmt", "CountyLocalTaxAmt", "CityLocalTaxAmt",
																					"TbCalcTaxAmt"}));





	HashSet<String> afterItemsGeneral = new HashSet<String>(Arrays.asList(new String[] { "GlExtractBatchNo", "GlExtractTimestamp", "DirectPayPermitFlag", "CalculateInd",
			"AllocationMatrixId", "AllocationSubtransId", "TaxAllocMatrixId", "SplitSubtransId",
			"MultiTransCode",
			//"LocationMatrixId",
			//"ManualJurisdictionInd",
			"GlDate",
			"GlLineItmDistAmt",
			"TaxcodeCode",
			"ManualTaxcodeInd", "TaxMatrixId", "Comments", "ProcessingNotes", "TransactionStateCode",
			"AutoTransactionStateCode", "TransactionCountryCode", "AutoTransactionCountryCode",
			"TransactionInd", "SuspendInd", "RejectFlag", "CountryTier1TaxableAmt",
			"CountryTier2TaxableAmt", "CountryTier3TaxableAmt", "StateTier1TaxableAmt",
			"StateTier2TaxableAmt", "StateTier3TaxableAmt", "CountyTier1TaxableAmt",
			"CountyTier2TaxableAmt", "CountyTier3TaxableAmt", "CityTier1TaxableAmt",
			"CityTier2TaxableAmt", "CityTier3TaxableAmt",
			"Stj1TaxableAmt",
			"Stj2TaxableAmt", "Stj3TaxableAmt", "Stj4TaxableAmt",
			"Stj5TaxableAmt", "Stj6TaxableAmt", "Stj7TaxableAmt",
			"Stj8TaxableAmt", "Stj9TaxableAmt", "Stj10TaxableAmt",
			"Distr01Amt", "Distr02Amt", "Distr03Amt", "Distr04Amt",
			"Distr05Amt", "Distr06Amt", "Distr07Amt", "Distr08Amt", "Distr09Amt",
			"Distr10Amt", "CountrySitusMatrixId", "StateSitusMatrixId", "CountySitusMatrixId",
			"CitySitusMatrixId", "Stj1SitusMatrixId", "Stj2SitusMatrixId", "Stj3SitusMatrixId",
			"Stj4SitusMatrixId", "Stj5SitusMatrixId", 
		    "ShiptoLocnMatrixId","ShipfromLocnMatrixId","OrdracptLocnMatrixId","OrdrorgnLocnMatrixId",
		    "FirstuseLocnMatrixId","BilltoLocnMatrixId","TtlxfrLocnMatrixId","ShiptoManualJurInd",		
			"CountryCode", "StateCode", "CountyName",
			"CityName", "Stj1Name", "Stj2Name", "Stj3Name", "Stj4Name", "Stj5Name", "Stj6Name",
			"Stj7Name", "Stj8Name", "Stj9Name", "Stj10Name",
			"CountryVendorNexusDtlId", "StateVendorNexusDtlId", "CountyVendorNexusDtlId", "CityVendorNexusDtlId",
			"CountryTaxcodeDetailId", "StateTaxcodeDetailId", "CountyTaxcodeDetailId",
			"CityTaxcodeDetailId", "Stj1TaxcodeDetailId", "Stj2TaxcodeDetailId",
			"Stj3TaxcodeDetailId", "Stj4TaxcodeDetailId", "Stj5TaxcodeDetailId",
			"Stj6TaxcodeDetailId", "Stj7TaxcodeDetailId", "Stj8TaxcodeDetailId",
			"Stj9TaxcodeDetailId", "Stj10TaxcodeDetailId", "CountryTaxtypeCode",
			"StateTaxtypeCode", "CountyTaxtypeCode", "CityTaxtypeCode", "Stj1TaxtypeCode",
			"Stj2TaxtypeCode", "Stj3TaxtypeCode", "Stj4TaxtypeCode", "Stj5TaxtypeCode",
			"Stj6TaxtypeCode", "Stj7TaxtypeCode", "Stj8TaxtypeCode", "Stj9TaxtypeCode",
			"Stj10TaxtypeCode", "CountryTaxrateId", "StateTaxrateId", "CountyTaxrateId",
			"CityTaxrateId", "Stj1TaxrateId", "Stj2TaxrateId", "Stj3TaxrateId", "Stj4TaxrateId",
			"Stj5TaxrateId", "Stj6TaxrateId", "Stj7TaxrateId", "Stj8TaxrateId", "Stj9TaxrateId",
			"Stj10TaxrateId", "CountryTaxcodeOverFlag", "StateTaxcodeOverFlag", "CountyTaxcodeOverFlag",
			"CityTaxcodeOverFlag", "Stj1TaxcodeOverFlag", "Stj2TaxcodeOverFlag", "Stj3TaxcodeOverFlag",
			"Stj4TaxcodeOverFlag", "Stj5TaxcodeOverFlag", "Stj6TaxcodeOverFlag", "Stj7TaxcodeOverFlag",
			"Stj8TaxcodeOverFlag", "Stj9TaxcodeOverFlag", "Stj10TaxcodeOverFlag", "ShiptoJurisdictionId",
			"ShipfromJurisdictionId", "OrdracptJurisdictionId", "OrdrorgnJurisdictionId",
			"FirstuseJurisdictionId", "BilltoJurisdictionId", "TtlxfrJurisdictionId",

			/*
			"CountryTier1Rate",
			"CountryTier2Rate", "CountryTier3Rate", "CountryTier1Setamt", "CountryTier2Setamt",
			"CountryTier3Setamt", "CountryTier1MaxAmt", "CountryTier2MinAmt", "CountryTier2MaxAmt",
			"CountryTier2EntamFlag", "CountryTier3EntamFlag", "CountryMaxtaxAmt",
			"CountryTaxableThresholdAmt", "CountryMinimumTaxableAmt", "CountryMaximumTaxableAmt",
			"CountryMaximumTaxAmt", "CountryBaseChangePct",
			"CountrySpecialRate", "CountrySpecialSetamt",
			"CountryTaxProcessTypeCode", "StateTier1Rate", "StateTier2Rate", "StateTier3Rate",
			"StateTier1Setamt", "StateTier2Setamt", "StateTier3Setamt", "StateTier1MaxAmt",
			"StateTier2MinAmt", "StateTier2MaxAmt", "StateTier2EntamFlag", "StateTier3EntamFlag",
			"StateMaxtaxAmt", "StateTaxableThresholdAmt", "StateMinimumTaxableAmt",
			"StateMaximumTaxableAmt", "StateMaximumTaxAmt", "StateBaseChangePct",
			"StateSpecialRate", "StateSpecialSetamt",
			"StateTaxProcessTypeCode", "CountyTier1Rate", "CountyTier2Rate",
			"CountyTier3Rate", "CountyTier1Setamt", "CountyTier2Setamt", "CountyTier3Setamt",
			"CountyTier1MaxAmt", "CountyTier2MinAmt", "CountyTier2MaxAmt", "CountyTier2EntamFlag",
			"CountyTier3EntamFlag", "CountyMaxtaxAmt", "CountyTaxableThresholdAmt", "CountyMinimumTaxableAmt",
			"CountyMaximumTaxableAmt", "CountyMaximumTaxAmt", "CountyBaseChangePct",
			"CountySpecialRate", "CountySpecialSetamt",
			"CountyTaxProcessTypeCode", "CityTier1Rate", "CityTier2Rate", "CityTier3Rate",
			"CityTier1Setamt", "CityTier2Setamt", "CityTier3Setamt", "CityTier1MaxAmt", "CityTier2MinAmt",
			"CityTier2MaxAmt", "CityTier2EntamFlag", "CityTier3EntamFlag", "CityMaxtaxAmt",
			"CityTaxableThresholdAmt", "CityMinimumTaxableAmt", "CityMaximumTaxableAmt", "CityMaximumTaxAmt",
			"CityBaseChangePct",
			"CitySpecialRate", "CitySpecialSetamt",
			"CityTaxProcessTypeCode", "Stj1Rate",
			"Stj1Setamt", "Stj2Rate", "Stj2Setamt", "Stj3Rate", "Stj3Setamt", "Stj4Rate", "Stj4Setamt",
			"Stj5Rate", "Stj5Setamt", "Stj6Rate", "Stj6Setamt", "Stj7Rate", "Stj7Setamt", "Stj8Rate",
			"Stj8Setamt", "Stj9Rate", "Stj9Setamt", "Stj10Rate", "Stj10Setamt", "CombinedRate",
			*/
			"CountryTaxtypeUsedCode", "StateTaxtypeUsedCode", "CountyTaxtypeUsedCode", "CityTaxtypeUsedCode",
			"Stj1TaxtypeUsedCode", "Stj2TaxtypeUsedCode", "Stj3TaxtypeUsedCode", "Stj4TaxtypeUsedCode",
			"Stj5TaxtypeUsedCode", "Stj6TaxtypeUsedCode", "Stj7TaxtypeUsedCode", "Stj8TaxtypeUsedCode",
			"Stj9TaxtypeUsedCode", "Stj10TaxtypeUsedCode", "ModifyUserId"}));

	/*public List<PurchaseTransactionLog> getAllGLTransactions(PurchaseTransactionLog exampleInstance,
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds)
			throws DataAccessException{
		return getDAO().getAllGLTransactions(exampleInstance, sqlWhere, orderBy, firstRow, maxResults, nullFileds);
	}*/
	@Override
	public Long getTransactionCount(PurchaseTransactionLog exampleInstance, 
			String sqlWhere, List<String> nullFileds) {
		return getDAO().getTransactionCount(exampleInstance, sqlWhere, nullFileds);
	}
	@Override
	public List<PurchaseTransactionLog> findTransactionDetailLogList(Long purchtransId) {
    	
    	return getDAO().findPurchaseTransactionLogList(purchtransId);
    }

	@Override
	public void fillBeforeEntries(TransactionLog transactionLog, PurchaseTransaction transaction) {

		for (String beforeField: itemsWithPrefix) {
			fillWithPrefix(transactionLog, transaction, beforeField, "B");
		}

	}

	@Override
	public void fillAfterEntries(TransactionLog transactionLog, PurchaseTransaction transaction) {
		for (String afterField: itemsWithPrefix) {
			fillWithPrefix(transactionLog, transaction, afterField, "A");
		}

		for (String afterField: afterItemsGeneral) {
			fillWithPrefix(transactionLog, transaction, afterField, "");
		}

	}
	
	@Override
	public void fillBeforeEntries(TransactionLog transactionLog, MicroApiTransaction transaction) {

		for (String beforeField: itemsWithPrefix) {
			fillWithPrefix(transactionLog, transaction, beforeField, "B");
		}

	}

	@Override
	public void fillAfterEntries(TransactionLog transactionLog, MicroApiTransaction transaction) {
		for (String afterField: itemsWithPrefix) {
			fillWithPrefix(transactionLog, transaction, afterField, "A");
		}

		for (String afterField: afterItemsGeneral) {
			fillWithPrefix(transactionLog, transaction, afterField, "");
		}

	}

	private void fillWithPrefix(TransactionLog transactionLog, PurchaseTransaction transaction, String fieldName, String prefix) {	
		Class<?> transactionClass = transaction.getClass();
		Method getter = null;
		try {
			getter = transactionClass.getMethod("get" + fieldName);
			Object value = getter.invoke(transaction, null);	
			
			if (prefix==null || prefix.equals(""))
				fieldName = fieldName.substring(0,1).toLowerCase() + fieldName.substring(1, fieldName.length());
			BeanUtils.copyProperty(transactionLog, prefix+fieldName, value);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void fillWithPrefix(TransactionLog transactionLog, MicroApiTransaction transaction, String fieldName, String prefix) {
		Class<?> transactionClass = transaction.getClass();
		Method getter = null;
		try {
			getter = transactionClass.getMethod("get" + fieldName);
			Object value = getter.invoke(transaction, null);

			if (prefix==null || prefix.equals(""))
				fieldName = fieldName.substring(0,1).toLowerCase() + fieldName.substring(1, fieldName.length());
			BeanUtils.copyProperty(transactionLog, prefix+fieldName, value);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
