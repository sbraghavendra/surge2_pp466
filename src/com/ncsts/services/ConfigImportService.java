package com.ncsts.services;

import java.io.InputStream;
import java.sql.Connection;
import java.util.List;

import com.ncsts.domain.BCPConfigStagingText;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.view.util.ImportConfigFileUploadParser;

public interface ConfigImportService {
	
	public List<BCPConfigStagingText> getImportedData(Long batchId); 
	public BatchMaintenance addBatchUsingJDBC(BatchMaintenance batchMaintenance);
	public BatchMaintenance importConfigFile(BatchMaintenance batchMaintenance,ImportConfigFileUploadParser parser,InputStream inputStream)throws Exception;
	public void updateBatchUsingJDBC(BatchMaintenance batchMaintenance) throws Exception;
	public void exportTableToFile(String tableName);
	public void createTemplate(String tableName);

	
}
