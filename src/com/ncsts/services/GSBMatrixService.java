package com.ncsts.services;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.GSBMatrix;

import java.util.List;

public interface GSBMatrixService extends MatrixService<GSBMatrix> {
	public List<GSBMatrix> getDetailRecords(GSBMatrix taxMatrix, OrderBy orderBy,String modulecode);
}

