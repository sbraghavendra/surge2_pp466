package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Menu;

/**
 * 
 * @author Paul Govindan
 *
 */
public interface MenuService {
	
    public abstract Menu findById(String menuCode) throws DataAccessException;	
    
    public abstract List<Menu> findAllMenus() throws DataAccessException;
    
    public abstract List<String> findAllMainMenuCodes() throws DataAccessException;
    
	public void remove(String id) throws DataAccessException;    
    
	public void saveOrUpdate (Menu menu) throws DataAccessException;     
    
	public String persist (Menu menu) throws DataAccessException;    
	
	public abstract List<Menu> findMenusByRole(String roleCode) throws DataAccessException;
	
}




