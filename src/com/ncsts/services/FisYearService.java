package com.ncsts.services;




import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.domain.FisYear;





public interface FisYearService extends GenericService<FisYear, String>{

	public List<FisYear> getAllYearPeriod(Long fisYearId) throws DataAccessException;
	public List<FisYear> getAllPeriodDesc(Long fisYearId) throws DataAccessException;
	public List<FisYear> getSelectedPeriod(String ID) throws DataAccessException;
	public List<FisYear> getAllPeriod() throws DataAccessException;
	public boolean getCloseFlag(Long fisYearId);
	public void updateFisYear(Long fisYear, List<FisYear> fisYearList) throws DataAccessException;
}
