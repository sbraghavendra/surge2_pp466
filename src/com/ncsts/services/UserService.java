package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.User;

/**
 * 
 * @author Paul Govindan
 *
 */
public interface UserService {
	
	public abstract boolean validateUser(String user, String password);
	
    public abstract List<User> findAllUsers() throws DataAccessException;
    
	public User findById(String id) throws DataAccessException;    
    
	public void remove(String id) throws DataAccessException;    
    
	public void saveOrUpdate (User user) throws DataAccessException;     
    
	public String persist (User user) throws DataAccessException;    
	
	public void updateUserLogoff(String userCode) throws DataAccessException; 
	
	public String getUserLogoff(String userCode) throws DataAccessException;
}



