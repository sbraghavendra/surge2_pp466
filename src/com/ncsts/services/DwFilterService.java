package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DwFilter;

public interface DwFilterService {
	public List<DwFilter> getFilterNamesByUserCode(String windowName, String userCode) throws DataAccessException;
	public List<String> getFilterNamesPerUserCode(String windowName, String userCode) throws DataAccessException;
	public List<DwFilter> getColumnsByFilterNameandUser(String windowName, String userCode, String filterName) throws DataAccessException;
	public void deleteColumnsByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException;
	public void updateColumnsByFilterNameandUser(String windowName, String filterName, String userCode, List<DwFilter> dwFilters) throws DataAccessException;
	public void addColumnsByFilterName(String windowName, String filterName, List<DwFilter> dwFilters) throws DataAccessException;
	public boolean isFilterNameExist(String windowName, String filterName) throws DataAccessException;
	public String getUserByFilterNameandUserCode(String windowName, String filterName, String userCode) throws DataAccessException;
}