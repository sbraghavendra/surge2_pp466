package com.ncsts.services;

import java.util.List;

import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.BatchDTO;

public interface ImportMapService {
	
	/**
	 * This saves the date imported from the file reader
	 * @param list
	 * @return error list
	 */
	public List<String> saveFileData(List<TransactionDetail> list); 

	/**
	 * Data for the main page
	 * @return
	 */
	public List<BatchDTO> getImportMapData(String batchTypeCode);
	
	/**
	 * After file has been uplaoded into the server, this method will read the file
	 * and push the data to database
	 *  
	 * @param path
	 * @return
	 */
	public List<String> importFileData(String path);
	
	/**
	 * This service method will invoke helper classes to read file
	 * @param path
	 * @return
	 */
	//public List readFile(String path);
	
	public List<BatchDTO> getAOLBatchRecords(String entryTimestamp, String statusCode);
}
