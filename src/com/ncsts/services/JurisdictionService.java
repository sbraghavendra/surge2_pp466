package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;

public interface JurisdictionService extends GenericService<Jurisdiction, Long> {
	
	/**
	 * To get the rich faces table for tax rate.
	 * @return
	 */
	public List<JurisdictionTaxrate> getAllJurisdictionTaxrate(JurisdictionTaxrate jurisdictionTaxrate, int count);

	public Jurisdiction copyToNew(Long jurisdictionId);
	public Long partialJurisdictionCount(Jurisdiction exampleInstance);

	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance);
	public List<Jurisdiction> searchByExample(Jurisdiction exampleInstance, boolean exact);
	public List<JurisdictionTaxrate> findByIdAndDate(JurisdictionTaxrate jurisdictionTaxrate)throws DataAccessException;
	public List<JurisdictionTaxrate> findByIdAndMeasureType(JurisdictionTaxrate jurisdictionTaxrate) throws DataAccessException;
	public List<Jurisdiction> findByTaxCodeStateCode(String stateCode) throws DataAccessException;
	
	public List<String> findTaxCodeCounty(String countryCode, String stateCode) throws DataAccessException;
	public List<String> findTaxCodeCity(String countryCode, String stateCode) throws DataAccessException;
	public List<Object[]> findNexusJurisdiction(Long entityId, String countryCode, String stateCode, String nexusIndCode, String type) throws DataAccessException;
	public List<String> findCountryStateStj(String countryCode, String stateCode) throws DataAccessException;
	public List<Object[]> findRegistrationJurisdiction(Long entityId, String countryCode, String stateCode, String nexusIndCode, String type) throws DataAccessException;
	public List<Object[]> findSitusJurisdiction(String countryCode, String stateCode, String transactionTypeCode, String ratetypeCode, String methodDeliveryCode, String nexusIndCode, String type) throws DataAccessException;
	public List<Object[]> findJurisdictionForCountryState(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException;
	public List<Object[]> findJurisdictionForCountyCityStj(Long id, String geocode, String country, String state, String county, String city, String zip, String stj, String area) throws DataAccessException;
	public List<String> findLocalCounty(String country, String state, String city, String stj) throws DataAccessException;
	public List<String> findLocalCity(String country, String state, String county, String stj) throws DataAccessException;
	public List<String> findLocalStj(String country, String state, String county, String city) throws DataAccessException;
	public List<String> checkJurisdictionNotUsed(Long jurisdictionId);
}
