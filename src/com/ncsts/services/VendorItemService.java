package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.VendorItem;

public interface VendorItemService extends GenericService<VendorItem,Long> {
	
    public abstract VendorItem findById(Long vendorId) throws DataAccessException;	
    
    public abstract Long count(Long entityId) throws DataAccessException;
    
    public VendorItem findByCode(String vendorCode) throws DataAccessException;
    public VendorItem findByName(String vendorName) throws DataAccessException;

    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(VendorItem vendorItem) throws DataAccessException;
    
    public boolean isAllowToDeleteEntity(Long entityId) throws DataAccessException;
    public void copyEntityComponents(Long fromEntityId, Long toEntityId, String selectedComponents) throws DataAccessException;
    public void deleteVendor(Long vendorId) throws DataAccessException;
    public void disableVendor(Long vendorId) throws DataAccessException;
    public boolean isVendorUsed(String vendorCode, String vendorName) throws DataAccessException;
    
    public abstract List<VendorItem> getAllRecords(VendorItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause) throws DataAccessException;
	public Long count(VendorItem exampleInstance, String whereClause);
	public boolean isNexusDefinitionsUsed(Long vendorID) throws DataAccessException;
}




