package com.ncsts.services;

import java.util.Date;
import java.util.List;

import com.ncsts.domain.MatrixDriver;
import com.ncsts.domain.Matrix;

public interface MatrixService<M extends Matrix> extends GenericService<M,Long> {
	
	public boolean matrixExists(M matrix, boolean checkDate);
	public List<MatrixDriver> getUniqueDrivers(M filter);
	public Date minEffectiveDate(M matrix);
	public Long matrixEffectiveDateCount(M matrix);
}
