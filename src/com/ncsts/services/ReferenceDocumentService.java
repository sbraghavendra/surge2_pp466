package com.ncsts.services;


import java.util.List;

import com.ncsts.domain.ReferenceDocument;

/**
 * 
 * @author Anand
 *
 */

public interface ReferenceDocumentService extends GenericService<ReferenceDocument,String> {
	public List<ReferenceDocument> searchForRefDoc(ReferenceDocument refDoc);
	public List<ReferenceDocument> findAll();
	public void update(ReferenceDocument referenceDocument);
	
}
