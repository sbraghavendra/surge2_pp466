package com.ncsts.services;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;
import com.seconddecimal.billing.domain.SaleTransaction;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;


public interface TransactionDetailSaleService extends GenericService<SaleTransaction,Long> {
	
	public Long count(SaleTransaction exampleInstance);
	
	public List<SaleTransaction> getAllRecords(SaleTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	public boolean suspendAndLog(Long saleTransactionId);

	HashMap<String, HashMap<String, String>> getNexusInfo(Long saleTransactionId);
}
