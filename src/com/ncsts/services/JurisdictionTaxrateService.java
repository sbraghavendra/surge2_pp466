package com.ncsts.services;

import java.util.Date;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.JurisdictionTaxrate;

/**
 * 
 * @author Jay Jungalwala
 * 
 */
public interface JurisdictionTaxrateService extends GenericService<JurisdictionTaxrate, Long> {
	
	public Date findByDate(Long jurisdictionId) throws DataAccessException ;
	public JurisdictionTaxrate findByIdInitialized(final Long id) throws DataAccessException;
	public long getTaxrateUsedCount(Long taxrateId) throws DataAccessException;
}
