package com.ncsts.services;

import java.util.Date;
import java.util.List;
import org.springframework.dao.DataAccessException;
import com.ncsts.domain.RegistrationDetail;

public interface RegistrationDetailService extends GenericService<RegistrationDetail, Long> {
	public List<RegistrationDetail> findAllRegistrationDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFlag) 
		throws DataAccessException;
	
}
