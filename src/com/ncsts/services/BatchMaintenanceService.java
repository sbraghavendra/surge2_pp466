package com.ncsts.services;

import java.util.Hashtable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.dto.BatchDTO;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.dto.ErrorLogDTO;

public interface BatchMaintenanceService extends GenericService<BatchMaintenance, Long> {

	public abstract void update(BatchMaintenance batchMaintenance);
	public abstract void remove(Long batchId);	
	public abstract BatchMaintenanceDTO save(BatchMaintenanceDTO batchMaintenanceDTO); // method added by anand
	public abstract List<BatchMaintenanceDTO> getBatchMaintenanceData(BatchMaintenanceDTO batchMaintenanceDTO,
																	  Hashtable<String,String> batchType, 
																	  Hashtable<String,String> batchStatus);	

	public abstract List<BatchMaintenanceDTO> getAllBatchMaintenanceData();
	public abstract List<BatchMaintenance> getBatchMaintenanceByBatchStatusCode(String batchStatusCode);
	public abstract List<BatchMaintenance> getBatchMaintenanceByBatchTypeCode(String batchTypeCode);
	public List<BatchMaintenanceDTO> getAllBatchesByBatchTypeCode(String batchTypeCode);//this method added by anand
	public List<BatchMaintenance> findFlaggedProcessTaxRateBatches() throws DataAccessException;
//	public abstract void processTaxRateUpdateBatch(Long batchId); // this method added by anand
//    public List<ErrorLogDTO> getErrorLog(Long batchId);
//    public List<BatchErrorLog> getErrorLogDetails(Long batchId);
	public abstract BatchErrorLog save(BatchErrorLog error) throws DataAccessException;
  public List<ErrorLogDTO> getBatchProcesses(Long batchId);
	public List<ErrorLogDTO> getBatchErrors(ErrorLogDTO batch);
	void saveBatchMetadata(BatchMetadata batchMetadata);
	public abstract BatchMaintenance getBatchMaintananceData(long batchId) throws DataAccessException;//Method Added by Krishna
    public List<BatchMaintenanceDTO> getAllBatchesByBatchTypeCode(String codeTypeCode,
        	List<String> batchtypeCodeCode, List<String> batchstatusCodeCode, List<String> errorsevCodeCode);
    public BatchMetadata findBatchMetadataById(String id) throws DataAccessException;
    public List<BatchMaintenance> findByStatusUpdateUser(String batchTypeCode, String batchStatusCode, String statusUpdateUserId) throws DataAccessException;
    public void createNewBatchGL(BatchMaintenance batchMaintenance,String glDate);
	List<BatchMetadata> loadValidMetadata(boolean isPurchasingSelected);
	public boolean hasExtractedTransactions(List<Long> ids);
	public boolean checkRunningBatches();

  }
