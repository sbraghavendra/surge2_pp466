package com.ncsts.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.dto.ListCodesDTO;
/**
 * 
 * @author Muneer
 *
 */
public interface ListCodesService {
	public abstract List<ListCodes> getListCodesByCodeTypeCode(String codeTypeCode);
	public abstract List<ListCodesDTO> getListCodesByCodeCode(String codeCode);//added by charishma
	public abstract List<ListCodes> getAllListCodes() throws DataAccessException;
	public  abstract List<ListCodesDTO> getListCodesByCodeTypeCodeAsDTO(String codeTypeCode) throws DataAccessException;
	
	public abstract ListCodes findByPK(ListCodesPK pk) throws DataAccessException;
	
	public abstract ListCodesDTO addListCodeRecord(ListCodesDTO instance) throws DataAccessException;
    public abstract void update(ListCodesDTO listCodesDTO) throws DataAccessException;
    public abstract void delete(ListCodesPK pk) throws DataAccessException;
    public List<ListCodes> getListCodesByTypeAndCode(String codeTypeCode, String code) throws DataAccessException;
    
    public ListCodes getListCode(String codeTypeCode, String codeCode, String description) throws SQLException;
}
