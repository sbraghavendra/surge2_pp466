package com.ncsts.services;

import java.util.List;
import java.util.Map;

import com.ncsts.domain.DataDefinitionColumnDrivers;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.dto.DriverNamesDTO;

public interface DriverReferenceService {
  
	public List<DriverReference> findDriverReference(String transDtlColName, String driverValue, String driverDesc, String userValue, int count);
	
	public List<DriverReference> findByTransactionDetailColumnName(String columnName, int count) throws DataAccessException ;	
	
	public List<DriverReference> findByDriverName(String drvValue, String columnName ,int count) throws DataAccessException; 
	
	public List<String> getDriverNamesByDrvCodeMatrixCol (String drvCode, String mtrxCol) throws DataAccessException;
	
	public DriverReference getDriverReference(String transDtlColName, String driverValue);
	public Long countDriverReference(String transDtlColName, String driverValue);
	
	public List<DriverNames> getDriverNamesByCode(String driverCode) throws DataAccessException;
	
	public DriverNames findDriverForTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException ;
	
	public DriverNames findTransactionDetailColumnName(String columnName, String code, String matrixColName) throws DataAccessException ;
	
	public void saveOrUpdate(DriverReference driverRef) throws DataAccessException;
	
	public void delete(DriverReference driverRef) throws DataAccessException;
	
	public Long getDriverNamesId(String drvCode) throws DataAccessException;
	
	public void saveOrUpdateNames(DriverNamesDTO driverName) throws DataAccessException;
	
	public void deleteNames(DriverNamesDTO driverName) throws DataAccessException;
	
	public boolean getTransactionDetailUpdate(String category,String driverId) throws DataAccessException;//4356
	
	public String findCurrentTrColumnName(String category,String driverId )throws DataAccessException;
	
	public boolean allowColChange(String code, String matrixColName) throws DataAccessException;

	Map<String, DataDefinitionColumnDrivers> getValidDrivers(String driverNamesCode, String tableName);
	
}
