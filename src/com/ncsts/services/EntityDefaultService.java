package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.EntityDefault;

public interface EntityDefaultService extends GenericService<EntityDefault,Long> {
	
    public abstract Long count(Long entityId) throws DataAccessException;
    
    public abstract List<EntityDefault> findAllEntityDefaults() throws DataAccessException;
    
    public List<EntityDefault> findAllEntityDefaultsByEntityId(Long entityId) throws DataAccessException;
    
    public List<EntityDefault> findEntityDefaultsByDefaultCode(Long entityId, String defaultCode) throws DataAccessException;
    
    public Long persist(EntityDefault entityDefault)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(EntityDefault entityDefault) throws DataAccessException;
    
    public void deleteEntity(Long entityId) throws DataAccessException;
	
}




