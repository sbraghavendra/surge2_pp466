package com.ncsts.services;

import java.util.List;

import com.ncsts.domain.GlExtractMap;
import com.ncsts.dto.GlExtractMapDTO;


public interface GlExtractMapService extends MatrixService<GlExtractMap>{
	public abstract List<GlExtractMapDTO> getGlExtractMapData(GlExtractMapDTO glExtractMapDTO);
	public abstract GlExtractMapDTO save(GlExtractMapDTO glExtractMapDTO);
	public abstract void update(GlExtractMapDTO glExtractMapDTO);
	public abstract void save(GlExtractMap glExtractMap);
	public abstract void remove(Long glExtractMapId);
	public abstract GlExtractMapDTO findByGlExtractMapId(Long glExtractMapId);
	public List<GlExtractMap> findByStateCode(String state);
}
