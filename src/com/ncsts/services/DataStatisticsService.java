package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DataStatistics;
import com.ncsts.dto.DataStatisticsDTO;

/**
 * 
 * @author Muneer Basha
 *
 */
public interface DataStatisticsService {	
	
	public abstract DataStatistics findById(Long dataStatisticsId) throws DataAccessException;    
    public abstract List<DataStatisticsDTO> findAllDataStatisticsLines() throws DataAccessException;
    public abstract void update(DataStatisticsDTO dataStatisticsDTO)throws DataAccessException;;
    public abstract void remove(Long dataStatisticsId)throws DataAccessException;;
    public abstract DataStatisticsDTO addNewStatisticsLine(DataStatisticsDTO dataStatisticsDTO) throws DataAccessException;
    public abstract boolean isValidWhereClause(String whereClause);
    public abstract boolean isValidGroupByOnDrilldown(String groupByOnDrilldown);
}




