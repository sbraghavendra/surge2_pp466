package com.ncsts.services;

import java.util.List;

import com.ncsts.dao.PreferenceDAO.AdminPreference;
import com.ncsts.dao.PreferenceDAO.SystemPreference;
import com.ncsts.dao.PreferenceDAO.UserPreference;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.dto.BatchPreferenceDTO;
import com.ncsts.dto.ListCodesDTO;

public interface PreferenceService {
	public String getAdminPreference(AdminPreference preference, String defValue);
	public Long getAdminPreferenceAsLong(AdminPreference preference, Long defValue);

	public String getSystemPreference(SystemPreference preference, String defValue);
	public Long getSystemPreferenceAsLong(SystemPreference preference, Long defValue);

	public String getUserPreference(UserPreference preference, String defValue);
	public Long getUserPreferenceAsLong(UserPreference preference, Long defValue);

//	public CompanyDTO getCompany();
	public List<ListCodesDTO> getTimeZoneList();
//	public BatchPreferenceDTO getBatchPreference();
//	public boolean getTaxPartnerClient();
	public List<ListCodesDTO> getDefTransIndList();
	public List<ListCodesDTO> getDefTaxCodeList();
	public List<ListCodesDTO> getDefTaxTypeList();
	public List<ListCodesDTO> getDefRateTypeList();
//	public MatrixDTO getMatrix();
}
