package com.ncsts.services;


import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.BillTransaction;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.exception.PurchaseTransactionSuspendedException;
import com.ncsts.ws.message.MicroApiTransactionDocument;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;
import com.ncsts.ws.message.PurchaseTransactionDocument;

import static com.ncsts.common.constants.PurchaseTransactionServiceConstants.*;

import java.util.Date;
import java.util.List;

public interface PurchaseTransactionService {


    /// SURGE project methods
    void normalProcess(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum callingFromMethod) throws PurchaseTransactionProcessingException;
    void taxEstimate(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    ProcessSuspendedTransactionDocument processSuspendedTransactions(Long newSuspendRuleId, SuspendReason suspendReason, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getTransEntity(MicroApiTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    
    void getTransEntityNexus(MicroApiTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
   
    void getTransLocation(MicroApiTransaction purchaseTransaction, LocationType locnType, String overrideFlag) throws PurchaseTransactionProcessingException;
    //void getTransLocation(PurchaseTransaction purchaseTransaction, LocationType locnType, String overrideFlag) throws PurchaseTransactionProcessingException;
    
    void getTransGoodSvc(MicroApiTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    //void getTransGoodSvc(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    
    void getTransTaxCodeDtl(MicroApiTransaction trans, MicroApiTransactionDocument document) throws PurchaseTransactionProcessingException;
    void getTransTaxRates(MicroApiTransaction trans) throws PurchaseTransactionProcessingException;

    void getTransCalcAllTaxes (MicroApiTransaction trans, MicroApiTransactionDocument document) throws PurchaseTransactionProcessingException;
    
    void getPurchaseTransactionInvoiceVerification(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionInvoiceVerification(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionReverse(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;  
    void getPurchaseTransactionLoadAndReverse(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;   
    void getPurchaseTransactionLoadAndReprocess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionLoadAndReprocess(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    
    void getPurchaseTransactionEvaluateForAccrual(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionEvaluateForAccrual(MicroApiTransactionDocument document, Boolean writeFlag, LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    void billTransactionProcess(MicroApiTransactionDocument document, Boolean writeFlag,LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    void billTransactionLogSave(MicroApiTransactionDocument doc);
    
    void getTransStatus(MicroApiTransaction trans) throws PurchaseTransactionProcessingException;
    void getTransFullExemption(MicroApiTransaction trans) throws PurchaseTransactionProcessingException;

    void getSitus(MicroApiTransaction trans) throws PurchaseTransactionProcessingException;

    Jurisdiction getJurViaRatepoint(PurchaseTransaction trans, LocationType locationType) throws PurchaseTransactionSuspendedException;

    Jurisdiction getJurViaLocationMatrix(MicroApiTransaction trans, LocationType locationType) throws PurchaseTransactionSuspendedException;
    
    List<Jurisdiction> getJurisdictionViaRatepoint(String country, String state, String county, String city, String zip,    		
    		String streetAddress1, String streetAddress2, String latitude, String longitude, Date glDate) throws PurchaseTransactionSuspendedException;
    
    void getTransSuspend(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    void getTransReprocess(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag) throws PurchaseTransactionProcessingException;
    void getBillTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag) throws PurchaseTransactionProcessingException;
    void getBillTransactionProcessBatch(BatchMaintenance responseBatch, String defaultFlag, LogSourceEnum logSourceEnum) throws PurchaseTransactionProcessingException;
    void getTransVariance(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    void getTransRelease(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    void getTransReverse(PurchaseTransaction purchaseTransaction) throws PurchaseTransactionProcessingException;
    void getTransVendorNexus(MicroApiTransaction purchaseTransaction, Boolean directPayPermitBoolean) throws PurchaseTransactionProcessingException;
    
    void getPurchaseTransactionReprocess(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionRelease(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionLoadAndRelease(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionSuspend(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionLoadAndSuspend(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    void getPurchaseTransactionLoad(MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;

    boolean suspendAndLog(Long transactionId, SuspendReason suspendReason, Long matrixId, Boolean suspendExtracted);
    void suspendByMatrixId(Long matrixId, MicroApiTransactionDocument document, Boolean writeFlag) throws PurchaseTransactionProcessingException;
    /// SURGE project methods />
	MicroApiTransactionDocument purchTransToMicroApiTransactionDocument(PurchaseTransaction purchTrans);
}
