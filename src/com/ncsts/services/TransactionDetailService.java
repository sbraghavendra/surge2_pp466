package com.ncsts.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.SuspendReason;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;

public interface TransactionDetailService extends GenericService<PurchaseTransaction,Long> {
	
    /** fetch processed transactions counts */
	public boolean getTaxMatrixInUse(Long taxMatrixId);
	public boolean getGSBMatrixInUse(Long taxMatrixId);//4620
	public void   updateaActiveFlagMatrixInUse(Long taxMatrixId);//4620
    public ProcessedTransactionDetail getSuspendedTaxMatrixTransactionDetails(Long taxMatrixId, Date date);
	public void suspendTaxMatrixTransactions(PurchaseTransactionService service, Long taxMatrixId, Date asOfDate, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine);
	
	public boolean getLocationMatrixInUse(Long locationMatrixId);
    public ProcessedTransactionDetail getSuspendedLocationMatrixTransactionDetails(Long locationMatrixId);
	public void suspendLocationMatrixTransactions(PurchaseTransactionService service, Long locationMatrixId, 
			boolean suspendExtracted, boolean suspendNonExtracted, boolean deleteMatrixLine);
	
	public void suspendTransaction(PurchaseTransactionService purchTransService, Long id, SuspendReason suspendReason);
	
	public void acceptHeldTransaction(Long id);
	public void appendComment(Long id, String comment);
	public void appendComment(Collection<Long> ids, String comment);
	
	public List<TransactionDetailDTO> getTransactionDetailRecordsWithDynamicWhere(String whereClause) ;
	public void updatetoHoldTransaction(PurchaseTransaction instance) ;
	
	public StringBuffer saveAs(List<PurchaseTransaction> trs) throws Exception;
    
    //public void suspendLM(Long locationMatrixId); // method added by anand to suspend a location matrix line. Location matrix line those are extracted can not be suspended.
	// 4184
	public List<PurchaseTransaction> saveRecords(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	


	
	public String getProcessSQLStatement(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	
	public String getProcessSQLCriteria(PurchaseTransaction exampleInstance, 
			Date effectiveDate, Date expirationDate, 
			Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
			Boolean glExtractFlagIsNull, String advancedFilter,
			OrderBy orderBy, int firstRow, int maxResults);
	
	public String getStatisticsSQLStatement(PurchaseTransaction exampleInstance, 
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds);
	
	//3987
	public void saveColumn(TransactionHeader transactionHeader);
	public void saveColumns(String userCode, String columnType, String windowName, String dataWindowName, List<TransactionHeader> transactionHeaders);
	
	public List<String> getColumnHeaders(String userCode, String columnType, String windowName, String dataWindowName);
	
	public void removeColumns(String userCode, String columnType, String windowName, String dataWindowName);
	
	public long getTransactionCountByTaxcodeDetail(Long taxcodeDetailId);
	
	public void updateTransaction(PurchaseTransaction updatedTransaction);
}
