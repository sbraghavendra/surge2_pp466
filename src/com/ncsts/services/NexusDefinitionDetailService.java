package com.ncsts.services;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.NexusDefinitionDetail;

public interface NexusDefinitionDetailService extends GenericService<NexusDefinitionDetail, Long> {
	public List<NexusDefinitionDetail> findAllNexusDefinitionDetails(Long entityId, String countryCode, String stateCode, String county, String city, String stj, boolean activeFlag) 
		throws DataAccessException;
	public void bulkExpireNexusDefinitionDetail(String countryCode, String stateCode, Date expirationDate) throws DataAccessException;
	public void bulkExpireRegistrationDetail(String countryCode, String stateCode, Date expirationDate) throws DataAccessException ;
	public boolean isRegistrationDetailExist(Long nexusDefId, String nexusTypeCode, Date effectiveDate) throws DataAccessException ;
	public boolean isNexusDefinitionExist(Long entityId, String countryCode, String stateCode, String county, String city, String stj, Date effectiveDate, String type) throws DataAccessException;
	public boolean isNexusDefinitionActive(Long entityId, String countryCode, String stateCode, String county, String city, String stj) throws DataAccessException;
}
