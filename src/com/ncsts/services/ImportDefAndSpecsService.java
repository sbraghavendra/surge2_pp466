package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.dto.UserDTO;

public interface ImportDefAndSpecsService {
	public List<ImportDefDTO> getAllImportDefinition();
	public List<ImportDefDTO> getAllImportDefinition(String type);
	
	
	public abstract ImportDefDTO addImportDefRecord(ImportDefDTO instance) throws DataAccessException;
	public abstract void update(ImportDefDTO importDefDTO) throws DataAccessException;
	public abstract void delete(String importDefCode) throws DataAccessException;
	public abstract ImportDefinition find(String importDefCode) throws DataAccessException;

     // Following Methods are related to ImportSpec methods
    public abstract List<ImportSpecDTO> getAllImportSpec() throws DataAccessException ;
    public abstract List<ImportSpecDTO> getImportSpecByDefCode(String defCode) throws DataAccessException ;
    public abstract ImportSpecDTO addImportSpecRecord(ImportSpecDTO instance) throws DataAccessException;
    public abstract void updateImportSpec(ImportSpecDTO importSpecDTO) throws DataAccessException;
    public abstract void deleteImportSpec(ImportSpecPK importSpecPK) throws DataAccessException;
    public abstract ImportSpec getImportSpec(ImportSpecPK importSpecPK) throws DataAccessException;
    
    // Methods Related to Import Spec Details
    
    public abstract List<UserDTO> getImportSpecUserBySpec(ImportSpecDTO instanceDTO) throws DataAccessException;
    public abstract List<UserDTO> getImportSpecUserBySpecPK(ImportSpecPK importSpecPK) throws DataAccessException;
    
    public abstract void updateImportSpecUserBySpec(ImportSpecDTO importSpecDTO, List<String> userCodeList) throws DataAccessException;

    public abstract List<ImportSpec> getAllForUserByType(String type) throws DataAccessException;
	
    public abstract List<ImportDefDTO> getDefinitionsByConnection(String connectionCode) throws DataAccessException;    
    public abstract List<ListCodes> getConnectionTypeCodes();
    
    public abstract void addConnection(ImportConnection importConnection) throws DataAccessException;
    public abstract void updateConnection(ImportConnection importConnection) throws DataAccessException;
    public abstract void deleteConnection(String importConnectionCode) throws DataAccessException;
    
    public List<ImportConnection> getAllImportConnection() throws DataAccessException;
    public List<ImportConnection> getConnectionsByCode(String importConnectionCode) throws DataAccessException;
    public List<String> getImportDefinitionCodes(String tablename) throws DataAccessException;
}
