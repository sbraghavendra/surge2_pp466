package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.EntityLocnSet;

public interface EntityLocnSetService extends GenericService<EntityLocnSet,Long> {

    public abstract Long count(Long entityId) throws DataAccessException;
    
    public abstract List<EntityLocnSet> findAllEntityLocnSets() throws DataAccessException;
    
    public List<EntityLocnSet> findAllEntityLocnSetsByEntityId(Long entityId) throws DataAccessException;
    
    public Long persist(EntityLocnSet entityLocnSet)throws DataAccessException;
    
    public void remove(Long id) throws DataAccessException;
    
    public void saveOrUpdate(EntityLocnSet entityLocnSet) throws DataAccessException;
    
    public void deleteEntity(Long entityId) throws DataAccessException;
    
    public EntityLocnSet getEntityLocationforView(Long transId) throws DataAccessException;
	
}




