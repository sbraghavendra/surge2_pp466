package com.ncsts.services;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.PurchaseTransaction;

public interface LocationMatrixService extends MatrixService<LocationMatrix> {
	public List<LocationMatrix> getDetailRecords(LocationMatrix locationMatrix, OrderBy orderBy);
	public List<LocationMatrix> getMatchingLocationMatrixLines(PurchaseTransaction exampleInstance);

	
}
