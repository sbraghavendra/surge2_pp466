package com.ncsts.services;

import com.ncsts.domain.NexusDefinitionLog;

public interface NexusDefinitionLogService extends GenericService<NexusDefinitionLog, Long> {

}
