package com.ncsts.services;


import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Procrule;
import com.ncsts.domain.ProcruleDetail;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;
import com.ncsts.domain.ProruleAudit;

public interface ProcruleService extends GenericService<Procrule, Long> {
	
	public void addProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public void updateProcruleDetailList(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public void deleteProcrule(Procrule updateProcrule) throws DataAccessException;
	public void deleteProcruleDetail(Procrule updateProcrule, List<ProcruleDetail> updateProcruleEntityList) throws DataAccessException;
	public List<ProcruleDetail> findProcruleDetail(Long procruleId, Date effectiveDate) throws DataAccessException;
	public List<String> findProcruleDetailDistinctDate(Long procruleId) throws DataAccessException;
	public List<Procrule> getAllProcrule() throws DataAccessException;
	public List<Procrule> getProcrule(String moduleCode, String ruletypeCode) throws DataAccessException;
	public List<ProruleAudit> getAllProcAuditRecords(String sessionId) throws DataAccessException;
	public void reorderProcruleList(List<Procrule> updateProcruleList) throws DataAccessException;
	public boolean getIsRuleNameExist(String procruleName, String moduleCode) throws DataAccessException;
	public List<ProruleAudit> getProcRuleRecords(String sessionId) throws DataAccessException;
	public boolean getIsRuleNameDateExist(String procruleName, String moduleCod, Date effectiveDate) throws DataAccessException;
	public boolean isAllowDeleteRules(Long procruleId) throws DataAccessException;
	public boolean isAllowDeleteDefinitions(Long procruleId, Date effectiveDate) throws DataAccessException;
}
