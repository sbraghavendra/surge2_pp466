package com.ncsts.services;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.ProcessedTransactionDetail;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.TransactionHeader;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.domain.BillTransaction;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;


public interface TransactionDetailBillService extends GenericService<BillTransaction,Long> {
	public Long count(BillTransaction exampleInstance);
	
	public List<BillTransaction> getAllRecords(BillTransaction exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	
	HashMap<String, HashMap<String, String>> getNexusInfo(Long billTransactionId);
	public HashMap<String, String> getTaxRateInfo(Long billtransId);
}
