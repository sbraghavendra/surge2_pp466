package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.dto.TaxCodeStateDTO;

/**
 * 
 * @author Anand
 *
 */

public interface TaxCodeStateService {
	public List<TaxCodeStateDTO> getActiveTaxCodeState(); 
	public List<TaxCodeStateDTO> findByTaxCode(String taxCodeType, String taxcodeCode);
	public List<TaxCodeStateDTO> findByStateName(String stateName);
	public List<TaxCodeStateDTO> findByStateCode(String stateCode);
	public void batchQuickUpdate(List<TaxCodeStateDTO> quickUpdateList);
	public boolean isCountryUsed(String countryName);
	public void deleteCountry(TaxCodeStateDTO instance);
	public void updateCountry(TaxCodeStateDTO instance);
	public List<TaxCodeStateDTO> findByCountryName(String countryName);

	public List<TaxCodeStateDTO> getAllTaxCodeStateDTO();
	public TaxCodeStateDTO findByPK(TaxCodeStatePK pk); 
	
	public TaxCodeStateDTO save(TaxCodeStateDTO instance) throws DataAccessException;
    public void update(TaxCodeStateDTO instance) throws DataAccessException;
    public void remove(TaxCodeStatePK pk) throws DataAccessException;
    
    public List<TaxCodeStateDTO> findByCountryStateActive(String country, String state, String activeFlag);

}
