package com.ncsts.services;


import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import com.ncsts.domain.Proctable;
import com.ncsts.domain.ProctableDetail;

public interface ProctableService extends GenericService<Proctable, Long> {
	
	public void addProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException;
	public void updateProctableDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableEntityList) throws DataAccessException;
	public void deleteProctableDetail(ProctableDetail aProctableDetail) throws DataAccessException;
	public void deleteProctable(Proctable updateProctable) throws DataAccessException;
	public List<ProctableDetail> findProctableDetail(Long proctableId) throws DataAccessException;
	public ProctableDetail findProctableDetailById(Long proctableId) throws DataAccessException;
	public List<Proctable> getAllProctable() throws DataAccessException;
	public void reorderProctableList(List<Proctable> updateProctableList) throws DataAccessException;
	public void populateProcruleDetailList(Proctable updateProctable, List<ProctableDetail> updateProctableDetailList) throws DataAccessException;
	public void updateProctableDetail(Proctable aProctable, ArrayList changedArrayList) throws DataAccessException;
	public void addProctableDetail(Proctable updateProctable, ProctableDetail proctableDetail) throws DataAccessException;
}
