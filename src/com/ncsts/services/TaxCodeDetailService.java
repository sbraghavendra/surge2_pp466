package com.ncsts.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.dto.TaxCodeDetailDTO;

public interface TaxCodeDetailService extends GenericService<TaxCodeDetail,Long> {

    public List<String[]> findTaxCodes();
    public List<String> findTaxCodeTypeForState(String stateCode);
    
    public List<String> findTaxCodeCounty(String countryCode, String stateCode);
    public List<String> findTaxCodeCity(String countryCode, String stateCode);
    public List<String> findTaxCodeStj(String countryCode, String stateCode);
    public List<TaxCodeDetail> findTaxCodeDetailListByCountrCity(String countryCode, String stateCode, String taxcodeCode, String taxcodeCounty, String taxcodeCity, String description);
    
    public List<TaxCodeDetail> findTaxCodeCode(String stateCode, String taxCodeType);
    public List<TaxCodeDetail> findTaxCodeDetailList(String stateCoountry, String stateCode, String taxCodeType, String taxcodeCode);
    public List<TaxCodeDetail> findTaxCodeDetailListByTaxCode(String taxcodeCode);
    public List<TaxCode> findTaxCodeListByTaxCode(String taxcodeCode);

    public TaxCode findTaxCode(TaxCodePK id);
    public List<TaxCode> getAllTaxCode();
    public List<TaxCode> findTaxCodeList(String stateCode, String taxCodeType, String pattern);
    public List<TaxCode> findTaxCodeListByCountry(String countryCode, String stateCode, String taxCodeType, String pattern);
    public List<TaxCode> findByTaxCodeCode(String taxCodeTypeCode);
    public List<TaxCode> findTaxCodeListByStateCode(String stateCode, String taxCodeType);

    public void removeTaxCode(TaxCode taxCode) throws IllegalAccessException;
    public boolean isTaxCodeUsed(TaxCode taxCode) throws DataAccessException;
    public void updateDetail(TaxCode taxCode, Set<String> states, Map<String,Long> stateJurisdictionMap);
    public void updateBackdate(TaxCodeDetail taxCodeDetail)throws DataAccessException;
    public void updateDetail(TaxCodeDetail taxCodeDetail) throws DataAccessException;

    public List<ReferenceDocument> getAllDocumentReferences(); // taxability code use case
    public List<ReferenceDocument> getDocumentReferencesByTaxCodeId(Long id);
    public List<ReferenceDocument> getDocumentReferencesByTypeAndCode(String refType, String code);
    public List<ReferenceDetail> getDocumentReferences(String taxCodeType, String taxCode, String taxStateCode);
    public List<ReferenceDetail> getReferenceDetails(Long taxcodeDetailId);

    public void addDocumentReference(ReferenceDetail detail);
    public void removeDocumentReference(ReferenceDetail detail);
   
    public List<ListCodes> findListCodeForTaxCode(String taxCode);
    public List<ListCodes> findListCodeForState(String stateCode);
    public void update(TaxCodeDetail taxCodeDetail)throws DataAccessException;

    public List<CCHCode> getAllCCHCode();
    public List<CCHTaxMatrix> getAllCCHGroup();
    public List<CCHTaxMatrix> getAllCCHGroupItems(String groupCode);

    public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId);
    public List<TaxCodeDetailDTO> getTaxcodeDetailsByCountry(String taxCodeCountry, String taxCodeState,String taxCodeType,String taxCode,String description,Long detailId);
    public List<TaxCodeDetailDTO> getTaxcodeDetails(String taxcodeCode, String taxCodeCountry, String taxCodeState, String taxCodeCounty, String taxCodeCity, boolean showLocalTaxability, boolean activeFlag, boolean definedFlag);
    public List<TaxCodeDetailDTO> getTaxcodeDetailsJurisList(String taxcodeCode, String taxCodeCountry, String taxCodeState, String taxCodeCounty, String taxCodeCity, String taxCodeStj) throws DataAccessException;
    
    public List<TaxCodeDetailDTO> getTaxcodeRules(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel);
    public long getRulesusedCount(Long ruleId);
    public long getTaxCodeRulesCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String stj, Date effectiveDate, String jurLevel);
    public long getTaxCodeJurisdInfoCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String stj,String jurLevel);
    public long getAllocBucketCount(Long taxcodeDetailId, String allocBucket);
    public Date minEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel);
    public Long matrixEffectiveDateCount(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj, String jurLevel, Date backEffectiveDate);
    public Date getRuleEffectiveDate(String taxcodeCode, String taxcodeCountryCode, String taxcodeStateCode, String taxcodeCounty, String taxcodeCity, String transInd, String suspendInd);
    public Long[] addRulesFromTransaction(TaxCodeDetailDTO stateRule, TaxCodeDetailDTO countyRule, TaxCodeDetailDTO cityRule) throws Exception;

}
