package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;

/**
 * 
 * @author Paul Govindan
 *
 */
public interface OptionService {
	
    Option findByPK(OptionCodePK codePK) throws DataAccessException;

	String findByPKSafe(OptionCodePK codePK) throws DataAccessException;
    
    List<Option> findAllOptions() throws DataAccessException;
    
	void remove(OptionCodePK codePK) throws DataAccessException;
    
	void saveOrUpdate (Option option) throws DataAccessException;
    
	String persist (Option option) throws DataAccessException;
	
	List<Option> getSystemPreferences() throws DataAccessException;
	
	List<Option> getAdminPreferences() throws DataAccessException;
	
	List<Option> getUserPreferences() throws DataAccessException;
	
	String getUserOption(String user, String name) throws DataAccessException;
	boolean isAolEnabled(String user) throws DataAccessException;
}
