package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.VendorNexus;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.view.bean.JurisdictionItem;

public interface VendorNexusService extends GenericService<VendorNexus, Long> {
	public VendorNexus findOneByExample(VendorNexus exampleInstance) throws DataAccessException;
	public boolean removeVendorNexus(List<VendorNexus> vendorNexasArray) throws DataAccessException;
	public boolean addVendorNexus(List<VendorNexus> vendorNexasArray, List<VendorNexusDtl> vendorNexasDetailArray) throws DataAccessException;
	public boolean isIndependentCounty(Long entityId, String geocode, String country, String state, String county, String city, String zip, String stj) throws DataAccessException;
	public boolean isVendorNexusUsed(Long vendorNexusID) throws DataAccessException;
	public boolean isVendorNexusDetailUsed(Long vendorNexusDetailID) throws DataAccessException;
}
