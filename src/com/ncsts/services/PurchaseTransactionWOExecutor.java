package com.ncsts.services;


import com.ncsts.task.PurchaseTransactionTask;
import com.ncsts.task.PurchaseTransactionWorkOrder;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.ProcessStep;

import java.util.LinkedList;

public class PurchaseTransactionWOExecutor {
    public void execute(PurchaseTransactionWorkOrder workOrder)  {
        LinkedList<PurchaseTransactionTask> tasks =  workOrder.getTasks();
        while (!tasks.isEmpty()) {
            PurchaseTransactionTask task = tasks.poll();
            PurchaseTransaction transaction = task.getPurchaseTransaction();
            ProcessStep step = task.getProcessStep();
            task.markStart();
            switch (step) {
                case init:
                    break;
                case transEntity:
                    break;
                case transLocation:
                    break;
                case transGoodSvc:
                    break;
                case transTaxCodeDtl:
                    break;
                case transTaxRates:
                    break;
                case transCalcAllTaxes:
                    break;
                case transStatus:
                    break;
                case transSitus:
                    break;
                case transVendorNexus:
                    break;
                /*
                * init, transEntity, transLocation,
        transGoodSvc, transTaxCodeDtl, transTaxRates, transCalcAllTaxes, transStatus, transSitus, transVendorNexus
                * */
            }
        }

    }
}
