package com.ncsts.services;

import java.util.List;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.TaxAllocationMatrix;

public interface TaxAllocationMatrixService extends MatrixService<TaxAllocationMatrix> {
	public TaxAllocationMatrix findByIdWithDetails(Long id);
	public List<TaxAllocationMatrix> getDetailRecords(TaxAllocationMatrix taxAllocMatrix, OrderBy orderBy);
	public boolean isTaxAllocationUsedinTransaction(Long taxAllocationMatrixId);
}
