package com.ncsts.services;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.dao.DataAccessException;

public interface CallGLExtractService {
	public abstract String callGlExtractProc(String where_clause,
			String file_name, int execution_mode, int return_code);

	public BigDecimal getTransactionCount(String GLdate) throws DataAccessException;

	public BigDecimal getRecordCount(String GLdate) throws DataAccessException;

	public BigDecimal getTaxAmountCount(String GLdate) throws DataAccessException;
}
