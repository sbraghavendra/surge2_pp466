package com.ncsts.services;


import com.ncsts.domain.CCHCode;
import com.ncsts.domain.CCHCodePK;

/**
 * 
 * @author Anand
 *
 */

public interface CCHCodeService extends GenericService<CCHCode,CCHCodePK> {
}
