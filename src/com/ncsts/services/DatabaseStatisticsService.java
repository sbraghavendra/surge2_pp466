package com.ncsts.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ncsts.domain.TempStatistics;
import com.ncsts.dto.DatabaseTransactionStatisticsDTO;
import com.ncsts.dto.LocationMatrixTransactionStatiticsDTO;
import com.ncsts.dto.TaxMatrixTransactionStatiticsDTO;

public interface DatabaseStatisticsService {
	
	/**
	 * Will return all the records for database statistics Transactions Tab
	 * @return
	 */
	public List<DatabaseTransactionStatisticsDTO> getTransactionsStatistics(String whereClauseforSearch)throws SQLException;
	public List<DatabaseTransactionStatisticsDTO> getTransactionsStatistics();
	
	
	/**
	 * Will return all the records for database statistics TaxMatrix tab
	 * @return
	 */
	public TaxMatrixTransactionStatiticsDTO getTaxMatrixStatistics(String andClauseforSearch);
	
	/**
	 * Will return all the records for database statistics LocationMatrix tab
	 * @return
	 */
	public LocationMatrixTransactionStatiticsDTO getLocationMatrixStatistics(String andClauseforSearch);
	
	public List<DatabaseTransactionStatisticsDTO> getStatisticsOnGroupBy(String groupBy, String whereClause);
	
	public StringBuffer saveStatisticsOnGroupBy(String groupBy, String whereClause);
	
	public StringBuffer saveAs(List<TempStatistics> tempStatisticsList) throws IOException;
}
