package com.ncsts.services;

import java.util.Hashtable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.dto.ErrorLogDTO;

public interface TaxHolidayService extends GenericService<TaxHoliday, String> {

	public abstract void update(TaxHoliday taxHoliday);
	public abstract void remove(String taxHolidayCode);	
	public abstract List<TaxHoliday> getAllTaxHolidayData();

	public abstract TaxHoliday getTaxHolidayData(String taxHolidayCode) throws DataAccessException;
	
	public List<TaxHolidayDetail> findTaxHolidayDetailFromTaxCode(String country, String state, String taxCode, String description, String county, String city, String stj) throws DataAccessException;
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String taxCode, String county, String city, String stj) throws DataAccessException;
	public List<TaxHolidayDetail> findTaxHolidayDetailFromJurisdiction(String country, String state, String county, String city) throws DataAccessException;
	public List<TaxHolidayDetail> findTaxHolidayDetailByTaxHolidayCode(String taxHolidayCode, String exceptInd, String taxCode) throws DataAccessException;
	public Long findTaxHolidayDetailCount(String taxHolidayCode) throws DataAccessException;
	public void executeTaxHolidayDetails(String taxHolidayCode) throws DataAccessException;
	public void addTaxHolidayDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, boolean copyMapFlag) throws DataAccessException;
	public void updateTaxHolidayExceptionDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, TaxHolidayDetail selectedTaxCodeDetail) throws DataAccessException;
	public void updateTaxHolidayMapDetails(TaxHoliday taxHoliday, List<TaxHolidayDetail> taxHolidayDetailList, String exceptInd, String taxCode, List<TaxHolidayDetail> unselectedList) throws DataAccessException;
	public void updateTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;
	public void deleteTaxHoliday(TaxHoliday taxHoliday) throws DataAccessException;
}
