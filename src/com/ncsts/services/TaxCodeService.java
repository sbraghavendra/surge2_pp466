package com.ncsts.services;


import java.util.List;

import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;

/**
 * 
 * @author Anand
 *
 */

public interface TaxCodeService extends GenericService<TaxCode,TaxCodePK> {
	public List<TaxCode> findTaxCodeList(String taxcodeCode, String desc, String activeFlag);
}
