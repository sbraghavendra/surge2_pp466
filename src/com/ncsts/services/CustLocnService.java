package com.ncsts.services;

import java.util.Hashtable;
import java.util.List;

import com.ncsts.common.helper.OrderBy;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.TaxHolidayDetail;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.dto.TaxHolidayDTO;
import com.ncsts.dto.ErrorLogDTO;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

public interface CustLocnService extends GenericService<CustLocn, Long> {
	
	public List<CustLocn> findAllCustLocn(CustLocn exampleInstance) throws DataAccessException;
	public CustLocn getCustLocnByCustId(Long custId, String custLocnCode) throws DataAccessException;
}
