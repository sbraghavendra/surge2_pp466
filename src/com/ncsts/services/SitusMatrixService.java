package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.SitusMatrix;

public interface SitusMatrixService extends GenericService<SitusMatrix, Long> {
	public List<SitusMatrix> findAllSitusMatrixDetailBySitusMatrixId(Long situsMatrixId) throws DataAccessException;
	public void addSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void updateSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void inactiveSitusMatrix(SitusMatrix situsMatrix) throws DataAccessException;
	public void deleteSitusMatrix(Long situsMatrixId) throws DataAccessException;
	
	public List<SitusMatrix> getAllRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults) throws DataAccessException;
	public List<SitusMatrix> getAllDetailRecords(SitusMatrix exampleInstance, String display) throws DataAccessException;
	
	public List<SitusMatrix> getMasterRulesRecords(SitusMatrix exampleInstance, OrderBy orderBy, int firstRow, int maxResults);
	public List<SitusMatrix> getMasterRulesDetailRecords(SitusMatrix situsMatrix);
	public Long masterRulesCount(SitusMatrix exampleInstance);
	
	public SitusMatrix find(SitusMatrix exampleInstance);
	public List<SitusMatrix> findByExample(SitusMatrix exampleInstance);
}
