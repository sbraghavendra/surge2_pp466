package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.RoleMenu;

public interface RoleUserMenuService {
	
	public abstract List<RoleMenu> findRoleMenuByRoleCode(String roleCode) throws DataAccessException;
	
	public void saveOrUpdate (RoleMenu roleMenu) throws DataAccessException;
	
	public void deleteRoleMenu (RoleMenu roleMenu) throws DataAccessException;
}
