package com.ncsts.services;

/**
 * @author AChen
 * 
 */

import java.util.Map;

import org.springframework.dao.DataAccessException;

public interface UtilitiesMenuService {
	public abstract String runMasterControlProgram() throws DataAccessException;
	public abstract String resumeBusService(Long jobNum, boolean resume) throws DataAccessException;
	public abstract String createBusService() throws DataAccessException;
	public Map<String, String> findBusProperty() throws DataAccessException;
	public abstract String resetDatawindowSyntax(String userCode) throws DataAccessException;
	public abstract String buildIndices(String indexParam) throws DataAccessException;
	public abstract String reweightMatrices() throws DataAccessException;
	public abstract String resetAllSequences() throws DataAccessException;
	public abstract String rebuildDriverReferences() throws DataAccessException;
	public abstract String truncateBCPTables() throws DataAccessException;
}