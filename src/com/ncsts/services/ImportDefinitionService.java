package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.dto.ImportDefDTO;

public interface ImportDefinitionService {
	public List<ImportDefDTO> getAllImportDefinition();
	public ImportDefDTO  findByPK(String importDefCode) throws DataAccessException;
	public List<ImportDefinitionDetail> getAllHeaders(String code);	
	public List<String> getImportDefinitionCodes(String tablename) throws DataAccessException;
	public List<ImportDefinitionDetail> getResetImpDefColHeadings(String code, String tablename);
	public void addImportDefinitionDetail(ImportDefinitionDetail importDefinitionDetail) throws DataAccessException;
}
