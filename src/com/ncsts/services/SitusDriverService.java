package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.SitusDriver;
import com.ncsts.domain.SitusDriverDetail;

public interface SitusDriverService extends GenericService<SitusDriver, Long> {
	public List<SitusDriverDetail> findAllSitusDriverDetailBySitusDriverId(Long situsDriverId) throws DataAccessException;
	public void addSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException;
	public void updateSitusDriver(SitusDriver situsDriver, List<SitusDriverDetail> situsDriverDetailList) throws DataAccessException;
	public void deleteSitusDriver(SitusDriver situsDriver) throws DataAccessException;
	public SitusDriver find(SitusDriver exampleInstance);
	public List<String> getDistinctSitusDriverColumn(String distinctColumn, String whereClause) throws DataAccessException;
}
