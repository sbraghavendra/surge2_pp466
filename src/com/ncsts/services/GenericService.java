package com.ncsts.services;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Idable;

public interface GenericService<T extends Idable<ID>, ID extends Serializable> {

	public Long count();
	public Long count(T exampleInstance, String... excludeProperty);
	
	public T findById(ID id) throws DataAccessException;
	
	public List<T> findById(Collection<ID> ids);
	
	public List<T> findAll();
	public List<T> findAllSorted(String sortField, boolean ascending);
	
	public List<T> findByExample(T exampleInstance, int count, String... excludeProperty);
	public List<T> findByExampleSorted(T exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty);
	
	public void save(T instance);
	public void update(T instance) throws DataAccessException;
	public void remove(T instance) throws DataAccessException;
}
