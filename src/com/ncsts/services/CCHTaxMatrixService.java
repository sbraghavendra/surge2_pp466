package com.ncsts.services;


import com.ncsts.domain.CCHTaxMatrix;
import com.ncsts.domain.CCHTaxMatrixPK;

/**
 * 
 * @author Anand
 *
 */

public interface CCHTaxMatrixService extends GenericService<CCHTaxMatrix,CCHTaxMatrixPK> {
}
