package com.ncsts.services;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.Idable;

public interface GenericServiceDTO<DTO, T extends Idable<ID>, ID extends Serializable> extends GenericService<T,ID> {

	// NOTE:  This class exposes both the T based interface as well as the DTO based version
	
	public Long count(DTO exampleInstance, String... excludeProperty);
	
	public DTO findDTOById(ID id) throws DataAccessException;
	
	public List<DTO> findDTOById(Collection<ID> ids);
	
	public List<DTO> findAllDTO();
	public List<DTO> findAllSortedDTO(String sortField, boolean ascending);
	
	public List<DTO> findByExample(DTO exampleInstance, int count, String... excludeProperty);
	public List<DTO> findByExampleSorted(DTO exampleInstance, int count, 
			String sortField, boolean ascending, String... excludeProperty);
	
	public void save(DTO instance);
	public void update(DTO instance) throws DataAccessException;
	public void remove(DTO instance)throws DataAccessException;
}
