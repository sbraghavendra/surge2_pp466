package com.ncsts.services;

import java.util.List;
import java.util.Map;

import com.ncsts.domain.DataDefinitionTable;
import org.springframework.dao.DataAccessException;

import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.dto.DataDefinitionColumnDTO;
import com.ncsts.dto.DataDefinitionTableDTO;

public interface DataDefinitionService {
	public abstract List<DataDefinitionTableDTO> getAllDataDefinitionTable()throws DataAccessException;
	public abstract DataDefinitionTableDTO findById(String tableName)throws DataAccessException;
	
	/**
	 * To get the rich faces table for DataDefinition Columns.
	 * @return
	 */
	public List<DataDefinitionColumn> getAllDataDefinitionColumnByTable(String tableName)throws DataAccessException;
	public List<DataDefinitionColumnDTO> getAllDataDefinitionColumn(DataDefinitionColumnDTO dataDefinitionColumnDTO)throws DataAccessException;
	public abstract DataDefinitionTableDTO addNewDataDefinitionTable(DataDefinitionTableDTO dataDefinitionTableDTO) throws DataAccessException;
	public abstract void update(DataDefinitionTableDTO dataDefinitionTableDTO) throws DataAccessException;
    public abstract void delete(String tableName) throws DataAccessException;	
    public abstract void update(DataDefinitionColumn dataDefinitionColumn) throws DataAccessException;
	public DataDefinitionColumn getDataDefinitionColumnByTable(String tableName , String columnName) ;
	public List<DataDefinitionColumn> getAllDataDefinitionColumnByColumnName(String columnName);
	public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDesc(String columnName, String description, String dataType) throws DataAccessException;
	public List<DataDefinitionColumn> getAllDataDefinitionByColumnNameAndDescDriver(String columnName, String description, String dataType,String driverCode) throws DataAccessException;
	public abstract DataDefinitionColumnDTO addNewDataDefinitionColumn(DataDefinitionColumnDTO dataDefinitionColumnDTO) throws DataAccessException;
	public abstract void update(DataDefinitionColumnDTO dataDefinitionColumnDTO) throws DataAccessException;
    public abstract void delete(String tableName , String columnName) throws DataAccessException;	 
    public List<String> getColumnNameList(String tableName);
    public List<String> getTableNameList();
    public List<String> getDataTypeList();
    public List<String> getDescriptionColumnList(String tableName);
    public abstract List<DataDefinitionTableDTO> getExportDataDefinitionTables() throws DataAccessException;
    public abstract DataDefinitionTableDTO getDataDefinitionByConfigType(String configType)throws DataAccessException;
    List<DataDefinitionTable> getAllImportExportTypes();


}
