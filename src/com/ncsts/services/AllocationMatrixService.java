package com.ncsts.services;

import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.common.helper.OrderBy;
import java.util.List;

public interface AllocationMatrixService extends MatrixService<AllocationMatrix> {
	public AllocationMatrix findByIdWithDetails(Long id);
	public List<AllocationMatrix> getDetailRecords(AllocationMatrix allocationMatrix, OrderBy orderBy);
}
