package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.domain.UserMenuEx;

public interface UserMenuExService {
	
	public List<UserMenuEx> findByUserEntity(String userCode,Long entityId) throws DataAccessException;
	
	public List<UserMenuEx> findByUserCode(String userCode) throws DataAccessException;
	
	public void saveOrUpdate (UserMenuEx userMenuEx) throws DataAccessException;
	
	public void delete(UserMenuEx userMenuEx) throws DataAccessException;
}
