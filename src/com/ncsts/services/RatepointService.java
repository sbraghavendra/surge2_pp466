package com.ncsts.services;


import java.util.Date;
import java.util.List;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.PurchaseTransaction;

public interface RatepointService {
    public Jurisdiction getRateforLatLon(String serverUrl, String latLonUrl, String userId, String password, String latitude, String longitude);
    public List<Jurisdiction> getJurGeneric(String serverUrl, String genericJurUrl, String userId, String password, String horl, String city, String county, String state, String zip, String country);
    public Jurisdiction getStreetLevelRate(String serverUrl, String streetLevelRateUrl,  String userId, String password, String streetAddress1, String streetAddress2, String city, String county, String state, String zip, String country, Date glDate);
}
