package com.ncsts.services;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.TransactionLog;
import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.PurchaseTransaction;

public interface PurchaseTransactionLogService extends GenericService<PurchaseTransactionLog,Long> {

/*	List<PurchaseTransactionLog> getAllGLTransactions(PurchaseTransactionLog exampleInstance,
			String sqlWhere, OrderBy orderBy, int firstRow, int maxResults,List<String> nullFileds)
			throws DataAccessException;*/
	
	Long getTransactionCount(PurchaseTransactionLog exampleInstance,
			String sqlWhere, List<String> nullFileds);
	
	List<PurchaseTransactionLog> findTransactionDetailLogList(Long purchtransLogId);

	void fillBeforeEntries(TransactionLog transactionLog, PurchaseTransaction transaction);
	void fillAfterEntries(TransactionLog transactionLog, PurchaseTransaction transaction);
	
	void fillBeforeEntries(TransactionLog transactionLog, MicroApiTransaction transaction);
	void fillAfterEntries(TransactionLog transactionLog, MicroApiTransaction transaction);
	

}
