package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.TaxCodeStateDTO;

/**
 * 
 * @author Anand
 *
 */
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_TAXCODE_STATE")
public class TaxCodeState extends Auditable implements Serializable,Idable<TaxCodeStatePK> {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * EmbeddedId primary key field
	 */
	@EmbeddedId
	TaxCodeStatePK taxCodeStatePK;

	@Column(name="GEOCODE")
	private String geoCode;
	
	@Column(name="NAME")
	private String name;

	@Column(name="LOCAL_TAXABILITY_CODE")
    private String localTaxabilityCode;
	
    @Column(name="ACTIVE_FLAG")
    private String activeFlag;
    
    @Column(name="CUSTOM_FLAG")
    private String customFlag;
    
    @Column(name="MODIFIED_FLAG")
    private String modifiedFlag;
    
    
    @Column(name="CP_FORMID")
    private String cpFormid;
    
    @Column(name="STATE_TAXABILITY_FLAG")
    private String stateTaxabilityFlag;

	@Column(name="MAX_LOCAL_RATE")
	private BigDecimal maxLocalRate;
    
    @Column (name="cp_cert_level_code")
    private String cpCertLevelCode;
    
    //Midtier project code added - january 2009
    @OneToMany(mappedBy = "taxCodeState")
    private Set<GlExtractMap> glExtractMap = new HashSet<GlExtractMap>();
    
    @OneToMany(mappedBy="taxCodeState")
    private Set<Jurisdiction> jurisdiction = new HashSet<Jurisdiction>();
    
    @OneToMany(mappedBy="taxCodeState")
    private Set<TaxCodeDetail> taxCodeDetail = new HashSet<TaxCodeDetail>();
    
    @OneToMany(mappedBy="taxCodeState")
    private Set<TransactionDetail> transactionDetail = new HashSet<TransactionDetail>();
    

	@Transient	
	TaxCodeStateDTO taxCodeStateDTO;
	
	/** default constructor */
    public TaxCodeState() {
    }

	public TaxCodeState(TaxCodeStatePK taxCodeStatePK) { 
    	this.taxCodeStatePK = taxCodeStatePK;
    }
	
	public TaxCodeStateDTO getTaxCodeStateDTO() {
		TaxCodeStateDTO taxCodeStateDTO = new TaxCodeStateDTO();
		BeanUtils.copyProperties(this, taxCodeStateDTO);
		return taxCodeStateDTO;
	}

	public void setTaxCodeStateDTO(TaxCodeStateDTO taxCodeStateDTO) {
		this.taxCodeStateDTO = taxCodeStateDTO;
	}
	
	public TaxCodeStatePK getTaxCodeStatePK() {
		return taxCodeStatePK;
	}
	
	public void setTaxCodeStatePK(TaxCodeStatePK taxCodeStatePK) {
		this.taxCodeStatePK = taxCodeStatePK;
	}
	
    // Idable interface
    public TaxCodeStatePK getId() {
        return taxCodeStatePK;
    }
    
    public void setId(TaxCodeStatePK id) {
    	this.taxCodeStatePK = id;
    }
    
    public String getIdPropertyName() {
    	return "taxCodeStatePK";
    }
    
	public String getTaxcodeStateCode() {
		return taxCodeStatePK.getTaxcodeStateCode();
	}

	public void setTaxCodeState(String taxCodeState) {
		taxCodeStatePK.setTaxcodeStateCode(taxCodeState);
	}
	
	//keep original code
	public String getTaxCodeState() {
		return taxCodeStatePK.getTaxcodeStateCode();
	}

	public String getGeoCode() {
		return geoCode;
	}

	public void setGeoCode(String geoCode) {
		this.geoCode = geoCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return taxCodeStatePK.getCountry();
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Set<GlExtractMap> getGlExtractMap() {
		return glExtractMap;
	}

	public void setGlExtractMap(Set<GlExtractMap> glExtractMap) {
		this.glExtractMap = glExtractMap;
	}

	public Set<Jurisdiction> getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Set<Jurisdiction> jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public Set<TaxCodeDetail> getTaxCodeDetail() {
		return taxCodeDetail;
	}

	public String getCpCertLevelCode() {
		return cpCertLevelCode;
	}

	public void setCpCertLevelCode(String cpCertLevelCode) {
		this.cpCertLevelCode = cpCertLevelCode;
	}

	public void setTaxCodeDetail(Set<TaxCodeDetail> taxCodeDetail) {
		this.taxCodeDetail = taxCodeDetail;
	}

	public Set<TransactionDetail> getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(Set<TransactionDetail> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}

	public String getLocalTaxabilityCode() {
		return localTaxabilityCode;
	}

	public void setLocalTaxabilityCode(String localTaxabilityCode) {
		this.localTaxabilityCode = localTaxabilityCode;
	}

	public String getCpFormId() {
		return cpFormid;
	}

	public void setCpFormId(String cpFormid) {
		this.cpFormid = cpFormid;
	}

	public String getStateTaxabilityFlag() {
		return stateTaxabilityFlag;
	}

	public void setStateTaxabilityFlag(String stateTaxabilityFlag) {
		this.stateTaxabilityFlag = stateTaxabilityFlag;
	}

	public BigDecimal getMaxLocalRate() {
		return maxLocalRate;
	}

	public void setMaxLocalRate(BigDecimal maxLocalRate) {
		this.maxLocalRate = maxLocalRate;
	}
}
