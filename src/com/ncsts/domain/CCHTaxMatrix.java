package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TB_CCH_TXMATRIX")
public class CCHTaxMatrix extends Auditable implements Serializable,Idable<CCHTaxMatrixPK> {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private CCHTaxMatrixPK cchTaxMatrixPK;

	@Column(name="GROUPDESC")
	private String groupDesc;
	@Column(name="CITY")
	private String city;
	@Column(name="COUNTY")
	private String county;
	@Column(name="STATE")
	private String state;
	@Column(name="ITEMDESC")
	private String itemDesc;
	@Temporal(TemporalType.DATE)
	@Column(name="EFFDATE")
	private Date effDate;
	@Column(name="CUSTOM_FLAG")
	private String customFlag;
	@Column(name="MODIFIED_FLAG")
	private String modifiedFlag;

	public CCHTaxMatrix() {
	}

	// Idable interface
	public CCHTaxMatrixPK getId() {
		return cchTaxMatrixPK;
	}
	
    public void setId(CCHTaxMatrixPK id) {
    	this.cchTaxMatrixPK = id;
    }
    
	public String getIdPropertyName() {
		return "cchTaxMatrixPK";
	}

	public CCHTaxMatrixPK getCchTaxMatrixPK() {
		return cchTaxMatrixPK;
	}

	public void setCchTaxMatrixPK(CCHTaxMatrixPK cchTaxMatrixPK) {
		this.cchTaxMatrixPK = cchTaxMatrixPK;
	}

	public String getGroupDesc() {
		return groupDesc;
	}
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public Date getEffDate() {
		return effDate;
	}
	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}
}
