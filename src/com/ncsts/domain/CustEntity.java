package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TB_CUST_ENTITY database table.
 * 
 */
@Entity
@Table(name="TB_CUST_ENTITY")
public class CustEntity extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustEntityPK id;

	//bi-directional many-to-one association to Cust
    //@ManyToOne
	//@JoinColumn(name="CUST_ID")
	//private Cust cust;

    public CustEntity() {
    }

	public CustEntityPK getId() {
		return this.id;
	}

	public void setId(CustEntityPK id) {
		this.id = id;
	}

	//public Cust getCust() {
	//	return this.cust;
	//}

	//public void setCust(Cust cust) {
	//	this.cust = cust;
	//}
	
}