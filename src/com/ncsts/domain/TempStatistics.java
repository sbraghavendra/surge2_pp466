package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DatabaseTransactionStatisticsDTO;

@Entity
@Table(name = "TB_TMP_STATISTICS")
public class TempStatistics extends Auditable implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	// Fields
	@Id
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "COUNT")
	private Long count;
	
	@Column(name = "COUNT_PCT")
	private Double countPct;
	
	@Column(name = "SUM")
	private Double sum;
	
	@Column(name = "SUM_PCT")
	private Double sumPct;
	
	@Column(name = "TAX_AMT")
	private Double taxAmt;
	
	@Column(name = "MAX")
	private Double max;
	
	@Column(name = "AVG")
	private Double avg;
	
	@Column(name = "WHERE_CLAUSE")
	private String whereClause;
	
	@Column(name = "GROUP_BY_ON_DRILLDOWN")
	private String groupByOnDrilldown;
	

	@Formula("CASE WHEN SUM > 0 THEN TAX_AMT/SUM ELSE 0.0 END")
	private Double effTaxRate;
	
	// Constructors

	/** default constructor */
	public TempStatistics() {
	}

	public DatabaseTransactionStatisticsDTO getDatabaseTransactionStatisticsDTO() {
		DatabaseTransactionStatisticsDTO databaseTransactionStatisticsDTO= new DatabaseTransactionStatisticsDTO();
    	BeanUtils.copyProperties(this,databaseTransactionStatisticsDTO);
    	return databaseTransactionStatisticsDTO;
    }
	
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public Long getCount() {
		return this.count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	
	public Double getCountPct() {
		return this.countPct;
	}

	public void setCountPct(Double countPct) {
		this.countPct = countPct;
	}

	
	public Double getSum() {
		return this.sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}

	
	public Double getSumPct() {
		return this.sumPct;
	}

	public void setSumPct(Double sumPct) {
		this.sumPct = sumPct;
	}

	
	public Double getTaxAmt() {
		return this.taxAmt;
	}

	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}

	
	public Double getMax() {
		return this.max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	
	public Double getAvg() {
		return this.avg;
	}

	public void setAvg(Double avg) {
		this.avg = avg;
	}

	
	public String getWhereClause() {
		return this.whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	
	public String getGroupByOnDrilldown() {
		return this.groupByOnDrilldown;
	}

	public void setGroupByOnDrilldown(String groupByOnDrilldown) {
		this.groupByOnDrilldown = groupByOnDrilldown;
	}
	
	/**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.title + "\t" + this.count + "\t" + this.countPct +"\t" + this.sum +"\t" + this.sumPct +"\t" + this.taxAmt + "\t" + this.effTaxRate + 
        "\t" + this.max + "\t" + this.avg + "\t" + this.whereClause + "\t" + this.groupByOnDrilldown;
    }

    public void setEffTaxRate(Double effTaxRate) {
		this.effTaxRate = effTaxRate;
	}
    
    public Double getEffTaxRate() {
		return effTaxRate;
	}
}