package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoleMenuPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="MENU_CODE")	
	private String menuCode;
	    
    @Column(name="ROLE_CODE")    
    private String roleCode; 
    
    public RoleMenuPK() {
		
	}
    public RoleMenuPK(String roleCode, String menuCode){
    	this.setMenuCode(menuCode);
    	this.setRoleCode(roleCode);
    }

	public String getMenuCode() {
		return menuCode;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof RoleMenuPK)) {
            return false;
        }

        final RoleMenuPK revision = (RoleMenuPK)other;

        String c1 = getMenuCode();
        String c2 = revision.getMenuCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String e1 = getRoleCode();
        String e2 = revision.getRoleCode();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getMenuCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String d1 = getRoleCode();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.menuCode + "::" + this.roleCode;
    }

	

}
