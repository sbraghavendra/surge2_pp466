package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TB_NEXUS_DEF_LOG")
public class NexusDefinitionLog extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="NEXUS_DEF_LOG_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="nexus_definition_log_sequence")
	@SequenceGenerator(name="nexus_definition_log_sequence" , allocationSize = 1, sequenceName="sq_tb_nexus_def_log_id")
	private Long nexusDefLogId;
	
	@Column(name="SOURCE_CODE")
	private String sourceCode;
	
	@Column(name="SOURCE_ID")
	private Long sourceId;
	
	@Column(name="ENTITY_ID")
	private Long entityId;
	
	@Column(name="NEXUS_DEF_DETAIL_ID")
	private Long nexusDefDetailId;
	
	@Column(name="NEXUS_COUNTRY_CODE")
	private String nexusCountryCode;
	
	@Column(name="NEXUS_STATE_CODE")
	private String nexusStateCode;
	
	@Column(name="NEXUS_COUNTY")
	private String nexusCounty;
	
	@Column(name="NEXUS_CITY")
	private String nexusCity;
	
	@Column(name="NEXUS_STJ")
	private String nexusStj;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="AUTO_USER_ID")
	private String autoUserId;
	
	@Override
	public Long getId() {
		return this.nexusDefLogId;
	}

	@Override
	public void setId(Long id) {
		this.nexusDefLogId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "nexusDefLogId";
	}

	public Long getNexusDefLogId() {
		return nexusDefLogId;
	}

	public void setNexusDefLogId(Long nexusDefLogId) {
		this.nexusDefLogId = nexusDefLogId;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public Long getNexusDefDetailId() {
		return nexusDefDetailId;
	}

	public void setNexusDefDetailId(Long nexusDefDetailId) {
		this.nexusDefDetailId = nexusDefDetailId;
	}

	public String getNexusCountryCode() {
		return nexusCountryCode;
	}

	public void setNexusCountryCode(String nexusCountryCode) {
		this.nexusCountryCode = nexusCountryCode;
	}

	public String getNexusStateCode() {
		return nexusStateCode;
	}

	public void setNexusStateCode(String nexusStateCode) {
		this.nexusStateCode = nexusStateCode;
	}

	public String getNexusCounty() {
		return nexusCounty;
	}

	public void setNexusCounty(String nexusCounty) {
		this.nexusCounty = nexusCounty;
	}

	public String getNexusCity() {
		return nexusCity;
	}

	public void setNexusCity(String nexusCity) {
		this.nexusCity = nexusCity;
	}

	public String getNexusStj() {
		return nexusStj;
	}

	public void setNexusStj(String nexusStj) {
		this.nexusStj = nexusStj;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public String getAutoUserId() {
		return autoUserId;
	}

	public void setAutoUserId(String autoUserId) {
		this.autoUserId = autoUserId;
	}
}
