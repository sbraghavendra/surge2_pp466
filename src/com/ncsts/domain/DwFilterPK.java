package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

@Embeddable
public class DwFilterPK implements Serializable {
	
	private static final long serialVersionUID = 1L;	

    @Column(name="WINDOW_NAME")    
    private String windowName;   
    
    @Column(name="FILTER_NAME")    
    private String filterName;
    
    @Column(name="USER_CODE")    
    private String userCode;   
    
    @Column(name="COLUMN_NAME")    
    private String columnName;
    
    public DwFilterPK(){	
    }
    
	public DwFilterPK(String windowName, String filterName, String userCode, String columnName) {   	
		this.windowName = windowName;
		this.filterName = filterName;
		this.userCode = userCode;
		this.columnName = columnName;
	}

    public String getWindowName() {
        return this.windowName;
    }
    
    public void setWindowName(String windowName) {
    	this.windowName = windowName;
    }

    public String getFilterName() {
        return this.filterName;
    }
    
    public void setFilterName(String filterName) {
    	this.filterName = filterName;
    }    
    
    public String getUserCode() {
        return this.userCode;
    }
    
    public void setUserCode(String userCode) {
    	this.userCode = userCode;
    }    
    
    public String getColumnName() {
        return this.columnName;
    }
    
    public void setColumnName(String columnName) {
    	this.columnName = columnName;
    }  
    
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DwFilterPK)) {
            return false;
        }

        final DwFilterPK revision = (DwFilterPK)other;

        String w1 = getWindowName();
        String w2 = revision.getWindowName();
        if (!((w1 == w2) || (w1 != null && w1.equals(w2)))) {
            return false;
        }

        String f1 = getFilterName();
        String f2 = revision.getFilterName();
        if (!((f1 == f2) || (f1 != null && f1.equals(f2)))) {
            return false;
        }
        
        String u1 = getUserCode();
        String u2 = revision.getUserCode();
        if (!((u1 == u2) || (u1 != null && u1.equals(u2)))) {
            return false;
        }
        
        String c1 = getColumnName();
        String c2 = revision.getColumnName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
    
    public int hashCode() {
		int hash = 7;
        String f1 = getWindowName();
		hash = 31 * hash + (null == f1 ? 0 : f1.hashCode());
        String f2 = getFilterName();
		hash = 31 * hash + (null == f2 ? 0 : f2.hashCode());
        String f3 = getUserCode();
		hash = 31 * hash + (null == f3 ? 0 : f3.hashCode());
		String f4 = getColumnName();
		hash = 31 * hash + (null == f4 ? 0 : f4.hashCode());

		return hash;
    }
}
