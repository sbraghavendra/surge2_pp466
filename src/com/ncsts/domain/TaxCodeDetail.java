package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;

/**
 * @author Vinay Singh & Paul Govindan
 *
 */
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_TAXCODE_DETAIL")
public class TaxCodeDetail extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="TAXCODE_DETAIL_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="taxcode_detail_sequence")
	@SequenceGenerator(name="taxcode_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_taxcode_detail_id")	
    private Long taxcodeDetailId;
    
    @Column(name="TAXCODE_STATE_CODE")
    private String taxcodeStateCode;
    
	@Column(name="TAXCODE_CODE")  	
	private String taxcodeCode;				// FK to TaxCode with TYPE_CODE 
	   
	@Column(name="TAXCODE_TYPE_CODE") 	
	private String taxcodeTypeCode;			// FK to TaxCode with CODE
	
	@Column(name="TAXCODE_COUNTRY_CODE")
    private String taxcodeCountryCode;

	//Midtier project code added - january 2009
	@OneToMany(mappedBy="taxCodeDetail")
	@Cascade(CascadeType.DELETE_ORPHAN)
	private Set<ReferenceDetail> referenceDetail = new HashSet<ReferenceDetail>();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns( {
		@JoinColumn(name="TAXCODE_STATE_CODE", referencedColumnName="TAXCODE_STATE_CODE", insertable=false, updatable=false),
		@JoinColumn(name="TAXCODE_COUNTRY_CODE", referencedColumnName="TAXCODE_COUNTRY_CODE", insertable=false, updatable=false) } )
		@ForeignKey(name="tb_taxcode_dtl_fk01")
	private TaxCodeState taxCodeState;
    
	@Column(name="TAXTYPE_CODE")    
    private String taxtypeCode;
    
    @Column(name="TAXCODE_COUNTY")
    private String taxCodeCounty;
    
    @Column(name="TAXCODE_CITY")
    private String taxCodeCity;
    
    @Column(name="TAXCODE_STJ")
    private String taxCodeStj;
    
    @Column(name="JUR_LEVEL")
    private String jurLevel;
    
    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
    
    @Column(name="RATETYPE_CODE")
    private String rateTypeCode;
    
    @Column(name="TAXABLE_THRESHOLD_AMT")
    private BigDecimal taxableThresholdAmt;

    @Column(name="BASE_CHANGE_PCT")
    private BigDecimal baseChangePct;
    
    @Column(name="SPECIAL_SETAMT")
    private BigDecimal specialSetAmt;
    
    @Column(name="MINIMUM_TAXABLE_AMT")
    private BigDecimal minimumTaxableAmt;
    
    @Column(name="MAXIMUM_TAXABLE_AMT")
    private BigDecimal maximumTaxableAmt;
    
    @Column(name="MAXIMUM_TAX_AMT")
    private BigDecimal maximumTaxAmt;
    
    @Column(name="GROUP_BY_DRIVER")
    private String groupByDriver;
    
    @Column(name="TAX_PROCESS_TYPE_CODE")
    private String taxProcessTypeCode;
    
    @Column(name="ALLOC_BUCKET")
    private String allocBucket;
    
    @Column(name="ALLOC_PRORATE_BY_CODE")
    private String allocProrateByCode;
    
    @Column(name="ALLOC_CONDITION_CODE")
    private String allocConditionCode;
    
    @Transient
	private boolean selected;

    @Column(name="SPECIAL_RATE")
    private BigDecimal specialRate;
    
    @Column(name="ACTIVE_FLAG")
    private String activeFlag;
    
    @Column(name="CUSTOM_FLAG")
    private String customFlag;
    
    @Column(name="MODIFIED_FLAG")
    private String modifiedFlag;
    
    @Column(name="SORT_NO")	
    private Long sortNo;
    
    @Column(name="CITATION_REF")
    private String citationRef;
    
    @Column(name="EXEMPT_REASON")
    private String exemptReason;
    
    @Column(name="GOOD_SVC_TYPE_CODE")
    private String goodSvcTypeCode;
    
    public TaxCodeDetail() { 
    	this.selected = false;
    }
    
    // Idable interface
    public Long getId() {
        return this.taxcodeDetailId;
    }
    
    public Boolean getSelected() {
		return this.selected;
	}
	
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
    
    public void setId(Long id) {
    	this.taxcodeDetailId = id;
    }
    
    public String getIdPropertyName() {
    	return "taxcodeDetailId";
    }
	
    public Long getTaxcodeDetailId() {
        return this.taxcodeDetailId;
    }
    
    public void setTaxcodeDetailId(Long taxcodeDetailId) {
    	this.taxcodeDetailId = taxcodeDetailId;
    }
    
	public String getTaxcodeStateCode() {
		return taxcodeStateCode;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
		this.taxcodeStateCode = taxcodeStateCode;
	}
	
	public String getTaxcodeCountryCode() {
		return taxcodeCountryCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
		this.taxcodeCountryCode = taxcodeCountryCode;
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	public String getTaxcodeTypeCode() {
		return taxcodeTypeCode;
	}

	public void setTaxcodeTypeCode(String taxcodeTypeCode) {
		this.taxcodeTypeCode = taxcodeTypeCode;
	}

    public String getTaxtypeCode() {
        return this.taxtypeCode;
    }
    
    public void setTaxtypeCode(String taxtypeCode) {
    	this.taxtypeCode = taxtypeCode;
    }
    
    public String getTaxCodeCounty() {
		return taxCodeCounty;
	}

	public void setTaxCodeCounty(String taxCodeCounty) {
		this.taxCodeCounty = taxCodeCounty;
	}

	public String getTaxCodeCity() {
		return taxCodeCity;
	}

	public void setTaxCodeCity(String taxCodeCity) {
		this.taxCodeCity = taxCodeCity;
	}

	public String getTaxCodeStj() {
		return taxCodeStj;
	}

	public void setTaxCodeStj(String taxCodeStj) {
		this.taxCodeStj = taxCodeStj;
	}

	public String getJurLevel() {
		return jurLevel;
	}

	public void setJurLevel(String jurLevel) {
		this.jurLevel = jurLevel;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getRateTypeCode() {
		return rateTypeCode;
	}

	public void setRateTypeCode(String rateTypeCode) {
		this.rateTypeCode = rateTypeCode;
	}

	public BigDecimal getTaxableThresholdAmt() {
		return taxableThresholdAmt;
	}

	public void setTaxableThresholdAmt(BigDecimal taxableThresholdAmt) {
		this.taxableThresholdAmt = taxableThresholdAmt;
	}

	public BigDecimal getBaseChangePct() {
		return baseChangePct;
	}

	public void setBaseChangePct(BigDecimal baseChangePct) {
		this.baseChangePct = baseChangePct;
	}

	public BigDecimal getSpecialRate() {
		return specialRate;
	}

	public void setSpecialRate(BigDecimal specialRate) {
		this.specialRate = specialRate;
	}
	
    public BigDecimal getSpecialSetAmt() {
		return specialSetAmt;
	}

	public void setSpecialSetAmt(BigDecimal specialSetAmt) {
		this.specialSetAmt = specialSetAmt;
	}
	
	public BigDecimal getMinimumTaxableAmt() {
		return minimumTaxableAmt;
	}

	public void setMinimumTaxableAmt(BigDecimal minimumTaxableAmt) {
		this.minimumTaxableAmt = minimumTaxableAmt;
	}
	
	public BigDecimal getMaximumTaxableAmt() {
		return maximumTaxableAmt;
	}

	public void setMaximumTaxableAmt(BigDecimal maximumTaxableAmt) {
		this.maximumTaxableAmt = maximumTaxableAmt;
	}
	
	public BigDecimal getMaximumTaxAmt() {
		return maximumTaxAmt;
	}

	public void setMaximumTaxAmt(BigDecimal maximumTaxAmt) {
		this.maximumTaxAmt = maximumTaxAmt;
	}
	
	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}
	
	public String getGroupByDriver() {
		return groupByDriver;
	}

	public void setGroupByDriver(String groupByDriver) {
		this.groupByDriver = groupByDriver;
	}
	
	public String getTaxProcessTypeCode() {
		return taxProcessTypeCode;
	}

	public void setTaxProcessTypeCode(String taxProcessTypeCode) {
		this.taxProcessTypeCode = taxProcessTypeCode;
	}
	
	public String getAllocBucket() {
		return allocBucket;
	}

	public void setAllocBucket(String allocBucket) {
		this.allocBucket = allocBucket;
	}
	
	public String getAllocProrateByCode() {
		return allocProrateByCode;
	}

	public void setAllocProrateByCode(String allocProrateByCode) {
		this.allocProrateByCode = allocProrateByCode;
	}
	
	public String getAllocConditionCode() {
		return allocConditionCode;
	}

	public void setAllocConditionCode(String allocConditionCode) {
		this.allocConditionCode = allocConditionCode;
	}

	public String getTaxcodeLabel() {
    	return String.format("%s - %s - %s",
    			((taxcodeStateCode == null)? "":taxcodeStateCode),
    			((taxcodeCode == null)? "":taxcodeCode),
    			((taxcodeTypeCode == null)? "":taxcodeTypeCode));
    }


	public Set<ReferenceDetail> getReferenceDetail() {
		return referenceDetail;
	}

	public void setReferenceDetail(Set<ReferenceDetail> referenceDetail) {
		this.referenceDetail = referenceDetail;
	}

	public TaxCodeState getTaxCodeState() {
		return taxCodeState;
	}

	public void setTaxCodeState(TaxCodeState taxCodeState) {
		this.taxCodeState = taxCodeState;
	}

	
	
	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Long getSortNo() {
        return this.sortNo;
    }
    
    public void setSortNo(Long sortNo) {
    	this.sortNo = sortNo;
    }
    
    public String getCitationRef() {
		return citationRef;
	}

	public void setCitationRef(String citationRef) {
		this.citationRef = citationRef;
	}
	
	public String getExemptReason() {
		return exemptReason;
	}

	public void setExemptReason(String exemptReason) {
		this.exemptReason = exemptReason;
	}
	
	public String getGoodSvcTypeCode() {
		return goodSvcTypeCode;
	}

	public void setGoodSvcTypeCode(String goodSvcTypeCode) {
		this.goodSvcTypeCode = goodSvcTypeCode;
	}
}
