package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DriverNamesDTO;

@Entity
@Table(name="TB_DRIVER_NAMES")
public class DriverNames extends Auditable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	DriverNamesPK driverNamesPK; 
	
	//@Column(name="DRIVER_NAMES_CODE")
	//private String drvNamesCode;
	
	@Column(name="TRANS_DTL_COLUMN_NAME")
	private String transDtlColName;
	
	@Column(name="MATRIX_COLUMN_NAME")
	private String matrixColName;
	
	@Column(name="TABLE_NAME")
	private String tableName;
	
	@Column(name="BUSINESS_UNIT_FLAG") 
	private String businessUnitFlag;
	
	@Column(name="MANDATORY_FLAG")
	private String mandatoryFlag; 
	
	@Column(name="NULL_DRIVER_FLAG")
	private String nullDriverFlag;
	
	@Column(name="WILDCARD_FLAG")
	private String wildcardFlag; 
	
	@Column(name="RANGE_FLAG")
	private String rangeFlag;
	
	@Column(name="TO_NUMBER_FLAG")
	private String toNumberFlag;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Transient	
	DriverNamesDTO driverNamesDTO;
	
    public DriverNames() { 
    }
	
    public DriverNames(DriverNamesPK driverNamesPK) { 
    	this.driverNamesPK = driverNamesPK;
    }
	/**
	 * @return the driverReferenceDTO
	*/
	public DriverNamesDTO getDriverNamesDTO() {
		DriverNamesDTO driverNamesDTO = new DriverNamesDTO();
		BeanUtils.copyProperties(this, driverNamesDTO);
		return driverNamesDTO;
	}
	/**
	 * @param driverReferenceDTO the driverReferenceDTO to set
	 */
	public void setDriverNamesDTO(DriverNamesDTO driverNamesDTO) {
		this.driverNamesDTO = driverNamesDTO;
	}
	
	public DriverNamesPK getDriverNamesPK() {
		return driverNamesPK;
	}

	public void setDriverNamesPK(DriverNamesPK driverNamesPK) {
		this.driverNamesPK = driverNamesPK;
	}

	public String getDrvNamesCode() {
		return driverNamesPK.getDrvNamesCode();
	}
	
	public Long getDriverId() {
		return driverNamesPK.getDriverId();
	}

	/*public void setDrvNamesCode(String drvNamesCode) {
		this.drvNamesCode = drvNamesCode;
	}*/

	public String getMatrixColName() {
		return matrixColName;
	}

	public void setMatrixColName(String matrixColName) {
		this.matrixColName = matrixColName;
	}
	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTransDtlColName() {
		return transDtlColName;
	}

	public void setTransDtlColName(String transDtlColName) {
		this.transDtlColName = transDtlColName;
	}

	public String getBusinessUnitFlag() {
		return businessUnitFlag;
	}
	public boolean isBusinessUnit() {
		return "1".equalsIgnoreCase(businessUnitFlag);
	}

	public void setBusinessUnitFlag(String businessUnitFlag) {
		this.businessUnitFlag = businessUnitFlag;
	}

	public String getMandatoryFlag() {
		return mandatoryFlag;
	}
	public boolean isMandatory() {
		return "1".equalsIgnoreCase(mandatoryFlag);
	}

	public void setMandatoryFlag(String mandatoryFlag) {
		this.mandatoryFlag = mandatoryFlag;
	}

	public String getNullDriverFlag() {
		return nullDriverFlag;
	}

	public void setNullDriverFlag(String nullDriverFlag) {
		this.nullDriverFlag = nullDriverFlag;
	}

	public String getWildcardFlag() {
		return wildcardFlag;
	}

	public void setWildcardFlag(String wildcardFlag) {
		this.wildcardFlag = wildcardFlag;
	}

	public String getRangeFlag() {
		return rangeFlag;
	}

	public void setRangeFlag(String rangeFlag) {
		this.rangeFlag = rangeFlag;
	}

	public String getToNumberFlag() {
		return toNumberFlag;
	}

	public void setToNumberFlag(String toNumberFlag) {
		this.toNumberFlag = toNumberFlag;
	}

	public String getActiveFlag() {
		return activeFlag;
	}
	
	public boolean isActive() {
		return "1".equalsIgnoreCase(activeFlag);
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	

}
