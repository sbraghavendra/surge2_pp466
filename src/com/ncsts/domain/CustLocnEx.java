package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TB_CUST_LOCN_EX database table.
 * 
 */
@Entity
@Table(name="TB_CUST_LOCN_EX")
public class CustLocnEx extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TB_CUST_LOCN_EX_CUSTLOCNEXID_GENERATOR" , allocationSize = 1, sequenceName="SQ_TB_CUST_LOCN_EX_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CUST_LOCN_EX_CUSTLOCNEXID_GENERATOR")
	@Column(name="CUST_LOCN_EX_ID")
	private Long custLocnExId;
	
	@Column(name="CUST_LOCN_ID")
	private Long custLocnId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	@Column(name="CITY_FLAG")
	private String cityFlag;

	@Column(name="COUNTRY_FLAG")
	private String countryFlag;

	@Column(name="COUNTY_FLAG")
	private String countyFlag;

	@Column(name="CUST_CERT_ID")
	private Long custCertId;

	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="ENTITY_ID")
	private Long entityId;

	@Column(name="EXEMPT_REASON_CODE")
	private String exemptReasonCode;

	@Column(name="EXEMPTION_TYPE_CODE")
	private String exemptionTypeCode;

	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	@Column(name="STATE_FLAG")
	private String stateFlag;

	@Column(name="STJ1_FLAG")
	private String stj1Flag;

	@Column(name="STJ2_FLAG")
	private String stj2Flag;

	@Column(name="STJ3_FLAG")
	private String stj3Flag;

	@Column(name="STJ4_FLAG")
	private String stj4Flag;

	@Column(name="STJ5_FLAG")
	private String stj5Flag;

	//bi-directional many-to-one association to CustLocn
    //@ManyToOne
	//@JoinColumn(name="CUST_LOCN_ID")
	//private CustLocn custLocn;

    public CustLocnEx() {
    }
    
    public Long getId() {
		return getCustLocnExId();
	}

	public String getIdPropertyName() {
		return "custLocnExId";
	}
	
	public void setId(Long id) {
		setCustLocnExId(id);
	}

	public Long getCustLocnExId() {
		return this.custLocnExId;
	}

	public void setCustLocnExId(Long custLocnExId) {
		this.custLocnExId = custLocnExId;
	}

	public Long getCustLocnId() {
		return this.custLocnId;
	}

	public void setCustLocnId(Long custLocnId) {
		this.custLocnId = custLocnId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getCityFlag() {
		return this.cityFlag;
	}

	public void setCityFlag(String cityFlag) {
		this.cityFlag = cityFlag;
	}

	public String getCountryFlag() {
		return this.countryFlag;
	}

	public void setCountryFlag(String countryFlag) {
		this.countryFlag = countryFlag;
	}

	public String getCountyFlag() {
		return this.countyFlag;
	}

	public void setCountyFlag(String countyFlag) {
		this.countyFlag = countyFlag;
	}

	public Long getCustCertId() {
		return this.custCertId;
	}

	public void setCustCertId(Long custCertId) {
		this.custCertId = custCertId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Long getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getExemptReasonCode() {
		return this.exemptReasonCode;
	}

	public void setExemptReasonCode(String exemptReasonCode) {
		this.exemptReasonCode = exemptReasonCode;
	}

	public String getExemptionTypeCode() {
		return this.exemptionTypeCode;
	}

	public void setExemptionTypeCode(String exemptionTypeCode) {
		this.exemptionTypeCode = exemptionTypeCode;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getStateFlag() {
		return this.stateFlag;
	}

	public void setStateFlag(String stateFlag) {
		this.stateFlag = stateFlag;
	}

	public String getStj1Flag() {
		return this.stj1Flag;
	}

	public void setStj1Flag(String stj1Flag) {
		this.stj1Flag = stj1Flag;
	}

	public String getStj2Flag() {
		return this.stj2Flag;
	}

	public void setStj2Flag(String stj2Flag) {
		this.stj2Flag = stj2Flag;
	}

	public String getStj3Flag() {
		return this.stj3Flag;
	}

	public void setStj3Flag(String stj3Flag) {
		this.stj3Flag = stj3Flag;
	}

	public String getStj4Flag() {
		return this.stj4Flag;
	}

	public void setStj4Flag(String stj4Flag) {
		this.stj4Flag = stj4Flag;
	}

	public String getStj5Flag() {
		return this.stj5Flag;
	}

	public void setStj5Flag(String stj5Flag) {
		this.stj5Flag = stj5Flag;
	}

	//public CustLocn getCustLocn() {
	//	return this.custLocn;
	//}

	//public void setCustLocn(CustLocn custLocn) {
	//	this.custLocn = custLocn;
	//}
	
}