
package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DataDefinitionColumnDTO;

/**
 *  @author Muneer
 */

@Entity
@Table (name="TB_DATA_DEF_COLUMN")
public class DataDefinitionColumn extends Auditable implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 DataDefinitionColumnPK dataDefinitionColumnPK;
	 
  
    @Column(name="DATA_TYPE" )
    private String dataType;
    
	@Column(name="DATA_LENGTH" )
    private Long datalength;
 
	@Column(name="DESCRIPTION" )
    private String description;

	@Column(name="ABBR_DESC" )
    private String abbrDesc;

	@Column(name="DEFINITION" )
    private String definition;
	
	@Column(name="DESC_COLUMN_NAME" )
    private String descColumnName;

	@Column(name="MAP_FLAG" )
    private String mapFlag;	
	
	@Column(name="MINIMUM_VIEW_FLAG" )
    private String minimumViewFlag;	
	
	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TABLE_NAME",insertable=false, updatable=false)
	@ForeignKey(name="tb_data_def_column_fk01")
	private DataDefinitionTable dataDefinitionTable;

	@Transient	
	DataDefinitionColumnDTO dataDefinitionColumnDTO;
	


	    
    // Constructors

    /** default constructor */
    public DataDefinitionColumn() {
    }
    
    public DataDefinitionColumn(DataDefinitionColumnPK dataDefinitionColumnPK) { 
    	this.dataDefinitionColumnPK = dataDefinitionColumnPK;
    }
    
    public DataDefinitionColumnDTO getDataDefinitionColumnDTO() {
    	DataDefinitionColumnDTO dataDefinitionColumnDTO= new DataDefinitionColumnDTO();
    	BeanUtils.copyProperties(this,dataDefinitionColumnDTO);
    	return dataDefinitionColumnDTO;
    }

	/**
	 * @param DataDefinitionColumnDTO the DataDefinitionColumnDTO to set
	 */
	public void setDataDefinitionColumnDTO(DataDefinitionColumnDTO dataDefinitionColumnDTO) {
		this.dataDefinitionColumnDTO = dataDefinitionColumnDTO;
	}

	public DataDefinitionColumnPK getDataDefinitionColumnPK() {
		return dataDefinitionColumnPK;
	}

	public void setDataDefinitionColumnPK(DataDefinitionColumnPK dataDefinitionColumnPK) {
		this.dataDefinitionColumnPK = dataDefinitionColumnPK;
	}

	/**
	 * @return the tranDetailColName
	 */
	public String getTableName() {
		return dataDefinitionColumnPK.getTableName();
	}
	
	/**
	 * @return the driverValue
	 */
	public String getColumnName() {
		return dataDefinitionColumnPK.getColumnName();
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Long getDatalength() {
		return datalength;
	}

	public void setDatalength(Long datalength) {
		this.datalength = datalength;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAbbrDesc() {
		return abbrDesc;
	}

	public void setAbbrDesc(String abbrDesc) {
		this.abbrDesc = abbrDesc;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getDescColumnName() {
		return descColumnName;
	}

	public void setDescColumnName(String descColumnName) {
		this.descColumnName = descColumnName;
	}

	public String getMapFlag() {
		return mapFlag;
	}

	public void setMapFlag(String mapFlag) {
		this.mapFlag = mapFlag;
	}
	
	public Boolean isMappable() {
		return "1".equals(mapFlag);
	}
	
	public String getMinimumViewFlag() {
		return minimumViewFlag;
	}

	public void setMinimumViewFlag(String minimumViewFlag) {
		this.minimumViewFlag = minimumViewFlag;
	}

	public DataDefinitionTable getDataDefinitionTable() {
		return dataDefinitionTable;
	}

	public void setDataDefinitionTable(DataDefinitionTable dataDefinitionTable) {
		this.dataDefinitionTable = dataDefinitionTable;
	}
}

