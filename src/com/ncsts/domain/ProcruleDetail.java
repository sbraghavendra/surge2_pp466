package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.ncsts.dto.SitusDriverDTO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TB_PROCRULE_DETAIL database table.
 * 
 */
@Entity
@Table(name="TB_PROCRULE_DETAIL")
public class ProcruleDetail extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PROCRULE_DETAIL_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="procrule_detail_sequence")
	@SequenceGenerator(name="procrule_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_procrule_detail_id")
	private Long procruleDetailId;

	@Column(name="CLOSEPAREN")
	private String closeparen;

	@Column(name="COMPARE_FIELD")
	private String compareField;

	@Column(name="COMPARE_RELATION")
	private String compareRelation;

	@Column(name="COMPARE_VALUE")
	private String compareValue;

	@Column(name="COMPARE_VALUE_CODE")
	private String compareValueCode;

	@Column(name="CONDITION")
	private String condition;

	@Column(name="LINE_NO")
	private Long lineNo;

	@Column(name="OPENPAREN")
	private String openparen;

	@Column(name="PROCRULE_ID")
	private Long procruleId;

	@Column(name="TABLE_PARMS")
	private String tableParms;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Transient
	private String operator;

	@Column(name="OPERATOR_VALUE_TABLE")
	private String operatorValueTable;
	
	@Column(name="OPERATOR_CODE")
	private String operatorCode;

	@Column(name="OPERATOR_VALUE")
	private String operatorValue;

	@Column(name="OPERATOR_VALUE_CODE")
	private String operatorValueCode;

	@Column(name="ANON_SQL")
	private String anonSql;
	
	@Column(name="COMPARE_FIELD_TABLE")
	private String compareFieldTable;
	
	@Column(name="COMPARE_VALUE_TABLE")
	private String compareValueTable;
	
	@Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
	
	@Transient
	private String sqlLike;
	
	@Transient
	private String textLike;
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public String getSqlLike() {
		return this.sqlLike;
	}

	public void setSqlLike(String sqlLike) {
		this.sqlLike = sqlLike;
	}
	
	public String getTextLike() {
		return this.textLike;
	}

	public void setTextLike(String textLike) {
		this.textLike = textLike;
	}

    public ProcruleDetail() {
    }
    
    // Idable interface
  	public Long getId() {
  		return getProcruleDetailId();
  	}
  	
    public void setId(Long id) {
    	setProcruleDetailId(id);
    }
     
    public String getIdPropertyName() {
 		return "procruleDetailId";
 	}

	public Long getProcruleDetailId() {
		return this.procruleDetailId;
	}

	public void setProcruleDetailId(Long procruleDetailId) {
		this.procruleDetailId = procruleDetailId;
	}

	public String getCloseparen() {
		return this.closeparen;
	}

	public void setCloseparen(String closeparen) {
		this.closeparen = closeparen;
	}

	public String getCompareField() {
		return this.compareField;
	}

	public void setCompareField(String compareField) {
		this.compareField = compareField;
	}

	public String getCompareRelation() {
		return this.compareRelation;
	}

	public void setCompareRelation(String compareRelation) {
		this.compareRelation = compareRelation;
	}

	public String getCompareValue() {
		return this.compareValue;
	}

	public void setCompareValue(String compareValue) {
		this.compareValue = compareValue;
	}

	public String getCompareValueCode() {
		return this.compareValueCode;
	}

	public void setCompareValueCode(String compareValueCode) {
		this.compareValueCode = compareValueCode;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Long getLineNo() {
		return this.lineNo;
	}

	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}

	public String getOpenparen() {
		return this.openparen;
	}

	public void setOpenparen(String openparen) {
		this.openparen = openparen;
	}

	public Long getProcruleId() {
		return this.procruleId;
	}

	public void setProcruleId(Long procruleId) {
		this.procruleId = procruleId;
	}

	public String getTableParms() {
		return this.tableParms;
	}

	public void setTableParms(String tableParms) {
		this.tableParms = tableParms;
	}
	
	public void setOperator(String operator) {
	    this.operator = operator;
	}

	public String getOperator() {
	    return this.operator;
	}

	public void setOperatorValueTable(String operatorValueTable) {
	    this.operatorValueTable = operatorValueTable;
	}

	public String getOperatorValueTable() {
	    return this.operatorValueTable;
	}

	public void setOperatorValue(String operatorValue) {
	    this.operatorValue = operatorValue;
	}

	public String getOperatorValue() {
	    return this.operatorValue;
	}
	
	public String getOperatorCode() {
	    return this.operatorCode;
	}
	
	public void setOperatorCode(String operatorCode) {
	    this.operatorCode = operatorCode;
	}

	public void setOperatorValueCode(String operatorValueCode) {
	    this.operatorValueCode = operatorValueCode;
	}

	public String getOperatorValueCode() {
	    return this.operatorValueCode;
	}

	public void setAnonSql(String anonSql) {
	    this.anonSql = anonSql;
	}

	public String getAnonSql() {
	    return this.anonSql;
	}
	
	public void setCompareFieldTable(String compareFieldTable) {
	    this.compareFieldTable = compareFieldTable;
	}

	public String getCompareFieldTable() {
	    return this.compareFieldTable;
	}
	
	public void setCompareValueTable(String compareValueTable) {
	    this.compareValueTable = compareValueTable;
	}

	public String getCompareValueTable() {
	    return this.compareValueTable;
	}
	
	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
}