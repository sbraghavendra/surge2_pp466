package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * 
 * @author Anand
 * @version 1.0
 * 
 */
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_LOCATION_MATRIX")
public class LocationMatrix extends Matrix implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int CITY_SIZE = 40;
	public static final int COUNTY_SIZE = 40;
	public static final int GEOCODE_SIZE = 12;

	public static final long DRIVER_COUNT = 10L;
	public static final String DRIVER_CODE = "L";
	
	@Id
	@Column(name="LOCATION_MATRIX_ID")
	//Code modified as part of Midtier project in Dec 2008
	@GeneratedValue (strategy = GenerationType.SEQUENCE, generator="location_sequence")
	@SequenceGenerator(name="location_sequence" , allocationSize = 1, sequenceName="SQ_TB_LOCATION_MATRIX_ID")
	private Long locationMatrixId;
	
	@Column(name="BINARY_WEIGHT")
	private Long binaryWt;
	
	@Column(name="DEFAULT_BINARY_WEIGHT")
	private Long defltBinaryWt;
	
	@Column(name="DRIVER_01")
	private String driver01;
	
	@Column(name="DRIVER_01_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver01Desc;
	
	@Column(name="DRIVER_02")
	private String driver02;
	
	@Column(name="DRIVER_02_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver02Desc;
    
	@Column(name="DRIVER_03")
	private String driver03;
	
	@Column(name="DRIVER_03_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
	private String driver03Desc;
	
	@Column(name="DRIVER_04")
	private String driver04;
	
	@Column(name="DRIVER_04_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver04Desc;
	
	@Column(name="DRIVER_05", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver05;
	
	@Column(name="DRIVER_05_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver05Desc;
	
	@Column(name="DRIVER_06", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver06;
	
	@Column(name="DRIVER_06_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver06Desc;
	
	@Column(name="DRIVER_07", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver07;
	
	@Column(name="DRIVER_07_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver07Desc;
	
	@Column(name="DRIVER_08", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver08;
	
	@Column(name="DRIVER_08_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver08Desc;
	
	@Column(name="DRIVER_09", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver09;
	
	@Column(name="DRIVER_09_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver09Desc;
	
	@Column(name="DRIVER_10", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver10;
	
	@Column(name="DRIVER_10_DESC", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    private String driver10Desc;
	
	@Column(name="SIGNIFICANT_DIGITS", unique=false, nullable=true, insertable=true, updatable=true, length=39)
    private String significantDigits;
	
	@Column(name="DEFAULT_FLAG", unique=false, nullable=true, insertable=true, updatable=true, length=1)
    private String defaultFlag;
	
	@Column(name="DEFAULT_SIGNIFICANT_DIGITS", unique=false, nullable=true, insertable=true, updatable=true, length=39)
    private String defaultSignificantDigits;
	
	@Temporal(TemporalType.DATE)
    @Column(name="EFFECTIVE_DATE", unique=false, nullable=true, insertable=true, updatable=true, length=7)
    private Date effectiveDate;
	
	@Temporal(TemporalType.DATE)
    @Column(name="EXPIRATION_DATE", unique=false, nullable=true, insertable=true, updatable=true, length=7)
    private Date expirationDate;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "JURISDICTION_ID", referencedColumnName = "JURISDICTION_ID",insertable=true,unique=false)
	@NotFound(action = NotFoundAction.IGNORE)
	private Jurisdiction jurisdiction;

//	@Column(name="JURISDICTION_ID", unique=false, nullable=true, insertable=true, updatable=true, precision=22, scale=0)
//	private Long jurisdictionId;
	
	@Column(name="OVERRIDE_TAXTYPE_CODE", unique=false, nullable=true, insertable=true, updatable=true, length=10)
    private String overrideTaxtypeCode;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Column(name="STATE_FLAG")
	private String stateFlag;
	
	@Column(name="COUNTY_FLAG")
	private String countyFlag;
	
	@Column(name="CITY_FLAG")
	private String cityFlag;
	
	@Column(name="COMMENTS", unique=false, nullable=true, insertable=true, updatable=true)
    private String comments;
	
	@Column(name="COUNTRY_FLAG", unique=false, nullable=true, insertable=true, updatable=true, length=1)
    private String countryFlag;
		
	@Column(name="ENTITY_ID")	
	private Long entityId;
	
	@Column(name="STJ1_FLAG")	
	private Long stj1Flag;
	
	@Column(name="STJ2_FLAG")	
	private Long stj2Flag;
	
	@Column(name="STJ3_FLAG")	
	private Long stj3Flag;
	
	@Column(name="STJ4_FLAG")	
	private Long stj4Flag;
	
	@Column(name="STJ5_FLAG")	
	private Long stj5Flag;
	
	//Midtier project code added - january 2009
	@OneToMany(mappedBy="locationMatrix")
	private Set<TransactionDetail> transactionDetail = new HashSet<TransactionDetail>();
	
	public Long getLocationMatrixId() {
		return locationMatrixId;
	}
	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}
	
	// Idable interface
	public Long getId() {
		return locationMatrixId;
	}

    public void setId(Long id) {
    	this.locationMatrixId = id;
    }
    
	public String getIdPropertyName() {
    	return "locationMatrixId";
    }

    // Matrix overrides
    @Override
    public Long getDriverCount() {
    	return DRIVER_COUNT;
    }
    
    @Override
    public String getDriverCode() {
    	return DRIVER_CODE;
    }

    @Override
    public boolean getHasDescriptions() {
    	return true;
    }

	public String getDriver01() {
		return driver01;
	}
	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}
	public String getDriver02() {
		return driver02;
	}
	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}
	public String getDriver03() {
		return driver03;
	}
	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}
	public String getDriver04() {
		return driver04;
	}
	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}
	public Long getBinaryWt() {
		return binaryWt;
	}
	public void setBinaryWt(Long binaryWt) {
		this.binaryWt = binaryWt;
	}
	public Long getDefltBinaryWt() {
		return defltBinaryWt;
	}
	public void setDefltBinaryWt(Long defltBinaryWt) {
		this.defltBinaryWt = defltBinaryWt;
	}
	
    public String getDriver01Desc() {
        return this.driver01Desc;
    }
    
    public void setDriver01Desc(String driver01Desc) {
        this.driver01Desc = driver01Desc;
    }
    public String getDriver02Desc() {
        return this.driver02Desc;
    }
    
    public void setDriver02Desc(String driver02Desc) {
        this.driver02Desc = driver02Desc;
    }
    public String getDriver03Desc() {
        return this.driver03Desc;
    }
    public void setDriver03Desc(String driver03Desc) {
        this.driver03Desc = driver03Desc;
    }
    public String getDriver04Desc() {
        return this.driver04Desc;
    }
    public void setDriver04Desc(String driver04Desc) {
        this.driver04Desc = driver04Desc;
    }
    public String getDriver05() {
        return this.driver05;
    }
    public void setDriver05(String driver05) {
        this.driver05 = driver05;
    }
    public String getDriver05Desc() {
        return this.driver05Desc;
    }
    public void setDriver05Desc(String driver05Desc) {
        this.driver05Desc = driver05Desc;
    }
    public String getDriver06() {
        return this.driver06;
    }
    public void setDriver06(String driver06) {
        this.driver06 = driver06;
    }
    public String getDriver06Desc() {
        return this.driver06Desc;
    }
    public void setDriver06Desc(String driver06Desc) {
        this.driver06Desc = driver06Desc;
    }
    public String getDriver07() {
        return this.driver07;
    }
    public void setDriver07(String driver07) {
        this.driver07 = driver07;
    }
    public String getDriver07Desc() {
        return this.driver07Desc;
    }
    public void setDriver07Desc(String driver07Desc) {
        this.driver07Desc = driver07Desc;
    }
    public String getDriver08() {
        return this.driver08;
    }
    public void setDriver08(String driver08) {
        this.driver08 = driver08;
    }
    public String getDriver08Desc() {
        return this.driver08Desc;
    }
    public void setDriver08Desc(String driver08Desc) {
        this.driver08Desc = driver08Desc;
    }
    public String getDriver09() {
        return this.driver09;
    }
    public void setDriver09(String driver09) {
        this.driver09 = driver09;
    }
    public String getDriver09Desc() {
        return this.driver09Desc;
    }
    public void setDriver09Desc(String driver09Desc) {
        this.driver09Desc = driver09Desc;
    }
    public String getDriver10() {
        return this.driver10;
    }
    public void setDriver10(String driver10) {
        this.driver10 = driver10;
    }
    public String getDriver10Desc() {
        return this.driver10Desc;
    }
    public void setDriver10Desc(String driver10Desc) {
        this.driver10Desc = driver10Desc;
    }

    public String getSignificantDigits() {
        return this.significantDigits;
    }
    public void setSignificantDigits(String significantDigits) {
        this.significantDigits = significantDigits;
    }
    public String getDefaultFlag() {
        return this.defaultFlag;
    }
    public void setDefaultFlag(String defaultFlag) {
        this.defaultFlag = defaultFlag;
    }
    
    public String getCountryFlag() {
        return this.countryFlag;
    }
    public void setCountryFlag(String countryFlag) {
        this.countryFlag = countryFlag;
    }

    public Boolean getDefaultBooleanFlag() {
		return (defaultFlag == null)? null:defaultFlag.equals("1");
	}

	public void setDefaultBooleanFlag(Boolean defaultFlag) {
		setDefaultFlag((defaultFlag == null)? null:((defaultFlag)? "1":"0"));
	}
	public void setActiveBooleanFlag(Boolean activeFlag) {
		setActiveFlag((activeFlag == null)? null:((activeFlag)? "1":"0"));
	}
	public Boolean getActiveBooleanFlag() {
		return (activeFlag == null)? null:activeFlag.equals("1");
	}

    public String getDefaultSignificantDigits() {
        return this.defaultSignificantDigits;
    }
    public void setDefaultSignificantDigits(String defaultSignificantDigits) {
        this.defaultSignificantDigits = defaultSignificantDigits;
    }
    public Date getEffectiveDate() {
        return this.effectiveDate;
    }
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
    public Date getExpirationDate() {
        return this.expirationDate;
    }
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Jurisdiction getJurisdiction() {
    	return jurisdiction;
	}
	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

//	public Long getJurisdictionId() {
//		return jurisdictionId;
//	}
//	public void setJurisdictionId(Long jurisdictionId) {
//		this.jurisdictionId = jurisdictionId;
//	}

    public String getOverrideTaxtypeCode() {
        return this.overrideTaxtypeCode;
    }
    public void setOverrideTaxtypeCode(String overrideTaxtypeCode) {
        this.overrideTaxtypeCode = overrideTaxtypeCode;
    }
    public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getStateFlag() {
		return stateFlag;
	}
	public void setStateFlag(String stateFlag) {
		this.stateFlag = stateFlag;
	}
    public Boolean getStateBooleanFlag() {
		return (stateFlag == null)? null:stateFlag.equals("1");
	}
	public void setStateBooleanFlag(Boolean stateFlag) {
		setStateFlag((stateFlag == null)? null:((stateFlag)? "1":"0"));
	}
	public Boolean getCountryBooleanFlag() {
		return (countryFlag == null)? null:countryFlag.equals("1");
	}
	public void setCountryBooleanFlag(Boolean countryFlag) {
		setCountryFlag((countryFlag == null)? null:((countryFlag)? "1":"0"));
	}
	public String getCountyFlag() {
		return countyFlag;
	}
	public void setCountyFlag(String countyFlag) {
		this.countyFlag = countyFlag;
	}
    public Boolean getCountyBooleanFlag() {
		return (countyFlag == null)? null:countyFlag.equals("1");
	}
	public void setCountyBooleanFlag(Boolean countyFlag) {
		setCountyFlag((countyFlag == null)? null:((countyFlag)? "1":"0"));
	}

	public String getCityFlag() {
		return cityFlag;
	}
	public void setCityFlag(String cityFlag) {
		this.cityFlag = cityFlag;
	}
    public Boolean getCityBooleanFlag() {
		return (cityFlag == null)? null:cityFlag.equals("1");
	}
	public void setCityBooleanFlag(Boolean cityFlag) {
		setCityFlag((cityFlag == null)? null:((cityFlag)? "1":"0"));
	}
	public String getComments() {
        return this.comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    
    public Long getEntityId() {
        return this.entityId;
    }
    
    public void setEntityId(Long entityId) {
    	this.entityId = entityId;
    }
      
    public Long getStj1Flag() {
        return this.stj1Flag;
    }
    
    public void setStj1Flag(Long stj1Flag) {
    	this.stj1Flag = stj1Flag;
    }

    public Long getStj2Flag() {
        return this.stj2Flag;
    }
    
    public void setStj2Flag(Long stj2Flag) {
    	this.stj2Flag = stj2Flag;
    }
    
    public Long getStj3Flag() {
        return this.stj3Flag;
    }
    
    public void setStj3Flag(Long stj3Flag) {
    	this.stj3Flag = stj3Flag;
    }
    
    public Long getStj4Flag() {
        return this.stj4Flag;
    }
    
    public void setStj4Flag(Long stj4Flag) {
    	this.stj4Flag = stj4Flag;
    }
    
    public Long getStj5Flag() {
        return this.stj5Flag;
    }
    
    public void setStj5Flag(Long stj5Flag) {
    	this.stj5Flag = stj5Flag;
    }
	
	@Override
	public void initializeDefaults() {
		super.initializeDefaults();
		if ((overrideTaxtypeCode == null) || overrideTaxtypeCode.equals("")) setOverrideTaxtypeCode("*NO");
		if ((defaultFlag == null) || defaultFlag.equals("")) setDefaultBooleanFlag(false);
		if ((stateFlag == null) || stateFlag.equals("")) setStateBooleanFlag(true);
		if ((countryFlag == null) || countryFlag.equals("")) setCountryBooleanFlag(true);
		if ((countyFlag == null) || countyFlag.equals("")) setCountyBooleanFlag(true);
		if ((cityFlag == null) || cityFlag.equals("")) setCityBooleanFlag(true);
	}
	public Set<TransactionDetail> getTransactionDetail() {
		return transactionDetail;
	}
	public void setTransactionDetail(Set<TransactionDetail> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public Boolean getStj1BooleanFlag() {
    	return (stj1Flag!=null && stj1Flag==1L);
	}

	public void setStj1BooleanFlag(Boolean stj1BooleanFlag) {
		this.stj1Flag = (stj1BooleanFlag == Boolean.TRUE) ? 1L : 0L;
	}
	
	public Boolean getStj2BooleanFlag() {
    	return (stj2Flag!=null && stj2Flag==1L);
	}

	public void setStj2BooleanFlag(Boolean stj2BooleanFlag) {
		this.stj2Flag = (stj2BooleanFlag == Boolean.TRUE) ? 1L : 0L;
	}
	
	public Boolean getStj3BooleanFlag() {
    	return (stj3Flag!=null && stj3Flag==1L);
	}

	public void setStj3BooleanFlag(Boolean stj3BooleanFlag) {
		this.stj3Flag = (stj3BooleanFlag == Boolean.TRUE) ? 1L : 0L;
	}
	
	public Boolean getStj4BooleanFlag() {
    	return (stj4Flag!=null && stj4Flag==1L);
	}

	public void setStj4BooleanFlag(Boolean stj4BooleanFlag) {
		this.stj4Flag = (stj4BooleanFlag == Boolean.TRUE) ? 1L : 0L;
	}
	
	public Boolean getStj5BooleanFlag() {
    	return (stj5Flag!=null && stj5Flag==1L);
	}

	public void setStj5BooleanFlag(Boolean stj5BooleanFlag) {
		this.stj5Flag = (stj5BooleanFlag == Boolean.TRUE) ? 1L : 0L;
	}
}
