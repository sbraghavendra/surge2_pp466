package com.ncsts.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ncsts.ws.service.impl.adapter.JaxbDateAdapter;



@Entity
@Table(name = "TB_IMPORT_CONNECTION")
//public class ImportConnection extends Auditable implements java.io.Serializable {
public class ImportConnection extends Auditable implements java.io.Serializable, Idable<String> {

	private static final long serialVersionUID = 2023895857069586422L;
	
	@Id
	@Column(name="IMPORT_CONNECTION_CODE")	
	private String importConnectionCode;
	
	@Column(name="IMPORT_CONNECTION_TYPE")	
	private String importConnectionType;
	
	@Column(name="DESCRIPTION")	
	private String description;
	
	@Column(name="DB_DRIVER_CLASS_NAME")	
	private String dbDriverClassName;
	
	@Column(name="DB_URL")	
	private String dbUrl;
	
	@Column(name="DB_USER_NAME")	
	private String dbUserName;
	
	@Column(name="DB_PASSWORD")	
	private String dbPassword;
	
	@Column(name="COMMENTS")	
	private String comments;
	

	public String getImportConnectionCode() {
		return importConnectionCode;
	}

	public void setImportConnectionCode(String importConnectionCode) {
		this.importConnectionCode = importConnectionCode;
	}

	public String getImportConnectionType() {
		return importConnectionType;
	}

	public void setImportConnectionType(String importConnectionType) {
		this.importConnectionType = importConnectionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDbDriverClassName() {
		return dbDriverClassName;
	}

	public void setDbDriverClassName(String dbDriverClassName) {
		this.dbDriverClassName = dbDriverClassName;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String getId() {		
		return importConnectionCode;
	}

	@Override
	public void setId(String id) {
		importConnectionCode = id;
		
	}

	@Override
	public String getIdPropertyName() {		
		return "importConnectionCode";
	}


}
