
package com.ncsts.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.ImportSpecUserDTO;

/**
 *  @author Muneer
 */

@Entity
@Table (name="TB_IMPORT_SPEC_USER")
public class ImportSpecUser implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 ImportSpecUserPK importSpecUserPK;	 
	 
	 //Midtier project code added - january 2009
	 @ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumns( {
	    	@JoinColumn(name="IMPORT_SPEC_TYPE", insertable=false, updatable=false),
	    	@JoinColumn(name="IMPORT_SPEC_CODE", insertable=false, updatable=false) } )
			@ForeignKey(name="tb_import_spec_user_fk01")
			private ImportSpec importSpec;
	 
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="USER_CODE", insertable=false, updatable=false)
	 @ForeignKey(name="tb_import_spec_user_fk02")
	 private User users;
  
	
	@Transient	
	ImportSpecUserDTO importSpecUserDTO;

	    
    // Constructors

    /** default constructor */
    public ImportSpecUser() {
    }
    
    public ImportSpecUser( ImportSpecUserPK importSpecUserPK) { 
    	this.importSpecUserPK = importSpecUserPK;
    }
    
    public ImportSpecUserDTO getImportSpecUserDTO() {
    	ImportSpecUserDTO importSpecUserDTO= new ImportSpecUserDTO();
    	BeanUtils.copyProperties(this,importSpecUserDTO);
    	return importSpecUserDTO;
    }
	public void setImportSpecUserDTO(ImportSpecUserDTO importSpecUserDTO) {
		this.importSpecUserDTO = importSpecUserDTO;
	}

	public ImportSpecUserPK getImportSpecUserPK() {
		return importSpecUserPK;
	}

	public void setImportSpecUserPK(ImportSpecUserPK importSpecUserPK) {
		this.importSpecUserPK = importSpecUserPK;
	}
	
	public String getImportSpecType(){
		return importSpecUserPK.getImportSpecType();
	}
	public String getImportSpecCode(){
		return importSpecUserPK.getImportSpecCode();
	}	
	public String getUserCode(){
		return importSpecUserPK.getUserCode();
	}

	public ImportSpec getImportSpec() {
		return importSpec;
	}

	public void setImportSpec(ImportSpec importSpec) {
		this.importSpec = importSpec;
	}

	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}
}

