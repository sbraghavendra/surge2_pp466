package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;
import com.ncsts.dto.RegistrationDTO;

@Entity
@Table(name="TB_REGISTRATION")
public class Registration extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="REGISTRATION_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="registration_sequence")
	@SequenceGenerator(name="registration_sequence" , allocationSize = 1, sequenceName="sq_tb_registration_id")
    private Long registrationId;
	
	@Column(name="ENTITY_ID")
	private Long entityId;
	
	@Column(name="REG_COUNTRY_CODE")
	private String regCountryCode;
	
	@Column(name="REG_STATE_CODE")
	private String regStateCode;
	
	@Column(name="REG_COUNTY")
	private String regCounty;
	
	@Column(name="REG_CITY")
	private String regCity;
	
	@Column(name="REG_STJ")
	private String regStj;
	
	@Transient	
	RegistrationDTO registrationDTO;
	
	public RegistrationDTO getRegistrationDTO() {
		RegistrationDTO registrationDTO = new RegistrationDTO();
		BeanUtils.copyProperties(this, registrationDTO);
		return registrationDTO;
	}

	public Long getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(Long registrationId) {
		this.registrationId = registrationId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getRegCountryCode() {
		return regCountryCode;
	}

	public void setRegCountryCode(String regCountryCode) {
		this.regCountryCode = regCountryCode;
	}
	
	public String getRegStateCode() {
		return regStateCode;
	}

	public void setRegStateCode(String regStateCode) {
		this.regStateCode = regStateCode;
	}
	
	public String getRegCounty() {
		return regCounty;
	}

	public void setRegCounty(String regCounty) {
		this.regCounty = regCounty;
	}
	
	public String getRegCity() {
		return regCity;
	}

	public void setRegCity(String regCity) {
		this.regCity = regCity;
	}
	
	public String getRegStj() {
		return regStj;
	}

	public void setRegStj(String regStj) {
		this.regStj = regStj;
	}
	
	@Override
	public Long getId() {
		return this.registrationId;
	}

	@Override
	public void setId(Long id) {
		this.registrationId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "registrationId";
	}
	
}
