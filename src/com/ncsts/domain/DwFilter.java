package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TB_DW_FILTER")
public class DwFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	DwFilterPK dwFilterPK; 
    
    @Column(name="FILTER_VALUE")    
    private String filterValue;

	public DwFilter() { 
		this.dwFilterPK = new DwFilterPK();
    }
	
	public DwFilter(DwFilterPK dwFilterPK) { 
    	this.dwFilterPK = dwFilterPK;
    }
	
	public DwFilterPK getDwFilterPK() {
		return dwFilterPK;
	}

	public void setDwFilterPK(DwFilterPK dwFilterPK) {
		this.dwFilterPK = dwFilterPK;
	}

    public String getWindowName() {
        return dwFilterPK.getWindowName();
    }
    
    public void setWindowName(String windowName) {
    	dwFilterPK.setWindowName(windowName);
    }

    public String getFilterName() {
        return dwFilterPK.getFilterName();
    }
    
    public void setFilterName(String filterName) {
    	dwFilterPK.setFilterName(filterName);
    }    
    
    public String getUserCode() {
        return dwFilterPK.getUserCode();
    }
    
    public void setUserCode(String userCode) {
    	dwFilterPK.setUserCode(userCode);
    }    
    
    public String getColumnName() {
        return dwFilterPK.getColumnName();
    }
    
    public void setColumnName(String columnName) {
    	dwFilterPK.setColumnName(columnName);
    }    
    
    public String getFilterValue() {
        return this.filterValue;
    }
    
    public void setFilterValue(String filterValue) {
    	this.filterValue = filterValue;
    } 
    
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DwFilter)) {
            return false;
        }

        final DwFilter revision = (DwFilter)other;

        String w1 = getWindowName();
        String w2 = revision.getWindowName();
        if (!((w1 == w2) || (w1 != null && w1.equals(w2)))) {
            return false;
        }

        String f1 = getFilterName();
        String f2 = revision.getFilterName();
        if (!((f1 == f2) || (f1 != null && f1.equals(f2)))) {
            return false;
        }
        
        String u1 = getUserCode();
        String u2 = revision.getUserCode();
        if (!((u1 == u2) || (u1 != null && u1.equals(u2)))) {
            return false;
        }
        
        String c1 = getColumnName();
        String c2 = revision.getColumnName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
    
    public int hashCode() {
		int hash = 7;
        String f1 = getWindowName();
		hash = 31 * hash + (null == f1 ? 0 : f1.hashCode());
        String f2 = getFilterName();
		hash = 31 * hash + (null == f2 ? 0 : f2.hashCode());
        String f3 = getUserCode();
		hash = 31 * hash + (null == f3 ? 0 : f3.hashCode());
		String f4 = getColumnName();
		hash = 31 * hash + (null == f4 ? 0 : f4.hashCode());

		return hash;
    }
}
