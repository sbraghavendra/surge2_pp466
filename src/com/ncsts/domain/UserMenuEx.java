package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.UserMenuExDTO;

@Entity
@Table(name="TB_USER_MENU_EX")
public class UserMenuEx implements Serializable, Idable<UserMenuExPK> {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	UserMenuExPK userMenuExPk; 
	
	@Column(name="EXCEPTION_TYPE")
	private String exceptionType;
	
	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns( {
    	@JoinColumn(name="USER_CODE",insertable=false, updatable=false),
    	@JoinColumn(name="ENTITY_ID",insertable=false, updatable=false) })
	@ForeignKey(name="tb_user_menu_ex_fk01")
	private UserEntity userEntity;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MENU_CODE",insertable=false, updatable=false)
	@ForeignKey(name="tb_user_menu_ex_fk02")
	private Menu menu;
	
	
	@Transient	
	UserMenuExDTO userMenuExDTO;
	
    public UserMenuEx() { 
    	
    }
	
	public UserMenuExDTO getUserMenuExDTO() {
		UserMenuExDTO userMenuExDTO = new UserMenuExDTO();
		BeanUtils.copyProperties(this, userMenuExDTO);
		return userMenuExDTO;
	}

	public void setUserMenuExDTO(UserMenuExDTO userMenuExDTO) {
		this.userMenuExDTO = userMenuExDTO;
	}

	public UserMenuExPK getUserMenuExPk(){
		return userMenuExPk;
	}
	
	public UserMenuExPK getId() {
		return userMenuExPk;
	}

	public String getIdPropertyName() {
		return "userMenuExPK";
	}

	public void setId(UserMenuExPK id) {
		userMenuExPk = id;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

}
