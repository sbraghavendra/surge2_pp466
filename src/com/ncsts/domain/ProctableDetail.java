package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TB_PROCTABLE_DETAIL database table.
 * 
 */
@Entity
@Table(name="TB_PROCTABLE_DETAIL")
public class ProctableDetail extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PROCTABLE_DETAIL_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="proctable_detail_sequence")
	@SequenceGenerator(name="proctable_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_proctable_detail_id")
	private Long proctableDetailId;

	@Column(name="IN_COLUMN_01")
	private String inColumn01;

	@Column(name="IN_COLUMN_02")
	private String inColumn02;

	@Column(name="IN_COLUMN_03")
	private String inColumn03;

	@Column(name="IN_COLUMN_04")
	private String inColumn04;

	@Column(name="IN_COLUMN_05")
	private String inColumn05;

	@Column(name="IN_COLUMN_06")
	private String inColumn06;

	@Column(name="IN_COLUMN_07")
	private String inColumn07;

	@Column(name="IN_COLUMN_08")
	private String inColumn08;

	@Column(name="IN_COLUMN_09")
	private String inColumn09;

	@Column(name="IN_COLUMN_10")
	private String inColumn10;

	@Column(name="IN_COLUMN_11")
	private String inColumn11;

	@Column(name="IN_COLUMN_12")
	private String inColumn12;

	@Column(name="IN_COLUMN_13")
	private String inColumn13;

	@Column(name="IN_COLUMN_14")
	private String inColumn14;

	@Column(name="IN_COLUMN_15")
	private String inColumn15;

	@Column(name="IN_COLUMN_16")
	private String inColumn16;

	@Column(name="IN_COLUMN_17")
	private String inColumn17;

	@Column(name="IN_COLUMN_18")
	private String inColumn18;

	@Column(name="IN_COLUMN_19")
	private String inColumn19;

	@Column(name="IN_COLUMN_20")
	private String inColumn20;

	@Column(name="OUT_COLUMN_TYPE_01")
	private String outColumnType01;

	@Column(name="OUT_COLUMN_TYPE_02")
	private String outColumnType02;

	@Column(name="OUT_COLUMN_TYPE_03")
	private String outColumnType03;

	@Column(name="OUT_COLUMN_TYPE_04")
	private String outColumnType04;

	@Column(name="OUT_COLUMN_TYPE_05")
	private String outColumnType05;

	@Column(name="OUT_COLUMN_TYPE_06")
	private String outColumnType06;

	@Column(name="OUT_COLUMN_TYPE_07")
	private String outColumnType07;

	@Column(name="OUT_COLUMN_TYPE_08")
	private String outColumnType08;

	@Column(name="OUT_COLUMN_TYPE_09")
	private String outColumnType09;

	@Column(name="OUT_COLUMN_TYPE_10")
	private String outColumnType10;

	@Column(name="OUT_COLUMN_NAME_01")
	private String outColumnName01;

	@Column(name="OUT_COLUMN_NAME_02")
	private String outColumnName02;

	@Column(name="OUT_COLUMN_NAME_03")
	private String outColumnName03;

	@Column(name="OUT_COLUMN_NAME_04")
	private String outColumnName04;

	@Column(name="OUT_COLUMN_NAME_05")
	private String outColumnName05;

	@Column(name="OUT_COLUMN_NAME_06")
	private String outColumnName06;

	@Column(name="OUT_COLUMN_NAME_07")
	private String outColumnName07;

	@Column(name="OUT_COLUMN_NAME_08")
	private String outColumnName08;

	@Column(name="OUT_COLUMN_NAME_09")
	private String outColumnName09;

	@Column(name="OUT_COLUMN_NAME_10")
	private String outColumnName10;

	@Column(name="PROCTABLE_ID")
	private Long proctableId;
	
	@Column(name="OUT_COLUMN_TABLE_01")
	private String outColumnTable01;

	@Column(name="OUT_COLUMN_TABLE_02")
	private String outColumnTable02;

	@Column(name="OUT_COLUMN_TABLE_03")
	private String outColumnTable03;

	@Column(name="OUT_COLUMN_TABLE_04")
	private String outColumnTable04;

	@Column(name="OUT_COLUMN_TABLE_05")
	private String outColumnTable05;

	@Column(name="OUT_COLUMN_TABLE_06")
	private String outColumnTable06;

	@Column(name="OUT_COLUMN_TABLE_07")
	private String outColumnTable07;

	@Column(name="OUT_COLUMN_TABLE_08")
	private String outColumnTable08;

	@Column(name="OUT_COLUMN_TABLE_09")
	private String outColumnTable09;

	@Column(name="OUT_COLUMN_TABLE_10")
	private String outColumnTable10;

    public ProctableDetail() {
    }
    
    // Idable interface
  	public Long getId() {
  		return getProctableDetailId();
  	}
  	
    public void setId(Long id) {
    	setProctableDetailId(id);
    }
     
    public String getIdPropertyName() {
 		return "proctableDetailId";
 	}

	public Long getProctableDetailId() {
		return this.proctableDetailId;
	}

	public void setProctableDetailId(Long proctableDetailId) {
		this.proctableDetailId = proctableDetailId;
	}

	public String getInColumn01() {
		return this.inColumn01;
	}

	public void setInColumn01(String inColumn01) {
		this.inColumn01 = inColumn01;
	}

	public String getInColumn02() {
		return this.inColumn02;
	}

	public void setInColumn02(String inColumn02) {
		this.inColumn02 = inColumn02;
	}

	public String getInColumn03() {
		return this.inColumn03;
	}

	public void setInColumn03(String inColumn03) {
		this.inColumn03 = inColumn03;
	}

	public String getInColumn04() {
		return this.inColumn04;
	}

	public void setInColumn04(String inColumn04) {
		this.inColumn04 = inColumn04;
	}

	public String getInColumn05() {
		return this.inColumn05;
	}

	public void setInColumn05(String inColumn05) {
		this.inColumn05 = inColumn05;
	}

	public String getInColumn06() {
		return this.inColumn06;
	}

	public void setInColumn06(String inColumn06) {
		this.inColumn06 = inColumn06;
	}

	public String getInColumn07() {
		return this.inColumn07;
	}

	public void setInColumn07(String inColumn07) {
		this.inColumn07 = inColumn07;
	}

	public String getInColumn08() {
		return this.inColumn08;
	}

	public void setInColumn08(String inColumn08) {
		this.inColumn08 = inColumn08;
	}

	public String getInColumn09() {
		return this.inColumn09;
	}

	public void setInColumn09(String inColumn09) {
		this.inColumn09 = inColumn09;
	}

	public String getInColumn10() {
		return this.inColumn10;
	}

	public void setInColumn10(String inColumn10) {
		this.inColumn10 = inColumn10;
	}

	public String getInColumn11() {
		return this.inColumn11;
	}

	public void setInColumn11(String inColumn11) {
		this.inColumn11 = inColumn11;
	}

	public String getInColumn12() {
		return this.inColumn12;
	}

	public void setInColumn12(String inColumn12) {
		this.inColumn12 = inColumn12;
	}

	public String getInColumn13() {
		return this.inColumn13;
	}

	public void setInColumn13(String inColumn13) {
		this.inColumn13 = inColumn13;
	}

	public String getInColumn14() {
		return this.inColumn14;
	}

	public void setInColumn14(String inColumn14) {
		this.inColumn14 = inColumn14;
	}

	public String getInColumn15() {
		return this.inColumn15;
	}

	public void setInColumn15(String inColumn15) {
		this.inColumn15 = inColumn15;
	}

	public String getInColumn16() {
		return this.inColumn16;
	}

	public void setInColumn16(String inColumn16) {
		this.inColumn16 = inColumn16;
	}

	public String getInColumn17() {
		return this.inColumn17;
	}

	public void setInColumn17(String inColumn17) {
		this.inColumn17 = inColumn17;
	}

	public String getInColumn18() {
		return this.inColumn18;
	}

	public void setInColumn18(String inColumn18) {
		this.inColumn18 = inColumn18;
	}

	public String getInColumn19() {
		return this.inColumn19;
	}

	public void setInColumn19(String inColumn19) {
		this.inColumn19 = inColumn19;
	}

	public String getInColumn20() {
		return this.inColumn20;
	}

	public void setInColumn20(String inColumn20) {
		this.inColumn20 = inColumn20;
	}

	public String getOutColumnType01() {
		return this.outColumnType01;
	}

	public void setOutColumnType01(String outColumnType01) {
		this.outColumnType01 = outColumnType01;
	}

	public String getOutColumnType02() {
		return this.outColumnType02;
	}

	public void setOutColumnType02(String outColumnType02) {
		this.outColumnType02 = outColumnType02;
	}

	public String getOutColumnType03() {
		return this.outColumnType03;
	}

	public void setOutColumnType03(String outColumnType03) {
		this.outColumnType03 = outColumnType03;
	}

	public String getOutColumnType04() {
		return this.outColumnType04;
	}

	public void setOutColumnType04(String outColumnType04) {
		this.outColumnType04 = outColumnType04;
	}

	public String getOutColumnType05() {
		return this.outColumnType05;
	}

	public void setOutColumnType05(String outColumnType05) {
		this.outColumnType05 = outColumnType05;
	}

	public String getOutColumnType06() {
		return this.outColumnType06;
	}

	public void setOutColumnType06(String outColumnType06) {
		this.outColumnType06 = outColumnType06;
	}

	public String getOutColumnType07() {
		return this.outColumnType07;
	}

	public void setOutColumnType07(String outColumnType07) {
		this.outColumnType07 = outColumnType07;
	}

	public String getOutColumnType08() {
		return this.outColumnType08;
	}

	public void setOutColumnType08(String outColumnType08) {
		this.outColumnType08 = outColumnType08;
	}

	public String getOutColumnType09() {
		return this.outColumnType09;
	}

	public void setOutColumnType09(String outColumnType09) {
		this.outColumnType09 = outColumnType09;
	}

	public String getOutColumnType10() {
		return this.outColumnType10;
	}

	public void setOutColumnType10(String outColumnType10) {
		this.outColumnType10 = outColumnType10;
	}

	public String getOutColumnName01() {
		return this.outColumnName01;
	}

	public void setOutColumnName01(String outColumnName01) {
		this.outColumnName01 = outColumnName01;
	}

	public String getOutColumnName02() {
		return this.outColumnName02;
	}

	public void setOutColumnName02(String outColumnName02) {
		this.outColumnName02 = outColumnName02;
	}

	public String getOutColumnName03() {
		return this.outColumnName03;
	}

	public void setOutColumnName03(String outColumnName03) {
		this.outColumnName03 = outColumnName03;
	}

	public String getOutColumnName04() {
		return this.outColumnName04;
	}

	public void setOutColumnName04(String outColumnName04) {
		this.outColumnName04 = outColumnName04;
	}

	public String getOutColumnName05() {
		return this.outColumnName05;
	}

	public void setOutColumnName05(String outColumnName05) {
		this.outColumnName05 = outColumnName05;
	}

	public String getOutColumnName06() {
		return this.outColumnName06;
	}

	public void setOutColumnName06(String outColumnName06) {
		this.outColumnName06 = outColumnName06;
	}

	public String getOutColumnName07() {
		return this.outColumnName07;
	}

	public void setOutColumnName07(String outColumnName07) {
		this.outColumnName07 = outColumnName07;
	}

	public String getOutColumnName08() {
		return this.outColumnName08;
	}

	public void setOutColumnName08(String outColumnName08) {
		this.outColumnName08 = outColumnName08;
	}

	public String getOutColumnName09() {
		return this.outColumnName09;
	}

	public void setOutColumnName09(String outColumnName09) {
		this.outColumnName09 = outColumnName09;
	}

	public String getOutColumnName10() {
		return this.outColumnName10;
	}

	public void setOutColumnName10(String outColumnName10) {
		this.outColumnName10 = outColumnName10;
	}

	public Long getProctableId() {
		return this.proctableId;
	}

	public void setProctableId(Long proctableId) {
		this.proctableId = proctableId;
	}
	
	public void setOutColumnTable01(String outColumnTable01) {
	    this.outColumnTable01 = outColumnTable01;
	}

	public String getOutColumnTable01() {
	    return this.outColumnTable01;
	}

	public void setOutColumnTable02(String outColumnTable02) {
	    this.outColumnTable02 = outColumnTable02;
	}

	public String getOutColumnTable02() {
	    return this.outColumnTable02;
	}

	public void setOutColumnTable03(String outColumnTable03) {
	    this.outColumnTable03 = outColumnTable03;
	}

	public String getOutColumnTable03() {
	    return this.outColumnTable03;
	}

	public void setOutColumnTable04(String outColumnTable04) {
	    this.outColumnTable04 = outColumnTable04;
	}

	public String getOutColumnTable04() {
	    return this.outColumnTable04;
	}

	public void setOutColumnTable05(String outColumnTable05) {
	    this.outColumnTable05 = outColumnTable05;
	}

	public String getOutColumnTable05() {
	    return this.outColumnTable05;
	}

	public void setOutColumnTable06(String outColumnTable06) {
	    this.outColumnTable06 = outColumnTable06;
	}

	public String getOutColumnTable06() {
	    return this.outColumnTable06;
	}

	public void setOutColumnTable07(String outColumnTable07) {
	    this.outColumnTable07 = outColumnTable07;
	}

	public String getOutColumnTable07() {
	    return this.outColumnTable07;
	}

	public void setOutColumnTable08(String outColumnTable08) {
	    this.outColumnTable08 = outColumnTable08;
	}

	public String getOutColumnTable08() {
	    return this.outColumnTable08;
	}

	public void setOutColumnTable09(String outColumnTable09) {
	    this.outColumnTable09 = outColumnTable09;
	}

	public String getOutColumnTable09() {
	    return this.outColumnTable09;
	}

	public void setOutColumnTable10(String outColumnTable10) {
	    this.outColumnTable10 = outColumnTable10;
	}

	public String getOutColumnTable10() {
	    return this.outColumnTable10;
	}
}