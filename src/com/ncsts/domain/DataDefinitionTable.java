
package com.ncsts.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DataDefinitionTableDTO;

/**
 *  @author Muneer
 */

@Entity
@Table (name="TB_DATA_DEF_TABLE")
public class DataDefinitionTable extends Auditable implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="TABLE_NAME")
	private String tableName;
    
	@Column(name="DESCRIPTION")
    private String description;
    
    @Column(name="DEFINITION" )
    private String definition;



	@Column(name="IMPORT_FLAG")
	private String importFlag;

    @Column(name="EXPORT_FLAG")
    private String exportFlag;
    @Column(name="CONFIG_FILETYPE_CODE")
    private String configFileTypeCode;
    @Column(name="IMPORT_INSERT_SQL")
    private String importInsertSql;
    //Midtier project code added - january 2009
    @OneToMany (cascade =(CascadeType.REMOVE), mappedBy ="dataDefinitionTable")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<DataDefinitionColumn> dataDefinitionColumn = new HashSet<DataDefinitionColumn>();
    
    /*@Transient	
    DataDefinitionTableDTO dataDefinitionTableDTO;
*/    // Constructors

    /** default constructor */
    public DataDefinitionTable() {
    }

	/** minimal constructor */
    public DataDefinitionTable(String tableName) {
    	this.tableName = tableName;
    }
    
    public DataDefinitionTableDTO getDataDefinitionTableDTO() {
    	DataDefinitionTableDTO dataDefinitionTableDTO= new DataDefinitionTableDTO();
    	BeanUtils.copyProperties(this,dataDefinitionTableDTO);
    	return dataDefinitionTableDTO;
    }

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<DataDefinitionColumn> getDataDefinitionColumn() {
		return dataDefinitionColumn;
	}

	public void setDataDefinitionColumn(
			Set<DataDefinitionColumn> dataDefinitionColumn) {
		this.dataDefinitionColumn = dataDefinitionColumn;
	}

	public String getExportFlag() {
		return this.exportFlag;
	}

	public void setExportFlag(String exportFlag) {
		this.exportFlag = exportFlag;
	}

	public String getImportFlag() {
		return importFlag;
	}

	public void setImportFlag(String importFlag) {
		this.importFlag = importFlag;
	}

	public String getConfigFileTypeCode() {
		return configFileTypeCode;
	}

	public void setConfigFileTypeCode(String configFileTypeCode) {
		this.configFileTypeCode = configFileTypeCode;
	}

	public String getImportInsertSql() {
		return importInsertSql;
	}

	public void setImportInsertSql(String importInsertSql) {
		this.importInsertSql = importInsertSql;
	}
	
}

