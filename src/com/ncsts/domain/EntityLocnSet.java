package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityLocnSetDTO;

@Entity
@Table(name="TB_ENTITY_LOCN_SET")
public class EntityLocnSet extends Auditable implements Serializable, Idable<Long>  {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="ENTITY_LOCN_SET_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="entity_locn_set_sequence")
	@SequenceGenerator(name="entity_locn_set_sequence" , allocationSize = 1, sequenceName="sq_tb_entity_locn_set_id")
    private Long entityLocnSetId;
	
	@Column(name="ENTITY_ID")
	private Long entityId;
	
	@Column(name="LOCN_SET_CODE")
	private String locnSetCode;
	
	@Column(name="ACTIVE_FLAG")    
    private String activeFlag; 
	
	@Column(name="NAME")
	private String name;
	
	@Transient	
	EntityLocnSetDTO entityLocnSetDTO;
	
	@Transient	
	private String locSetCode;
	
	@Transient	
	private String locName;
	
	@Transient	
	private String locNameDesc;

	
	public EntityLocnSetDTO getEntityLocnSetDTO() {
		EntityLocnSetDTO entityLocnSetDTO = new EntityLocnSetDTO();
		BeanUtils.copyProperties(this, entityLocnSetDTO);
		return entityLocnSetDTO;
	}
	
	public EntityLocnSet(){ 
    }

	public Long getEntityLocnSetId() {
		return entityLocnSetId;
	}

	public void setEntityLocnSetId(Long entityLocnSetId) {
		this.entityLocnSetId = entityLocnSetId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getLocnSetCode() {
		return locnSetCode;
	}

	public void setLocnSetCode(String locnSetCode) {
		this.locnSetCode = locnSetCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  
    
    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	@Override
	public Long getId() {
		return this.entityLocnSetId;
	}

	@Override
	public void setId(Long id) {
		this.entityLocnSetId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "entityLocnSetId";
	}

	public String getLocSetCode() {
		return locSetCode;
	}

	public void setLocSetCode(String locSetCode) {
		this.locSetCode = locSetCode;
	}

	public String getLocName() {
		return locName;
	}

	public void setLocName(String locName) {
		this.locName = locName;
	}

	public String getLocNameDesc() {
		return locNameDesc;
	}

	public void setLocNameDesc(String locNameDesc) {
		this.locNameDesc = locNameDesc;
	}
	

}
