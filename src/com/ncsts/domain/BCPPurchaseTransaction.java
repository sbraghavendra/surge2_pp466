package com.ncsts.domain;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.common.Util;
import com.ncsts.view.bean.ImportMapAddBean;
import com.ncsts.view.util.ArithmeticUtils;


@Entity
@Table(name="TB_BCP_PURCHTRANS")
public class BCPPurchaseTransaction implements java.io.Serializable, Idable<Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="BCP_PURCHTRANS_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="bcp_purchtrans_sequence")
	@SequenceGenerator(name="bcp_purchtrans_sequence" , allocationSize = 1, sequenceName="sq_tb_bcp_purchtrans_id")	
	private Long bcpPurchtransId;

	@Column(name="PROCESSTYPE_CODE")
	private String processtypeCode;

	@Column(name="BCP_TRANSACTION_ID")
	private Long bcpTransactionId;

	@Column(name="PROCESS_BATCH_NO")
	private Long processBatchNo;

	@Column(name="GL_EXTRACT_BATCH_NO")
	private Long glExtractBatchNo;

	@Column(name="ENTERED_DATE")
	private Date enteredDate;

	@Column(name="LOAD_TIMESTAMP")
	private Date loadTimestamp;

	@Column(name="TRANSACTION_STATE_CODE")
	private String transactionStateCode;

	@Column(name="AUTO_TRANSACTION_STATE_CODE")
	private String autoTransactionStateCode;

	@Column(name="TRANSACTION_COUNTRY_CODE")
	private String transactionCountryCode;

	@Column(name="AUTO_TRANSACTION_COUNTRY_CODE")
	private String autoTransactionCountryCode;

	@Column(name="TRANSACTION_IND")
	private String transactionInd;

	@Column(name="SUSPEND_IND")
	private String suspendInd;

	@Column(name="DIRECT_PAY_PERMIT_FLAG")
	private String directPayPermitFlag;

	@Column(name="CALCULATE_IND")
	private String calculateInd;

	@Column(name="METHOD_DELIVERY_CODE")
	private String methodDeliveryCode;

	@Column(name="RATETYPE_CODE")
	private String ratetypeCode;

	@Column(name="GEO_RECON_CODE")
	private String geoReconCode;

	@Column(name="ENTITY_ID")
	private Long entityId;

	@Column(name="ENTITY_CODE")
	private String entityCode;

	@Column(name="UNLOCK_ENTITY_ID")
	private Long unlockEntityId;

	@Column(name="CP_FILING_ENTITY_CODE")
	private String cpFilingEntityCode;

	@Column(name="ALLOCATION_MATRIX_ID")
	private Long allocationMatrixId;

	@Column(name="ALLOCATION_SUBTRANS_ID")
	private Long allocationSubtransId;

	@Column(name="TAX_ALLOC_MATRIX_ID")
	private Long taxAllocMatrixId;

	@Column(name="SPLIT_SUBTRANS_ID")
	private Long splitSubtransId;

	@Column(name="MULTI_TRANS_CODE")
	private String multiTransCode;

	@Column(name="TAXCODE_CODE")
	private String taxcodeCode;

	@Column(name="MANUAL_TAXCODE_IND")
	private String manualTaxcodeInd;

	@Column(name="TAX_MATRIX_ID")
	private Long taxMatrixId;

	@Column(name="PROCRULE_SESSION_ID")
	private String procruleSessionId;

	@Column(name="SOURCE_TRANSACTION_ID")
	private String sourceTransactionId;

	@Column(name="TRANSACTION_STATUS")
	private String transactionStatus;

	@Column(name="COMMENTS")
	private String comments;

	@Column(name="URL_LINK")
	private String urlLink;

	@Column(name="PROCESSING_NOTES")
	private String processingNotes;

	@Column(name="REJECT_FLAG")
	private String rejectFlag;

	@Column(name="GL_DATE")
	private Date glDate;

	@Column(name="GL_COMPANY_NBR")
	private String glCompanyNbr;

	@Column(name="GL_COMPANY_NAME")
	private String glCompanyName;

	@Column(name="GL_DIVISION_NBR")
	private String glDivisionNbr;

	@Column(name="GL_DIVISION_NAME")
	private String glDivisionName;

	@Column(name="GL_CC_NBR_DEPT_ID")
	private String glCcNbrDeptId;

	@Column(name="GL_CC_NBR_DEPT_NAME")
	private String glCcNbrDeptName;

	@Column(name="GL_LOCAL_ACCT_NBR")
	private String glLocalAcctNbr;

	@Column(name="GL_LOCAL_ACCT_NAME")
	private String glLocalAcctName;

	@Column(name="GL_LOCAL_SUB_ACCT_NBR")
	private String glLocalSubAcctNbr;

	@Column(name="GL_LOCAL_SUB_ACCT_NAME")
	private String glLocalSubAcctName;

	@Column(name="GL_FULL_ACCT_NBR")
	private String glFullAcctNbr;

	@Column(name="GL_FULL_ACCT_NAME")
	private String glFullAcctName;

	@Column(name="GL_LINE_ITM_DIST_AMT")
	private BigDecimal glLineItmDistAmt;

	@Column(name="PRODUCT_CODE")
	private String productCode;

	@Column(name="PRODUCT_NAME")
	private String productName;

	@Column(name="VENDOR_ID")
	private Long vendorId;

	@Column(name="VENDOR_CODE")
	private String vendorCode;

	@Column(name="VENDOR_NAME")
	private String vendorName;

	@Column(name="VENDOR_ADDRESS_LINE_1")
	private String vendorAddressLine1;

	@Column(name="VENDOR_ADDRESS_LINE_2")
	private String vendorAddressLine2;

	@Column(name="VENDOR_ADDRESS_CITY")
	private String vendorAddressCity;

	@Column(name="VENDOR_ADDRESS_COUNTY")
	private String vendorAddressCounty;

	@Column(name="VENDOR_ADDRESS_STATE")
	private String vendorAddressState;

	@Column(name="VENDOR_ADDRESS_ZIP")
	private String vendorAddressZip;

	@Column(name="VENDOR_ADDRESS_ZIPPLUS4")
	private String vendorAddressZipplus4;

	@Column(name="VENDOR_ADDRESS_COUNTRY")
	private String vendorAddressCountry;

	@Column(name="VENDOR_TYPE")
	private String vendorType;

	@Column(name="VENDOR_TYPE_NAME")
	private String vendorTypeName;

	@Column(name="INVOICE_NBR")
	private String invoiceNbr;

	@Column(name="INVOICE_DESC")
	private String invoiceDesc;

	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	@Column(name="INVOICE_FREIGHT_AMT")
	private BigDecimal invoiceFreightAmt;

	@Column(name="INVOICE_DISCOUNT_AMT")
	private BigDecimal invoiceDiscountAmt;

	@Column(name="INVOICE_TAX_AMT")
	private BigDecimal invoiceTaxAmt;

	@Column(name="INVOICE_TOTAL_AMT")
	private BigDecimal invoiceTotalAmt;

	@Column(name="INVOICE_TAX_FLG")
	private String invoiceTaxFlg;

	@Column(name="INVOICE_LINE_NBR")
	private String invoiceLineNbr;

	@Column(name="INVOICE_LINE_NAME")
	private String invoiceLineName;

	@Column(name="INVOICE_LINE_TYPE")
	private String invoiceLineType;

	@Column(name="INVOICE_LINE_TYPE_NAME")
	private String invoiceLineTypeName;

	@Column(name="INVOICE_LINE_AMT")
	private BigDecimal invoiceLineAmt;

	@Column(name="INVOICE_LINE_TAX")
	private BigDecimal invoiceLineTax;

	@Column(name="INVOICE_LINE_COUNT")
	private Long invoiceLineCount;

	@Column(name="INVOICE_LINE_WEIGHT")
	private Long invoiceLineWeight;

	@Column(name="INVOICE_LINE_FREIGHT_AMT")
	private BigDecimal invoiceLineFreightAmt;

	@Column(name="INVOICE_LINE_DISC_AMT")
	private BigDecimal invoiceLineDiscAmt;

	@Column(name="INVOICE_LINE_DISC_TYPE_CODE")
	private String invoiceLineDiscTypeCode;

	@Column(name="AFE_PROJECT_NBR")
	private String afeProjectNbr;

	@Column(name="AFE_PROJECT_NAME")
	private String afeProjectName;

	@Column(name="AFE_CATEGORY_NBR")
	private String afeCategoryNbr;

	@Column(name="AFE_CATEGORY_NAME")
	private String afeCategoryName;

	@Column(name="AFE_SUB_CAT_NBR")
	private String afeSubCatNbr;

	@Column(name="AFE_SUB_CAT_NAME")
	private String afeSubCatName;

	@Column(name="AFE_USE")
	private String afeUse;

	@Column(name="AFE_CONTRACT_TYPE")
	private String afeContractType;

	@Column(name="AFE_CONTRACT_STRUCTURE")
	private String afeContractStructure;

	@Column(name="AFE_PROPERTY_CAT")
	private String afePropertyCat;

	@Column(name="INVENTORY_NBR")
	private String inventoryNbr;

	@Column(name="INVENTORY_NAME")
	private String inventoryName;

	@Column(name="INVENTORY_CLASS")
	private String inventoryClass;

	@Column(name="INVENTORY_CLASS_NAME")
	private String inventoryClassName;

	@Column(name="PO_NBR")
	private String poNbr;

	@Column(name="PO_NAME")
	private String poName;

	@Column(name="PO_DATE")
	private Date poDate;

	@Column(name="PO_LINE_NBR")
	private String poLineNbr;

	@Column(name="PO_LINE_NAME")
	private String poLineName;

	@Column(name="PO_LINE_TYPE")
	private String poLineType;

	@Column(name="PO_LINE_TYPE_NAME")
	private String poLineTypeName;

	@Column(name="WO_NBR")
	private String woNbr;

	@Column(name="WO_NAME")
	private String woName;

	@Column(name="WO_DATE")
	private Date woDate;

	@Column(name="WO_TYPE")
	private String woType;

	@Column(name="WO_TYPE_DESC")
	private String woTypeDesc;

	@Column(name="WO_CLASS")
	private String woClass;

	@Column(name="WO_CLASS_DESC")
	private String woClassDesc;

	@Column(name="WO_ENTITY")
	private String woEntity;

	@Column(name="WO_ENTITY_DESC")
	private String woEntityDesc;

	@Column(name="WO_LINE_NBR")
	private String woLineNbr;

	@Column(name="WO_LINE_NAME")
	private String woLineName;

	@Column(name="WO_LINE_TYPE")
	private String woLineType;

	@Column(name="WO_LINE_TYPE_DESC")
	private String woLineTypeDesc;

	@Column(name="WO_SHUT_DOWN_CD")
	private String woShutDownCd;

	@Column(name="WO_SHUT_DOWN_CD_DESC")
	private String woShutDownCdDesc;

	@Column(name="VOUCHER_ID")
	private String voucherId;

	@Column(name="VOUCHER_NAME")
	private String voucherName;

	@Column(name="VOUCHER_DATE")
	private Date voucherDate;

	@Column(name="VOUCHER_LINE_NBR")
	private String voucherLineNbr;

	@Column(name="VOUCHER_LINE_DESC")
	private String voucherLineDesc;

	@Column(name="CHECK_NBR")
	private String checkNbr;

	@Column(name="CHECK_NO")
	private Long checkNo;

	@Column(name="CHECK_DATE")
	private Date checkDate;

	@Column(name="CHECK_AMT")
	private BigDecimal checkAmt;

	@Column(name="CHECK_DESC")
	private String checkDesc;

	@Column(name="GL_EXTRACT_FLAG")
	private String glExtractFlag;

	@Column(name="GL_EXTRACT_UPDATER")
	private String glExtractUpdater;

	@Column(name="GL_EXTRACT_TIMESTAMP")
	private Date glExtractTimestamp;

	@Column(name="TAXABLE_AMT")
	private BigDecimal taxableAmt;

	@Column(name="COUNTRY_TIER1_TAXABLE_AMT")
	private BigDecimal countryTier1TaxableAmt;

	@Column(name="COUNTRY_TIER2_TAXABLE_AMT")
	private BigDecimal countryTier2TaxableAmt;

	@Column(name="COUNTRY_TIER3_TAXABLE_AMT")
	private BigDecimal countryTier3TaxableAmt;

	@Column(name="STATE_TIER1_TAXABLE_AMT")
	private BigDecimal stateTier1TaxableAmt;

	@Column(name="STATE_TIER2_TAXABLE_AMT")
	private BigDecimal stateTier2TaxableAmt;

	@Column(name="STATE_TIER3_TAXABLE_AMT")
	private BigDecimal stateTier3TaxableAmt;

	@Column(name="COUNTY_TIER1_TAXABLE_AMT")
	private BigDecimal countyTier1TaxableAmt;

	@Column(name="COUNTY_TIER2_TAXABLE_AMT")
	private BigDecimal countyTier2TaxableAmt;

	@Column(name="COUNTY_TIER3_TAXABLE_AMT")
	private BigDecimal countyTier3TaxableAmt;

	@Column(name="CITY_TIER1_TAXABLE_AMT")
	private BigDecimal cityTier1TaxableAmt;

	@Column(name="CITY_TIER2_TAXABLE_AMT")
	private BigDecimal cityTier2TaxableAmt;

	@Column(name="CITY_TIER3_TAXABLE_AMT")
	private BigDecimal cityTier3TaxableAmt;

	@Column(name="STJ1_TAXABLE_AMT")
	private BigDecimal stj1TaxableAmt;

	@Column(name="STJ2_TAXABLE_AMT")
	private BigDecimal stj2TaxableAmt;

	@Column(name="STJ3_TAXABLE_AMT")
	private BigDecimal stj3TaxableAmt;

	@Column(name="STJ4_TAXABLE_AMT")
	private BigDecimal stj4TaxableAmt;

	@Column(name="STJ5_TAXABLE_AMT")
	private BigDecimal stj5TaxableAmt;

	@Column(name="STJ6_TAXABLE_AMT")
	private BigDecimal stj6TaxableAmt;

	@Column(name="STJ7_TAXABLE_AMT")
	private BigDecimal stj7TaxableAmt;

	@Column(name="STJ8_TAXABLE_AMT")
	private BigDecimal stj8TaxableAmt;

	@Column(name="STJ9_TAXABLE_AMT")
	private BigDecimal stj9TaxableAmt;

	@Column(name="STJ10_TAXABLE_AMT")
	private BigDecimal stj10TaxableAmt;

	@Column(name="EXEMPT_IND")
	private String exemptInd;

	@Column(name="EXEMPT_CODE")
	private String exemptCode;

	@Column(name="EXEMPT_AMT")
	private BigDecimal exemptAmt;

	@Column(name="COUNTRY_EXEMPT_AMT")
	private BigDecimal countryExemptAmt;

	@Column(name="STATE_EXEMPT_AMT")
	private BigDecimal stateExemptAmt;

	@Column(name="COUNTY_EXEMPT_AMT")
	private BigDecimal countyExemptAmt;

	@Column(name="CITY_EXEMPT_AMT")
	private BigDecimal cityExemptAmt;

	@Column(name="STJ1_EXEMPT_AMT")
	private BigDecimal stj1ExemptAmt;

	@Column(name="STJ2_EXEMPT_AMT")
	private BigDecimal stj2ExemptAmt;

	@Column(name="STJ3_EXEMPT_AMT")
	private BigDecimal stj3ExemptAmt;

	@Column(name="STJ4_EXEMPT_AMT")
	private BigDecimal stj4ExemptAmt;

	@Column(name="STJ5_EXEMPT_AMT")
	private BigDecimal stj5ExemptAmt;

	@Column(name="STJ6_EXEMPT_AMT")
	private BigDecimal stj6ExemptAmt;

	@Column(name="STJ7_EXEMPT_AMT")
	private BigDecimal stj7ExemptAmt;

	@Column(name="STJ8_EXEMPT_AMT")
	private BigDecimal stj8ExemptAmt;

	@Column(name="STJ9_EXEMPT_AMT")
	private BigDecimal stj9ExemptAmt;

	@Column(name="STJ10_EXEMPT_AMT")
	private BigDecimal stj10ExemptAmt;

	@Column(name="EXCLUSION_AMT")
	private BigDecimal exclusionAmt;

	@Column(name="TB_CALC_TAX_AMT")
	private BigDecimal tbCalcTaxAmt;

	@Column(name="COUNTRY_TIER1_TAX_AMT")
	private BigDecimal countryTier1TaxAmt;

	@Column(name="COUNTRY_TIER2_TAX_AMT")
	private BigDecimal countryTier2TaxAmt;

	@Column(name="COUNTRY_TIER3_TAX_AMT")
	private BigDecimal countryTier3TaxAmt;

	@Column(name="STATE_TIER1_TAX_AMT")
	private BigDecimal stateTier1TaxAmt;

	@Column(name="STATE_TIER2_TAX_AMT")
	private BigDecimal stateTier2TaxAmt;

	@Column(name="STATE_TIER3_TAX_AMT")
	private BigDecimal stateTier3TaxAmt;

	@Column(name="COUNTY_TIER1_TAX_AMT")
	private BigDecimal countyTier1TaxAmt;

	@Column(name="COUNTY_TIER2_TAX_AMT")
	private BigDecimal countyTier2TaxAmt;

	@Column(name="COUNTY_TIER3_TAX_AMT")
	private BigDecimal countyTier3TaxAmt;

	@Column(name="CITY_TIER1_TAX_AMT")
	private BigDecimal cityTier1TaxAmt;

	@Column(name="CITY_TIER2_TAX_AMT")
	private BigDecimal cityTier2TaxAmt;

	@Column(name="CITY_TIER3_TAX_AMT")
	private BigDecimal cityTier3TaxAmt;

	@Column(name="STJ1_TAX_AMT")
	private BigDecimal stj1TaxAmt;

	@Column(name="STJ2_TAX_AMT")
	private BigDecimal stj2TaxAmt;

	@Column(name="STJ3_TAX_AMT")
	private BigDecimal stj3TaxAmt;

	@Column(name="STJ4_TAX_AMT")
	private BigDecimal stj4TaxAmt;

	@Column(name="STJ5_TAX_AMT")
	private BigDecimal stj5TaxAmt;

	@Column(name="STJ6_TAX_AMT")
	private BigDecimal stj6TaxAmt;

	@Column(name="STJ7_TAX_AMT")
	private BigDecimal stj7TaxAmt;

	@Column(name="STJ8_TAX_AMT")
	private BigDecimal stj8TaxAmt;

	@Column(name="STJ9_TAX_AMT")
	private BigDecimal stj9TaxAmt;

	@Column(name="STJ10_TAX_AMT")
	private BigDecimal stj10TaxAmt;

	@Column(name="COUNTY_LOCAL_TAX_AMT")
	private BigDecimal countyLocalTaxAmt;

	@Column(name="CITY_LOCAL_TAX_AMT")
	private BigDecimal cityLocalTaxAmt;

	@Column(name="DISTR_01_AMT")
	private BigDecimal distr01Amt;

	@Column(name="DISTR_02_AMT")
	private BigDecimal distr02Amt;

	@Column(name="DISTR_03_AMT")
	private BigDecimal distr03Amt;

	@Column(name="DISTR_04_AMT")
	private BigDecimal distr04Amt;

	@Column(name="DISTR_05_AMT")
	private BigDecimal distr05Amt;

	@Column(name="DISTR_06_AMT")
	private BigDecimal distr06Amt;

	@Column(name="DISTR_07_AMT")
	private BigDecimal distr07Amt;

	@Column(name="DISTR_08_AMT")
	private BigDecimal distr08Amt;

	@Column(name="DISTR_09_AMT")
	private BigDecimal distr09Amt;

	@Column(name="DISTR_10_AMT")
	private BigDecimal distr10Amt;

	@Column(name="USER_TEXT_01")
	private String userText01;

	@Column(name="USER_TEXT_02")
	private String userText02;

	@Column(name="USER_TEXT_03")
	private String userText03;

	@Column(name="USER_TEXT_04")
	private String userText04;

	@Column(name="USER_TEXT_05")
	private String userText05;

	@Column(name="USER_TEXT_06")
	private String userText06;

	@Column(name="USER_TEXT_07")
	private String userText07;

	@Column(name="USER_TEXT_08")
	private String userText08;

	@Column(name="USER_TEXT_09")
	private String userText09;

	@Column(name="USER_TEXT_10")
	private String userText10;

	@Column(name="USER_TEXT_11")
	private String userText11;

	@Column(name="USER_TEXT_12")
	private String userText12;

	@Column(name="USER_TEXT_13")
	private String userText13;

	@Column(name="USER_TEXT_14")
	private String userText14;

	@Column(name="USER_TEXT_15")
	private String userText15;

	@Column(name="USER_TEXT_16")
	private String userText16;

	@Column(name="USER_TEXT_17")
	private String userText17;

	@Column(name="USER_TEXT_18")
	private String userText18;

	@Column(name="USER_TEXT_19")
	private String userText19;

	@Column(name="USER_TEXT_20")
	private String userText20;

	@Column(name="USER_TEXT_21")
	private String userText21;

	@Column(name="USER_TEXT_22")
	private String userText22;

	@Column(name="USER_TEXT_23")
	private String userText23;

	@Column(name="USER_TEXT_24")
	private String userText24;

	@Column(name="USER_TEXT_25")
	private String userText25;

	@Column(name="USER_TEXT_26")
	private String userText26;

	@Column(name="USER_TEXT_27")
	private String userText27;

	@Column(name="USER_TEXT_28")
	private String userText28;

	@Column(name="USER_TEXT_29")
	private String userText29;

	@Column(name="USER_TEXT_30")
	private String userText30;

	@Column(name="USER_TEXT_31")
	private String userText31;

	@Column(name="USER_TEXT_32")
	private String userText32;

	@Column(name="USER_TEXT_33")
	private String userText33;

	@Column(name="USER_TEXT_34")
	private String userText34;

	@Column(name="USER_TEXT_35")
	private String userText35;

	@Column(name="USER_TEXT_36")
	private String userText36;

	@Column(name="USER_TEXT_37")
	private String userText37;

	@Column(name="USER_TEXT_38")
	private String userText38;

	@Column(name="USER_TEXT_39")
	private String userText39;

	@Column(name="USER_TEXT_40")
	private String userText40;

	@Column(name="USER_TEXT_41")
	private String userText41;

	@Column(name="USER_TEXT_42")
	private String userText42;

	@Column(name="USER_TEXT_43")
	private String userText43;

	@Column(name="USER_TEXT_44")
	private String userText44;

	@Column(name="USER_TEXT_45")
	private String userText45;

	@Column(name="USER_TEXT_46")
	private String userText46;

	@Column(name="USER_TEXT_47")
	private String userText47;

	@Column(name="USER_TEXT_48")
	private String userText48;

	@Column(name="USER_TEXT_49")
	private String userText49;

	@Column(name="USER_TEXT_50")
	private String userText50;

	@Column(name="USER_TEXT_51")
	private String userText51;

	@Column(name="USER_TEXT_52")
	private String userText52;

	@Column(name="USER_TEXT_53")
	private String userText53;

	@Column(name="USER_TEXT_54")
	private String userText54;

	@Column(name="USER_TEXT_55")
	private String userText55;

	@Column(name="USER_TEXT_56")
	private String userText56;

	@Column(name="USER_TEXT_57")
	private String userText57;

	@Column(name="USER_TEXT_58")
	private String userText58;

	@Column(name="USER_TEXT_59")
	private String userText59;

	@Column(name="USER_TEXT_60")
	private String userText60;

	@Column(name="USER_TEXT_61")
	private String userText61;

	@Column(name="USER_TEXT_62")
	private String userText62;

	@Column(name="USER_TEXT_63")
	private String userText63;

	@Column(name="USER_TEXT_64")
	private String userText64;

	@Column(name="USER_TEXT_65")
	private String userText65;

	@Column(name="USER_TEXT_66")
	private String userText66;

	@Column(name="USER_TEXT_67")
	private String userText67;

	@Column(name="USER_TEXT_68")
	private String userText68;

	@Column(name="USER_TEXT_69")
	private String userText69;

	@Column(name="USER_TEXT_70")
	private String userText70;

	@Column(name="USER_TEXT_71")
	private String userText71;

	@Column(name="USER_TEXT_72")
	private String userText72;

	@Column(name="USER_TEXT_73")
	private String userText73;

	@Column(name="USER_TEXT_74")
	private String userText74;

	@Column(name="USER_TEXT_75")
	private String userText75;

	@Column(name="USER_TEXT_76")
	private String userText76;

	@Column(name="USER_TEXT_77")
	private String userText77;

	@Column(name="USER_TEXT_78")
	private String userText78;

	@Column(name="USER_TEXT_79")
	private String userText79;

	@Column(name="USER_TEXT_80")
	private String userText80;

	@Column(name="USER_TEXT_81")
	private String userText81;

	@Column(name="USER_TEXT_82")
	private String userText82;

	@Column(name="USER_TEXT_83")
	private String userText83;

	@Column(name="USER_TEXT_84")
	private String userText84;

	@Column(name="USER_TEXT_85")
	private String userText85;

	@Column(name="USER_TEXT_86")
	private String userText86;

	@Column(name="USER_TEXT_87")
	private String userText87;

	@Column(name="USER_TEXT_88")
	private String userText88;

	@Column(name="USER_TEXT_89")
	private String userText89;

	@Column(name="USER_TEXT_90")
	private String userText90;

	@Column(name="USER_TEXT_91")
	private String userText91;

	@Column(name="USER_TEXT_92")
	private String userText92;

	@Column(name="USER_TEXT_93")
	private String userText93;

	@Column(name="USER_TEXT_94")
	private String userText94;

	@Column(name="USER_TEXT_95")
	private String userText95;

	@Column(name="USER_TEXT_96")
	private String userText96;

	@Column(name="USER_TEXT_97")
	private String userText97;

	@Column(name="USER_TEXT_98")
	private String userText98;

	@Column(name="USER_TEXT_99")
	private String userText99;

	@Column(name="USER_TEXT_100")
	private String userText100;

	@Column(name="USER_NUMBER_01")
	private BigDecimal userNumber01;

	@Column(name="USER_NUMBER_02")
	private BigDecimal userNumber02;

	@Column(name="USER_NUMBER_03")
	private BigDecimal userNumber03;

	@Column(name="USER_NUMBER_04")
	private BigDecimal userNumber04;

	@Column(name="USER_NUMBER_05")
	private BigDecimal userNumber05;

	@Column(name="USER_NUMBER_06")
	private BigDecimal userNumber06;

	@Column(name="USER_NUMBER_07")
	private BigDecimal userNumber07;

	@Column(name="USER_NUMBER_08")
	private BigDecimal userNumber08;

	@Column(name="USER_NUMBER_09")
	private BigDecimal userNumber09;

	@Column(name="USER_NUMBER_10")
	private BigDecimal userNumber10;

	@Column(name="USER_NUMBER_11")
	private BigDecimal userNumber11;

	@Column(name="USER_NUMBER_12")
	private BigDecimal userNumber12;

	@Column(name="USER_NUMBER_13")
	private BigDecimal userNumber13;

	@Column(name="USER_NUMBER_14")
	private BigDecimal userNumber14;

	@Column(name="USER_NUMBER_15")
	private BigDecimal userNumber15;

	@Column(name="USER_NUMBER_16")
	private BigDecimal userNumber16;

	@Column(name="USER_NUMBER_17")
	private BigDecimal userNumber17;

	@Column(name="USER_NUMBER_18")
	private BigDecimal userNumber18;

	@Column(name="USER_NUMBER_19")
	private BigDecimal userNumber19;

	@Column(name="USER_NUMBER_20")
	private BigDecimal userNumber20;

	@Column(name="USER_NUMBER_21")
	private BigDecimal userNumber21;

	@Column(name="USER_NUMBER_22")
	private BigDecimal userNumber22;

	@Column(name="USER_NUMBER_23")
	private BigDecimal userNumber23;

	@Column(name="USER_NUMBER_24")
	private BigDecimal userNumber24;

	@Column(name="USER_NUMBER_25")
	private BigDecimal userNumber25;

	@Column(name="USER_DATE_01")
	private Date userDate01;

	@Column(name="USER_DATE_02")
	private Date userDate02;

	@Column(name="USER_DATE_03")
	private Date userDate03;

	@Column(name="USER_DATE_04")
	private Date userDate04;

	@Column(name="USER_DATE_05")
	private Date userDate05;

	@Column(name="USER_DATE_06")
	private Date userDate06;

	@Column(name="USER_DATE_07")
	private Date userDate07;

	@Column(name="USER_DATE_08")
	private Date userDate08;

	@Column(name="USER_DATE_09")
	private Date userDate09;

	@Column(name="USER_DATE_10")
	private Date userDate10;

	@Column(name="USER_DATE_11")
	private Date userDate11;

	@Column(name="USER_DATE_12")
	private Date userDate12;

	@Column(name="USER_DATE_13")
	private Date userDate13;

	@Column(name="USER_DATE_14")
	private Date userDate14;

	@Column(name="USER_DATE_15")
	private Date userDate15;

	@Column(name="USER_DATE_16")
	private Date userDate16;

	@Column(name="USER_DATE_17")
	private Date userDate17;

	@Column(name="USER_DATE_18")
	private Date userDate18;

	@Column(name="USER_DATE_19")
	private Date userDate19;

	@Column(name="USER_DATE_20")
	private Date userDate20;

	@Column(name="USER_DATE_21")
	private Date userDate21;

	@Column(name="USER_DATE_22")
	private Date userDate22;

	@Column(name="USER_DATE_23")
	private Date userDate23;

	@Column(name="USER_DATE_24")
	private Date userDate24;

	@Column(name="USER_DATE_25")
	private Date userDate25;

	@Column(name="SHIPTO_LOCATION")
	private String shiptoLocation;

	@Column(name="SHIPTO_LOCATION_NAME")
	private String shiptoLocationName;

	@Column(name="SHIPTO_MANUAL_JUR_IND")
	private String shiptoManualJurInd;

	@Column(name="SHIPTO_ENTITY_ID")
	private Long shiptoEntityId;

	@Column(name="SHIPTO_ENTITY_CODE")
	private String shiptoEntityCode;

	@Column(name="SHIPTO_LAT")
	private String shiptoLat;

	@Column(name="SHIPTO_LONG")
	private String shiptoLong;

	@Column(name="SHIPTO_LOCN_MATRIX_ID")
	private Long shiptoLocnMatrixId;

	@Column(name="SHIPTO_JURISDICTION_ID")
	private Long shiptoJurisdictionId;

	@Column(name="SHIPTO_GEOCODE")
	private String shiptoGeocode;

	@Column(name="SHIPTO_ADDRESS_LINE_1")
	private String shiptoAddressLine1;

	@Column(name="SHIPTO_ADDRESS_LINE_2")
	private String shiptoAddressLine2;

	@Column(name="SHIPTO_CITY")
	private String shiptoCity;

	@Column(name="SHIPTO_COUNTY")
	private String shiptoCounty;

	@Column(name="SHIPTO_STATE_CODE")
	private String shiptoStateCode;

	@Column(name="SHIPTO_ZIP")
	private String shiptoZip;

	@Column(name="SHIPTO_ZIPPLUS4")
	private String shiptoZipplus4;

	@Column(name="SHIPTO_COUNTRY_CODE")
	private String shiptoCountryCode;

	@Column(name="SHIPTO_STJ1_NAME")
	private String shiptoStj1Name;

	@Column(name="SHIPTO_STJ2_NAME")
	private String shiptoStj2Name;

	@Column(name="SHIPTO_STJ3_NAME")
	private String shiptoStj3Name;

	@Column(name="SHIPTO_STJ4_NAME")
	private String shiptoStj4Name;

	@Column(name="SHIPTO_STJ5_NAME")
	private String shiptoStj5Name;

	@Column(name="SHIPTO_COUNTRY_NEXUSIND_CODE")
	private String shiptoCountryNexusindCode;

	@Column(name="SHIPTO_STATE_NEXUSIND_CODE")
	private String shiptoStateNexusindCode;

	@Column(name="SHIPTO_COUNTY_NEXUSIND_CODE")
	private String shiptoCountyNexusindCode;

	@Column(name="SHIPTO_CITY_NEXUSIND_CODE")
	private String shiptoCityNexusindCode;

	@Column(name="SHIPTO_STJ1_NEXUSIND_CODE")
	private String shiptoStj1NexusindCode;

	@Column(name="SHIPTO_STJ2_NEXUSIND_CODE")
	private String shiptoStj2NexusindCode;

	@Column(name="SHIPTO_STJ3_NEXUSIND_CODE")
	private String shiptoStj3NexusindCode;

	@Column(name="SHIPTO_STJ4_NEXUSIND_CODE")
	private String shiptoStj4NexusindCode;

	@Column(name="SHIPTO_STJ5_NEXUSIND_CODE")
	private String shiptoStj5NexusindCode;

	@Column(name="SHIPFROM_ENTITY_ID")
	private Long shipfromEntityId;

	@Column(name="SHIPFROM_ENTITY_CODE")
	private String shipfromEntityCode;

	@Column(name="SHIPFROM_LAT")
	private String shipfromLat;

	@Column(name="SHIPFROM_LONG")
	private String shipfromLong;

	@Column(name="SHIPFROM_LOCN_MATRIX_ID")
	private Long shipfromLocnMatrixId;

	@Column(name="SHIPFROM_JURISDICTION_ID")
	private Long shipfromJurisdictionId;

	@Column(name="SHIPFROM_GEOCODE")
	private String shipfromGeocode;

	@Column(name="SHIPFROM_ADDRESS_LINE_1")
	private String shipfromAddressLine1;

	@Column(name="SHIPFROM_ADDRESS_LINE_2")
	private String shipfromAddressLine2;

	@Column(name="SHIPFROM_CITY")
	private String shipfromCity;

	@Column(name="SHIPFROM_COUNTY")
	private String shipfromCounty;

	@Column(name="SHIPFROM_STATE_CODE")
	private String shipfromStateCode;

	@Column(name="SHIPFROM_ZIP")
	private String shipfromZip;

	@Column(name="SHIPFROM_ZIPPLUS4")
	private String shipfromZipplus4;

	@Column(name="SHIPFROM_COUNTRY_CODE")
	private String shipfromCountryCode;

	@Column(name="SHIPFROM_STJ1_NAME")
	private String shipfromStj1Name;

	@Column(name="SHIPFROM_STJ2_NAME")
	private String shipfromStj2Name;

	@Column(name="SHIPFROM_STJ3_NAME")
	private String shipfromStj3Name;

	@Column(name="SHIPFROM_STJ4_NAME")
	private String shipfromStj4Name;

	@Column(name="SHIPFROM_STJ5_NAME")
	private String shipfromStj5Name;

	@Column(name="SHIPFROM_COUNTRY_NEXUSIND_CODE")
	private String shipfromCountryNexusindCode;

	@Column(name="SHIPFROM_STATE_NEXUSIND_CODE")
	private String shipfromStateNexusindCode;

	@Column(name="SHIPFROM_COUNTY_NEXUSIND_CODE")
	private String shipfromCountyNexusindCode;

	@Column(name="SHIPFROM_CITY_NEXUSIND_CODE")
	private String shipfromCityNexusindCode;

	@Column(name="SHIPFROM_STJ1_NEXUSIND_CODE")
	private String shipfromStj1NexusindCode;

	@Column(name="SHIPFROM_STJ2_NEXUSIND_CODE")
	private String shipfromStj2NexusindCode;

	@Column(name="SHIPFROM_STJ3_NEXUSIND_CODE")
	private String shipfromStj3NexusindCode;

	@Column(name="SHIPFROM_STJ4_NEXUSIND_CODE")
	private String shipfromStj4NexusindCode;

	@Column(name="SHIPFROM_STJ5_NEXUSIND_CODE")
	private String shipfromStj5NexusindCode;

	@Column(name="ORDRACPT_ENTITY_ID")
	private Long ordracptEntityId;

	@Column(name="ORDRACPT_ENTITY_CODE")
	private String ordracptEntityCode;

	@Column(name="ORDRACPT_LAT")
	private String ordracptLat;

	@Column(name="ORDRACPT_LONG")
	private String ordracptLong;

	@Column(name="ORDRACPT_LOCN_MATRIX_ID")
	private Long ordracptLocnMatrixId;

	@Column(name="ORDRACPT_JURISDICTION_ID")
	private Long ordracptJurisdictionId;

	@Column(name="ORDRACPT_GEOCODE")
	private String ordracptGeocode;

	@Column(name="ORDRACPT_ADDRESS_LINE_1")
	private String ordracptAddressLine1;

	@Column(name="ORDRACPT_ADDRESS_LINE_2")
	private String ordracptAddressLine2;

	@Column(name="ORDRACPT_CITY")
	private String ordracptCity;

	@Column(name="ORDRACPT_COUNTY")
	private String ordracptCounty;

	@Column(name="ORDRACPT_STATE_CODE")
	private String ordracptStateCode;

	@Column(name="ORDRACPT_ZIP")
	private String ordracptZip;

	@Column(name="ORDRACPT_ZIPPLUS4")
	private String ordracptZipplus4;

	@Column(name="ORDRACPT_COUNTRY_CODE")
	private String ordracptCountryCode;

	@Column(name="ORDRACPT_STJ1_NAME")
	private String ordracptStj1Name;

	@Column(name="ORDRACPT_STJ2_NAME")
	private String ordracptStj2Name;

	@Column(name="ORDRACPT_STJ3_NAME")
	private String ordracptStj3Name;

	@Column(name="ORDRACPT_STJ4_NAME")
	private String ordracptStj4Name;

	@Column(name="ORDRACPT_STJ5_NAME")
	private String ordracptStj5Name;

	@Column(name="ORDRACPT_COUNTRY_NEXUSIND_CODE")
	private String ordracptCountryNexusindCode;

	@Column(name="ORDRACPT_STATE_NEXUSIND_CODE")
	private String ordracptStateNexusindCode;

	@Column(name="ORDRACPT_COUNTY_NEXUSIND_CODE")
	private String ordracptCountyNexusindCode;

	@Column(name="ORDRACPT_CITY_NEXUSIND_CODE")
	private String ordracptCityNexusindCode;

	@Column(name="ORDRACPT_STJ1_NEXUSIND_CODE")
	private String ordracptStj1NexusindCode;

	@Column(name="ORDRACPT_STJ2_NEXUSIND_CODE")
	private String ordracptStj2NexusindCode;

	@Column(name="ORDRACPT_STJ3_NEXUSIND_CODE")
	private String ordracptStj3NexusindCode;

	@Column(name="ORDRACPT_STJ4_NEXUSIND_CODE")
	private String ordracptStj4NexusindCode;

	@Column(name="ORDRACPT_STJ5_NEXUSIND_CODE")
	private String ordracptStj5NexusindCode;

	@Column(name="ORDRORGN_ENTITY_ID")
	private Long ordrorgnEntityId;

	@Column(name="ORDRORGN_ENTITY_CODE")
	private String ordrorgnEntityCode;

	@Column(name="ORDRORGN_LAT")
	private String ordrorgnLat;

	@Column(name="ORDRORGN_LONG")
	private String ordrorgnLong;

	@Column(name="ORDRORGN_LOCN_MATRIX_ID")
	private Long ordrorgnLocnMatrixId;

	@Column(name="ORDRORGN_JURISDICTION_ID")
	private Long ordrorgnJurisdictionId;

	@Column(name="ORDRORGN_GEOCODE")
	private String ordrorgnGeocode;

	@Column(name="ORDRORGN_ADDRESS_LINE_1")
	private String ordrorgnAddressLine1;

	@Column(name="ORDRORGN_ADDRESS_LINE_2")
	private String ordrorgnAddressLine2;

	@Column(name="ORDRORGN_CITY")
	private String ordrorgnCity;

	@Column(name="ORDRORGN_COUNTY")
	private String ordrorgnCounty;

	@Column(name="ORDRORGN_STATE_CODE")
	private String ordrorgnStateCode;

	@Column(name="ORDRORGN_ZIP")
	private String ordrorgnZip;

	@Column(name="ORDRORGN_ZIPPLUS4")
	private String ordrorgnZipplus4;

	@Column(name="ORDRORGN_COUNTRY_CODE")
	private String ordrorgnCountryCode;

	@Column(name="ORDRORGN_STJ1_NAME")
	private String ordrorgnStj1Name;

	@Column(name="ORDRORGN_STJ2_NAME")
	private String ordrorgnStj2Name;

	@Column(name="ORDRORGN_STJ3_NAME")
	private String ordrorgnStj3Name;

	@Column(name="ORDRORGN_STJ4_NAME")
	private String ordrorgnStj4Name;

	@Column(name="ORDRORGN_STJ5_NAME")
	private String ordrorgnStj5Name;

	@Column(name="ORDRORGN_COUNTRY_NEXUSIND_CODE")
	private String ordrorgnCountryNexusindCode;

	@Column(name="ORDRORGN_STATE_NEXUSIND_CODE")
	private String ordrorgnStateNexusindCode;

	@Column(name="ORDRORGN_COUNTY_NEXUSIND_CODE")
	private String ordrorgnCountyNexusindCode;

	@Column(name="ORDRORGN_CITY_NEXUSIND_CODE")
	private String ordrorgnCityNexusindCode;

	@Column(name="ORDRORGN_STJ1_NEXUSIND_CODE")
	private String ordrorgnStj1NexusindCode;

	@Column(name="ORDRORGN_STJ2_NEXUSIND_CODE")
	private String ordrorgnStj2NexusindCode;

	@Column(name="ORDRORGN_STJ3_NEXUSIND_CODE")
	private String ordrorgnStj3NexusindCode;

	@Column(name="ORDRORGN_STJ4_NEXUSIND_CODE")
	private String ordrorgnStj4NexusindCode;

	@Column(name="ORDRORGN_STJ5_NEXUSIND_CODE")
	private String ordrorgnStj5NexusindCode;

	@Column(name="FIRSTUSE_ENTITY_ID")
	private Long firstuseEntityId;

	@Column(name="FIRSTUSE_ENTITY_CODE")
	private String firstuseEntityCode;

	@Column(name="FIRSTUSE_LAT")
	private String firstuseLat;

	@Column(name="FIRSTUSE_LONG")
	private String firstuseLong;

	@Column(name="FIRSTUSE_LOCN_MATRIX_ID")
	private Long firstuseLocnMatrixId;

	@Column(name="FIRSTUSE_JURISDICTION_ID")
	private Long firstuseJurisdictionId;

	@Column(name="FIRSTUSE_GEOCODE")
	private String firstuseGeocode;

	@Column(name="FIRSTUSE_ADDRESS_LINE_1")
	private String firstuseAddressLine1;

	@Column(name="FIRSTUSE_ADDRESS_LINE_2")
	private String firstuseAddressLine2;

	@Column(name="FIRSTUSE_CITY")
	private String firstuseCity;

	@Column(name="FIRSTUSE_COUNTY")
	private String firstuseCounty;

	@Column(name="FIRSTUSE_STATE_CODE")
	private String firstuseStateCode;

	@Column(name="FIRSTUSE_ZIP")
	private String firstuseZip;

	@Column(name="FIRSTUSE_ZIPPLUS4")
	private String firstuseZipplus4;

	@Column(name="FIRSTUSE_COUNTRY_CODE")
	private String firstuseCountryCode;

	@Column(name="FIRSTUSE_STJ1_NAME")
	private String firstuseStj1Name;

	@Column(name="FIRSTUSE_STJ2_NAME")
	private String firstuseStj2Name;

	@Column(name="FIRSTUSE_STJ3_NAME")
	private String firstuseStj3Name;

	@Column(name="FIRSTUSE_STJ4_NAME")
	private String firstuseStj4Name;

	@Column(name="FIRSTUSE_STJ5_NAME")
	private String firstuseStj5Name;

	@Column(name="FIRSTUSE_COUNTRY_NEXUSIND_CODE")
	private String firstuseCountryNexusindCode;

	@Column(name="FIRSTUSE_STATE_NEXUSIND_CODE")
	private String firstuseStateNexusindCode;

	@Column(name="FIRSTUSE_COUNTY_NEXUSIND_CODE")
	private String firstuseCountyNexusindCode;

	@Column(name="FIRSTUSE_CITY_NEXUSIND_CODE")
	private String firstuseCityNexusindCode;

	@Column(name="FIRSTUSE_STJ1_NEXUSIND_CODE")
	private String firstuseStj1NexusindCode;

	@Column(name="FIRSTUSE_STJ2_NEXUSIND_CODE")
	private String firstuseStj2NexusindCode;

	@Column(name="FIRSTUSE_STJ3_NEXUSIND_CODE")
	private String firstuseStj3NexusindCode;

	@Column(name="FIRSTUSE_STJ4_NEXUSIND_CODE")
	private String firstuseStj4NexusindCode;

	@Column(name="FIRSTUSE_STJ5_NEXUSIND_CODE")
	private String firstuseStj5NexusindCode;

	@Column(name="BILLTO_ENTITY_ID")
	private Long billtoEntityId;

	@Column(name="BILLTO_ENTITY_CODE")
	private String billtoEntityCode;

	@Column(name="BILLTO_LAT")
	private String billtoLat;

	@Column(name="BILLTO_LONG")
	private String billtoLong;

	@Column(name="BILLTO_LOCN_MATRIX_ID")
	private Long billtoLocnMatrixId;

	@Column(name="BILLTO_JURISDICTION_ID")
	private Long billtoJurisdictionId;

	@Column(name="BILLTO_GEOCODE")
	private String billtoGeocode;

	@Column(name="BILLTO_ADDRESS_LINE_1")
	private String billtoAddressLine1;

	@Column(name="BILLTO_ADDRESS_LINE_2")
	private String billtoAddressLine2;

	@Column(name="BILLTO_CITY")
	private String billtoCity;

	@Column(name="BILLTO_COUNTY")
	private String billtoCounty;

	@Column(name="BILLTO_STATE_CODE")
	private String billtoStateCode;

	@Column(name="BILLTO_ZIP")
	private String billtoZip;

	@Column(name="BILLTO_ZIPPLUS4")
	private String billtoZipplus4;

	@Column(name="BILLTO_COUNTRY_CODE")
	private String billtoCountryCode;

	@Column(name="BILLTO_STJ1_NAME")
	private String billtoStj1Name;

	@Column(name="BILLTO_STJ2_NAME")
	private String billtoStj2Name;

	@Column(name="BILLTO_STJ3_NAME")
	private String billtoStj3Name;

	@Column(name="BILLTO_STJ4_NAME")
	private String billtoStj4Name;

	@Column(name="BILLTO_STJ5_NAME")
	private String billtoStj5Name;

	@Column(name="BILLTO_COUNTRY_NEXUSIND_CODE")
	private String billtoCountryNexusindCode;

	@Column(name="BILLTO_STATE_NEXUSIND_CODE")
	private String billtoStateNexusindCode;

	@Column(name="BILLTO_COUNTY_NEXUSIND_CODE")
	private String billtoCountyNexusindCode;

	@Column(name="BILLTO_CITY_NEXUSIND_CODE")
	private String billtoCityNexusindCode;

	@Column(name="BILLTO_STJ1_NEXUSIND_CODE")
	private String billtoStj1NexusindCode;

	@Column(name="BILLTO_STJ2_NEXUSIND_CODE")
	private String billtoStj2NexusindCode;

	@Column(name="BILLTO_STJ3_NEXUSIND_CODE")
	private String billtoStj3NexusindCode;

	@Column(name="BILLTO_STJ4_NEXUSIND_CODE")
	private String billtoStj4NexusindCode;

	@Column(name="BILLTO_STJ5_NEXUSIND_CODE")
	private String billtoStj5NexusindCode;

	@Column(name="TTLXFR_ENTITY_ID")
	private Long ttlxfrEntityId;

	@Column(name="TTLXFR_ENTITY_CODE")
	private String ttlxfrEntityCode;

	@Column(name="TTLXFR_LAT")
	private String ttlxfrLat;

	@Column(name="TTLXFR_LONG")
	private String ttlxfrLong;

	@Column(name="TTLXFR_LOCN_MATRIX_ID")
	private Long ttlxfrLocnMatrixId;

	@Column(name="TTLXFR_JURISDICTION_ID")
	private Long ttlxfrJurisdictionId;

	@Column(name="TTLXFR_GEOCODE")
	private String ttlxfrGeocode;

	@Column(name="TTLXFR_ADDRESS_LINE_1")
	private String ttlxfrAddressLine1;

	@Column(name="TTLXFR_ADDRESS_LINE_2")
	private String ttlxfrAddressLine2;

	@Column(name="TTLXFR_CITY")
	private String ttlxfrCity;

	@Column(name="TTLXFR_COUNTY")
	private String ttlxfrCounty;

	@Column(name="TTLXFR_STATE_CODE")
	private String ttlxfrStateCode;

	@Column(name="TTLXFR_ZIP")
	private String ttlxfrZip;

	@Column(name="TTLXFR_ZIPPLUS4")
	private String ttlxfrZipplus4;

	@Column(name="TTLXFR_COUNTRY_CODE")
	private String ttlxfrCountryCode;

	@Column(name="TTLXFR_STJ1_NAME")
	private String ttlxfrStj1Name;

	@Column(name="TTLXFR_STJ2_NAME")
	private String ttlxfrStj2Name;

	@Column(name="TTLXFR_STJ3_NAME")
	private String ttlxfrStj3Name;

	@Column(name="TTLXFR_STJ4_NAME")
	private String ttlxfrStj4Name;

	@Column(name="TTLXFR_STJ5_NAME")
	private String ttlxfrStj5Name;

	@Column(name="TTLXFR_COUNTRY_NEXUSIND_CODE")
	private String ttlxfrCountryNexusindCode;

	@Column(name="TTLXFR_STATE_NEXUSIND_CODE")
	private String ttlxfrStateNexusindCode;

	@Column(name="TTLXFR_COUNTY_NEXUSIND_CODE")
	private String ttlxfrCountyNexusindCode;

	@Column(name="TTLXFR_CITY_NEXUSIND_CODE")
	private String ttlxfrCityNexusindCode;

	@Column(name="TTLXFR_STJ1_NEXUSIND_CODE")
	private String ttlxfrStj1NexusindCode;

	@Column(name="TTLXFR_STJ2_NEXUSIND_CODE")
	private String ttlxfrStj2NexusindCode;

	@Column(name="TTLXFR_STJ3_NEXUSIND_CODE")
	private String ttlxfrStj3NexusindCode;

	@Column(name="TTLXFR_STJ4_NEXUSIND_CODE")
	private String ttlxfrStj4NexusindCode;

	@Column(name="TTLXFR_STJ5_NEXUSIND_CODE")
	private String ttlxfrStj5NexusindCode;
////////////////////////////////////////////////
	
	@Transient
	private Long instanceCreatedId; 
	
	private static long nextId = 0L;
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
	/** default constructor */
	public BCPPurchaseTransaction(){
		instanceCreatedId = new Long(getNextId());
	}
	
	public Long getId() {
    	return bcpPurchtransId == null ? instanceCreatedId : bcpPurchtransId;
    }
    
    public void setId(Long id) {
    	this.bcpPurchtransId = id;
    }

    public String getIdPropertyName() {
    	return "bcpPurchtransId";
    }

	////////////////////////////////////////
    public void setBcpPurchtransId(Long bcpPurchtransId){
        this.bcpPurchtransId = bcpPurchtransId;
    }

    public Long getBcpPurchtransId(){
        return bcpPurchtransId;
    }

    public void setProcesstypeCode(String processtypeCode){
        this.processtypeCode = processtypeCode;
    }

    public String getProcesstypeCode(){
        return processtypeCode;
    }

    public void setBcpTransactionId(Long bcpTransactionId){
        this.bcpTransactionId = bcpTransactionId;
    }

    public Long getBcpTransactionId(){
        return bcpTransactionId;
    }

    public void setProcessBatchNo(Long processBatchNo){
        this.processBatchNo = processBatchNo;
    }

    public Long getProcessBatchNo(){
        return processBatchNo;
    }

    public void setGlExtractBatchNo(Long glExtractBatchNo){
        this.glExtractBatchNo = glExtractBatchNo;
    }

    public Long getGlExtractBatchNo(){
        return glExtractBatchNo;
    }

    public void setEnteredDate(Date enteredDate){
        this.enteredDate = enteredDate;
    }

    public Date getEnteredDate(){
        return enteredDate;
    }

    public void setLoadTimestamp(Date loadTimestamp){
        this.loadTimestamp = loadTimestamp;
    }

    public Date getLoadTimestamp(){
        return loadTimestamp;
    }

    public void setTransactionStateCode(String transactionStateCode){
        this.transactionStateCode = transactionStateCode;
    }

    public String getTransactionStateCode(){
        return transactionStateCode;
    }

    public void setAutoTransactionStateCode(String autoTransactionStateCode){
        this.autoTransactionStateCode = autoTransactionStateCode;
    }

    public String getAutoTransactionStateCode(){
        return autoTransactionStateCode;
    }

    public void setTransactionCountryCode(String transactionCountryCode){
        this.transactionCountryCode = transactionCountryCode;
    }

    public String getTransactionCountryCode(){
        return transactionCountryCode;
    }

    public void setAutoTransactionCountryCode(String autoTransactionCountryCode){
        this.autoTransactionCountryCode = autoTransactionCountryCode;
    }

    public String getAutoTransactionCountryCode(){
        return autoTransactionCountryCode;
    }

    public void setTransactionInd(String transactionInd){
        this.transactionInd = transactionInd;
    }

    public String getTransactionInd(){
        return transactionInd;
    }

    public void setSuspendInd(String suspendInd){
        this.suspendInd = suspendInd;
    }

    public String getSuspendInd(){
        return suspendInd;
    }

    public void setDirectPayPermitFlag(String directPayPermitFlag){
        this.directPayPermitFlag = directPayPermitFlag;
    }

    public String getDirectPayPermitFlag(){
        return directPayPermitFlag;
    }

    public void setCalculateInd(String calculateInd){
        this.calculateInd = calculateInd;
    }

    public String getCalculateInd(){
        return calculateInd;
    }

    public void setMethodDeliveryCode(String methodDeliveryCode){
        this.methodDeliveryCode = methodDeliveryCode;
    }

    public String getMethodDeliveryCode(){
        return methodDeliveryCode;
    }

    public void setRatetypeCode(String ratetypeCode){
        this.ratetypeCode = ratetypeCode;
    }

    public String getRatetypeCode(){
        return ratetypeCode;
    }

    public void setGeoReconCode(String geoReconCode){
        this.geoReconCode = geoReconCode;
    }

    public String getGeoReconCode(){
        return geoReconCode;
    }

    public void setEntityId(Long entityId){
        this.entityId = entityId;
    }

    public Long getEntityId(){
        return entityId;
    }

    public void setEntityCode(String entityCode){
        this.entityCode = entityCode;
    }

    public String getEntityCode(){
        return entityCode;
    }

    public void setUnlockEntityId(Long unlockEntityId){
        this.unlockEntityId = unlockEntityId;
    }

    public Long getUnlockEntityId(){
        return unlockEntityId;
    }

    public void setCpFilingEntityCode(String cpFilingEntityCode){
        this.cpFilingEntityCode = cpFilingEntityCode;
    }

    public String getCpFilingEntityCode(){
        return cpFilingEntityCode;
    }

    public void setAllocationMatrixId(Long allocationMatrixId){
        this.allocationMatrixId = allocationMatrixId;
    }

    public Long getAllocationMatrixId(){
        return allocationMatrixId;
    }

    public void setAllocationSubtransId(Long allocationSubtransId){
        this.allocationSubtransId = allocationSubtransId;
    }

    public Long getAllocationSubtransId(){
        return allocationSubtransId;
    }

    public void setTaxAllocMatrixId(Long taxAllocMatrixId){
        this.taxAllocMatrixId = taxAllocMatrixId;
    }

    public Long getTaxAllocMatrixId(){
        return taxAllocMatrixId;
    }

    public void setSplitSubtransId(Long splitSubtransId){
        this.splitSubtransId = splitSubtransId;
    }

    public Long getSplitSubtransId(){
        return splitSubtransId;
    }

    public void setMultiTransCode(String multiTransCode){
        this.multiTransCode = multiTransCode;
    }

    public String getMultiTransCode(){
        return multiTransCode;
    }

    public void setTaxcodeCode(String taxcodeCode){
        this.taxcodeCode = taxcodeCode;
    }

    public String getTaxcodeCode(){
        return taxcodeCode;
    }

    public void setManualTaxcodeInd(String manualTaxcodeInd){
        this.manualTaxcodeInd = manualTaxcodeInd;
    }

    public String getManualTaxcodeInd(){
        return manualTaxcodeInd;
    }

    public void setTaxMatrixId(Long taxMatrixId){
        this.taxMatrixId = taxMatrixId;
    }

    public Long getTaxMatrixId(){
        return taxMatrixId;
    }

    public void setProcruleSessionId(String procruleSessionId){
        this.procruleSessionId = procruleSessionId;
    }

    public String getProcruleSessionId(){
        return procruleSessionId;
    }

    public void setSourceTransactionId(String sourceTransactionId){
        this.sourceTransactionId = sourceTransactionId;
    }

    public String getSourceTransactionId(){
        return sourceTransactionId;
    }

    public void setTransactionStatus(String transactionStatus){
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatus(){
        return transactionStatus;
    }

    public void setComments(String comments){
        this.comments = comments;
    }

    public String getComments(){
        return comments;
    }

    public void setUrlLink(String urlLink){
        this.urlLink = urlLink;
    }

    public String getUrlLink(){
        return urlLink;
    }

    public void setProcessingNotes(String processingNotes){
        this.processingNotes = processingNotes;
    }

    public String getProcessingNotes(){
        return processingNotes;
    }

    public void setRejectFlag(String rejectFlag){
        this.rejectFlag = rejectFlag;
    }

    public String getRejectFlag(){
        return rejectFlag;
    }

    public void setGlDate(Date glDate){
        this.glDate = glDate;
    }

    public Date getGlDate(){
        return glDate;
    }

    public void setGlCompanyNbr(String glCompanyNbr){
        this.glCompanyNbr = glCompanyNbr;
    }

    public String getGlCompanyNbr(){
        return glCompanyNbr;
    }

    public void setGlCompanyName(String glCompanyName){
        this.glCompanyName = glCompanyName;
    }

    public String getGlCompanyName(){
        return glCompanyName;
    }

    public void setGlDivisionNbr(String glDivisionNbr){
        this.glDivisionNbr = glDivisionNbr;
    }

    public String getGlDivisionNbr(){
        return glDivisionNbr;
    }

    public void setGlDivisionName(String glDivisionName){
        this.glDivisionName = glDivisionName;
    }

    public String getGlDivisionName(){
        return glDivisionName;
    }

    public void setGlCcNbrDeptId(String glCcNbrDeptId){
        this.glCcNbrDeptId = glCcNbrDeptId;
    }

    public String getGlCcNbrDeptId(){
        return glCcNbrDeptId;
    }

    public void setGlCcNbrDeptName(String glCcNbrDeptName){
        this.glCcNbrDeptName = glCcNbrDeptName;
    }

    public String getGlCcNbrDeptName(){
        return glCcNbrDeptName;
    }

    public void setGlLocalAcctNbr(String glLocalAcctNbr){
        this.glLocalAcctNbr = glLocalAcctNbr;
    }

    public String getGlLocalAcctNbr(){
        return glLocalAcctNbr;
    }

    public void setGlLocalAcctName(String glLocalAcctName){
        this.glLocalAcctName = glLocalAcctName;
    }

    public String getGlLocalAcctName(){
        return glLocalAcctName;
    }

    public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr){
        this.glLocalSubAcctNbr = glLocalSubAcctNbr;
    }

    public String getGlLocalSubAcctNbr(){
        return glLocalSubAcctNbr;
    }

    public void setGlLocalSubAcctName(String glLocalSubAcctName){
        this.glLocalSubAcctName = glLocalSubAcctName;
    }

    public String getGlLocalSubAcctName(){
        return glLocalSubAcctName;
    }

    public void setGlFullAcctNbr(String glFullAcctNbr){
        this.glFullAcctNbr = glFullAcctNbr;
    }

    public String getGlFullAcctNbr(){
        return glFullAcctNbr;
    }

    public void setGlFullAcctName(String glFullAcctName){
        this.glFullAcctName = glFullAcctName;
    }

    public String getGlFullAcctName(){
        return glFullAcctName;
    }

    public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt){
        this.glLineItmDistAmt = glLineItmDistAmt;
    }

    public BigDecimal getGlLineItmDistAmt(){
        return glLineItmDistAmt;
    }

    public void setProductCode(String productCode){
        this.productCode = productCode;
    }

    public String getProductCode(){
        return productCode;
    }

    public void setProductName(String productName){
        this.productName = productName;
    }

    public String getProductName(){
        return productName;
    }

    public void setVendorId(Long vendorId){
        this.vendorId = vendorId;
    }

    public Long getVendorId(){
        return vendorId;
    }

    public void setVendorCode(String vendorCode){
        this.vendorCode = vendorCode;
    }

    public String getVendorCode(){
        return vendorCode;
    }

    public void setVendorName(String vendorName){
        this.vendorName = vendorName;
    }

    public String getVendorName(){
        return vendorName;
    }

    public void setVendorAddressLine1(String vendorAddressLine1){
        this.vendorAddressLine1 = vendorAddressLine1;
    }

    public String getVendorAddressLine1(){
        return vendorAddressLine1;
    }

    public void setVendorAddressLine2(String vendorAddressLine2){
        this.vendorAddressLine2 = vendorAddressLine2;
    }

    public String getVendorAddressLine2(){
        return vendorAddressLine2;
    }

    public void setVendorAddressCity(String vendorAddressCity){
        this.vendorAddressCity = vendorAddressCity;
    }

    public String getVendorAddressCity(){
        return vendorAddressCity;
    }

    public void setVendorAddressCounty(String vendorAddressCounty){
        this.vendorAddressCounty = vendorAddressCounty;
    }

    public String getVendorAddressCounty(){
        return vendorAddressCounty;
    }

    public void setVendorAddressState(String vendorAddressState){
        this.vendorAddressState = vendorAddressState;
    }

    public String getVendorAddressState(){
        return vendorAddressState;
    }

    public void setVendorAddressZip(String vendorAddressZip){
        this.vendorAddressZip = vendorAddressZip;
    }

    public String getVendorAddressZip(){
        return vendorAddressZip;
    }

    public void setVendorAddressZipplus4(String vendorAddressZipplus4){
        this.vendorAddressZipplus4 = vendorAddressZipplus4;
    }

    public String getVendorAddressZipplus4(){
        return vendorAddressZipplus4;
    }

    public void setVendorAddressCountry(String vendorAddressCountry){
        this.vendorAddressCountry = vendorAddressCountry;
    }

    public String getVendorAddressCountry(){
        return vendorAddressCountry;
    }

    public void setVendorType(String vendorType){
        this.vendorType = vendorType;
    }

    public String getVendorType(){
        return vendorType;
    }

    public void setVendorTypeName(String vendorTypeName){
        this.vendorTypeName = vendorTypeName;
    }

    public String getVendorTypeName(){
        return vendorTypeName;
    }

    public void setInvoiceNbr(String invoiceNbr){
        this.invoiceNbr = invoiceNbr;
    }

    public String getInvoiceNbr(){
        return invoiceNbr;
    }

    public void setInvoiceDesc(String invoiceDesc){
        this.invoiceDesc = invoiceDesc;
    }

    public String getInvoiceDesc(){
        return invoiceDesc;
    }

    public void setInvoiceDate(Date invoiceDate){
        this.invoiceDate = invoiceDate;
    }

    public Date getInvoiceDate(){
        return invoiceDate;
    }

    public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt){
        this.invoiceFreightAmt = invoiceFreightAmt;
    }

    public BigDecimal getInvoiceFreightAmt(){
        return invoiceFreightAmt;
    }

    public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt){
        this.invoiceDiscountAmt = invoiceDiscountAmt;
    }

    public BigDecimal getInvoiceDiscountAmt(){
        return invoiceDiscountAmt;
    }

    public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt){
        this.invoiceTaxAmt = invoiceTaxAmt;
    }

    public BigDecimal getInvoiceTaxAmt(){
        return invoiceTaxAmt;
    }

    public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt){
        this.invoiceTotalAmt = invoiceTotalAmt;
    }

    public BigDecimal getInvoiceTotalAmt(){
        return invoiceTotalAmt;
    }

    public void setInvoiceTaxFlg(String invoiceTaxFlg){
        this.invoiceTaxFlg = invoiceTaxFlg;
    }

    public String getInvoiceTaxFlg(){
        return invoiceTaxFlg;
    }

    public void setInvoiceLineNbr(String invoiceLineNbr){
        this.invoiceLineNbr = invoiceLineNbr;
    }

    public String getInvoiceLineNbr(){
        return invoiceLineNbr;
    }

    public void setInvoiceLineName(String invoiceLineName){
        this.invoiceLineName = invoiceLineName;
    }

    public String getInvoiceLineName(){
        return invoiceLineName;
    }

    public void setInvoiceLineType(String invoiceLineType){
        this.invoiceLineType = invoiceLineType;
    }

    public String getInvoiceLineType(){
        return invoiceLineType;
    }

    public void setInvoiceLineTypeName(String invoiceLineTypeName){
        this.invoiceLineTypeName = invoiceLineTypeName;
    }

    public String getInvoiceLineTypeName(){
        return invoiceLineTypeName;
    }

    public void setInvoiceLineAmt(BigDecimal invoiceLineAmt){
        this.invoiceLineAmt = invoiceLineAmt;
    }

    public BigDecimal getInvoiceLineAmt(){
        return invoiceLineAmt;
    }

    public void setInvoiceLineTax(BigDecimal invoiceLineTax){
        this.invoiceLineTax = invoiceLineTax;
    }

    public BigDecimal getInvoiceLineTax(){
        return invoiceLineTax;
    }

    public void setInvoiceLineCount(Long invoiceLineCount){
        this.invoiceLineCount = invoiceLineCount;
    }

    public Long getInvoiceLineCount(){
        return invoiceLineCount;
    }

    public void setInvoiceLineWeight(Long invoiceLineWeight){
        this.invoiceLineWeight = invoiceLineWeight;
    }

    public Long getInvoiceLineWeight(){
        return invoiceLineWeight;
    }

    public void setInvoiceLineFreightAmt(BigDecimal invoiceLineFreightAmt){
        this.invoiceLineFreightAmt = invoiceLineFreightAmt;
    }

    public BigDecimal getInvoiceLineFreightAmt(){
        return invoiceLineFreightAmt;
    }

    public void setInvoiceLineDiscAmt(BigDecimal invoiceLineDiscAmt){
        this.invoiceLineDiscAmt = invoiceLineDiscAmt;
    }

    public BigDecimal getInvoiceLineDiscAmt(){
        return invoiceLineDiscAmt;
    }

    public void setInvoiceLineDiscTypeCode(String invoiceLineDiscTypeCode){
        this.invoiceLineDiscTypeCode = invoiceLineDiscTypeCode;
    }

    public String getInvoiceLineDiscTypeCode(){
        return invoiceLineDiscTypeCode;
    }

    public void setAfeProjectNbr(String afeProjectNbr){
        this.afeProjectNbr = afeProjectNbr;
    }

    public String getAfeProjectNbr(){
        return afeProjectNbr;
    }

    public void setAfeProjectName(String afeProjectName){
        this.afeProjectName = afeProjectName;
    }

    public String getAfeProjectName(){
        return afeProjectName;
    }

    public void setAfeCategoryNbr(String afeCategoryNbr){
        this.afeCategoryNbr = afeCategoryNbr;
    }

    public String getAfeCategoryNbr(){
        return afeCategoryNbr;
    }

    public void setAfeCategoryName(String afeCategoryName){
        this.afeCategoryName = afeCategoryName;
    }

    public String getAfeCategoryName(){
        return afeCategoryName;
    }

    public void setAfeSubCatNbr(String afeSubCatNbr){
        this.afeSubCatNbr = afeSubCatNbr;
    }

    public String getAfeSubCatNbr(){
        return afeSubCatNbr;
    }

    public void setAfeSubCatName(String afeSubCatName){
        this.afeSubCatName = afeSubCatName;
    }

    public String getAfeSubCatName(){
        return afeSubCatName;
    }

    public void setAfeUse(String afeUse){
        this.afeUse = afeUse;
    }

    public String getAfeUse(){
        return afeUse;
    }

    public void setAfeContractType(String afeContractType){
        this.afeContractType = afeContractType;
    }

    public String getAfeContractType(){
        return afeContractType;
    }

    public void setAfeContractStructure(String afeContractStructure){
        this.afeContractStructure = afeContractStructure;
    }

    public String getAfeContractStructure(){
        return afeContractStructure;
    }

    public void setAfePropertyCat(String afePropertyCat){
        this.afePropertyCat = afePropertyCat;
    }

    public String getAfePropertyCat(){
        return afePropertyCat;
    }

    public void setInventoryNbr(String inventoryNbr){
        this.inventoryNbr = inventoryNbr;
    }

    public String getInventoryNbr(){
        return inventoryNbr;
    }

    public void setInventoryName(String inventoryName){
        this.inventoryName = inventoryName;
    }

    public String getInventoryName(){
        return inventoryName;
    }

    public void setInventoryClass(String inventoryClass){
        this.inventoryClass = inventoryClass;
    }

    public String getInventoryClass(){
        return inventoryClass;
    }

    public void setInventoryClassName(String inventoryClassName){
        this.inventoryClassName = inventoryClassName;
    }

    public String getInventoryClassName(){
        return inventoryClassName;
    }

    public void setPoNbr(String poNbr){
        this.poNbr = poNbr;
    }

    public String getPoNbr(){
        return poNbr;
    }

    public void setPoName(String poName){
        this.poName = poName;
    }

    public String getPoName(){
        return poName;
    }

    public void setPoDate(Date poDate){
        this.poDate = poDate;
    }

    public Date getPoDate(){
        return poDate;
    }

    public void setPoLineNbr(String poLineNbr){
        this.poLineNbr = poLineNbr;
    }

    public String getPoLineNbr(){
        return poLineNbr;
    }

    public void setPoLineName(String poLineName){
        this.poLineName = poLineName;
    }

    public String getPoLineName(){
        return poLineName;
    }

    public void setPoLineType(String poLineType){
        this.poLineType = poLineType;
    }

    public String getPoLineType(){
        return poLineType;
    }

    public void setPoLineTypeName(String poLineTypeName){
        this.poLineTypeName = poLineTypeName;
    }

    public String getPoLineTypeName(){
        return poLineTypeName;
    }

    public void setWoNbr(String woNbr){
        this.woNbr = woNbr;
    }

    public String getWoNbr(){
        return woNbr;
    }

    public void setWoName(String woName){
        this.woName = woName;
    }

    public String getWoName(){
        return woName;
    }

    public void setWoDate(Date woDate){
        this.woDate = woDate;
    }

    public Date getWoDate(){
        return woDate;
    }

    public void setWoType(String woType){
        this.woType = woType;
    }

    public String getWoType(){
        return woType;
    }

    public void setWoTypeDesc(String woTypeDesc){
        this.woTypeDesc = woTypeDesc;
    }

    public String getWoTypeDesc(){
        return woTypeDesc;
    }

    public void setWoClass(String woClass){
        this.woClass = woClass;
    }

    public String getWoClass(){
        return woClass;
    }

    public void setWoClassDesc(String woClassDesc){
        this.woClassDesc = woClassDesc;
    }

    public String getWoClassDesc(){
        return woClassDesc;
    }

    public void setWoEntity(String woEntity){
        this.woEntity = woEntity;
    }

    public String getWoEntity(){
        return woEntity;
    }

    public void setWoEntityDesc(String woEntityDesc){
        this.woEntityDesc = woEntityDesc;
    }

    public String getWoEntityDesc(){
        return woEntityDesc;
    }

    public void setWoLineNbr(String woLineNbr){
        this.woLineNbr = woLineNbr;
    }

    public String getWoLineNbr(){
        return woLineNbr;
    }

    public void setWoLineName(String woLineName){
        this.woLineName = woLineName;
    }

    public String getWoLineName(){
        return woLineName;
    }

    public void setWoLineType(String woLineType){
        this.woLineType = woLineType;
    }

    public String getWoLineType(){
        return woLineType;
    }

    public void setWoLineTypeDesc(String woLineTypeDesc){
        this.woLineTypeDesc = woLineTypeDesc;
    }

    public String getWoLineTypeDesc(){
        return woLineTypeDesc;
    }

    public void setWoShutDownCd(String woShutDownCd){
        this.woShutDownCd = woShutDownCd;
    }

    public String getWoShutDownCd(){
        return woShutDownCd;
    }

    public void setWoShutDownCdDesc(String woShutDownCdDesc){
        this.woShutDownCdDesc = woShutDownCdDesc;
    }

    public String getWoShutDownCdDesc(){
        return woShutDownCdDesc;
    }

    public void setVoucherId(String voucherId){
        this.voucherId = voucherId;
    }

    public String getVoucherId(){
        return voucherId;
    }

    public void setVoucherName(String voucherName){
        this.voucherName = voucherName;
    }

    public String getVoucherName(){
        return voucherName;
    }

    public void setVoucherDate(Date voucherDate){
        this.voucherDate = voucherDate;
    }

    public Date getVoucherDate(){
        return voucherDate;
    }

    public void setVoucherLineNbr(String voucherLineNbr){
        this.voucherLineNbr = voucherLineNbr;
    }

    public String getVoucherLineNbr(){
        return voucherLineNbr;
    }

    public void setVoucherLineDesc(String voucherLineDesc){
        this.voucherLineDesc = voucherLineDesc;
    }

    public String getVoucherLineDesc(){
        return voucherLineDesc;
    }

    public void setCheckNbr(String checkNbr){
        this.checkNbr = checkNbr;
    }

    public String getCheckNbr(){
        return checkNbr;
    }

    public void setCheckNo(Long checkNo){
        this.checkNo = checkNo;
    }

    public Long getCheckNo(){
        return checkNo;
    }

    public void setCheckDate(Date checkDate){
        this.checkDate = checkDate;
    }

    public Date getCheckDate(){
        return checkDate;
    }

    public void setCheckAmt(BigDecimal checkAmt){
        this.checkAmt = checkAmt;
    }

    public BigDecimal getCheckAmt(){
        return checkAmt;
    }

    public void setCheckDesc(String checkDesc){
        this.checkDesc = checkDesc;
    }

    public String getCheckDesc(){
        return checkDesc;
    }

    public void setGlExtractFlag(String glExtractFlag){
        this.glExtractFlag = glExtractFlag;
    }

    public String getGlExtractFlag(){
        return glExtractFlag;
    }

    public void setGlExtractUpdater(String glExtractUpdater){
        this.glExtractUpdater = glExtractUpdater;
    }

    public String getGlExtractUpdater(){
        return glExtractUpdater;
    }

    public void setGlExtractTimestamp(Date glExtractTimestamp){
        this.glExtractTimestamp = glExtractTimestamp;
    }

    public Date getGlExtractTimestamp(){
        return glExtractTimestamp;
    }

    public void setTaxableAmt(BigDecimal taxableAmt){
        this.taxableAmt = taxableAmt;
    }

    public BigDecimal getTaxableAmt(){
        return taxableAmt;
    }

    public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt){
        this.countryTier1TaxableAmt = countryTier1TaxableAmt;
    }

    public BigDecimal getCountryTier1TaxableAmt(){
        return countryTier1TaxableAmt;
    }

    public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt){
        this.countryTier2TaxableAmt = countryTier2TaxableAmt;
    }

    public BigDecimal getCountryTier2TaxableAmt(){
        return countryTier2TaxableAmt;
    }

    public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt){
        this.countryTier3TaxableAmt = countryTier3TaxableAmt;
    }

    public BigDecimal getCountryTier3TaxableAmt(){
        return countryTier3TaxableAmt;
    }

    public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt){
        this.stateTier1TaxableAmt = stateTier1TaxableAmt;
    }

    public BigDecimal getStateTier1TaxableAmt(){
        return stateTier1TaxableAmt;
    }

    public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt){
        this.stateTier2TaxableAmt = stateTier2TaxableAmt;
    }

    public BigDecimal getStateTier2TaxableAmt(){
        return stateTier2TaxableAmt;
    }

    public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt){
        this.stateTier3TaxableAmt = stateTier3TaxableAmt;
    }

    public BigDecimal getStateTier3TaxableAmt(){
        return stateTier3TaxableAmt;
    }

    public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt){
        this.countyTier1TaxableAmt = countyTier1TaxableAmt;
    }

    public BigDecimal getCountyTier1TaxableAmt(){
        return countyTier1TaxableAmt;
    }

    public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt){
        this.countyTier2TaxableAmt = countyTier2TaxableAmt;
    }

    public BigDecimal getCountyTier2TaxableAmt(){
        return countyTier2TaxableAmt;
    }

    public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt){
        this.countyTier3TaxableAmt = countyTier3TaxableAmt;
    }

    public BigDecimal getCountyTier3TaxableAmt(){
        return countyTier3TaxableAmt;
    }

    public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt){
        this.cityTier1TaxableAmt = cityTier1TaxableAmt;
    }

    public BigDecimal getCityTier1TaxableAmt(){
        return cityTier1TaxableAmt;
    }

    public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt){
        this.cityTier2TaxableAmt = cityTier2TaxableAmt;
    }

    public BigDecimal getCityTier2TaxableAmt(){
        return cityTier2TaxableAmt;
    }

    public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt){
        this.cityTier3TaxableAmt = cityTier3TaxableAmt;
    }

    public BigDecimal getCityTier3TaxableAmt(){
        return cityTier3TaxableAmt;
    }

    public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt){
        this.stj1TaxableAmt = stj1TaxableAmt;
    }

    public BigDecimal getStj1TaxableAmt(){
        return stj1TaxableAmt;
    }

    public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt){
        this.stj2TaxableAmt = stj2TaxableAmt;
    }

    public BigDecimal getStj2TaxableAmt(){
        return stj2TaxableAmt;
    }

    public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt){
        this.stj3TaxableAmt = stj3TaxableAmt;
    }

    public BigDecimal getStj3TaxableAmt(){
        return stj3TaxableAmt;
    }

    public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt){
        this.stj4TaxableAmt = stj4TaxableAmt;
    }

    public BigDecimal getStj4TaxableAmt(){
        return stj4TaxableAmt;
    }

    public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt){
        this.stj5TaxableAmt = stj5TaxableAmt;
    }

    public BigDecimal getStj5TaxableAmt(){
        return stj5TaxableAmt;
    }

    public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt){
        this.stj6TaxableAmt = stj6TaxableAmt;
    }

    public BigDecimal getStj6TaxableAmt(){
        return stj6TaxableAmt;
    }

    public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt){
        this.stj7TaxableAmt = stj7TaxableAmt;
    }

    public BigDecimal getStj7TaxableAmt(){
        return stj7TaxableAmt;
    }

    public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt){
        this.stj8TaxableAmt = stj8TaxableAmt;
    }

    public BigDecimal getStj8TaxableAmt(){
        return stj8TaxableAmt;
    }

    public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt){
        this.stj9TaxableAmt = stj9TaxableAmt;
    }

    public BigDecimal getStj9TaxableAmt(){
        return stj9TaxableAmt;
    }

    public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt){
        this.stj10TaxableAmt = stj10TaxableAmt;
    }

    public BigDecimal getStj10TaxableAmt(){
        return stj10TaxableAmt;
    }

    public void setExemptInd(String exemptInd){
        this.exemptInd = exemptInd;
    }

    public String getExemptInd(){
        return exemptInd;
    }

    public void setExemptCode(String exemptCode){
        this.exemptCode = exemptCode;
    }

    public String getExemptCode(){
        return exemptCode;
    }

    public void setExemptAmt(BigDecimal exemptAmt){
        this.exemptAmt = exemptAmt;
    }

    public BigDecimal getExemptAmt(){
        return exemptAmt;
    }

    public void setCountryExemptAmt(BigDecimal countryExemptAmt){
        this.countryExemptAmt = countryExemptAmt;
    }

    public BigDecimal getCountryExemptAmt(){
        return countryExemptAmt;
    }

    public void setStateExemptAmt(BigDecimal stateExemptAmt){
        this.stateExemptAmt = stateExemptAmt;
    }

    public BigDecimal getStateExemptAmt(){
        return stateExemptAmt;
    }

    public void setCountyExemptAmt(BigDecimal countyExemptAmt){
        this.countyExemptAmt = countyExemptAmt;
    }

    public BigDecimal getCountyExemptAmt(){
        return countyExemptAmt;
    }

    public void setCityExemptAmt(BigDecimal cityExemptAmt){
        this.cityExemptAmt = cityExemptAmt;
    }

    public BigDecimal getCityExemptAmt(){
        return cityExemptAmt;
    }

    public void setStj1ExemptAmt(BigDecimal stj1ExemptAmt){
        this.stj1ExemptAmt = stj1ExemptAmt;
    }

    public BigDecimal getStj1ExemptAmt(){
        return stj1ExemptAmt;
    }

    public void setStj2ExemptAmt(BigDecimal stj2ExemptAmt){
        this.stj2ExemptAmt = stj2ExemptAmt;
    }

    public BigDecimal getStj2ExemptAmt(){
        return stj2ExemptAmt;
    }

    public void setStj3ExemptAmt(BigDecimal stj3ExemptAmt){
        this.stj3ExemptAmt = stj3ExemptAmt;
    }

    public BigDecimal getStj3ExemptAmt(){
        return stj3ExemptAmt;
    }

    public void setStj4ExemptAmt(BigDecimal stj4ExemptAmt){
        this.stj4ExemptAmt = stj4ExemptAmt;
    }

    public BigDecimal getStj4ExemptAmt(){
        return stj4ExemptAmt;
    }

    public void setStj5ExemptAmt(BigDecimal stj5ExemptAmt){
        this.stj5ExemptAmt = stj5ExemptAmt;
    }

    public BigDecimal getStj5ExemptAmt(){
        return stj5ExemptAmt;
    }

    public void setStj6ExemptAmt(BigDecimal stj6ExemptAmt){
        this.stj6ExemptAmt = stj6ExemptAmt;
    }

    public BigDecimal getStj6ExemptAmt(){
        return stj6ExemptAmt;
    }

    public void setStj7ExemptAmt(BigDecimal stj7ExemptAmt){
        this.stj7ExemptAmt = stj7ExemptAmt;
    }

    public BigDecimal getStj7ExemptAmt(){
        return stj7ExemptAmt;
    }

    public void setStj8ExemptAmt(BigDecimal stj8ExemptAmt){
        this.stj8ExemptAmt = stj8ExemptAmt;
    }

    public BigDecimal getStj8ExemptAmt(){
        return stj8ExemptAmt;
    }

    public void setStj9ExemptAmt(BigDecimal stj9ExemptAmt){
        this.stj9ExemptAmt = stj9ExemptAmt;
    }

    public BigDecimal getStj9ExemptAmt(){
        return stj9ExemptAmt;
    }

    public void setStj10ExemptAmt(BigDecimal stj10ExemptAmt){
        this.stj10ExemptAmt = stj10ExemptAmt;
    }

    public BigDecimal getStj10ExemptAmt(){
        return stj10ExemptAmt;
    }

    public void setExclusionAmt(BigDecimal exclusionAmt){
        this.exclusionAmt = exclusionAmt;
    }

    public BigDecimal getExclusionAmt(){
        return exclusionAmt;
    }

    public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt){
        this.tbCalcTaxAmt = tbCalcTaxAmt;
    }

    public BigDecimal getTbCalcTaxAmt(){
        return tbCalcTaxAmt;
    }

    public void setCountryTier1TaxAmt(BigDecimal countryTier1TaxAmt){
        this.countryTier1TaxAmt = countryTier1TaxAmt;
    }

    public BigDecimal getCountryTier1TaxAmt(){
        return countryTier1TaxAmt;
    }

    public void setCountryTier2TaxAmt(BigDecimal countryTier2TaxAmt){
        this.countryTier2TaxAmt = countryTier2TaxAmt;
    }

    public BigDecimal getCountryTier2TaxAmt(){
        return countryTier2TaxAmt;
    }

    public void setCountryTier3TaxAmt(BigDecimal countryTier3TaxAmt){
        this.countryTier3TaxAmt = countryTier3TaxAmt;
    }

    public BigDecimal getCountryTier3TaxAmt(){
        return countryTier3TaxAmt;
    }

    public void setStateTier1TaxAmt(BigDecimal stateTier1TaxAmt){
        this.stateTier1TaxAmt = stateTier1TaxAmt;
    }

    public BigDecimal getStateTier1TaxAmt(){
        return stateTier1TaxAmt;
    }

    public void setStateTier2TaxAmt(BigDecimal stateTier2TaxAmt){
        this.stateTier2TaxAmt = stateTier2TaxAmt;
    }

    public BigDecimal getStateTier2TaxAmt(){
        return stateTier2TaxAmt;
    }

    public void setStateTier3TaxAmt(BigDecimal stateTier3TaxAmt){
        this.stateTier3TaxAmt = stateTier3TaxAmt;
    }

    public BigDecimal getStateTier3TaxAmt(){
        return stateTier3TaxAmt;
    }

    public void setCountyTier1TaxAmt(BigDecimal countyTier1TaxAmt){
        this.countyTier1TaxAmt = countyTier1TaxAmt;
    }

    public BigDecimal getCountyTier1TaxAmt(){
        return countyTier1TaxAmt;
    }

    public void setCountyTier2TaxAmt(BigDecimal countyTier2TaxAmt){
        this.countyTier2TaxAmt = countyTier2TaxAmt;
    }

    public BigDecimal getCountyTier2TaxAmt(){
        return countyTier2TaxAmt;
    }

    public void setCountyTier3TaxAmt(BigDecimal countyTier3TaxAmt){
        this.countyTier3TaxAmt = countyTier3TaxAmt;
    }

    public BigDecimal getCountyTier3TaxAmt(){
        return countyTier3TaxAmt;
    }

    public void setCityTier1TaxAmt(BigDecimal cityTier1TaxAmt){
        this.cityTier1TaxAmt = cityTier1TaxAmt;
    }

    public BigDecimal getCityTier1TaxAmt(){
        return cityTier1TaxAmt;
    }

    public void setCityTier2TaxAmt(BigDecimal cityTier2TaxAmt){
        this.cityTier2TaxAmt = cityTier2TaxAmt;
    }

    public BigDecimal getCityTier2TaxAmt(){
        return cityTier2TaxAmt;
    }

    public void setCityTier3TaxAmt(BigDecimal cityTier3TaxAmt){
        this.cityTier3TaxAmt = cityTier3TaxAmt;
    }

    public BigDecimal getCityTier3TaxAmt(){
        return cityTier3TaxAmt;
    }

    public void setStj1TaxAmt(BigDecimal stj1TaxAmt){
        this.stj1TaxAmt = stj1TaxAmt;
    }

    public BigDecimal getStj1TaxAmt(){
        return stj1TaxAmt;
    }

    public void setStj2TaxAmt(BigDecimal stj2TaxAmt){
        this.stj2TaxAmt = stj2TaxAmt;
    }

    public BigDecimal getStj2TaxAmt(){
        return stj2TaxAmt;
    }

    public void setStj3TaxAmt(BigDecimal stj3TaxAmt){
        this.stj3TaxAmt = stj3TaxAmt;
    }

    public BigDecimal getStj3TaxAmt(){
        return stj3TaxAmt;
    }

    public void setStj4TaxAmt(BigDecimal stj4TaxAmt){
        this.stj4TaxAmt = stj4TaxAmt;
    }

    public BigDecimal getStj4TaxAmt(){
        return stj4TaxAmt;
    }

    public void setStj5TaxAmt(BigDecimal stj5TaxAmt){
        this.stj5TaxAmt = stj5TaxAmt;
    }

    public BigDecimal getStj5TaxAmt(){
        return stj5TaxAmt;
    }

    public void setStj6TaxAmt(BigDecimal stj6TaxAmt){
        this.stj6TaxAmt = stj6TaxAmt;
    }

    public BigDecimal getStj6TaxAmt(){
        return stj6TaxAmt;
    }

    public void setStj7TaxAmt(BigDecimal stj7TaxAmt){
        this.stj7TaxAmt = stj7TaxAmt;
    }

    public BigDecimal getStj7TaxAmt(){
        return stj7TaxAmt;
    }

    public void setStj8TaxAmt(BigDecimal stj8TaxAmt){
        this.stj8TaxAmt = stj8TaxAmt;
    }

    public BigDecimal getStj8TaxAmt(){
        return stj8TaxAmt;
    }

    public void setStj9TaxAmt(BigDecimal stj9TaxAmt){
        this.stj9TaxAmt = stj9TaxAmt;
    }

    public BigDecimal getStj9TaxAmt(){
        return stj9TaxAmt;
    }

    public void setStj10TaxAmt(BigDecimal stj10TaxAmt){
        this.stj10TaxAmt = stj10TaxAmt;
    }

    public BigDecimal getStj10TaxAmt(){
        return stj10TaxAmt;
    }

    public void setCountyLocalTaxAmt(BigDecimal countyLocalTaxAmt){
        this.countyLocalTaxAmt = countyLocalTaxAmt;
    }

    public BigDecimal getCountyLocalTaxAmt(){
        return countyLocalTaxAmt;
    }

    public void setCityLocalTaxAmt(BigDecimal cityLocalTaxAmt){
        this.cityLocalTaxAmt = cityLocalTaxAmt;
    }

    public BigDecimal getCityLocalTaxAmt(){
        return cityLocalTaxAmt;
    }

    public void setDistr01Amt(BigDecimal distr01Amt){
        this.distr01Amt = distr01Amt;
    }

    public BigDecimal getDistr01Amt(){
        return distr01Amt;
    }

    public void setDistr02Amt(BigDecimal distr02Amt){
        this.distr02Amt = distr02Amt;
    }

    public BigDecimal getDistr02Amt(){
        return distr02Amt;
    }

    public void setDistr03Amt(BigDecimal distr03Amt){
        this.distr03Amt = distr03Amt;
    }

    public BigDecimal getDistr03Amt(){
        return distr03Amt;
    }

    public void setDistr04Amt(BigDecimal distr04Amt){
        this.distr04Amt = distr04Amt;
    }

    public BigDecimal getDistr04Amt(){
        return distr04Amt;
    }

    public void setDistr05Amt(BigDecimal distr05Amt){
        this.distr05Amt = distr05Amt;
    }

    public BigDecimal getDistr05Amt(){
        return distr05Amt;
    }

    public void setDistr06Amt(BigDecimal distr06Amt){
        this.distr06Amt = distr06Amt;
    }

    public BigDecimal getDistr06Amt(){
        return distr06Amt;
    }

    public void setDistr07Amt(BigDecimal distr07Amt){
        this.distr07Amt = distr07Amt;
    }

    public BigDecimal getDistr07Amt(){
        return distr07Amt;
    }

    public void setDistr08Amt(BigDecimal distr08Amt){
        this.distr08Amt = distr08Amt;
    }

    public BigDecimal getDistr08Amt(){
        return distr08Amt;
    }

    public void setDistr09Amt(BigDecimal distr09Amt){
        this.distr09Amt = distr09Amt;
    }

    public BigDecimal getDistr09Amt(){
        return distr09Amt;
    }

    public void setDistr10Amt(BigDecimal distr10Amt){
        this.distr10Amt = distr10Amt;
    }

    public BigDecimal getDistr10Amt(){
        return distr10Amt;
    }

    public void setUserText01(String userText01){
        this.userText01 = userText01;
    }

    public String getUserText01(){
        return userText01;
    }

    public void setUserText02(String userText02){
        this.userText02 = userText02;
    }

    public String getUserText02(){
        return userText02;
    }

    public void setUserText03(String userText03){
        this.userText03 = userText03;
    }

    public String getUserText03(){
        return userText03;
    }

    public void setUserText04(String userText04){
        this.userText04 = userText04;
    }

    public String getUserText04(){
        return userText04;
    }

    public void setUserText05(String userText05){
        this.userText05 = userText05;
    }

    public String getUserText05(){
        return userText05;
    }

    public void setUserText06(String userText06){
        this.userText06 = userText06;
    }

    public String getUserText06(){
        return userText06;
    }

    public void setUserText07(String userText07){
        this.userText07 = userText07;
    }

    public String getUserText07(){
        return userText07;
    }

    public void setUserText08(String userText08){
        this.userText08 = userText08;
    }

    public String getUserText08(){
        return userText08;
    }

    public void setUserText09(String userText09){
        this.userText09 = userText09;
    }

    public String getUserText09(){
        return userText09;
    }

    public void setUserText10(String userText10){
        this.userText10 = userText10;
    }

    public String getUserText10(){
        return userText10;
    }

    public void setUserText11(String userText11){
        this.userText11 = userText11;
    }

    public String getUserText11(){
        return userText11;
    }

    public void setUserText12(String userText12){
        this.userText12 = userText12;
    }

    public String getUserText12(){
        return userText12;
    }

    public void setUserText13(String userText13){
        this.userText13 = userText13;
    }

    public String getUserText13(){
        return userText13;
    }

    public void setUserText14(String userText14){
        this.userText14 = userText14;
    }

    public String getUserText14(){
        return userText14;
    }

    public void setUserText15(String userText15){
        this.userText15 = userText15;
    }

    public String getUserText15(){
        return userText15;
    }

    public void setUserText16(String userText16){
        this.userText16 = userText16;
    }

    public String getUserText16(){
        return userText16;
    }

    public void setUserText17(String userText17){
        this.userText17 = userText17;
    }

    public String getUserText17(){
        return userText17;
    }

    public void setUserText18(String userText18){
        this.userText18 = userText18;
    }

    public String getUserText18(){
        return userText18;
    }

    public void setUserText19(String userText19){
        this.userText19 = userText19;
    }

    public String getUserText19(){
        return userText19;
    }

    public void setUserText20(String userText20){
        this.userText20 = userText20;
    }

    public String getUserText20(){
        return userText20;
    }

    public void setUserText21(String userText21){
        this.userText21 = userText21;
    }

    public String getUserText21(){
        return userText21;
    }

    public void setUserText22(String userText22){
        this.userText22 = userText22;
    }

    public String getUserText22(){
        return userText22;
    }

    public void setUserText23(String userText23){
        this.userText23 = userText23;
    }

    public String getUserText23(){
        return userText23;
    }

    public void setUserText24(String userText24){
        this.userText24 = userText24;
    }

    public String getUserText24(){
        return userText24;
    }

    public void setUserText25(String userText25){
        this.userText25 = userText25;
    }

    public String getUserText25(){
        return userText25;
    }

    public void setUserText26(String userText26){
        this.userText26 = userText26;
    }

    public String getUserText26(){
        return userText26;
    }

    public void setUserText27(String userText27){
        this.userText27 = userText27;
    }

    public String getUserText27(){
        return userText27;
    }

    public void setUserText28(String userText28){
        this.userText28 = userText28;
    }

    public String getUserText28(){
        return userText28;
    }

    public void setUserText29(String userText29){
        this.userText29 = userText29;
    }

    public String getUserText29(){
        return userText29;
    }

    public void setUserText30(String userText30){
        this.userText30 = userText30;
    }

    public String getUserText30(){
        return userText30;
    }

    public void setUserText31(String userText31){
        this.userText31 = userText31;
    }

    public String getUserText31(){
        return userText31;
    }

    public void setUserText32(String userText32){
        this.userText32 = userText32;
    }

    public String getUserText32(){
        return userText32;
    }

    public void setUserText33(String userText33){
        this.userText33 = userText33;
    }

    public String getUserText33(){
        return userText33;
    }

    public void setUserText34(String userText34){
        this.userText34 = userText34;
    }

    public String getUserText34(){
        return userText34;
    }

    public void setUserText35(String userText35){
        this.userText35 = userText35;
    }

    public String getUserText35(){
        return userText35;
    }

    public void setUserText36(String userText36){
        this.userText36 = userText36;
    }

    public String getUserText36(){
        return userText36;
    }

    public void setUserText37(String userText37){
        this.userText37 = userText37;
    }

    public String getUserText37(){
        return userText37;
    }

    public void setUserText38(String userText38){
        this.userText38 = userText38;
    }

    public String getUserText38(){
        return userText38;
    }

    public void setUserText39(String userText39){
        this.userText39 = userText39;
    }

    public String getUserText39(){
        return userText39;
    }

    public void setUserText40(String userText40){
        this.userText40 = userText40;
    }

    public String getUserText40(){
        return userText40;
    }

    public void setUserText41(String userText41){
        this.userText41 = userText41;
    }

    public String getUserText41(){
        return userText41;
    }

    public void setUserText42(String userText42){
        this.userText42 = userText42;
    }

    public String getUserText42(){
        return userText42;
    }

    public void setUserText43(String userText43){
        this.userText43 = userText43;
    }

    public String getUserText43(){
        return userText43;
    }

    public void setUserText44(String userText44){
        this.userText44 = userText44;
    }

    public String getUserText44(){
        return userText44;
    }

    public void setUserText45(String userText45){
        this.userText45 = userText45;
    }

    public String getUserText45(){
        return userText45;
    }

    public void setUserText46(String userText46){
        this.userText46 = userText46;
    }

    public String getUserText46(){
        return userText46;
    }

    public void setUserText47(String userText47){
        this.userText47 = userText47;
    }

    public String getUserText47(){
        return userText47;
    }

    public void setUserText48(String userText48){
        this.userText48 = userText48;
    }

    public String getUserText48(){
        return userText48;
    }

    public void setUserText49(String userText49){
        this.userText49 = userText49;
    }

    public String getUserText49(){
        return userText49;
    }

    public void setUserText50(String userText50){
        this.userText50 = userText50;
    }

    public String getUserText50(){
        return userText50;
    }

    public void setUserText51(String userText51){
        this.userText51 = userText51;
    }

    public String getUserText51(){
        return userText51;
    }

    public void setUserText52(String userText52){
        this.userText52 = userText52;
    }

    public String getUserText52(){
        return userText52;
    }

    public void setUserText53(String userText53){
        this.userText53 = userText53;
    }

    public String getUserText53(){
        return userText53;
    }

    public void setUserText54(String userText54){
        this.userText54 = userText54;
    }

    public String getUserText54(){
        return userText54;
    }

    public void setUserText55(String userText55){
        this.userText55 = userText55;
    }

    public String getUserText55(){
        return userText55;
    }

    public void setUserText56(String userText56){
        this.userText56 = userText56;
    }

    public String getUserText56(){
        return userText56;
    }

    public void setUserText57(String userText57){
        this.userText57 = userText57;
    }

    public String getUserText57(){
        return userText57;
    }

    public void setUserText58(String userText58){
        this.userText58 = userText58;
    }

    public String getUserText58(){
        return userText58;
    }

    public void setUserText59(String userText59){
        this.userText59 = userText59;
    }

    public String getUserText59(){
        return userText59;
    }

    public void setUserText60(String userText60){
        this.userText60 = userText60;
    }

    public String getUserText60(){
        return userText60;
    }

    public void setUserText61(String userText61){
        this.userText61 = userText61;
    }

    public String getUserText61(){
        return userText61;
    }

    public void setUserText62(String userText62){
        this.userText62 = userText62;
    }

    public String getUserText62(){
        return userText62;
    }

    public void setUserText63(String userText63){
        this.userText63 = userText63;
    }

    public String getUserText63(){
        return userText63;
    }

    public void setUserText64(String userText64){
        this.userText64 = userText64;
    }

    public String getUserText64(){
        return userText64;
    }

    public void setUserText65(String userText65){
        this.userText65 = userText65;
    }

    public String getUserText65(){
        return userText65;
    }

    public void setUserText66(String userText66){
        this.userText66 = userText66;
    }

    public String getUserText66(){
        return userText66;
    }

    public void setUserText67(String userText67){
        this.userText67 = userText67;
    }

    public String getUserText67(){
        return userText67;
    }

    public void setUserText68(String userText68){
        this.userText68 = userText68;
    }

    public String getUserText68(){
        return userText68;
    }

    public void setUserText69(String userText69){
        this.userText69 = userText69;
    }

    public String getUserText69(){
        return userText69;
    }

    public void setUserText70(String userText70){
        this.userText70 = userText70;
    }

    public String getUserText70(){
        return userText70;
    }

    public void setUserText71(String userText71){
        this.userText71 = userText71;
    }

    public String getUserText71(){
        return userText71;
    }

    public void setUserText72(String userText72){
        this.userText72 = userText72;
    }

    public String getUserText72(){
        return userText72;
    }

    public void setUserText73(String userText73){
        this.userText73 = userText73;
    }

    public String getUserText73(){
        return userText73;
    }

    public void setUserText74(String userText74){
        this.userText74 = userText74;
    }

    public String getUserText74(){
        return userText74;
    }

    public void setUserText75(String userText75){
        this.userText75 = userText75;
    }

    public String getUserText75(){
        return userText75;
    }

    public void setUserText76(String userText76){
        this.userText76 = userText76;
    }

    public String getUserText76(){
        return userText76;
    }

    public void setUserText77(String userText77){
        this.userText77 = userText77;
    }

    public String getUserText77(){
        return userText77;
    }

    public void setUserText78(String userText78){
        this.userText78 = userText78;
    }

    public String getUserText78(){
        return userText78;
    }

    public void setUserText79(String userText79){
        this.userText79 = userText79;
    }

    public String getUserText79(){
        return userText79;
    }

    public void setUserText80(String userText80){
        this.userText80 = userText80;
    }

    public String getUserText80(){
        return userText80;
    }

    public void setUserText81(String userText81){
        this.userText81 = userText81;
    }

    public String getUserText81(){
        return userText81;
    }

    public void setUserText82(String userText82){
        this.userText82 = userText82;
    }

    public String getUserText82(){
        return userText82;
    }

    public void setUserText83(String userText83){
        this.userText83 = userText83;
    }

    public String getUserText83(){
        return userText83;
    }

    public void setUserText84(String userText84){
        this.userText84 = userText84;
    }

    public String getUserText84(){
        return userText84;
    }

    public void setUserText85(String userText85){
        this.userText85 = userText85;
    }

    public String getUserText85(){
        return userText85;
    }

    public void setUserText86(String userText86){
        this.userText86 = userText86;
    }

    public String getUserText86(){
        return userText86;
    }

    public void setUserText87(String userText87){
        this.userText87 = userText87;
    }

    public String getUserText87(){
        return userText87;
    }

    public void setUserText88(String userText88){
        this.userText88 = userText88;
    }

    public String getUserText88(){
        return userText88;
    }

    public void setUserText89(String userText89){
        this.userText89 = userText89;
    }

    public String getUserText89(){
        return userText89;
    }

    public void setUserText90(String userText90){
        this.userText90 = userText90;
    }

    public String getUserText90(){
        return userText90;
    }

    public void setUserText91(String userText91){
        this.userText91 = userText91;
    }

    public String getUserText91(){
        return userText91;
    }

    public void setUserText92(String userText92){
        this.userText92 = userText92;
    }

    public String getUserText92(){
        return userText92;
    }

    public void setUserText93(String userText93){
        this.userText93 = userText93;
    }

    public String getUserText93(){
        return userText93;
    }

    public void setUserText94(String userText94){
        this.userText94 = userText94;
    }

    public String getUserText94(){
        return userText94;
    }

    public void setUserText95(String userText95){
        this.userText95 = userText95;
    }

    public String getUserText95(){
        return userText95;
    }

    public void setUserText96(String userText96){
        this.userText96 = userText96;
    }

    public String getUserText96(){
        return userText96;
    }

    public void setUserText97(String userText97){
        this.userText97 = userText97;
    }

    public String getUserText97(){
        return userText97;
    }

    public void setUserText98(String userText98){
        this.userText98 = userText98;
    }

    public String getUserText98(){
        return userText98;
    }

    public void setUserText99(String userText99){
        this.userText99 = userText99;
    }

    public String getUserText99(){
        return userText99;
    }

    public void setUserText100(String userText100){
        this.userText100 = userText100;
    }

    public String getUserText100(){
        return userText100;
    }

    public void setUserNumber01(BigDecimal userNumber01){
        this.userNumber01 = userNumber01;
    }

    public BigDecimal getUserNumber01(){
        return userNumber01;
    }

    public void setUserNumber02(BigDecimal userNumber02){
        this.userNumber02 = userNumber02;
    }

    public BigDecimal getUserNumber02(){
        return userNumber02;
    }

    public void setUserNumber03(BigDecimal userNumber03){
        this.userNumber03 = userNumber03;
    }

    public BigDecimal getUserNumber03(){
        return userNumber03;
    }

    public void setUserNumber04(BigDecimal userNumber04){
        this.userNumber04 = userNumber04;
    }

    public BigDecimal getUserNumber04(){
        return userNumber04;
    }

    public void setUserNumber05(BigDecimal userNumber05){
        this.userNumber05 = userNumber05;
    }

    public BigDecimal getUserNumber05(){
        return userNumber05;
    }

    public void setUserNumber06(BigDecimal userNumber06){
        this.userNumber06 = userNumber06;
    }

    public BigDecimal getUserNumber06(){
        return userNumber06;
    }

    public void setUserNumber07(BigDecimal userNumber07){
        this.userNumber07 = userNumber07;
    }

    public BigDecimal getUserNumber07(){
        return userNumber07;
    }

    public void setUserNumber08(BigDecimal userNumber08){
        this.userNumber08 = userNumber08;
    }

    public BigDecimal getUserNumber08(){
        return userNumber08;
    }

    public void setUserNumber09(BigDecimal userNumber09){
        this.userNumber09 = userNumber09;
    }

    public BigDecimal getUserNumber09(){
        return userNumber09;
    }

    public void setUserNumber10(BigDecimal userNumber10){
        this.userNumber10 = userNumber10;
    }

    public BigDecimal getUserNumber10(){
        return userNumber10;
    }

    public void setUserNumber11(BigDecimal userNumber11){
        this.userNumber11 = userNumber11;
    }

    public BigDecimal getUserNumber11(){
        return userNumber11;
    }

    public void setUserNumber12(BigDecimal userNumber12){
        this.userNumber12 = userNumber12;
    }

    public BigDecimal getUserNumber12(){
        return userNumber12;
    }

    public void setUserNumber13(BigDecimal userNumber13){
        this.userNumber13 = userNumber13;
    }

    public BigDecimal getUserNumber13(){
        return userNumber13;
    }

    public void setUserNumber14(BigDecimal userNumber14){
        this.userNumber14 = userNumber14;
    }

    public BigDecimal getUserNumber14(){
        return userNumber14;
    }

    public void setUserNumber15(BigDecimal userNumber15){
        this.userNumber15 = userNumber15;
    }

    public BigDecimal getUserNumber15(){
        return userNumber15;
    }

    public void setUserNumber16(BigDecimal userNumber16){
        this.userNumber16 = userNumber16;
    }

    public BigDecimal getUserNumber16(){
        return userNumber16;
    }

    public void setUserNumber17(BigDecimal userNumber17){
        this.userNumber17 = userNumber17;
    }

    public BigDecimal getUserNumber17(){
        return userNumber17;
    }

    public void setUserNumber18(BigDecimal userNumber18){
        this.userNumber18 = userNumber18;
    }

    public BigDecimal getUserNumber18(){
        return userNumber18;
    }

    public void setUserNumber19(BigDecimal userNumber19){
        this.userNumber19 = userNumber19;
    }

    public BigDecimal getUserNumber19(){
        return userNumber19;
    }

    public void setUserNumber20(BigDecimal userNumber20){
        this.userNumber20 = userNumber20;
    }

    public BigDecimal getUserNumber20(){
        return userNumber20;
    }

    public void setUserNumber21(BigDecimal userNumber21){
        this.userNumber21 = userNumber21;
    }

    public BigDecimal getUserNumber21(){
        return userNumber21;
    }

    public void setUserNumber22(BigDecimal userNumber22){
        this.userNumber22 = userNumber22;
    }

    public BigDecimal getUserNumber22(){
        return userNumber22;
    }

    public void setUserNumber23(BigDecimal userNumber23){
        this.userNumber23 = userNumber23;
    }

    public BigDecimal getUserNumber23(){
        return userNumber23;
    }

    public void setUserNumber24(BigDecimal userNumber24){
        this.userNumber24 = userNumber24;
    }

    public BigDecimal getUserNumber24(){
        return userNumber24;
    }

    public void setUserNumber25(BigDecimal userNumber25){
        this.userNumber25 = userNumber25;
    }

    public BigDecimal getUserNumber25(){
        return userNumber25;
    }

    public void setUserDate01(Date userDate01){
        this.userDate01 = userDate01;
    }

    public Date getUserDate01(){
        return userDate01;
    }

    public void setUserDate02(Date userDate02){
        this.userDate02 = userDate02;
    }

    public Date getUserDate02(){
        return userDate02;
    }

    public void setUserDate03(Date userDate03){
        this.userDate03 = userDate03;
    }

    public Date getUserDate03(){
        return userDate03;
    }

    public void setUserDate04(Date userDate04){
        this.userDate04 = userDate04;
    }

    public Date getUserDate04(){
        return userDate04;
    }

    public void setUserDate05(Date userDate05){
        this.userDate05 = userDate05;
    }

    public Date getUserDate05(){
        return userDate05;
    }

    public void setUserDate06(Date userDate06){
        this.userDate06 = userDate06;
    }

    public Date getUserDate06(){
        return userDate06;
    }

    public void setUserDate07(Date userDate07){
        this.userDate07 = userDate07;
    }

    public Date getUserDate07(){
        return userDate07;
    }

    public void setUserDate08(Date userDate08){
        this.userDate08 = userDate08;
    }

    public Date getUserDate08(){
        return userDate08;
    }

    public void setUserDate09(Date userDate09){
        this.userDate09 = userDate09;
    }

    public Date getUserDate09(){
        return userDate09;
    }

    public void setUserDate10(Date userDate10){
        this.userDate10 = userDate10;
    }

    public Date getUserDate10(){
        return userDate10;
    }

    public void setUserDate11(Date userDate11){
        this.userDate11 = userDate11;
    }

    public Date getUserDate11(){
        return userDate11;
    }

    public void setUserDate12(Date userDate12){
        this.userDate12 = userDate12;
    }

    public Date getUserDate12(){
        return userDate12;
    }

    public void setUserDate13(Date userDate13){
        this.userDate13 = userDate13;
    }

    public Date getUserDate13(){
        return userDate13;
    }

    public void setUserDate14(Date userDate14){
        this.userDate14 = userDate14;
    }

    public Date getUserDate14(){
        return userDate14;
    }

    public void setUserDate15(Date userDate15){
        this.userDate15 = userDate15;
    }

    public Date getUserDate15(){
        return userDate15;
    }

    public void setUserDate16(Date userDate16){
        this.userDate16 = userDate16;
    }

    public Date getUserDate16(){
        return userDate16;
    }

    public void setUserDate17(Date userDate17){
        this.userDate17 = userDate17;
    }

    public Date getUserDate17(){
        return userDate17;
    }

    public void setUserDate18(Date userDate18){
        this.userDate18 = userDate18;
    }

    public Date getUserDate18(){
        return userDate18;
    }

    public void setUserDate19(Date userDate19){
        this.userDate19 = userDate19;
    }

    public Date getUserDate19(){
        return userDate19;
    }

    public void setUserDate20(Date userDate20){
        this.userDate20 = userDate20;
    }

    public Date getUserDate20(){
        return userDate20;
    }

    public void setUserDate21(Date userDate21){
        this.userDate21 = userDate21;
    }

    public Date getUserDate21(){
        return userDate21;
    }

    public void setUserDate22(Date userDate22){
        this.userDate22 = userDate22;
    }

    public Date getUserDate22(){
        return userDate22;
    }

    public void setUserDate23(Date userDate23){
        this.userDate23 = userDate23;
    }

    public Date getUserDate23(){
        return userDate23;
    }

    public void setUserDate24(Date userDate24){
        this.userDate24 = userDate24;
    }

    public Date getUserDate24(){
        return userDate24;
    }

    public void setUserDate25(Date userDate25){
        this.userDate25 = userDate25;
    }

    public Date getUserDate25(){
        return userDate25;
    }

    public void setShiptoLocation(String shiptoLocation){
        this.shiptoLocation = shiptoLocation;
    }

    public String getShiptoLocation(){
        return shiptoLocation;
    }

    public void setShiptoLocationName(String shiptoLocationName){
        this.shiptoLocationName = shiptoLocationName;
    }

    public String getShiptoLocationName(){
        return shiptoLocationName;
    }

    public void setShiptoManualJurInd(String shiptoManualJurInd){
        this.shiptoManualJurInd = shiptoManualJurInd;
    }

    public String getShiptoManualJurInd(){
        return shiptoManualJurInd;
    }

    public void setShiptoEntityId(Long shiptoEntityId){
        this.shiptoEntityId = shiptoEntityId;
    }

    public Long getShiptoEntityId(){
        return shiptoEntityId;
    }

    public void setShiptoEntityCode(String shiptoEntityCode){
        this.shiptoEntityCode = shiptoEntityCode;
    }

    public String getShiptoEntityCode(){
        return shiptoEntityCode;
    }

    public void setShiptoLat(String shiptoLat){
        this.shiptoLat = shiptoLat;
    }

    public String getShiptoLat(){
        return shiptoLat;
    }

    public void setShiptoLong(String shiptoLong){
        this.shiptoLong = shiptoLong;
    }

    public String getShiptoLong(){
        return shiptoLong;
    }

    public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId){
        this.shiptoLocnMatrixId = shiptoLocnMatrixId;
    }

    public Long getShiptoLocnMatrixId(){
        return shiptoLocnMatrixId;
    }

    public void setShiptoJurisdictionId(Long shiptoJurisdictionId){
        this.shiptoJurisdictionId = shiptoJurisdictionId;
    }

    public Long getShiptoJurisdictionId(){
        return shiptoJurisdictionId;
    }

    public void setShiptoGeocode(String shiptoGeocode){
        this.shiptoGeocode = shiptoGeocode;
    }

    public String getShiptoGeocode(){
        return shiptoGeocode;
    }

    public void setShiptoAddressLine1(String shiptoAddressLine1){
        this.shiptoAddressLine1 = shiptoAddressLine1;
    }

    public String getShiptoAddressLine1(){
        return shiptoAddressLine1;
    }

    public void setShiptoAddressLine2(String shiptoAddressLine2){
        this.shiptoAddressLine2 = shiptoAddressLine2;
    }

    public String getShiptoAddressLine2(){
        return shiptoAddressLine2;
    }

    public void setShiptoCity(String shiptoCity){
        this.shiptoCity = shiptoCity;
    }

    public String getShiptoCity(){
        return shiptoCity;
    }

    public void setShiptoCounty(String shiptoCounty){
        this.shiptoCounty = shiptoCounty;
    }

    public String getShiptoCounty(){
        return shiptoCounty;
    }

    public void setShiptoStateCode(String shiptoStateCode){
        this.shiptoStateCode = shiptoStateCode;
    }

    public String getShiptoStateCode(){
        return shiptoStateCode;
    }

    public void setShiptoZip(String shiptoZip){
        this.shiptoZip = shiptoZip;
    }

    public String getShiptoZip(){
        return shiptoZip;
    }

    public void setShiptoZipplus4(String shiptoZipplus4){
        this.shiptoZipplus4 = shiptoZipplus4;
    }

    public String getShiptoZipplus4(){
        return shiptoZipplus4;
    }

    public void setShiptoCountryCode(String shiptoCountryCode){
        this.shiptoCountryCode = shiptoCountryCode;
    }

    public String getShiptoCountryCode(){
        return shiptoCountryCode;
    }

    public void setShiptoStj1Name(String shiptoStj1Name){
        this.shiptoStj1Name = shiptoStj1Name;
    }

    public String getShiptoStj1Name(){
        return shiptoStj1Name;
    }

    public void setShiptoStj2Name(String shiptoStj2Name){
        this.shiptoStj2Name = shiptoStj2Name;
    }

    public String getShiptoStj2Name(){
        return shiptoStj2Name;
    }

    public void setShiptoStj3Name(String shiptoStj3Name){
        this.shiptoStj3Name = shiptoStj3Name;
    }

    public String getShiptoStj3Name(){
        return shiptoStj3Name;
    }

    public void setShiptoStj4Name(String shiptoStj4Name){
        this.shiptoStj4Name = shiptoStj4Name;
    }

    public String getShiptoStj4Name(){
        return shiptoStj4Name;
    }

    public void setShiptoStj5Name(String shiptoStj5Name){
        this.shiptoStj5Name = shiptoStj5Name;
    }

    public String getShiptoStj5Name(){
        return shiptoStj5Name;
    }

    public void setShiptoCountryNexusindCode(String shiptoCountryNexusindCode){
        this.shiptoCountryNexusindCode = shiptoCountryNexusindCode;
    }

    public String getShiptoCountryNexusindCode(){
        return shiptoCountryNexusindCode;
    }

    public void setShiptoStateNexusindCode(String shiptoStateNexusindCode){
        this.shiptoStateNexusindCode = shiptoStateNexusindCode;
    }

    public String getShiptoStateNexusindCode(){
        return shiptoStateNexusindCode;
    }

    public void setShiptoCountyNexusindCode(String shiptoCountyNexusindCode){
        this.shiptoCountyNexusindCode = shiptoCountyNexusindCode;
    }

    public String getShiptoCountyNexusindCode(){
        return shiptoCountyNexusindCode;
    }

    public void setShiptoCityNexusindCode(String shiptoCityNexusindCode){
        this.shiptoCityNexusindCode = shiptoCityNexusindCode;
    }

    public String getShiptoCityNexusindCode(){
        return shiptoCityNexusindCode;
    }

    public void setShiptoStj1NexusindCode(String shiptoStj1NexusindCode){
        this.shiptoStj1NexusindCode = shiptoStj1NexusindCode;
    }

    public String getShiptoStj1NexusindCode(){
        return shiptoStj1NexusindCode;
    }

    public void setShiptoStj2NexusindCode(String shiptoStj2NexusindCode){
        this.shiptoStj2NexusindCode = shiptoStj2NexusindCode;
    }

    public String getShiptoStj2NexusindCode(){
        return shiptoStj2NexusindCode;
    }

    public void setShiptoStj3NexusindCode(String shiptoStj3NexusindCode){
        this.shiptoStj3NexusindCode = shiptoStj3NexusindCode;
    }

    public String getShiptoStj3NexusindCode(){
        return shiptoStj3NexusindCode;
    }

    public void setShiptoStj4NexusindCode(String shiptoStj4NexusindCode){
        this.shiptoStj4NexusindCode = shiptoStj4NexusindCode;
    }

    public String getShiptoStj4NexusindCode(){
        return shiptoStj4NexusindCode;
    }

    public void setShiptoStj5NexusindCode(String shiptoStj5NexusindCode){
        this.shiptoStj5NexusindCode = shiptoStj5NexusindCode;
    }

    public String getShiptoStj5NexusindCode(){
        return shiptoStj5NexusindCode;
    }

    public void setShipfromEntityId(Long shipfromEntityId){
        this.shipfromEntityId = shipfromEntityId;
    }

    public Long getShipfromEntityId(){
        return shipfromEntityId;
    }

    public void setShipfromEntityCode(String shipfromEntityCode){
        this.shipfromEntityCode = shipfromEntityCode;
    }

    public String getShipfromEntityCode(){
        return shipfromEntityCode;
    }

    public void setShipfromLat(String shipfromLat){
        this.shipfromLat = shipfromLat;
    }

    public String getShipfromLat(){
        return shipfromLat;
    }

    public void setShipfromLong(String shipfromLong){
        this.shipfromLong = shipfromLong;
    }

    public String getShipfromLong(){
        return shipfromLong;
    }

    public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId){
        this.shipfromLocnMatrixId = shipfromLocnMatrixId;
    }

    public Long getShipfromLocnMatrixId(){
        return shipfromLocnMatrixId;
    }

    public void setShipfromJurisdictionId(Long shipfromJurisdictionId){
        this.shipfromJurisdictionId = shipfromJurisdictionId;
    }

    public Long getShipfromJurisdictionId(){
        return shipfromJurisdictionId;
    }

    public void setShipfromGeocode(String shipfromGeocode){
        this.shipfromGeocode = shipfromGeocode;
    }

    public String getShipfromGeocode(){
        return shipfromGeocode;
    }

    public void setShipfromAddressLine1(String shipfromAddressLine1){
        this.shipfromAddressLine1 = shipfromAddressLine1;
    }

    public String getShipfromAddressLine1(){
        return shipfromAddressLine1;
    }

    public void setShipfromAddressLine2(String shipfromAddressLine2){
        this.shipfromAddressLine2 = shipfromAddressLine2;
    }

    public String getShipfromAddressLine2(){
        return shipfromAddressLine2;
    }

    public void setShipfromCity(String shipfromCity){
        this.shipfromCity = shipfromCity;
    }

    public String getShipfromCity(){
        return shipfromCity;
    }

    public void setShipfromCounty(String shipfromCounty){
        this.shipfromCounty = shipfromCounty;
    }

    public String getShipfromCounty(){
        return shipfromCounty;
    }

    public void setShipfromStateCode(String shipfromStateCode){
        this.shipfromStateCode = shipfromStateCode;
    }

    public String getShipfromStateCode(){
        return shipfromStateCode;
    }

    public void setShipfromZip(String shipfromZip){
        this.shipfromZip = shipfromZip;
    }

    public String getShipfromZip(){
        return shipfromZip;
    }

    public void setShipfromZipplus4(String shipfromZipplus4){
        this.shipfromZipplus4 = shipfromZipplus4;
    }

    public String getShipfromZipplus4(){
        return shipfromZipplus4;
    }

    public void setShipfromCountryCode(String shipfromCountryCode){
        this.shipfromCountryCode = shipfromCountryCode;
    }

    public String getShipfromCountryCode(){
        return shipfromCountryCode;
    }

    public void setShipfromStj1Name(String shipfromStj1Name){
        this.shipfromStj1Name = shipfromStj1Name;
    }

    public String getShipfromStj1Name(){
        return shipfromStj1Name;
    }

    public void setShipfromStj2Name(String shipfromStj2Name){
        this.shipfromStj2Name = shipfromStj2Name;
    }

    public String getShipfromStj2Name(){
        return shipfromStj2Name;
    }

    public void setShipfromStj3Name(String shipfromStj3Name){
        this.shipfromStj3Name = shipfromStj3Name;
    }

    public String getShipfromStj3Name(){
        return shipfromStj3Name;
    }

    public void setShipfromStj4Name(String shipfromStj4Name){
        this.shipfromStj4Name = shipfromStj4Name;
    }

    public String getShipfromStj4Name(){
        return shipfromStj4Name;
    }

    public void setShipfromStj5Name(String shipfromStj5Name){
        this.shipfromStj5Name = shipfromStj5Name;
    }

    public String getShipfromStj5Name(){
        return shipfromStj5Name;
    }

    public void setShipfromCountryNexusindCode(String shipfromCountryNexusindCode){
        this.shipfromCountryNexusindCode = shipfromCountryNexusindCode;
    }

    public String getShipfromCountryNexusindCode(){
        return shipfromCountryNexusindCode;
    }

    public void setShipfromStateNexusindCode(String shipfromStateNexusindCode){
        this.shipfromStateNexusindCode = shipfromStateNexusindCode;
    }

    public String getShipfromStateNexusindCode(){
        return shipfromStateNexusindCode;
    }

    public void setShipfromCountyNexusindCode(String shipfromCountyNexusindCode){
        this.shipfromCountyNexusindCode = shipfromCountyNexusindCode;
    }

    public String getShipfromCountyNexusindCode(){
        return shipfromCountyNexusindCode;
    }

    public void setShipfromCityNexusindCode(String shipfromCityNexusindCode){
        this.shipfromCityNexusindCode = shipfromCityNexusindCode;
    }

    public String getShipfromCityNexusindCode(){
        return shipfromCityNexusindCode;
    }

    public void setShipfromStj1NexusindCode(String shipfromStj1NexusindCode){
        this.shipfromStj1NexusindCode = shipfromStj1NexusindCode;
    }

    public String getShipfromStj1NexusindCode(){
        return shipfromStj1NexusindCode;
    }

    public void setShipfromStj2NexusindCode(String shipfromStj2NexusindCode){
        this.shipfromStj2NexusindCode = shipfromStj2NexusindCode;
    }

    public String getShipfromStj2NexusindCode(){
        return shipfromStj2NexusindCode;
    }

    public void setShipfromStj3NexusindCode(String shipfromStj3NexusindCode){
        this.shipfromStj3NexusindCode = shipfromStj3NexusindCode;
    }

    public String getShipfromStj3NexusindCode(){
        return shipfromStj3NexusindCode;
    }

    public void setShipfromStj4NexusindCode(String shipfromStj4NexusindCode){
        this.shipfromStj4NexusindCode = shipfromStj4NexusindCode;
    }

    public String getShipfromStj4NexusindCode(){
        return shipfromStj4NexusindCode;
    }

    public void setShipfromStj5NexusindCode(String shipfromStj5NexusindCode){
        this.shipfromStj5NexusindCode = shipfromStj5NexusindCode;
    }

    public String getShipfromStj5NexusindCode(){
        return shipfromStj5NexusindCode;
    }

    public void setOrdracptEntityId(Long ordracptEntityId){
        this.ordracptEntityId = ordracptEntityId;
    }

    public Long getOrdracptEntityId(){
        return ordracptEntityId;
    }

    public void setOrdracptEntityCode(String ordracptEntityCode){
        this.ordracptEntityCode = ordracptEntityCode;
    }

    public String getOrdracptEntityCode(){
        return ordracptEntityCode;
    }

    public void setOrdracptLat(String ordracptLat){
        this.ordracptLat = ordracptLat;
    }

    public String getOrdracptLat(){
        return ordracptLat;
    }

    public void setOrdracptLong(String ordracptLong){
        this.ordracptLong = ordracptLong;
    }

    public String getOrdracptLong(){
        return ordracptLong;
    }

    public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId){
        this.ordracptLocnMatrixId = ordracptLocnMatrixId;
    }

    public Long getOrdracptLocnMatrixId(){
        return ordracptLocnMatrixId;
    }

    public void setOrdracptJurisdictionId(Long ordracptJurisdictionId){
        this.ordracptJurisdictionId = ordracptJurisdictionId;
    }

    public Long getOrdracptJurisdictionId(){
        return ordracptJurisdictionId;
    }

    public void setOrdracptGeocode(String ordracptGeocode){
        this.ordracptGeocode = ordracptGeocode;
    }

    public String getOrdracptGeocode(){
        return ordracptGeocode;
    }

    public void setOrdracptAddressLine1(String ordracptAddressLine1){
        this.ordracptAddressLine1 = ordracptAddressLine1;
    }

    public String getOrdracptAddressLine1(){
        return ordracptAddressLine1;
    }

    public void setOrdracptAddressLine2(String ordracptAddressLine2){
        this.ordracptAddressLine2 = ordracptAddressLine2;
    }

    public String getOrdracptAddressLine2(){
        return ordracptAddressLine2;
    }

    public void setOrdracptCity(String ordracptCity){
        this.ordracptCity = ordracptCity;
    }

    public String getOrdracptCity(){
        return ordracptCity;
    }

    public void setOrdracptCounty(String ordracptCounty){
        this.ordracptCounty = ordracptCounty;
    }

    public String getOrdracptCounty(){
        return ordracptCounty;
    }

    public void setOrdracptStateCode(String ordracptStateCode){
        this.ordracptStateCode = ordracptStateCode;
    }

    public String getOrdracptStateCode(){
        return ordracptStateCode;
    }

    public void setOrdracptZip(String ordracptZip){
        this.ordracptZip = ordracptZip;
    }

    public String getOrdracptZip(){
        return ordracptZip;
    }

    public void setOrdracptZipplus4(String ordracptZipplus4){
        this.ordracptZipplus4 = ordracptZipplus4;
    }

    public String getOrdracptZipplus4(){
        return ordracptZipplus4;
    }

    public void setOrdracptCountryCode(String ordracptCountryCode){
        this.ordracptCountryCode = ordracptCountryCode;
    }

    public String getOrdracptCountryCode(){
        return ordracptCountryCode;
    }

    public void setOrdracptStj1Name(String ordracptStj1Name){
        this.ordracptStj1Name = ordracptStj1Name;
    }

    public String getOrdracptStj1Name(){
        return ordracptStj1Name;
    }

    public void setOrdracptStj2Name(String ordracptStj2Name){
        this.ordracptStj2Name = ordracptStj2Name;
    }

    public String getOrdracptStj2Name(){
        return ordracptStj2Name;
    }

    public void setOrdracptStj3Name(String ordracptStj3Name){
        this.ordracptStj3Name = ordracptStj3Name;
    }

    public String getOrdracptStj3Name(){
        return ordracptStj3Name;
    }

    public void setOrdracptStj4Name(String ordracptStj4Name){
        this.ordracptStj4Name = ordracptStj4Name;
    }

    public String getOrdracptStj4Name(){
        return ordracptStj4Name;
    }

    public void setOrdracptStj5Name(String ordracptStj5Name){
        this.ordracptStj5Name = ordracptStj5Name;
    }

    public String getOrdracptStj5Name(){
        return ordracptStj5Name;
    }

    public void setOrdracptCountryNexusindCode(String ordracptCountryNexusindCode){
        this.ordracptCountryNexusindCode = ordracptCountryNexusindCode;
    }

    public String getOrdracptCountryNexusindCode(){
        return ordracptCountryNexusindCode;
    }

    public void setOrdracptStateNexusindCode(String ordracptStateNexusindCode){
        this.ordracptStateNexusindCode = ordracptStateNexusindCode;
    }

    public String getOrdracptStateNexusindCode(){
        return ordracptStateNexusindCode;
    }

    public void setOrdracptCountyNexusindCode(String ordracptCountyNexusindCode){
        this.ordracptCountyNexusindCode = ordracptCountyNexusindCode;
    }

    public String getOrdracptCountyNexusindCode(){
        return ordracptCountyNexusindCode;
    }

    public void setOrdracptCityNexusindCode(String ordracptCityNexusindCode){
        this.ordracptCityNexusindCode = ordracptCityNexusindCode;
    }

    public String getOrdracptCityNexusindCode(){
        return ordracptCityNexusindCode;
    }

    public void setOrdracptStj1NexusindCode(String ordracptStj1NexusindCode){
        this.ordracptStj1NexusindCode = ordracptStj1NexusindCode;
    }

    public String getOrdracptStj1NexusindCode(){
        return ordracptStj1NexusindCode;
    }

    public void setOrdracptStj2NexusindCode(String ordracptStj2NexusindCode){
        this.ordracptStj2NexusindCode = ordracptStj2NexusindCode;
    }

    public String getOrdracptStj2NexusindCode(){
        return ordracptStj2NexusindCode;
    }

    public void setOrdracptStj3NexusindCode(String ordracptStj3NexusindCode){
        this.ordracptStj3NexusindCode = ordracptStj3NexusindCode;
    }

    public String getOrdracptStj3NexusindCode(){
        return ordracptStj3NexusindCode;
    }

    public void setOrdracptStj4NexusindCode(String ordracptStj4NexusindCode){
        this.ordracptStj4NexusindCode = ordracptStj4NexusindCode;
    }

    public String getOrdracptStj4NexusindCode(){
        return ordracptStj4NexusindCode;
    }

    public void setOrdracptStj5NexusindCode(String ordracptStj5NexusindCode){
        this.ordracptStj5NexusindCode = ordracptStj5NexusindCode;
    }

    public String getOrdracptStj5NexusindCode(){
        return ordracptStj5NexusindCode;
    }

    public void setOrdrorgnEntityId(Long ordrorgnEntityId){
        this.ordrorgnEntityId = ordrorgnEntityId;
    }

    public Long getOrdrorgnEntityId(){
        return ordrorgnEntityId;
    }

    public void setOrdrorgnEntityCode(String ordrorgnEntityCode){
        this.ordrorgnEntityCode = ordrorgnEntityCode;
    }

    public String getOrdrorgnEntityCode(){
        return ordrorgnEntityCode;
    }

    public void setOrdrorgnLat(String ordrorgnLat){
        this.ordrorgnLat = ordrorgnLat;
    }

    public String getOrdrorgnLat(){
        return ordrorgnLat;
    }

    public void setOrdrorgnLong(String ordrorgnLong){
        this.ordrorgnLong = ordrorgnLong;
    }

    public String getOrdrorgnLong(){
        return ordrorgnLong;
    }

    public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId){
        this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
    }

    public Long getOrdrorgnLocnMatrixId(){
        return ordrorgnLocnMatrixId;
    }

    public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId){
        this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
    }

    public Long getOrdrorgnJurisdictionId(){
        return ordrorgnJurisdictionId;
    }

    public void setOrdrorgnGeocode(String ordrorgnGeocode){
        this.ordrorgnGeocode = ordrorgnGeocode;
    }

    public String getOrdrorgnGeocode(){
        return ordrorgnGeocode;
    }

    public void setOrdrorgnAddressLine1(String ordrorgnAddressLine1){
        this.ordrorgnAddressLine1 = ordrorgnAddressLine1;
    }

    public String getOrdrorgnAddressLine1(){
        return ordrorgnAddressLine1;
    }

    public void setOrdrorgnAddressLine2(String ordrorgnAddressLine2){
        this.ordrorgnAddressLine2 = ordrorgnAddressLine2;
    }

    public String getOrdrorgnAddressLine2(){
        return ordrorgnAddressLine2;
    }

    public void setOrdrorgnCity(String ordrorgnCity){
        this.ordrorgnCity = ordrorgnCity;
    }

    public String getOrdrorgnCity(){
        return ordrorgnCity;
    }

    public void setOrdrorgnCounty(String ordrorgnCounty){
        this.ordrorgnCounty = ordrorgnCounty;
    }

    public String getOrdrorgnCounty(){
        return ordrorgnCounty;
    }

    public void setOrdrorgnStateCode(String ordrorgnStateCode){
        this.ordrorgnStateCode = ordrorgnStateCode;
    }

    public String getOrdrorgnStateCode(){
        return ordrorgnStateCode;
    }

    public void setOrdrorgnZip(String ordrorgnZip){
        this.ordrorgnZip = ordrorgnZip;
    }

    public String getOrdrorgnZip(){
        return ordrorgnZip;
    }

    public void setOrdrorgnZipplus4(String ordrorgnZipplus4){
        this.ordrorgnZipplus4 = ordrorgnZipplus4;
    }

    public String getOrdrorgnZipplus4(){
        return ordrorgnZipplus4;
    }

    public void setOrdrorgnCountryCode(String ordrorgnCountryCode){
        this.ordrorgnCountryCode = ordrorgnCountryCode;
    }

    public String getOrdrorgnCountryCode(){
        return ordrorgnCountryCode;
    }

    public void setOrdrorgnStj1Name(String ordrorgnStj1Name){
        this.ordrorgnStj1Name = ordrorgnStj1Name;
    }

    public String getOrdrorgnStj1Name(){
        return ordrorgnStj1Name;
    }

    public void setOrdrorgnStj2Name(String ordrorgnStj2Name){
        this.ordrorgnStj2Name = ordrorgnStj2Name;
    }

    public String getOrdrorgnStj2Name(){
        return ordrorgnStj2Name;
    }

    public void setOrdrorgnStj3Name(String ordrorgnStj3Name){
        this.ordrorgnStj3Name = ordrorgnStj3Name;
    }

    public String getOrdrorgnStj3Name(){
        return ordrorgnStj3Name;
    }

    public void setOrdrorgnStj4Name(String ordrorgnStj4Name){
        this.ordrorgnStj4Name = ordrorgnStj4Name;
    }

    public String getOrdrorgnStj4Name(){
        return ordrorgnStj4Name;
    }

    public void setOrdrorgnStj5Name(String ordrorgnStj5Name){
        this.ordrorgnStj5Name = ordrorgnStj5Name;
    }

    public String getOrdrorgnStj5Name(){
        return ordrorgnStj5Name;
    }

    public void setOrdrorgnCountryNexusindCode(String ordrorgnCountryNexusindCode){
        this.ordrorgnCountryNexusindCode = ordrorgnCountryNexusindCode;
    }

    public String getOrdrorgnCountryNexusindCode(){
        return ordrorgnCountryNexusindCode;
    }

    public void setOrdrorgnStateNexusindCode(String ordrorgnStateNexusindCode){
        this.ordrorgnStateNexusindCode = ordrorgnStateNexusindCode;
    }

    public String getOrdrorgnStateNexusindCode(){
        return ordrorgnStateNexusindCode;
    }

    public void setOrdrorgnCountyNexusindCode(String ordrorgnCountyNexusindCode){
        this.ordrorgnCountyNexusindCode = ordrorgnCountyNexusindCode;
    }

    public String getOrdrorgnCountyNexusindCode(){
        return ordrorgnCountyNexusindCode;
    }

    public void setOrdrorgnCityNexusindCode(String ordrorgnCityNexusindCode){
        this.ordrorgnCityNexusindCode = ordrorgnCityNexusindCode;
    }

    public String getOrdrorgnCityNexusindCode(){
        return ordrorgnCityNexusindCode;
    }

    public void setOrdrorgnStj1NexusindCode(String ordrorgnStj1NexusindCode){
        this.ordrorgnStj1NexusindCode = ordrorgnStj1NexusindCode;
    }

    public String getOrdrorgnStj1NexusindCode(){
        return ordrorgnStj1NexusindCode;
    }

    public void setOrdrorgnStj2NexusindCode(String ordrorgnStj2NexusindCode){
        this.ordrorgnStj2NexusindCode = ordrorgnStj2NexusindCode;
    }

    public String getOrdrorgnStj2NexusindCode(){
        return ordrorgnStj2NexusindCode;
    }

    public void setOrdrorgnStj3NexusindCode(String ordrorgnStj3NexusindCode){
        this.ordrorgnStj3NexusindCode = ordrorgnStj3NexusindCode;
    }

    public String getOrdrorgnStj3NexusindCode(){
        return ordrorgnStj3NexusindCode;
    }

    public void setOrdrorgnStj4NexusindCode(String ordrorgnStj4NexusindCode){
        this.ordrorgnStj4NexusindCode = ordrorgnStj4NexusindCode;
    }

    public String getOrdrorgnStj4NexusindCode(){
        return ordrorgnStj4NexusindCode;
    }

    public void setOrdrorgnStj5NexusindCode(String ordrorgnStj5NexusindCode){
        this.ordrorgnStj5NexusindCode = ordrorgnStj5NexusindCode;
    }

    public String getOrdrorgnStj5NexusindCode(){
        return ordrorgnStj5NexusindCode;
    }

    public void setFirstuseEntityId(Long firstuseEntityId){
        this.firstuseEntityId = firstuseEntityId;
    }

    public Long getFirstuseEntityId(){
        return firstuseEntityId;
    }

    public void setFirstuseEntityCode(String firstuseEntityCode){
        this.firstuseEntityCode = firstuseEntityCode;
    }

    public String getFirstuseEntityCode(){
        return firstuseEntityCode;
    }

    public void setFirstuseLat(String firstuseLat){
        this.firstuseLat = firstuseLat;
    }

    public String getFirstuseLat(){
        return firstuseLat;
    }

    public void setFirstuseLong(String firstuseLong){
        this.firstuseLong = firstuseLong;
    }

    public String getFirstuseLong(){
        return firstuseLong;
    }

    public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId){
        this.firstuseLocnMatrixId = firstuseLocnMatrixId;
    }

    public Long getFirstuseLocnMatrixId(){
        return firstuseLocnMatrixId;
    }

    public void setFirstuseJurisdictionId(Long firstuseJurisdictionId){
        this.firstuseJurisdictionId = firstuseJurisdictionId;
    }

    public Long getFirstuseJurisdictionId(){
        return firstuseJurisdictionId;
    }

    public void setFirstuseGeocode(String firstuseGeocode){
        this.firstuseGeocode = firstuseGeocode;
    }

    public String getFirstuseGeocode(){
        return firstuseGeocode;
    }

    public void setFirstuseAddressLine1(String firstuseAddressLine1){
        this.firstuseAddressLine1 = firstuseAddressLine1;
    }

    public String getFirstuseAddressLine1(){
        return firstuseAddressLine1;
    }

    public void setFirstuseAddressLine2(String firstuseAddressLine2){
        this.firstuseAddressLine2 = firstuseAddressLine2;
    }

    public String getFirstuseAddressLine2(){
        return firstuseAddressLine2;
    }

    public void setFirstuseCity(String firstuseCity){
        this.firstuseCity = firstuseCity;
    }

    public String getFirstuseCity(){
        return firstuseCity;
    }

    public void setFirstuseCounty(String firstuseCounty){
        this.firstuseCounty = firstuseCounty;
    }

    public String getFirstuseCounty(){
        return firstuseCounty;
    }

    public void setFirstuseStateCode(String firstuseStateCode){
        this.firstuseStateCode = firstuseStateCode;
    }

    public String getFirstuseStateCode(){
        return firstuseStateCode;
    }

    public void setFirstuseZip(String firstuseZip){
        this.firstuseZip = firstuseZip;
    }

    public String getFirstuseZip(){
        return firstuseZip;
    }

    public void setFirstuseZipplus4(String firstuseZipplus4){
        this.firstuseZipplus4 = firstuseZipplus4;
    }

    public String getFirstuseZipplus4(){
        return firstuseZipplus4;
    }

    public void setFirstuseCountryCode(String firstuseCountryCode){
        this.firstuseCountryCode = firstuseCountryCode;
    }

    public String getFirstuseCountryCode(){
        return firstuseCountryCode;
    }

    public void setFirstuseStj1Name(String firstuseStj1Name){
        this.firstuseStj1Name = firstuseStj1Name;
    }

    public String getFirstuseStj1Name(){
        return firstuseStj1Name;
    }

    public void setFirstuseStj2Name(String firstuseStj2Name){
        this.firstuseStj2Name = firstuseStj2Name;
    }

    public String getFirstuseStj2Name(){
        return firstuseStj2Name;
    }

    public void setFirstuseStj3Name(String firstuseStj3Name){
        this.firstuseStj3Name = firstuseStj3Name;
    }

    public String getFirstuseStj3Name(){
        return firstuseStj3Name;
    }

    public void setFirstuseStj4Name(String firstuseStj4Name){
        this.firstuseStj4Name = firstuseStj4Name;
    }

    public String getFirstuseStj4Name(){
        return firstuseStj4Name;
    }

    public void setFirstuseStj5Name(String firstuseStj5Name){
        this.firstuseStj5Name = firstuseStj5Name;
    }

    public String getFirstuseStj5Name(){
        return firstuseStj5Name;
    }

    public void setFirstuseCountryNexusindCode(String firstuseCountryNexusindCode){
        this.firstuseCountryNexusindCode = firstuseCountryNexusindCode;
    }

    public String getFirstuseCountryNexusindCode(){
        return firstuseCountryNexusindCode;
    }

    public void setFirstuseStateNexusindCode(String firstuseStateNexusindCode){
        this.firstuseStateNexusindCode = firstuseStateNexusindCode;
    }

    public String getFirstuseStateNexusindCode(){
        return firstuseStateNexusindCode;
    }

    public void setFirstuseCountyNexusindCode(String firstuseCountyNexusindCode){
        this.firstuseCountyNexusindCode = firstuseCountyNexusindCode;
    }

    public String getFirstuseCountyNexusindCode(){
        return firstuseCountyNexusindCode;
    }

    public void setFirstuseCityNexusindCode(String firstuseCityNexusindCode){
        this.firstuseCityNexusindCode = firstuseCityNexusindCode;
    }

    public String getFirstuseCityNexusindCode(){
        return firstuseCityNexusindCode;
    }

    public void setFirstuseStj1NexusindCode(String firstuseStj1NexusindCode){
        this.firstuseStj1NexusindCode = firstuseStj1NexusindCode;
    }

    public String getFirstuseStj1NexusindCode(){
        return firstuseStj1NexusindCode;
    }

    public void setFirstuseStj2NexusindCode(String firstuseStj2NexusindCode){
        this.firstuseStj2NexusindCode = firstuseStj2NexusindCode;
    }

    public String getFirstuseStj2NexusindCode(){
        return firstuseStj2NexusindCode;
    }

    public void setFirstuseStj3NexusindCode(String firstuseStj3NexusindCode){
        this.firstuseStj3NexusindCode = firstuseStj3NexusindCode;
    }

    public String getFirstuseStj3NexusindCode(){
        return firstuseStj3NexusindCode;
    }

    public void setFirstuseStj4NexusindCode(String firstuseStj4NexusindCode){
        this.firstuseStj4NexusindCode = firstuseStj4NexusindCode;
    }

    public String getFirstuseStj4NexusindCode(){
        return firstuseStj4NexusindCode;
    }

    public void setFirstuseStj5NexusindCode(String firstuseStj5NexusindCode){
        this.firstuseStj5NexusindCode = firstuseStj5NexusindCode;
    }

    public String getFirstuseStj5NexusindCode(){
        return firstuseStj5NexusindCode;
    }

    public void setBilltoEntityId(Long billtoEntityId){
        this.billtoEntityId = billtoEntityId;
    }

    public Long getBilltoEntityId(){
        return billtoEntityId;
    }

    public void setBilltoEntityCode(String billtoEntityCode){
        this.billtoEntityCode = billtoEntityCode;
    }

    public String getBilltoEntityCode(){
        return billtoEntityCode;
    }

    public void setBilltoLat(String billtoLat){
        this.billtoLat = billtoLat;
    }

    public String getBilltoLat(){
        return billtoLat;
    }

    public void setBilltoLong(String billtoLong){
        this.billtoLong = billtoLong;
    }

    public String getBilltoLong(){
        return billtoLong;
    }

    public void setBilltoLocnMatrixId(Long billtoLocnMatrixId){
        this.billtoLocnMatrixId = billtoLocnMatrixId;
    }

    public Long getBilltoLocnMatrixId(){
        return billtoLocnMatrixId;
    }

    public void setBilltoJurisdictionId(Long billtoJurisdictionId){
        this.billtoJurisdictionId = billtoJurisdictionId;
    }

    public Long getBilltoJurisdictionId(){
        return billtoJurisdictionId;
    }

    public void setBilltoGeocode(String billtoGeocode){
        this.billtoGeocode = billtoGeocode;
    }

    public String getBilltoGeocode(){
        return billtoGeocode;
    }

    public void setBilltoAddressLine1(String billtoAddressLine1){
        this.billtoAddressLine1 = billtoAddressLine1;
    }

    public String getBilltoAddressLine1(){
        return billtoAddressLine1;
    }

    public void setBilltoAddressLine2(String billtoAddressLine2){
        this.billtoAddressLine2 = billtoAddressLine2;
    }

    public String getBilltoAddressLine2(){
        return billtoAddressLine2;
    }

    public void setBilltoCity(String billtoCity){
        this.billtoCity = billtoCity;
    }

    public String getBilltoCity(){
        return billtoCity;
    }

    public void setBilltoCounty(String billtoCounty){
        this.billtoCounty = billtoCounty;
    }

    public String getBilltoCounty(){
        return billtoCounty;
    }

    public void setBilltoStateCode(String billtoStateCode){
        this.billtoStateCode = billtoStateCode;
    }

    public String getBilltoStateCode(){
        return billtoStateCode;
    }

    public void setBilltoZip(String billtoZip){
        this.billtoZip = billtoZip;
    }

    public String getBilltoZip(){
        return billtoZip;
    }

    public void setBilltoZipplus4(String billtoZipplus4){
        this.billtoZipplus4 = billtoZipplus4;
    }

    public String getBilltoZipplus4(){
        return billtoZipplus4;
    }

    public void setBilltoCountryCode(String billtoCountryCode){
        this.billtoCountryCode = billtoCountryCode;
    }

    public String getBilltoCountryCode(){
        return billtoCountryCode;
    }

    public void setBilltoStj1Name(String billtoStj1Name){
        this.billtoStj1Name = billtoStj1Name;
    }

    public String getBilltoStj1Name(){
        return billtoStj1Name;
    }

    public void setBilltoStj2Name(String billtoStj2Name){
        this.billtoStj2Name = billtoStj2Name;
    }

    public String getBilltoStj2Name(){
        return billtoStj2Name;
    }

    public void setBilltoStj3Name(String billtoStj3Name){
        this.billtoStj3Name = billtoStj3Name;
    }

    public String getBilltoStj3Name(){
        return billtoStj3Name;
    }

    public void setBilltoStj4Name(String billtoStj4Name){
        this.billtoStj4Name = billtoStj4Name;
    }

    public String getBilltoStj4Name(){
        return billtoStj4Name;
    }

    public void setBilltoStj5Name(String billtoStj5Name){
        this.billtoStj5Name = billtoStj5Name;
    }

    public String getBilltoStj5Name(){
        return billtoStj5Name;
    }

    public void setBilltoCountryNexusindCode(String billtoCountryNexusindCode){
        this.billtoCountryNexusindCode = billtoCountryNexusindCode;
    }

    public String getBilltoCountryNexusindCode(){
        return billtoCountryNexusindCode;
    }

    public void setBilltoStateNexusindCode(String billtoStateNexusindCode){
        this.billtoStateNexusindCode = billtoStateNexusindCode;
    }

    public String getBilltoStateNexusindCode(){
        return billtoStateNexusindCode;
    }

    public void setBilltoCountyNexusindCode(String billtoCountyNexusindCode){
        this.billtoCountyNexusindCode = billtoCountyNexusindCode;
    }

    public String getBilltoCountyNexusindCode(){
        return billtoCountyNexusindCode;
    }

    public void setBilltoCityNexusindCode(String billtoCityNexusindCode){
        this.billtoCityNexusindCode = billtoCityNexusindCode;
    }

    public String getBilltoCityNexusindCode(){
        return billtoCityNexusindCode;
    }

    public void setBilltoStj1NexusindCode(String billtoStj1NexusindCode){
        this.billtoStj1NexusindCode = billtoStj1NexusindCode;
    }

    public String getBilltoStj1NexusindCode(){
        return billtoStj1NexusindCode;
    }

    public void setBilltoStj2NexusindCode(String billtoStj2NexusindCode){
        this.billtoStj2NexusindCode = billtoStj2NexusindCode;
    }

    public String getBilltoStj2NexusindCode(){
        return billtoStj2NexusindCode;
    }

    public void setBilltoStj3NexusindCode(String billtoStj3NexusindCode){
        this.billtoStj3NexusindCode = billtoStj3NexusindCode;
    }

    public String getBilltoStj3NexusindCode(){
        return billtoStj3NexusindCode;
    }

    public void setBilltoStj4NexusindCode(String billtoStj4NexusindCode){
        this.billtoStj4NexusindCode = billtoStj4NexusindCode;
    }

    public String getBilltoStj4NexusindCode(){
        return billtoStj4NexusindCode;
    }

    public void setBilltoStj5NexusindCode(String billtoStj5NexusindCode){
        this.billtoStj5NexusindCode = billtoStj5NexusindCode;
    }

    public String getBilltoStj5NexusindCode(){
        return billtoStj5NexusindCode;
    }

    public void setTtlxfrEntityId(Long ttlxfrEntityId){
        this.ttlxfrEntityId = ttlxfrEntityId;
    }

    public Long getTtlxfrEntityId(){
        return ttlxfrEntityId;
    }

    public void setTtlxfrEntityCode(String ttlxfrEntityCode){
        this.ttlxfrEntityCode = ttlxfrEntityCode;
    }

    public String getTtlxfrEntityCode(){
        return ttlxfrEntityCode;
    }

    public void setTtlxfrLat(String ttlxfrLat){
        this.ttlxfrLat = ttlxfrLat;
    }

    public String getTtlxfrLat(){
        return ttlxfrLat;
    }

    public void setTtlxfrLong(String ttlxfrLong){
        this.ttlxfrLong = ttlxfrLong;
    }

    public String getTtlxfrLong(){
        return ttlxfrLong;
    }

    public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId){
        this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
    }

    public Long getTtlxfrLocnMatrixId(){
        return ttlxfrLocnMatrixId;
    }

    public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId){
        this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
    }

    public Long getTtlxfrJurisdictionId(){
        return ttlxfrJurisdictionId;
    }

    public void setTtlxfrGeocode(String ttlxfrGeocode){
        this.ttlxfrGeocode = ttlxfrGeocode;
    }

    public String getTtlxfrGeocode(){
        return ttlxfrGeocode;
    }

    public void setTtlxfrAddressLine1(String ttlxfrAddressLine1){
        this.ttlxfrAddressLine1 = ttlxfrAddressLine1;
    }

    public String getTtlxfrAddressLine1(){
        return ttlxfrAddressLine1;
    }

    public void setTtlxfrAddressLine2(String ttlxfrAddressLine2){
        this.ttlxfrAddressLine2 = ttlxfrAddressLine2;
    }

    public String getTtlxfrAddressLine2(){
        return ttlxfrAddressLine2;
    }

    public void setTtlxfrCity(String ttlxfrCity){
        this.ttlxfrCity = ttlxfrCity;
    }

    public String getTtlxfrCity(){
        return ttlxfrCity;
    }

    public void setTtlxfrCounty(String ttlxfrCounty){
        this.ttlxfrCounty = ttlxfrCounty;
    }

    public String getTtlxfrCounty(){
        return ttlxfrCounty;
    }

    public void setTtlxfrStateCode(String ttlxfrStateCode){
        this.ttlxfrStateCode = ttlxfrStateCode;
    }

    public String getTtlxfrStateCode(){
        return ttlxfrStateCode;
    }

    public void setTtlxfrZip(String ttlxfrZip){
        this.ttlxfrZip = ttlxfrZip;
    }

    public String getTtlxfrZip(){
        return ttlxfrZip;
    }

    public void setTtlxfrZipplus4(String ttlxfrZipplus4){
        this.ttlxfrZipplus4 = ttlxfrZipplus4;
    }

    public String getTtlxfrZipplus4(){
        return ttlxfrZipplus4;
    }

    public void setTtlxfrCountryCode(String ttlxfrCountryCode){
        this.ttlxfrCountryCode = ttlxfrCountryCode;
    }

    public String getTtlxfrCountryCode(){
        return ttlxfrCountryCode;
    }

    public void setTtlxfrStj1Name(String ttlxfrStj1Name){
        this.ttlxfrStj1Name = ttlxfrStj1Name;
    }

    public String getTtlxfrStj1Name(){
        return ttlxfrStj1Name;
    }

    public void setTtlxfrStj2Name(String ttlxfrStj2Name){
        this.ttlxfrStj2Name = ttlxfrStj2Name;
    }

    public String getTtlxfrStj2Name(){
        return ttlxfrStj2Name;
    }

    public void setTtlxfrStj3Name(String ttlxfrStj3Name){
        this.ttlxfrStj3Name = ttlxfrStj3Name;
    }

    public String getTtlxfrStj3Name(){
        return ttlxfrStj3Name;
    }

    public void setTtlxfrStj4Name(String ttlxfrStj4Name){
        this.ttlxfrStj4Name = ttlxfrStj4Name;
    }

    public String getTtlxfrStj4Name(){
        return ttlxfrStj4Name;
    }

    public void setTtlxfrStj5Name(String ttlxfrStj5Name){
        this.ttlxfrStj5Name = ttlxfrStj5Name;
    }

    public String getTtlxfrStj5Name(){
        return ttlxfrStj5Name;
    }

    public void setTtlxfrCountryNexusindCode(String ttlxfrCountryNexusindCode){
        this.ttlxfrCountryNexusindCode = ttlxfrCountryNexusindCode;
    }

    public String getTtlxfrCountryNexusindCode(){
        return ttlxfrCountryNexusindCode;
    }

    public void setTtlxfrStateNexusindCode(String ttlxfrStateNexusindCode){
        this.ttlxfrStateNexusindCode = ttlxfrStateNexusindCode;
    }

    public String getTtlxfrStateNexusindCode(){
        return ttlxfrStateNexusindCode;
    }

    public void setTtlxfrCountyNexusindCode(String ttlxfrCountyNexusindCode){
        this.ttlxfrCountyNexusindCode = ttlxfrCountyNexusindCode;
    }

    public String getTtlxfrCountyNexusindCode(){
        return ttlxfrCountyNexusindCode;
    }

    public void setTtlxfrCityNexusindCode(String ttlxfrCityNexusindCode){
        this.ttlxfrCityNexusindCode = ttlxfrCityNexusindCode;
    }

    public String getTtlxfrCityNexusindCode(){
        return ttlxfrCityNexusindCode;
    }

    public void setTtlxfrStj1NexusindCode(String ttlxfrStj1NexusindCode){
        this.ttlxfrStj1NexusindCode = ttlxfrStj1NexusindCode;
    }

    public String getTtlxfrStj1NexusindCode(){
        return ttlxfrStj1NexusindCode;
    }

    public void setTtlxfrStj2NexusindCode(String ttlxfrStj2NexusindCode){
        this.ttlxfrStj2NexusindCode = ttlxfrStj2NexusindCode;
    }

    public String getTtlxfrStj2NexusindCode(){
        return ttlxfrStj2NexusindCode;
    }

    public void setTtlxfrStj3NexusindCode(String ttlxfrStj3NexusindCode){
        this.ttlxfrStj3NexusindCode = ttlxfrStj3NexusindCode;
    }

    public String getTtlxfrStj3NexusindCode(){
        return ttlxfrStj3NexusindCode;
    }

    public void setTtlxfrStj4NexusindCode(String ttlxfrStj4NexusindCode){
        this.ttlxfrStj4NexusindCode = ttlxfrStj4NexusindCode;
    }

    public String getTtlxfrStj4NexusindCode(){
        return ttlxfrStj4NexusindCode;
    }

    public void setTtlxfrStj5NexusindCode(String ttlxfrStj5NexusindCode){
        this.ttlxfrStj5NexusindCode = ttlxfrStj5NexusindCode;
    }

    public String getTtlxfrStj5NexusindCode(){
        return ttlxfrStj5NexusindCode;
    }
	
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	  static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.DECIMAL);
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPPurchaseTransaction.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into tb_bcp_purchtrans (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPPurchaseTransaction.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){
					s.setNull(idx, sqlTypes.get(f.getType()));
				}
				else{				
					if("LOAD_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getLoadTimestamp().getTime()));
					}
					else if("GL_EXTRACT_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getGlExtractTimestamp().getTime()));
					}
					else if(sqlTypes.get(f.getType()) == Types.DECIMAL){
						//Make sure it is float instead of Float object
						s.setBigDecimal(idx, ArithmeticUtils.toBigDecimal(fld));
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){					
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{					
						s.setObject(idx, fld);
					}
				}
			}
		}
	}
}

