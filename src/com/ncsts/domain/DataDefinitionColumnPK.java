package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
/**
 * 
 * @author Muneer
 *
 */
@Embeddable
public class DataDefinitionColumnPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="TABLE_NAME")  	
	private String tableName; 
	   
	@Column(name="COLUMN_NAME") 	
	private String columnName;
	
	public DataDefinitionColumnPK(){
		
	}
	
	public DataDefinitionColumnPK(String tableName, String columnName) {
		this.tableName = tableName;
		this.columnName = columnName;
	}
	

	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DataDefinitionColumnPK)) {
            return false;
        }

        final DataDefinitionColumnPK revision = (DataDefinitionColumnPK)other;

        String t1 = getTableName();
        String t2 = revision.getTableName();
        if (!((t1 == t2) || (t1 != null && t1.equals(t2)))) {
            return false;
        }

        String c1 = getColumnName();
        String c2 = revision.getColumnName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String t1 = getTableName();
		hash = 31 * hash + (null == t1 ? 0 : t1.hashCode());
        String c1 = getColumnName();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.tableName + "::" + this.columnName;
    }

}
