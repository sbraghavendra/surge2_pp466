package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DataStatisticsDTO;

/**
 * @author Muneer Basha
 *
 */

@Entity
@Table(name="TB_DB_STATISTICS")
public class DataStatistics extends Auditable implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -8879120725833185536L;

	@Id
    @Column(name="DB_STATISTICS_ID")
    private Long dataStatisticsId;
    
    @Column(name="ROW_TITLE")    
    private String rowTitle;   
    
    @Column(name="ROW_WHERE")    
    private String rowWhere;      
    
	@Column(name="GROUP_BY_ON_DRILLDOWN")
    private String groupByOnDrilldown;
	
	@Transient	
	DataStatisticsDTO dataStatisticsDTO;
	
    public DataStatistics() { 
    	
    }
	
	public DataStatisticsDTO getDataStatisticsDTO() {
		DataStatisticsDTO dataStatisticsDTO = new DataStatisticsDTO();
		BeanUtils.copyProperties(this, dataStatisticsDTO);
		return dataStatisticsDTO;
	}

	public Long getDataStatisticsId() {
		return dataStatisticsId;
	}

	public void setDataStatisticsId(Long dataStatisticsId) {
		this.dataStatisticsId = dataStatisticsId;
	}

	public String getRowTitle() {
		return rowTitle;
	}

	public void setRowTitle(String rowTitle) {
		this.rowTitle = rowTitle;
	}

	public String getRowWhere() {
		return rowWhere;
	}

	public void setRowWhere(String rowWhere) {
		this.rowWhere = rowWhere;
	}

	public String getGroupByOnDrilldown() {
		return groupByOnDrilldown;
	}

	public void setGroupByOnDrilldown(String groupByOnDrilldown) {
		this.groupByOnDrilldown = groupByOnDrilldown;
	}

}






