package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.VendorNexusDTO;

@Entity
@Table(name="TB_VENDOR_NEXUS")
public class VendorNexus extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="VENDOR_NEXUS_ID")	
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="vendor_nexus_sequence")
	@SequenceGenerator(name="vendor_nexus_sequence" , allocationSize = 1, sequenceName="sq_tb_vendor_nexus_id")
    private Long vendorNexusId;
    
    @Column(name="VENDOR_ID")	
    private Long vendorId;
    
    @Column(name="NEXUS_COUNTRY_CODE")    
    private String nexusCountryCode; 
    
    @Column(name="NEXUS_STATE_CODE")    
    private String nexusStateCode;
    
    @Column(name="NEXUS_COUNTY")    
    private String nexusCounty; 
    
    @Column(name="NEXUS_CITY")    
    private String nexusCity; 
    
    @Column(name="NEXUS_STJ")    
    private String nexusStj; 
    
    @Column(name="NEXUS_FLAG")    
    private String nexusFlag; 

    public VendorNexus(){
    }
	
	public VendorNexusDTO getVendorNexusDTO() {
		VendorNexusDTO vendorNexusDTO = new VendorNexusDTO();
		BeanUtils.copyProperties(this, vendorNexusDTO);
		return vendorNexusDTO;
	}
	
	public Long getVendorNexusId() {
        return this.vendorNexusId;
    }
    
    public void setVendorNexusId(Long vendorNexusId) {
    	this.vendorNexusId = vendorNexusId;
    }
    
    public Long getVendorId() {
        return this.vendorId;
    }
    
    public void setVendorId(Long vendorId) {
    	this.vendorId = vendorId;
    }
    
    public String getNexusCountryCode() {
        return this.nexusCountryCode;
    }
    
    public void setNexusCountryCode(String nexusCountryCode) {
    	this.nexusCountryCode = nexusCountryCode;
    }  
    
    public String getNexusStateCode() {
        return this.nexusStateCode;
    }
    
    public void setNexusStateCode(String nexusStateCode) {
    	this.nexusStateCode = nexusStateCode;
    }
    
    public String getNexusCounty() {
        return this.nexusCounty;
    }
    
    public void setNexusCounty(String nexusCounty) {
    	this.nexusCounty = nexusCounty;
    }  
     
    public String getNexusCity() {
        return this.nexusCity;
    }
    
    public void setNexusCity(String nexusCity) {
    	this.nexusCity = nexusCity;
    }
    
    public String getNexusStj() {
        return this.nexusStj;
    }
    
    public void setNexusStj(String nexusStj) {
    	this.nexusStj = nexusStj;
    }
    
    public String getNexusFlag() {
        return this.nexusFlag;
    }
    
    public void setNexusFlag(String nexusFlag) {
    	this.nexusFlag = nexusFlag;
    }  

    public Boolean getNexusBooleanFlag() {
    	return "1".equals(this.nexusFlag);
	}

	public void setNexusBooleanFlag(Boolean nexusFlag) {
		this.nexusFlag = nexusFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return this.vendorNexusId;
	}

	@Override
	public void setId(Long id) {
		this.vendorNexusId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "vendorNexusId";
	}
}