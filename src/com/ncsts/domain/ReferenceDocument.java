package com.ncsts.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Check;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_REFERENCE_DOCUMENT")
public class ReferenceDocument extends Auditable implements Idable<String> {
	
	@Id
	@Column(name="REFERENCE_DOCUMENT_CODE")	
	 private String refDocCode;
	
	@Column(name="REFERENCE_TYPE_CODE")
	@Check(constraints = "REFERENCE_TYPE_CODE IN (SELECT CODE_CODE FROM TB_LIST_CODE WHERE CODE_TYPE_CODE='REFTYPE')" )
	 private String refTypeCode;
	
	@Column(name="DESCRIPTION")	
	 private String description;
	
	@Column(name="KEYWORDS")	
	 private String keyWords;
	
	@Column(name="DOCUMENT_NAME")	
	 private String docName;
	
	@Column(name="URL")	
	 private String url;
	
	//Midtier project code added - january 2009
	@OneToMany(mappedBy="referenceDocument")
	@Cascade(CascadeType.DELETE_ORPHAN)
	private Set<ReferenceDetail> referenceDetail = new HashSet<ReferenceDetail>();
	
	// Idable interface
	public String getId() {
		return refDocCode;
	}

    public void setId(String id) {
    	this.refDocCode = id;
    }
    
	public String getIdPropertyName() {
    	return "refDocCode";
    }

	public String getRefDocCode() {
		return refDocCode;
	}

	public void setRefDocCode(String refDocCode) {
		this.refDocCode = refDocCode;
	}

	public String getRefTypeCode() {
		return refTypeCode;
	}

	public void setRefTypeCode(String refTypeCode) {
		this.refTypeCode = refTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<ReferenceDetail> getReferenceDetail() {
		return referenceDetail;
	}

	public void setReferenceDetail(Set<ReferenceDetail> referenceDetail) {
		this.referenceDetail = referenceDetail;
	}
}
