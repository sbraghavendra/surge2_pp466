package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityItemDTO;

/**
 * @author Paul Govindan
 *
 */

@Entity
@Table(name="TB_ENTITY")
public class EntityItem extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="ENTITY_ID")	
	/*@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="entity_sequence")*/
	/*@SequenceGenerator(name="entity_sequence" , allocationSize = 1, sequenceName="sq_tb_entity_id")	*/
    private Long entityId;
    
    @Column(name="ENTITY_CODE")    
    private String entityCode;  
    
    @Column(name="ENTITY_NAME")    
    private String entityName;  
    
    @Column(name="ENTITY_LEVEL_ID")    
    private Long entityLevelId;  
    
    @Column(name="PARENT_ENTITY_ID")    
    private Long parentEntityId; 
 
    @Column(name="FEIN")    
    private String fein;  
    
    @Column(name="DESCRIPTION")    
    private String description; 
    
    @Column(name="JURISDICTION_ID")    
    private Long jurisdictionId;  
    
    @Column(name="GEOCODE")    
    private String geocode; 
    
    @Column(name="ADDRESS_LINE_1")    
    private String addressLine1; 
    
    @Column(name="ADDRESS_LINE_2")    
    private String addressLine2; 
    
    @Column(name="CITY")    
    private String city; 
    
    @Column(name="COUNTY")    
    private String county; 
    
    @Column(name="STATE")    
    private String state; 
    
    @Column(name="ZIP")    
    private String zip; 
    
    @Column(name="ZIPPLUS4")    
    private String zipPlus4; 
    
    @Column(name="COUNTRY")    
    private String country; 
    
    @Column(name="LOCK_FLAG")    
    private String lockFlag; 
    
    @Column(name="ACTIVE_FLAG")    
    private String activeFlag; 
    
    @Column(name="EXEMPT_GRACE_DAYS")    
    private Long exemptGraceDays;  
    
    //Midtier project code added - january 2009
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ENTITY_LEVEL_ID",insertable=false,updatable=false)
    @ForeignKey(name="tb_entity_fk01")
    private EntityLevel entityLevelObj;
    
    @OneToMany(cascade = {CascadeType.REMOVE},mappedBy  = "entity")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	private Set<UserEntity> userEntity = new HashSet<UserEntity>();
    
	@Transient	
	EntityItemDTO entityItemDTO;
	
	@Transient
	private String type = "";
	
	@Transient
	private String selectFlag;
	
	@Transient
	private String compCode = "";
	
	@Transient
	private String compName = "";
	
	@Transient
	private String divnCode = "";
	
	@Transient
	private String divnName = "";
	
	@Transient
	private String locnCode = "";
	
	@Transient
	private String entityCodeCallback = "";
	
	@Transient
	private String locnName = "";
	
    
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
    	this.type = type;
    } 
    
	public String getCompCode() {
        return this.compCode;
    }
    
    public void setCompCode(String compCode) {
    	this.compCode = compCode;
    } 
    
    public String getCompName() {
        return this.compName;
    }
    
    public void setCompName(String compName) {
    	this.compName = compName;
    } 
    
    public String getDivnCode() {
        return this.divnCode;
    }
    
    public void setDivnCode(String divnCode) {
    	this.divnCode = divnCode;
    } 
    
    public String getDivnName() {
        return this.divnName;
    }
    
    public void setDivnName(String divnName) {
    	this.divnName = divnName;
    } 
    
    public String getLocnCode() {
        return this.locnCode;
    }
    
    public void setLocnCode(String locnCode) {
    	this.locnCode = locnCode;
    } 
    
    public String getLocnName() {
        return this.locnName;
    }
    
    public void setLocnName(String locnName) {
    	this.locnName = locnName;
    } 
	
    public EntityItem() { 
    	
    }
    
    public EntityItem(String entityId,String entityCode,String entityName){
    	this.entityId=Long.valueOf(entityId);
    	this.entityCode=entityCode;
    	this.entityName=entityName;
    }
	
	public EntityItemDTO getEntityItemDTO() {
		EntityItemDTO entityItemDTO = new EntityItemDTO();
		BeanUtils.copyProperties(this, entityItemDTO);
		return entityItemDTO;
	}
    
    public Long getEntityId() {
        return this.entityId;
    }
    
    public void setEntityId(Long entityId) {
    	this.entityId = entityId;
    }
    
    public String getEntityCode() {
        return this.entityCode;
    }
    
    public void setEntityCode(String entityCode) {
    	this.entityCode = entityCode;
    }    
    
    public String getEntityName() {
        return this.entityName;
    }
    
    public void setEntityName(String entityName) {
    	this.entityName = entityName;
    }      
    
    public Long getEntityLevelId() {
        return this.entityLevelId;
    }
    
    public void setEntityLevelId(Long entityLevelId) {
    	this.entityLevelId = entityLevelId;
    }    
    
    public Long getParentEntityId() {
        return this.parentEntityId;
    }
    
    public void setParentEntityId(Long parentEntityId) {
    	this.parentEntityId = parentEntityId;
    }  
    
    public String getFein() {
        return this.fein;
    }
    
    public void setFein(String fein) {
    	this.fein = fein;
    }   
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }  
    
    public Long getJurisdictionId() {
        return this.jurisdictionId;
    }
    
    public void setJurisdictionId(Long jurisdictionId) {
    	this.jurisdictionId = jurisdictionId;
    }   
    
    public Long getExemptGraceDays() {
        return this.exemptGraceDays;
    }
    
    public void setExemptGraceDays(Long exemptGraceDays) {
    	this.exemptGraceDays = exemptGraceDays;
    }  
    
    public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
    	this.geocode = geocode;
    }  
    
    public String getAddressLine1() {
        return this.addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
    	this.addressLine1 = addressLine1;
    }  
    
    public String getAddressLine2() {
        return this.addressLine2;
    }
    
    public void setAddressLine2(String addressLine2) {
    	this.addressLine2 = addressLine2;
    }  
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
    	this.city = city;
    }  
    
    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
    	this.county = county;
    }  
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }  
    
    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
    	this.zip = zip;
    }  
    
    public String getZipPlus4() {
        return this.zipPlus4;
    }
    
    public void setZipPlus4(String zipPlus4) {
    	this.zipPlus4 = zipPlus4;
    }  
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }  
    
    public String getLockFlag() {
        return this.lockFlag;
    }
    
    public void setLockFlag(String lockFlag) {
    	this.lockFlag = lockFlag;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  

    public String getSelectFlag() {
        return this.selectFlag;
    }
    
    public void setSelectFlag(String selectFlag) {
    	this.selectFlag = selectFlag;
    }  
    
    public Boolean getLockBooleanFlag() {
    	return "1".equals(this.lockFlag);
	}

	public void setLockBooleanFlag(Boolean lockFlag) {
		this.lockFlag = lockFlag == Boolean.TRUE ? "1" : "0";
	}
	
    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getSelectBooleanFlag() {
    	return "1".equals(this.selectFlag);
	}

	public void setSelectBooleanFlag(Boolean selectFlag) {
		this.selectFlag = selectFlag == Boolean.TRUE ? "1" : "0";
	}
    
    /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof EntityItem)) {
            return false;
        }

        final EntityItem revision = (EntityItem)other;

        Long c1 = getEntityId();
        Long c2 = revision.getEntityId();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }
        
        String d1 = getEntityCode();
        String d2 = revision.getEntityCode();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }
        
        String e1 = getEntityName();
        String e2 = revision.getEntityName();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		Long c1 = getEntityId();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());  
		String dnc1 = getEntityCode();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
		String d1 = getEntityName();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.entityId + "::" + this.entityCode + "::" + this.entityName;
    }

	public Set<UserEntity> getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(Set<UserEntity> userEntity) {
		this.userEntity = userEntity;
	}

	public EntityLevel getEntityLevelObj() {
		return entityLevelObj;
	}

	public void setEntityLevelObj(EntityLevel entityLevelObj) {
		this.entityLevelObj = entityLevelObj;
	}

	@Override
	public Long getId() {
		return this.entityId;
	}

	@Override
	public void setId(Long id) {
		this.entityId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "entityId";
	}

	public String getEntityCodeCallback() {
		return entityCodeCallback;
	}

	public void setEntityCodeCallback(String entityCodeCallback) {
		this.entityCodeCallback = entityCodeCallback;
	}
	
	
}





