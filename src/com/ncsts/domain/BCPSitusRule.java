package com.ncsts.domain;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TB_BCP_SITUS_RULES")
public class BCPSitusRule implements java.io.Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="BCP_SITUS_RULES_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="bcp_situs_rule_sequence")
	@SequenceGenerator(name="bcp_situs_rule_sequence" , allocationSize = 1, sequenceName="sq_tb_bcp_situs_rules_id")	
    private Long bcpSitusRuleId;
	
	@Column(name="BATCH_ID")
	private Long batchId;
	
	@Column(name="RECORD_TYPE")
	private String recordType;
	
	@Column(name="TRANSACTION_TYPE_CODE")
	private String transactionTypeCode;
	
	@Column(name="RATETYPE_CODE")
	private String ratetypeCode;
	
	@Column(name="METHOD_DELIVERY_CODE")
	private String methodDeliveryCode;
	
	@Column(name="SDD_SITUS_DRIVER_ID")
	private Long sddSitusDriverId;
	
	@Column(name="SDD_SITUS_DRIVER_DETAIL_ID")
	private Long sddSitusDriverDetailId;
	
	@Column(name="SDD_LOCATION_TYPE_CODE")
	private String sddLocationTypeCode;
	
	@Column(name="SDD_DRIVER_ID")
	private Long sddDriverId;
	
	@Column(name="SDD_MANDATORY_FLAG")
	private String sddMandatoryFlag;
	
	@Column(name="SM_SITUS_MATRIX_ID")
	private Long smSitusMatrixId;
	
	@Column(name="SM_SITUS_COUNTRY_CODE")
	private String smSitusCountryCode;
	
	@Column(name="SM_SITUS_STATE_CODE")
	private String smSitusStateCode;
	
	@Column(name="SM_JUR_LEVEL")
	private String smJurLevel;
	
	@Column(name="SM_BINARY_WEIGHT")
	private BigDecimal smBinaryWeight;
	
	@Column(name="SM_EFFECTIVE_DATE")
	private Date smEffectiveDate;
	
	@Column(name="SM_EXPIRATION_DATE")
	private Date smExpirationDate;
	
	@Column(name="SM_PRIMARY_SITUS_CODE")
	private String smPrimarySitusCode;
	
	@Column(name="SM_PRIMARY_TAXTYPE_CODE")
	private String smPrimaryTaxtypeCode;
	
	@Column(name="SM_SECONDARY_SITUS_CODE")
	private String smSecondarySitusCode;
	
	@Column(name="SM_SECONDARY_TAXTYPE_CODE")
	private String smSecondaryTaxtypeCode;

	@Column(name="SM_COMMENTS")
	private String smComments;
	
	@Column(name="SM_ALL_LEVELS_FLAG")
	private String smAllLevelsFlag;
	
	@Column(name="SM_ACTIVE_FLAG")
	private String smActiveFlag;
	
	@Column(name="UPDATE_CODE")
	private String updateCode;
	
	@Column(name="SM_DRIVER_01")
    private String smDriver01;
    
    @Column(name="SM_DRIVER_02" )
    private String smDriver02;
    
    @Column(name="SM_DRIVER_03" )
    private String smDriver03;
    
    @Column(name="SM_DRIVER_04" )
    private String smDriver04;
    
    @Column(name="SM_DRIVER_05" )
    private String smDriver05;
    
    @Column(name="SM_DRIVER_06" )
    private String smDriver06;
  
    @Column(name="SM_DRIVER_07" )
    private String smDriver07;
    
    @Column(name="SM_DRIVER_08" )
    private String smDriver08;
    
    @Column(name="SM_DRIVER_09" )
    private String smDriver09;
    
    @Column(name="SM_DRIVER_10" )
    private String smDriver10;
  
    @Column(name="SM_DRIVER_11" )
    private String smDriver11;
 
    @Column(name="SM_DRIVER_12" )
    private String smDriver12;
  
    @Column(name="SM_DRIVER_13" )
    private String smDriver13;
    
    @Column(name="SM_DRIVER_14" )
    private String smDriver14;
    
    @Column(name="SM_DRIVER_15" )
    private String smDriver15;
   
    @Column(name="SM_DRIVER_16" )
    private String smDriver16;
    
    @Column(name="SM_DRIVER_17" )
	private String smDriver17;
            
	@Column(name="SM_DRIVER_18")
	private String smDriver18;
	
	@Column(name="SM_DRIVER_19")
	private String smDriver19;

	@Column(name="SM_DRIVER_20")
	private String smDriver20;
	
	@Column(name="SM_DRIVER_21")
	private String smDriver21;
   
	@Column(name="SM_DRIVER_22")
	private String smDriver22;
	
	@Column(name="SM_DRIVER_23")
	private String smDriver23;
	
	@Column(name="SM_DRIVER_24")
	private String smDriver24;
	
	@Column(name="SM_DRIVER_25")
	private String smDriver25;

	@Column(name="SM_DRIVER_26")
	private String smDriver26;
	
	@Column(name="SM_DRIVER_27")
	private String smDriver27;
	
	@Column(name="SM_DRIVER_28")
	private String smDriver28;
	
	@Column(name="SM_DRIVER_29")
	private String smDriver29;
	
	@Column(name="SM_DRIVER_30")
	private String smDriver30;
	
	@Transient
	private String text;
	
	@Transient
	private Long line;
	
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.NUMERIC);
	}
	
	public BCPSitusRule() {
	}

	public Long getId() {
		return bcpSitusRuleId;
	}

	public void setId(Long id) {
		this.bcpSitusRuleId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "bcpSitusRuleId";
	}

	public Long getBcpSitusRuleId() {
		return bcpSitusRuleId;
	}

	public void setBcpSitusRuleId(Long bcpSitusRuleId) {
		this.bcpSitusRuleId = bcpSitusRuleId;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getTransactionTypeCode() {
		return transactionTypeCode;
	}

	public void setTransactionTypeCode(String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}

	public String getRatetypeCode() {
		return ratetypeCode;
	}

	public void setRatetypeCode(String ratetypeCode) {
		this.ratetypeCode = ratetypeCode;
	}

	public String getMethodDeliveryCode() {
		return methodDeliveryCode;
	}

	public void setMethodDeliveryCode(String methodDeliveryCode) {
		this.methodDeliveryCode = methodDeliveryCode;
	}

	public String getSddLocationTypeCode() {
		return sddLocationTypeCode;
	}

	public void setSddLocationTypeCode(String sddLocationTypeCode) {
		this.sddLocationTypeCode = sddLocationTypeCode;
	}

	public Long getSddDriverId() {
		return sddDriverId;
	}

	public void setSddDriverId(Long sddDriverId) {
		this.sddDriverId = sddDriverId;
	}

	public String getSmSitusCountryCode() {
		return smSitusCountryCode;
	}

	public void setSmSitusCountryCode(String smSitusCountryCode) {
		this.smSitusCountryCode = smSitusCountryCode;
	}

	public String getSddMandatoryFlag() {
		return sddMandatoryFlag;
	}

	public void setSddMandatoryFlag(String sddMandatoryFlag) {
		this.sddMandatoryFlag = sddMandatoryFlag;
	}

	public String getSmSitusStateCode() {
		return smSitusStateCode;
	}

	public void setSmSitusStateCode(String smSitusStateCode) {
		this.smSitusStateCode = smSitusStateCode;
	}

	public String getSmJurLevel() {
		return smJurLevel;
	}

	public void setSmJurLevel(String smJurLevel) {
		this.smJurLevel = smJurLevel;
	}
	
	public BigDecimal getSmBinaryWeight() {
		return smBinaryWeight;
	}

	public void setSmBinaryWeight(BigDecimal smBinaryWeight) {
		this.smBinaryWeight = smBinaryWeight;
	}

	public Date getSmEffectiveDate() {
		return smEffectiveDate;
	}

	public void setSmEffectiveDate(Date smEffectiveDate) {
		this.smEffectiveDate = smEffectiveDate;
	}

	public Date getSmExpirationDate() {
		return smExpirationDate;
	}

	public void setSmExpirationDate(Date smExpirationDate) {
		this.smExpirationDate = smExpirationDate;
	}

	public String getSmPrimarySitusCode() {
		return smPrimarySitusCode;
	}

	public void setSmPrimarySitusCode(String smPrimarySitusCode) {
		this.smPrimarySitusCode = smPrimarySitusCode;
	}

	public String getSmPrimaryTaxtypeCode() {
		return smPrimaryTaxtypeCode;
	}

	public void setSmPrimaryTaxtypeCode(String smPrimaryTaxtypeCode) {
		this.smPrimaryTaxtypeCode = smPrimaryTaxtypeCode;
	}

	public String getSmSecondarySitusCode() {
		return smSecondarySitusCode;
	}

	public void setSmSecondarySitusCode(String smSecondarySitusCode) {
		this.smSecondarySitusCode = smSecondarySitusCode;
	}

	public String getSmSecondaryTaxtypeCode() {
		return smSecondaryTaxtypeCode;
	}

	public void setSmSecondaryTaxtypeCode(String smSecondaryTaxtypeCode) {
		this.smSecondaryTaxtypeCode = smSecondaryTaxtypeCode;
	}

	public String getSmComments() {
		return smComments;
	}

	public void setSmComments(String smComments) {
		this.smComments = smComments;
	}

	public String getSmAllLevelsFlag() {
		return smAllLevelsFlag;
	}

	public void setSmAllLevelsFlag(String smAllLevelsFlag) {
		this.smAllLevelsFlag = smAllLevelsFlag;
	}
	
	public String getSmActiveFlag() {
		return smActiveFlag;
	}

	public void setSmActiveFlag(String smActiveFlag) {
		this.smActiveFlag = smActiveFlag;
	}
	
	public String getUpdateCode() {
		return updateCode;
	}
	
	public Long getSddSitusDriverId() {
		return sddSitusDriverId;
	}

	public void setSddSitusDriverId(Long sddSitusDriverId) {
		this.sddSitusDriverId = sddSitusDriverId;
	}
	
	public Long getSddSitusDriverDetailId() {
		return sddSitusDriverDetailId;
	}

	public void setSddSitusDriverDetailId(Long sddSitusDriverDetailId) {
		this.sddSitusDriverDetailId = sddSitusDriverDetailId;
	}
	
	public Long getSmSitusMatrixId() {
		return smSitusMatrixId;
	}

	public void setSmSitusMatrixId(Long smSitusMatrixId) {
		this.smSitusMatrixId = smSitusMatrixId;
	}

	public void setUpdateCode(String updateCode) {
		this.updateCode = updateCode;
	}
	
	public String getSmDriver01() {
		return smDriver01;
	}

	public void setSmDriver01(String smDriver01) {
		this.smDriver01 = smDriver01;
	}

	public String getSmDriver02() {
		return smDriver02;
	}

	public void setSmDriver02(String smDriver02) {
		this.smDriver02 = smDriver02;
	}

	public String getSmDriver03() {
		return smDriver03;
	}

	public void setSmDriver03(String smDriver03) {
		this.smDriver03 = smDriver03;
	}

	public String getSmDriver04() {
		return smDriver04;
	}

	public void setSmDriver04(String smDriver04) {
		this.smDriver04 = smDriver04;
	}

	public String getSmDriver05() {
		return smDriver05;
	}

	public void setSmDriver05(String smDriver05) {
		this.smDriver05 = smDriver05;
	}

	public String getSmDriver06() {
		return smDriver06;
	}

	public void setSmDriver06(String smDriver06) {
		this.smDriver06 = smDriver06;
	}

	public String getSmDriver07() {
		return smDriver07;
	}

	public void setSmDriver07(String smDriver07) {
		this.smDriver07 = smDriver07;
	}

	public String getSmDriver08() {
		return smDriver08;
	}

	public void setSmDriver08(String smDriver08) {
		this.smDriver08 = smDriver08;
	}

	public String getSmDriver09() {
		return smDriver09;
	}

	public void setSmDriver09(String smDriver09) {
		this.smDriver09 = smDriver09;
	}

	public String getSmDriver10() {
		return smDriver10;
	}

	public void setSmDriver10(String smDriver10) {
		this.smDriver10 = smDriver10;
	}

	public String getSmDriver11() {
		return smDriver11;
	}

	public void setSmDriver11(String smDriver11) {
		this.smDriver11 = smDriver11;
	}

	public String getSmDriver12() {
		return smDriver12;
	}

	public void setSmDriver12(String smDriver12) {
		this.smDriver12 = smDriver12;
	}

	public String getSmDriver13() {
		return smDriver13;
	}

	public void setSmDriver13(String smDriver13) {
		this.smDriver13 = smDriver13;
	}
	
	public String getSmDriver14() {
		return smDriver14;
	}

	public void setSmDriver14(String smDriver14) {
		this.smDriver14 = smDriver14;
	}

	public String getSmDriver15() {
		return smDriver15;
	}

	public void setSmDriver15(String smDriver15) {
		this.smDriver15 = smDriver15;
	}

	public String getSmDriver16() {
		return smDriver16;
	}

	public void setSmDriver16(String smDriver16) {
		this.smDriver16 = smDriver16;
	}

	public String getSmDriver17() {
		return smDriver17;
	}

	public void setSmDriver17(String smDriver17) {
		this.smDriver17 = smDriver17;
	}

	public String getSmDriver18() {
		return smDriver18;
	}

	public void setSmDriver18(String smDriver18) {
		this.smDriver18 = smDriver18;
	}

	public String getSmDriver19() {
		return smDriver19;
	}

	public void setSmDriver19(String smDriver19) {
		this.smDriver19 = smDriver19;
	}

	public String getSmDriver20() {
		return smDriver20;
	}

	public void setSmDriver20(String smDriver20) {
		this.smDriver20 = smDriver20;
	}

	public String getSmDriver21() {
		return smDriver21;
	}

	public void setSmDriver21(String smDriver21) {
		this.smDriver21 = smDriver21;
	}

	public String getSmDriver22() {
		return smDriver22;
	}

	public void setSmDriver22(String smDriver22) {
		this.smDriver22 = smDriver22;
	}

	public String getSmDriver23() {
		return smDriver23;
	}

	public void setSmDriver23(String smDriver23) {
		this.smDriver23 = smDriver23;
	}

	public String getSmDriver24() {
		return smDriver24;
	}

	public void setSmDriver24(String smDriver24) {
		this.smDriver24 = smDriver24;
	}

	public String getSmDriver25() {
		return smDriver25;
	}

	public void setSmDriver25(String smDriver25) {
		this.smDriver25 = smDriver25;
	}

	public String getSmDriver26() {
		return smDriver26;
	}

	public void setSmDriver26(String smDriver26) {
		this.smDriver26 = smDriver26;
	}

	public String getSmDriver27() {
		return smDriver27;
	}

	public void setSmDriver27(String smDriver27) {
		this.smDriver27 = smDriver27;
	}

	public String getSmDriver28() {
		return smDriver28;
	}

	public void setSmDriver28(String smDriver28) {
		this.smDriver28 = smDriver28;
	}

	public String getSmDriver29() {
		return smDriver29;
	}

	public void setSmDriver29(String smDriver29) {
		this.smDriver29 = smDriver29;
	}

	public String getSmDriver30() {
		return smDriver30;
	}

	public void setSmDriver30(String smDriver30) {
		this.smDriver30 = smDriver30;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getLine() {
		return line;
	}

	public void setLine(Long line) {
		this.line = line;
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPSitusRule.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_SITUS_RULES (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPSitusRule.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){					
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}		
	}
}
