package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.VendorNexusDtlDTO;

@Entity
@Table(name="TB_VENDOR_NEXUS_DTL")
public class VendorNexusDtl extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="VENDOR_NEXUS_DTL_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="vendor_nexus_dtl_sequence")
	@SequenceGenerator(name="vendor_nexus_dtl_sequence" , allocationSize = 1, sequenceName="sq_tb_vendor_nexus_dtl_id")
	private Long vendorNexusDtlId;
      
    @Column(name="VENDOR_NEXUS_ID")	
    private Long vendorNexusId;
    
    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
    
    @Column(name="NEXUS_TYPE_CODE")    
    private String nexusTypeCode;

    @Column(name="ACTIVE_FLAG")    
    private String activeFlag; 

    public VendorNexusDtl(){
    }
	
	public VendorNexusDtlDTO getVendorNexusDtlDTO() {
		VendorNexusDtlDTO vendorNexusDtlDTO = new VendorNexusDtlDTO();
		BeanUtils.copyProperties(this, vendorNexusDtlDTO);
		return vendorNexusDtlDTO;
	}
	
	public Long getVendorNexusDtlId() {
        return this.vendorNexusDtlId;
    }
    
    public void setVendorNexusDtlId(Long vendorNexusDtlId) {
    	this.vendorNexusDtlId = vendorNexusDtlId;
    }
	
	public Long getVendorNexusId() {
        return this.vendorNexusId;
    }
    
    public void setVendorNexusId(Long vendorNexusId) {
    	this.vendorNexusId = vendorNexusId;
    }
    
    public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
    
    public String getNexusTypeCode() {
        return this.nexusTypeCode;
    }
    
    public void setNexusTypeCode(String nexusTypeCode) {
    	this.nexusTypeCode = nexusTypeCode;
    }
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  

    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return this.vendorNexusDtlId;
	}

	@Override
	public void setId(Long id) {
		this.vendorNexusDtlId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "vendorNexusDtlId";
	}
}