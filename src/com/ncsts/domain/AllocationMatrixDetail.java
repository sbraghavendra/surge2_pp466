package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.NotFound;

@Embeddable
public class AllocationMatrixDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	
	//Midtier project code added - january 2009
    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action=NotFoundAction.IGNORE)
    @JoinColumn(name="JURISDICTION_ID", nullable = false)
	private Jurisdiction jurisdiction;
	
	@Column(name="ALLOCATION_PERCENT", nullable = false)
	private Double allocationPercent;
	
	@Transient
	private Long instanceCreatedId; //bug 5090
	
	private static long nextId = 0L;
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
    public AllocationMatrixDetail() { 
    	instanceCreatedId = new Long(getNextId());
    }
	
	public Double getAllocationPercent() {
		return allocationPercent;
	}	

	// MODIFIED - 02-12-2009 - Bug 0003956  
	public void setAllocationPercent(Double allocationPercent) {
		this.allocationPercent = allocationPercent;
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	
	
	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}

	
}

