package com.ncsts.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.lang.reflect.Field;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.SecondaryTable;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Formula;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.Util;
import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.service.impl.adapter.JaxbDateAdapter;

@Entity
@Table(name="TB_TRANSACTION_DETAIL")
@SecondaryTable(name="TB_TRANS_USER_PURCH", pkJoinColumns= {@PrimaryKeyJoinColumn(name="TRANS_USER_ID",referencedColumnName="TRANSACTION_DETAIL_ID")})


@XmlRootElement(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionDetail extends Auditable implements java.io.Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="TRANSACTION_DETAIL_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="tr_detail_sequence")
	@SequenceGenerator(name="tr_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_transaction_detail_id")
     private Long transactionDetailId;
	
	@Column(name="SOURCE_TRANSACTION_ID")
     private String sourceTransactionId;
	
	@Column(name="PROCESS_BATCH_NO")
     private Long processBatchNo;
	
	@Column(name="GL_EXTRACT_BATCH_NO")
     private Long glExtractBatchNo;
	
	@Column(name="ARCHIVE_BATCH_NO")
     private Long archiveBatchNo;
	
	@Column(name="ALLOCATION_MATRIX_ID")
     private Long allocationMatrixId;
	
	@Column(name="ALLOCATION_SUBTRANS_ID")
     private Long allocationSubtransId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date enteredDate;
	
	@Column(name="TRANSACTION_STATUS")
     private String transactionStatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name="GL_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date glDate;
	
	@Column(name="GL_COMPANY_NBR")
     private String glCompanyNbr;
	
	@Column(name="GL_COMPANY_NAME")
     private String glCompanyName;
	
	@Column(name="GL_DIVISION_NBR")
     private String glDivisionNbr;
	
	@Column(name="GL_DIVISION_NAME")
     private String glDivisionName;
	
	@Column(name="GL_CC_NBR_DEPT_ID")
     private String glCcNbrDeptId;
	
	@Column(name="GL_CC_NBR_DEPT_NAME")
     private String glCcNbrDeptName;
	
	@Column(name="GL_LOCAL_ACCT_NBR")
     private String glLocalAcctNbr;
	
	@Column(name="GL_LOCAL_ACCT_NAME")
     private String glLocalAcctName;
	
	@Column(name="GL_LOCAL_SUB_ACCT_NBR")
     private String glLocalSubAcctNbr;
	
	@Column(name="GL_LOCAL_SUB_ACCT_NAME")
     private String glLocalSubAcctName;
	
	@Column(name="GL_FULL_ACCT_NBR")
     private String glFullAcctNbr;
	
	@Column(name="GL_FULL_ACCT_NAME")
     private String glFullAcctName;
	
	@Column(name="GL_LINE_ITM_DIST_AMT")
     private BigDecimal glLineItmDistAmt;
	
	@Column(name="ORIG_GL_LINE_ITM_DIST_AMT")
     private BigDecimal origGlLineItmDistAmt;
	
	@Column(name="VENDOR_NBR")
     private String vendorNbr;
	
	@Column(name="VENDOR_NAME")
     private String vendorName;
	
	@Column(name="VENDOR_ADDRESS_LINE_1")
     private String vendorAddressLine1;
	
	@Column(name="VENDOR_ADDRESS_LINE_2")
     private String vendorAddressLine2;
	
	@Column(name="VENDOR_ADDRESS_LINE_3")
     private String vendorAddressLine3;
	
	@Column(name="VENDOR_ADDRESS_LINE_4")
     private String vendorAddressLine4;
	
	@Column(name="VENDOR_ADDRESS_CITY")
     private String vendorAddressCity;
	
	@Column(name="VENDOR_ADDRESS_COUNTY")
     private String vendorAddressCounty;
	
	@Column(name="VENDOR_ADDRESS_STATE")
     private String vendorAddressState;
	
	@Column(name="VENDOR_ADDRESS_ZIP")
     private String vendorAddressZip;
	
	@Column(name="VENDOR_ADDRESS_COUNTRY")
     private String vendorAddressCountry;
	
	@Column(name="VENDOR_TYPE")
     private String vendorType;
	
	@Column(name="VENDOR_TYPE_NAME")
     private String vendorTypeName;
	
	@Column(name="INVOICE_NBR")
     private String invoiceNbr;
	
	@Column(name="INVOICE_DESC")
     private String invoiceDesc;
	
	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date invoiceDate;
	
	@Column(name="INVOICE_FREIGHT_AMT")
     private BigDecimal invoiceFreightAmt;
	
	@Column(name="INVOICE_DISCOUNT_AMT")
     private BigDecimal invoiceDiscountAmt;
	
	@Column(name="INVOICE_TAX_AMT")
     private BigDecimal invoiceTaxAmt;
	
	@Column(name="INVOICE_TOTAL_AMT")
     private BigDecimal invoiceTotalAmt;
	
	@Column(name="INVOICE_TAX_FLG")
     private String invoiceTaxFlg;
	
	@Column(name="INVOICE_LINE_NBR")
     private String invoiceLineNbr;
	
	@Column(name="INVOICE_LINE_NAME")
     private String invoiceLineName;
	
	@Column(name="INVOICE_LINE_TYPE")
     private String invoiceLineType;
	
	@Column(name="INVOICE_LINE_TYPE_NAME")
     private String invoiceLineTypeName;
	
	@Column(name="INVOICE_LINE_AMT")
     private BigDecimal invoiceLineAmt;
	
	@Column(name="INVOICE_LINE_TAX")
     private BigDecimal invoiceLineTax;
	
	@Column(name="AFE_PROJECT_NBR")
     private String afeProjectNbr;
	
	@Column(name="AFE_PROJECT_NAME")
     private String afeProjectName;
	
	@Column(name="AFE_CATEGORY_NBR")
     private String afeCategoryNbr;
	
	@Column(name="AFE_CATEGORY_NAME")
     private String afeCategoryName;
	
	@Column(name="AFE_SUB_CAT_NBR")
     private String afeSubCatNbr;
	
	@Column(name="AFE_SUB_CAT_NAME")
     private String afeSubCatName;
	
	@Column(name="AFE_USE")
     private String afeUse;
	
	@Column(name="AFE_CONTRACT_TYPE")
     private String afeContractType;
	
	@Column(name="AFE_CONTRACT_STRUCTURE")
     private String afeContractStructure;
	
	@Column(name="AFE_PROPERTY_CAT")
     private String afePropertyCat;
	
	@Column(name="INVENTORY_NBR")
     private String inventoryNbr;
	
	@Column(name="INVENTORY_NAME")
     private String inventoryName;
	
	@Column(name="INVENTORY_CLASS")
     private String inventoryClass;
	
	@Column(name="INVENTORY_CLASS_NAME")
     private String inventoryClassName;
	
	@Column(name="PO_NBR")
     private String poNbr;
	
	@Column(name="PO_NAME")
     private String poName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="PO_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date poDate;
	
	@Column(name="PO_LINE_NBR")
     private String poLineNbr;
	
	@Column(name="PO_LINE_NAME")
     private String poLineName;
	
	@Column(name="PO_LINE_TYPE")
     private String poLineType;
	
	@Column(name="PO_LINE_TYPE_NAME")
     private String poLineTypeName;
	
	@Column(name="SHIP_TO_LOCATION")
     private String shipToLocation;
	
	@Column(name="SHIP_TO_LOCATION_NAME")
     private String shipToLocationName;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_1")
     private String shipToAddressLine1;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_2")
     private String shipToAddressLine2;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_3")
     private String shipToAddressLine3;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_4")
     private String shipToAddressLine4;
	
	@Column(name="SHIP_TO_ADDRESS_CITY")
     private String shipToAddressCity;
	
	@Column(name="SHIP_TO_ADDRESS_COUNTY")
     private String shipToAddressCounty;
	
	@Column(name="SHIP_TO_ADDRESS_STATE")
     private String shipToAddressState;
	
	@Column(name="SHIP_TO_ADDRESS_ZIP")
     private String shipToAddressZip;
	
	@Column(name="SHIP_TO_ADDRESS_COUNTRY")
     private String shipToAddressCountry;
	
	@Column(name="WO_NBR")
     private String woNbr;
	
	@Column(name="WO_NAME")
     private String woName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="WO_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date woDate;
	
	@Column(name="WO_TYPE")
     private String woType;
	
	@Column(name="WO_TYPE_DESC")
     private String woTypeDesc;
	
	@Column(name="WO_CLASS")
     private String woClass;
	
	@Column(name="WO_CLASS_DESC")
     private String woClassDesc;
	
	@Column(name="WO_ENTITY")
     private String woEntity;
	
	@Column(name="WO_ENTITY_DESC")
     private String woEntityDesc;
	
	@Column(name="WO_LINE_NBR")
     private String woLineNbr;
	
	@Column(name="WO_LINE_NAME")
     private String woLineName;
	
	@Column(name="WO_LINE_TYPE")
     private String woLineType;
	
	@Column(name="WO_LINE_TYPE_DESC")
     private String woLineTypeDesc;
	
	@Column(name="WO_SHUT_DOWN_CD")
     private String woShutDownCd;
	
	@Column(name="WO_SHUT_DOWN_CD_DESC")
     private String woShutDownCdDesc;
	
	@Column(name="VOUCHER_ID")
     private String voucherId;
	
	@Column(name="VOUCHER_NAME")
     private String voucherName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="VOUCHER_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date voucherDate;
	
	
	@Column(name="VOUCHER_LINE_NBR")
     private String voucherLineNbr;
	
	@Column(name="VOUCHER_LINE_DESC")
     private String voucherLineDesc;
	
	@Column(name="CHECK_NBR")
     private String checkNbr;
	
	@Column(name="CHECK_NO")
     private Long checkNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CHECK_DATE")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date checkDate;
	
	@Column(name="CHECK_AMT")
     private BigDecimal checkAmt;
	
	@Column(name="CHECK_DESC")
     private String checkDesc;
	
	@Column(name="USER_TEXT_01")
     private String userText01;
	
	@Column(name="USER_TEXT_02")
     private String userText02;
	
	@Column(name="USER_TEXT_03")
     private String userText03;
	
	@Column(name="USER_TEXT_04")
     private String userText04;
	
	@Column(name="USER_TEXT_05")
     private String userText05;
	
	@Column(name="USER_TEXT_06")
     private String userText06;
	
	@Column(name="USER_TEXT_07")
     private String userText07;
	
	@Column(name="USER_TEXT_08")
     private String userText08;
	
	@Column(name="USER_TEXT_09")
     private String userText09;
	
	@Column(name="USER_TEXT_10")
     private String userText10;
	
	@Column(name="USER_TEXT_11")
     private String userText11;
	
	@Column(name="USER_TEXT_12")
     private String userText12;
	
	@Column(name="USER_TEXT_13")
     private String userText13;
	
	@Column(name="USER_TEXT_14")
     private String userText14;
	
	@Column(name="USER_TEXT_15")
     private String userText15;
	
	@Column(name="USER_TEXT_16")
     private String userText16;
	
	@Column(name="USER_TEXT_17")
     private String userText17;
	
	@Column(name="USER_TEXT_18")
     private String userText18;
	
	@Column(name="USER_TEXT_19")
     private String userText19;
	
	@Column(name="USER_TEXT_20")
     private String userText20;
	
	@Column(name="USER_TEXT_21")
     private String userText21;
	
	@Column(name="USER_TEXT_22")
     private String userText22;
	
	@Column(name="USER_TEXT_23")
     private String userText23;
	
	@Column(name="USER_TEXT_24")
     private String userText24;
	
	@Column(name="USER_TEXT_25")
     private String userText25;
	
	@Column(name="USER_TEXT_26")
     private String userText26;
	
	@Column(name="USER_TEXT_27")
     private String userText27;
	
	@Column(name="USER_TEXT_28")
     private String userText28;
	
	@Column(name="USER_TEXT_29")
     private String userText29;
	
	@Column(name="USER_TEXT_30")
     private String userText30;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_31")
	private String userText31;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_32")
	private String userText32;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_33")
	private String userText33;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_34")
	private String userText34;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_35")
	private String userText35;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_36")
	private String userText36;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_37")
	private String userText37;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_38")
	private String userText38;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_39")
	private String userText39;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_40")
	private String userText40;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_41")
	private String userText41;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_42")
	private String userText42;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_43")
	private String userText43;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_44")
	private String userText44;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_45")
	private String userText45;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_46")
	private String userText46;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_47")
	private String userText47;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_48")
	private String userText48;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_49")
	private String userText49;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_50")
	private String userText50;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_51")
	private String userText51;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_52")
	private String userText52;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_53")
	private String userText53;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_54")
	private String userText54;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_55")
	private String userText55;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_56")
	private String userText56;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_57")
	private String userText57;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_58")
	private String userText58;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_59")
	private String userText59;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_60")
	private String userText60;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_61")
	private String userText61;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_62")
	private String userText62;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_63")
	private String userText63;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_64")
	private String userText64;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_65")
	private String userText65;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_66")
	private String userText66;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_67")
	private String userText67;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_68")
	private String userText68;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_69")
	private String userText69;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_70")
	private String userText70;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_71")
	private String userText71;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_72")
	private String userText72;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_73")
	private String userText73;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_74")
	private String userText74;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_75")
	private String userText75;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_76")
	private String userText76;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_77")
	private String userText77;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_78")
	private String userText78;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_79")
	private String userText79;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_80")
	private String userText80;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_81")
	private String userText81;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_82")
	private String userText82;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_83")
	private String userText83;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_84")
	private String userText84;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_85")
	private String userText85;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_86")
	private String userText86;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_87")
	private String userText87;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_88")
	private String userText88;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_89")
	private String userText89;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_90")
	private String userText90;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_91")
	private String userText91;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_92")
	private String userText92;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_93")
	private String userText93;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_94")
	private String userText94;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_95")
	private String userText95;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_96")
	private String userText96;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_97")
	private String userText97;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_98")
	private String userText98;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_99")
	private String userText99;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_TEXT_100")
	private String userText100;
	
	@Column(name="USER_NUMBER_01")
     private BigDecimal userNumber01;
	
	@Column(name="USER_NUMBER_02")
     private BigDecimal userNumber02;
	
	@Column(name="USER_NUMBER_03")
     private BigDecimal userNumber03;
	
	@Column(name="USER_NUMBER_04")
     private BigDecimal userNumber04;
	
	@Column(name="USER_NUMBER_05")
     private BigDecimal userNumber05;
	
	@Column(name="USER_NUMBER_06")
     private BigDecimal userNumber06;
	
	@Column(name="USER_NUMBER_07")
     private BigDecimal userNumber07;
	
	@Column(name="USER_NUMBER_08")
     private BigDecimal userNumber08;
	
	@Column(name="USER_NUMBER_09")
     private BigDecimal userNumber09;
	
	@Column(name="USER_NUMBER_10")
     private BigDecimal userNumber10;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_11")
	private BigDecimal userNumber11;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_12")
	private BigDecimal userNumber12;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_13")
	private BigDecimal userNumber13;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_14")
	private BigDecimal userNumber14;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_15")
	private BigDecimal userNumber15;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_16")
	private BigDecimal userNumber16;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_17")
	private BigDecimal userNumber17;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_18")
	private BigDecimal userNumber18;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_19")
	private BigDecimal userNumber19;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_20")
	private BigDecimal userNumber20;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_21")
	private BigDecimal userNumber21;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_22")
	private BigDecimal userNumber22;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_23")
	private BigDecimal userNumber23;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_24")
	private BigDecimal userNumber24;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_NUMBER_25")
	private BigDecimal userNumber25;
		
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_01")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate01;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_02")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate02;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_03")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate03;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_04")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate04;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_05")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate05;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_06")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate06;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_07")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate07;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_08")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate08;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_09")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate09;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USER_DATE_10")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date userDate10;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_11")
	private Date userDate11;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_12")
	private Date userDate12;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_13")
	private Date userDate13;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_14")
	private Date userDate14;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_15")
	private Date userDate15;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_16")
	private Date userDate16;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_17")
	private Date userDate17;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_18")
	private Date userDate18;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_19")
	private Date userDate19;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_20")
	private Date userDate20;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_21")
	private Date userDate21;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_22")
	private Date userDate22;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_23")
	private Date userDate23;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_24")
	private Date userDate24;
	
	@Column(table="TB_TRANS_USER_PURCH",name="USER_DATE_25")
	private Date userDate25;
	
	@Column(name="COMMENTS")
     private String comments;
	
	@Column(name="TB_CALC_TAX_AMT")
     private BigDecimal tbCalcTaxAmt;
	
	@Column(name="STATE_USE_AMOUNT")
     private BigDecimal stateUseAmount;
	
	@Column(name="STATE_USE_TIER2_AMOUNT")
     private BigDecimal stateUseTier2Amount;
	
	@Column(name="STATE_USE_TIER3_AMOUNT")
     private BigDecimal stateUseTier3Amount;
	
	@Column(name="COUNTY_USE_AMOUNT")
     private BigDecimal countyUseAmount;
	
	@Column(name="COUNTY_LOCAL_USE_AMOUNT")
     private BigDecimal countyLocalUseAmount;
	
	@Column(name="CITY_USE_AMOUNT")
     private BigDecimal cityUseAmount;
	
	@Column(name="CITY_LOCAL_USE_AMOUNT")
     private BigDecimal cityLocalUseAmount;
	
	@Column(name="TRANSACTION_STATE_CODE")
     private String transactionStateCode;
	
	@Column(name="AUTO_TRANSACTION_STATE_CODE")
     private String autoTransactionStateCode;
	
	@Column(name="TRANSACTION_IND")
     private String transactionInd;
	
	@Column(name="SUSPEND_IND")
     private String suspendInd;
	
	@Column(name="TAXCODE_CODE")
     private String taxcodeCode;
	
	//CCH	@Column(name="CCH_TAXCAT_CODE")
	//CCH     private String cchTaxcatCode;
	
	//CCH	@Column(name="CCH_GROUP_CODE")
	//CCH     private String cchGroupCode;
	
	//CCH	@Column(name="CCH_ITEM_CODE")
	//CCH     private String cchItemCode;
	
	@Column(name="MANUAL_TAXCODE_IND")
     private String manualTaxcodeInd;
	
	@Column(name="TAX_MATRIX_ID")
     private Long taxMatrixId;
	
	@Column(name="LOCATION_MATRIX_ID")
     private Long locationMatrixId;
	
	@Column(name="JURISDICTION_ID")
     private Long jurisdictionId;
	
	@Column(name="JURISDICTION_TAXRATE_ID")
     private Long jurisdictionTaxrateId;
	
	@Column(name="MANUAL_JURISDICTION_IND")
     private String manualJurisdictionInd;
	
	@Column(name="MEASURE_TYPE_CODE")
	private String measureTypeCode;
	
	@Column(name="STATE_USE_RATE")
     private BigDecimal stateUseRate;
	
	@Column(name="STATE_USE_TIER2_RATE")
     private BigDecimal stateUseTier2Rate;
	
	@Column(name="STATE_USE_TIER3_RATE")
     private BigDecimal stateUseTier3Rate;
	
	@Column(name="STATE_SPLIT_AMOUNT")
     private BigDecimal stateSplitAmount;
	
	@Column(name="STATE_TIER2_MIN_AMOUNT")
     private BigDecimal stateTier2MinAmount;
	
	@Column(name="STATE_TIER2_MAX_AMOUNT")
     private BigDecimal stateTier2MaxAmount;
	
	@Column(name="STATE_MAXTAX_AMOUNT")
     private BigDecimal stateMaxtaxAmount;
	
	@Column(name="COUNTY_USE_RATE")
     private BigDecimal countyUseRate;
	
	@Column(name="COUNTY_LOCAL_USE_RATE")
     private BigDecimal countyLocalUseRate;
	
	@Column(name="COUNTY_SPLIT_AMOUNT")
     private BigDecimal countySplitAmount;
	
	@Column(name="COUNTY_MAXTAX_AMOUNT")
     private BigDecimal countyMaxtaxAmount;
	
	@Column(name="COUNTY_SINGLE_FLAG")
     private String countySingleFlag;
	
	@Column(name="COUNTY_DEFAULT_FLAG")
     private String countyDefaultFlag;
	
	@Column(name="CITY_USE_RATE")
     private BigDecimal cityUseRate;
	
	@Column(name="CITY_LOCAL_USE_RATE")
     private BigDecimal cityLocalUseRate;
	
	@Column(name="CITY_SPLIT_AMOUNT")
     private BigDecimal citySplitAmount;
	
	@Column(name="CITY_SPLIT_USE_RATE")
     private BigDecimal citySplitUseRate;
	
	@Column(name="CITY_SINGLE_FLAG")
     private String citySingleFlag;
	
	@Column(name="CITY_DEFAULT_FLAG")
     private String cityDefaultFlag;
	
	@Column(name="COMBINED_USE_RATE")
     private BigDecimal combinedUseRate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LOAD_TIMESTAMP")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date loadTimestamp;
	
	@Column(name="GL_EXTRACT_UPDATER")
     private String glExtractUpdater;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GL_EXTRACT_TIMESTAMP")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date glExtractTimestamp;
	
	@Column(name="GL_EXTRACT_FLAG")
     private Long glExtractFlag;
	
	@Column(name="GL_LOG_FLAG")
     private Long glLogFlag;
	
	@Column(name="GL_EXTRACT_AMT")
     private BigDecimal glExtractAmt;
	
	@Column(name="AUDIT_FLAG")
     private String auditFlag;
	
	@Column(name="REJECT_FLAG")
    private String rejectFlag;
	
	@Column(name="COUNTRY_USE_AMOUNT")
	private BigDecimal countryUseAmount;
		
	@Column(name="TRANSACTION_COUNTRY_CODE")
	private String transactionCountryCode;
		
	@Column(name="AUTO_TRANSACTION_COUNTRY_CODE")
	private String autoTransactionCountryCode;
		
	@Column(name="COUNTRY_USE_RATE")
	private BigDecimal countryUseRate;
	
	@Column(name="ENTITY_CODE")
    private String entityCode;
	
	@Column(name="AUDIT_USER_ID")
     private String auditUserId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AUDIT_TIMESTAMP")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date auditTimestamp;
	
	@Column(name="MODIFY_USER_ID")
     private String modifyUserId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_TIMESTAMP")
	@XmlJavaTypeAdapter(JaxbDateAdapter.class)
	@XmlElement
     private Date modifyTimestamp;
	
	//0005077
	@Column(name="STJ1_RATE")
	private BigDecimal stj1Rate;

	@Column(name="STJ2_RATE")
	private BigDecimal stj2Rate;

	@Column(name="STJ3_RATE")
	private BigDecimal stj3Rate;

	@Column(name="STJ4_RATE")
	private BigDecimal stj4Rate;

	@Column(name="STJ5_RATE")
	private BigDecimal stj5Rate;
	
	@Column(name="STJ1_AMOUNT")
	private BigDecimal stj1Amount;

	@Column(name="STJ2_AMOUNT")
	private BigDecimal stj2Amount;

	@Column(name="STJ3_AMOUNT")
	private BigDecimal stj3Amount;

	@Column(name="STJ4_AMOUNT")
	private BigDecimal stj4Amount;

	@Column(name="STJ5_AMOUNT")
	private BigDecimal stj5Amount;
	
	//@Formula removed, Bug - 3925
	//JMW: no CONCAT in Postgres, changing to use ||
	/*@Formula("CASE TRANSACTION_IND "+
				" WHEN 'P' THEN 'Processed/' ||" +
	                  " CASE GL_EXTRACT_FLAG " + 
	                  		" WHEN 1 THEN 'Closed' " +
	                  		" ELSE 'OPEN' " +
	                  	" END" +
	            " WHEN 'S' THEN 'Suspended/' || " +
                      "CASE SUSPEND_IND " + 
		              		" WHEN 'T' THEN 'No Tax Matrix' " +
		              		" WHEN 'L' THEN 'No Location Matrix' " +
		              		" WHEN 'R' THEN 'No Tax Rate' " +
		              		" WHEN '?' THEN 'Unknown Suspend Error' " +
		              		" ELSE 'Suspend Ind. Error' " +
                      " END " +
      	        " WHEN 'Q' THEN 'Question' " +
      	        " WHEN 'H' THEN 'Held' " +
	            " ELSE 'TRANSACTION Ind. Error' " +
	        " END")
	private String processStatus;*/
	
	@Column(name="TRANSACTION_IND", insertable=false, updatable=false)
	private String processStatus;
	
	@Formula("CASE WHEN COMMENTS IS NULL THEN 0 WHEN LENGTH(TRIM(COMMENTS)) = 0 THEN 0 ELSE 1 END")
	private Boolean hasComments;
	

	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROCESS_BATCH_NO", referencedColumnName="BATCH_ID", insertable=false, updatable=false)
	@ForeignKey(name="tb_transaction_detail_fk01")
	@XmlTransient
	private BatchMaintenance batch;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ALLOCATION_MATRIX_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk02")
	@XmlTransient
	private AllocationMatrix allocationMatrix;
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="TRANSACTION_STATE_CODE", referencedColumnName="TAXCODE_STATE_CODE", insertable=false, updatable=false)
//	@ForeignKey(name="tb_transaction_detail_fk03")
//	private TaxCodeState taxCodeState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns( {
		@JoinColumn(name="TRANSACTION_STATE_CODE", referencedColumnName="TAXCODE_STATE_CODE", insertable=false, updatable=false),
		@JoinColumn(name="TRANSACTION_COUNTRY_CODE", referencedColumnName="TAXCODE_COUNTRY_CODE",insertable=false, updatable=false) } )
		@ForeignKey(name="tb_transaction_detail_fk03")
	@XmlTransient
	private TaxCodeState taxCodeState;
	
	
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TAXCODE_DETAIL_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk04")
	@XmlTransient
	private TaxCodeDetail taxCodeDetail;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TAX_MATRIX_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk05")
	@XmlTransient
	private TaxMatrix taxMatrix;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATION_MATRIX_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk06")
	@XmlTransient
	private LocationMatrix locationMatrix;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="JURISDICTION_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk07")
	@XmlTransient
	private Jurisdiction jurisdiction;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="JURISDICTION_TAXRATE_ID", insertable=false, updatable=false )
	@ForeignKey(name="tb_transaction_detail_fk08")
	@XmlTransient
	private JurisdictionTaxrate jurisdictionTaxrate;
	
	@Column(name="SPLIT_SUBTRANS_ID")
    private Long splitSubtransId;
	
	@Column(name="MULTI_TRANS_CODE")
    private String multiTransCode;
		
	@Column(name="TAX_ALLOC_MATRIX_ID")
    private Long taxAllocMatrixId;
	
	@Column(name="STATE_TAXCODE_DETAIL_ID")
	private Long stateTaxcodeDetailId;
	
	@Column(name="COUNTY_TAXCODE_DETAIL_ID")
	private Long countyTaxcodeDetailId;
	
	@Column(name="CITY_TAXCODE_DETAIL_ID")
	private Long  cityTaxcodeDetailId;
	
	@Column(name="TAXTYPE_USED_CODE")
	private String taxtypeUsedCode;
	
	@Column(name="STATE_TAXABLE_AMT")
	private BigDecimal stateTaxableAmt;
	
	@Column(name="COUNTY_TAXABLE_AMT")
	private BigDecimal  countyTaxableAmt;
	
	@Column(name="CITY_TAXABLE_AMT")
	private BigDecimal cityTaxableAmt;
	
	@Column(name="STATE_TAXCODE_TYPE_CODE")
	private String stateTaxcodeTypeCode;
	
	@Column(name="COUNTY_TAXCODE_TYPE_CODE")
	private String countyTaxcodeTypeCode;
	
	@Column(name="CITY_TAXCODE_TYPE_CODE")
	private String cityTaxcodeTypeCode;
	
	@Column(name="STATE_USE_TIER1_SETAMT")
	private Double stateUseTier1Setamt;
	
	@Column(name="STATE_USE_TIER2_SETAMT")
	private Double stateUseTier2Setamt;
	
	@Column(name="STATE_USE_TIER3_SETAMT")
	private Double stateUseTier3Setamt;
	
	@Column(name="PRODUCT_CODE")
	private String productCode;
	
	@Column(name="CP_FILING_ENTITY_CODE")
	private String cpFilingEntityCode;
	
	@Column(name="BCP_TRANSACTION_ID")
    private String bcpTransactionId;
	
	@Transient
	@XmlTransient
	private Long instanceCreatedId; //bug 5090
	
	private static long nextId = 0L;
	
	//0005238
	@Transient
	@XmlTransient
	private boolean isReprocessing = false;
	
	//0005659 to display the rule results based on procRuleSessionId
	@Column(name="PROCRULE_SESSION_ID")
	private String procruleSessionId;
	
	public String getProcruleSessionId() {
		return procruleSessionId;
	}

	public void setProcruleSessionId(String procruleSessionId) {
		this.procruleSessionId = procruleSessionId;
	}

	public boolean getIsReprocessing() {
		return isReprocessing;
	}
	
	public void setIsReprocessing(boolean isPreprocessing) {
		this.isReprocessing = isPreprocessing;
	}
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
	/** default constructor */
	public TransactionDetail(){
		instanceCreatedId = new Long(getNextId());
	}
	
	public TransactionDetailDTO getTransactionDetailDTO(){
		TransactionDetailDTO transactionDetailDTO = new TransactionDetailDTO();
		BeanUtils.copyProperties(this, transactionDetailDTO);
		return transactionDetailDTO;
	}
	
	// Idable interface
	public Long getId() {
		return transactionDetailId == null ? instanceCreatedId : transactionDetailId;
	}

    public void setId(Long id) {
    	this.transactionDetailId = id;
    }
    
    public String getIdPropertyName() {
    	return "transactionDetailId";
    }

	public Long getTransactionDetailId() {
		return transactionDetailId;
	}

	public void setTransactionDetailId(Long transactionDetailId) {
		this.transactionDetailId = transactionDetailId;
	}

	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}
	
	public String getBcpTransactionId() {
		return bcpTransactionId;
	}

	public void setBcpTransactionId(String bcpTransactionId) {
		this.bcpTransactionId = bcpTransactionId;
	}

	public Long getProcessBatchNo() {
		return processBatchNo;
	}

	public void setProcessBatchNo(Long processBatchNo) {
		this.processBatchNo = processBatchNo;
	}

	public Long getGlExtractBatchNo() {
		return glExtractBatchNo;
	}

	public void setGlExtractBatchNo(Long glExtractBatchNo) {
		this.glExtractBatchNo = glExtractBatchNo;
	}

	public Long getArchiveBatchNo() {
		return archiveBatchNo;
	}

	public void setArchiveBatchNo(Long archiveBatchNo) {
		this.archiveBatchNo = archiveBatchNo;
	}

	public Long getAllocationMatrixId() {
		return allocationMatrixId;
	}

	public void setAllocationMatrixId(Long allocationMatrixId) {
		this.allocationMatrixId = allocationMatrixId;
	}

	public Long getAllocationSubtransId() {
		return allocationSubtransId;
	}

	public void setAllocationSubtransId(Long allocationSubtransId) {
		this.allocationSubtransId = allocationSubtransId;
	}

	public Date getEnteredDate() {
		return enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Date getGlDate() {
		return glDate;
	}

	public void setGlDate(Date glDate) {
		this.glDate = glDate;
	}

	public String getGlCompanyNbr() {
		return glCompanyNbr;
	}

	public void setGlCompanyNbr(String glCompanyNbr) {
		this.glCompanyNbr = glCompanyNbr;
	}

	public String getGlCompanyName() {
		return glCompanyName;
	}

	public void setGlCompanyName(String glCompanyName) {
		this.glCompanyName = glCompanyName;
	}

	public String getGlDivisionNbr() {
		return glDivisionNbr;
	}

	public void setGlDivisionNbr(String glDivisionNbr) {
		this.glDivisionNbr = glDivisionNbr;
	}

	public String getGlDivisionName() {
		return glDivisionName;
	}

	public void setGlDivisionName(String glDivisionName) {
		this.glDivisionName = glDivisionName;
	}

	public String getGlCcNbrDeptId() {
		return glCcNbrDeptId;
	}

	public void setGlCcNbrDeptId(String glCcNbrDeptId) {
		this.glCcNbrDeptId = glCcNbrDeptId;
	}

	public String getGlCcNbrDeptName() {
		return glCcNbrDeptName;
	}

	public void setGlCcNbrDeptName(String glCcNbrDeptName) {
		this.glCcNbrDeptName = glCcNbrDeptName;
	}

	public String getGlLocalAcctNbr() {
		return glLocalAcctNbr;
	}

	public void setGlLocalAcctNbr(String glLocalAcctNbr) {
		this.glLocalAcctNbr = glLocalAcctNbr;
	}

	public String getGlLocalAcctName() {
		return glLocalAcctName;
	}

	public void setGlLocalAcctName(String glLocalAcctName) {
		this.glLocalAcctName = glLocalAcctName;
	}

	public String getGlLocalSubAcctNbr() {
		return glLocalSubAcctNbr;
	}

	public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr) {
		this.glLocalSubAcctNbr = glLocalSubAcctNbr;
	}

	public String getGlLocalSubAcctName() {
		return glLocalSubAcctName;
	}

	public void setGlLocalSubAcctName(String glLocalSubAcctName) {
		this.glLocalSubAcctName = glLocalSubAcctName;
	}

	public String getGlFullAcctNbr() {
		return glFullAcctNbr;
	}

	public void setGlFullAcctNbr(String glFullAcctNbr) {
		this.glFullAcctNbr = glFullAcctNbr;
	}

	public String getGlFullAcctName() {
		return glFullAcctName;
	}

	public void setGlFullAcctName(String glFullAcctName) {
		this.glFullAcctName = glFullAcctName;
	}

	public BigDecimal getGlLineItmDistAmt() {
		return glLineItmDistAmt;
	}

	public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt) {
		this.glLineItmDistAmt = glLineItmDistAmt;
	}

	public BigDecimal getOrigGlLineItmDistAmt() {
		return origGlLineItmDistAmt;
	}

	public void setOrigGlLineItmDistAmt(BigDecimal origGlLineItmDistAmt) {
		this.origGlLineItmDistAmt = origGlLineItmDistAmt;
	}

	public String getVendorNbr() {
		return vendorNbr;
	}

	public void setVendorNbr(String vendorNbr) {
		this.vendorNbr = vendorNbr;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	public String getVendorAddressLine4() {
		return vendorAddressLine4;
	}

	public void setVendorAddressLine4(String vendorAddressLine4) {
		this.vendorAddressLine4 = vendorAddressLine4;
	}

	public String getVendorAddressCity() {
		return vendorAddressCity;
	}

	public void setVendorAddressCity(String vendorAddressCity) {
		this.vendorAddressCity = vendorAddressCity;
	}

	public String getVendorAddressCounty() {
		return vendorAddressCounty;
	}

	public void setVendorAddressCounty(String vendorAddressCounty) {
		this.vendorAddressCounty = vendorAddressCounty;
	}

	public String getVendorAddressState() {
		return vendorAddressState;
	}

	public void setVendorAddressState(String vendorAddressState) {
		this.vendorAddressState = vendorAddressState;
	}

	public String getVendorAddressZip() {
		return vendorAddressZip;
	}

	public void setVendorAddressZip(String vendorAddressZip) {
		this.vendorAddressZip = vendorAddressZip;
	}

	public String getVendorAddressCountry() {
		return vendorAddressCountry;
	}

	public void setVendorAddressCountry(String vendorAddressCountry) {
		this.vendorAddressCountry = vendorAddressCountry;
	}

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	public String getVendorTypeName() {
		return vendorTypeName;
	}

	public void setVendorTypeName(String vendorTypeName) {
		this.vendorTypeName = vendorTypeName;
	}

	public String getInvoiceNbr() {
		return invoiceNbr;
	}

	public void setInvoiceNbr(String invoiceNbr) {
		this.invoiceNbr = invoiceNbr;
	}

	public String getInvoiceDesc() {
		return invoiceDesc;
	}

	public void setInvoiceDesc(String invoiceDesc) {
		this.invoiceDesc = invoiceDesc;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getInvoiceFreightAmt() {
		return invoiceFreightAmt;
	}

	public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt) {
		this.invoiceFreightAmt = invoiceFreightAmt;
	}

	public BigDecimal getInvoiceDiscountAmt() {
		return invoiceDiscountAmt;
	}

	public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt) {
		this.invoiceDiscountAmt = invoiceDiscountAmt;
	}

	public BigDecimal getInvoiceTaxAmt() {
		return invoiceTaxAmt;
	}

	public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt) {
		this.invoiceTaxAmt = invoiceTaxAmt;
	}

	public BigDecimal getInvoiceTotalAmt() {
		return invoiceTotalAmt;
	}

	public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt) {
		this.invoiceTotalAmt = invoiceTotalAmt;
	}

	public void setInvoiceLineAmt(BigDecimal invoiceLineAmt) {
		this.invoiceLineAmt = invoiceLineAmt;
	}

	public void setInvoiceLineTax(BigDecimal invoiceLineTax) {
		this.invoiceLineTax = invoiceLineTax;
	}

	public String getInvoiceTaxFlg() {
		return invoiceTaxFlg;
	}

	public void setInvoiceTaxFlg(String invoiceTaxFlg) {
		this.invoiceTaxFlg = invoiceTaxFlg;
	}

	public String getInvoiceLineNbr() {
		return invoiceLineNbr;
	}

	public void setInvoiceLineNbr(String invoiceLineNbr) {
		this.invoiceLineNbr = invoiceLineNbr;
	}

	public String getInvoiceLineName() {
		return invoiceLineName;
	}

	public void setInvoiceLineName(String invoiceLineName) {
		this.invoiceLineName = invoiceLineName;
	}

	public String getInvoiceLineType() {
		return invoiceLineType;
	}

	public void setInvoiceLineType(String invoiceLineType) {
		this.invoiceLineType = invoiceLineType;
	}

	public String getInvoiceLineTypeName() {
		return invoiceLineTypeName;
	}

	public void setInvoiceLineTypeName(String invoiceLineTypeName) {
		this.invoiceLineTypeName = invoiceLineTypeName;
	}

	public BigDecimal getInvoiceLineAmt() {
		return invoiceLineAmt;
	}

	public BigDecimal getInvoiceLineTax() {
		return invoiceLineTax;
	}

	public String getAfeProjectNbr() {
		return afeProjectNbr;
	}

	public void setAfeProjectNbr(String afeProjectNbr) {
		this.afeProjectNbr = afeProjectNbr;
	}

	public String getAfeProjectName() {
		return afeProjectName;
	}

	public void setAfeProjectName(String afeProjectName) {
		this.afeProjectName = afeProjectName;
	}

	public String getAfeCategoryNbr() {
		return afeCategoryNbr;
	}

	public void setAfeCategoryNbr(String afeCategoryNbr) {
		this.afeCategoryNbr = afeCategoryNbr;
	}

	public String getAfeCategoryName() {
		return afeCategoryName;
	}

	public void setAfeCategoryName(String afeCategoryName) {
		this.afeCategoryName = afeCategoryName;
	}

	public String getAfeSubCatNbr() {
		return afeSubCatNbr;
	}

	public void setAfeSubCatNbr(String afeSubCatNbr) {
		this.afeSubCatNbr = afeSubCatNbr;
	}

	public String getAfeSubCatName() {
		return afeSubCatName;
	}

	public void setAfeSubCatName(String afeSubCatName) {
		this.afeSubCatName = afeSubCatName;
	}

	public String getAfeUse() {
		return afeUse;
	}

	public void setAfeUse(String afeUse) {
		this.afeUse = afeUse;
	}

	public String getAfeContractType() {
		return afeContractType;
	}

	public void setAfeContractType(String afeContractType) {
		this.afeContractType = afeContractType;
	}

	public String getAfeContractStructure() {
		return afeContractStructure;
	}

	public void setAfeContractStructure(String afeContractStructure) {
		this.afeContractStructure = afeContractStructure;
	}

	public String getAfePropertyCat() {
		return afePropertyCat;
	}

	public void setAfePropertyCat(String afePropertyCat) {
		this.afePropertyCat = afePropertyCat;
	}

	public String getInventoryNbr() {
		return inventoryNbr;
	}

	public void setInventoryNbr(String inventoryNbr) {
		this.inventoryNbr = inventoryNbr;
	}

	public String getInventoryName() {
		return inventoryName;
	}

	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}

	public String getInventoryClass() {
		return inventoryClass;
	}

	public void setInventoryClass(String inventoryClass) {
		this.inventoryClass = inventoryClass;
	}

	public String getInventoryClassName() {
		return inventoryClassName;
	}

	public void setInventoryClassName(String inventoryClassName) {
		this.inventoryClassName = inventoryClassName;
	}

	public String getPoNbr() {
		return poNbr;
	}

	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}

	public String getPoName() {
		return poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getPoLineNbr() {
		return poLineNbr;
	}

	public void setPoLineNbr(String poLineNbr) {
		this.poLineNbr = poLineNbr;
	}

	public String getPoLineName() {
		return poLineName;
	}

	public void setPoLineName(String poLineName) {
		this.poLineName = poLineName;
	}

	public String getPoLineType() {
		return poLineType;
	}

	public void setPoLineType(String poLineType) {
		this.poLineType = poLineType;
	}

	public String getPoLineTypeName() {
		return poLineTypeName;
	}

	public void setPoLineTypeName(String poLineTypeName) {
		this.poLineTypeName = poLineTypeName;
	}

	public String getShipToLocation() {
		return shipToLocation;
	}

	public void setShipToLocation(String shipToLocation) {
		this.shipToLocation = shipToLocation;
	}

	public String getShipToLocationName() {
		return shipToLocationName;
	}

	public void setShipToLocationName(String shipToLocationName) {
		this.shipToLocationName = shipToLocationName;
	}

	public String getShipToAddressLine1() {
		return shipToAddressLine1;
	}

	public void setShipToAddressLine1(String shipToAddressLine1) {
		this.shipToAddressLine1 = shipToAddressLine1;
	}

	public String getShipToAddressLine2() {
		return shipToAddressLine2;
	}

	public void setShipToAddressLine2(String shipToAddressLine2) {
		this.shipToAddressLine2 = shipToAddressLine2;
	}

	public String getShipToAddressLine3() {
		return shipToAddressLine3;
	}

	public void setShipToAddressLine3(String shipToAddressLine3) {
		this.shipToAddressLine3 = shipToAddressLine3;
	}

	public String getShipToAddressLine4() {
		return shipToAddressLine4;
	}

	public void setShipToAddressLine4(String shipToAddressLine4) {
		this.shipToAddressLine4 = shipToAddressLine4;
	}

	public String getShipToAddressCity() {
		return shipToAddressCity;
	}

	public void setShipToAddressCity(String shipToAddressCity) {
		this.shipToAddressCity = shipToAddressCity;
	}

	public String getShipToAddressCounty() {
		return shipToAddressCounty;
	}

	public void setShipToAddressCounty(String shipToAddressCounty) {
		this.shipToAddressCounty = shipToAddressCounty;
	}

	public String getShipToAddressState() {
		return shipToAddressState;
	}

	public void setShipToAddressState(String shipToAddressState) {
		this.shipToAddressState = shipToAddressState;
	}

	public String getShipToAddressZip() {
		return shipToAddressZip;
	}

	public void setShipToAddressZip(String shipToAddressZip) {
		this.shipToAddressZip = shipToAddressZip;
	}

	public String getShipToAddressCountry() {
		return shipToAddressCountry;
	}

	public void setShipToAddressCountry(String shipToAddressCountry) {
		this.shipToAddressCountry = shipToAddressCountry;
	}

	public String getWoNbr() {
		return woNbr;
	}

	public void setWoNbr(String woNbr) {
		this.woNbr = woNbr;
	}

	public String getWoName() {
		return woName;
	}

	public void setWoName(String woName) {
		this.woName = woName;
	}

	public Date getWoDate() {
		return woDate;
	}

	public void setWoDate(Date woDate) {
		this.woDate = woDate;
	}

	public String getWoType() {
		return woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public String getWoTypeDesc() {
		return woTypeDesc;
	}

	public void setWoTypeDesc(String woTypeDesc) {
		this.woTypeDesc = woTypeDesc;
	}

	public String getWoClass() {
		return woClass;
	}

	public void setWoClass(String woClass) {
		this.woClass = woClass;
	}

	public String getWoClassDesc() {
		return woClassDesc;
	}

	public void setWoClassDesc(String woClassDesc) {
		this.woClassDesc = woClassDesc;
	}

	public String getWoEntity() {
		return woEntity;
	}

	public void setWoEntity(String woEntity) {
		this.woEntity = woEntity;
	}

	public String getWoEntityDesc() {
		return woEntityDesc;
	}

	public void setWoEntityDesc(String woEntityDesc) {
		this.woEntityDesc = woEntityDesc;
	}

	public String getWoLineNbr() {
		return woLineNbr;
	}

	public void setWoLineNbr(String woLineNbr) {
		this.woLineNbr = woLineNbr;
	}

	public String getWoLineName() {
		return woLineName;
	}

	public void setWoLineName(String woLineName) {
		this.woLineName = woLineName;
	}

	public String getWoLineType() {
		return woLineType;
	}

	public void setWoLineType(String woLineType) {
		this.woLineType = woLineType;
	}

	public String getWoLineTypeDesc() {
		return woLineTypeDesc;
	}

	public void setWoLineTypeDesc(String woLineTypeDesc) {
		this.woLineTypeDesc = woLineTypeDesc;
	}

	public String getWoShutDownCd() {
		return woShutDownCd;
	}

	public void setWoShutDownCd(String woShutDownCd) {
		this.woShutDownCd = woShutDownCd;
	}

	public String getWoShutDownCdDesc() {
		return woShutDownCdDesc;
	}

	public void setWoShutDownCdDesc(String woShutDownCdDesc) {
		this.woShutDownCdDesc = woShutDownCdDesc;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherLineNbr() {
		return voucherLineNbr;
	}

	public void setVoucherLineNbr(String voucherLineNbr) {
		this.voucherLineNbr = voucherLineNbr;
	}

	public String getVoucherLineDesc() {
		return voucherLineDesc;
	}

	public void setVoucherLineDesc(String voucherLineDesc) {
		this.voucherLineDesc = voucherLineDesc;
	}

	public String getCheckNbr() {
		return checkNbr;
	}

	public void setCheckNbr(String checkNbr) {
		this.checkNbr = checkNbr;
	}

	public Long getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(Long checkNo) {
		this.checkNo = checkNo;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public BigDecimal getCheckAmt() {
		return checkAmt;
	}

	public void setCheckAmt(BigDecimal checkAmt) {
		this.checkAmt = checkAmt;
	}

	public String getCheckDesc() {
		return checkDesc;
	}

	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}

	public String getUserText01() {
		return userText01;
	}

	public void setUserText01(String userText01) {
		this.userText01 = userText01;
	}

	public String getUserText02() {
		return userText02;
	}

	public void setUserText02(String userText02) {
		this.userText02 = userText02;
	}

	public String getUserText03() {
		return userText03;
	}

	public void setUserText03(String userText03) {
		this.userText03 = userText03;
	}

	public String getUserText04() {
		return userText04;
	}

	public void setUserText04(String userText04) {
		this.userText04 = userText04;
	}

	public String getUserText05() {
		return userText05;
	}

	public void setUserText05(String userText05) {
		this.userText05 = userText05;
	}

	public String getUserText06() {
		return userText06;
	}

	public void setUserText06(String userText06) {
		this.userText06 = userText06;
	}

	public String getUserText07() {
		return userText07;
	}

	public void setUserText07(String userText07) {
		this.userText07 = userText07;
	}

	public String getUserText08() {
		return userText08;
	}

	public void setUserText08(String userText08) {
		this.userText08 = userText08;
	}

	public String getUserText09() {
		return userText09;
	}

	public void setUserText09(String userText09) {
		this.userText09 = userText09;
	}

	public String getUserText10() {
		return userText10;
	}

	public void setUserText10(String userText10) {
		this.userText10 = userText10;
	}

	public String getUserText11() {
		return userText11;
	}

	public void setUserText11(String userText11) {
		this.userText11 = userText11;
	}

	public String getUserText12() {
		return userText12;
	}

	public void setUserText12(String userText12) {
		this.userText12 = userText12;
	}

	public String getUserText13() {
		return userText13;
	}

	public void setUserText13(String userText13) {
		this.userText13 = userText13;
	}

	public String getUserText14() {
		return userText14;
	}

	public void setUserText14(String userText14) {
		this.userText14 = userText14;
	}

	public String getUserText15() {
		return userText15;
	}

	public void setUserText15(String userText15) {
		this.userText15 = userText15;
	}

	public String getUserText16() {
		return userText16;
	}

	public void setUserText16(String userText16) {
		this.userText16 = userText16;
	}

	public String getUserText17() {
		return userText17;
	}

	public void setUserText17(String userText17) {
		this.userText17 = userText17;
	}

	public String getUserText18() {
		return userText18;
	}

	public void setUserText18(String userText18) {
		this.userText18 = userText18;
	}

	public String getUserText19() {
		return userText19;
	}

	public void setUserText19(String userText19) {
		this.userText19 = userText19;
	}

	public String getUserText20() {
		return userText20;
	}

	public void setUserText20(String userText20) {
		this.userText20 = userText20;
	}

	public String getUserText21() {
		return userText21;
	}

	public void setUserText21(String userText21) {
		this.userText21 = userText21;
	}

	public String getUserText22() {
		return userText22;
	}

	public void setUserText22(String userText22) {
		this.userText22 = userText22;
	}

	public String getUserText23() {
		return userText23;
	}

	public void setUserText23(String userText23) {
		this.userText23 = userText23;
	}

	public String getUserText24() {
		return userText24;
	}

	public void setUserText24(String userText24) {
		this.userText24 = userText24;
	}

	public String getUserText25() {
		return userText25;
	}

	public void setUserText25(String userText25) {
		this.userText25 = userText25;
	}

	public String getUserText26() {
		return userText26;
	}

	public void setUserText26(String userText26) {
		this.userText26 = userText26;
	}

	public String getUserText27() {
		return userText27;
	}

	public void setUserText27(String userText27) {
		this.userText27 = userText27;
	}

	public String getUserText28() {
		return userText28;
	}

	public void setUserText28(String userText28) {
		this.userText28 = userText28;
	}

	public String getUserText29() {
		return userText29;
	}

	public void setUserText29(String userText29) {
		this.userText29 = userText29;
	}

	public String getUserText30() {
		return userText30;
	}

	public void setUserText30(String userText30) {
		this.userText30 = userText30;
	}
	
	
	public String getUserText31() {
		return userText31;
	}

	public void setUserText31(String userText31) {
		this.userText31 = userText31;
	}

	public String getUserText32() {
		return userText32;
	}

	public void setUserText32(String userText32) {
		this.userText32 = userText32;
	}

	public String getUserText33() {
		return userText33;
	}

	public void setUserText33(String userText33) {
		this.userText33 = userText33;
	}

	public String getUserText34() {
		return userText34;
	}

	public void setUserText34(String userText34) {
		this.userText34 = userText34;
	}

	public String getUserText35() {
		return userText35;
	}

	public void setUserText35(String userText35) {
		this.userText35 = userText35;
	}

	public String getUserText36() {
		return userText36;
	}

	public void setUserText36(String userText36) {
		this.userText36 = userText36;
	}

	public String getUserText37() {
		return userText37;
	}

	public void setUserText37(String userText37) {
		this.userText37 = userText37;
	}

	public String getUserText38() {
		return userText38;
	}

	public void setUserText38(String userText38) {
		this.userText38 = userText38;
	}

	public String getUserText39() {
		return userText39;
	}

	public void setUserText39(String userText39) {
		this.userText39 = userText39;
	}

	public String getUserText40() {
		return userText40;
	}

	public void setUserText40(String userText40) {
		this.userText40 = userText40;
	}

	public String getUserText41() {
		return userText41;
	}

	public void setUserText41(String userText41) {
		this.userText41 = userText41;
	}

	public String getUserText42() {
		return userText42;
	}

	public void setUserText42(String userText42) {
		this.userText42 = userText42;
	}

	public String getUserText43() {
		return userText43;
	}

	public void setUserText43(String userText43) {
		this.userText43 = userText43;
	}

	public String getUserText44() {
		return userText44;
	}

	public void setUserText44(String userText44) {
		this.userText44 = userText44;
	}

	public String getUserText45() {
		return userText45;
	}

	public void setUserText45(String userText45) {
		this.userText45 = userText45;
	}

	public String getUserText46() {
		return userText46;
	}

	public void setUserText46(String userText46) {
		this.userText46 = userText46;
	}

	public String getUserText47() {
		return userText47;
	}

	public void setUserText47(String userText47) {
		this.userText47 = userText47;
	}

	public String getUserText48() {
		return userText48;
	}

	public void setUserText48(String userText48) {
		this.userText48 = userText48;
	}

	public String getUserText49() {
		return userText49;
	}

	public void setUserText49(String userText49) {
		this.userText49 = userText49;
	}

	public String getUserText50() {
		return userText50;
	}

	public void setUserText50(String userText50) {
		this.userText50 = userText50;
	}

	public String getUserText51() {
		return userText51;
	}

	public void setUserText51(String userText51) {
		this.userText51 = userText51;
	}

	public String getUserText52() {
		return userText52;
	}

	public void setUserText52(String userText52) {
		this.userText52 = userText52;
	}

	public String getUserText53() {
		return userText53;
	}

	public void setUserText53(String userText53) {
		this.userText53 = userText53;
	}

	public String getUserText54() {
		return userText54;
	}

	public void setUserText54(String userText54) {
		this.userText54 = userText54;
	}

	public String getUserText55() {
		return userText55;
	}

	public void setUserText55(String userText55) {
		this.userText55 = userText55;
	}

	public String getUserText56() {
		return userText56;
	}

	public void setUserText56(String userText56) {
		this.userText56 = userText56;
	}

	public String getUserText57() {
		return userText57;
	}

	public void setUserText57(String userText57) {
		this.userText57 = userText57;
	}

	public String getUserText58() {
		return userText58;
	}

	public void setUserText58(String userText58) {
		this.userText58 = userText58;
	}

	public String getUserText59() {
		return userText59;
	}

	public void setUserText59(String userText59) {
		this.userText59 = userText59;
	}

	public String getUserText60() {
		return userText60;
	}

	public void setUserText60(String userText60) {
		this.userText60 = userText60;
	}

	public String getUserText61() {
		return userText61;
	}

	public void setUserText61(String userText61) {
		this.userText61 = userText61;
	}

	public String getUserText62() {
		return userText62;
	}

	public void setUserText62(String userText62) {
		this.userText62 = userText62;
	}

	public String getUserText63() {
		return userText63;
	}

	public void setUserText63(String userText63) {
		this.userText63 = userText63;
	}

	public String getUserText64() {
		return userText64;
	}

	public void setUserText64(String userText64) {
		this.userText64 = userText64;
	}

	public String getUserText65() {
		return userText65;
	}

	public void setUserText65(String userText65) {
		this.userText65 = userText65;
	}

	public String getUserText66() {
		return userText66;
	}

	public void setUserText66(String userText66) {
		this.userText66 = userText66;
	}

	public String getUserText67() {
		return userText67;
	}

	public void setUserText67(String userText67) {
		this.userText67 = userText67;
	}

	public String getUserText68() {
		return userText68;
	}

	public void setUserText68(String userText68) {
		this.userText68 = userText68;
	}

	public String getUserText69() {
		return userText69;
	}

	public void setUserText69(String userText69) {
		this.userText69 = userText69;
	}

	public String getUserText70() {
		return userText70;
	}

	public void setUserText70(String userText70) {
		this.userText70 = userText70;
	}

	public String getUserText71() {
		return userText71;
	}

	public void setUserText71(String userText71) {
		this.userText71 = userText71;
	}

	public String getUserText72() {
		return userText72;
	}

	public void setUserText72(String userText72) {
		this.userText72 = userText72;
	}

	public String getUserText73() {
		return userText73;
	}

	public void setUserText73(String userText73) {
		this.userText73 = userText73;
	}

	public String getUserText74() {
		return userText74;
	}

	public void setUserText74(String userText74) {
		this.userText74 = userText74;
	}

	public String getUserText75() {
		return userText75;
	}

	public void setUserText75(String userText75) {
		this.userText75 = userText75;
	}

	public String getUserText76() {
		return userText76;
	}

	public void setUserText76(String userText76) {
		this.userText76 = userText76;
	}

	public String getUserText77() {
		return userText77;
	}

	public void setUserText77(String userText77) {
		this.userText77 = userText77;
	}

	public String getUserText78() {
		return userText78;
	}

	public void setUserText78(String userText78) {
		this.userText78 = userText78;
	}

	public String getUserText79() {
		return userText79;
	}

	public void setUserText79(String userText79) {
		this.userText79 = userText79;
	}

	public String getUserText80() {
		return userText80;
	}

	public void setUserText80(String userText80) {
		this.userText80 = userText80;
	}

	public String getUserText81() {
		return userText81;
	}

	public void setUserText81(String userText81) {
		this.userText81 = userText81;
	}

	public String getUserText82() {
		return userText82;
	}

	public void setUserText82(String userText82) {
		this.userText82 = userText82;
	}

	public String getUserText83() {
		return userText83;
	}

	public void setUserText83(String userText83) {
		this.userText83 = userText83;
	}

	public String getUserText84() {
		return userText84;
	}

	public void setUserText84(String userText84) {
		this.userText84 = userText84;
	}

	public String getUserText85() {
		return userText85;
	}

	public void setUserText85(String userText85) {
		this.userText85 = userText85;
	}

	public String getUserText86() {
		return userText86;
	}

	public void setUserText86(String userText86) {
		this.userText86 = userText86;
	}

	public String getUserText87() {
		return userText87;
	}

	public void setUserText87(String userText87) {
		this.userText87 = userText87;
	}

	public String getUserText88() {
		return userText88;
	}

	public void setUserText88(String userText88) {
		this.userText88 = userText88;
	}

	public String getUserText89() {
		return userText89;
	}

	public void setUserText89(String userText89) {
		this.userText89 = userText89;
	}

	public String getUserText90() {
		return userText90;
	}

	public void setUserText90(String userText90) {
		this.userText90 = userText90;
	}

	public String getUserText91() {
		return userText91;
	}

	public void setUserText91(String userText91) {
		this.userText91 = userText91;
	}

	public String getUserText92() {
		return userText92;
	}

	public void setUserText92(String userText92) {
		this.userText92 = userText92;
	}

	public String getUserText93() {
		return userText93;
	}

	public void setUserText93(String userText93) {
		this.userText93 = userText93;
	}

	public String getUserText94() {
		return userText94;
	}

	public void setUserText94(String userText94) {
		this.userText94 = userText94;
	}

	public String getUserText95() {
		return userText95;
	}

	public void setUserText95(String userText95) {
		this.userText95 = userText95;
	}

	public String getUserText96() {
		return userText96;
	}

	public void setUserText96(String userText96) {
		this.userText96 = userText96;
	}

	public String getUserText97() {
		return userText97;
	}

	public void setUserText97(String userText97) {
		this.userText97 = userText97;
	}

	public String getUserText98() {
		return userText98;
	}

	public void setUserText98(String userText98) {
		this.userText98 = userText98;
	}

	public String getUserText99() {
		return userText99;
	}

	public void setUserText99(String userText99) {
		this.userText99 = userText99;
	}

	public String getUserText100() {
		return userText100;
	}

	public void setUserText100(String userText100) {
		this.userText100 = userText100;
	}

	public BigDecimal getUserNumber01() {
		return userNumber01;
	}

	public void setUserNumber01(BigDecimal userNumber01) {
		this.userNumber01 = userNumber01;
	}

	public BigDecimal getUserNumber02() {
		return userNumber02;
	}

	public void setUserNumber02(BigDecimal userNumber02) {
		this.userNumber02 = userNumber02;
	}

	public BigDecimal getUserNumber03() {
		return userNumber03;
	}

	public void setUserNumber03(BigDecimal userNumber03) {
		this.userNumber03 = userNumber03;
	}

	public BigDecimal getUserNumber04() {
		return userNumber04;
	}

	public void setUserNumber04(BigDecimal userNumber04) {
		this.userNumber04 = userNumber04;
	}

	public BigDecimal getUserNumber05() {
		return userNumber05;
	}

	public void setUserNumber05(BigDecimal userNumber05) {
		this.userNumber05 = userNumber05;
	}

	public BigDecimal getUserNumber06() {
		return userNumber06;
	}

	public void setUserNumber06(BigDecimal userNumber06) {
		this.userNumber06 = userNumber06;
	}

	public BigDecimal getUserNumber07() {
		return userNumber07;
	}

	public void setUserNumber07(BigDecimal userNumber07) {
		this.userNumber07 = userNumber07;
	}

	public BigDecimal getUserNumber08() {
		return userNumber08;
	}

	public void setUserNumber08(BigDecimal userNumber08) {
		this.userNumber08 = userNumber08;
	}

	public BigDecimal getUserNumber09() {
		return userNumber09;
	}

	public void setUserNumber09(BigDecimal userNumber09) {
		this.userNumber09 = userNumber09;
	}

	public BigDecimal getUserNumber10() {
		return userNumber10;
	}

	public void setUserNumber10(BigDecimal userNumber10) {
		this.userNumber10 = userNumber10;
	}
	public BigDecimal getUserNumber11() {
		return userNumber11;
	}

	public void setUserNumber11(BigDecimal userNumber11) {
		this.userNumber11 = userNumber11;
	}

	public BigDecimal getUserNumber12() {
		return userNumber12;
	}

	public void setUserNumber12(BigDecimal userNumber12) {
		this.userNumber12 = userNumber12;
	}

	public BigDecimal getUserNumber13() {
		return userNumber13;
	}

	public void setUserNumber13(BigDecimal userNumber13) {
		this.userNumber13 = userNumber13;
	}

	public BigDecimal getUserNumber14() {
		return userNumber14;
	}

	public void setUserNumber14(BigDecimal userNumber14) {
		this.userNumber14 = userNumber14;
	}

	public BigDecimal getUserNumber15() {
		return userNumber15;
	}

	public void setUserNumber15(BigDecimal userNumber15) {
		this.userNumber15 = userNumber15;
	}

	public BigDecimal getUserNumber16() {
		return userNumber16;
	}

	public void setUserNumber16(BigDecimal userNumber16) {
		this.userNumber16 = userNumber16;
	}

	public BigDecimal getUserNumber17() {
		return userNumber17;
	}

	public void setUserNumber17(BigDecimal userNumber17) {
		this.userNumber17 = userNumber17;
	}

	public BigDecimal getUserNumber18() {
		return userNumber18;
	}

	public void setUserNumber18(BigDecimal userNumber18) {
		this.userNumber18 = userNumber18;
	}

	public BigDecimal getUserNumber19() {
		return userNumber19;
	}

	public void setUserNumber19(BigDecimal userNumber19) {
		this.userNumber19 = userNumber19;
	}

	public BigDecimal getUserNumber20() {
		return userNumber20;
	}

	public void setUserNumber20(BigDecimal userNumber20) {
		this.userNumber20 = userNumber20;
	}

	public BigDecimal getUserNumber21() {
		return userNumber21;
	}

	public void setUserNumber21(BigDecimal userNumber21) {
		this.userNumber21 = userNumber21;
	}

	public BigDecimal getUserNumber22() {
		return userNumber22;
	}

	public void setUserNumber22(BigDecimal userNumber22) {
		this.userNumber22 = userNumber22;
	}

	public BigDecimal getUserNumber23() {
		return userNumber23;
	}

	public void setUserNumber23(BigDecimal userNumber23) {
		this.userNumber23 = userNumber23;
	}

	public BigDecimal getUserNumber24() {
		return userNumber24;
	}

	public void setUserNumber24(BigDecimal userNumber24) {
		this.userNumber24 = userNumber24;
	}

	public BigDecimal getUserNumber25() {
		return userNumber25;
	}

	public void setUserNumber25(BigDecimal userNumber25) {
		this.userNumber25 = userNumber25;
	}

	public Date getUserDate01() {
		return userDate01;
	}

	public void setUserDate01(Date userDate01) {
		this.userDate01 = userDate01;
	}

	public Date getUserDate02() {
		return userDate02;
	}

	public void setUserDate02(Date userDate02) {
		this.userDate02 = userDate02;
	}

	public Date getUserDate03() {
		return userDate03;
	}

	public void setUserDate03(Date userDate03) {
		this.userDate03 = userDate03;
	}

	public Date getUserDate04() {
		return userDate04;
	}

	public void setUserDate04(Date userDate04) {
		this.userDate04 = userDate04;
	}

	public Date getUserDate05() {
		return userDate05;
	}

	public void setUserDate05(Date userDate05) {
		this.userDate05 = userDate05;
	}

	public Date getUserDate06() {
		return userDate06;
	}

	public void setUserDate06(Date userDate06) {
		this.userDate06 = userDate06;
	}

	public Date getUserDate07() {
		return userDate07;
	}

	public void setUserDate07(Date userDate07) {
		this.userDate07 = userDate07;
	}

	public Date getUserDate08() {
		return userDate08;
	}

	public void setUserDate08(Date userDate08) {
		this.userDate08 = userDate08;
	}

	public Date getUserDate09() {
		return userDate09;
	}

	public void setUserDate09(Date userDate09) {
		this.userDate09 = userDate09;
	}

	public Date getUserDate10() {
		return userDate10;
	}

	public void setUserDate10(Date userDate10) {
		this.userDate10 = userDate10;
	}
	
	public Date getUserDate11() {
		return userDate11;
	}

	public void setUserDate11(Date userDate11) {
		this.userDate11 = userDate11;
	}

	public Date getUserDate12() {
		return userDate12;
	}

	public void setUserDate12(Date userDate12) {
		this.userDate12 = userDate12;
	}

	public Date getUserDate13() {
		return userDate13;
	}

	public void setUserDate13(Date userDate13) {
		this.userDate13 = userDate13;
	}

	public Date getUserDate14() {
		return userDate14;
	}

	public void setUserDate14(Date userDate14) {
		this.userDate14 = userDate14;
	}

	public Date getUserDate15() {
		return userDate15;
	}

	public void setUserDate15(Date userDate15) {
		this.userDate15 = userDate15;
	}

	public Date getUserDate16() {
		return userDate16;
	}

	public void setUserDate16(Date userDate16) {
		this.userDate16 = userDate16;
	}

	public Date getUserDate17() {
		return userDate17;
	}

	public void setUserDate17(Date userDate17) {
		this.userDate17 = userDate17;
	}

	public Date getUserDate18() {
		return userDate18;
	}

	public void setUserDate18(Date userDate18) {
		this.userDate18 = userDate18;
	}

	public Date getUserDate19() {
		return userDate19;
	}

	public void setUserDate19(Date userDate19) {
		this.userDate19 = userDate19;
	}

	public Date getUserDate20() {
		return userDate20;
	}

	public void setUserDate20(Date userDate20) {
		this.userDate20 = userDate20;
	}

	public Date getUserDate21() {
		return userDate21;
	}

	public void setUserDate21(Date userDate21) {
		this.userDate21 = userDate21;
	}

	public Date getUserDate22() {
		return userDate22;
	}

	public void setUserDate22(Date userDate22) {
		this.userDate22 = userDate22;
	}

	public Date getUserDate23() {
		return userDate23;
	}

	public void setUserDate23(Date userDate23) {
		this.userDate23 = userDate23;
	}

	public Date getUserDate24() {
		return userDate24;
	}

	public void setUserDate24(Date userDate24) {
		this.userDate24 = userDate24;
	}

	public Date getUserDate25() {
		return userDate25;
	}

	public void setUserDate25(Date userDate25) {
		this.userDate25 = userDate25;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public BigDecimal getTbCalcTaxAmt() {
		return tbCalcTaxAmt;
	}

	public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt) {
		this.tbCalcTaxAmt = tbCalcTaxAmt;
	}

	public BigDecimal getStateUseAmount() {
		return stateUseAmount;
	}

	public void setStateUseAmount(BigDecimal stateUseAmount) {
		this.stateUseAmount = stateUseAmount;
	}

	public BigDecimal getStateUseTier2Amount() {
		return stateUseTier2Amount;
	}

	public void setStateUseTier2Amount(BigDecimal stateUseTier2Amount) {
		this.stateUseTier2Amount = stateUseTier2Amount;
	}

	public BigDecimal getStateUseTier3Amount() {
		return stateUseTier3Amount;
	}

	public void setStateUseTier3Amount(BigDecimal stateUseTier3Amount) {
		this.stateUseTier3Amount = stateUseTier3Amount;
	}

	public BigDecimal getCountyUseAmount() {
		return countyUseAmount;
	}

	public void setCountyUseAmount(BigDecimal countyUseAmount) {
		this.countyUseAmount = countyUseAmount;
	}

	public BigDecimal getCountyLocalUseAmount() {
		return countyLocalUseAmount;
	}

	public void setCountyLocalUseAmount(BigDecimal countyLocalUseAmount) {
		this.countyLocalUseAmount = countyLocalUseAmount;
	}

	public BigDecimal getCityUseAmount() {
		return cityUseAmount;
	}

	public void setCityUseAmount(BigDecimal cityUseAmount) {
		this.cityUseAmount = cityUseAmount;
	}

	public BigDecimal getCityLocalUseAmount() {
		return cityLocalUseAmount;
	}

	public void setCityLocalUseAmount(BigDecimal cityLocalUseAmount) {
		this.cityLocalUseAmount = cityLocalUseAmount;
	}

	public String getTransactionStateCode() {
		return transactionStateCode;
	}

	public void setTransactionStateCode(String transactionStateCode) {
		this.transactionStateCode = transactionStateCode;
	}

	public String getAutoTransactionStateCode() {
		return autoTransactionStateCode;
	}

	public void setAutoTransactionStateCode(String autoTransactionStateCode) {
		this.autoTransactionStateCode = autoTransactionStateCode;
	}

	public String getTransactionInd() {
		return transactionInd;
	}

	public void setTransactionInd(String transactionInd) {
		this.transactionInd = transactionInd;
	}

	public String getSuspendInd() {
		return suspendInd;
	}

	public void setSuspendInd(String suspendInd) {
		this.suspendInd = suspendInd;
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	//CCH	public String getCchTaxcatCode() {
	//CCH		return cchTaxcatCode;
	//CCH	}

	//CCH	public void setCchTaxcatCode(String cchTaxcatCode) {
	//CCH		this.cchTaxcatCode = cchTaxcatCode;
	//CCH	}

	//CCH	public String getCchGroupCode() {
	//CCH		return cchGroupCode;
	//CCH	}

	//CCH	public void setCchGroupCode(String cchGroupCode) {
	//CCH		this.cchGroupCode = cchGroupCode;
	//CCH	}

	//CCH	public String getCchItemCode() {
	//CCH		return cchItemCode;
	//CCH	}

	//CCH	public void setCchItemCode(String cchItemCode) {
	//CCH		this.cchItemCode = cchItemCode;
	//CCH	}

	public String getManualTaxcodeInd() {
		return manualTaxcodeInd;
	}

	public void setManualTaxcodeInd(String manualTaxcodeInd) {
		this.manualTaxcodeInd = manualTaxcodeInd;
	}

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getLocationMatrixId() {
		return locationMatrixId;
	}

	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public Long getJurisdictionTaxrateId() {
		return jurisdictionTaxrateId;
	}

	public void setJurisdictionTaxrateId(Long jurisdictionTaxrateId) {
		this.jurisdictionTaxrateId = jurisdictionTaxrateId;
	}

	public String getManualJurisdictionInd() {
		return manualJurisdictionInd;
	}

	public void setManualJurisdictionInd(String manualJurisdictionInd) {
		this.manualJurisdictionInd = manualJurisdictionInd;
	}
	
	public String getMeasureTypeCode() {
		return measureTypeCode;
	}

	public void setMeasureTypeCode(String measureTypeCode) {
		this.measureTypeCode = measureTypeCode;
	}

	public BigDecimal getStateUseRate() {
		return stateUseRate;
	}

	public void setStateUseRate(BigDecimal stateUseRate) {
		this.stateUseRate = stateUseRate;
	}

	public BigDecimal getStateUseTier2Rate() {
		return stateUseTier2Rate;
	}

	public void setStateUseTier2Rate(BigDecimal stateUseTier2Rate) {
		this.stateUseTier2Rate = stateUseTier2Rate;
	}

	public BigDecimal getStateUseTier3Rate() {
		return stateUseTier3Rate;
	}

	public void setStateUseTier3Rate(BigDecimal stateUseTier3Rate) {
		this.stateUseTier3Rate = stateUseTier3Rate;
	}

	public BigDecimal getStateSplitAmount() {
		return stateSplitAmount;
	}

	public void setStateSplitAmount(BigDecimal stateSplitAmount) {
		this.stateSplitAmount = stateSplitAmount;
	}

	public BigDecimal getStateTier2MinAmount() {
		return stateTier2MinAmount;
	}

	public void setStateTier2MinAmount(BigDecimal stateTier2MinAmount) {
		this.stateTier2MinAmount = stateTier2MinAmount;
	}

	public BigDecimal getStateTier2MaxAmount() {
		return stateTier2MaxAmount;
	}

	public void setStateTier2MaxAmount(BigDecimal stateTier2MaxAmount) {
		this.stateTier2MaxAmount = stateTier2MaxAmount;
	}

	public BigDecimal getStateMaxtaxAmount() {
		return stateMaxtaxAmount;
	}

	public void setStateMaxtaxAmount(BigDecimal stateMaxtaxAmount) {
		this.stateMaxtaxAmount = stateMaxtaxAmount;
	}

	public BigDecimal getCountyUseRate() {
		return countyUseRate;
	}

	public void setCountyUseRate(BigDecimal countyUseRate) {
		this.countyUseRate = countyUseRate;
	}

	public BigDecimal getCountyLocalUseRate() {
		return countyLocalUseRate;
	}

	public void setCountyLocalUseRate(BigDecimal countyLocalUseRate) {
		this.countyLocalUseRate = countyLocalUseRate;
	}

	public BigDecimal getCountySplitAmount() {
		return countySplitAmount;
	}

	public void setCountySplitAmount(BigDecimal countySplitAmount) {
		this.countySplitAmount = countySplitAmount;
	}

	public BigDecimal getCountyMaxtaxAmount() {
		return countyMaxtaxAmount;
	}

	public void setCountyMaxtaxAmount(BigDecimal countyMaxtaxAmount) {
		this.countyMaxtaxAmount = countyMaxtaxAmount;
	}

	public String getCountySingleFlag() {
		return countySingleFlag;
	}

	public void setCountySingleFlag(String countySingleFlag) {
		this.countySingleFlag = countySingleFlag;
	}

	public String getCountyDefaultFlag() {
		return countyDefaultFlag;
	}

	public void setCountyDefaultFlag(String countyDefaultFlag) {
		this.countyDefaultFlag = countyDefaultFlag;
	}

	public BigDecimal getCityUseRate() {
		return cityUseRate;
	}

	public void setCityUseRate(BigDecimal cityUseRate) {
		this.cityUseRate = cityUseRate;
	}

	public BigDecimal getCityLocalUseRate() {
		return cityLocalUseRate;
	}

	public void setCityLocalUseRate(BigDecimal cityLocalUseRate) {
		this.cityLocalUseRate = cityLocalUseRate;
	}

	public BigDecimal getCitySplitAmount() {
		return citySplitAmount;
	}

	public void setCitySplitAmount(BigDecimal citySplitAmount) {
		this.citySplitAmount = citySplitAmount;
	}

	public BigDecimal getCitySplitUseRate() {
		return citySplitUseRate;
	}

	public void setCitySplitUseRate(BigDecimal citySplitUseRate) {
		this.citySplitUseRate = citySplitUseRate;
	}

	public String getCitySingleFlag() {
		return citySingleFlag;
	}

	public void setCitySingleFlag(String citySingleFlag) {
		this.citySingleFlag = citySingleFlag;
	}

	public String getCityDefaultFlag() {
		return cityDefaultFlag;
	}

	public void setCityDefaultFlag(String cityDefaultFlag) {
		this.cityDefaultFlag = cityDefaultFlag;
	}

	public BigDecimal getCombinedUseRate() {
		return combinedUseRate;
	}

	public void setCombinedUseRate(BigDecimal combinedUseRate) {
		this.combinedUseRate = combinedUseRate;
	}

	public Date getLoadTimestamp() {
		return loadTimestamp;
	}

	public void setLoadTimestamp(Date loadTimestamp) {
		this.loadTimestamp = loadTimestamp;
	}

	public String getGlExtractUpdater() {
		return glExtractUpdater;
	}

	public void setGlExtractUpdater(String glExtractUpdater) {
		this.glExtractUpdater = glExtractUpdater;
	}

	public Date getGlExtractTimestamp() {
		return glExtractTimestamp;
	}

	public void setGlExtractTimestamp(Date glExtractTimestamp) {
		this.glExtractTimestamp = glExtractTimestamp;
	}

	public Long getGlExtractFlag() {
		return glExtractFlag;
	}

	public void setGlExtractFlag(Long glExtractFlag) {
		this.glExtractFlag = glExtractFlag;
	}

	public Long getGlLogFlag() {
		return glLogFlag;
	}

	public void setGlLogFlag(Long glLogFlag) {
		this.glLogFlag = glLogFlag;
	}

	public BigDecimal getGlExtractAmt() {
		return glExtractAmt;
	}

	public void setGlExtractAmt(BigDecimal glExtractAmt) {
		this.glExtractAmt = glExtractAmt;
	}

	public String getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}
	
	public BigDecimal getCountryUseAmount() {
		return countryUseAmount;
	}

	public void setCountryUseAmount(BigDecimal countryUseAmount) {
		this.countryUseAmount= countryUseAmount;
	}

	public String getTransactionCountryCode() {
		return transactionCountryCode;
	}

	public void setTransactionCountryCode(String transactionCountryCode) {
		this.transactionCountryCode= transactionCountryCode;
	}

	public String getAutoTransactionCountryCode() {
		return autoTransactionCountryCode;
	}

	public void setAutoTransactionCountryCode(String autoTransactionCountryCode) {
		this.autoTransactionCountryCode= autoTransactionCountryCode;
	}

	public BigDecimal getCountryUseRate() {
		return countryUseRate;
	}

	public void setCountryUseRate(BigDecimal countryUseRate) {
		this.countryUseRate= countryUseRate;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode= entityCode;
	}

	public String getAuditUserId() {
		return auditUserId;
	}

	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}

	public Date getAuditTimestamp() {
		return auditTimestamp;
	}

	public void setAuditTimestamp(Date auditTimestamp) {
		this.auditTimestamp = auditTimestamp;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public Date getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(Date modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}
	
	public Long getStateTaxcodeDetailId(){
		return this.stateTaxcodeDetailId;
	}
	
	public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId){
		this.stateTaxcodeDetailId = stateTaxcodeDetailId;
	}
	
	public Long getCountyTaxcodeDetailId(){
		return this.countyTaxcodeDetailId;
	}
	
	public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId){
		this.countyTaxcodeDetailId = countyTaxcodeDetailId;
	}

	public Long getCityTaxcodeDetailId(){
		return this.cityTaxcodeDetailId;
	}
	
	public void setCityTaxcodeDetailId(Long  cityTaxcodeDetailId){
		this.cityTaxcodeDetailId = cityTaxcodeDetailId;
	}

	public String getTaxtypeUsedCode(){
		return this.taxtypeUsedCode;
	}
	
	public void setTaxtypeUsedCode(String taxtypeUsedCode){
		this.taxtypeUsedCode = taxtypeUsedCode;
	}

	public BigDecimal getStateTaxableAmt(){
		return this.stateTaxableAmt;
	}
	
	public void setStateTaxableAmt(BigDecimal stateTaxableAmt){
		this.stateTaxableAmt = stateTaxableAmt;
	}

	public BigDecimal getCountyTaxableAmt(){
		return this.countyTaxableAmt;
	}
	
	public void setCountyTaxableAmt(BigDecimal countyTaxableAmt){
		this.countyTaxableAmt = countyTaxableAmt;
	}

	public BigDecimal getCityTaxableAmt(){
		return this.cityTaxableAmt;
	}

	public void setCityTaxableAmt(BigDecimal cityTaxableAmt){
		this.cityTaxableAmt = cityTaxableAmt;
	}

	// Formulas - getters only!
	public Boolean getHasComments() {
		return this.hasComments;
	}

	public BatchMaintenance getBatch() {
		return batch;
	}

	public void setBatch(BatchMaintenance batch) {
		this.batch = batch;
	}

	public AllocationMatrix getAllocationMatrix() {
		return allocationMatrix;
	}

	public void setAllocationMatrix(AllocationMatrix allocationMatrix) {
		this.allocationMatrix = allocationMatrix;
	}

	public TaxCodeState getTaxCodeState() {
		return taxCodeState;
	}

	public void setTaxCodeState(TaxCodeState taxCodeState) {
		this.taxCodeState = taxCodeState;
	}

	public TaxCodeDetail getTaxCodeDetail() {
		return taxCodeDetail;
	}

	public void setTaxCodeDetail(TaxCodeDetail taxCodeDetail) {
		this.taxCodeDetail = taxCodeDetail;
	}

	public TaxMatrix getTaxMatrix() {
		return taxMatrix;
	}

	public void setTaxMatrix(TaxMatrix taxMatrix) {
		this.taxMatrix = taxMatrix;
	}

	public LocationMatrix getLocationMatrix() {
		return locationMatrix;
	}

	public void setLocationMatrix(LocationMatrix locationMatrix) {
		this.locationMatrix = locationMatrix;
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public JurisdictionTaxrate getJurisdictionTaxrate() {
		return jurisdictionTaxrate;
	}

	public void setJurisdictionTaxrate(JurisdictionTaxrate jurisdictionTaxrate) {
		this.jurisdictionTaxrate = jurisdictionTaxrate;
	}
	
	//Bug 3925
	public String getProcessStatus() {
		if ("P".equalsIgnoreCase(getTransactionInd())) {
			
			if(getGlExtractFlag() != null && getGlExtractFlag() !=0 &&
					getGlExtractFlag().toString().equalsIgnoreCase("1")){
				processStatus = "Processed/Closed";
			}else {
				processStatus = "Processed/OPEN";
			}
		}
		else if ("S".equalsIgnoreCase(getTransactionInd())) {
			if("T".equalsIgnoreCase(getSuspendInd())){
				processStatus = "Suspended/No Tax Matrix";
			}else if ("L".equalsIgnoreCase(getSuspendInd())) {
				processStatus = "Suspended/No Location Matrix";
			}else if ("D".equalsIgnoreCase(getSuspendInd())) {
				processStatus = "Suspended/No TaxCode Rule";
			}else if ("R".equalsIgnoreCase(getSuspendInd())) {
				processStatus = "Suspended/No Tax Rate";
			}else if ("?".equalsIgnoreCase(getSuspendInd())) {
				processStatus = "Unknown Suspend Error";
			}else {
				processStatus = "Suspend Ind. Error";
			}
		}
		else if("Q".equalsIgnoreCase(getTransactionInd())){
			processStatus = "Question";
		}
		else if("H".equalsIgnoreCase(getTransactionInd())){
			processStatus = "Held";
			if("FS".equalsIgnoreCase(getMultiTransCode())) {
        		processStatus += "/Future Split";
        	}
		}
		else if("O".equalsIgnoreCase(getTransactionInd())) {
			processStatus = "Original Transaction";
		}
		else if(getTransactionInd() == null) {
			processStatus = "Unprocessed";
		}
		else {
			processStatus = "TRANSACTION Ind. Error";
		}

		return processStatus;

	}
	
	public Long getSplitSubtransId() {
		return splitSubtransId;
	}

	public void setSplitSubtransId(Long splitSubtransId) {
		this.splitSubtransId = splitSubtransId;
	}

	public String getMultiTransCode() {
		return multiTransCode;
	}

	public void setMultiTransCode(String multiTransCode) {
		this.multiTransCode = multiTransCode;
	}
	
	public BigDecimal getStj1Rate() {
		return stj1Rate;
	}

	public void setStj1Rate(BigDecimal stj1Rate) {
		this.stj1Rate = stj1Rate;
	}

	public BigDecimal getStj2Rate() {
		return stj2Rate;
	}

	public void setStj2Rate(BigDecimal stj2Rate) {
		this.stj2Rate = stj2Rate;
	}

	public BigDecimal getStj3Rate() {
		return stj3Rate;
	}

	public void setStj3Rate(BigDecimal stj3Rate) {
		this.stj3Rate = stj3Rate;
	}

	public BigDecimal getStj4Rate() {
		return stj4Rate;
	}

	public void setStj4Rate(BigDecimal stj4Rate) {
		this.stj4Rate = stj4Rate;
	}

	public BigDecimal getStj5Rate() {
		return stj5Rate;
	}

	public void setStj5Rate(BigDecimal stj5Rate) {
		this.stj5Rate = stj5Rate;
	}
	
	public BigDecimal getStj1Amount() {
		return stj1Amount;
	}

	public void setStj1Amount(BigDecimal stj1Amount) {
		this.stj1Amount = stj1Amount;
	}

	public BigDecimal getStj2Amount() {
		return stj2Amount;
	}

	public void setStj2Amount(BigDecimal stj2Amount) {
		this.stj2Amount = stj2Amount;
	}

	public BigDecimal getStj3Amount() {
		return stj3Amount;
	}

	public void setStj3Amount(BigDecimal stj3Amount) {
		this.stj3Amount = stj3Amount;
	}

	public BigDecimal getStj4Amount() {
		return stj4Amount;
	}

	public void setStj4Amount(BigDecimal stj4Amount) {
		this.stj4Amount = stj4Amount;
	}

	public BigDecimal getStj5Amount() {
		return stj5Amount;
	}

	public void setStj5Amount(BigDecimal stj5Amount) {
		this.stj5Amount = stj5Amount;
	}

	/*public void setProcessStatus(String processStatus) {
		this.processStatus = getTransactionInd();
	}*/
	public String toString(){
		return toString(false);
	
	}
	//Bug 4184
	public String toString(boolean removeLinebreaks){
		StringBuilder sb = new StringBuilder();
		sb.append(this.processStatus).append("\t");
		sb.append(this.transactionDetailId).append("\t");
		sb.append(this.sourceTransactionId).append("\t");
		sb.append(this. processBatchNo).append("\t");
		sb.append(this.glExtractBatchNo).append("\t");
		sb.append(this.archiveBatchNo).append("\t");
		sb.append(this.allocationMatrixId).append("\t");
		sb.append(this.allocationSubtransId).append("\t");
		sb.append(this.enteredDate).append("\t");
		sb.append(this.transactionStatus).append("\t");
		sb.append(this.glDate).append("\t");
		sb.append(this.glCompanyNbr).append("\t");
		sb.append(this.glCompanyName).append("\t");
		sb.append(this.glDivisionNbr).append("\t");
		sb.append(this.glDivisionName).append("\t");
		sb.append(this.glCcNbrDeptId).append("\t");
		sb.append(this.glCcNbrDeptName).append("\t");
		sb.append(this.glLocalAcctNbr).append("\t");
		sb.append(this.glLocalAcctName).append("\t");
		sb.append(this.glLocalSubAcctNbr).append("\t");
		sb.append(this.glLocalSubAcctName).append("\t");
		sb.append(this.glFullAcctNbr).append("\t");
		sb.append(this.glFullAcctName).append("\t");
		sb.append(this.glLineItmDistAmt).append("\t");
		sb.append(this.origGlLineItmDistAmt).append("\t");
		sb.append(this.vendorNbr).append("\t");
		sb.append(this.vendorName).append("\t");
		sb.append(this.vendorAddressLine1).append("\t");
		sb.append(this.vendorAddressLine2).append("\t");
		sb.append(this.vendorAddressLine3).append("\t");
		sb.append(this.vendorAddressLine4).append("\t");
		sb.append(this.vendorAddressCity).append("\t");
		sb.append(this.vendorAddressCounty).append("\t");
		sb.append(this.vendorAddressState).append("\t");
		sb.append(this.vendorAddressZip).append("\t");
		sb.append(this.vendorAddressCountry).append("\t");
		sb.append(this.vendorType).append("\t");
		sb.append(this. vendorTypeName).append("\t");
		sb.append(this.invoiceNbr).append("\t");
		sb.append(this.invoiceDesc).append("\t");
		sb.append(this.invoiceDate).append("\t");
		sb.append(this.invoiceFreightAmt).append("\t");
		sb.append(this.invoiceDiscountAmt).append("\t");
		sb.append(this.invoiceTaxAmt).append("\t");
		sb.append(this.invoiceTotalAmt).append("\t");
		sb.append(this.invoiceTaxFlg).append("\t");
		sb.append(this.invoiceLineNbr).append("\t");
		sb.append(this.invoiceLineName).append("\t");
		sb.append(this.invoiceLineType).append("\t");
		sb.append(this.invoiceLineTypeName).append("\t");
		sb.append(this.invoiceLineAmt).append("\t");
		sb.append(this.invoiceLineTax).append("\t");
		sb.append(this.afeProjectNbr).append("\t");
		sb.append(this.afeProjectName).append("\t");
		sb.append(this.afeCategoryNbr).append("\t");
		sb.append(this.afeCategoryName).append("\t");
		sb.append(this.afeSubCatNbr).append("\t");
		sb.append(this.afeSubCatName).append("\t");
		sb.append(this.afeUse).append("\t");
		sb.append(this.afeContractType).append("\t");
		sb.append(this.afeContractStructure).append("\t");
		sb.append(this.afePropertyCat).append("\t");
		sb.append(this.inventoryNbr).append("\t");
		sb.append(this.inventoryName).append("\t");
		sb.append(this.inventoryClass).append("\t");
		sb.append(this.inventoryClassName).append("\t");
		sb.append(this.poNbr).append("\t");
		sb.append(this.poName).append("\t");
		sb.append(this.poDate).append("\t");
		sb.append(this.poLineNbr).append("\t");
		sb.append(this.poLineName).append("\t");
		sb.append(this.poLineType).append("\t");
		sb.append(this.poLineTypeName).append("\t");
		sb.append(this.shipToLocation).append("\t");
		sb.append(this.shipToLocationName).append("\t");
		sb.append(this.shipToAddressLine1).append("\t");
		sb.append(this.shipToAddressLine2).append("\t");
		sb.append(this.shipToAddressLine3).append("\t");
		sb.append(this.shipToAddressLine4).append("\t");
		sb.append(this.shipToAddressCity).append("\t");
		sb.append(this.shipToAddressCounty).append("\t");
		sb.append(this.shipToAddressState).append("\t");
		sb.append(this.shipToAddressZip).append("\t");
		sb.append(this.shipToAddressCountry).append("\t");
		sb.append(this.woNbr).append("\t");
		sb.append(this.woName).append("\t");
		sb.append(this.woDate).append("\t");
		sb.append(this.woType).append("\t");
		sb.append(this.woTypeDesc).append("\t");
		sb.append(this.woClass).append("\t");
		sb.append(this.woClassDesc).append("\t");
		sb.append(this.woEntity).append("\t");
		sb.append(this.woEntityDesc).append("\t");
		sb.append(this.woLineNbr).append("\t");
		sb.append(this.woLineName).append("\t");
		sb.append(this.woLineType).append("\t");
		sb.append(this.woLineTypeDesc).append("\t");
		sb.append(this.woShutDownCd).append("\t");
		sb.append(this.woShutDownCdDesc).append("\t");
		sb.append(this.voucherId).append("\t");
		sb.append(this.voucherName).append("\t");
		sb.append(this.voucherDate).append("\t");
		sb.append(this.voucherLineNbr).append("\t");
		sb.append(this.voucherLineDesc).append("\t");
		sb.append(this.checkNbr).append("\t");
		sb.append(this.checkNo).append("\t");
		sb.append(this.checkDate).append("\t");
		sb.append(this.checkAmt).append("\t");
		sb.append(this. checkDesc).append("\t");
		sb.append(this.userText01).append("\t");
		sb.append(this.userText02).append("\t");
		sb.append(this.userText03).append("\t");
		sb.append(this.userText04).append("\t");
		sb.append(this.userText05).append("\t");
		sb.append(this.userText06).append("\t");
		sb.append(this.userText07).append("\t");
		sb.append(this.userText08).append("\t");
		sb.append(this.userText09).append("\t");
		sb.append(this.userText10).append("\t");
		sb.append(this.userText11).append("\t");
		sb.append(this.userText12).append("\t");
		sb.append(this.userText13).append("\t");
		sb.append(this.userText14).append("\t");
		sb.append(this.userText15).append("\t");
		sb.append(this.userText16).append("\t");
		sb.append(this.userText17).append("\t");
		sb.append(this.userText18).append("\t");
		sb.append(this.userText19).append("\t");
		sb.append(this.userText20).append("\t");
		sb.append(this.userText21).append("\t");
		sb.append(this.userText22).append("\t");
		sb.append(this.userText23).append("\t");
		sb.append(this.userText24).append("\t");
		sb.append(this.userText25).append("\t");
		sb.append(this.userText26).append("\t");
		sb.append(this.userText27).append("\t");
		sb.append(this.userText28).append("\t");
		sb.append(this.userText29).append("\t");
		sb.append(this.userText30).append("\t");
		sb.append(this.userText31).append("\t");
		sb.append(this.userText32).append("\t");
		sb.append(this.userText33).append("\t");
		sb.append(this.userText34).append("\t");
		sb.append(this.userText35).append("\t");
		sb.append(this.userText36).append("\t");
		sb.append(this.userText37).append("\t");
		sb.append(this.userText38).append("\t");
		sb.append(this.userText39).append("\t");
		sb.append(this.userText40).append("\t");
		sb.append(this.userText41).append("\t");
		sb.append(this.userText42).append("\t");
		sb.append(this.userText43).append("\t");
		sb.append(this.userText44).append("\t");
		sb.append(this.userText45).append("\t");
		sb.append(this.userText46).append("\t");
		sb.append(this.userText47).append("\t");
		sb.append(this.userText48).append("\t");
		sb.append(this.userText49).append("\t");
		sb.append(this.userText50).append("\t");
		sb.append(this.userText51).append("\t");
		sb.append(this.userText52).append("\t");
		sb.append(this.userText53).append("\t");
		sb.append(this.userText54).append("\t");
		sb.append(this.userText55).append("\t");
		sb.append(this.userText56).append("\t");
		sb.append(this.userText57).append("\t");
		sb.append(this.userText58).append("\t");
		sb.append(this.userText59).append("\t");
		sb.append(this.userText60).append("\t");
		sb.append(this.userText61).append("\t");
		sb.append(this.userText62).append("\t");
		sb.append(this.userText63).append("\t");
		sb.append(this.userText64).append("\t");
		sb.append(this.userText65).append("\t");
		sb.append(this.userText66).append("\t");
		sb.append(this.userText67).append("\t");
		sb.append(this.userText68).append("\t");
		sb.append(this.userText69).append("\t");
		sb.append(this.userText70).append("\t");
		sb.append(this.userText71).append("\t");
		sb.append(this.userText72).append("\t");
		sb.append(this.userText73).append("\t");
		sb.append(this.userText74).append("\t");
		sb.append(this.userText75).append("\t");
		sb.append(this.userText76).append("\t");
		sb.append(this.userText77).append("\t");
		sb.append(this.userText78).append("\t");
		sb.append(this.userText79).append("\t");
		sb.append(this.userText80).append("\t");
		sb.append(this.userText81).append("\t");
		sb.append(this.userText82).append("\t");
		sb.append(this.userText83).append("\t");
		sb.append(this.userText84).append("\t");
		sb.append(this.userText85).append("\t");
		sb.append(this.userText86).append("\t");
		sb.append(this.userText87).append("\t");
		sb.append(this.userText88).append("\t");
		sb.append(this.userText89).append("\t");
		sb.append(this.userText90).append("\t");
		sb.append(this.userText91).append("\t");
		sb.append(this.userText92).append("\t");
		sb.append(this.userText93).append("\t");
		sb.append(this.userText94).append("\t");
		sb.append(this.userText95).append("\t");
		sb.append(this.userText96).append("\t");
		sb.append(this.userText97).append("\t");
		sb.append(this.userText98).append("\t");
		sb.append(this.userText99).append("\t");
		sb.append(this.userText100).append("\t");
		sb.append(this.userNumber01).append("\t");
		sb.append(this.userNumber02).append("\t");
		sb.append(this.userNumber03).append("\t");
		sb.append(this.userNumber04).append("\t");
		sb.append(this.userNumber05).append("\t");
		sb.append(this.userNumber06).append("\t");
		sb.append(this.userNumber07).append("\t");
		sb.append(this.userNumber08).append("\t");
		sb.append(this.userNumber09).append("\t");
		sb.append(this.userNumber10).append("\t");
		sb.append(this.userNumber11).append("\t");
		sb.append(this.userNumber12).append("\t");
		sb.append(this.userNumber13).append("\t");
		sb.append(this.userNumber14).append("\t");
		sb.append(this.userNumber15).append("\t");
		sb.append(this.userNumber16).append("\t");
		sb.append(this.userNumber17).append("\t");
		sb.append(this.userNumber18).append("\t");
		sb.append(this.userNumber19).append("\t");
		sb.append(this.userNumber20).append("\t");
		sb.append(this.userNumber21).append("\t");
		sb.append(this.userNumber22).append("\t");
		sb.append(this.userNumber23).append("\t");
		sb.append(this.userNumber24).append("\t");
		sb.append(this.userNumber25).append("\t");
		sb.append(this.userDate01).append("\t");
		sb.append(this.userDate02).append("\t");
		sb.append(this.userDate03).append("\t");
		sb.append(this.userDate04).append("\t");
		sb.append(this.userDate05).append("\t");
		sb.append(this.userDate06).append("\t");
		sb.append(this.userDate07).append("\t");
		sb.append(this.userDate08).append("\t");
		sb.append(this.userDate09).append("\t");
		sb.append(this.userDate10).append("\t");
		sb.append(this.userDate11).append("\t");
		sb.append(this.userDate12).append("\t");
		sb.append(this.userDate13).append("\t");
		sb.append(this.userDate14).append("\t");
		sb.append(this.userDate15).append("\t");
		sb.append(this.userDate16).append("\t");
		sb.append(this.userDate17).append("\t");
		sb.append(this.userDate18).append("\t");
		sb.append(this.userDate19).append("\t");
		sb.append(this.userDate20).append("\t");
		sb.append(this.userDate21).append("\t");
		sb.append(this.userDate22).append("\t");
		sb.append(this.userDate23).append("\t");
		sb.append(this.userDate24).append("\t");
		sb.append(this.userDate25).append("\t");
		//fix for 3943 - comments were coming up zero?
		String fixedComments = this.comments;
		if(fixedComments == null || fixedComments.equals("0")){
			fixedComments = "";
		}
		sb.append(fixedComments).append("\t");
		sb.append(this.tbCalcTaxAmt).append("\t");
		sb.append(this.stateUseAmount).append("\t");
		sb.append(this.stateUseTier2Amount).append("\t");
		sb.append(this.stateUseTier3Amount).append("\t");
		sb.append(this.countyUseAmount).append("\t");
		sb.append(this.countyLocalUseAmount).append("\t");
		sb.append(this.cityUseAmount).append("\t");
		sb.append(this.cityLocalUseAmount).append("\t");
		sb.append(this.transactionStateCode).append("\t");
		sb.append(this.autoTransactionStateCode).append("\t");
		sb.append(this.transactionInd).append("\t");
		sb.append(this.suspendInd).append("\t");
//ac		sb.append(this.taxcodeDetailId).append("\t");
//ac		sb.append(this.taxcodeStateCode).append("\t");
//ac		sb.append(this.taxcodeTypeCode).append("\t");
		sb.append(this.taxcodeCode).append("\t");
		//CCH		sb.append(this.cchTaxcatCode).append("\t");
		//CCH		sb.append(this.cchGroupCode).append("\t");
		//CCH		sb.append(this.cchItemCode).append("\t");
		sb.append(this.manualTaxcodeInd).append("\t");
		sb.append(this.taxMatrixId).append("\t");
		sb.append(this.locationMatrixId).append("\t");
		sb.append(this.jurisdictionId).append("\t");
		sb.append(this.jurisdictionTaxrateId).append("\t");
		sb.append(this.manualJurisdictionInd).append("\t");
		sb.append(this.measureTypeCode).append("\t");
		sb.append(this.stateUseRate).append("\t");
		sb.append(this.stateUseTier2Rate).append("\t");
		sb.append(this.stateUseTier3Rate).append("\t");
		sb.append(this.stateSplitAmount).append("\t");
		sb.append(this.stateTier2MinAmount).append("\t");
		sb.append(this.stateTier2MaxAmount).append("\t");
		sb.append(this.stateMaxtaxAmount).append("\t");
		sb.append(this.countyUseRate).append("\t");
		sb.append(this.countyLocalUseRate).append("\t");
		sb.append(this.countySplitAmount).append("\t");
		sb.append(this.countyMaxtaxAmount).append("\t");
		sb.append(this.countySingleFlag).append("\t");
		sb.append(this.countyDefaultFlag).append("\t");
		sb.append(this.cityUseRate).append("\t");
		sb.append(this.cityLocalUseRate).append("\t");
		sb.append(this.citySplitAmount).append("\t");
		sb.append(this.citySplitUseRate).append("\t");
		sb.append(this.citySingleFlag).append("\t");
		sb.append(this.cityDefaultFlag).append("\t");
		sb.append(this.combinedUseRate).append("\t");
		sb.append(this.loadTimestamp).append("\t");
		sb.append(this.glExtractUpdater).append("\t");
		sb.append(this.glExtractTimestamp).append("\t");
		sb.append(this.glExtractFlag).append("\t");
		sb.append(this.glLogFlag).append("\t");
		sb.append(this.glExtractAmt).append("\t");
		sb.append(this.auditFlag).append("\t");
		sb.append(this.rejectFlag).append("\t");
		sb.append(this.auditUserId).append("\t");
		sb.append(this.auditTimestamp).append("\t");
		sb.append(this.modifyUserId).append("\t");
		sb.append(this.modifyTimestamp).append("\t");
		
		sb.append(this.countryUseAmount).append("\t");
		sb.append(this.transactionCountryCode).append("\t");
		sb.append(this.autoTransactionCountryCode).append("\t");
//ac		sb.append(this.taxcodeCountryCode).append("\t");
		sb.append(this.countryUseRate).append("\t");
		sb.append(this.stateUseTier1Setamt).append("\t");
		sb.append(this.stateUseTier2Setamt).append("\t");
		sb.append(this.stateUseTier3Setamt);
		sb.append(this.cpFilingEntityCode).append("\t");
		
		String result = sb.toString();
		if(removeLinebreaks){
			result.replaceAll("\r", "");
			result.replaceAll("\n", "");			
		}
		return result;
	}

	public Long getTaxAllocMatrixId() {
		return taxAllocMatrixId;
	}

	public void setTaxAllocMatrixId(Long taxAllocMatrixId) {
		this.taxAllocMatrixId = taxAllocMatrixId;
	}

	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}

	public String getMultiTransCodeDesc() {
		String desc = "";
		if(this.multiTransCode != null) {
			if("OS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Split";
			}
			else if("S".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Split";
			}
			else if("OA".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Allocation";
			}
			else if("A".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Allocation";
			}
			else if("FS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Future Split";
			}
			else {
				desc = this.multiTransCode;
			}
		}
		
		return desc;
	}

	public String getStateTaxcodeTypeCode() {
		return stateTaxcodeTypeCode;
	}

	public void setStateTaxcodeTypeCode(String stateTaxcodeTypeCode) {
		this.stateTaxcodeTypeCode = stateTaxcodeTypeCode;
	}

	public String getCountyTaxcodeTypeCode() {
		return countyTaxcodeTypeCode;
	}

	public void setCountyTaxcodeTypeCode(String countyTaxcodeTypeCode) {
		this.countyTaxcodeTypeCode = countyTaxcodeTypeCode;
	}

	public String getCityTaxcodeTypeCode() {
		return cityTaxcodeTypeCode;
	}

	public void setCityTaxcodeTypeCode(String cityTaxcodeTypeCode) {
		this.cityTaxcodeTypeCode = cityTaxcodeTypeCode;
	}

	public String getRejectFlag() {
		return rejectFlag;
	}

	public void setRejectFlag(String rejectFlag) {
		this.rejectFlag = rejectFlag;
	}
	
	public Double getStateUseTier1Setamt() {
		return stateUseTier1Setamt;
	}

	public void setStateUseTier1Setamt(Double stateUseTier1Setamt) {
		this.stateUseTier1Setamt = stateUseTier1Setamt;
	}

	public Double getStateUseTier2Setamt() {
		return stateUseTier2Setamt;
	}

	public void setStateUseTier2Setamt(Double stateUseTier2Setamt) {
		this.stateUseTier2Setamt = stateUseTier2Setamt;
	}

	public Double getStateUseTier3Setamt() {
		return stateUseTier3Setamt;
	}

	public void setStateUseTier3Setamt(Double stateUseTier3Setamt) {
		this.stateUseTier3Setamt = stateUseTier3Setamt;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCpFilingEntityCode() {
		return cpFilingEntityCode;
	}

	public void setCpFilingEntityCode(String cpFilingEntityCode) {
		this.cpFilingEntityCode = cpFilingEntityCode;
	}
	
	public static void main(String[] str){
		Set<String> fixedColumns = TransactionDetail.getTransactionDetailProperty(); 
		
		for (String temp:fixedColumns) {
			System.out.println(temp);				
		}	
	}
	
	public static Set<String> getTransactionDetailProperty() {
		Class<?> cls = TransactionDetail.class;
		Set<String> result = new LinkedHashSet<String>();		
		Field [] fields = cls.getDeclaredFields();
		try{
			for (Field f : fields) {
				Column col = f.getAnnotation(Column.class);
				if (col != null) {		
					result.add(Util.columnToProperty(col.name()));
				}
			}
		}
		catch(Exception e){	
		}

		return result;
	}
	
	public static Set<String> getTransactionColumnName() {
		Class<?> cls = TransactionDetail.class;
		Set<String> result = new LinkedHashSet<String>();		
		Field [] fields = cls.getDeclaredFields();
		try{
			for (Field f : fields) {
				Column col = f.getAnnotation(Column.class);
				if (col != null) {		
					result.add(col.name());
				}
			}
		}
		catch(Exception e){	
		}

		return result;
	}
}
