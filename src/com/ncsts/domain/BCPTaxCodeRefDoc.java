package com.ncsts.domain;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TB_BCP_TAXCODE_REFDOC")
public class BCPTaxCodeRefDoc implements java.io.Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="BCP_TAXCODE_REFDOC_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="bcp_taxcode_refdoc_sequence")
	@SequenceGenerator(name="bcp_taxcode_refdoc_sequence" , allocationSize = 1, sequenceName="sq_tb_bcp_taxcode_refdoc_id")	
    private Long bcpTaxcodeRefdocId;
	
	@Column(name="BATCH_ID")
	private Long batchId;
	
	@Column(name="TAXCODE")
	private String taxcode;
	
	@Column(name="TAXCODE_COUNTRY_CODE")
	private String taxcodeCountryCode;
	
	@Column(name="TAXCODE_STATE_CODE")
	private String taxcodeStateCode;
	
	@Column(name="TAXCODE_COUNTY")
	private String taxcodeCounty;
	
	@Column(name="TAXCODE_CITY")
	private String taxcodeCity;
	
	@Column(name="TAXCODE_STJ")
	private String taxcodeStj;
	
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name="CUSTOM_FLAG")
	private String customFlag;
	
	@Column(name= "REFERENCE_DOCUMENT_CODE")
    private String referenceDocumentCode;
	
	@Column(name= "REFERENCE_TYPE_CODE")
	private String referenceTypeCode;
	
	@Column(name= "DESCRIPTION")
	private String description;
	
	@Column(name= "KEYWORDS")
	private String keywords;
	
	@Column(name= "DOCUMENT_NAME")
	private String documentName;
	
	@Column(name= "URL")
	private String url;
	
	@Column(name= "UPDATE_CODE")
	private String updateCode;
	
	@Transient
	private String text;
	
	@Transient
	private Long line;
	
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.NUMERIC);
	}
	
	public BCPTaxCodeRefDoc() {
	}

	public Long getId() {
		return bcpTaxcodeRefdocId;
	}

	public void setId(Long id) {
		this.bcpTaxcodeRefdocId = id;
	}
	
	@Override
	public String getIdPropertyName() {
		return "bcpTaxcodeRefdocId";
	}
	
	public Long getBcpTaxcodeRefdocId() {
		return bcpTaxcodeRefdocId;
	}

	public void setBcpTaxcodeRefdocId(Long bcpTaxcodeRefdocId) {
		this.bcpTaxcodeRefdocId = bcpTaxcodeRefdocId;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public String getTaxcodeCountryCode() {
		return taxcodeCountryCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
		this.taxcodeCountryCode = taxcodeCountryCode;
	}

	public String getTaxcodeStateCode() {
		return taxcodeStateCode;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
		this.taxcodeStateCode = taxcodeStateCode;
	}

	public String getTaxcodeCounty() {
		return taxcodeCounty;
	}

	public void setTaxcodeCounty(String taxcodeCounty) {
		this.taxcodeCounty = taxcodeCounty;
	}

	public String getTaxcodeCity() {
		return taxcodeCity;
	}

	public void setTaxcodeCity(String taxcodeCity) {
		this.taxcodeCity = taxcodeCity;
	}

	public String getTaxcodeStj() {
		return taxcodeStj;
	}

	public void setTaxcodeStj(String taxcodeStj) {
		this.taxcodeStj = taxcodeStj;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getReferenceDocumentCode() {
		return referenceDocumentCode;
	}

	public void setReferenceDocumentCode(String referenceDocumentCode) {
		this.referenceDocumentCode = referenceDocumentCode;
	}

	public String getReferenceTypeCode() {
		return referenceTypeCode;
	}

	public void setReferenceTypeCode(String referenceTypeCode) {
		this.referenceTypeCode = referenceTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUpdateCode() {
		return updateCode;
	}

	public void setUpdateCode(String updateCode) {
		this.updateCode = updateCode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getLine() {
		return line;
	}

	public void setLine(Long line) {
		this.line = line;
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPTaxCodeRefDoc.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_TAXCODE_REFDOC (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPTaxCodeRefDoc.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){					
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}		
	}
}
