package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Vinay Singh & Paul Govindan
 *
 */

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_TAXCODE")
public class TaxCode extends Auditable implements Serializable,Idable<TaxCodePK> {
	
	private static final long serialVersionUID = 8953767585322548149L;
	
	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 TaxCodePK taxCodePK; 
		
	 @Column(name="DESCRIPTION")	
	 private String description;
	    
	 @Column(name="ERP_TAXCODE")    
	 private String erpTaxcode;
	    
	 @Column(name="COMMENTS")    
	 private String comments;
	 
	 @Column(name="ACTIVE_FLAG")
	 private String activeFlag;
	 
	 @Column(name="CUSTOM_FLAG")
	 private String customFlag;
	 
	 @Column(name="MODIFIED_FLAG")
	 private String modifiedFlag;
	    
	 @Transient
	 private String copyRulesFlag; 
		 
		 
	 @Transient
	 private BigDecimal refdoccount;
	 

	 public TaxCode() {
		 this.taxCodePK = new TaxCodePK();
	 }
	
	 public TaxCode(TaxCodePK taxCodePK) {
	    this.taxCodePK=taxCodePK;
	 }
	
	// Idable interface
	public TaxCodePK getId() {
	    return this.taxCodePK;
	}
	
	public void setId(TaxCodePK id) {
		this.taxCodePK = id;
	}
	
	public String getIdPropertyName() {
		return "taxCodePK";
	}
	
	 public TaxCodePK getTaxCodePK() {
		return this.taxCodePK;
	 }
	
	 public void setTaxCodePK(TaxCodePK taxCodePK) {
		this.taxCodePK = taxCodePK;
	 }
	 
	 public String getTaxcodeCode() {
	     return taxCodePK.getTaxcodeCode();
	 }
	 
	 public String getDescription() {
	     return this.description;
	 }
	 
	 public void setDescription(String description) {
	 	this.description = description;
	 }
	 
	 public String getErpTaxcode() {
	     return this.erpTaxcode;
	 }
	 
	 public void setErpTaxcode(String erpTaxcode) {
	 	this.erpTaxcode = erpTaxcode;
	 }
	 
	 public String getComments() {
	     return this.comments;
	 }
	 
	 public void setComments(String comments) {
	 	this.comments = comments;
	 }
	 

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}
	
	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag) || "-1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	public BigDecimal getRefdoccount() {
		return refdoccount;
	}

	public void setRefdoccount(BigDecimal refdoccount) {
		this.refdoccount = refdoccount;
	}
	
	public Boolean getRefdoccountBooleanFlag() {
		return (refdoccount!=null && refdoccount.compareTo(new BigDecimal("0")) > 0);
	}
	
	public String getCopyRulesFlag() {
		return copyRulesFlag;
	}

	public void setCopyRulesFlag(String copyRulesFlag) {
		this.copyRulesFlag = copyRulesFlag;
	}
	
	public Boolean getCopyRulesBooleanFlag() {
		return "1".equals(copyRulesFlag);
	}
	
	public void setCopyRulesBooleanFlag(Boolean copyRulesFlag) {
		this.copyRulesFlag = copyRulesFlag == Boolean.TRUE ? "1" : "0";
	}

}
