package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Embeddable
public class BCPSitusRuleTextPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "BATCH_ID")
	private Long batchId;

	@Column(name = "line")
	private Long line;

	public BCPSitusRuleTextPK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if ((other == null) || !(other instanceof BCPSitusRuleTextPK)) {
			return false;
		}

		final BCPSitusRuleTextPK revision = (BCPSitusRuleTextPK) other;

		return new EqualsBuilder().append(batchId, revision.batchId).append(line, revision.line).isEquals();
}

	/**
	 * 
	 */
	public int hashCode() {
	 return	new HashCodeBuilder().append(batchId).append(line).toHashCode();
	}

	/**
	 * 
	 * @return a string representation of this object.
	 */
	public String toString() {
		return this.batchId + "::" + this.line + "::"; 
	}

	public Long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public Long getLine() {
		return this.line;
	}

	public void setLine(Long line) {
		this.line = line;
	}
}
