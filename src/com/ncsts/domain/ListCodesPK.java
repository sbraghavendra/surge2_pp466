package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
/**
 * 
 * @author Muneer
 *
 */
@Embeddable
public class ListCodesPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_TYPE_CODE")  	
	private String codeTypeCode; 
	   
	@Column(name="CODE_CODE") 	
	private String codeCode;
	
	public ListCodesPK(){
		
	}
	
	public ListCodesPK(String codeTypeCode, String codeCode) {
		this.codeTypeCode = codeTypeCode;
		this.codeCode = codeCode;
	}
	@Column(name="codeTypeCode")
	public String getCodeTypeCode() {
		return codeTypeCode;
	}

	public void setCodeTypeCode(String codeTypeCode) {
		this.codeTypeCode = codeTypeCode;
	}
	@Column(name="codeCode")
	public String getCodeCode() {
		return codeCode;
	}

	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof ListCodesPK)) {
            return false;
        }

        final ListCodesPK revision = (ListCodesPK)other;

        String c1 = getCodeCode();
        String c2 = revision.getCodeCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String t1 = getCodeTypeCode();
        String t2 = revision.getCodeTypeCode();
        if (!((t1 == t2) || (t1 != null && t1.equals(t2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getCodeCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String t1 = getCodeTypeCode();
		hash = 31 * hash + (null == t1 ? 0 : t1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.codeTypeCode + "::" + this.codeCode;
    }

}
