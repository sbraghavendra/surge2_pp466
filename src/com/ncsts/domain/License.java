package com.ncsts.domain;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ncsts.view.util.DesEncrypter;

public class License {
	public static final int LICENSE_TYPE_PURCHASE = 1;
	public static final int LICENSE_TYPE_SALE = 2;
	private static final String SEPARATOR = "~~";
	private static final String DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";
	
	private String key;
	
	public License(String key) {
		super();
		this.key = key;
		decrypt();
	}

	private String company;
	private int licenseType;	
	private Date expiration;

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(int licenseType) {
		this.licenseType = licenseType;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
	
	public boolean hasPurchaseLicense() {
		int mask = 1;
		return (mask & licenseType) != 0; 
	}
	
	public boolean hasSaleLicense() {
		int mask = LICENSE_TYPE_PURCHASE << 1;
		return (mask & licenseType) != 0; 
	}
	
	public boolean isLicenseExpired() {
		if(expiration != null) {
			Date now = new Date();
			return now.getTime() > expiration.getTime();
		}
		
		return false;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	private void decrypt() {
		if(key != null) {
			DesEncrypter desEncrypter = DesEncrypter.getInstance();
			String decrypted = null;
			
			try {
				decrypted = desEncrypter.decrypt(key);
			}
			catch(Exception e){}
			
			if(decrypted != null) {
				String[] fields = decrypted.split(SEPARATOR);
				if(fields != null && fields.length > 1) {
					company = fields[0];
					try {
						licenseType = Integer.parseInt(fields[1]);
					}
					catch(NumberFormatException ne) {}
					if(fields.length > 2) {
						try {
							SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
							if(fields[2].length() > 10) {
								expiration = format.parse(fields[2]);
							}
							else {
								expiration = format.parse(fields[2] + " 23:59:59");
							}
						}
						catch(ParseException pe) {}
					}
				}
			}
		}
	}
	
	public static String encrypt(String companyName, String licenseType, String expirationDate) {
		StringBuilder sb = new StringBuilder();
		sb.append(companyName).append("~~").append(licenseType).append("~~").append(expirationDate);
		
		DesEncrypter enc = DesEncrypter.getInstance();
		String encrypted = null;
		
		try {
			encrypted = enc.encrypt(sb.toString()).replaceAll("\\\r\\\n", "");
		}
		catch(Exception e){}
		
		return encrypted;
	}
	
	public static void main(String[] args) throws Exception {
		if(args == null || args.length == 0) {
			InputStreamReader in = new InputStreamReader(System.in);
			BufferedReader reader = new BufferedReader(in);
			System.out.print("c - Create key or d - Decrypt key : ");
			args = new String[1];
			args[0] = reader.readLine().trim();
		}
		
		if("c".equals(args[0])) {
			String s = null;
			if(args.length > 1) {
				s = args[1];
			}
			else {
				InputStreamReader in = new InputStreamReader(System.in);
				BufferedReader reader = new BufferedReader(in);
				StringBuilder sb = new StringBuilder();
				System.out.print("Enter company name: ");
				sb.append(reader.readLine()).append("~~");
				System.out.print("Enter license type (1 - Purchase, 2 - Sale, 3 - Purchase and Sale): ");
				sb.append(reader.readLine()).append("~~");
				System.out.print("Enter expiration date (mm/dd/yyyy): ");
				sb.append(reader.readLine());
				s = sb.toString();
				in.close();
			}
			DesEncrypter enc = DesEncrypter.getInstance();
			String encrypted = null;
			
			try {
				encrypted = enc.encrypt(s).replaceAll("\\\r\\\n", "");
			}
			catch(Exception e){}
			
			System.out.println("Key: " + encrypted);
		}
		else if("d".equals(args[0])) {
			String s = null;
			if(args.length > 1) {
				s = args[1];
			}
			else {
				InputStreamReader in = new InputStreamReader(System.in);
				BufferedReader reader = new BufferedReader(in);
				System.out.print("Enter key: ");
				s = reader.readLine();
				in.close();
			}
			
			License lic = new License(s);
			
			StringBuilder sb = new StringBuilder();
			sb.append("Key: ").append(lic.getKey()).append("\n");
			sb.append("Company: ").append(lic.getCompany()).append("\n");
			sb.append("LicenseType: ").append(lic.getLicenseType()).append("\n");
			sb.append("Expiration: ").append(lic.getExpiration()).append("\n");
			sb.append("HasPurchaseLicense: ").append(lic.hasPurchaseLicense()).append("\n");
			sb.append("HasSaleLicense: ").append(lic.hasSaleLicense()).append("\n");
			sb.append("IsLicenseExpired: ").append(lic.isLicenseExpired()).append("\n");
			
			System.out.println(sb);
		}
		else {
			System.out.println("Invalid option");
		}
	}
	
	public boolean isValidLicense() {
		return !this.isLicenseExpired() && (this.hasPurchaseLicense() || this.hasSaleLicense());
	}
}
