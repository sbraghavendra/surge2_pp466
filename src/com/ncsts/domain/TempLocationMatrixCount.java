package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TMP_LOC_MTRX_DRIVERS_COUNT")
public class TempLocationMatrixCount {
	
	@Id
	@Column(name="DRIVER_01")
    private Long driver01;
    
    @Column(name="DRIVER_02" )
    private Long driver02;
    
    @Column(name="DRIVER_03" )
    private Long driver03;
    
    @Column(name="DRIVER_04" )
    private Long driver04;
    
    @Column(name="DRIVER_05" )
    private Long driver05;
  
    @Column(name="DRIVER_06" )
    private Long driver06;
    
    @Column(name="DRIVER_07" )
    private Long driver07;
    
    @Column(name="DRIVER_08" )
    private Long driver08;
    
    @Column(name="DRIVER_09" )
    private Long driver09;
    
    @Column(name="DRIVER_10" )
    private Long driver10;

	public Long getDriver01() {
		return driver01;
	}

	public Long getDriver02() {
		return driver02;
	}

	public Long getDriver03() {
		return driver03;
	}

	public Long getDriver04() {
		return driver04;
	}

	public Long getDriver05() {
		return driver05;
	}

	public Long getDriver06() {
		return driver06;
	}

	public Long getDriver07() {
		return driver07;
	}

	public Long getDriver08() {
		return driver08;
	}

	public Long getDriver09() {
		return driver09;
	}

	public Long getDriver10() {
		return driver10;
	}

	public void setDriver01(Long driver01) {
		this.driver01 = driver01;
	}

	public void setDriver02(Long driver02) {
		this.driver02 = driver02;
	}

	public void setDriver03(Long driver03) {
		this.driver03 = driver03;
	}

	public void setDriver04(Long driver04) {
		this.driver04 = driver04;
	}

	public void setDriver05(Long driver05) {
		this.driver05 = driver05;
	}

	public void setDriver06(Long driver06) {
		this.driver06 = driver06;
	}

	public void setDriver07(Long driver07) {
		this.driver07 = driver07;
	}

	public void setDriver08(Long driver08) {
		this.driver08 = driver08;
	}

	public void setDriver09(Long driver09) {
		this.driver09 = driver09;
	}

	public void setDriver10(Long driver10) {
		this.driver10 = driver10;
	}
}
