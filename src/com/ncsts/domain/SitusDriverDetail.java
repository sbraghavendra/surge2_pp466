package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.beans.BeanUtils;
import com.ncsts.dto.SitusDriverDetailDTO;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="TB_SITUS_DRIVER_DETAIL")
public class SitusDriverDetail extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
	@Id
    @Column(name="SITUS_DRIVER_DETAIL_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="situs_detail_sequence")
	@SequenceGenerator(name="situs_detail_sequence" , allocationSize = 1, sequenceName="SQ_TB_SITUS_DRIVER_DETAIL_ID")
	private Long situsDriverDetailId;
	
    @Column(name="SITUS_DRIVER_ID")	
    private Long situsDriverId;
    
    @Column(name="LOCATION_TYPE_CODE")    
    private String locationTypeCode;  
    
    @Column(name="DRIVER_ID")	
    private Long driverId;
    
    @Column(name="MANDATORY_FLAG")    
    private String mandatoryFlag; 
    
    @Column(name="CUSTOM_MANDATORY_FLAG")    
    private String customMandatoryFlag; 
    
	@Transient	
	SitusDriverDetailDTO situsDriverDetailDTO;
	
    public SitusDriverDetail() { 	
    }
    
	public SitusDriverDetailDTO getSitusDriverDetailDTO() {
		SitusDriverDetailDTO situsDriverDetailDTO = new SitusDriverDetailDTO();
		BeanUtils.copyProperties(this, situsDriverDetailDTO);
		return situsDriverDetailDTO;
	}
    
    public Long getSitusDriverDetailId() {
        return this.situsDriverDetailId;
    }
    
    public void setSitusDriverDetailId(Long situsDriverDetailId) {
    	this.situsDriverDetailId = situsDriverDetailId;
    }
    
    public Long getSitusDriverId() {
        return this.situsDriverId;
    }
    
    public void setSitusDriverId(Long situsDriverId) {
    	this.situsDriverId = situsDriverId;
    }
    
    public String getLocationTypeCode() {
        return this.locationTypeCode;
    }
    
    public void setLocationTypeCode(String locationTypeCode) {
    	this.locationTypeCode = locationTypeCode;
    } 
    
    public Long getDriverId() {
        return this.driverId;
    }
    
    public void setDriverId(Long driverId) {
    	this.driverId = driverId;
    }

    public String getMandatoryFlag() {
        return this.mandatoryFlag;
    }
    
    public void setMandatoryFlag(String mandatoryFlag) {
    	this.mandatoryFlag = mandatoryFlag;
    }  
    
    public Boolean getMandatoryBooleanFlag() {
    	return "1".equals(this.mandatoryFlag);
	}

	public void setMandatoryBooleanFlag(Boolean mandatoryFlag) {
		this.mandatoryFlag = mandatoryFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getCustomMandatoryFlag() {
        return this.customMandatoryFlag;
    }
    
    public void setCustomMandatoryFlag(String customMandatoryFlag) {
    	this.customMandatoryFlag = customMandatoryFlag;
    }  
    
    public Boolean getCustomMandatoryBooleanFlag() {
    	return "1".equals(this.customMandatoryFlag);
	}

	public void setCustomMandatoryBooleanFlag(Boolean customMandatoryFlag) {
		this.customMandatoryFlag = customMandatoryFlag == Boolean.TRUE ? "1" : "0";
	}
    
    /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof SitusDriverDetail)) {
            return false;
        }

        final SitusDriverDetail revision = (SitusDriverDetail)other;

        Long c1 = getSitusDriverDetailId();
        Long c2 = revision.getSitusDriverDetailId();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		Long c1 = getSitusDriverDetailId();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());  

		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.situsDriverDetailId;
    }

	@Override
	public Long getId() {
		return this.situsDriverDetailId;
	}

	@Override
	public void setId(Long id) {
		this.situsDriverDetailId = id;
	}
	
	@Override
	public String getIdPropertyName() {
		return "situsDriverDetailId";
	}
}