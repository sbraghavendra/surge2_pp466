package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityLevelDTO;

/**
 * @author Paul Govindan
 *
 */

@Entity
@Table(name="TB_ENTITY_LEVEL")
public class EntityLevel extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="ENTITY_LEVEL_ID")	
    private Long entityLevelId;
    
    @Column(name="TRANS_DTL_COLUMN_NAME")    
    private String transDetailColumnName;   
    
    @Column(name="DESCRIPTION")    
    private String description;
    
    //Midtier project code added - january 2009
    @OneToMany(mappedBy="entityLevelObj")
    private Set<EntityItem> entityItem = new HashSet<EntityItem>();
    

    
	@Transient	
	EntityLevelDTO entityLevelDTO;
	
	public EntityLevel() { 
    	
    }
	
	public EntityLevelDTO getEntityLevelDTO() {
		EntityLevelDTO entityLevelDTO = new EntityLevelDTO();
		BeanUtils.copyProperties(this, entityLevelDTO);
		return entityLevelDTO;
	}   
    
    public String getTransDetailColumnName() {
        return this.transDetailColumnName;
    }
    
    public void setTransDetailColumnName(String entityName) {
    	this.transDetailColumnName = entityName;
    }      
    
    public Long getEntityLevelId() {
        return this.entityLevelId;
    }
    
    public void setEntityLevelId(Long entityLevelId) {
    	this.entityLevelId = entityLevelId;
    }    
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }

	public Set<EntityItem> getEntityItem() {
		return entityItem;
	}

	public void setEntityItem(Set<EntityItem> entityItem) {
		this.entityItem = entityItem;
	}         
    
}






