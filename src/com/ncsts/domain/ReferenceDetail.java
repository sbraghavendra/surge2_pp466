package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_REFERENCE_DETAIL")
public class ReferenceDetail extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 private ReferenceDetailPK referenceDetailPK;
	 
	//Midtier project code added - january 2009
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="TAXCODE_DETAIL_ID", insertable=false, updatable=false)
	 private TaxCodeDetail taxCodeDetail;
	 
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="REFERENCE_DOCUMENT_CODE", insertable=false, updatable=false)
	 private ReferenceDocument referenceDocument;
	 
	 public ReferenceDetail() {
	
	 }
	 
	 public ReferenceDetail(ReferenceDetailPK referenceDetailPK) {
		this.referenceDetailPK = referenceDetailPK;
	 }
	 
	 public ReferenceDetailPK getReferenceDetailPK() {
		return referenceDetailPK;
	 }

	 public void setReferenceDetailPK(ReferenceDetailPK referenceDetailPK) {
		this.referenceDetailPK = referenceDetailPK;
	 }

	public TaxCodeDetail getTaxCodeDetail() {
		return taxCodeDetail;
	}

	public void setTaxCodeDetail(TaxCodeDetail taxCodeDetail) {
		this.taxCodeDetail = taxCodeDetail;
	}

	public ReferenceDocument getReferenceDocument() {
		return referenceDocument;
	}

	public void setReferenceDocument(ReferenceDocument referenceDocument) {
		this.referenceDocument = referenceDocument;
	}



}
