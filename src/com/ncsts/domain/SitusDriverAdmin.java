package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.beans.BeanUtils;
import com.ncsts.dto.SitusDriverDTO;

@Entity
@Table(name="TB_SITUS_DRIVER")
public class SitusDriverAdmin extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="SITUS_DRIVER_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="situs_sequence_admin")
	@SequenceGenerator(name="situs_sequence_admin" , allocationSize = 1, sequenceName="sq_tb_situs_driver_sys")
	private Long situsDriverId;
    
    @Column(name="TRANSACTION_TYPE_CODE")    
    private String transactionTypeCode;  
    
    @Column(name="RATETYPE_CODE")    
    private String ratetypeCode;  

    @Column(name="METHOD_DELIVERY_CODE")    
    private String methodDeliveryCode;  
    
    @Column(name="CUSTOM_FLAG")    
    private String customFlag; 
    
	@Transient	
	SitusDriverDTO situsDriverDTO;
	
    public SitusDriverAdmin() { 	
    }
    
	public SitusDriverDTO getSitusDriverDTO() {
		SitusDriverDTO situsDriverDTO = new SitusDriverDTO();
		BeanUtils.copyProperties(this, situsDriverDTO);
		return situsDriverDTO;
	}
    
    public Long getSitusDriverId() {
        return this.situsDriverId;
    }
    
    public void setSitusDriverId(Long situsDriverId) {
    	this.situsDriverId = situsDriverId;
    }
    
    public String getTransactionTypeCode() {
        return this.transactionTypeCode;
    }
    
    public void setTransactionTypeCode(String transactionTypeCode) {
    	this.transactionTypeCode = transactionTypeCode;
    }    
    
    public String getRatetypeCode() {
        return this.ratetypeCode;
    }
    
    public void setRatetypeCode(String ratetypeCode) {
    	this.ratetypeCode = ratetypeCode;
    }      
    
    public String getMethodDeliveryCode() {
        return this.methodDeliveryCode;
    }
    
    public void setMethodDeliveryCode(String methodDeliveryCode) {
    	this.methodDeliveryCode = methodDeliveryCode;
    }   
    
    public String getCustomFlag() {
        return this.customFlag;
    }
    
    public void setCustomFlag(String customFlag) {
    	this.customFlag = customFlag;
    }  
    
    public Boolean getCustomBooleanFlag() {
    	return "1".equals(this.customFlag);
	}

	public void setCustomBooleanFlag(Boolean customFlag) {
		this.customFlag = customFlag == Boolean.TRUE ? "1" : "0";
	}
    
    /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof SitusDriverAdmin)) {
            return false;
        }

        final SitusDriverAdmin revision = (SitusDriverAdmin)other;

        Long c1 = getSitusDriverId();
        Long c2 = revision.getSitusDriverId();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		Long c1 = getSitusDriverId();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());  

		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.situsDriverId;
    }

	@Override
	public Long getId() {
		return this.situsDriverId;
	}

	@Override
	public void setId(Long id) {
		this.situsDriverId = id;
	}
	
	@Override
	public String getIdPropertyName() {
		return "situsDriverId";
	}
}