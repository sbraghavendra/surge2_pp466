package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DriverNamesPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="DRIVER_NAMES_CODE")  	
	private String drvNamesCode; 
	   
	@Column(name="DRIVER_ID") 	
	private Long driverId;
	
	public DriverNamesPK(){
		
	}
	
	public DriverNamesPK(String drvNamesCode, Long driverId) {
		this.drvNamesCode = drvNamesCode;
		this.driverId = driverId;
	}
	
	@Column(name="drvNamesCode")
	public String getDrvNamesCode() {
		return drvNamesCode;
	}

	public void setDrvNamesCode(String drvNamesCode) {
		this.drvNamesCode = drvNamesCode;
	}
	
	@Column(name="driverId")
	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DriverReferencePK)) {
            return false;
        }

        final DriverNamesPK revision = (DriverNamesPK)other;

        String dnc1 = getDrvNamesCode();
        String dnc2 = revision.getDrvNamesCode();
        if (!((dnc1 == dnc2) || (dnc1 != null && dnc1.equals(dnc2)))) {
            return false;
        }

        Long d1 = getDriverId();
        Long d2 = revision.getDriverId();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String dnc1 = getDrvNamesCode();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
        Long d1 = getDriverId();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.drvNamesCode + "::" + this.driverId;
    }
	
}
