package com.ncsts.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.common.Util;
import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@MappedSuperclass
public abstract class Matrix extends Auditable implements Idable<Long> {
	public abstract Long getDriverCount();
	public abstract String getDriverCode();
	public abstract boolean getHasDescriptions();
	public abstract Date getEffectiveDate();
	public abstract void setEffectiveDate(Date effectiveDate);
	public abstract Date getExpirationDate();
	public abstract void setExpirationDate(Date expirationDate);
	
	public void initializeDefaults() {
		// Set default expiration date
		if (getExpirationDate() == null) {
			try {
				Date expDate = (new SimpleDateFormat("MM/dd/yyyy")).parse("12/31/9999");
				setExpirationDate(expDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// Set default effective date
		if (getEffectiveDate() == null) {
			setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		}
	}
	
	public void adjustDriverFields() {
		for (Long i = 1L; i <= getDriverCount(); i++) {
			String value = getDriver(i);
			if ((value == null) || value.trim().equals("")) {
				setDriver(i, null);
			}
		}
	}
	
	public void initNullDriverFields() {
		for (Long i = 1L; i <= getDriverCount(); i++) {
			String value = getDriver(i);
			if ((value == null) || value.trim().equals("")) {
				setDriver(i, "*ALL");
			}
		}
	}
	
	public void clearDriverDesc() {
		for (Long i = 1L; i <= getDriverCount(); i++) {
			setDriverDesc(i, null);
		}
	}
	
	public void setDriver(Long id, String value) {
		// "clean" value
		String driver = ((value == null) || value.trim().equals(""))? null:value.trim();
		Util.setProperty(this, String.format("driver%02d", id), driver, String.class);
	}
	
	public String getDriver(Long id) {
		return (String) Util.getProperty(this, String.format("driver%02d", id));
	}

	public void setDriverDesc(Long id, String value) {
		if (getHasDescriptions()) {
			// "clean" value
			String desc = ((value == null) || value.trim().equals(""))? null:value.trim();
			Util.setProperty(this, String.format("driver%02dDesc", id), desc, String.class);
		}
	} 
	
	public String getDriverDesc(Long id) {
		if (getHasDescriptions()) {
			return (String) Util.getProperty(this, String.format("driver%02dDesc", id));
		} 
		
		return null;
	}
}
