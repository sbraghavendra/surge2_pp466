package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TB_REGISTRATION_DETAIL")
public class RegistrationDetail extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="REGISTRATION_DETAIL_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="registration_detail_sequence")
	@SequenceGenerator(name="registration_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_registration_detail_id")
	private Long registrationDetailId;
	
	@Column(name="REGISTRATION_ID")
	private Long registrationId;
	
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;
	
	@Column(name="REG_TYPE_CODE")
	private String regTypeCode;
	
	@Column(name="REG_NO")
	private String regNo;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	public Long getRegistrationDetailId() {
		return registrationDetailId;
	}

	public void setRegistrationDetailId(Long registrationDetailId) {
		this.registrationDetailId = registrationDetailId;
	}

	public Long getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(Long registrationId) {
		this.registrationId = registrationId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getRegTypeCode() {
		return regTypeCode;
	}

	public void setRegTypeCode(String regTypeCode) {
		this.regTypeCode = regTypeCode;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return this.registrationDetailId;
	}

	@Override
	public void setId(Long id) {
		this.registrationDetailId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "registrationDetailId";
	}
}
