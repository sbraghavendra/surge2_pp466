package com.ncsts.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="TB_TAX_MATRIX")
public  class GSBMatrix extends Matrix implements java.io.Serializable {

    private static final long serialVersionUID = 8495211375429088594L;
	
	public static final long DRIVER_COUNT = 30L;
	
	public static final String DRIVER_CODE = "GSB";

    @Id
    @Column(name="TAX_MATRIX_ID")
    //Code modified as part of Midtier project in Dec 2008
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="tax_matrix_sequence")
	@SequenceGenerator(name="tax_matrix_sequence" , allocationSize = 1, sequenceName="sq_tb_tax_matrix_id")
	private Long taxMatrixId;
    
    @Column(name="DRIVER_GLOBAL_FLAG")
    private String driverGlobalFlag;
    
    @Column(name="DRIVER_01")
    private String driver01;
    
    @Column(name="DRIVER_01_DESC")
    private String driver01Desc;
    
    @Column(name="DRIVER_01_THRU")
    private String driver01Thru;
    
    @Column(name="DRIVER_01_THRU_DESC" )
    private String driver01ThruDesc;
    
    @Column(name="DRIVER_02" )
    private String driver02;
    
    @Column(name="DRIVER_02_DESC" )
    private String driver02Desc;
    
    @Column(name="DRIVER_02_THRU" )
    private String driver02Thru;
    
    @Column(name="DRIVER_02_THRU_DESC" )
    private String driver02ThruDesc;
    
    @Column(name="DRIVER_03" )
    private String driver03;
    
    @Column(name="DRIVER_03_DESC" )
    private String driver03Desc;
    
    @Column(name="DRIVER_03_THRU" )
    private String driver03Thru;
    
    @Column(name="DRIVER_03_THRU_DESC" )
    private String driver03ThruDesc;
    
    @Column(name="DRIVER_04" )
    private String driver04;
    
    @Column(name="DRIVER_04_DESC" )
    private String driver04Desc;

    @Column(name="DRIVER_04_THRU" )
    private String driver04Thru;
    
    @Column(name="DRIVER_04_THRU_DESC" )
    private String driver04ThruDesc;
    
    @Column(name="DRIVER_05" )
    private String driver05;
    
    @Column(name="DRIVER_05_DESC" )
    private String driver05Desc;
    
    @Column(name="DRIVER_05_THRU" )
    private String driver05Thru;
    
    @Column(name="DRIVER_05_THRU_DESC" )
    private String driver05ThruDesc;
    
    @Column(name="DRIVER_06" )
    private String driver06;
    
    @Column(name="DRIVER_06_DESC" )
    private String driver06Desc;
    
    @Column(name="DRIVER_06_THRU" )
    private String driver06Thru;
    
    @Column(name="DRIVER_06_THRU_DESC" )
    private String driver06ThruDesc;
    
    @Column(name="DRIVER_07" )
    private String driver07;
    
    @Column(name="DRIVER_07_DESC" )
    private String driver07Desc;
    
    @Column(name="DRIVER_07_THRU" )
    private String driver07Thru;
    
    @Column(name="DRIVER_07_THRU_DESC" )
    private String driver07ThruDesc;
    
    @Column(name="DRIVER_08" )
    private String driver08;
    
    @Column(name="DRIVER_08_DESC" )
    private String driver08Desc;
    
    @Column(name="DRIVER_08_THRU" )
    private String driver08Thru;
    
    @Column(name="DRIVER_08_THRU_DESC" )
    private String driver08ThruDesc;
    
    @Column(name="DRIVER_09" )
    private String driver09;
    
    @Column(name="DRIVER_09_DESC" )
    private String driver09Desc;
    
    @Column(name="DRIVER_09_THRU" )
    private String driver09Thru;
    
    @Column(name="DRIVER_09_THRU_DESC" )
    private String driver09ThruDesc;
    
    @Column(name="DRIVER_10" )
    private String driver10;
    
    @Column(name="DRIVER_10_DESC" )
    private String driver10Desc;
    
    @Column(name="DRIVER_10_THRU" )
    private String driver10Thru;
    
    @Column(name="DRIVER_10_THRU_DESC" )
    private String driver10ThruDesc;
    
    @Column(name="DRIVER_11" )
    private String driver11;
    
    @Column(name="DRIVER_11_DESC" )
    private String driver11Desc;
    
    @Column(name="DRIVER_11_THRU" )
    private String driver11Thru;
    
    @Column(name="DRIVER_11_THRU_DESC" )
    private String driver11ThruDesc;
    
    @Column(name="DRIVER_12" )
    private String driver12;
    
    @Column(name="DRIVER_12_DESC" )
    private String driver12Desc;
    
    @Column(name="DRIVER_12_THRU" )
    private String driver12Thru;
    
    @Column(name="DRIVER_12_THRU_DESC" )
    private String driver12ThruDesc;
    
    @Column(name="DRIVER_13" )
    private String driver13;
    
    @Column(name="DRIVER_13_DESC" )
    private String driver13Desc;
    
    @Column(name="DRIVER_13_THRU" )
    private String driver13Thru;
    
    @Column(name="DRIVER_13_THRU_DESC" )
    private String driver13ThruDesc;
    
    @Column(name="DRIVER_14" )
    private String driver14;
    
    @Column(name="DRIVER_14_DESC" )
    private String driver14Desc;
    
    @Column(name="DRIVER_14_THRU" )
    private String driver14Thru;
    
    @Column(name="DRIVER_14_THRU_DESC" )
    private String driver14ThruDesc;
    
    @Column(name="DRIVER_15" )
    private String driver15;
    
    @Column(name="DRIVER_15_DESC" )
    private String driver15Desc;
    
    @Column(name="DRIVER_15_THRU" )
    private String driver15Thru;
    
    @Column(name="DRIVER_15_THRU_DESC" )
    private String driver15ThruDesc;
    
    @Column(name="DRIVER_16" )
    private String driver16;
        
    @Column(name="DRIVER_16_DESC" )
     private String driver16Desc;
       
    @Column(name="DRIVER_16_THRU" )
    private String driver16Thru;
        
    @Column(name="DRIVER_16_THRU_DESC" )
    private String driver16ThruDesc;
    
    @Column(name="DRIVER_17" )
	private String driver17;
            
	@Column(name="DRIVER_17_DESC" )
	private String driver17Desc;
            
	@Column(name="DRIVER_17_THRU" )
	private String driver17Thru;
            
	@Column(name="DRIVER_17_THRU_DESC")
	private String driver17ThruDesc;
   
	@Column(name="DRIVER_18")
	private String driver18;
           
	@Column(name="DRIVER_18_DESC")
	private String driver18Desc;
                
	@Column(name="DRIVER_18_THRU")
	private String driver18Thru;
                
	@Column(name="DRIVER_18_THRU_DESC")
	private String driver18ThruDesc;
	
	@Column(name="DRIVER_19")
	private String driver19;
	
	@Column(name="DRIVER_19_DESC")
	private String driver19Desc;
	
	@Column(name="DRIVER_19_THRU")
	private String driver19Thru;
	
	@Column(name="DRIVER_19_THRU_DESC")
	private String driver19ThruDesc;
	
	@Column(name="DRIVER_20")
	private String driver20;
	
	@Column(name="DRIVER_20_DESC")
	private String driver20Desc;
	
	@Column(name="DRIVER_20_THRU")
	private String driver20Thru;
	
	@Column(name="DRIVER_20_THRU_DESC")
	private String driver20ThruDesc;
	
	@Column(name="DRIVER_21")
	private String driver21;
	
	@Column(name="DRIVER_21_DESC")
	private String driver21Desc;
	
	@Column(name="DRIVER_21_THRU")
	private String driver21Thru;
	
	@Column(name="DRIVER_21_THRU_DESC")
	private String driver21ThruDesc;
   
	@Column(name="DRIVER_22")
	private String driver22;
	
	@Column(name="DRIVER_22_DESC")
	private String driver22Desc;
	
	@Column(name="DRIVER_22_THRU")
	private String driver22Thru;
	
	@Column(name="DRIVER_22_THRU_DESC")
	private String driver22ThruDesc;
	
	@Column(name="DRIVER_23")
	private String driver23;
	
	@Column(name="DRIVER_23_DESC")
	private String driver23Desc;
	
	@Column(name="DRIVER_23_THRU")
	private String driver23Thru;
	
	@Column(name="DRIVER_23_THRU_DESC")
	private String driver23ThruDesc;
	
	@Column(name="DRIVER_24")
	private String driver24;
	
	@Column(name="DRIVER_24_DESC")
	private String driver24Desc;
	
	@Column(name="DRIVER_24_THRU")
	private String driver24Thru;
	
	@Column(name="DRIVER_24_THRU_DESC")
	private String driver24ThruDesc;
	
	@Column(name="DRIVER_25")
	private String driver25;
	
	@Column(name="DRIVER_25_DESC")
	private String driver25Desc;
	
	@Column(name="DRIVER_25_THRU")
	private String driver25Thru;
	
	@Column(name="DRIVER_25_THRU_DESC")
	private String driver25ThruDesc;
	
	@Column(name="DRIVER_26")
	private String driver26;
	
	@Column(name="DRIVER_26_DESC")
	private String driver26Desc;
	
	@Column(name="DRIVER_26_THRU")
	private String driver26Thru;
	
	@Column(name="DRIVER_26_THRU_DESC")
	private String driver26ThruDesc;
	
	@Column(name="DRIVER_27")
	private String driver27;
	
	@Column(name="DRIVER_27_DESC")
	private String driver27Desc;
	
	@Column(name="DRIVER_27_THRU")
	private String driver27Thru;
	
	@Column(name="DRIVER_27_THRU_DESC")
	private String driver27ThruDesc;
	
	@Column(name="DRIVER_28")
	private String driver28;
	
	@Column(name="DRIVER_28_DESC")
	private String driver28Desc;
	
	@Column(name="DRIVER_28_THRU")
	private String driver28Thru;
	
	@Column(name="DRIVER_28_THRU_DESC")
	private String driver28ThruDesc;
	
	@Column(name="DRIVER_29")
	private String driver29;
	
	@Column(name="DRIVER_29_DESC")
	private String driver29Desc;
	
	@Column(name="DRIVER_29_THRU")
	private String driver29Thru;
	
	@Column(name="DRIVER_29_THRU_DESC")
	private String driver29ThruDesc;
	
	@Column(name="DRIVER_30")
	private String driver30;
	
	@Column(name="DRIVER_30_DESC")
	private String driver30Desc;
	
	@Column(name="DRIVER_30_THRU")
	private String driver30Thru;
	
	@Column(name="DRIVER_30_THRU_DESC")
	private String driver30ThruDesc;
	
	@Column(name="BINARY_WEIGHT")
    private Long binaryWeight;
    
    @Column(name="SIGNIFICANT_DIGITS")
    private String significantDigits;
    
    @Column(name="DEFAULT_FLAG")
    private String defaultFlag;
    
    @Column(name="DEFAULT_BINARY_WEIGHT")
    private Long defaultBinaryWeight;
    
	@Column(name="DEFAULT_SIGNIFICANT_DIGITS")
    private String defaultSignificantDigits;
    
    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
    
    @Column(name="RELATION_SIGN")
    private String relationSign;
    
	@Column(name="RELATION_AMOUNT")
    private Long relationAmount;
    
    @Column(name="THEN_HOLD_CODE_FLAG")
    private String thenHoldCodeFlag;
    
    @Column(name="THEN_TAXCODE_CODE")
    private String thenTaxcodeCode;
        
    @Column(name="ELSE_HOLD_CODE_FLAG")
    private String elseHoldCodeFlag;
    
    @Column(name="ELSE_TAXCODE_CODE")
    private String elseTaxcodeCode;
         
    @Column(name="COMMENTS")
    private String comments;
    
    @Column(name="ACTIVE_FLAG")
    private String activeFlag;
    
    @Column(name="MODULE_CODE")
    private String moduleCode;
    
    @Column(name="ENTITY_ID")
    private Long entityId;

    /** default constructor */
    public GSBMatrix() {
    }

	/** minimal constructor */
    public GSBMatrix(Long taxMatrixId) {
    	this.taxMatrixId = taxMatrixId;
    }
    
    // Idable interface
    public Long getId() {
    	return taxMatrixId;
    }

    public void setId(Long id) {
    	this.taxMatrixId = id;
    }
    
    public String getIdPropertyName() {
    	return "taxMatrixId";
    }
    
    // Matrix overrides
    @Override
    public Long getDriverCount() {
    	return DRIVER_COUNT;
    }
    
    @Override
    public String getDriverCode() {
    	return DRIVER_CODE;
    }

    @Override
    public boolean getHasDescriptions() {
    	return true;
    }

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getBinaryWeight() {
		return binaryWeight;
	}

	public void setBinaryWeight(Long binaryWeight) {
		this.binaryWeight = binaryWeight;
	}

	public Long getDefaultBinaryWeight() {
		return defaultBinaryWeight;
	}

	public void setDefaultBinaryWeight(Long defaultBinaryWeight) {
		this.defaultBinaryWeight = defaultBinaryWeight;
	}

	public String getDefaultFlag() {
		return defaultFlag;
	}

	public void setDefaultFlag(String defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	public Boolean getDefaultBooleanFlag() {
		return (defaultFlag == null)? null:defaultFlag.equals("1");
	}

	public void setDefaultBooleanFlag(Boolean defaultFlag) {
		setDefaultFlag((defaultFlag == null)? null:((defaultFlag)? "1":"0"));
	}

	public String getDefaultSignificantDigits() {
		return defaultSignificantDigits;
	}

	public void setDefaultSignificantDigits(String defaultSignificantDigits) {
		this.defaultSignificantDigits = defaultSignificantDigits;
	}

	public String getDriver01() {
		return driver01;
	}

	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}

	public String getDriver01Desc() {
		return driver01Desc;
	}

	public void setDriver01Desc(String driver01Desc) {
		this.driver01Desc = driver01Desc;
	}

	public String getDriver01Thru() {
		return driver01Thru;
	}

	public void setDriver01Thru(String driver01Thru) {
		this.driver01Thru = driver01Thru;
	}

	public String getDriver01ThruDesc() {
		return driver01ThruDesc;
	}

	public void setDriver01ThruDesc(String driver01ThruDesc) {
		this.driver01ThruDesc = driver01ThruDesc;
	}

	public String getDriver02() {
		return driver02;
	}

	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}

	public String getDriver02Desc() {
		return driver02Desc;
	}

	public void setDriver02Desc(String driver02Desc) {
		this.driver02Desc = driver02Desc;
	}

	public String getDriver02Thru() {
		return driver02Thru;
	}

	public void setDriver02Thru(String driver02Thru) {
		this.driver02Thru = driver02Thru;
	}

	public String getDriver02ThruDesc() {
		return driver02ThruDesc;
	}

	public void setDriver02ThruDesc(String driver02ThruDesc) {
		this.driver02ThruDesc = driver02ThruDesc;
	}

	public String getDriver03() {
		return driver03;
	}

	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}

	public String getDriver03Desc() {
		return driver03Desc;
	}

	public void setDriver03Desc(String driver03Desc) {
		this.driver03Desc = driver03Desc;
	}

	public String getDriver03Thru() {
		return driver03Thru;
	}

	public void setDriver03Thru(String driver03Thru) {
		this.driver03Thru = driver03Thru;
	}

	public String getDriver03ThruDesc() {
		return driver03ThruDesc;
	}

	public void setDriver03ThruDesc(String driver03ThruDesc) {
		this.driver03ThruDesc = driver03ThruDesc;
	}

	public String getDriver04() {
		return driver04;
	}

	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}

	public String getDriver04Desc() {
		return driver04Desc;
	}

	public void setDriver04Desc(String driver04Desc) {
		this.driver04Desc = driver04Desc;
	}

	public String getDriver04Thru() {
		return driver04Thru;
	}

	public void setDriver04Thru(String driver04Thru) {
		this.driver04Thru = driver04Thru;
	}

	public String getDriver04ThruDesc() {
		return driver04ThruDesc;
	}

	public void setDriver04ThruDesc(String driver04ThruDesc) {
		this.driver04ThruDesc = driver04ThruDesc;
	}

	public String getDriver05() {
		return driver05;
	}

	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}

	public String getDriver05Desc() {
		return driver05Desc;
	}

	public void setDriver05Desc(String driver05Desc) {
		this.driver05Desc = driver05Desc;
	}

	public String getDriver05Thru() {
		return driver05Thru;
	}

	public void setDriver05Thru(String driver05Thru) {
		this.driver05Thru = driver05Thru;
	}

	public String getDriver05ThruDesc() {
		return driver05ThruDesc;
	}

	public void setDriver05ThruDesc(String driver05ThruDesc) {
		this.driver05ThruDesc = driver05ThruDesc;
	}

	public String getDriver06() {
		return driver06;
	}

	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}

	public String getDriver06Desc() {
		return driver06Desc;
	}

	public void setDriver06Desc(String driver06Desc) {
		this.driver06Desc = driver06Desc;
	}

	public String getDriver06Thru() {
		return driver06Thru;
	}

	public void setDriver06Thru(String driver06Thru) {
		this.driver06Thru = driver06Thru;
	}

	public String getDriver06ThruDesc() {
		return driver06ThruDesc;
	}

	public void setDriver06ThruDesc(String driver06ThruDesc) {
		this.driver06ThruDesc = driver06ThruDesc;
	}

	public String getDriver07() {
		return driver07;
	}

	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}

	public String getDriver07Desc() {
		return driver07Desc;
	}

	public void setDriver07Desc(String driver07Desc) {
		this.driver07Desc = driver07Desc;
	}

	public String getDriver07Thru() {
		return driver07Thru;
	}

	public void setDriver07Thru(String driver07Thru) {
		this.driver07Thru = driver07Thru;
	}

	public String getDriver07ThruDesc() {
		return driver07ThruDesc;
	}

	public void setDriver07ThruDesc(String driver07ThruDesc) {
		this.driver07ThruDesc = driver07ThruDesc;
	}

	public String getDriver08() {
		return driver08;
	}

	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}

	public String getDriver08Desc() {
		return driver08Desc;
	}

	public void setDriver08Desc(String driver08Desc) {
		this.driver08Desc = driver08Desc;
	}

	public String getDriver08Thru() {
		return driver08Thru;
	}

	public void setDriver08Thru(String driver08Thru) {
		this.driver08Thru = driver08Thru;
	}

	public String getDriver08ThruDesc() {
		return driver08ThruDesc;
	}

	public void setDriver08ThruDesc(String driver08ThruDesc) {
		this.driver08ThruDesc = driver08ThruDesc;
	}

	public String getDriver09() {
		return driver09;
	}

	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}

	public String getDriver09Desc() {
		return driver09Desc;
	}

	public void setDriver09Desc(String driver09Desc) {
		this.driver09Desc = driver09Desc;
	}

	public String getDriver09Thru() {
		return driver09Thru;
	}

	public void setDriver09Thru(String driver09Thru) {
		this.driver09Thru = driver09Thru;
	}

	public String getDriver09ThruDesc() {
		return driver09ThruDesc;
	}

	public void setDriver09ThruDesc(String driver09ThruDesc) {
		this.driver09ThruDesc = driver09ThruDesc;
	}

	public String getDriver10() {
		return driver10;
	}

	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}

	public String getDriver10Desc() {
		return driver10Desc;
	}

	public void setDriver10Desc(String driver10Desc) {
		this.driver10Desc = driver10Desc;
	}

	public String getDriver10Thru() {
		return driver10Thru;
	}

	public void setDriver10Thru(String driver10Thru) {
		this.driver10Thru = driver10Thru;
	}

	public String getDriver10ThruDesc() {
		return driver10ThruDesc;
	}

	public void setDriver10ThruDesc(String driver10ThruDesc) {
		this.driver10ThruDesc = driver10ThruDesc;
	}

	public String getDriver11() {
		return driver11;
	}

	public void setDriver11(String driver11) {
		this.driver11 = driver11;
	}

	public String getDriver11Desc() {
		return driver11Desc;
	}

	public void setDriver11Desc(String driver11Desc) {
		this.driver11Desc = driver11Desc;
	}

	public String getDriver11Thru() {
		return driver11Thru;
	}

	public void setDriver11Thru(String driver11Thru) {
		this.driver11Thru = driver11Thru;
	}

	public String getDriver11ThruDesc() {
		return driver11ThruDesc;
	}

	public void setDriver11ThruDesc(String driver11ThruDesc) {
		this.driver11ThruDesc = driver11ThruDesc;
	}

	public String getDriver12() {
		return driver12;
	}

	public void setDriver12(String driver12) {
		this.driver12 = driver12;
	}

	public String getDriver12Desc() {
		return driver12Desc;
	}

	public void setDriver12Desc(String driver12Desc) {
		this.driver12Desc = driver12Desc;
	}

	public String getDriver12Thru() {
		return driver12Thru;
	}

	public void setDriver12Thru(String driver12Thru) {
		this.driver12Thru = driver12Thru;
	}

	public String getDriver12ThruDesc() {
		return driver12ThruDesc;
	}

	public void setDriver12ThruDesc(String driver12ThruDesc) {
		this.driver12ThruDesc = driver12ThruDesc;
	}

	public String getDriver13() {
		return driver13;
	}

	public void setDriver13(String driver13) {
		this.driver13 = driver13;
	}

	public String getDriver13Desc() {
		return driver13Desc;
	}

	public void setDriver13Desc(String driver13Desc) {
		this.driver13Desc = driver13Desc;
	}

	public String getDriver13Thru() {
		return driver13Thru;
	}

	public void setDriver13Thru(String driver13Thru) {
		this.driver13Thru = driver13Thru;
	}

	public String getDriver13ThruDesc() {
		return driver13ThruDesc;
	}

	public void setDriver13ThruDesc(String driver13ThruDesc) {
		this.driver13ThruDesc = driver13ThruDesc;
	}

	public String getDriver14() {
		return driver14;
	}

	public void setDriver14(String driver14) {
		this.driver14 = driver14;
	}

	public String getDriver14Desc() {
		return driver14Desc;
	}

	public void setDriver14Desc(String driver14Desc) {
		this.driver14Desc = driver14Desc;
	}

	public String getDriver14Thru() {
		return driver14Thru;
	}

	public void setDriver14Thru(String driver14Thru) {
		this.driver14Thru = driver14Thru;
	}

	public String getDriver14ThruDesc() {
		return driver14ThruDesc;
	}

	public void setDriver14ThruDesc(String driver14ThruDesc) {
		this.driver14ThruDesc = driver14ThruDesc;
	}

	public String getDriver15() {
		return driver15;
	}

	public void setDriver15(String driver15) {
		this.driver15 = driver15;
	}

	public String getDriver15Desc() {
		return driver15Desc;
	}

	public void setDriver15Desc(String driver15Desc) {
		this.driver15Desc = driver15Desc;
	}

	public String getDriver15Thru() {
		return driver15Thru;
	}

	public void setDriver15Thru(String driver15Thru) {
		this.driver15Thru = driver15Thru;
	}

	public String getDriver15ThruDesc() {
		return driver15ThruDesc;
	}

	public void setDriver15ThruDesc(String driver15ThruDesc) {
		this.driver15ThruDesc = driver15ThruDesc;
	}

	public String getDriver16() {
		return driver16;
	}

	public void setDriver16(String driver16) {
		this.driver16 = driver16;
	}

	public String getDriver16Desc() {
		return driver16Desc;
	}

	public void setDriver16Desc(String driver16Desc) {
		this.driver16Desc = driver16Desc;
	}

	public String getDriver16Thru() {
		return driver16Thru;
	}

	public void setDriver16Thru(String driver16Thru) {
		this.driver16Thru = driver16Thru;
	}

	public String getDriver16ThruDesc() {
		return driver16ThruDesc;
	}

	public void setDriver16ThruDesc(String driver16ThruDesc) {
		this.driver16ThruDesc = driver16ThruDesc;
	}

	public String getDriver17() {
		return driver17;
	}

	public void setDriver17(String driver17) {
		this.driver17 = driver17;
	}

	public String getDriver17Desc() {
		return driver17Desc;
	}

	public void setDriver17Desc(String driver17Desc) {
		this.driver17Desc = driver17Desc;
	}

	public String getDriver17Thru() {
		return driver17Thru;
	}

	public void setDriver17Thru(String driver17Thru) {
		this.driver17Thru = driver17Thru;
	}

	public String getDriver17ThruDesc() {
		return driver17ThruDesc;
	}

	public void setDriver17ThruDesc(String driver17ThruDesc) {
		this.driver17ThruDesc = driver17ThruDesc;
	}

	public String getDriver18() {
		return driver18;
	}

	public void setDriver18(String driver18) {
		this.driver18 = driver18;
	}

	public String getDriver18Desc() {
		return driver18Desc;
	}

	public void setDriver18Desc(String driver18Desc) {
		this.driver18Desc = driver18Desc;
	}

	public String getDriver18Thru() {
		return driver18Thru;
	}

	public void setDriver18Thru(String driver18Thru) {
		this.driver18Thru = driver18Thru;
	}

	public String getDriver18ThruDesc() {
		return driver18ThruDesc;
	}

	public void setDriver18ThruDesc(String driver18ThruDesc) {
		this.driver18ThruDesc = driver18ThruDesc;
	}

	public String getDriver19() {
		return driver19;
	}

	public void setDriver19(String driver19) {
		this.driver19 = driver19;
	}

	public String getDriver19Desc() {
		return driver19Desc;
	}

	public void setDriver19Desc(String driver19Desc) {
		this.driver19Desc = driver19Desc;
	}

	public String getDriver19Thru() {
		return driver19Thru;
	}

	public void setDriver19Thru(String driver19Thru) {
		this.driver19Thru = driver19Thru;
	}

	public String getDriver19ThruDesc() {
		return driver19ThruDesc;
	}

	public void setDriver19ThruDesc(String driver19ThruDesc) {
		this.driver19ThruDesc = driver19ThruDesc;
	}

	public String getDriver20() {
		return driver20;
	}

	public void setDriver20(String driver20) {
		this.driver20 = driver20;
	}

	public String getDriver20Desc() {
		return driver20Desc;
	}

	public void setDriver20Desc(String driver20Desc) {
		this.driver20Desc = driver20Desc;
	}

	public String getDriver20Thru() {
		return driver20Thru;
	}

	public void setDriver20Thru(String driver20Thru) {
		this.driver20Thru = driver20Thru;
	}

	public String getDriver20ThruDesc() {
		return driver20ThruDesc;
	}

	public void setDriver20ThruDesc(String driver20ThruDesc) {
		this.driver20ThruDesc = driver20ThruDesc;
	}

	public String getDriver21() {
		return driver21;
	}

	public void setDriver21(String driver21) {
		this.driver21 = driver21;
	}

	public String getDriver21Desc() {
		return driver21Desc;
	}

	public void setDriver21Desc(String driver21Desc) {
		this.driver21Desc = driver21Desc;
	}

	public String getDriver21Thru() {
		return driver21Thru;
	}

	public void setDriver21Thru(String driver21Thru) {
		this.driver21Thru = driver21Thru;
	}

	public String getDriver21ThruDesc() {
		return driver21ThruDesc;
	}

	public void setDriver21ThruDesc(String driver21ThruDesc) {
		this.driver21ThruDesc = driver21ThruDesc;
	}

	public String getDriver22() {
		return driver22;
	}

	public void setDriver22(String driver22) {
		this.driver22 = driver22;
	}

	public String getDriver22Desc() {
		return driver22Desc;
	}

	public void setDriver22Desc(String driver22Desc) {
		this.driver22Desc = driver22Desc;
	}

	public String getDriver22Thru() {
		return driver22Thru;
	}

	public void setDriver22Thru(String driver22Thru) {
		this.driver22Thru = driver22Thru;
	}

	public String getDriver22ThruDesc() {
		return driver22ThruDesc;
	}

	public void setDriver22ThruDesc(String driver22ThruDesc) {
		this.driver22ThruDesc = driver22ThruDesc;
	}

	public String getDriver23() {
		return driver23;
	}

	public void setDriver23(String driver23) {
		this.driver23 = driver23;
	}

	public String getDriver23Desc() {
		return driver23Desc;
	}

	public void setDriver23Desc(String driver23Desc) {
		this.driver23Desc = driver23Desc;
	}

	public String getDriver23Thru() {
		return driver23Thru;
	}

	public void setDriver23Thru(String driver23Thru) {
		this.driver23Thru = driver23Thru;
	}

	public String getDriver23ThruDesc() {
		return driver23ThruDesc;
	}

	public void setDriver23ThruDesc(String driver23ThruDesc) {
		this.driver23ThruDesc = driver23ThruDesc;
	}

	public String getDriver24() {
		return driver24;
	}

	public void setDriver24(String driver24) {
		this.driver24 = driver24;
	}

	public String getDriver24Desc() {
		return driver24Desc;
	}

	public void setDriver24Desc(String driver24Desc) {
		this.driver24Desc = driver24Desc;
	}

	public String getDriver24Thru() {
		return driver24Thru;
	}

	public void setDriver24Thru(String driver24Thru) {
		this.driver24Thru = driver24Thru;
	}

	public String getDriver24ThruDesc() {
		return driver24ThruDesc;
	}

	public void setDriver24ThruDesc(String driver24ThruDesc) {
		this.driver24ThruDesc = driver24ThruDesc;
	}

	public String getDriver25() {
		return driver25;
	}

	public void setDriver25(String driver25) {
		this.driver25 = driver25;
	}

	public String getDriver25Desc() {
		return driver25Desc;
	}

	public void setDriver25Desc(String driver25Desc) {
		this.driver25Desc = driver25Desc;
	}

	public String getDriver25Thru() {
		return driver25Thru;
	}

	public void setDriver25Thru(String driver25Thru) {
		this.driver25Thru = driver25Thru;
	}

	public String getDriver25ThruDesc() {
		return driver25ThruDesc;
	}

	public void setDriver25ThruDesc(String driver25ThruDesc) {
		this.driver25ThruDesc = driver25ThruDesc;
	}

	public String getDriver26() {
		return driver26;
	}

	public void setDriver26(String driver26) {
		this.driver26 = driver26;
	}

	public String getDriver26Desc() {
		return driver26Desc;
	}

	public void setDriver26Desc(String driver26Desc) {
		this.driver26Desc = driver26Desc;
	}

	public String getDriver26Thru() {
		return driver26Thru;
	}

	public void setDriver26Thru(String driver26Thru) {
		this.driver26Thru = driver26Thru;
	}

	public String getDriver26ThruDesc() {
		return driver26ThruDesc;
	}

	public void setDriver26ThruDesc(String driver26ThruDesc) {
		this.driver26ThruDesc = driver26ThruDesc;
	}

	public String getDriver27() {
		return driver27;
	}

	public void setDriver27(String driver27) {
		this.driver27 = driver27;
	}

	public String getDriver27Desc() {
		return driver27Desc;
	}

	public void setDriver27Desc(String driver27Desc) {
		this.driver27Desc = driver27Desc;
	}

	public String getDriver27Thru() {
		return driver27Thru;
	}

	public void setDriver27Thru(String driver27Thru) {
		this.driver27Thru = driver27Thru;
	}

	public String getDriver27ThruDesc() {
		return driver27ThruDesc;
	}

	public void setDriver27ThruDesc(String driver27ThruDesc) {
		this.driver27ThruDesc = driver27ThruDesc;
	}

	public String getDriver28() {
		return driver28;
	}

	public void setDriver28(String driver28) {
		this.driver28 = driver28;
	}

	public String getDriver28Desc() {
		return driver28Desc;
	}

	public void setDriver28Desc(String driver28Desc) {
		this.driver28Desc = driver28Desc;
	}

	public String getDriver28Thru() {
		return driver28Thru;
	}

	public void setDriver28Thru(String driver28Thru) {
		this.driver28Thru = driver28Thru;
	}

	public String getDriver28ThruDesc() {
		return driver28ThruDesc;
	}

	public void setDriver28ThruDesc(String driver28ThruDesc) {
		this.driver28ThruDesc = driver28ThruDesc;
	}

	public String getDriver29() {
		return driver29;
	}

	public void setDriver29(String driver29) {
		this.driver29 = driver29;
	}

	public String getDriver29Desc() {
		return driver29Desc;
	}

	public void setDriver29Desc(String driver29Desc) {
		this.driver29Desc = driver29Desc;
	}

	public String getDriver29Thru() {
		return driver29Thru;
	}

	public void setDriver29Thru(String driver29Thru) {
		this.driver29Thru = driver29Thru;
	}

	public String getDriver29ThruDesc() {
		return driver29ThruDesc;
	}

	public void setDriver29ThruDesc(String driver29ThruDesc) {
		this.driver29ThruDesc = driver29ThruDesc;
	}

	public String getDriver30() {
		return driver30;
	}

	public void setDriver30(String driver30) {
		this.driver30 = driver30;
	}

	public String getDriver30Desc() {
		return driver30Desc;
	}

	public void setDriver30Desc(String driver30Desc) {
		this.driver30Desc = driver30Desc;
	}

	public String getDriver30Thru() {
		return driver30Thru;
	}

	public void setDriver30Thru(String driver30Thru) {
		this.driver30Thru = driver30Thru;
	}

	public String getDriver30ThruDesc() {
		return driver30ThruDesc;
	}

	public void setDriver30ThruDesc(String driver30ThruDesc) {
		this.driver30ThruDesc = driver30ThruDesc;
	}

	public String getDriverGlobalFlag() {
		return driverGlobalFlag;
	}

	public void setDriverGlobalFlag(String driverGlobalFlag) {
		this.driverGlobalFlag = driverGlobalFlag;
	}

	public Boolean getDriverGlobalBooleanFlag() {
		return (driverGlobalFlag == null)? null:driverGlobalFlag.equals("1");
	}

	public void setDriverGlobalBooleanFlag(Boolean driverGlobalFlag) {
		setDriverGlobalFlag((driverGlobalFlag == null)? null:((driverGlobalFlag)? "1":"0"));
	}

	public String getSignificantDigits() {
		return significantDigits;
	}

	public void setSignificantDigits(String significantDigits) {
		this.significantDigits = significantDigits;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public boolean getHasComments() {
		return ((comments != null) && !comments.trim().equals(""));
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getElseHoldCodeFlag() {
		return elseHoldCodeFlag;
	}

	public void setElseHoldCodeFlag(String elseHoldCodeFlag) {
		this.elseHoldCodeFlag = elseHoldCodeFlag;
	}

	public Boolean getElseHoldCodeBooleanFlag() {
		return (elseHoldCodeFlag == null)? null:elseHoldCodeFlag.equals("1");
	}

	public void setElseHoldCodeBooleanFlag(Boolean elseHoldCodeFlag) {
		setElseHoldCodeFlag((elseHoldCodeFlag == null)? null:((elseHoldCodeFlag)? "1":"0"));
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Long getRelationAmount() {
		return relationAmount;
	}

	public void setRelationAmount(Long relationAmount) {
		this.relationAmount = relationAmount;
	}

	public String getRelationSign() {
		return relationSign;
	}

	public void setRelationSign(String relationSign) {
		this.relationSign = relationSign;
	}

	public String getThenHoldCodeFlag() {
		return thenHoldCodeFlag;
	}

	public void setThenHoldCodeFlag(String thenHoldCodeFlag) {
		this.thenHoldCodeFlag = thenHoldCodeFlag;
	}

	public Boolean getThenHoldCodeBooleanFlag() {
		return (thenHoldCodeFlag == null)? null:thenHoldCodeFlag.equals("1");
	}

	public void setThenHoldCodeBooleanFlag(Boolean thenHoldCodeFlag) {
		setThenHoldCodeFlag((thenHoldCodeFlag == null)? null:((thenHoldCodeFlag)? "1":"0"));
	}

	public String getThenTaxcodeCode() {
		return thenTaxcodeCode;
	}

	public void setThenTaxcodeCode(String thenTaxcodeCode) {
		this.thenTaxcodeCode = thenTaxcodeCode;
	}

	public String getElseTaxcodeCode() {
		return elseTaxcodeCode;
	}

	public void setElseTaxcodeCode(String elseTaxcodeCode) {
		this.elseTaxcodeCode = elseTaxcodeCode;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public void initializeDefaults() {
		super.initializeDefaults();
		if ((relationSign == null) || relationSign.equals("")) setRelationSign("na");
		if ((defaultFlag == null) || defaultFlag.equals("")) setDefaultBooleanFlag(false);
		if ((driverGlobalFlag == null) || driverGlobalFlag.equals("")) setDriverGlobalBooleanFlag(false);
		if((activeFlag == null)  || activeFlag.equals(""))setActiveBooleanFlag(false);
		if ((thenHoldCodeFlag == null) || thenHoldCodeFlag.equals("")) setThenHoldCodeBooleanFlag(false);
		if ((elseHoldCodeFlag == null) || elseHoldCodeFlag.equals("")) setElseHoldCodeBooleanFlag(false);
		
		if ((driver01Thru == null) || driver01Thru.equals("")) setDriver01Thru("*NA");
		if ((driver02Thru == null) || driver02Thru.equals("")) setDriver02Thru("*NA");
		if ((driver03Thru == null) || driver03Thru.equals("")) setDriver03Thru("*NA");
		if ((driver04Thru == null) || driver04Thru.equals("")) setDriver04Thru("*NA");
		if ((driver05Thru == null) || driver05Thru.equals("")) setDriver05Thru("*NA");
		if ((driver06Thru == null) || driver06Thru.equals("")) setDriver06Thru("*NA");
		if ((driver07Thru == null) || driver07Thru.equals("")) setDriver07Thru("*NA");
		if ((driver08Thru == null) || driver08Thru.equals("")) setDriver08Thru("*NA");
		if ((driver09Thru == null) || driver09Thru.equals("")) setDriver09Thru("*NA");
		if ((driver10Thru == null) || driver10Thru.equals("")) setDriver10Thru("*NA");
		if ((driver11Thru == null) || driver11Thru.equals("")) setDriver11Thru("*NA");
		if ((driver12Thru == null) || driver12Thru.equals("")) setDriver12Thru("*NA");
		if ((driver13Thru == null) || driver13Thru.equals("")) setDriver13Thru("*NA");
		if ((driver14Thru == null) || driver14Thru.equals("")) setDriver14Thru("*NA");
		if ((driver15Thru == null) || driver15Thru.equals("")) setDriver15Thru("*NA");
		if ((driver16Thru == null) || driver16Thru.equals("")) setDriver16Thru("*NA");
		if ((driver17Thru == null) || driver17Thru.equals("")) setDriver17Thru("*NA");
		if ((driver18Thru == null) || driver18Thru.equals("")) setDriver18Thru("*NA");
		if ((driver19Thru == null) || driver19Thru.equals("")) setDriver19Thru("*NA");
		if ((driver20Thru == null) || driver20Thru.equals("")) setDriver20Thru("*NA");
		if ((driver21Thru == null) || driver21Thru.equals("")) setDriver21Thru("*NA");
		if ((driver22Thru == null) || driver22Thru.equals("")) setDriver22Thru("*NA");
		if ((driver23Thru == null) || driver23Thru.equals("")) setDriver23Thru("*NA");
		if ((driver24Thru == null) || driver24Thru.equals("")) setDriver24Thru("*NA");
		if ((driver25Thru == null) || driver25Thru.equals("")) setDriver25Thru("*NA");
		if ((driver26Thru == null) || driver26Thru.equals("")) setDriver26Thru("*NA");
		if ((driver27Thru == null) || driver27Thru.equals("")) setDriver27Thru("*NA");
		if ((driver28Thru == null) || driver28Thru.equals("")) setDriver28Thru("*NA");
		if ((driver29Thru == null) || driver29Thru.equals("")) setDriver29Thru("*NA");
		if ((driver30Thru == null) || driver30Thru.equals("")) setDriver30Thru("*NA");
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	

	
}
    

    