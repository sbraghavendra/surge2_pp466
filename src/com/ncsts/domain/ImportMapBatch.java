package com.ncsts.domain;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 * A useful class for dealing with import map batches
 */
public class ImportMapBatch extends BatchMaintenance implements Comparable<ImportMapBatch> {
	
	private static Logger log = Logger.getLogger(ImportMapBatch.class);

	public static final String IMPORTED= "I";
	public static final String AIDIMPORTED= "IA";
	

	/* a convenient place to cache an error */
	private String error;
	
	public ImportMapBatch() {
		super();
	}
	public ImportMapBatch(BatchMaintenance batch) {
		this();
		if (batch != null) BeanUtils.copyProperties(batch, this);
	}

	public boolean isImported() {
		return (IMPORTED.equalsIgnoreCase(getBatchStatusCode()) || AIDIMPORTED.equalsIgnoreCase(getBatchStatusCode()));
	}
	
	public String getError() {
		return this.error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	public int compareTo(ImportMapBatch other) {
		if (getBatchId() < other.getBatchId()) 
			return -1;
		if (getBatchId() > other.getBatchId()) 
			return 1;
		return 0;
	}
	
	public boolean equals(Object batch) {
		return (batch instanceof BatchMaintenance) && this.getBatchId().equals(((BatchMaintenance)batch).getBatchId());
	}
	public int hashCode() {
		return getBatchId().hashCode();
	}

}
