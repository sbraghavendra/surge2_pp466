package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TB_CUST_ENTITY database table.
 * 
 */
@Embeddable
public class CustEntityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CUST_ID")
	private Long custId;

	@Column(name="ENTITY_ID")
	private Long entityId;

    public CustEntityPK() {
    }
    
    public CustEntityPK(Long custId, Long entityId) {
		this.custId = custId;
		this.entityId = entityId;
    }
    
	public Long getCustId() {
		return this.custId;
	}
	public void setCustId(Long custId) {
		this.custId = custId;
	}
	public Long getEntityId() {
		return this.entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustEntityPK)) {
			return false;
		}
		CustEntityPK castOther = (CustEntityPK)other;
		return 
			(this.custId == castOther.custId)
			&& (this.entityId == castOther.entityId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.custId ^ (this.custId >>> 32)));
		hash = hash * prime + ((int) (this.entityId ^ (this.entityId >>> 32)));
		
		return hash;
    }
}