
package com.ncsts.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.view.util.BatchTypeSpecific;
import com.ncsts.ws.PinPointWebServiceConstants;

/**
 *  @author Muneer
 */


@Entity
@Table (name="TB_BATCH")
@XmlRootElement(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BatchMaintenance extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1L;
	
	public enum ErrorState {
		Warning("10"),
		Error("20"),
		Terminal("30");
		private String text;
		private ErrorState(String state) {
			this.text= state;
		}
		public String getText() {
			return text;
		}
	}

	@Id
    @Column(name="BATCH_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="batch_sequence")
	@SequenceGenerator(name="batch_sequence" , allocationSize = 1, sequenceName="sq_tb_batch_id")	
	private Long batchId;
    
/*    @Transient
    private List<AllocationMatrixDetailDTO> allocationMatrixDetailDTOs;  */  
    
	@Column(name="BATCH_TYPE_CODE")
    private String batchTypeCode;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="ENTRY_TIMESTAMP" )
    private Date entryTimestamp;
    
    @Column(name="BATCH_STATUS_CODE" )
    private String batchStatusCode;
    
    @Column(name="HELD_FLAG" )
    private String heldFlag;
    
    @Column(name="TOTAL_BYTES" )
    private Long totalBytes;
    
    @Column(name="ERROR_SEV_CODE" )
    private String errorSevCode;
    
    @Column(name="TOTAL_ROWS" )
    private Long totalRows;
    
    @Column(name="START_ROW" )
    private Long startRow;
    
    @Column(name="PROCESSED_BYTES" )
    private Long processedBytes;
  
    @Column(name="PROCESSED_ROWS" )
    private Long processedRows;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="SCHED_START_TIMESTAMP" )
    private Date schedStartTimestamp;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="ACTUAL_START_TIMESTAMP" )
    private Date actualStartTimestamp;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="ACTUAL_END_TIMESTAMP" )
    private Date actualEndTimestamp;
    
        
    @Column(name="VC01" )
    private String vc01;
    
    @Column(name="VC02" )
    private String vc02;
   
    @Column(name="VC03")
    private String vc03;

    @Column(name="VC04")
    private String vc04;

    @Column(name="VC05")
    private String vc05;
    
    @Column(name="VC06")
    private String vc06;

    @Column(name="VC07")
    private String vc07;

    @Column(name="VC08")
    private String vc08;

    @Column(name="VC09")
    private String vc09;
                                
    @Column(name="VC10")
    private String vc10;
    
    @Column(name="NU01")
    private Double nu01;
    
    @Column(name="NU02")
    private Double nu02;     

    @Column(name="NU03")
    private Double nu03;
    
    @Column(name="NU04")
    private Double nu04;
    
    @Column(name="NU05")
    private Double nu05;
    
    @Column(name="NU06")
    private Double nu06;
    
    @Column(name="NU07")
    private Double nu07;

    @Column(name="NU08")
    private Double nu08;
    
    @Column(name="NU09")
    private Double nu09;
    
    @Column(name="NU10")
    private Double nu10;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS01")
    private Date ts01;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS02")
    private Date ts02;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS03")
    private Date ts03;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS04")
    private Date ts04;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS05")
    private Date ts05;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS06")
    private Date ts06;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS07")
    private Date ts07;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS08")
    private Date ts08;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS09")
    private Date ts09;
    
    @Temporal(TemporalType.DATE)
    @Column(name="TS10")
    private Date ts10;
    
    @Column(name="YEAR_PERIOD")
	private String yearPeriod;
	@Column(name="REL_SEQN")
	private Long relSeqn;
	
    @Column(name="STATUS_UPDATE_USER_ID")
    private String statusUpdateUserId;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="STATUS_UPDATE_TIMESTAMP")
    private Date statusUpdateTimestamp;


	@Column(name="UVC01")
	private String uvc01;

	@Column(name="UVC02")
	private String uvc02;

	@Column(name="UVC03")
	private String uvc03;

	@Column(name="UVC04")
	private String uvc04;

	@Column(name="UVC05")
	private String uvc05;

	@Column(name="UVC06")
	private String uvc06;

	@Column(name="UVC07")
	private String uvc07;

	@Column(name="UVC08")
	private String uvc08;

	@Column(name="UVC09")
	private String uvc09;

	@Column(name="UVC10")
	private String uvc10;


	@Column(name="UNU01")
	private BigDecimal unu01;

	@Column(name="UNU02")
	private BigDecimal unu02;

	@Column(name="UNU03")
	private BigDecimal unu03;

	@Column(name="UNU04")
	private BigDecimal unu04;

	@Column(name="UNU05")
	private BigDecimal unu05;

	@Column(name="UNU06")
	private BigDecimal unu06;

	@Column(name="UNU07")
	private BigDecimal unu07;

	@Column(name="UNU08")
	private BigDecimal unu08;

	@Column(name="UNU09")
	private BigDecimal unu09;

	@Column(name="UNU10")
	private BigDecimal unu10;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS01")
	private Date uts01;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS02")
	private Date uts02;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS03")
	private Date uts03;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS04")
	private Date uts04;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS05")
	private Date uts05;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS06")
	private Date uts06;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS07")
	private Date uts07;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS08")
	private Date uts08;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS09")
	private Date uts09;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UTS10")
	private Date uts10;

	@Column(name="EMAIL_SENT_FLAG" )
	private String emailSentFlag;
	
	
	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BATCH_TYPE_CODE", insertable=false, updatable=false)
	@ForeignKey(name="tb_batch_fk01")
	@XmlTransient
	private BatchMetadata batchMetadata;
	
	@Transient
	private String batchTypeDesc;
	
	@Transient
	private String batchStatusDesc;
                                
	/**
	 * Allow UI to select/deselect this element (row) 
	 */
	@Transient
	private boolean selected;
	@Transient
	private String vc01Desc;
	
    
	@Transient
	private boolean attachPcoToCriteria;
	
	@Transient
	private List<BatchTypeSpecific> searchBatchTypeSpecificList = null; 
	
	@Transient
	private String lastModuelId;
	
	@Transient
	private boolean isCalledFromConfigImportExpMenu; 
	
	public boolean isCalledFromConfigImportExpMenu() {
		return isCalledFromConfigImportExpMenu;
	}

	public void setCalledFromConfigImportExpMenu(
			boolean isCalledFromConfigImportExpMenu) {
		this.isCalledFromConfigImportExpMenu = isCalledFromConfigImportExpMenu;
	}
	
	
	public String getLastModuelId() {
		return lastModuelId;
	}

	public void setLastModuelId(String lastModuelId) {
		this.lastModuelId = lastModuelId;
	}

	public List<BatchTypeSpecific> getSearchBatchTypeSpecificList() {
		return searchBatchTypeSpecificList;
	}

	public void setSearchBatchTypeSpecificList(
			List<BatchTypeSpecific> searchBatchTypeSpecificList) {
		this.searchBatchTypeSpecificList = searchBatchTypeSpecificList;
	}
	
    /*@Transient	
    BatchMaintenanceDTO atchMaintenanceDTO;
*/    // Constructors

    public String getVc01Desc() {
		return vc01Desc;
	}

	public void setVc01Desc(String vc01Desc) {
		this.vc01Desc = vc01Desc;
	}

	/** default constructor */
    public BatchMaintenance() {
    		this.selected = false;
    }

	/** minimal constructor */
    public BatchMaintenance(Long batchId) {
    	this();
    	this.batchId = batchId;
    }
    
    public BatchMaintenance(BatchMaintenance batch) {
    	this();
    	BeanUtils.copyProperties(batch, this);
    }
    
    public BatchMaintenanceDTO getBatchMaintenanceDTO() {
    	BatchMaintenanceDTO batchMaintenanceDTO= new BatchMaintenanceDTO();
    	BeanUtils.copyProperties(this,batchMaintenanceDTO);
    	return batchMaintenanceDTO;
    }


    
	public String toString() {
		StringBuffer buff = new StringBuffer();
		if (this.batchId != null) buff.append(" id = " + this.batchId.toString());
		buff.append("; batchId = " + this.batchId);
		return buff.toString();
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchTypeCode() {
		return batchTypeCode;
	}

	public void setBatchTypeCode(String batchTypeCode) {
		this.batchTypeCode = batchTypeCode;
	}

	public Date getEntryTimestamp() {
		return entryTimestamp;
	}

	public void setEntryTimestamp(Date entryTimestamp) {
		this.entryTimestamp = entryTimestamp;
	}

	public String getBatchStatusCode() {
		return batchStatusCode;
	}

	public void setBatchStatusCode(String batchStatusCode) {
		this.batchStatusCode = batchStatusCode;
	}


	public String getErrorSevCode() {
		return errorSevCode;
	}
	public void setErrorSevCode(ErrorState state) {
		this.errorSevCode = state.getText();
	}

	public void setErrorSevCode(String errorSevCode) {
		this.errorSevCode = errorSevCode;
	}
	
	public Long getTotalBytes() {
		return totalBytes;
	}

	public void setTotalBytes(Long totalBytes) {
		this.totalBytes = totalBytes;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

	public Long getStartRow() {
		return startRow;
	}

	public void setStartRow(Long startRow) {
		this.startRow = startRow;
	}

	public Long getProcessedBytes() {
		return processedBytes;
	}

	public void setProcessedBytes(Long processedBytes) {
		this.processedBytes = processedBytes;
	}

	public Long getProcessedRows() {
		return processedRows;
	}

	public void setProcessedRows(Long processedRows) {
		this.processedRows = processedRows;
	}


	public Date getSchedStartTimestamp() {
		return schedStartTimestamp;
	}

	public void setSchedStartTimestamp(Date schedStartTimestamp) {
		this.schedStartTimestamp = schedStartTimestamp;
	}

	public Date getActualStartTimestamp() {
		return actualStartTimestamp;
	}

	public void setActualStartTimestamp(Date actualStartTimestamp) {
		this.actualStartTimestamp = actualStartTimestamp;
	}

	public Date getActualEndTimestamp() {
		return actualEndTimestamp;
	}

	public void setActualEndTimestamp(Date actualEndTimestamp) {
		this.actualEndTimestamp = actualEndTimestamp;
	}

	public String getVc01() {
		return vc01;
	}

	public void setVc01(String vc01) {
		this.vc01 = vc01;
	}

	public String getVc02() {
		return vc02;
	}

	public void setVc02(String vc02) {
		this.vc02 = vc02;
	}

	public String getVc03() {
		return vc03;
	}

	public void setVc03(String vc03) {
		this.vc03 = vc03;
	}

	public String getVc04() {
		return vc04;
	}

	public void setVc04(String vc04) {
		this.vc04 = vc04;
	}

	public String getVc05() {
		return vc05;
	}

	public void setVc05(String vc05) {
		this.vc05 = vc05;
	}

	public String getVc06() {
		return vc06;
	}

	public void setVc06(String vc06) {
		this.vc06 = vc06;
	}

	public String getVc07() {
		return vc07;
	}

	public void setVc07(String vc07) {
		this.vc07 = vc07;
	}

	public String getVc08() {
		return vc08;
	}

	public void setVc08(String vc08) {
		this.vc08 = vc08;
	}

	public String getVc09() {
		return vc09;
	}

	public void setVc09(String vc09) {
		this.vc09 = vc09;
	}

	public String getVc10() {
		return vc10;
	}

	public void setVc10(String vc10) {
		this.vc10 = vc10;
	}

	
	public Date getTs01() {
		return ts01;
	}

	public void setTs01(Date ts01) {
		this.ts01 = ts01;
	}

	public Date getTs02() {
		return ts02;
	}

	public void setTs02(Date ts02) {
		this.ts02 = ts02;
	}

	public Date getTs03() {
		return ts03;
	}

	public void setTs03(Date ts03) {
		this.ts03 = ts03;
	}

	public Date getTs04() {
		return ts04;
	}

	public void setTs04(Date ts04) {
		this.ts04 = ts04;
	}

	public Date getTs05() {
		return ts05;
	}

	public void setTs05(Date ts05) {
		this.ts05 = ts05;
	}

	public Date getTs06() {
		return ts06;
	}

	public void setTs06(Date ts06) {
		this.ts06 = ts06;
	}

	public Date getTs07() {
		return ts07;
	}

	public void setTs07(Date ts07) {
		this.ts07 = ts07;
	}

	public Date getTs08() {
		return ts08;
	}

	public void setTs08(Date ts08) {
		this.ts08 = ts08;
	}

	public Date getTs09() {
		return ts09;
	}

	public void setTs09(Date ts09) {
		this.ts09 = ts09;
	}

	public Date getTs10() {
		return ts10;
	}

	public void setTs10(Date ts10) {
		this.ts10 = ts10;
	}

	public String getStatusUpdateUserId() {
		return statusUpdateUserId;
	}

	public void setStatusUpdateUserId(String statusUpdateUserId) {
		this.statusUpdateUserId = statusUpdateUserId;
	}

	public Date getStatusUpdateTimestamp() {
		return statusUpdateTimestamp;
	}

	public void setStatusUpdateTimestamp(Date statusUpdateTimestamp) {
		this.statusUpdateTimestamp = statusUpdateTimestamp;
	}

	public String getHeldFlag() {
		return heldFlag;
	}
	
	public boolean isHeld() {
		return "1".equalsIgnoreCase(heldFlag);
	}

	public void setHeldFlag(String heldFlag) {
		this.heldFlag = heldFlag;
	}
	
	public String getEmailSentFlag() {
		return emailSentFlag;
	}
	
	public boolean isEmailSent() {
		return "1".equalsIgnoreCase(emailSentFlag);
	}

	public void setEmailSentFlag(String emailSentFlag) {
		this.emailSentFlag = emailSentFlag;
	}

	public Double getNu01() {
		return nu01;
	}

	public void setNu01(Double nu01) {
		this.nu01 = nu01;
	}

	public Double getNu02() {
		return nu02;
	}

	public void setNu02(Double nu02) {
		this.nu02 = nu02;
	}

	public Double getNu03() {
		return nu03;
	}

	public void setNu03(Double nu03) {
		this.nu03 = nu03;
	}

	public Double getNu04() {
		return nu04;
	}

	public void setNu04(Double nu04) {
		this.nu04 = nu04;
	}

	public Double getNu05() {
		return nu05;
	}

	public void setNu05(Double nu05) {
		this.nu05 = nu05;
	}

	public Double getNu06() {
		return nu06;
	}

	public void setNu06(Double nu06) {
		this.nu06 = nu06;
	}

	public Double getNu07() {
		return nu07;
	}

	public void setNu07(Double nu07) {
		this.nu07 = nu07;
	}

	public Double getNu08() {
		return nu08;
	}

	public void setNu08(Double nu08) {
		this.nu08 = nu08;
	}

	public Double getNu09() {
		return nu09;
	}

	public void setNu09(Double nu09) {
		this.nu09 = nu09;
	}

	public Double getNu10() {
		return nu10;
	}

	public void setNu10(Double nu10) {
		this.nu10 = nu10;
	}

	public Long getId() {
		return getBatchId();
	}

	public String getIdPropertyName() {
		return "batchId";
	}
	
	public void setId(Long id) {
		setBatchId(id);
	}
	
	public Long getbatchId(){
		return this.batchId;
	}

	public String getBatchTypeDesc() {
		return batchTypeDesc;
	}

	public void setBatchTypeDesc(String batchTypeDesc) {
		this.batchTypeDesc = batchTypeDesc;
	}

	public String getBatchStatusDesc() {
		return batchStatusDesc;
	}

	public void setBatchStatusDesc(String batchStatusDesc) {
		this.batchStatusDesc = batchStatusDesc;
	}

	/**
	 * @return the selected
	 */
	public Boolean getSelected() {
		return this.selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	/* stupid jsf bean rules */
	public Boolean getInProgress() {
		return isInProgress();
	}
	public Boolean isInProgress() {
		return (batchStatusCode != null) && batchStatusCode.startsWith("X");
	}

	public Boolean canBeDeleted() {
		return (batchStatusCode != null) && 
		(batchStatusCode.equalsIgnoreCase("I") || batchStatusCode.equalsIgnoreCase("IA") || batchStatusCode.equalsIgnoreCase("FP") 
				|| batchStatusCode.equalsIgnoreCase("P")
				//6042
			//	|| batchStatusCode.equalsIgnoreCase("ITR") 
				|| batchStatusCode.equalsIgnoreCase("TR")
				//1328
				|| batchStatusCode.startsWith("AB")
				//6042
			//	|| batchStatusCode.equalsIgnoreCase("ITCR") 
				|| batchStatusCode.equalsIgnoreCase("TCR"));
		        //6042
				//|| batchStatusCode.equalsIgnoreCase("ISDM"));
	}

	public BatchMetadata getBatchMetadata() {
		return batchMetadata;
	}

	public void setBatchMetadata(BatchMetadata batchMetadata) {
		this.batchMetadata = batchMetadata;
	}
	
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	  static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
	}
	
	public String getRawUpdateStatement() throws Exception {
		Class<?> cls = BatchMaintenance.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("update TB_BATCH set ");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if ((col != null)
					&& !"BATCH_ID".equalsIgnoreCase(col.name())) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
				}
				colBuf.append(col.name()+"=?");
			}
		}

		idx++;
		String sql = colBuf.toString() + " where BATCH_ID=?";
		//System.out.println(sql);
		
		return sql;
	}
	
	public void populateUpdatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BatchMaintenance.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if ((col != null) && !"BATCH_ID".equalsIgnoreCase(col.name())) {
				Object fld = f.get(this);
				idx++;
				if (fld == null)
					//s.setNull(idx, sqlTypes.get(f.getType()));
					s.setNull(idx, Types.NULL);
				else{
					if("ENTRY_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getEntryTimestamp().getTime()));
					}
					else if("SCHED_START_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getSchedStartTimestamp().getTime()));
					}
					else if("ACTUAL_START_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getActualStartTimestamp().getTime()));
					}
					else if("ACTUAL_END_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getActualEndTimestamp().getTime()));
					}
					else if("STATUS_UPDATE_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getStatusUpdateTimestamp().getTime()));
					}
					else if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}
		
		idx++;
		s.setObject(idx, getBatchId());
	}

	public String getRawAddStatement() throws Exception {
		Class<?> cls = BatchMaintenance.class;
		Field [] fields = cls.getDeclaredFields();
		//StringBuffer colBuf = new StringBuffer("insert into TB_BATCH ");
		StringBuffer colBuf = new StringBuffer("");
		StringBuffer valBuf = new StringBuffer("");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first){
					first = false;
					colBuf.append("(");
					valBuf.append("(");
				}
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		
		if(!first){
			colBuf.append(")");
			valBuf.append(")");
		}

		String sql = "insert into TB_BATCH " + colBuf.toString() + " values " + valBuf.toString();
		//System.out.println(sql);
		
		return sql;
	}
	public String getYearPeriod() {
		return yearPeriod;
	}

	public void setYearPeriod(String yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Long getRelSeqn() {
		return relSeqn;
	}

	public void setRelSeqn(Long relSeqn) {
		this.relSeqn = relSeqn;
	}
	
	public void populateAddPreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BatchMaintenance.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null)
					//s.setNull(idx, sqlTypes.get(f.getType()));
					s.setNull(idx, Types.NULL);
				else{
					if("ENTRY_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getEntryTimestamp().getTime()));
					}
					else if("SCHED_START_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getSchedStartTimestamp().getTime()));
					}
					else if("ACTUAL_START_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getActualStartTimestamp().getTime()));
					}
					else if("ACTUAL_END_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getActualEndTimestamp().getTime()));
					}
					else if("STATUS_UPDATE_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getStatusUpdateTimestamp().getTime()));
					}
					else if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}
	}

	public boolean isAttachPcoToCriteria() {
		return attachPcoToCriteria;
	}

	public void setAttachPcoToCriteria(boolean attachPcoToCriteria) {
		this.attachPcoToCriteria = attachPcoToCriteria;
	}


	public String getUvc01() {
		return uvc01;
	}

	public void setUvc01(String uvc01) {
		this.uvc01 = uvc01;
	}

	public String getUvc02() {
		return uvc02;
	}

	public void setUvc02(String uvc02) {
		this.uvc02 = uvc02;
	}

	public String getUvc03() {
		return uvc03;
	}

	public void setUvc03(String uvc03) {
		this.uvc03 = uvc03;
	}

	public String getUvc04() {
		return uvc04;
	}

	public void setUvc04(String uvc04) {
		this.uvc04 = uvc04;
	}

	public String getUvc05() {
		return uvc05;
	}

	public void setUvc05(String uvc05) {
		this.uvc05 = uvc05;
	}

	public String getUvc06() {
		return uvc06;
	}

	public void setUvc06(String uvc06) {
		this.uvc06 = uvc06;
	}

	public String getUvc07() {
		return uvc07;
	}

	public void setUvc07(String uvc07) {
		this.uvc07 = uvc07;
	}

	public String getUvc08() {
		return uvc08;
	}

	public void setUvc08(String uvc08) {
		this.uvc08 = uvc08;
	}

	public String getUvc09() {
		return uvc09;
	}

	public void setUvc09(String uvc09) {
		this.uvc09 = uvc09;
	}

	public String getUvc10() {
		return uvc10;
	}

	public void setUvc10(String uvc10) {
		this.uvc10 = uvc10;
	}

	public BigDecimal getUnu01() {
		return unu01;
	}

	public void setUnu01(BigDecimal unu01) {
		this.unu01 = unu01;
	}

	public BigDecimal getUnu02() {
		return unu02;
	}

	public void setUnu02(BigDecimal unu02) {
		this.unu02 = unu02;
	}

	public BigDecimal getUnu03() {
		return unu03;
	}

	public void setUnu03(BigDecimal unu03) {
		this.unu03 = unu03;
	}

	public BigDecimal getUnu04() {
		return unu04;
	}

	public void setUnu04(BigDecimal unu04) {
		this.unu04 = unu04;
	}

	public BigDecimal getUnu05() {
		return unu05;
	}

	public void setUnu05(BigDecimal unu05) {
		this.unu05 = unu05;
	}

	public BigDecimal getUnu06() {
		return unu06;
	}

	public void setUnu06(BigDecimal unu06) {
		this.unu06 = unu06;
	}

	public BigDecimal getUnu07() {
		return unu07;
	}

	public void setUnu07(BigDecimal unu07) {
		this.unu07 = unu07;
	}

	public BigDecimal getUnu08() {
		return unu08;
	}

	public void setUnu08(BigDecimal unu08) {
		this.unu08 = unu08;
	}

	public BigDecimal getUnu09() {
		return unu09;
	}

	public void setUnu09(BigDecimal unu09) {
		this.unu09 = unu09;
	}

	public BigDecimal getUnu10() {
		return unu10;
	}

	public void setUnu10(BigDecimal unu10) {
		this.unu10 = unu10;
	}

	public Date getUts01() {
		return uts01;
	}

	public void setUts01(Date uts01) {
		this.uts01 = uts01;
	}

	public Date getUts02() {
		return uts02;
	}

	public void setUts02(Date uts02) {
		this.uts02 = uts02;
	}

	public Date getUts03() {
		return uts03;
	}

	public void setUts03(Date uts03) {
		this.uts03 = uts03;
	}

	public Date getUts04() {
		return uts04;
	}

	public void setUts04(Date uts04) {
		this.uts04 = uts04;
	}

	public Date getUts05() {
		return uts05;
	}

	public void setUts05(Date uts05) {
		this.uts05 = uts05;
	}

	public Date getUts06() {
		return uts06;
	}

	public void setUts06(Date uts06) {
		this.uts06 = uts06;
	}

	public Date getUts07() {
		return uts07;
	}

	public void setUts07(Date uts07) {
		this.uts07 = uts07;
	}

	public Date getUts08() {
		return uts08;
	}

	public void setUts08(Date uts08) {
		this.uts08 = uts08;
	}

	public Date getUts09() {
		return uts09;
	}

	public void setUts09(Date uts09) {
		this.uts09 = uts09;
	}

	public Date getUts10() {
		return uts10;
	}

	public void setUts10(Date uts10) {
		this.uts10 = uts10;
	}


}

