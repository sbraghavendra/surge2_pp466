package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
public class OptionCodePK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="OPTION_CODE")  	
	private String optionCode;
	@Column(name="OPTION_TYPE_CODE")  	
	private String optionTypeCode;
	@Column(name="USER_CODE")  	
	private String userCode;
	
	
	public OptionCodePK() {
		
	}
	public OptionCodePK(String optionCode, String optionTypeCode, String userCode) {
		this.optionCode = optionCode;
		this.optionTypeCode = optionTypeCode;
		this.userCode = userCode;
	}
	public String getOptionCode() {
		return optionCode;
	}
	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}
	public String getOptionTypeCode() {
		return optionTypeCode;
	}
	public void setOptionTypeCode(String optionTypeCode) {
		this.optionTypeCode = optionTypeCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof OptionCodePK)) {
            return false;
        }

        final OptionCodePK revision = (OptionCodePK)other;

        String f1 = getOptionCode();
        String f2 = revision.getOptionCode();
        if (!((f1 == f2) || (f1 != null && f1.equals(f2)))) {
            return false;
        }

        String f3 = getOptionTypeCode();
        String f4 = revision.getOptionTypeCode();
        if (!((f3 == f4) || (f3 != null && f3.equals(f4)))) {
            return false;
        }
        
        String c1 = getUserCode();
        String c2 = revision.getUserCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String f1 = getOptionCode();
		hash = 31 * hash + (null == f1 ? 0 : f1.hashCode());
        String f3 = getOptionTypeCode();
		hash = 31 * hash + (null == f3 ? 0 : f3.hashCode());
        String c1 = getUserCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.optionCode + "::" + this.optionTypeCode + "::" + this.userCode;
    }	
	
}
