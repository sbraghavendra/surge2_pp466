
package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.TransactionBlobDTO;


@Entity
@Table (name="TB_DOWNLOAD_FILES")
public class TransactionBlob implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="FILE_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="download_files_sequence")
	@SequenceGenerator(name="download_files_sequence" , allocationSize = 1, sequenceName="SQ_TB_DOWNLOAD_FILES_ID")	
	private Long fileId;
    
	@Column(name="FILE_NAME")
    private String fileName;

    @Column(name="USERNAME" )
    private String userName;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATE_DATE" )
    private Date createDate;
    
    @Column(name="ROW_COUNT" )
    private Long rowCount;
    
    @Column(name="FILE_SIZE")
        private Double fileSize;

    public void setFileSize(Double fileSize) {
		this.fileSize = fileSize;
	}

	public Double getFileSize() {
		return fileSize;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="START_TIME" )
    private Date startTime;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="END_TIME" )
    private Date endTime;
	
    /** default constructor */
    public TransactionBlob() {
    }

	/** minimal constructor */
    public TransactionBlob(Long fileId) {
    	this();
    	this.fileId = fileId;
    }
    
    public TransactionBlob(TransactionBlob transactionBlob) {
    	this();
    	BeanUtils.copyProperties(transactionBlob, this);
    }
    
    public TransactionBlobDTO getTransactionBlobDTO() {
    	TransactionBlobDTO transactionBlobDTO= new TransactionBlobDTO();
    	BeanUtils.copyProperties(this,transactionBlobDTO);
    	return transactionBlobDTO;
    }

	public String toString() {
		StringBuffer buff = new StringBuffer();
		if (this.fileId != null) buff.append(" id = " + this.fileId.toString());
		buff.append("; fileId = " + this.fileId);
		return buff.toString();
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Long getRowCount() {
		return rowCount;
	}

	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	public Long getId() {
		return getFileId();
	}
	
	public void setId(Long id) {
		setFileId(id);
	}

	public String getIdPropertyName() {
		return "fileId";
	}
	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	

}

