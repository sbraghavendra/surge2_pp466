
package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

public class TransactionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long purchtransLogId;
    private Long billtransLogId;  
    private Long purchtransId;
    private Long billtransId;

    public Long getPurchtransLogId() {
        return purchtransLogId;
    }

    public void setPurchtransLogId(Long purchtransLogId) {
        this.purchtransLogId = purchtransLogId;
    }

    public Long getPurchtransId() {
        return purchtransId;
    }

    public void setPurchtransId(Long purchtransId) {
        this.purchtransId = purchtransId;
    }
    
    public void setBilltransId(Long billtransId){
        this.billtransId = billtransId;
    }
    public Long getBilltransId(){
        return billtransId;
    }

    public void setBilltransLogId(Long billtransLogId){
        this.billtransLogId = billtransLogId;
    }
    public Long getBilltransLogId(){
        return billtransLogId;
    }
    
    private BigDecimal countryMinimumTaxableAmt;
    public void setCountryMinimumTaxableAmt(BigDecimal countryMinimumTaxableAmt){
        this.countryMinimumTaxableAmt = countryMinimumTaxableAmt;
    }
    public BigDecimal getCountryMinimumTaxableAmt(){
        return countryMinimumTaxableAmt;
    }

    private BigDecimal countryMaxtaxAmt;
    public void setCountryMaxtaxAmt(BigDecimal countryMaxtaxAmt){
        this.countryMaxtaxAmt = countryMaxtaxAmt;
    }
    public BigDecimal getCountryMaxtaxAmt(){
        return countryMaxtaxAmt;
    }

    private String countyBaseChangePct;
    public void setCountyBaseChangePct(String countyBaseChangePct){
        this.countyBaseChangePct = countyBaseChangePct;
    }
    public String getCountyBaseChangePct(){
        return countyBaseChangePct;
    }

    private BigDecimal aStj6TaxAmt;
    public void setAStj6TaxAmt(BigDecimal aStj6TaxAmt){
        this.aStj6TaxAmt = aStj6TaxAmt;
    }
    public BigDecimal getAStj6TaxAmt(){
        return aStj6TaxAmt;
    }

    private String stj9TaxtypeUsedCode;
    public void setStj9TaxtypeUsedCode(String stj9TaxtypeUsedCode){
        this.stj9TaxtypeUsedCode = stj9TaxtypeUsedCode;
    }
    public String getStj9TaxtypeUsedCode(){
        return stj9TaxtypeUsedCode;
    }

    private BigDecimal cityTier3Setamt;
    public void setCityTier3Setamt(BigDecimal cityTier3Setamt){
        this.cityTier3Setamt = cityTier3Setamt;
    }
    public BigDecimal getCityTier3Setamt(){
        return cityTier3Setamt;
    }

    private Long shipfromLocnMatrixId;
    public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId){
        this.shipfromLocnMatrixId = shipfromLocnMatrixId;
    }
    public Long getShipfromLocnMatrixId(){
        return shipfromLocnMatrixId;
    }

    private String cityBaseChangePct;
    public void setCityBaseChangePct(String cityBaseChangePct){
        this.cityBaseChangePct = cityBaseChangePct;
    }
    public String getCityBaseChangePct(){
        return cityBaseChangePct;
    }

    private String autoTransactionCountryCode;
    public void setAutoTransactionCountryCode(String autoTransactionCountryCode){
        this.autoTransactionCountryCode = autoTransactionCountryCode;
    }
    public String getAutoTransactionCountryCode(){
        return autoTransactionCountryCode;
    }

    private BigDecimal distr04Amt;
    public void setDistr04Amt(BigDecimal distr04Amt){
        this.distr04Amt = distr04Amt;
    }
    public BigDecimal getDistr04Amt(){
        return distr04Amt;
    }

    private BigDecimal bCityTier3TaxAmt;
    public void setBCityTier3TaxAmt(BigDecimal bCityTier3TaxAmt){
        this.bCityTier3TaxAmt = bCityTier3TaxAmt;
    }
    public BigDecimal getBCityTier3TaxAmt(){
        return bCityTier3TaxAmt;
    }

    private Long ordrorgnLocnMatrixId;
    public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId){
        this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
    }
    public Long getOrdrorgnLocnMatrixId(){
        return ordrorgnLocnMatrixId;
    }

    private BigDecimal countryTaxableThresholdAmt;
    public void setCountryTaxableThresholdAmt(BigDecimal countryTaxableThresholdAmt){
        this.countryTaxableThresholdAmt = countryTaxableThresholdAmt;
    }
    public BigDecimal getCountryTaxableThresholdAmt(){
        return countryTaxableThresholdAmt;
    }

    private BigDecimal bStj9TaxAmt;
    public void setBStj9TaxAmt(BigDecimal bStj9TaxAmt){
        this.bStj9TaxAmt = bStj9TaxAmt;
    }
    public BigDecimal getBStj9TaxAmt(){
        return bStj9TaxAmt;
    }

    private String directPayPermitFlag;
    public void setDirectPayPermitFlag(String directPayPermitFlag){
        this.directPayPermitFlag = directPayPermitFlag;
    }
    public String getDirectPayPermitFlag(){
        return directPayPermitFlag;
    }

    private String stj8TaxtypeUsedCode;
    public void setStj8TaxtypeUsedCode(String stj8TaxtypeUsedCode){
        this.stj8TaxtypeUsedCode = stj8TaxtypeUsedCode;
    }
    public String getStj8TaxtypeUsedCode(){
        return stj8TaxtypeUsedCode;
    }

    private BigDecimal bStj1TaxAmt;
    public void setBStj1TaxAmt(BigDecimal bStj1TaxAmt){
        this.bStj1TaxAmt = bStj1TaxAmt;
    }
    public BigDecimal getBStj1TaxAmt(){
        return bStj1TaxAmt;
    }

    private String countyTier3EntamFlag;
    public void setCountyTier3EntamFlag(String countyTier3EntamFlag){
        this.countyTier3EntamFlag = countyTier3EntamFlag;
    }
    public String getCountyTier3EntamFlag(){
        return countyTier3EntamFlag;
    }

    private String stj6Name;
    public void setStj6Name(String stj6Name){
        this.stj6Name = stj6Name;
    }
    public String getStj6Name(){
        return stj6Name;
    }

    private BigDecimal countyTier3Rate;
    public void setCountyTier3Rate(BigDecimal countyTier3Rate){
        this.countyTier3Rate = countyTier3Rate;
    }
    public BigDecimal getCountyTier3Rate(){
        return countyTier3Rate;
    }

    private BigDecimal stateTier1MaxAmt;
    public void setStateTier1MaxAmt(BigDecimal stateTier1MaxAmt){
        this.stateTier1MaxAmt = stateTier1MaxAmt;
    }
    public BigDecimal getStateTier1MaxAmt(){
        return stateTier1MaxAmt;
    }

    private BigDecimal aCityLocalTaxAmt;
    public void setACityLocalTaxAmt(BigDecimal aCityLocalTaxAmt){
        this.aCityLocalTaxAmt = aCityLocalTaxAmt;
    }
    public BigDecimal getACityLocalTaxAmt(){
        return aCityLocalTaxAmt;
    }

    private Long stj4TaxcodeDetailId;
    public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId){
        this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
    }
    public Long getStj4TaxcodeDetailId(){
        return stj4TaxcodeDetailId;
    }

    private BigDecimal countyTier3Setamt;
    public void setCountyTier3Setamt(BigDecimal countyTier3Setamt){
        this.countyTier3Setamt = countyTier3Setamt;
    }
    public BigDecimal getCountyTier3Setamt(){
        return countyTier3Setamt;
    }

    private Long stj3TaxcodeDetailId;
    public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId){
        this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
    }
    public Long getStj3TaxcodeDetailId(){
        return stj3TaxcodeDetailId;
    }

    private String countryBaseChangePct;
    public void setCountryBaseChangePct(String countryBaseChangePct){
        this.countryBaseChangePct = countryBaseChangePct;
    }
    public String getCountryBaseChangePct(){
        return countryBaseChangePct;
    }

    private String transactionStateCode;
    public void setTransactionStateCode(String transactionStateCode){
        this.transactionStateCode = transactionStateCode;
    }
    public String getTransactionStateCode(){
        return transactionStateCode;
    }

    private String stj7TaxtypeUsedCode;
    public void setStj7TaxtypeUsedCode(String stj7TaxtypeUsedCode){
        this.stj7TaxtypeUsedCode = stj7TaxtypeUsedCode;
    }
    public String getStj7TaxtypeUsedCode(){
        return stj7TaxtypeUsedCode;
    }

    private BigDecimal stateTier1Setamt;
    public void setStateTier1Setamt(BigDecimal stateTier1Setamt){
        this.stateTier1Setamt = stateTier1Setamt;
    }
    public BigDecimal getStateTier1Setamt(){
        return stateTier1Setamt;
    }

    private Date glDate;
    public void setGlDate(Date glDate){
        this.glDate = glDate;
    }
    public Date getGlDate(){
        return glDate;
    }

    private Long allocationMatrixId;
    public void setAllocationMatrixId(Long allocationMatrixId){
        this.allocationMatrixId = allocationMatrixId;
    }
    public Long getAllocationMatrixId(){
        return allocationMatrixId;
    }

    private BigDecimal countyTier1Setamt;
    public void setCountyTier1Setamt(BigDecimal countyTier1Setamt){
        this.countyTier1Setamt = countyTier1Setamt;
    }
    public BigDecimal getCountyTier1Setamt(){
        return countyTier1Setamt;
    }

    private BigDecimal stateTier3Setamt;
    public void setStateTier3Setamt(BigDecimal stateTier3Setamt){
        this.stateTier3Setamt = stateTier3Setamt;
    }
    public BigDecimal getStateTier3Setamt(){
        return stateTier3Setamt;
    }

    private Long stj2TaxcodeDetailId;
    public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId){
        this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
    }
    public Long getStj2TaxcodeDetailId(){
        return stj2TaxcodeDetailId;
    }

    private BigDecimal cityTier1Setamt;
    public void setCityTier1Setamt(BigDecimal cityTier1Setamt){
        this.cityTier1Setamt = cityTier1Setamt;
    }
    public BigDecimal getCityTier1Setamt(){
        return cityTier1Setamt;
    }

    private BigDecimal countyTier1Rate;
    public void setCountyTier1Rate(BigDecimal countyTier1Rate){
        this.countyTier1Rate = countyTier1Rate;
    }
    public BigDecimal getCountyTier1Rate(){
        return countyTier1Rate;
    }

    private String stj10Name;
    public void setStj10Name(String stj10Name){
        this.stj10Name = stj10Name;
    }
    public String getStj10Name(){
        return stj10Name;
    }

    private BigDecimal citySpecialSetamt;
    public void setCitySpecialSetamt(BigDecimal citySpecialSetamt){
        this.citySpecialSetamt = citySpecialSetamt;
    }
    public BigDecimal getCitySpecialSetamt(){
        return citySpecialSetamt;
    }

    private BigDecimal stj8Rate;
    public void setStj8Rate(BigDecimal stj8Rate){
        this.stj8Rate = stj8Rate;
    }
    public BigDecimal getStj8Rate(){
        return stj8Rate;
    }

    private BigDecimal bStj4TaxAmt;
    public void setBStj4TaxAmt(BigDecimal bStj4TaxAmt){
        this.bStj4TaxAmt = bStj4TaxAmt;
    }
    public BigDecimal getBStj4TaxAmt(){
        return bStj4TaxAmt;
    }

    private BigDecimal distr08Amt;
    public void setDistr08Amt(BigDecimal distr08Amt){
        this.distr08Amt = distr08Amt;
    }
    public BigDecimal getDistr08Amt(){
        return distr08Amt;
    }

    private Long allocationSubtransId;
    public void setAllocationSubtransId(Long allocationSubtransId){
        this.allocationSubtransId = allocationSubtransId;
    }
    public Long getAllocationSubtransId(){
        return allocationSubtransId;
    }

    private BigDecimal stj10TaxableAmt;
    public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt){
        this.stj10TaxableAmt = stj10TaxableAmt;
    }
    public BigDecimal getStj10TaxableAmt(){
        return stj10TaxableAmt;
    }

    private BigDecimal cityTier3TaxableAmt;
    public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt){
        this.cityTier3TaxableAmt = cityTier3TaxableAmt;
    }
    public BigDecimal getCityTier3TaxableAmt(){
        return cityTier3TaxableAmt;
    }

    private String stj4Name;
    public void setStj4Name(String stj4Name){
        this.stj4Name = stj4Name;
    }
    public String getStj4Name(){
        return stj4Name;
    }

    private Long countryTaxcodeDetailId;
    public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId){
        this.countryTaxcodeDetailId = countryTaxcodeDetailId;
    }
    public Long getCountryTaxcodeDetailId(){
        return countryTaxcodeDetailId;
    }

    private BigDecimal aStj3TaxAmt;
    public void setAStj3TaxAmt(BigDecimal aStj3TaxAmt){
        this.aStj3TaxAmt = aStj3TaxAmt;
    }
    public BigDecimal getAStj3TaxAmt(){
        return aStj3TaxAmt;
    }

    private BigDecimal cityTier1TaxableAmt;
    public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt){
        this.cityTier1TaxableAmt = cityTier1TaxableAmt;
    }
    public BigDecimal getCityTier1TaxableAmt(){
        return cityTier1TaxableAmt;
    }

    private BigDecimal countyTier3TaxableAmt;
    public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt){
        this.countyTier3TaxableAmt = countyTier3TaxableAmt;
    }
    public BigDecimal getCountyTier3TaxableAmt(){
        return countyTier3TaxableAmt;
    }

    private String stj10TaxtypeUsedCode;
    public void setStj10TaxtypeUsedCode(String stj10TaxtypeUsedCode){
        this.stj10TaxtypeUsedCode = stj10TaxtypeUsedCode;
    }
    public String getStj10TaxtypeUsedCode(){
        return stj10TaxtypeUsedCode;
    }

    private Long stj6TaxrateId;
    public void setStj6TaxrateId(Long stj6TaxrateId){
        this.stj6TaxrateId = stj6TaxrateId;
    }
    public Long getStj6TaxrateId(){
        return stj6TaxrateId;
    }

    private String countryTaxProcessTypeCode;
    public void setCountryTaxProcessTypeCode(String countryTaxProcessTypeCode){
        this.countryTaxProcessTypeCode = countryTaxProcessTypeCode;
    }
    public String getCountryTaxProcessTypeCode(){
        return countryTaxProcessTypeCode;
    }

    private BigDecimal cityTier2TaxableAmt;
    public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt){
        this.cityTier2TaxableAmt = cityTier2TaxableAmt;
    }
    public BigDecimal getCityTier2TaxableAmt(){
        return cityTier2TaxableAmt;
    }

    private BigDecimal aStateTier2TaxAmt;
    public void setAStateTier2TaxAmt(BigDecimal aStateTier2TaxAmt){
        this.aStateTier2TaxAmt = aStateTier2TaxAmt;
    }
    public BigDecimal getAStateTier2TaxAmt(){
        return aStateTier2TaxAmt;
    }

    private BigDecimal stj3Setamt;
    public void setStj3Setamt(BigDecimal stj3Setamt){
        this.stj3Setamt = stj3Setamt;
    }
    public BigDecimal getStj3Setamt(){
        return stj3Setamt;
    }

    private Long stj10TaxrateId;
    public void setStj10TaxrateId(Long stj10TaxrateId){
        this.stj10TaxrateId = stj10TaxrateId;
    }
    public Long getStj10TaxrateId(){
        return stj10TaxrateId;
    }

    private BigDecimal cityTier2MaxAmt;
    public void setCityTier2MaxAmt(BigDecimal cityTier2MaxAmt){
        this.cityTier2MaxAmt = cityTier2MaxAmt;
    }
    public BigDecimal getCityTier2MaxAmt(){
        return cityTier2MaxAmt;
    }

    private BigDecimal aStj9TaxAmt;
    public void setAStj9TaxAmt(BigDecimal aStj9TaxAmt){
        this.aStj9TaxAmt = aStj9TaxAmt;
    }
    public BigDecimal getAStj9TaxAmt(){
        return aStj9TaxAmt;
    }

    private Long stj1TaxcodeDetailId;
    public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId){
        this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
    }
    public Long getStj1TaxcodeDetailId(){
        return stj1TaxcodeDetailId;
    }

    private BigDecimal stj7Setamt;
    public void setStj7Setamt(BigDecimal stj7Setamt){
        this.stj7Setamt = stj7Setamt;
    }
    public BigDecimal getStj7Setamt(){
        return stj7Setamt;
    }

    private String stj5TaxtypeUsedCode;
    public void setStj5TaxtypeUsedCode(String stj5TaxtypeUsedCode){
        this.stj5TaxtypeUsedCode = stj5TaxtypeUsedCode;
    }
    public String getStj5TaxtypeUsedCode(){
        return stj5TaxtypeUsedCode;
    }

    private String stj2TaxcodeOverFlag;
    public void setStj2TaxcodeOverFlag(String stj2TaxcodeOverFlag){
        this.stj2TaxcodeOverFlag = stj2TaxcodeOverFlag;
    }
    public String getStj2TaxcodeOverFlag(){
        return stj2TaxcodeOverFlag;
    }

    private BigDecimal stj10Rate;
    public void setStj10Rate(BigDecimal stj10Rate){
        this.stj10Rate = stj10Rate;
    }
    public BigDecimal getStj10Rate(){
        return stj10Rate;
    }

    private BigDecimal aTbCalcTaxAmt;
    public void setATbCalcTaxAmt(BigDecimal aTbCalcTaxAmt){
        this.aTbCalcTaxAmt = aTbCalcTaxAmt;
    }
    public BigDecimal getATbCalcTaxAmt(){
        return aTbCalcTaxAmt;
    }

    private String logSource;
    public void setLogSource(String logSource){
        this.logSource = logSource;
    }
    public String getLogSource(){
        return logSource;
    }

    private Long billtoLocnMatrixId;
    public void setBilltoLocnMatrixId(Long billtoLocnMatrixId){
        this.billtoLocnMatrixId = billtoLocnMatrixId;
    }
    public Long getBilltoLocnMatrixId(){
        return billtoLocnMatrixId;
    }

    private String countyTaxProcessTypeCode;
    public void setCountyTaxProcessTypeCode(String countyTaxProcessTypeCode){
        this.countyTaxProcessTypeCode = countyTaxProcessTypeCode;
    }
    public String getCountyTaxProcessTypeCode(){
        return countyTaxProcessTypeCode;
    }

    private String cityTaxcodeOverFlag;
    public void setCityTaxcodeOverFlag(String cityTaxcodeOverFlag){
        this.cityTaxcodeOverFlag = cityTaxcodeOverFlag;
    }
    public String getCityTaxcodeOverFlag(){
        return cityTaxcodeOverFlag;
    }

    private Long stj4TaxrateId;
    public void setStj4TaxrateId(Long stj4TaxrateId){
        this.stj4TaxrateId = stj4TaxrateId;
    }
    public Long getStj4TaxrateId(){
        return stj4TaxrateId;
    }

    private BigDecimal countrySpecialRate;
    public void setCountrySpecialRate(BigDecimal countrySpecialRate){
        this.countrySpecialRate = countrySpecialRate;
    }
    public BigDecimal getCountrySpecialRate(){
        return countrySpecialRate;
    }

    private BigDecimal countryTier2MinAmt;
    public void setCountryTier2MinAmt(BigDecimal countryTier2MinAmt){
        this.countryTier2MinAmt = countryTier2MinAmt;
    }
    public BigDecimal getCountryTier2MinAmt(){
        return countryTier2MinAmt;
    }

    private BigDecimal countyTier1MaxAmt;
    public void setCountyTier1MaxAmt(BigDecimal countyTier1MaxAmt){
        this.countyTier1MaxAmt = countyTier1MaxAmt;
    }
    public BigDecimal getCountyTier1MaxAmt(){
        return countyTier1MaxAmt;
    }

    private BigDecimal cityMaximumTaxableAmt;
    public void setCityMaximumTaxableAmt(BigDecimal cityMaximumTaxableAmt){
        this.cityMaximumTaxableAmt = cityMaximumTaxableAmt;
    }
    public BigDecimal getCityMaximumTaxableAmt(){
        return cityMaximumTaxableAmt;
    }

    private BigDecimal countyTier1TaxableAmt;
    public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt){
        this.countyTier1TaxableAmt = countyTier1TaxableAmt;
    }
    public BigDecimal getCountyTier1TaxableAmt(){
        return countyTier1TaxableAmt;
    }

    private BigDecimal countyTier2TaxableAmt;
    public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt){
        this.countyTier2TaxableAmt = countyTier2TaxableAmt;
    }
    public BigDecimal getCountyTier2TaxableAmt(){
        return countyTier2TaxableAmt;
    }

    private String stj8TaxtypeCode;
    public void setStj8TaxtypeCode(String stj8TaxtypeCode){
        this.stj8TaxtypeCode = stj8TaxtypeCode;
    }
    public String getStj8TaxtypeCode(){
        return stj8TaxtypeCode;
    }

    private Long stj6TaxcodeDetailId;
    public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId){
        this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
    }
    public Long getStj6TaxcodeDetailId(){
        return stj6TaxcodeDetailId;
    }

    private Long stj5TaxrateId;
    public void setStj5TaxrateId(Long stj5TaxrateId){
        this.stj5TaxrateId = stj5TaxrateId;
    }
    public Long getStj5TaxrateId(){
        return stj5TaxrateId;
    }

    private Long firstuseJurisdictionId;
    public void setFirstuseJurisdictionId(Long firstuseJurisdictionId){
        this.firstuseJurisdictionId = firstuseJurisdictionId;
    }
    public Long getFirstuseJurisdictionId(){
        return firstuseJurisdictionId;
    }

    private BigDecimal aCityTier1TaxAmt;
    public void setACityTier1TaxAmt(BigDecimal aCityTier1TaxAmt){
        this.aCityTier1TaxAmt = aCityTier1TaxAmt;
    }
    public BigDecimal getACityTier1TaxAmt(){
        return aCityTier1TaxAmt;
    }

    private String stj6TaxtypeCode;
    public void setStj6TaxtypeCode(String stj6TaxtypeCode){
        this.stj6TaxtypeCode = stj6TaxtypeCode;
    }
    public String getStj6TaxtypeCode(){
        return stj6TaxtypeCode;
    }

    private String countryTaxcodeOverFlag;
    public void setCountryTaxcodeOverFlag(String countryTaxcodeOverFlag){
        this.countryTaxcodeOverFlag = countryTaxcodeOverFlag;
    }
    public String getCountryTaxcodeOverFlag(){
        return countryTaxcodeOverFlag;
    }

    private BigDecimal countrySpecialSetamt;
    public void setCountrySpecialSetamt(BigDecimal countrySpecialSetamt){
        this.countrySpecialSetamt = countrySpecialSetamt;
    }
    public BigDecimal getCountrySpecialSetamt(){
        return countrySpecialSetamt;
    }

    private BigDecimal stj6TaxableAmt;
    public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt){
        this.stj6TaxableAmt = stj6TaxableAmt;
    }
    public BigDecimal getStj6TaxableAmt(){
        return stj6TaxableAmt;
    }

    private BigDecimal cityTier2MinAmt;
    public void setCityTier2MinAmt(BigDecimal cityTier2MinAmt){
        this.cityTier2MinAmt = cityTier2MinAmt;
    }
    public BigDecimal getCityTier2MinAmt(){
        return cityTier2MinAmt;
    }

    private BigDecimal distr03Amt;
    public void setDistr03Amt(BigDecimal distr03Amt){
        this.distr03Amt = distr03Amt;
    }
    public BigDecimal getDistr03Amt(){
        return distr03Amt;
    }

    private BigDecimal countryTier3Rate;
    public void setCountryTier3Rate(BigDecimal countryTier3Rate){
        this.countryTier3Rate = countryTier3Rate;
    }
    public BigDecimal getCountryTier3Rate(){
        return countryTier3Rate;
    }

    private BigDecimal countryTier2MaxAmt;
    public void setCountryTier2MaxAmt(BigDecimal countryTier2MaxAmt){
        this.countryTier2MaxAmt = countryTier2MaxAmt;
    }
    public BigDecimal getCountryTier2MaxAmt(){
        return countryTier2MaxAmt;
    }

    private String processingNotes;
    public void setProcessingNotes(String processingNotes){
        this.processingNotes = processingNotes;
    }
    public String getProcessingNotes(){
        return processingNotes;
    }

    private String stj3TaxtypeUsedCode;
    public void setStj3TaxtypeUsedCode(String stj3TaxtypeUsedCode){
        this.stj3TaxtypeUsedCode = stj3TaxtypeUsedCode;
    }
    public String getStj3TaxtypeUsedCode(){
        return stj3TaxtypeUsedCode;
    }

    private BigDecimal stj2TaxableAmt;
    public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt){
        this.stj2TaxableAmt = stj2TaxableAmt;
    }
    public BigDecimal getStj2TaxableAmt(){
        return stj2TaxableAmt;
    }

    private BigDecimal bStj7TaxAmt;
    public void setBStj7TaxAmt(BigDecimal bStj7TaxAmt){
        this.bStj7TaxAmt = bStj7TaxAmt;
    }
    public BigDecimal getBStj7TaxAmt(){
        return bStj7TaxAmt;
    }

    private String modifyUserId;
    public void setModifyUserId(String modifyUserId){
        this.modifyUserId = modifyUserId;
    }
    public String getModifyUserId(){
        return modifyUserId;
    }

    private BigDecimal stj4TaxableAmt;
    public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt){
        this.stj4TaxableAmt = stj4TaxableAmt;
    }
    public BigDecimal getStj4TaxableAmt(){
        return stj4TaxableAmt;
    }

    private BigDecimal stateTier2MinAmt;
    public void setStateTier2MinAmt(BigDecimal stateTier2MinAmt){
        this.stateTier2MinAmt = stateTier2MinAmt;
    }
    public BigDecimal getStateTier2MinAmt(){
        return stateTier2MinAmt;
    }

    private String countryTier2EntamFlag;
    public void setCountryTier2EntamFlag(String countryTier2EntamFlag){
        this.countryTier2EntamFlag = countryTier2EntamFlag;
    }
    public String getCountryTier2EntamFlag(){
        return countryTier2EntamFlag;
    }

    private String stj2Name;
    public void setStj2Name(String stj2Name){
        this.stj2Name = stj2Name;
    }
    public String getStj2Name(){
        return stj2Name;
    }

    private BigDecimal aStj8TaxAmt;
    public void setAStj8TaxAmt(BigDecimal aStj8TaxAmt){
        this.aStj8TaxAmt = aStj8TaxAmt;
    }
    public BigDecimal getAStj8TaxAmt(){
        return aStj8TaxAmt;
    }

    private BigDecimal bCountryTier1TaxAmt;
    public void setBCountryTier1TaxAmt(BigDecimal bCountryTier1TaxAmt){
        this.bCountryTier1TaxAmt = bCountryTier1TaxAmt;
    }
    public BigDecimal getBCountryTier1TaxAmt(){
        return bCountryTier1TaxAmt;
    }

    private Long billtoJurisdictionId;
    public void setBilltoJurisdictionId(Long billtoJurisdictionId){
        this.billtoJurisdictionId = billtoJurisdictionId;
    }
    public Long getBilltoJurisdictionId(){
        return billtoJurisdictionId;
    }

    private Long stj4SitusMatrixId;
    public void setStj4SitusMatrixId(Long stj4SitusMatrixId){
        this.stj4SitusMatrixId = stj4SitusMatrixId;
    }
    public Long getStj4SitusMatrixId(){
        return stj4SitusMatrixId;
    }

    private BigDecimal countryTier2Rate;
    public void setCountryTier2Rate(BigDecimal countryTier2Rate){
        this.countryTier2Rate = countryTier2Rate;
    }
    public BigDecimal getCountryTier2Rate(){
        return countryTier2Rate;
    }

    private BigDecimal bCountyTier3TaxAmt;
    public void setBCountyTier3TaxAmt(BigDecimal bCountyTier3TaxAmt){
        this.bCountyTier3TaxAmt = bCountyTier3TaxAmt;
    }
    public BigDecimal getBCountyTier3TaxAmt(){
        return bCountyTier3TaxAmt;
    }

    private String countyTier2EntamFlag;
    public void setCountyTier2EntamFlag(String countyTier2EntamFlag){
        this.countyTier2EntamFlag = countyTier2EntamFlag;
    }
    public String getCountyTier2EntamFlag(){
        return countyTier2EntamFlag;
    }

    private BigDecimal countyTier2MinAmt;
    public void setCountyTier2MinAmt(BigDecimal countyTier2MinAmt){
        this.countyTier2MinAmt = countyTier2MinAmt;
    }
    public BigDecimal getCountyTier2MinAmt(){
        return countyTier2MinAmt;
    }

    private BigDecimal cityTier1Rate;
    public void setCityTier1Rate(BigDecimal cityTier1Rate){
        this.cityTier1Rate = cityTier1Rate;
    }
    public BigDecimal getCityTier1Rate(){
        return cityTier1Rate;
    }

    private String transactionCountryCode;
    public void setTransactionCountryCode(String transactionCountryCode){
        this.transactionCountryCode = transactionCountryCode;
    }
    public String getTransactionCountryCode(){
        return transactionCountryCode;
    }

    private String countryTaxtypeCode;
    public void setCountryTaxtypeCode(String countryTaxtypeCode){
        this.countryTaxtypeCode = countryTaxtypeCode;
    }
    public String getCountryTaxtypeCode(){
        return countryTaxtypeCode;
    }

    private Long ordrorgnJurisdictionId;
    public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId){
        this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
    }
    public Long getOrdrorgnJurisdictionId(){
        return ordrorgnJurisdictionId;
    }

    private BigDecimal stj6Rate;
    public void setStj6Rate(BigDecimal stj6Rate){
        this.stj6Rate = stj6Rate;
    }
    public BigDecimal getStj6Rate(){
        return stj6Rate;
    }

    private Long stj1TaxrateId;
    public void setStj1TaxrateId(Long stj1TaxrateId){
        this.stj1TaxrateId = stj1TaxrateId;
    }
    public Long getStj1TaxrateId(){
        return stj1TaxrateId;
    }

    private BigDecimal countyMaximumTaxableAmt;
    public void setCountyMaximumTaxableAmt(BigDecimal countyMaximumTaxableAmt){
        this.countyMaximumTaxableAmt = countyMaximumTaxableAmt;
    }
    public BigDecimal getCountyMaximumTaxableAmt(){
        return countyMaximumTaxableAmt;
    }

    private BigDecimal combinedRate;
    public void setCombinedRate(BigDecimal combinedRate){
        this.combinedRate = combinedRate;
    }
    public BigDecimal getCombinedRate(){
        return combinedRate;
    }

    private String shiptoManualJurInd;
    public void setShiptoManualJurInd(String shiptoManualJurInd){
        this.shiptoManualJurInd = shiptoManualJurInd;
    }
    public String getShiptoManualJurInd(){
        return shiptoManualJurInd;
    }

    private String stj6TaxcodeOverFlag;
    public void setStj6TaxcodeOverFlag(String stj6TaxcodeOverFlag){
        this.stj6TaxcodeOverFlag = stj6TaxcodeOverFlag;
    }
    public String getStj6TaxcodeOverFlag(){
        return stj6TaxcodeOverFlag;
    }

    private Long countrySitusMatrixId;
    public void setCountrySitusMatrixId(Long countrySitusMatrixId){
        this.countrySitusMatrixId = countrySitusMatrixId;
    }
    public Long getCountrySitusMatrixId(){
        return countrySitusMatrixId;
    }

    private BigDecimal stj8TaxableAmt;
    public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt){
        this.stj8TaxableAmt = stj8TaxableAmt;
    }
    public BigDecimal getStj8TaxableAmt(){
        return stj8TaxableAmt;
    }

    private String stj1Name;
    public void setStj1Name(String stj1Name){
        this.stj1Name = stj1Name;
    }
    public String getStj1Name(){
        return stj1Name;
    }

    private String stj10TaxcodeOverFlag;
    public void setStj10TaxcodeOverFlag(String stj10TaxcodeOverFlag){
        this.stj10TaxcodeOverFlag = stj10TaxcodeOverFlag;
    }
    public String getStj10TaxcodeOverFlag(){
        return stj10TaxcodeOverFlag;
    }

    private String stj6TaxtypeUsedCode;
    public void setStj6TaxtypeUsedCode(String stj6TaxtypeUsedCode){
        this.stj6TaxtypeUsedCode = stj6TaxtypeUsedCode;
    }
    public String getStj6TaxtypeUsedCode(){
        return stj6TaxtypeUsedCode;
    }

    private String countyName;
    public void setCountyName(String countyName){
        this.countyName = countyName;
    }
    public String getCountyName(){
        return countyName;
    }

    private Long stj7TaxcodeDetailId;
    public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId){
        this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
    }
    public Long getStj7TaxcodeDetailId(){
        return stj7TaxcodeDetailId;
    }

    private BigDecimal aCountryTier2TaxAmt;
    public void setACountryTier2TaxAmt(BigDecimal aCountryTier2TaxAmt){
        this.aCountryTier2TaxAmt = aCountryTier2TaxAmt;
    }
    public BigDecimal getACountryTier2TaxAmt(){
        return aCountryTier2TaxAmt;
    }

    private BigDecimal bStj2TaxAmt;
    public void setBStj2TaxAmt(BigDecimal bStj2TaxAmt){
        this.bStj2TaxAmt = bStj2TaxAmt;
    }
    public BigDecimal getBStj2TaxAmt(){
        return bStj2TaxAmt;
    }

    private Long citySitusMatrixId;
    public void setCitySitusMatrixId(Long citySitusMatrixId){
        this.citySitusMatrixId = citySitusMatrixId;
    }
    public Long getCitySitusMatrixId(){
        return citySitusMatrixId;
    }

    private BigDecimal aCountyTier2TaxAmt;
    public void setACountyTier2TaxAmt(BigDecimal aCountyTier2TaxAmt){
        this.aCountyTier2TaxAmt = aCountyTier2TaxAmt;
    }
    public BigDecimal getACountyTier2TaxAmt(){
        return aCountyTier2TaxAmt;
    }

    private String cityTaxtypeCode;
    public void setCityTaxtypeCode(String cityTaxtypeCode){
        this.cityTaxtypeCode = cityTaxtypeCode;
    }
    public String getCityTaxtypeCode(){
        return cityTaxtypeCode;
    }

    private BigDecimal stateMaximumTaxableAmt;
    public void setStateMaximumTaxableAmt(BigDecimal stateMaximumTaxableAmt){
        this.stateMaximumTaxableAmt = stateMaximumTaxableAmt;
    }
    public BigDecimal getStateMaximumTaxableAmt(){
        return stateMaximumTaxableAmt;
    }

    private BigDecimal stj5Setamt;
    public void setStj5Setamt(BigDecimal stj5Setamt){
        this.stj5Setamt = stj5Setamt;
    }
    public BigDecimal getStj5Setamt(){
        return stj5Setamt;
    }

    private Long countySitusMatrixId;
    public void setCountySitusMatrixId(Long countySitusMatrixId){
        this.countySitusMatrixId = countySitusMatrixId;
    }
    public Long getCountySitusMatrixId(){
        return countySitusMatrixId;
    }

    private String cityTaxtypeUsedCode;
    public void setCityTaxtypeUsedCode(String cityTaxtypeUsedCode){
        this.cityTaxtypeUsedCode = cityTaxtypeUsedCode;
    }
    public String getCityTaxtypeUsedCode(){
        return cityTaxtypeUsedCode;
    }

    private Long countryTaxrateId;
    public void setCountryTaxrateId(Long countryTaxrateId){
        this.countryTaxrateId = countryTaxrateId;
    }
    public Long getCountryTaxrateId(){
        return countryTaxrateId;
    }

    private BigDecimal aCountyLocalTaxAmt;
    public void setACountyLocalTaxAmt(BigDecimal aCountyLocalTaxAmt){
        this.aCountyLocalTaxAmt = aCountyLocalTaxAmt;
    }
    public BigDecimal getACountyLocalTaxAmt(){
        return aCountyLocalTaxAmt;
    }

    private Long stj10TaxcodeDetailId;
    public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId){
        this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
    }
    public Long getStj10TaxcodeDetailId(){
        return stj10TaxcodeDetailId;
    }

    private BigDecimal stj2Setamt;
    public void setStj2Setamt(BigDecimal stj2Setamt){
        this.stj2Setamt = stj2Setamt;
    }
    public BigDecimal getStj2Setamt(){
        return stj2Setamt;
    }

    private Long stj1SitusMatrixId;
    public void setStj1SitusMatrixId(Long stj1SitusMatrixId){
        this.stj1SitusMatrixId = stj1SitusMatrixId;
    }
    public Long getStj1SitusMatrixId(){
        return stj1SitusMatrixId;
    }

    private String stj2TaxtypeUsedCode;
    public void setStj2TaxtypeUsedCode(String stj2TaxtypeUsedCode){
        this.stj2TaxtypeUsedCode = stj2TaxtypeUsedCode;
    }
    public String getStj2TaxtypeUsedCode(){
        return stj2TaxtypeUsedCode;
    }

    private BigDecimal countryTier2Setamt;
    public void setCountryTier2Setamt(BigDecimal countryTier2Setamt){
        this.countryTier2Setamt = countryTier2Setamt;
    }
    public BigDecimal getCountryTier2Setamt(){
        return countryTier2Setamt;
    }

    private BigDecimal stj1Rate;
    public void setStj1Rate(BigDecimal stj1Rate){
        this.stj1Rate = stj1Rate;
    }
    public BigDecimal getStj1Rate(){
        return stj1Rate;
    }

    private BigDecimal stj7Rate;
    public void setStj7Rate(BigDecimal stj7Rate){
        this.stj7Rate = stj7Rate;
    }
    public BigDecimal getStj7Rate(){
        return stj7Rate;
    }

    private String stj1TaxtypeCode;
    public void setStj1TaxtypeCode(String stj1TaxtypeCode){
        this.stj1TaxtypeCode = stj1TaxtypeCode;
    }
    public String getStj1TaxtypeCode(){
        return stj1TaxtypeCode;
    }

    private BigDecimal aCityTier3TaxAmt;
    public void setACityTier3TaxAmt(BigDecimal aCityTier3TaxAmt){
        this.aCityTier3TaxAmt = aCityTier3TaxAmt;
    }
    public BigDecimal getACityTier3TaxAmt(){
        return aCityTier3TaxAmt;
    }

    private String stj5TaxtypeCode;
    public void setStj5TaxtypeCode(String stj5TaxtypeCode){
        this.stj5TaxtypeCode = stj5TaxtypeCode;
    }
    public String getStj5TaxtypeCode(){
        return stj5TaxtypeCode;
    }

    private String stateTier2EntamFlag;
    public void setStateTier2EntamFlag(String stateTier2EntamFlag){
        this.stateTier2EntamFlag = stateTier2EntamFlag;
    }
    public String getStateTier2EntamFlag(){
        return stateTier2EntamFlag;
    }

    private BigDecimal distr07Amt;
    public void setDistr07Amt(BigDecimal distr07Amt){
        this.distr07Amt = distr07Amt;
    }
    public BigDecimal getDistr07Amt(){
        return distr07Amt;
    }

    private String stj8Name;
    public void setStj8Name(String stj8Name){
        this.stj8Name = stj8Name;
    }
    public String getStj8Name(){
        return stj8Name;
    }

    private String stj9TaxtypeCode;
    public void setStj9TaxtypeCode(String stj9TaxtypeCode){
        this.stj9TaxtypeCode = stj9TaxtypeCode;
    }
    public String getStj9TaxtypeCode(){
        return stj9TaxtypeCode;
    }

    private BigDecimal cityTier2Setamt;
    public void setCityTier2Setamt(BigDecimal cityTier2Setamt){
        this.cityTier2Setamt = cityTier2Setamt;
    }
    public BigDecimal getCityTier2Setamt(){
        return cityTier2Setamt;
    }

    private String countryTaxtypeUsedCode;
    public void setCountryTaxtypeUsedCode(String countryTaxtypeUsedCode){
        this.countryTaxtypeUsedCode = countryTaxtypeUsedCode;
    }
    public String getCountryTaxtypeUsedCode(){
        return countryTaxtypeUsedCode;
    }

    private BigDecimal bStateTier3TaxAmt;
    public void setBStateTier3TaxAmt(BigDecimal bStateTier3TaxAmt){
        this.bStateTier3TaxAmt = bStateTier3TaxAmt;
    }
    public BigDecimal getBStateTier3TaxAmt(){
        return bStateTier3TaxAmt;
    }

    private String stj3TaxcodeOverFlag;
    public void setStj3TaxcodeOverFlag(String stj3TaxcodeOverFlag){
        this.stj3TaxcodeOverFlag = stj3TaxcodeOverFlag;
    }
    public String getStj3TaxcodeOverFlag(){
        return stj3TaxcodeOverFlag;
    }

    private BigDecimal aCountryTier3TaxAmt;
    public void setACountryTier3TaxAmt(BigDecimal aCountryTier3TaxAmt){
        this.aCountryTier3TaxAmt = aCountryTier3TaxAmt;
    }
    public BigDecimal getACountryTier3TaxAmt(){
        return aCountryTier3TaxAmt;
    }

    private Long stateSitusMatrixId;
    public void setStateSitusMatrixId(Long stateSitusMatrixId){
        this.stateSitusMatrixId = stateSitusMatrixId;
    }
    public Long getStateSitusMatrixId(){
        return stateSitusMatrixId;
    }

    private Long stj2TaxrateId;
    public void setStj2TaxrateId(Long stj2TaxrateId){
        this.stj2TaxrateId = stj2TaxrateId;
    }
    public Long getStj2TaxrateId(){
        return stj2TaxrateId;
    }

    private BigDecimal countyTier2Rate;
    public void setCountyTier2Rate(BigDecimal countyTier2Rate){
        this.countyTier2Rate = countyTier2Rate;
    }
    public BigDecimal getCountyTier2Rate(){
        return countyTier2Rate;
    }

    private String stj9TaxcodeOverFlag;
    public void setStj9TaxcodeOverFlag(String stj9TaxcodeOverFlag){
        this.stj9TaxcodeOverFlag = stj9TaxcodeOverFlag;
    }
    public String getStj9TaxcodeOverFlag(){
        return stj9TaxcodeOverFlag;
    }

    private String stj7Name;
    public void setStj7Name(String stj7Name){
        this.stj7Name = stj7Name;
    }
    public String getStj7Name(){
        return stj7Name;
    }

    private BigDecimal aStj2TaxAmt;
    public void setAStj2TaxAmt(BigDecimal aStj2TaxAmt){
        this.aStj2TaxAmt = aStj2TaxAmt;
    }
    public BigDecimal getAStj2TaxAmt(){
        return aStj2TaxAmt;
    }

    private Long glExtractBatchNo;
    public void setGlExtractBatchNo(Long glExtractBatchNo){
        this.glExtractBatchNo = glExtractBatchNo;
    }
    public Long getGlExtractBatchNo(){
        return glExtractBatchNo;
    }

    private BigDecimal distr02Amt;
    public void setDistr02Amt(BigDecimal distr02Amt){
        this.distr02Amt = distr02Amt;
    }
    public BigDecimal getDistr02Amt(){
        return distr02Amt;
    }

    private BigDecimal countryMaximumTaxableAmt;
    public void setCountryMaximumTaxableAmt(BigDecimal countryMaximumTaxableAmt){
        this.countryMaximumTaxableAmt = countryMaximumTaxableAmt;
    }
    public BigDecimal getCountryMaximumTaxableAmt(){
        return countryMaximumTaxableAmt;
    }

    private BigDecimal stateMaxtaxAmt;
    public void setStateMaxtaxAmt(BigDecimal stateMaxtaxAmt){
        this.stateMaxtaxAmt = stateMaxtaxAmt;
    }
    public BigDecimal getStateMaxtaxAmt(){
        return stateMaxtaxAmt;
    }

    private Long stj8TaxrateId;
    public void setStj8TaxrateId(Long stj8TaxrateId){
        this.stj8TaxrateId = stj8TaxrateId;
    }
    public Long getStj8TaxrateId(){
        return stj8TaxrateId;
    }

    private BigDecimal cityTier1MaxAmt;
    public void setCityTier1MaxAmt(BigDecimal cityTier1MaxAmt){
        this.cityTier1MaxAmt = cityTier1MaxAmt;
    }
    public BigDecimal getCityTier1MaxAmt(){
        return cityTier1MaxAmt;
    }

    private BigDecimal distr10Amt;
    public void setDistr10Amt(BigDecimal distr10Amt){
        this.distr10Amt = distr10Amt;
    }
    public BigDecimal getDistr10Amt(){
        return distr10Amt;
    }

    private BigDecimal countryMaximumTaxAmt;
    public void setCountryMaximumTaxAmt(BigDecimal countryMaximumTaxAmt){
        this.countryMaximumTaxAmt = countryMaximumTaxAmt;
    }
    public BigDecimal getCountryMaximumTaxAmt(){
        return countryMaximumTaxAmt;
    }

    private BigDecimal cityMinimumTaxableAmt;
    public void setCityMinimumTaxableAmt(BigDecimal cityMinimumTaxableAmt){
        this.cityMinimumTaxableAmt = cityMinimumTaxableAmt;
    }
    public BigDecimal getCityMinimumTaxableAmt(){
        return cityMinimumTaxableAmt;
    }

    private BigDecimal aStateTier3TaxAmt;
    public void setAStateTier3TaxAmt(BigDecimal aStateTier3TaxAmt){
        this.aStateTier3TaxAmt = aStateTier3TaxAmt;
    }
    public BigDecimal getAStateTier3TaxAmt(){
        return aStateTier3TaxAmt;
    }

    private String autoTransactionStateCode;
    public void setAutoTransactionStateCode(String autoTransactionStateCode){
        this.autoTransactionStateCode = autoTransactionStateCode;
    }
    public String getAutoTransactionStateCode(){
        return autoTransactionStateCode;
    }

    private String stj8TaxcodeOverFlag;
    public void setStj8TaxcodeOverFlag(String stj8TaxcodeOverFlag){
        this.stj8TaxcodeOverFlag = stj8TaxcodeOverFlag;
    }
    public String getStj8TaxcodeOverFlag(){
        return stj8TaxcodeOverFlag;
    }

    private Date modifyTimestamp;
    public void setModifyTimestamp(Date modifyTimestamp){
        this.modifyTimestamp = modifyTimestamp;
    }
    public Date getModifyTimestamp(){
        return modifyTimestamp;
    }

    private BigDecimal cityTaxableThresholdAmt;
    public void setCityTaxableThresholdAmt(BigDecimal cityTaxableThresholdAmt){
        this.cityTaxableThresholdAmt = cityTaxableThresholdAmt;
    }
    public BigDecimal getCityTaxableThresholdAmt(){
        return cityTaxableThresholdAmt;
    }

    private BigDecimal cityMaxtaxAmt;
    public void setCityMaxtaxAmt(BigDecimal cityMaxtaxAmt){
        this.cityMaxtaxAmt = cityMaxtaxAmt;
    }
    public BigDecimal getCityMaxtaxAmt(){
        return cityMaxtaxAmt;
    }

    private BigDecimal bCountyTier1TaxAmt;
    public void setBCountyTier1TaxAmt(BigDecimal bCountyTier1TaxAmt){
        this.bCountyTier1TaxAmt = bCountyTier1TaxAmt;
    }
    public BigDecimal getBCountyTier1TaxAmt(){
        return bCountyTier1TaxAmt;
    }

    private BigDecimal stj9Rate;
    public void setStj9Rate(BigDecimal stj9Rate){
        this.stj9Rate = stj9Rate;
    }
    public BigDecimal getStj9Rate(){
        return stj9Rate;
    }

    private BigDecimal stateSpecialRate;
    public void setStateSpecialRate(BigDecimal stateSpecialRate){
        this.stateSpecialRate = stateSpecialRate;
    }
    public BigDecimal getStateSpecialRate(){
        return stateSpecialRate;
    }

    private BigDecimal cityMaximumTaxAmt;
    public void setCityMaximumTaxAmt(BigDecimal cityMaximumTaxAmt){
        this.cityMaximumTaxAmt = cityMaximumTaxAmt;
    }
    public BigDecimal getCityMaximumTaxAmt(){
        return cityMaximumTaxAmt;
    }

    private BigDecimal stateTier1TaxableAmt;
    public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt){
        this.stateTier1TaxableAmt = stateTier1TaxableAmt;
    }
    public BigDecimal getStateTier1TaxableAmt(){
        return stateTier1TaxableAmt;
    }

    private BigDecimal countryTier1TaxableAmt;
    public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt){
        this.countryTier1TaxableAmt = countryTier1TaxableAmt;
    }
    public BigDecimal getCountryTier1TaxableAmt(){
        return countryTier1TaxableAmt;
    }

    private BigDecimal stateTier2TaxableAmt;
    public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt){
        this.stateTier2TaxableAmt = stateTier2TaxableAmt;
    }
    public BigDecimal getStateTier2TaxableAmt(){
        return stateTier2TaxableAmt;
    }

    private String cityTier3EntamFlag;
    public void setCityTier3EntamFlag(String cityTier3EntamFlag){
        this.cityTier3EntamFlag = cityTier3EntamFlag;
    }
    public String getCityTier3EntamFlag(){
        return cityTier3EntamFlag;
    }

    private BigDecimal stateTier3TaxableAmt;
    public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt){
        this.stateTier3TaxableAmt = stateTier3TaxableAmt;
    }
    public BigDecimal getStateTier3TaxableAmt(){
        return stateTier3TaxableAmt;
    }

    private BigDecimal countySpecialRate;
    public void setCountySpecialRate(BigDecimal countySpecialRate){
        this.countySpecialRate = countySpecialRate;
    }
    public BigDecimal getCountySpecialRate(){
        return countySpecialRate;
    }

    private String suspendInd;
    public void setSuspendInd(String suspendInd){
        this.suspendInd = suspendInd;
    }
    public String getSuspendInd(){
        return suspendInd;
    }

    private BigDecimal bTbCalcTaxAmt;
    public void setBTbCalcTaxAmt(BigDecimal bTbCalcTaxAmt){
        this.bTbCalcTaxAmt = bTbCalcTaxAmt;
    }
    public BigDecimal getBTbCalcTaxAmt(){
        return bTbCalcTaxAmt;
    }

    private String cityName;
    public void setCityName(String cityName){
        this.cityName = cityName;
    }
    public String getCityName(){
        return cityName;
    }

    private BigDecimal bStateTier2TaxAmt;
    public void setBStateTier2TaxAmt(BigDecimal bStateTier2TaxAmt){
        this.bStateTier2TaxAmt = bStateTier2TaxAmt;
    }
    public BigDecimal getBStateTier2TaxAmt(){
        return bStateTier2TaxAmt;
    }

    private Long cityTaxcodeDetailId;
    public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId){
        this.cityTaxcodeDetailId = cityTaxcodeDetailId;
    }
    public Long getCityTaxcodeDetailId(){
        return cityTaxcodeDetailId;
    }

    private BigDecimal countryTier3TaxableAmt;
    public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt){
        this.countryTier3TaxableAmt = countryTier3TaxableAmt;
    }
    public BigDecimal getCountryTier3TaxableAmt(){
        return countryTier3TaxableAmt;
    }

    private BigDecimal countryTier2TaxableAmt;
    public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt){
        this.countryTier2TaxableAmt = countryTier2TaxableAmt;
    }
    public BigDecimal getCountryTier2TaxableAmt(){
        return countryTier2TaxableAmt;
    }

    private BigDecimal aStj5TaxAmt;
    public void setAStj5TaxAmt(BigDecimal aStj5TaxAmt){
        this.aStj5TaxAmt = aStj5TaxAmt;
    }
    public BigDecimal getAStj5TaxAmt(){
        return aStj5TaxAmt;
    }

    private BigDecimal bStj5TaxAmt;
    public void setBStj5TaxAmt(BigDecimal bStj5TaxAmt){
        this.bStj5TaxAmt = bStj5TaxAmt;
    }
    public BigDecimal getBStj5TaxAmt(){
        return bStj5TaxAmt;
    }

    private String countryCode;
    public void setCountryCode(String countryCode){
        this.countryCode = countryCode;
    }
    public String getCountryCode(){
        return countryCode;
    }

    private String multiTransCode;
    public void setMultiTransCode(String multiTransCode){
        this.multiTransCode = multiTransCode;
    }
    public String getMultiTransCode(){
        return multiTransCode;
    }

    private BigDecimal stj2Rate;
    public void setStj2Rate(BigDecimal stj2Rate){
        this.stj2Rate = stj2Rate;
    }
    public BigDecimal getStj2Rate(){
        return stj2Rate;
    }

    private BigDecimal stateTier2MaxAmt;
    public void setStateTier2MaxAmt(BigDecimal stateTier2MaxAmt){
        this.stateTier2MaxAmt = stateTier2MaxAmt;
    }
    public BigDecimal getStateTier2MaxAmt(){
        return stateTier2MaxAmt;
    }

    private String comments;
    public void setComments(String comments){
        this.comments = comments;
    }
    public String getComments(){
        return comments;
    }

    private BigDecimal bStj8TaxAmt;
    public void setBStj8TaxAmt(BigDecimal bStj8TaxAmt){
        this.bStj8TaxAmt = bStj8TaxAmt;
    }
    public BigDecimal getBStj8TaxAmt(){
        return bStj8TaxAmt;
    }

    private String countyTaxtypeUsedCode;
    public void setCountyTaxtypeUsedCode(String countyTaxtypeUsedCode){
        this.countyTaxtypeUsedCode = countyTaxtypeUsedCode;
    }
    public String getCountyTaxtypeUsedCode(){
        return countyTaxtypeUsedCode;
    }

    private String transactionInd;
    public void setTransactionInd(String transactionInd){
        this.transactionInd = transactionInd;
    }
    public String getTransactionInd(){
        return transactionInd;
    }

    private BigDecimal stateMinimumTaxableAmt;
    public void setStateMinimumTaxableAmt(BigDecimal stateMinimumTaxableAmt){
        this.stateMinimumTaxableAmt = stateMinimumTaxableAmt;
    }
    public BigDecimal getStateMinimumTaxableAmt(){
        return stateMinimumTaxableAmt;
    }

    private BigDecimal stj4Setamt;
    public void setStj4Setamt(BigDecimal stj4Setamt){
        this.stj4Setamt = stj4Setamt;
    }
    public BigDecimal getStj4Setamt(){
        return stj4Setamt;
    }

    private String stj1TaxtypeUsedCode;
    public void setStj1TaxtypeUsedCode(String stj1TaxtypeUsedCode){
        this.stj1TaxtypeUsedCode = stj1TaxtypeUsedCode;
    }
    public String getStj1TaxtypeUsedCode(){
        return stj1TaxtypeUsedCode;
    }

    private String stj7TaxcodeOverFlag;
    public void setStj7TaxcodeOverFlag(String stj7TaxcodeOverFlag){
        this.stj7TaxcodeOverFlag = stj7TaxcodeOverFlag;
    }
    public String getStj7TaxcodeOverFlag(){
        return stj7TaxcodeOverFlag;
    }

    private Long stj3TaxrateId;
    public void setStj3TaxrateId(Long stj3TaxrateId){
        this.stj3TaxrateId = stj3TaxrateId;
    }
    public Long getStj3TaxrateId(){
        return stj3TaxrateId;
    }

    private BigDecimal bCountryTier2TaxAmt;
    public void setBCountryTier2TaxAmt(BigDecimal bCountryTier2TaxAmt){
        this.bCountryTier2TaxAmt = bCountryTier2TaxAmt;
    }
    public BigDecimal getBCountryTier2TaxAmt(){
        return bCountryTier2TaxAmt;
    }

    private String stj10TaxtypeCode;
    public void setStj10TaxtypeCode(String stj10TaxtypeCode){
        this.stj10TaxtypeCode = stj10TaxtypeCode;
    }
    public String getStj10TaxtypeCode(){
        return stj10TaxtypeCode;
    }

    private BigDecimal stj8Setamt;
    public void setStj8Setamt(BigDecimal stj8Setamt){
        this.stj8Setamt = stj8Setamt;
    }
    public BigDecimal getStj8Setamt(){
        return stj8Setamt;
    }

    private String stj9Name;
    public void setStj9Name(String stj9Name){
        this.stj9Name = stj9Name;
    }
    public String getStj9Name(){
        return stj9Name;
    }

    private String manualTaxcodeInd;
    public void setManualTaxcodeInd(String manualTaxcodeInd){
        this.manualTaxcodeInd = manualTaxcodeInd;
    }
    public String getManualTaxcodeInd(){
        return manualTaxcodeInd;
    }

    private Long ordracptJurisdictionId;
    public void setOrdracptJurisdictionId(Long ordracptJurisdictionId){
        this.ordracptJurisdictionId = ordracptJurisdictionId;
    }
    public Long getOrdracptJurisdictionId(){
        return ordracptJurisdictionId;
    }

    private BigDecimal citySpecialRate;
    public void setCitySpecialRate(BigDecimal citySpecialRate){
        this.citySpecialRate = citySpecialRate;
    }
    public BigDecimal getCitySpecialRate(){
        return citySpecialRate;
    }

    private Long updateUserId;
    public void setUpdateUserId(Long updateUserId){
        this.updateUserId = updateUserId;
    }
    public Long getUpdateUserId(){
        return updateUserId;
    }

    private String stj3Name;
    public void setStj3Name(String stj3Name){
        this.stj3Name = stj3Name;
    }
    public String getStj3Name(){
        return stj3Name;
    }

    private BigDecimal stj10Setamt;
    public void setStj10Setamt(BigDecimal stj10Setamt){
        this.stj10Setamt = stj10Setamt;
    }
    public BigDecimal getStj10Setamt(){
        return stj10Setamt;
    }

    private BigDecimal distr06Amt;
    public void setDistr06Amt(BigDecimal distr06Amt){
        this.distr06Amt = distr06Amt;
    }
    public BigDecimal getDistr06Amt(){
        return distr06Amt;
    }

    private String stj2TaxtypeCode;
    public void setStj2TaxtypeCode(String stj2TaxtypeCode){
        this.stj2TaxtypeCode = stj2TaxtypeCode;
    }
    public String getStj2TaxtypeCode(){
        return stj2TaxtypeCode;
    }

    private BigDecimal stateMaximumTaxAmt;
    public void setStateMaximumTaxAmt(BigDecimal stateMaximumTaxAmt){
        this.stateMaximumTaxAmt = stateMaximumTaxAmt;
    }
    public BigDecimal getStateMaximumTaxAmt(){
        return stateMaximumTaxAmt;
    }

    private String stateTaxtypeCode;
    public void setStateTaxtypeCode(String stateTaxtypeCode){
        this.stateTaxtypeCode = stateTaxtypeCode;
    }
    public String getStateTaxtypeCode(){
        return stateTaxtypeCode;
    }

    private String stj4TaxtypeCode;
    public void setStj4TaxtypeCode(String stj4TaxtypeCode){
        this.stj4TaxtypeCode = stj4TaxtypeCode;
    }
    public String getStj4TaxtypeCode(){
        return stj4TaxtypeCode;
    }

    private Long stj3SitusMatrixId;
    public void setStj3SitusMatrixId(Long stj3SitusMatrixId){
        this.stj3SitusMatrixId = stj3SitusMatrixId;
    }
    public Long getStj3SitusMatrixId(){
        return stj3SitusMatrixId;
    }

    private BigDecimal aCountyTier1TaxAmt;
    public void setACountyTier1TaxAmt(BigDecimal aCountyTier1TaxAmt){
        this.aCountyTier1TaxAmt = aCountyTier1TaxAmt;
    }
    public BigDecimal getACountyTier1TaxAmt(){
        return aCountyTier1TaxAmt;
    }

    private BigDecimal stateTier2Setamt;
    public void setStateTier2Setamt(BigDecimal stateTier2Setamt){
        this.stateTier2Setamt = stateTier2Setamt;
    }
    public BigDecimal getStateTier2Setamt(){
        return stateTier2Setamt;
    }

    private Long ttlxfrLocnMatrixId;
    public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId){
        this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
    }
    public Long getTtlxfrLocnMatrixId(){
        return ttlxfrLocnMatrixId;
    }

    private BigDecimal countyMaximumTaxAmt;
    public void setCountyMaximumTaxAmt(BigDecimal countyMaximumTaxAmt){
        this.countyMaximumTaxAmt = countyMaximumTaxAmt;
    }
    public BigDecimal getCountyMaximumTaxAmt(){
        return countyMaximumTaxAmt;
    }

    private BigDecimal bCityTier1TaxAmt;
    public void setBCityTier1TaxAmt(BigDecimal bCityTier1TaxAmt){
        this.bCityTier1TaxAmt = bCityTier1TaxAmt;
    }
    public BigDecimal getBCityTier1TaxAmt(){
        return bCityTier1TaxAmt;
    }

    private String rejectFlag;
    public void setRejectFlag(String rejectFlag){
        this.rejectFlag = rejectFlag;
    }
    public String getRejectFlag(){
        return rejectFlag;
    }

    private String stateTaxtypeUsedCode;
    public void setStateTaxtypeUsedCode(String stateTaxtypeUsedCode){
        this.stateTaxtypeUsedCode = stateTaxtypeUsedCode;
    }
    public String getStateTaxtypeUsedCode(){
        return stateTaxtypeUsedCode;
    }

    private Long ordracptLocnMatrixId;
    public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId){
        this.ordracptLocnMatrixId = ordracptLocnMatrixId;
    }
    public Long getOrdracptLocnMatrixId(){
        return ordracptLocnMatrixId;
    }

    private BigDecimal stateTier1Rate;
    public void setStateTier1Rate(BigDecimal stateTier1Rate){
        this.stateTier1Rate = stateTier1Rate;
    }
    public BigDecimal getStateTier1Rate(){
        return stateTier1Rate;
    }

    private BigDecimal aStj4TaxAmt;
    public void setAStj4TaxAmt(BigDecimal aStj4TaxAmt){
        this.aStj4TaxAmt = aStj4TaxAmt;
    }
    public BigDecimal getAStj4TaxAmt(){
        return aStj4TaxAmt;
    }

    private BigDecimal stj5TaxableAmt;
    public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt){
        this.stj5TaxableAmt = stj5TaxableAmt;
    }
    public BigDecimal getStj5TaxableAmt(){
        return stj5TaxableAmt;
    }

    private BigDecimal stj7TaxableAmt;
    public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt){
        this.stj7TaxableAmt = stj7TaxableAmt;
    }
    public BigDecimal getStj7TaxableAmt(){
        return stj7TaxableAmt;
    }

    private Long shiptoLocnMatrixId;
    public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId){
        this.shiptoLocnMatrixId = shiptoLocnMatrixId;
    }
    public Long getShiptoLocnMatrixId(){
        return shiptoLocnMatrixId;
    }

    private BigDecimal stj5Rate;
    public void setStj5Rate(BigDecimal stj5Rate){
        this.stj5Rate = stj5Rate;
    }
    public BigDecimal getStj5Rate(){
        return stj5Rate;
    }

    private BigDecimal bStateTier1TaxAmt;
    public void setBStateTier1TaxAmt(BigDecimal bStateTier1TaxAmt){
        this.bStateTier1TaxAmt = bStateTier1TaxAmt;
    }
    public BigDecimal getBStateTier1TaxAmt(){
        return bStateTier1TaxAmt;
    }

    private BigDecimal countySpecialSetamt;
    public void setCountySpecialSetamt(BigDecimal countySpecialSetamt){
        this.countySpecialSetamt = countySpecialSetamt;
    }
    public BigDecimal getCountySpecialSetamt(){
        return countySpecialSetamt;
    }

    private String cityTier2EntamFlag;
    public void setCityTier2EntamFlag(String cityTier2EntamFlag){
        this.cityTier2EntamFlag = cityTier2EntamFlag;
    }
    public String getCityTier2EntamFlag(){
        return cityTier2EntamFlag;
    }

    private BigDecimal aCountyTier3TaxAmt;
    public void setACountyTier3TaxAmt(BigDecimal aCountyTier3TaxAmt){
        this.aCountyTier3TaxAmt = aCountyTier3TaxAmt;
    }
    public BigDecimal getACountyTier3TaxAmt(){
        return aCountyTier3TaxAmt;
    }

    private Long shiptoJurisdictionId;
    public void setShiptoJurisdictionId(Long shiptoJurisdictionId){
        this.shiptoJurisdictionId = shiptoJurisdictionId;
    }
    public Long getShiptoJurisdictionId(){
        return shiptoJurisdictionId;
    }

    private BigDecimal cityTier2Rate;
    public void setCityTier2Rate(BigDecimal cityTier2Rate){
        this.cityTier2Rate = cityTier2Rate;
    }
    public BigDecimal getCityTier2Rate(){
        return cityTier2Rate;
    }

    private BigDecimal stj3TaxableAmt;
    public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt){
        this.stj3TaxableAmt = stj3TaxableAmt;
    }
    public BigDecimal getStj3TaxableAmt(){
        return stj3TaxableAmt;
    }

    private BigDecimal bStj3TaxAmt;
    public void setBStj3TaxAmt(BigDecimal bStj3TaxAmt){
        this.bStj3TaxAmt = bStj3TaxAmt;
    }
    public BigDecimal getBStj3TaxAmt(){
        return bStj3TaxAmt;
    }

    private BigDecimal aCountryTier1TaxAmt;
    public void setACountryTier1TaxAmt(BigDecimal aCountryTier1TaxAmt){
        this.aCountryTier1TaxAmt = aCountryTier1TaxAmt;
    }
    public BigDecimal getACountryTier1TaxAmt(){
        return aCountryTier1TaxAmt;
    }

    private BigDecimal countyMinimumTaxableAmt;
    public void setCountyMinimumTaxableAmt(BigDecimal countyMinimumTaxableAmt){
        this.countyMinimumTaxableAmt = countyMinimumTaxableAmt;
    }
    public BigDecimal getCountyMinimumTaxableAmt(){
        return countyMinimumTaxableAmt;
    }

    private BigDecimal stateTier3Rate;
    public void setStateTier3Rate(BigDecimal stateTier3Rate){
        this.stateTier3Rate = stateTier3Rate;
    }
    public BigDecimal getStateTier3Rate(){
        return stateTier3Rate;
    }

    private String stj4TaxcodeOverFlag;
    public void setStj4TaxcodeOverFlag(String stj4TaxcodeOverFlag){
        this.stj4TaxcodeOverFlag = stj4TaxcodeOverFlag;
    }
    public String getStj4TaxcodeOverFlag(){
        return stj4TaxcodeOverFlag;
    }

    private String taxcodeCode;
    public void setTaxcodeCode(String taxcodeCode){
        this.taxcodeCode = taxcodeCode;
    }
    public String getTaxcodeCode(){
        return taxcodeCode;
    }

    private BigDecimal stj1TaxableAmt;
    public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt){
        this.stj1TaxableAmt = stj1TaxableAmt;
    }
    public BigDecimal getStj1TaxableAmt(){
        return stj1TaxableAmt;
    }

    private BigDecimal distr05Amt;
    public void setDistr05Amt(BigDecimal distr05Amt){
        this.distr05Amt = distr05Amt;
    }
    public BigDecimal getDistr05Amt(){
        return distr05Amt;
    }

    private Long shipfromJurisdictionId;
    public void setShipfromJurisdictionId(Long shipfromJurisdictionId){
        this.shipfromJurisdictionId = shipfromJurisdictionId;
    }
    public Long getShipfromJurisdictionId(){
        return shipfromJurisdictionId;
    }

    private BigDecimal countyMaxtaxAmt;
    public void setCountyMaxtaxAmt(BigDecimal countyMaxtaxAmt){
        this.countyMaxtaxAmt = countyMaxtaxAmt;
    }
    public BigDecimal getCountyMaxtaxAmt(){
        return countyMaxtaxAmt;
    }

    private String calculateInd;
    public void setCalculateInd(String calculateInd){
        this.calculateInd = calculateInd;
    }
    public String getCalculateInd(){
        return calculateInd;
    }

    private BigDecimal aStateTier1TaxAmt;
    public void setAStateTier1TaxAmt(BigDecimal aStateTier1TaxAmt){
        this.aStateTier1TaxAmt = aStateTier1TaxAmt;
    }
    public BigDecimal getAStateTier1TaxAmt(){
        return aStateTier1TaxAmt;
    }

    private Long stj7TaxrateId;
    public void setStj7TaxrateId(Long stj7TaxrateId){
        this.stj7TaxrateId = stj7TaxrateId;
    }
    public Long getStj7TaxrateId(){
        return stj7TaxrateId;
    }

    private BigDecimal cityTier3Rate;
    public void setCityTier3Rate(BigDecimal cityTier3Rate){
        this.cityTier3Rate = cityTier3Rate;
    }
    public BigDecimal getCityTier3Rate(){
        return cityTier3Rate;
    }

    private Long firstuseLocnMatrixId;
    public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId){
        this.firstuseLocnMatrixId = firstuseLocnMatrixId;
    }
    public Long getFirstuseLocnMatrixId(){
        return firstuseLocnMatrixId;
    }

    private BigDecimal distr01Amt;
    public void setDistr01Amt(BigDecimal distr01Amt){
        this.distr01Amt = distr01Amt;
    }
    public BigDecimal getDistr01Amt(){
        return distr01Amt;
    }

    private Long countryVendorNexusDtlId;
    public void setCountryVendorNexusDtlId(Long countryVendorNexusDtlId){
        this.countryVendorNexusDtlId = countryVendorNexusDtlId;
    }
    public Long getCountryVendorNexusDtlId(){
        return countryVendorNexusDtlId;
    }

    private BigDecimal stateTier2Rate;
    public void setStateTier2Rate(BigDecimal stateTier2Rate){
        this.stateTier2Rate = stateTier2Rate;
    }
    public BigDecimal getStateTier2Rate(){
        return stateTier2Rate;
    }

    private String stj5TaxcodeOverFlag;
    public void setStj5TaxcodeOverFlag(String stj5TaxcodeOverFlag){
        this.stj5TaxcodeOverFlag = stj5TaxcodeOverFlag;
    }
    public String getStj5TaxcodeOverFlag(){
        return stj5TaxcodeOverFlag;
    }

    private BigDecimal countryTier3Setamt;
    public void setCountryTier3Setamt(BigDecimal countryTier3Setamt){
        this.countryTier3Setamt = countryTier3Setamt;
    }
    public BigDecimal getCountryTier3Setamt(){
        return countryTier3Setamt;
    }

    private BigDecimal countryTier1Setamt;
    public void setCountryTier1Setamt(BigDecimal countryTier1Setamt){
        this.countryTier1Setamt = countryTier1Setamt;
    }
    public BigDecimal getCountryTier1Setamt(){
        return countryTier1Setamt;
    }

    private Long splitSubtransId;
    public void setSplitSubtransId(Long splitSubtransId){
        this.splitSubtransId = splitSubtransId;
    }
    public Long getSplitSubtransId(){
        return splitSubtransId;
    }

    private Long stj8TaxcodeDetailId;
    public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId){
        this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
    }
    public Long getStj8TaxcodeDetailId(){
        return stj8TaxcodeDetailId;
    }

    private Long ttlxfrJurisdictionId;
    public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId){
        this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
    }
    public Long getTtlxfrJurisdictionId(){
        return ttlxfrJurisdictionId;
    }

    private BigDecimal stj3Rate;
    public void setStj3Rate(BigDecimal stj3Rate){
        this.stj3Rate = stj3Rate;
    }
    public BigDecimal getStj3Rate(){
        return stj3Rate;
    }

    private BigDecimal bCityTier2TaxAmt;
    public void setBCityTier2TaxAmt(BigDecimal bCityTier2TaxAmt){
        this.bCityTier2TaxAmt = bCityTier2TaxAmt;
    }
    public BigDecimal getBCityTier2TaxAmt(){
        return bCityTier2TaxAmt;
    }

    private BigDecimal countyTaxableThresholdAmt;
    public void setCountyTaxableThresholdAmt(BigDecimal countyTaxableThresholdAmt){
        this.countyTaxableThresholdAmt = countyTaxableThresholdAmt;
    }
    public BigDecimal getCountyTaxableThresholdAmt(){
        return countyTaxableThresholdAmt;
    }

    private BigDecimal aCityTier2TaxAmt;
    public void setACityTier2TaxAmt(BigDecimal aCityTier2TaxAmt){
        this.aCityTier2TaxAmt = aCityTier2TaxAmt;
    }
    public BigDecimal getACityTier2TaxAmt(){
        return aCityTier2TaxAmt;
    }

    private BigDecimal countryTier1Rate;
    public void setCountryTier1Rate(BigDecimal countryTier1Rate){
        this.countryTier1Rate = countryTier1Rate;
    }
    public BigDecimal getCountryTier1Rate(){
        return countryTier1Rate;
    }

    private Long cityVendorNexusDtlId;
    public void setCityVendorNexusDtlId(Long cityVendorNexusDtlId){
        this.cityVendorNexusDtlId = cityVendorNexusDtlId;
    }
    public Long getCityVendorNexusDtlId(){
        return cityVendorNexusDtlId;
    }

    private Long stj9TaxcodeDetailId;
    public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId){
        this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
    }
    public Long getStj9TaxcodeDetailId(){
        return stj9TaxcodeDetailId;
    }

    private String countyTaxcodeOverFlag;
    public void setCountyTaxcodeOverFlag(String countyTaxcodeOverFlag){
        this.countyTaxcodeOverFlag = countyTaxcodeOverFlag;
    }
    public String getCountyTaxcodeOverFlag(){
        return countyTaxcodeOverFlag;
    }

    private BigDecimal stateSpecialSetamt;
    public void setStateSpecialSetamt(BigDecimal stateSpecialSetamt){
        this.stateSpecialSetamt = stateSpecialSetamt;
    }
    public BigDecimal getStateSpecialSetamt(){
        return stateSpecialSetamt;
    }

    private Long taxAllocMatrixId;
    public void setTaxAllocMatrixId(Long taxAllocMatrixId){
        this.taxAllocMatrixId = taxAllocMatrixId;
    }
    public Long getTaxAllocMatrixId(){
        return taxAllocMatrixId;
    }

    private BigDecimal stj9TaxableAmt;
    public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt){
        this.stj9TaxableAmt = stj9TaxableAmt;
    }
    public BigDecimal getStj9TaxableAmt(){
        return stj9TaxableAmt;
    }

    private Date glExtractTimestamp;
    public void setGlExtractTimestamp(Date glExtractTimestamp){
        this.glExtractTimestamp = glExtractTimestamp;
    }
    public Date getGlExtractTimestamp(){
        return glExtractTimestamp;
    }

    private Long stj9TaxrateId;
    public void setStj9TaxrateId(Long stj9TaxrateId){
        this.stj9TaxrateId = stj9TaxrateId;
    }
    public Long getStj9TaxrateId(){
        return stj9TaxrateId;
    }

    private Date updateTimestamp;
    public void setUpdateTimestamp(Date updateTimestamp){
        this.updateTimestamp = updateTimestamp;
    }
    public Date getUpdateTimestamp(){
        return updateTimestamp;
    }

    private Long stj5SitusMatrixId;
    public void setStj5SitusMatrixId(Long stj5SitusMatrixId){
        this.stj5SitusMatrixId = stj5SitusMatrixId;
    }
    public Long getStj5SitusMatrixId(){
        return stj5SitusMatrixId;
    }

    private Long stateTaxcodeDetailId;
    public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId){
        this.stateTaxcodeDetailId = stateTaxcodeDetailId;
    }
    public Long getStateTaxcodeDetailId(){
        return stateTaxcodeDetailId;
    }

    private Long taxMatrixId;
    public void setTaxMatrixId(Long taxMatrixId){
        this.taxMatrixId = taxMatrixId;
    }
    public Long getTaxMatrixId(){
        return taxMatrixId;
    }

    private Long stj2SitusMatrixId;
    public void setStj2SitusMatrixId(Long stj2SitusMatrixId){
        this.stj2SitusMatrixId = stj2SitusMatrixId;
    }
    public Long getStj2SitusMatrixId(){
        return stj2SitusMatrixId;
    }

    private BigDecimal countryTier1MaxAmt;
    public void setCountryTier1MaxAmt(BigDecimal countryTier1MaxAmt){
        this.countryTier1MaxAmt = countryTier1MaxAmt;
    }
    public BigDecimal getCountryTier1MaxAmt(){
        return countryTier1MaxAmt;
    }

    private BigDecimal glLineItmDistAmt;
    public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt){
        this.glLineItmDistAmt = glLineItmDistAmt;
    }
    public BigDecimal getGlLineItmDistAmt(){
        return glLineItmDistAmt;
    }

    private String stj1TaxcodeOverFlag;
    public void setStj1TaxcodeOverFlag(String stj1TaxcodeOverFlag){
        this.stj1TaxcodeOverFlag = stj1TaxcodeOverFlag;
    }
    public String getStj1TaxcodeOverFlag(){
        return stj1TaxcodeOverFlag;
    }

    private BigDecimal countyTier2MaxAmt;
    public void setCountyTier2MaxAmt(BigDecimal countyTier2MaxAmt){
        this.countyTier2MaxAmt = countyTier2MaxAmt;
    }
    public BigDecimal getCountyTier2MaxAmt(){
        return countyTier2MaxAmt;
    }

    private BigDecimal aStj1TaxAmt;
    public void setAStj1TaxAmt(BigDecimal aStj1TaxAmt){
        this.aStj1TaxAmt = aStj1TaxAmt;
    }
    public BigDecimal getAStj1TaxAmt(){
        return aStj1TaxAmt;
    }

    private BigDecimal aStj7TaxAmt;
    public void setAStj7TaxAmt(BigDecimal aStj7TaxAmt){
        this.aStj7TaxAmt = aStj7TaxAmt;
    }
    public BigDecimal getAStj7TaxAmt(){
        return aStj7TaxAmt;
    }

    private BigDecimal bCountyLocalTaxAmt;
    public void setBCountyLocalTaxAmt(BigDecimal bCountyLocalTaxAmt){
        this.bCountyLocalTaxAmt = bCountyLocalTaxAmt;
    }
    public BigDecimal getBCountyLocalTaxAmt(){
        return bCountyLocalTaxAmt;
    }

    private BigDecimal aStj10TaxAmt;
    public void setAStj10TaxAmt(BigDecimal aStj10TaxAmt){
        this.aStj10TaxAmt = aStj10TaxAmt;
    }
    public BigDecimal getAStj10TaxAmt(){
        return aStj10TaxAmt;
    }

    private BigDecimal bStj10TaxAmt;
    public void setBStj10TaxAmt(BigDecimal bStj10TaxAmt){
        this.bStj10TaxAmt = bStj10TaxAmt;
    }
    public BigDecimal getBStj10TaxAmt(){
        return bStj10TaxAmt;
    }

    private BigDecimal bCityLocalTaxAmt;
    public void setBCityLocalTaxAmt(BigDecimal bCityLocalTaxAmt){
        this.bCityLocalTaxAmt = bCityLocalTaxAmt;
    }
    public BigDecimal getBCityLocalTaxAmt(){
        return bCityLocalTaxAmt;
    }

    private Long countyTaxrateId;
    public void setCountyTaxrateId(Long countyTaxrateId){
        this.countyTaxrateId = countyTaxrateId;
    }
    public Long getCountyTaxrateId(){
        return countyTaxrateId;
    }

    private BigDecimal stj6Setamt;
    public void setStj6Setamt(BigDecimal stj6Setamt){
        this.stj6Setamt = stj6Setamt;
    }
    public BigDecimal getStj6Setamt(){
        return stj6Setamt;
    }

    private BigDecimal distr09Amt;
    public void setDistr09Amt(BigDecimal distr09Amt){
        this.distr09Amt = distr09Amt;
    }
    public BigDecimal getDistr09Amt(){
        return distr09Amt;
    }

    private Long cityTaxrateId;
    public void setCityTaxrateId(Long cityTaxrateId){
        this.cityTaxrateId = cityTaxrateId;
    }
    public Long getCityTaxrateId(){
        return cityTaxrateId;
    }

    private String stateBaseChangePct;
    public void setStateBaseChangePct(String stateBaseChangePct){
        this.stateBaseChangePct = stateBaseChangePct;
    }
    public String getStateBaseChangePct(){
        return stateBaseChangePct;
    }

    private BigDecimal stj9Setamt;
    public void setStj9Setamt(BigDecimal stj9Setamt){
        this.stj9Setamt = stj9Setamt;
    }
    public BigDecimal getStj9Setamt(){
        return stj9Setamt;
    }

    private BigDecimal stj1Setamt;
    public void setStj1Setamt(BigDecimal stj1Setamt){
        this.stj1Setamt = stj1Setamt;
    }
    public BigDecimal getStj1Setamt(){
        return stj1Setamt;
    }

    private Long stateTaxrateId;
    public void setStateTaxrateId(Long stateTaxrateId){
        this.stateTaxrateId = stateTaxrateId;
    }
    public Long getStateTaxrateId(){
        return stateTaxrateId;
    }

    private String countryTier3EntamFlag;
    public void setCountryTier3EntamFlag(String countryTier3EntamFlag){
        this.countryTier3EntamFlag = countryTier3EntamFlag;
    }
    public String getCountryTier3EntamFlag(){
        return countryTier3EntamFlag;
    }

    private String stateTaxProcessTypeCode;
    public void setStateTaxProcessTypeCode(String stateTaxProcessTypeCode){
        this.stateTaxProcessTypeCode = stateTaxProcessTypeCode;
    }
    public String getStateTaxProcessTypeCode(){
        return stateTaxProcessTypeCode;
    }

    private BigDecimal bCountyTier2TaxAmt;
    public void setBCountyTier2TaxAmt(BigDecimal bCountyTier2TaxAmt){
        this.bCountyTier2TaxAmt = bCountyTier2TaxAmt;
    }
    public BigDecimal getBCountyTier2TaxAmt(){
        return bCountyTier2TaxAmt;
    }

    private String stateTier3EntamFlag;
    public void setStateTier3EntamFlag(String stateTier3EntamFlag){
        this.stateTier3EntamFlag = stateTier3EntamFlag;
    }
    public String getStateTier3EntamFlag(){
        return stateTier3EntamFlag;
    }

    private BigDecimal stj4Rate;
    public void setStj4Rate(BigDecimal stj4Rate){
        this.stj4Rate = stj4Rate;
    }
    public BigDecimal getStj4Rate(){
        return stj4Rate;
    }

    private String countyTaxtypeCode;
    public void setCountyTaxtypeCode(String countyTaxtypeCode){
        this.countyTaxtypeCode = countyTaxtypeCode;
    }
    public String getCountyTaxtypeCode(){
        return countyTaxtypeCode;
    }

    private BigDecimal countyTier2Setamt;
    public void setCountyTier2Setamt(BigDecimal countyTier2Setamt){
        this.countyTier2Setamt = countyTier2Setamt;
    }
    public BigDecimal getCountyTier2Setamt(){
        return countyTier2Setamt;
    }

    private String stateCode;
    public void setStateCode(String stateCode){
        this.stateCode = stateCode;
    }
    public String getStateCode(){
        return stateCode;
    }

    private String stj4TaxtypeUsedCode;
    public void setStj4TaxtypeUsedCode(String stj4TaxtypeUsedCode){
        this.stj4TaxtypeUsedCode = stj4TaxtypeUsedCode;
    }
    public String getStj4TaxtypeUsedCode(){
        return stj4TaxtypeUsedCode;
    }

    private Long stateVendorNexusDtlId;
    public void setStateVendorNexusDtlId(Long stateVendorNexusDtlId){
        this.stateVendorNexusDtlId = stateVendorNexusDtlId;
    }
    public Long getStateVendorNexusDtlId(){
        return stateVendorNexusDtlId;
    }

    private String stateTaxcodeOverFlag;
    public void setStateTaxcodeOverFlag(String stateTaxcodeOverFlag){
        this.stateTaxcodeOverFlag = stateTaxcodeOverFlag;
    }
    public String getStateTaxcodeOverFlag(){
        return stateTaxcodeOverFlag;
    }

    private BigDecimal bCountryTier3TaxAmt;
    public void setBCountryTier3TaxAmt(BigDecimal bCountryTier3TaxAmt){
        this.bCountryTier3TaxAmt = bCountryTier3TaxAmt;
    }
    public BigDecimal getBCountryTier3TaxAmt(){
        return bCountryTier3TaxAmt;
    }

    private String stj5Name;
    public void setStj5Name(String stj5Name){
        this.stj5Name = stj5Name;
    }
    public String getStj5Name(){
        return stj5Name;
    }

    private Long countyVendorNexusDtlId;
    public void setCountyVendorNexusDtlId(Long countyVendorNexusDtlId){
        this.countyVendorNexusDtlId = countyVendorNexusDtlId;
    }
    public Long getCountyVendorNexusDtlId(){
        return countyVendorNexusDtlId;
    }

    private String stj3TaxtypeCode;
    public void setStj3TaxtypeCode(String stj3TaxtypeCode){
        this.stj3TaxtypeCode = stj3TaxtypeCode;
    }
    public String getStj3TaxtypeCode(){
        return stj3TaxtypeCode;
    }

    private Long stj5TaxcodeDetailId;
    public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId){
        this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
    }
    public Long getStj5TaxcodeDetailId(){
        return stj5TaxcodeDetailId;
    }

    private BigDecimal stateTaxableThresholdAmt;
    public void setStateTaxableThresholdAmt(BigDecimal stateTaxableThresholdAmt){
        this.stateTaxableThresholdAmt = stateTaxableThresholdAmt;
    }
    public BigDecimal getStateTaxableThresholdAmt(){
        return stateTaxableThresholdAmt;
    }

    private String cityTaxProcessTypeCode;
    public void setCityTaxProcessTypeCode(String cityTaxProcessTypeCode){
        this.cityTaxProcessTypeCode = cityTaxProcessTypeCode;
    }
    public String getCityTaxProcessTypeCode(){
        return cityTaxProcessTypeCode;
    }

    private BigDecimal bStj6TaxAmt;
    public void setBStj6TaxAmt(BigDecimal bStj6TaxAmt){
        this.bStj6TaxAmt = bStj6TaxAmt;
    }
    public BigDecimal getBStj6TaxAmt(){
        return bStj6TaxAmt;
    }

    private Long countyTaxcodeDetailId;
    public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId){
        this.countyTaxcodeDetailId = countyTaxcodeDetailId;
    }
    public Long getCountyTaxcodeDetailId(){
        return countyTaxcodeDetailId;
    }

    private String stj7TaxtypeCode;
    public void setStj7TaxtypeCode(String stj7TaxtypeCode){
        this.stj7TaxtypeCode = stj7TaxtypeCode;
    }
    public String getStj7TaxtypeCode(){
        return stj7TaxtypeCode;
    }

   
  	
}
