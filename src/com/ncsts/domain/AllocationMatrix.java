package com.ncsts.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CollectionOfElements;

/**
 *  @author Paul Govindan
 */

@Entity
@Table(name="TB_ALLOCATION_MATRIX")
public class AllocationMatrix extends Matrix implements java.io.Serializable {
	
	private static final long serialVersionUID = 8495211375429088594L;
	
	public static final long DRIVER_COUNT = 30L;
	public static final String DRIVER_CODE = "T";
	
    @Id
    @Column(name="ALLOCATION_MATRIX_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="allocation_sequence")
	@SequenceGenerator(name="allocation_sequence" , allocationSize = 1, sequenceName="sq_tb_allocation_matrix_id")	
	private Long allocationMatrixId;
    
    @Column(name="DRIVER_01")
    private String driver01;
    
    @Column(name="DRIVER_02" )
    private String driver02;
    
    @Column(name="DRIVER_03" )
    private String driver03;
    
    @Column(name="DRIVER_04" )
    private String driver04;
    
    @Column(name="DRIVER_05" )
    private String driver05;
    
    @Column(name="DRIVER_06" )
    private String driver06;
    
    @Column(name="DRIVER_07" )
    private String driver07;
    
    @Column(name="DRIVER_08" )
    private String driver08;
    
    @Column(name="DRIVER_09" )
    private String driver09;
    
    @Column(name="DRIVER_10" )
    private String driver10;
    
    @Column(name="DRIVER_11" )
    private String driver11;
    
    @Column(name="DRIVER_12" )
    private String driver12;
    
    @Column(name="DRIVER_13" )
    private String driver13;
    
    @Column(name="DRIVER_14" )
    private String driver14;
    
    @Column(name="DRIVER_15" )
    private String driver15;
        
    @Column(name="DRIVER_16" )
    private String driver16;
    
    @Column(name="DRIVER_17" )
    private String driver17;
   
    @Column(name="DRIVER_18")
    private String driver18;

    @Column(name="DRIVER_19")
    private String driver19;

    @Column(name="DRIVER_20")
    private String driver20;
    
    @Column(name="DRIVER_21")
    private String driver21;

    @Column(name="DRIVER_22")
    private String driver22;

    @Column(name="DRIVER_23")
    private String driver23;

    @Column(name="DRIVER_24")
    private String driver24;
                                
    @Column(name="DRIVER_25")
    private String driver25;

    @Column(name="DRIVER_26")
    private String driver26;

    @Column(name="DRIVER_27")
    private String driver27;
                                
    @Column(name="DRIVER_28")
    private String driver28;

    @Column(name="DRIVER_29")
    private String driver29;

    @Column(name="DRIVER_30")
    private String driver30;
       
    @Column(name="BINARY_WEIGHT")
    private Long binaryWeight;
    
    @Column(name="SIGNIFICANT_DIGITS")
    private String significantDigits;
    
    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
    
    @Column(name="COMMENTS")
    private String comments;
    
    @CollectionOfElements(fetch=FetchType.LAZY)
    @JoinTable(
            name="TB_ALLOCATION_MATRIX_DETAIL",
            joinColumns = @JoinColumn(name="ALLOCATION_MATRIX_ID")
            // uniqueConstraints = @UniqueConstraint(columnNames={"ALLOCATION_MATRIX_ID","JURISDICTION_ID"})
    )
	private List<AllocationMatrixDetail> allocationMatrixDetails = new ArrayList<AllocationMatrixDetail>();
    
  //Midtier project code added - january 2009 
   @OneToMany(mappedBy="allocationMatrix")
    private Set<TransactionDetail> transactionDetail = new HashSet<TransactionDetail>();

    // Constructors

    /** default constructor */
    public AllocationMatrix() {
    }

	/** minimal constructor */
    public AllocationMatrix(Long allocationMatrixId) {
    	this.allocationMatrixId = allocationMatrixId;
    }
    
    // Idable interface
    public Long getId() {
    	return allocationMatrixId;
    }
    
    public void setId(Long id) {
    	this.allocationMatrixId = id;
    }
    
    public String getIdPropertyName() {
    	return "allocationMatrixId";
    }

    // Matrix overrides
    @Override
    public Long getDriverCount() {
    	return DRIVER_COUNT;
    }
    
    @Override
    public String getDriverCode() {
    	return DRIVER_CODE;
    }
    
    @Override
    public boolean getHasDescriptions() {
    	return false;
    }

	public List<AllocationMatrixDetail> getAllocationMatrixDetails() {
		return allocationMatrixDetails;
	}

	public void setAllocationMatrixDetails(
			List<AllocationMatrixDetail> allocationMatrixDetails) {
		this.allocationMatrixDetails = allocationMatrixDetails;
	}

	public Long getBinaryWeight() {
		return binaryWeight;
	}

	public void setBinaryWeight(Long binaryWeight) {
		this.binaryWeight = binaryWeight;
	}

	public String getDriver01() {
		return driver01;
	}

	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}

	public String getDriver02() {
		return driver02;
	}

	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}
	
	public String getDriver03() {
		return driver03;
	}

	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}

	public String getDriver04() {
		return driver04;
	}

	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}

	public String getDriver05() {
		return driver05;
	}

	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}

	public String getDriver06() {
		return driver06;
	}

	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}

	public String getDriver07() {
		return driver07;
	}

	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}

	public String getDriver08() {
		return driver08;
	}

	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}

	public String getDriver09() {
		return driver09;
	}

	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}

	public String getDriver10() {
		return driver10;
	}

	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}

	public String getDriver11() {
		return driver11;
	}

	public void setDriver11(String driver11) {
		this.driver11 = driver11;
	}

	public String getDriver12() {
		return driver12;
	}

	public void setDriver12(String driver12) {
		this.driver12 = driver12;
	}

	public String getDriver13() {
		return driver13;
	}

	public void setDriver13(String driver13) {
		this.driver13 = driver13;
	}

	public String getDriver14() {
		return driver14;
	}

	public void setDriver14(String driver14) {
		this.driver14 = driver14;
	}

	public String getDriver15() {
		return driver15;
	}

	public void setDriver15(String driver15) {
		this.driver15 = driver15;
	}

	public String getDriver16() {
		return driver16;
	}

	public void setDriver16(String driver16) {
		this.driver16 = driver16;
	}

	public String getDriver17() {
		return driver17;
	}

	public void setDriver17(String driver17) {
		this.driver17 = driver17;
	}

	public String getDriver18() {
		return driver18;
	}

	public void setDriver18(String driver18) {
		this.driver18 = driver18;
	}

	public String getDriver19() {
		return driver19;
	}

	public void setDriver19(String driver19) {
		this.driver19 = driver19;
	}

	public String getDriver20() {
		return driver20;
	}

	public void setDriver20(String driver20) {
		this.driver20 = driver20;
	}

	public String getDriver21() {
		return driver21;
	}

	public void setDriver21(String driver21) {
		this.driver21 = driver21;
	}

	public String getDriver22() {
		return driver22;
	}

	public void setDriver22(String driver22) {
		this.driver22 = driver22;
	}

	public String getDriver23() {
		return driver23;
	}

	public void setDriver23(String driver23) {
		this.driver23 = driver23;
	}

	public String getDriver24() {
		return driver24;
	}

	public void setDriver24(String driver24) {
		this.driver24 = driver24;
	}

	public String getDriver25() {
		return driver25;
	}

	public void setDriver25(String driver25) {
		this.driver25 = driver25;
	}

	public String getDriver26() {
		return driver26;
	}

	public void setDriver26(String driver26) {
		this.driver26 = driver26;
	}

	public String getDriver27() {
		return driver27;
	}

	public void setDriver27(String driver27) {
		this.driver27 = driver27;
	}

	public String getDriver28() {
		return driver28;
	}

	public void setDriver28(String driver28) {
		this.driver28 = driver28;
	}

	public String getDriver29() {
		return driver29;
	}

	public void setDriver29(String driver29) {
		this.driver29 = driver29;
	}

	public String getDriver30() {
		return driver30;
	}

	public void setDriver30(String driver30) {
		this.driver30 = driver30;
	}

	public String getSignificantDigits() {
		return significantDigits;
	}

	public void setSignificantDigits(String significantDigits) {
		this.significantDigits = significantDigits;
	}

	public Long getAllocationMatrixId() {
		return allocationMatrixId;
	}

	public void setAllocationMatrixId(Long allocationMatrixId) {
		this.allocationMatrixId = allocationMatrixId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public double getTotalAllocationPercent() {
		double sum = 0;
		if (allocationMatrixDetails != null) {
			for (AllocationMatrixDetail detail : allocationMatrixDetails) {
				if (detail.getAllocationPercent() != null) {
				    sum = sum + detail.getAllocationPercent().doubleValue();	
				}					
			}
		}
		return sum;
	}
	
	public String toString() {
		StringBuffer buff = new StringBuffer();
		if (this.allocationMatrixId != null) buff.append(" id = " + this.allocationMatrixId.toString());
		buff.append("; driver01 = " + this.driver01);
		buff.append("; driver02 = " + this.driver02);
		buff.append("; driver03 = " + this.driver03);
		buff.append("; driver04 = " + this.driver04);
		buff.append("; driver05 = " + this.driver05);
		buff.append("; driver06 = " + this.driver06);
		buff.append("; driver07 = " + this.driver07);
		buff.append("; driver08 = " + this.driver08);
		buff.append("; driver09 = " + this.driver09);
		buff.append("; driver10 = " + this.driver10);
		buff.append("; driver11 = " + this.driver11);
		buff.append("; driver12 = " + this.driver12);	
		buff.append("; driver13 = " + this.driver13);
		buff.append("; driver14 = " + this.driver14);		
		buff.append("; comments = " + this.comments);
		buff.append("; binary weight = " + this.binaryWeight);		
		return buff.toString();
	}

	public Set<TransactionDetail> getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(Set<TransactionDetail> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}
}
