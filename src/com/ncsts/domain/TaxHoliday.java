package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.TaxHolidayDTO;


@Entity
@Table(name="TB_TAX_HOLIDAY")
public class TaxHoliday extends Auditable implements Idable<String>{

	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="TAX_HOLIDAY_CODE")	
    private String taxHolidayCode;
    
    @Column(name="TAXCODE_COUNTRY_CODE")    
    private String taxcodeCountryCode;  
    
    @Column(name="TAXCODE_STATE_CODE")    
    private String taxcodeStateCode;  
    
    @Column(name="DESCRIPTION")    
    private String description;  
    
    @Column(name="NOTES")    
    private String notes;    

    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
  
    @Column(name="TAXCODE_TYPE_CODE")    
    private String taxcodeTypeCode;  
    
    @Column(name="OVERRIDE_TAXTYPE_CODE")    
    private String overrideTaxtypeCode;  
    
    @Column(name="RATETYPE_CODE")    
    private String ratetypeCode;  
    
    @Column(name="TAXABLE_THRESHOLD_AMT")
    private BigDecimal taxableThresholdAmt;
    
    @Column(name="TAXABLE_LIMITATION_AMT")
    private BigDecimal taxableLimitationAmt;
    
    @Column(name="TAX_LIMITATION_AMT")
    private BigDecimal taxLimitationAmt;
    
    @Column(name="CAP_AMT")
    private BigDecimal capAmt;
    
    @Column(name="BASE_CHANGE_PCT")
    private BigDecimal baseChangePct;
    
    @Column(name="SPECIAL_RATE")
    private BigDecimal specialRate;
    
    @Column(name="EXECUTE_FLAG")
    private String executeFlag;
   
    
	@Transient	
	TaxHolidayDTO taxHolidayDTO;
	
	@Transient
	private boolean copyMapFlag;   
	
	
    public boolean getCopyMapFlag() {
		return copyMapFlag;
	}

	public void setCopyMapFlag(boolean copyMapFlag) {
		this.copyMapFlag = copyMapFlag;
	}

	public TaxHoliday() { 	
    }
    
    // Idable interface
	public String getId() {
		return taxHolidayCode;
	}

    public void setId(String id) {
    	this.taxHolidayCode = id;
    }
    
	public String getIdPropertyName() {
    	return "taxHolidayCode";
    }
    
	public TaxHolidayDTO getTaxHolidayDTO() {
		TaxHolidayDTO taxHolidayDTO = new TaxHolidayDTO();
		BeanUtils.copyProperties(this, taxHolidayDTO);
		return taxHolidayDTO;
	}
    
    public String getTaxHolidayCode() {
        return this.taxHolidayCode;
    }
    
    public void setTaxHolidayCode(String taxHolidayCode) {
    	this.taxHolidayCode = taxHolidayCode;
    }
    
    public String getTaxcodeCountryCode() {
        return this.taxcodeCountryCode;
    }
    
    public void setTaxcodeCountryCode(String taxcodeCountryCode) {
    	this.taxcodeCountryCode = taxcodeCountryCode;
    }    
    
    public String getTaxcodeStateCode() {
        return this.taxcodeStateCode;
    }
    
    public void setTaxcodeStateCode(String taxcodeStateCode) {
    	this.taxcodeStateCode = taxcodeStateCode;
    }      
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }    
    
    public String getNotes() {
        return this.notes;
    }
    
    public void setNotes(String notes) {
    	this.notes = notes;
    }  
    
    public String getExecuteFlag() {
		return executeFlag;
	}

	public void setExecuteFlag(String executeFlag) {
		this.executeFlag= executeFlag;
	}

	public Boolean getExecuteBooleanFlag() {
		return (executeFlag == null)? false : executeFlag.equals("1");
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getTaxcodeTypeCode() {
        return this.taxcodeTypeCode;
    }
    
    public void setTaxcodeTypeCode(String taxcodeTypeCode) {
    	this.taxcodeTypeCode = taxcodeTypeCode;
    }
    
    public String getOverrideTaxtypeCode() {
        return this.overrideTaxtypeCode;
    }
    
    public void setOverrideTaxtypeCode(String overrideTaxtypeCode) {
    	this.overrideTaxtypeCode = overrideTaxtypeCode;
    }
    
    public String getRatetypeCode() {
        return this.ratetypeCode;
    }
    
    public void setRatetypeCode(String ratetypeCode) {
    	this.ratetypeCode = ratetypeCode;
    }
    
    public BigDecimal getTaxableThresholdAmt() {
		return this.taxableThresholdAmt;
	}

	public void setTaxableThresholdAmt(BigDecimal taxableThresholdAmt) {
		this.taxableThresholdAmt = taxableThresholdAmt;
	}
	
	public BigDecimal getTaxableLimitationAmt() {
		return taxableLimitationAmt;
	}

	public void setTaxableLimitationAmt(BigDecimal taxableLimitationAmt) {
		this.taxableLimitationAmt = taxableLimitationAmt;
	}
	
	public BigDecimal getTaxLimitationAmt() {
		return this.taxLimitationAmt;
	}

	public void setTaxLimitationAmt(BigDecimal taxLimitationAmt) {
		this.taxLimitationAmt = taxLimitationAmt;
	}
	
	public BigDecimal getCapAmt() {
		return this.capAmt;
	}

	public void setCapAmt(BigDecimal capAmt) {
		this.capAmt = capAmt;
	}
	
	public BigDecimal getBaseChangePct() {
		return this.baseChangePct;
	}

	public void setBaseChangePct(BigDecimal baseChangePct) {
		this.baseChangePct = baseChangePct;
	}
	
	public BigDecimal getSpecialRate() {
		return this.specialRate;
	}

	public void setSpecialRate(BigDecimal specialRate) {
		this.specialRate = specialRate;
	}
    
    
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxHoliday)) {
            return false;
        }

        final TaxHoliday revision = (TaxHoliday)other;

        String c1 = getTaxHolidayCode();
        String c2 = revision.getTaxHolidayCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getTaxHolidayCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.taxHolidayCode + "::" + this.description;
    }
}



