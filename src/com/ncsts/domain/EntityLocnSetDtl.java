package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityLocnSetDTO;
import com.ncsts.dto.EntityLocnSetDtlDTO;

@Entity
@Table(name="TB_ENTITY_LOCN_SET_DTL")
public class EntityLocnSetDtl extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="ENTITY_LOCN_SET_DTL_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="entity_locn_set_dtl_sequence")
	@SequenceGenerator(name="entity_locn_set_dtl_sequence" , allocationSize = 1, sequenceName="sq_tb_entity_locn_set_dtl_id")
    private Long entityLocnSetDtlId;
	
	@Column(name="ENTITY_LOCN_SET_ID")
	private Long entityLocnSetId;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="SF_ENTITY_ID")
	private Long sfEntityId;
	
	@Column(name="POO_ENTITY_ID")
	private Long pooEntityId;
	
	@Column(name="POA_ENTITY_ID")
	private Long poaEntityId;
	
	@Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
	
	@Transient	
	EntityLocnSetDtlDTO entityLocnSetDtlDTO;
	
	public EntityLocnSetDtlDTO getEntityLocnSetDtlDTO() {
		EntityLocnSetDtlDTO entityLocnSetDtlDTO = new EntityLocnSetDtlDTO();
		BeanUtils.copyProperties(this, entityLocnSetDtlDTO);
		return entityLocnSetDtlDTO;
	}
	
	public EntityLocnSetDtl(){ 
    }
	
	public Long getEntityLocnSetDtlId() {
		return entityLocnSetDtlId;
	}

	public void setEntityLocnSetDtlId(Long entityLocnSetDtlId) {
		this.entityLocnSetDtlId = entityLocnSetDtlId;
	}

	public Long getEntityLocnSetId() {
		return entityLocnSetId;
	}

	public void setEntityLocnSetId(Long entityLocnSetId) {
		this.entityLocnSetId = entityLocnSetId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSfEntityId() {
		return sfEntityId;
	}

	public void setSfEntityId(Long sfEntityId) {
		this.sfEntityId = sfEntityId;
	}

	public Long getPooEntityId() {
		return pooEntityId;
	}

	public void setPooEntityId(Long pooEntityId) {
		this.pooEntityId = pooEntityId;
	}
	
	public Long getPoaEntityId() {
		return poaEntityId;
	}

	public void setPoaEntityId(Long poaEntityId) {
		this.poaEntityId = poaEntityId;
	}
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	@Override
	public Long getId() {
		return this.entityLocnSetDtlId;
	}

	@Override
	public void setId(Long id) {
		this.entityLocnSetDtlId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "entityLocnSetDtlId";
	}

}
