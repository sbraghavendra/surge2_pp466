package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the TB_CUST database table.
 * 
 */
@Entity
@Table(name="TB_CUST")
public class Cust extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TB_CUST_CUSTID_GENERATOR" , allocationSize = 1, sequenceName="SQ_TB_CUST_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CUST_CUSTID_GENERATOR")
	@Column(name="CUST_ID")
	private Long custId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	@Column(name="CUST_NAME")
	private String custName;

	@Column(name="CUST_NBR")
	private String custNbr;
	
	@Transient
	private Long entityId;
	
	@Transient
	private String custLocnCode;

	@Transient
	private String custLocnName;

	@Transient
	private String custLocnCountry;

	@Transient
	private String custLocnActiveFlag;
	
	@Transient
	private String custLocnState;
	
	@Transient
	private String lookupFlag;

	public void setCustLocnCountry(String custLocnCountry) {
		this.custLocnCountry = custLocnCountry;
	}

	public String getCustLocnCountry() {
		return this.custLocnCountry;
	}

	public String getCustLocnState() {
		return this.custLocnState;
	}

	public void setCustLocnState(String custLocnState) {
		this.custLocnState = custLocnState;
	}

	public String getCustLocnName() {
		return this.custLocnName;
	}

	public void setCustLocnName(String custLocnName) {
		this.custLocnName = custLocnName;
	}

	public String getCustLocnCode() {
		return this.custLocnCode;
	}

	public void setCustLocnCode(String custLocnCode) {
		this.custLocnCode = custLocnCode;
	}

	public String getCustLocnActiveFlag() {
		return this.custLocnActiveFlag;
	}

	public void setCustLocnActiveFlag(String custLocnActiveFlag) {
		this.custLocnActiveFlag = custLocnActiveFlag;
	}
	
	public String getLookupFlag() {
		return this.lookupFlag;
	}

	public void setLookupFlag(String lookupFlag) {
		this.lookupFlag = lookupFlag;
	}

	//bi-directional many-to-one association to CustEntity
	//@OneToMany(mappedBy="cust")
	//private Set<CustEntity> custEntities;

	//bi-directional many-to-one association to CustLocn
	//@OneToMany(mappedBy="cust")
	//private Set<CustLocn> custLocns;

    public Cust() {
    }
    
    public Long getId() {
		return getCustId();
	}

	public String getIdPropertyName() {
		return "custId";
	}
	
	public void setId(Long id) {
		setCustId(id);
	}

	public Long getCustId() {
		return this.custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNbr() {
		return this.custNbr;
	}

	public void setCustNbr(String custNbr) {
		this.custNbr = custNbr;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getLookupBooleanFlag() {
    	return "1".equals(this.lookupFlag);
	}

	public void setLookupBooleanFlag(Boolean lookupFlag) {
		this.lookupFlag = lookupFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Long getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	//public Set<CustEntity> getCustEntities() {
	//	return this.custEntities;
	//}

	//public void setCustEntities(Set<CustEntity> custEntities) {
	//	this.custEntities = custEntities;
	//}
	
	//public Set<CustLocn> getCustLocns() {
	//	return this.custLocns;
	//}

	//public void setCustLocns(Set<CustLocn> custLocns) {
	//	this.custLocns = custLocns;
	//}
	
}