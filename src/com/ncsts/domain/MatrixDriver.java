package com.ncsts.domain;

import java.util.List;


public class MatrixDriver implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<String> drivers;

    public MatrixDriver(List<String> drivers) {
    	this.drivers = drivers;
    }

	public String getDriver01() {
		return drivers.get(0);
	}

	public String getDriver02() {
		return drivers.get(1);
	}

	public String getDriver03() {
		return drivers.get(2);
	}

	public String getDriver04() {
		return drivers.get(3);
	}

	public String getDriver05() {
		return drivers.get(4);
	}

	public String getDriver06() {
		return drivers.get(5);
	}

	public String getDriver07() {
		return drivers.get(6);
	}

	public String getDriver08() {
		return drivers.get(7);
	}

	public String getDriver09() {
		return drivers.get(8);
	}

	public String getDriver10() {
		return drivers.get(9);
	}

	public String getDriver11() {
		return drivers.get(10);
	}

	public String getDriver12() {
		return drivers.get(11);
	}

	public String getDriver13() {
		return drivers.get(12);
	}

	public String getDriver14() {
		return drivers.get(13);
	}

	public String getDriver15() {
		return drivers.get(14);
	}

	public String getDriver16() {
		return drivers.get(15);
	}

	public String getDriver17() {
		return drivers.get(16);
	}

	public String getDriver18() {
		return drivers.get(17);
	}

	public String getDriver19() {
		return drivers.get(18);
	}

	public String getDriver20() {
		return drivers.get(19);
	}
	
	public String getDriver21() {
		return drivers.get(20);
	}

	public String getDriver22() {
		return drivers.get(21);
	}

	public String getDriver23() {
		return drivers.get(22);
	}

	public String getDriver24() {
		return drivers.get(23);
	}

	public String getDriver25() {
		return drivers.get(24);
	}

	public String getDriver26() {
		return drivers.get(25);
	}

	public String getDriver27() {
		return drivers.get(26);
	}

	public String getDriver28() {
		return drivers.get(27);
	}

	public String getDriver29() {
		return drivers.get(28);
	}

	public String getDriver30() {
		return drivers.get(29);
	}
}
