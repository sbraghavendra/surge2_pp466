package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.EntityDefaultDTO;
import com.ncsts.dto.EntityItemDTO;

@Entity
@Table(name="TB_ENTITY_DEFAULT")
public class EntityDefault extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="ENTITY_DEFAULT_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="entity_default_sequence")
	@SequenceGenerator(name="entity_default_sequence" , allocationSize = 1, sequenceName="sq_tb_entity_default_id")
    private Long entityDefaultId;
	
	@Column(name="ENTITY_ID")
	private Long entityId;
	
	@Column(name="DEFAULT_CODE")
	private String defaultCode;
	
	@Column(name="DEFAULT_VALUE")
	private String defaultValue;
	
	@Transient	
	EntityDefaultDTO entityDefaultDTO;
	
	public EntityDefaultDTO getEntityDefaultDTO() {
		EntityDefaultDTO entityDefaultDTO = new EntityDefaultDTO();
		BeanUtils.copyProperties(this, entityDefaultDTO);
		return entityDefaultDTO;
	}

	public Long getEntityDefaultId() {
		return entityDefaultId;
	}

	public void setEntityDefaultId(Long entityDefaultId) {
		this.entityDefaultId = entityDefaultId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getDefaultCode() {
		return defaultCode;
	}

	public void setDefaultCode(String defaultCode) {
		this.defaultCode = defaultCode;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public Long getId() {
		return this.entityDefaultId;
	}

	@Override
	public void setId(Long id) {
		this.entityDefaultId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "entityDefaultId";
	}
}
