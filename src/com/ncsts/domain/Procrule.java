package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.ncsts.dto.UserEntityDTO;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TB_PROCRULE database table.
 * 
 */
@Entity
@Table(name="TB_PROCRULE")
public class Procrule extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PROCRULE_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="procrule_sequence")
	@SequenceGenerator(name="procrule_sequence" , allocationSize = 1, sequenceName="sq_tb_procrule_id")
	private Long procruleId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="ORDER_NO")
	private Long orderNo;
	
	@Column(name="PROCRULE_NAME")
	private String procruleName;

	@Column(name="MODULE_CODE")
	private String moduleCode;

	@Column(name="RULETYPE_CODE")
	private String ruletypeCode;
	
	@Column(name="REPROCESS_CODE")
	private String reprocessCode;
	
	@Transient	
	private String alwaysFlag;

    public Procrule() {
		reprocessCode = "*NONE";
    }
    
    // Idable interface
 	public Long getId() {
 		return getProcruleId();
 	}
 	
    public void setId(Long id) {
    	setProcruleId(id);
    }
    
    public String getIdPropertyName() {
		return "procruleId";
	}

	public Long getProcruleId() {
		return this.procruleId;
	}

	public void setProcruleId(Long procruleId) {
		this.procruleId = procruleId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public String getReprocessCode() {
		return this.reprocessCode;
	}

	public void setReprocessCode(String reprocessCode) {
		this.reprocessCode = reprocessCode;
	}

	public String getAlwaysFlag() {
		return this.alwaysFlag;
	}

	public void setAlwaysFlag(String alwaysFlag) {
		this.alwaysFlag = alwaysFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	
	public String getProcruleName() {
		return this.procruleName;
	}

	public void setProcruleName(String procruleName) {
		this.procruleName = procruleName;
	}
	
	public String getModuleCode() {
		return this.moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getRuletypeCode() {
		return this.ruletypeCode;
	}

	public void setRuletypeCode(String ruletypeCode) {
		this.ruletypeCode = ruletypeCode;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getAlwaysBooleanFlag() {
    	return "1".equals(this.alwaysFlag);
	}

	public void setAlwaysBooleanFlag(Boolean alwaysFlag) {
		this.alwaysFlag = alwaysFlag == Boolean.TRUE ? "1" : "0";
	}

}