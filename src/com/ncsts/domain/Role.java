package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.RoleDTO;

/**
 * @author Paul Govindan
 *
 */

@Entity
@Table(name="TB_ROLE")
public class Role extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="ROLE_CODE")	
    private String roleCode;
    
    @Column(name="ROLE_NAME")    
    private String roleName;  
    
    @Column(name="GLOBAL_BU_FLAG")    
    private String globalBuFlag; 
    
    @Transient
    private boolean globalBooleanFlag = false;
    
    @Transient
    private boolean activeBooleanFlag = false;
    
    @Column(name="ACTIVE_FLAG")    
    private String activeFlag;  
    
	@Transient	
	RoleDTO roleDTO;
	
	//Midtier project code added - january 2009
	 @OneToMany(mappedBy  = "role")
	 private Set<UserEntity> userEntity = new HashSet<UserEntity>();
	 
	 @OneToMany(mappedBy="role")
	 @Cascade(CascadeType.DELETE_ORPHAN)
	 private Set<RoleMenu> roleMenu = new HashSet<RoleMenu>();
	
    public Role() { 
    	
    }
	public Role(String roleCode,String roleName){
		this.roleCode = roleCode;
		this.roleName = roleName;
	}
    
	public RoleDTO getRoleDTO() {
		RoleDTO roleDTO = new RoleDTO();
		BeanUtils.copyProperties(this, roleDTO);
		return roleDTO;
	}
    
    public String getRoleCode() {
        return this.roleCode;
    }
    
    public void setRoleCode(String roleCode) {
    	this.roleCode = roleCode;
    }
    
    public String getRoleName() {
        return this.roleName;
    }
    
    public void setRoleName(String roleName) {
    	this.roleName = roleName;
    }    
    
    public String getGlobalBuFlag() {
        return this.globalBuFlag;
    }
    
    public void setGlobalBuFlag(String globalBuFlag) {
    	this.globalBuFlag = globalBuFlag;
    }      
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }    
    
	public boolean getGlobalBooleanFlag() {
		if ("1".equalsIgnoreCase(this.globalBuFlag)) return true;		
		return this.globalBooleanFlag;
	}
	
	public void setGlobalBooleanFlag (boolean globalBooleanFlag) {
		this.globalBooleanFlag = globalBooleanFlag;
		if (globalBooleanFlag) {
			this.globalBuFlag = "1";
		} else {
			this.globalBuFlag = "0";
		}
	}
	
	public boolean getActiveBooleanFlag() {
		if ("1".equalsIgnoreCase(this.activeFlag)) return true;
		return this.activeBooleanFlag;
	}
	
	public void setActiveBooleanFlag (boolean activeBooleanFlag) {
		this.activeBooleanFlag = activeBooleanFlag;
		if (activeBooleanFlag) {
			this.activeFlag = "1";
		} else {
			this.activeFlag = "0";
		}		
	}	
	
	 /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof Role)) {
            return false;
        }

        final Role revision = (Role)other;

        String c1 = getRoleCode();
        String c2 = revision.getRoleCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }
        
        String d1 = getRoleName();
        String d2 = revision.getRoleName();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		String c1 = getRoleCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());   
		String dnc1 = getRoleName();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.roleCode + "::" + this.roleName;
    }
	public Set<UserEntity> getUserEntity() {
		return userEntity;
	}
	public void setUserEntity(Set<UserEntity> userEntity) {
		this.userEntity = userEntity;
	}
	public Set<RoleMenu> getRoleMenu() {
		return roleMenu;
	}
	public void setRoleMenu(Set<RoleMenu> roleMenu) {
		this.roleMenu = roleMenu;
	}
    
    
}


