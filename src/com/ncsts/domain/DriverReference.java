package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.DriverReferenceDTO;


@Entity
@Table(name="TB_DRIVER_REFERENCE")
public class DriverReference extends Auditable implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 DriverReferencePK driverReferencePK; 
	
	//@Column(name="TRANS_DTL_COLUMN_NAME",insertable=false,updatable=false)
	//private String transDetailColName;
	
	//@Column(name="DRIVER_VALUE",insertable=false,updatable=false)
	//private String driverValue;
	
	@Column(name="DRIVER_DESCRIPTION")
	private String driverDesc;
	
	@Column(name="USER_VALUE")
	private String userValue;
	
	@Transient	
	DriverReferenceDTO driverReferenceDTO;
	
    public DriverReference() { 
    	
    }
    
    public DriverReference(DriverReferencePK driverReferencePK) { 
    	this.driverReferencePK = driverReferencePK;
    }
	
	/**
	 * @return the driverReferenceDTO
	*/
	public DriverReferenceDTO getDriverReferenceDTO() {
		DriverReferenceDTO driverReferenceDTO = new DriverReferenceDTO();
		BeanUtils.copyProperties(this, driverReferenceDTO);
		
		// Primary key fields need to be copied separately
		driverReferenceDTO.setTransDtlColName(driverReferencePK.getTransDetailColName());
		driverReferenceDTO.setDriverValue(driverReferencePK.getDriverValue());
		
		return driverReferenceDTO;
	}
	/**
	 * @param driverReferenceDTO the driverReferenceDTO to set
	 */
	public void setDriverReferenceDTO(DriverReferenceDTO driverReferenceDTO) {
		this.driverReferenceDTO = driverReferenceDTO;
	}
	
	public DriverReferencePK getDriverReferencePK() {
		return driverReferencePK;
	}

	public void setDriverReferencePK(DriverReferencePK driverReferencePK) {
		this.driverReferencePK = driverReferencePK;
	}

	/**
	 * @return the tranDetailColName
	 */
	public String getTransDetailColName() {
		return driverReferencePK.getTransDetailColName();
	}
	/**
	 * @param tranDetailColName the tranDetailColName to set
	 */
	//public void setTransDetailColName(String transDetailColName) {
	//	this.transDetailColName = transDetailColName;
	//}
	/**
	 * @return the driverValue
	 */
	public String getDriverValue() {
		return driverReferencePK.getDriverValue();
	}
	/**
	 * @param driverValue the driverValue to set
	 */
	//public void setDriverValue(String driverValue) {
	//	this.driverValue = driverValue;
	//}
	/**
	 * @return the driverDesc
	 */
	public String getDriverDesc() {
		return driverDesc;
	}
	/**
	 * @param driverDesc the driverDesc to set
	 */
	public void setDriverDesc(String driverDesc) {
		this.driverDesc = driverDesc;
	}
	/**
	 * @return the userValue
	 */
	public String getUserValue() {
		return userValue;
	}
	/**
	 * @param userValue the userValue to set
	 */
	public void setUserValue(String userValue) {
		this.userValue = userValue;
	}
}
