package com.ncsts.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.ncsts.view.util.ArithmeticUtils;

@Entity
@Table(name="TB_TRANSACTION_DETAIL_LOG")
public class TransactionDetailLog implements java.io.Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Transient
	private Long rownumber;	
	
	@Id
	@Column(name="TRANSACTION_DETAIL_ID")
	private Long transactionDetailId;

	@Column(name="LOG_SOURCE")
	private String logSource;

	@Column(name="GL_EXTRACT_BATCH_NO")
	private Long glExtractBatchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GL_EXTRACT_TIMESTAMP")
	private Date glExtractTimestamp;

	@Column(name="B_COMMENTS")
	private String blogComments;

	@Column(name="B_TB_CALC_TAX_AMT")
	private BigDecimal blogTbCalcTaxAmt;

	@Column(name="B_STATE_USE_AMOUNT")
	private BigDecimal blogStateUseAmount;

	@Column(name="B_STATE_USE_TIER2_AMOUNT")
	private BigDecimal blogStateUseTier2Amount;

	@Column(name="B_STATE_USE_TIER3_AMOUNT")
	private BigDecimal blogStateUseTier3Amount;

	@Column(name="B_COUNTY_USE_AMOUNT")
	private BigDecimal blogCountyUseAmount;

	@Column(name="B_COUNTY_LOCAL_USE_AMOUNT")
	private BigDecimal blogCountyLocalUseAmount;

	@Column(name="B_CITY_USE_AMOUNT")
	private BigDecimal blogCityUseAmount;

	@Column(name="B_CITY_LOCAL_USE_AMOUNT")
	private BigDecimal blogCityLocalUseAmount;

	@Column(name="B_TRANSACTION_STATE_CODE")
	private String blogTransactionStateCode;

	@Column(name="B_AUTO_TRANSACTION_STATE_CODE")
	private String blogAutoTransactionStateCode;

	@Column(name="B_TRANSACTION_IND")
	private String blogTransactionInd;

	@Column(name="B_SUSPEND_IND")
	private String blogSuspendInd;

	@Column(name="B_TAXCODE_DETAIL_ID")
	private Long blogTaxcodeDetailId;

	@Column(name="B_TAXCODE_STATE_CODE")
	private String blogTaxcodeStateCode;

	@Column(name="B_TAXCODE_TYPE_CODE")
	private String blogTaxcodeTypeCode;

	@Column(name="B_TAXCODE_CODE")
	private String blogTaxcodeCode;

	@Column(name="B_MANUAL_TAXCODE_IND")
	private String blogManualTaxcodeInd;

	@Column(name="B_TAX_MATRIX_ID")
	private Long blogTaxMatrixId;

	@Column(name="B_LOCATION_MATRIX_ID")
	private Long blogLocationMatrixId;

	@Column(name="B_JURISDICTION_ID")
	private Long blogJurisdictionId;

	@Column(name="B_JURISDICTION_TAXRATE_ID")
	private Long blogJurisdictionTaxrateId;

	@Column(name="B_MANUAL_JURISDICTION_IND")
	private String blogManualJurisdictionInd;

	@Column(name="B_MEASURE_TYPE_CODE")
	private String blogMeasureTypeCode;

	@Column(name="B_STATE_USE_RATE")
	private BigDecimal blogStateUseRate;

	@Column(name="B_STATE_USE_TIER2_RATE")
	private BigDecimal blogStateUseTier2Rate;

	@Column(name="B_STATE_USE_TIER3_RATE")
	private BigDecimal blogStateUseTier3Rate;

	@Column(name="B_STATE_SPLIT_AMOUNT")
	private BigDecimal blogStateSplitAmount;

	@Column(name="B_STATE_TIER2_MIN_AMOUNT")
	private BigDecimal blogStateTier2MinAmount;

	@Column(name="B_STATE_TIER2_MAX_AMOUNT")
	private BigDecimal blogStateTier2MaxAmount;

	@Column(name="B_STATE_MAXTAX_AMOUNT")
	private BigDecimal blogStateMaxtaxAmount;

	@Column(name="B_COUNTY_USE_RATE")
	private BigDecimal blogCountyUseRate;

	@Column(name="B_COUNTY_LOCAL_USE_RATE")
	private BigDecimal blogCountyLocalUseRate;

	@Column(name="B_COUNTY_SPLIT_AMOUNT")
	private BigDecimal blogCountySplitAmount;

	@Column(name="B_COUNTY_MAXTAX_AMOUNT")
	private BigDecimal blogCountyMaxtaxAmount;

	@Column(name="B_COUNTY_SINGLE_FLAG")
	private String blogCountySingleFlag;

	@Column(name="B_COUNTY_DEFAULT_FLAG")
	private String blogCountyDefaultFlag;

	@Column(name="B_CITY_USE_RATE")
	private BigDecimal blogCityUseRate;

	@Column(name="B_CITY_LOCAL_USE_RATE")
	private BigDecimal blogCityLocalUseRate;

	@Column(name="B_CITY_SPLIT_AMOUNT")
	private BigDecimal blogCitySplitAmount;

	@Column(name="B_CITY_SPLIT_USE_RATE")
	private BigDecimal blogCitySplitUseRate;

	@Column(name="B_CITY_SINGLE_FLAG")
	private String blogCitySingleFlag;

	@Column(name="B_CITY_DEFAULT_FLAG")
	private String blogCityDefaultFlag;

	@Column(name="B_COMBINED_USE_RATE")
	private BigDecimal blogCombinedUseRate;

	@Column(name="B_AUDIT_FLAG")
	private String blogAuditFlag;

	@Column(name="B_AUDIT_USER_ID")
	private String blogAuditUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="B_AUDIT_TIMESTAMP")
	private Date blogAuditTimestamp;

	@Column(name="B_MODIFY_USER_ID")
	private String blogModifyUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="B_MODIFY_TIMESTAMP")
	private Date blogModifyTimestamp;

	@Column(name="B_UPDATE_USER_ID")
	private String blogUpdateUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="B_UPDATE_TIMESTAMP")
	private Date blogUpdateTimestamp;

	@Column(name="B_COUNTRY_USE_AMOUNT")
	private BigDecimal blogCountryUseAmount;

	@Column(name="B_TRANSACTION_COUNTRY_CODE")
	private String blogTransactionCountryCode;

	@Column(name="B_AUTO_TRANS_COUNTRY_CODE")
	private String blogAutoTransCountryCode;

	@Column(name="B_TAXCODE_COUNTRY_CODE")
	private String blogTaxcodeCountryCode;

	@Column(name="B_COUNTRY_USE_RATE")
	private BigDecimal blogCountryUseRate;

	@Column(name="B_SPLIT_SUBTRANS_ID")
	private Long blogSplitSubtransId;

	@Column(name="B_MULTI_TRANS_CODE")
	private String blogMultiTransCode;

	@Column(name="B_TAX_ALLOC_MATRIX_ID")
	private Long blogTaxAllocMatrixId;

	@Column(name="B_STATE_TAXCODE_DETAIL_ID")
	private Long blogStateTaxcodeDetailId;

	@Column(name="B_COUNTY_TAXCODE_DETAIL_ID")
	private Long blogCountyTaxcodeDetailId;

	@Column(name="B_CITY_TAXCODE_DETAIL_ID")
	private Long blogCityTaxcodeDetailId;

	@Column(name="B_TAXTYPE_USED_CODE")
	private String blogTaxtypeUsedCode;

	@Column(name="B_STATE_TAXABLE_AMT")
	private BigDecimal blogStateTaxableAmt;

	@Column(name="B_COUNTY_TAXABLE_AMT")
	private BigDecimal blogCountyTaxableAmt;

	@Column(name="B_CITY_TAXABLE_AMT")
	private BigDecimal blogCityTaxableAmt;

	@Column(name="B_STATE_TAXCODE_TYPE_CODE")
	private String blogStateTaxcodeTypeCode;

	@Column(name="B_COUNTY_TAXCODE_TYPE_CODE")
	private String blogCountyTaxcodeTypeCode;

	@Column(name="B_CITY_TAXCODE_TYPE_CODE")
	private String blogCityTaxcodeTypeCode;

	@Column(name="B_ENTITY_CODE")
	private String blogEntityCode;

	@Column(name="B_STJ1_RATE")
	private BigDecimal blogStj1Rate;

	@Column(name="B_STJ2_RATE")
	private BigDecimal blogStj2Rate;

	@Column(name="B_STJ3_RATE")
	private BigDecimal blogStj3Rate;

	@Column(name="B_STJ4_RATE")
	private BigDecimal blogStj4Rate;

	@Column(name="B_STJ5_RATE")
	private BigDecimal blogStj5Rate;

	@Column(name="B_STJ1_AMOUNT")
	private BigDecimal blogStj1Amount;

	@Column(name="B_STJ2_AMOUNT")
	private BigDecimal blogStj2Amount;

	@Column(name="B_STJ3_AMOUNT")
	private BigDecimal blogStj3Amount;

	@Column(name="B_STJ4_AMOUNT")
	private BigDecimal blogStj4Amount;

	@Column(name="B_STJ5_AMOUNT")
	private BigDecimal blogStj5Amount;

	@Column(name="B_PRODUCT_CODE")
	private String blogProductCode;

	@Column(name="B_REJECT_FLAG")
	private String blogRejectFlag;

	@Column(name="B_STATE_USE_TIER1_SETAMT")
	private BigDecimal blogStateUseTier1Setamt;

	@Column(name="B_STATE_USE_TIER2_SETAMT")
	private BigDecimal blogStateUseTier2Setamt;

	@Column(name="B_STATE_USE_TIER3_SETAMT")
	private BigDecimal blogStateUseTier3Setamt;

	@Column(name="A_COMMENTS")
	private String alogComments;

	@Column(name="A_TB_CALC_TAX_AMT")
	private BigDecimal alogTbCalcTaxAmt;

	@Column(name="A_STATE_USE_AMOUNT")
	private BigDecimal alogStateUseAmount;

	@Column(name="A_STATE_USE_TIER2_AMOUNT")
	private BigDecimal alogStateUseTier2Amount;

	@Column(name="A_STATE_USE_TIER3_AMOUNT")
	private BigDecimal alogStateUseTier3Amount;

	@Column(name="A_COUNTY_USE_AMOUNT")
	private BigDecimal alogCountyUseAmount;

	@Column(name="A_COUNTY_LOCAL_USE_AMOUNT")
	private BigDecimal alogCountyLocalUseAmount;

	@Column(name="A_CITY_USE_AMOUNT")
	private BigDecimal alogCityUseAmount;

	@Column(name="A_CITY_LOCAL_USE_AMOUNT")
	private BigDecimal alogCityLocalUseAmount;

	@Column(name="A_TRANSACTION_STATE_CODE")
	private String alogTransactionStateCode;

	@Column(name="A_AUTO_TRANSACTION_STATE_CODE")
	private String alogAutoTransactionStateCode;

	@Column(name="A_TRANSACTION_IND")
	private String alogTransactionInd;

	@Column(name="A_SUSPEND_IND")
	private String alogSuspendInd;

	@Column(name="A_TAXCODE_DETAIL_ID")
	private Long alogTaxcodeDetailId;

	@Column(name="A_TAXCODE_STATE_CODE")
	private String alogTaxcodeStateCode;

	@Column(name="A_TAXCODE_TYPE_CODE")
	private String alogTaxcodeTypeCode;

	@Column(name="A_TAXCODE_CODE")
	private String alogTaxcodeCode;

	@Column(name="A_MANUAL_TAXCODE_IND")
	private String alogManualTaxcodeInd;

	@Column(name="A_TAX_MATRIX_ID")
	private Long alogTaxMatrixId;

	@Column(name="A_LOCATION_MATRIX_ID")
	private Long alogLocationMatrixId;

	@Column(name="A_JURISDICTION_ID")
	private Long alogJurisdictionId;

	@Column(name="A_JURISDICTION_TAXRATE_ID")
	private Long alogJurisdictionTaxrateId;

	@Column(name="A_MANUAL_JURISDICTION_IND")
	private String alogManualJurisdictionInd;

	@Column(name="A_MEASURE_TYPE_CODE")
	private String alogMeasureTypeCode;

	@Column(name="A_STATE_USE_RATE")
	private BigDecimal alogStateUseRate;

	@Column(name="A_STATE_USE_TIER2_RATE")
	private BigDecimal alogStateUseTier2Rate;

	@Column(name="A_STATE_USE_TIER3_RATE")
	private BigDecimal alogStateUseTier3Rate;

	@Column(name="A_STATE_SPLIT_AMOUNT")
	private BigDecimal alogStateSplitAmount;

	@Column(name="A_STATE_TIER2_MIN_AMOUNT")
	private BigDecimal alogStateTier2MinAmount;

	@Column(name="A_STATE_TIER2_MAX_AMOUNT")
	private BigDecimal alogStateTier2MaxAmount;

	@Column(name="A_STATE_MAXTAX_AMOUNT")
	private BigDecimal alogStateMaxtaxAmount;

	@Column(name="A_COUNTY_USE_RATE")
	private BigDecimal alogCountyUseRate;

	@Column(name="A_COUNTY_LOCAL_USE_RATE")
	private BigDecimal alogCountyLocalUseRate;

	@Column(name="A_COUNTY_SPLIT_AMOUNT")
	private BigDecimal alogCountySplitAmount;

	@Column(name="A_COUNTY_MAXTAX_AMOUNT")
	private BigDecimal alogCountyMaxtaxAmount;

	@Column(name="A_COUNTY_SINGLE_FLAG")
	private String alogCountySingleFlag;

	@Column(name="A_COUNTY_DEFAULT_FLAG")
	private String alogCountyDefaultFlag;

	@Column(name="A_CITY_USE_RATE")
	private BigDecimal alogCityUseRate;

	@Column(name="A_CITY_LOCAL_USE_RATE")
	private BigDecimal alogCityLocalUseRate;

	@Column(name="A_CITY_SPLIT_AMOUNT")
	private BigDecimal alogCitySplitAmount;

	@Column(name="A_CITY_SPLIT_USE_RATE")
	private BigDecimal alogCitySplitUseRate;

	@Column(name="A_CITY_SINGLE_FLAG")
	private String alogCitySingleFlag;

	@Column(name="A_CITY_DEFAULT_FLAG")
	private String alogCityDefaultFlag;

	@Column(name="A_COMBINED_USE_RATE")
	private BigDecimal alogCombinedUseRate;

	@Column(name="A_AUDIT_FLAG")
	private String alogAuditFlag;

	@Column(name="A_AUDIT_USER_ID")
	private String alogAuditUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="A_AUDIT_TIMESTAMP")
	private Date alogAuditTimestamp;

	@Column(name="A_MODIFY_USER_ID")
	private String alogModifyUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="A_MODIFY_TIMESTAMP")
	private Date alogModifyTimestamp;

	@Column(name="A_UPDATE_USER_ID")
	private String alogUpdateUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="A_UPDATE_TIMESTAMP")
	private Date alogUpdateTimestamp;

	@Column(name="A_COUNTRY_USE_AMOUNT")
	private BigDecimal alogCountryUseAmount;

	@Column(name="A_TRANSACTION_COUNTRY_CODE")
	private String alogTransactionCountryCode;

	@Column(name="A_AUTO_TRANS_COUNTRY_CODE")
	private String alogAutoTransCountryCode;

	@Column(name="A_TAXCODE_COUNTRY_CODE")
	private String alogTaxcodeCountryCode;

	@Column(name="A_COUNTRY_USE_RATE")
	private BigDecimal alogCountryUseRate;

	@Column(name="A_SPLIT_SUBTRANS_ID")
	private Long alogSplitSubtransId;

	@Column(name="A_MULTI_TRANS_CODE")
	private String alogMultiTransCode;

	@Column(name="A_TAX_ALLOC_MATRIX_ID")
	private Long alogTaxAllocMatrixId;

	@Column(name="A_STATE_TAXCODE_DETAIL_ID")
	private Long alogStateTaxcodeDetailId;

	@Column(name="A_COUNTY_TAXCODE_DETAIL_ID")
	private Long alogCountyTaxcodeDetailId;

	@Column(name="A_CITY_TAXCODE_DETAIL_ID")
	private Long alogCityTaxcodeDetailId;

	@Column(name="A_TAXTYPE_USED_CODE")
	private String alogTaxtypeUsedCode;

	@Column(name="A_STATE_TAXABLE_AMT")
	private BigDecimal alogStateTaxableAmt;

	@Column(name="A_COUNTY_TAXABLE_AMT")
	private BigDecimal alogCountyTaxableAmt;

	@Column(name="A_CITY_TAXABLE_AMT")
	private BigDecimal alogCityTaxableAmt;

	@Column(name="A_STATE_TAXCODE_TYPE_CODE")
	private String alogStateTaxcodeTypeCode;

	@Column(name="A_COUNTY_TAXCODE_TYPE_CODE")
	private String alogCountyTaxcodeTypeCode;

	@Column(name="A_CITY_TAXCODE_TYPE_CODE")
	private String alogCityTaxcodeTypeCode;

	@Column(name="A_ENTITY_CODE")
	private String alogEntityCode;

	@Column(name="A_STJ1_RATE")
	private BigDecimal alogStj1Rate;

	@Column(name="A_STJ2_RATE")
	private BigDecimal alogStj2Rate;

	@Column(name="A_STJ3_RATE")
	private BigDecimal alogStj3Rate;

	@Column(name="A_STJ4_RATE")
	private BigDecimal alogStj4Rate;

	@Column(name="A_STJ5_RATE")
	private BigDecimal alogStj5Rate;

	@Column(name="A_STJ1_AMOUNT")
	private BigDecimal alogStj1Amount;

	@Column(name="A_STJ2_AMOUNT")
	private BigDecimal alogStj2Amount;

	@Column(name="A_STJ3_AMOUNT")
	private BigDecimal alogStj3Amount;

	@Column(name="A_STJ4_AMOUNT")
	private BigDecimal alogStj4Amount;

	@Column(name="A_STJ5_AMOUNT")
	private BigDecimal alogStj5Amount;

	@Column(name="A_PRODUCT_CODE")
	private String alogProductCode;

	@Column(name="A_REJECT_FLAG")
	private String alogRejectFlag;

	@Column(name="A_STATE_USE_TIER1_SETAMT")
	private BigDecimal alogStateUseTier1Setamt;

	@Column(name="A_STATE_USE_TIER2_SETAMT")
	private BigDecimal alogStateUseTier2Setamt;

	@Column(name="A_STATE_USE_TIER3_SETAMT")
	private BigDecimal alogStateUseTier3Setamt;

	@Column(name="UPDATE_USER_ID")
	private String updateUserId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_TIMESTAMP")
	private Date updateTimestamp;
	
	// Idable interface
	public Long getId() {
		return rownumber;
	}

    public void setId(Long id) {
    	this.rownumber = id;
    }
    
    public Long getRownumber() {
		return rownumber;
	}

	public void setRownumber(Long rownumber) {
		this.rownumber = rownumber;
	}
    
    public String getIdPropertyName() {
    	return "rownumber";
    }
    
    public BigDecimal getTaxAmt(){
    	BigDecimal aTaxAmount = ArithmeticUtils.ZERO;
    	if(alogTbCalcTaxAmt!=null){
    		aTaxAmount = alogTbCalcTaxAmt;
    	}
    	
    	BigDecimal bTaxAmount = ArithmeticUtils.ZERO;
    	if(blogTbCalcTaxAmt!=null){
    		bTaxAmount = blogTbCalcTaxAmt;
    	}
    	
        return ArithmeticUtils.subtract(aTaxAmount, bTaxAmount);
    }

    public Long getTransactionDetailId(){
        return transactionDetailId;
    }

    public void setTransactionDetailId(Long transactionDetailId){
        this.transactionDetailId = transactionDetailId;
    }

    public String getLogSource(){
        return logSource;
    }

    public void setLogSource(String logSource){
        this.logSource = logSource;
    }

    public Long getGlExtractBatchNo(){
        return glExtractBatchNo;
    }

    public void setGlExtractBatchNo(Long glExtractBatchNo){
        this.glExtractBatchNo = glExtractBatchNo;
    }

    public Date getGlExtractTimestamp(){
        return glExtractTimestamp;
    }

    public void setGlExtractTimestamp(Date glExtractTimestamp){
        this.glExtractTimestamp = glExtractTimestamp;
    }

    public String getBlogComments(){
        return blogComments;
    }

    public void setBlogComments(String blogComments){
        this.blogComments = blogComments;
    }

    public BigDecimal getBlogTbCalcTaxAmt(){
        return blogTbCalcTaxAmt;
    }

    public void setBlogTbCalcTaxAmt(BigDecimal blogTbCalcTaxAmt){
        this.blogTbCalcTaxAmt = blogTbCalcTaxAmt;
    }

    public BigDecimal getBlogStateUseAmount(){
        return blogStateUseAmount;
    }

    public void setBlogStateUseAmount(BigDecimal blogStateUseAmount){
        this.blogStateUseAmount = blogStateUseAmount;
    }

    public BigDecimal getBlogStateUseTier2Amount(){
        return blogStateUseTier2Amount;
    }

    public void setBlogStateUseTier2Amount(BigDecimal blogStateUseTier2Amount){
        this.blogStateUseTier2Amount = blogStateUseTier2Amount;
    }

    public BigDecimal getBlogStateUseTier3Amount(){
        return blogStateUseTier3Amount;
    }

    public void setBlogStateUseTier3Amount(BigDecimal blogStateUseTier3Amount){
        this.blogStateUseTier3Amount = blogStateUseTier3Amount;
    }

    public BigDecimal getBlogCountyUseAmount(){
        return blogCountyUseAmount;
    }

    public void setBlogCountyUseAmount(BigDecimal blogCountyUseAmount){
        this.blogCountyUseAmount = blogCountyUseAmount;
    }

    public BigDecimal getBlogCountyLocalUseAmount(){
        return blogCountyLocalUseAmount;
    }

    public void setBlogCountyLocalUseAmount(BigDecimal blogCountyLocalUseAmount){
        this.blogCountyLocalUseAmount = blogCountyLocalUseAmount;
    }

    public BigDecimal getBlogCityUseAmount(){
        return blogCityUseAmount;
    }

    public void setBlogCityUseAmount(BigDecimal blogCityUseAmount){
        this.blogCityUseAmount = blogCityUseAmount;
    }

    public BigDecimal getBlogCityLocalUseAmount(){
        return blogCityLocalUseAmount;
    }

    public void setBlogCityLocalUseAmount(BigDecimal blogCityLocalUseAmount){
        this.blogCityLocalUseAmount = blogCityLocalUseAmount;
    }

    public String getBlogTransactionStateCode(){
        return blogTransactionStateCode;
    }

    public void setBlogTransactionStateCode(String blogTransactionStateCode){
        this.blogTransactionStateCode = blogTransactionStateCode;
    }

    public String getBlogAutoTransactionStateCode(){
        return blogAutoTransactionStateCode;
    }

    public void setBlogAutoTransactionStateCode(String blogAutoTransactionStateCode){
        this.blogAutoTransactionStateCode = blogAutoTransactionStateCode;
    }

    public String getBlogTransactionInd(){
        return blogTransactionInd;
    }

    public void setBlogTransactionInd(String blogTransactionInd){
        this.blogTransactionInd = blogTransactionInd;
    }

    public String getBlogSuspendInd(){
        return blogSuspendInd;
    }

    public void setBlogSuspendInd(String blogSuspendInd){
        this.blogSuspendInd = blogSuspendInd;
    }

    public Long getBlogTaxcodeDetailId(){
        return blogTaxcodeDetailId;
    }

    public void setBlogTaxcodeDetailId(Long blogTaxcodeDetailId){
        this.blogTaxcodeDetailId = blogTaxcodeDetailId;
    }

    public String getBlogTaxcodeStateCode(){
        return blogTaxcodeStateCode;
    }

    public void setBlogTaxcodeStateCode(String blogTaxcodeStateCode){
        this.blogTaxcodeStateCode = blogTaxcodeStateCode;
    }

    public String getBlogTaxcodeTypeCode(){
        return blogTaxcodeTypeCode;
    }

    public void setBlogTaxcodeTypeCode(String blogTaxcodeTypeCode){
        this.blogTaxcodeTypeCode = blogTaxcodeTypeCode;
    }

    public String getBlogTaxcodeCode(){
        return blogTaxcodeCode;
    }

    public void setBlogTaxcodeCode(String blogTaxcodeCode){
        this.blogTaxcodeCode = blogTaxcodeCode;
    }

    public String getBlogManualTaxcodeInd(){
        return blogManualTaxcodeInd;
    }

    public void setBlogManualTaxcodeInd(String blogManualTaxcodeInd){
        this.blogManualTaxcodeInd = blogManualTaxcodeInd;
    }

    public Long getBlogTaxMatrixId(){
        return blogTaxMatrixId;
    }

    public void setBlogTaxMatrixId(Long blogTaxMatrixId){
        this.blogTaxMatrixId = blogTaxMatrixId;
    }

    public Long getBlogLocationMatrixId(){
        return blogLocationMatrixId;
    }

    public void setBlogLocationMatrixId(Long blogLocationMatrixId){
        this.blogLocationMatrixId = blogLocationMatrixId;
    }

    public Long getBlogJurisdictionId(){
        return blogJurisdictionId;
    }

    public void setBlogJurisdictionId(Long blogJurisdictionId){
        this.blogJurisdictionId = blogJurisdictionId;
    }

    public Long getBlogJurisdictionTaxrateId(){
        return blogJurisdictionTaxrateId;
    }

    public void setBlogJurisdictionTaxrateId(Long blogJurisdictionTaxrateId){
        this.blogJurisdictionTaxrateId = blogJurisdictionTaxrateId;
    }

    public String getBlogManualJurisdictionInd(){
        return blogManualJurisdictionInd;
    }

    public void setBlogManualJurisdictionInd(String blogManualJurisdictionInd){
        this.blogManualJurisdictionInd = blogManualJurisdictionInd;
    }

    public String getBlogMeasureTypeCode(){
        return blogMeasureTypeCode;
    }

    public void setBlogMeasureTypeCode(String blogMeasureTypeCode){
        this.blogMeasureTypeCode = blogMeasureTypeCode;
    }

    public BigDecimal getBlogStateUseRate(){
        return blogStateUseRate;
    }

    public void setBlogStateUseRate(BigDecimal blogStateUseRate){
        this.blogStateUseRate = blogStateUseRate;
    }

    public BigDecimal getBlogStateUseTier2Rate(){
        return blogStateUseTier2Rate;
    }

    public void setBlogStateUseTier2Rate(BigDecimal blogStateUseTier2Rate){
        this.blogStateUseTier2Rate = blogStateUseTier2Rate;
    }

    public BigDecimal getBlogStateUseTier3Rate(){
        return blogStateUseTier3Rate;
    }

    public void setBlogStateUseTier3Rate(BigDecimal blogStateUseTier3Rate){
        this.blogStateUseTier3Rate = blogStateUseTier3Rate;
    }

    public BigDecimal getBlogStateSplitAmount(){
        return blogStateSplitAmount;
    }

    public void setBlogStateSplitAmount(BigDecimal blogStateSplitAmount){
        this.blogStateSplitAmount = blogStateSplitAmount;
    }

    public BigDecimal getBlogStateTier2MinAmount(){
        return blogStateTier2MinAmount;
    }

    public void setBlogStateTier2MinAmount(BigDecimal blogStateTier2MinAmount){
        this.blogStateTier2MinAmount = blogStateTier2MinAmount;
    }

    public BigDecimal getBlogStateTier2MaxAmount(){
        return blogStateTier2MaxAmount;
    }

    public void setBlogStateTier2MaxAmount(BigDecimal blogStateTier2MaxAmount){
        this.blogStateTier2MaxAmount = blogStateTier2MaxAmount;
    }

    public BigDecimal getBlogStateMaxtaxAmount(){
        return blogStateMaxtaxAmount;
    }

    public void setBlogStateMaxtaxAmount(BigDecimal blogStateMaxtaxAmount){
        this.blogStateMaxtaxAmount = blogStateMaxtaxAmount;
    }

    public BigDecimal getBlogCountyUseRate(){
        return blogCountyUseRate;
    }

    public void setBlogCountyUseRate(BigDecimal blogCountyUseRate){
        this.blogCountyUseRate = blogCountyUseRate;
    }

    public BigDecimal getBlogCountyLocalUseRate(){
        return blogCountyLocalUseRate;
    }

    public void setBlogCountyLocalUseRate(BigDecimal blogCountyLocalUseRate){
        this.blogCountyLocalUseRate = blogCountyLocalUseRate;
    }

    public BigDecimal getBlogCountySplitAmount(){
        return blogCountySplitAmount;
    }

    public void setBlogCountySplitAmount(BigDecimal blogCountySplitAmount){
        this.blogCountySplitAmount = blogCountySplitAmount;
    }

    public BigDecimal getBlogCountyMaxtaxAmount(){
        return blogCountyMaxtaxAmount;
    }

    public void setBlogCountyMaxtaxAmount(BigDecimal blogCountyMaxtaxAmount){
        this.blogCountyMaxtaxAmount = blogCountyMaxtaxAmount;
    }

    public String getBlogCountySingleFlag(){
        return blogCountySingleFlag;
    }

    public void setBlogCountySingleFlag(String blogCountySingleFlag){
        this.blogCountySingleFlag = blogCountySingleFlag;
    }

    public String getBlogCountyDefaultFlag(){
        return blogCountyDefaultFlag;
    }

    public void setBlogCountyDefaultFlag(String blogCountyDefaultFlag){
        this.blogCountyDefaultFlag = blogCountyDefaultFlag;
    }

    public BigDecimal getBlogCityUseRate(){
        return blogCityUseRate;
    }

    public void setBlogCityUseRate(BigDecimal blogCityUseRate){
        this.blogCityUseRate = blogCityUseRate;
    }

    public BigDecimal getBlogCityLocalUseRate(){
        return blogCityLocalUseRate;
    }

    public void setBlogCityLocalUseRate(BigDecimal blogCityLocalUseRate){
        this.blogCityLocalUseRate = blogCityLocalUseRate;
    }

    public BigDecimal getBlogCitySplitAmount(){
        return blogCitySplitAmount;
    }

    public void setBlogCitySplitAmount(BigDecimal blogCitySplitAmount){
        this.blogCitySplitAmount = blogCitySplitAmount;
    }

    public BigDecimal getBlogCitySplitUseRate(){
        return blogCitySplitUseRate;
    }

    public void setBlogCitySplitUseRate(BigDecimal blogCitySplitUseRate){
        this.blogCitySplitUseRate = blogCitySplitUseRate;
    }

    public String getBlogCitySingleFlag(){
        return blogCitySingleFlag;
    }

    public void setBlogCitySingleFlag(String blogCitySingleFlag){
        this.blogCitySingleFlag = blogCitySingleFlag;
    }

    public String getBlogCityDefaultFlag(){
        return blogCityDefaultFlag;
    }

    public void setBlogCityDefaultFlag(String blogCityDefaultFlag){
        this.blogCityDefaultFlag = blogCityDefaultFlag;
    }

    public BigDecimal getBlogCombinedUseRate(){
        return blogCombinedUseRate;
    }

    public void setBlogCombinedUseRate(BigDecimal blogCombinedUseRate){
        this.blogCombinedUseRate = blogCombinedUseRate;
    }

    public String getBlogAuditFlag(){
        return blogAuditFlag;
    }

    public void setBlogAuditFlag(String blogAuditFlag){
        this.blogAuditFlag = blogAuditFlag;
    }

    public String getBlogAuditUserId(){
        return blogAuditUserId;
    }

    public void setBlogAuditUserId(String blogAuditUserId){
        this.blogAuditUserId = blogAuditUserId;
    }

    public Date getBlogAuditTimestamp(){
        return blogAuditTimestamp;
    }

    public void setBlogAuditTimestamp(Date blogAuditTimestamp){
        this.blogAuditTimestamp = blogAuditTimestamp;
    }

    public String getBlogModifyUserId(){
        return blogModifyUserId;
    }

    public void setBlogModifyUserId(String blogModifyUserId){
        this.blogModifyUserId = blogModifyUserId;
    }

    public Date getBlogModifyTimestamp(){
        return blogModifyTimestamp;
    }

    public void setBlogModifyTimestamp(Date blogModifyTimestamp){
        this.blogModifyTimestamp = blogModifyTimestamp;
    }

    public String getBlogUpdateUserId(){
        return blogUpdateUserId;
    }

    public void setBlogUpdateUserId(String blogUpdateUserId){
        this.blogUpdateUserId = blogUpdateUserId;
    }

    public Date getBlogUpdateTimestamp(){
        return blogUpdateTimestamp;
    }

    public void setBlogUpdateTimestamp(Date blogUpdateTimestamp){
        this.blogUpdateTimestamp = blogUpdateTimestamp;
    }

    public BigDecimal getBlogCountryUseAmount(){
        return blogCountryUseAmount;
    }

    public void setBlogCountryUseAmount(BigDecimal blogCountryUseAmount){
        this.blogCountryUseAmount = blogCountryUseAmount;
    }

    public String getBlogTransactionCountryCode(){
        return blogTransactionCountryCode;
    }

    public void setBlogTransactionCountryCode(String blogTransactionCountryCode){
        this.blogTransactionCountryCode = blogTransactionCountryCode;
    }

    public String getBlogAutoTransCountryCode(){
        return blogAutoTransCountryCode;
    }

    public void setBlogAutoTransCountryCode(String blogAutoTransCountryCode){
        this.blogAutoTransCountryCode = blogAutoTransCountryCode;
    }

    public String getBlogTaxcodeCountryCode(){
        return blogTaxcodeCountryCode;
    }

    public void setBlogTaxcodeCountryCode(String blogTaxcodeCountryCode){
        this.blogTaxcodeCountryCode = blogTaxcodeCountryCode;
    }

    public BigDecimal getBlogCountryUseRate(){
        return blogCountryUseRate;
    }

    public void setBlogCountryUseRate(BigDecimal blogCountryUseRate){
        this.blogCountryUseRate = blogCountryUseRate;
    }

    public Long getBlogSplitSubtransId(){
        return blogSplitSubtransId;
    }

    public void setBlogSplitSubtransId(Long blogSplitSubtransId){
        this.blogSplitSubtransId = blogSplitSubtransId;
    }

    public String getBlogMultiTransCode(){
        return blogMultiTransCode;
    }

    public void setBlogMultiTransCode(String blogMultiTransCode){
        this.blogMultiTransCode = blogMultiTransCode;
    }

    public Long getBlogTaxAllocMatrixId(){
        return blogTaxAllocMatrixId;
    }

    public void setBlogTaxAllocMatrixId(Long blogTaxAllocMatrixId){
        this.blogTaxAllocMatrixId = blogTaxAllocMatrixId;
    }

    public Long getBlogStateTaxcodeDetailId(){
        return blogStateTaxcodeDetailId;
    }

    public void setBlogStateTaxcodeDetailId(Long blogStateTaxcodeDetailId){
        this.blogStateTaxcodeDetailId = blogStateTaxcodeDetailId;
    }

    public Long getBlogCountyTaxcodeDetailId(){
        return blogCountyTaxcodeDetailId;
    }

    public void setBlogCountyTaxcodeDetailId(Long blogCountyTaxcodeDetailId){
        this.blogCountyTaxcodeDetailId = blogCountyTaxcodeDetailId;
    }

    public Long getBlogCityTaxcodeDetailId(){
        return blogCityTaxcodeDetailId;
    }

    public void setBlogCityTaxcodeDetailId(Long blogCityTaxcodeDetailId){
        this.blogCityTaxcodeDetailId = blogCityTaxcodeDetailId;
    }

    public String getBlogTaxtypeUsedCode(){
        return blogTaxtypeUsedCode;
    }

    public void setBlogTaxtypeUsedCode(String blogTaxtypeUsedCode){
        this.blogTaxtypeUsedCode = blogTaxtypeUsedCode;
    }

    public BigDecimal getBlogStateTaxableAmt(){
        return blogStateTaxableAmt;
    }

    public void setBlogStateTaxableAmt(BigDecimal blogStateTaxableAmt){
        this.blogStateTaxableAmt = blogStateTaxableAmt;
    }

    public BigDecimal getBlogCountyTaxableAmt(){
        return blogCountyTaxableAmt;
    }

    public void setBlogCountyTaxableAmt(BigDecimal blogCountyTaxableAmt){
        this.blogCountyTaxableAmt = blogCountyTaxableAmt;
    }

    public BigDecimal getBlogCityTaxableAmt(){
        return blogCityTaxableAmt;
    }

    public void setBlogCityTaxableAmt(BigDecimal blogCityTaxableAmt){
        this.blogCityTaxableAmt = blogCityTaxableAmt;
    }

    public String getBlogStateTaxcodeTypeCode(){
        return blogStateTaxcodeTypeCode;
    }

    public void setBlogStateTaxcodeTypeCode(String blogStateTaxcodeTypeCode){
        this.blogStateTaxcodeTypeCode = blogStateTaxcodeTypeCode;
    }

    public String getBlogCountyTaxcodeTypeCode(){
        return blogCountyTaxcodeTypeCode;
    }

    public void setBlogCountyTaxcodeTypeCode(String blogCountyTaxcodeTypeCode){
        this.blogCountyTaxcodeTypeCode = blogCountyTaxcodeTypeCode;
    }

    public String getBlogCityTaxcodeTypeCode(){
        return blogCityTaxcodeTypeCode;
    }

    public void setBlogCityTaxcodeTypeCode(String blogCityTaxcodeTypeCode){
        this.blogCityTaxcodeTypeCode = blogCityTaxcodeTypeCode;
    }

    public String getBlogEntityCode(){
        return blogEntityCode;
    }

    public void setBlogEntityCode(String blogEntityCode){
        this.blogEntityCode = blogEntityCode;
    }

    public BigDecimal getBlogStj1Rate(){
        return blogStj1Rate;
    }

    public void setBlogStj1Rate(BigDecimal blogStj1Rate){
        this.blogStj1Rate = blogStj1Rate;
    }

    public BigDecimal getBlogStj2Rate(){
        return blogStj2Rate;
    }

    public void setBlogStj2Rate(BigDecimal blogStj2Rate){
        this.blogStj2Rate = blogStj2Rate;
    }

    public BigDecimal getBlogStj3Rate(){
        return blogStj3Rate;
    }

    public void setBlogStj3Rate(BigDecimal blogStj3Rate){
        this.blogStj3Rate = blogStj3Rate;
    }

    public BigDecimal getBlogStj4Rate(){
        return blogStj4Rate;
    }

    public void setBlogStj4Rate(BigDecimal blogStj4Rate){
        this.blogStj4Rate = blogStj4Rate;
    }

    public BigDecimal getBlogStj5Rate(){
        return blogStj5Rate;
    }

    public void setBlogStj5Rate(BigDecimal blogStj5Rate){
        this.blogStj5Rate = blogStj5Rate;
    }

    public BigDecimal getBlogStj1Amount(){
        return blogStj1Amount;
    }

    public void setBlogStj1Amount(BigDecimal blogStj1Amount){
        this.blogStj1Amount = blogStj1Amount;
    }

    public BigDecimal getBlogStj2Amount(){
        return blogStj2Amount;
    }

    public void setBlogStj2Amount(BigDecimal blogStj2Amount){
        this.blogStj2Amount = blogStj2Amount;
    }

    public BigDecimal getBlogStj3Amount(){
        return blogStj3Amount;
    }

    public void setBlogStj3Amount(BigDecimal blogStj3Amount){
        this.blogStj3Amount = blogStj3Amount;
    }

    public BigDecimal getBlogStj4Amount(){
        return blogStj4Amount;
    }

    public void setBlogStj4Amount(BigDecimal blogStj4Amount){
        this.blogStj4Amount = blogStj4Amount;
    }

    public BigDecimal getBlogStj5Amount(){
        return blogStj5Amount;
    }

    public void setBlogStj5Amount(BigDecimal blogStj5Amount){
        this.blogStj5Amount = blogStj5Amount;
    }

    public String getBlogProductCode(){
        return blogProductCode;
    }

    public void setBlogProductCode(String blogProductCode){
        this.blogProductCode = blogProductCode;
    }

    public String getBlogRejectFlag(){
        return blogRejectFlag;
    }

    public void setBlogRejectFlag(String blogRejectFlag){
        this.blogRejectFlag = blogRejectFlag;
    }

    public BigDecimal getBlogStateUseTier1Setamt(){
        return blogStateUseTier1Setamt;
    }

    public void setBlogStateUseTier1Setamt(BigDecimal blogStateUseTier1Setamt){
        this.blogStateUseTier1Setamt = blogStateUseTier1Setamt;
    }

    public BigDecimal getBlogStateUseTier2Setamt(){
        return blogStateUseTier2Setamt;
    }

    public void setBlogStateUseTier2Setamt(BigDecimal blogStateUseTier2Setamt){
        this.blogStateUseTier2Setamt = blogStateUseTier2Setamt;
    }

    public BigDecimal getBlogStateUseTier3Setamt(){
        return blogStateUseTier3Setamt;
    }

    public void setBlogStateUseTier3Setamt(BigDecimal blogStateUseTier3Setamt){
        this.blogStateUseTier3Setamt = blogStateUseTier3Setamt;
    }

    public String getAlogComments(){
        return alogComments;
    }

    public void setAlogComments(String alogComments){
        this.alogComments = alogComments;
    }

    public BigDecimal getAlogTbCalcTaxAmt(){
        return alogTbCalcTaxAmt;
    }

    public void setAlogTbCalcTaxAmt(BigDecimal alogTbCalcTaxAmt){
        this.alogTbCalcTaxAmt = alogTbCalcTaxAmt;
    }

    public BigDecimal getAlogStateUseAmount(){
        return alogStateUseAmount;
    }

    public void setAlogStateUseAmount(BigDecimal alogStateUseAmount){
        this.alogStateUseAmount = alogStateUseAmount;
    }

    public BigDecimal getAlogStateUseTier2Amount(){
        return alogStateUseTier2Amount;
    }

    public void setAlogStateUseTier2Amount(BigDecimal alogStateUseTier2Amount){
        this.alogStateUseTier2Amount = alogStateUseTier2Amount;
    }

    public BigDecimal getAlogStateUseTier3Amount(){
        return alogStateUseTier3Amount;
    }

    public void setAlogStateUseTier3Amount(BigDecimal alogStateUseTier3Amount){
        this.alogStateUseTier3Amount = alogStateUseTier3Amount;
    }

    public BigDecimal getAlogCountyUseAmount(){
        return alogCountyUseAmount;
    }

    public void setAlogCountyUseAmount(BigDecimal alogCountyUseAmount){
        this.alogCountyUseAmount = alogCountyUseAmount;
    }

    public BigDecimal getAlogCountyLocalUseAmount(){
        return alogCountyLocalUseAmount;
    }

    public void setAlogCountyLocalUseAmount(BigDecimal alogCountyLocalUseAmount){
        this.alogCountyLocalUseAmount = alogCountyLocalUseAmount;
    }

    public BigDecimal getAlogCityUseAmount(){
        return alogCityUseAmount;
    }

    public void setAlogCityUseAmount(BigDecimal alogCityUseAmount){
        this.alogCityUseAmount = alogCityUseAmount;
    }

    public BigDecimal getAlogCityLocalUseAmount(){
        return alogCityLocalUseAmount;
    }

    public void setAlogCityLocalUseAmount(BigDecimal alogCityLocalUseAmount){
        this.alogCityLocalUseAmount = alogCityLocalUseAmount;
    }

    public String getAlogTransactionStateCode(){
        return alogTransactionStateCode;
    }

    public void setAlogTransactionStateCode(String alogTransactionStateCode){
        this.alogTransactionStateCode = alogTransactionStateCode;
    }

    public String getAlogAutoTransactionStateCode(){
        return alogAutoTransactionStateCode;
    }

    public void setAlogAutoTransactionStateCode(String alogAutoTransactionStateCode){
        this.alogAutoTransactionStateCode = alogAutoTransactionStateCode;
    }

    public String getAlogTransactionInd(){
        return alogTransactionInd;
    }

    public void setAlogTransactionInd(String alogTransactionInd){
        this.alogTransactionInd = alogTransactionInd;
    }

    public String getAlogSuspendInd(){
        return alogSuspendInd;
    }

    public void setAlogSuspendInd(String alogSuspendInd){
        this.alogSuspendInd = alogSuspendInd;
    }

    public Long getAlogTaxcodeDetailId(){
        return alogTaxcodeDetailId;
    }

    public void setAlogTaxcodeDetailId(Long alogTaxcodeDetailId){
        this.alogTaxcodeDetailId = alogTaxcodeDetailId;
    }

    public String getAlogTaxcodeStateCode(){
        return alogTaxcodeStateCode;
    }

    public void setAlogTaxcodeStateCode(String alogTaxcodeStateCode){
        this.alogTaxcodeStateCode = alogTaxcodeStateCode;
    }

    public String getAlogTaxcodeTypeCode(){
        return alogTaxcodeTypeCode;
    }

    public void setAlogTaxcodeTypeCode(String alogTaxcodeTypeCode){
        this.alogTaxcodeTypeCode = alogTaxcodeTypeCode;
    }

    public String getAlogTaxcodeCode(){
        return alogTaxcodeCode;
    }

    public void setAlogTaxcodeCode(String alogTaxcodeCode){
        this.alogTaxcodeCode = alogTaxcodeCode;
    }

    public String getAlogManualTaxcodeInd(){
        return alogManualTaxcodeInd;
    }

    public void setAlogManualTaxcodeInd(String alogManualTaxcodeInd){
        this.alogManualTaxcodeInd = alogManualTaxcodeInd;
    }

    public Long getAlogTaxMatrixId(){
        return alogTaxMatrixId;
    }

    public void setAlogTaxMatrixId(Long alogTaxMatrixId){
        this.alogTaxMatrixId = alogTaxMatrixId;
    }

    public Long getAlogLocationMatrixId(){
        return alogLocationMatrixId;
    }

    public void setAlogLocationMatrixId(Long alogLocationMatrixId){
        this.alogLocationMatrixId = alogLocationMatrixId;
    }

    public Long getAlogJurisdictionId(){
        return alogJurisdictionId;
    }

    public void setAlogJurisdictionId(Long alogJurisdictionId){
        this.alogJurisdictionId = alogJurisdictionId;
    }

    public Long getAlogJurisdictionTaxrateId(){
        return alogJurisdictionTaxrateId;
    }

    public void setAlogJurisdictionTaxrateId(Long alogJurisdictionTaxrateId){
        this.alogJurisdictionTaxrateId = alogJurisdictionTaxrateId;
    }

    public String getAlogManualJurisdictionInd(){
        return alogManualJurisdictionInd;
    }

    public void setAlogManualJurisdictionInd(String alogManualJurisdictionInd){
        this.alogManualJurisdictionInd = alogManualJurisdictionInd;
    }

    public String getAlogMeasureTypeCode(){
        return alogMeasureTypeCode;
    }

    public void setAlogMeasureTypeCode(String alogMeasureTypeCode){
        this.alogMeasureTypeCode = alogMeasureTypeCode;
    }

    public BigDecimal getAlogStateUseRate(){
        return alogStateUseRate;
    }

    public void setAlogStateUseRate(BigDecimal alogStateUseRate){
        this.alogStateUseRate = alogStateUseRate;
    }

    public BigDecimal getAlogStateUseTier2Rate(){
        return alogStateUseTier2Rate;
    }

    public void setAlogStateUseTier2Rate(BigDecimal alogStateUseTier2Rate){
        this.alogStateUseTier2Rate = alogStateUseTier2Rate;
    }

    public BigDecimal getAlogStateUseTier3Rate(){
        return alogStateUseTier3Rate;
    }

    public void setAlogStateUseTier3Rate(BigDecimal alogStateUseTier3Rate){
        this.alogStateUseTier3Rate = alogStateUseTier3Rate;
    }

    public BigDecimal getAlogStateSplitAmount(){
        return alogStateSplitAmount;
    }

    public void setAlogStateSplitAmount(BigDecimal alogStateSplitAmount){
        this.alogStateSplitAmount = alogStateSplitAmount;
    }

    public BigDecimal getAlogStateTier2MinAmount(){
        return alogStateTier2MinAmount;
    }

    public void setAlogStateTier2MinAmount(BigDecimal alogStateTier2MinAmount){
        this.alogStateTier2MinAmount = alogStateTier2MinAmount;
    }

    public BigDecimal getAlogStateTier2MaxAmount(){
        return alogStateTier2MaxAmount;
    }

    public void setAlogStateTier2MaxAmount(BigDecimal alogStateTier2MaxAmount){
        this.alogStateTier2MaxAmount = alogStateTier2MaxAmount;
    }

    public BigDecimal getAlogStateMaxtaxAmount(){
        return alogStateMaxtaxAmount;
    }

    public void setAlogStateMaxtaxAmount(BigDecimal alogStateMaxtaxAmount){
        this.alogStateMaxtaxAmount = alogStateMaxtaxAmount;
    }

    public BigDecimal getAlogCountyUseRate(){
        return alogCountyUseRate;
    }

    public void setAlogCountyUseRate(BigDecimal alogCountyUseRate){
        this.alogCountyUseRate = alogCountyUseRate;
    }

    public BigDecimal getAlogCountyLocalUseRate(){
        return alogCountyLocalUseRate;
    }

    public void setAlogCountyLocalUseRate(BigDecimal alogCountyLocalUseRate){
        this.alogCountyLocalUseRate = alogCountyLocalUseRate;
    }

    public BigDecimal getAlogCountySplitAmount(){
        return alogCountySplitAmount;
    }

    public void setAlogCountySplitAmount(BigDecimal alogCountySplitAmount){
        this.alogCountySplitAmount = alogCountySplitAmount;
    }

    public BigDecimal getAlogCountyMaxtaxAmount(){
        return alogCountyMaxtaxAmount;
    }

    public void setAlogCountyMaxtaxAmount(BigDecimal alogCountyMaxtaxAmount){
        this.alogCountyMaxtaxAmount = alogCountyMaxtaxAmount;
    }

    public String getAlogCountySingleFlag(){
        return alogCountySingleFlag;
    }

    public void setAlogCountySingleFlag(String alogCountySingleFlag){
        this.alogCountySingleFlag = alogCountySingleFlag;
    }

    public String getAlogCountyDefaultFlag(){
        return alogCountyDefaultFlag;
    }

    public void setAlogCountyDefaultFlag(String alogCountyDefaultFlag){
        this.alogCountyDefaultFlag = alogCountyDefaultFlag;
    }

    public BigDecimal getAlogCityUseRate(){
        return alogCityUseRate;
    }

    public void setAlogCityUseRate(BigDecimal alogCityUseRate){
        this.alogCityUseRate = alogCityUseRate;
    }

    public BigDecimal getAlogCityLocalUseRate(){
        return alogCityLocalUseRate;
    }

    public void setAlogCityLocalUseRate(BigDecimal alogCityLocalUseRate){
        this.alogCityLocalUseRate = alogCityLocalUseRate;
    }

    public BigDecimal getAlogCitySplitAmount(){
        return alogCitySplitAmount;
    }

    public void setAlogCitySplitAmount(BigDecimal alogCitySplitAmount){
        this.alogCitySplitAmount = alogCitySplitAmount;
    }

    public BigDecimal getAlogCitySplitUseRate(){
        return alogCitySplitUseRate;
    }

    public void setAlogCitySplitUseRate(BigDecimal alogCitySplitUseRate){
        this.alogCitySplitUseRate = alogCitySplitUseRate;
    }

    public String getAlogCitySingleFlag(){
        return alogCitySingleFlag;
    }

    public void setAlogCitySingleFlag(String alogCitySingleFlag){
        this.alogCitySingleFlag = alogCitySingleFlag;
    }

    public String getAlogCityDefaultFlag(){
        return alogCityDefaultFlag;
    }

    public void setAlogCityDefaultFlag(String alogCityDefaultFlag){
        this.alogCityDefaultFlag = alogCityDefaultFlag;
    }

    public BigDecimal getAlogCombinedUseRate(){
        return alogCombinedUseRate;
    }

    public void setAlogCombinedUseRate(BigDecimal alogCombinedUseRate){
        this.alogCombinedUseRate = alogCombinedUseRate;
    }

    public String getAlogAuditFlag(){
        return alogAuditFlag;
    }

    public void setAlogAuditFlag(String alogAuditFlag){
        this.alogAuditFlag = alogAuditFlag;
    }

    public String getAlogAuditUserId(){
        return alogAuditUserId;
    }

    public void setAlogAuditUserId(String alogAuditUserId){
        this.alogAuditUserId = alogAuditUserId;
    }

    public Date getAlogAuditTimestamp(){
        return alogAuditTimestamp;
    }

    public void setAlogAuditTimestamp(Date alogAuditTimestamp){
        this.alogAuditTimestamp = alogAuditTimestamp;
    }

    public String getAlogModifyUserId(){
        return alogModifyUserId;
    }

    public void setAlogModifyUserId(String alogModifyUserId){
        this.alogModifyUserId = alogModifyUserId;
    }

    public Date getAlogModifyTimestamp(){
        return alogModifyTimestamp;
    }

    public void setAlogModifyTimestamp(Date alogModifyTimestamp){
        this.alogModifyTimestamp = alogModifyTimestamp;
    }

    public String getAlogUpdateUserId(){
        return alogUpdateUserId;
    }

    public void setAlogUpdateUserId(String alogUpdateUserId){
        this.alogUpdateUserId = alogUpdateUserId;
    }

    public Date getAlogUpdateTimestamp(){
        return alogUpdateTimestamp;
    }

    public void setAlogUpdateTimestamp(Date alogUpdateTimestamp){
        this.alogUpdateTimestamp = alogUpdateTimestamp;
    }

    public BigDecimal getAlogCountryUseAmount(){
        return alogCountryUseAmount;
    }

    public void setAlogCountryUseAmount(BigDecimal alogCountryUseAmount){
        this.alogCountryUseAmount = alogCountryUseAmount;
    }

    public String getAlogTransactionCountryCode(){
        return alogTransactionCountryCode;
    }

    public void setAlogTransactionCountryCode(String alogTransactionCountryCode){
        this.alogTransactionCountryCode = alogTransactionCountryCode;
    }

    public String getAlogAutoTransCountryCode(){
        return alogAutoTransCountryCode;
    }

    public void setAlogAutoTransCountryCode(String alogAutoTransCountryCode){
        this.alogAutoTransCountryCode = alogAutoTransCountryCode;
    }

    public String getAlogTaxcodeCountryCode(){
        return alogTaxcodeCountryCode;
    }

    public void setAlogTaxcodeCountryCode(String alogTaxcodeCountryCode){
        this.alogTaxcodeCountryCode = alogTaxcodeCountryCode;
    }

    public BigDecimal getAlogCountryUseRate(){
        return alogCountryUseRate;
    }

    public void setAlogCountryUseRate(BigDecimal alogCountryUseRate){
        this.alogCountryUseRate = alogCountryUseRate;
    }

    public Long getAlogSplitSubtransId(){
        return alogSplitSubtransId;
    }

    public void setAlogSplitSubtransId(Long alogSplitSubtransId){
        this.alogSplitSubtransId = alogSplitSubtransId;
    }

    public String getAlogMultiTransCode(){
        return alogMultiTransCode;
    }

    public void setAlogMultiTransCode(String alogMultiTransCode){
        this.alogMultiTransCode = alogMultiTransCode;
    }

    public Long getAlogTaxAllocMatrixId(){
        return alogTaxAllocMatrixId;
    }

    public void setAlogTaxAllocMatrixId(Long alogTaxAllocMatrixId){
        this.alogTaxAllocMatrixId = alogTaxAllocMatrixId;
    }

    public Long getAlogStateTaxcodeDetailId(){
        return alogStateTaxcodeDetailId;
    }

    public void setAlogStateTaxcodeDetailId(Long alogStateTaxcodeDetailId){
        this.alogStateTaxcodeDetailId = alogStateTaxcodeDetailId;
    }

    public Long getAlogCountyTaxcodeDetailId(){
        return alogCountyTaxcodeDetailId;
    }

    public void setAlogCountyTaxcodeDetailId(Long alogCountyTaxcodeDetailId){
        this.alogCountyTaxcodeDetailId = alogCountyTaxcodeDetailId;
    }

    public Long getAlogCityTaxcodeDetailId(){
        return alogCityTaxcodeDetailId;
    }

    public void setAlogCityTaxcodeDetailId(Long alogCityTaxcodeDetailId){
        this.alogCityTaxcodeDetailId = alogCityTaxcodeDetailId;
    }

    public String getAlogTaxtypeUsedCode(){
        return alogTaxtypeUsedCode;
    }

    public void setAlogTaxtypeUsedCode(String alogTaxtypeUsedCode){
        this.alogTaxtypeUsedCode = alogTaxtypeUsedCode;
    }

    public BigDecimal getAlogStateTaxableAmt(){
        return alogStateTaxableAmt;
    }

    public void setAlogStateTaxableAmt(BigDecimal alogStateTaxableAmt){
        this.alogStateTaxableAmt = alogStateTaxableAmt;
    }

    public BigDecimal getAlogCountyTaxableAmt(){
        return alogCountyTaxableAmt;
    }

    public void setAlogCountyTaxableAmt(BigDecimal alogCountyTaxableAmt){
        this.alogCountyTaxableAmt = alogCountyTaxableAmt;
    }

    public BigDecimal getAlogCityTaxableAmt(){
        return alogCityTaxableAmt;
    }

    public void setAlogCityTaxableAmt(BigDecimal alogCityTaxableAmt){
        this.alogCityTaxableAmt = alogCityTaxableAmt;
    }

    public String getAlogStateTaxcodeTypeCode(){
        return alogStateTaxcodeTypeCode;
    }

    public void setAlogStateTaxcodeTypeCode(String alogStateTaxcodeTypeCode){
        this.alogStateTaxcodeTypeCode = alogStateTaxcodeTypeCode;
    }

    public String getAlogCountyTaxcodeTypeCode(){
        return alogCountyTaxcodeTypeCode;
    }

    public void setAlogCountyTaxcodeTypeCode(String alogCountyTaxcodeTypeCode){
        this.alogCountyTaxcodeTypeCode = alogCountyTaxcodeTypeCode;
    }

    public String getAlogCityTaxcodeTypeCode(){
        return alogCityTaxcodeTypeCode;
    }

    public void setAlogCityTaxcodeTypeCode(String alogCityTaxcodeTypeCode){
        this.alogCityTaxcodeTypeCode = alogCityTaxcodeTypeCode;
    }

    public String getAlogEntityCode(){
        return alogEntityCode;
    }

    public void setAlogEntityCode(String alogEntityCode){
        this.alogEntityCode = alogEntityCode;
    }

    public BigDecimal getAlogStj1Rate(){
        return alogStj1Rate;
    }

    public void setAlogStj1Rate(BigDecimal alogStj1Rate){
        this.alogStj1Rate = alogStj1Rate;
    }

    public BigDecimal getAlogStj2Rate(){
        return alogStj2Rate;
    }

    public void setAlogStj2Rate(BigDecimal alogStj2Rate){
        this.alogStj2Rate = alogStj2Rate;
    }

    public BigDecimal getAlogStj3Rate(){
        return alogStj3Rate;
    }

    public void setAlogStj3Rate(BigDecimal alogStj3Rate){
        this.alogStj3Rate = alogStj3Rate;
    }

    public BigDecimal getAlogStj4Rate(){
        return alogStj4Rate;
    }

    public void setAlogStj4Rate(BigDecimal alogStj4Rate){
        this.alogStj4Rate = alogStj4Rate;
    }

    public BigDecimal getAlogStj5Rate(){
        return alogStj5Rate;
    }

    public void setAlogStj5Rate(BigDecimal alogStj5Rate){
        this.alogStj5Rate = alogStj5Rate;
    }

    public BigDecimal getAlogStj1Amount(){
        return alogStj1Amount;
    }

    public void setAlogStj1Amount(BigDecimal alogStj1Amount){
        this.alogStj1Amount = alogStj1Amount;
    }

    public BigDecimal getAlogStj2Amount(){
        return alogStj2Amount;
    }

    public void setAlogStj2Amount(BigDecimal alogStj2Amount){
        this.alogStj2Amount = alogStj2Amount;
    }

    public BigDecimal getAlogStj3Amount(){
        return alogStj3Amount;
    }

    public void setAlogStj3Amount(BigDecimal alogStj3Amount){
        this.alogStj3Amount = alogStj3Amount;
    }

    public BigDecimal getAlogStj4Amount(){
        return alogStj4Amount;
    }

    public void setAlogStj4Amount(BigDecimal alogStj4Amount){
        this.alogStj4Amount = alogStj4Amount;
    }

    public BigDecimal getAlogStj5Amount(){
        return alogStj5Amount;
    }

    public void setAlogStj5Amount(BigDecimal alogStj5Amount){
        this.alogStj5Amount = alogStj5Amount;
    }

    public String getAlogProductCode(){
        return alogProductCode;
    }

    public void setAlogProductCode(String alogProductCode){
        this.alogProductCode = alogProductCode;
    }

    public String getAlogRejectFlag(){
        return alogRejectFlag;
    }

    public void setAlogRejectFlag(String alogRejectFlag){
        this.alogRejectFlag = alogRejectFlag;
    }

    public BigDecimal getAlogStateUseTier1Setamt(){
        return alogStateUseTier1Setamt;
    }

    public void setAlogStateUseTier1Setamt(BigDecimal alogStateUseTier1Setamt){
        this.alogStateUseTier1Setamt = alogStateUseTier1Setamt;
    }

    public BigDecimal getAlogStateUseTier2Setamt(){
        return alogStateUseTier2Setamt;
    }

    public void setAlogStateUseTier2Setamt(BigDecimal alogStateUseTier2Setamt){
        this.alogStateUseTier2Setamt = alogStateUseTier2Setamt;
    }

    public BigDecimal getAlogStateUseTier3Setamt(){
        return alogStateUseTier3Setamt;
    }

    public void setAlogStateUseTier3Setamt(BigDecimal alogStateUseTier3Setamt){
        this.alogStateUseTier3Setamt = alogStateUseTier3Setamt;
    }

    public String getUpdateUserId(){
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId){
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTimestamp(){
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp){
        this.updateTimestamp = updateTimestamp;
    }

}
