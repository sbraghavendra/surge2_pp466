package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
public class TaxCodeStatePK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="TAXCODE_COUNTRY_CODE")
	private String country;
	   
	@Column(name="TAXCODE_STATE_CODE") 	
	private String taxcodeStateCode;
	
	public TaxCodeStatePK(){
		
	}
	
	public TaxCodeStatePK(String country, String taxcodeStateCode) {
		this.country = country;
		this.taxcodeStateCode = taxcodeStateCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTaxcodeStateCode() {
		return taxcodeStateCode;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
		this.taxcodeStateCode = taxcodeStateCode;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxCodeStatePK)) {
            return false;
        }

        final TaxCodeStatePK revision = (TaxCodeStatePK)other;

        String c1 = getCountry();
        String c2 = revision.getCountry();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String d1 = getTaxcodeStateCode();
        String d2 = revision.getTaxcodeStateCode();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getCountry();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String d1 = getTaxcodeStateCode();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.country + "::" + this.taxcodeStateCode;
    }

}
