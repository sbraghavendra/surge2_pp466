package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.VendorItemDTO;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="TB_VENDOR")
public class VendorItem extends Auditable implements Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="VENDOR_ID")
	@GeneratedValue (strategy = GenerationType.AUTO , generator="vendor_sequence")
	@SequenceGenerator(name="vendor_sequence" , allocationSize = 1, sequenceName="SQ_TB_VENDOR_ID")	
    private Long vendorId;
    
    @Column(name="VENDOR_CODE")    
    private String vendorCode;  
    
    @Column(name="VENDOR_NAME")    
    private String vendorName;  
    
    @Column(name="GEOCODE")    
    private String geocode; 
    
    @Column(name="ADDRESS_LINE_1")    
    private String addressLine1; 
    
    @Column(name="ADDRESS_LINE_2")    
    private String addressLine2; 
    
    @Column(name="CITY")    
    private String city; 
    
    @Column(name="COUNTY")    
    private String county; 
    
    @Column(name="STATE")    
    private String state; 
    
    @Column(name="ZIP")    
    private String zip; 
    
    @Column(name="ZIPPLUS4")    
    private String zipplus4; 
    
    @Column(name="COUNTRY")    
    private String country; 
    
    @Column(name="ACTIVE_FLAG")    
    private String activeFlag; 
    
    @Transient
	private String type = "";
	
	@Transient
	private String selectFlag;

	@Transient	
	VendorItemDTO vendorItemDTO;
	
	public VendorItem(){
		
	}

    public VendorItem(String vendorId, String vendorCode,String vendorName){
    	this.vendorId=Long.valueOf(vendorId);
    	this.vendorCode=vendorCode;
    	this.vendorName=vendorName;
    }
	
	public VendorItemDTO getVendorItemDTO() {
		VendorItemDTO vendorItemDTO = new VendorItemDTO();
		BeanUtils.copyProperties(this, vendorItemDTO);
		return vendorItemDTO;
	}
    
    public Long getVendorId() {
        return this.vendorId;
    }
    
    public void setVendorId(Long vendorId) {
    	this.vendorId = vendorId;
    }
    
    public String getVendorCode() {
        return this.vendorCode;
    }
    
    public void setVendorCode(String vendorCode) {
    	this.vendorCode = vendorCode;
    }    
    
    public String getVendorName() {
        return this.vendorName;
    }
    
    public void setVendorName(String vendorName) {
    	this.vendorName = vendorName;
    }      
    
    public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
    	this.geocode = geocode;
    }  
    
    public String getAddressLine1() {
        return this.addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
    	this.addressLine1 = addressLine1;
    }  
    
    public String getAddressLine2() {
        return this.addressLine2;
    }
    
    public void setAddressLine2(String addressLine2) {
    	this.addressLine2 = addressLine2;
    }  
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
    	this.city = city;
    }  
    
    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
    	this.county = county;
    }  
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }  
    
    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
    	this.zip = zip;
    }  
    
    public String getZipplus4() {
        return this.zipplus4;
    }
    
    public void setZipplus4(String zipplus4) {
    	this.zipplus4 = zipplus4;
    }  
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  

    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
    
    /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof VendorItem)) {
            return false;
        }

        final VendorItem revision = (VendorItem)other;

        Long c1 = getVendorId();
        Long c2 = revision.getVendorId();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }
        
        String d1 = getVendorCode();
        String d2 = revision.getVendorCode();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }
        
        String e1 = getVendorName();
        String e2 = revision.getVendorName();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		Long c1 = getVendorId();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());  
		String dnc1 = getVendorCode();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
		String d1 = getVendorName();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.vendorId + "::" + this.vendorCode + "::" + this.vendorName;
    }

	@Override
	public Long getId() {
		return this.vendorId;
	}

	@Override
	public void setId(Long id) {
		this.vendorId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "vendorId";
	}
	
	public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
    	this.type = type;
    } 
    
    public void setSelectFlag(String selectFlag) {
    	this.selectFlag = selectFlag;
    }  
	
	public Boolean getSelectBooleanFlag() {
    	return "1".equals(this.selectFlag);
	}

	public void setSelectBooleanFlag(Boolean selectFlag) {
		this.selectFlag = selectFlag == Boolean.TRUE ? "1" : "0";
	}
}





