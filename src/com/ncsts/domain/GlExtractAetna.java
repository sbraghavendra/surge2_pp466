/*
 * Author: Jim M. Wilson
 * Created: Aug 19, 2008
 * $Date: 2008-08-21 12:32:57 -0500 (Thu, 21 Aug 2008) $: - $Revision: 1872 $: 
 */

package com.ncsts.domain;

import com.ncsts.domain.Idable;


/**
 *
 */
public class GlExtractAetna implements Idable<Long> {
	/* unmitigated ugly hack to get this DTO to play well as a DataModel. Note this data is never written */
	  private Long id;
	  
    private String recordType;
    private String userJeSourceName;
    private String groupId;
    private String enteredDebit;
    private String enteredCredit;
    private String ifsBusPortfolioSeg;
    private String ifsNatAcctMajClass;
    private String ifsNatAcctMinClass;
    private String ifsGeoCode;
    private String ifsProduct;
    private String ifsCostCenter;
    private String ifsMgmtRptgInfo1;
    private String ifsMgmtRptgInfo2;
    private String ifsAffiliate;
    private String ifsBusinessType;
    private String ifsRegReptgBasis;
    private String originatingCostCenter;
    private String currencyCode;
    private String allAttribute;
    private String userJeCategoryName;
    private String descriptiveText;

	public GlExtractAetna() {
		super();
	}
	
	/**
	 * Convert array of objects from sql query (columns in order from SQL) 
	 * @param resultSet 
	 */
	public GlExtractAetna(Object[] resultSet, long id) {
		setId(id);
		if (resultSet != null) {
			setRecordType(getValue(resultSet[0]));
			setUserJeSourceName(getValue(resultSet[1]));
			setGroupId(getValue(resultSet[2]));
			setEnteredDebit(getValue(resultSet[3]));
			setEnteredCredit(getValue(resultSet[4]));
			setIfsBusPortfolioSeg(getValue(resultSet[5]));
			setIfsNatAcctMajClass(getValue(resultSet[6]));
			setIfsNatAcctMinClass(getValue(resultSet[7]));
			setIfsGeoCode(getValue(resultSet[8]));
			setIfsProduct(getValue(resultSet[9]));
			setIfsCostCenter(getValue(resultSet[10]));
			setIfsMgmtRptgInfo1(getValue(resultSet[11]));
			setIfsMgmtRptgInfo2(getValue(resultSet[12]));
			setIfsAffiliate(getValue(resultSet[13]));
			setIfsBusinessType(getValue(resultSet[14]));
			setIfsRegReptgBasis(getValue(resultSet[15]));
			setOriginatingCostCenter(getValue(resultSet[16]));
			setCurrencyCode(getValue(resultSet[17]));
			setAllAttribute(getValue(resultSet[18]));
			setUserJeCategoryName(getValue(resultSet[19]));
			setDescriptiveText(getValue(resultSet[20]));
		}
	}
	private String getValue(Object val) {
		if (val == null) return null;
		return val.toString();
	}

	public String getIdPropertyName() {
		return "id";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the recordType
	 */
	public String getRecordType() {
		return this.recordType;
	}

	/**
	 * @param recordType the recordType to set
	 */
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	/**
	 * @return the userJeSourceName
	 */
	public String getUserJeSourceName() {
		return this.userJeSourceName;
	}

	/**
	 * @param userJeSourceName the userJeSourceName to set
	 */
	public void setUserJeSourceName(String userJeSourceName) {
		this.userJeSourceName = userJeSourceName;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return this.groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the enteredDebit
	 */
	public String getEnteredDebit() {
		return this.enteredDebit;
	}

	/**
	 * @param enteredDebit the enteredDebit to set
	 */
	public void setEnteredDebit(String enteredDebit) {
		this.enteredDebit = enteredDebit;
	}

	/**
	 * @return the enteredCredit
	 */
	public String getEnteredCredit() {
		return this.enteredCredit;
	}

	/**
	 * @param enteredCredit the enteredCredit to set
	 */
	public void setEnteredCredit(String enteredCredit) {
		this.enteredCredit = enteredCredit;
	}

	/**
	 * @return the ifsBusPortfolioSeg
	 */
	public String getIfsBusPortfolioSeg() {
		return this.ifsBusPortfolioSeg;
	}

	/**
	 * @param ifsBusPortfolioSeg the ifsBusPortfolioSeg to set
	 */
	public void setIfsBusPortfolioSeg(String ifsBusPortfolioSeg) {
		this.ifsBusPortfolioSeg = ifsBusPortfolioSeg;
	}

	/**
	 * @return the ifsNatAcctMajClass
	 */
	public String getIfsNatAcctMajClass() {
		return this.ifsNatAcctMajClass;
	}

	/**
	 * @param ifsNatAcctMajClass the ifsNatAcctMajClass to set
	 */
	public void setIfsNatAcctMajClass(String ifsNatAcctMajClass) {
		this.ifsNatAcctMajClass = ifsNatAcctMajClass;
	}

	/**
	 * @return the ifsNatAcctdMinClass
	 */
	public String getIfsNatAcctMinClass() {
		return this.ifsNatAcctMinClass;
	}

	/**
	 * @param ifsNatAcctdMinClass the ifsNatAcctdMinClass to set
	 */
	public void setIfsNatAcctMinClass(String ifsNatAcctMinClass) {
		this.ifsNatAcctMinClass = ifsNatAcctMinClass;
	}

	/**
	 * @return the ifsGeoCode
	 */
	public String getIfsGeoCode() {
		return this.ifsGeoCode;
	}

	/**
	 * @param ifsGeoCode the ifsGeoCode to set
	 */
	public void setIfsGeoCode(String ifsGeoCode) {
		this.ifsGeoCode = ifsGeoCode;
	}

	/**
	 * @return the ifsProduct
	 */
	public String getIfsProduct() {
		return this.ifsProduct;
	}

	/**
	 * @param ifsProduct the ifsProduct to set
	 */
	public void setIfsProduct(String ifsProduct) {
		this.ifsProduct = ifsProduct;
	}

	/**
	 * @return the ifsCostCenter
	 */
	public String getIfsCostCenter() {
		return this.ifsCostCenter;
	}

	/**
	 * @param ifsCostCenter the ifsCostCenter to set
	 */
	public void setIfsCostCenter(String ifsCostCenter) {
		this.ifsCostCenter = ifsCostCenter;
	}

	/**
	 * @return the ifsMgmtRptgInfoi
	 */
	public String getIfsMgmtRptgInfo1() {
		return this.ifsMgmtRptgInfo1;
	}

	/**
	 * @param ifsMgmtRptgInfoi the ifsMgmtRptgInfoi to set
	 */
	public void setIfsMgmtRptgInfo1(String ifsMgmtRptgInfoi) {
		this.ifsMgmtRptgInfo1 = ifsMgmtRptgInfoi;
	}

	/**
	 * @return the ifsMgmtRptgInfoii
	 */
	public String getIfsMgmtRptgInfo2() {
		return this.ifsMgmtRptgInfo2;
	}

	/**
	 * @param ifsMgmtRptgInfoii the ifsMgmtRptgInfoii to set
	 */
	public void setIfsMgmtRptgInfo2(String ifsMgmtRptgInfoii) {
		this.ifsMgmtRptgInfo2 = ifsMgmtRptgInfoii;
	}

	/**
	 * @return the ifsAffiliate
	 */
	public String getIfsAffiliate() {
		return this.ifsAffiliate;
	}

	/**
	 * @param ifsAffiliate the ifsAffiliate to set
	 */
	public void setIfsAffiliate(String ifsAffiliate) {
		this.ifsAffiliate = ifsAffiliate;
	}

	/**
	 * @return the ifsBusinessType
	 */
	public String getIfsBusinessType() {
		return this.ifsBusinessType;
	}

	/**
	 * @param ifsBusinessType the ifsBusinessType to set
	 */
	public void setIfsBusinessType(String ifsBusinessType) {
		this.ifsBusinessType = ifsBusinessType;
	}

	/**
	 * @return the ifsRegReptgBasis
	 */
	public String getIfsRegReptgBasis() {
		return this.ifsRegReptgBasis;
	}

	/**
	 * @param ifsRegReptgBasis the ifsRegReptgBasis to set
	 */
	public void setIfsRegReptgBasis(String ifsRegReptgBasis) {
		this.ifsRegReptgBasis = ifsRegReptgBasis;
	}

	/**
	 * @return the originatingCostCenter
	 */
	public String getOriginatingCostCenter() {
		return this.originatingCostCenter;
	}

	/**
	 * @param originatingCostCenter the originatingCostCenter to set
	 */
	public void setOriginatingCostCenter(String originatingCostCenter) {
		this.originatingCostCenter = originatingCostCenter;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return this.currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the allAttribute
	 */
	public String getAllAttribute() {
		return this.allAttribute;
	}

	/**
	 * @param allAttribute the allAttribute to set
	 */
	public void setAllAttribute(String allAttribute) {
		this.allAttribute = allAttribute;
	}

	/**
	 * @return the userJeCategory_name
	 */
	public String getUserJeCategoryName() {
		return this.userJeCategoryName;
	}

	/**
	 * @param userJeCategory_name the userJeCategory_name to set
	 */
	public void setUserJeCategoryName(String userJeCategory_name) {
		this.userJeCategoryName = userJeCategory_name;
	}

	/**
	 * @return the descriptiveText
	 */
	public String getDescriptiveText() {
		return this.descriptiveText;
	}

	/**
	 * @param descriptiveText the descriptiveText to set
	 */
	public void setDescriptiveText(String descriptiveText) {
		this.descriptiveText = descriptiveText;
	}

}
