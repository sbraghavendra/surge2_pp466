package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.MenuDTO;


public class DbProperties implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
    private String databaseName;
    
    private String driverClassName;
    private String url;
    private String user;
    private String password;
	
	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    public DbProperties() { 	
    }
    
    public DbProperties(String databaseName) { 
    	setDatabaseName(databaseName);
    }
	
    public String getDatabaseName() {
        return this.databaseName;
    }
    
    public void setDatabaseName(String databaseName) {
    	this.databaseName = databaseName;
    }
   
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DbProperties)) {
            return false;
        }

        final DbProperties revision = (DbProperties)other;

        String c1 = getDatabaseName();
        String c2 = revision.getDatabaseName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
    public int hashCode() {
		int hash = 7;
        String c1 = getDatabaseName();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    public String toString() {
        return this.getClass().getName() + "::" + this.databaseName;
    }
}



