package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
public class ReferenceDetailPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//Midtier project code added - january 2009
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="TAXCODE_DETAIL_ID")  	
	private TaxCodeDetail taxcodeDetail; 
	   
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="REFERENCE_DOCUMENT_CODE") 	
	private ReferenceDocument refDoc;

	public ReferenceDetailPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaxCodeDetail getTaxcodeDetail() {
		return taxcodeDetail;
	}

	public void setTaxcodeDetail(TaxCodeDetail taxcodeDetail) {
		this.taxcodeDetail = taxcodeDetail;
	}

	public ReferenceDocument getRefDoc() {
		return refDoc;
	}

	public void setRefDoc(ReferenceDocument refDoc) {
		this.refDoc = refDoc;
	}

	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof ReferenceDetailPK)) {
            return false;
        }

        final ReferenceDetailPK revision = (ReferenceDetailPK)other;

        ReferenceDocument r1 = getRefDoc();
        ReferenceDocument r2 = revision.getRefDoc();
        if (!((r1 == r2) || (r1 != null && r1.getRefDocCode().equals(r2.getRefDocCode())))) {
            return false;
        }

        TaxCodeDetail t1 = getTaxcodeDetail();
        TaxCodeDetail t2 = revision.getTaxcodeDetail();
        if (!((t1 == t2) || (t1 != null && t1.getTaxcodeDetailId().equals(t2.getTaxcodeDetailId())))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        ReferenceDocument r1 = getRefDoc();
		hash = 31 * hash + (null == r1 ? 0 : r1.getRefDocCode().hashCode());
        TaxCodeDetail t1 = getTaxcodeDetail();
		hash = 31 * hash + (null == t1 ? 0 : t1.getTaxcodeDetailId().hashCode());
		return hash;
    }
}
