package com.ncsts.domain;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TB_BCP_CUST_LOCN_TEXT")
public class BCPCustLocnText implements java.io.Serializable, Idable<BCPCustLocnTextPK> {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BCPCustLocnTextPK id;

	@Column(name = "TEXT")
	private String text;
	
	public BCPCustLocnText() {
		this.id = new BCPCustLocnTextPK();
	}
	public BCPCustLocnText(Long batchId, Long line, String text) {
		this();
		this.id.setBatchId(batchId);
		this.id.setLine(line);
		this.text = text;
	}
	public String getIdPropertyName() {return "id";}

	/**
	 * @return the id
	 */
	public BCPCustLocnTextPK getId() {
		return this.id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(BCPCustLocnTextPK id) {
		this.id = id;
	}
	public Long getLine() {
		return id.getLine();
	}
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	//Fix bugs 4381, 4380,4377, and 3921
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	  static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPCustLocnText.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_CUST_LOCN_TEXT (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		
		//Add primary key
		colBuf.append(", BATCH_ID, LINE");
		valBuf.append(", ?, ?");
		

		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPCustLocnText.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){				
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){					
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{					
						s.setObject(idx, fld);
					}
				}
			}
		}
		
		//Add primary key
		idx++;
		s.setLong(idx, this.getId().getBatchId());

		idx++;
		s.setLong(idx, this.getId().getLine());
	}
}
