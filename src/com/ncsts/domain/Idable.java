package com.ncsts.domain;

public interface Idable<ID> {
	public ID getId();
	public void setId(ID id);
	public String getIdPropertyName();
}
