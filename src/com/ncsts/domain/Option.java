package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;

@Entity
@Table(name="TB_OPTION")
public class Option extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = LoggerFactory.getInstance().getLogger(Auditable.class);	
	
	@EmbeddedId
	private OptionCodePK codePK;
	
	@Column(name="DESCRIPTION")
	String description;
	
	@Column(name="VALUE")
	String value;	
	
	public Option() {
		
	}
	public Option(OptionCodePK codePK) {
		this.codePK = codePK;
	}
	public OptionCodePK getCodePK() {
		return codePK;
	}
	public void setCodePK(OptionCodePK codePK) {
		this.codePK = codePK;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

