package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.MenuDTO;


public class UserSecurity implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
    private String userName;
	
    public UserSecurity() { 	
    }
    
    public UserSecurity(String userName) { 
    	setUserName(userName);
    }
	
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
    	this.userName = userName;
    }
   
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof UserSecurity)) {
            return false;
        }

        final UserSecurity revision = (UserSecurity)other;

        String c1 = getUserName();
        String c2 = revision.getUserName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
    public int hashCode() {
		int hash = 7;
        String c1 = getUserName();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    public String toString() {
        return this.getClass().getName() + "::" + this.userName;
    }
}



