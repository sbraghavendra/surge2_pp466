package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DriverReferencePK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="TRANS_DTL_COLUMN_NAME")  	
	private String transDetailColName; 
	   
	@Column(name="DRIVER_VALUE") 	
	private String driverValue;
	
	public DriverReferencePK(){
		
	}
	
	public DriverReferencePK(String transDetailColName, String driverValue) {
		this.transDetailColName = transDetailColName;
		this.driverValue = driverValue;
	}
	@Column(name="transDetailColName")
	public String getTransDetailColName() {
		return transDetailColName;
	}

	public void setTransDetailColName(String transDetailColName) {
		this.transDetailColName = transDetailColName;
	}
	@Column(name="driverValue")
	public String getDriverValue() {
		return driverValue;
	}

	public void setDriverValue(String driverValue) {
		this.driverValue = driverValue;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DriverReferencePK)) {
            return false;
        }

        final DriverReferencePK revision = (DriverReferencePK)other;

        String c1 = getTransDetailColName();
        String c2 = revision.getTransDetailColName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String d1 = getDriverValue();
        String d2 = revision.getDriverValue();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getTransDetailColName();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String d1 = getDriverValue();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.transDetailColName + "::" + this.driverValue;
    }

}
