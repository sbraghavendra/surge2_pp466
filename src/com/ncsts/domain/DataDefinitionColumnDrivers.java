package com.ncsts.domain;

import javax.persistence.*;

@Entity
@Table (name="TB_DATA_DEF_COLUMN")
@SecondaryTables({
        @SecondaryTable(name="TB_DRIVER_NAMES", pkJoinColumns= {@PrimaryKeyJoinColumn(  name="TRANS_DTL_COLUMN_NAME",referencedColumnName="COLUMN_NAME"   ),
                @PrimaryKeyJoinColumn(  name="TABLE_NAME",referencedColumnName="TABLE_NAME"   )})
})
public class DataDefinitionColumnDrivers extends Auditable implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * EmbeddedId primary key field
     */
    @EmbeddedId
    DataDefinitionColumnPK dataDefinitionColumnPK;


    @Column(name="DATA_TYPE" )
    private String dataType;

    @Column(name="DATA_LENGTH" )
    private Long datalength;

    @Column(name="DESCRIPTION" )
    private String description;

    @Column(name="ABBR_DESC" )
    private String abbrDesc;

    @Column(name="DEFINITION" )
    private String definition;

    @Column(name="DESC_COLUMN_NAME" )
    private String descColumnName;

    @Column(name="MAP_FLAG" )
    private String mapFlag;

    @Column(name="MINIMUM_VIEW_FLAG" )
    private String minimumViewFlag;

/*
    //Midtier project code added - january 2009
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TABLE_NAME",insertable=false, updatable=false)
    @ForeignKey(name="tb_data_def_column_fk01")
    private DataDefinitionTable dataDefinitionTable;
*/

/*
    @Transient
    DataDefinitionColumnDTO dataDefinitionColumnDTO;
*/




    // Constructors

    /** default constructor */
    public DataDefinitionColumnDrivers() {
    }

  /*  public DataDefinitionColumn(DataDefinitionColumnPK dataDefinitionColumnPK) {
        this.dataDefinitionColumnPK = dataDefinitionColumnPK;
    }
*/
   /* public DataDefinitionColumnDTO getDataDefinitionColumnDTO() {
        DataDefinitionColumnDTO dataDefinitionColumnDTO= new DataDefinitionColumnDTO();
        BeanUtils.copyProperties(this, dataDefinitionColumnDTO);
        return dataDefinitionColumnDTO;
    }*/


   /* public void setDataDefinitionColumnDTO(DataDefinitionColumnDTO dataDefinitionColumnDTO) {
        this.dataDefinitionColumnDTO = dataDefinitionColumnDTO;
    }
*/
    public DataDefinitionColumnPK getDataDefinitionColumnPK() {
        return dataDefinitionColumnPK;
    }

    public void setDataDefinitionColumnPK(DataDefinitionColumnPK dataDefinitionColumnPK) {
        this.dataDefinitionColumnPK = dataDefinitionColumnPK;
    }

    /**
     * @return the tranDetailColName
     */
    public String getTableName() {
        return dataDefinitionColumnPK.getTableName();
    }

    /**
     * @return the driverValue
     */
    public String getColumnName() {
        return dataDefinitionColumnPK.getColumnName();
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Long getDatalength() {
        return datalength;
    }

    public void setDatalength(Long datalength) {
        this.datalength = datalength;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbbrDesc() {
        return abbrDesc;
    }

    public void setAbbrDesc(String abbrDesc) {
        this.abbrDesc = abbrDesc;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getDescColumnName() {
        return descColumnName;
    }

    public void setDescColumnName(String descColumnName) {
        this.descColumnName = descColumnName;
    }

    public String getMapFlag() {
        return mapFlag;
    }

    public void setMapFlag(String mapFlag) {
        this.mapFlag = mapFlag;
    }

    public Boolean isMappable() {
        return "1".equals(mapFlag);
    }

    public String getMinimumViewFlag() {
        return minimumViewFlag;
    }

    public void setMinimumViewFlag(String minimumViewFlag) {
        this.minimumViewFlag = minimumViewFlag;
    }

   /* public DataDefinitionTable getDataDefinitionTable() {
        return dataDefinitionTable;
    }

    public void setDataDefinitionTable(DataDefinitionTable dataDefinitionTable) {
        this.dataDefinitionTable = dataDefinitionTable;
    }*/























//



    @Column(table = "TB_DRIVER_NAMES", name="DRIVER_ID", insertable = false, updatable = false)
    private Long driverId;


    @Column(table = "TB_DRIVER_NAMES", name="TRANS_DTL_COLUMN_NAME", insertable = false, updatable = false)
    private String transDtlColName;

    @Column(table = "TB_DRIVER_NAMES", name="MATRIX_COLUMN_NAME", insertable = false, updatable = false )
    private String matrixColName;



  /*  @Column(table = "TB_DRIVER_NAMES", name="TABLE_NAME", insertable = false, updatable = false )
    private String tableName;*/

    @Column(table = "TB_DRIVER_NAMES", name="BUSINESS_UNIT_FLAG", insertable = false, updatable = false )
    private String businessUnitFlag;

    @Column(table = "TB_DRIVER_NAMES", name="MANDATORY_FLAG", insertable = false, updatable = false )
    private String mandatoryFlag;

    @Column(table = "TB_DRIVER_NAMES", name="NULL_DRIVER_FLAG", insertable = false, updatable = false )
    private String nullDriverFlag;

    @Column(table = "TB_DRIVER_NAMES", name="WILDCARD_FLAG", insertable = false, updatable = false )
    private String wildcardFlag;

    @Column(table = "TB_DRIVER_NAMES", name="RANGE_FLAG", insertable = false, updatable = false )
    private String rangeFlag;

    @Column(table = "TB_DRIVER_NAMES", name="TO_NUMBER_FLAG", insertable = false, updatable = false )
    private String toNumberFlag;

    @Column(table = "TB_DRIVER_NAMES", name="ACTIVE_FLAG", insertable = false, updatable = false )
    private String activeFlag;



    @Column (table = "TB_DRIVER_NAMES", name="DRIVER_NAMES_CODE", insertable = false, updatable = false )
    private String driverNamesCode;

    public String getTransDtlColName() {
        return transDtlColName;
    }

    public void setTransDtlColName(String transDtlColName) {
        this.transDtlColName = transDtlColName;
    }

    public String getMatrixColName() {
        return matrixColName;
    }

    public void setMatrixColName(String matrixColName) {
        this.matrixColName = matrixColName;
    }


    public String getBusinessUnitFlag() {
        return businessUnitFlag;
    }

    public void setBusinessUnitFlag(String businessUnitFlag) {
        this.businessUnitFlag = businessUnitFlag;
    }

    public String getMandatoryFlag() {
        return mandatoryFlag;
    }

    public void setMandatoryFlag(String mandatoryFlag) {
        this.mandatoryFlag = mandatoryFlag;
    }

    public String getNullDriverFlag() {
        return nullDriverFlag;
    }

    public void setNullDriverFlag(String nullDriverFlag) {
        this.nullDriverFlag = nullDriverFlag;
    }

    public String getWildcardFlag() {
        return wildcardFlag;
    }

    public void setWildcardFlag(String wildcardFlag) {
        this.wildcardFlag = wildcardFlag;
    }

    public String getRangeFlag() {
        return rangeFlag;
    }

    public void setRangeFlag(String rangeFlag) {
        this.rangeFlag = rangeFlag;
    }

    public String getToNumberFlag() {
        return toNumberFlag;
    }

    public void setToNumberFlag(String toNumberFlag) {
        this.toNumberFlag = toNumberFlag;
    }

    public String getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }


    public String getDriverNamesCode() {
        return driverNamesCode;
    }

    public void setDriverNamesCode(String driverNamesCode) {
        this.driverNamesCode = driverNamesCode;
    }

/*
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tablename) {
        this.tableName = tablename;
    }*/
    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }
}
