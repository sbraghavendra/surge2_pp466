
package com.ncsts.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.ImportSpecDTO;

/**
 *  @author Muneer
 */

@Entity
@Table (name="TB_IMPORT_SPEC")
public class ImportSpec extends Auditable implements java.io.Serializable, Idable<ImportSpecPK> {
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	@EmbeddedId
	ImportSpecPK importSpecPK;	 
  
    @Column(name="DESCRIPTION" )
    private String description;
    
	@Column(name="IMPORT_DEFINITION_CODE" )
    private String importDefinitionCode;
 
	@Column(name="DEFAULT_DIRECTORY" )
    private String defaultDirectory;

	@Column(name="LAST_IMPORT_FILE_NAME" )
    private String lastImportFileName;

	@Column(name="WHERE_CLAUSE" )
    private String whereClause;	

	@Column(name="COMMENTS" )
    private String comments;	
	
	@Column(name="FILESIZEDUPRES")
	private String filesizedupres;
	
	@Column(name="FILENAMEDUPRES")
	private String filenamedupres;
	
	@Column(name="IMPORTDATEMAP")
	private String importdatemap;
	
	@Column(name="DONOTIMPORT")
	private String donotimport;
	
	@Column(name="REPLACEWITH")
	private String replacewith;
	
	@Column(name="BATCHEOFMARKER")
	private String batcheofmarker;
	
	@Column(name="BATCHCOUNTMARKER")
	private String batchcountmarker;
	
	@Column(name="BATCHTOTALMARKER")
	private String batchtotalmarker;
	
	@Column(name="DELETE0AMTFLAG")
	private String delete0amtflag;
	
	@Column(name="HOLDTRANSFLAG")
	private String holdtransflag;
	
	@Column(name="HOLDTRANSAMT")
	private Long holdtransamt;
	
	@Column(name="AUTOPROCFLAG")
	private String autoprocflag;
	
	@Column(name="AUTOPROCAMT")
	private Long autoprocamt;
	
	@Column(name="AUTOPROCID")
	private String autoprocid;
	
	@Column(name="AUTOPROCESS_FLAG")
	private String autoprocessFlag;
	
	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IMPORT_DEFINITION_CODE", insertable=false, updatable=false)
	@ForeignKey(name="fk_tb_import_spec_1")
	private ImportDefinition importDefinition;
	
	@OneToMany(mappedBy="importSpec")
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	private Set<ImportSpecUser> importSpecUser = new HashSet<ImportSpecUser>();

	@Transient	
	ImportSpecDTO importSpecDTO;

	    
    // Constructors

    /** default constructor */
    public ImportSpec() {
    }
    
    public ImportSpec( ImportSpecPK importSpecPK) { 
    	this.importSpecPK = importSpecPK;
    }
    
    public ImportSpecDTO getImportSpecDTO() {
    	ImportSpecDTO importSpecDTO= new ImportSpecDTO();
    	BeanUtils.copyProperties(this,importSpecDTO);
    	return importSpecDTO;
    }
	public void setImportSpecDTO(ImportSpecDTO importSpecDTO) {
		this.importSpecDTO = importSpecDTO;
	}

	public ImportSpecPK getImportSpecPK() {
		return importSpecPK;
	}

	public void setImportSpecPK(ImportSpecPK importSpecPK) {
		this.importSpecPK = importSpecPK;
	}
	
	public String getImportSpecType(){
		return importSpecPK.getImportSpecType();
	}
	public String getImportSpecCode(){
		return importSpecPK.getImportSpecCode();
	}	
	public void setImportSpecType(String importSpecType){
		importSpecPK.setImportSpecType(importSpecType);
	}
	public void setImportSpecCode(String importSpecCode ){
		importSpecPK.setImportSpecCode(importSpecCode);
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImportDefinitionCode() {
		return importDefinitionCode;
	}

	public void setImportDefinitionCode(String importDefinitionCode) {
		this.importDefinitionCode = importDefinitionCode;
	}

	public String getDefaultDirectory() {
		return defaultDirectory;
	}

	public void setDefaultDirectory(String defaultDirectory) {
		this.defaultDirectory = defaultDirectory;
	}

	public String getLastImportFileName() {
		return lastImportFileName;
	}

	public void setLastImportFileName(String lastImportFileName) {
		this.lastImportFileName = lastImportFileName;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public ImportDefinition getImportDefinition() {
		return importDefinition;
	}

	public void setImportDefinition(ImportDefinition importDefinition) {
		this.importDefinition = importDefinition;
	}

	public Set<ImportSpecUser> getImportSpecUser() {
		return importSpecUser;
	}

	public void setImportSpecUser(Set<ImportSpecUser> importSpecUser) {
		this.importSpecUser = importSpecUser;
	}

	public String getFilesizedupres() {
		return filesizedupres;
	}

	public void setFilesizedupres(String filesizedupres) {
		this.filesizedupres = filesizedupres;
	}

	public String getFilenamedupres() {
		return filenamedupres;
	}

	public void setFilenamedupres(String filenamedupres) {
		this.filenamedupres = filenamedupres;
	}

	public String getImportdatemap() {
		return importdatemap;
	}

	public void setImportdatemap(String importdatemap) {
		this.importdatemap = importdatemap;
	}

	public String getDonotimport() {
		return donotimport;
	}

	public void setDonotimport(String donotimport) {
		this.donotimport = donotimport;
	}

	public String getReplacewith() {
		return replacewith;
	}

	public void setReplacewith(String replacewith) {
		this.replacewith = replacewith;
	}

	public String getBatcheofmarker() {
		return batcheofmarker;
	}

	public void setBatcheofmarker(String batcheofmarker) {
		this.batcheofmarker = batcheofmarker;
	}

	public String getBatchcountmarker() {
		return batchcountmarker;
	}

	public void setBatchcountmarker(String batchcountmarker) {
		this.batchcountmarker = batchcountmarker;
	}

	public String getBatchtotalmarker() {
		return batchtotalmarker;
	}

	public void setBatchtotalmarker(String batchtotalmarker) {
		this.batchtotalmarker = batchtotalmarker;
	}

	public String getDelete0amtflag() {
		return delete0amtflag;
	}

	public void setDelete0amtflag(String delete0amtflag) {
		this.delete0amtflag = delete0amtflag;
	}

	public String getHoldtransflag() {
		return holdtransflag;
	}

	public void setHoldtransflag(String holdtransflag) {
		this.holdtransflag = holdtransflag;
	}

	public Long getHoldtransamt() {
		return holdtransamt;
	}

	public void setHoldtransamt(Long holdtransamt) {
		this.holdtransamt = holdtransamt;
	}

	public String getAutoprocflag() {
		return autoprocflag;
	}

	public void setAutoprocflag(String autoprocflag) {
		this.autoprocflag = autoprocflag;
	}

	public Long getAutoprocamt() {
		return autoprocamt;
	}

	public void setAutoprocamt(Long autoprocamt) {
		this.autoprocamt = autoprocamt;
	}

	public String getAutoprocid() {
		return autoprocid;
	}

	public void setAutoprocid(String autoprocid) {
		this.autoprocid = autoprocid;
	}

	public String getAutoprocessFlag() {
		return autoprocessFlag;
	}

	public void setAutoprocessFlag(String autoprocessFlag) {
		this.autoprocessFlag = autoprocessFlag;
	} 
	
	public Boolean getAutoprocessBooleanFlag() {
    	return "1".equals(this.autoprocessFlag);
	}

	public void setAutoprocessBooleanFlag(Boolean autoprocessFlag) {
		this.autoprocessFlag = autoprocessFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public ImportSpecPK getId() {
		return importSpecPK;
	}

	@Override
	public void setId(ImportSpecPK id) {
		importSpecPK = id;
		
	}

	@Override
	public String getIdPropertyName() {
		return "id";
	}
}

