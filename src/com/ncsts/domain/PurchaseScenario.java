package com.ncsts.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.service.impl.adapter.JaxbDateAdapter;

@Entity
@Table(name="TB_PURCHSCENARIO")
@SecondaryTables({
@SecondaryTable(name="TB_PURCHSCENARIO_JURDTL", pkJoinColumns = { @PrimaryKeyJoinColumn(name="PURCHSCENARIO_JURDTL_ID",referencedColumnName="PURCHSCENARIO_ID")}),
@SecondaryTable(name="TB_PURCHSCENARIO_USER", pkJoinColumns = { @PrimaryKeyJoinColumn(name="PURCHSCENARIO_USER_ID", referencedColumnName="PURCHSCENARIO_ID")}),
@SecondaryTable(name="TB_PURCHSCENARIO_LOCN", pkJoinColumns = { @PrimaryKeyJoinColumn(name="PURCHSCENARIO_LOCN_ID", referencedColumnName="PURCHSCENARIO_ID")})
})
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseScenario extends Auditable implements java.io.Serializable, Idable<Long> {

	
	@Id
	@Column(name="PURCHSCENARIO_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="scenario_sequence")
	@SequenceGenerator(name="scenario_sequence" , allocationSize = 1, sequenceName="SQ_TB_PURCHSCENARIO_ID")
	private Long purchScenarioId;
	public void setPurchScenarioId(Long purchscenarioId){
	    this.purchScenarioId = purchscenarioId;
	}
	public Long getPurchScenarioId(){
	    return purchScenarioId;
	}
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return getPurchScenarioId();
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		setPurchScenarioId(id);
	}

	@Override
	public String getIdPropertyName() {
		// TODO Auto-generated method stub
		return "purchscenarioId";
	}
	
	
	
@Column(name = "CITY_TIER1_TAXABLE_AMT")
private BigDecimal cityTier1TaxableAmt;
public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt){
    this.cityTier1TaxableAmt = cityTier1TaxableAmt;
}
public BigDecimal getCityTier1TaxableAmt(){
    return cityTier1TaxableAmt;
}

@Column(name = "DIFF_STATE_TIER2_TAX_AMT")
private BigDecimal diffStateTier2TaxAmt;
public void setDiffStateTier2TaxAmt(BigDecimal diffStateTier2TaxAmt){
    this.diffStateTier2TaxAmt = diffStateTier2TaxAmt;
}
public BigDecimal getDiffStateTier2TaxAmt(){
    return diffStateTier2TaxAmt;
}

@Column(name = "WO_LINE_NAME")
private String woLineName;
public void setWoLineName(String woLineName){
    this.woLineName = woLineName;
}
public String getWoLineName(){
    return woLineName;
}

@Column(name = "STJ9_TAX_AMT")
private BigDecimal stj9TaxAmt;
public void setStj9TaxAmt(BigDecimal stj9TaxAmt){
    this.stj9TaxAmt = stj9TaxAmt;
}
public BigDecimal getStj9TaxAmt(){
    return stj9TaxAmt;
}

@Column(name = "GL_LOCAL_SUB_ACCT_NBR")
private String glLocalSubAcctNbr;
public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr){
    this.glLocalSubAcctNbr = glLocalSubAcctNbr;
}
public String getGlLocalSubAcctNbr(){
    return glLocalSubAcctNbr;
}

@Column(name = "COUNTRY_TIER2_TAX_AMT")
private BigDecimal countryTier2TaxAmt;
public void setCountryTier2TaxAmt(BigDecimal countryTier2TaxAmt){
    this.countryTier2TaxAmt = countryTier2TaxAmt;
}
public BigDecimal getCountryTier2TaxAmt(){
    return countryTier2TaxAmt;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "ENTERED_DATE")
private Date enteredDate;
public void setEnteredDate(Date enteredDate){
    this.enteredDate = enteredDate;
}
public Date getEnteredDate(){
    return enteredDate;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "AUDIT_TIMESTAMP")
private Date auditTimestamp;
public void setAuditTimestamp(Date auditTimestamp){
    this.auditTimestamp = auditTimestamp;
}
public Date getAuditTimestamp(){
    return auditTimestamp;
}

@Column(name = "REJECT_FLAG")
private String rejectFlag;
public void setRejectFlag(String rejectFlag){
    this.rejectFlag = rejectFlag;
}
public String getRejectFlag(){
    return rejectFlag;
}

@Column(name = "STATE_TIER2_TAXABLE_AMT")
private BigDecimal stateTier2TaxableAmt;
public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt){
    this.stateTier2TaxableAmt = stateTier2TaxableAmt;
}
public BigDecimal getStateTier2TaxableAmt(){
    return stateTier2TaxableAmt;
}

@Column(name = "STJ7_TAXABLE_AMT")
private BigDecimal stj7TaxableAmt;
public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt){
    this.stj7TaxableAmt = stj7TaxableAmt;
}
public BigDecimal getStj7TaxableAmt(){
    return stj7TaxableAmt;
}

@Column(name = "GL_LINE_ITM_DIST_AMT")
private BigDecimal glLineItmDistAmt;
public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt){
    this.glLineItmDistAmt = glLineItmDistAmt;
}
public BigDecimal getGlLineItmDistAmt(){
    return glLineItmDistAmt;
}

@Column(name = "DIFF_COUNTY_TIER2_TAX_AMT")
private BigDecimal diffCountyTier2TaxAmt;
public void setDiffCountyTier2TaxAmt(BigDecimal diffCountyTier2TaxAmt){
    this.diffCountyTier2TaxAmt = diffCountyTier2TaxAmt;
}
public BigDecimal getDiffCountyTier2TaxAmt(){
    return diffCountyTier2TaxAmt;
}

@Column(name = "FATAL_ERROR_CODE")
private String fatalErrorCode;
public void setFatalErrorCode(String fatalErrorCode){
    this.fatalErrorCode = fatalErrorCode;
}
public String getFatalErrorCode(){
    return fatalErrorCode;
}

@Column(name = "COMP_TAX_AMT")
private BigDecimal compTaxAmt;
public void setCompTaxAmt(BigDecimal compTaxAmt){
    this.compTaxAmt = compTaxAmt;
}
public BigDecimal getCompTaxAmt(){
    return compTaxAmt;
}

@Column(name = "PRODUCT_CLASS_CODE")
private String productClassCode;
public void setProductClassCode(String productClassCode){
    this.productClassCode = productClassCode;
}
public String getProductClassCode(){
    return productClassCode;
}

@Column(name = "STJ6_TAXABLE_AMT")
private BigDecimal stj6TaxableAmt;
public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt){
    this.stj6TaxableAmt = stj6TaxableAmt;
}
public BigDecimal getStj6TaxableAmt(){
    return stj6TaxableAmt;
}

@Column(name = "DISCOUNT_TYPE")
private String discountType;
public void setDiscountType(String discountType){
    this.discountType = discountType;
}
public String getDiscountType(){
    return discountType;
}

@Column(name = "URL_LINK")
private String urlLink;
public void setUrlLink(String urlLink){
    this.urlLink = urlLink;
}
public String getUrlLink(){
    return urlLink;
}

@Column(name = "SUSPEND_IND")
private String suspendInd;
public void setSuspendInd(String suspendInd){
    this.suspendInd = suspendInd;
}
public String getSuspendInd(){
    return suspendInd;
}

@Column(name = "COUNTRY_TIER1_TAXABLE_AMT")
private BigDecimal countryTier1TaxableAmt;
public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt){
    this.countryTier1TaxableAmt = countryTier1TaxableAmt;
}
public BigDecimal getCountryTier1TaxableAmt(){
    return countryTier1TaxableAmt;
}

@Column(name = "CITY_TIER2_TAXABLE_AMT")
private BigDecimal cityTier2TaxableAmt;
public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt){
    this.cityTier2TaxableAmt = cityTier2TaxableAmt;
}
public BigDecimal getCityTier2TaxableAmt(){
    return cityTier2TaxableAmt;
}

@Column(name = "STJ9_EXEMPT_AMT")
private BigDecimal stj9ExemptAmt;
public void setStj9ExemptAmt(BigDecimal stj9ExemptAmt){
    this.stj9ExemptAmt = stj9ExemptAmt;
}
public BigDecimal getStj9ExemptAmt(){
    return stj9ExemptAmt;
}

@Column(name = "STJ8_TAXABLE_AMT")
private BigDecimal stj8TaxableAmt;
public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt){
    this.stj8TaxableAmt = stj8TaxableAmt;
}
public BigDecimal getStj8TaxableAmt(){
    return stj8TaxableAmt;
}

@Column(name = "VOUCHER_LINE_NBR")
private String voucherLineNbr;
public void setVoucherLineNbr(String voucherLineNbr){
    this.voucherLineNbr = voucherLineNbr;
}
public String getVoucherLineNbr(){
    return voucherLineNbr;
}

@Column(name = "COMMENTS")
private String comments;
public void setComments(String comments){
    this.comments = comments;
}
public String getComments(){
    return comments;
}

@Column(name = "MANUAL_TAXCODE_IND")
private String manualTaxcodeInd;
public void setManualTaxcodeInd(String manualTaxcodeInd){
    this.manualTaxcodeInd = manualTaxcodeInd;
}
public String getManualTaxcodeInd(){
    return manualTaxcodeInd;
}

@Column(name = "STJ2_TAX_AMT")
private BigDecimal stj2TaxAmt;
public void setStj2TaxAmt(BigDecimal stj2TaxAmt){
    this.stj2TaxAmt = stj2TaxAmt;
}
public BigDecimal getStj2TaxAmt(){
    return stj2TaxAmt;
}

@Column(name = "USAGE_NAME")
private String usageName;
public void setUsageName(String usageName){
    this.usageName = usageName;
}
public String getUsageName(){
    return usageName;
}

@Column(name = "TAX_MATRIX_ID")
private Long taxMatrixId;
public void setTaxMatrixId(Long taxMatrixId){
    this.taxMatrixId = taxMatrixId;
}
public Long getTaxMatrixId(){
    return taxMatrixId;
}

@Column(name = "STATE_TIER3_TAXABLE_AMT")
private BigDecimal stateTier3TaxableAmt;
public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt){
    this.stateTier3TaxableAmt = stateTier3TaxableAmt;
}
public BigDecimal getStateTier3TaxableAmt(){
    return stateTier3TaxableAmt;
}

@Column(name = "CHECK_NO")
private Long checkNo;
public void setCheckNo(Long checkNo){
    this.checkNo = checkNo;
}
public Long getCheckNo(){
    return checkNo;
}

@Column(name = "STJ4_EXEMPT_AMT")
private BigDecimal stj4ExemptAmt;
public void setStj4ExemptAmt(BigDecimal stj4ExemptAmt){
    this.stj4ExemptAmt = stj4ExemptAmt;
}
public BigDecimal getStj4ExemptAmt(){
    return stj4ExemptAmt;
}

@Column(name = "STATE_TIER1_TAX_AMT")
private BigDecimal stateTier1TaxAmt;
public void setStateTier1TaxAmt(BigDecimal stateTier1TaxAmt){
    this.stateTier1TaxAmt = stateTier1TaxAmt;
}
public BigDecimal getStateTier1TaxAmt(){
    return stateTier1TaxAmt;
}

@Column(name = "ENTITY_ID")
private Long entityId;
public void setEntityId(Long entityId){
    this.entityId = entityId;
}
public Long getEntityId(){
    return entityId;
}

@Column(name = "INVOICE_DISCOUNT_AMT")
private BigDecimal invoiceDiscountAmt;
public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt){
    this.invoiceDiscountAmt = invoiceDiscountAmt;
}
public BigDecimal getInvoiceDiscountAmt(){
    return invoiceDiscountAmt;
}

@Column(name = "DIFF_STATE_TIER1_TAX_AMT")
private BigDecimal diffStateTier1TaxAmt;
public void setDiffStateTier1TaxAmt(BigDecimal diffStateTier1TaxAmt){
    this.diffStateTier1TaxAmt = diffStateTier1TaxAmt;
}
public BigDecimal getDiffStateTier1TaxAmt(){
    return diffStateTier1TaxAmt;
}

@Column(name = "INVENTORY_NBR")
private String inventoryNbr;
public void setInventoryNbr(String inventoryNbr){
    this.inventoryNbr = inventoryNbr;
}
public String getInventoryNbr(){
    return inventoryNbr;
}

@Column(name = "VENDOR_ADDRESS_ZIPPLUS4")
private String vendorAddressZipplus4;
public void setVendorAddressZipplus4(String vendorAddressZipplus4){
    this.vendorAddressZipplus4 = vendorAddressZipplus4;
}
public String getVendorAddressZipplus4(){
    return vendorAddressZipplus4;
}

@Column(name = "DIFF_STATE_TAX_AMT")
private BigDecimal diffStateTaxAmt;
public void setDiffStateTaxAmt(BigDecimal diffStateTaxAmt){
    this.diffStateTaxAmt = diffStateTaxAmt;
}
public BigDecimal getDiffStateTaxAmt(){
    return diffStateTaxAmt;
}

@Column(name = "STJ10_EXEMPT_AMT")
private BigDecimal stj10ExemptAmt;
public void setStj10ExemptAmt(BigDecimal stj10ExemptAmt){
    this.stj10ExemptAmt = stj10ExemptAmt;
}
public BigDecimal getStj10ExemptAmt(){
    return stj10ExemptAmt;
}

@Column(name = "AUDIT_USER_ID")
private String auditUserId;
public void setAuditUserId(String auditUserId){
    this.auditUserId = auditUserId;
}
public String getAuditUserId(){
    return auditUserId;
}

@Column(name = "COMP_STJ7_TAX_AMT")
private BigDecimal compStj7TaxAmt;
public void setCompStj7TaxAmt(BigDecimal compStj7TaxAmt){
    this.compStj7TaxAmt = compStj7TaxAmt;
}
public BigDecimal getCompStj7TaxAmt(){
    return compStj7TaxAmt;
}

//@Temporal(TemporalType.DATE)
//@XmlJavaTypeAdapter(JaxbDateAdapter.class)
//@Column(name = "UPDATE_TIMESTAMP")
//private Date updateTimestamp;
//public void setUpdateTimestamp(Date updateTimestamp){
//    this.updateTimestamp = updateTimestamp;
//}
//public Date getUpdateTimestamp(){
//    return updateTimestamp;
//}

@Column(name = "INVOICE_LINE_TYPE_NAME")
private String invoiceLineTypeName;
public void setInvoiceLineTypeName(String invoiceLineTypeName){
    this.invoiceLineTypeName = invoiceLineTypeName;
}
public String getInvoiceLineTypeName(){
    return invoiceLineTypeName;
}

@Column(name = "DIFF_CITY_TIER1_TAX_AMT")
private BigDecimal diffCityTier1TaxAmt;
public void setDiffCityTier1TaxAmt(BigDecimal diffCityTier1TaxAmt){
    this.diffCityTier1TaxAmt = diffCityTier1TaxAmt;
}
public BigDecimal getDiffCityTier1TaxAmt(){
    return diffCityTier1TaxAmt;
}

@Column(name = "TRANSACTION_TYPE_CODE")
private String transactionTypeCode;
public void setTransactionTypeCode(String transactionTypeCode){
    this.transactionTypeCode = transactionTypeCode;
}
public String getTransactionTypeCode(){
    return transactionTypeCode;
}

@Column(name = "CITY_TIER2_TAX_AMT")
private BigDecimal cityTier2TaxAmt;
public void setCityTier2TaxAmt(BigDecimal cityTier2TaxAmt){
    this.cityTier2TaxAmt = cityTier2TaxAmt;
}
public BigDecimal getCityTier2TaxAmt(){
    return cityTier2TaxAmt;
}

@Column(name = "VOUCHER_ID")
private Long voucherId;
public void setVoucherId(Long voucherId){
    this.voucherId = voucherId;
}
public Long getVoucherId(){
    return voucherId;
}

@Column(name = "COMP_CITY_TIER1_TAX_AMT")
private BigDecimal compCityTier1TaxAmt;
public void setCompCityTier1TaxAmt(BigDecimal compCityTier1TaxAmt){
    this.compCityTier1TaxAmt = compCityTier1TaxAmt;
}
public BigDecimal getCompCityTier1TaxAmt(){
    return compCityTier1TaxAmt;
}

@Column(name = "INVOICE_LINE_GROSS_AMT")
private BigDecimal invoiceLineGrossAmt;
public void setInvoiceLineGrossAmt(BigDecimal invoiceLineGrossAmt){
    this.invoiceLineGrossAmt = invoiceLineGrossAmt;
}
public BigDecimal getInvoiceLineGrossAmt(){
    return invoiceLineGrossAmt;
}

@Column(name = "ENTITY_CODE")
private String entityCode;
public void setEntityCode(String entityCode){
    this.entityCode = entityCode;
}
public String getEntityCode(){
    return entityCode;
}

@Column(name = "INVOICE_FREIGHT_AMT")
private BigDecimal invoiceFreightAmt;
public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt){
    this.invoiceFreightAmt = invoiceFreightAmt;
}
public BigDecimal getInvoiceFreightAmt(){
    return invoiceFreightAmt;
}

@Column(name = "VENDOR_ID")
private Long vendorId;
public void setVendorId(Long vendorId){
    this.vendorId = vendorId;
}
public Long getVendorId(){
    return vendorId;
}

@Column(name = "COMP_COUNTY_TIER1_TAX_AMT")
private BigDecimal compCountyTier1TaxAmt;
public void setCompCountyTier1TaxAmt(BigDecimal compCountyTier1TaxAmt){
    this.compCountyTier1TaxAmt = compCountyTier1TaxAmt;
}
public BigDecimal getCompCountyTier1TaxAmt(){
    return compCountyTier1TaxAmt;
}

@Column(name = "TAX_DRIVER_30")
private String taxDriver30;
public void setTaxDriver30(String taxDriver30){
    this.taxDriver30 = taxDriver30;
}
public String getTaxDriver30(){
    return taxDriver30;
}

@Column(name = "CITY_TIER3_TAX_AMT")
private BigDecimal cityTier3TaxAmt;
public void setCityTier3TaxAmt(BigDecimal cityTier3TaxAmt){
    this.cityTier3TaxAmt = cityTier3TaxAmt;
}
public BigDecimal getCityTier3TaxAmt(){
    return cityTier3TaxAmt;
}

@Column(name = "COMP_COUNTY_TIER3_TAX_AMT")
private BigDecimal compCountyTier3TaxAmt;
public void setCompCountyTier3TaxAmt(BigDecimal compCountyTier3TaxAmt){
    this.compCountyTier3TaxAmt = compCountyTier3TaxAmt;
}
public BigDecimal getCompCountyTier3TaxAmt(){
    return compCountyTier3TaxAmt;
}

@Column(name = "COMP_COUNTY_TAX_AMT")
private BigDecimal compCountyTaxAmt;
public void setCompCountyTaxAmt(BigDecimal compCountyTaxAmt){
    this.compCountyTaxAmt = compCountyTaxAmt;
}
public BigDecimal getCompCountyTaxAmt(){
    return compCountyTaxAmt;
}

@Column(name = "DIRECT_PAY_PERMIT_FLAG")
private String directPayPermitFlag;
public void setDirectPayPermitFlag(String directPayPermitFlag){
    this.directPayPermitFlag = directPayPermitFlag;
}
public String getDirectPayPermitFlag(){
    return directPayPermitFlag;
}

@Column(name = "MODIFY_USER_ID")
private String modifyUserId;
public void setModifyUserId(String modifyUserId){
    this.modifyUserId = modifyUserId;
}
public String getModifyUserId(){
    return modifyUserId;
}

@Column(name = "DIFF_CITY_TAX_AMT")
private BigDecimal diffCityTaxAmt;
public void setDiffCityTaxAmt(BigDecimal diffCityTaxAmt){
    this.diffCityTaxAmt = diffCityTaxAmt;
}
public BigDecimal getDiffCityTaxAmt(){
    return diffCityTaxAmt;
}

@Column(name = "AFE_PROPERTY_CAT")
private String afePropertyCat;
public void setAfePropertyCat(String afePropertyCat){
    this.afePropertyCat = afePropertyCat;
}
public String getAfePropertyCat(){
    return afePropertyCat;
}

@Column(name = "STJ3_TAXABLE_AMT")
private BigDecimal stj3TaxableAmt;
public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt){
    this.stj3TaxableAmt = stj3TaxableAmt;
}
public BigDecimal getStj3TaxableAmt(){
    return stj3TaxableAmt;
}

@Column(name = "AUDIT_FLAG")
private String auditFlag;
public void setAuditFlag(String auditFlag){
    this.auditFlag = auditFlag;
}
public String getAuditFlag(){
    return auditFlag;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "WO_DATE")
private Date woDate;
public void setWoDate(Date woDate){
    this.woDate = woDate;
}
public Date getWoDate(){
    return woDate;
}

@Column(name = "USAGE_CODE")
private String usageCode;
public void setUsageCode(String usageCode){
    this.usageCode = usageCode;
}
public String getUsageCode(){
    return usageCode;
}

@Column(name = "CREDIT_IND")
private String creditInd;
public void setCreditInd(String creditInd){
    this.creditInd = creditInd;
}
public String getCreditInd(){
    return creditInd;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "LOAD_TIMESTAMP")
private Date loadTimestamp;
public void setLoadTimestamp(Date loadTimestamp){
    this.loadTimestamp = loadTimestamp;
}
public Date getLoadTimestamp(){
    return loadTimestamp;
}

@Column(name = "PURCHSCENARIO_NAME")
private String purchscenarioName;
public void setPurchscenarioName(String purchscenarioName){
    this.purchscenarioName = purchscenarioName;
}
public String getPurchscenarioName(){
    return purchscenarioName;
}

@Column(name = "GL_EXTRACT_UPDATER")
private String glExtractUpdater;
public void setGlExtractUpdater(String glExtractUpdater){
    this.glExtractUpdater = glExtractUpdater;
}
public String getGlExtractUpdater(){
    return glExtractUpdater;
}

@Column(name = "COMP_STJ9_TAX_AMT")
private BigDecimal compStj9TaxAmt;
public void setCompStj9TaxAmt(BigDecimal compStj9TaxAmt){
    this.compStj9TaxAmt = compStj9TaxAmt;
}
public BigDecimal getCompStj9TaxAmt(){
    return compStj9TaxAmt;
}

@Column(name = "STJ3_EXEMPT_AMT")
private BigDecimal stj3ExemptAmt;
public void setStj3ExemptAmt(BigDecimal stj3ExemptAmt){
    this.stj3ExemptAmt = stj3ExemptAmt;
}
public BigDecimal getStj3ExemptAmt(){
    return stj3ExemptAmt;
}

@Column(name = "GL_DIVISION_NAME")
private String glDivisionName;
public void setGlDivisionName(String glDivisionName){
    this.glDivisionName = glDivisionName;
}
public String getGlDivisionName(){
    return glDivisionName;
}

@Column(name = "TRANSACTION_STATE_CODE")
private String transactionStateCode;
public void setTransactionStateCode(String transactionStateCode){
    this.transactionStateCode = transactionStateCode;
}
public String getTransactionStateCode(){
    return transactionStateCode;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "GL_DATE")
private Date glDate;
public void setGlDate(Date glDate){
    this.glDate = glDate;
}
public Date getGlDate(){
    return glDate;
}

@Column(name = "TAX_DRIVER_25")
private String taxDriver25;
public void setTaxDriver25(String taxDriver25){
    this.taxDriver25 = taxDriver25;
}
public String getTaxDriver25(){
    return taxDriver25;
}

@Column(name = "TAX_DRIVER_26")
private String taxDriver26;
public void setTaxDriver26(String taxDriver26){
    this.taxDriver26 = taxDriver26;
}
public String getTaxDriver26(){
    return taxDriver26;
}

@Column(name = "INVENTORY_NAME")
private String inventoryName;
public void setInventoryName(String inventoryName){
    this.inventoryName = inventoryName;
}
public String getInventoryName(){
    return inventoryName;
}

@Column(name = "TAX_DRIVER_23")
private String taxDriver23;
public void setTaxDriver23(String taxDriver23){
    this.taxDriver23 = taxDriver23;
}
public String getTaxDriver23(){
    return taxDriver23;
}

@Column(name = "DIFF_STJ4_TAX_AMT")
private BigDecimal diffStj4TaxAmt;
public void setDiffStj4TaxAmt(BigDecimal diffStj4TaxAmt){
    this.diffStj4TaxAmt = diffStj4TaxAmt;
}
public BigDecimal getDiffStj4TaxAmt(){
    return diffStj4TaxAmt;
}

@Column(name = "TAX_DRIVER_24")
private String taxDriver24;
public void setTaxDriver24(String taxDriver24){
    this.taxDriver24 = taxDriver24;
}
public String getTaxDriver24(){
    return taxDriver24;
}

@Column(name = "TAX_DRIVER_21")
private String taxDriver21;
public void setTaxDriver21(String taxDriver21){
    this.taxDriver21 = taxDriver21;
}
public String getTaxDriver21(){
    return taxDriver21;
}

@Column(name = "TAX_DRIVER_22")
private String taxDriver22;
public void setTaxDriver22(String taxDriver22){
    this.taxDriver22 = taxDriver22;
}
public String getTaxDriver22(){
    return taxDriver22;
}

@Column(name = "SPLIT_SUBTRANS_ID")
private Long splitSubtransId;
public void setSplitSubtransId(Long splitSubtransId){
    this.splitSubtransId = splitSubtransId;
}
public Long getSplitSubtransId(){
    return splitSubtransId;
}

@Column(name = "DIFF_STJ8_TAX_AMT")
private BigDecimal diffStj8TaxAmt;
public void setDiffStj8TaxAmt(BigDecimal diffStj8TaxAmt){
    this.diffStj8TaxAmt = diffStj8TaxAmt;
}
public BigDecimal getDiffStj8TaxAmt(){
    return diffStj8TaxAmt;
}

@Column(name = "INVOICE_LINE_AMT")
private BigDecimal invoiceLineAmt;
public void setInvoiceLineAmt(BigDecimal invoiceLineAmt){
    this.invoiceLineAmt = invoiceLineAmt;
}
public BigDecimal getInvoiceLineAmt(){
    return invoiceLineAmt;
}

@Column(name = "TAX_DRIVER_20")
private String taxDriver20;
public void setTaxDriver20(String taxDriver20){
    this.taxDriver20 = taxDriver20;
}
public String getTaxDriver20(){
    return taxDriver20;
}

@Column(name = "GL_CC_NBR_DEPT_ID")
private Long glCcNbrDeptId;
public void setGlCcNbrDeptId(Long glCcNbrDeptId){
    this.glCcNbrDeptId = glCcNbrDeptId;
}
public Long getGlCcNbrDeptId(){
    return glCcNbrDeptId;
}

@Column(name = "TAX_DRIVER_29")
private String taxDriver29;
public void setTaxDriver29(String taxDriver29){
    this.taxDriver29 = taxDriver29;
}
public String getTaxDriver29(){
    return taxDriver29;
}

@Column(name = "AUTO_TRANSACTION_STATE_CODE")
private String autoTransactionStateCode;
public void setAutoTransactionStateCode(String autoTransactionStateCode){
    this.autoTransactionStateCode = autoTransactionStateCode;
}
public String getAutoTransactionStateCode(){
    return autoTransactionStateCode;
}

@Column(name = "VOUCHER_NAME")
private String voucherName;
public void setVoucherName(String voucherName){
    this.voucherName = voucherName;
}
public String getVoucherName(){
    return voucherName;
}

@Column(name = "GL_DIVISION_NBR")
private String glDivisionNbr;
public void setGlDivisionNbr(String glDivisionNbr){
    this.glDivisionNbr = glDivisionNbr;
}
public String getGlDivisionNbr(){
    return glDivisionNbr;
}

@Column(name = "TAX_DRIVER_27")
private String taxDriver27;
public void setTaxDriver27(String taxDriver27){
    this.taxDriver27 = taxDriver27;
}
public String getTaxDriver27(){
    return taxDriver27;
}

@Column(name = "DIFF_COUNTRY_TAX_AMT")
private BigDecimal diffCountryTaxAmt;
public void setDiffCountryTaxAmt(BigDecimal diffCountryTaxAmt){
    this.diffCountryTaxAmt = diffCountryTaxAmt;
}
public BigDecimal getDiffCountryTaxAmt(){
    return diffCountryTaxAmt;
}

@Column(name = "ALLOCATION_MATRIX_ID")
private Long allocationMatrixId;
public void setAllocationMatrixId(Long allocationMatrixId){
    this.allocationMatrixId = allocationMatrixId;
}
public Long getAllocationMatrixId(){
    return allocationMatrixId;
}

@Column(name = "WO_SHUT_DOWN_CD_DESC")
private String woShutDownCdDesc;
public void setWoShutDownCdDesc(String woShutDownCdDesc){
    this.woShutDownCdDesc = woShutDownCdDesc;
}
public String getWoShutDownCdDesc(){
    return woShutDownCdDesc;
}

@Column(name = "TAX_DRIVER_28")
private String taxDriver28;
public void setTaxDriver28(String taxDriver28){
    this.taxDriver28 = taxDriver28;
}
public String getTaxDriver28(){
    return taxDriver28;
}

@Column(name = "STJ3_TAX_AMT")
private BigDecimal stj3TaxAmt;
public void setStj3TaxAmt(BigDecimal stj3TaxAmt){
    this.stj3TaxAmt = stj3TaxAmt;
}
public BigDecimal getStj3TaxAmt(){
    return stj3TaxAmt;
}

@Column(name = "INVOICE_LINE_FREIGHT_AMT")
private BigDecimal invoiceLineFreightAmt;
public void setInvoiceLineFreightAmt(BigDecimal invoiceLineFreightAmt){
    this.invoiceLineFreightAmt = invoiceLineFreightAmt;
}
public BigDecimal getInvoiceLineFreightAmt(){
    return invoiceLineFreightAmt;
}

@Column(name = "COUNTY_TIER2_TAXABLE_AMT")
private BigDecimal countyTier2TaxableAmt;
public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt){
    this.countyTier2TaxableAmt = countyTier2TaxableAmt;
}
public BigDecimal getCountyTier2TaxableAmt(){
    return countyTier2TaxableAmt;
}

@Column(name = "STJ5_TAXABLE_AMT")
private BigDecimal stj5TaxableAmt;
public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt){
    this.stj5TaxableAmt = stj5TaxableAmt;
}
public BigDecimal getStj5TaxableAmt(){
    return stj5TaxableAmt;
}

@Column(name = "DISTR_03_AMT")
private BigDecimal distr03Amt;
public void setDistr03Amt(BigDecimal distr03Amt){
    this.distr03Amt = distr03Amt;
}
public BigDecimal getDistr03Amt(){
    return distr03Amt;
}

@Column(name = "DIFF_STATE_TIER3_TAX_AMT")
private BigDecimal diffStateTier3TaxAmt;
public void setDiffStateTier3TaxAmt(BigDecimal diffStateTier3TaxAmt){
    this.diffStateTier3TaxAmt = diffStateTier3TaxAmt;
}
public BigDecimal getDiffStateTier3TaxAmt(){
    return diffStateTier3TaxAmt;
}

@Column(name = "COUNTY_LOCAL_TAX_AMT")
private BigDecimal countyLocalTaxAmt;
public void setCountyLocalTaxAmt(BigDecimal countyLocalTaxAmt){
    this.countyLocalTaxAmt = countyLocalTaxAmt;
}
public BigDecimal getCountyLocalTaxAmt(){
    return countyLocalTaxAmt;
}

@Column(name = "INVOICE_LINE_COUNT")
private BigDecimal invoiceLineCount;
public void setInvoiceLineCount(BigDecimal invoiceLineCount){
    this.invoiceLineCount = invoiceLineCount;
}
public BigDecimal getInvoiceLineCount(){
    return invoiceLineCount;
}

@Column(name = "INVOICE_LINE_TYPE")
private String invoiceLineType;
public void setInvoiceLineType(String invoiceLineType){
    this.invoiceLineType = invoiceLineType;
}
public String getInvoiceLineType(){
    return invoiceLineType;
}

@Column(name = "PROCRULE_SESSION_ID")
private Long procruleSessionId;
public void setProcruleSessionId(Long procruleSessionId){
    this.procruleSessionId = procruleSessionId;
}
public Long getProcruleSessionId(){
    return procruleSessionId;
}

@Column(name = "WO_SHUT_DOWN_CD")
private String woShutDownCd;
public void setWoShutDownCd(String woShutDownCd){
    this.woShutDownCd = woShutDownCd;
}
public String getWoShutDownCd(){
    return woShutDownCd;
}

@Column(name = "VOUCHER_LINE_DESC")
private String voucherLineDesc;
public void setVoucherLineDesc(String voucherLineDesc){
    this.voucherLineDesc = voucherLineDesc;
}
public String getVoucherLineDesc(){
    return voucherLineDesc;
}

@Column(name = "DIFF_STJ2_TAX_AMT")
private BigDecimal diffStj2TaxAmt;
public void setDiffStj2TaxAmt(BigDecimal diffStj2TaxAmt){
    this.diffStj2TaxAmt = diffStj2TaxAmt;
}
public BigDecimal getDiffStj2TaxAmt(){
    return diffStj2TaxAmt;
}

@Column(name = "COMP_STJ6_TAX_AMT")
private BigDecimal compStj6TaxAmt;
public void setCompStj6TaxAmt(BigDecimal compStj6TaxAmt){
    this.compStj6TaxAmt = compStj6TaxAmt;
}
public BigDecimal getCompStj6TaxAmt(){
    return compStj6TaxAmt;
}

@Column(name = "RELATED_SUBTRANS_ID")
private Long relatedSubtransId;
public void setRelatedSubtransId(Long relatedSubtransId){
    this.relatedSubtransId = relatedSubtransId;
}
public Long getRelatedSubtransId(){
    return relatedSubtransId;
}

@Column(name = "AFE_CONTRACT_STRUCTURE")
private String afeContractStructure;
public void setAfeContractStructure(String afeContractStructure){
    this.afeContractStructure = afeContractStructure;
}
public String getAfeContractStructure(){
    return afeContractStructure;
}

@Column(name = "DIFF_CITY_TIER3_TAX_AMT")
private BigDecimal diffCityTier3TaxAmt;
public void setDiffCityTier3TaxAmt(BigDecimal diffCityTier3TaxAmt){
    this.diffCityTier3TaxAmt = diffCityTier3TaxAmt;
}
public BigDecimal getDiffCityTier3TaxAmt(){
    return diffCityTier3TaxAmt;
}

@Column(name = "CITY_TIER1_TAX_AMT")
private BigDecimal cityTier1TaxAmt;
public void setCityTier1TaxAmt(BigDecimal cityTier1TaxAmt){
    this.cityTier1TaxAmt = cityTier1TaxAmt;
}
public BigDecimal getCityTier1TaxAmt(){
    return cityTier1TaxAmt;
}

@Column(name = "INVOICE_LINE_DISC_AMT")
private BigDecimal invoiceLineDiscAmt;
public void setInvoiceLineDiscAmt(BigDecimal invoiceLineDiscAmt){
    this.invoiceLineDiscAmt = invoiceLineDiscAmt;
}
public BigDecimal getInvoiceLineDiscAmt(){
    return invoiceLineDiscAmt;
}

@Column(name = "PRODUCT_CLASS_NAME")
private String productClassName;
public void setProductClassName(String productClassName){
    this.productClassName = productClassName;
}
public String getProductClassName(){
    return productClassName;
}

@Column(name = "AUTO_TRANSACTION_COUNTRY_CODE")
private String autoTransactionCountryCode;
public void setAutoTransactionCountryCode(String autoTransactionCountryCode){
    this.autoTransactionCountryCode = autoTransactionCountryCode;
}
public String getAutoTransactionCountryCode(){
    return autoTransactionCountryCode;
}

@Column(name = "STJ6_TAX_AMT")
private BigDecimal stj6TaxAmt;
public void setStj6TaxAmt(BigDecimal stj6TaxAmt){
    this.stj6TaxAmt = stj6TaxAmt;
}
public BigDecimal getStj6TaxAmt(){
    return stj6TaxAmt;
}

@Column(name = "COUNTRY_TIER3_TAX_AMT")
private BigDecimal countryTier3TaxAmt;
public void setCountryTier3TaxAmt(BigDecimal countryTier3TaxAmt){
    this.countryTier3TaxAmt = countryTier3TaxAmt;
}
public BigDecimal getCountryTier3TaxAmt(){
    return countryTier3TaxAmt;
}

@Column(name = "PO_LINE_TYPE")
private String poLineType;
public void setPoLineType(String poLineType){
    this.poLineType = poLineType;
}
public String getPoLineType(){
    return poLineType;
}

@Column(name = "DIFF_COUNTY_TIER3_TAX_AMT")
private BigDecimal diffCountyTier3TaxAmt;
public void setDiffCountyTier3TaxAmt(BigDecimal diffCountyTier3TaxAmt){
    this.diffCountyTier3TaxAmt = diffCountyTier3TaxAmt;
}
public BigDecimal getDiffCountyTier3TaxAmt(){
    return diffCountyTier3TaxAmt;
}

@Column(name = "COUNTY_TIER1_TAX_AMT")
private BigDecimal countyTier1TaxAmt;
public void setCountyTier1TaxAmt(BigDecimal countyTier1TaxAmt){
    this.countyTier1TaxAmt = countyTier1TaxAmt;
}
public BigDecimal getCountyTier1TaxAmt(){
    return countyTier1TaxAmt;
}

@Column(name = "PO_NAME")
private String poName;
public void setPoName(String poName){
    this.poName = poName;
}
public String getPoName(){
    return poName;
}

@Column(name = "AFE_USE")
private String afeUse;
public void setAfeUse(String afeUse){
    this.afeUse = afeUse;
}
public String getAfeUse(){
    return afeUse;
}

@Column(name = "DIFF_STJ5_TAX_AMT")
private BigDecimal diffStj5TaxAmt;
public void setDiffStj5TaxAmt(BigDecimal diffStj5TaxAmt){
    this.diffStj5TaxAmt = diffStj5TaxAmt;
}
public BigDecimal getDiffStj5TaxAmt(){
    return diffStj5TaxAmt;
}

@Column(name = "DIFF_TAX_AMT")
private BigDecimal diffTaxAmt;
public void setDiffTaxAmt(BigDecimal diffTaxAmt){
    this.diffTaxAmt = diffTaxAmt;
}
public BigDecimal getDiffTaxAmt(){
    return diffTaxAmt;
}

@Column(name = "CHECK_AMT")
private BigDecimal checkAmt;
public void setCheckAmt(BigDecimal checkAmt){
    this.checkAmt = checkAmt;
}
public BigDecimal getCheckAmt(){
    return checkAmt;
}

//@Column(name = "UPDATE_USER_ID")
//private String updateUserId;
//public void setUpdateUserId(String updateUserId){
//    this.updateUserId = updateUserId;
//}
//public String getUpdateUserId(){
//    return updateUserId;
//}

@Column(name = "GL_CC_NBR_DEPT_NAME")
private String glCcNbrDeptName;
public void setGlCcNbrDeptName(String glCcNbrDeptName){
    this.glCcNbrDeptName = glCcNbrDeptName;
}
public String getGlCcNbrDeptName(){
    return glCcNbrDeptName;
}

@Column(name = "CHECK_DESC")
private String checkDesc;
public void setCheckDesc(String checkDesc){
    this.checkDesc = checkDesc;
}
public String getCheckDesc(){
    return checkDesc;
}

@Column(name = "GL_EXTRACT_FLAG")
private String glExtractFlag;
public void setGlExtractFlag(String glExtractFlag){
    this.glExtractFlag = glExtractFlag;
}
public String getGlExtractFlag(){
    return glExtractFlag;
}

@Column(name = "INVOICE_LINE_NAME")
private String invoiceLineName;
public void setInvoiceLineName(String invoiceLineName){
    this.invoiceLineName = invoiceLineName;
}
public String getInvoiceLineName(){
    return invoiceLineName;
}

@Column(name = "COMP_STATE_TIER3_TAX_AMT")
private BigDecimal compStateTier3TaxAmt;
public void setCompStateTier3TaxAmt(BigDecimal compStateTier3TaxAmt){
    this.compStateTier3TaxAmt = compStateTier3TaxAmt;
}
public BigDecimal getCompStateTier3TaxAmt(){
    return compStateTier3TaxAmt;
}

@Column(name = "ALLOCATION_SUBTRANS_ID")
private Long allocationSubtransId;
public void setAllocationSubtransId(Long allocationSubtransId){
    this.allocationSubtransId = allocationSubtransId;
}
public Long getAllocationSubtransId(){
    return allocationSubtransId;
}

@Column(name = "INVOICE_DISC_TYPE_CODE")
private String invoiceDiscTypeCode;
public void setInvoiceDiscTypeCode(String invoiceDiscTypeCode){
    this.invoiceDiscTypeCode = invoiceDiscTypeCode;
}
public String getInvoiceDiscTypeCode(){
    return invoiceDiscTypeCode;
}

@Column(name = "CHECK_NBR")
private String checkNbr;
public void setCheckNbr(String checkNbr){
    this.checkNbr = checkNbr;
}
public String getCheckNbr(){
    return checkNbr;
}

@Column(name = "STJ10_TAXABLE_AMT")
private BigDecimal stj10TaxableAmt;
public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt){
    this.stj10TaxableAmt = stj10TaxableAmt;
}
public BigDecimal getStj10TaxableAmt(){
    return stj10TaxableAmt;
}

@Column(name = "GL_LOCAL_SUB_ACCT_NAME")
private String glLocalSubAcctName;
public void setGlLocalSubAcctName(String glLocalSubAcctName){
    this.glLocalSubAcctName = glLocalSubAcctName;
}
public String getGlLocalSubAcctName(){
    return glLocalSubAcctName;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "CHECK_DATE")
private Date checkDate;
public void setCheckDate(Date checkDate){
    this.checkDate = checkDate;
}
public Date getCheckDate(){
    return checkDate;
}

@Column(name = "REPROCESS_CODE")
private String reprocessCode;
public void setReprocessCode(String reprocessCode){
    this.reprocessCode = reprocessCode;
}
public String getReprocessCode(){
    return reprocessCode;
}

@Column(name = "GL_FULL_ACCT_NBR")
private String glFullAcctNbr;
public void setGlFullAcctNbr(String glFullAcctNbr){
    this.glFullAcctNbr = glFullAcctNbr;
}
public String getGlFullAcctNbr(){
    return glFullAcctNbr;
}

@Column(name = "ENTITY_LOCN_SET_DTL_ID")
private Long entityLocnSetDtlId;
public void setEntityLocnSetDtlId(Long entityLocnSetDtlId){
    this.entityLocnSetDtlId = entityLocnSetDtlId;
}
public Long getEntityLocnSetDtlId(){
    return entityLocnSetDtlId;
}

@Column(name = "INVOICE_LINE_PRODUCT_CODE")
private String invoiceLineProductCode;
public void setInvoiceLineProductCode(String invoiceLineProductCode){
    this.invoiceLineProductCode = invoiceLineProductCode;
}
public String getInvoiceLineProductCode(){
    return invoiceLineProductCode;
}

@Column(name = "TB_CALC_TAX_AMT")
private BigDecimal tbCalcTaxAmt;
public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt){
    this.tbCalcTaxAmt = tbCalcTaxAmt;
}
public BigDecimal getTbCalcTaxAmt(){
    return tbCalcTaxAmt;
}

@Column(name = "DISTR_04_AMT")
private BigDecimal distr04Amt;
public void setDistr04Amt(BigDecimal distr04Amt){
    this.distr04Amt = distr04Amt;
}
public BigDecimal getDistr04Amt(){
    return distr04Amt;
}

@Column(name = "STJ1_TAXABLE_AMT")
private BigDecimal stj1TaxableAmt;
public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt){
    this.stj1TaxableAmt = stj1TaxableAmt;
}
public BigDecimal getStj1TaxableAmt(){
    return stj1TaxableAmt;
}

@Column(name = "DIFF_STJ3_TAX_AMT")
private BigDecimal diffStj3TaxAmt;
public void setDiffStj3TaxAmt(BigDecimal diffStj3TaxAmt){
    this.diffStj3TaxAmt = diffStj3TaxAmt;
}
public BigDecimal getDiffStj3TaxAmt(){
    return diffStj3TaxAmt;
}

@Column(name = "DIFF_STJ1_TAX_AMT")
private BigDecimal diffStj1TaxAmt;
public void setDiffStj1TaxAmt(BigDecimal diffStj1TaxAmt){
    this.diffStj1TaxAmt = diffStj1TaxAmt;
}
public BigDecimal getDiffStj1TaxAmt(){
    return diffStj1TaxAmt;
}

@Column(name = "DIFF_COUNTY_TIER1_TAX_AMT")
private BigDecimal diffCountyTier1TaxAmt;
public void setDiffCountyTier1TaxAmt(BigDecimal diffCountyTier1TaxAmt){
    this.diffCountyTier1TaxAmt = diffCountyTier1TaxAmt;
}
public BigDecimal getDiffCountyTier1TaxAmt(){
    return diffCountyTier1TaxAmt;
}

@Column(name = "STJ4_TAXABLE_AMT")
private BigDecimal stj4TaxableAmt;
public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt){
    this.stj4TaxableAmt = stj4TaxableAmt;
}
public BigDecimal getStj4TaxableAmt(){
    return stj4TaxableAmt;
}

@Column(name = "WO_ENTITY")
private String woEntity;
public void setWoEntity(String woEntity){
    this.woEntity = woEntity;
}
public String getWoEntity(){
    return woEntity;
}

@Column(name = "STJ9_TAXABLE_AMT")
private BigDecimal stj9TaxableAmt;
public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt){
    this.stj9TaxableAmt = stj9TaxableAmt;
}
public BigDecimal getStj9TaxableAmt(){
    return stj9TaxableAmt;
}

@Column(name = "GL_LOCAL_ACCT_NAME")
private String glLocalAcctName;
public void setGlLocalAcctName(String glLocalAcctName){
    this.glLocalAcctName = glLocalAcctName;
}
public String getGlLocalAcctName(){
    return glLocalAcctName;
}

@Column(name = "PO_NBR")
private String poNbr;
public void setPoNbr(String poNbr){
    this.poNbr = poNbr;
}
public String getPoNbr(){
    return poNbr;
}

@Column(name = "INVOICE_LINE_TAX")
private String invoiceLineTax;
public void setInvoiceLineTax(String invoiceLineTax){
    this.invoiceLineTax = invoiceLineTax;
}
public String getInvoiceLineTax(){
    return invoiceLineTax;
}

@Column(name = "COUNTRY_TIER1_TAX_AMT")
private BigDecimal countryTier1TaxAmt;
public void setCountryTier1TaxAmt(BigDecimal countryTier1TaxAmt){
    this.countryTier1TaxAmt = countryTier1TaxAmt;
}
public BigDecimal getCountryTier1TaxAmt(){
    return countryTier1TaxAmt;
}

@Column(name = "COMP_COUNTRY_TIER1_TAX_AMT")
private BigDecimal compCountryTier1TaxAmt;
public void setCompCountryTier1TaxAmt(BigDecimal compCountryTier1TaxAmt){
    this.compCountryTier1TaxAmt = compCountryTier1TaxAmt;
}
public BigDecimal getCompCountryTier1TaxAmt(){
    return compCountryTier1TaxAmt;
}

@Column(name = "TAX_ALLOC_MATRIX_ID")
private Long taxAllocMatrixId;
public void setTaxAllocMatrixId(Long taxAllocMatrixId){
    this.taxAllocMatrixId = taxAllocMatrixId;
}
public Long getTaxAllocMatrixId(){
    return taxAllocMatrixId;
}

@Column(name = "EXEMPT_REASON")
private String exemptReason;
public void setExemptReason(String exemptReason){
    this.exemptReason = exemptReason;
}
public String getExemptReason(){
    return exemptReason;
}

@Column(name = "INVOICE_LINE_WEIGHT")
private BigDecimal invoiceLineWeight;
public void setInvoiceLineWeight(BigDecimal invoiceLineWeight){
    this.invoiceLineWeight = invoiceLineWeight;
}
public BigDecimal getInvoiceLineWeight(){
    return invoiceLineWeight;
}

@Column(name = "STJ1_EXEMPT_AMT")
private BigDecimal stj1ExemptAmt;
public void setStj1ExemptAmt(BigDecimal stj1ExemptAmt){
    this.stj1ExemptAmt = stj1ExemptAmt;
}
public BigDecimal getStj1ExemptAmt(){
    return stj1ExemptAmt;
}

@Column(name = "VENDOR_ADDRESS_COUNTRY")
private String vendorAddressCountry;
public void setVendorAddressCountry(String vendorAddressCountry){
    this.vendorAddressCountry = vendorAddressCountry;
}
public String getVendorAddressCountry(){
    return vendorAddressCountry;
}

@Column(name = "GL_FULL_ACCT_NAME")
private String glFullAcctName;
public void setGlFullAcctName(String glFullAcctName){
    this.glFullAcctName = glFullAcctName;
}
public String getGlFullAcctName(){
    return glFullAcctName;
}

@Column(name = "DIFF_COUNTRY_TIER1_TAX_AMT")
private BigDecimal diffCountryTier1TaxAmt;
public void setDiffCountryTier1TaxAmt(BigDecimal diffCountryTier1TaxAmt){
    this.diffCountryTier1TaxAmt = diffCountryTier1TaxAmt;
}
public BigDecimal getDiffCountryTier1TaxAmt(){
    return diffCountryTier1TaxAmt;
}

@Column(name = "DIFF_CITY_TIER2_TAX_AMT")
private BigDecimal diffCityTier2TaxAmt;
public void setDiffCityTier2TaxAmt(BigDecimal diffCityTier2TaxAmt){
    this.diffCityTier2TaxAmt = diffCityTier2TaxAmt;
}
public BigDecimal getDiffCityTier2TaxAmt(){
    return diffCityTier2TaxAmt;
}

@Column(name = "RATETYPE_CODE")
private String ratetypeCode;
public void setRatetypeCode(String ratetypeCode){
    this.ratetypeCode = ratetypeCode;
}
public String getRatetypeCode(){
    return ratetypeCode;
}

@Column(name = "INVOICE_NBR")
private String invoiceNbr;
public void setInvoiceNbr(String invoiceNbr){
    this.invoiceNbr = invoiceNbr;
}
public String getInvoiceNbr(){
    return invoiceNbr;
}

@Column(name = "STJ7_TAX_AMT")
private BigDecimal stj7TaxAmt;
public void setStj7TaxAmt(BigDecimal stj7TaxAmt){
    this.stj7TaxAmt = stj7TaxAmt;
}
public BigDecimal getStj7TaxAmt(){
    return stj7TaxAmt;
}

@Column(name = "PROCESSING_NOTES")
private String processingNotes;
public void setProcessingNotes(String processingNotes){
    this.processingNotes = processingNotes;
}
public String getProcessingNotes(){
    return processingNotes;
}
//Cut
@Column(name = "COMP_STJ2_TAX_AMT")
private BigDecimal compStj2TaxAmt;
public void setCompStj2TaxAmt(BigDecimal compStj2TaxAmt){
    this.compStj2TaxAmt = compStj2TaxAmt;
}
public BigDecimal getCompStj2TaxAmt(){
    return compStj2TaxAmt;
}

@Column(name = "VENDOR_ADDRESS_ZIP")
private String vendorAddressZip;
public void setVendorAddressZip(String vendorAddressZip){
    this.vendorAddressZip = vendorAddressZip;
}
public String getVendorAddressZip(){
    return vendorAddressZip;
}

@Column(name = "VENDOR_TYPE")
private String vendorType;
public void setVendorType(String vendorType){
    this.vendorType = vendorType;
}
public String getVendorType(){
    return vendorType;
}

@Column(name = "COMP_STATE_TIER2_TAX_AMT")
private BigDecimal compStateTier2TaxAmt;
public void setCompStateTier2TaxAmt(BigDecimal compStateTier2TaxAmt){
    this.compStateTier2TaxAmt = compStateTier2TaxAmt;
}
public BigDecimal getCompStateTier2TaxAmt(){
    return compStateTier2TaxAmt;
}

@Column(name = "WO_LINE_TYPE_DESC")
private String woLineTypeDesc;
public void setWoLineTypeDesc(String woLineTypeDesc){
    this.woLineTypeDesc = woLineTypeDesc;
}
public String getWoLineTypeDesc(){
    return woLineTypeDesc;
}

@Column(name = "DIFF_COUNTRY_TIER3_TAX_AMT")
private BigDecimal diffCountryTier3TaxAmt;
public void setDiffCountryTier3TaxAmt(BigDecimal diffCountryTier3TaxAmt){
    this.diffCountryTier3TaxAmt = diffCountryTier3TaxAmt;
}
public BigDecimal getDiffCountryTier3TaxAmt(){
    return diffCountryTier3TaxAmt;
}

@Column(name = "INVENTORY_CLASS")
private String inventoryClass;
public void setInventoryClass(String inventoryClass){
    this.inventoryClass = inventoryClass;
}
public String getInventoryClass(){
    return inventoryClass;
}

@Column(name = "DISTR_09_AMT")
private BigDecimal distr09Amt;
public void setDistr09Amt(BigDecimal distr09Amt){
    this.distr09Amt = distr09Amt;
}
public BigDecimal getDistr09Amt(){
    return distr09Amt;
}

@Column(name = "DIFF_STJ7_TAX_AMT")
private BigDecimal diffStj7TaxAmt;
public void setDiffStj7TaxAmt(BigDecimal diffStj7TaxAmt){
    this.diffStj7TaxAmt = diffStj7TaxAmt;
}
public BigDecimal getDiffStj7TaxAmt(){
    return diffStj7TaxAmt;
}

@Column(name = "COMP_CITY_TAX_AMT")
private BigDecimal compCityTaxAmt;
public void setCompCityTaxAmt(BigDecimal compCityTaxAmt){
    this.compCityTaxAmt = compCityTaxAmt;
}
public BigDecimal getCompCityTaxAmt(){
    return compCityTaxAmt;
}

@Column(name = "STATE_TIER1_TAXABLE_AMT")
private BigDecimal stateTier1TaxableAmt;
public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt){
    this.stateTier1TaxableAmt = stateTier1TaxableAmt;
}
public BigDecimal getStateTier1TaxableAmt(){
    return stateTier1TaxableAmt;
}

@Column(name = "STJ4_TAX_AMT")
private BigDecimal stj4TaxAmt;
public void setStj4TaxAmt(BigDecimal stj4TaxAmt){
    this.stj4TaxAmt = stj4TaxAmt;
}
public BigDecimal getStj4TaxAmt(){
    return stj4TaxAmt;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "MODIFY_TIMESTAMP")
private Date modifyTimestamp;
public void setModifyTimestamp(Date modifyTimestamp){
    this.modifyTimestamp = modifyTimestamp;
}
public Date getModifyTimestamp(){
    return modifyTimestamp;
}

@Column(name = "VENDOR_NAME")
private String vendorName;
public void setVendorName(String vendorName){
    this.vendorName = vendorName;
}
public String getVendorName(){
    return vendorName;
}

@Column(name = "DISTR_06_AMT")
private BigDecimal distr06Amt;
public void setDistr06Amt(BigDecimal distr06Amt){
    this.distr06Amt = distr06Amt;
}
public BigDecimal getDistr06Amt(){
    return distr06Amt;
}

@Column(name = "GL_EXTRACT_BATCH_NO")
private Long glExtractBatchNo;
public void setGlExtractBatchNo(Long glExtractBatchNo){
    this.glExtractBatchNo = glExtractBatchNo;
}
public Long getGlExtractBatchNo(){
    return glExtractBatchNo;
}

@Column(name = "STJ6_EXEMPT_AMT")
private BigDecimal stj6ExemptAmt;
public void setStj6ExemptAmt(BigDecimal stj6ExemptAmt){
    this.stj6ExemptAmt = stj6ExemptAmt;
}
public BigDecimal getStj6ExemptAmt(){
    return stj6ExemptAmt;
}

@Column(name = "METHOD_DELIVERY_CODE")
private String methodDeliveryCode;
public void setMethodDeliveryCode(String methodDeliveryCode){
    this.methodDeliveryCode = methodDeliveryCode;
}
public String getMethodDeliveryCode(){
    return methodDeliveryCode;
}

@Column(name = "COMP_STATE_TIER1_TAX_AMT")
private BigDecimal compStateTier1TaxAmt;
public void setCompStateTier1TaxAmt(BigDecimal compStateTier1TaxAmt){
    this.compStateTier1TaxAmt = compStateTier1TaxAmt;
}
public BigDecimal getCompStateTier1TaxAmt(){
    return compStateTier1TaxAmt;
}

@Column(name = "DISTR_10_AMT")
private BigDecimal distr10Amt;
public void setDistr10Amt(BigDecimal distr10Amt){
    this.distr10Amt = distr10Amt;
}
public BigDecimal getDistr10Amt(){
    return distr10Amt;
}

@Column(name = "CALCULATE_IND")
private String calculateInd;
public void setCalculateInd(String calculateInd){
    this.calculateInd = calculateInd;
}
public String getCalculateInd(){
    return calculateInd;
}

@Column(name = "PO_LINE_NAME")
private String poLineName;
public void setPoLineName(String poLineName){
    this.poLineName = poLineName;
}
public String getPoLineName(){
    return poLineName;
}

@Column(name = "CITY_LOCAL_TAX_AMT")
private BigDecimal cityLocalTaxAmt;
public void setCityLocalTaxAmt(BigDecimal cityLocalTaxAmt){
    this.cityLocalTaxAmt = cityLocalTaxAmt;
}
public BigDecimal getCityLocalTaxAmt(){
    return cityLocalTaxAmt;
}

@Column(name = "UNLOCK_ENTITY_ID")
private Long unlockEntityId;
public void setUnlockEntityId(Long unlockEntityId){
    this.unlockEntityId = unlockEntityId;
}
public Long getUnlockEntityId(){
    return unlockEntityId;
}

@Column(name = "DISTR_08_AMT")
private BigDecimal distr08Amt;
public void setDistr08Amt(BigDecimal distr08Amt){
    this.distr08Amt = distr08Amt;
}
public BigDecimal getDistr08Amt(){
    return distr08Amt;
}

@Column(name = "COUNTY_TIER3_TAX_AMT")
private BigDecimal countyTier3TaxAmt;
public void setCountyTier3TaxAmt(BigDecimal countyTier3TaxAmt){
    this.countyTier3TaxAmt = countyTier3TaxAmt;
}
public BigDecimal getCountyTier3TaxAmt(){
    return countyTier3TaxAmt;
}

@Column(name = "EXEMPT_CODE")
private String exemptCode;
public void setExemptCode(String exemptCode){
    this.exemptCode = exemptCode;
}
public String getExemptCode(){
    return exemptCode;
}

@Column(name = "WO_NAME")
private String woName;
public void setWoName(String woName){
    this.woName = woName;
}
public String getWoName(){
    return woName;
}

@Column(name = "COMP_STJ1_TAX_AMT")
private BigDecimal compStj1TaxAmt;
public void setCompStj1TaxAmt(BigDecimal compStj1TaxAmt){
    this.compStj1TaxAmt = compStj1TaxAmt;
}
public BigDecimal getCompStj1TaxAmt(){
    return compStj1TaxAmt;
}

@Column(name = "TRANSACTION_COUNTRY_CODE")
private String transactionCountryCode;
public void setTransactionCountryCode(String transactionCountryCode){
    this.transactionCountryCode = transactionCountryCode;
}
public String getTransactionCountryCode(){
    return transactionCountryCode;
}

@Column(name = "COMP_STJ4_TAX_AMT")
private BigDecimal compStj4TaxAmt;
public void setCompStj4TaxAmt(BigDecimal compStj4TaxAmt){
    this.compStj4TaxAmt = compStj4TaxAmt;
}
public BigDecimal getCompStj4TaxAmt(){
    return compStj4TaxAmt;
}

@Column(name = "VENDOR_CODE")
private String vendorCode;
public void setVendorCode(String vendorCode){
    this.vendorCode = vendorCode;
}
public String getVendorCode(){
    return vendorCode;
}

@Column(name = "AFE_CATEGORY_NBR")
private String afeCategoryNbr;
public void setAfeCategoryNbr(String afeCategoryNbr){
    this.afeCategoryNbr = afeCategoryNbr;
}
public String getAfeCategoryNbr(){
    return afeCategoryNbr;
}

@Column(name = "TRANSACTION_STATUS")
private String transactionStatus;
public void setTransactionStatus(String transactionStatus){
    this.transactionStatus = transactionStatus;
}
public String getTransactionStatus(){
    return transactionStatus;
}

@Column(name = "EXEMPT_AMT")
private BigDecimal exemptAmt;
public void setExemptAmt(BigDecimal exemptAmt){
    this.exemptAmt = exemptAmt;
}
public BigDecimal getExemptAmt(){
    return exemptAmt;
}

@Column(name = "GEO_RECON_CODE")
private String geoReconCode;
public void setGeoReconCode(String geoReconCode){
    this.geoReconCode = geoReconCode;
}
public String getGeoReconCode(){
    return geoReconCode;
}

@Column(name = "COUNTY_TIER1_TAXABLE_AMT")
private BigDecimal countyTier1TaxableAmt;
public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt){
    this.countyTier1TaxableAmt = countyTier1TaxableAmt;
}
public BigDecimal getCountyTier1TaxableAmt(){
    return countyTier1TaxableAmt;
}

@Column(name = "PO_LINE_NBR")
private String poLineNbr;
public void setPoLineNbr(String poLineNbr){
    this.poLineNbr = poLineNbr;
}
public String getPoLineNbr(){
    return poLineNbr;
}

@Column(name = "COMP_STJ10_TAX_AMT")
private BigDecimal compStj10TaxAmt;
public void setCompStj10TaxAmt(BigDecimal compStj10TaxAmt){
    this.compStj10TaxAmt = compStj10TaxAmt;
}
public BigDecimal getCompStj10TaxAmt(){
    return compStj10TaxAmt;
}

@Column(name = "COMP_COUNTY_TIER2_TAX_AMT")
private BigDecimal compCountyTier2TaxAmt;
public void setCompCountyTier2TaxAmt(BigDecimal compCountyTier2TaxAmt){
    this.compCountyTier2TaxAmt = compCountyTier2TaxAmt;
}
public BigDecimal getCompCountyTier2TaxAmt(){
    return compCountyTier2TaxAmt;
}

@Column(name = "BCP_TRANSACTION_ID")
private Long bcpTransactionId;
public void setBcpTransactionId(Long bcpTransactionId){
    this.bcpTransactionId = bcpTransactionId;
}
public Long getBcpTransactionId(){
    return bcpTransactionId;
}

@Column(name = "INVOICE_LINE_NBR")
private String invoiceLineNbr;
public void setInvoiceLineNbr(String invoiceLineNbr){
    this.invoiceLineNbr = invoiceLineNbr;
}
public String getInvoiceLineNbr(){
    return invoiceLineNbr;
}

@Column(name = "INVOICE_TAX_AMT")
private BigDecimal invoiceTaxAmt;
public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt){
    this.invoiceTaxAmt = invoiceTaxAmt;
}
public BigDecimal getInvoiceTaxAmt(){
    return invoiceTaxAmt;
}

@Column(name = "TAXCODE_CODE")
private String taxcodeCode;
public void setTaxcodeCode(String taxcodeCode){
    this.taxcodeCode = taxcodeCode;
}
public String getTaxcodeCode(){
    return taxcodeCode;
}

@Column(name = "INVENTORY_CLASS_NAME")
private String inventoryClassName;
public void setInventoryClassName(String inventoryClassName){
    this.inventoryClassName = inventoryClassName;
}
public String getInventoryClassName(){
    return inventoryClassName;
}

@Column(name = "STJ2_TAXABLE_AMT")
private BigDecimal stj2TaxableAmt;
public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt){
    this.stj2TaxableAmt = stj2TaxableAmt;
}
public BigDecimal getStj2TaxableAmt(){
    return stj2TaxableAmt;
}

@Column(name = "COMP_CITY_TIER3_TAX_AMT")
private BigDecimal compCityTier3TaxAmt;
public void setCompCityTier3TaxAmt(BigDecimal compCityTier3TaxAmt){
    this.compCityTier3TaxAmt = compCityTier3TaxAmt;
}
public BigDecimal getCompCityTier3TaxAmt(){
    return compCityTier3TaxAmt;
}

@Column(name = "GL_COMPANY_NBR")
private String glCompanyNbr;
public void setGlCompanyNbr(String glCompanyNbr){
    this.glCompanyNbr = glCompanyNbr;
}
public String getGlCompanyNbr(){
    return glCompanyNbr;
}

@Column(name = "GL_COMPANY_NAME")
private String glCompanyName;
public void setGlCompanyName(String glCompanyName){
    this.glCompanyName = glCompanyName;
}
public String getGlCompanyName(){
    return glCompanyName;
}

@Column(name = "STATE_TIER2_TAX_AMT")
private BigDecimal stateTier2TaxAmt;
public void setStateTier2TaxAmt(BigDecimal stateTier2TaxAmt){
    this.stateTier2TaxAmt = stateTier2TaxAmt;
}
public BigDecimal getStateTier2TaxAmt(){
    return stateTier2TaxAmt;
}

@Column(name = "LOCATION_DRIVER_10")
private String locationDriver10;
public void setLocationDriver10(String locationDriver10){
    this.locationDriver10 = locationDriver10;
}
public String getLocationDriver10(){
    return locationDriver10;
}

@Column(name = "WO_TYPE")
private String woType;
public void setWoType(String woType){
    this.woType = woType;
}
public String getWoType(){
    return woType;
}

@Column(name = "CITY_TIER3_TAXABLE_AMT")
private BigDecimal cityTier3TaxableAmt;
public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt){
    this.cityTier3TaxableAmt = cityTier3TaxableAmt;
}
public BigDecimal getCityTier3TaxableAmt(){
    return cityTier3TaxableAmt;
}

@Column(name = "STJ10_TAX_AMT")
private BigDecimal stj10TaxAmt;
public void setStj10TaxAmt(BigDecimal stj10TaxAmt){
    this.stj10TaxAmt = stj10TaxAmt;
}
public BigDecimal getStj10TaxAmt(){
    return stj10TaxAmt;
}

@Column(name = "AFE_PROJECT_NAME")
private String afeProjectName;
public void setAfeProjectName(String afeProjectName){
    this.afeProjectName = afeProjectName;
}
public String getAfeProjectName(){
    return afeProjectName;
}

@Column(name = "AFE_PROJECT_NBR")
private String afeProjectNbr;
public void setAfeProjectNbr(String afeProjectNbr){
    this.afeProjectNbr = afeProjectNbr;
}
public String getAfeProjectNbr(){
    return afeProjectNbr;
}

@Column(name = "COUNTRY_TIER3_TAXABLE_AMT")
private BigDecimal countryTier3TaxableAmt;
public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt){
    this.countryTier3TaxableAmt = countryTier3TaxableAmt;
}
public BigDecimal getCountryTier3TaxableAmt(){
    return countryTier3TaxableAmt;
}
//Cut
@Column(name = "VENDOR_ADDRESS_LINE_2")
private String vendorAddressLine2;
public void setVendorAddressLine2(String vendorAddressLine2){
    this.vendorAddressLine2 = vendorAddressLine2;
}
public String getVendorAddressLine2(){
    return vendorAddressLine2;
}

@Column(name = "COMP_COUNTRY_TAX_AMT")
private BigDecimal compCountryTaxAmt;
public void setCompCountryTaxAmt(BigDecimal compCountryTaxAmt){
    this.compCountryTaxAmt = compCountryTaxAmt;
}
public BigDecimal getCompCountryTaxAmt(){
    return compCountryTaxAmt;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "INVOICE_DATE")
private Date invoiceDate;
public void setInvoiceDate(Date invoiceDate){
    this.invoiceDate = invoiceDate;
}
public Date getInvoiceDate(){
    return invoiceDate;
}

@Column(name = "VENDOR_ADDRESS_LINE_1")
private String vendorAddressLine1;
public void setVendorAddressLine1(String vendorAddressLine1){
    this.vendorAddressLine1 = vendorAddressLine1;
}
public String getVendorAddressLine1(){
    return vendorAddressLine1;
}

@Column(name = "WO_LINE_NBR")
private String woLineNbr;
public void setWoLineNbr(String woLineNbr){
    this.woLineNbr = woLineNbr;
}
public String getWoLineNbr(){
    return woLineNbr;
}

@Column(name = "STJ5_EXEMPT_AMT")
private BigDecimal stj5ExemptAmt;
public void setStj5ExemptAmt(BigDecimal stj5ExemptAmt){
    this.stj5ExemptAmt = stj5ExemptAmt;
}
public BigDecimal getStj5ExemptAmt(){
    return stj5ExemptAmt;
}

@Column(name = "DISTR_05_AMT")
private BigDecimal distr05Amt;
public void setDistr05Amt(BigDecimal distr05Amt){
    this.distr05Amt = distr05Amt;
}
public BigDecimal getDistr05Amt(){
    return distr05Amt;
}

@Column(name = "DIFF_STJ10_TAX_AMT")
private BigDecimal diffStj10TaxAmt;
public void setDiffStj10TaxAmt(BigDecimal diffStj10TaxAmt){
    this.diffStj10TaxAmt = diffStj10TaxAmt;
}
public BigDecimal getDiffStj10TaxAmt(){
    return diffStj10TaxAmt;
}

@Column(name = "SOURCE_TRANSACTION_ID")
private Long sourceTransactionId;
public void setSourceTransactionId(Long sourceTransactionId){
    this.sourceTransactionId = sourceTransactionId;
}
public Long getSourceTransactionId(){
    return sourceTransactionId;
}

@Column(name = "VENDOR_ADDRESS_CITY")
private String vendorAddressCity;
public void setVendorAddressCity(String vendorAddressCity){
    this.vendorAddressCity = vendorAddressCity;
}
public String getVendorAddressCity(){
    return vendorAddressCity;
}

@Column(name = "PO_LINE_TYPE_NAME")
private String poLineTypeName;
public void setPoLineTypeName(String poLineTypeName){
    this.poLineTypeName = poLineTypeName;
}
public String getPoLineTypeName(){
    return poLineTypeName;
}

@Column(name = "COUNTY_TIER3_TAXABLE_AMT")
private BigDecimal countyTier3TaxableAmt;
public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt){
    this.countyTier3TaxableAmt = countyTier3TaxableAmt;
}
public BigDecimal getCountyTier3TaxableAmt(){
    return countyTier3TaxableAmt;
}

@Column(name = "COMP_STATE_TAX_AMT")
private BigDecimal compStateTaxAmt;
public void setCompStateTaxAmt(BigDecimal compStateTaxAmt){
    this.compStateTaxAmt = compStateTaxAmt;
}
public BigDecimal getCompStateTaxAmt(){
    return compStateTaxAmt;
}

@Column(name = "TAX_DRIVER_09")
private String taxDriver09;
public void setTaxDriver09(String taxDriver09){
    this.taxDriver09 = taxDriver09;
}
public String getTaxDriver09(){
    return taxDriver09;
}

@Column(name = "COMP_CITY_TIER2_TAX_AMT")
private BigDecimal compCityTier2TaxAmt;
public void setCompCityTier2TaxAmt(BigDecimal compCityTier2TaxAmt){
    this.compCityTier2TaxAmt = compCityTier2TaxAmt;
}
public BigDecimal getCompCityTier2TaxAmt(){
    return compCityTier2TaxAmt;
}

@Column(name = "DIFF_STJ6_TAX_AMT")
private BigDecimal diffStj6TaxAmt;
public void setDiffStj6TaxAmt(BigDecimal diffStj6TaxAmt){
    this.diffStj6TaxAmt = diffStj6TaxAmt;
}
public BigDecimal getDiffStj6TaxAmt(){
    return diffStj6TaxAmt;
}

@Column(name = "STJ8_TAX_AMT")
private BigDecimal stj8TaxAmt;
public void setStj8TaxAmt(BigDecimal stj8TaxAmt){
    this.stj8TaxAmt = stj8TaxAmt;
}
public BigDecimal getStj8TaxAmt(){
    return stj8TaxAmt;
}

@Column(name = "TAX_DRIVER_06")
private String taxDriver06;
public void setTaxDriver06(String taxDriver06){
    this.taxDriver06 = taxDriver06;
}
public String getTaxDriver06(){
    return taxDriver06;
}

@Column(name = "DISTR_01_AMT")
private BigDecimal distr01Amt;
public void setDistr01Amt(BigDecimal distr01Amt){
    this.distr01Amt = distr01Amt;
}
public BigDecimal getDistr01Amt(){
    return distr01Amt;
}

@Column(name = "WO_CLASS_DESC")
private String woClassDesc;
public void setWoClassDesc(String woClassDesc){
    this.woClassDesc = woClassDesc;
}
public String getWoClassDesc(){
    return woClassDesc;
}

@Column(name = "TAX_DRIVER_05")
private String taxDriver05;
public void setTaxDriver05(String taxDriver05){
    this.taxDriver05 = taxDriver05;
}
public String getTaxDriver05(){
    return taxDriver05;
}

@Column(name = "WO_CLASS")
private String woClass;
public void setWoClass(String woClass){
    this.woClass = woClass;
}
public String getWoClass(){
    return woClass;
}

@Column(name = "TAX_DRIVER_08")
private String taxDriver08;
public void setTaxDriver08(String taxDriver08){
    this.taxDriver08 = taxDriver08;
}
public String getTaxDriver08(){
    return taxDriver08;
}

@Column(name = "MULTI_TRANS_CODE")
private String multiTransCode;
public void setMultiTransCode(String multiTransCode){
    this.multiTransCode = multiTransCode;
}
public String getMultiTransCode(){
    return multiTransCode;
}

@Column(name = "TAX_DRIVER_07")
private String taxDriver07;
public void setTaxDriver07(String taxDriver07){
    this.taxDriver07 = taxDriver07;
}
public String getTaxDriver07(){
    return taxDriver07;
}

@Column(name = "GL_LOCAL_ACCT_NBR")
private String glLocalAcctNbr;
public void setGlLocalAcctNbr(String glLocalAcctNbr){
    this.glLocalAcctNbr = glLocalAcctNbr;
}
public String getGlLocalAcctNbr(){
    return glLocalAcctNbr;
}

@Column(name = "TAX_DRIVER_02")
private String taxDriver02;
public void setTaxDriver02(String taxDriver02){
    this.taxDriver02 = taxDriver02;
}
public String getTaxDriver02(){
    return taxDriver02;
}

@Column(name = "TAX_DRIVER_01")
private String taxDriver01;
public void setTaxDriver01(String taxDriver01){
    this.taxDriver01 = taxDriver01;
}
public String getTaxDriver01(){
    return taxDriver01;
}

@Column(name = "TAX_DRIVER_04")
private String taxDriver04;
public void setTaxDriver04(String taxDriver04){
    this.taxDriver04 = taxDriver04;
}
public String getTaxDriver04(){
    return taxDriver04;
}

@Column(name = "TAXABLE_AMT")
private BigDecimal taxableAmt;
public void setTaxableAmt(BigDecimal taxableAmt){
    this.taxableAmt = taxableAmt;
}
public BigDecimal getTaxableAmt(){
    return taxableAmt;
}

@Column(name = "COUNTRY_EXEMPT_AMT")
private BigDecimal countryExemptAmt;
public void setCountryExemptAmt(BigDecimal countryExemptAmt){
    this.countryExemptAmt = countryExemptAmt;
}
public BigDecimal getCountryExemptAmt(){
    return countryExemptAmt;
}

@Column(name = "TAX_DRIVER_03")
private String taxDriver03;
public void setTaxDriver03(String taxDriver03){
    this.taxDriver03 = taxDriver03;
}
public String getTaxDriver03(){
    return taxDriver03;
}

@Column(name = "DISTR_02_AMT")
private BigDecimal distr02Amt;
public void setDistr02Amt(BigDecimal distr02Amt){
    this.distr02Amt = distr02Amt;
}
public BigDecimal getDistr02Amt(){
    return distr02Amt;
}

@Column(name = "STJ8_EXEMPT_AMT")
private BigDecimal stj8ExemptAmt;
public void setStj8ExemptAmt(BigDecimal stj8ExemptAmt){
    this.stj8ExemptAmt = stj8ExemptAmt;
}
public BigDecimal getStj8ExemptAmt(){
    return stj8ExemptAmt;
}

@Column(name = "STATE_EXEMPT_AMT")
private BigDecimal stateExemptAmt;
public void setStateExemptAmt(BigDecimal stateExemptAmt){
    this.stateExemptAmt = stateExemptAmt;
}
public BigDecimal getStateExemptAmt(){
    return stateExemptAmt;
}

@Column(name = "COUNTY_TIER2_TAX_AMT")
private BigDecimal countyTier2TaxAmt;
public void setCountyTier2TaxAmt(BigDecimal countyTier2TaxAmt){
    this.countyTier2TaxAmt = countyTier2TaxAmt;
}
public BigDecimal getCountyTier2TaxAmt(){
    return countyTier2TaxAmt;
}

@Column(name = "STJ7_EXEMPT_AMT")
private BigDecimal stj7ExemptAmt;
public void setStj7ExemptAmt(BigDecimal stj7ExemptAmt){
    this.stj7ExemptAmt = stj7ExemptAmt;
}
public BigDecimal getStj7ExemptAmt(){
    return stj7ExemptAmt;
}

@Column(name = "STJ5_TAX_AMT")
private BigDecimal stj5TaxAmt;
public void setStj5TaxAmt(BigDecimal stj5TaxAmt){
    this.stj5TaxAmt = stj5TaxAmt;
}
public BigDecimal getStj5TaxAmt(){
    return stj5TaxAmt;
}

@Column(name = "COMP_COUNTRY_TIER3_TAX_AMT")
private BigDecimal compCountryTier3TaxAmt;
public void setCompCountryTier3TaxAmt(BigDecimal compCountryTier3TaxAmt){
    this.compCountryTier3TaxAmt = compCountryTier3TaxAmt;
}
public BigDecimal getCompCountryTier3TaxAmt(){
    return compCountryTier3TaxAmt;
}

@Column(name = "AFE_CATEGORY_NAME")
private String afeCategoryName;
public void setAfeCategoryName(String afeCategoryName){
    this.afeCategoryName = afeCategoryName;
}
public String getAfeCategoryName(){
    return afeCategoryName;
}

@Column(name = "COUNTY_EXEMPT_AMT")
private BigDecimal countyExemptAmt;
public void setCountyExemptAmt(BigDecimal countyExemptAmt){
    this.countyExemptAmt = countyExemptAmt;
}
public BigDecimal getCountyExemptAmt(){
    return countyExemptAmt;
}

@Column(name = "WO_NBR")
private String woNbr;
public void setWoNbr(String woNbr){
    this.woNbr = woNbr;
}
public String getWoNbr(){
    return woNbr;
}

@Column(name = "VENDOR_ADDRESS_COUNTY")
private String vendorAddressCounty;
public void setVendorAddressCounty(String vendorAddressCounty){
    this.vendorAddressCounty = vendorAddressCounty;
}
public String getVendorAddressCounty(){
    return vendorAddressCounty;
}

@Column(name = "TAX_DRIVER_19")
private String taxDriver19;
public void setTaxDriver19(String taxDriver19){
    this.taxDriver19 = taxDriver19;
}
public String getTaxDriver19(){
    return taxDriver19;
}

@Column(name = "DIFF_COUNTY_TAX_AMT")
private BigDecimal diffCountyTaxAmt;
public void setDiffCountyTaxAmt(BigDecimal diffCountyTaxAmt){
    this.diffCountyTaxAmt = diffCountyTaxAmt;
}
public BigDecimal getDiffCountyTaxAmt(){
    return diffCountyTaxAmt;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "GL_EXTRACT_TIMESTAMP")
private Date glExtractTimestamp;
public void setGlExtractTimestamp(Date glExtractTimestamp){
    this.glExtractTimestamp = glExtractTimestamp;
}
public Date getGlExtractTimestamp(){
    return glExtractTimestamp;
}

@Column(name = "TAX_DRIVER_18")
private String taxDriver18;
public void setTaxDriver18(String taxDriver18){
    this.taxDriver18 = taxDriver18;
}
public String getTaxDriver18(){
    return taxDriver18;
}

@Column(name = "TAX_DRIVER_17")
private String taxDriver17;
public void setTaxDriver17(String taxDriver17){
    this.taxDriver17 = taxDriver17;
}
public String getTaxDriver17(){
    return taxDriver17;
}

@Column(name = "TAX_DRIVER_16")
private String taxDriver16;
public void setTaxDriver16(String taxDriver16){
    this.taxDriver16 = taxDriver16;
}
public String getTaxDriver16(){
    return taxDriver16;
}

@Column(name = "CP_FILING_ENTITY_CODE")
private String cpFilingEntityCode;
public void setCpFilingEntityCode(String cpFilingEntityCode){
    this.cpFilingEntityCode = cpFilingEntityCode;
}
public String getCpFilingEntityCode(){
    return cpFilingEntityCode;
}

@Column(name = "TAX_DRIVER_15")
private String taxDriver15;
public void setTaxDriver15(String taxDriver15){
    this.taxDriver15 = taxDriver15;
}
public String getTaxDriver15(){
    return taxDriver15;
}

@Column(name = "LOCATION_DRIVER_09")
private String locationDriver09;
public void setLocationDriver09(String locationDriver09){
    this.locationDriver09 = locationDriver09;
}
public String getLocationDriver09(){
    return locationDriver09;
}

@Column(name = "TAX_DRIVER_14")
private String taxDriver14;
public void setTaxDriver14(String taxDriver14){
    this.taxDriver14 = taxDriver14;
}
public String getTaxDriver14(){
    return taxDriver14;
}

@Column(name = "STJ1_TAX_AMT")
private BigDecimal stj1TaxAmt;
public void setStj1TaxAmt(BigDecimal stj1TaxAmt){
    this.stj1TaxAmt = stj1TaxAmt;
}
public BigDecimal getStj1TaxAmt(){
    return stj1TaxAmt;
}

@Column(name = "TAX_DRIVER_13")
private String taxDriver13;
public void setTaxDriver13(String taxDriver13){
    this.taxDriver13 = taxDriver13;
}
public String getTaxDriver13(){
    return taxDriver13;
}

@Column(name = "LOCATION_DRIVER_07")
private String locationDriver07;
public void setLocationDriver07(String locationDriver07){
    this.locationDriver07 = locationDriver07;
}
public String getLocationDriver07(){
    return locationDriver07;
}

@Column(name = "TAX_DRIVER_12")
private String taxDriver12;
public void setTaxDriver12(String taxDriver12){
    this.taxDriver12 = taxDriver12;
}
public String getTaxDriver12(){
    return taxDriver12;
}

@Column(name = "LOCATION_DRIVER_08")
private String locationDriver08;
public void setLocationDriver08(String locationDriver08){
    this.locationDriver08 = locationDriver08;
}
public String getLocationDriver08(){
    return locationDriver08;
}

@Column(name = "TAX_DRIVER_11")
private String taxDriver11;
public void setTaxDriver11(String taxDriver11){
    this.taxDriver11 = taxDriver11;
}
public String getTaxDriver11(){
    return taxDriver11;
}

@Column(name = "LOCATION_DRIVER_05")
private String locationDriver05;
public void setLocationDriver05(String locationDriver05){
    this.locationDriver05 = locationDriver05;
}
public String getLocationDriver05(){
    return locationDriver05;
}

@Column(name = "TAX_DRIVER_10")
private String taxDriver10;
public void setTaxDriver10(String taxDriver10){
    this.taxDriver10 = taxDriver10;
}
public String getTaxDriver10(){
    return taxDriver10;
}

@Column(name = "LOCATION_DRIVER_06")
private String locationDriver06;
public void setLocationDriver06(String locationDriver06){
    this.locationDriver06 = locationDriver06;
}
public String getLocationDriver06(){
    return locationDriver06;
}

@Column(name = "LOCATION_DRIVER_03")
private String locationDriver03;
public void setLocationDriver03(String locationDriver03){
    this.locationDriver03 = locationDriver03;
}
public String getLocationDriver03(){
    return locationDriver03;
}

@Column(name = "LOCATION_DRIVER_04")
private String locationDriver04;
public void setLocationDriver04(String locationDriver04){
    this.locationDriver04 = locationDriver04;
}
public String getLocationDriver04(){
    return locationDriver04;
}

@Column(name = "INVOICE_LINE_DISC_TYPE_CODE")
private String invoiceLineDiscTypeCode;
public void setInvoiceLineDiscTypeCode(String invoiceLineDiscTypeCode){
    this.invoiceLineDiscTypeCode = invoiceLineDiscTypeCode;
}
public String getInvoiceLineDiscTypeCode(){
    return invoiceLineDiscTypeCode;
}

@Column(name = "LOCATION_DRIVER_02")
private String locationDriver02;
public void setLocationDriver02(String locationDriver02){
    this.locationDriver02 = locationDriver02;
}
public String getLocationDriver02(){
    return locationDriver02;
}

@Column(name = "LOCATION_DRIVER_01")
private String locationDriver01;
public void setLocationDriver01(String locationDriver01){
    this.locationDriver01 = locationDriver01;
}
public String getLocationDriver01(){
    return locationDriver01;
}

@Column(name = "WO_ENTITY_DESC")
private String woEntityDesc;
public void setWoEntityDesc(String woEntityDesc){
    this.woEntityDesc = woEntityDesc;
}
public String getWoEntityDesc(){
    return woEntityDesc;
}

@Column(name = "INVOICE_TAX_FLG")
private String invoiceTaxFlg;
public void setInvoiceTaxFlg(String invoiceTaxFlg){
    this.invoiceTaxFlg = invoiceTaxFlg;
}
public String getInvoiceTaxFlg(){
    return invoiceTaxFlg;
}

@Column(name = "WO_LINE_TYPE")
private String woLineType;
public void setWoLineType(String woLineType){
    this.woLineType = woLineType;
}
public String getWoLineType(){
    return woLineType;
}
// First cut
@Column(name = "AFE_SUB_CAT_NBR")
private String afeSubCatNbr;
public void setAfeSubCatNbr(String afeSubCatNbr){
    this.afeSubCatNbr = afeSubCatNbr;
}
public String getAfeSubCatNbr(){
    return afeSubCatNbr;
}

@Column(name = "INVOICE_DESC")
private String invoiceDesc;
public void setInvoiceDesc(String invoiceDesc){
    this.invoiceDesc = invoiceDesc;
}
public String getInvoiceDesc(){
    return invoiceDesc;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "PO_DATE")
private Date poDate;
public void setPoDate(Date poDate){
    this.poDate = poDate;
}
public Date getPoDate(){
    return poDate;
}

@Column(name = "DISTR_07_AMT")
private BigDecimal distr07Amt;
public void setDistr07Amt(BigDecimal distr07Amt){
    this.distr07Amt = distr07Amt;
}
public BigDecimal getDistr07Amt(){
    return distr07Amt;
}

@Column(name = "AFE_CONTRACT_TYPE")
private String afeContractType;
public void setAfeContractType(String afeContractType){
    this.afeContractType = afeContractType;
}
public String getAfeContractType(){
    return afeContractType;
}

@Column(name = "PRODUCT_NAME")
private String productName;
public void setProductName(String productName){
    this.productName = productName;
}
public String getProductName(){
    return productName;
}

@Column(name = "CITY_EXEMPT_AMT")
private BigDecimal cityExemptAmt;
public void setCityExemptAmt(BigDecimal cityExemptAmt){
    this.cityExemptAmt = cityExemptAmt;
}
public BigDecimal getCityExemptAmt(){
    return cityExemptAmt;
}

@Column(name = "STATE_TIER3_TAX_AMT")
private BigDecimal stateTier3TaxAmt;
public void setStateTier3TaxAmt(BigDecimal stateTier3TaxAmt){
    this.stateTier3TaxAmt = stateTier3TaxAmt;
}
public BigDecimal getStateTier3TaxAmt(){
    return stateTier3TaxAmt;
}

@Column(name = "PRODUCT_CODE")
private String productCode;
public void setProductCode(String productCode){
    this.productCode = productCode;
}
public String getProductCode(){
    return productCode;
}

@Column(name = "DIFF_STJ9_TAX_AMT")
private BigDecimal diffStj9TaxAmt;
public void setDiffStj9TaxAmt(BigDecimal diffStj9TaxAmt){
    this.diffStj9TaxAmt = diffStj9TaxAmt;
}
public BigDecimal getDiffStj9TaxAmt(){
    return diffStj9TaxAmt;
}

@Column(name = "COMP_STJ5_TAX_AMT")
private BigDecimal compStj5TaxAmt;
public void setCompStj5TaxAmt(BigDecimal compStj5TaxAmt){
    this.compStj5TaxAmt = compStj5TaxAmt;
}
public BigDecimal getCompStj5TaxAmt(){
    return compStj5TaxAmt;
}

@Column(name = "COUNTRY_TIER2_TAXABLE_AMT")
private BigDecimal countryTier2TaxableAmt;
public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt){
    this.countryTier2TaxableAmt = countryTier2TaxableAmt;
}
public BigDecimal getCountryTier2TaxableAmt(){
    return countryTier2TaxableAmt;
}

@Column(name = "COMP_COUNTRY_TIER2_TAX_AMT")
private BigDecimal compCountryTier2TaxAmt;
public void setCompCountryTier2TaxAmt(BigDecimal compCountryTier2TaxAmt){
    this.compCountryTier2TaxAmt = compCountryTier2TaxAmt;
}
public BigDecimal getCompCountryTier2TaxAmt(){
    return compCountryTier2TaxAmt;
}

@Column(name = "WO_TYPE_DESC")
private String woTypeDesc;
public void setWoTypeDesc(String woTypeDesc){
    this.woTypeDesc = woTypeDesc;
}
public String getWoTypeDesc(){
    return woTypeDesc;
}

@Temporal(TemporalType.DATE)
@XmlJavaTypeAdapter(JaxbDateAdapter.class)
@Column(name = "VOUCHER_DATE")
private Date voucherDate;
public void setVoucherDate(Date voucherDate){
    this.voucherDate = voucherDate;
}
public Date getVoucherDate(){
    return voucherDate;
}

@Column(name = "COMP_STJ3_TAX_AMT")
private BigDecimal compStj3TaxAmt;
public void setCompStj3TaxAmt(BigDecimal compStj3TaxAmt){
    this.compStj3TaxAmt = compStj3TaxAmt;
}
public BigDecimal getCompStj3TaxAmt(){
    return compStj3TaxAmt;
}

@Column(name = "STJ2_EXEMPT_AMT")
private BigDecimal stj2ExemptAmt;
public void setStj2ExemptAmt(BigDecimal stj2ExemptAmt){
    this.stj2ExemptAmt = stj2ExemptAmt;
}
public BigDecimal getStj2ExemptAmt(){
    return stj2ExemptAmt;
}

@Column(name = "AFE_SUB_CAT_NAME")
private String afeSubCatName;
public void setAfeSubCatName(String afeSubCatName){
    this.afeSubCatName = afeSubCatName;
}
public String getAfeSubCatName(){
    return afeSubCatName;
}

@Column(name = "VENDOR_ADDRESS_STATE")
private String vendorAddressState;
public void setVendorAddressState(String vendorAddressState){
    this.vendorAddressState = vendorAddressState;
}
public String getVendorAddressState(){
    return vendorAddressState;
}

@Column(name = "COMP_STJ8_TAX_AMT")
private BigDecimal compStj8TaxAmt;
public void setCompStj8TaxAmt(BigDecimal compStj8TaxAmt){
    this.compStj8TaxAmt = compStj8TaxAmt;
}
public BigDecimal getCompStj8TaxAmt(){
    return compStj8TaxAmt;
}

@Column(name = "EXCLUSION_AMT")
private BigDecimal exclusionAmt;
public void setExclusionAmt(BigDecimal exclusionAmt){
    this.exclusionAmt = exclusionAmt;
}
public BigDecimal getExclusionAmt(){
    return exclusionAmt;
}

@Column(name = "INVOICE_TOTAL_AMT")
private BigDecimal invoiceTotalAmt;
public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt){
    this.invoiceTotalAmt = invoiceTotalAmt;
}
public BigDecimal getInvoiceTotalAmt(){
    return invoiceTotalAmt;
}

@Column(name = "TRANSACTION_IND")
private String transactionInd;
public void setTransactionInd(String transactionInd){
    this.transactionInd = transactionInd;
}
public String getTransactionInd(){
    return transactionInd;
}

@Column(name = "DIFF_COUNTRY_TIER2_TAX_AMT")
private BigDecimal diffCountryTier2TaxAmt;
public void setDiffCountryTier2TaxAmt(BigDecimal diffCountryTier2TaxAmt){
    this.diffCountryTier2TaxAmt = diffCountryTier2TaxAmt;
}
public BigDecimal getDiffCountryTier2TaxAmt(){
    return diffCountryTier2TaxAmt;
}

@Column(name = "VENDOR_TYPE_NAME")
private String vendorTypeName;
public void setVendorTypeName(String vendorTypeName){
    this.vendorTypeName = vendorTypeName;
}
public String getVendorTypeName(){
    return vendorTypeName;
}

@Column(name = "PROCESS_BATCH_NO")
private Long processBatchNo;
public void setProcessBatchNo(Long processBatchNo){
    this.processBatchNo = processBatchNo;
}
public Long getProcessBatchNo(){
    return processBatchNo;
}

@Column(name = "EXEMPT_IND")
private String exemptInd;
public void setExemptInd(String exemptInd){
    this.exemptInd = exemptInd;
}
public String getExemptInd(){
    return exemptInd;
}

@Column(name = "PROCESSTYPE_CODE")
private String processtypeCode;
public void setProcesstypeCode(String processtypeCode){
    this.processtypeCode = processtypeCode;
}
public String getProcesstypeCode(){
    return processtypeCode;
}


@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_SITUS_JUR_ID")
private Long stj5SitusJurId;
public void setStj5SitusJurId(Long stj5SitusJurId){
    this.stj5SitusJurId = stj5SitusJurId;
}
public Long getStj5SitusJurId(){
    return stj5SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_NAME")
private String stj1Name;
public void setStj1Name(String stj1Name){
    this.stj1Name = stj1Name;
}
public String getStj1Name(){
    return stj1Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_TAXTYPE_USED_CODE")
private String stj7TaxtypeUsedCode;
public void setStj7TaxtypeUsedCode(String stj7TaxtypeUsedCode){
    this.stj7TaxtypeUsedCode = stj7TaxtypeUsedCode;
}
public String getStj7TaxtypeUsedCode(){
    return stj7TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_VENDOR_NEXUS_DTL_ID")
private Long stj3VendorNexusDtlId;
public void setStj3VendorNexusDtlId(Long stj3VendorNexusDtlId){
    this.stj3VendorNexusDtlId = stj3VendorNexusDtlId;
}
public Long getStj3VendorNexusDtlId(){
    return stj3VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_TAXCODE_OVER_FLAG")
private String cityTaxcodeOverFlag;
public void setCityTaxcodeOverFlag(String cityTaxcodeOverFlag){
    this.cityTaxcodeOverFlag = cityTaxcodeOverFlag;
}
public String getCityTaxcodeOverFlag(){
    return cityTaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_TAXTYPE_USED_CODE")
private String stj10TaxtypeUsedCode;
public void setStj10TaxtypeUsedCode(String stj10TaxtypeUsedCode){
    this.stj10TaxtypeUsedCode = stj10TaxtypeUsedCode;
}
public String getStj10TaxtypeUsedCode(){
    return stj10TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_NAME")
private String stj4Name;
public void setStj4Name(String stj4Name){
    this.stj4Name = stj4Name;
}
public String getStj4Name(){
    return stj4Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_SITUS_CODE")
private String stj3SitusCode;
public void setStj3SitusCode(String stj3SitusCode){
    this.stj3SitusCode = stj3SitusCode;
}
public String getStj3SitusCode(){
    return stj3SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_SITUS_CODE")
private String stj7SitusCode;
public void setStj7SitusCode(String stj7SitusCode){
    this.stj7SitusCode = stj7SitusCode;
}
public String getStj7SitusCode(){
    return stj7SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_TAXTYPE_USED_CODE")
private String stj3TaxtypeUsedCode;
public void setStj3TaxtypeUsedCode(String stj3TaxtypeUsedCode){
    this.stj3TaxtypeUsedCode = stj3TaxtypeUsedCode;
}
public String getStj3TaxtypeUsedCode(){
    return stj3TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_TAXCODE_OVER_FLAG")
private String stj5TaxcodeOverFlag;
public void setStj5TaxcodeOverFlag(String stj5TaxcodeOverFlag){
    this.stj5TaxcodeOverFlag = stj5TaxcodeOverFlag;
}
public String getStj5TaxcodeOverFlag(){
    return stj5TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_VENDOR_NEXUS_DTL_ID")
private Long stj7VendorNexusDtlId;
public void setStj7VendorNexusDtlId(Long stj7VendorNexusDtlId){
    this.stj7VendorNexusDtlId = stj7VendorNexusDtlId;
}
public Long getStj7VendorNexusDtlId(){
    return stj7VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_SITUS_CODE")
private String stj9SitusCode;
public void setStj9SitusCode(String stj9SitusCode){
    this.stj9SitusCode = stj9SitusCode;
}
public String getStj9SitusCode(){
    return stj9SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_NAME")
private String countyName;
public void setCountyName(String countyName){
    this.countyName = countyName;
}
public String getCountyName(){
    return countyName;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_NEXUSIND_CODE")
private String stj10NexusindCode;
public void setStj10NexusindCode(String stj10NexusindCode){
    this.stj10NexusindCode = stj10NexusindCode;
}
public String getStj10NexusindCode(){
    return stj10NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_NEXUSIND_CODE")
private String stateNexusindCode;
public void setStateNexusindCode(String stateNexusindCode){
    this.stateNexusindCode = stateNexusindCode;
}
public String getStateNexusindCode(){
    return stateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_SITUS_MATRIX_ID")
private Long stj3SitusMatrixId;
public void setStj3SitusMatrixId(Long stj3SitusMatrixId){
    this.stj3SitusMatrixId = stj3SitusMatrixId;
}
public Long getStj3SitusMatrixId(){
    return stj3SitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_TAXCODE_DETAIL_ID")
private Long stj6TaxcodeDetailId;
public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId){
    this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
}
public Long getStj6TaxcodeDetailId(){
    return stj6TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_NEXUSIND_CODE")
private String cityNexusindCode;
public void setCityNexusindCode(String cityNexusindCode){
    this.cityNexusindCode = cityNexusindCode;
}
public String getCityNexusindCode(){
    return cityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_SITUS_CODE")
private String stj5SitusCode;
public void setStj5SitusCode(String stj5SitusCode){
    this.stj5SitusCode = stj5SitusCode;
}
public String getStj5SitusCode(){
    return stj5SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_NEXUSIND_CODE")
private String stj5NexusindCode;
public void setStj5NexusindCode(String stj5NexusindCode){
    this.stj5NexusindCode = stj5NexusindCode;
}
public String getStj5NexusindCode(){
    return stj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_NEXUSIND_CODE")
private String stj8NexusindCode;
public void setStj8NexusindCode(String stj8NexusindCode){
    this.stj8NexusindCode = stj8NexusindCode;
}
public String getStj8NexusindCode(){
    return stj8NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_TAXCODE_DETAIL_ID")
private Long stj8TaxcodeDetailId;
public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId){
    this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
}
public Long getStj8TaxcodeDetailId(){
    return stj8TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_SITUS_JUR_ID")
private Long stj1SitusJurId;
public void setStj1SitusJurId(Long stj1SitusJurId){
    this.stj1SitusJurId = stj1SitusJurId;
}
public Long getStj1SitusJurId(){
    return stj1SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_SITUS_MATRIX_ID")
private Long stj2SitusMatrixId;
public void setStj2SitusMatrixId(Long stj2SitusMatrixId){
    this.stj2SitusMatrixId = stj2SitusMatrixId;
}
public Long getStj2SitusMatrixId(){
    return stj2SitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_TAXRATE_ID")
private Long stj10TaxrateId;
public void setStj10TaxrateId(Long stj10TaxrateId){
    this.stj10TaxrateId = stj10TaxrateId;
}
public Long getStj10TaxrateId(){
    return stj10TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_SITUS_JUR_ID")
private Long countySitusJurId;
public void setCountySitusJurId(Long countySitusJurId){
    this.countySitusJurId = countySitusJurId;
}
public Long getCountySitusJurId(){
    return countySitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_NAME")
private String stj3Name;
public void setStj3Name(String stj3Name){
    this.stj3Name = stj3Name;
}
public String getStj3Name(){
    return stj3Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_SITUS_CODE")
private String stj4SitusCode;
public void setStj4SitusCode(String stj4SitusCode){
    this.stj4SitusCode = stj4SitusCode;
}
public String getStj4SitusCode(){
    return stj4SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_TAXCODE_TYPE_CODE")
private String stateTaxcodeTypeCode;
public void setStateTaxcodeTypeCode(String stateTaxcodeTypeCode){
    this.stateTaxcodeTypeCode = stateTaxcodeTypeCode;
}
public String getStateTaxcodeTypeCode(){
    return stateTaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_TAXCODE_DETAIL_ID")
private Long stj4TaxcodeDetailId;
public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId){
    this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
}
public Long getStj4TaxcodeDetailId(){
    return stj4TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_TAXRATE_ID")
private Long countryTaxrateId;
public void setCountryTaxrateId(Long countryTaxrateId){
    this.countryTaxrateId = countryTaxrateId;
}
public Long getCountryTaxrateId(){
    return countryTaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_TAXCODE_TYPE_CODE")
private String stj1TaxcodeTypeCode;
public void setStj1TaxcodeTypeCode(String stj1TaxcodeTypeCode){
    this.stj1TaxcodeTypeCode = stj1TaxcodeTypeCode;
}
public String getStj1TaxcodeTypeCode(){
    return stj1TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_TAXTYPE_USED_CODE")
private String stj9TaxtypeUsedCode;
public void setStj9TaxtypeUsedCode(String stj9TaxtypeUsedCode){
    this.stj9TaxtypeUsedCode = stj9TaxtypeUsedCode;
}
public String getStj9TaxtypeUsedCode(){
    return stj9TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_TAXRATE_ID")
private Long cityTaxrateId;
public void setCityTaxrateId(Long cityTaxrateId){
    this.cityTaxrateId = cityTaxrateId;
}
public Long getCityTaxrateId(){
    return cityTaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_TAXCODE_TYPE_CODE")
private String stj6TaxcodeTypeCode;
public void setStj6TaxcodeTypeCode(String stj6TaxcodeTypeCode){
    this.stj6TaxcodeTypeCode = stj6TaxcodeTypeCode;
}
public String getStj6TaxcodeTypeCode(){
    return stj6TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_TAXCODE_TYPE_CODE")
private String stj3TaxcodeTypeCode;
public void setStj3TaxcodeTypeCode(String stj3TaxcodeTypeCode){
    this.stj3TaxcodeTypeCode = stj3TaxcodeTypeCode;
}
public String getStj3TaxcodeTypeCode(){
    return stj3TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_TAXCODE_OVER_FLAG")
private String stj7TaxcodeOverFlag;
public void setStj7TaxcodeOverFlag(String stj7TaxcodeOverFlag){
    this.stj7TaxcodeOverFlag = stj7TaxcodeOverFlag;
}
public String getStj7TaxcodeOverFlag(){
    return stj7TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_TAXRATE_ID")
private Long stj8TaxrateId;
public void setStj8TaxrateId(Long stj8TaxrateId){
    this.stj8TaxrateId = stj8TaxrateId;
}
public Long getStj8TaxrateId(){
    return stj8TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_TAXTYPE_USED_CODE")
private String stateTaxtypeUsedCode;
public void setStateTaxtypeUsedCode(String stateTaxtypeUsedCode){
    this.stateTaxtypeUsedCode = stateTaxtypeUsedCode;
}
public String getStateTaxtypeUsedCode(){
    return stateTaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_NEXUSIND_CODE")
private String stj2NexusindCode;
public void setStj2NexusindCode(String stj2NexusindCode){
    this.stj2NexusindCode = stj2NexusindCode;
}
public String getStj2NexusindCode(){
    return stj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_SITUS_MATRIX_ID")
private Long stj1SitusMatrixId;
public void setStj1SitusMatrixId(Long stj1SitusMatrixId){
    this.stj1SitusMatrixId = stj1SitusMatrixId;
}
public Long getStj1SitusMatrixId(){
    return stj1SitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_TAXCODE_DETAIL_ID")
private Long stj9TaxcodeDetailId;
public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId){
    this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
}
public Long getStj9TaxcodeDetailId(){
    return stj9TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_SITUS_MATRIX_ID")
private Long stj4SitusMatrixId;
public void setStj4SitusMatrixId(Long stj4SitusMatrixId){
    this.stj4SitusMatrixId = stj4SitusMatrixId;
}
public Long getStj4SitusMatrixId(){
    return stj4SitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_TAXCODE_DETAIL_ID")
private Long stj1TaxcodeDetailId;
public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId){
    this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
}
public Long getStj1TaxcodeDetailId(){
    return stj1TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_TAXCODE_OVER_FLAG")
private String stj10TaxcodeOverFlag;
public void setStj10TaxcodeOverFlag(String stj10TaxcodeOverFlag){
    this.stj10TaxcodeOverFlag = stj10TaxcodeOverFlag;
}
public String getStj10TaxcodeOverFlag(){
    return stj10TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_TAXRATE_ID")
private Long stj1TaxrateId;
public void setStj1TaxrateId(Long stj1TaxrateId){
    this.stj1TaxrateId = stj1TaxrateId;
}
public Long getStj1TaxrateId(){
    return stj1TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_NEXUSIND_CODE")
private String stj1NexusindCode;
public void setStj1NexusindCode(String stj1NexusindCode){
    this.stj1NexusindCode = stj1NexusindCode;
}
public String getStj1NexusindCode(){
    return stj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_TAXTYPE_USED_CODE")
private String stj8TaxtypeUsedCode;
public void setStj8TaxtypeUsedCode(String stj8TaxtypeUsedCode){
    this.stj8TaxtypeUsedCode = stj8TaxtypeUsedCode;
}
public String getStj8TaxtypeUsedCode(){
    return stj8TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_VENDOR_NEXUS_DTL_ID")
private Long stj6VendorNexusDtlId;
public void setStj6VendorNexusDtlId(Long stj6VendorNexusDtlId){
    this.stj6VendorNexusDtlId = stj6VendorNexusDtlId;
}
public Long getStj6VendorNexusDtlId(){
    return stj6VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_TAXCODE_OVER_FLAG")
private String stj3TaxcodeOverFlag;
public void setStj3TaxcodeOverFlag(String stj3TaxcodeOverFlag){
    this.stj3TaxcodeOverFlag = stj3TaxcodeOverFlag;
}
public String getStj3TaxcodeOverFlag(){
    return stj3TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_VENDOR_NEXUS_DTL_ID")
private Long stj8VendorNexusDtlId;
public void setStj8VendorNexusDtlId(Long stj8VendorNexusDtlId){
    this.stj8VendorNexusDtlId = stj8VendorNexusDtlId;
}
public Long getStj8VendorNexusDtlId(){
    return stj8VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_TAXCODE_OVER_FLAG")
private String stj6TaxcodeOverFlag;
public void setStj6TaxcodeOverFlag(String stj6TaxcodeOverFlag){
    this.stj6TaxcodeOverFlag = stj6TaxcodeOverFlag;
}
public String getStj6TaxcodeOverFlag(){
    return stj6TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_NEXUSIND_CODE")
private String countryNexusindCode;
public void setCountryNexusindCode(String countryNexusindCode){
    this.countryNexusindCode = countryNexusindCode;
}
public String getCountryNexusindCode(){
    return countryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_TAXCODE_OVER_FLAG")
private String stj2TaxcodeOverFlag;
public void setStj2TaxcodeOverFlag(String stj2TaxcodeOverFlag){
    this.stj2TaxcodeOverFlag = stj2TaxcodeOverFlag;
}
public String getStj2TaxcodeOverFlag(){
    return stj2TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_TAXRATE_ID")
private Long stj7TaxrateId;
public void setStj7TaxrateId(Long stj7TaxrateId){
    this.stj7TaxrateId = stj7TaxrateId;
}
public Long getStj7TaxrateId(){
    return stj7TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_NAME")
private String stj7Name;
public void setStj7Name(String stj7Name){
    this.stj7Name = stj7Name;
}
public String getStj7Name(){
    return stj7Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_TAXCODE_TYPE_CODE")
private String stj5TaxcodeTypeCode;
public void setStj5TaxcodeTypeCode(String stj5TaxcodeTypeCode){
    this.stj5TaxcodeTypeCode = stj5TaxcodeTypeCode;
}
public String getStj5TaxcodeTypeCode(){
    return stj5TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_SITUS_CODE")
private String stj8SitusCode;
public void setStj8SitusCode(String stj8SitusCode){
    this.stj8SitusCode = stj8SitusCode;
}
public String getStj8SitusCode(){
    return stj8SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_VENDOR_NEXUS_DTL_ID")
private Long countryVendorNexusDtlId;
public void setCountryVendorNexusDtlId(Long countryVendorNexusDtlId){
    this.countryVendorNexusDtlId = countryVendorNexusDtlId;
}
public Long getCountryVendorNexusDtlId(){
    return countryVendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_TAXCODE_TYPE_CODE")
private String cityTaxcodeTypeCode;
public void setCityTaxcodeTypeCode(String cityTaxcodeTypeCode){
    this.cityTaxcodeTypeCode = cityTaxcodeTypeCode;
}
public String getCityTaxcodeTypeCode(){
    return cityTaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_NAME")
private String stj6Name;
public void setStj6Name(String stj6Name){
    this.stj6Name = stj6Name;
}
public String getStj6Name(){
    return stj6Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_TAXCODE_TYPE_CODE")
private String countryTaxcodeTypeCode;
public void setCountryTaxcodeTypeCode(String countryTaxcodeTypeCode){
    this.countryTaxcodeTypeCode = countryTaxcodeTypeCode;
}
public String getCountryTaxcodeTypeCode(){
    return countryTaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_SITUS_CODE")
private String citySitusCode;
public void setCitySitusCode(String citySitusCode){
    this.citySitusCode = citySitusCode;
}
public String getCitySitusCode(){
    return citySitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_SITUS_JUR_ID")
private Long stateSitusJurId;
public void setStateSitusJurId(Long stateSitusJurId){
    this.stateSitusJurId = stateSitusJurId;
}
public Long getStateSitusJurId(){
    return stateSitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_SITUS_JUR_ID")
private Long countrySitusJurId;
public void setCountrySitusJurId(Long countrySitusJurId){
    this.countrySitusJurId = countrySitusJurId;
}
public Long getCountrySitusJurId(){
    return countrySitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_TAXCODE_TYPE_CODE")
private String stj9TaxcodeTypeCode;
public void setStj9TaxcodeTypeCode(String stj9TaxcodeTypeCode){
    this.stj9TaxcodeTypeCode = stj9TaxcodeTypeCode;
}
public String getStj9TaxcodeTypeCode(){
    return stj9TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_SITUS_JUR_ID")
private Long stj9SitusJurId;
public void setStj9SitusJurId(Long stj9SitusJurId){
    this.stj9SitusJurId = stj9SitusJurId;
}
public Long getStj9SitusJurId(){
    return stj9SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_TAXCODE_OVER_FLAG")
private String stateTaxcodeOverFlag;
public void setStateTaxcodeOverFlag(String stateTaxcodeOverFlag){
    this.stateTaxcodeOverFlag = stateTaxcodeOverFlag;
}
public String getStateTaxcodeOverFlag(){
    return stateTaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_SITUS_CODE")
private String countySitusCode;
public void setCountySitusCode(String countySitusCode){
    this.countySitusCode = countySitusCode;
}
public String getCountySitusCode(){
    return countySitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_TAXCODE_OVER_FLAG")
private String stj4TaxcodeOverFlag;
public void setStj4TaxcodeOverFlag(String stj4TaxcodeOverFlag){
    this.stj4TaxcodeOverFlag = stj4TaxcodeOverFlag;
}
public String getStj4TaxcodeOverFlag(){
    return stj4TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_TAXCODE_DETAIL_ID")
private Long stateTaxcodeDetailId;
public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId){
    this.stateTaxcodeDetailId = stateTaxcodeDetailId;
}
public Long getStateTaxcodeDetailId(){
    return stateTaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_NAME")
private String stj9Name;
public void setStj9Name(String stj9Name){
    this.stj9Name = stj9Name;
}
public String getStj9Name(){
    return stj9Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_SITUS_MATRIX_ID")
private Long countySitusMatrixId;
public void setCountySitusMatrixId(Long countySitusMatrixId){
    this.countySitusMatrixId = countySitusMatrixId;
}
public Long getCountySitusMatrixId(){
    return countySitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_TAXRATE_ID")
private Long countyTaxrateId;
public void setCountyTaxrateId(Long countyTaxrateId){
    this.countyTaxrateId = countyTaxrateId;
}
public Long getCountyTaxrateId(){
    return countyTaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_TAXCODE_OVER_FLAG")
private String countryTaxcodeOverFlag;
public void setCountryTaxcodeOverFlag(String countryTaxcodeOverFlag){
    this.countryTaxcodeOverFlag = countryTaxcodeOverFlag;
}
public String getCountryTaxcodeOverFlag(){
    return countryTaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_NEXUSIND_CODE")
private String stj4NexusindCode;
public void setStj4NexusindCode(String stj4NexusindCode){
    this.stj4NexusindCode = stj4NexusindCode;
}
public String getStj4NexusindCode(){
    return stj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_SITUS_CODE")
private String stateSitusCode;
public void setStateSitusCode(String stateSitusCode){
    this.stateSitusCode = stateSitusCode;
}
public String getStateSitusCode(){
    return stateSitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_TAXCODE_OVER_FLAG")
private String stj8TaxcodeOverFlag;
public void setStj8TaxcodeOverFlag(String stj8TaxcodeOverFlag){
    this.stj8TaxcodeOverFlag = stj8TaxcodeOverFlag;
}
public String getStj8TaxcodeOverFlag(){
    return stj8TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_SITUS_JUR_ID")
private Long stj2SitusJurId;
public void setStj2SitusJurId(Long stj2SitusJurId){
    this.stj2SitusJurId = stj2SitusJurId;
}
public Long getStj2SitusJurId(){
    return stj2SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_SITUS_JUR_ID")
private Long citySitusJurId;
public void setCitySitusJurId(Long citySitusJurId){
    this.citySitusJurId = citySitusJurId;
}
public Long getCitySitusJurId(){
    return citySitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_NAME")
private String stj2Name;
public void setStj2Name(String stj2Name){
    this.stj2Name = stj2Name;
}
public String getStj2Name(){
    return stj2Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_SITUS_CODE")
private String stj1SitusCode;
public void setStj1SitusCode(String stj1SitusCode){
    this.stj1SitusCode = stj1SitusCode;
}
public String getStj1SitusCode(){
    return stj1SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_VENDOR_NEXUS_DTL_ID")
private Long countyVendorNexusDtlId;
public void setCountyVendorNexusDtlId(Long countyVendorNexusDtlId){
    this.countyVendorNexusDtlId = countyVendorNexusDtlId;
}
public Long getCountyVendorNexusDtlId(){
    return countyVendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_SITUS_CODE")
private String stj6SitusCode;
public void setStj6SitusCode(String stj6SitusCode){
    this.stj6SitusCode = stj6SitusCode;
}
public String getStj6SitusCode(){
    return stj6SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_TAXTYPE_USED_CODE")
private String stj4TaxtypeUsedCode;
public void setStj4TaxtypeUsedCode(String stj4TaxtypeUsedCode){
    this.stj4TaxtypeUsedCode = stj4TaxtypeUsedCode;
}
public String getStj4TaxtypeUsedCode(){
    return stj4TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_TAXCODE_DETAIL_ID")
private Long stj2TaxcodeDetailId;
public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId){
    this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
}
public Long getStj2TaxcodeDetailId(){
    return stj2TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_SITUS_CODE")
private String stj10SitusCode;
public void setStj10SitusCode(String stj10SitusCode){
    this.stj10SitusCode = stj10SitusCode;
}
public String getStj10SitusCode(){
    return stj10SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_NEXUSIND_CODE")
private String stj9NexusindCode;
public void setStj9NexusindCode(String stj9NexusindCode){
    this.stj9NexusindCode = stj9NexusindCode;
}
public String getStj9NexusindCode(){
    return stj9NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_TAXRATE_ID")
private Long stj2TaxrateId;
public void setStj2TaxrateId(Long stj2TaxrateId){
    this.stj2TaxrateId = stj2TaxrateId;
}
public Long getStj2TaxrateId(){
    return stj2TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_TAXTYPE_USED_CODE")
private String cityTaxtypeUsedCode;
public void setCityTaxtypeUsedCode(String cityTaxtypeUsedCode){
    this.cityTaxtypeUsedCode = cityTaxtypeUsedCode;
}
public String getCityTaxtypeUsedCode(){
    return cityTaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_TAXCODE_OVER_FLAG")
private String countyTaxcodeOverFlag;
public void setCountyTaxcodeOverFlag(String countyTaxcodeOverFlag){
    this.countyTaxcodeOverFlag = countyTaxcodeOverFlag;
}
public String getCountyTaxcodeOverFlag(){
    return countyTaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_VENDOR_NEXUS_DTL_ID")
private Long stj4VendorNexusDtlId;
public void setStj4VendorNexusDtlId(Long stj4VendorNexusDtlId){
    this.stj4VendorNexusDtlId = stj4VendorNexusDtlId;
}
public Long getStj4VendorNexusDtlId(){
    return stj4VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_NAME")
private String stj5Name;
public void setStj5Name(String stj5Name){
    this.stj5Name = stj5Name;
}
public String getStj5Name(){
    return stj5Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_TAXRATE_ID")
private Long stateTaxrateId;
public void setStateTaxrateId(Long stateTaxrateId){
    this.stateTaxrateId = stateTaxrateId;
}
public Long getStateTaxrateId(){
    return stateTaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_NEXUSIND_CODE")
private String stj7NexusindCode;
public void setStj7NexusindCode(String stj7NexusindCode){
    this.stj7NexusindCode = stj7NexusindCode;
}
public String getStj7NexusindCode(){
    return stj7NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_TAXTYPE_USED_CODE")
private String countyTaxtypeUsedCode;
public void setCountyTaxtypeUsedCode(String countyTaxtypeUsedCode){
    this.countyTaxtypeUsedCode = countyTaxtypeUsedCode;
}
public String getCountyTaxtypeUsedCode(){
    return countyTaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_TAXCODE_DETAIL_ID")
private Long stj3TaxcodeDetailId;
public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId){
    this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
}
public Long getStj3TaxcodeDetailId(){
    return stj3TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_CODE")
private String countryCode;
public void setCountryCode(String countryCode){
    this.countryCode = countryCode;
}
public String getCountryCode(){
    return countryCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_VENDOR_NEXUS_DTL_ID")
private Long stj1VendorNexusDtlId;
public void setStj1VendorNexusDtlId(Long stj1VendorNexusDtlId){
    this.stj1VendorNexusDtlId = stj1VendorNexusDtlId;
}
public Long getStj1VendorNexusDtlId(){
    return stj1VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_NEXUSIND_CODE")
private String stj3NexusindCode;
public void setStj3NexusindCode(String stj3NexusindCode){
    this.stj3NexusindCode = stj3NexusindCode;
}
public String getStj3NexusindCode(){
    return stj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_TAXCODE_DETAIL_ID")
private Long cityTaxcodeDetailId;
public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId){
    this.cityTaxcodeDetailId = cityTaxcodeDetailId;
}
public Long getCityTaxcodeDetailId(){
    return cityTaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_NEXUSIND_CODE")
private String countyNexusindCode;
public void setCountyNexusindCode(String countyNexusindCode){
    this.countyNexusindCode = countyNexusindCode;
}
public String getCountyNexusindCode(){
    return countyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_SITUS_JUR_ID")
private Long stj4SitusJurId;
public void setStj4SitusJurId(Long stj4SitusJurId){
    this.stj4SitusJurId = stj4SitusJurId;
}
public Long getStj4SitusJurId(){
    return stj4SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_TAXRATE_ID")
private Long stj4TaxrateId;
public void setStj4TaxrateId(Long stj4TaxrateId){
    this.stj4TaxrateId = stj4TaxrateId;
}
public Long getStj4TaxrateId(){
    return stj4TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_TAXCODE_OVER_FLAG")
private String stj9TaxcodeOverFlag;
public void setStj9TaxcodeOverFlag(String stj9TaxcodeOverFlag){
    this.stj9TaxcodeOverFlag = stj9TaxcodeOverFlag;
}
public String getStj9TaxcodeOverFlag(){
    return stj9TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ4_TAXCODE_TYPE_CODE")
private String stj4TaxcodeTypeCode;
public void setStj4TaxcodeTypeCode(String stj4TaxcodeTypeCode){
    this.stj4TaxcodeTypeCode = stj4TaxcodeTypeCode;
}
public String getStj4TaxcodeTypeCode(){
    return stj4TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_VENDOR_NEXUS_DTL_ID")
private Long stj2VendorNexusDtlId;
public void setStj2VendorNexusDtlId(Long stj2VendorNexusDtlId){
    this.stj2VendorNexusDtlId = stj2VendorNexusDtlId;
}
public Long getStj2VendorNexusDtlId(){
    return stj2VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_SITUS_MATRIX_ID")
private Long citySitusMatrixId;
public void setCitySitusMatrixId(Long citySitusMatrixId){
    this.citySitusMatrixId = citySitusMatrixId;
}
public Long getCitySitusMatrixId(){
    return citySitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_TAXCODE_DETAIL_ID")
private Long countyTaxcodeDetailId;
public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId){
    this.countyTaxcodeDetailId = countyTaxcodeDetailId;
}
public Long getCountyTaxcodeDetailId(){
    return countyTaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_TAXCODE_TYPE_CODE")
private String stj2TaxcodeTypeCode;
public void setStj2TaxcodeTypeCode(String stj2TaxcodeTypeCode){
    this.stj2TaxcodeTypeCode = stj2TaxcodeTypeCode;
}
public String getStj2TaxcodeTypeCode(){
    return stj2TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_TAXCODE_DETAIL_ID")
private Long stj7TaxcodeDetailId;
public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId){
    this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
}
public Long getStj7TaxcodeDetailId(){
    return stj7TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_TAXTYPE_USED_CODE")
private String stj6TaxtypeUsedCode;
public void setStj6TaxtypeUsedCode(String stj6TaxtypeUsedCode){
    this.stj6TaxtypeUsedCode = stj6TaxtypeUsedCode;
}
public String getStj6TaxtypeUsedCode(){
    return stj6TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_TAXRATE_ID")
private Long stj9TaxrateId;
public void setStj9TaxrateId(Long stj9TaxrateId){
    this.stj9TaxrateId = stj9TaxrateId;
}
public Long getStj9TaxrateId(){
    return stj9TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_NEXUSIND_CODE")
private String stj6NexusindCode;
public void setStj6NexusindCode(String stj6NexusindCode){
    this.stj6NexusindCode = stj6NexusindCode;
}
public String getStj6NexusindCode(){
    return stj6NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_TAXCODE_TYPE_CODE")
private String stj10TaxcodeTypeCode;
public void setStj10TaxcodeTypeCode(String stj10TaxcodeTypeCode){
    this.stj10TaxcodeTypeCode = stj10TaxcodeTypeCode;
}
public String getStj10TaxcodeTypeCode(){
    return stj10TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_TAXCODE_DETAIL_ID")
private Long stj10TaxcodeDetailId;
public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId){
    this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
}
public Long getStj10TaxcodeDetailId(){
    return stj10TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_SITUS_MATRIX_ID")
private Long countrySitusMatrixId;
public void setCountrySitusMatrixId(Long countrySitusMatrixId){
    this.countrySitusMatrixId = countrySitusMatrixId;
}
public Long getCountrySitusMatrixId(){
    return countrySitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTY_TAXCODE_TYPE_CODE")
private String countyTaxcodeTypeCode;
public void setCountyTaxcodeTypeCode(String countyTaxcodeTypeCode){
    this.countyTaxcodeTypeCode = countyTaxcodeTypeCode;
}
public String getCountyTaxcodeTypeCode(){
    return countyTaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_TAXTYPE_USED_CODE")
private String countryTaxtypeUsedCode;
public void setCountryTaxtypeUsedCode(String countryTaxtypeUsedCode){
    this.countryTaxtypeUsedCode = countryTaxtypeUsedCode;
}
public String getCountryTaxtypeUsedCode(){
    return countryTaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_TAXRATE_ID")
private Long stj5TaxrateId;
public void setStj5TaxrateId(Long stj5TaxrateId){
    this.stj5TaxrateId = stj5TaxrateId;
}
public Long getStj5TaxrateId(){
    return stj5TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_NAME")
private String stj8Name;
public void setStj8Name(String stj8Name){
    this.stj8Name = stj8Name;
}
public String getStj8Name(){
    return stj8Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_SITUS_MATRIX_ID")
private Long stj5SitusMatrixId;
public void setStj5SitusMatrixId(Long stj5SitusMatrixId){
    this.stj5SitusMatrixId = stj5SitusMatrixId;
}
public Long getStj5SitusMatrixId(){
    return stj5SitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_TAXRATE_ID")
private Long stj6TaxrateId;
public void setStj6TaxrateId(Long stj6TaxrateId){
    this.stj6TaxrateId = stj6TaxrateId;
}
public Long getStj6TaxrateId(){
    return stj6TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_TAXTYPE_USED_CODE")
private String stj2TaxtypeUsedCode;
public void setStj2TaxtypeUsedCode(String stj2TaxtypeUsedCode){
    this.stj2TaxtypeUsedCode = stj2TaxtypeUsedCode;
}
public String getStj2TaxtypeUsedCode(){
    return stj2TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_SITUS_MATRIX_ID")
private Long stateSitusMatrixId;
public void setStateSitusMatrixId(Long stateSitusMatrixId){
    this.stateSitusMatrixId = stateSitusMatrixId;
}
public Long getStateSitusMatrixId(){
    return stateSitusMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_SITUS_JUR_ID")
private Long stj10SitusJurId;
public void setStj10SitusJurId(Long stj10SitusJurId){
    this.stj10SitusJurId = stj10SitusJurId;
}
public Long getStj10SitusJurId(){
    return stj10SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_TAXCODE_OVER_FLAG")
private String stj1TaxcodeOverFlag;
public void setStj1TaxcodeOverFlag(String stj1TaxcodeOverFlag){
    this.stj1TaxcodeOverFlag = stj1TaxcodeOverFlag;
}
public String getStj1TaxcodeOverFlag(){
    return stj1TaxcodeOverFlag;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ1_TAXTYPE_USED_CODE")
private String stj1TaxtypeUsedCode;
public void setStj1TaxtypeUsedCode(String stj1TaxtypeUsedCode){
    this.stj1TaxtypeUsedCode = stj1TaxtypeUsedCode;
}
public String getStj1TaxtypeUsedCode(){
    return stj1TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_TAXCODE_DETAIL_ID")
private Long countryTaxcodeDetailId;
public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId){
    this.countryTaxcodeDetailId = countryTaxcodeDetailId;
}
public Long getCountryTaxcodeDetailId(){
    return countryTaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_TAXCODE_TYPE_CODE")
private String stj8TaxcodeTypeCode;
public void setStj8TaxcodeTypeCode(String stj8TaxcodeTypeCode){
    this.stj8TaxcodeTypeCode = stj8TaxcodeTypeCode;
}
public String getStj8TaxcodeTypeCode(){
    return stj8TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_VENDOR_NEXUS_DTL_ID")
private Long cityVendorNexusDtlId;
public void setCityVendorNexusDtlId(Long cityVendorNexusDtlId){
    this.cityVendorNexusDtlId = cityVendorNexusDtlId;
}
public Long getCityVendorNexusDtlId(){
    return cityVendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ6_SITUS_JUR_ID")
private Long stj6SitusJurId;
public void setStj6SitusJurId(Long stj6SitusJurId){
    this.stj6SitusJurId = stj6SitusJurId;
}
public Long getStj6SitusJurId(){
    return stj6SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_SITUS_JUR_ID")
private Long stj3SitusJurId;
public void setStj3SitusJurId(Long stj3SitusJurId){
    this.stj3SitusJurId = stj3SitusJurId;
}
public Long getStj3SitusJurId(){
    return stj3SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_TAXCODE_DETAIL_ID")
private Long stj5TaxcodeDetailId;
public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId){
    this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
}
public Long getStj5TaxcodeDetailId(){
    return stj5TaxcodeDetailId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_VENDOR_NEXUS_DTL_ID")
private Long stj5VendorNexusDtlId;
public void setStj5VendorNexusDtlId(Long stj5VendorNexusDtlId){
    this.stj5VendorNexusDtlId = stj5VendorNexusDtlId;
}
public Long getStj5VendorNexusDtlId(){
    return stj5VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ2_SITUS_CODE")
private String stj2SitusCode;
public void setStj2SitusCode(String stj2SitusCode){
    this.stj2SitusCode = stj2SitusCode;
}
public String getStj2SitusCode(){
    return stj2SitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_CODE")
private String stateCode;
public void setStateCode(String stateCode){
    this.stateCode = stateCode;
}
public String getStateCode(){
    return stateCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "COUNTRY_SITUS_CODE")
private String countrySitusCode;
public void setCountrySitusCode(String countrySitusCode){
    this.countrySitusCode = countrySitusCode;
}
public String getCountrySitusCode(){
    return countrySitusCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ3_TAXRATE_ID")
private Long stj3TaxrateId;
public void setStj3TaxrateId(Long stj3TaxrateId){
    this.stj3TaxrateId = stj3TaxrateId;
}
public Long getStj3TaxrateId(){
    return stj3TaxrateId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_SITUS_JUR_ID")
private Long stj7SitusJurId;
public void setStj7SitusJurId(Long stj7SitusJurId){
    this.stj7SitusJurId = stj7SitusJurId;
}
public Long getStj7SitusJurId(){
    return stj7SitusJurId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_VENDOR_NEXUS_DTL_ID")
private Long stj10VendorNexusDtlId;
public void setStj10VendorNexusDtlId(Long stj10VendorNexusDtlId){
    this.stj10VendorNexusDtlId = stj10VendorNexusDtlId;
}
public Long getStj10VendorNexusDtlId(){
    return stj10VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ7_TAXCODE_TYPE_CODE")
private String stj7TaxcodeTypeCode;
public void setStj7TaxcodeTypeCode(String stj7TaxcodeTypeCode){
    this.stj7TaxcodeTypeCode = stj7TaxcodeTypeCode;
}
public String getStj7TaxcodeTypeCode(){
    return stj7TaxcodeTypeCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ9_VENDOR_NEXUS_DTL_ID")
private Long stj9VendorNexusDtlId;
public void setStj9VendorNexusDtlId(Long stj9VendorNexusDtlId){
    this.stj9VendorNexusDtlId = stj9VendorNexusDtlId;
}
public Long getStj9VendorNexusDtlId(){
    return stj9VendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ5_TAXTYPE_USED_CODE")
private String stj5TaxtypeUsedCode;
public void setStj5TaxtypeUsedCode(String stj5TaxtypeUsedCode){
    this.stj5TaxtypeUsedCode = stj5TaxtypeUsedCode;
}
public String getStj5TaxtypeUsedCode(){
    return stj5TaxtypeUsedCode;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STATE_VENDOR_NEXUS_DTL_ID")
private Long stateVendorNexusDtlId;
public void setStateVendorNexusDtlId(Long stateVendorNexusDtlId){
    this.stateVendorNexusDtlId = stateVendorNexusDtlId;
}
public Long getStateVendorNexusDtlId(){
    return stateVendorNexusDtlId;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "CITY_NAME")
private String cityName;
public void setCityName(String cityName){
    this.cityName = cityName;
}
public String getCityName(){
    return cityName;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ10_NAME")
private String stj10Name;
public void setStj10Name(String stj10Name){
    this.stj10Name = stj10Name;
}
public String getStj10Name(){
    return stj10Name;
}

@Column(table= "TB_PURCHSCENARIO_JURDTL", name = "STJ8_SITUS_JUR_ID")
private Long stj8SitusJurId;
public void setStj8SitusJurId(Long stj8SitusJurId){
    this.stj8SitusJurId = stj8SitusJurId;
}
public Long getStj8SitusJurId(){
    return stj8SitusJurId;
}


@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ2_NEXUSIND_CODE")
private String shiptoStj2NexusindCode;
public void setShiptoStj2NexusindCode(String shiptoStj2NexusindCode){
    this.shiptoStj2NexusindCode = shiptoStj2NexusindCode;
}
public String getShiptoStj2NexusindCode(){
    return shiptoStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_LOCN_MATRIX_ID")
private Long firstuseLocnMatrixId;
public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId){
    this.firstuseLocnMatrixId = firstuseLocnMatrixId;
}
public Long getFirstuseLocnMatrixId(){
    return firstuseLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_COUNTY")
private String billtoCounty;
public void setBilltoCounty(String billtoCounty){
    this.billtoCounty = billtoCounty;
}
public String getBilltoCounty(){
    return billtoCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_JURISDICTION_ID")
private Long ordracptJurisdictionId;
public void setOrdracptJurisdictionId(Long ordracptJurisdictionId){
    this.ordracptJurisdictionId = ordracptJurisdictionId;
}
public Long getOrdracptJurisdictionId(){
    return ordracptJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_CITY")
private String shiptoCity;
public void setShiptoCity(String shiptoCity){
    this.shiptoCity = shiptoCity;
}
public String getShiptoCity(){
    return shiptoCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_COUNTRY_CODE")
private String firstuseCountryCode;
public void setFirstuseCountryCode(String firstuseCountryCode){
    this.firstuseCountryCode = firstuseCountryCode;
}
public String getFirstuseCountryCode(){
    return firstuseCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ5_NEXUSIND_CODE")
private String ordracptStj5NexusindCode;
public void setOrdracptStj5NexusindCode(String ordracptStj5NexusindCode){
    this.ordracptStj5NexusindCode = ordracptStj5NexusindCode;
}
public String getOrdracptStj5NexusindCode(){
    return ordracptStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ1_NEXUSIND_CODE")
private String ordrorgnStj1NexusindCode;
public void setOrdrorgnStj1NexusindCode(String ordrorgnStj1NexusindCode){
    this.ordrorgnStj1NexusindCode = ordrorgnStj1NexusindCode;
}
public String getOrdrorgnStj1NexusindCode(){
    return ordrorgnStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_COUNTY")
private String ordracptCounty;
public void setOrdracptCounty(String ordracptCounty){
    this.ordracptCounty = ordracptCounty;
}
public String getOrdracptCounty(){
    return ordracptCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_LAT")
private String ttlxfrLat;
public void setTtlxfrLat(String ttlxfrLat){
    this.ttlxfrLat = ttlxfrLat;
}
public String getTtlxfrLat(){
    return ttlxfrLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_GEOCODE")
private String firstuseGeocode;
public void setFirstuseGeocode(String firstuseGeocode){
    this.firstuseGeocode = firstuseGeocode;
}
public String getFirstuseGeocode(){
    return firstuseGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ3_NAME")
private String ordracptStj3Name;
public void setOrdracptStj3Name(String ordracptStj3Name){
    this.ordracptStj3Name = ordracptStj3Name;
}
public String getOrdracptStj3Name(){
    return ordracptStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_MANUAL_JUR_IND")
private String shiptoManualJurInd;
public void setShiptoManualJurInd(String shiptoManualJurInd){
    this.shiptoManualJurInd = shiptoManualJurInd;
}
public String getShiptoManualJurInd(){
    return shiptoManualJurInd;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ3_NEXUSIND_CODE")
private String ordracptStj3NexusindCode;
public void setOrdracptStj3NexusindCode(String ordracptStj3NexusindCode){
    this.ordracptStj3NexusindCode = ordracptStj3NexusindCode;
}
public String getOrdracptStj3NexusindCode(){
    return ordracptStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ4_NEXUSIND_CODE")
private String ttlxfrStj4NexusindCode;
public void setTtlxfrStj4NexusindCode(String ttlxfrStj4NexusindCode){
    this.ttlxfrStj4NexusindCode = ttlxfrStj4NexusindCode;
}
public String getTtlxfrStj4NexusindCode(){
    return ttlxfrStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ1_NAME")
private String ttlxfrStj1Name;
public void setTtlxfrStj1Name(String ttlxfrStj1Name){
    this.ttlxfrStj1Name = ttlxfrStj1Name;
}
public String getTtlxfrStj1Name(){
    return ttlxfrStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_LOCATION_NAME")
private String shiptoLocationName;
public void setShiptoLocationName(String shiptoLocationName){
    this.shiptoLocationName = shiptoLocationName;
}
public String getShiptoLocationName(){
    return shiptoLocationName;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_GEOCODE")
private String billtoGeocode;
public void setBilltoGeocode(String billtoGeocode){
    this.billtoGeocode = billtoGeocode;
}
public String getBilltoGeocode(){
    return billtoGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_LAT")
private String ordrorgnLat;
public void setOrdrorgnLat(String ordrorgnLat){
    this.ordrorgnLat = ordrorgnLat;
}
public String getOrdrorgnLat(){
    return ordrorgnLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_CITY")
private String ordrorgnCity;
public void setOrdrorgnCity(String ordrorgnCity){
    this.ordrorgnCity = ordrorgnCity;
}
public String getOrdrorgnCity(){
    return ordrorgnCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_CITY_NEXUSIND_CODE")
private String shipfromCityNexusindCode;
public void setShipfromCityNexusindCode(String shipfromCityNexusindCode){
    this.shipfromCityNexusindCode = shipfromCityNexusindCode;
}
public String getShipfromCityNexusindCode(){
    return shipfromCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_LONG")
private String ordracptLong;
public void setOrdracptLong(String ordracptLong){
    this.ordracptLong = ordracptLong;
}
public String getOrdracptLong(){
    return ordracptLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_COUNTRY_NEXUSIND_CODE")
private String shiptoCountryNexusindCode;
public void setShiptoCountryNexusindCode(String shiptoCountryNexusindCode){
    this.shiptoCountryNexusindCode = shiptoCountryNexusindCode;
}
public String getShiptoCountryNexusindCode(){
    return shiptoCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ4_NAME")
private String billtoStj4Name;
public void setBilltoStj4Name(String billtoStj4Name){
    this.billtoStj4Name = billtoStj4Name;
}
public String getBilltoStj4Name(){
    return billtoStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ5_NAME")
private String shipfromStj5Name;
public void setShipfromStj5Name(String shipfromStj5Name){
    this.shipfromStj5Name = shipfromStj5Name;
}
public String getShipfromStj5Name(){
    return shipfromStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_COUNTY_NEXUSIND_CODE")
private String shiptoCountyNexusindCode;
public void setShiptoCountyNexusindCode(String shiptoCountyNexusindCode){
    this.shiptoCountyNexusindCode = shiptoCountyNexusindCode;
}
public String getShiptoCountyNexusindCode(){
    return shiptoCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ2_NEXUSIND_CODE")
private String firstuseStj2NexusindCode;
public void setFirstuseStj2NexusindCode(String firstuseStj2NexusindCode){
    this.firstuseStj2NexusindCode = firstuseStj2NexusindCode;
}
public String getFirstuseStj2NexusindCode(){
    return firstuseStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ4_NAME")
private String shipfromStj4Name;
public void setShipfromStj4Name(String shipfromStj4Name){
    this.shipfromStj4Name = shipfromStj4Name;
}
public String getShipfromStj4Name(){
    return shipfromStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ1_NAME")
private String shipfromStj1Name;
public void setShipfromStj1Name(String shipfromStj1Name){
    this.shipfromStj1Name = shipfromStj1Name;
}
public String getShipfromStj1Name(){
    return shipfromStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ2_NAME")
private String shiptoStj2Name;
public void setShiptoStj2Name(String shiptoStj2Name){
    this.shiptoStj2Name = shiptoStj2Name;
}
public String getShiptoStj2Name(){
    return shiptoStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ENTITY_ID")
private Long ordrorgnEntityId;
public void setOrdrorgnEntityId(Long ordrorgnEntityId){
    this.ordrorgnEntityId = ordrorgnEntityId;
}
public Long getOrdrorgnEntityId(){
    return ordrorgnEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_CITY_NEXUSIND_CODE")
private String ttlxfrCityNexusindCode;
public void setTtlxfrCityNexusindCode(String ttlxfrCityNexusindCode){
    this.ttlxfrCityNexusindCode = ttlxfrCityNexusindCode;
}
public String getTtlxfrCityNexusindCode(){
    return ttlxfrCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ENTITY_CODE")
private String shiptoEntityCode;
public void setShiptoEntityCode(String shiptoEntityCode){
    this.shiptoEntityCode = shiptoEntityCode;
}
public String getShiptoEntityCode(){
    return shiptoEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ1_NEXUSIND_CODE")
private String ordracptStj1NexusindCode;
public void setOrdracptStj1NexusindCode(String ordracptStj1NexusindCode){
    this.ordracptStj1NexusindCode = ordracptStj1NexusindCode;
}
public String getOrdracptStj1NexusindCode(){
    return ordracptStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ3_NAME")
private String shiptoStj3Name;
public void setShiptoStj3Name(String shiptoStj3Name){
    this.shiptoStj3Name = shiptoStj3Name;
}
public String getShiptoStj3Name(){
    return shiptoStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ZIP")
private String ordracptZip;
public void setOrdracptZip(String ordracptZip){
    this.ordracptZip = ordracptZip;
}
public String getOrdracptZip(){
    return ordracptZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_JURISDICTION_ID")
private Long shipfromJurisdictionId;
public void setShipfromJurisdictionId(Long shipfromJurisdictionId){
    this.shipfromJurisdictionId = shipfromJurisdictionId;
}
public Long getShipfromJurisdictionId(){
    return shipfromJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ5_NAME")
private String shiptoStj5Name;
public void setShiptoStj5Name(String shiptoStj5Name){
    this.shiptoStj5Name = shiptoStj5Name;
}
public String getShiptoStj5Name(){
    return shiptoStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_LOCN_MATRIX_ID")
private Long ttlxfrLocnMatrixId;
public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId){
    this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
}
public Long getTtlxfrLocnMatrixId(){
    return ttlxfrLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ5_NEXUSIND_CODE")
private String ordrorgnStj5NexusindCode;
public void setOrdrorgnStj5NexusindCode(String ordrorgnStj5NexusindCode){
    this.ordrorgnStj5NexusindCode = ordrorgnStj5NexusindCode;
}
public String getOrdrorgnStj5NexusindCode(){
    return ordrorgnStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_COUNTY_NEXUSIND_CODE")
private String ordracptCountyNexusindCode;
public void setOrdracptCountyNexusindCode(String ordracptCountyNexusindCode){
    this.ordracptCountyNexusindCode = ordracptCountyNexusindCode;
}
public String getOrdracptCountyNexusindCode(){
    return ordracptCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_COUNTY_NEXUSIND_CODE")
private String shipfromCountyNexusindCode;
public void setShipfromCountyNexusindCode(String shipfromCountyNexusindCode){
    this.shipfromCountyNexusindCode = shipfromCountyNexusindCode;
}
public String getShipfromCountyNexusindCode(){
    return shipfromCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_COUNTY_NEXUSIND_CODE")
private String firstuseCountyNexusindCode;
public void setFirstuseCountyNexusindCode(String firstuseCountyNexusindCode){
    this.firstuseCountyNexusindCode = firstuseCountyNexusindCode;
}
public String getFirstuseCountyNexusindCode(){
    return firstuseCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_LOCN_MATRIX_ID")
private Long billtoLocnMatrixId;
public void setBilltoLocnMatrixId(Long billtoLocnMatrixId){
    this.billtoLocnMatrixId = billtoLocnMatrixId;
}
public Long getBilltoLocnMatrixId(){
    return billtoLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ENTITY_ID")
private Long ordracptEntityId;
public void setOrdracptEntityId(Long ordracptEntityId){
    this.ordracptEntityId = ordracptEntityId;
}
public Long getOrdracptEntityId(){
    return ordracptEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ4_NAME")
private String ordracptStj4Name;
public void setOrdracptStj4Name(String ordracptStj4Name){
    this.ordracptStj4Name = ordracptStj4Name;
}
public String getOrdracptStj4Name(){
    return ordracptStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_COUNTRY_NEXUSIND_CODE")
private String firstuseCountryNexusindCode;
public void setFirstuseCountryNexusindCode(String firstuseCountryNexusindCode){
    this.firstuseCountryNexusindCode = firstuseCountryNexusindCode;
}
public String getFirstuseCountryNexusindCode(){
    return firstuseCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_CITY")
private String ordracptCity;
public void setOrdracptCity(String ordracptCity){
    this.ordracptCity = ordracptCity;
}
public String getOrdracptCity(){
    return ordracptCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ZIP")
private String ordrorgnZip;
public void setOrdrorgnZip(String ordrorgnZip){
    this.ordrorgnZip = ordrorgnZip;
}
public String getOrdrorgnZip(){
    return ordrorgnZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ3_NAME")
private String ordrorgnStj3Name;
public void setOrdrorgnStj3Name(String ordrorgnStj3Name){
    this.ordrorgnStj3Name = ordrorgnStj3Name;
}
public String getOrdrorgnStj3Name(){
    return ordrorgnStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ZIP")
private String ttlxfrZip;
public void setTtlxfrZip(String ttlxfrZip){
    this.ttlxfrZip = ttlxfrZip;
}
public String getTtlxfrZip(){
    return ttlxfrZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ENTITY_CODE")
private String billtoEntityCode;
public void setBilltoEntityCode(String billtoEntityCode){
    this.billtoEntityCode = billtoEntityCode;
}
public String getBilltoEntityCode(){
    return billtoEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_GEOCODE")
private String ordrorgnGeocode;
public void setOrdrorgnGeocode(String ordrorgnGeocode){
    this.ordrorgnGeocode = ordrorgnGeocode;
}
public String getOrdrorgnGeocode(){
    return ordrorgnGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ1_NEXUSIND_CODE")
private String shiptoStj1NexusindCode;
public void setShiptoStj1NexusindCode(String shiptoStj1NexusindCode){
    this.shiptoStj1NexusindCode = shiptoStj1NexusindCode;
}
public String getShiptoStj1NexusindCode(){
    return shiptoStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ5_NEXUSIND_CODE")
private String shiptoStj5NexusindCode;
public void setShiptoStj5NexusindCode(String shiptoStj5NexusindCode){
    this.shiptoStj5NexusindCode = shiptoStj5NexusindCode;
}
public String getShiptoStj5NexusindCode(){
    return shiptoStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ENTITY_ID")
private Long firstuseEntityId;
public void setFirstuseEntityId(Long firstuseEntityId){
    this.firstuseEntityId = firstuseEntityId;
}
public Long getFirstuseEntityId(){
    return firstuseEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ZIP")
private String shiptoZip;
public void setShiptoZip(String shiptoZip){
    this.shiptoZip = shiptoZip;
}
public String getShiptoZip(){
    return shiptoZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ2_NEXUSIND_CODE")
private String ordrorgnStj2NexusindCode;
public void setOrdrorgnStj2NexusindCode(String ordrorgnStj2NexusindCode){
    this.ordrorgnStj2NexusindCode = ordrorgnStj2NexusindCode;
}
public String getOrdrorgnStj2NexusindCode(){
    return ordrorgnStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STATE_NEXUSIND_CODE")
private String firstuseStateNexusindCode;
public void setFirstuseStateNexusindCode(String firstuseStateNexusindCode){
    this.firstuseStateNexusindCode = firstuseStateNexusindCode;
}
public String getFirstuseStateNexusindCode(){
    return firstuseStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ENTITY_ID")
private Long billtoEntityId;
public void setBilltoEntityId(Long billtoEntityId){
    this.billtoEntityId = billtoEntityId;
}
public Long getBilltoEntityId(){
    return billtoEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_COUNTRY_NEXUSIND_CODE")
private String ttlxfrCountryNexusindCode;
public void setTtlxfrCountryNexusindCode(String ttlxfrCountryNexusindCode){
    this.ttlxfrCountryNexusindCode = ttlxfrCountryNexusindCode;
}
public String getTtlxfrCountryNexusindCode(){
    return ttlxfrCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ2_NEXUSIND_CODE")
private String shipfromStj2NexusindCode;
public void setShipfromStj2NexusindCode(String shipfromStj2NexusindCode){
    this.shipfromStj2NexusindCode = shipfromStj2NexusindCode;
}
public String getShipfromStj2NexusindCode(){
    return shipfromStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_COUNTRY_CODE")
private String billtoCountryCode;
public void setBilltoCountryCode(String billtoCountryCode){
    this.billtoCountryCode = billtoCountryCode;
}
public String getBilltoCountryCode(){
    return billtoCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ2_NEXUSIND_CODE")
private String ordracptStj2NexusindCode;
public void setOrdracptStj2NexusindCode(String ordracptStj2NexusindCode){
    this.ordracptStj2NexusindCode = ordracptStj2NexusindCode;
}
public String getOrdracptStj2NexusindCode(){
    return ordracptStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ2_NAME")
private String ordrorgnStj2Name;
public void setOrdrorgnStj2Name(String ordrorgnStj2Name){
    this.ordrorgnStj2Name = ordrorgnStj2Name;
}
public String getOrdrorgnStj2Name(){
    return ordrorgnStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_GEOCODE")
private String ordracptGeocode;
public void setOrdracptGeocode(String ordracptGeocode){
    this.ordracptGeocode = ordracptGeocode;
}
public String getOrdracptGeocode(){
    return ordracptGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_COUNTY")
private String ttlxfrCounty;
public void setTtlxfrCounty(String ttlxfrCounty){
    this.ttlxfrCounty = ttlxfrCounty;
}
public String getTtlxfrCounty(){
    return ttlxfrCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_COUNTY_NEXUSIND_CODE")
private String ttlxfrCountyNexusindCode;
public void setTtlxfrCountyNexusindCode(String ttlxfrCountyNexusindCode){
    this.ttlxfrCountyNexusindCode = ttlxfrCountyNexusindCode;
}
public String getTtlxfrCountyNexusindCode(){
    return ttlxfrCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ4_NAME")
private String ordrorgnStj4Name;
public void setOrdrorgnStj4Name(String ordrorgnStj4Name){
    this.ordrorgnStj4Name = ordrorgnStj4Name;
}
public String getOrdrorgnStj4Name(){
    return ordrorgnStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STATE_CODE")
private String shipfromStateCode;
public void setShipfromStateCode(String shipfromStateCode){
    this.shipfromStateCode = shipfromStateCode;
}
public String getShipfromStateCode(){
    return shipfromStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ3_NAME")
private String ttlxfrStj3Name;
public void setTtlxfrStj3Name(String ttlxfrStj3Name){
    this.ttlxfrStj3Name = ttlxfrStj3Name;
}
public String getTtlxfrStj3Name(){
    return ttlxfrStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_COUNTY_NEXUSIND_CODE")
private String ordrorgnCountyNexusindCode;
public void setOrdrorgnCountyNexusindCode(String ordrorgnCountyNexusindCode){
    this.ordrorgnCountyNexusindCode = ordrorgnCountyNexusindCode;
}
public String getOrdrorgnCountyNexusindCode(){
    return ordrorgnCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STATE_CODE")
private String ordrorgnStateCode;
public void setOrdrorgnStateCode(String ordrorgnStateCode){
    this.ordrorgnStateCode = ordrorgnStateCode;
}
public String getOrdrorgnStateCode(){
    return ordrorgnStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_CITY")
private String billtoCity;
public void setBilltoCity(String billtoCity){
    this.billtoCity = billtoCity;
}
public String getBilltoCity(){
    return billtoCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STATE_CODE")
private String firstuseStateCode;
public void setFirstuseStateCode(String firstuseStateCode){
    this.firstuseStateCode = firstuseStateCode;
}
public String getFirstuseStateCode(){
    return firstuseStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ4_NAME")
private String shiptoStj4Name;
public void setShiptoStj4Name(String shiptoStj4Name){
    this.shiptoStj4Name = shiptoStj4Name;
}
public String getShiptoStj4Name(){
    return shiptoStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ZIPPLUS4")
private String shipfromZipplus4;
public void setShipfromZipplus4(String shipfromZipplus4){
    this.shipfromZipplus4 = shipfromZipplus4;
}
public String getShipfromZipplus4(){
    return shipfromZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_COUNTRY_NEXUSIND_CODE")
private String shipfromCountryNexusindCode;
public void setShipfromCountryNexusindCode(String shipfromCountryNexusindCode){
    this.shipfromCountryNexusindCode = shipfromCountryNexusindCode;
}
public String getShipfromCountryNexusindCode(){
    return shipfromCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STATE_CODE")
private String billtoStateCode;
public void setBilltoStateCode(String billtoStateCode){
    this.billtoStateCode = billtoStateCode;
}
public String getBilltoStateCode(){
    return billtoStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ5_NAME")
private String firstuseStj5Name;
public void setFirstuseStj5Name(String firstuseStj5Name){
    this.firstuseStj5Name = firstuseStj5Name;
}
public String getFirstuseStj5Name(){
    return firstuseStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ4_NEXUSIND_CODE")
private String shipfromStj4NexusindCode;
public void setShipfromStj4NexusindCode(String shipfromStj4NexusindCode){
    this.shipfromStj4NexusindCode = shipfromStj4NexusindCode;
}
public String getShipfromStj4NexusindCode(){
    return shipfromStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ5_NAME")
private String ordracptStj5Name;
public void setOrdracptStj5Name(String ordracptStj5Name){
    this.ordracptStj5Name = ordracptStj5Name;
}
public String getOrdracptStj5Name(){
    return ordracptStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_LOCN_MATRIX_ID")
private Long shiptoLocnMatrixId;
public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId){
    this.shiptoLocnMatrixId = shiptoLocnMatrixId;
}
public Long getShiptoLocnMatrixId(){
    return shiptoLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STATE_CODE")
private String ordracptStateCode;
public void setOrdracptStateCode(String ordracptStateCode){
    this.ordracptStateCode = ordracptStateCode;
}
public String getOrdracptStateCode(){
    return ordracptStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ADDRESS_LINE_2")
private String shipfromAddressLine2;
public void setShipfromAddressLine2(String shipfromAddressLine2){
    this.shipfromAddressLine2 = shipfromAddressLine2;
}
public String getShipfromAddressLine2(){
    return shipfromAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_LAT")
private String ordracptLat;
public void setOrdracptLat(String ordracptLat){
    this.ordracptLat = ordracptLat;
}
public String getOrdracptLat(){
    return ordracptLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ADDRESS_LINE_1")
private String shipfromAddressLine1;
public void setShipfromAddressLine1(String shipfromAddressLine1){
    this.shipfromAddressLine1 = shipfromAddressLine1;
}
public String getShipfromAddressLine1(){
    return shipfromAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ZIPPLUS4")
private String ordrorgnZipplus4;
public void setOrdrorgnZipplus4(String ordrorgnZipplus4){
    this.ordrorgnZipplus4 = ordrorgnZipplus4;
}
public String getOrdrorgnZipplus4(){
    return ordrorgnZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ZIPPLUS4")
private String ttlxfrZipplus4;
public void setTtlxfrZipplus4(String ttlxfrZipplus4){
    this.ttlxfrZipplus4 = ttlxfrZipplus4;
}
public String getTtlxfrZipplus4(){
    return ttlxfrZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ENTITY_ID")
private Long shiptoEntityId;
public void setShiptoEntityId(Long shiptoEntityId){
    this.shiptoEntityId = shiptoEntityId;
}
public Long getShiptoEntityId(){
    return shiptoEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ5_NEXUSIND_CODE")
private String billtoStj5NexusindCode;
public void setBilltoStj5NexusindCode(String billtoStj5NexusindCode){
    this.billtoStj5NexusindCode = billtoStj5NexusindCode;
}
public String getBilltoStj5NexusindCode(){
    return billtoStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ZIPPLUS4")
private String shiptoZipplus4;
public void setShiptoZipplus4(String shiptoZipplus4){
    this.shiptoZipplus4 = shiptoZipplus4;
}
public String getShiptoZipplus4(){
    return shiptoZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_CITY_NEXUSIND_CODE")
private String billtoCityNexusindCode;
public void setBilltoCityNexusindCode(String billtoCityNexusindCode){
    this.billtoCityNexusindCode = billtoCityNexusindCode;
}
public String getBilltoCityNexusindCode(){
    return billtoCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_CITY_NEXUSIND_CODE")
private String ordracptCityNexusindCode;
public void setOrdracptCityNexusindCode(String ordracptCityNexusindCode){
    this.ordracptCityNexusindCode = ordracptCityNexusindCode;
}
public String getOrdracptCityNexusindCode(){
    return ordracptCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ADDRESS_LINE_2")
private String billtoAddressLine2;
public void setBilltoAddressLine2(String billtoAddressLine2){
    this.billtoAddressLine2 = billtoAddressLine2;
}
public String getBilltoAddressLine2(){
    return billtoAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_JURISDICTION_ID")
private Long ttlxfrJurisdictionId;
public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId){
    this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
}
public Long getTtlxfrJurisdictionId(){
    return ttlxfrJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ADDRESS_LINE_1")
private String billtoAddressLine1;
public void setBilltoAddressLine1(String billtoAddressLine1){
    this.billtoAddressLine1 = billtoAddressLine1;
}
public String getBilltoAddressLine1(){
    return billtoAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_GEOCODE")
private String ttlxfrGeocode;
public void setTtlxfrGeocode(String ttlxfrGeocode){
    this.ttlxfrGeocode = ttlxfrGeocode;
}
public String getTtlxfrGeocode(){
    return ttlxfrGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_COUNTY_NEXUSIND_CODE")
private String billtoCountyNexusindCode;
public void setBilltoCountyNexusindCode(String billtoCountyNexusindCode){
    this.billtoCountyNexusindCode = billtoCountyNexusindCode;
}
public String getBilltoCountyNexusindCode(){
    return billtoCountyNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ3_NEXUSIND_CODE")
private String ttlxfrStj3NexusindCode;
public void setTtlxfrStj3NexusindCode(String ttlxfrStj3NexusindCode){
    this.ttlxfrStj3NexusindCode = ttlxfrStj3NexusindCode;
}
public String getTtlxfrStj3NexusindCode(){
    return ttlxfrStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STATE_NEXUSIND_CODE")
private String ordracptStateNexusindCode;
public void setOrdracptStateNexusindCode(String ordracptStateNexusindCode){
    this.ordracptStateNexusindCode = ordracptStateNexusindCode;
}
public String getOrdracptStateNexusindCode(){
    return ordracptStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ZIPPLUS4")
private String firstuseZipplus4;
public void setFirstuseZipplus4(String firstuseZipplus4){
    this.firstuseZipplus4 = firstuseZipplus4;
}
public String getFirstuseZipplus4(){
    return firstuseZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ5_NAME")
private String ordrorgnStj5Name;
public void setOrdrorgnStj5Name(String ordrorgnStj5Name){
    this.ordrorgnStj5Name = ordrorgnStj5Name;
}
public String getOrdrorgnStj5Name(){
    return ordrorgnStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ5_NEXUSIND_CODE")
private String shipfromStj5NexusindCode;
public void setShipfromStj5NexusindCode(String shipfromStj5NexusindCode){
    this.shipfromStj5NexusindCode = shipfromStj5NexusindCode;
}
public String getShipfromStj5NexusindCode(){
    return shipfromStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ3_NAME")
private String billtoStj3Name;
public void setBilltoStj3Name(String billtoStj3Name){
    this.billtoStj3Name = billtoStj3Name;
}
public String getBilltoStj3Name(){
    return billtoStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ZIPPLUS4")
private String ordracptZipplus4;
public void setOrdracptZipplus4(String ordracptZipplus4){
    this.ordracptZipplus4 = ordracptZipplus4;
}
public String getOrdracptZipplus4(){
    return ordracptZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_LONG")
private String billtoLong;
public void setBilltoLong(String billtoLong){
    this.billtoLong = billtoLong;
}
public String getBilltoLong(){
    return billtoLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_COUNTY")
private String shipfromCounty;
public void setShipfromCounty(String shipfromCounty){
    this.shipfromCounty = shipfromCounty;
}
public String getShipfromCounty(){
    return shipfromCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_COUNTRY_NEXUSIND_CODE")
private String ordrorgnCountryNexusindCode;
public void setOrdrorgnCountryNexusindCode(String ordrorgnCountryNexusindCode){
    this.ordrorgnCountryNexusindCode = ordrorgnCountryNexusindCode;
}
public String getOrdrorgnCountryNexusindCode(){
    return ordrorgnCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ZIPPLUS4")
private String billtoZipplus4;
public void setBilltoZipplus4(String billtoZipplus4){
    this.billtoZipplus4 = billtoZipplus4;
}
public String getBilltoZipplus4(){
    return billtoZipplus4;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ2_NAME")
private String ordracptStj2Name;
public void setOrdracptStj2Name(String ordracptStj2Name){
    this.ordracptStj2Name = ordracptStj2Name;
}
public String getOrdracptStj2Name(){
    return ordracptStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STATE_NEXUSIND_CODE")
private String shiptoStateNexusindCode;
public void setShiptoStateNexusindCode(String shiptoStateNexusindCode){
    this.shiptoStateNexusindCode = shiptoStateNexusindCode;
}
public String getShiptoStateNexusindCode(){
    return shiptoStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ENTITY_CODE")
private String ordracptEntityCode;
public void setOrdracptEntityCode(String ordracptEntityCode){
    this.ordracptEntityCode = ordracptEntityCode;
}
public String getOrdracptEntityCode(){
    return ordracptEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_CITY")
private String shipfromCity;
public void setShipfromCity(String shipfromCity){
    this.shipfromCity = shipfromCity;
}
public String getShipfromCity(){
    return shipfromCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_LAT")
private String firstuseLat;
public void setFirstuseLat(String firstuseLat){
    this.firstuseLat = firstuseLat;
}
public String getFirstuseLat(){
    return firstuseLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ2_NEXUSIND_CODE")
private String billtoStj2NexusindCode;
public void setBilltoStj2NexusindCode(String billtoStj2NexusindCode){
    this.billtoStj2NexusindCode = billtoStj2NexusindCode;
}
public String getBilltoStj2NexusindCode(){
    return billtoStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ1_NAME")
private String ordracptStj1Name;
public void setOrdracptStj1Name(String ordracptStj1Name){
    this.ordracptStj1Name = ordracptStj1Name;
}
public String getOrdracptStj1Name(){
    return ordracptStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ4_NAME")
private String ttlxfrStj4Name;
public void setTtlxfrStj4Name(String ttlxfrStj4Name){
    this.ttlxfrStj4Name = ttlxfrStj4Name;
}
public String getTtlxfrStj4Name(){
    return ttlxfrStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_LONG")
private String shipfromLong;
public void setShipfromLong(String shipfromLong){
    this.shipfromLong = shipfromLong;
}
public String getShipfromLong(){
    return shipfromLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ4_NEXUSIND_CODE")
private String shiptoStj4NexusindCode;
public void setShiptoStj4NexusindCode(String shiptoStj4NexusindCode){
    this.shiptoStj4NexusindCode = shiptoStj4NexusindCode;
}
public String getShiptoStj4NexusindCode(){
    return shiptoStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STATE_NEXUSIND_CODE")
private String ttlxfrStateNexusindCode;
public void setTtlxfrStateNexusindCode(String ttlxfrStateNexusindCode){
    this.ttlxfrStateNexusindCode = ttlxfrStateNexusindCode;
}
public String getTtlxfrStateNexusindCode(){
    return ttlxfrStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ2_NAME")
private String shipfromStj2Name;
public void setShipfromStj2Name(String shipfromStj2Name){
    this.shipfromStj2Name = shipfromStj2Name;
}
public String getShipfromStj2Name(){
    return shipfromStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ5_NEXUSIND_CODE")
private String ttlxfrStj5NexusindCode;
public void setTtlxfrStj5NexusindCode(String ttlxfrStj5NexusindCode){
    this.ttlxfrStj5NexusindCode = ttlxfrStj5NexusindCode;
}
public String getTtlxfrStj5NexusindCode(){
    return ttlxfrStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ1_NAME")
private String billtoStj1Name;
public void setBilltoStj1Name(String billtoStj1Name){
    this.billtoStj1Name = billtoStj1Name;
}
public String getBilltoStj1Name(){
    return billtoStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_GEOCODE")
private String shipfromGeocode;
public void setShipfromGeocode(String shipfromGeocode){
    this.shipfromGeocode = shipfromGeocode;
}
public String getShipfromGeocode(){
    return shipfromGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ1_NAME")
private String firstuseStj1Name;
public void setFirstuseStj1Name(String firstuseStj1Name){
    this.firstuseStj1Name = firstuseStj1Name;
}
public String getFirstuseStj1Name(){
    return firstuseStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ADDRESS_LINE_1")
private String ordracptAddressLine1;
public void setOrdracptAddressLine1(String ordracptAddressLine1){
    this.ordracptAddressLine1 = ordracptAddressLine1;
}
public String getOrdracptAddressLine1(){
    return ordracptAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_ADDRESS_LINE_2")
private String ordracptAddressLine2;
public void setOrdracptAddressLine2(String ordracptAddressLine2){
    this.ordracptAddressLine2 = ordracptAddressLine2;
}
public String getOrdracptAddressLine2(){
    return ordracptAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_LONG")
private String ttlxfrLong;
public void setTtlxfrLong(String ttlxfrLong){
    this.ttlxfrLong = ttlxfrLong;
}
public String getTtlxfrLong(){
    return ttlxfrLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ3_NEXUSIND_CODE")
private String ordrorgnStj3NexusindCode;
public void setOrdrorgnStj3NexusindCode(String ordrorgnStj3NexusindCode){
    this.ordrorgnStj3NexusindCode = ordrorgnStj3NexusindCode;
}
public String getOrdrorgnStj3NexusindCode(){
    return ordrorgnStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ3_NEXUSIND_CODE")
private String shipfromStj3NexusindCode;
public void setShipfromStj3NexusindCode(String shipfromStj3NexusindCode){
    this.shipfromStj3NexusindCode = shipfromStj3NexusindCode;
}
public String getShipfromStj3NexusindCode(){
    return shipfromStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_LONG")
private String firstuseLong;
public void setFirstuseLong(String firstuseLong){
    this.firstuseLong = firstuseLong;
}
public String getFirstuseLong(){
    return firstuseLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_LOCATION")
private String shiptoLocation;
public void setShiptoLocation(String shiptoLocation){
    this.shiptoLocation = shiptoLocation;
}
public String getShiptoLocation(){
    return shiptoLocation;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ENTITY_CODE")
private String firstuseEntityCode;
public void setFirstuseEntityCode(String firstuseEntityCode){
    this.firstuseEntityCode = firstuseEntityCode;
}
public String getFirstuseEntityCode(){
    return firstuseEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ1_NEXUSIND_CODE")
private String firstuseStj1NexusindCode;
public void setFirstuseStj1NexusindCode(String firstuseStj1NexusindCode){
    this.firstuseStj1NexusindCode = firstuseStj1NexusindCode;
}
public String getFirstuseStj1NexusindCode(){
    return firstuseStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_LAT")
private String shiptoLat;
public void setShiptoLat(String shiptoLat){
    this.shiptoLat = shiptoLat;
}
public String getShiptoLat(){
    return shiptoLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ3_NEXUSIND_CODE")
private String firstuseStj3NexusindCode;
public void setFirstuseStj3NexusindCode(String firstuseStj3NexusindCode){
    this.firstuseStj3NexusindCode = firstuseStj3NexusindCode;
}
public String getFirstuseStj3NexusindCode(){
    return firstuseStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_JURISDICTION_ID")
private Long billtoJurisdictionId;
public void setBilltoJurisdictionId(Long billtoJurisdictionId){
    this.billtoJurisdictionId = billtoJurisdictionId;
}
public Long getBilltoJurisdictionId(){
    return billtoJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ5_NAME")
private String billtoStj5Name;
public void setBilltoStj5Name(String billtoStj5Name){
    this.billtoStj5Name = billtoStj5Name;
}
public String getBilltoStj5Name(){
    return billtoStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ5_NEXUSIND_CODE")
private String firstuseStj5NexusindCode;
public void setFirstuseStj5NexusindCode(String firstuseStj5NexusindCode){
    this.firstuseStj5NexusindCode = firstuseStj5NexusindCode;
}
public String getFirstuseStj5NexusindCode(){
    return firstuseStj5NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ENTITY_CODE")
private String shipfromEntityCode;
public void setShipfromEntityCode(String shipfromEntityCode){
    this.shipfromEntityCode = shipfromEntityCode;
}
public String getShipfromEntityCode(){
    return shipfromEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STATE_NEXUSIND_CODE")
private String shipfromStateNexusindCode;
public void setShipfromStateNexusindCode(String shipfromStateNexusindCode){
    this.shipfromStateNexusindCode = shipfromStateNexusindCode;
}
public String getShipfromStateNexusindCode(){
    return shipfromStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_JURISDICTION_ID")
private Long shiptoJurisdictionId;
public void setShiptoJurisdictionId(Long shiptoJurisdictionId){
    this.shiptoJurisdictionId = shiptoJurisdictionId;
}
public Long getShiptoJurisdictionId(){
    return shiptoJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_JURISDICTION_ID")
private Long ordrorgnJurisdictionId;
public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId){
    this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
}
public Long getOrdrorgnJurisdictionId(){
    return ordrorgnJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ2_NAME")
private String firstuseStj2Name;
public void setFirstuseStj2Name(String firstuseStj2Name){
    this.firstuseStj2Name = firstuseStj2Name;
}
public String getFirstuseStj2Name(){
    return firstuseStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ1_NEXUSIND_CODE")
private String ttlxfrStj1NexusindCode;
public void setTtlxfrStj1NexusindCode(String ttlxfrStj1NexusindCode){
    this.ttlxfrStj1NexusindCode = ttlxfrStj1NexusindCode;
}
public String getTtlxfrStj1NexusindCode(){
    return ttlxfrStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_LAT")
private String shipfromLat;
public void setShipfromLat(String shipfromLat){
    this.shipfromLat = shipfromLat;
}
public String getShipfromLat(){
    return shipfromLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ENTITY_CODE")
private String ordrorgnEntityCode;
public void setOrdrorgnEntityCode(String ordrorgnEntityCode){
    this.ordrorgnEntityCode = ordrorgnEntityCode;
}
public String getOrdrorgnEntityCode(){
    return ordrorgnEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_LONG")
private String shiptoLong;
public void setShiptoLong(String shiptoLong){
    this.shiptoLong = shiptoLong;
}
public String getShiptoLong(){
    return shiptoLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_COUNTRY_NEXUSIND_CODE")
private String billtoCountryNexusindCode;
public void setBilltoCountryNexusindCode(String billtoCountryNexusindCode){
    this.billtoCountryNexusindCode = billtoCountryNexusindCode;
}
public String getBilltoCountryNexusindCode(){
    return billtoCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_CITY_NEXUSIND_CODE")
private String shiptoCityNexusindCode;
public void setShiptoCityNexusindCode(String shiptoCityNexusindCode){
    this.shiptoCityNexusindCode = shiptoCityNexusindCode;
}
public String getShiptoCityNexusindCode(){
    return shiptoCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_JURISDICTION_ID")
private Long firstuseJurisdictionId;
public void setFirstuseJurisdictionId(Long firstuseJurisdictionId){
    this.firstuseJurisdictionId = firstuseJurisdictionId;
}
public Long getFirstuseJurisdictionId(){
    return firstuseJurisdictionId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_STJ4_NEXUSIND_CODE")
private String ordracptStj4NexusindCode;
public void setOrdracptStj4NexusindCode(String ordracptStj4NexusindCode){
    this.ordracptStj4NexusindCode = ordracptStj4NexusindCode;
}
public String getOrdracptStj4NexusindCode(){
    return ordracptStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_ZIP")
private String billtoZip;
public void setBilltoZip(String billtoZip){
    this.billtoZip = billtoZip;
}
public String getBilltoZip(){
    return billtoZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ5_NAME")
private String ttlxfrStj5Name;
public void setTtlxfrStj5Name(String ttlxfrStj5Name){
    this.ttlxfrStj5Name = ttlxfrStj5Name;
}
public String getTtlxfrStj5Name(){
    return ttlxfrStj5Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ3_NEXUSIND_CODE")
private String shiptoStj3NexusindCode;
public void setShiptoStj3NexusindCode(String shiptoStj3NexusindCode){
    this.shiptoStj3NexusindCode = shiptoStj3NexusindCode;
}
public String getShiptoStj3NexusindCode(){
    return shiptoStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_COUNTRY_CODE")
private String ttlxfrCountryCode;
public void setTtlxfrCountryCode(String ttlxfrCountryCode){
    this.ttlxfrCountryCode = ttlxfrCountryCode;
}
public String getTtlxfrCountryCode(){
    return ttlxfrCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STATE_CODE")
private String ttlxfrStateCode;
public void setTtlxfrStateCode(String ttlxfrStateCode){
    this.ttlxfrStateCode = ttlxfrStateCode;
}
public String getTtlxfrStateCode(){
    return ttlxfrStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ4_NAME")
private String firstuseStj4Name;
public void setFirstuseStj4Name(String firstuseStj4Name){
    this.firstuseStj4Name = firstuseStj4Name;
}
public String getFirstuseStj4Name(){
    return firstuseStj4Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_COUNTRY_CODE")
private String shiptoCountryCode;
public void setShiptoCountryCode(String shiptoCountryCode){
    this.shiptoCountryCode = shiptoCountryCode;
}
public String getShiptoCountryCode(){
    return shiptoCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_COUNTRY_CODE")
private String ordracptCountryCode;
public void setOrdracptCountryCode(String ordracptCountryCode){
    this.ordracptCountryCode = ordracptCountryCode;
}
public String getOrdracptCountryCode(){
    return ordracptCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ADDRESS_LINE_2")
private String firstuseAddressLine2;
public void setFirstuseAddressLine2(String firstuseAddressLine2){
    this.firstuseAddressLine2 = firstuseAddressLine2;
}
public String getFirstuseAddressLine2(){
    return firstuseAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ADDRESS_LINE_1")
private String firstuseAddressLine1;
public void setFirstuseAddressLine1(String firstuseAddressLine1){
    this.firstuseAddressLine1 = firstuseAddressLine1;
}
public String getFirstuseAddressLine1(){
    return firstuseAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ZIP")
private String shipfromZip;
public void setShipfromZip(String shipfromZip){
    this.shipfromZip = shipfromZip;
}
public String getShipfromZip(){
    return shipfromZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ2_NEXUSIND_CODE")
private String ttlxfrStj2NexusindCode;
public void setTtlxfrStj2NexusindCode(String ttlxfrStj2NexusindCode){
    this.ttlxfrStj2NexusindCode = ttlxfrStj2NexusindCode;
}
public String getTtlxfrStj2NexusindCode(){
    return ttlxfrStj2NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_COUNTRY_NEXUSIND_CODE")
private String ordracptCountryNexusindCode;
public void setOrdracptCountryNexusindCode(String ordracptCountryNexusindCode){
    this.ordracptCountryNexusindCode = ordracptCountryNexusindCode;
}
public String getOrdracptCountryNexusindCode(){
    return ordracptCountryNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_ENTITY_ID")
private Long shipfromEntityId;
public void setShipfromEntityId(Long shipfromEntityId){
    this.shipfromEntityId = shipfromEntityId;
}
public Long getShipfromEntityId(){
    return shipfromEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ADDRESS_LINE_2")
private String shiptoAddressLine2;
public void setShiptoAddressLine2(String shiptoAddressLine2){
    this.shiptoAddressLine2 = shiptoAddressLine2;
}
public String getShiptoAddressLine2(){
    return shiptoAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_ADDRESS_LINE_1")
private String shiptoAddressLine1;
public void setShiptoAddressLine1(String shiptoAddressLine1){
    this.shiptoAddressLine1 = shiptoAddressLine1;
}
public String getShiptoAddressLine1(){
    return shiptoAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_LOCN_MATRIX_ID")
private Long shipfromLocnMatrixId;
public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId){
    this.shipfromLocnMatrixId = shipfromLocnMatrixId;
}
public Long getShipfromLocnMatrixId(){
    return shipfromLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ1_NAME")
private String ordrorgnStj1Name;
public void setOrdrorgnStj1Name(String ordrorgnStj1Name){
    this.ordrorgnStj1Name = ordrorgnStj1Name;
}
public String getOrdrorgnStj1Name(){
    return ordrorgnStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_CITY_NEXUSIND_CODE")
private String ordrorgnCityNexusindCode;
public void setOrdrorgnCityNexusindCode(String ordrorgnCityNexusindCode){
    this.ordrorgnCityNexusindCode = ordrorgnCityNexusindCode;
}
public String getOrdrorgnCityNexusindCode(){
    return ordrorgnCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_COUNTY")
private String ordrorgnCounty;
public void setOrdrorgnCounty(String ordrorgnCounty){
    this.ordrorgnCounty = ordrorgnCounty;
}
public String getOrdrorgnCounty(){
    return ordrorgnCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_CITY")
private String firstuseCity;
public void setFirstuseCity(String firstuseCity){
    this.firstuseCity = firstuseCity;
}
public String getFirstuseCity(){
    return firstuseCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ADDRESS_LINE_2")
private String ttlxfrAddressLine2;
public void setTtlxfrAddressLine2(String ttlxfrAddressLine2){
    this.ttlxfrAddressLine2 = ttlxfrAddressLine2;
}
public String getTtlxfrAddressLine2(){
    return ttlxfrAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ADDRESS_LINE_1")
private String ttlxfrAddressLine1;
public void setTtlxfrAddressLine1(String ttlxfrAddressLine1){
    this.ttlxfrAddressLine1 = ttlxfrAddressLine1;
}
public String getTtlxfrAddressLine1(){
    return ttlxfrAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_CITY")
private String ttlxfrCity;
public void setTtlxfrCity(String ttlxfrCity){
    this.ttlxfrCity = ttlxfrCity;
}
public String getTtlxfrCity(){
    return ttlxfrCity;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_COUNTY")
private String firstuseCounty;
public void setFirstuseCounty(String firstuseCounty){
    this.firstuseCounty = firstuseCounty;
}
public String getFirstuseCounty(){
    return firstuseCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_COUNTY")
private String shiptoCounty;
public void setShiptoCounty(String shiptoCounty){
    this.shiptoCounty = shiptoCounty;
}
public String getShiptoCounty(){
    return shiptoCounty;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ3_NEXUSIND_CODE")
private String billtoStj3NexusindCode;
public void setBilltoStj3NexusindCode(String billtoStj3NexusindCode){
    this.billtoStj3NexusindCode = billtoStj3NexusindCode;
}
public String getBilltoStj3NexusindCode(){
    return billtoStj3NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STATE_NEXUSIND_CODE")
private String ordrorgnStateNexusindCode;
public void setOrdrorgnStateNexusindCode(String ordrorgnStateNexusindCode){
    this.ordrorgnStateNexusindCode = ordrorgnStateNexusindCode;
}
public String getOrdrorgnStateNexusindCode(){
    return ordrorgnStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_ZIP")
private String firstuseZip;
public void setFirstuseZip(String firstuseZip){
    this.firstuseZip = firstuseZip;
}
public String getFirstuseZip(){
    return firstuseZip;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ4_NEXUSIND_CODE")
private String firstuseStj4NexusindCode;
public void setFirstuseStj4NexusindCode(String firstuseStj4NexusindCode){
    this.firstuseStj4NexusindCode = firstuseStj4NexusindCode;
}
public String getFirstuseStj4NexusindCode(){
    return firstuseStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRACPT_LOCN_MATRIX_ID")
private Long ordracptLocnMatrixId;
public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId){
    this.ordracptLocnMatrixId = ordracptLocnMatrixId;
}
public Long getOrdracptLocnMatrixId(){
    return ordracptLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_COUNTRY_CODE")
private String ordrorgnCountryCode;
public void setOrdrorgnCountryCode(String ordrorgnCountryCode){
    this.ordrorgnCountryCode = ordrorgnCountryCode;
}
public String getOrdrorgnCountryCode(){
    return ordrorgnCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ3_NAME")
private String shipfromStj3Name;
public void setShipfromStj3Name(String shipfromStj3Name){
    this.shipfromStj3Name = shipfromStj3Name;
}
public String getShipfromStj3Name(){
    return shipfromStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STATE_CODE")
private String shiptoStateCode;
public void setShiptoStateCode(String shiptoStateCode){
    this.shiptoStateCode = shiptoStateCode;
}
public String getShiptoStateCode(){
    return shiptoStateCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_STJ1_NAME")
private String shiptoStj1Name;
public void setShiptoStj1Name(String shiptoStj1Name){
    this.shiptoStj1Name = shiptoStj1Name;
}
public String getShiptoStj1Name(){
    return shiptoStj1Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_LONG")
private String ordrorgnLong;
public void setOrdrorgnLong(String ordrorgnLong){
    this.ordrorgnLong = ordrorgnLong;
}
public String getOrdrorgnLong(){
    return ordrorgnLong;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_LOCN_MATRIX_ID")
private Long ordrorgnLocnMatrixId;
public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId){
    this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
}
public Long getOrdrorgnLocnMatrixId(){
    return ordrorgnLocnMatrixId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ADDRESS_LINE_1")
private String ordrorgnAddressLine1;
public void setOrdrorgnAddressLine1(String ordrorgnAddressLine1){
    this.ordrorgnAddressLine1 = ordrorgnAddressLine1;
}
public String getOrdrorgnAddressLine1(){
    return ordrorgnAddressLine1;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ1_NEXUSIND_CODE")
private String billtoStj1NexusindCode;
public void setBilltoStj1NexusindCode(String billtoStj1NexusindCode){
    this.billtoStj1NexusindCode = billtoStj1NexusindCode;
}
public String getBilltoStj1NexusindCode(){
    return billtoStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPTO_GEOCODE")
private String shiptoGeocode;
public void setShiptoGeocode(String shiptoGeocode){
    this.shiptoGeocode = shiptoGeocode;
}
public String getShiptoGeocode(){
    return shiptoGeocode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_ADDRESS_LINE_2")
private String ordrorgnAddressLine2;
public void setOrdrorgnAddressLine2(String ordrorgnAddressLine2){
    this.ordrorgnAddressLine2 = ordrorgnAddressLine2;
}
public String getOrdrorgnAddressLine2(){
    return ordrorgnAddressLine2;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_LAT")
private String billtoLat;
public void setBilltoLat(String billtoLat){
    this.billtoLat = billtoLat;
}
public String getBilltoLat(){
    return billtoLat;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_STJ2_NAME")
private String ttlxfrStj2Name;
public void setTtlxfrStj2Name(String ttlxfrStj2Name){
    this.ttlxfrStj2Name = ttlxfrStj2Name;
}
public String getTtlxfrStj2Name(){
    return ttlxfrStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_STJ1_NEXUSIND_CODE")
private String shipfromStj1NexusindCode;
public void setShipfromStj1NexusindCode(String shipfromStj1NexusindCode){
    this.shipfromStj1NexusindCode = shipfromStj1NexusindCode;
}
public String getShipfromStj1NexusindCode(){
    return shipfromStj1NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "SHIPFROM_COUNTRY_CODE")
private String shipfromCountryCode;
public void setShipfromCountryCode(String shipfromCountryCode){
    this.shipfromCountryCode = shipfromCountryCode;
}
public String getShipfromCountryCode(){
    return shipfromCountryCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ2_NAME")
private String billtoStj2Name;
public void setBilltoStj2Name(String billtoStj2Name){
    this.billtoStj2Name = billtoStj2Name;
}
public String getBilltoStj2Name(){
    return billtoStj2Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_CITY_NEXUSIND_CODE")
private String firstuseCityNexusindCode;
public void setFirstuseCityNexusindCode(String firstuseCityNexusindCode){
    this.firstuseCityNexusindCode = firstuseCityNexusindCode;
}
public String getFirstuseCityNexusindCode(){
    return firstuseCityNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ENTITY_CODE")
private String ttlxfrEntityCode;
public void setTtlxfrEntityCode(String ttlxfrEntityCode){
    this.ttlxfrEntityCode = ttlxfrEntityCode;
}
public String getTtlxfrEntityCode(){
    return ttlxfrEntityCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "FIRSTUSE_STJ3_NAME")
private String firstuseStj3Name;
public void setFirstuseStj3Name(String firstuseStj3Name){
    this.firstuseStj3Name = firstuseStj3Name;
}
public String getFirstuseStj3Name(){
    return firstuseStj3Name;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STATE_NEXUSIND_CODE")
private String billtoStateNexusindCode;
public void setBilltoStateNexusindCode(String billtoStateNexusindCode){
    this.billtoStateNexusindCode = billtoStateNexusindCode;
}
public String getBilltoStateNexusindCode(){
    return billtoStateNexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "BILLTO_STJ4_NEXUSIND_CODE")
private String billtoStj4NexusindCode;
public void setBilltoStj4NexusindCode(String billtoStj4NexusindCode){
    this.billtoStj4NexusindCode = billtoStj4NexusindCode;
}
public String getBilltoStj4NexusindCode(){
    return billtoStj4NexusindCode;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "TTLXFR_ENTITY_ID")
private Long ttlxfrEntityId;
public void setTtlxfrEntityId(Long ttlxfrEntityId){
    this.ttlxfrEntityId = ttlxfrEntityId;
}
public Long getTtlxfrEntityId(){
    return ttlxfrEntityId;
}

@Column(table= "TB_PURCHSCENARIO_LOCN", name = "ORDRORGN_STJ4_NEXUSIND_CODE")
private String ordrorgnStj4NexusindCode;
public void setOrdrorgnStj4NexusindCode(String ordrorgnStj4NexusindCode){
    this.ordrorgnStj4NexusindCode = ordrorgnStj4NexusindCode;
}
public String getOrdrorgnStj4NexusindCode(){
    return ordrorgnStj4NexusindCode;
}


@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_40")
private String userText40;
public void setUserText40(String userText40){
    this.userText40 = userText40;
}
public String getUserText40(){
    return userText40;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_41")
private String userText41;
public void setUserText41(String userText41){
    this.userText41 = userText41;
}
public String getUserText41(){
    return userText41;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_44")
private String userText44;
public void setUserText44(String userText44){
    this.userText44 = userText44;
}
public String getUserText44(){
    return userText44;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_45")
private String userText45;
public void setUserText45(String userText45){
    this.userText45 = userText45;
}
public String getUserText45(){
    return userText45;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_42")
private String userText42;
public void setUserText42(String userText42){
    this.userText42 = userText42;
}
public String getUserText42(){
    return userText42;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_43")
private String userText43;
public void setUserText43(String userText43){
    this.userText43 = userText43;
}
public String getUserText43(){
    return userText43;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_38")
private String userText38;
public void setUserText38(String userText38){
    this.userText38 = userText38;
}
public String getUserText38(){
    return userText38;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_01")
private String userNumber01;
public void setUserNumber01(String userNumber01){
    this.userNumber01 = userNumber01;
}
public String getUserNumber01(){
    return userNumber01;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_37")
private String userText37;
public void setUserText37(String userText37){
    this.userText37 = userText37;
}
public String getUserText37(){
    return userText37;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_36")
private String userText36;
public void setUserText36(String userText36){
    this.userText36 = userText36;
}
public String getUserText36(){
    return userText36;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_35")
private String userText35;
public void setUserText35(String userText35){
    this.userText35 = userText35;
}
public String getUserText35(){
    return userText35;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_04")
private String userNumber04;
public void setUserNumber04(String userNumber04){
    this.userNumber04 = userNumber04;
}
public String getUserNumber04(){
    return userNumber04;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_05")
private String userNumber05;
public void setUserNumber05(String userNumber05){
    this.userNumber05 = userNumber05;
}
public String getUserNumber05(){
    return userNumber05;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_02")
private String userNumber02;
public void setUserNumber02(String userNumber02){
    this.userNumber02 = userNumber02;
}
public String getUserNumber02(){
    return userNumber02;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_03")
private String userNumber03;
public void setUserNumber03(String userNumber03){
    this.userNumber03 = userNumber03;
}
public String getUserNumber03(){
    return userNumber03;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_39")
private String userText39;
public void setUserText39(String userText39){
    this.userText39 = userText39;
}
public String getUserText39(){
    return userText39;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_08")
private String userNumber08;
public void setUserNumber08(String userNumber08){
    this.userNumber08 = userNumber08;
}
public String getUserNumber08(){
    return userNumber08;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_09")
private String userNumber09;
public void setUserNumber09(String userNumber09){
    this.userNumber09 = userNumber09;
}
public String getUserNumber09(){
    return userNumber09;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_06")
private String userNumber06;
public void setUserNumber06(String userNumber06){
    this.userNumber06 = userNumber06;
}
public String getUserNumber06(){
    return userNumber06;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_07")
private String userNumber07;
public void setUserNumber07(String userNumber07){
    this.userNumber07 = userNumber07;
}
public String getUserNumber07(){
    return userNumber07;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_50")
private String userText50;
public void setUserText50(String userText50){
    this.userText50 = userText50;
}
public String getUserText50(){
    return userText50;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_51")
private String userText51;
public void setUserText51(String userText51){
    this.userText51 = userText51;
}
public String getUserText51(){
    return userText51;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_52")
private String userText52;
public void setUserText52(String userText52){
    this.userText52 = userText52;
}
public String getUserText52(){
    return userText52;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_53")
private String userText53;
public void setUserText53(String userText53){
    this.userText53 = userText53;
}
public String getUserText53(){
    return userText53;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_54")
private String userText54;
public void setUserText54(String userText54){
    this.userText54 = userText54;
}
public String getUserText54(){
    return userText54;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_55")
private String userText55;
public void setUserText55(String userText55){
    this.userText55 = userText55;
}
public String getUserText55(){
    return userText55;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_56")
private String userText56;
public void setUserText56(String userText56){
    this.userText56 = userText56;
}
public String getUserText56(){
    return userText56;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_47")
private String userText47;
public void setUserText47(String userText47){
    this.userText47 = userText47;
}
public String getUserText47(){
    return userText47;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_10")
private String userNumber10;
public void setUserNumber10(String userNumber10){
    this.userNumber10 = userNumber10;
}
public String getUserNumber10(){
    return userNumber10;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_46")
private String userText46;
public void setUserText46(String userText46){
    this.userText46 = userText46;
}
public String getUserText46(){
    return userText46;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_11")
private String userNumber11;
public void setUserNumber11(String userNumber11){
    this.userNumber11 = userNumber11;
}
public String getUserNumber11(){
    return userNumber11;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_49")
private String userText49;
public void setUserText49(String userText49){
    this.userText49 = userText49;
}
public String getUserText49(){
    return userText49;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_12")
private String userNumber12;
public void setUserNumber12(String userNumber12){
    this.userNumber12 = userNumber12;
}
public String getUserNumber12(){
    return userNumber12;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_48")
private String userText48;
public void setUserText48(String userText48){
    this.userText48 = userText48;
}
public String getUserText48(){
    return userText48;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_13")
private String userNumber13;
public void setUserNumber13(String userNumber13){
    this.userNumber13 = userNumber13;
}
public String getUserNumber13(){
    return userNumber13;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_14")
private String userNumber14;
public void setUserNumber14(String userNumber14){
    this.userNumber14 = userNumber14;
}
public String getUserNumber14(){
    return userNumber14;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_15")
private String userNumber15;
public void setUserNumber15(String userNumber15){
    this.userNumber15 = userNumber15;
}
public String getUserNumber15(){
    return userNumber15;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_16")
private String userNumber16;
public void setUserNumber16(String userNumber16){
    this.userNumber16 = userNumber16;
}
public String getUserNumber16(){
    return userNumber16;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_17")
private String userNumber17;
public void setUserNumber17(String userNumber17){
    this.userNumber17 = userNumber17;
}
public String getUserNumber17(){
    return userNumber17;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_18")
private String userNumber18;
public void setUserNumber18(String userNumber18){
    this.userNumber18 = userNumber18;
}
public String getUserNumber18(){
    return userNumber18;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_19")
private String userNumber19;
public void setUserNumber19(String userNumber19){
    this.userNumber19 = userNumber19;
}
public String getUserNumber19(){
    return userNumber19;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_22")
private String userText22;
public void setUserText22(String userText22){
    this.userText22 = userText22;
}
public String getUserText22(){
    return userText22;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_23")
private String userText23;
public void setUserText23(String userText23){
    this.userText23 = userText23;
}
public String getUserText23(){
    return userText23;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_20")
private String userText20;
public void setUserText20(String userText20){
    this.userText20 = userText20;
}
public String getUserText20(){
    return userText20;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_21")
private String userText21;
public void setUserText21(String userText21){
    this.userText21 = userText21;
}
public String getUserText21(){
    return userText21;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_19")
private String userText19;
public void setUserText19(String userText19){
    this.userText19 = userText19;
}
public String getUserText19(){
    return userText19;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_18")
private String userText18;
public void setUserText18(String userText18){
    this.userText18 = userText18;
}
public String getUserText18(){
    return userText18;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_17")
private String userText17;
public void setUserText17(String userText17){
    this.userText17 = userText17;
}
public String getUserText17(){
    return userText17;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_16")
private String userText16;
public void setUserText16(String userText16){
    this.userText16 = userText16;
}
public String getUserText16(){
    return userText16;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_15")
private String userText15;
public void setUserText15(String userText15){
    this.userText15 = userText15;
}
public String getUserText15(){
    return userText15;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_14")
private String userText14;
public void setUserText14(String userText14){
    this.userText14 = userText14;
}
public String getUserText14(){
    return userText14;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_13")
private String userText13;
public void setUserText13(String userText13){
    this.userText13 = userText13;
}
public String getUserText13(){
    return userText13;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_100")
private String userText100;
public void setUserText100(String userText100){
    this.userText100 = userText100;
}
public String getUserText100(){
    return userText100;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_31")
private String userText31;
public void setUserText31(String userText31){
    this.userText31 = userText31;
}
public String getUserText31(){
    return userText31;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_32")
private String userText32;
public void setUserText32(String userText32){
    this.userText32 = userText32;
}
public String getUserText32(){
    return userText32;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_33")
private String userText33;
public void setUserText33(String userText33){
    this.userText33 = userText33;
}
public String getUserText33(){
    return userText33;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_34")
private String userText34;
public void setUserText34(String userText34){
    this.userText34 = userText34;
}
public String getUserText34(){
    return userText34;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_30")
private String userText30;
public void setUserText30(String userText30){
    this.userText30 = userText30;
}
public String getUserText30(){
    return userText30;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_29")
private String userText29;
public void setUserText29(String userText29){
    this.userText29 = userText29;
}
public String getUserText29(){
    return userText29;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_28")
private String userText28;
public void setUserText28(String userText28){
    this.userText28 = userText28;
}
public String getUserText28(){
    return userText28;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_25")
private String userText25;
public void setUserText25(String userText25){
    this.userText25 = userText25;
}
public String getUserText25(){
    return userText25;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_24")
private String userText24;
public void setUserText24(String userText24){
    this.userText24 = userText24;
}
public String getUserText24(){
    return userText24;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_27")
private String userText27;
public void setUserText27(String userText27){
    this.userText27 = userText27;
}
public String getUserText27(){
    return userText27;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_26")
private String userText26;
public void setUserText26(String userText26){
    this.userText26 = userText26;
}
public String getUserText26(){
    return userText26;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_25")
private String userDate25;
public void setUserDate25(String userDate25){
    this.userDate25 = userDate25;
}
public String getUserDate25(){
    return userDate25;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_85")
private String userText85;
public void setUserText85(String userText85){
    this.userText85 = userText85;
}
public String getUserText85(){
    return userText85;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_84")
private String userText84;
public void setUserText84(String userText84){
    this.userText84 = userText84;
}
public String getUserText84(){
    return userText84;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_83")
private String userText83;
public void setUserText83(String userText83){
    this.userText83 = userText83;
}
public String getUserText83(){
    return userText83;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_82")
private String userText82;
public void setUserText82(String userText82){
    this.userText82 = userText82;
}
public String getUserText82(){
    return userText82;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_89")
private String userText89;
public void setUserText89(String userText89){
    this.userText89 = userText89;
}
public String getUserText89(){
    return userText89;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_88")
private String userText88;
public void setUserText88(String userText88){
    this.userText88 = userText88;
}
public String getUserText88(){
    return userText88;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_87")
private String userText87;
public void setUserText87(String userText87){
    this.userText87 = userText87;
}
public String getUserText87(){
    return userText87;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_86")
private String userText86;
public void setUserText86(String userText86){
    this.userText86 = userText86;
}
public String getUserText86(){
    return userText86;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_08")
private String userText08;
public void setUserText08(String userText08){
    this.userText08 = userText08;
}
public String getUserText08(){
    return userText08;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_09")
private String userText09;
public void setUserText09(String userText09){
    this.userText09 = userText09;
}
public String getUserText09(){
    return userText09;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_06")
private String userText06;
public void setUserText06(String userText06){
    this.userText06 = userText06;
}
public String getUserText06(){
    return userText06;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_07")
private String userText07;
public void setUserText07(String userText07){
    this.userText07 = userText07;
}
public String getUserText07(){
    return userText07;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_04")
private String userText04;
public void setUserText04(String userText04){
    this.userText04 = userText04;
}
public String getUserText04(){
    return userText04;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_81")
private String userText81;
public void setUserText81(String userText81){
    this.userText81 = userText81;
}
public String getUserText81(){
    return userText81;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_05")
private String userText05;
public void setUserText05(String userText05){
    this.userText05 = userText05;
}
public String getUserText05(){
    return userText05;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_80")
private String userText80;
public void setUserText80(String userText80){
    this.userText80 = userText80;
}
public String getUserText80(){
    return userText80;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_02")
private String userText02;
public void setUserText02(String userText02){
    this.userText02 = userText02;
}
public String getUserText02(){
    return userText02;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_03")
private String userText03;
public void setUserText03(String userText03){
    this.userText03 = userText03;
}
public String getUserText03(){
    return userText03;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_12")
private String userText12;
public void setUserText12(String userText12){
    this.userText12 = userText12;
}
public String getUserText12(){
    return userText12;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_11")
private String userText11;
public void setUserText11(String userText11){
    this.userText11 = userText11;
}
public String getUserText11(){
    return userText11;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_10")
private String userText10;
public void setUserText10(String userText10){
    this.userText10 = userText10;
}
public String getUserText10(){
    return userText10;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_79")
private String userText79;
public void setUserText79(String userText79){
    this.userText79 = userText79;
}
public String getUserText79(){
    return userText79;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_94")
private String userText94;
public void setUserText94(String userText94){
    this.userText94 = userText94;
}
public String getUserText94(){
    return userText94;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_93")
private String userText93;
public void setUserText93(String userText93){
    this.userText93 = userText93;
}
public String getUserText93(){
    return userText93;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_96")
private String userText96;
public void setUserText96(String userText96){
    this.userText96 = userText96;
}
public String getUserText96(){
    return userText96;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_95")
private String userText95;
public void setUserText95(String userText95){
    this.userText95 = userText95;
}
public String getUserText95(){
    return userText95;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_98")
private String userText98;
public void setUserText98(String userText98){
    this.userText98 = userText98;
}
public String getUserText98(){
    return userText98;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_97")
private String userText97;
public void setUserText97(String userText97){
    this.userText97 = userText97;
}
public String getUserText97(){
    return userText97;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_99")
private String userText99;
public void setUserText99(String userText99){
    this.userText99 = userText99;
}
public String getUserText99(){
    return userText99;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_90")
private String userText90;
public void setUserText90(String userText90){
    this.userText90 = userText90;
}
public String getUserText90(){
    return userText90;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_92")
private String userText92;
public void setUserText92(String userText92){
    this.userText92 = userText92;
}
public String getUserText92(){
    return userText92;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_91")
private String userText91;
public void setUserText91(String userText91){
    this.userText91 = userText91;
}
public String getUserText91(){
    return userText91;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_01")
private String userDate01;
public void setUserDate01(String userDate01){
    this.userDate01 = userDate01;
}
public String getUserDate01(){
    return userDate01;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_02")
private String userDate02;
public void setUserDate02(String userDate02){
    this.userDate02 = userDate02;
}
public String getUserDate02(){
    return userDate02;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_01")
private String userText01;
public void setUserText01(String userText01){
    this.userText01 = userText01;
}
public String getUserText01(){
    return userText01;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_67")
private String userText67;
public void setUserText67(String userText67){
    this.userText67 = userText67;
}
public String getUserText67(){
    return userText67;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_66")
private String userText66;
public void setUserText66(String userText66){
    this.userText66 = userText66;
}
public String getUserText66(){
    return userText66;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_65")
private String userText65;
public void setUserText65(String userText65){
    this.userText65 = userText65;
}
public String getUserText65(){
    return userText65;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_64")
private String userText64;
public void setUserText64(String userText64){
    this.userText64 = userText64;
}
public String getUserText64(){
    return userText64;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_63")
private String userText63;
public void setUserText63(String userText63){
    this.userText63 = userText63;
}
public String getUserText63(){
    return userText63;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_62")
private String userText62;
public void setUserText62(String userText62){
    this.userText62 = userText62;
}
public String getUserText62(){
    return userText62;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_61")
private String userText61;
public void setUserText61(String userText61){
    this.userText61 = userText61;
}
public String getUserText61(){
    return userText61;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_60")
private String userText60;
public void setUserText60(String userText60){
    this.userText60 = userText60;
}
public String getUserText60(){
    return userText60;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_04")
private String userDate04;
public void setUserDate04(String userDate04){
    this.userDate04 = userDate04;
}
public String getUserDate04(){
    return userDate04;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_03")
private String userDate03;
public void setUserDate03(String userDate03){
    this.userDate03 = userDate03;
}
public String getUserDate03(){
    return userDate03;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_06")
private String userDate06;
public void setUserDate06(String userDate06){
    this.userDate06 = userDate06;
}
public String getUserDate06(){
    return userDate06;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_05")
private String userDate05;
public void setUserDate05(String userDate05){
    this.userDate05 = userDate05;
}
public String getUserDate05(){
    return userDate05;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_08")
private String userDate08;
public void setUserDate08(String userDate08){
    this.userDate08 = userDate08;
}
public String getUserDate08(){
    return userDate08;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_07")
private String userDate07;
public void setUserDate07(String userDate07){
    this.userDate07 = userDate07;
}
public String getUserDate07(){
    return userDate07;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_09")
private String userDate09;
public void setUserDate09(String userDate09){
    this.userDate09 = userDate09;
}
public String getUserDate09(){
    return userDate09;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_10")
private String userDate10;
public void setUserDate10(String userDate10){
    this.userDate10 = userDate10;
}
public String getUserDate10(){
    return userDate10;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_11")
private String userDate11;
public void setUserDate11(String userDate11){
    this.userDate11 = userDate11;
}
public String getUserDate11(){
    return userDate11;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_12")
private String userDate12;
public void setUserDate12(String userDate12){
    this.userDate12 = userDate12;
}
public String getUserDate12(){
    return userDate12;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_13")
private String userDate13;
public void setUserDate13(String userDate13){
    this.userDate13 = userDate13;
}
public String getUserDate13(){
    return userDate13;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_25")
private String userNumber25;
public void setUserNumber25(String userNumber25){
    this.userNumber25 = userNumber25;
}
public String getUserNumber25(){
    return userNumber25;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_24")
private String userNumber24;
public void setUserNumber24(String userNumber24){
    this.userNumber24 = userNumber24;
}
public String getUserNumber24(){
    return userNumber24;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_23")
private String userNumber23;
public void setUserNumber23(String userNumber23){
    this.userNumber23 = userNumber23;
}
public String getUserNumber23(){
    return userNumber23;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_59")
private String userText59;
public void setUserText59(String userText59){
    this.userText59 = userText59;
}
public String getUserText59(){
    return userText59;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_22")
private String userNumber22;
public void setUserNumber22(String userNumber22){
    this.userNumber22 = userNumber22;
}
public String getUserNumber22(){
    return userNumber22;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_21")
private String userNumber21;
public void setUserNumber21(String userNumber21){
    this.userNumber21 = userNumber21;
}
public String getUserNumber21(){
    return userNumber21;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_57")
private String userText57;
public void setUserText57(String userText57){
    this.userText57 = userText57;
}
public String getUserText57(){
    return userText57;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_NUMBER_20")
private String userNumber20;
public void setUserNumber20(String userNumber20){
    this.userNumber20 = userNumber20;
}
public String getUserNumber20(){
    return userNumber20;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_58")
private String userText58;
public void setUserText58(String userText58){
    this.userText58 = userText58;
}
public String getUserText58(){
    return userText58;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_76")
private String userText76;
public void setUserText76(String userText76){
    this.userText76 = userText76;
}
public String getUserText76(){
    return userText76;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_75")
private String userText75;
public void setUserText75(String userText75){
    this.userText75 = userText75;
}
public String getUserText75(){
    return userText75;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_78")
private String userText78;
public void setUserText78(String userText78){
    this.userText78 = userText78;
}
public String getUserText78(){
    return userText78;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_77")
private String userText77;
public void setUserText77(String userText77){
    this.userText77 = userText77;
}
public String getUserText77(){
    return userText77;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_72")
private String userText72;
public void setUserText72(String userText72){
    this.userText72 = userText72;
}
public String getUserText72(){
    return userText72;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_71")
private String userText71;
public void setUserText71(String userText71){
    this.userText71 = userText71;
}
public String getUserText71(){
    return userText71;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_74")
private String userText74;
public void setUserText74(String userText74){
    this.userText74 = userText74;
}
public String getUserText74(){
    return userText74;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_73")
private String userText73;
public void setUserText73(String userText73){
    this.userText73 = userText73;
}
public String getUserText73(){
    return userText73;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_17")
private String userDate17;
public void setUserDate17(String userDate17){
    this.userDate17 = userDate17;
}
public String getUserDate17(){
    return userDate17;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_16")
private String userDate16;
public void setUserDate16(String userDate16){
    this.userDate16 = userDate16;
}
public String getUserDate16(){
    return userDate16;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_15")
private String userDate15;
public void setUserDate15(String userDate15){
    this.userDate15 = userDate15;
}
public String getUserDate15(){
    return userDate15;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_70")
private String userText70;
public void setUserText70(String userText70){
    this.userText70 = userText70;
}
public String getUserText70(){
    return userText70;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_14")
private String userDate14;
public void setUserDate14(String userDate14){
    this.userDate14 = userDate14;
}
public String getUserDate14(){
    return userDate14;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_19")
private String userDate19;
public void setUserDate19(String userDate19){
    this.userDate19 = userDate19;
}
public String getUserDate19(){
    return userDate19;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_18")
private String userDate18;
public void setUserDate18(String userDate18){
    this.userDate18 = userDate18;
}
public String getUserDate18(){
    return userDate18;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_20")
private String userDate20;
public void setUserDate20(String userDate20){
    this.userDate20 = userDate20;
}
public String getUserDate20(){
    return userDate20;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_23")
private String userDate23;
public void setUserDate23(String userDate23){
    this.userDate23 = userDate23;
}
public String getUserDate23(){
    return userDate23;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_24")
private String userDate24;
public void setUserDate24(String userDate24){
    this.userDate24 = userDate24;
}
public String getUserDate24(){
    return userDate24;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_21")
private String userDate21;
public void setUserDate21(String userDate21){
    this.userDate21 = userDate21;
}
public String getUserDate21(){
    return userDate21;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_DATE_22")
private String userDate22;
public void setUserDate22(String userDate22){
    this.userDate22 = userDate22;
}
public String getUserDate22(){
    return userDate22;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_68")
private String userText68;
public void setUserText68(String userText68){
    this.userText68 = userText68;
}
public String getUserText68(){
    return userText68;
}

@Column(table= "TB_PURCHSCENARIO_USER", name = "USER_TEXT_69")
private String userText69;
public void setUserText69(String userText69){
    this.userText69 = userText69;
}
public String getUserText69(){
    return userText69;
}


}
