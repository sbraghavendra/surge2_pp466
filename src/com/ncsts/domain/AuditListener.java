package com.ncsts.domain;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;

public class AuditListener {

	private Logger logger = LoggerFactory.getInstance().getLogger(AuditListener.class);	
	
	@PreUpdate
    @PrePersist
    public void setUser(Auditable instance) {
		String userCode = Auditable.currentUserCode();
		if ((userCode != null) && (userCode.trim().length() > 0)) {
			logger.debug("Setting updateUserId to " + userCode);
			instance.setUpdateUserId(userCode);
		}
		instance.setUpdateTimestamp(new Date());
	}
}
