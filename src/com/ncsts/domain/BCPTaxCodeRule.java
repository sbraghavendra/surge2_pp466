package com.ncsts.domain;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TB_BCP_TAXCODE_RULES")
public class BCPTaxCodeRule implements java.io.Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="BCP_TAXCODE_RULE_ID")	
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="bcp_taxcode_rule_sequence")
	@SequenceGenerator(name="bcp_taxcode_rule_sequence" , allocationSize = 1, sequenceName="sq_tb_bcp_taxcode_rule_id")	
    private Long bcpTaxcodeRuleId;
	
	@Column(name="BATCH_ID")
	private Long batchId;
	
	@Column(name="TAXCODE_COUNTRY_CODE")
	private String taxcodeCountryCode;
	
	@Column(name="LC_DESCRIPTION")
	private String lcDescription;
	
	@Column(name="TAXCODE_STATE_CODE")
	private String taxcodeStateCode;
	
	@Column(name="TS_GEOCODE")
	private String tsGeocode;
	
	@Column(name="TS_NAME")
	private String tsName;
	
	@Column(name="TS_LOCAL_TAXABILITY_CODE")
	private String tsLocalTaxabilityCode;
	
	@Column(name="TS_ACTIVE_FLAG")
	private String tsActiveFlag;
	
	@Column(name="TAXCODE_CODE")
	private String taxcodeCode;
	
	@Column(name="TC_DESCRIPTION")
	private String tcDescription;
	
	@Column(name="TC_COMMENTS")
	private String tcComments;
	
	@Column(name="TC_ACTIVE_FLAG")
	private String tcActiveFlag;
	
	@Column(name="TD_JUR_LEVEL")
	private String tdJurLevel;
	
	@Column(name="TD_TAXCODE_COUNTY")
	private String tdTaxcodeCounty;
	
	@Column(name="TD_TAXCODE_CITY")
	private String tdTaxcodeCity;
	
	@Column(name="TD_TAXCODE_STJ")
	private String tdTaxcodeStj;
	
	@Column(name="TD_EFFECTIVE_DATE")
	private Date tdEffectiveDate;
	
	@Column(name="TD_EXPIRATION_DATE")
	private Date tdExpirationDate;
	
	@Column(name="TD_TAXCODE_TYPE_CODE")
	private String tdTaxcodeTypeCode;
	
	@Column(name="TD_OVERRIDE_TAXTYPE_CODE")
	private String tdOverrideTaxtypeCode;
	
	@Column(name="TD_RATETYPE_CODE")
	private String tdRatetypeCode;
	
	@Column(name="TD_TAXABLE_THRESHOLD_AMT")
	private BigDecimal tdTaxableThresholdAmt;
	
	@Column(name="TD_TAXABLE_LIMITATION_AMT")
	private BigDecimal tdTaxableLimitationAmt;
	
	@Column(name="TD_TAX_LIMITATION_AMT")
	private BigDecimal tdTaxLimitationAmt;
	
	@Column(name="TD_CAP_AMT")
	private BigDecimal tdCapAmt;
	
	@Column(name="TD_BASE_CHANGE_PCT")
	private BigDecimal tdBaseChangePct;
	
	@Column(name="TD_SPECIAL_RATE")
	private BigDecimal tdSpecialRate;
	
	@Column(name="TD_ACTIVE_FLAG")
	private String tdActiveFlag;
	
	@Column(name="UPDATE_CODE")
	private String updateCode;
	
	@Column(name="TD_SORT_NO")
	private BigDecimal tdSortNo;
	
	@Column(name="TC_STATUS_IND")
	private String tcStatusInd;
	
	public String getTcStatusInd() {
		return tcStatusInd;
	}

	public void setTcStatusInd(String tcStatusInd) {
		this.tcStatusInd = tcStatusInd;
	}
	
	@Transient
	private String text;
	
	@Transient
	private Long line;
	
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.NUMERIC);
	}
	
	public BCPTaxCodeRule() {
	}

	public Long getId() {
		return bcpTaxcodeRuleId;
	}

	public void setId(Long id) {
		this.bcpTaxcodeRuleId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "bcpTaxcodeRuleId";
	}

	public Long getBcpTaxcodeRuleId() {
		return bcpTaxcodeRuleId;
	}

	public void setBcpTaxcodeRuleId(Long bcpTaxcodeRuleId) {
		this.bcpTaxcodeRuleId = bcpTaxcodeRuleId;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getTaxcodeCountryCode() {
		return taxcodeCountryCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
		this.taxcodeCountryCode = taxcodeCountryCode;
	}

	public String getLcDescription() {
		return lcDescription;
	}

	public void setLcDescription(String lcDescription) {
		this.lcDescription = lcDescription;
	}

	public String getTaxcodeStateCode() {
		return taxcodeStateCode;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
		this.taxcodeStateCode = taxcodeStateCode;
	}

	public String getTsGeocode() {
		return tsGeocode;
	}

	public void setTsGeocode(String tsGeocode) {
		this.tsGeocode = tsGeocode;
	}

	public String getTsName() {
		return tsName;
	}

	public void setTsName(String tsName) {
		this.tsName = tsName;
	}

	public String getTsLocalTaxabilityCode() {
		return tsLocalTaxabilityCode;
	}

	public void setTsLocalTaxabilityCode(String tsLocalTaxabilityCode) {
		this.tsLocalTaxabilityCode = tsLocalTaxabilityCode;
	}

	public String getTsActiveFlag() {
		return tsActiveFlag;
	}

	public void setTsActiveFlag(String tsActiveFlag) {
		this.tsActiveFlag = tsActiveFlag;
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	public String getTcDescription() {
		return tcDescription;
	}

	public void setTcDescription(String tcDescription) {
		this.tcDescription = tcDescription;
	}

	public String getTcComments() {
		return tcComments;
	}

	public void setTcComments(String tcComments) {
		this.tcComments = tcComments;
	}

	public String getTcActiveFlag() {
		return tcActiveFlag;
	}

	public void setTcActiveFlag(String tcActiveFlag) {
		this.tcActiveFlag = tcActiveFlag;
	}
	
	public String getTdJurLevel() {
		return tdJurLevel;
	}

	public void setTdJurLevel(String tdJurLevel) {
		this.tdJurLevel = tdJurLevel;
	}

	public String getTdTaxcodeCounty() {
		return tdTaxcodeCounty;
	}

	public void setTdTaxcodeCounty(String tdTaxcodeCounty) {
		this.tdTaxcodeCounty = tdTaxcodeCounty;
	}

	public String getTdTaxcodeCity() {
		return tdTaxcodeCity;
	}

	public void setTdTaxcodeCity(String tdTaxcodeCity) {
		this.tdTaxcodeCity = tdTaxcodeCity;
	}
	
	public String getTdTaxcodeStj() {
		return tdTaxcodeStj;
	}

	public void setTdTaxcodeStj(String tdTaxcodeStj) {
		this.tdTaxcodeStj = tdTaxcodeStj;
	}

	public Date getTdEffectiveDate() {
		return tdEffectiveDate;
	}

	public void setTdEffectiveDate(Date tdEffectiveDate) {
		this.tdEffectiveDate = tdEffectiveDate;
	}

	public Date getTdExpirationDate() {
		return tdExpirationDate;
	}

	public void setTdExpirationDate(Date tdExpirationDate) {
		this.tdExpirationDate = tdExpirationDate;
	}

	public String getTdTaxcodeTypeCode() {
		return tdTaxcodeTypeCode;
	}

	public void setTdTaxcodeTypeCode(String tdTaxcodeTypeCode) {
		this.tdTaxcodeTypeCode = tdTaxcodeTypeCode;
	}

	public String getTdOverrideTaxtypeCode() {
		return tdOverrideTaxtypeCode;
	}

	public void setTdOverrideTaxtypeCode(String tdOverrideTaxtypeCode) {
		this.tdOverrideTaxtypeCode = tdOverrideTaxtypeCode;
	}

	public String getTdRatetypeCode() {
		return tdRatetypeCode;
	}

	public void setTdRatetypeCode(String tdRatetypeCode) {
		this.tdRatetypeCode = tdRatetypeCode;
	}

	public BigDecimal getTdTaxableThresholdAmt() {
		return tdTaxableThresholdAmt;
	}

	public void setTdTaxableThresholdAmt(BigDecimal tdTaxableThresholdAmt) {
		this.tdTaxableThresholdAmt = tdTaxableThresholdAmt;
	}
	
	public BigDecimal getTdTaxableLimitationAmt() {
		return tdTaxableLimitationAmt;
	}

	public void setTdTaxableLimitationAmt(BigDecimal tdTaxableLimitationAmt) {
		this.tdTaxableLimitationAmt = tdTaxableLimitationAmt;
	}

	public BigDecimal getTdTaxLimitationAmt() {
		return tdTaxLimitationAmt;
	}

	public void setTdTaxLimitationAmt(BigDecimal tdTaxLimitationAmt) {
		this.tdTaxLimitationAmt = tdTaxLimitationAmt;
	}

	public BigDecimal getTdSortNo() {
		return tdSortNo;
	}

	public void setTdSortNo(BigDecimal tdSortNo) {
		this.tdSortNo = tdSortNo;
	}

	public BigDecimal getTdCapAmt() {
		return tdCapAmt;
	}

	public void setTdCapAmt(BigDecimal tdCapAmt) {
		this.tdCapAmt = tdCapAmt;
	}

	public BigDecimal getTdBaseChangePct() {
		return tdBaseChangePct;
	}

	public void setTdBaseChangePct(BigDecimal tdBaseChangePct) {
		this.tdBaseChangePct = tdBaseChangePct;
	}

	public BigDecimal getTdSpecialRate() {
		return tdSpecialRate;
	}

	public void setTdSpecialRate(BigDecimal tdSpecialRate) {
		this.tdSpecialRate = tdSpecialRate;
	}

	public String getTdActiveFlag() {
		return tdActiveFlag;
	}

	public void setTdActiveFlag(String tdActiveFlag) {
		this.tdActiveFlag = tdActiveFlag;
	}

	public String getUpdateCode() {
		return updateCode;
	}

	public void setUpdateCode(String updateCode) {
		this.updateCode = updateCode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getLine() {
		return line;
	}

	public void setLine(Long line) {
		this.line = line;
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPTaxCodeRule.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_TAXCODE_RULES (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPTaxCodeRule.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){					
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}		
	}
}
