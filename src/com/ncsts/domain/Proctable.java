package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the TB_PROCTABLE database table.
 * 
 */
@Entity
@Table(name="TB_PROCTABLE")
public class Proctable extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PROCTABLE_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="proctable_sequence")
	@SequenceGenerator(name="proctable_sequence" , allocationSize = 1, sequenceName="sq_tb_proctable_id")
	private Long proctableId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Column(name="MODULE_CODE")
	private String moduleCode;

	private String description;

	@Column(name="IN_COLUMN_TYPE_01")
	private String inColumnType01;

	@Column(name="IN_COLUMN_TYPE_02")
	private String inColumnType02;

	@Column(name="IN_COLUMN_TYPE_03")
	private String inColumnType03;

	@Column(name="IN_COLUMN_TYPE_04")
	private String inColumnType04;

	@Column(name="IN_COLUMN_TYPE_05")
	private String inColumnType05;

	@Column(name="IN_COLUMN_TYPE_06")
	private String inColumnType06;

	@Column(name="IN_COLUMN_TYPE_07")
	private String inColumnType07;

	@Column(name="IN_COLUMN_TYPE_08")
	private String inColumnType08;

	@Column(name="IN_COLUMN_TYPE_09")
	private String inColumnType09;

	@Column(name="IN_COLUMN_TYPE_10")
	private String inColumnType10;

	@Column(name="IN_COLUMN_TYPE_11")
	private String inColumnType11;

	@Column(name="IN_COLUMN_TYPE_12")
	private String inColumnType12;

	@Column(name="IN_COLUMN_TYPE_13")
	private String inColumnType13;

	@Column(name="IN_COLUMN_TYPE_14")
	private String inColumnType14;

	@Column(name="IN_COLUMN_TYPE_15")
	private String inColumnType15;

	@Column(name="IN_COLUMN_TYPE_16")
	private String inColumnType16;

	@Column(name="IN_COLUMN_TYPE_17")
	private String inColumnType17;

	@Column(name="IN_COLUMN_TYPE_18")
	private String inColumnType18;

	@Column(name="IN_COLUMN_TYPE_19")
	private String inColumnType19;

	@Column(name="IN_COLUMN_TYPE_20")
	private String inColumnType20;

	@Column(name="IN_COLUMN_NAME_01")
	private String inColumnName01;

	@Column(name="IN_COLUMN_NAME_02")
	private String inColumnName02;

	@Column(name="IN_COLUMN_NAME_03")
	private String inColumnName03;

	@Column(name="IN_COLUMN_NAME_04")
	private String inColumnName04;

	@Column(name="IN_COLUMN_NAME_05")
	private String inColumnName05;

	@Column(name="IN_COLUMN_NAME_06")
	private String inColumnName06;

	@Column(name="IN_COLUMN_NAME_07")
	private String inColumnName07;

	@Column(name="IN_COLUMN_NAME_08")
	private String inColumnName08;

	@Column(name="IN_COLUMN_NAME_09")
	private String inColumnName09;

	@Column(name="IN_COLUMN_NAME_10")
	private String inColumnName10;

	@Column(name="IN_COLUMN_NAME_11")
	private String inColumnName11;

	@Column(name="IN_COLUMN_NAME_12")
	private String inColumnName12;

	@Column(name="IN_COLUMN_NAME_13")
	private String inColumnName13;

	@Column(name="IN_COLUMN_NAME_14")
	private String inColumnName14;

	@Column(name="IN_COLUMN_NAME_15")
	private String inColumnName15;

	@Column(name="IN_COLUMN_NAME_16")
	private String inColumnName16;

	@Column(name="IN_COLUMN_NAME_17")
	private String inColumnName17;

	@Column(name="IN_COLUMN_NAME_18")
	private String inColumnName18;

	@Column(name="IN_COLUMN_NAME_19")
	private String inColumnName19;

	@Column(name="IN_COLUMN_NAME_20")
	private String inColumnName20;

	@Column(name="OUT_COLUMN_TYPE_01")
	private String outColumnType01;

	@Column(name="OUT_COLUMN_TYPE_02")
	private String outColumnType02;

	@Column(name="OUT_COLUMN_TYPE_03")
	private String outColumnType03;

	@Column(name="OUT_COLUMN_TYPE_04")
	private String outColumnType04;

	@Column(name="OUT_COLUMN_TYPE_05")
	private String outColumnType05;

	@Column(name="OUT_COLUMN_TYPE_06")
	private String outColumnType06;

	@Column(name="OUT_COLUMN_TYPE_07")
	private String outColumnType07;

	@Column(name="OUT_COLUMN_TYPE_08")
	private String outColumnType08;

	@Column(name="OUT_COLUMN_TYPE_09")
	private String outColumnType09;

	@Column(name="OUT_COLUMN_TYPE_10")
	private String outColumnType10;

	@Column(name="OUT_COLUMN_NAME_01")
	private String outColumnName01;

	@Column(name="OUT_COLUMN_NAME_02")
	private String outColumnName02;

	@Column(name="OUT_COLUMN_NAME_03")
	private String outColumnName03;

	@Column(name="OUT_COLUMN_NAME_04")
	private String outColumnName04;

	@Column(name="OUT_COLUMN_NAME_05")
	private String outColumnName05;

	@Column(name="OUT_COLUMN_NAME_06")
	private String outColumnName06;

	@Column(name="OUT_COLUMN_NAME_07")
	private String outColumnName07;

	@Column(name="OUT_COLUMN_NAME_08")
	private String outColumnName08;

	@Column(name="OUT_COLUMN_NAME_09")
	private String outColumnName09;

	@Column(name="OUT_COLUMN_NAME_10")
	private String outColumnName10;

	@Column(name="PROCTABLE_NAME")
	private String proctableName;
	
	@Column(name="IN_COLUMN_TABLE_01")
	private String inColumnTable01;

	@Column(name="IN_COLUMN_TABLE_02")
	private String inColumnTable02;

	@Column(name="IN_COLUMN_TABLE_03")
	private String inColumnTable03;

	@Column(name="IN_COLUMN_TABLE_04")
	private String inColumnTable04;

	@Column(name="IN_COLUMN_TABLE_05")
	private String inColumnTable05;

	@Column(name="IN_COLUMN_TABLE_06")
	private String inColumnTable06;

	@Column(name="IN_COLUMN_TABLE_07")
	private String inColumnTable07;

	@Column(name="IN_COLUMN_TABLE_08")
	private String inColumnTable08;

	@Column(name="IN_COLUMN_TABLE_09")
	private String inColumnTable09;

	@Column(name="IN_COLUMN_TABLE_10")
	private String inColumnTable10;

	@Column(name="IN_COLUMN_TABLE_11")
	private String inColumnTable11;

	@Column(name="IN_COLUMN_TABLE_12")
	private String inColumnTable12;

	@Column(name="IN_COLUMN_TABLE_13")
	private String inColumnTable13;

	@Column(name="IN_COLUMN_TABLE_14")
	private String inColumnTable14;

	@Column(name="IN_COLUMN_TABLE_15")
	private String inColumnTable15;

	@Column(name="IN_COLUMN_TABLE_16")
	private String inColumnTable16;

	@Column(name="IN_COLUMN_TABLE_17")
	private String inColumnTable17;

	@Column(name="IN_COLUMN_TABLE_18")
	private String inColumnTable18;

	@Column(name="IN_COLUMN_TABLE_19")
	private String inColumnTable19;

	@Column(name="IN_COLUMN_TABLE_20")
	private String inColumnTable20;

    public Proctable() {
    }
    
    // Idable interface
   	public Long getId() {
   		return getProctableId();
   	}
   	
    public void setId(Long id) {
    	setProctableId(id);
    }
      
    public String getIdPropertyName() {
  		return "proctableId";
  	}

	public Long getProctableId() {
		return this.proctableId;
	}

	public void setProctableId(Long proctableId) {
		this.proctableId = proctableId;
	}
	
	public String getModuleCode() {
		return this.moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInColumnType01() {
		return this.inColumnType01;
	}

	public void setInColumnType01(String inColumnType01) {
		this.inColumnType01 = inColumnType01;
	}

	public String getInColumnType02() {
		return this.inColumnType02;
	}

	public void setInColumnType02(String inColumnType02) {
		this.inColumnType02 = inColumnType02;
	}

	public String getInColumnType03() {
		return this.inColumnType03;
	}

	public void setInColumnType03(String inColumnType03) {
		this.inColumnType03 = inColumnType03;
	}

	public String getInColumnType04() {
		return this.inColumnType04;
	}

	public void setInColumnType04(String inColumnType04) {
		this.inColumnType04 = inColumnType04;
	}

	public String getInColumnType05() {
		return this.inColumnType05;
	}

	public void setInColumnType05(String inColumnType05) {
		this.inColumnType05 = inColumnType05;
	}

	public String getInColumnType06() {
		return this.inColumnType06;
	}

	public void setInColumnType06(String inColumnType06) {
		this.inColumnType06 = inColumnType06;
	}

	public String getInColumnType07() {
		return this.inColumnType07;
	}

	public void setInColumnType07(String inColumnType07) {
		this.inColumnType07 = inColumnType07;
	}

	public String getInColumnType08() {
		return this.inColumnType08;
	}

	public void setInColumnType08(String inColumnType08) {
		this.inColumnType08 = inColumnType08;
	}

	public String getInColumnType09() {
		return this.inColumnType09;
	}

	public void setInColumnType09(String inColumnType09) {
		this.inColumnType09 = inColumnType09;
	}

	public String getInColumnType10() {
		return this.inColumnType10;
	}

	public void setInColumnType10(String inColumnType10) {
		this.inColumnType10 = inColumnType10;
	}

	public String getInColumnType11() {
		return this.inColumnType11;
	}

	public void setInColumnType11(String inColumnType11) {
		this.inColumnType11 = inColumnType11;
	}

	public String getInColumnType12() {
		return this.inColumnType12;
	}

	public void setInColumnType12(String inColumnType12) {
		this.inColumnType12 = inColumnType12;
	}

	public String getInColumnType13() {
		return this.inColumnType13;
	}

	public void setInColumnType13(String inColumnType13) {
		this.inColumnType13 = inColumnType13;
	}

	public String getInColumnType14() {
		return this.inColumnType14;
	}

	public void setInColumnType14(String inColumnType14) {
		this.inColumnType14 = inColumnType14;
	}

	public String getInColumnType15() {
		return this.inColumnType15;
	}

	public void setInColumnType15(String inColumnType15) {
		this.inColumnType15 = inColumnType15;
	}

	public String getInColumnType16() {
		return this.inColumnType16;
	}

	public void setInColumnType16(String inColumnType16) {
		this.inColumnType16 = inColumnType16;
	}

	public String getInColumnType17() {
		return this.inColumnType17;
	}

	public void setInColumnType17(String inColumnType17) {
		this.inColumnType17 = inColumnType17;
	}

	public String getInColumnType18() {
		return this.inColumnType18;
	}

	public void setInColumnType18(String inColumnType18) {
		this.inColumnType18 = inColumnType18;
	}

	public String getInColumnType19() {
		return this.inColumnType19;
	}

	public void setInColumnType19(String inColumnType19) {
		this.inColumnType19 = inColumnType19;
	}

	public String getInColumnType20() {
		return this.inColumnType20;
	}

	public void setInColumnType20(String inColumnType20) {
		this.inColumnType20 = inColumnType20;
	}

	public String getInColumnName01() {
		return this.inColumnName01;
	}

	public void setInColumnName01(String inColumnName01) {
		this.inColumnName01 = inColumnName01;
	}

	public String getInColumnName02() {
		return this.inColumnName02;
	}

	public void setInColumnName02(String inColumnName02) {
		this.inColumnName02 = inColumnName02;
	}

	public String getInColumnName03() {
		return this.inColumnName03;
	}

	public void setInColumnName03(String inColumnName03) {
		this.inColumnName03 = inColumnName03;
	}

	public String getInColumnName04() {
		return this.inColumnName04;
	}

	public void setInColumnName04(String inColumnName04) {
		this.inColumnName04 = inColumnName04;
	}

	public String getInColumnName05() {
		return this.inColumnName05;
	}

	public void setInColumnName05(String inColumnName05) {
		this.inColumnName05 = inColumnName05;
	}

	public String getInColumnName06() {
		return this.inColumnName06;
	}

	public void setInColumnName06(String inColumnName06) {
		this.inColumnName06 = inColumnName06;
	}

	public String getInColumnName07() {
		return this.inColumnName07;
	}

	public void setInColumnName07(String inColumnName07) {
		this.inColumnName07 = inColumnName07;
	}

	public String getInColumnName08() {
		return this.inColumnName08;
	}

	public void setInColumnName08(String inColumnName08) {
		this.inColumnName08 = inColumnName08;
	}

	public String getInColumnName09() {
		return this.inColumnName09;
	}

	public void setInColumnName09(String inColumnName09) {
		this.inColumnName09 = inColumnName09;
	}

	public String getInColumnName10() {
		return this.inColumnName10;
	}

	public void setInColumnName10(String inColumnName10) {
		this.inColumnName10 = inColumnName10;
	}

	public String getInColumnName11() {
		return this.inColumnName11;
	}

	public void setInColumnName11(String inColumnName11) {
		this.inColumnName11 = inColumnName11;
	}

	public String getInColumnName12() {
		return this.inColumnName12;
	}

	public void setInColumnName12(String inColumnName12) {
		this.inColumnName12 = inColumnName12;
	}

	public String getInColumnName13() {
		return this.inColumnName13;
	}

	public void setInColumnName13(String inColumnName13) {
		this.inColumnName13 = inColumnName13;
	}

	public String getInColumnName14() {
		return this.inColumnName14;
	}

	public void setInColumnName14(String inColumnName14) {
		this.inColumnName14 = inColumnName14;
	}

	public String getInColumnName15() {
		return this.inColumnName15;
	}

	public void setInColumnName15(String inColumnName15) {
		this.inColumnName15 = inColumnName15;
	}

	public String getInColumnName16() {
		return this.inColumnName16;
	}

	public void setInColumnName16(String inColumnName16) {
		this.inColumnName16 = inColumnName16;
	}

	public String getInColumnName17() {
		return this.inColumnName17;
	}

	public void setInColumnName17(String inColumnName17) {
		this.inColumnName17 = inColumnName17;
	}

	public String getInColumnName18() {
		return this.inColumnName18;
	}

	public void setInColumnName18(String inColumnName18) {
		this.inColumnName18 = inColumnName18;
	}

	public String getInColumnName19() {
		return this.inColumnName19;
	}

	public void setInColumnName19(String inColumnName19) {
		this.inColumnName19 = inColumnName19;
	}

	public String getInColumnName20() {
		return this.inColumnName20;
	}

	public void setInColumnName20(String inColumnName20) {
		this.inColumnName20 = inColumnName20;
	}

	public String getOutColumnType01() {
		return this.outColumnType01;
	}

	public void setOutColumnType01(String outColumnType01) {
		this.outColumnType01 = outColumnType01;
	}

	public String getOutColumnType02() {
		return this.outColumnType02;
	}

	public void setOutColumnType02(String outColumnType02) {
		this.outColumnType02 = outColumnType02;
	}

	public String getOutColumnType03() {
		return this.outColumnType03;
	}

	public void setOutColumnType03(String outColumnType03) {
		this.outColumnType03 = outColumnType03;
	}

	public String getOutColumnType04() {
		return this.outColumnType04;
	}

	public void setOutColumnType04(String outColumnType04) {
		this.outColumnType04 = outColumnType04;
	}

	public String getOutColumnType05() {
		return this.outColumnType05;
	}

	public void setOutColumnType05(String outColumnType05) {
		this.outColumnType05 = outColumnType05;
	}

	public String getOutColumnType06() {
		return this.outColumnType06;
	}

	public void setOutColumnType06(String outColumnType06) {
		this.outColumnType06 = outColumnType06;
	}

	public String getOutColumnType07() {
		return this.outColumnType07;
	}

	public void setOutColumnType07(String outColumnType07) {
		this.outColumnType07 = outColumnType07;
	}

	public String getOutColumnType08() {
		return this.outColumnType08;
	}

	public void setOutColumnType08(String outColumnType08) {
		this.outColumnType08 = outColumnType08;
	}

	public String getOutColumnType09() {
		return this.outColumnType09;
	}

	public void setOutColumnType09(String outColumnType09) {
		this.outColumnType09 = outColumnType09;
	}

	public String getOutColumnType10() {
		return this.outColumnType10;
	}

	public void setOutColumnType10(String outColumnType10) {
		this.outColumnType10 = outColumnType10;
	}

	public String getOutColumnName01() {
		return this.outColumnName01;
	}

	public void setOutColumnName01(String outColumnName01) {
		this.outColumnName01 = outColumnName01;
	}

	public String getOutColumnName02() {
		return this.outColumnName02;
	}

	public void setOutColumnName02(String outColumnName02) {
		this.outColumnName02 = outColumnName02;
	}

	public String getOutColumnName03() {
		return this.outColumnName03;
	}

	public void setOutColumnName03(String outColumnName03) {
		this.outColumnName03 = outColumnName03;
	}

	public String getOutColumnName04() {
		return this.outColumnName04;
	}

	public void setOutColumnName04(String outColumnName04) {
		this.outColumnName04 = outColumnName04;
	}

	public String getOutColumnName05() {
		return this.outColumnName05;
	}

	public void setOutColumnName05(String outColumnName05) {
		this.outColumnName05 = outColumnName05;
	}

	public String getOutColumnName06() {
		return this.outColumnName06;
	}

	public void setOutColumnName06(String outColumnName06) {
		this.outColumnName06 = outColumnName06;
	}

	public String getOutColumnName07() {
		return this.outColumnName07;
	}

	public void setOutColumnName07(String outColumnName07) {
		this.outColumnName07 = outColumnName07;
	}

	public String getOutColumnName08() {
		return this.outColumnName08;
	}

	public void setOutColumnName08(String outColumnName08) {
		this.outColumnName08 = outColumnName08;
	}

	public String getOutColumnName09() {
		return this.outColumnName09;
	}

	public void setOutColumnName09(String outColumnName09) {
		this.outColumnName09 = outColumnName09;
	}

	public String getOutColumnName10() {
		return this.outColumnName10;
	}

	public void setOutColumnName10(String outColumnName10) {
		this.outColumnName10 = outColumnName10;
	}

	public String getProctableName() {
		return this.proctableName;
	}

	public void setProctableName(String proctableName) {
		this.proctableName = proctableName;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	public void setInColumnTable01(String inColumnTable01) {
	    this.inColumnTable01 = inColumnTable01;
	}

	public String getInColumnTable01() {
	    return this.inColumnTable01;
	}

	public void setInColumnTable02(String inColumnTable02) {
	    this.inColumnTable02 = inColumnTable02;
	}

	public String getInColumnTable02() {
	    return this.inColumnTable02;
	}

	public void setInColumnTable03(String inColumnTable03) {
	    this.inColumnTable03 = inColumnTable03;
	}

	public String getInColumnTable03() {
	    return this.inColumnTable03;
	}

	public void setInColumnTable04(String inColumnTable04) {
	    this.inColumnTable04 = inColumnTable04;
	}

	public String getInColumnTable04() {
	    return this.inColumnTable04;
	}

	public void setInColumnTable05(String inColumnTable05) {
	    this.inColumnTable05 = inColumnTable05;
	}

	public String getInColumnTable05() {
	    return this.inColumnTable05;
	}

	public void setInColumnTable06(String inColumnTable06) {
	    this.inColumnTable06 = inColumnTable06;
	}

	public String getInColumnTable06() {
	    return this.inColumnTable06;
	}

	public void setInColumnTable07(String inColumnTable07) {
	    this.inColumnTable07 = inColumnTable07;
	}

	public String getInColumnTable07() {
	    return this.inColumnTable07;
	}

	public void setInColumnTable08(String inColumnTable08) {
	    this.inColumnTable08 = inColumnTable08;
	}

	public String getInColumnTable08() {
	    return this.inColumnTable08;
	}

	public void setInColumnTable09(String inColumnTable09) {
	    this.inColumnTable09 = inColumnTable09;
	}

	public String getInColumnTable09() {
	    return this.inColumnTable09;
	}

	public void setInColumnTable10(String inColumnTable10) {
	    this.inColumnTable10 = inColumnTable10;
	}

	public String getInColumnTable10() {
	    return this.inColumnTable10;
	}

	public void setInColumnTable11(String inColumnTable11) {
	    this.inColumnTable11 = inColumnTable11;
	}

	public String getInColumnTable11() {
	    return this.inColumnTable11;
	}

	public void setInColumnTable12(String inColumnTable12) {
	    this.inColumnTable12 = inColumnTable12;
	}

	public String getInColumnTable12() {
	    return this.inColumnTable12;
	}

	public void setInColumnTable13(String inColumnTable13) {
	    this.inColumnTable13 = inColumnTable13;
	}

	public String getInColumnTable13() {
	    return this.inColumnTable13;
	}

	public void setInColumnTable14(String inColumnTable14) {
	    this.inColumnTable14 = inColumnTable14;
	}

	public String getInColumnTable14() {
	    return this.inColumnTable14;
	}

	public void setInColumnTable15(String inColumnTable15) {
	    this.inColumnTable15 = inColumnTable15;
	}

	public String getInColumnTable15() {
	    return this.inColumnTable15;
	}

	public void setInColumnTable16(String inColumnTable16) {
	    this.inColumnTable16 = inColumnTable16;
	}

	public String getInColumnTable16() {
	    return this.inColumnTable16;
	}

	public void setInColumnTable17(String inColumnTable17) {
	    this.inColumnTable17 = inColumnTable17;
	}

	public String getInColumnTable17() {
	    return this.inColumnTable17;
	}

	public void setInColumnTable18(String inColumnTable18) {
	    this.inColumnTable18 = inColumnTable18;
	}

	public String getInColumnTable18() {
	    return this.inColumnTable18;
	}

	public void setInColumnTable19(String inColumnTable19) {
	    this.inColumnTable19 = inColumnTable19;
	}

	public String getInColumnTable19() {
	    return this.inColumnTable19;
	}

	public void setInColumnTable20(String inColumnTable20) {
	    this.inColumnTable20 = inColumnTable20;
	}

	public String getInColumnTable20() {
	    return this.inColumnTable20;
	}
	
}