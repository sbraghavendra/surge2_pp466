package com.ncsts.domain;
/*
 * Author: Jim M. Wilson
 * Created: Aug 22, 2008
 * $Date: 2009-02-28 00:41:27 -0600 (Sat, 28 Feb 2009) $: - $Revision: 4015 $: 
 */

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="TB_BCP_JURISDICTION_TAXRATE")
public class BCPJurisdictionTaxRate implements java.io.Serializable, Idable<BCPJurisdictionTaxRatePK> {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BCPJurisdictionTaxRatePK id;

//	@Column(name = "BATCH_ID")
//	private Long batchId;
//
//	@Column(name = "GEOCODE")
//	private String geocode;
//
//	@Column(name="COUNTY")
//	private String county;
//
//	@Column(name = "ZIP")
//	private String zip;
//
//	@Column(name="CITY")
//	private String city;
//
	@Column(name="COUNTRY")
	private String country;

	@Column(name="STATE")
	private String state;

	@Column(name="REPORTING_CITY")
	private String reportingCity;

	@Column(name="REPORTING_CITY_FIPS")
	private String reportingCityFips;

	@Column(name="STJ1_NAME")
	private String stj1Name;

	@Column(name="STJ2_NAME")
	private String stj2Name;

	@Column(name="STJ3_NAME")
	private String stj3Name;

	@Column(name="STJ4_NAME")
	private String stj4Name;

	@Column(name="STJ5_NAME")
	private String stj5Name;

	@Column(name="ZIPPLUS4")
	private String zipplus4;

	@Column(name="IN_OUT")
	private String inOut;

	@Column(name="JUR_EXPIRATION_DATE")
	private Date jurExpirationDate;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="COUNTRY_NEXUSIND_CODE")
	private String countryNexusindCode;

	@Column(name="STATE_NEXUSIND_CODE")
	private String stateNexusindCode;

	@Column(name="COUNTY_NEXUSIND_CODE")
	private String countyNexusindCode;

	@Column(name="CITY_NEXUSIND_CODE")
	private String cityNexusindCode;

	@Column(name="STJ1_NEXUSIND_CODE")
	private String stj1NexusindCode;

	@Column(name="STJ2_NEXUSIND_CODE")
	private String stj2NexusindCode;

	@Column(name="STJ3_NEXUSIND_CODE")
	private String stj3NexusindCode;

	@Column(name="STJ4_NEXUSIND_CODE")
	private String stj4NexusindCode;

	@Column(name="STJ5_NEXUSIND_CODE")
	private String stj5NexusindCode;

	@Column(name="STJ1_LOCAL_CODE")
	private String stj1LocalCode;

	@Column(name="STJ2_LOCAL_CODE")
	private String stj2LocalCode;

	@Column(name="STJ3_LOCAL_CODE")
	private String stj3LocalCode;

	@Column(name="STJ4_LOCAL_CODE")
	private String stj4LocalCode;

	@Column(name="STJ5_LOCAL_CODE")
	private String stj5LocalCode;

	@Column(name="RATETYPE_CODE")
	private String ratetypeCode;

	@Column(name="RATE_EFFECTIVE_DATE")
	private Date rateEffectiveDate;

	@Column(name="RATE_EXPIRATION_DATE")
	private Date rateExpirationDate;

	@Column(name="COUNTRY_SALES_TIER1_RATE")
	private Double countrySalesTier1Rate;

	@Column(name="COUNTRY_SALES_TIER2_RATE")
	private Double countrySalesTier2Rate;

	@Column(name="COUNTRY_SALES_TIER3_RATE")
	private Double countrySalesTier3Rate;

	@Column(name="COUNTRY_SALES_TIER1_SETAMT")
	private Double countrySalesTier1Setamt;

	@Column(name="COUNTRY_SALES_TIER2_SETAMT")
	private Double countrySalesTier2Setamt;

	@Column(name="COUNTRY_SALES_TIER3_SETAMT")
	private Double countrySalesTier3Setamt;

	@Column(name="COUNTRY_SALES_TIER1_MAX_AMT")
	private Double countrySalesTier1MaxAmt;

	@Column(name="COUNTRY_SALES_TIER2_MIN_AMT")
	private Double countrySalesTier2MinAmt;

	@Column(name="COUNTRY_SALES_TIER2_MAX_AMT")
	private Double countrySalesTier2MaxAmt;

	@Column(name="COUNTRY_SALES_TIER2_ENTAM_FLAG")
	private String countrySalesTier2EntamFlag;

	@Column(name="COUNTRY_SALES_TIER3_ENTAM_FLAG")
	private String countrySalesTier3EntamFlag;

	@Column(name="COUNTRY_SALES_MAXTAX_AMT")
	private Double countrySalesMaxtaxAmt;

	@Column(name="COUNTRY_USE_TIER1_RATE")
	private Double countryUseTier1Rate;

	@Column(name="COUNTRY_USE_TIER2_RATE")
	private Double countryUseTier2Rate;

	@Column(name="COUNTRY_USE_TIER3_RATE")
	private Double countryUseTier3Rate;

	@Column(name="COUNTRY_USE_TIER1_SETAMT")
	private Double countryUseTier1Setamt;

	@Column(name="COUNTRY_USE_TIER2_SETAMT")
	private Double countryUseTier2Setamt;

	@Column(name="COUNTRY_USE_TIER3_SETAMT")
	private Double countryUseTier3Setamt;

	@Column(name="COUNTRY_USE_TIER1_MAX_AMT")
	private Double countryUseTier1MaxAmt;

	@Column(name="COUNTRY_USE_TIER2_MIN_AMT")
	private Double countryUseTier2MinAmt;

	@Column(name="COUNTRY_USE_TIER2_MAX_AMT")
	private Double countryUseTier2MaxAmt;

	@Column(name="COUNTRY_USE_TIER2_ENTAM_FLAG")
	private String countryUseTier2EntamFlag;

	@Column(name="COUNTRY_USE_TIER3_ENTAM_FLAG")
	private String countryUseTier3EntamFlag;

	@Column(name="COUNTRY_USE_MAXTAX_AMT")
	private Double countryUseMaxtaxAmt;

	@Column(name="COUNTRY_USE_SAME_SALES")
	private String countryUseSameSales;

	@Column(name="STATE_SALES_TIER1_RATE")
	private Double stateSalesTier1Rate;

	@Column(name="STATE_SALES_TIER2_RATE")
	private Double stateSalesTier2Rate;

	@Column(name="STATE_SALES_TIER3_RATE")
	private Double stateSalesTier3Rate;

	@Column(name="STATE_SALES_TIER1_SETAMT")
	private Double stateSalesTier1Setamt;

	@Column(name="STATE_SALES_TIER2_SETAMT")
	private Double stateSalesTier2Setamt;

	@Column(name="STATE_SALES_TIER3_SETAMT")
	private Double stateSalesTier3Setamt;

	@Column(name="STATE_SALES_TIER1_MAX_AMT")
	private Double stateSalesTier1MaxAmt;

	@Column(name="STATE_SALES_TIER2_MIN_AMT")
	private Double stateSalesTier2MinAmt;

	@Column(name="STATE_SALES_TIER2_MAX_AMT")
	private Double stateSalesTier2MaxAmt;

	@Column(name="STATE_SALES_TIER2_ENTAM_FLAG")
	private String stateSalesTier2EntamFlag;

	@Column(name="STATE_SALES_TIER3_ENTAM_FLAG")
	private String stateSalesTier3EntamFlag;

	@Column(name="STATE_SALES_MAXTAX_AMT")
	private Double stateSalesMaxtaxAmt;

	@Column(name="STATE_USE_TIER1_RATE")
	private Double stateUseTier1Rate;

	@Column(name="STATE_USE_TIER2_RATE")
	private Double stateUseTier2Rate;

	@Column(name="STATE_USE_TIER3_RATE")
	private Double stateUseTier3Rate;

	@Column(name="STATE_USE_TIER1_SETAMT")
	private Double stateUseTier1Setamt;

	@Column(name="STATE_USE_TIER2_SETAMT")
	private Double stateUseTier2Setamt;

	@Column(name="STATE_USE_TIER3_SETAMT")
	private Double stateUseTier3Setamt;

	@Column(name="STATE_USE_TIER1_MAX_AMT")
	private Double stateUseTier1MaxAmt;

	@Column(name="STATE_USE_TIER2_MIN_AMT")
	private Double stateUseTier2MinAmt;

	@Column(name="STATE_USE_TIER2_MAX_AMT")
	private Double stateUseTier2MaxAmt;

	@Column(name="STATE_USE_TIER2_ENTAM_FLAG")
	private String stateUseTier2EntamFlag;

	@Column(name="STATE_USE_TIER3_ENTAM_FLAG")
	private String stateUseTier3EntamFlag;

	@Column(name="STATE_USE_MAXTAX_AMT")
	private Double stateUseMaxtaxAmt;

	@Column(name="STATE_USE_SAME_SALES")
	private String stateUseSameSales;

	@Column(name="COUNTY_SALES_TIER1_RATE")
	private Double countySalesTier1Rate;

	@Column(name="COUNTY_SALES_TIER2_RATE")
	private Double countySalesTier2Rate;

	@Column(name="COUNTY_SALES_TIER3_RATE")
	private Double countySalesTier3Rate;

	@Column(name="COUNTY_SALES_TIER1_SETAMT")
	private Double countySalesTier1Setamt;

	@Column(name="COUNTY_SALES_TIER2_SETAMT")
	private Double countySalesTier2Setamt;

	@Column(name="COUNTY_SALES_TIER3_SETAMT")
	private Double countySalesTier3Setamt;

	@Column(name="COUNTY_SALES_TIER1_MAX_AMT")
	private Double countySalesTier1MaxAmt;

	@Column(name="COUNTY_SALES_TIER2_MIN_AMT")
	private Double countySalesTier2MinAmt;

	@Column(name="COUNTY_SALES_TIER2_MAX_AMT")
	private Double countySalesTier2MaxAmt;

	@Column(name="COUNTY_SALES_TIER2_ENTAM_FLAG")
	private String countySalesTier2EntamFlag;

	@Column(name="COUNTY_SALES_TIER3_ENTAM_FLAG")
	private String countySalesTier3EntamFlag;

	@Column(name="COUNTY_SALES_MAXTAX_AMT")
	private Double countySalesMaxtaxAmt;

	@Column(name="COUNTY_USE_TIER1_RATE")
	private Double countyUseTier1Rate;

	@Column(name="COUNTY_USE_TIER2_RATE")
	private Double countyUseTier2Rate;

	@Column(name="COUNTY_USE_TIER3_RATE")
	private Double countyUseTier3Rate;

	@Column(name="COUNTY_USE_TIER1_SETAMT")
	private Double countyUseTier1Setamt;

	@Column(name="COUNTY_USE_TIER2_SETAMT")
	private Double countyUseTier2Setamt;

	@Column(name="COUNTY_USE_TIER3_SETAMT")
	private Double countyUseTier3Setamt;

	@Column(name="COUNTY_USE_TIER1_MAX_AMT")
	private Double countyUseTier1MaxAmt;

	@Column(name="COUNTY_USE_TIER2_MIN_AMT")
	private Double countyUseTier2MinAmt;

	@Column(name="COUNTY_USE_TIER2_MAX_AMT")
	private Double countyUseTier2MaxAmt;

	@Column(name="COUNTY_USE_TIER2_ENTAM_FLAG")
	private String countyUseTier2EntamFlag;

	@Column(name="COUNTY_USE_TIER3_ENTAM_FLAG")
	private String countyUseTier3EntamFlag;

	@Column(name="COUNTY_USE_MAXTAX_AMT")
	private Double countyUseMaxtaxAmt;

	@Column(name="COUNTY_USE_SAME_SALES")
	private String countyUseSameSales;

	@Column(name="CITY_SALES_TIER1_RATE")
	private Double citySalesTier1Rate;

	@Column(name="CITY_SALES_TIER2_RATE")
	private Double citySalesTier2Rate;

	@Column(name="CITY_SALES_TIER3_RATE")
	private Double citySalesTier3Rate;

	@Column(name="CITY_SALES_TIER1_SETAMT")
	private Double citySalesTier1Setamt;

	@Column(name="CITY_SALES_TIER2_SETAMT")
	private Double citySalesTier2Setamt;

	@Column(name="CITY_SALES_TIER3_SETAMT")
	private Double citySalesTier3Setamt;

	@Column(name="CITY_SALES_TIER1_MAX_AMT")
	private Double citySalesTier1MaxAmt;

	@Column(name="CITY_SALES_TIER2_MIN_AMT")
	private Double citySalesTier2MinAmt;

	@Column(name="CITY_SALES_TIER2_MAX_AMT")
	private Double citySalesTier2MaxAmt;

	@Column(name="CITY_SALES_TIER2_ENTAM_FLAG")
	private String citySalesTier2EntamFlag;

	@Column(name="CITY_SALES_TIER3_ENTAM_FLAG")
	private String citySalesTier3EntamFlag;

	@Column(name="CITY_SALES_MAXTAX_AMT")
	private Double citySalesMaxtaxAmt;

	@Column(name="CITY_USE_TIER1_RATE")
	private Double cityUseTier1Rate;

	@Column(name="CITY_USE_TIER2_RATE")
	private Double cityUseTier2Rate;

	@Column(name="CITY_USE_TIER3_RATE")
	private Double cityUseTier3Rate;

	@Column(name="CITY_USE_TIER1_SETAMT")
	private Double cityUseTier1Setamt;

	@Column(name="CITY_USE_TIER2_SETAMT")
	private Double cityUseTier2Setamt;

	@Column(name="CITY_USE_TIER3_SETAMT")
	private Double cityUseTier3Setamt;

	@Column(name="CITY_USE_TIER1_MAX_AMT")
	private Double cityUseTier1MaxAmt;

	@Column(name="CITY_USE_TIER2_MIN_AMT")
	private Double cityUseTier2MinAmt;

	@Column(name="CITY_USE_TIER2_MAX_AMT")
	private Double cityUseTier2MaxAmt;

	@Column(name="CITY_USE_TIER2_ENTAM_FLAG")
	private String cityUseTier2EntamFlag;

	@Column(name="CITY_USE_TIER3_ENTAM_FLAG")
	private String cityUseTier3EntamFlag;

	@Column(name="CITY_USE_MAXTAX_AMT")
	private Double cityUseMaxtaxAmt;

	@Column(name="CITY_USE_SAME_SALES")
	private String cityUseSameSales;

	@Column(name="STJ1_SALES_RATE")
	private Double stj1SalesRate;

	@Column(name="STJ1_SALES_SETAMT")
	private Double stj1SalesSetamt;

	@Column(name="STJ2_SALES_RATE")
	private Double stj2SalesRate;

	@Column(name="STJ2_SALES_SETAMT")
	private Double stj2SalesSetamt;

	@Column(name="STJ3_SALES_RATE")
	private Double stj3SalesRate;

	@Column(name="STJ3_SALES_SETAMT")
	private Double stj3SalesSetamt;

	@Column(name="STJ4_SALES_RATE")
	private Double stj4SalesRate;

	@Column(name="STJ4_SALES_SETAMT")
	private Double stj4SalesSetamt;

	@Column(name="STJ5_SALES_RATE")
	private Double stj5SalesRate;

	@Column(name="STJ5_SALES_SETAMT")
	private Double stj5SalesSetamt;

	@Column(name="STJ1_USE_RATE")
	private Double stj1UseRate;

	@Column(name="STJ1_USE_SETAMT")
	private Double stj1UseSetamt;

	@Column(name="STJ2_USE_RATE")
	private Double stj2UseRate;

	@Column(name="STJ2_USE_SETAMT")
	private Double stj2UseSetamt;

	@Column(name="STJ3_USE_RATE")
	private Double stj3UseRate;

	@Column(name="STJ3_USE_SETAMT")
	private Double stj3UseSetamt;

	@Column(name="STJ4_USE_RATE")
	private Double stj4UseRate;

	@Column(name="STJ4_USE_SETAMT")
	private Double stj4UseSetamt;

	@Column(name="STJ5_USE_RATE")
	private Double stj5UseRate;

	@Column(name="STJ5_USE_SETAMT")
	private Double stj5UseSetamt;

	@Column(name="STJ_USE_SAME_SALES")
	private String stjUseSameSales;

	@Column(name="COMBINED_USE_RATE")
	private Double combinedUseRate;

	@Column(name="COMBINED_SALES_RATE")
	private Double combinedSalesRate;

	@Column(name="UPDATE_FLAG")
	private String updateFlag;
	
	@Transient
	// so I can have one object
	private String text;
	@Transient
	// so I can have one object
	private Long line;
	
	public BCPJurisdictionTaxRate() {
		this.id = new BCPJurisdictionTaxRatePK();
	}
	
	public String getIdPropertyName() {return "id";}

	public String getCounty() {return id.getCounty();}
	
	public void setCounty(String county) {id.setCounty(county);}
	
	public String getGeocode() {return id.getGeocode();}
	
	public void setGeocode(String s) {id.setGeocode(s);}

	public Long getBatchId() {return id.getBatchId();}
	
	public void setBatchId(Long l) {id.setBatchId(l);}

	public Long getBcpJurisdictionTaxRateId() {
		return id.getBcpJurisdictionTaxRateId();
	}
	
	public void setBcpJurisdictionTaxRateId(Long l) {
		id.setBcpJurisdictionTaxRateId(l);
	}
	
	public Date getJurEffectiveDate() {return id.getJurEffectiveDate();}
	
	public void setJurEffectiveDate(Date d) {id.setJurEffectiveDate(d);}

	public String getZip() {return id.getZip();}
	
	public void setZip(String zip) {id.setZip(zip);}
	
	public String getCity() {return id.getCity();}
	
	public void setCity(String City) {id.setCity(City);}
	
	public void setCountry(String country){
	    this.country = country;
	}

	public String getCountry(){
	    return country;
	}

	public void setState(String state){
	    this.state = state;
	}

	public String getState(){
	    return state;
	}

	public void setReportingCity(String reportingCity){
	    this.reportingCity = reportingCity;
	}

	public String getReportingCity(){
	    return reportingCity;
	}

	public void setReportingCityFips(String reportingCityFips){
	    this.reportingCityFips = reportingCityFips;
	}

	public String getReportingCityFips(){
	    return reportingCityFips;
	}

	public void setStj1Name(String stj1Name){
	    this.stj1Name = stj1Name;
	}

	public String getStj1Name(){
	    return stj1Name;
	}

	public void setStj2Name(String stj2Name){
	    this.stj2Name = stj2Name;
	}

	public String getStj2Name(){
	    return stj2Name;
	}

	public void setStj3Name(String stj3Name){
	    this.stj3Name = stj3Name;
	}

	public String getStj3Name(){
	    return stj3Name;
	}

	public void setStj4Name(String stj4Name){
	    this.stj4Name = stj4Name;
	}

	public String getStj4Name(){
	    return stj4Name;
	}

	public void setStj5Name(String stj5Name){
	    this.stj5Name = stj5Name;
	}

	public String getStj5Name(){
	    return stj5Name;
	}

	public void setZipplus4(String zipplus4){
	    this.zipplus4 = zipplus4;
	}

	public String getZipplus4(){
	    return zipplus4;
	}

	public void setInOut(String inOut){
	    this.inOut = inOut;
	}

	public String getInOut(){
	    return inOut;
	}

	public void setJurExpirationDate(Date jurExpirationDate){
	    this.jurExpirationDate = jurExpirationDate;
	}

	public Date getJurExpirationDate(){
	    return jurExpirationDate;
	}

	public void setDescription(String description){
	    this.description = description;
	}

	public String getDescription(){
	    return description;
	}

	public void setCountryNexusindCode(String countryNexusindCode){
	    this.countryNexusindCode = countryNexusindCode;
	}

	public String getCountryNexusindCode(){
	    return countryNexusindCode;
	}

	public void setStateNexusindCode(String stateNexusindCode){
	    this.stateNexusindCode = stateNexusindCode;
	}

	public String getStateNexusindCode(){
	    return stateNexusindCode;
	}

	public void setCountyNexusindCode(String countyNexusindCode){
	    this.countyNexusindCode = countyNexusindCode;
	}

	public String getCountyNexusindCode(){
	    return countyNexusindCode;
	}

	public void setCityNexusindCode(String cityNexusindCode){
	    this.cityNexusindCode = cityNexusindCode;
	}

	public String getCityNexusindCode(){
	    return cityNexusindCode;
	}

	public void setStj1NexusindCode(String stj1NexusindCode){
	    this.stj1NexusindCode = stj1NexusindCode;
	}

	public String getStj1NexusindCode(){
	    return stj1NexusindCode;
	}

	public void setStj2NexusindCode(String stj2NexusindCode){
	    this.stj2NexusindCode = stj2NexusindCode;
	}

	public String getStj2NexusindCode(){
	    return stj2NexusindCode;
	}

	public void setStj3NexusindCode(String stj3NexusindCode){
	    this.stj3NexusindCode = stj3NexusindCode;
	}

	public String getStj3NexusindCode(){
	    return stj3NexusindCode;
	}

	public void setStj4NexusindCode(String stj4NexusindCode){
	    this.stj4NexusindCode = stj4NexusindCode;
	}

	public String getStj4NexusindCode(){
	    return stj4NexusindCode;
	}

	public void setStj5NexusindCode(String stj5NexusindCode){
	    this.stj5NexusindCode = stj5NexusindCode;
	}

	public String getStj5NexusindCode(){
	    return stj5NexusindCode;
	}

	public void setStj1LocalCode(String stj1LocalCode){
	    this.stj1LocalCode = stj1LocalCode;
	}

	public String getStj1LocalCode(){
	    return stj1LocalCode;
	}

	public void setStj2LocalCode(String stj2LocalCode){
	    this.stj2LocalCode = stj2LocalCode;
	}

	public String getStj2LocalCode(){
	    return stj2LocalCode;
	}

	public void setStj3LocalCode(String stj3LocalCode){
	    this.stj3LocalCode = stj3LocalCode;
	}

	public String getStj3LocalCode(){
	    return stj3LocalCode;
	}

	public void setStj4LocalCode(String stj4LocalCode){
	    this.stj4LocalCode = stj4LocalCode;
	}

	public String getStj4LocalCode(){
	    return stj4LocalCode;
	}

	public void setStj5LocalCode(String stj5LocalCode){
	    this.stj5LocalCode = stj5LocalCode;
	}

	public String getStj5LocalCode(){
	    return stj5LocalCode;
	}

	public void setRatetypeCode(String ratetypeCode){
	    this.ratetypeCode = ratetypeCode;
	}

	public String getRatetypeCode(){
	    return ratetypeCode;
	}

	public void setRateEffectiveDate(Date rateEffectiveDate){
	    this.rateEffectiveDate = rateEffectiveDate;
	}

	public Date getRateEffectiveDate(){
	    return rateEffectiveDate;
	}

	public void setRateExpirationDate(Date rateExpirationDate){
	    this.rateExpirationDate = rateExpirationDate;
	}

	public Date getRateExpirationDate(){
	    return rateExpirationDate;
	}

	public void setCountrySalesTier1Rate(Double countrySalesTier1Rate){
	    this.countrySalesTier1Rate = countrySalesTier1Rate;
	}

	public Double getCountrySalesTier1Rate(){
	    return countrySalesTier1Rate;
	}

	public void setCountrySalesTier2Rate(Double countrySalesTier2Rate){
	    this.countrySalesTier2Rate = countrySalesTier2Rate;
	}

	public Double getCountrySalesTier2Rate(){
	    return countrySalesTier2Rate;
	}

	public void setCountrySalesTier3Rate(Double countrySalesTier3Rate){
	    this.countrySalesTier3Rate = countrySalesTier3Rate;
	}

	public Double getCountrySalesTier3Rate(){
	    return countrySalesTier3Rate;
	}

	public void setCountrySalesTier1Setamt(Double countrySalesTier1Setamt){
	    this.countrySalesTier1Setamt = countrySalesTier1Setamt;
	}

	public Double getCountrySalesTier1Setamt(){
	    return countrySalesTier1Setamt;
	}

	public void setCountrySalesTier2Setamt(Double countrySalesTier2Setamt){
	    this.countrySalesTier2Setamt = countrySalesTier2Setamt;
	}

	public Double getCountrySalesTier2Setamt(){
	    return countrySalesTier2Setamt;
	}

	public void setCountrySalesTier3Setamt(Double countrySalesTier3Setamt){
	    this.countrySalesTier3Setamt = countrySalesTier3Setamt;
	}

	public Double getCountrySalesTier3Setamt(){
	    return countrySalesTier3Setamt;
	}

	public void setCountrySalesTier1MaxAmt(Double countrySalesTier1MaxAmt){
	    this.countrySalesTier1MaxAmt = countrySalesTier1MaxAmt;
	}

	public Double getCountrySalesTier1MaxAmt(){
	    return countrySalesTier1MaxAmt;
	}

	public void setCountrySalesTier2MinAmt(Double countrySalesTier2MinAmt){
	    this.countrySalesTier2MinAmt = countrySalesTier2MinAmt;
	}

	public Double getCountrySalesTier2MinAmt(){
	    return countrySalesTier2MinAmt;
	}

	public void setCountrySalesTier2MaxAmt(Double countrySalesTier2MaxAmt){
	    this.countrySalesTier2MaxAmt = countrySalesTier2MaxAmt;
	}

	public Double getCountrySalesTier2MaxAmt(){
	    return countrySalesTier2MaxAmt;
	}

	public void setCountrySalesTier2EntamFlag(String countrySalesTier2EntamFlag){
	    this.countrySalesTier2EntamFlag = countrySalesTier2EntamFlag;
	}

	public String getCountrySalesTier2EntamFlag(){
	    return countrySalesTier2EntamFlag;
	}

	public void setCountrySalesTier3EntamFlag(String countrySalesTier3EntamFlag){
	    this.countrySalesTier3EntamFlag = countrySalesTier3EntamFlag;
	}

	public String getCountrySalesTier3EntamFlag(){
	    return countrySalesTier3EntamFlag;
	}

	public void setCountrySalesMaxtaxAmt(Double countrySalesMaxtaxAmt){
	    this.countrySalesMaxtaxAmt = countrySalesMaxtaxAmt;
	}

	public Double getCountrySalesMaxtaxAmt(){
	    return countrySalesMaxtaxAmt;
	}

	public void setCountryUseTier1Rate(Double countryUseTier1Rate){
	    this.countryUseTier1Rate = countryUseTier1Rate;
	}

	public Double getCountryUseTier1Rate(){
	    return countryUseTier1Rate;
	}

	public void setCountryUseTier2Rate(Double countryUseTier2Rate){
	    this.countryUseTier2Rate = countryUseTier2Rate;
	}

	public Double getCountryUseTier2Rate(){
	    return countryUseTier2Rate;
	}

	public void setCountryUseTier3Rate(Double countryUseTier3Rate){
	    this.countryUseTier3Rate = countryUseTier3Rate;
	}

	public Double getCountryUseTier3Rate(){
	    return countryUseTier3Rate;
	}

	public void setCountryUseTier1Setamt(Double countryUseTier1Setamt){
	    this.countryUseTier1Setamt = countryUseTier1Setamt;
	}

	public Double getCountryUseTier1Setamt(){
	    return countryUseTier1Setamt;
	}

	public void setCountryUseTier2Setamt(Double countryUseTier2Setamt){
	    this.countryUseTier2Setamt = countryUseTier2Setamt;
	}

	public Double getCountryUseTier2Setamt(){
	    return countryUseTier2Setamt;
	}

	public void setCountryUseTier3Setamt(Double countryUseTier3Setamt){
	    this.countryUseTier3Setamt = countryUseTier3Setamt;
	}

	public Double getCountryUseTier3Setamt(){
	    return countryUseTier3Setamt;
	}

	public void setCountryUseTier1MaxAmt(Double countryUseTier1MaxAmt){
	    this.countryUseTier1MaxAmt = countryUseTier1MaxAmt;
	}

	public Double getCountryUseTier1MaxAmt(){
	    return countryUseTier1MaxAmt;
	}

	public void setCountryUseTier2MinAmt(Double countryUseTier2MinAmt){
	    this.countryUseTier2MinAmt = countryUseTier2MinAmt;
	}

	public Double getCountryUseTier2MinAmt(){
	    return countryUseTier2MinAmt;
	}

	public void setCountryUseTier2MaxAmt(Double countryUseTier2MaxAmt){
	    this.countryUseTier2MaxAmt = countryUseTier2MaxAmt;
	}

	public Double getCountryUseTier2MaxAmt(){
	    return countryUseTier2MaxAmt;
	}

	public void setCountryUseTier2EntamFlag(String countryUseTier2EntamFlag){
	    this.countryUseTier2EntamFlag = countryUseTier2EntamFlag;
	}

	public String getCountryUseTier2EntamFlag(){
	    return countryUseTier2EntamFlag;
	}

	public void setCountryUseTier3EntamFlag(String countryUseTier3EntamFlag){
	    this.countryUseTier3EntamFlag = countryUseTier3EntamFlag;
	}

	public String getCountryUseTier3EntamFlag(){
	    return countryUseTier3EntamFlag;
	}

	public void setCountryUseMaxtaxAmt(Double countryUseMaxtaxAmt){
	    this.countryUseMaxtaxAmt = countryUseMaxtaxAmt;
	}

	public Double getCountryUseMaxtaxAmt(){
	    return countryUseMaxtaxAmt;
	}

	public void setCountryUseSameSales(String countryUseSameSales){
	    this.countryUseSameSales = countryUseSameSales;
	}

	public String getCountryUseSameSales(){
	    return countryUseSameSales;
	}

	public void setStateSalesTier1Rate(Double stateSalesTier1Rate){
	    this.stateSalesTier1Rate = stateSalesTier1Rate;
	}

	public Double getStateSalesTier1Rate(){
	    return stateSalesTier1Rate;
	}

	public void setStateSalesTier2Rate(Double stateSalesTier2Rate){
	    this.stateSalesTier2Rate = stateSalesTier2Rate;
	}

	public Double getStateSalesTier2Rate(){
	    return stateSalesTier2Rate;
	}

	public void setStateSalesTier3Rate(Double stateSalesTier3Rate){
	    this.stateSalesTier3Rate = stateSalesTier3Rate;
	}

	public Double getStateSalesTier3Rate(){
	    return stateSalesTier3Rate;
	}

	public void setStateSalesTier1Setamt(Double stateSalesTier1Setamt){
	    this.stateSalesTier1Setamt = stateSalesTier1Setamt;
	}

	public Double getStateSalesTier1Setamt(){
	    return stateSalesTier1Setamt;
	}

	public void setStateSalesTier2Setamt(Double stateSalesTier2Setamt){
	    this.stateSalesTier2Setamt = stateSalesTier2Setamt;
	}

	public Double getStateSalesTier2Setamt(){
	    return stateSalesTier2Setamt;
	}

	public void setStateSalesTier3Setamt(Double stateSalesTier3Setamt){
	    this.stateSalesTier3Setamt = stateSalesTier3Setamt;
	}

	public Double getStateSalesTier3Setamt(){
	    return stateSalesTier3Setamt;
	}

	public void setStateSalesTier1MaxAmt(Double stateSalesTier1MaxAmt){
	    this.stateSalesTier1MaxAmt = stateSalesTier1MaxAmt;
	}

	public Double getStateSalesTier1MaxAmt(){
	    return stateSalesTier1MaxAmt;
	}

	public void setStateSalesTier2MinAmt(Double stateSalesTier2MinAmt){
	    this.stateSalesTier2MinAmt = stateSalesTier2MinAmt;
	}

	public Double getStateSalesTier2MinAmt(){
	    return stateSalesTier2MinAmt;
	}

	public void setStateSalesTier2MaxAmt(Double stateSalesTier2MaxAmt){
	    this.stateSalesTier2MaxAmt = stateSalesTier2MaxAmt;
	}

	public Double getStateSalesTier2MaxAmt(){
	    return stateSalesTier2MaxAmt;
	}

	public void setStateSalesTier2EntamFlag(String stateSalesTier2EntamFlag){
	    this.stateSalesTier2EntamFlag = stateSalesTier2EntamFlag;
	}

	public String getStateSalesTier2EntamFlag(){
	    return stateSalesTier2EntamFlag;
	}

	public void setStateSalesTier3EntamFlag(String stateSalesTier3EntamFlag){
	    this.stateSalesTier3EntamFlag = stateSalesTier3EntamFlag;
	}

	public String getStateSalesTier3EntamFlag(){
	    return stateSalesTier3EntamFlag;
	}

	public void setStateSalesMaxtaxAmt(Double stateSalesMaxtaxAmt){
	    this.stateSalesMaxtaxAmt = stateSalesMaxtaxAmt;
	}

	public Double getStateSalesMaxtaxAmt(){
	    return stateSalesMaxtaxAmt;
	}

	public void setStateUseTier1Rate(Double stateUseTier1Rate){
	    this.stateUseTier1Rate = stateUseTier1Rate;
	}

	public Double getStateUseTier1Rate(){
	    return stateUseTier1Rate;
	}

	public void setStateUseTier2Rate(Double stateUseTier2Rate){
	    this.stateUseTier2Rate = stateUseTier2Rate;
	}

	public Double getStateUseTier2Rate(){
	    return stateUseTier2Rate;
	}

	public void setStateUseTier3Rate(Double stateUseTier3Rate){
	    this.stateUseTier3Rate = stateUseTier3Rate;
	}

	public Double getStateUseTier3Rate(){
	    return stateUseTier3Rate;
	}

	public void setStateUseTier1Setamt(Double stateUseTier1Setamt){
	    this.stateUseTier1Setamt = stateUseTier1Setamt;
	}

	public Double getStateUseTier1Setamt(){
	    return stateUseTier1Setamt;
	}

	public void setStateUseTier2Setamt(Double stateUseTier2Setamt){
	    this.stateUseTier2Setamt = stateUseTier2Setamt;
	}

	public Double getStateUseTier2Setamt(){
	    return stateUseTier2Setamt;
	}

	public void setStateUseTier3Setamt(Double stateUseTier3Setamt){
	    this.stateUseTier3Setamt = stateUseTier3Setamt;
	}

	public Double getStateUseTier3Setamt(){
	    return stateUseTier3Setamt;
	}

	public void setStateUseTier1MaxAmt(Double stateUseTier1MaxAmt){
	    this.stateUseTier1MaxAmt = stateUseTier1MaxAmt;
	}

	public Double getStateUseTier1MaxAmt(){
	    return stateUseTier1MaxAmt;
	}

	public void setStateUseTier2MinAmt(Double stateUseTier2MinAmt){
	    this.stateUseTier2MinAmt = stateUseTier2MinAmt;
	}

	public Double getStateUseTier2MinAmt(){
	    return stateUseTier2MinAmt;
	}

	public void setStateUseTier2MaxAmt(Double stateUseTier2MaxAmt){
	    this.stateUseTier2MaxAmt = stateUseTier2MaxAmt;
	}

	public Double getStateUseTier2MaxAmt(){
	    return stateUseTier2MaxAmt;
	}

	public void setStateUseTier2EntamFlag(String stateUseTier2EntamFlag){
	    this.stateUseTier2EntamFlag = stateUseTier2EntamFlag;
	}

	public String getStateUseTier2EntamFlag(){
	    return stateUseTier2EntamFlag;
	}

	public void setStateUseTier3EntamFlag(String stateUseTier3EntamFlag){
	    this.stateUseTier3EntamFlag = stateUseTier3EntamFlag;
	}

	public String getStateUseTier3EntamFlag(){
	    return stateUseTier3EntamFlag;
	}

	public void setStateUseMaxtaxAmt(Double stateUseMaxtaxAmt){
	    this.stateUseMaxtaxAmt = stateUseMaxtaxAmt;
	}

	public Double getStateUseMaxtaxAmt(){
	    return stateUseMaxtaxAmt;
	}

	public void setStateUseSameSales(String stateUseSameSales){
	    this.stateUseSameSales = stateUseSameSales;
	}

	public String getStateUseSameSales(){
	    return stateUseSameSales;
	}

	public void setCountySalesTier1Rate(Double countySalesTier1Rate){
	    this.countySalesTier1Rate = countySalesTier1Rate;
	}

	public Double getCountySalesTier1Rate(){
	    return countySalesTier1Rate;
	}

	public void setCountySalesTier2Rate(Double countySalesTier2Rate){
	    this.countySalesTier2Rate = countySalesTier2Rate;
	}

	public Double getCountySalesTier2Rate(){
	    return countySalesTier2Rate;
	}

	public void setCountySalesTier3Rate(Double countySalesTier3Rate){
	    this.countySalesTier3Rate = countySalesTier3Rate;
	}

	public Double getCountySalesTier3Rate(){
	    return countySalesTier3Rate;
	}

	public void setCountySalesTier1Setamt(Double countySalesTier1Setamt){
	    this.countySalesTier1Setamt = countySalesTier1Setamt;
	}

	public Double getCountySalesTier1Setamt(){
	    return countySalesTier1Setamt;
	}

	public void setCountySalesTier2Setamt(Double countySalesTier2Setamt){
	    this.countySalesTier2Setamt = countySalesTier2Setamt;
	}

	public Double getCountySalesTier2Setamt(){
	    return countySalesTier2Setamt;
	}

	public void setCountySalesTier3Setamt(Double countySalesTier3Setamt){
	    this.countySalesTier3Setamt = countySalesTier3Setamt;
	}

	public Double getCountySalesTier3Setamt(){
	    return countySalesTier3Setamt;
	}

	public void setCountySalesTier1MaxAmt(Double countySalesTier1MaxAmt){
	    this.countySalesTier1MaxAmt = countySalesTier1MaxAmt;
	}

	public Double getCountySalesTier1MaxAmt(){
	    return countySalesTier1MaxAmt;
	}

	public void setCountySalesTier2MinAmt(Double countySalesTier2MinAmt){
	    this.countySalesTier2MinAmt = countySalesTier2MinAmt;
	}

	public Double getCountySalesTier2MinAmt(){
	    return countySalesTier2MinAmt;
	}

	public void setCountySalesTier2MaxAmt(Double countySalesTier2MaxAmt){
	    this.countySalesTier2MaxAmt = countySalesTier2MaxAmt;
	}

	public Double getCountySalesTier2MaxAmt(){
	    return countySalesTier2MaxAmt;
	}

	public void setCountySalesTier2EntamFlag(String countySalesTier2EntamFlag){
	    this.countySalesTier2EntamFlag = countySalesTier2EntamFlag;
	}

	public String getCountySalesTier2EntamFlag(){
	    return countySalesTier2EntamFlag;
	}

	public void setCountySalesTier3EntamFlag(String countySalesTier3EntamFlag){
	    this.countySalesTier3EntamFlag = countySalesTier3EntamFlag;
	}

	public String getCountySalesTier3EntamFlag(){
	    return countySalesTier3EntamFlag;
	}

	public void setCountySalesMaxtaxAmt(Double countySalesMaxtaxAmt){
	    this.countySalesMaxtaxAmt = countySalesMaxtaxAmt;
	}

	public Double getCountySalesMaxtaxAmt(){
	    return countySalesMaxtaxAmt;
	}

	public void setCountyUseTier1Rate(Double countyUseTier1Rate){
	    this.countyUseTier1Rate = countyUseTier1Rate;
	}

	public Double getCountyUseTier1Rate(){
	    return countyUseTier1Rate;
	}

	public void setCountyUseTier2Rate(Double countyUseTier2Rate){
	    this.countyUseTier2Rate = countyUseTier2Rate;
	}

	public Double getCountyUseTier2Rate(){
	    return countyUseTier2Rate;
	}

	public void setCountyUseTier3Rate(Double countyUseTier3Rate){
	    this.countyUseTier3Rate = countyUseTier3Rate;
	}

	public Double getCountyUseTier3Rate(){
	    return countyUseTier3Rate;
	}

	public void setCountyUseTier1Setamt(Double countyUseTier1Setamt){
	    this.countyUseTier1Setamt = countyUseTier1Setamt;
	}

	public Double getCountyUseTier1Setamt(){
	    return countyUseTier1Setamt;
	}

	public void setCountyUseTier2Setamt(Double countyUseTier2Setamt){
	    this.countyUseTier2Setamt = countyUseTier2Setamt;
	}

	public Double getCountyUseTier2Setamt(){
	    return countyUseTier2Setamt;
	}

	public void setCountyUseTier3Setamt(Double countyUseTier3Setamt){
	    this.countyUseTier3Setamt = countyUseTier3Setamt;
	}

	public Double getCountyUseTier3Setamt(){
	    return countyUseTier3Setamt;
	}

	public void setCountyUseTier1MaxAmt(Double countyUseTier1MaxAmt){
	    this.countyUseTier1MaxAmt = countyUseTier1MaxAmt;
	}

	public Double getCountyUseTier1MaxAmt(){
	    return countyUseTier1MaxAmt;
	}

	public void setCountyUseTier2MinAmt(Double countyUseTier2MinAmt){
	    this.countyUseTier2MinAmt = countyUseTier2MinAmt;
	}

	public Double getCountyUseTier2MinAmt(){
	    return countyUseTier2MinAmt;
	}

	public void setCountyUseTier2MaxAmt(Double countyUseTier2MaxAmt){
	    this.countyUseTier2MaxAmt = countyUseTier2MaxAmt;
	}

	public Double getCountyUseTier2MaxAmt(){
	    return countyUseTier2MaxAmt;
	}

	public void setCountyUseTier2EntamFlag(String countyUseTier2EntamFlag){
	    this.countyUseTier2EntamFlag = countyUseTier2EntamFlag;
	}

	public String getCountyUseTier2EntamFlag(){
	    return countyUseTier2EntamFlag;
	}

	public void setCountyUseTier3EntamFlag(String countyUseTier3EntamFlag){
	    this.countyUseTier3EntamFlag = countyUseTier3EntamFlag;
	}

	public String getCountyUseTier3EntamFlag(){
	    return countyUseTier3EntamFlag;
	}

	public void setCountyUseMaxtaxAmt(Double countyUseMaxtaxAmt){
	    this.countyUseMaxtaxAmt = countyUseMaxtaxAmt;
	}

	public Double getCountyUseMaxtaxAmt(){
	    return countyUseMaxtaxAmt;
	}

	public void setCountyUseSameSales(String countyUseSameSales){
	    this.countyUseSameSales = countyUseSameSales;
	}

	public String getCountyUseSameSales(){
	    return countyUseSameSales;
	}

	public void setCitySalesTier1Rate(Double citySalesTier1Rate){
	    this.citySalesTier1Rate = citySalesTier1Rate;
	}

	public Double getCitySalesTier1Rate(){
	    return citySalesTier1Rate;
	}

	public void setCitySalesTier2Rate(Double citySalesTier2Rate){
	    this.citySalesTier2Rate = citySalesTier2Rate;
	}

	public Double getCitySalesTier2Rate(){
	    return citySalesTier2Rate;
	}

	public void setCitySalesTier3Rate(Double citySalesTier3Rate){
	    this.citySalesTier3Rate = citySalesTier3Rate;
	}

	public Double getCitySalesTier3Rate(){
	    return citySalesTier3Rate;
	}

	public void setCitySalesTier1Setamt(Double citySalesTier1Setamt){
	    this.citySalesTier1Setamt = citySalesTier1Setamt;
	}

	public Double getCitySalesTier1Setamt(){
	    return citySalesTier1Setamt;
	}

	public void setCitySalesTier2Setamt(Double citySalesTier2Setamt){
	    this.citySalesTier2Setamt = citySalesTier2Setamt;
	}

	public Double getCitySalesTier2Setamt(){
	    return citySalesTier2Setamt;
	}

	public void setCitySalesTier3Setamt(Double citySalesTier3Setamt){
	    this.citySalesTier3Setamt = citySalesTier3Setamt;
	}

	public Double getCitySalesTier3Setamt(){
	    return citySalesTier3Setamt;
	}

	public void setCitySalesTier1MaxAmt(Double citySalesTier1MaxAmt){
	    this.citySalesTier1MaxAmt = citySalesTier1MaxAmt;
	}

	public Double getCitySalesTier1MaxAmt(){
	    return citySalesTier1MaxAmt;
	}

	public void setCitySalesTier2MinAmt(Double citySalesTier2MinAmt){
	    this.citySalesTier2MinAmt = citySalesTier2MinAmt;
	}

	public Double getCitySalesTier2MinAmt(){
	    return citySalesTier2MinAmt;
	}

	public void setCitySalesTier2MaxAmt(Double citySalesTier2MaxAmt){
	    this.citySalesTier2MaxAmt = citySalesTier2MaxAmt;
	}

	public Double getCitySalesTier2MaxAmt(){
	    return citySalesTier2MaxAmt;
	}

	public void setCitySalesTier2EntamFlag(String citySalesTier2EntamFlag){
	    this.citySalesTier2EntamFlag = citySalesTier2EntamFlag;
	}

	public String getCitySalesTier2EntamFlag(){
	    return citySalesTier2EntamFlag;
	}

	public void setCitySalesTier3EntamFlag(String citySalesTier3EntamFlag){
	    this.citySalesTier3EntamFlag = citySalesTier3EntamFlag;
	}

	public String getCitySalesTier3EntamFlag(){
	    return citySalesTier3EntamFlag;
	}

	public void setCitySalesMaxtaxAmt(Double citySalesMaxtaxAmt){
	    this.citySalesMaxtaxAmt = citySalesMaxtaxAmt;
	}

	public Double getCitySalesMaxtaxAmt(){
	    return citySalesMaxtaxAmt;
	}

	public void setCityUseTier1Rate(Double cityUseTier1Rate){
	    this.cityUseTier1Rate = cityUseTier1Rate;
	}

	public Double getCityUseTier1Rate(){
	    return cityUseTier1Rate;
	}

	public void setCityUseTier2Rate(Double cityUseTier2Rate){
	    this.cityUseTier2Rate = cityUseTier2Rate;
	}

	public Double getCityUseTier2Rate(){
	    return cityUseTier2Rate;
	}

	public void setCityUseTier3Rate(Double cityUseTier3Rate){
	    this.cityUseTier3Rate = cityUseTier3Rate;
	}

	public Double getCityUseTier3Rate(){
	    return cityUseTier3Rate;
	}

	public void setCityUseTier1Setamt(Double cityUseTier1Setamt){
	    this.cityUseTier1Setamt = cityUseTier1Setamt;
	}

	public Double getCityUseTier1Setamt(){
	    return cityUseTier1Setamt;
	}

	public void setCityUseTier2Setamt(Double cityUseTier2Setamt){
	    this.cityUseTier2Setamt = cityUseTier2Setamt;
	}

	public Double getCityUseTier2Setamt(){
	    return cityUseTier2Setamt;
	}

	public void setCityUseTier3Setamt(Double cityUseTier3Setamt){
	    this.cityUseTier3Setamt = cityUseTier3Setamt;
	}

	public Double getCityUseTier3Setamt(){
	    return cityUseTier3Setamt;
	}

	public void setCityUseTier1MaxAmt(Double cityUseTier1MaxAmt){
	    this.cityUseTier1MaxAmt = cityUseTier1MaxAmt;
	}

	public Double getCityUseTier1MaxAmt(){
	    return cityUseTier1MaxAmt;
	}

	public void setCityUseTier2MinAmt(Double cityUseTier2MinAmt){
	    this.cityUseTier2MinAmt = cityUseTier2MinAmt;
	}

	public Double getCityUseTier2MinAmt(){
	    return cityUseTier2MinAmt;
	}

	public void setCityUseTier2MaxAmt(Double cityUseTier2MaxAmt){
	    this.cityUseTier2MaxAmt = cityUseTier2MaxAmt;
	}

	public Double getCityUseTier2MaxAmt(){
	    return cityUseTier2MaxAmt;
	}

	public void setCityUseTier2EntamFlag(String cityUseTier2EntamFlag){
	    this.cityUseTier2EntamFlag = cityUseTier2EntamFlag;
	}

	public String getCityUseTier2EntamFlag(){
	    return cityUseTier2EntamFlag;
	}

	public void setCityUseTier3EntamFlag(String cityUseTier3EntamFlag){
	    this.cityUseTier3EntamFlag = cityUseTier3EntamFlag;
	}

	public String getCityUseTier3EntamFlag(){
	    return cityUseTier3EntamFlag;
	}

	public void setCityUseMaxtaxAmt(Double cityUseMaxtaxAmt){
	    this.cityUseMaxtaxAmt = cityUseMaxtaxAmt;
	}

	public Double getCityUseMaxtaxAmt(){
	    return cityUseMaxtaxAmt;
	}

	public void setCityUseSameSales(String cityUseSameSales){
	    this.cityUseSameSales = cityUseSameSales;
	}

	public String getCityUseSameSales(){
	    return cityUseSameSales;
	}

	public void setStj1SalesRate(Double stj1SalesRate){
	    this.stj1SalesRate = stj1SalesRate;
	}

	public Double getStj1SalesRate(){
	    return stj1SalesRate;
	}

	public void setStj1SalesSetamt(Double stj1SalesSetamt){
	    this.stj1SalesSetamt = stj1SalesSetamt;
	}

	public Double getStj1SalesSetamt(){
	    return stj1SalesSetamt;
	}

	public void setStj2SalesRate(Double stj2SalesRate){
	    this.stj2SalesRate = stj2SalesRate;
	}

	public Double getStj2SalesRate(){
	    return stj2SalesRate;
	}

	public void setStj2SalesSetamt(Double stj2SalesSetamt){
	    this.stj2SalesSetamt = stj2SalesSetamt;
	}

	public Double getStj2SalesSetamt(){
	    return stj2SalesSetamt;
	}

	public void setStj3SalesRate(Double stj3SalesRate){
	    this.stj3SalesRate = stj3SalesRate;
	}

	public Double getStj3SalesRate(){
	    return stj3SalesRate;
	}

	public void setStj3SalesSetamt(Double stj3SalesSetamt){
	    this.stj3SalesSetamt = stj3SalesSetamt;
	}

	public Double getStj3SalesSetamt(){
	    return stj3SalesSetamt;
	}

	public void setStj4SalesRate(Double stj4SalesRate){
	    this.stj4SalesRate = stj4SalesRate;
	}

	public Double getStj4SalesRate(){
	    return stj4SalesRate;
	}

	public void setStj4SalesSetamt(Double stj4SalesSetamt){
	    this.stj4SalesSetamt = stj4SalesSetamt;
	}

	public Double getStj4SalesSetamt(){
	    return stj4SalesSetamt;
	}

	public void setStj5SalesRate(Double stj5SalesRate){
	    this.stj5SalesRate = stj5SalesRate;
	}

	public Double getStj5SalesRate(){
	    return stj5SalesRate;
	}

	public void setStj5SalesSetamt(Double stj5SalesSetamt){
	    this.stj5SalesSetamt = stj5SalesSetamt;
	}

	public Double getStj5SalesSetamt(){
	    return stj5SalesSetamt;
	}

	public void setStj1UseRate(Double stj1UseRate){
	    this.stj1UseRate = stj1UseRate;
	}

	public Double getStj1UseRate(){
	    return stj1UseRate;
	}

	public void setStj1UseSetamt(Double stj1UseSetamt){
	    this.stj1UseSetamt = stj1UseSetamt;
	}

	public Double getStj1UseSetamt(){
	    return stj1UseSetamt;
	}

	public void setStj2UseRate(Double stj2UseRate){
	    this.stj2UseRate = stj2UseRate;
	}

	public Double getStj2UseRate(){
	    return stj2UseRate;
	}

	public void setStj2UseSetamt(Double stj2UseSetamt){
	    this.stj2UseSetamt = stj2UseSetamt;
	}

	public Double getStj2UseSetamt(){
	    return stj2UseSetamt;
	}

	public void setStj3UseRate(Double stj3UseRate){
	    this.stj3UseRate = stj3UseRate;
	}

	public Double getStj3UseRate(){
	    return stj3UseRate;
	}

	public void setStj3UseSetamt(Double stj3UseSetamt){
	    this.stj3UseSetamt = stj3UseSetamt;
	}

	public Double getStj3UseSetamt(){
	    return stj3UseSetamt;
	}

	public void setStj4UseRate(Double stj4UseRate){
	    this.stj4UseRate = stj4UseRate;
	}

	public Double getStj4UseRate(){
	    return stj4UseRate;
	}

	public void setStj4UseSetamt(Double stj4UseSetamt){
	    this.stj4UseSetamt = stj4UseSetamt;
	}

	public Double getStj4UseSetamt(){
	    return stj4UseSetamt;
	}

	public void setStj5UseRate(Double stj5UseRate){
	    this.stj5UseRate = stj5UseRate;
	}

	public Double getStj5UseRate(){
	    return stj5UseRate;
	}

	public void setStj5UseSetamt(Double stj5UseSetamt){
	    this.stj5UseSetamt = stj5UseSetamt;
	}

	public Double getStj5UseSetamt(){
	    return stj5UseSetamt;
	}

	public void setStjUseSameSales(String stjUseSameSales){
	    this.stjUseSameSales = stjUseSameSales;
	}

	public String getStjUseSameSales(){
	    return stjUseSameSales;
	}

	public void setCombinedUseRate(Double combinedUseRate){
	    this.combinedUseRate = combinedUseRate;
	}

	public Double getCombinedUseRate(){
	    return combinedUseRate;
	}

	public void setCombinedSalesRate(Double combinedSalesRate){
	    this.combinedSalesRate = combinedSalesRate;
	}

	public Double getCombinedSalesRate(){
	    return combinedSalesRate;
	}

	public void setUpdateFlag(String updateFlag){
	    this.updateFlag = updateFlag;
	}

	public String getUpdateFlag(){
	    return updateFlag;
	}
	
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Long getLine() {
		return this.line;
	}
	public void setLine(Long line) {
		this.line = line;
	}	
	
	/**
	 * @return the id
	 */
	public BCPJurisdictionTaxRatePK getId() {
		return this.id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(BCPJurisdictionTaxRatePK id) {
		this.id = id;
	}
	
	//Fix bugs 4381, 4380,4377, and 3921
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	  static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPJurisdictionTaxRate.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_JURISDICTION_TAXRATE (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		
		//Add primary key
		colBuf.append(", BATCH_ID, GEOCODE, ZIP, CITY, COUNTY, JUR_EFFECTIVE_DATE, BCP_JURISDICTION_TAXRATE_ID");
		valBuf.append(", ?, ?, ?, ?, ?, ?, ?");
		
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPJurisdictionTaxRate.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){					
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}
		
		//Add primary key
		idx++;
		s.setLong(idx, this.getId().getBatchId());

		idx++;
		s.setString(idx, this.getId().getGeocode());
		
		idx++;
		s.setString(idx, this.getId().getZip());
		
		idx++;
		s.setString(idx, this.getId().getCity());
		
		idx++;
		s.setString(idx, this.getId().getCounty());
		
		idx++;
		s.setDate(idx, new java.sql.Date(this.getId().getJurEffectiveDate().getTime()));
		
		idx++;
		s.setLong(idx, this.getId().getBcpJurisdictionTaxRateId());
	}
	
	public static void main(String[] arg){
		//String aColumn = "COUNTRY_SALES_TIER1_RATE";
		
		String[] COLUMNS = {
				"COUNTRY",
				"GEOCODE",
				"STATE",
				"COUNTY",
				"CITY",
				"REPORTING_CITY",
				"REPORTING_CITY_FIPS",
				"STJ1_NAME",
				"STJ2_NAME",
				"STJ3_NAME",
				"STJ4_NAME",
				"STJ5_NAME",
				"ZIP",
				"ZIPPLUS4",
				"IN_OUT",
				"JUR_EFFECTIVE_DATE",
				"JUR_EXPIRATION_DATE",
				"DESCRIPTION",
				"COUNTRY_NEXUSIND_CODE",
				"STATE_NEXUSIND_CODE",
				"COUNTY_NEXUSIND_CODE",
				"CITY_NEXUSIND_CODE",
				"STJ1_NEXUSIND_CODE",
				"STJ2_NEXUSIND_CODE",
				"STJ3_NEXUSIND_CODE",
				"STJ4_NEXUSIND_CODE",
				"STJ5_NEXUSIND_CODE",
				"STJ1_LOCAL_CODE",
				"STJ2_LOCAL_CODE",
				"STJ3_LOCAL_CODE",
				"STJ4_LOCAL_CODE",
				"STJ5_LOCAL_CODE",
				"RATETYPE_CODE",
				"RATE_EFFECTIVE_DATE",
				"RATE_EXPIRATION_DATE",
				"COUNTRY_SALES_TIER1_RATE",
				"COUNTRY_SALES_TIER2_RATE",
				"COUNTRY_SALES_TIER3_RATE",
				"COUNTRY_SALES_TIER1_SETAMT",
				"COUNTRY_SALES_TIER2_SETAMT",
				"COUNTRY_SALES_TIER3_SETAMT",
				"COUNTRY_SALES_TIER1_MAX_AMT",
				"COUNTRY_SALES_TIER2_MIN_AMT",
				"COUNTRY_SALES_TIER2_MAX_AMT",
				"COUNTRY_SALES_TIER2_ENTAM_FLAG",
				"COUNTRY_SALES_TIER3_ENTAM_FLAG",
				"COUNTRY_SALES_MAXTAX_AMT",
				"COUNTRY_USE_TIER1_RATE",
				"COUNTRY_USE_TIER2_RATE",
				"COUNTRY_USE_TIER3_RATE",
				"COUNTRY_USE_TIER1_SETAMT",
				"COUNTRY_USE_TIER2_SETAMT",
				"COUNTRY_USE_TIER3_SETAMT",
				"COUNTRY_USE_TIER1_MAX_AMT",
				"COUNTRY_USE_TIER2_MIN_AMT",
				"COUNTRY_USE_TIER2_MAX_AMT",
				"COUNTRY_USE_TIER2_ENTAM_FLAG",
				"COUNTRY_USE_TIER3_ENTAM_FLAG",
				"COUNTRY_USE_MAXTAX_AMT",
				"COUNTRY_USE_SAME_SALES",
				"STATE_SALES_TIER1_RATE",
				"STATE_SALES_TIER2_RATE",
				"STATE_SALES_TIER3_RATE",
				"STATE_SALES_TIER1_SETAMT",
				"STATE_SALES_TIER2_SETAMT",
				"STATE_SALES_TIER3_SETAMT",
				"STATE_SALES_TIER1_MAX_AMT",
				"STATE_SALES_TIER2_MIN_AMT",
				"STATE_SALES_TIER2_MAX_AMT",
				"STATE_SALES_TIER2_ENTAM_FLAG",
				"STATE_SALES_TIER3_ENTAM_FLAG",
				"STATE_SALES_MAXTAX_AMT",
				"STATE_USE_TIER1_RATE",
				"STATE_USE_TIER2_RATE",
				"STATE_USE_TIER3_RATE",
				"STATE_USE_TIER1_SETAMT",
				"STATE_USE_TIER2_SETAMT",
				"STATE_USE_TIER3_SETAMT",
				"STATE_USE_TIER1_MAX_AMT",
				"STATE_USE_TIER2_MIN_AMT",
				"STATE_USE_TIER2_MAX_AMT",
				"STATE_USE_TIER2_ENTAM_FLAG",
				"STATE_USE_TIER3_ENTAM_FLAG",
				"STATE_USE_MAXTAX_AMT",
				"STATE_USE_SAME_SALES",
				"COUNTY_SALES_TIER1_RATE",
				"COUNTY_SALES_TIER2_RATE",
				"COUNTY_SALES_TIER3_RATE",
				"COUNTY_SALES_TIER1_SETAMT",
				"COUNTY_SALES_TIER2_SETAMT",
				"COUNTY_SALES_TIER3_SETAMT",
				"COUNTY_SALES_TIER1_MAX_AMT",
				"COUNTY_SALES_TIER2_MIN_AMT",
				"COUNTY_SALES_TIER2_MAX_AMT",
				"COUNTY_SALES_TIER2_ENTAM_FLAG",
				"COUNTY_SALES_TIER3_ENTAM_FLAG",
				"COUNTY_SALES_MAXTAX_AMT",
				"COUNTY_USE_TIER1_RATE",
				"COUNTY_USE_TIER2_RATE",
				"COUNTY_USE_TIER3_RATE",
				"COUNTY_USE_TIER1_SETAMT",
				"COUNTY_USE_TIER2_SETAMT",
				"COUNTY_USE_TIER3_SETAMT",
				"COUNTY_USE_TIER1_MAX_AMT",
				"COUNTY_USE_TIER2_MIN_AMT",
				"COUNTY_USE_TIER2_MAX_AMT",
				"COUNTY_USE_TIER2_ENTAM_FLAG",
				"COUNTY_USE_TIER3_ENTAM_FLAG",
				"COUNTY_USE_MAXTAX_AMT",
				"COUNTY_USE_SAME_SALES",
				"CITY_SALES_TIER1_RATE",
				"CITY_SALES_TIER2_RATE",
				"CITY_SALES_TIER3_RATE",
				"CITY_SALES_TIER1_SETAMT",
				"CITY_SALES_TIER2_SETAMT",
				"CITY_SALES_TIER3_SETAMT",
				"CITY_SALES_TIER1_MAX_AMT",
				"CITY_SALES_TIER2_MIN_AMT",
				"CITY_SALES_TIER2_MAX_AMT",
				"CITY_SALES_TIER2_ENTAM_FLAG",
				"CITY_SALES_TIER3_ENTAM_FLAG",
				"CITY_SALES_MAXTAX_AMT",
				"CITY_USE_TIER1_RATE",
				"CITY_USE_TIER2_RATE",
				"CITY_USE_TIER3_RATE",
				"CITY_USE_TIER1_SETAMT",
				"CITY_USE_TIER2_SETAMT",
				"CITY_USE_TIER3_SETAMT",
				"CITY_USE_TIER1_MAX_AMT",
				"CITY_USE_TIER2_MIN_AMT",
				"CITY_USE_TIER2_MAX_AMT",
				"CITY_USE_TIER2_ENTAM_FLAG",
				"CITY_USE_TIER3_ENTAM_FLAG",
				"CITY_USE_MAXTAX_AMT",
				"CITY_USE_SAME_SALES",
				"STJ1_SALES_RATE",
				"STJ1_SALES_SETAMT",
				"STJ2_SALES_RATE",
				"STJ2_SALES_SETAMT",
				"STJ3_SALES_RATE",
				"STJ3_SALES_SETAMT",
				"STJ4_SALES_RATE",
				"STJ4_SALES_SETAMT",
				"STJ5_SALES_RATE",
				"STJ5_SALES_SETAMT",
				"STJ1_USE_RATE",
				"STJ1_USE_SETAMT",
				"STJ2_USE_RATE",
				"STJ2_USE_SETAMT",
				"STJ3_USE_RATE",
				"STJ3_USE_SETAMT",
				"STJ4_USE_RATE",
				"STJ4_USE_SETAMT",
				"STJ5_USE_RATE",
				"STJ5_USE_SETAMT",
				"STJ_USE_SAME_SALES",
				"COMBINED_USE_RATE",
				"COMBINED_SALES_RATE",
				"UPDATE_FLAG"


		
		
		
		};
		
		String columnType = "BigDecimal";
		
		//populate
		for(int j=0; j< COLUMNS.length; j++){
			String aColumn = COLUMNS[j];
	
			String[] strings= aColumn.split("_");	
			String upperColumn = "";
			String lowerColumn = "";
			for(int i=0; i<strings.length; i++){
				upperColumn = upperColumn + strings[i].substring(0,1).toUpperCase() + strings[i].substring(1,strings[i].length()).toLowerCase();
			}
			
			lowerColumn = upperColumn.substring(0,1).toLowerCase() + upperColumn.substring(1,upperColumn.length());
			

			if(aColumn.endsWith("RATE") || aColumn.endsWith("AMT")){
				System.out.println("taxrate.set" + upperColumn + "((null!=bcptaxrate.get" + upperColumn + "()) ? BigDecimal.valueOf(bcptaxrate.get" + upperColumn + "()) : BigDecimal.ZERO);");
			}
			else{
				System.out.println("taxrate.set" + upperColumn + "(bcptaxrate.get" + upperColumn + "());");
			}
		}
		
		//datagrid
		for(int j=0; j< COLUMNS.length; j++){
			String aColumn = COLUMNS[j];
			
			String[] strings= aColumn.split("_");	
			String upperColumn = "";
			String lowerColumn = "";
			for(int i=0; i<strings.length; i++){
				upperColumn = upperColumn + strings[i].substring(0,1).toUpperCase() + strings[i].substring(1,strings[i].length()).toLowerCase();
			}
			
			lowerColumn = upperColumn.substring(0,1).toLowerCase() + upperColumn.substring(1,upperColumn.length());
			
			System.out.println("<rich:column>");
			System.out.println("    <f:facet name=\"header\">"+upperColumn+"</f:facet>");
			System.out.println("    <h:outputText id=\"row"+upperColumn+"\" value=\"#{row."+lowerColumn+"}\" />");
			System.out.println("</rich:column>");
			System.out.println("");
		}

		//setter
		for(int j=0; j< COLUMNS.length; j++){
			String aColumn = COLUMNS[j];
			
			if(aColumn.endsWith("DATE")){
				columnType = "Date";
			}
			else if(aColumn.endsWith("RATE") || aColumn.endsWith("AMT")){
				columnType = "Double";
			}
			else{
				columnType = "String";
			}
			
			String[] strings= aColumn.split("_");	
			String upperColumn = "";
			String lowerColumn = "";
			for(int i=0; i<strings.length; i++){
				upperColumn = upperColumn + strings[i].substring(0,1).toUpperCase() + strings[i].substring(1,strings[i].length()).toLowerCase();
			}
			
			lowerColumn = upperColumn.substring(0,1).toLowerCase() + upperColumn.substring(1,upperColumn.length());
			
			if(aColumn.endsWith("DATE")){
				System.out.println("r.set"+upperColumn+"(sdf.parse(parsedColumn = tokens[i++]));");
			}
			else if(aColumn.endsWith("RATE") || aColumn.endsWith("AMT")){
				System.out.println("r.set"+upperColumn+"(parseDouble(parsedColumn = tokens[i++]));");
			}
			else{
				System.out.println("r.set"+upperColumn+"(parsedColumn = tokens[i++]);");
			}
		}
		
		//declare
		for(int j=0; j< COLUMNS.length; j++){
			String aColumn = COLUMNS[j];
			
			if(aColumn.endsWith("DATE")){
				columnType = "Date";
			}
			else if(aColumn.endsWith("RATE") || aColumn.endsWith("AMT")){
				columnType = "Double";
			}
			else{
				columnType = "String";
			}
			
			System.out.println("@Column(name=\"" + aColumn + "\")");	
			String[] strings= aColumn.split("_");	
			String upperColumn = "";
			String lowerColumn = "";
			for(int i=0; i<strings.length; i++){
				upperColumn = upperColumn + strings[i].substring(0,1).toUpperCase() + strings[i].substring(1,strings[i].length()).toLowerCase();
			}
			
			lowerColumn = upperColumn.substring(0,1).toLowerCase() + upperColumn.substring(1,upperColumn.length());
			
			System.out.println("private " + columnType + " " + lowerColumn + ";");
			System.out.println("");
		}
		
		for(int j=0; j< COLUMNS.length; j++){
			String aColumn = COLUMNS[j];
			
			if(aColumn.endsWith("DATE")){
				columnType = "Date";
			}
			else if(aColumn.endsWith("RATE") || aColumn.endsWith("AMT")){
				columnType = "Double";
			}
			else{
				columnType = "String";
			}
			
			String[] strings= aColumn.split("_");	
			String upperColumn = "";
			String lowerColumn = "";
			for(int i=0; i<strings.length; i++){
				upperColumn = upperColumn + strings[i].substring(0,1).toUpperCase() + strings[i].substring(1,strings[i].length()).toLowerCase();
			}
			
			lowerColumn = upperColumn.substring(0,1).toLowerCase() + upperColumn.substring(1,upperColumn.length());
			
			System.out.println("public void set" + upperColumn + "("+ columnType +" " + lowerColumn + "){");
			System.out.println("    this." + lowerColumn + " = " + lowerColumn + ";");
			System.out.println("}");
			System.out.println("");

			System.out.println("public "+columnType+" get" + upperColumn + "(){");
			System.out.println("    return " + lowerColumn + ";");
			System.out.println("}");
			System.out.println("");
		}
	}

	
}