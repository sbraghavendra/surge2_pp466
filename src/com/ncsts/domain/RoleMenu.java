package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="TB_ROLE_MENU")
public class RoleMenu extends Auditable implements Serializable {
	
private static final long serialVersionUID = 1L;	
	
	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 RoleMenuPK roleMenuPK;   
	 
	//Midtier project code added - january 2009
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="ROLE_CODE", insertable=false, updatable=false)
	 private Role role;
	 
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="MENU_CODE", insertable=false, updatable=false)
	 private Menu menu;
	 
	 public RoleMenu(){
		 
	 }
	 
    public RoleMenu(RoleMenuPK roleMenuPK) {
		this.roleMenuPK = roleMenuPK;
	}

	public RoleMenu(String roleCode, String menuCode) {
    	roleMenuPK = new RoleMenuPK(roleCode,menuCode);    	
    }

	public RoleMenuPK getRoleMenuPK() {
		return roleMenuPK;
	}

	public void setRoleMenuPK(RoleMenuPK roleMenuPK) {
		this.roleMenuPK = roleMenuPK;
	}

	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof RoleMenu)) {
            return false;
        }

        final RoleMenu revision = (RoleMenu)other;

        String c1 = roleMenuPK.getMenuCode();
        String c2 = revision.roleMenuPK.getMenuCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String e1 = roleMenuPK.getRoleCode();
        String e2 = revision.roleMenuPK.getRoleCode();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = roleMenuPK.getMenuCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String d1 = roleMenuPK.getRoleCode();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}
