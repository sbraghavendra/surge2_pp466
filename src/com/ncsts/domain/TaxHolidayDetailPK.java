package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import java.io.Serializable;

/**
 * Created by RC05200 on 11/19/2015.
 */

@Embeddable
public class TaxHolidayDetailPK implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name="TAX_HOLIDAY_CODE")
    private String taxHolidayCode;

    @Column(name="TAXCODE_CODE")
    private String taxcodeCode;

    @Column(name="TAXCODE_COUNTY")
    private String taxcodeCounty;

    @Column(name="TAXCODE_CITY")
    private String taxcodeCity;

    @Column(name="TAXCODE_STJ")
    private String taxcodeStj;
    
    @Column(name="JUR_LEVEL")    
    private String jurLevel;

    public TaxHolidayDetailPK() {}

    public TaxHolidayDetailPK(String taxHolidayCode, String taxcodeCode, String taxcodeCounty, String taxcodeCity, String taxcodeStj) {
        this.taxHolidayCode = taxHolidayCode;
        this.taxcodeCode = taxcodeCode;
        this.taxcodeCounty = taxcodeCounty;
        this.taxcodeCity = taxcodeCity;
        this.taxcodeStj = taxcodeStj;
    }

    public String getTaxHolidayCode() {
        return this.taxHolidayCode;
    }

    public void setTaxHolidayCode(String taxHolidayCode) {
        this.taxHolidayCode = taxHolidayCode;
    }

    public String getTaxcodeCode() {
        return this.taxcodeCode;
    }

    public void setTaxcodeCode(String taxcodeCode) {
        this.taxcodeCode = taxcodeCode;
    }

    public String getTaxcodeCounty() {
        return this.taxcodeCounty;
    }

    public void setTaxcodeCounty(String taxcodeCounty) {
        this.taxcodeCounty = taxcodeCounty;
    }

    public String getTaxcodeCity() {
        return this.taxcodeCity;
    }

    public void setTaxcodeCity(String taxcodeCity) {
        this.taxcodeCity = taxcodeCity;
    }

    public String getTaxcodeStj() {
        return this.taxcodeStj;
    }

    public void setTaxcodeStj(String taxcodeStj) {
        this.taxcodeStj = taxcodeStj;
    }
    
    public String getJurLevel() {
        return this.jurLevel;
    }
    
    public void setJurLevel(String jurLevel) {
    	this.jurLevel = jurLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxHolidayDetailPK that = (TaxHolidayDetailPK) o;

        if (!taxHolidayCode.equals(that.taxHolidayCode)) return false;
        if (!taxcodeCode.equals(that.taxcodeCode)) return false;
        if (!taxcodeCounty.equals(that.taxcodeCounty)) return false;
        if (!taxcodeCity.equals(that.taxcodeCity)) return false;
        if (!taxcodeStj.equals(that.taxcodeStj)) return false;
        return jurLevel.equals(that.jurLevel);

    }

    @Override
    public int hashCode() {
        int result = taxHolidayCode.hashCode();
        result = 31 * result + taxcodeCode.hashCode();
        result = 31 * result + taxcodeCounty.hashCode();
        result = 31 * result + taxcodeCity.hashCode();
        result = 31 * result + taxcodeStj.hashCode();
        return result;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaxHolidayDetailPK{");
        sb.append("taxHolidayCode='").append(taxHolidayCode).append('\'');
        sb.append(", taxcodeCode='").append(taxcodeCode).append('\'');
        sb.append(", taxcodeCounty='").append(taxcodeCounty).append('\'');
        sb.append(", taxcodeCity='").append(taxcodeCity).append('\'');
        sb.append(", taxcodeStj='").append(taxcodeStj).append('\'');
        sb.append(", jurLevel='").append(jurLevel).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
