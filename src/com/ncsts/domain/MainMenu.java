package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="TB_MAIN_MENU")
public class MainMenu extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="MENU_CODE")	
    private String menuCode;
    
    @Column(name="MENU_NAME")    
    private String menuName;  
    
    @Column(name="MENU_SEQUENCE")    
    private Long menuSequence;  
    
    //Midtier project code added - january 2009
    @OneToMany(mappedBy  = "mainMenu")
    private Set<Menu> menu = new HashSet<Menu>(); 
 
    public MainMenu() { 
    	
    }
    
    public String getMenuCode() {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode) {
    	this.menuCode = menuCode;
    }
    
    public Long getMenuSequence() {
        return this.menuSequence;
    }
    
    public void setMenuSequence(Long menuSequence) {
    	this.menuSequence = menuSequence;
    }      
    
  
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof MainMenu)) {
            return false;
        }

        final MainMenu revision = (MainMenu)other;

        String c1 = getMenuCode();
        String c2 = revision.getMenuCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getMenuCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.menuCode + "::" + this.menuName;
    }


	public String getMenuName() {
		return menuName;
	}


	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Set<Menu> getMenu() {
		return menu;
	}

	public void setMenu(Set<Menu> menu) {
		this.menu = menu;
	}
}



