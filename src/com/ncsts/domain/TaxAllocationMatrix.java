package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;

import com.ncsts.view.util.ArithmeticUtils;

/**
 * @author Jeesmon Jacob
 *
 */

@Entity
@Table(name="TB_TAX_ALLOC_MATRIX")
public class TaxAllocationMatrix extends Matrix implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final long DRIVER_COUNT = 30L;
	public static final String DRIVER_CODE = "T";

	@Id
	@Column(name="TAX_ALLOC_MATRIX_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="tax_allocation_sequence")
	@SequenceGenerator(name="tax_allocation_sequence" , allocationSize = 1, sequenceName="SQ_TB_TAX_ALLOC_MATRIX_ID")
	private Long taxAllocationMatrixId;
	
	@Column(name="DRIVER_01")
    private String driver01;
    
    @Column(name="DRIVER_02" )
    private String driver02;
    
    @Column(name="DRIVER_03" )
    private String driver03;
    
    @Column(name="DRIVER_04" )
    private String driver04;
    
    @Column(name="DRIVER_05" )
    private String driver05;
    
    @Column(name="DRIVER_06" )
    private String driver06;
    
    @Column(name="DRIVER_07" )
    private String driver07;
    
    @Column(name="DRIVER_08" )
    private String driver08;
    
    @Column(name="DRIVER_09" )
    private String driver09;
    
    @Column(name="DRIVER_10" )
    private String driver10;
    
    @Column(name="DRIVER_11" )
    private String driver11;
    
    @Column(name="DRIVER_12" )
    private String driver12;
    
    @Column(name="DRIVER_13" )
    private String driver13;
    
    @Column(name="DRIVER_14" )
    private String driver14;
    
    @Column(name="DRIVER_15" )
    private String driver15;
        
    @Column(name="DRIVER_16" )
    private String driver16;
    
    @Column(name="DRIVER_17" )
    private String driver17;
   
    @Column(name="DRIVER_18")
    private String driver18;

    @Column(name="DRIVER_19")
    private String driver19;

    @Column(name="DRIVER_20")
    private String driver20;
    
    @Column(name="DRIVER_21")
    private String driver21;

    @Column(name="DRIVER_22")
    private String driver22;

    @Column(name="DRIVER_23")
    private String driver23;

    @Column(name="DRIVER_24")
    private String driver24;
                                
    @Column(name="DRIVER_25")
    private String driver25;

    @Column(name="DRIVER_26")
    private String driver26;

    @Column(name="DRIVER_27")
    private String driver27;
                                
    @Column(name="DRIVER_28")
    private String driver28;

    @Column(name="DRIVER_29")
    private String driver29;

    @Column(name="DRIVER_30")
    private String driver30;
       
    @Column(name="BINARY_WEIGHT")
    private Long binaryWeight;
    
    @Column(name="SIGNIFICANT_DIGITS")
    private String significantDigits;
    
    @Column(name="EFFECTIVE_DATE")
    private Date effectiveDate;
    
    @Column(name="EXPIRATION_DATE")
    private Date expirationDate;
    
    @Column(name="FUTURE_SPLIT")
    String futureSplit;
    
    @Column(name="COMMENTS")
    private String comments;
    
    @Column(name="ACTIVE_FLAG")
	private String activeFlag;
    
    @CollectionOfElements(fetch=FetchType.LAZY)
    @JoinTable(
            name="TB_TAX_ALLOC_MATRIX_DETAIL",
            joinColumns = @JoinColumn(name="TAX_ALLOC_MATRIX_ID")
    )
    private List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails = new ArrayList<TaxAllocationMatrixDetail>();
    
    @CollectionOfElements(fetch=FetchType.LAZY)
    @JoinTable(
            name="TB_JUR_ALLOC_MATRIX_DETAIL",
            joinColumns = @JoinColumn(name="TAX_ALLOC_MATRIX_ID")
    )
    private List<JurisdictionAllocationMatrixDetail> jurisdictionAllocationMatrixDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
    
    @Transient
    private boolean globalSplit = false;
    
    public Boolean getGlobalSplit() {
		return this.globalSplit;
	}
	
	public void setGlobalSplit(Boolean globalSplit){
		this.globalSplit = globalSplit;
	}
    
	public Long getTaxAllocationMatrixId() {
		return taxAllocationMatrixId;
	}

	public void setTaxAllocationMatrixId(Long taxAllocationMatrixId) {
		this.taxAllocationMatrixId = taxAllocationMatrixId;
	}

	public String getDriver01() {
		return driver01;
	}

	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}

	public String getDriver02() {
		return driver02;
	}

	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}

	public String getDriver03() {
		return driver03;
	}

	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}

	public String getDriver04() {
		return driver04;
	}

	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}

	public String getDriver05() {
		return driver05;
	}

	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}

	public String getDriver06() {
		return driver06;
	}

	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}

	public String getDriver07() {
		return driver07;
	}

	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}

	public String getDriver08() {
		return driver08;
	}

	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}

	public String getDriver09() {
		return driver09;
	}

	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}

	public String getDriver10() {
		return driver10;
	}

	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}

	public String getDriver11() {
		return driver11;
	}

	public void setDriver11(String driver11) {
		this.driver11 = driver11;
	}

	public String getDriver12() {
		return driver12;
	}

	public void setDriver12(String driver12) {
		this.driver12 = driver12;
	}

	public String getDriver13() {
		return driver13;
	}

	public void setDriver13(String driver13) {
		this.driver13 = driver13;
	}

	public String getDriver14() {
		return driver14;
	}

	public void setDriver14(String driver14) {
		this.driver14 = driver14;
	}

	public String getDriver15() {
		return driver15;
	}

	public void setDriver15(String driver15) {
		this.driver15 = driver15;
	}

	public String getDriver16() {
		return driver16;
	}

	public void setDriver16(String driver16) {
		this.driver16 = driver16;
	}

	public String getDriver17() {
		return driver17;
	}

	public void setDriver17(String driver17) {
		this.driver17 = driver17;
	}

	public String getDriver18() {
		return driver18;
	}

	public void setDriver18(String driver18) {
		this.driver18 = driver18;
	}

	public String getDriver19() {
		return driver19;
	}

	public void setDriver19(String driver19) {
		this.driver19 = driver19;
	}

	public String getDriver20() {
		return driver20;
	}

	public void setDriver20(String driver20) {
		this.driver20 = driver20;
	}

	public String getDriver21() {
		return driver21;
	}

	public void setDriver21(String driver21) {
		this.driver21 = driver21;
	}

	public String getDriver22() {
		return driver22;
	}

	public void setDriver22(String driver22) {
		this.driver22 = driver22;
	}

	public String getDriver23() {
		return driver23;
	}

	public void setDriver23(String driver23) {
		this.driver23 = driver23;
	}

	public String getDriver24() {
		return driver24;
	}

	public void setDriver24(String driver24) {
		this.driver24 = driver24;
	}

	public String getDriver25() {
		return driver25;
	}

	public void setDriver25(String driver25) {
		this.driver25 = driver25;
	}

	public String getDriver26() {
		return driver26;
	}

	public void setDriver26(String driver26) {
		this.driver26 = driver26;
	}

	public String getDriver27() {
		return driver27;
	}

	public void setDriver27(String driver27) {
		this.driver27 = driver27;
	}

	public String getDriver28() {
		return driver28;
	}

	public void setDriver28(String driver28) {
		this.driver28 = driver28;
	}

	public String getDriver29() {
		return driver29;
	}

	public void setDriver29(String driver29) {
		this.driver29 = driver29;
	}

	public String getDriver30() {
		return driver30;
	}

	public void setDriver30(String driver30) {
		this.driver30 = driver30;
	}

	public Long getBinaryWeight() {
		return binaryWeight;
	}

	public void setBinaryWeight(Long binaryWeight) {
		this.binaryWeight = binaryWeight;
	}

	public String getSignificantDigits() {
		return significantDigits;
	}

	public void setSignificantDigits(String significantDigits) {
		this.significantDigits = significantDigits;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFutureSplit() {
		return futureSplit;
	}

	public void setFutureSplit(String futureSplit) {
		this.futureSplit = futureSplit;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return taxAllocationMatrixId;
	}

	@Override
	public void setId(Long id) {
		this.taxAllocationMatrixId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "taxAllocationMatrixId";
	}

	public List<TaxAllocationMatrixDetail> getTaxAllocationMatrixDetails() {
		return taxAllocationMatrixDetails;
	}

	public void setTaxAllocationMatrixDetails(
			List<TaxAllocationMatrixDetail> taxAllocationMatrixDetails) {
		this.taxAllocationMatrixDetails = taxAllocationMatrixDetails;
	}
	
	public BigDecimal getTotalAllocationPercent() {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getAllocationPercent() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getAllocationPercent());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getTotalGlLineItmDistAmt() {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getGlLineItmDistAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getGlLineItmDistAmt());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getTotalInvoiceFreightAmt() {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getInvoiceFreightAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getInvoiceFreightAmt());	
				}					
			}
		}
		
		return sum;
	}
	
	public BigDecimal getTotalInvoiceDiscountAmt() {
		BigDecimal sum = ArithmeticUtils.ZERO;
		if (taxAllocationMatrixDetails != null) {
			for (TaxAllocationMatrixDetail detail : taxAllocationMatrixDetails) {
				if (detail.getInvoiceDiscountAmt() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getInvoiceDiscountAmt());	
				}					
			}
		}
		
		return sum;
	}
	
    // Matrix overrides
    @Override
    public Long getDriverCount() {
    	return DRIVER_COUNT;
    }
    
    @Override
    public String getDriverCode() {
    	return DRIVER_CODE;
    }

    @Override
    public boolean getHasDescriptions() {
    	return false;
    }

	public List<JurisdictionAllocationMatrixDetail> getJurisdictionAllocationMatrixDetails() {
		return jurisdictionAllocationMatrixDetails;
	}

	public void setJurisdictionAllocationMatrixDetails(
			List<JurisdictionAllocationMatrixDetail> jurisdictionAllocationMatrixDetails) {
		this.jurisdictionAllocationMatrixDetails = jurisdictionAllocationMatrixDetails;
	}
}
