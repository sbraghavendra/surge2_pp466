package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserEntityPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="USER_CODE")  	
	private String userCode; 
	   
	@Column(name="ENTITY_ID") 	
	private Long entityId;
	
	public UserEntityPK(){
		
	}
	
	public UserEntityPK(String userCode, Long entityId) {
		this.userCode = userCode;
		this.entityId = entityId;
	}
	
	@Column(name="userCode")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	@Column(name="entityId")
	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof UserEntityPK)) {
            return false;
        }

        final UserEntityPK revision = (UserEntityPK)other;

        String c1 = getUserCode();
        String c2 = revision.getUserCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        Long e1 = getEntityId();
        Long e2 = revision.getEntityId();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getUserCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
		Long e1 = getEntityId();
		hash = 31 * hash + (null == e1 ? 0 : e1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.userCode + "::" + this.entityId;
    }
	
}

