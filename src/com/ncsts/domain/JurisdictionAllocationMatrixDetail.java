package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Embeddable
public class JurisdictionAllocationMatrixDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="TAX_ALLOC_MATRIX_DTL_ID")
	private Long taxAllocationMatrixDetailId;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="JURISDICTION_ID", nullable = false)
	private Jurisdiction jurisdiction;
	
	@Column(name="ALLOCATION_PERCENT", nullable = false, scale = 7)
	private BigDecimal allocationPercent;
	
	@Transient
	private Long instanceCreatedId;
	
	@Transient
	private Long taxAllocationMatrixDetailInstanceId;
	
	private static long nextId = 0L;
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
	public JurisdictionAllocationMatrixDetail() {
		instanceCreatedId = new Long(getNextId());
	}

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public BigDecimal getAllocationPercent() {
		return allocationPercent;
	}

	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}

	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}

	public Long getTaxAllocationMatrixDetailId() {
		return taxAllocationMatrixDetailId;
	}

	public void setTaxAllocationMatrixDetailId(Long taxAllocationMatrixDetailId) {
		this.taxAllocationMatrixDetailId = taxAllocationMatrixDetailId;
	}

	public Long getTaxAllocationMatrixDetailInstanceId() {
		return taxAllocationMatrixDetailInstanceId;
	}

	public void setTaxAllocationMatrixDetailInstanceId(
			Long taxAllocationMatrixDetailInstanceId) {
		this.taxAllocationMatrixDetailInstanceId = taxAllocationMatrixDetailInstanceId;
	}
}
