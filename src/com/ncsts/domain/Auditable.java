package com.ncsts.domain;

import java.util.Date;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.http.HttpSession;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dto.UserDTO;
import com.ncsts.ws.PinPointWebServiceConstants;

@MappedSuperclass
@EntityListeners({AuditListener.class})
@XmlRootElement(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
public class Auditable {

	private static Logger logger = LoggerFactory.getInstance().getLogger(Auditable.class);	
	
	public static final int UPDATEUSERID_SIZE = 40;
	public static final int UPDATETIMESTAMP_SIZE = 19;
	
	@Column(name="UPDATE_USER_ID", unique=false, nullable=true, insertable=true, updatable=true, length=UPDATEUSERID_SIZE)
    private String updateUserId;
    
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="UPDATE_TIMESTAMP")
    private Date updateTimestamp;

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public static String currentUserCode() {
		String result = "";
	    HttpSession session = (FacesContext.getCurrentInstance() == null)? 
	    		null:(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    Object obj = (session == null)? null:session.getAttribute("user");
	    if (obj != null) {
	    	if (obj instanceof UserDTO) {
	            UserDTO user = (UserDTO) obj;
	            result = user.getUserCode();
	    	} else {
	    		logger.warn("Unexpected user object type found");
	    	}
	    } else {
			logger.warn("Could not get user from session");
	    }
	    
	    return result;
	}
}
