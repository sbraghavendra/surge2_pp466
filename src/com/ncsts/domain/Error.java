package com.ncsts.domain;

import java.util.Map;

public class Error {
	private Map<String, Object> errorMap;

	public Map<String, Object> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(Map<String, Object> errorMap) {
		this.errorMap = errorMap;
	}
}
