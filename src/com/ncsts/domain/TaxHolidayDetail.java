package com.ncsts.domain;

import javax.persistence.*;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.TaxHolidayDetailDTO;


@Entity
@Table(name="TB_TAX_HOLIDAY_DETAIL")
public class TaxHolidayDetail   implements Idable<TaxHolidayDetailPK> {

	private static final long serialVersionUID = 1318470152133640111L;



    @EmbeddedId
    private TaxHolidayDetailPK id;
    
    @Column(name="SORT_NO")	
    private Long sortNo;
    
    @Column(name="EXCEPT_IND")
    private String exceptInd;
    
    @Transient   
    private String description;  
    
    @Transient
	private boolean selected;
    
	@Transient	
	TaxHolidayDetailDTO taxHolidayDetailDTO;
	
	@Transient
	private String separator = "&#58;";
	
    public TaxHolidayDetail() { 
        this.selected = false;
        id = new TaxHolidayDetailPK();
    }
    
    // Idable interface
	public TaxHolidayDetailPK getId() {
		return id;
	}

    public void setId(TaxHolidayDetailPK id) {
    	this.id = id;
    }
    
	public String getIdPropertyName() {
    	return "taxHolidayCode";
    }
    
	public TaxHolidayDetailDTO getTaxHolidayDetailDTO() {
		TaxHolidayDetailDTO taxHolidayDetailDTO = new TaxHolidayDetailDTO();
		BeanUtils.copyProperties(this, taxHolidayDetailDTO);
		return taxHolidayDetailDTO;
	}

    public Long getSortNo() {
        return this.sortNo;
    }
    
    public void setSortNo(Long sortNo) {
    	this.sortNo = sortNo;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    } 
    
    public Boolean getSelected() {
		return this.selected;
	}
	
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public String getExceptInd() {
		return exceptInd;
	}

	public void setExceptInd(String exceptInd) {
		this.exceptInd= exceptInd;
	}

	public Boolean getExceptBooleanInd() {
		return (exceptInd == null)? false : exceptInd.equals("1");
	}

 
    public String getTaxHolidayCode() {
        return id.getTaxHolidayCode();
    }

    public String getTaxcodeCode() {
        return id.getTaxcodeCode();
    }

    public String getTaxcodeCounty() {
        return id.getTaxcodeCounty();
    }

    public String getTaxcodeCity() {
        return id.getTaxcodeCity();
    }

    public String getTaxcodeStj() {
        return id.getTaxcodeStj();
    }
    
    public String getJurLevel() {
        return id.getJurLevel();
    }

    public void setTaxHolidayCode(String taxHolidayCode) {
        id.setTaxHolidayCode(taxHolidayCode);
    }

    public void setTaxcodeCode(String taxcodeCode) {
        id.setTaxcodeCode(taxcodeCode);
    }

    public void setTaxcodeCounty(String taxcodeCounty) {
        id.setTaxcodeCounty(taxcodeCounty);
    }

    public void setTaxcodeCity(String taxcodeCity) {
        id.setTaxcodeCity(taxcodeCity);
    }

    public void setTaxcodeStj(String taxcodeStj) {
        id.setTaxcodeStj(taxcodeStj);
    }

    public void setJurLevel(String jurLevel) {
        id.setJurLevel(jurLevel);
    }
    
    public String getKey(){
    	return (getTaxcodeCode()+separator+getTaxcodeCounty()+separator+(getTaxcodeCity() + "").toUpperCase()
    			+separator+(getTaxcodeStj() + "").toUpperCase()  +separator+(getJurLevel() + "").toUpperCase() );
    }
    
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxHolidayDetail)) {
            return false;
        }

        if (!id.equals(((TaxHolidayDetail) other).getId()))
            return false;

        return true;
    }
	

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.getId() + "::" + this.description;
    }
}



