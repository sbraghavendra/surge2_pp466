package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.MenuDTO;

/**
 * @author Paul Govindan
 *
 */

@Entity
@Table(name="TB_MENU")
public class Menu extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="MENU_CODE")	
    private String menuCode;
    
    @Column(name="MAIN_MENU_CODE")    
    private String mainMenuCode;  
    
    @Column(name="MENU_SEQUENCE")    
    private Long menuSequence;  
    
    @Column(name="OPTION_NAME")    
    private String optionName;  
    
    @Column(name="COMMAND_TYPE")    
    private String commandType;    
    
    @Column(name="COMMAND_LINE")    
    private String commandLine;     
    
    @Column(name="KEY_VALUES")    
    private String keyValues; 
    
    @Column(name="PANEL_CLASS_NAME")    
    private String panelClassName;   
    
    //Midtier project code added - january 2009
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "MAIN_MENU_CODE", referencedColumnName = "MENU_CODE",insertable=false, updatable=false )
    private MainMenu mainMenu;
    
    @OneToMany(cascade = {CascadeType.REMOVE},mappedBy  = "menu")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<UserMenuEx> userMenuEx = new HashSet<UserMenuEx>();
    
    @OneToMany(mappedBy="menu")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<RoleMenu> roleMenu = new HashSet<RoleMenu>();
    
	@Transient	
	MenuDTO menuDTO;
	
    public Menu() { 
    	
    }
    
    public Menu(String menuCode,String optionName) { 
    	setMenuCode(menuCode);
    	setOptionName(optionName);
    }
	
	public MenuDTO getMenuDTO() {
		MenuDTO menuDTO = new MenuDTO();
		BeanUtils.copyProperties(this, menuDTO);
		return menuDTO;
	}
    
    public String getMenuCode() {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode) {
    	this.menuCode = menuCode;
    }
    
    public String getMainMenuCode() {
        return this.mainMenuCode;
    }
    
    public void setMainMenuCode(String mainMenuCode) {
    	this.mainMenuCode = mainMenuCode;
    }    
    
    public Long getMenuSequence() {
        return this.menuSequence;
    }
    
    public void setMenuSequence(Long menuSequence) {
    	this.menuSequence = menuSequence;
    }      
    
    public String getOptionName() {
        return this.optionName;
    }
    
    public void setOptionName(String optionName) {
    	this.optionName = optionName;
    }    
    
    public String getCommandType() {
        return this.commandType;
    }
    
    public void setCommandType(String commandType) {
    	this.commandType = commandType;
    }    
    
    public String getCommandLine() {
        return this.commandLine;
    }
    
    public void setCommandLine(String commandLine) {
    	this.commandLine = commandLine;
    }    
    
    public String getKeyValues() {
        return this.keyValues;
    }
    
    public void setKeyValues(String keyValues) {
    	this.keyValues = keyValues;
    }     
    
    public String getPanelClassName() {
        return this.panelClassName;
    }
    
    public void setPanelClassName(String panelClassName) {
    	this.panelClassName = panelClassName;
    } 
    
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof Menu)) {
            return false;
        }

        final Menu revision = (Menu)other;

        String c1 = getMenuCode();
        String c2 = revision.getMenuCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getMenuCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.menuCode + "::" + this.optionName;
    }

	public MainMenu getMainMenu() {
		return mainMenu;
	}

	public void setMainMenu(MainMenu mainMenu) {
		this.mainMenu = mainMenu;
	}

	public Set<UserMenuEx> getUserMenuEx() {
		return userMenuEx;
	}

	public void setUserMenuEx(Set<UserMenuEx> userMenuEx) {
		this.userMenuEx = userMenuEx;
	}

	public Set<RoleMenu> getRoleMenu() {
		return roleMenu;
	}

	public void setRoleMenu(Set<RoleMenu> roleMenu) {
		this.roleMenu = roleMenu;
	}
}



