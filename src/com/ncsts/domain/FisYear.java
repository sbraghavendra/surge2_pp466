package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;

import java.util.Date;



/**
 * The persistent class for the TB_FISCAL_YEAR database table.
 * 
 */
@Entity
@Table(name="TB_FISCAL_YEAR")
public class FisYear extends Auditable implements Serializable, Idable<String>  {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getInstance().getLogger(Auditable.class);	
	
    @Id
	@Column(name="YEAR_PERIOD",unique=true)
  	private String yearPeriod;
	
	@Column(name="FISCAL_YEAR")
	private Long fiscalYear;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="CLOSE_FLAG")
	private String closeFlag;
	
	@Transient
    private boolean selectedYear;

	@Transient
    private boolean selectedPeriod;
	
	@Transient
    private String firstMonth;
	
	@Transient
    private String firstYear;
	
	@Transient
    private boolean checkCount;
	

	public FisYear() {
		 this.selectedYear = false;
		 this.selectedPeriod=false;
		 this.checkCount=false;
	}

	// Idable interface
 	public String getId() {
 		return getYearPeriod();
 	}
 	
    public void setId(String id) {
    	setYearPeriod(id);
    }
    
    public String getIdPropertyName() {
		return "yearPeriod";
	}
	
	public String getYearPeriod() {
		return yearPeriod;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setYearPeriod(String yearPeriod) {
		this.yearPeriod = yearPeriod;
	}

	public Long getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(Long fiscalYear) {
		this.fiscalYear = fiscalYear;
		// Empty values get converted to 0, change back to null
				if ((fiscalYear != null) && (fiscalYear <= 0L)) {
					fiscalYear = null;
				}
				this.fiscalYear = fiscalYear;
				
	}


	
	public String getCloseFlag() {
		return closeFlag;
	}

	public void setCloseFlag(String closeFlag) {
		this.closeFlag = closeFlag;
	}

	public Boolean getBooleanCloseFlag() {
    	return "1".equals(this.closeFlag);
	}

	public void setCloseBooleanFlag(Boolean closeFlag) {
		this.closeFlag = closeFlag == Boolean.TRUE ? "1" : "0";
	}
	public boolean getSelectedYear() {
		return this.selectedYear;
	}

	public boolean getSelectedPeriod () {
		return this.selectedPeriod;
	} 
	public void setSelectedYear(boolean selectedYear) {
		this.selectedYear = selectedYear;
	}

	public void setSelectedPeriod(boolean selectedPeriod) {
		this.selectedPeriod = selectedPeriod;
	}
  
	public String getFirstMonth() {
		return firstMonth;
	}

	public void setFirstMonth(String firstMonth) {
		this.firstMonth = firstMonth;
	}
	
	public String getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	public boolean isCheckCount() {
		return checkCount;
	}

	public void setCheckCount(boolean checkCount) {
		this.checkCount = checkCount;
	}
	
	
}