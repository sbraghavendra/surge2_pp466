package com.ncsts.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

/**
 * 
 * @author Anand
 *
 */
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_JURISDICTION")
public class Jurisdiction extends Auditable implements Serializable, Idable<Long> {

	private static final long serialVersionUID = 1L;

	public static final String NEW_JURISDICTION_ID_STRING = "*New";
	
	public static final int JURISDICTIONID_SIZE = 31;
	public static final int GEOCODE_SIZE = 12;
	public static final int STATE_SIZE = 10;
	public static final int COUNTY_SIZE = 40;
	public static final int CITY_SIZE = 40;
	public static final int ZIP_SIZE = 5;
	public static final int ZIPPLUS4_SIZE = 4;
	public static final int INOUT_SIZE = 1;
	public static final int CUSTOMFLAG_SIZE = 1;
	public static final int DESCRIPTION_SIZE = 50;
	public static final int CLIENTGEOCODE_SIZE = 12;
	public static final int COMPGEOCODE_SIZE = 12;
	public static final int UPDATEUSERID_SIZE = 40;
	public static final int UPDATETIMESTAMP_SIZE = 19;
	public static final int REPORTINGCITY_SIZE = 50;
	public static final int REPORTINGCITYFIPS_SIZE = 5;
	public static final int STJ1NAME_SIZE = 50;
	public static final int STJ2NAME_SIZE = 50;
	public static final int STJ3NAME_SIZE = 50;
	public static final int STJ4NAME_SIZE = 50;
	public static final int STJ5NAME_SIZE = 50;
	public static final int MODIFIED_FLAG_SIZE = 1;
	public static final int EFFECTIVEDATE_SIZE = 10;
	public static final int EXPIRATIONDATE_SIZE = 10;
	public static final int COUNTRYNEXUSINDCODE_SIZE = 10;
	public static final int STATENEXUSINDCODE_SIZE = 10;
	public static final int COUNTYNEXUSINDCODE_SIZE = 10;
	public static final int CITYNEXUSINDCODE_SIZE = 10;
	public static final int STJ1NEXUSINDCODE_SIZE = 10;
	public static final int STJ2NEXUSINDCODE_SIZE = 10;
	public static final int STJ3NEXUSINDCODE_SIZE = 10;
	public static final int STJ4NEXUSINDCODE_SIZE = 10;
	public static final int STJ5NEXUSINDCODE_SIZE = 10;
	public static final int STJ1LOCALCODE_SIZE = 10;
	public static final int STJ2LOCALCODE_SIZE = 10;
	public static final int STJ3LOCALCODE_SIZE = 10;
	public static final int STJ4LOCALCODE_SIZE = 10;
	public static final int STJ5LOCALCODE_SIZE = 10;

	// Fields  
	@Id
    @Column(name="JURISDICTION_ID")
	@GeneratedValue (strategy = GenerationType.AUTO , generator="jurisdiction_sequence")
	@SequenceGenerator(name="jurisdiction_sequence" , allocationSize = 1, sequenceName="sq_tb_jurisdiction_id")	
    private Long jurisdictionId;
	
	@Column(name="GEOCODE")
	private String geocode;
	
	@Column(name="STATE")
    private String state;
	
	@Column(name="COUNTY", unique=false, nullable=true, insertable=true, updatable=true, length=40)
    private String county;
	
	@Column(name="CITY", unique=false, nullable=true, insertable=true, updatable=true, length=40)
    private String city;
	
	@Column(name="REPORTING_CITY")
	private String reportingCity;
	
	@Column(name="REPORTING_CITY_FIPS")
	private String reportingCityFips;
	
	@Column(name="STJ1_NAME")
	private String stj1Name;
	
	@Column(name="STJ2_NAME")
	private String stj2Name;
	
	@Column(name="STJ3_NAME")
	private String stj3Name;
	
	@Column(name="STJ4_NAME")
	private String stj4Name;
	
	@Column(name="STJ5_NAME")
	private String stj5Name;
	
	@Column(name="ZIP", unique=false, nullable=true, insertable=true, updatable=true, length=5)
    private String zip;
	
	@Column(name="ZIPPLUS4", unique=false, nullable=true, insertable=true, updatable=true, length=4)
    private String zipplus4;
	
	@Column(name="IN_OUT", unique=false, nullable=true, insertable=true, updatable=true, length=1)
    private String inOut;
	
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;
	
	@Column(name="CUSTOM_FLAG", unique=false, nullable=true, insertable=true, updatable=true, length=1)
    private String customFlag;
	
	@Column(name="MODIFIED_FLAG")
	private String modifiedFlag;
	
	@Column(name="DESCRIPTION", unique=false, nullable=true, insertable=true, updatable=true, length=50)
    private String description;
	
	@Column(name="CLIENT_GEOCODE", unique=false, nullable=true, insertable=true, updatable=true, length=12)
    private String clientGeocode;
	
	@Column(name="COMP_GEOCODE", unique=false, nullable=true, insertable=true, updatable=true, length=12)
    private String compGeocode;
	
	@Column(name="COUNTRY_NEXUSIND_CODE")
	private String countryNexusindCode;
	
	@Column(name="STATE_NEXUSIND_CODE")
	private String stateNexusindCode;
	
	@Column(name="COUNTY_NEXUSIND_CODE")
	private String countyNexusindCode;
	
	@Column(name="CITY_NEXUSIND_CODE")
	private String cityNexusindCode;
	
	@Column(name="STJ1_NEXUSIND_CODE")
	private String stj1NexusindCode;
	
	@Column(name="STJ2_NEXUSIND_CODE")
	private String stj2NexusindCode;
	
	@Column(name="STJ3_NEXUSIND_CODE")
	private String stj3NexusindCode;
	
	@Column(name="STJ4_NEXUSIND_CODE")
	private String stj4NexusindCode;
	
	@Column(name="STJ5_NEXUSIND_CODE")
	private String stj5NexusindCode;

	@Column(name="STJ1_LOCAL_CODE")
	private String stj1LocalCode;
	
	@Column(name="STJ2_LOCAL_CODE")
	private String stj2LocalCode;
	
	@Column(name="STJ3_LOCAL_CODE")
	private String stj3LocalCode;
	
	@Column(name="STJ4_LOCAL_CODE")
	private String stj4LocalCode;
	
	@Column(name="STJ5_LOCAL_CODE")
	private String stj5LocalCode;
	
	@Column(name="COUNTRY", unique=false, nullable=true, insertable=true, updatable=true, length=10)
    private String country;
	
	//Midtier project code added - january 2009
	@OneToMany(mappedBy="jurisdiction", fetch=FetchType.LAZY)
	//@Cascade(CascadeType.DELETE_ORPHAN)
	@OrderBy("jurisdictionTaxrateId")
	private List<JurisdictionTaxrate> jursidictionTaxrates = new ArrayList<JurisdictionTaxrate>();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns( {
		@JoinColumn(name="STATE", referencedColumnName="TAXCODE_STATE_CODE", insertable=false, updatable=false),
		@JoinColumn(name="COUNTRY", referencedColumnName="TAXCODE_COUNTRY_CODE", insertable=false, updatable=false) } )
		@ForeignKey(name="tb_jurisdiction_fk01")
	private TaxCodeState taxCodeState;
	
	@OneToMany(mappedBy="jurisdiction")
	private Set<LocationMatrix> locationMatrix = new HashSet<LocationMatrix>();
	
	@OneToMany(mappedBy="jurisdiction")
	private Set<TransactionDetail> transactionDetail = new HashSet<TransactionDetail>();
	
	// Transient properties
	@Transient
	private String jurisdictionIdString;
	
	@Transient
	private boolean autoWildSearch = false;

	@Transient
	private String notes;
	
	@Transient
	private boolean copyRatesFlag;  
	
	public boolean getCopyRatesFlag() {
		return copyRatesFlag;
	}
	
	public void setCopyRatesFlag(boolean copyRatesFlag) {
		this.copyRatesFlag = copyRatesFlag;
	}

	public boolean getAutoWildSearch() {
		return this.autoWildSearch;
	}
	public void setAutoWildSearch(boolean autoWildSearch) {
		this.autoWildSearch = autoWildSearch;
	}

	// Default constructor
	public Jurisdiction() {
		// Do nothing
	}

	// Minimal constructor
	public Jurisdiction(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	// Copy constructor
	public Jurisdiction(Jurisdiction jurisdiction) {
		if (jurisdiction != null) {
			BeanUtils.copyProperties(jurisdiction, this);
		}
	}
	// Idable interface
	public Long getId() {
		return getJurisdictionId();
	}
	
    public void setId(Long id) {
    	setJurisdictionId(id);
    }
    
	public String getIdPropertyName() {
		return "jurisdictionId";
	}

	public Long getJurisdictionId() {
        return this.jurisdictionId;
    }
    
    public void setJurisdictionId(Long jurisdictionId) {
		// Empty values get converted to 0, change back to null
		if ((jurisdictionId != null) && (jurisdictionId <= 0L)) {
			jurisdictionId = null;
		}
		this.jurisdictionId = jurisdictionId;
		this.jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
    }

	public String getJurisdictionIdString() {
		return jurisdictionIdString;
	}

	public void setJurisdictionIdString(String jurisdictionIdString) {
		if (StringUtils.isBlank(jurisdictionIdString)) {
			jurisdictionIdString = null;
		} else {
			jurisdictionIdString = jurisdictionIdString.trim();
		}
		Long l;
		try {
			l = new Long(jurisdictionIdString);
		} catch (Exception e) {
			l = null;
		}
		this.jurisdictionId = l;
		this.jurisdictionIdString = jurisdictionIdString;
	}

	public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
        this.geocode = geocode;
    }
    
    

    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    

    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
        this.county = county;
    }
    
    

    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    

    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
        this.zip = zip;
    }
    
    

    public String getZipplus4() {
        return this.zipplus4;
    }
    
    public void setZipplus4(String zipplus4) {
        this.zipplus4 = zipplus4;
    }
    
    

    public String getInOut() {
        return this.inOut;
    }
    
    public void setInOut(String inOut) {
        this.inOut = inOut;
    }
    
    

    public String getCustomFlag() {
        return this.customFlag;
    }
    
    public void setCustomFlag(String customFlag) {
        this.customFlag = customFlag;
    }


    public Boolean getCustomFlagBoolean() {
		return (customFlag == null)? null : customFlag.equals("1");
	}

	public void setCustomFlagBoolean(Boolean customFlag) {
		setCustomFlag((customFlag == null) ?  null : ((customFlag) ? "1" : "0"));
	}

	
	public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    

    public String getClientGeocode() {
        return this.clientGeocode;
    }
    
    public void setClientGeocode(String clientGeocode) {
        this.clientGeocode = clientGeocode;
    }
    
    

    public String getCompGeocode() {
        return this.compGeocode;
    }
    
    public void setCompGeocode(String compGeocode) {
        this.compGeocode = compGeocode;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public List<JurisdictionTaxrate> getJurisdictionTaxrates() {
    	return jursidictionTaxrates;
    }
    
    public void getJurisdictionTaxrates(List<JurisdictionTaxrate> jursidictionTaxrates) {
    	this.jursidictionTaxrates = jursidictionTaxrates;
    }

	public TaxCodeState getTaxCodeState() {
		return taxCodeState;
	}

	public void setTaxCodeState(TaxCodeState taxCodeState) {
		this.taxCodeState = taxCodeState;
	}

	public Set<LocationMatrix> getLocationMatrix() {
		return locationMatrix;
	}

	public void setLocationMatrix(Set<LocationMatrix> locationMatrix) {
		this.locationMatrix = locationMatrix;
	}

	public Set<TransactionDetail> getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(Set<TransactionDetail> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getReportingCity() {
		return reportingCity;
	}

	public void setReportingCity(String reportingCity) {
		this.reportingCity = reportingCity;
	}

	public String getReportingCityFips() {
		return reportingCityFips;
	}

	public void setReportingCityFips(String reportingCityFips) {
		this.reportingCityFips = reportingCityFips;
	}

	public String getStj1Name() {
		return stj1Name;
	}

	public void setStj1Name(String stj1Name) {
		this.stj1Name = stj1Name;
	}

	public String getStj2Name() {
		return stj2Name;
	}

	public void setStj2Name(String stj2Name) {
		this.stj2Name = stj2Name;
	}

	public String getStj3Name() {
		return stj3Name;
	}

	public void setStj3Name(String stj3Name) {
		this.stj3Name = stj3Name;
	}

	public String getStj4Name() {
		return stj4Name;
	}

	public void setStj4Name(String stj4Name) {
		this.stj4Name = stj4Name;
	}

	public String getStj5Name() {
		return stj5Name;
	}

	public void setStj5Name(String stj5Name) {
		this.stj5Name = stj5Name;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}

	public String getCountryNexusindCode() {
		return countryNexusindCode;
	}

	public void setCountryNexusindCode(String countryNexusindCode) {
		this.countryNexusindCode = countryNexusindCode;
	}

	public String getStateNexusindCode() {
		return stateNexusindCode;
	}

	public void setStateNexusindCode(String stateNexusindCode) {
		this.stateNexusindCode = stateNexusindCode;
	}

	public String getCountyNexusindCode() {
		return countyNexusindCode;
	}

	public void setCountyNexusindCode(String countyNexusindCode) {
		this.countyNexusindCode = countyNexusindCode;
	}

	public String getCityNexusindCode() {
		return cityNexusindCode;
	}

	public void setCityNexusindCode(String cityNexusindCode) {
		this.cityNexusindCode = cityNexusindCode;
	}

	public String getStj1NexusindCode() {
		return stj1NexusindCode;
	}

	public void setStj1NexusindCode(String stj1NexusindCode) {
		this.stj1NexusindCode = stj1NexusindCode;
	}

	public String getStj2NexusindCode() {
		return stj2NexusindCode;
	}

	public void setStj2NexusindCode(String stj2NexusindCode) {
		this.stj2NexusindCode = stj2NexusindCode;
	}

	public String getStj3NexusindCode() {
		return stj3NexusindCode;
	}

	public void setStj3NexusindCode(String stj3NexusindCode) {
		this.stj3NexusindCode = stj3NexusindCode;
	}

	public String getStj4NexusindCode() {
		return stj4NexusindCode;
	}

	public void setStj4NexusindCode(String stj4NexusindCode) {
		this.stj4NexusindCode = stj4NexusindCode;
	}

	public String getStj5NexusindCode() {
		return stj5NexusindCode;
	}

	public void setStj5NexusindCode(String stj5NexusindCode) {
		this.stj5NexusindCode = stj5NexusindCode;
	}
	
	public String getStj1LocalCode() {
		return stj1LocalCode;
	}

	public void setStj1LocalCode(String stj1LocalCode) {
		this.stj1LocalCode = stj1LocalCode;
	}

	public String getStj2LocalCode() {
		return stj2LocalCode;
	}

	public void setStj2LocalCode(String stj2LocalCode) {
		this.stj2LocalCode = stj2LocalCode;
	}

	public String getStj3LocalCode() {
		return stj3LocalCode;
	}

	public void setStj3LocalCode(String stj3LocalCode) {
		this.stj3LocalCode = stj3LocalCode;
	}

	public String getStj4LocalCode() {
		return stj4LocalCode;
	}

	public void setStj4LocalCode(String stj4LocalCode) {
		this.stj4LocalCode = stj4LocalCode;
	}

	public String getStj5LocalCode() {
		return stj5LocalCode;
	}

	public void setStj5LocalCode(String stj5LocalCode) {
		this.stj5LocalCode = stj5LocalCode;
	}
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
