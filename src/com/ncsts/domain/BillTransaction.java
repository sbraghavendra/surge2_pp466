package com.ncsts.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.TransactionDetailDTO;
import com.ncsts.dto.TransientPurchaseTransaction;
import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.service.impl.adapter.JaxbDateAdapter;

@Entity
@Table(name = "TB_BILLTRANS" )
@SecondaryTables({
        @SecondaryTable(name="TB_BILLTRANS_USER", pkJoinColumns= { @PrimaryKeyJoinColumn(name="BILLTRANS_USER_ID",referencedColumnName="BILLTRANS_ID")}),
        @SecondaryTable(name="TB_BILLTRANS_JURDTL", pkJoinColumns= { @PrimaryKeyJoinColumn(name="BILLTRANS_JURDTL_ID",referencedColumnName="BILLTRANS_ID")}),
        @SecondaryTable(name="TB_BILLTRANS_LOCN", pkJoinColumns= { @PrimaryKeyJoinColumn(name="BILLTRANS_LOCN_ID",referencedColumnName="BILLTRANS_ID")})
})
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BillTransaction extends Auditable implements Serializable, Idable<Long>  {

    @Transient
    private TransientPurchaseTransaction transientTransaction = new TransientPurchaseTransaction();

    @Transient
    private String errorCode;                      // the error code for the transaction
    @Transient
    private String errorText;                      // the error description for the transaction
    @Transient
    private Boolean abortFlag;                     // indicates if Processing is to be aborted

    @Id
    @Column(name = "BILLTRANS_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="billtrans_sequence")
    @GenericGenerator(name="billtrans_sequence", strategy="com.ncsts.dao.sequence.BillTransactionSequenceGenerator", parameters = {@Parameter(name = "sequence", value = "SQ_TB_BILLTRANS_ID")})
    private Long billtransId;

    public Long getBilltransId() {
        return billtransId;
    }

    public void setBilltransId(Long billtransId) {
        this.billtransId = billtransId;
    }
    
    public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
    
    private static long nextId = 0L;

    public static Map<String, String> getTransactionColumnName() {
		Class<?> cls = BillTransaction.class;
		Map<String, String> result = new TreeMap<String, String>();
		Field [] fields = cls.getDeclaredFields();
		try{
			for (Field f : fields) {
				Column col = f.getAnnotation(Column.class);
				if (col != null) {		
					result.put(col.name(), f.getName());
				}
			}
		}
		catch(Exception e){	
		}

		return result;
	}

    public Long getId() {
    	return billtransId == null ? instanceCreatedId : billtransId;
    }

    public void setId(Long id) {
        this.billtransId = id;
    }
    
    public TransientPurchaseTransaction getTransientTransaction() {
        return transientTransaction;
    }

    public void setTransientTransaction(TransientPurchaseTransaction transientTransaction) {
        this.transientTransaction = transientTransaction;
    }

    public String getIdPropertyName() {
        return "billtransId";
    }

    public Boolean getAbortFlag() {
        return abortFlag;
    }

    public void setAbortFlag(Boolean abortFlag) {
        this.abortFlag = abortFlag;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
    
    public BillTransaction() {
		instanceCreatedId = new Long(getNextId());
    }
    
    //Start of TB_BILLTRANS
    @Basic
    @Column(name = "COUNTY_TIER3_TAX_AMT")
    private BigDecimal countyTier3TaxAmt;

    public void setCountyTier3TaxAmt(BigDecimal countyTier3TaxAmt){
        this.countyTier3TaxAmt = countyTier3TaxAmt;
    }
    public BigDecimal getCountyTier3TaxAmt(){
        return countyTier3TaxAmt;
    }

    @Basic
    @Column(name = "AUTO_TRANSACTION_COUNTRY_CODE")
    private String autoTransactionCountryCode;

    public void setAutoTransactionCountryCode(String autoTransactionCountryCode){
        this.autoTransactionCountryCode = autoTransactionCountryCode;
    }
    public String getAutoTransactionCountryCode(){
        return autoTransactionCountryCode;
    }

    @Basic
    @Column(name = "INVOICE_GROSS_AMT")
    private BigDecimal invoiceGrossAmt;

    public void setInvoiceGrossAmt(BigDecimal invoiceGrossAmt){
        this.invoiceGrossAmt = invoiceGrossAmt;
    }
    public BigDecimal getInvoiceGrossAmt(){
        return invoiceGrossAmt;
    }

    @Basic
    @Column(name = "DISTR_04_AMT")
    private BigDecimal distr04Amt;

    public void setDistr04Amt(BigDecimal distr04Amt){
        this.distr04Amt = distr04Amt;
    }
    public BigDecimal getDistr04Amt(){
        return distr04Amt;
    }

    @Basic
    @Column(name = "EXEMPT_REASON")
    private String exemptReason;

    public void setExemptReason(String exemptReason){
        this.exemptReason = exemptReason;
    }
    public String getExemptReason(){
        return exemptReason;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "AUDIT_TIMESTAMP")
    private Date auditTimestamp;

    public void setAuditTimestamp(Date auditTimestamp){
        this.auditTimestamp = auditTimestamp;
    }
    public Date getAuditTimestamp(){
        return auditTimestamp;
    }

    @Basic
    @Column(name = "GL_DIVISION_NAME")
    private String glDivisionName;

    public void setGlDivisionName(String glDivisionName){
        this.glDivisionName = glDivisionName;
    }
    public String getGlDivisionName(){
        return glDivisionName;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "INVOICE_DATE")
    private Date invoiceDate;

    public void setInvoiceDate(Date invoiceDate){
        this.invoiceDate = invoiceDate;
    }
    public Date getInvoiceDate(){
        return invoiceDate;
    }

    @Basic
    @Column(name = "INVOICE_LINE_COUNT")
    private BigDecimal invoiceLineCount;

    public void setInvoiceLineCount(BigDecimal invoiceLineCount){
        this.invoiceLineCount = invoiceLineCount;
    }
    public BigDecimal getInvoiceLineCount(){
        return invoiceLineCount;
    }

    @Basic
    @Column(name = "STJ2_EXEMPT_AMT")
    private BigDecimal stj2ExemptAmt;

    public void setStj2ExemptAmt(BigDecimal stj2ExemptAmt){
        this.stj2ExemptAmt = stj2ExemptAmt;
    }
    public BigDecimal getStj2ExemptAmt(){
        return stj2ExemptAmt;
    }

    @Basic
    @Column(name = "STJ5_TAX_AMT")
    private BigDecimal stj5TaxAmt;

    public void setStj5TaxAmt(BigDecimal stj5TaxAmt){
        this.stj5TaxAmt = stj5TaxAmt;
    }
    public BigDecimal getStj5TaxAmt(){
        return stj5TaxAmt;
    }

    @Basic
    @Column(name = "RELATED_SUBTRANS_ID")
    private Long relatedSubtransId;

    public void setRelatedSubtransId(Long relatedSubtransId){
        this.relatedSubtransId = relatedSubtransId;
    }
    public Long getRelatedSubtransId(){
        return relatedSubtransId;
    }

    @Basic
    @Column(name = "CUST_COUNTRY_CODE")
    private String custCountryCode;

    public void setCustCountryCode(String custCountryCode){
        this.custCountryCode = custCountryCode;
    }
    public String getCustCountryCode(){
        return custCountryCode;
    }

    @Basic
    @Column(name = "GL_EXTRACT_UPDATER")
    private String glExtractUpdater;

    public void setGlExtractUpdater(String glExtractUpdater){
        this.glExtractUpdater = glExtractUpdater;
    }
    public String getGlExtractUpdater(){
        return glExtractUpdater;
    }

    @Basic
    @Column(name = "CREDIT_IND")
    private String creditInd;

    public void setCreditInd(String creditInd){
        this.creditInd = creditInd;
    }
    public String getCreditInd(){
        return creditInd;
    }

    @Basic
    @Column(name = "CUST_COUNTY")
    private String custCounty;

    public void setCustCounty(String custCounty){
        this.custCounty = custCounty;
    }
    public String getCustCounty(){
        return custCounty;
    }

    @Basic
    @Column(name = "AFE_PROJECT_NBR")
    private String afeProjectNbr;

    public void setAfeProjectNbr(String afeProjectNbr){
        this.afeProjectNbr = afeProjectNbr;
    }
    public String getAfeProjectNbr(){
        return afeProjectNbr;
    }

    @Basic
    @Column(name = "INVOICE_TAX_AMT")
    private BigDecimal invoiceTaxAmt;

    public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt){
        this.invoiceTaxAmt = invoiceTaxAmt;
    }
    public BigDecimal getInvoiceTaxAmt(){
        return invoiceTaxAmt;
    }

    @Basic
    @Column(name = "EXCLUSION_AMT")
    private BigDecimal exclusionAmt;

    public void setExclusionAmt(BigDecimal exclusionAmt){
        this.exclusionAmt = exclusionAmt;
    }
    public BigDecimal getExclusionAmt(){
        return exclusionAmt;
    }

    @Basic
    @Column(name = "TRANSACTION_STATE_CODE")
    private String transactionStateCode;

    public void setTransactionStateCode(String transactionStateCode){
        this.transactionStateCode = transactionStateCode;
    }
    public String getTransactionStateCode(){
        return transactionStateCode;
    }

    @Basic
    @Column(name = "TB_CALC_TAX_AMT")
    private BigDecimal tbCalcTaxAmt;

    public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt){
        this.tbCalcTaxAmt = tbCalcTaxAmt;
    }
    public BigDecimal getTbCalcTaxAmt(){
        return tbCalcTaxAmt;
    }

    @Basic
    @Column(name = "ALLOCATION_MATRIX_ID")
    private Long allocationMatrixId;

    public void setAllocationMatrixId(Long allocationMatrixId){
        this.allocationMatrixId = allocationMatrixId;
    }
    public Long getAllocationMatrixId(){
        return allocationMatrixId;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "GL_DATE")
    private Date glDate;

    public void setGlDate(Date glDate){
        this.glDate = glDate;
    }
    public Date getGlDate(){
        return glDate;
    }

    @Basic
    @Column(name = "STJ8_EXEMPT_AMT")
    private BigDecimal stj8ExemptAmt;

    public void setStj8ExemptAmt(BigDecimal stj8ExemptAmt){
        this.stj8ExemptAmt = stj8ExemptAmt;
    }
    public BigDecimal getStj8ExemptAmt(){
        return stj8ExemptAmt;
    }

    @Basic
    @Column(name = "CUST_ZIPPLUS4")
    private String custZipplus4;

    public void setCustZipplus4(String custZipplus4){
        this.custZipplus4 = custZipplus4;
    }
    public String getCustZipplus4(){
        return custZipplus4;
    }

    @Basic
    @Column(name = "INVOICE_MISC_AMT")
    private BigDecimal invoiceMiscAmt;

    public void setInvoiceMiscAmt(BigDecimal invoiceMiscAmt){
        this.invoiceMiscAmt = invoiceMiscAmt;
    }
    public BigDecimal getInvoiceMiscAmt(){
        return invoiceMiscAmt;
    }

    @Basic
    @Column(name = "CERT_NBR")
    private String certNbr;

    public void setCertNbr(String certNbr){
        this.certNbr = certNbr;
    }
    public String getCertNbr(){
        return certNbr;
    }

    @Basic
    @Column(name = "CITY_TIER2_TAX_AMT")
    private BigDecimal cityTier2TaxAmt;

    public void setCityTier2TaxAmt(BigDecimal cityTier2TaxAmt){
        this.cityTier2TaxAmt = cityTier2TaxAmt;
    }
    public BigDecimal getCityTier2TaxAmt(){
        return cityTier2TaxAmt;
    }

    @Basic
    @Column(name = "AFE_CONTRACT_STRUCTURE")
    private String afeContractStructure;

    public void setAfeContractStructure(String afeContractStructure){
        this.afeContractStructure = afeContractStructure;
    }
    public String getAfeContractStructure(){
        return afeContractStructure;
    }

    @Basic
    @Column(name = "STATE_EXEMPT_AMT")
    private BigDecimal stateExemptAmt;

    public void setStateExemptAmt(BigDecimal stateExemptAmt){
        this.stateExemptAmt = stateExemptAmt;
    }
    public BigDecimal getStateExemptAmt(){
        return stateExemptAmt;
    }

    @Basic
    @Column(name = "GEO_RECON_CODE")
    private String geoReconCode;

    public void setGeoReconCode(String geoReconCode){
        this.geoReconCode = geoReconCode;
    }
    public String getGeoReconCode(){
        return geoReconCode;
    }

    @Basic
    @Column(name = "CITY_EXEMPT_AMT")
    private BigDecimal cityExemptAmt;

    public void setCityExemptAmt(BigDecimal cityExemptAmt){
        this.cityExemptAmt = cityExemptAmt;
    }
    public BigDecimal getCityExemptAmt(){
        return cityExemptAmt;
    }

    @Basic
    @Column(name = "INVOICE_DISCOUNT_AMT")
    private BigDecimal invoiceDiscountAmt;

    public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt){
        this.invoiceDiscountAmt = invoiceDiscountAmt;
    }
    public BigDecimal getInvoiceDiscountAmt(){
        return invoiceDiscountAmt;
    }

    @Basic
    @Column(name = "DISTR_08_AMT")
    private BigDecimal distr08Amt;

    public void setDistr08Amt(BigDecimal distr08Amt){
        this.distr08Amt = distr08Amt;
    }
    public BigDecimal getDistr08Amt(){
        return distr08Amt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_10")
    private String taxDriver10;

    public void setTaxDriver10(String taxDriver10){
        this.taxDriver10 = taxDriver10;
    }
    public String getTaxDriver10(){
        return taxDriver10;
    }

    @Basic
    @Column(name = "ALLOCATION_SUBTRANS_ID")
    private Long allocationSubtransId;

    public void setAllocationSubtransId(Long allocationSubtransId){
        this.allocationSubtransId = allocationSubtransId;
    }
    public Long getAllocationSubtransId(){
        return allocationSubtransId;
    }

    @Basic
    @Column(name = "STJ10_TAXABLE_AMT")
    private BigDecimal stj10TaxableAmt;

    public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt){
        this.stj10TaxableAmt = stj10TaxableAmt;
    }
    public BigDecimal getStj10TaxableAmt(){
        return stj10TaxableAmt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_11")
    private String taxDriver11;

    public void setTaxDriver11(String taxDriver11){
        this.taxDriver11 = taxDriver11;
    }
    public String getTaxDriver11(){
        return taxDriver11;
    }

    @Basic
    @Column(name = "CITY_TIER3_TAXABLE_AMT")
    private BigDecimal cityTier3TaxableAmt;

    public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt){
        this.cityTier3TaxableAmt = cityTier3TaxableAmt;
    }
    public BigDecimal getCityTier3TaxableAmt(){
        return cityTier3TaxableAmt;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_01")
    private String locationDriver01;

    public void setLocationDriver01(String locationDriver01){
        this.locationDriver01 = locationDriver01;
    }
    public String getLocationDriver01(){
        return locationDriver01;
    }

    @Basic
    @Column(name = "TAX_DRIVER_12")
    private String taxDriver12;

    public void setTaxDriver12(String taxDriver12){
        this.taxDriver12 = taxDriver12;
    }
    public String getTaxDriver12(){
        return taxDriver12;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_02")
    private String locationDriver02;

    public void setLocationDriver02(String locationDriver02){
        this.locationDriver02 = locationDriver02;
    }
    public String getLocationDriver02(){
        return locationDriver02;
    }

    @Basic
    @Column(name = "TAX_DRIVER_13")
    private String taxDriver13;

    public void setTaxDriver13(String taxDriver13){
        this.taxDriver13 = taxDriver13;
    }
    public String getTaxDriver13(){
        return taxDriver13;
    }

    @Basic
    @Column(name = "TAX_DRIVER_14")
    private String taxDriver14;

    public void setTaxDriver14(String taxDriver14){
        this.taxDriver14 = taxDriver14;
    }
    public String getTaxDriver14(){
        return taxDriver14;
    }

    @Basic
    @Column(name = "TAX_DRIVER_15")
    private String taxDriver15;

    public void setTaxDriver15(String taxDriver15){
        this.taxDriver15 = taxDriver15;
    }
    public String getTaxDriver15(){
        return taxDriver15;
    }

    @Basic
    @Column(name = "CITY_TIER1_TAXABLE_AMT")
    private BigDecimal cityTier1TaxableAmt;

    public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt){
        this.cityTier1TaxableAmt = cityTier1TaxableAmt;
    }
    public BigDecimal getCityTier1TaxableAmt(){
        return cityTier1TaxableAmt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_16")
    private String taxDriver16;

    public void setTaxDriver16(String taxDriver16){
        this.taxDriver16 = taxDriver16;
    }
    public String getTaxDriver16(){
        return taxDriver16;
    }

    @Basic
    @Column(name = "COUNTY_TIER3_TAXABLE_AMT")
    private BigDecimal countyTier3TaxableAmt;

    public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt){
        this.countyTier3TaxableAmt = countyTier3TaxableAmt;
    }
    public BigDecimal getCountyTier3TaxableAmt(){
        return countyTier3TaxableAmt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_17")
    private String taxDriver17;

    public void setTaxDriver17(String taxDriver17){
        this.taxDriver17 = taxDriver17;
    }
    public String getTaxDriver17(){
        return taxDriver17;
    }

    @Basic
    @Column(name = "TAX_DRIVER_18")
    private String taxDriver18;

    public void setTaxDriver18(String taxDriver18){
        this.taxDriver18 = taxDriver18;
    }
    public String getTaxDriver18(){
        return taxDriver18;
    }

    @Basic
    @Column(name = "TAX_DRIVER_19")
    private String taxDriver19;

    public void setTaxDriver19(String taxDriver19){
        this.taxDriver19 = taxDriver19;
    }
    public String getTaxDriver19(){
        return taxDriver19;
    }

    @Basic
    @Column(name = "CITY_TIER2_TAXABLE_AMT")
    private BigDecimal cityTier2TaxableAmt;

    public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt){
        this.cityTier2TaxableAmt = cityTier2TaxableAmt;
    }
    public BigDecimal getCityTier2TaxableAmt(){
        return cityTier2TaxableAmt;
    }

    @Basic
    @Column(name = "STJ9_EXEMPT_AMT")
    private BigDecimal stj9ExemptAmt;

    public void setStj9ExemptAmt(BigDecimal stj9ExemptAmt){
        this.stj9ExemptAmt = stj9ExemptAmt;
    }
    public BigDecimal getStj9ExemptAmt(){
        return stj9ExemptAmt;
    }

    @Basic
    @Column(name = "STJ10_TAX_AMT")
    private BigDecimal stj10TaxAmt;

    public void setStj10TaxAmt(BigDecimal stj10TaxAmt){
        this.stj10TaxAmt = stj10TaxAmt;
    }
    public BigDecimal getStj10TaxAmt(){
        return stj10TaxAmt;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_05")
    private String locationDriver05;

    public void setLocationDriver05(String locationDriver05){
        this.locationDriver05 = locationDriver05;
    }
    public String getLocationDriver05(){
        return locationDriver05;
    }

    @Basic
    @Column(name = "USAGE_CODE")
    private String usageCode;

    public void setUsageCode(String usageCode){
        this.usageCode = usageCode;
    }
    public String getUsageCode(){
        return usageCode;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_06")
    private String locationDriver06;

    public void setLocationDriver06(String locationDriver06){
        this.locationDriver06 = locationDriver06;
    }
    public String getLocationDriver06(){
        return locationDriver06;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_03")
    private String locationDriver03;

    public void setLocationDriver03(String locationDriver03){
        this.locationDriver03 = locationDriver03;
    }
    public String getLocationDriver03(){
        return locationDriver03;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_04")
    private String locationDriver04;

    public void setLocationDriver04(String locationDriver04){
        this.locationDriver04 = locationDriver04;
    }
    public String getLocationDriver04(){
        return locationDriver04;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_09")
    private String locationDriver09;

    public void setLocationDriver09(String locationDriver09){
        this.locationDriver09 = locationDriver09;
    }
    public String getLocationDriver09(){
        return locationDriver09;
    }

    @Basic
    @Column(name = "PRODUCT_CLASS_CODE")
    private String productClassCode;

    public void setProductClassCode(String productClassCode){
        this.productClassCode = productClassCode;
    }
    public String getProductClassCode(){
        return productClassCode;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_07")
    private String locationDriver07;

    public void setLocationDriver07(String locationDriver07){
        this.locationDriver07 = locationDriver07;
    }
    public String getLocationDriver07(){
        return locationDriver07;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_08")
    private String locationDriver08;

    public void setLocationDriver08(String locationDriver08){
        this.locationDriver08 = locationDriver08;
    }
    public String getLocationDriver08(){
        return locationDriver08;
    }

    @Basic
    @Column(name = "TRANSACTION_TYPE_CODE")
    private String transactionTypeCode;

    public void setTransactionTypeCode(String transactionTypeCode){
        this.transactionTypeCode = transactionTypeCode;
    }
    public String getTransactionTypeCode(){
        return transactionTypeCode;
    }

    @Basic
    @Column(name = "AFE_CONTRACT_TYPE")
    private String afeContractType;

    public void setAfeContractType(String afeContractType){
        this.afeContractType = afeContractType;
    }
    public String getAfeContractType(){
        return afeContractType;
    }

    @Basic
    @Column(name = "TAX_DRIVER_01")
    private String taxDriver01;

    public void setTaxDriver01(String taxDriver01){
        this.taxDriver01 = taxDriver01;
    }
    public String getTaxDriver01(){
        return taxDriver01;
    }

    @Basic
    @Column(name = "INVENTORY_NAME")
    private String inventoryName;

    public void setInventoryName(String inventoryName){
        this.inventoryName = inventoryName;
    }
    public String getInventoryName(){
        return inventoryName;
    }

    @Basic
    @Column(name = "TAX_DRIVER_02")
    private String taxDriver02;

    public void setTaxDriver02(String taxDriver02){
        this.taxDriver02 = taxDriver02;
    }
    public String getTaxDriver02(){
        return taxDriver02;
    }

    @Basic
    @Column(name = "TAX_DRIVER_03")
    private String taxDriver03;

    public void setTaxDriver03(String taxDriver03){
        this.taxDriver03 = taxDriver03;
    }
    public String getTaxDriver03(){
        return taxDriver03;
    }

    @Basic
    @Column(name = "CERT_TYPE")
    private String certType;

    public void setCertType(String certType){
        this.certType = certType;
    }
    public String getCertType(){
        return certType;
    }

    @Basic
    @Column(name = "TAX_DRIVER_04")
    private String taxDriver04;

    public void setTaxDriver04(String taxDriver04){
        this.taxDriver04 = taxDriver04;
    }
    public String getTaxDriver04(){
        return taxDriver04;
    }

    @Basic
    @Column(name = "TAX_DRIVER_05")
    private String taxDriver05;

    public void setTaxDriver05(String taxDriver05){
        this.taxDriver05 = taxDriver05;
    }
    public String getTaxDriver05(){
        return taxDriver05;
    }

    @Basic
    @Column(name = "TAX_DRIVER_06")
    private String taxDriver06;

    public void setTaxDriver06(String taxDriver06){
        this.taxDriver06 = taxDriver06;
    }
    public String getTaxDriver06(){
        return taxDriver06;
    }

    @Basic
    @Column(name = "TAX_DRIVER_07")
    private String taxDriver07;

    public void setTaxDriver07(String taxDriver07){
        this.taxDriver07 = taxDriver07;
    }
    public String getTaxDriver07(){
        return taxDriver07;
    }

    @Basic
    @Column(name = "TAX_DRIVER_08")
    private String taxDriver08;

    public void setTaxDriver08(String taxDriver08){
        this.taxDriver08 = taxDriver08;
    }
    public String getTaxDriver08(){
        return taxDriver08;
    }

    @Basic
    @Column(name = "TAX_DRIVER_09")
    private String taxDriver09;

    public void setTaxDriver09(String taxDriver09){
        this.taxDriver09 = taxDriver09;
    }
    public String getTaxDriver09(){
        return taxDriver09;
    }

    @Basic
    @Column(name = "INVOICE_LINE_TAX_PAID_AMT")
    private BigDecimal invoiceLineTaxPaidAmt;

    public void setInvoiceLineTaxPaidAmt(BigDecimal invoiceLineTaxPaidAmt){
        this.invoiceLineTaxPaidAmt = invoiceLineTaxPaidAmt;
    }
    public BigDecimal getInvoiceLineTaxPaidAmt(){
        return invoiceLineTaxPaidAmt;
    }

    @Basic
    @Column(name = "GL_DIVISION_NBR")
    private String glDivisionNbr;

    public void setGlDivisionNbr(String glDivisionNbr){
        this.glDivisionNbr = glDivisionNbr;
    }
    public String getGlDivisionNbr(){
        return glDivisionNbr;
    }

    @Basic
    @Column(name = "AFE_CATEGORY_NBR")
    private String afeCategoryNbr;

    public void setAfeCategoryNbr(String afeCategoryNbr){
        this.afeCategoryNbr = afeCategoryNbr;
    }
    public String getAfeCategoryNbr(){
        return afeCategoryNbr;
    }

    @Basic
    @Column(name = "COUNTY_TIER1_TAXABLE_AMT")
    private BigDecimal countyTier1TaxableAmt;

    public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt){
        this.countyTier1TaxableAmt = countyTier1TaxableAmt;
    }
    public BigDecimal getCountyTier1TaxableAmt(){
        return countyTier1TaxableAmt;
    }

    @Basic
    @Column(name = "COUNTY_TIER2_TAXABLE_AMT")
    private BigDecimal countyTier2TaxableAmt;

    public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt){
        this.countyTier2TaxableAmt = countyTier2TaxableAmt;
    }
    public BigDecimal getCountyTier2TaxableAmt(){
        return countyTier2TaxableAmt;
    }

    @Basic
    @Column(name = "STJ3_TAX_AMT")
    private BigDecimal stj3TaxAmt;

    public void setStj3TaxAmt(BigDecimal stj3TaxAmt){
        this.stj3TaxAmt = stj3TaxAmt;
    }
    public BigDecimal getStj3TaxAmt(){
        return stj3TaxAmt;
    }

    @Basic
    @Column(name = "FATAL_ERROR_CODE")
    private String fatalErrorCode;

    public void setFatalErrorCode(String fatalErrorCode){
        this.fatalErrorCode = fatalErrorCode;
    }
    public String getFatalErrorCode(){
        return fatalErrorCode;
    }

    @Basic
    @Column(name = "AUDIT_USER_ID")
    private String auditUserId;

    public void setAuditUserId(String auditUserId){
        this.auditUserId = auditUserId;
    }
    public String getAuditUserId(){
        return auditUserId;
    }

    @Basic
    @Column(name = "URL_LINK")
    private String urlLink;

    public void setUrlLink(String urlLink){
        this.urlLink = urlLink;
    }
    public String getUrlLink(){
        return urlLink;
    }

    @Basic
    @Column(name = "METHOD_DELIVERY_CODE")
    private String methodDeliveryCode;

    public void setMethodDeliveryCode(String methodDeliveryCode){
        this.methodDeliveryCode = methodDeliveryCode;
    }
    public String getMethodDeliveryCode(){
        return methodDeliveryCode;
    }

    @Basic
    @Column(name = "AFE_PROJECT_NAME")
    private String afeProjectName;

    public void setAfeProjectName(String afeProjectName){
        this.afeProjectName = afeProjectName;
    }
    public String getAfeProjectName(){
        return afeProjectName;
    }

    @Basic
    @Column(name = "COUNTRY_TIER2_TAX_AMT")
    private BigDecimal countryTier2TaxAmt;

    public void setCountryTier2TaxAmt(BigDecimal countryTier2TaxAmt){
        this.countryTier2TaxAmt = countryTier2TaxAmt;
    }
    public BigDecimal getCountryTier2TaxAmt(){
        return countryTier2TaxAmt;
    }

    @Basic
    @Column(name = "STJ6_TAXABLE_AMT")
    private BigDecimal stj6TaxableAmt;

    public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt){
        this.stj6TaxableAmt = stj6TaxableAmt;
    }
    public BigDecimal getStj6TaxableAmt(){
        return stj6TaxableAmt;
    }

    @Basic
    @Column(name = "DISTR_03_AMT")
    private BigDecimal distr03Amt;

    public void setDistr03Amt(BigDecimal distr03Amt){
        this.distr03Amt = distr03Amt;
    }
    public BigDecimal getDistr03Amt(){
        return distr03Amt;
    }

    @Basic
    @Column(name = "PROCESSING_NOTES")
    private String processingNotes;

    public void setProcessingNotes(String processingNotes){
        this.processingNotes = processingNotes;
    }
    public String getProcessingNotes(){
        return processingNotes;
    }

    @Basic
    @Column(name = "CP_FILING_ENTITY_CODE")
    private String cpFilingEntityCode;

    public void setCpFilingEntityCode(String cpFilingEntityCode){
        this.cpFilingEntityCode = cpFilingEntityCode;
    }
    public String getCpFilingEntityCode(){
        return cpFilingEntityCode;
    }

    @Basic
    @Column(name = "GL_LOCAL_SUB_ACCT_NAME")
    private String glLocalSubAcctName;

    public void setGlLocalSubAcctName(String glLocalSubAcctName){
        this.glLocalSubAcctName = glLocalSubAcctName;
    }
    public String getGlLocalSubAcctName(){
        return glLocalSubAcctName;
    }

    @Basic
    @Column(name = "STJ2_TAXABLE_AMT")
    private BigDecimal stj2TaxableAmt;

    public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt){
        this.stj2TaxableAmt = stj2TaxableAmt;
    }
    public BigDecimal getStj2TaxableAmt(){
        return stj2TaxableAmt;
    }

    @Basic
    @Column(name = "CITY_LOCAL_TAX_AMT")
    private BigDecimal cityLocalTaxAmt;

    public void setCityLocalTaxAmt(BigDecimal cityLocalTaxAmt){
        this.cityLocalTaxAmt = cityLocalTaxAmt;
    }
    public BigDecimal getCityLocalTaxAmt(){
        return cityLocalTaxAmt;
    }

    @Basic
    @Column(name = "AFE_USE")
    private String afeUse;

    public void setAfeUse(String afeUse){
        this.afeUse = afeUse;
    }
    public String getAfeUse(){
        return afeUse;
    }

    @Basic
    @Column(name = "STATE_TIER1_TAX_AMT")
    private BigDecimal stateTier1TaxAmt;

    public void setStateTier1TaxAmt(BigDecimal stateTier1TaxAmt){
        this.stateTier1TaxAmt = stateTier1TaxAmt;
    }
    public BigDecimal getStateTier1TaxAmt(){
        return stateTier1TaxAmt;
    }

    @Basic
    @Column(name = "STJ4_TAXABLE_AMT")
    private BigDecimal stj4TaxableAmt;

    public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt){
        this.stj4TaxableAmt = stj4TaxableAmt;
    }
    public BigDecimal getStj4TaxableAmt(){
        return stj4TaxableAmt;
    }

    @Basic
    @Column(name = "CUST_ENTITY_CODE")
    private String custEntityCode;

    public void setCustEntityCode(String custEntityCode){
        this.custEntityCode = custEntityCode;
    }
    public String getCustEntityCode(){
        return custEntityCode;
    }

    @Basic
    @Column(name = "GL_FULL_ACCT_NBR")
    private String glFullAcctNbr;

    public void setGlFullAcctNbr(String glFullAcctNbr){
        this.glFullAcctNbr = glFullAcctNbr;
    }
    public String getGlFullAcctNbr(){
        return glFullAcctNbr;
    }

    @Basic
    @Column(name = "REPROCESS_CODE")
    private String reprocessCode;

    public void setReprocessCode(String reprocessCode){
        this.reprocessCode = reprocessCode;
    }
    public String getReprocessCode(){
        return reprocessCode;
    }

    @Basic
    @Column(name = "INVOICE_LINE_DISC_AMT")
    private BigDecimal invoiceLineDiscAmt;

    public void setInvoiceLineDiscAmt(BigDecimal invoiceLineDiscAmt){
        this.invoiceLineDiscAmt = invoiceLineDiscAmt;
    }
    public BigDecimal getInvoiceLineDiscAmt(){
        return invoiceLineDiscAmt;
    }

    @Basic
    @Column(name = "GL_CC_NBR_DEPT_NAME")
    private String glCcNbrDeptName;

    public void setGlCcNbrDeptName(String glCcNbrDeptName){
        this.glCcNbrDeptName = glCcNbrDeptName;
    }
    public String getGlCcNbrDeptName(){
        return glCcNbrDeptName;
    }

    @Basic
    @Column(name = "STJ7_TAX_AMT")
    private BigDecimal stj7TaxAmt;

    public void setStj7TaxAmt(BigDecimal stj7TaxAmt){
        this.stj7TaxAmt = stj7TaxAmt;
    }
    public BigDecimal getStj7TaxAmt(){
        return stj7TaxAmt;
    }

    @Basic
    @Column(name = "STJ1_EXEMPT_AMT")
    private BigDecimal stj1ExemptAmt;

    public void setStj1ExemptAmt(BigDecimal stj1ExemptAmt){
        this.stj1ExemptAmt = stj1ExemptAmt;
    }
    public BigDecimal getStj1ExemptAmt(){
        return stj1ExemptAmt;
    }

    @Basic
    @Column(name = "LOCATION_DRIVER_10")
    private String locationDriver10;

    public void setLocationDriver10(String locationDriver10){
        this.locationDriver10 = locationDriver10;
    }
    public String getLocationDriver10(){
        return locationDriver10;
    }

    @Basic
    @Column(name = "SOURCE_TRANSACTION_ID")
    private String sourceTransactionId;

    public void setSourceTransactionId(String sourceTransactionId){
        this.sourceTransactionId = sourceTransactionId;
    }
    public String getSourceTransactionId(){
        return sourceTransactionId;
    }

    @Basic
    @Column(name = "TRANSACTION_COUNTRY_CODE")
    private String transactionCountryCode;

    public void setTransactionCountryCode(String transactionCountryCode){
        this.transactionCountryCode = transactionCountryCode;
    }
    public String getTransactionCountryCode(){
        return transactionCountryCode;
    }

    @Basic
    @Column(name = "AFE_SUB_CAT_NAME")
    private String afeSubCatName;

    public void setAfeSubCatName(String afeSubCatName){
        this.afeSubCatName = afeSubCatName;
    }
    public String getAfeSubCatName(){
        return afeSubCatName;
    }

    @Basic
    @Column(name = "ENTITY_CODE")
    private String entityCode;

    public void setEntityCode(String entityCode){
        this.entityCode = entityCode;
    }
    public String getEntityCode(){
        return entityCode;
    }

    @Basic
    @Column(name = "AFE_SUB_CAT_NBR")
    private String afeSubCatNbr;

    public void setAfeSubCatNbr(String afeSubCatNbr){
        this.afeSubCatNbr = afeSubCatNbr;
    }
    public String getAfeSubCatNbr(){
        return afeSubCatNbr;
    }

    @Basic
    @Column(name = "GL_PROFIT_CENTER_NBR")
    private String glProfitCenterNbr;

    public void setGlProfitCenterNbr(String glProfitCenterNbr){
        this.glProfitCenterNbr = glProfitCenterNbr;
    }
    public String getGlProfitCenterNbr(){
        return glProfitCenterNbr;
    }

    @Basic
    @Column(name = "STJ8_TAXABLE_AMT")
    private BigDecimal stj8TaxableAmt;

    public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt){
        this.stj8TaxableAmt = stj8TaxableAmt;
    }
    public BigDecimal getStj8TaxableAmt(){
        return stj8TaxableAmt;
    }

    @Basic
    @Column(name = "LOCATION_NAME")
    private String locationName;

    public void setLocationName(String locationName){
        this.locationName = locationName;
    }
    public String getLocationName(){
        return locationName;
    }

    @Basic
    @Column(name = "STJ7_EXEMPT_AMT")
    private BigDecimal stj7ExemptAmt;

    public void setStj7ExemptAmt(BigDecimal stj7ExemptAmt){
        this.stj7ExemptAmt = stj7ExemptAmt;
    }
    public BigDecimal getStj7ExemptAmt(){
        return stj7ExemptAmt;
    }

    @Basic
    @Column(name = "STJ3_EXEMPT_AMT")
    private BigDecimal stj3ExemptAmt;

    public void setStj3ExemptAmt(BigDecimal stj3ExemptAmt){
        this.stj3ExemptAmt = stj3ExemptAmt;
    }
    public BigDecimal getStj3ExemptAmt(){
        return stj3ExemptAmt;
    }

    @Basic
    @Column(name = "COUNTRY_TIER1_TAX_AMT")
    private BigDecimal countryTier1TaxAmt;

    public void setCountryTier1TaxAmt(BigDecimal countryTier1TaxAmt){
        this.countryTier1TaxAmt = countryTier1TaxAmt;
    }
    public BigDecimal getCountryTier1TaxAmt(){
        return countryTier1TaxAmt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_DISC_TYPE_CODE")
    private String invoiceLineDiscTypeCode;

    public void setInvoiceLineDiscTypeCode(String invoiceLineDiscTypeCode){
        this.invoiceLineDiscTypeCode = invoiceLineDiscTypeCode;
    }
    public String getInvoiceLineDiscTypeCode(){
        return invoiceLineDiscTypeCode;
    }

    @Basic
    @Column(name = "INVOICE_LINE_PRODUCT_CODE")
    private String invoiceLineProductCode;

    public void setInvoiceLineProductCode(String invoiceLineProductCode){
        this.invoiceLineProductCode = invoiceLineProductCode;
    }
    public String getInvoiceLineProductCode(){
        return invoiceLineProductCode;
    }

    @Basic
    @Column(name = "STJ10_EXEMPT_AMT")
    private BigDecimal stj10ExemptAmt;

    public void setStj10ExemptAmt(BigDecimal stj10ExemptAmt){
        this.stj10ExemptAmt = stj10ExemptAmt;
    }
    public BigDecimal getStj10ExemptAmt(){
        return stj10ExemptAmt;
    }

    @Basic
    @Column(name = "EXEMPT_IND")
    private String exemptInd;

    public void setExemptInd(String exemptInd){
        this.exemptInd = exemptInd;
    }
    public String getExemptInd(){
        return exemptInd;
    }

    @Basic
    @Column(name = "GL_PROFIT_CENTER_NAME")
    private String glProfitCenterName;

    public void setGlProfitCenterName(String glProfitCenterName){
        this.glProfitCenterName = glProfitCenterName;
    }
    public String getGlProfitCenterName(){
        return glProfitCenterName;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "CERT_EXPIRATION_DATE")
    private Date certExpirationDate;

    public void setCertExpirationDate(Date certExpirationDate){
        this.certExpirationDate = certExpirationDate;
    }
    public Date getCertExpirationDate(){
        return certExpirationDate;
    }

    @Basic
    @Column(name = "STJ4_TAX_AMT")
    private BigDecimal stj4TaxAmt;

    public void setStj4TaxAmt(BigDecimal stj4TaxAmt){
        this.stj4TaxAmt = stj4TaxAmt;
    }
    public BigDecimal getStj4TaxAmt(){
        return stj4TaxAmt;
    }

    @Basic
    @Column(name = "TRANSACTION_STATUS")
    private String transactionStatus;

    public void setTransactionStatus(String transactionStatus){
        this.transactionStatus = transactionStatus;
    }
    public String getTransactionStatus(){
        return transactionStatus;
    }

    @Basic
    @Column(name = "INVOICE_TOTAL_AMT")
    private BigDecimal invoiceTotalAmt;

    public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt){
        this.invoiceTotalAmt = invoiceTotalAmt;
    }
    public BigDecimal getInvoiceTotalAmt(){
        return invoiceTotalAmt;
    }

    @Basic
    @Column(name = "CUST_JURISDICTION_ID")
    private Long custJurisdictionId;

    public void setCustJurisdictionId(Long custJurisdictionId){
        this.custJurisdictionId = custJurisdictionId;
    }
    public Long getCustJurisdictionId(){
        return custJurisdictionId;
    }

    @Basic
    @Column(name = "CUST_CITY")
    private String custCity;

    public void setCustCity(String custCity){
        this.custCity = custCity;
    }
    public String getCustCity(){
        return custCity;
    }

    @Basic
    @Column(name = "EXEMPT_AMT")
    private BigDecimal exemptAmt;

    public void setExemptAmt(BigDecimal exemptAmt){
        this.exemptAmt = exemptAmt;
    }
    public BigDecimal getExemptAmt(){
        return exemptAmt;
    }

    @Basic
    @Column(name = "TAX_PAID_TO_VENDOR_FLAG")
    private String taxPaidToVendorFlag;

    public void setTaxPaidToVendorFlag(String taxPaidToVendorFlag){
        this.taxPaidToVendorFlag = taxPaidToVendorFlag;
    }
    public String getTaxPaidToVendorFlag(){
        return taxPaidToVendorFlag;
    }

    @Basic
    @Column(name = "COUNTY_TIER2_TAX_AMT")
    private BigDecimal countyTier2TaxAmt;

    public void setCountyTier2TaxAmt(BigDecimal countyTier2TaxAmt){
        this.countyTier2TaxAmt = countyTier2TaxAmt;
    }
    public BigDecimal getCountyTier2TaxAmt(){
        return countyTier2TaxAmt;
    }

    @Basic
    @Column(name = "CUST_GEOCODE")
    private String custGeocode;

    public void setCustGeocode(String custGeocode){
        this.custGeocode = custGeocode;
    }
    public String getCustGeocode(){
        return custGeocode;
    }

    @Basic
    @Column(name = "INVOICE_TAX_PAID_AMT")
    private BigDecimal invoiceTaxPaidAmt;

    public void setInvoiceTaxPaidAmt(BigDecimal invoiceTaxPaidAmt){
        this.invoiceTaxPaidAmt = invoiceTaxPaidAmt;
    }
    public BigDecimal getInvoiceTaxPaidAmt(){
        return invoiceTaxPaidAmt;
    }

    @Basic
    @Column(name = "COUNTY_LOCAL_TAX_AMT")
    private BigDecimal countyLocalTaxAmt;

    public void setCountyLocalTaxAmt(BigDecimal countyLocalTaxAmt){
        this.countyLocalTaxAmt = countyLocalTaxAmt;
    }
    public BigDecimal getCountyLocalTaxAmt(){
        return countyLocalTaxAmt;
    }

    @Basic
    @Column(name = "DISCOUNT_TYPE")
    private String discountType;

    public void setDiscountType(String discountType){
        this.discountType = discountType;
    }
    public String getDiscountType(){
        return discountType;
    }

    @Basic
    @Column(name = "DISTR_07_AMT")
    private BigDecimal distr07Amt;

    public void setDistr07Amt(BigDecimal distr07Amt){
        this.distr07Amt = distr07Amt;
    }
    public BigDecimal getDistr07Amt(){
        return distr07Amt;
    }

    @Basic
    @Column(name = "AUDIT_FLAG")
    private String auditFlag;

    public void setAuditFlag(String auditFlag){
        this.auditFlag = auditFlag;
    }
    public String getAuditFlag(){
        return auditFlag;
    }

    @Basic
    @Column(name = "CERT_IMAGE")
    private String certImage;

    public void setCertImage(String certImage){
        this.certImage = certImage;
    }
    public String getCertImage(){
        return certImage;
    }

    @Basic
    @Column(name = "STJ1_TAX_AMT")
    private BigDecimal stj1TaxAmt;

    public void setStj1TaxAmt(BigDecimal stj1TaxAmt){
        this.stj1TaxAmt = stj1TaxAmt;
    }
    public BigDecimal getStj1TaxAmt(){
        return stj1TaxAmt;
    }

    @Basic
    @Column(name = "EXEMPT_CODE")
    private String exemptCode;

    public void setExemptCode(String exemptCode){
        this.exemptCode = exemptCode;
    }
    public String getExemptCode(){
        return exemptCode;
    }

    @Basic
    @Column(name = "INVOICE_LINE_FREIGHT_AMT")
    private BigDecimal invoiceLineFreightAmt;

    public void setInvoiceLineFreightAmt(BigDecimal invoiceLineFreightAmt){
        this.invoiceLineFreightAmt = invoiceLineFreightAmt;
    }
    public BigDecimal getInvoiceLineFreightAmt(){
        return invoiceLineFreightAmt;
    }

    @Basic
    @Column(name = "ENTITY_ID")
    private Long entityId;

    public void setEntityId(Long entityId){
        this.entityId = entityId;
    }
    public Long getEntityId(){
        return entityId;
    }

    @Basic
    @Column(name = "GL_FULL_ACCT_NAME")
    private String glFullAcctName;

    public void setGlFullAcctName(String glFullAcctName){
        this.glFullAcctName = glFullAcctName;
    }
    public String getGlFullAcctName(){
        return glFullAcctName;
    }

    @Basic
    @Column(name = "GL_EXTRACT_BATCH_NO")
    private Long glExtractBatchNo;

    public void setGlExtractBatchNo(Long glExtractBatchNo){
        this.glExtractBatchNo = glExtractBatchNo;
    }
    public Long getGlExtractBatchNo(){
        return glExtractBatchNo;
    }

    @Basic
    @Column(name = "GL_LOCAL_SUB_ACCT_NBR")
    private String glLocalSubAcctNbr;

    public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr){
        this.glLocalSubAcctNbr = glLocalSubAcctNbr;
    }
    public String getGlLocalSubAcctNbr(){
        return glLocalSubAcctNbr;
    }

    @Basic
    @Column(name = "PRODUCT_CODE")
    private String productCode;

    public void setProductCode(String productCode){
        this.productCode = productCode;
    }
    public String getProductCode(){
        return productCode;
    }

    @Basic
    @Column(name = "DISTR_02_AMT")
    private BigDecimal distr02Amt;

    public void setDistr02Amt(BigDecimal distr02Amt){
        this.distr02Amt = distr02Amt;
    }
    public BigDecimal getDistr02Amt(){
        return distr02Amt;
    }

    @Basic
    @Column(name = "DISTR_10_AMT")
    private BigDecimal distr10Amt;

    public void setDistr10Amt(BigDecimal distr10Amt){
        this.distr10Amt = distr10Amt;
    }
    public BigDecimal getDistr10Amt(){
        return distr10Amt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_TAX")
    private BigDecimal invoiceLineTax;

    public void setInvoiceLineTax(BigDecimal invoiceLineTax){
        this.invoiceLineTax = invoiceLineTax;
    }
    public BigDecimal getInvoiceLineTax(){
        return invoiceLineTax;
    }

    @Basic
    @Column(name = "AUTO_TRANSACTION_STATE_CODE")
    private String autoTransactionStateCode;

    public void setAutoTransactionStateCode(String autoTransactionStateCode){
        this.autoTransactionStateCode = autoTransactionStateCode;
    }
    public String getAutoTransactionStateCode(){
        return autoTransactionStateCode;
    }

    @Basic
    @Column(name = "STJ5_EXEMPT_AMT")
    private BigDecimal stj5ExemptAmt;

    public void setStj5ExemptAmt(BigDecimal stj5ExemptAmt){
        this.stj5ExemptAmt = stj5ExemptAmt;
    }
    public BigDecimal getStj5ExemptAmt(){
        return stj5ExemptAmt;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "LOAD_TIMESTAMP")
    private Date loadTimestamp;

    public void setLoadTimestamp(Date loadTimestamp){
        this.loadTimestamp = loadTimestamp;
    }
    public Date getLoadTimestamp(){
        return loadTimestamp;
    }

    @Basic
    @Column(name = "STJ9_TAX_AMT")
    private BigDecimal stj9TaxAmt;

    public void setStj9TaxAmt(BigDecimal stj9TaxAmt){
        this.stj9TaxAmt = stj9TaxAmt;
    }
    public BigDecimal getStj9TaxAmt(){
        return stj9TaxAmt;
    }

    @Basic
    @Column(name = "INVOICE_DISC_TYPE_CODE")
    private String invoiceDiscTypeCode;

    public void setInvoiceDiscTypeCode(String invoiceDiscTypeCode){
        this.invoiceDiscTypeCode = invoiceDiscTypeCode;
    }
    public String getInvoiceDiscTypeCode(){
        return invoiceDiscTypeCode;
    }

    @Basic
    @Column(name = "GL_COMPANY_NAME")
    private String glCompanyName;

    public void setGlCompanyName(String glCompanyName){
        this.glCompanyName = glCompanyName;
    }
    public String getGlCompanyName(){
        return glCompanyName;
    }

    @Basic
    @Column(name = "STATE_TIER1_TAXABLE_AMT")
    private BigDecimal stateTier1TaxableAmt;

    public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt){
        this.stateTier1TaxableAmt = stateTier1TaxableAmt;
    }
    public BigDecimal getStateTier1TaxableAmt(){
        return stateTier1TaxableAmt;
    }

    @Basic
    @Column(name = "INVOICE_DESC")
    private String invoiceDesc;

    public void setInvoiceDesc(String invoiceDesc){
        this.invoiceDesc = invoiceDesc;
    }
    public String getInvoiceDesc(){
        return invoiceDesc;
    }

    @Basic
    @Column(name = "COUNTRY_TIER1_TAXABLE_AMT")
    private BigDecimal countryTier1TaxableAmt;

    public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt){
        this.countryTier1TaxableAmt = countryTier1TaxableAmt;
    }
    public BigDecimal getCountryTier1TaxableAmt(){
        return countryTier1TaxableAmt;
    }

    @Basic
    @Column(name = "STATE_TIER2_TAXABLE_AMT")
    private BigDecimal stateTier2TaxableAmt;

    public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt){
        this.stateTier2TaxableAmt = stateTier2TaxableAmt;
    }
    public BigDecimal getStateTier2TaxableAmt(){
        return stateTier2TaxableAmt;
    }

    @Basic
    @Column(name = "STATE_TIER2_TAX_AMT")
    private BigDecimal stateTier2TaxAmt;

    public void setStateTier2TaxAmt(BigDecimal stateTier2TaxAmt){
        this.stateTier2TaxAmt = stateTier2TaxAmt;
    }
    public BigDecimal getStateTier2TaxAmt(){
        return stateTier2TaxAmt;
    }

    @Basic
    @Column(name = "STATE_TIER3_TAXABLE_AMT")
    private BigDecimal stateTier3TaxableAmt;

    public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt){
        this.stateTier3TaxableAmt = stateTier3TaxableAmt;
    }
    public BigDecimal getStateTier3TaxableAmt(){
        return stateTier3TaxableAmt;
    }

    @Basic
    @Column(name = "SUSPEND_IND")
    private String suspendInd;

    public void setSuspendInd(String suspendInd){
        this.suspendInd = suspendInd;
    }
    public String getSuspendInd(){
        return suspendInd;
    }

    @Basic
    @Column(name = "COUNTRY_TIER3_TAXABLE_AMT")
    private BigDecimal countryTier3TaxableAmt;

    public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt){
        this.countryTier3TaxableAmt = countryTier3TaxableAmt;
    }
    public BigDecimal getCountryTier3TaxableAmt(){
        return countryTier3TaxableAmt;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "ENTERED_DATE")
    private Date enteredDate;

    public void setEnteredDate(Date enteredDate){
        this.enteredDate = enteredDate;
    }
    public Date getEnteredDate(){
        return enteredDate;
    }

    @Basic
    @Column(name = "COUNTRY_TIER2_TAXABLE_AMT")
    private BigDecimal countryTier2TaxableAmt;

    public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt){
        this.countryTier2TaxableAmt = countryTier2TaxableAmt;
    }
    public BigDecimal getCountryTier2TaxableAmt(){
        return countryTier2TaxableAmt;
    }

    @Basic
    @Column(name = "PRODUCT_CLASS_NAME")
    private String productClassName;

    public void setProductClassName(String productClassName){
        this.productClassName = productClassName;
    }
    public String getProductClassName(){
        return productClassName;
    }

    @Basic
    @Column(name = "USAGE_NAME")
    private String usageName;

    public void setUsageName(String usageName){
        this.usageName = usageName;
    }
    public String getUsageName(){
        return usageName;
    }

    @Basic
    @Column(name = "MULTI_TRANS_CODE")
    private String multiTransCode;

    public void setMultiTransCode(String multiTransCode){
        this.multiTransCode = multiTransCode;
    }
    public String getMultiTransCode(){
        return multiTransCode;
    }

    @Basic
    @Column(name = "RATETYPE_CODE")
    private String ratetypeCode;

    public void setRatetypeCode(String ratetypeCode){
        this.ratetypeCode = ratetypeCode;
    }
    public String getRatetypeCode(){
        return ratetypeCode;
    }

    @Basic
    @Column(name = "INVENTORY_CLASS_NAME")
    private String inventoryClassName;

    public void setInventoryClassName(String inventoryClassName){
        this.inventoryClassName = inventoryClassName;
    }
    public String getInventoryClassName(){
        return inventoryClassName;
    }

    @Basic
    @Column(name = "COMMENTS")
    private String comments;

    public void setComments(String comments){
        this.comments = comments;
    }
    public String getComments(){
        return comments;
    }

    @Basic
    @Column(name = "COUNTY_EXEMPT_AMT")
    private BigDecimal countyExemptAmt;

    public void setCountyExemptAmt(BigDecimal countyExemptAmt){
        this.countyExemptAmt = countyExemptAmt;
    }
    public BigDecimal getCountyExemptAmt(){
        return countyExemptAmt;
    }

    @Basic
    @Column(name = "TRANSACTION_IND")
    private String transactionInd;

    public void setTransactionInd(String transactionInd){
        this.transactionInd = transactionInd;
    }
    public String getTransactionInd(){
        return transactionInd;
    }

    @Basic
    @Column(name = "GL_EXTRACT_FLAG")
    private String glExtractFlag;

    public void setGlExtractFlag(String glExtractFlag){
        this.glExtractFlag = glExtractFlag;
    }
    public String getGlExtractFlag(){
        return glExtractFlag;
    }

    @Basic
    @Column(name = "STJ6_TAX_AMT")
    private BigDecimal stj6TaxAmt;

    public void setStj6TaxAmt(BigDecimal stj6TaxAmt){
        this.stj6TaxAmt = stj6TaxAmt;
    }
    public BigDecimal getStj6TaxAmt(){
        return stj6TaxAmt;
    }

    @Basic
    @Column(name = "PROCESSTYPE_CODE")
    private String processtypeCode;

    public void setProcesstypeCode(String processtypeCode){
        this.processtypeCode = processtypeCode;
    }
    public String getProcesstypeCode(){
        return processtypeCode;
    }

    @Basic
    @Column(name = "MANUAL_TAXCODE_IND")
    private String manualTaxcodeInd;

    public void setManualTaxcodeInd(String manualTaxcodeInd){
        this.manualTaxcodeInd = manualTaxcodeInd;
    }
    public String getManualTaxcodeInd(){
        return manualTaxcodeInd;
    }

    @Basic
    @Column(name = "BCP_BILLTRANS_ID")
    private Long bcpBilltransId;

    public void setBcpBilltransId(Long bcpBilltransId){
        this.bcpBilltransId = bcpBilltransId;
    }
    public Long getBcpBilltransId(){
        return bcpBilltransId;
    }

    @Basic
    @Column(name = "COUNTRY_EXEMPT_AMT")
    private BigDecimal countryExemptAmt;

    public void setCountryExemptAmt(BigDecimal countryExemptAmt){
        this.countryExemptAmt = countryExemptAmt;
    }
    public BigDecimal getCountryExemptAmt(){
        return countryExemptAmt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_TYPE")
    private String invoiceLineType;

    public void setInvoiceLineType(String invoiceLineType){
        this.invoiceLineType = invoiceLineType;
    }
    public String getInvoiceLineType(){
        return invoiceLineType;
    }

    @Basic
    @Column(name = "INVOICE_LINE_AMT")
    private BigDecimal invoiceLineAmt;

    public void setInvoiceLineAmt(BigDecimal invoiceLineAmt){
        this.invoiceLineAmt = invoiceLineAmt;
    }
    public BigDecimal getInvoiceLineAmt(){
        return invoiceLineAmt;
    }

    @Basic
    @Column(name = "PROCESS_METHOD")
    private String processMethod;

    public void setProcessMethod(String processMethod){
        this.processMethod = processMethod;
    }
    public String getProcessMethod(){
        return processMethod;
    }

    @Basic
    @Column(name = "INVOICE_FREIGHT_AMT")
    private BigDecimal invoiceFreightAmt;

    public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt){
        this.invoiceFreightAmt = invoiceFreightAmt;
    }
    public BigDecimal getInvoiceFreightAmt(){
        return invoiceFreightAmt;
    }

    @Basic
    @Column(name = "DISTR_06_AMT")
    private BigDecimal distr06Amt;

    public void setDistr06Amt(BigDecimal distr06Amt){
        this.distr06Amt = distr06Amt;
    }
    public BigDecimal getDistr06Amt(){
        return distr06Amt;
    }

    @Basic
    @Column(name = "GL_LOCAL_ACCT_NBR")
    private String glLocalAcctNbr;

    public void setGlLocalAcctNbr(String glLocalAcctNbr){
        this.glLocalAcctNbr = glLocalAcctNbr;
    }
    public String getGlLocalAcctNbr(){
        return glLocalAcctNbr;
    }

    @Basic
    @Column(name = "CUST_ENTITY_ID")
    private Long custEntityId;

    public void setCustEntityId(Long custEntityId){
        this.custEntityId = custEntityId;
    }
    public Long getCustEntityId(){
        return custEntityId;
    }

    @Basic
    @Column(name = "AFE_PROPERTY_CAT")
    private String afePropertyCat;

    public void setAfePropertyCat(String afePropertyCat){
        this.afePropertyCat = afePropertyCat;
    }
    public String getAfePropertyCat(){
        return afePropertyCat;
    }

    @Basic
    @Column(name = "CITY_TIER1_TAX_AMT")
    private BigDecimal cityTier1TaxAmt;

    public void setCityTier1TaxAmt(BigDecimal cityTier1TaxAmt){
        this.cityTier1TaxAmt = cityTier1TaxAmt;
    }
    public BigDecimal getCityTier1TaxAmt(){
        return cityTier1TaxAmt;
    }

    @Basic
    @Column(name = "REJECT_FLAG")
    private String rejectFlag;

    public void setRejectFlag(String rejectFlag){
        this.rejectFlag = rejectFlag;
    }
    public String getRejectFlag(){
        return rejectFlag;
    }

    @Basic
    @Column(name = "PROCESS_BATCH_NO")
    private Long processBatchNo;

    public void setProcessBatchNo(Long processBatchNo){
        this.processBatchNo = processBatchNo;
    }
    public Long getProcessBatchNo(){
        return processBatchNo;
    }

    @Basic
    @Column(name = "INVENTORY_NBR")
    private String inventoryNbr;

    public void setInventoryNbr(String inventoryNbr){
        this.inventoryNbr = inventoryNbr;
    }
    public String getInventoryNbr(){
        return inventoryNbr;
    }

    @Basic
    @Column(name = "TAX_DRIVER_30")
    private String taxDriver30;

    public void setTaxDriver30(String taxDriver30){
        this.taxDriver30 = taxDriver30;
    }
    public String getTaxDriver30(){
        return taxDriver30;
    }

    @Basic
    @Column(name = "STJ5_TAXABLE_AMT")
    private BigDecimal stj5TaxableAmt;

    public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt){
        this.stj5TaxableAmt = stj5TaxableAmt;
    }
    public BigDecimal getStj5TaxableAmt(){
        return stj5TaxableAmt;
    }

    @Basic
    @Column(name = "STJ7_TAXABLE_AMT")
    private BigDecimal stj7TaxableAmt;

    public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt){
        this.stj7TaxableAmt = stj7TaxableAmt;
    }
    public BigDecimal getStj7TaxableAmt(){
        return stj7TaxableAmt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_MISC_AMT")
    private BigDecimal invoiceLineMiscAmt;

    public void setInvoiceLineMiscAmt(BigDecimal invoiceLineMiscAmt){
        this.invoiceLineMiscAmt = invoiceLineMiscAmt;
    }
    public BigDecimal getInvoiceLineMiscAmt(){
        return invoiceLineMiscAmt;
    }

    @Basic
    @Column(name = "GL_LOCAL_ACCT_NAME")
    private String glLocalAcctName;

    public void setGlLocalAcctName(String glLocalAcctName){
        this.glLocalAcctName = glLocalAcctName;
    }
    public String getGlLocalAcctName(){
        return glLocalAcctName;
    }

    @Basic
    @Column(name = "CUST_LOCN_CODE")
    private String custLocnCode;

    public void setCustLocnCode(String custLocnCode){
        this.custLocnCode = custLocnCode;
    }
    public String getCustLocnCode(){
        return custLocnCode;
    }

    @Basic
    @Column(name = "COUNTY_TIER1_TAX_AMT")
    private BigDecimal countyTier1TaxAmt;

    public void setCountyTier1TaxAmt(BigDecimal countyTier1TaxAmt){
        this.countyTier1TaxAmt = countyTier1TaxAmt;
    }
    public BigDecimal getCountyTier1TaxAmt(){
        return countyTier1TaxAmt;
    }

    @Basic
    @Column(name = "PROCRULE_SESSION_ID")
    private String procruleSessionId;

    public void setProcruleSessionId(String procruleSessionId){
        this.procruleSessionId = procruleSessionId;
    }
    public String getProcruleSessionId(){
        return procruleSessionId;
    }

    @Basic
    @Column(name = "STJ3_TAXABLE_AMT")
    private BigDecimal stj3TaxableAmt;

    public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt){
        this.stj3TaxableAmt = stj3TaxableAmt;
    }
    public BigDecimal getStj3TaxableAmt(){
        return stj3TaxableAmt;
    }

    @Basic
    @Column(name = "GL_COMPANY_NBR")
    private String glCompanyNbr;

    public void setGlCompanyNbr(String glCompanyNbr){
        this.glCompanyNbr = glCompanyNbr;
    }
    public String getGlCompanyNbr(){
        return glCompanyNbr;
    }

    @Basic
    @Column(name = "TAXCODE_CODE")
    private String taxcodeCode;

    public void setTaxcodeCode(String taxcodeCode){
        this.taxcodeCode = taxcodeCode;
    }
    public String getTaxcodeCode(){
        return taxcodeCode;
    }

    @Basic
    @Column(name = "STJ1_TAXABLE_AMT")
    private BigDecimal stj1TaxableAmt;

    public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt){
        this.stj1TaxableAmt = stj1TaxableAmt;
    }
    public BigDecimal getStj1TaxableAmt(){
        return stj1TaxableAmt;
    }

    @Basic
    @Column(name = "DISTR_05_AMT")
    private BigDecimal distr05Amt;

    public void setDistr05Amt(BigDecimal distr05Amt){
        this.distr05Amt = distr05Amt;
    }
    public BigDecimal getDistr05Amt(){
        return distr05Amt;
    }

    @Basic
    @Column(name = "CALCULATE_IND")
    private String calculateInd;

    public void setCalculateInd(String calculateInd){
        this.calculateInd = calculateInd;
    }
    public String getCalculateInd(){
        return calculateInd;
    }

    @Basic
    @Column(name = "INVENTORY_CLASS")
    private String inventoryClass;

    public void setInventoryClass(String inventoryClass){
        this.inventoryClass = inventoryClass;
    }
    public String getInventoryClass(){
        return inventoryClass;
    }

    @Basic
    @Column(name = "DISTR_01_AMT")
    private BigDecimal distr01Amt;

    public void setDistr01Amt(BigDecimal distr01Amt){
        this.distr01Amt = distr01Amt;
    }
    public BigDecimal getDistr01Amt(){
        return distr01Amt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_20")
    private String taxDriver20;

    public void setTaxDriver20(String taxDriver20){
        this.taxDriver20 = taxDriver20;
    }
    public String getTaxDriver20(){
        return taxDriver20;
    }

    @Basic
    @Column(name = "TAX_DRIVER_21")
    private String taxDriver21;

    public void setTaxDriver21(String taxDriver21){
        this.taxDriver21 = taxDriver21;
    }
    public String getTaxDriver21(){
        return taxDriver21;
    }

    @Basic
    @Column(name = "TAX_DRIVER_22")
    private String taxDriver22;

    public void setTaxDriver22(String taxDriver22){
        this.taxDriver22 = taxDriver22;
    }
    public String getTaxDriver22(){
        return taxDriver22;
    }

    @Basic
    @Column(name = "TAX_DRIVER_23")
    private String taxDriver23;

    public void setTaxDriver23(String taxDriver23){
        this.taxDriver23 = taxDriver23;
    }
    public String getTaxDriver23(){
        return taxDriver23;
    }

    @Basic
    @Column(name = "TAX_DRIVER_24")
    private String taxDriver24;

    public void setTaxDriver24(String taxDriver24){
        this.taxDriver24 = taxDriver24;
    }
    public String getTaxDriver24(){
        return taxDriver24;
    }

    @Basic
    @Column(name = "TAX_DRIVER_25")
    private String taxDriver25;

    public void setTaxDriver25(String taxDriver25){
        this.taxDriver25 = taxDriver25;
    }
    public String getTaxDriver25(){
        return taxDriver25;
    }

    @Basic
    @Column(name = "TAX_DRIVER_26")
    private String taxDriver26;

    public void setTaxDriver26(String taxDriver26){
        this.taxDriver26 = taxDriver26;
    }
    public String getTaxDriver26(){
        return taxDriver26;
    }

    @Basic
    @Column(name = "TAX_DRIVER_27")
    private String taxDriver27;

    public void setTaxDriver27(String taxDriver27){
        this.taxDriver27 = taxDriver27;
    }
    public String getTaxDriver27(){
        return taxDriver27;
    }

    @Basic
    @Column(name = "TAXABLE_AMT")
    private BigDecimal taxableAmt;

    public void setTaxableAmt(BigDecimal taxableAmt){
        this.taxableAmt = taxableAmt;
    }
    public BigDecimal getTaxableAmt(){
        return taxableAmt;
    }

    @Basic
    @Column(name = "TAX_DRIVER_28")
    private String taxDriver28;

    public void setTaxDriver28(String taxDriver28){
        this.taxDriver28 = taxDriver28;
    }
    public String getTaxDriver28(){
        return taxDriver28;
    }

    @Basic
    @Column(name = "TAX_DRIVER_29")
    private String taxDriver29;

    public void setTaxDriver29(String taxDriver29){
        this.taxDriver29 = taxDriver29;
    }
    public String getTaxDriver29(){
        return taxDriver29;
    }

    @Basic
    @Column(name = "SPLIT_SUBTRANS_ID")
    private Long splitSubtransId;

    public void setSplitSubtransId(Long splitSubtransId){
        this.splitSubtransId = splitSubtransId;
    }
    public Long getSplitSubtransId(){
        return splitSubtransId;
    }

    @Basic
    @Column(name = "PRODUCT_NAME")
    private String productName;

    public void setProductName(String productName){
        this.productName = productName;
    }
    public String getProductName(){
        return productName;
    }

    @Basic
    @Column(name = "CUST_STATE_CODE")
    private String custStateCode;

    public void setCustStateCode(String custStateCode){
        this.custStateCode = custStateCode;
    }
    public String getCustStateCode(){
        return custStateCode;
    }

    @Basic
    @Column(name = "VENDOR_TAX_AMT")
    private BigDecimal vendorTaxAmt;

    public void setVendorTaxAmt(BigDecimal vendorTaxAmt){
        this.vendorTaxAmt = vendorTaxAmt;
    }
    public BigDecimal getVendorTaxAmt(){
        return vendorTaxAmt;
    }

    @Basic
    @Column(name = "TAX_ALLOC_MATRIX_ID")
    private Long taxAllocMatrixId;

    public void setTaxAllocMatrixId(Long taxAllocMatrixId){
        this.taxAllocMatrixId = taxAllocMatrixId;
    }
    public Long getTaxAllocMatrixId(){
        return taxAllocMatrixId;
    }

    @Basic
    @Column(name = "STJ9_TAXABLE_AMT")
    private BigDecimal stj9TaxableAmt;

    public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt){
        this.stj9TaxableAmt = stj9TaxableAmt;
    }
    public BigDecimal getStj9TaxableAmt(){
        return stj9TaxableAmt;
    }

    @Basic
    @Column(name = "COUNTRY_TIER3_TAX_AMT")
    private BigDecimal countryTier3TaxAmt;

    public void setCountryTier3TaxAmt(BigDecimal countryTier3TaxAmt){
        this.countryTier3TaxAmt = countryTier3TaxAmt;
    }
    public BigDecimal getCountryTier3TaxAmt(){
        return countryTier3TaxAmt;
    }

    @Basic
    @Column(name = "STJ2_TAX_AMT")
    private BigDecimal stj2TaxAmt;

    public void setStj2TaxAmt(BigDecimal stj2TaxAmt){
        this.stj2TaxAmt = stj2TaxAmt;
    }
    public BigDecimal getStj2TaxAmt(){
        return stj2TaxAmt;
    }

    @Basic
    @Column(name = "INVOICE_TAX_FLG")
    private String invoiceTaxFlg;

    public void setInvoiceTaxFlg(String invoiceTaxFlg){
        this.invoiceTaxFlg = invoiceTaxFlg;
    }
    public String getInvoiceTaxFlg(){
        return invoiceTaxFlg;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "GL_EXTRACT_TIMESTAMP")
    private Date glExtractTimestamp;

    public void setGlExtractTimestamp(Date glExtractTimestamp){
        this.glExtractTimestamp = glExtractTimestamp;
    }
    public Date getGlExtractTimestamp(){
        return glExtractTimestamp;
    }

    @Basic
    @Column(name = "TAX_MATRIX_ID")
    private Long taxMatrixId;

    public void setTaxMatrixId(Long taxMatrixId){
        this.taxMatrixId = taxMatrixId;
    }
    public Long getTaxMatrixId(){
        return taxMatrixId;
    }

    @Basic
    @Column(name = "GL_CC_NBR_DEPT_ID")
    private String glCcNbrDeptId;

    public void setGlCcNbrDeptId(String glCcNbrDeptId){
        this.glCcNbrDeptId = glCcNbrDeptId;
    }
    public String getGlCcNbrDeptId(){
        return glCcNbrDeptId;
    }

    @Basic
    @Column(name = "GL_LINE_ITM_DIST_AMT")
    private BigDecimal glLineItmDistAmt;

    public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt){
        this.glLineItmDistAmt = glLineItmDistAmt;
    }
    public BigDecimal getGlLineItmDistAmt(){
        return glLineItmDistAmt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_GROSS_AMT")
    private BigDecimal invoiceLineGrossAmt;

    public void setInvoiceLineGrossAmt(BigDecimal invoiceLineGrossAmt){
        this.invoiceLineGrossAmt = invoiceLineGrossAmt;
    }
    public BigDecimal getInvoiceLineGrossAmt(){
        return invoiceLineGrossAmt;
    }

    @Basic
    @Column(name = "STJ6_EXEMPT_AMT")
    private BigDecimal stj6ExemptAmt;

    public void setStj6ExemptAmt(BigDecimal stj6ExemptAmt){
        this.stj6ExemptAmt = stj6ExemptAmt;
    }
    public BigDecimal getStj6ExemptAmt(){
        return stj6ExemptAmt;
    }

    @Basic
    @Column(name = "AFE_CATEGORY_NAME")
    private String afeCategoryName;

    public void setAfeCategoryName(String afeCategoryName){
        this.afeCategoryName = afeCategoryName;
    }
    public String getAfeCategoryName(){
        return afeCategoryName;
    }

    @Basic
    @Column(name = "ENTITY_LOCN_SET_DTL_ID")
    private Long entityLocnSetDtlId;

    public void setEntityLocnSetDtlId(Long entityLocnSetDtlId){
        this.entityLocnSetDtlId = entityLocnSetDtlId;
    }
    public Long getEntityLocnSetDtlId(){
        return entityLocnSetDtlId;
    }

    @Basic
    @Column(name = "INVOICE_LINE_TYPE_NAME")
    private String invoiceLineTypeName;

    public void setInvoiceLineTypeName(String invoiceLineTypeName){
        this.invoiceLineTypeName = invoiceLineTypeName;
    }
    public String getInvoiceLineTypeName(){
        return invoiceLineTypeName;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(name = "CERT_SIGN_DATE")
    private Date certSignDate;

    public void setCertSignDate(Date certSignDate){
        this.certSignDate = certSignDate;
    }
    public Date getCertSignDate(){
        return certSignDate;
    }

    @Basic
    @Column(name = "DISTR_09_AMT")
    private BigDecimal distr09Amt;

    public void setDistr09Amt(BigDecimal distr09Amt){
        this.distr09Amt = distr09Amt;
    }
    public BigDecimal getDistr09Amt(){
        return distr09Amt;
    }

    @Basic
    @Column(name = "INVOICE_LINE_NAME")
    private String invoiceLineName;

    public void setInvoiceLineName(String invoiceLineName){
        this.invoiceLineName = invoiceLineName;
    }
    public String getInvoiceLineName(){
        return invoiceLineName;
    }

    @Basic
    @Column(name = "CUST_ZIP")
    private String custZip;

    public void setCustZip(String custZip){
        this.custZip = custZip;
    }
    public String getCustZip(){
        return custZip;
    }

    @Basic
    @Column(name = "INVOICE_LINE_WEIGHT")
    private BigDecimal invoiceLineWeight;

    public void setInvoiceLineWeight(BigDecimal invoiceLineWeight){
        this.invoiceLineWeight = invoiceLineWeight;
    }
    public BigDecimal getInvoiceLineWeight(){
        return invoiceLineWeight;
    }

    @Basic
    @Column(name = "CUST_ADDRESS_LINE_1")
    private String custAddressLine1;

    public void setCustAddressLine1(String custAddressLine1){
        this.custAddressLine1 = custAddressLine1;
    }
    public String getCustAddressLine1(){
        return custAddressLine1;
    }

    @Basic
    @Column(name = "STJ8_TAX_AMT")
    private BigDecimal stj8TaxAmt;

    public void setStj8TaxAmt(BigDecimal stj8TaxAmt){
        this.stj8TaxAmt = stj8TaxAmt;
    }
    public BigDecimal getStj8TaxAmt(){
        return stj8TaxAmt;
    }

    @Basic
    @Column(name = "CUST_ADDRESS_LINE_2")
    private String custAddressLine2;

    public void setCustAddressLine2(String custAddressLine2){
        this.custAddressLine2 = custAddressLine2;
    }
    public String getCustAddressLine2(){
        return custAddressLine2;
    }

    @Basic
    @Column(name = "INVOICE_NBR")
    private String invoiceNbr;

    public void setInvoiceNbr(String invoiceNbr){
        this.invoiceNbr = invoiceNbr;
    }
    public String getInvoiceNbr(){
        return invoiceNbr;
    }

    @Basic
    @Column(name = "INVOICE_LINE_NBR")
    private String invoiceLineNbr;

    public void setInvoiceLineNbr(String invoiceLineNbr){
        this.invoiceLineNbr = invoiceLineNbr;
    }
    public String getInvoiceLineNbr(){
        return invoiceLineNbr;
    }

    @Basic
    @Column(name = "STATE_TIER3_TAX_AMT")
    private BigDecimal stateTier3TaxAmt;

    public void setStateTier3TaxAmt(BigDecimal stateTier3TaxAmt){
        this.stateTier3TaxAmt = stateTier3TaxAmt;
    }
    public BigDecimal getStateTier3TaxAmt(){
        return stateTier3TaxAmt;
    }

    @Basic
    @Column(name = "STJ4_EXEMPT_AMT")
    private BigDecimal stj4ExemptAmt;

    public void setStj4ExemptAmt(BigDecimal stj4ExemptAmt){
        this.stj4ExemptAmt = stj4ExemptAmt;
    }
    public BigDecimal getStj4ExemptAmt(){
        return stj4ExemptAmt;
    }

    @Basic
    @Column(name = "CUST_NBR")
    private String custNbr;

    public void setCustNbr(String custNbr){
        this.custNbr = custNbr;
    }
    public String getCustNbr(){
        return custNbr;
    }

    @Basic
    @Column(name = "CITY_TIER3_TAX_AMT")
    private BigDecimal cityTier3TaxAmt;

    public void setCityTier3TaxAmt(BigDecimal cityTier3TaxAmt){
        this.cityTier3TaxAmt = cityTier3TaxAmt;
    }
    public BigDecimal getCityTier3TaxAmt(){
        return cityTier3TaxAmt;
    }

    @Basic
    @Column(name = "UNLOCK_ENTITY_ID")
    private Long unlockEntityId;

    public void setUnlockEntityId(Long unlockEntityId){
        this.unlockEntityId = unlockEntityId;
    }
    public Long getUnlockEntityId(){
        return unlockEntityId;
    }

    @Basic
    @Column(name = "CUST_NAME")
    private String custName;

    public void setCustName(String custName){
        this.custName = custName;
    }
    public String getCustName(){
        return custName;
    }

    //Start of TB_BILLTRANS_USER
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_01", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText01;
    public String getUserText01() {
        return userText01;
    }

    public void setUserText01(String userText01) {
        this.userText01 = userText01;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_02", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText02;
    public String getUserText02() {
        return userText02;
    }

    public void setUserText02(String userText02) {
        this.userText02 = userText02;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_03", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText03;
    public String getUserText03() {
        return userText03;
    }

    public void setUserText03(String userText03) {
        this.userText03 = userText03;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_04", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText04;
    public String getUserText04() {
        return userText04;
    }

    public void setUserText04(String userText04) {
        this.userText04 = userText04;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_05", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText05;
    public String getUserText05() {
        return userText05;
    }

    public void setUserText05(String userText05) {
        this.userText05 = userText05;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_06", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText06;
    public String getUserText06() {
        return userText06;
    }

    public void setUserText06(String userText06) {
        this.userText06 = userText06;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_07", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText07;
    public String getUserText07() {
        return userText07;
    }

    public void setUserText07(String userText07) {
        this.userText07 = userText07;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_08", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText08;
    public String getUserText08() {
        return userText08;
    }

    public void setUserText08(String userText08) {
        this.userText08 = userText08;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_09", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText09;
    public String getUserText09() {
        return userText09;
    }

    public void setUserText09(String userText09) {
        this.userText09 = userText09;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_10", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText10;
    public String getUserText10() {
        return userText10;
    }

    public void setUserText10(String userText10) {
        this.userText10 = userText10;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_11", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText11;
    public String getUserText11() {
        return userText11;
    }

    public void setUserText11(String userText11) {
        this.userText11 = userText11;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_12", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText12;
    public String getUserText12() {
        return userText12;
    }

    public void setUserText12(String userText12) {
        this.userText12 = userText12;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_13", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText13;
    public String getUserText13() {
        return userText13;
    }

    public void setUserText13(String userText13) {
        this.userText13 = userText13;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_14", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText14;
    public String getUserText14() {
        return userText14;
    }

    public void setUserText14(String userText14) {
        this.userText14 = userText14;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_15", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText15;
    public String getUserText15() {
        return userText15;
    }

    public void setUserText15(String userText15) {
        this.userText15 = userText15;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_16", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText16;
    public String getUserText16() {
        return userText16;
    }

    public void setUserText16(String userText16) {
        this.userText16 = userText16;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_17", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText17;
    public String getUserText17() {
        return userText17;
    }

    public void setUserText17(String userText17) {
        this.userText17 = userText17;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_18", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText18;
    public String getUserText18() {
        return userText18;
    }

    public void setUserText18(String userText18) {
        this.userText18 = userText18;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_19", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText19;
    public String getUserText19() {
        return userText19;
    }

    public void setUserText19(String userText19) {
        this.userText19 = userText19;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_20", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText20;
    public String getUserText20() {
        return userText20;
    }

    public void setUserText20(String userText20) {
        this.userText20 = userText20;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_21", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText21;
    public String getUserText21() {
        return userText21;
    }

    public void setUserText21(String userText21) {
        this.userText21 = userText21;
    }



    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_22", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText22;
    public String getUserText22() {
        return userText22;
    }

    public void setUserText22(String userText22) {
        this.userText22 = userText22;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_23", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText23;
    public String getUserText23() {
        return userText23;
    }

    public void setUserText23(String userText23) {
        this.userText23 = userText23;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_24", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText24;
    public String getUserText24() {
        return userText24;
    }

    public void setUserText24(String userText24) {
        this.userText24 = userText24;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_25", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText25;
    public String getUserText25() {
        return userText25;
    }

    public void setUserText25(String userText25) {
        this.userText25 = userText25;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_26", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText26;
    public String getUserText26() {
        return userText26;
    }

    public void setUserText26(String userText26) {
        this.userText26 = userText26;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_27", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText27;
    public String getUserText27() {
        return userText27;
    }

    public void setUserText27(String userText27) {
        this.userText27 = userText27;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_28", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText28;
    public String getUserText28() {
        return userText28;
    }

    public void setUserText28(String userText28) {
        this.userText28 = userText28;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_29", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText29;
    public String getUserText29() {
        return userText29;
    }

    public void setUserText29(String userText29) {
        this.userText29 = userText29;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER",name = "USER_TEXT_30", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText30;
    public String getUserText30() {
        return userText30;
    }

    public void setUserText30(String userText30) {
        this.userText30 = userText30;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_31", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText31;
    public String getUserText31() {
        return userText31;
    }

    public void setUserText31(String userText31) {
        this.userText31 = userText31;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_32", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText32;
    public String getUserText32() {
        return userText32;
    }

    public void setUserText32(String userText32) {
        this.userText32 = userText32;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_33", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText33;
    public String getUserText33() {
        return userText33;
    }

    public void setUserText33(String userText33) {
        this.userText33 = userText33;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_34", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText34;
    public String getUserText34() {
        return userText34;
    }

    public void setUserText34(String userText34) {
        this.userText34 = userText34;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_35", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText35;
    public String getUserText35() {
        return userText35;
    }

    public void setUserText35(String userText35) {
        this.userText35 = userText35;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_36", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText36;
    public String getUserText36() {
        return userText36;
    }

    public void setUserText36(String userText36) {
        this.userText36 = userText36;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_37", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText37;
    public String getUserText37() {
        return userText37;
    }

    public void setUserText37(String userText37) {
        this.userText37 = userText37;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_38", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText38;
    public String getUserText38() {
        return userText38;
    }

    public void setUserText38(String userText38) {
        this.userText38 = userText38;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_39", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText39;
    public String getUserText39() {
        return userText39;
    }

    public void setUserText39(String userText39) {
        this.userText39 = userText39;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_40", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText40;
    public String getUserText40() {
        return userText40;
    }

    public void setUserText40(String userText40) {
        this.userText40 = userText40;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_41", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText41;
    public String getUserText41() {
        return userText41;
    }

    public void setUserText41(String userText41) {
        this.userText41 = userText41;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_42", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText42;
    public String getUserText42() {
        return userText42;
    }

    public void setUserText42(String userText42) {
        this.userText42 = userText42;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_43", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText43;
    public String getUserText43() {
        return userText43;
    }

    public void setUserText43(String userText43) {
        this.userText43 = userText43;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_44", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText44;
    public String getUserText44() {
        return userText44;
    }

    public void setUserText44(String userText44) {
        this.userText44 = userText44;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_45", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText45;
    public String getUserText45() {
        return userText45;
    }

    public void setUserText45(String userText45) {
        this.userText45 = userText45;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_46", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText46;
    public String getUserText46() {
        return userText46;
    }

    public void setUserText46(String userText46) {
        this.userText46 = userText46;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_47", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText47;
    public String getUserText47() {
        return userText47;
    }

    public void setUserText47(String userText47) {
        this.userText47 = userText47;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_48", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText48;
    public String getUserText48() {
        return userText48;
    }

    public void setUserText48(String userText48) {
        this.userText48 = userText48;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_49", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText49;
    public String getUserText49() {
        return userText49;
    }

    public void setUserText49(String userText49) {
        this.userText49 = userText49;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_50", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText50;
    public String getUserText50() {
        return userText50;
    }

    public void setUserText50(String userText50) {
        this.userText50 = userText50;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_51", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText51;
    public String getUserText51() {
        return userText51;
    }

    public void setUserText51(String userText51) {
        this.userText51 = userText51;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_52", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText52;
    public String getUserText52() {
        return userText52;
    }

    public void setUserText52(String userText52) {
        this.userText52 = userText52;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_53", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText53;
    public String getUserText53() {
        return userText53;
    }

    public void setUserText53(String userText53) {
        this.userText53 = userText53;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_54", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText54;
    public String getUserText54() {
        return userText54;
    }

    public void setUserText54(String userText54) {
        this.userText54 = userText54;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_55", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText55;
    public String getUserText55() {
        return userText55;
    }

    public void setUserText55(String userText55) {
        this.userText55 = userText55;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_56", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText56;
    public String getUserText56() {
        return userText56;
    }

    public void setUserText56(String userText56) {
        this.userText56 = userText56;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_57", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText57;
    public String getUserText57() {
        return userText57;
    }

    public void setUserText57(String userText57) {
        this.userText57 = userText57;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_58", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText58;
    public String getUserText58() {
        return userText58;
    }

    public void setUserText58(String userText58) {
        this.userText58 = userText58;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_59", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText59;
    public String getUserText59() {
        return userText59;
    }

    public void setUserText59(String userText59) {
        this.userText59 = userText59;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_60", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText60;
    public String getUserText60() {
        return userText60;
    }

    public void setUserText60(String userText60) {
        this.userText60 = userText60;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_61", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText61;
    public String getUserText61() {
        return userText61;
    }

    public void setUserText61(String userText61) {
        this.userText61 = userText61;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_62", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText62;
    public String getUserText62() {
        return userText62;
    }

    public void setUserText62(String userText62) {
        this.userText62 = userText62;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_63", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText63;
    public String getUserText63() {
        return userText63;
    }

    public void setUserText63(String userText63) {
        this.userText63 = userText63;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_64", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText64;
    public String getUserText64() {
        return userText64;
    }

    public void setUserText64(String userText64) {
        this.userText64 = userText64;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_65", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText65;
    public String getUserText65() {
        return userText65;
    }

    public void setUserText65(String userText65) {
        this.userText65 = userText65;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_66", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText66;
    public String getUserText66() {
        return userText66;
    }

    public void setUserText66(String userText66) {
        this.userText66 = userText66;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_67", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText67;
    public String getUserText67() {
        return userText67;
    }

    public void setUserText67(String userText67) {
        this.userText67 = userText67;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_68", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText68;
    public String getUserText68() {
        return userText68;
    }

    public void setUserText68(String userText68) {
        this.userText68 = userText68;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_69", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText69;
    public String getUserText69() {
        return userText69;
    }

    public void setUserText69(String userText69) {
        this.userText69 = userText69;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_70", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText70;
    public String getUserText70() {
        return userText70;
    }

    public void setUserText70(String userText70) {
        this.userText70 = userText70;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_71", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText71;
    public String getUserText71() {
        return userText71;
    }

    public void setUserText71(String userText71) {
        this.userText71 = userText71;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_72", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText72;
    public String getUserText72() {
        return userText72;
    }

    public void setUserText72(String userText72) {
        this.userText72 = userText72;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_73", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText73;
    public String getUserText73() {
        return userText73;
    }

    public void setUserText73(String userText73) {
        this.userText73 = userText73;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_74", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText74;
    public String getUserText74() {
        return userText74;
    }

    public void setUserText74(String userText74) {
        this.userText74 = userText74;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_75", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText75;
    public String getUserText75() {
        return userText75;
    }

    public void setUserText75(String userText75) {
        this.userText75 = userText75;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_76", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText76;
    public String getUserText76() {
        return userText76;
    }

    public void setUserText76(String userText76) {
        this.userText76 = userText76;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_77", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText77;
    public String getUserText77() {
        return userText77;
    }

    public void setUserText77(String userText77) {
        this.userText77 = userText77;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_78", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText78;
    public String getUserText78() {
        return userText78;
    }

    public void setUserText78(String userText78) {
        this.userText78 = userText78;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_79", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText79;
    public String getUserText79() {
        return userText79;
    }

    public void setUserText79(String userText79) {
        this.userText79 = userText79;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_80", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText80;
    public String getUserText80() {
        return userText80;
    }

    public void setUserText80(String userText80) {
        this.userText80 = userText80;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_81", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText81;
    public String getUserText81() {
        return userText81;
    }

    public void setUserText81(String userText81) {
        this.userText81 = userText81;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_82", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText82;
    public String getUserText82() {
        return userText82;
    }

    public void setUserText82(String userText82) {
        this.userText82 = userText82;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_83", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText83;
    public String getUserText83() {
        return userText83;
    }

    public void setUserText83(String userText83) {
        this.userText83 = userText83;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_84", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText84;
    public String getUserText84() {
        return userText84;
    }

    public void setUserText84(String userText84) {
        this.userText84 = userText84;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_85", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText85;
    public String getUserText85() {
        return userText85;
    }

    public void setUserText85(String userText85) {
        this.userText85 = userText85;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_86", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText86;
    public String getUserText86() {
        return userText86;
    }

    public void setUserText86(String userText86) {
        this.userText86 = userText86;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_87", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText87;
    public String getUserText87() {
        return userText87;
    }

    public void setUserText87(String userText87) {
        this.userText87 = userText87;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_88", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText88;
    public String getUserText88() {
        return userText88;
    }

    public void setUserText88(String userText88) {
        this.userText88 = userText88;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_89", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText89;
    public String getUserText89() {
        return userText89;
    }

    public void setUserText89(String userText89) {
        this.userText89 = userText89;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_90", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText90;
    public String getUserText90() {
        return userText90;
    }

    public void setUserText90(String userText90) {
        this.userText90 = userText90;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_91", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText91;
    public String getUserText91() {
        return userText91;
    }

    public void setUserText91(String userText91) {
        this.userText91 = userText91;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_92", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText92;
    public String getUserText92() {
        return userText92;
    }

    public void setUserText92(String userText92) {
        this.userText92 = userText92;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_93", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText93;
    public String getUserText93() {
        return userText93;
    }

    public void setUserText93(String userText93) {
        this.userText93 = userText93;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_94", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText94;
    public String getUserText94() {
        return userText94;
    }

    public void setUserText94(String userText94) {
        this.userText94 = userText94;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_95", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText95;
    public String getUserText95() {
        return userText95;
    }

    public void setUserText95(String userText95) {
        this.userText95 = userText95;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_96", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText96;
    public String getUserText96() {
        return userText96;
    }

    public void setUserText96(String userText96) {
        this.userText96 = userText96;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_97", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText97;
    public String getUserText97() {
        return userText97;
    }

    public void setUserText97(String userText97) {
        this.userText97 = userText97;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_98", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText98;
    public String getUserText98() {
        return userText98;
    }

    public void setUserText98(String userText98) {
        this.userText98 = userText98;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_99", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText99;
    public String getUserText99() {
        return userText99;
    }

    public void setUserText99(String userText99) {
        this.userText99 = userText99;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_TEXT_100", nullable = true, insertable = true, updatable = true, length = 100)
    private String userText100;
    public String getUserText100() {
        return userText100;
    }

    public void setUserText100(String userText100) {
        this.userText100 = userText100;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_01", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber01;
    public BigDecimal getUserNumber01() {
        return userNumber01;
    }

    public void setUserNumber01(BigDecimal userNumber01) {
        this.userNumber01 = userNumber01;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_02", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber02;
    public BigDecimal getUserNumber02() {
        return userNumber02;
    }

    public void setUserNumber02(BigDecimal userNumber02) {
        this.userNumber02 = userNumber02;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_03", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber03;
    public BigDecimal getUserNumber03() {
        return userNumber03;
    }

    public void setUserNumber03(BigDecimal userNumber03) {
        this.userNumber03 = userNumber03;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_04", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber04;
    public BigDecimal getUserNumber04() {
        return userNumber04;
    }

    public void setUserNumber04(BigDecimal userNumber04) {
        this.userNumber04 = userNumber04;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_05", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber05;
    public BigDecimal getUserNumber05() {
        return userNumber05;
    }

    public void setUserNumber05(BigDecimal userNumber05) {
        this.userNumber05 = userNumber05;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_06", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber06;
    public BigDecimal getUserNumber06() {
        return userNumber06;
    }

    public void setUserNumber06(BigDecimal userNumber06) {
        this.userNumber06 = userNumber06;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_07", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber07;
    public BigDecimal getUserNumber07() {
        return userNumber07;
    }

    public void setUserNumber07(BigDecimal userNumber07) {
        this.userNumber07 = userNumber07;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_08", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber08;
    public BigDecimal getUserNumber08() {
        return userNumber08;
    }

    public void setUserNumber08(BigDecimal userNumber08) {
        this.userNumber08 = userNumber08;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_09", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber09;
    public BigDecimal getUserNumber09() {
        return userNumber09;
    }

    public void setUserNumber09(BigDecimal userNumber09) {
        this.userNumber09 = userNumber09;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_10", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber10;
    public BigDecimal getUserNumber10() {
        return userNumber10;
    }

    public void setUserNumber10(BigDecimal userNumber10) {
        this.userNumber10 = userNumber10;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_11", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber11;
    public BigDecimal getUserNumber11() {
        return userNumber11;
    }

    public void setUserNumber11(BigDecimal userNumber11) {
        this.userNumber11 = userNumber11;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_12", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber12;
    public BigDecimal getUserNumber12() {
        return userNumber12;
    }

    public void setUserNumber12(BigDecimal userNumber12) {
        this.userNumber12 = userNumber12;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_13", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber13;
    public BigDecimal getUserNumber13() {
        return userNumber13;
    }

    public void setUserNumber13(BigDecimal userNumber13) {
        this.userNumber13 = userNumber13;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_14", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber14;
    public BigDecimal getUserNumber14() {
        return userNumber14;
    }

    public void setUserNumber14(BigDecimal userNumber14) {
        this.userNumber14 = userNumber14;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_15", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber15;
    public BigDecimal getUserNumber15() {
        return userNumber15;
    }

    public void setUserNumber15(BigDecimal userNumber15) {
        this.userNumber15 = userNumber15;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_16", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber16;
    public BigDecimal getUserNumber16() {
        return userNumber16;
    }

    public void setUserNumber16(BigDecimal userNumber16) {
        this.userNumber16 = userNumber16;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_17", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber17;
    public BigDecimal getUserNumber17() {
        return userNumber17;
    }

    public void setUserNumber17(BigDecimal userNumber17) {
        this.userNumber17 = userNumber17;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_18", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber18;
    public BigDecimal getUserNumber18() {
        return userNumber18;
    }

    public void setUserNumber18(BigDecimal userNumber18) {
        this.userNumber18 = userNumber18;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_19", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber19;
    public BigDecimal getUserNumber19() {
        return userNumber19;
    }

    public void setUserNumber19(BigDecimal userNumber19) {
        this.userNumber19 = userNumber19;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_20", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber20;
    public BigDecimal getUserNumber20() {
        return userNumber20;
    }

    public void setUserNumber20(BigDecimal userNumber20) {
        this.userNumber20 = userNumber20;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_21", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber21;
    public BigDecimal getUserNumber21() {
        return userNumber21;
    }

    public void setUserNumber21(BigDecimal userNumber21) {
        this.userNumber21 = userNumber21;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_22", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber22;
    public BigDecimal getUserNumber22() {
        return userNumber22;
    }

    public void setUserNumber22(BigDecimal userNumber22) {
        this.userNumber22 = userNumber22;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_23", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber23;
    public BigDecimal getUserNumber23() {
        return userNumber23;
    }

    public void setUserNumber23(BigDecimal userNumber23) {
        this.userNumber23 = userNumber23;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_24", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber24;
    public BigDecimal getUserNumber24() {
        return userNumber24;
    }

    public void setUserNumber24(BigDecimal userNumber24) {
        this.userNumber24 = userNumber24;
    }

    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_NUMBER_25", nullable = true, insertable = true, updatable = true, precision = -127)
    private BigDecimal userNumber25;
    public BigDecimal getUserNumber25() {
        return userNumber25;
    }

    public void setUserNumber25(BigDecimal userNumber25) {
        this.userNumber25 = userNumber25;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_01", nullable = true, insertable = true, updatable = true)
    private Date userDate01;
    public Date getUserDate01() {
        return userDate01;
    }

    public void setUserDate01(Date userDate01) {
        this.userDate01 = userDate01;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_02", nullable = true, insertable = true, updatable = true)
    private Date userDate02;
    public Date getUserDate02() {
        return userDate02;
    }

    public void setUserDate02(Date userDate02) {
        this.userDate02 = userDate02;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_03", nullable = true, insertable = true, updatable = true)
    private Date userDate03;
    public Date getUserDate03() {
        return userDate03;
    }

    public void setUserDate03(Date userDate03) {
        this.userDate03 = userDate03;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_04", nullable = true, insertable = true, updatable = true)
    private Date userDate04;
    public Date getUserDate04() {
        return userDate04;
    }

    public void setUserDate04(Date userDate04) {
        this.userDate04 = userDate04;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_05", nullable = true, insertable = true, updatable = true)
    private Date userDate05;
    public Date getUserDate05() {
        return userDate05;
    }

    public void setUserDate05(Date userDate05) {
        this.userDate05 = userDate05;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_06", nullable = true, insertable = true, updatable = true)
    private Date userDate06;
    public Date getUserDate06() {
        return userDate06;
    }

    public void setUserDate06(Date userDate06) {
        this.userDate06 = userDate06;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_07", nullable = true, insertable = true, updatable = true)
    private Date userDate07;
    public Date getUserDate07() {
        return userDate07;
    }

    public void setUserDate07(Date userDate07) {
        this.userDate07 = userDate07;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_08", nullable = true, insertable = true, updatable = true)
    private Date userDate08;
    public Date getUserDate08() {
        return userDate08;
    }

    public void setUserDate08(Date userDate08) {
        this.userDate08 = userDate08;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_09", nullable = true, insertable = true, updatable = true)
    private Date userDate09;
    public Date getUserDate09() {
        return userDate09;
    }

    public void setUserDate09(Date userDate09) {
        this.userDate09 = userDate09;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_10", nullable = true, insertable = true, updatable = true)
    private Date userDate10;
    public Date getUserDate10() {
        return userDate10;
    }

    public void setUserDate10(Date userDate10) {
        this.userDate10 = userDate10;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_11", nullable = true, insertable = true, updatable = true)
    private Date userDate11;
    public Date getUserDate11() {
        return userDate11;
    }

    public void setUserDate11(Date userDate11) {
        this.userDate11 = userDate11;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_12", nullable = true, insertable = true, updatable = true)
    private Date userDate12;
    public Date getUserDate12() {
        return userDate12;
    }

    public void setUserDate12(Date userDate12) {
        this.userDate12 = userDate12;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_13", nullable = true, insertable = true, updatable = true)
    private Date userDate13;
    public Date getUserDate13() {
        return userDate13;
    }

    public void setUserDate13(Date userDate13) {
        this.userDate13 = userDate13;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_14", nullable = true, insertable = true, updatable = true)
    private Date userDate14;
    public Date getUserDate14() {
        return userDate14;
    }

    public void setUserDate14(Date userDate14) {
        this.userDate14 = userDate14;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_15", nullable = true, insertable = true, updatable = true)
    private Date userDate15;
    public Date getUserDate15() {
        return userDate15;
    }

    public void setUserDate15(Date userDate15) {
        this.userDate15 = userDate15;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_16", nullable = true, insertable = true, updatable = true)
    private Date userDate16;
    public Date getUserDate16() {
        return userDate16;
    }

    public void setUserDate16(Date userDate16) {
        this.userDate16 = userDate16;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_17", nullable = true, insertable = true, updatable = true)
    private Date userDate17;
    public Date getUserDate17() {
        return userDate17;
    }

    public void setUserDate17(Date userDate17) {
        this.userDate17 = userDate17;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_18", nullable = true, insertable = true, updatable = true)
    private Date userDate18;
    public Date getUserDate18() {
        return userDate18;
    }

    public void setUserDate18(Date userDate18) {
        this.userDate18 = userDate18;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_19", nullable = true, insertable = true, updatable = true)
    private Date userDate19;
    public Date getUserDate19() {
        return userDate19;
    }

    public void setUserDate19(Date userDate19) {
        this.userDate19 = userDate19;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_20", nullable = true, insertable = true, updatable = true)
    private Date userDate20;
    public Date getUserDate20() {
        return userDate20;
    }

    public void setUserDate20(Date userDate20) {
        this.userDate20 = userDate20;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_21", nullable = true, insertable = true, updatable = true)
    private Date userDate21;
    public Date getUserDate21() {
        return userDate21;
    }

    public void setUserDate21(Date userDate21) {
        this.userDate21 = userDate21;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_22", nullable = true, insertable = true, updatable = true)
    private Date userDate22;
    public Date getUserDate22() {
        return userDate22;
    }

    public void setUserDate22(Date userDate22) {
        this.userDate22 = userDate22;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_23", nullable = true, insertable = true, updatable = true)
    private Date userDate23;
    public Date getUserDate23() {
        return userDate23;
    }

    public void setUserDate23(Date userDate23) {
        this.userDate23 = userDate23;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_24", nullable = true, insertable = true, updatable = true)
    private Date userDate24;
    public Date getUserDate24() {
        return userDate24;
    }

    public void setUserDate24(Date userDate24) {
        this.userDate24 = userDate24;
    }

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @Basic
    @Column(table="TB_BILLTRANS_USER", name = "USER_DATE_25", nullable = true, insertable = true, updatable = true)
    private Date userDate25;
    public Date getUserDate25() {
        return userDate25;
    }

    public void setUserDate25(Date userDate25) {
        this.userDate25 = userDate25;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_LOCATION", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoLocation;
    public String getShiptoLocation() {
        return shiptoLocation;
    }

    public void setShiptoLocation(String shiptoLocation) {
        this.shiptoLocation = shiptoLocation;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_LOCATION_NAME", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoLocationName;
    public String getShiptoLocationName() {
        return shiptoLocationName;
    }

    public void setShiptoLocationName(String shiptoLocationName) {
        this.shiptoLocationName = shiptoLocationName;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_MANUAL_JUR_IND", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoManualJurInd;
    public String getShiptoManualJurInd() {
        return shiptoManualJurInd;
    }

    public void setShiptoManualJurInd(String shiptoManualJurInd) {
        this.shiptoManualJurInd = shiptoManualJurInd;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shiptoEntityId;
    public Long getShiptoEntityId() {
        return shiptoEntityId;
    }

    public void setShiptoEntityId(Long shiptoEntityId) {
        this.shiptoEntityId = shiptoEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoEntityCode;
    public String getShiptoEntityCode() {
        return shiptoEntityCode;
    }

    public void setShiptoEntityCode(String shiptoEntityCode) {
        this.shiptoEntityCode = shiptoEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoLat;
    public String getShiptoLat() {
        return shiptoLat;
    }

    public void setShiptoLat(String shiptoLat) {
        this.shiptoLat = shiptoLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoLong;
    public String getShiptoLong() {
        return shiptoLong;
    }

    public void setShiptoLong(String shiptoLong) {
        this.shiptoLong = shiptoLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shiptoLocnMatrixId;
    public Long getShiptoLocnMatrixId() {
        return shiptoLocnMatrixId;
    }

    public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId) {
        this.shiptoLocnMatrixId = shiptoLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shiptoJurisdictionId;
    public Long getShiptoJurisdictionId() {
        return shiptoJurisdictionId;
    }

    public void setShiptoJurisdictionId(Long shiptoJurisdictionId) {
        this.shiptoJurisdictionId = shiptoJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String shiptoGeocode;
    public String getShiptoGeocode() {
        return shiptoGeocode;
    }

    public void setShiptoGeocode(String shiptoGeocode) {
        this.shiptoGeocode = shiptoGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoAddressLine1;
    public String getShiptoAddressLine1() {
        return shiptoAddressLine1;
    }

    public void setShiptoAddressLine1(String shiptoAddressLine1) {
        this.shiptoAddressLine1 = shiptoAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String shiptoAddressLine2;
    public String getShiptoAddressLine2() {
        return shiptoAddressLine2;
    }

    public void setShiptoAddressLine2(String shiptoAddressLine2) {
        this.shiptoAddressLine2 = shiptoAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String shiptoCity;
    public String getShiptoCity() {
        return shiptoCity;
    }

    public void setShiptoCity(String shiptoCity) {
        this.shiptoCity = shiptoCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String shiptoCounty;
    public String getShiptoCounty() {
        return shiptoCounty;
    }

    public void setShiptoCounty(String shiptoCounty) {
        this.shiptoCounty = shiptoCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStateCode;
    public String getShiptoStateCode() {
        return shiptoStateCode;
    }

    public void setShiptoStateCode(String shiptoStateCode) {
        this.shiptoStateCode = shiptoStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoZip;
    public String getShiptoZip() {
        return shiptoZip;
    }

    public void setShiptoZip(String shiptoZip) {
        this.shiptoZip = shiptoZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoZipplus4;
    public String getShiptoZipplus4() {
        return shiptoZipplus4;
    }

    public void setShiptoZipplus4(String shiptoZipplus4) {
        this.shiptoZipplus4 = shiptoZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoCountryCode;
    public String getShiptoCountryCode() {
        return shiptoCountryCode;
    }

    public void setShiptoCountryCode(String shiptoCountryCode) {
        this.shiptoCountryCode = shiptoCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shiptoStj1Name;
    public String getShiptoStj1Name() {
        return shiptoStj1Name;
    }

    public void setShiptoStj1Name(String shiptoStj1Name) {
        this.shiptoStj1Name = shiptoStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shiptoStj2Name;
    public String getShiptoStj2Name() {
        return shiptoStj2Name;
    }

    public void setShiptoStj2Name(String shiptoStj2Name) {
        this.shiptoStj2Name = shiptoStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shiptoStj3Name;
    public String getShiptoStj3Name() {
        return shiptoStj3Name;
    }

    public void setShiptoStj3Name(String shiptoStj3Name) {
        this.shiptoStj3Name = shiptoStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shiptoStj4Name;
    public String getShiptoStj4Name() {
        return shiptoStj4Name;
    }

    public void setShiptoStj4Name(String shiptoStj4Name) {
        this.shiptoStj4Name = shiptoStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shiptoStj5Name;
    public String getShiptoStj5Name() {
        return shiptoStj5Name;
    }

    public void setShiptoStj5Name(String shiptoStj5Name) {
        this.shiptoStj5Name = shiptoStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoCountryNexusindCode;
    public String getShiptoCountryNexusindCode() {
        return shiptoCountryNexusindCode;
    }

    public void setShiptoCountryNexusindCode(String shiptoCountryNexusindCode) {
        this.shiptoCountryNexusindCode = shiptoCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStateNexusindCode;
    public String getShiptoStateNexusindCode() {
        return shiptoStateNexusindCode;
    }

    public void setShiptoStateNexusindCode(String shiptoStateNexusindCode) {
        this.shiptoStateNexusindCode = shiptoStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoCountyNexusindCode;
    public String getShiptoCountyNexusindCode() {
        return shiptoCountyNexusindCode;
    }

    public void setShiptoCountyNexusindCode(String shiptoCountyNexusindCode) {
        this.shiptoCountyNexusindCode = shiptoCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoCityNexusindCode;
    public String getShiptoCityNexusindCode() {
        return shiptoCityNexusindCode;
    }

    public void setShiptoCityNexusindCode(String shiptoCityNexusindCode) {
        this.shiptoCityNexusindCode = shiptoCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStj1NexusindCode;
    public String getShiptoStj1NexusindCode() {
        return shiptoStj1NexusindCode;
    }

    public void setShiptoStj1NexusindCode(String shiptoStj1NexusindCode) {
        this.shiptoStj1NexusindCode = shiptoStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStj2NexusindCode;
    public String getShiptoStj2NexusindCode() {
        return shiptoStj2NexusindCode;
    }

    public void setShiptoStj2NexusindCode(String shiptoStj2NexusindCode) {
        this.shiptoStj2NexusindCode = shiptoStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStj3NexusindCode;
    public String getShiptoStj3NexusindCode() {
        return shiptoStj3NexusindCode;
    }

    public void setShiptoStj3NexusindCode(String shiptoStj3NexusindCode) {
        this.shiptoStj3NexusindCode = shiptoStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStj4NexusindCode;
    public String getShiptoStj4NexusindCode() {
        return shiptoStj4NexusindCode;
    }

    public void setShiptoStj4NexusindCode(String shiptoStj4NexusindCode) {
        this.shiptoStj4NexusindCode = shiptoStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPTO_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shiptoStj5NexusindCode;
    public String getShiptoStj5NexusindCode() {
        return shiptoStj5NexusindCode;
    }

    public void setShiptoStj5NexusindCode(String shiptoStj5NexusindCode) {
        this.shiptoStj5NexusindCode = shiptoStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shipfromEntityId;
    public Long getShipfromEntityId() {
        return shipfromEntityId;
    }

    public void setShipfromEntityId(Long shipfromEntityId) {
        this.shipfromEntityId = shipfromEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromEntityCode;
    public String getShipfromEntityCode() {
        return shipfromEntityCode;
    }

    public void setShipfromEntityCode(String shipfromEntityCode) {
        this.shipfromEntityCode = shipfromEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String shipfromLat;
    public String getShipfromLat() {
        return shipfromLat;
    }

    public void setShipfromLat(String shipfromLat) {
        this.shipfromLat = shipfromLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String shipfromLong;
    public String getShipfromLong() {
        return shipfromLong;
    }

    public void setShipfromLong(String shipfromLong) {
        this.shipfromLong = shipfromLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shipfromLocnMatrixId;
    public Long getShipfromLocnMatrixId() {
        return shipfromLocnMatrixId;
    }

    public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId) {
        this.shipfromLocnMatrixId = shipfromLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long shipfromJurisdictionId;
    public Long getShipfromJurisdictionId() {
        return shipfromJurisdictionId;
    }

    public void setShipfromJurisdictionId(Long shipfromJurisdictionId) {
        this.shipfromJurisdictionId = shipfromJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String shipfromGeocode;
    public String getShipfromGeocode() {
        return shipfromGeocode;
    }

    public void setShipfromGeocode(String shipfromGeocode) {
        this.shipfromGeocode = shipfromGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String shipfromAddressLine1;
    public String getShipfromAddressLine1() {
        return shipfromAddressLine1;
    }

    public void setShipfromAddressLine1(String shipfromAddressLine1) {
        this.shipfromAddressLine1 = shipfromAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String shipfromAddressLine2;
    public String getShipfromAddressLine2() {
        return shipfromAddressLine2;
    }

    public void setShipfromAddressLine2(String shipfromAddressLine2) {
        this.shipfromAddressLine2 = shipfromAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String shipfromCity;
    public String getShipfromCity() {
        return shipfromCity;
    }

    public void setShipfromCity(String shipfromCity) {
        this.shipfromCity = shipfromCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String shipfromCounty;
    public String getShipfromCounty() {
        return shipfromCounty;
    }

    public void setShipfromCounty(String shipfromCounty) {
        this.shipfromCounty = shipfromCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStateCode;
    public String getShipfromStateCode() {
        return shipfromStateCode;
    }

    public void setShipfromStateCode(String shipfromStateCode) {
        this.shipfromStateCode = shipfromStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromZip;
    public String getShipfromZip() {
        return shipfromZip;
    }

    public void setShipfromZip(String shipfromZip) {
        this.shipfromZip = shipfromZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromZipplus4;
    public String getShipfromZipplus4() {
        return shipfromZipplus4;
    }

    public void setShipfromZipplus4(String shipfromZipplus4) {
        this.shipfromZipplus4 = shipfromZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromCountryCode;
    public String getShipfromCountryCode() {
        return shipfromCountryCode;
    }

    public void setShipfromCountryCode(String shipfromCountryCode) {
        this.shipfromCountryCode = shipfromCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shipfromStj1Name;
    public String getShipfromStj1Name() {
        return shipfromStj1Name;
    }

    public void setShipfromStj1Name(String shipfromStj1Name) {
        this.shipfromStj1Name = shipfromStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shipfromStj2Name;
    public String getShipfromStj2Name() {
        return shipfromStj2Name;
    }

    public void setShipfromStj2Name(String shipfromStj2Name) {
        this.shipfromStj2Name = shipfromStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shipfromStj3Name;
    public String getShipfromStj3Name() {
        return shipfromStj3Name;
    }

    public void setShipfromStj3Name(String shipfromStj3Name) {
        this.shipfromStj3Name = shipfromStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shipfromStj4Name;
    public String getShipfromStj4Name() {
        return shipfromStj4Name;
    }

    public void setShipfromStj4Name(String shipfromStj4Name) {
        this.shipfromStj4Name = shipfromStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String shipfromStj5Name;
    public String getShipfromStj5Name() {
        return shipfromStj5Name;
    }

    public void setShipfromStj5Name(String shipfromStj5Name) {
        this.shipfromStj5Name = shipfromStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromCountryNexusindCode;
    public String getShipfromCountryNexusindCode() {
        return shipfromCountryNexusindCode;
    }

    public void setShipfromCountryNexusindCode(String shipfromCountryNexusindCode) {
        this.shipfromCountryNexusindCode = shipfromCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStateNexusindCode;
    public String getShipfromStateNexusindCode() {
        return shipfromStateNexusindCode;
    }

    public void setShipfromStateNexusindCode(String shipfromStateNexusindCode) {
        this.shipfromStateNexusindCode = shipfromStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromCountyNexusindCode;
    public String getShipfromCountyNexusindCode() {
        return shipfromCountyNexusindCode;
    }

    public void setShipfromCountyNexusindCode(String shipfromCountyNexusindCode) {
        this.shipfromCountyNexusindCode = shipfromCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromCityNexusindCode;
    public String getShipfromCityNexusindCode() {
        return shipfromCityNexusindCode;
    }

    public void setShipfromCityNexusindCode(String shipfromCityNexusindCode) {
        this.shipfromCityNexusindCode = shipfromCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStj1NexusindCode;
    public String getShipfromStj1NexusindCode() {
        return shipfromStj1NexusindCode;
    }

    public void setShipfromStj1NexusindCode(String shipfromStj1NexusindCode) {
        this.shipfromStj1NexusindCode = shipfromStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStj2NexusindCode;
    public String getShipfromStj2NexusindCode() {
        return shipfromStj2NexusindCode;
    }

    public void setShipfromStj2NexusindCode(String shipfromStj2NexusindCode) {
        this.shipfromStj2NexusindCode = shipfromStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStj3NexusindCode;
    public String getShipfromStj3NexusindCode() {
        return shipfromStj3NexusindCode;
    }

    public void setShipfromStj3NexusindCode(String shipfromStj3NexusindCode) {
        this.shipfromStj3NexusindCode = shipfromStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStj4NexusindCode;
    public String getShipfromStj4NexusindCode() { return shipfromStj4NexusindCode; }

    public void setShipfromStj4NexusindCode(String shipfromStj4NexusindCode) {
        this.shipfromStj4NexusindCode = shipfromStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "SHIPFROM_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String shipfromStj5NexusindCode;
    public String getShipfromStj5NexusindCode() { return shipfromStj5NexusindCode; }

    public void setShipfromStj5NexusindCode(String shipfromStj5NexusindCode) {
        this.shipfromStj5NexusindCode = shipfromStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordracptEntityId;
    public Long getOrdracptEntityId() {
        return ordracptEntityId;
    }

    public void setOrdracptEntityId(Long ordracptEntityId) {
        this.ordracptEntityId = ordracptEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptEntityCode;
    public String getOrdracptEntityCode() {
        return ordracptEntityCode;
    }

    public void setOrdracptEntityCode(String ordracptEntityCode) {
        this.ordracptEntityCode = ordracptEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordracptLat;
    public String getOrdracptLat() {
        return ordracptLat;
    }

    public void setOrdracptLat(String ordracptLat) {
        this.ordracptLat = ordracptLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordracptLong;
    public String getOrdracptLong() {
        return ordracptLong;
    }

    public void setOrdracptLong(String ordracptLong) {
        this.ordracptLong = ordracptLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordracptLocnMatrixId;
    public Long getOrdracptLocnMatrixId() {
        return ordracptLocnMatrixId;
    }

    public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId) {
        this.ordracptLocnMatrixId = ordracptLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordracptJurisdictionId;
    public Long getOrdracptJurisdictionId() {
        return ordracptJurisdictionId;
    }

    public void setOrdracptJurisdictionId(Long ordracptJurisdictionId) {
        this.ordracptJurisdictionId = ordracptJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String ordracptGeocode;
    public String getOrdracptGeocode() {
        return ordracptGeocode;
    }

    public void setOrdracptGeocode(String ordracptGeocode) {
        this.ordracptGeocode = ordracptGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordracptAddressLine1;
    public String getOrdracptAddressLine1() {
        return ordracptAddressLine1;
    }

    public void setOrdracptAddressLine1(String ordracptAddressLine1) {
        this.ordracptAddressLine1 = ordracptAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordracptAddressLine2;
    public String getOrdracptAddressLine2() {
        return ordracptAddressLine2;
    }

    public void setOrdracptAddressLine2(String ordracptAddressLine2) {
        this.ordracptAddressLine2 = ordracptAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ordracptCity;
    public String getOrdracptCity() {
        return ordracptCity;
    }

    public void setOrdracptCity(String ordracptCity) {
        this.ordracptCity = ordracptCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ordracptCounty;
    public String getOrdracptCounty() {
        return ordracptCounty;
    }

    public void setOrdracptCounty(String ordracptCounty) {
        this.ordracptCounty = ordracptCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStateCode;
    public String getOrdracptStateCode() {
        return ordracptStateCode;
    }

    public void setOrdracptStateCode(String ordracptStateCode) {
        this.ordracptStateCode = ordracptStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptZip;
    public String getOrdracptZip() {
        return ordracptZip;
    }

    public void setOrdracptZip(String ordracptZip) {
        this.ordracptZip = ordracptZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptZipplus4;
    public String getOrdracptZipplus4() {
        return ordracptZipplus4;
    }

    public void setOrdracptZipplus4(String ordracptZipplus4) {
        this.ordracptZipplus4 = ordracptZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptCountryCode;
    public String getOrdracptCountryCode() {
        return ordracptCountryCode;
    }

    public void setOrdracptCountryCode(String ordracptCountryCode) {
        this.ordracptCountryCode = ordracptCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordracptStj1Name;
    public String getOrdracptStj1Name() {
        return ordracptStj1Name;
    }

    public void setOrdracptStj1Name(String ordracptStj1Name) {
        this.ordracptStj1Name = ordracptStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordracptStj2Name;
    public String getOrdracptStj2Name() {
        return ordracptStj2Name;
    }

    public void setOrdracptStj2Name(String ordracptStj2Name) {
        this.ordracptStj2Name = ordracptStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordracptStj3Name;
    public String getOrdracptStj3Name() {
        return ordracptStj3Name;
    }

    public void setOrdracptStj3Name(String ordracptStj3Name) {
        this.ordracptStj3Name = ordracptStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordracptStj4Name;
    public String getOrdracptStj4Name() {
        return ordracptStj4Name;
    }

    public void setOrdracptStj4Name(String ordracptStj4Name) {
        this.ordracptStj4Name = ordracptStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordracptStj5Name;
    public String getOrdracptStj5Name() {
        return ordracptStj5Name;
    }

    public void setOrdracptStj5Name(String ordracptStj5Name) {
        this.ordracptStj5Name = ordracptStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptCountryNexusindCode;
    public String getOrdracptCountryNexusindCode() {
        return ordracptCountryNexusindCode;
    }

    public void setOrdracptCountryNexusindCode(String ordracptCountryNexusindCode) {
        this.ordracptCountryNexusindCode = ordracptCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStateNexusindCode;
    public String getOrdracptStateNexusindCode() {
        return ordracptStateNexusindCode;
    }

    public void setOrdracptStateNexusindCode(String ordracptStateNexusindCode) {
        this.ordracptStateNexusindCode = ordracptStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptCountyNexusindCode;
    public String getOrdracptCountyNexusindCode() {
        return ordracptCountyNexusindCode;
    }

    public void setOrdracptCountyNexusindCode(String ordracptCountyNexusindCode) {
        this.ordracptCountyNexusindCode = ordracptCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptCityNexusindCode;
    public String getOrdracptCityNexusindCode() {
        return ordracptCityNexusindCode;
    }

    public void setOrdracptCityNexusindCode(String ordracptCityNexusindCode) {
        this.ordracptCityNexusindCode = ordracptCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStj1NexusindCode;
    public String getOrdracptStj1NexusindCode() {
        return ordracptStj1NexusindCode;
    }

    public void setOrdracptStj1NexusindCode(String ordracptStj1NexusindCode) {
        this.ordracptStj1NexusindCode = ordracptStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStj2NexusindCode;
    public String getOrdracptStj2NexusindCode() {
        return ordracptStj2NexusindCode;
    }

    public void setOrdracptStj2NexusindCode(String ordracptStj2NexusindCode) {
        this.ordracptStj2NexusindCode = ordracptStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStj3NexusindCode;
    public String getOrdracptStj3NexusindCode() {
        return ordracptStj3NexusindCode;
    }

    public void setOrdracptStj3NexusindCode(String ordracptStj3NexusindCode) {
        this.ordracptStj3NexusindCode = ordracptStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStj4NexusindCode;
    public String getOrdracptStj4NexusindCode() {
        return ordracptStj4NexusindCode;
    }

    public void setOrdracptStj4NexusindCode(String ordracptStj4NexusindCode) {
        this.ordracptStj4NexusindCode = ordracptStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRACPT_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordracptStj5NexusindCode;
    public String getOrdracptStj5NexusindCode() {
        return ordracptStj5NexusindCode;
    }

    public void setOrdracptStj5NexusindCode(String ordracptStj5NexusindCode) {
        this.ordracptStj5NexusindCode = ordracptStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordrorgnEntityId;
    public Long getOrdrorgnEntityId() {
        return ordrorgnEntityId;
    }

    public void setOrdrorgnEntityId(Long ordrorgnEntityId) {
        this.ordrorgnEntityId = ordrorgnEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnEntityCode;
    public String getOrdrorgnEntityCode() {
        return ordrorgnEntityCode;
    }

    public void setOrdrorgnEntityCode(String ordrorgnEntityCode) {
        this.ordrorgnEntityCode = ordrorgnEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordrorgnLat;
    public String getOrdrorgnLat() {
        return ordrorgnLat;
    }

    public void setOrdrorgnLat(String ordrorgnLat) {
        this.ordrorgnLat = ordrorgnLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordrorgnLong;
    public String getOrdrorgnLong() {
        return ordrorgnLong;
    }

    public void setOrdrorgnLong(String ordrorgnLong) {
        this.ordrorgnLong = ordrorgnLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordrorgnLocnMatrixId;
    public Long getOrdrorgnLocnMatrixId() {
        return ordrorgnLocnMatrixId;
    }

    public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId) {
        this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ordrorgnJurisdictionId;
    public Long getOrdrorgnJurisdictionId() {
        return ordrorgnJurisdictionId;
    }

    public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId) {
        this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String ordrorgnGeocode;
    public String getOrdrorgnGeocode() {
        return ordrorgnGeocode;
    }

    public void setOrdrorgnGeocode(String ordrorgnGeocode) {
        this.ordrorgnGeocode = ordrorgnGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordrorgnAddressLine1;
    public String getOrdrorgnAddressLine1() {
        return ordrorgnAddressLine1;
    }

    public void setOrdrorgnAddressLine1(String ordrorgnAddressLine1) {
        this.ordrorgnAddressLine1 = ordrorgnAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String ordrorgnAddressLine2;
    public String getOrdrorgnAddressLine2() {
        return ordrorgnAddressLine2;
    }

    public void setOrdrorgnAddressLine2(String ordrorgnAddressLine2) {
        this.ordrorgnAddressLine2 = ordrorgnAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ordrorgnCity;
    public String getOrdrorgnCity() {
        return ordrorgnCity;
    }

    public void setOrdrorgnCity(String ordrorgnCity) {
        this.ordrorgnCity = ordrorgnCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ordrorgnCounty;
    public String getOrdrorgnCounty() {
        return ordrorgnCounty;
    }

    public void setOrdrorgnCounty(String ordrorgnCounty) {
        this.ordrorgnCounty = ordrorgnCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStateCode;
    public String getOrdrorgnStateCode() {
        return ordrorgnStateCode;
    }

    public void setOrdrorgnStateCode(String ordrorgnStateCode) {
        this.ordrorgnStateCode = ordrorgnStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnZip;
    public String getOrdrorgnZip() {
        return ordrorgnZip;
    }

    public void setOrdrorgnZip(String ordrorgnZip) {
        this.ordrorgnZip = ordrorgnZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnZipplus4;
    public String getOrdrorgnZipplus4() {
        return ordrorgnZipplus4;
    }

    public void setOrdrorgnZipplus4(String ordrorgnZipplus4) {
        this.ordrorgnZipplus4 = ordrorgnZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnCountryCode;
    public String getOrdrorgnCountryCode() {
        return ordrorgnCountryCode;
    }

    public void setOrdrorgnCountryCode(String ordrorgnCountryCode) {
        this.ordrorgnCountryCode = ordrorgnCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordrorgnStj1Name;
    public String getOrdrorgnStj1Name() {
        return ordrorgnStj1Name;
    }

    public void setOrdrorgnStj1Name(String ordrorgnStj1Name) {
        this.ordrorgnStj1Name = ordrorgnStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordrorgnStj2Name;
    public String getOrdrorgnStj2Name() {
        return ordrorgnStj2Name;
    }

    public void setOrdrorgnStj2Name(String ordrorgnStj2Name) {
        this.ordrorgnStj2Name = ordrorgnStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordrorgnStj3Name;
    public String getOrdrorgnStj3Name() {
        return ordrorgnStj3Name;
    }

    public void setOrdrorgnStj3Name(String ordrorgnStj3Name) {
        this.ordrorgnStj3Name = ordrorgnStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordrorgnStj4Name;
    public String getOrdrorgnStj4Name() {
        return ordrorgnStj4Name;
    }

    public void setOrdrorgnStj4Name(String ordrorgnStj4Name) {
        this.ordrorgnStj4Name = ordrorgnStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ordrorgnStj5Name;
    public String getOrdrorgnStj5Name() {
        return ordrorgnStj5Name;
    }

    public void setOrdrorgnStj5Name(String ordrorgnStj5Name) {
        this.ordrorgnStj5Name = ordrorgnStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnCountryNexusindCode;
    public String getOrdrorgnCountryNexusindCode() {
        return ordrorgnCountryNexusindCode;
    }

    public void setOrdrorgnCountryNexusindCode(String ordrorgnCountryNexusindCode) {
        this.ordrorgnCountryNexusindCode = ordrorgnCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStateNexusindCode;
    public String getOrdrorgnStateNexusindCode() {
        return ordrorgnStateNexusindCode;
    }

    public void setOrdrorgnStateNexusindCode(String ordrorgnStateNexusindCode) {
        this.ordrorgnStateNexusindCode = ordrorgnStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnCountyNexusindCode;
    public String getOrdrorgnCountyNexusindCode() {
        return ordrorgnCountyNexusindCode;
    }

    public void setOrdrorgnCountyNexusindCode(String ordrorgnCountyNexusindCode) {
        this.ordrorgnCountyNexusindCode = ordrorgnCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnCityNexusindCode;
    public String getOrdrorgnCityNexusindCode() {
        return ordrorgnCityNexusindCode;
    }

    public void setOrdrorgnCityNexusindCode(String ordrorgnCityNexusindCode) {
        this.ordrorgnCityNexusindCode = ordrorgnCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStj1NexusindCode;
    public String getOrdrorgnStj1NexusindCode() {
        return ordrorgnStj1NexusindCode;
    }

    public void setOrdrorgnStj1NexusindCode(String ordrorgnStj1NexusindCode) {
        this.ordrorgnStj1NexusindCode = ordrorgnStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStj2NexusindCode;
    public String getOrdrorgnStj2NexusindCode() {
        return ordrorgnStj2NexusindCode;
    }

    public void setOrdrorgnStj2NexusindCode(String ordrorgnStj2NexusindCode) {
        this.ordrorgnStj2NexusindCode = ordrorgnStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStj3NexusindCode;
    public String getOrdrorgnStj3NexusindCode() {
        return ordrorgnStj3NexusindCode;
    }

    public void setOrdrorgnStj3NexusindCode(String shiptoStj3NexusindCode) {
        this.ordrorgnStj3NexusindCode = shiptoStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStj4NexusindCode;
    public String getOrdrorgnStj4NexusindCode() {
        return ordrorgnStj4NexusindCode;
    }

    public void setOrdrorgnStj4NexusindCode(String ordrorgnStj4NexusindCode) {
        this.ordrorgnStj4NexusindCode = ordrorgnStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "ORDRORGN_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ordrorgnStj5NexusindCode;
    public String getOrdrorgnStj5NexusindCode() {
        return ordrorgnStj5NexusindCode;
    }

    public void setOrdrorgnStj5NexusindCode(String ordrorgnStj5NexusindCode) {
        this.ordrorgnStj5NexusindCode = ordrorgnStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long firstuseEntityId;
    public Long getFirstuseEntityId() {
        return firstuseEntityId;
    }

    public void setFirstuseEntityId(Long firstuseEntityId) {
        this.firstuseEntityId = firstuseEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseEntityCode;
    public String getFirstuseEntityCode() {
        return firstuseEntityCode;
    }

    public void setFirstuseEntityCode(String firstuseEntityCode) {
        this.firstuseEntityCode = firstuseEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String firstuseLat;
    public String getFirstuseLat() {
        return firstuseLat;
    }

    public void setFirstuseLat(String firstuseLat) {
        this.firstuseLat = firstuseLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String firstuseLong;
    public String getFirstuseLong() {
        return firstuseLong;
    }

    public void setFirstuseLong(String firstuseLong) {
        this.firstuseLong = firstuseLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long firstuseLocnMatrixId;
    public Long getFirstuseLocnMatrixId() {
        return firstuseLocnMatrixId;
    }

    public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId) {
        this.firstuseLocnMatrixId = firstuseLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long firstuseJurisdictionId;
    public Long getFirstuseJurisdictionId() {
        return firstuseJurisdictionId;
    }

    public void setFirstuseJurisdictionId(Long firstuseJurisdictionId) {
        this.firstuseJurisdictionId = firstuseJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String firstuseGeocode;
    public String getFirstuseGeocode() {
        return firstuseGeocode;
    }

    public void setFirstuseGeocode(String firstuseGeocode) {
        this.firstuseGeocode = firstuseGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String firstuseAddressLine1;
    public String getFirstuseAddressLine1() {
        return firstuseAddressLine1;
    }

    public void setFirstuseAddressLine1(String firstuseAddressLine1) {
        this.firstuseAddressLine1 = firstuseAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String firstuseAddressLine2;
    public String getFirstuseAddressLine2() {
        return firstuseAddressLine2;
    }

    public void setFirstuseAddressLine2(String firstuseAddressLine2) {
        this.firstuseAddressLine2 = firstuseAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String firstuseCity;
    public String getFirstuseCity() {
        return firstuseCity;
    }

    public void setFirstuseCity(String firstuseCity) {
        this.firstuseCity = firstuseCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String firstuseCounty;
    public String getFirstuseCounty() {
        return firstuseCounty;
    }

    public void setFirstuseCounty(String firstuseCounty) {
        this.firstuseCounty = firstuseCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStateCode;
    public String getFirstuseStateCode() {
        return firstuseStateCode;
    }

    public void setFirstuseStateCode(String firstuseStateCode) {
        this.firstuseStateCode = firstuseStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseZip;
    public String getFirstuseZip() {
        return firstuseZip;
    }

    public void setFirstuseZip(String firstuseZip) {
        this.firstuseZip = firstuseZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseZipplus4;
    public String getFirstuseZipplus4() {
        return firstuseZipplus4;
    }

    public void setFirstuseZipplus4(String firstuseZipplus4) {
        this.firstuseZipplus4 = firstuseZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseCountryCode;
    public String getFirstuseCountryCode() {
        return firstuseCountryCode;
    }

    public void setFirstuseCountryCode(String firstuseCountryCode) {
        this.firstuseCountryCode = firstuseCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String firstuseStj1Name;
    public String getFirstuseStj1Name() {
        return firstuseStj1Name;
    }

    public void setFirstuseStj1Name(String firstuseStj1Name) {
        this.firstuseStj1Name = firstuseStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String firstuseStj2Name;
    public String getFirstuseStj2Name() {
        return firstuseStj2Name;
    }

    public void setFirstuseStj2Name(String firstuseStj2Name) {
        this.firstuseStj2Name = firstuseStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String firstuseStj3Name;
    public String getFirstuseStj3Name() {
        return firstuseStj3Name;
    }

    public void setFirstuseStj3Name(String firstuseStj3Name) {
        this.firstuseStj3Name = firstuseStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String firstuseStj4Name;
    public String getFirstuseStj4Name() {
        return firstuseStj4Name;
    }

    public void setFirstuseStj4Name(String firstuseStj4Name) {
        this.firstuseStj4Name = firstuseStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String firstuseStj5Name;
    public String getFirstuseStj5Name() {
        return firstuseStj5Name;
    }

    public void setFirstuseStj5Name(String firstuseStj5Name) {
        this.firstuseStj5Name = firstuseStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseCountryNexusindCode;
    public String getFirstuseCountryNexusindCode() {
        return firstuseCountryNexusindCode;
    }

    public void setFirstuseCountryNexusindCode(String firstuseCountryNexusindCode) {
        this.firstuseCountryNexusindCode = firstuseCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStateNexusindCode;
    public String getFirstuseStateNexusindCode() {
        return firstuseStateNexusindCode;
    }

    public void setFirstuseStateNexusindCode(String firstuseStateNexusindCode) {
        this.firstuseStateNexusindCode = firstuseStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseCountyNexusindCode;
    public String getFirstuseCountyNexusindCode() {
        return firstuseCountyNexusindCode;
    }

    public void setFirstuseCountyNexusindCode(String firstuseCountyNexusindCode) {
        this.firstuseCountyNexusindCode = firstuseCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseCityNexusindCode;
    public String getFirstuseCityNexusindCode() {
        return firstuseCityNexusindCode;
    }

    public void setFirstuseCityNexusindCode(String firstuseCityNexusindCode) {
        this.firstuseCityNexusindCode = firstuseCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStj1NexusindCode;
    public String getFirstuseStj1NexusindCode() {
        return firstuseStj1NexusindCode;
    }

    public void setFirstuseStj1NexusindCode(String firstuseStj1NexusindCode) {
        this.firstuseStj1NexusindCode = firstuseStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStj2NexusindCode;
    public String getFirstuseStj2NexusindCode() {
        return firstuseStj2NexusindCode;
    }

    public void setFirstuseStj2NexusindCode(String firstuseStj2NexusindCode) {
        this.firstuseStj2NexusindCode = firstuseStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStj3NexusindCode;
    public String getFirstuseStj3NexusindCode() {
        return firstuseStj3NexusindCode;
    }

    public void setFirstuseStj3NexusindCode(String firstuseStj3NexusindCode) {
        this.firstuseStj3NexusindCode = firstuseStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStj4NexusindCode;
    public String getFirstuseStj4NexusindCode() {
        return firstuseStj4NexusindCode;
    }

    public void setFirstuseStj4NexusindCode(String firstuseStj4NexusindCode) {
        this.firstuseStj4NexusindCode = firstuseStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "FIRSTUSE_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String firstuseStj5NexusindCode;
    public String getFirstuseStj5NexusindCode() {
        return firstuseStj5NexusindCode;
    }

    public void setFirstuseStj5NexusindCode(String firstuseStj5NexusindCode) {
        this.firstuseStj5NexusindCode = firstuseStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long billtoEntityId;
    public Long getBilltoEntityId() {
        return billtoEntityId;
    }

    public void setBilltoEntityId(Long billtoEntityId) {
        this.billtoEntityId = billtoEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoEntityCode;
    public String getBilltoEntityCode() {
        return billtoEntityCode;
    }

    public void setBilltoEntityCode(String billtoEntityCode) {
        this.billtoEntityCode = billtoEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String billtoLat;
    public String getBilltoLat() {
        return billtoLat;
    }

    public void setBilltoLat(String billtoLat) {
        this.billtoLat = billtoLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String billtoLong;
    public String getBilltoLong() {
        return billtoLong;
    }

    public void setBilltoLong(String billtoLong) {
        this.billtoLong = billtoLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long billtoLocnMatrixId;
    public Long getBilltoLocnMatrixId() {
        return billtoLocnMatrixId;
    }

    public void setBilltoLocnMatrixId(Long billtoLocnMatrixId) {
        this.billtoLocnMatrixId = billtoLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long billtoJurisdictionId;
    public Long getBilltoJurisdictionId() {
        return billtoJurisdictionId;
    }

    public void setBilltoJurisdictionId(Long billtoJurisdictionId) {
        this.billtoJurisdictionId = billtoJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String billtoGeocode;
    public String getBilltoGeocode() {
        return billtoGeocode;
    }

    public void setBilltoGeocode(String billtoGeocode) {
        this.billtoGeocode = billtoGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String billtoAddressLine1;
    public String getBilltoAddressLine1() {
        return billtoAddressLine1;
    }

    public void setBilltoAddressLine1(String billtoAddressLine1) {
        this.billtoAddressLine1 = billtoAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String billtoAddressLine2;
    public String getBilltoAddressLine2() {
        return billtoAddressLine2;
    }

    public void setBilltoAddressLine2(String billtoAddressLine2) {
        this.billtoAddressLine2 = billtoAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String billtoCity;
    public String getBilltoCity() {
        return billtoCity;
    }

    public void setBilltoCity(String billtoCity) {
        this.billtoCity = billtoCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String billtoCounty;
    public String getBilltoCounty() {
        return billtoCounty;
    }

    public void setBilltoCounty(String billtoCounty) {
        this.billtoCounty = billtoCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStateCode;
    public String getBilltoStateCode() {
        return billtoStateCode;
    }

    public void setBilltoStateCode(String billtoStateCode) {
        this.billtoStateCode = billtoStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoZip;
    public String getBilltoZip() {
        return billtoZip;
    }

    public void setBilltoZip(String billtoZip) {
        this.billtoZip = billtoZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoZipplus4;
    public String getBilltoZipplus4() {
        return billtoZipplus4;
    }

    public void setBilltoZipplus4(String billtoZipplus4) {
        this.billtoZipplus4 = billtoZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoCountryCode;
    public String getBilltoCountryCode() {
        return billtoCountryCode;
    }

    public void setBilltoCountryCode(String billtoCountryCode) {
        this.billtoCountryCode = billtoCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String billtoStj1Name;
    public String getBilltoStj1Name() {
        return billtoStj1Name;
    }

    public void setBilltoStj1Name(String billtoStj1Name) {
        this.billtoStj1Name = billtoStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String billtoStj2Name;
    public String getBilltoStj2Name() {
        return billtoStj2Name;
    }

    public void setBilltoStj2Name(String billtoStj2Name) {
        this.billtoStj2Name = billtoStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String billtoStj3Name;
    public String getBilltoStj3Name() {
        return billtoStj3Name;
    }

    public void setBilltoStj3Name(String billtoStj3Name) {
        this.billtoStj3Name = billtoStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String billtoStj4Name;
    public String getBilltoStj4Name() {
        return billtoStj4Name;
    }

    public void setBilltoStj4Name(String billtoStj4Name) {
        this.billtoStj4Name = billtoStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String billtoStj5Name;
    public String getBilltoStj5Name() {
        return billtoStj5Name;
    }

    public void setBilltoStj5Name(String billtoStj5Name) {
        this.billtoStj5Name = billtoStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoCountryNexusindCode;
    public String getBilltoCountryNexusindCode() {
        return billtoCountryNexusindCode;
    }

    public void setBilltoCountryNexusindCode(String billtoCountryNexusindCode) {
        this.billtoCountryNexusindCode = billtoCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStateNexusindCode;
    public String getBilltoStateNexusindCode() {
        return billtoStateNexusindCode;
    }

    public void setBilltoStateNexusindCode(String billtoStateNexusindCode) {
        this.billtoStateNexusindCode = billtoStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoCountyNexusindCode;
    public String getBilltoCountyNexusindCode() {
        return billtoCountyNexusindCode;
    }

    public void setBilltoCountyNexusindCode(String billtoCountyNexusindCode) {
        this.billtoCountyNexusindCode = billtoCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoCityNexusindCode;
    public String getBilltoCityNexusindCode() {
        return billtoCityNexusindCode;
    }

    public void setBilltoCityNexusindCode(String billtoCityNexusindCode) {
        this.billtoCityNexusindCode = billtoCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStj1NexusindCode;
    public String getBilltoStj1NexusindCode() {
        return billtoStj1NexusindCode;
    }

    public void setBilltoStj1NexusindCode(String billtoStj1NexusindCode) {
        this.billtoStj1NexusindCode = billtoStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStj2NexusindCode;
    public String getBilltoStj2NexusindCode() {
        return billtoStj2NexusindCode;
    }

    public void setBilltoStj2NexusindCode(String billtoStj2NexusindCode) {
        this.billtoStj2NexusindCode = billtoStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStj3NexusindCode;
    public String getBilltoStj3NexusindCode() {
        return billtoStj3NexusindCode;
    }

    public void setBilltoStj3NexusindCode(String billtoStj3NexusindCode) {
        this.billtoStj3NexusindCode = billtoStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStj4NexusindCode;
    public String getBilltoStj4NexusindCode() {
        return billtoStj4NexusindCode;
    }

    public void setBilltoStj4NexusindCode(String billtoStj4NexusindCode) {
        this.billtoStj4NexusindCode = billtoStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "BILLTO_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String billtoStj5NexusindCode;
    public String getBilltoStj5NexusindCode() {
        return billtoStj5NexusindCode;
    }

    public void setBilltoStj5NexusindCode(String billtoStj5NexusindCode) {
        this.billtoStj5NexusindCode = billtoStj5NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ENTITY_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ttlxfrEntityId;
    public Long getTtlxfrEntityId() {
        return ttlxfrEntityId;
    }

    public void setTtlxfrEntityId(Long ttlxfrEntityId) {
        this.ttlxfrEntityId = ttlxfrEntityId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ENTITY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrEntityCode;
    public String getTtlxfrEntityCode() {
        return ttlxfrEntityCode;
    }

    public void setTtlxfrEntityCode(String ttlxfrEntityCode) {
        this.ttlxfrEntityCode = ttlxfrEntityCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_LAT", nullable = true, insertable = true, updatable = true, length = 100)
    private String ttlxfrLat;
    public String getTtlxfrLat() {
        return ttlxfrLat;
    }

    public void setTtlxfrLat(String ttlxfrLat) {
        this.ttlxfrLat = ttlxfrLat;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_LONG", nullable = true, insertable = true, updatable = true, length = 100)
    private String ttlxfrLong;
    public String getTtlxfrLong() {
        return ttlxfrLong;
    }

    public void setTtlxfrLong(String ttlxfrLong) {
        this.ttlxfrLong = ttlxfrLong;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_LOCN_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ttlxfrLocnMatrixId;
    public Long getTtlxfrLocnMatrixId() {
        return ttlxfrLocnMatrixId;
    }

    public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId) {
        this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_JURISDICTION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long ttlxfrJurisdictionId;
    public Long getTtlxfrJurisdictionId() {
        return ttlxfrJurisdictionId;
    }

    public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId) {
        this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_GEOCODE", nullable = true, insertable = true, updatable = true, length = 12)
    private String ttlxfrGeocode;
    public String getTtlxfrGeocode() {
        return ttlxfrGeocode;
    }

    public void setTtlxfrGeocode(String ttlxfrGeocode) {
        this.ttlxfrGeocode = ttlxfrGeocode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ADDRESS_LINE_1", nullable = true, insertable = true, updatable = true, length = 100)
    private String ttlxfrAddressLine1;
    public String getTtlxfrAddressLine1() {
        return ttlxfrAddressLine1;
    }

    public void setTtlxfrAddressLine1(String ttlxfrAddressLine1) {
        this.ttlxfrAddressLine1 = ttlxfrAddressLine1;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ADDRESS_LINE_2", nullable = true, insertable = true, updatable = true, length = 100)
    private String ttlxfrAddressLine2;
    public String getTtlxfrAddressLine2() {
        return ttlxfrAddressLine2;
    }

    public void setTtlxfrAddressLine2(String ttlxfrAddressLine2) {
        this.ttlxfrAddressLine2 = ttlxfrAddressLine2;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_CITY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ttlxfrCity;
    public String getTtlxfrCity() {
        return ttlxfrCity;
    }

    public void setTtlxfrCity(String ttlxfrCity) {
        this.ttlxfrCity = ttlxfrCity;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_COUNTY", nullable = true, insertable = true, updatable = true, length = 50)
    private String ttlxfrCounty;
    public String getTtlxfrCounty() {
        return ttlxfrCounty;
    }

    public void setTtlxfrCounty(String ttlxfrCounty) {
        this.ttlxfrCounty = ttlxfrCounty;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStateCode;
    public String getTtlxfrStateCode() {
        return ttlxfrStateCode;
    }

    public void setTtlxfrStateCode(String ttlxfrStateCode) {
        this.ttlxfrStateCode = ttlxfrStateCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ZIP", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrZip;
    public String getTtlxfrZip() {
        return ttlxfrZip;
    }

    public void setTtlxfrZip(String ttlxfrZip) {
        this.ttlxfrZip = ttlxfrZip;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_ZIPPLUS4", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrZipplus4;
    public String getTtlxfrZipplus4() {
        return ttlxfrZipplus4;
    }

    public void setTtlxfrZipplus4(String ttlxfrZipplus4) {
        this.ttlxfrZipplus4 = ttlxfrZipplus4;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrCountryCode;
    public String getTtlxfrCountryCode() {
        return ttlxfrCountryCode;
    }

    public void setTtlxfrCountryCode(String ttlxfrCountryCode) {
        this.ttlxfrCountryCode = ttlxfrCountryCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ttlxfrStj1Name;
    public String getTtlxfrStj1Name() {
        return ttlxfrStj1Name;
    }

    public void setTtlxfrStj1Name(String ttlxfrStj1Name) {
        this.ttlxfrStj1Name = ttlxfrStj1Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ttlxfrStj2Name;
    public String getTtlxfrStj2Name() {
        return ttlxfrStj2Name;
    }

    public void setTtlxfrStj2Name(String ttlxfrStj2Name) {
        this.ttlxfrStj2Name = ttlxfrStj2Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ttlxfrStj3Name;
    public String getTtlxfrStj3Name() {
        return ttlxfrStj3Name;
    }

    public void setTtlxfrStj3Name(String ttlxfrStj3Name) {
        this.ttlxfrStj3Name = ttlxfrStj3Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ttlxfrStj4Name;
    public String getTtlxfrStj4Name() {
        return ttlxfrStj4Name;
    }

    public void setTtlxfrStj4Name(String ttlxfrStj4Name) {
        this.ttlxfrStj4Name = ttlxfrStj4Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String ttlxfrStj5Name;
    public String getTtlxfrStj5Name() {
        return ttlxfrStj5Name;
    }

    public void setTtlxfrStj5Name(String ttlxfrStj5Name) {
        this.ttlxfrStj5Name = ttlxfrStj5Name;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrCountryNexusindCode;
    public String getTtlxfrCountryNexusindCode() {
        return ttlxfrCountryNexusindCode;
    }

    public void setTtlxfrCountryNexusindCode(String ttlxfrCountryNexusindCode) {
        this.ttlxfrCountryNexusindCode = ttlxfrCountryNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStateNexusindCode;
    public String getTtlxfrStateNexusindCode() {
        return ttlxfrStateNexusindCode;
    }

    public void setTtlxfrStateNexusindCode(String ttlxfrStateNexusindCode) {
        this.ttlxfrStateNexusindCode = ttlxfrStateNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrCountyNexusindCode;
    public String getTtlxfrCountyNexusindCode() {
        return ttlxfrCountyNexusindCode;
    }

    public void setTtlxfrCountyNexusindCode(String ttlxfrCountyNexusindCode) {
        this.ttlxfrCountyNexusindCode = ttlxfrCountyNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrCityNexusindCode;
    public String getTtlxfrCityNexusindCode() {
        return ttlxfrCityNexusindCode;
    }

    public void setTtlxfrCityNexusindCode(String ttlxfrCityNexusindCode) {
        this.ttlxfrCityNexusindCode = ttlxfrCityNexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStj1NexusindCode;
    public String getTtlxfrStj1NexusindCode() {
        return ttlxfrStj1NexusindCode;
    }

    public void setTtlxfrStj1NexusindCode(String ttlxfrStj1NexusindCode) {
        this.ttlxfrStj1NexusindCode = ttlxfrStj1NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStj2NexusindCode;
    public String getTtlxfrStj2NexusindCode() {
        return ttlxfrStj2NexusindCode;
    }

    public void setTtlxfrStj2NexusindCode(String ttlxfrStj2NexusindCode) {
        this.ttlxfrStj2NexusindCode = ttlxfrStj2NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStj3NexusindCode;
    public String getTtlxfrStj3NexusindCode() {
        return ttlxfrStj3NexusindCode;
    }

    public void setTtlxfrStj3NexusindCode(String ttlxfrStj3NexusindCode) {
        this.ttlxfrStj3NexusindCode = ttlxfrStj3NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStj4NexusindCode;
    public String getTtlxfrStj4NexusindCode() {
        return ttlxfrStj4NexusindCode;
    }

    public void setTtlxfrStj4NexusindCode(String ttlxfrStj4NexusindCode) {
        this.ttlxfrStj4NexusindCode = ttlxfrStj4NexusindCode;
    }

    @Basic
    @Column(table="TB_BILLTRANS_LOCN", name = "TTLXFR_STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String ttlxfrStj5NexusindCode;
    public String getTtlxfrStj5NexusindCode() {
        return ttlxfrStj5NexusindCode;
    }

    public void setTtlxfrStj5NexusindCode(String ttlxfrStj5NexusindCode) {
        this.ttlxfrStj5NexusindCode = ttlxfrStj5NexusindCode;
    }

    /*TB_BILLTRANS_JURDTL*/
    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countrySitusMatrixId;
    public Long getCountrySitusMatrixId() {
        return countrySitusMatrixId;
    }

    public void setCountrySitusMatrixId(Long countrySitusMatrixId) {
        this.countrySitusMatrixId = countrySitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stateSitusMatrixId;
    public Long getStateSitusMatrixId() {
        return stateSitusMatrixId;
    }

    public void setStateSitusMatrixId(Long stateSitusMatrixId) {
        this.stateSitusMatrixId = stateSitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countySitusMatrixId;
    public Long getCountySitusMatrixId() {
        return countySitusMatrixId;
    }

    public void setCountySitusMatrixId(Long countySitusMatrixId) {
        this.countySitusMatrixId = countySitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long citySitusMatrixId;
    public Long getCitySitusMatrixId() {
        return citySitusMatrixId;
    }

    public void setCitySitusMatrixId(Long citySitusMatrixId) {
        this.citySitusMatrixId = citySitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj1SitusMatrixId;
    public Long getStj1SitusMatrixId() {
        return stj1SitusMatrixId;
    }

    public void setStj1SitusMatrixId(Long stj1SitusMatrixId) {
        this.stj1SitusMatrixId = stj1SitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj2SitusMatrixId;
    public Long getStj2SitusMatrixId() {
        return stj2SitusMatrixId;
    }

    public void setStj2SitusMatrixId(Long stj2SitusMatrixId) {
        this.stj2SitusMatrixId = stj2SitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj3SitusMatrixId;
    public Long getStj3SitusMatrixId() {
        return stj3SitusMatrixId;
    }

    public void setStj3SitusMatrixId(Long stj3SitusMatrixId) {
        this.stj3SitusMatrixId = stj3SitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj4SitusMatrixId;
    public Long getStj4SitusMatrixId() {
        return stj4SitusMatrixId;
    }

    public void setStj4SitusMatrixId(Long stj4SitusMatrixId) {
        this.stj4SitusMatrixId = stj4SitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_SITUS_MATRIX_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj5SitusMatrixId;
    public Long getStj5SitusMatrixId() {
        return stj5SitusMatrixId;
    }

    public void setStj5SitusMatrixId(Long stj5SitusMatrixId) {
        this.stj5SitusMatrixId = stj5SitusMatrixId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countrySitusCode;
    public String getCountrySitusCode() {
        return countrySitusCode;
    }

    public void setCountrySitusCode(String countrySitusCode) {
        this.countrySitusCode = countrySitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stateSitusCode;
    public String getStateSitusCode() {
        return stateSitusCode;
    }

    public void setStateSitusCode(String stateSitusCode) {
        this.stateSitusCode = stateSitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 50)
    private String countySitusCode;
    public String getCountySitusCode() {
        return countySitusCode;
    }

    public void setCountySitusCode(String countySitusCode) {
        this.countySitusCode = countySitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 50)
    private String citySitusCode;
    public String getCitySitusCode() {
        return citySitusCode;
    }

    public void setCitySitusCode(String citySitusCode) {
        this.citySitusCode = citySitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj1SitusCode;
    public String getStj1SitusCode() {
        return stj1SitusCode;
    }

    public void setStj1SitusCode(String stj1SitusCode) {
        this.stj1SitusCode = stj1SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj2SitusCode;
    public String getStj2SitusCode() {
        return stj2SitusCode;
    }

    public void setStj2SitusCode(String stj2SitusCode) {
        this.stj2SitusCode = stj2SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj3SitusCode;
    public String getStj3SitusCode() { return stj3SitusCode; }

    public void setStj3SitusCode(String stj3SitusCode) { this.stj3SitusCode = stj3SitusCode; }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj4SitusCode;
    public String getStj4SitusCode() {
        return stj4SitusCode;
    }

    public void setStj4SitusCode(String stj4SitusCode) {
        this.stj4SitusCode = stj4SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj5SitusCode;
    public String getStj5SitusCode() {
        return stj5SitusCode;
    }

    public void setStj5SitusCode(String stj5SitusCode) {
        this.stj5SitusCode = stj5SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj6SitusCode;
    public String getStj6SitusCode() {
        return stj6SitusCode;
    }

    public void setStj6SitusCode(String stj6SitusCode) {
        this.stj6SitusCode = stj6SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj7SitusCode;
    public String getStj7SitusCode() {
        return stj7SitusCode;
    }

    public void setStj7SitusCode(String stj7SitusCode) {
        this.stj7SitusCode = stj7SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj8SitusCode;
    public String getStj8SitusCode() {
        return stj8SitusCode;
    }

    public void setStj8SitusCode(String stj8SitusCode) {
        this.stj8SitusCode = stj8SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj9SitusCode;
    public String getStj9SitusCode() {
        return stj9SitusCode;
    }

    public void setStj9SitusCode(String stj9SitusCode) {
        this.stj9SitusCode = stj9SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_SITUS_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj10SitusCode;
    public String getStj10SitusCode() {
        return stj10SitusCode;
    }

    public void setStj10SitusCode(String stj10SitusCode) {
        this.stj10SitusCode = stj10SitusCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countryCode;
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stateCode;
    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_NAME", nullable = true, insertable = true, updatable = true, length = 50)
    private String countyName;
    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_NAME", nullable = true, insertable = true, updatable = true, length = 50)
    private String cityName;
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj1Name;
    public String getStj1Name() {
        return stj1Name;
    }

    public void setStj1Name(String stj1Name) {
        this.stj1Name = stj1Name;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj2Name;
    public String getStj2Name() {
        return stj2Name;
    }

    public void setStj2Name(String stj2Name) {
        this.stj2Name = stj2Name;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj3Name;
    public String getStj3Name() {
        return stj3Name;
    }

    public void setStj3Name(String stj3Name) {
        this.stj3Name = stj3Name;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj4Name;
    public String getStj4Name() {
        return stj4Name;
    }

    public void setStj4Name(String stj4Name) {
        this.stj4Name = stj4Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj5Name;
    public String getStj5Name() {
        return stj5Name;
    }

    public void setStj5Name(String stj5Name) {
        this.stj5Name = stj5Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj6Name;
    public String getStj6Name() {
        return stj6Name;
    }

    public void setStj6Name(String stj6Name) {
        this.stj6Name = stj6Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj7Name;
    public String getStj7Name() {
        return stj7Name;
    }

    public void setStj7Name(String stj7Name) {
        this.stj7Name = stj7Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj8Name;
    public String getStj8Name() {
        return stj8Name;
    }

    public void setStj8Name(String stj8Name) {
        this.stj8Name = stj8Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj9Name;
    public String getStj9Name() {
        return stj9Name;
    }

    public void setStj9Name(String stj9Name) {
        this.stj9Name = stj9Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_NAME", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj10Name;
    public String getStj10Name() {
        return stj10Name;
    }

    public void setStj10Name(String stj10Name) {
        this.stj10Name = stj10Name;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countrySitusJurId;
    public Long getCountrySitusJurId() {
        return countrySitusJurId;
    }

    public void setCountrySitusJurId(Long countrySitusJurId) {
        this.countrySitusJurId = countrySitusJurId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stateSitusJurId;
    public Long getStateSitusJurId() {
        return stateSitusJurId;
    }

    public void setStateSitusJurId(Long stateSitusJurId) {
        this.stateSitusJurId = stateSitusJurId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countySitusJurId;
    public Long getCountySitusJurId() {
        return countySitusJurId;
    }

    public void setCountySitusJurId(Long countySitusJurId) {
        this.countySitusJurId = countySitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long citySitusJurId;
    public Long getCitySitusJurId() {
        return citySitusJurId;
    }

    public void setCitySitusJurId(Long citySitusJurId) {
        this.citySitusJurId = citySitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj1SitusJurId;
    public Long getStj1SitusJurId() {
        return stj1SitusJurId;
    }

    public void setStj1SitusJurId(Long stj1SitusJurId) {
        this.stj1SitusJurId = stj1SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj2SitusJurId;
    public Long getStj2SitusJurId() {
        return stj2SitusJurId;
    }

    public void setStj2SitusJurId(Long stj2SitusJurId) {
        this.stj2SitusJurId = stj2SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj3SitusJurId;
    public Long getStj3SitusJurId() {
        return stj3SitusJurId;
    }

    public void setStj3SitusJurId(Long stj3SitusJurId) {
        this.stj3SitusJurId = stj3SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj4SitusJurId;
    public Long getStj4SitusJurId() {
        return stj4SitusJurId;
    }

    public void setStj4SitusJurId(Long stj4SitusJurId) {
        this.stj4SitusJurId = stj4SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj5SitusJurId;
    public Long getStj5SitusJurId() {
        return stj5SitusJurId;
    }

    public void setStj5SitusJurId(Long stj5SitusJurId) {
        this.stj5SitusJurId = stj5SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj6SitusJurId;
    public Long getStj6SitusJurId() {
        return stj6SitusJurId;
    }

    public void setStj6SitusJurId(Long stj6SitusJurId) {
        this.stj6SitusJurId = stj6SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj7SitusJurId;
    public Long getStj7SitusJurId() {
        return stj7SitusJurId;
    }

    public void setStj7SitusJurId(Long stj7SitusJurId) {
        this.stj7SitusJurId = stj7SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj8SitusJurId;
    public Long getStj8SitusJurId() {
        return stj8SitusJurId;
    }

    public void setStj8SitusJurId(Long stj8SitusJurId) {
        this.stj8SitusJurId = stj8SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj9SitusJurId;
    public Long getStj9SitusJurId() {
        return stj9SitusJurId;
    }

    public void setStj9SitusJurId(Long stj9SitusJurId) {
        this.stj9SitusJurId = stj9SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_SITUS_JUR_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj10SitusJurId;
    public Long getStj10SitusJurId() {
        return stj10SitusJurId;
    }

    public void setStj10SitusJurId(Long stj10SitusJurId) {
        this.stj10SitusJurId = stj10SitusJurId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countryTaxcodeDetailId;
    public Long getCountryTaxcodeDetailId() {
        return countryTaxcodeDetailId;
    }

    public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId) {
        this.countryTaxcodeDetailId = countryTaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stateTaxcodeDetailId;
    public Long getStateTaxcodeDetailId() {
        return stateTaxcodeDetailId;
    }

    public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId) {
        this.stateTaxcodeDetailId = stateTaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countyTaxcodeDetailId;
    public Long getCountyTaxcodeDetailId() {
        return countyTaxcodeDetailId;
    }

    public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId) {
        this.countyTaxcodeDetailId = countyTaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long cityTaxcodeDetailId;
    public Long getCityTaxcodeDetailId() {
        return cityTaxcodeDetailId;
    }

    public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId) {
        this.cityTaxcodeDetailId = cityTaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj1TaxcodeDetailId;
    public Long getStj1TaxcodeDetailId() {
        return stj1TaxcodeDetailId;
    }

    public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId) {
        this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj2TaxcodeDetailId;
    public Long getStj2TaxcodeDetailId() {
        return stj2TaxcodeDetailId;
    }

    public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId) {
        this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj3TaxcodeDetailId;
    public Long getStj3TaxcodeDetailId() {
        return stj3TaxcodeDetailId;
    }

    public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId) {
        this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj4TaxcodeDetailId;
    public Long getStj4TaxcodeDetailId() {
        return stj4TaxcodeDetailId;
    }

    public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId) {
        this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj5TaxcodeDetailId;
    public Long getStj5TaxcodeDetailId() {
        return stj5TaxcodeDetailId;
    }

    public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId) {
        this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj6TaxcodeDetailId;
    public Long getStj6TaxcodeDetailId() {
        return stj6TaxcodeDetailId;
    }

    public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId) {
        this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj7TaxcodeDetailId;
    public Long getStj7TaxcodeDetailId() {
        return stj7TaxcodeDetailId;
    }

    public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId) {
        this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj8TaxcodeDetailId;
    public Long getStj8TaxcodeDetailId() {
        return stj8TaxcodeDetailId;
    }

    public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId) {
        this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj9TaxcodeDetailId;
    public Long getStj9TaxcodeDetailId() {
        return stj9TaxcodeDetailId;
    }

    public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId) {
        this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_TAXCODE_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj10TaxcodeDetailId;
    public Long getStj10TaxcodeDetailId() {
        return stj10TaxcodeDetailId;
    }

    public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId) {
        this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
    }
    
    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countryTaxrateId;
    public Long getCountryTaxrateId() {
        return countryTaxrateId;
    }

    public void setCountryTaxrateId(Long countryTaxrateId) {
        this.countryTaxrateId = countryTaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stateTaxrateId;
    public Long getStateTaxrateId() {
        return stateTaxrateId;
    }

    public void setStateTaxrateId(Long stateTaxrateId) {
        this.stateTaxrateId = stateTaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countyTaxrateId;
    public Long getCountyTaxrateId() {
        return countyTaxrateId;
    }

    public void setCountyTaxrateId(Long countyTaxrateId) {
        this.countyTaxrateId = countyTaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long cityTaxrateId;
    public Long getCityTaxrateId() {
        return cityTaxrateId;
    }

    public void setCityTaxrateId(Long cityTaxrateId) {
        this.cityTaxrateId = cityTaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj1TaxrateId;
    public Long getStj1TaxrateId() {
        return stj1TaxrateId;
    }

    public void setStj1TaxrateId(Long stj1TaxrateId) {
        this.stj1TaxrateId = stj1TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj2TaxrateId;
    public Long getStj2TaxrateId() {
        return stj2TaxrateId;
    }

    public void setStj2TaxrateId(Long stj2TaxrateId) {
        this.stj2TaxrateId = stj2TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj3TaxrateId;
    public Long getStj3TaxrateId() {
        return stj3TaxrateId;
    }

    public void setStj3TaxrateId(Long stj3TaxrateId) {
        this.stj3TaxrateId = stj3TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj4TaxrateId;
    public Long getStj4TaxrateId() {
        return stj4TaxrateId;
    }

    public void setStj4TaxrateId(Long stj4TaxrateId) {
        this.stj4TaxrateId = stj4TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj5TaxrateId;
    public Long getStj5TaxrateId() {
        return stj5TaxrateId;
    }

    public void setStj5TaxrateId(Long stj5TaxrateId) {
        this.stj5TaxrateId = stj5TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj6TaxrateId;
    public Long getStj6TaxrateId() {
        return stj6TaxrateId;
    }

    public void setStj6TaxrateId(Long stj6TaxrateId) {
        this.stj6TaxrateId = stj6TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj7TaxrateId;
    public Long getStj7TaxrateId() {
        return stj7TaxrateId;
    }

    public void setStj7TaxrateId(Long stj7TaxrateId) {
        this.stj7TaxrateId = stj7TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj8TaxrateId;
    public Long getStj8TaxrateId() {
        return stj8TaxrateId;
    }

    public void setStj8TaxrateId(Long stj8TaxrateId) {
        this.stj8TaxrateId = stj8TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj9TaxrateId;
    public Long getStj9TaxrateId() {
        return stj9TaxrateId;
    }

    public void setStj9TaxrateId(Long stj9TaxrateId) {
        this.stj9TaxrateId = stj9TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_TAXRATE_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj10TaxrateId;
    public Long getStj10TaxrateId() {
        return stj10TaxrateId;
    }

    public void setStj10TaxrateId(Long stj10TaxrateId) {
        this.stj10TaxrateId = stj10TaxrateId;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String countryTaxcodeOverFlag;
    public String getCountryTaxcodeOverFlag() {
        return countryTaxcodeOverFlag;
    }

    public void setCountryTaxcodeOverFlag(String countryTaxcodeOverFlag) {
        this.countryTaxcodeOverFlag = countryTaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stateTaxcodeOverFlag;
    public String getStateTaxcodeOverFlag() {
        return stateTaxcodeOverFlag;
    }

    public void setStateTaxcodeOverFlag(String stateTaxcodeOverFlag) {
        this.stateTaxcodeOverFlag = stateTaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String countyTaxcodeOverFlag;
    public String getCountyTaxcodeOverFlag() {
        return countyTaxcodeOverFlag;
    }

    public void setCountyTaxcodeOverFlag(String countyTaxcodeOverFlag) {
        this.countyTaxcodeOverFlag = countyTaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String cityTaxcodeOverFlag;
    public String getCityTaxcodeOverFlag() {
        return cityTaxcodeOverFlag;
    }

    public void setCityTaxcodeOverFlag(String cityTaxcodeOverFlag) {
        this.cityTaxcodeOverFlag = cityTaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj1TaxcodeOverFlag;
    public String getStj1TaxcodeOverFlag() {
        return stj1TaxcodeOverFlag;
    }

    public void setStj1TaxcodeOverFlag(String stj1TaxcodeOverFlag) {
        this.stj1TaxcodeOverFlag = stj1TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj2TaxcodeOverFlag;
    public String getStj2TaxcodeOverFlag() {
        return stj2TaxcodeOverFlag;
    }

    public void setStj2TaxcodeOverFlag(String stj2TaxcodeOverFlag) {
        this.stj2TaxcodeOverFlag = stj2TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj3TaxcodeOverFlag;
    public String getStj3TaxcodeOverFlag() {
        return stj3TaxcodeOverFlag;
    }

    public void setStj3TaxcodeOverFlag(String stj3TaxcodeOverFlag) {
        this.stj3TaxcodeOverFlag = stj3TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj4TaxcodeOverFlag;
    public String getStj4TaxcodeOverFlag() {
        return stj4TaxcodeOverFlag;
    }

    public void setStj4TaxcodeOverFlag(String stj4TaxcodeOverFlag) {
        this.stj4TaxcodeOverFlag = stj4TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj5TaxcodeOverFlag;
    public String getStj5TaxcodeOverFlag() {
        return stj5TaxcodeOverFlag;
    }

    public void setStj5TaxcodeOverFlag(String stj5TaxcodeOverFlag) {
        this.stj5TaxcodeOverFlag = stj5TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj6TaxcodeOverFlag;
    public String getStj6TaxcodeOverFlag() {
        return stj6TaxcodeOverFlag;
    }

    public void setStj6TaxcodeOverFlag(String stj6TaxcodeOverFlag) {
        this.stj6TaxcodeOverFlag = stj6TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj7TaxcodeOverFlag;
    public String getStj7TaxcodeOverFlag() {
        return stj7TaxcodeOverFlag;
    }

    public void setStj7TaxcodeOverFlag(String stj7TaxcodeOverFlag) {
        this.stj7TaxcodeOverFlag = stj7TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj8TaxcodeOverFlag;
    public String getStj8TaxcodeOverFlag() {
        return stj8TaxcodeOverFlag;
    }

    public void setStj8TaxcodeOverFlag(String stj8TaxcodeOverFlag) {
        this.stj8TaxcodeOverFlag = stj8TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj9TaxcodeOverFlag;
    public String getStj9TaxcodeOverFlag() {
        return stj9TaxcodeOverFlag;
    }

    public void setStj9TaxcodeOverFlag(String stj9TaxcodeOverFlag) {
        this.stj9TaxcodeOverFlag = stj9TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_TAXCODE_OVER_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    private String stj10TaxcodeOverFlag;
    public String getStj10TaxcodeOverFlag() {
        return stj10TaxcodeOverFlag;
    }

    public void setStj10TaxcodeOverFlag(String stj10TaxcodeOverFlag) {
        this.stj10TaxcodeOverFlag = stj10TaxcodeOverFlag;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countryTaxtypeUsedCode;
    public String getCountryTaxtypeUsedCode() {
        return countryTaxtypeUsedCode;
    }

    public void setCountryTaxtypeUsedCode(String countryTaxtypeUsedCode) {
        this.countryTaxtypeUsedCode = countryTaxtypeUsedCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stateTaxtypeUsedCode;
    public String getStateTaxtypeUsedCode() {
        return stateTaxtypeUsedCode;
    }

    public void setStateTaxtypeUsedCode(String stateTaxtypeUsedCode) {
        this.stateTaxtypeUsedCode = stateTaxtypeUsedCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countyTaxtypeUsedCode;
    public String getCountyTaxtypeUsedCode() {
        return countyTaxtypeUsedCode;
    }

    public void setCountyTaxtypeUsedCode(String countyTaxtypeUsedCode) {
        this.countyTaxtypeUsedCode = countyTaxtypeUsedCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String cityTaxtypeUsedCode;
    public String getCityTaxtypeUsedCode() {
        return cityTaxtypeUsedCode;
    }

    public void setCityTaxtypeUsedCode(String cityTaxtypeUsedCode) {
        this.cityTaxtypeUsedCode = cityTaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj1TaxtypeUsedCode;
    public String getStj1TaxtypeUsedCode() {
        return stj1TaxtypeUsedCode;
    }

    public void setStj1TaxtypeUsedCode(String stj1TaxtypeUsedCode) {
        this.stj1TaxtypeUsedCode = stj1TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj2TaxtypeUsedCode;
    public String getStj2TaxtypeUsedCode() {
        return stj2TaxtypeUsedCode;
    }

    public void setStj2TaxtypeUsedCode(String stj2TaxtypeUsedCode) {
        this.stj2TaxtypeUsedCode = stj2TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj3TaxtypeUsedCode;
    public String getStj3TaxtypeUsedCode() {
        return stj3TaxtypeUsedCode;
    }

    public void setStj3TaxtypeUsedCode(String stj3TaxtypeUsedCode) {
        this.stj3TaxtypeUsedCode = stj3TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj4TaxtypeUsedCode;
    public String getStj4TaxtypeUsedCode() {
        return stj4TaxtypeUsedCode;
    }

    public void setStj4TaxtypeUsedCode(String stj4TaxtypeUsedCode) {
        this.stj4TaxtypeUsedCode = stj4TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj5TaxtypeUsedCode;
    public String getStj5TaxtypeUsedCode() {
        return stj5TaxtypeUsedCode;
    }

    public void setStj5TaxtypeUsedCode(String stj5TaxtypeUsedCode) {
        this.stj5TaxtypeUsedCode = stj5TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj6TaxtypeUsedCode;
    public String getStj6TaxtypeUsedCode() {
        return stj6TaxtypeUsedCode;
    }

    public void setStj6TaxtypeUsedCode(String stj6TaxtypeUsedCode) {
        this.stj6TaxtypeUsedCode = stj6TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj7TaxtypeUsedCode;
    public String getStj7TaxtypeUsedCode() {
        return stj7TaxtypeUsedCode;
    }

    public void setStj7TaxtypeUsedCode(String stj7TaxtypeUsedCode) {
        this.stj7TaxtypeUsedCode = stj7TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj8TaxtypeUsedCode;
    public String getStj8TaxtypeUsedCode() {
        return stj8TaxtypeUsedCode;
    }

    public void setStj8TaxtypeUsedCode(String stj8TaxtypeUsedCode) {
        this.stj8TaxtypeUsedCode = stj8TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj9TaxtypeUsedCode;
    public String getStj9TaxtypeUsedCode() {
        return stj9TaxtypeUsedCode;
    }

    public void setStj9TaxtypeUsedCode(String stj9TaxtypeUsedCode) {
        this.stj9TaxtypeUsedCode = stj9TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_TAXTYPE_USED_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj10TaxtypeUsedCode;
    public String getStj10TaxtypeUsedCode() {
        return stj10TaxtypeUsedCode;
    }

    public void setStj10TaxtypeUsedCode(String stj10TaxtypeUsedCode) {
        this.stj10TaxtypeUsedCode = stj10TaxtypeUsedCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countryNexusindCode;
    public String getCountryNexusindCode() {
        return countryNexusindCode;
    }

    public void setCountryNexusindCode(String countryNexusindCode) {
        this.countryNexusindCode = countryNexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stateNexusindCode;
    public String getStateNexusindCode() {
        return stateNexusindCode;
    }

    public void setStateNexusindCode(String stateNexusindCode) {
        this.stateNexusindCode = stateNexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 50)
    private String countyNexusindCode;
    public String getCountyNexusindCode() {
        return countyNexusindCode;
    }

    public void setCountyNexusindCode(String countyNexusindCode) {
        this.countyNexusindCode = countyNexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 50)
    private String cityNexusindCode;
    public String getCityNexusindCode() {
        return cityNexusindCode;
    }

    public void setCityNexusindCode(String cityNexusindCode) {
        this.cityNexusindCode = cityNexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj1NexusindCode;
    public String getStj1NexusindCode() {
        return stj1NexusindCode;
    }

    public void setStj1NexusindCode(String stj1NexusindCode) {
        this.stj1NexusindCode = stj1NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj2NexusindCode;
    public String getStj2NexusindCode() {
        return stj2NexusindCode;
    }

    public void setStj2NexusindCode(String stj2NexusindCode) {
        this.stj2NexusindCode = stj2NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj3NexusindCode;
    public String getStj3NexusindCode() { return stj3NexusindCode; }

    public void setStj3NexusindCode(String stj3NexusindCode) { this.stj3NexusindCode = stj3NexusindCode; }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj4NexusindCode;
    public String getStj4NexusindCode() {
        return stj4NexusindCode;
    }

    public void setStj4NexusindCode(String stj4NexusindCode) {
        this.stj4NexusindCode = stj4NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj5NexusindCode;
    public String getStj5NexusindCode() {
        return stj5NexusindCode;
    }

    public void setStj5NexusindCode(String stj5NexusindCode) {
        this.stj5NexusindCode = stj5NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj6NexusindCode;
    public String getStj6NexusindCode() {
        return stj6NexusindCode;
    }

    public void setStj6NexusindCode(String stj6NexusindCode) {
        this.stj6NexusindCode = stj6NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj7NexusindCode;
    public String getStj7NexusindCode() {
        return stj7NexusindCode;
    }

    public void setStj7NexusindCode(String stj7NexusindCode) {
        this.stj7NexusindCode = stj7NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj8NexusindCode;
    public String getStj8NexusindCode() {
        return stj8NexusindCode;
    }

    public void setStj8NexusindCode(String stj8NexusindCode) {
        this.stj8NexusindCode = stj8NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj9NexusindCode;
    public String getStj9NexusindCode() {
        return stj9NexusindCode;
    }

    public void setStj9NexusindCode(String stj9NexusindCode) {
        this.stj9NexusindCode = stj9NexusindCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_NEXUSIND_CODE", nullable = true, insertable = true, updatable = true, length = 255)
    private String stj10NexusindCode;
    public String getStj10NexusindCode() {
        return stj10NexusindCode;
    }

    public void setStj10NexusindCode(String stj10NexusindCode) {
        this.stj10NexusindCode = stj10NexusindCode;
    }


//COUNTRY_VENDOR_NEXUS_DTL_ID -> COUNTRY_NEXUS_DEF_DETAIL_ID

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countryNexusDefDetailId;
    public Long getCountryNexusDefDetailId() {
        return countryNexusDefDetailId;
    }

    public void setCountryNexusDefDetailId(Long countryNexusDefDetailId) {
        this.countryNexusDefDetailId = countryNexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stateNexusDefDetailId;
    public Long getStateNexusDefDetailId() {
        return stateNexusDefDetailId;
    }

    public void setStateNexusDefDetailId(Long stateNexusDefDetailId) {
        this.stateNexusDefDetailId = stateNexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long countyNexusDefDetailId;
    public Long getCountyNexusDefDetailId() {
        return countyNexusDefDetailId;
    }

    public void setCountyNexusDefDetailId(Long countyNexusDefDetailId) {
        this.countyNexusDefDetailId = countyNexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long cityNexusDefDetailId;
    public Long getCityNexusDefDetailId() {
        return cityNexusDefDetailId;
    }

    public void setCityNexusDefDetailId(Long cityNexusDefDetailId) {
        this.cityNexusDefDetailId = cityNexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj1NexusDefDetailId;
    public Long getStj1NexusDefDetailId() {
        return stj1NexusDefDetailId;
    }

    public void setStj1NexusDefDetailId(Long stj1NexusDefDetailId) {
        this.stj1NexusDefDetailId = stj1NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj2NexusDefDetailId;
    public Long getStj2NexusDefDetailId() {
        return stj2NexusDefDetailId;
    }

    public void setStj2NexusDefDetailId(Long stj2NexusDefDetailId) {
        this.stj2NexusDefDetailId = stj2NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj3NexusDefDetailId;
    public Long getStj3NexusDefDetailId() {
        return stj3NexusDefDetailId;
    }

    public void setStj3NexusDefDetailId(Long stj3NexusDefDetailId) {
        this.stj3NexusDefDetailId = stj3NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj4NexusDefDetailId;
    public Long getStj4NexusDefDetailId() {
        return stj4NexusDefDetailId;
    }

    public void setStj4NexusDefDetailId(Long stj4NexusDefDetailId) {
        this.stj4NexusDefDetailId = stj4NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj5NexusDefDetailId;
    public Long getStj5NexusDefDetailId() {
        return stj5NexusDefDetailId;
    }

    public void setStj5NexusDefDetailId(Long stj5NexusDefDetailId) {
        this.stj5NexusDefDetailId = stj5NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj6NexusDefDetailId;
    public Long getStj6NexusDefDetailId() {
        return stj6NexusDefDetailId;
    }

    public void setStj6NexusDefDetailId(Long stj6NexusDefDetailId) {
        this.stj6NexusDefDetailId = stj6NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj7NexusDefDetailId;
    public Long getStj7NexusDefDetailId() {
        return stj7NexusDefDetailId;
    }

    public void setStj7NexusDefDetailId(Long stj7NexusDefDetailId) {
        this.stj7NexusDefDetailId = stj7NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj8NexusDefDetailId;
    public Long getStj8NexusDefDetailId() {
        return stj8NexusDefDetailId;
    }

    public void setStj8NexusDefDetailId(Long stj8NexusDefDetailId) {
        this.stj8NexusDefDetailId = stj8NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj9NexusDefDetailId;
    public Long getStj9NexusDefDetailId() {
        return stj9NexusDefDetailId;
    }

    public void setStj9NexusDefDetailId(Long stj9NexusDefDetailId) {
        this.stj9NexusDefDetailId = stj9NexusDefDetailId;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_NEXUS_DEF_DETAIL_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    private Long stj10NexusDefDetailId;
    public Long getStj10NexusDefDetailId() {
        return stj10NexusDefDetailId;
    }

    public void setStj10NexusDefDetailId(Long stj10NexusDefDetailId) {
        this.stj10NexusDefDetailId = stj10NexusDefDetailId;
    }



    // MBF - 11/08/2017 - Added all TaxCode Type Codes to TB_BILLTRANS_JURDTL

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTRY_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countryTaxcodeTypeCode;
    public String getCountryTaxcodeTypeCode() {
        return countryTaxcodeTypeCode;
    }

    public void setCountryTaxcodeTypeCode(String countryTaxcodeTypeCode) {
        this.countryTaxcodeTypeCode = countryTaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STATE_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stateTaxcodeTypeCode;
    public String getStateTaxcodeTypeCode() {
        return stateTaxcodeTypeCode;
    }

    public void setStateTaxcodeTypeCode(String stateTaxcodeTypeCode) {
        this.stateTaxcodeTypeCode = stateTaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "COUNTY_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String countyTaxcodeTypeCode;
    public String getCountyTaxcodeTypeCode() {
        return countyTaxcodeTypeCode;
    }

    public void setCountyTaxcodeTypeCode(String countyTaxcodeTypeCode) {
        this.countyTaxcodeTypeCode = countyTaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "CITY_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String cityTaxcodeTypeCode;
    public String getCityTaxcodeTypeCode() {
        return cityTaxcodeTypeCode;
    }

    public void setCityTaxcodeTypeCode(String cityTaxcodeTypeCode) {
        this.cityTaxcodeTypeCode = cityTaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ1_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj1TaxcodeTypeCode;
    public String getStj1TaxcodeTypeCode() {
        return stj1TaxcodeTypeCode;
    }

    public void setStj1TaxcodeTypeCode(String stj1TaxcodeTypeCode) {
        this.stj1TaxcodeTypeCode = stj1TaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ2_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj2TaxcodeTypeCode;
    public String getStj2TaxcodeTypeCode() {
        return stj2TaxcodeTypeCode;
    }

    public void setStj2TaxcodeTypeCode(String stj2TaxcodeTypeCode) {
        this.stj2TaxcodeTypeCode = stj2TaxcodeTypeCode;
    }



    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ3_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj3TaxcodeTypeCode;
    public String getStj3TaxcodeTypeCode() {
        return stj3TaxcodeTypeCode;
    }

    public void setStj3TaxcodeTypeCode(String stj3TaxcodeTypeCode) {
        this.stj3TaxcodeTypeCode = stj3TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ4_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj4TaxcodeTypeCode;
    public String getStj4TaxcodeTypeCode() {
        return stj4TaxcodeTypeCode;
    }

    public void setStj4TaxcodeTypeCode(String stj4TaxcodeTypeCode) {
        this.stj4TaxcodeTypeCode = stj4TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ5_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj5TaxcodeTypeCode;
    public String getStj5TaxcodeTypeCode() {
        return stj5TaxcodeTypeCode;
    }

    public void setStj5TaxcodeTypeCode(String stj5TaxcodeTypeCode) {
        this.stj5TaxcodeTypeCode = stj5TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ6_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj6TaxcodeTypeCode;
    public String getStj6TaxcodeTypeCode() {
        return stj6TaxcodeTypeCode;
    }

    public void setStj6TaxcodeTypeCode(String stj6TaxcodeTypeCode) {
        this.stj6TaxcodeTypeCode = stj6TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ7_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj7TaxcodeTypeCode;
    public String getStj7TaxcodeTypeCode() {
        return stj7TaxcodeTypeCode;
    }

    public void setStj7TaxcodeTypeCode(String stj7TaxcodeTypeCode) {
        this.stj7TaxcodeTypeCode = stj7TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ8_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj8TaxcodeTypeCode;
    public String getStj8TaxcodeTypeCode() {
        return stj8TaxcodeTypeCode;
    }

    public void setStj8TaxcodeTypeCode(String stj8TaxcodeTypeCode) {
        this.stj8TaxcodeTypeCode = stj8TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ9_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj9TaxcodeTypeCode;
    public String getStj9TaxcodeTypeCode() {
        return stj9TaxcodeTypeCode;
    }

    public void setStj9TaxcodeTypeCode(String stj9TaxcodeTypeCode) {
        this.stj9TaxcodeTypeCode = stj9TaxcodeTypeCode;
    }

    @Basic
    @Column(table= "TB_BILLTRANS_JURDTL",name = "STJ10_TAXCODE_TYPE_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    private String stj10TaxcodeTypeCode;
    public String getStj10TaxcodeTypeCode() {
        return stj10TaxcodeTypeCode;
    }

    public void setStj10TaxcodeTypeCode(String stj10TaxcodeTypeCode) {
        this.stj10TaxcodeTypeCode = stj10TaxcodeTypeCode;
    }
    
    @Transient
	private Date glToDate;
    public Date getGlToDate() {
		return glToDate;
	}

	public void setGlToDate(Date glToDate) {
		this.glToDate = glToDate;
	}

    @Transient
    private BigDecimal countryTier1Rate;
    public BigDecimal getCountryTier1Rate() {
        return countryTier1Rate;
    }

    public void setCountryTier1Rate(BigDecimal countryTier1Rate) {
        this.countryTier1Rate = countryTier1Rate;
    }

    @Transient
    private BigDecimal countryTier2Rate;
    public BigDecimal getCountryTier2Rate() {
        return countryTier2Rate;
    }

    public void setCountryTier2Rate(BigDecimal countryTier2Rate) {
        this.countryTier2Rate = countryTier2Rate;
    }

    @Transient
    private BigDecimal countryTier3Rate;
    public BigDecimal getCountryTier3Rate() {
        return countryTier3Rate;
    }

    public void setCountryTier3Rate(BigDecimal countryTier3Rate) {
        this.countryTier3Rate = countryTier3Rate;
    }

    @Transient
    private BigDecimal countryTier1Setamt;
    public BigDecimal getCountryTier1Setamt() {
        return countryTier1Setamt;
    }

    public void setCountryTier1Setamt(BigDecimal countryTier1Setamt) {
        this.countryTier1Setamt = countryTier1Setamt;
    }

    @Transient
    private BigDecimal countryTier2Setamt;
    public BigDecimal getCountryTier2Setamt() {
        return countryTier2Setamt;
    }

    public void setCountryTier2Setamt(BigDecimal countryTier2Setamt) {
        this.countryTier2Setamt = countryTier2Setamt;
    }

    @Transient
    private BigDecimal countryTier3Setamt;
    public BigDecimal getCountryTier3Setamt() {
        return countryTier3Setamt;
    }

    public void setCountryTier3Setamt(BigDecimal countryTier3Setamt) {
        this.countryTier3Setamt = countryTier3Setamt;
    }

    @Transient
    private BigDecimal countryTier1MaxAmt;
    public BigDecimal getCountryTier1MaxAmt() {
        return countryTier1MaxAmt;
    }

    public void setCountryTier1MaxAmt(BigDecimal countryTier1MaxAmt) {
        this.countryTier1MaxAmt = countryTier1MaxAmt;
    }

    @Transient
    private BigDecimal countryTier2MinAmt;
    public BigDecimal getCountryTier2MinAmt() {
        return countryTier2MinAmt;
    }

    public void setCountryTier2MinAmt(BigDecimal countryTier2MinAmt) {
        this.countryTier2MinAmt = countryTier2MinAmt;
    }

    @Transient
    private BigDecimal countryTier2MaxAmt;
    public BigDecimal getCountryTier2MaxAmt() {
        return countryTier2MaxAmt;
    }

    public void setCountryTier2MaxAmt(BigDecimal countryTier2MaxAmt) {
        this.countryTier2MaxAmt = countryTier2MaxAmt;
    }

    @Transient
    private String countryTier2EntamFlag;
    public String getCountryTier2EntamFlag() {
        return countryTier2EntamFlag;
    }

    public void setCountryTier2EntamFlag(String countryTier2EntamFlag) {
        this.countryTier2EntamFlag = countryTier2EntamFlag;
    }

    @Transient
    private String countryTier3EntamFlag;
    public String getCountryTier3EntamFlag() {
        return countryTier3EntamFlag;
    }

    public void setCountryTier3EntamFlag(String countryTier3EntamFlag) {
        this.countryTier3EntamFlag = countryTier3EntamFlag;
    }

    @Transient
    private BigDecimal countrySpecialSetamt;
    public BigDecimal getCountrySpecialSetamt() {
        return countrySpecialSetamt;
    }

    public void setCountrySpecialSetamt(BigDecimal countrySpecialSetamt) {
        this.countrySpecialSetamt = countrySpecialSetamt;
    }

    @Transient
    private BigDecimal countrySpecialRate;
    public BigDecimal getCountrySpecialRate() {
        return countrySpecialRate;
    }

    public void setCountrySpecialRate(BigDecimal countrySpecialRate) {
        this.countrySpecialRate = countrySpecialRate;
    }

    @Transient
    private BigDecimal countryMaxtaxAmt;
    public BigDecimal getCountryMaxtaxAmt() {
        return countryMaxtaxAmt;
    }

    public void setCountryMaxtaxAmt(BigDecimal countryMaxtaxAmt) {
        this.countryMaxtaxAmt = countryMaxtaxAmt;
    }

    @Transient
    private BigDecimal countryTaxableThresholdAmt;
    public BigDecimal getCountryTaxableThresholdAmt() {
        return countryTaxableThresholdAmt;
    }

    public void setCountryTaxableThresholdAmt(BigDecimal countryTaxableThresholdAmt) {
        this.countryTaxableThresholdAmt = countryTaxableThresholdAmt;
    }

    @Transient
    private BigDecimal countryMinimumTaxableAmt;
    public BigDecimal getCountryMinimumTaxableAmt() {
        return countryMinimumTaxableAmt;
    }

    public void setCountryMinimumTaxableAmt(BigDecimal countryMinimumTaxableAmt) {
        this.countryMinimumTaxableAmt = countryMinimumTaxableAmt;
    }

    @Transient
    private BigDecimal countryMaximumTaxableAmt;
    public BigDecimal getCountryMaximumTaxableAmt() {
        return countryMaximumTaxableAmt;
    }

    public void setCountryMaximumTaxableAmt(BigDecimal countryMaximumTaxableAmt) {
        this.countryMaximumTaxableAmt = countryMaximumTaxableAmt;
    }

    @Transient
    private BigDecimal countryMaximumTaxAmt;
    public BigDecimal getCountryMaximumTaxAmt() {
        return countryMaximumTaxAmt;
    }

    public void setCountryMaximumTaxAmt(BigDecimal countryMaximumTaxAmt) {
        this.countryMaximumTaxAmt = countryMaximumTaxAmt;
    }

    @Transient
    private BigDecimal countryBaseChangePct;
    public BigDecimal getCountryBaseChangePct() {
        return countryBaseChangePct;
    }

    public void setCountryBaseChangePct(BigDecimal countryBaseChangePct) {
        this.countryBaseChangePct = countryBaseChangePct;
    }

    @Transient
    private String countryTaxProcessTypeCode;
    public String getCountryTaxProcessTypeCode() {
        return countryTaxProcessTypeCode;
    }

    public void setCountryTaxProcessTypeCode(String countryTaxProcessTypeCode) {
        this.countryTaxProcessTypeCode = countryTaxProcessTypeCode;
    }

    @Transient
    private BigDecimal stateTier1Rate;
    public BigDecimal getStateTier1Rate() {
        return stateTier1Rate;
    }

    public void setStateTier1Rate(BigDecimal stateTier1Rate) {
        this.stateTier1Rate = stateTier1Rate;
    }

    @Transient
    private BigDecimal stateTier2Rate;
    public BigDecimal getStateTier2Rate() {
        return stateTier2Rate;
    }

    public void setStateTier2Rate(BigDecimal stateTier2Rate) {
        this.stateTier2Rate = stateTier2Rate;
    }
    
    @Transient
    private BigDecimal stateTier3Rate;
    public BigDecimal getStateTier3Rate() {
        return stateTier3Rate;
    }

    public void setStateTier3Rate(BigDecimal stateTier3Rate) {
        this.stateTier3Rate = stateTier3Rate;
    }

    @Transient
    private BigDecimal stateTier1Setamt;
    public BigDecimal getStateTier1Setamt() {
        return stateTier1Setamt;
    }

    public void setStateTier1Setamt(BigDecimal stateTier1Setamt) {
        this.stateTier1Setamt = stateTier1Setamt;
    }

    @Transient
    private BigDecimal stateTier2Setamt;
    public BigDecimal getStateTier2Setamt() {
        return stateTier2Setamt;
    }

    public void setStateTier2Setamt(BigDecimal stateTier2Setamt) {
        this.stateTier2Setamt = stateTier2Setamt;
    }

    @Transient
    private BigDecimal stateTier3Setamt;
    public BigDecimal getStateTier3Setamt() {
        return stateTier3Setamt;
    }

    public void setStateTier3Setamt(BigDecimal stateTier3Setamt) {
        this.stateTier3Setamt = stateTier3Setamt;
    }

    @Transient
    private BigDecimal stateTier1MaxAmt;
    public BigDecimal getStateTier1MaxAmt() {
        return stateTier1MaxAmt;
    }

    public void setStateTier1MaxAmt(BigDecimal stateTier1MaxAmt) {
        this.stateTier1MaxAmt = stateTier1MaxAmt;
    }

    @Transient
    private BigDecimal stateTier2MinAmt;
    public BigDecimal getStateTier2MinAmt() {
        return stateTier2MinAmt;
    }

    public void setStateTier2MinAmt(BigDecimal stateTier2MinAmt) {
        this.stateTier2MinAmt = stateTier2MinAmt;
    }

    @Transient
    private BigDecimal stateTier2MaxAmt;
    public BigDecimal getStateTier2MaxAmt() {
        return stateTier2MaxAmt;
    }

    public void setStateTier2MaxAmt(BigDecimal stateTier2MaxAmt) {
        this.stateTier2MaxAmt = stateTier2MaxAmt;
    }

    @Transient
    private String stateTier2EntamFlag;
    public String getStateTier2EntamFlag() {
        return stateTier2EntamFlag;
    }

    public void setStateTier2EntamFlag(String stateTier2EntamFlag) {
        this.stateTier2EntamFlag = stateTier2EntamFlag;
    }

    @Transient
    private String stateTier3EntamFlag;
    public String getStateTier3EntamFlag() {
        return stateTier3EntamFlag;
    }

    public void setStateTier3EntamFlag(String stateTier3EntamFlag) {
        this.stateTier3EntamFlag = stateTier3EntamFlag;
    }

    @Transient
    private BigDecimal stateSpecialSetamt;
    public BigDecimal getStateSpecialSetamt() {
        return stateSpecialSetamt;
    }

    public void setStateSpecialSetamt(BigDecimal stateSpecialSetamt) {
        this.stateSpecialSetamt = stateSpecialSetamt;
    }

    @Transient
    private BigDecimal stateSpecialRate;
    public BigDecimal getStateSpecialRate() {
        return stateSpecialRate;
    }

    public void setStateSpecialRate(BigDecimal stateSpecialRate) {
        this.stateSpecialRate = stateSpecialRate;
    }

    @Transient
    private BigDecimal stateMaxtaxAmt;
    public BigDecimal getStateMaxtaxAmt() {
        return stateMaxtaxAmt;
    }

    public void setStateMaxtaxAmt(BigDecimal stateMaxtaxAmt) {
        this.stateMaxtaxAmt = stateMaxtaxAmt;
    }

    @Transient
    private BigDecimal stateTaxableThresholdAmt;
    public BigDecimal getStateTaxableThresholdAmt() {
        return stateTaxableThresholdAmt;
    }

    public void setStateTaxableThresholdAmt(BigDecimal stateTaxableThresholdAmt) {
        this.stateTaxableThresholdAmt = stateTaxableThresholdAmt;
    }

    @Transient
    private BigDecimal stateMinimumTaxableAmt;
    public BigDecimal getStateMinimumTaxableAmt() {
        return stateMinimumTaxableAmt;
    }

    public void setStateMinimumTaxableAmt(BigDecimal stateMinimumTaxableAmt) {
        this.stateMinimumTaxableAmt = stateMinimumTaxableAmt;
    }

    @Transient
    private BigDecimal stateMaximumTaxableAmt;
    public BigDecimal getStateMaximumTaxableAmt() {
        return stateMaximumTaxableAmt;
    }

    public void setStateMaximumTaxableAmt(BigDecimal stateMaximumTaxableAmt) {
        this.stateMaximumTaxableAmt = stateMaximumTaxableAmt;
    }

    @Transient
    private BigDecimal stateMaximumTaxAmt;
    public BigDecimal getStateMaximumTaxAmt() {
        return stateMaximumTaxAmt;
    }

    public void setStateMaximumTaxAmt(BigDecimal stateMaximumTaxAmt) {
        this.stateMaximumTaxAmt = stateMaximumTaxAmt;
    }

    @Transient
    private BigDecimal stateBaseChangePct;
    public BigDecimal getStateBaseChangePct() {
        return stateBaseChangePct;
    }

    public void setStateBaseChangePct(BigDecimal stateBaseChangePct) {
        this.stateBaseChangePct = stateBaseChangePct;
    }

    @Transient
    private String stateTaxProcessTypeCode;
    public String getStateTaxProcessTypeCode() {
        return stateTaxProcessTypeCode;
    }

    public void setStateTaxProcessTypeCode(String stateTaxProcessTypeCode) {
        this.stateTaxProcessTypeCode = stateTaxProcessTypeCode;
    }

    @Transient
    private BigDecimal countyTier1Rate;
    public BigDecimal getCountyTier1Rate() {
        return countyTier1Rate;
    }

    public void setCountyTier1Rate(BigDecimal countyTier1Rate) {
        this.countyTier1Rate = countyTier1Rate;
    }

    @Transient
    private BigDecimal countyTier2Rate;
    public BigDecimal getCountyTier2Rate() {
        return countyTier2Rate;
    }

    public void setCountyTier2Rate(BigDecimal countyTier2Rate) {
        this.countyTier2Rate = countyTier2Rate;
    }

    @Transient
    private BigDecimal countyTier3Rate;
    public BigDecimal getCountyTier3Rate() {
        return countyTier3Rate;
    }

    public void setCountyTier3Rate(BigDecimal countyTier3Rate) {
        this.countyTier3Rate = countyTier3Rate;
    }

    @Transient
    private BigDecimal countyTier1Setamt;
    public BigDecimal getCountyTier1Setamt() {
        return countyTier1Setamt;
    }

    public void setCountyTier1Setamt(BigDecimal countyTier1Setamt) {
        this.countyTier1Setamt = countyTier1Setamt;
    }

    @Transient
    private BigDecimal countyTier2Setamt;
    public BigDecimal getCountyTier2Setamt() {
        return countyTier2Setamt;
    }

    public void setCountyTier2Setamt(BigDecimal countyTier2Setamt) {
        this.countyTier2Setamt = countyTier2Setamt;
    }

    @Transient
    private BigDecimal countyTier3Setamt;
    public BigDecimal getCountyTier3Setamt() {
        return countyTier3Setamt;
    }

    public void setCountyTier3Setamt(BigDecimal countyTier3Setamt) {
        this.countyTier3Setamt = countyTier3Setamt;
    }

    @Transient
    private BigDecimal countyTier1MaxAmt;
    public BigDecimal getCountyTier1MaxAmt() {
        return countyTier1MaxAmt;
    }

    public void setCountyTier1MaxAmt(BigDecimal countyTier1MaxAmt) {
        this.countyTier1MaxAmt = countyTier1MaxAmt;
    }

    @Transient
    private BigDecimal countyTier2MinAmt;
    public BigDecimal getCountyTier2MinAmt() {
        return countyTier2MinAmt;
    }

    public void setCountyTier2MinAmt(BigDecimal countyTier2MinAmt) {
        this.countyTier2MinAmt = countyTier2MinAmt;
    }

    @Transient
    private BigDecimal countyTier2MaxAmt;
    public BigDecimal getCountyTier2MaxAmt() {
        return countyTier2MaxAmt;
    }

    public void setCountyTier2MaxAmt(BigDecimal countyTier2MaxAmt) {
        this.countyTier2MaxAmt = countyTier2MaxAmt;
    }

    @Transient
    private String countyTier2EntamFlag;
    public String getCountyTier2EntamFlag() {
        return countyTier2EntamFlag;
    }

    public void setCountyTier2EntamFlag(String countyTier2EntamFlag) {
        this.countyTier2EntamFlag = countyTier2EntamFlag;
    }

    @Transient
    private String countyTier3EntamFlag;
    public String getCountyTier3EntamFlag() {
        return countyTier3EntamFlag;
    }

    public void setCountyTier3EntamFlag(String countyTier3EntamFlag) {
        this.countyTier3EntamFlag = countyTier3EntamFlag;
    }

    @Transient
    private BigDecimal countySpecialSetamt;
    public BigDecimal getCountySpecialSetamt() {
        return countySpecialSetamt;
    }

    public void setCountySpecialSetamt(BigDecimal countySpecialSetamt) {
        this.countySpecialSetamt = countySpecialSetamt;
    }

    @Transient
    private BigDecimal countySpecialRate;
    public BigDecimal getCountySpecialRate() {
        return countySpecialRate;
    }

    public void setCountySpecialRate(BigDecimal countySpecialRate) {
        this.countySpecialRate = countySpecialRate;
    }

    @Transient
    private BigDecimal countyMaxtaxAmt;
    public BigDecimal getCountyMaxtaxAmt() {
        return countyMaxtaxAmt;
    }

    public void setCountyMaxtaxAmt(BigDecimal countyMaxtaxAmt) {
        this.countyMaxtaxAmt = countyMaxtaxAmt;
    }

    @Transient
    private BigDecimal countyTaxableThresholdAmt;
    public BigDecimal getCountyTaxableThresholdAmt() {
        return countyTaxableThresholdAmt;
    }

    public void setCountyTaxableThresholdAmt(BigDecimal countyTaxableThresholdAmt) {
        this.countyTaxableThresholdAmt = countyTaxableThresholdAmt;
    }

    @Transient
    private BigDecimal countyMinimumTaxableAmt;
    public BigDecimal getCountyMinimumTaxableAmt() {
        return countyMinimumTaxableAmt;
    }

    public void setCountyMinimumTaxableAmt(BigDecimal countyMinimumTaxableAmt) {
        this.countyMinimumTaxableAmt = countyMinimumTaxableAmt;
    }

    @Transient
    private BigDecimal countyMaximumTaxableAmt;
    public BigDecimal getCountyMaximumTaxableAmt() {
        return countyMaximumTaxableAmt;
    }

    public void setCountyMaximumTaxableAmt(BigDecimal countyMaximumTaxableAmt) {
        this.countyMaximumTaxableAmt = countyMaximumTaxableAmt;
    }

    @Transient
    private BigDecimal countyMaximumTaxAmt;
    public BigDecimal getCountyMaximumTaxAmt() {
        return countyMaximumTaxAmt;
    }

    public void setCountyMaximumTaxAmt(BigDecimal countyMaximumTaxAmt) {
        this.countyMaximumTaxAmt = countyMaximumTaxAmt;
    }

    @Transient
    private BigDecimal countyBaseChangePct;
    public BigDecimal getCountyBaseChangePct() {
        return countyBaseChangePct;
    }

    public void setCountyBaseChangePct(BigDecimal countyBaseChangePct) {
        this.countyBaseChangePct = countyBaseChangePct;
    }

    @Transient
    private String countyTaxProcessTypeCode;
    public String getCountyTaxProcessTypeCode() {
        return countyTaxProcessTypeCode;
    }

    public void setCountyTaxProcessTypeCode(String countyTaxProcessTypeCode) {
        this.countyTaxProcessTypeCode = countyTaxProcessTypeCode;
    }

    @Transient
    private BigDecimal cityTier1Rate;
    public BigDecimal getCityTier1Rate() {
        return cityTier1Rate;
    }

    public void setCityTier1Rate(BigDecimal cityTier1Rate) {
        this.cityTier1Rate = cityTier1Rate;
    }

    @Transient
    private BigDecimal cityTier2Rate;
    public BigDecimal getCityTier2Rate() {
        return cityTier2Rate;
    }

    public void setCityTier2Rate(BigDecimal cityTier2Rate) {
        this.cityTier2Rate = cityTier2Rate;
    }

    @Transient
    private BigDecimal cityTier3Rate;
    public BigDecimal getCityTier3Rate() {
        return cityTier3Rate;
    }

    public void setCityTier3Rate(BigDecimal cityTier3Rate) {
        this.cityTier3Rate = cityTier3Rate;
    }

    @Transient
    private BigDecimal cityTier1Setamt;
    public BigDecimal getCityTier1Setamt() {
        return cityTier1Setamt;
    }

    public void setCityTier1Setamt(BigDecimal cityTier1Setamt) {
        this.cityTier1Setamt = cityTier1Setamt;
    }

    @Transient
    private BigDecimal cityTier2Setamt;
    public BigDecimal getCityTier2Setamt() {
        return cityTier2Setamt;
    }

    public void setCityTier2Setamt(BigDecimal cityTier2Setamt) {
        this.cityTier2Setamt = cityTier2Setamt;
    }

    @Transient
    private BigDecimal cityTier3Setamt;
    public BigDecimal getCityTier3Setamt() {
        return cityTier3Setamt;
    }

    public void setCityTier3Setamt(BigDecimal cityTier3Setamt) {
        this.cityTier3Setamt = cityTier3Setamt;
    }

    @Transient
    private BigDecimal cityTier1MaxAmt;
    public BigDecimal getCityTier1MaxAmt() {
        return cityTier1MaxAmt;
    }

    public void setCityTier1MaxAmt(BigDecimal cityTier1MaxAmt) {
        this.cityTier1MaxAmt = cityTier1MaxAmt;
    }

    @Transient
    private BigDecimal cityTier2MinAmt;
    public BigDecimal getCityTier2MinAmt() {
        return cityTier2MinAmt;
    }

    public void setCityTier2MinAmt(BigDecimal cityTier2MinAmt) {
        this.cityTier2MinAmt = cityTier2MinAmt;
    }

    @Transient
    private BigDecimal cityTier2MaxAmt;
    public BigDecimal getCityTier2MaxAmt() {
        return cityTier2MaxAmt;
    }

    public void setCityTier2MaxAmt(BigDecimal cityTier2MaxAmt) {
        this.cityTier2MaxAmt = cityTier2MaxAmt;
    }

    @Transient
    private String cityTier2EntamFlag;
    public String getCityTier2EntamFlag() {
        return cityTier2EntamFlag;
    }

    public void setCityTier2EntamFlag(String cityTier2EntamFlag) {
        this.cityTier2EntamFlag = cityTier2EntamFlag;
    }

    @Transient
    private String cityTier3EntamFlag;
    public String getCityTier3EntamFlag() {
        return cityTier3EntamFlag;
    }

    public void setCityTier3EntamFlag(String cityTier3EntamFlag) {
        this.cityTier3EntamFlag = cityTier3EntamFlag;
    }

    @Transient
    private BigDecimal citySpecialSetamt;
    public BigDecimal getCitySpecialSetamt() {
        return citySpecialSetamt;
    }

    public void setCitySpecialSetamt(BigDecimal citySpecialSetamt) {
        this.citySpecialSetamt = citySpecialSetamt;
    }

    @Transient
    private BigDecimal citySpecialRate;
    public BigDecimal getCitySpecialRate() {
        return citySpecialRate;
    }

    public void setCitySpecialRate(BigDecimal citySpecialRate) {
        this.citySpecialRate = citySpecialRate;
    }

    @Transient
    private BigDecimal cityMaxtaxAmt;
    public BigDecimal getCityMaxtaxAmt() {
        return cityMaxtaxAmt;
    }

    public void setCityMaxtaxAmt(BigDecimal cityMaxtaxAmt) {
        this.cityMaxtaxAmt = cityMaxtaxAmt;
    }

    @Transient
    private BigDecimal cityTaxableThresholdAmt;
    public BigDecimal getCityTaxableThresholdAmt() {
        return cityTaxableThresholdAmt;
    }

    public void setCityTaxableThresholdAmt(BigDecimal cityTaxableThresholdAmt) {
        this.cityTaxableThresholdAmt = cityTaxableThresholdAmt;
    }

    @Transient
    private BigDecimal cityMinimumTaxableAmt;
    public BigDecimal getCityMinimumTaxableAmt() {
        return cityMinimumTaxableAmt;
    }

    public void setCityMinimumTaxableAmt(BigDecimal cityMinimumTaxableAmt) {
        this.cityMinimumTaxableAmt = cityMinimumTaxableAmt;
    }

    @Transient
    private BigDecimal cityMaximumTaxableAmt;
    public BigDecimal getCityMaximumTaxableAmt() {
        return cityMaximumTaxableAmt;
    }

    public void setCityMaximumTaxableAmt(BigDecimal cityMaximumTaxableAmt) {
        this.cityMaximumTaxableAmt = cityMaximumTaxableAmt;
    }

    @Transient
    private BigDecimal cityMaximumTaxAmt;
    public BigDecimal getCityMaximumTaxAmt() {
        return cityMaximumTaxAmt;
    }

    public void setCityMaximumTaxAmt(BigDecimal cityMaximumTaxAmt) {
        this.cityMaximumTaxAmt = cityMaximumTaxAmt;
    }

    @Transient
    private BigDecimal cityBaseChangePct;
    public BigDecimal getCityBaseChangePct() {
        return cityBaseChangePct;
    }

    public void setCityBaseChangePct(BigDecimal cityBaseChangePct) {
        this.cityBaseChangePct = cityBaseChangePct;
    }

    @Transient
    private String cityTaxProcessTypeCode;
    public String getCityTaxProcessTypeCode() {
        return cityTaxProcessTypeCode;
    }

    public void setCityTaxProcessTypeCode(String cityTaxProcessTypeCode) {
        this.cityTaxProcessTypeCode = cityTaxProcessTypeCode;
    }

    @Transient
    private BigDecimal stj1Rate;
    public BigDecimal getStj1Rate() {
        return stj1Rate;
    }

    public void setStj1Rate(BigDecimal stj1Rate) {
        this.stj1Rate = stj1Rate;
    }

    @Transient
    private BigDecimal stj1Setamt;
    public BigDecimal getStj1Setamt() {
        return stj1Setamt;
    }

    public void setStj1Setamt(BigDecimal stj1Setamt) {
        this.stj1Setamt = stj1Setamt;
    }

    @Transient
    private BigDecimal stj2Rate;
    public BigDecimal getStj2Rate() {
        return stj2Rate;
    }

    public void setStj2Rate(BigDecimal stj2Rate) {
        this.stj2Rate = stj2Rate;
    }

    @Transient
    private BigDecimal stj2Setamt;
    public BigDecimal getStj2Setamt() {
        return stj2Setamt;
    }

    public void setStj2Setamt(BigDecimal stj2Setamt) {
        this.stj2Setamt = stj2Setamt;
    }

    @Transient
    private BigDecimal stj3Rate;
    public BigDecimal getStj3Rate() {
        return stj3Rate;
    }

    public void setStj3Rate(BigDecimal stj3Rate) {
        this.stj3Rate = stj3Rate;
    }

    @Transient
    private BigDecimal stj3Setamt;
    public BigDecimal getStj3Setamt() {
        return stj3Setamt;
    }

    public void setStj3Setamt(BigDecimal stj3Setamt) {
        this.stj3Setamt = stj3Setamt;
    }

    @Transient
    private BigDecimal stj4Rate;
    public BigDecimal getStj4Rate() {
        return stj4Rate;
    }

    public void setStj4Rate(BigDecimal stj4Rate) {
        this.stj4Rate = stj4Rate;
    }

    @Transient
    private BigDecimal stj4Setamt;
    public BigDecimal getStj4Setamt() {
        return stj4Setamt;
    }

    public void setStj4Setamt(BigDecimal stj4Setamt) {
        this.stj4Setamt = stj4Setamt;
    }

    @Transient
    private BigDecimal stj5Rate;
    public BigDecimal getStj5Rate() {
        return stj5Rate;
    }

    public void setStj5Rate(BigDecimal stj5Rate) {
        this.stj5Rate = stj5Rate;
    }

    @Transient
    private BigDecimal stj5Setamt;
    public BigDecimal getStj5Setamt() {
        return stj5Setamt;
    }

    public void setStj5Setamt(BigDecimal stj5Setamt) {
        this.stj5Setamt = stj5Setamt;
    }

    @Transient
    private BigDecimal stj6Rate;
    public BigDecimal getStj6Rate() {
        return stj6Rate;
    }

    public void setStj6Rate(BigDecimal stj6Rate) {
        this.stj6Rate = stj6Rate;
    }

    @Transient
    private BigDecimal stj6Setamt;
    public BigDecimal getStj6Setamt() {
        return stj6Setamt;
    }

    public void setStj6Setamt(BigDecimal stj6Setamt) {
        this.stj6Setamt = stj6Setamt;
    }

    @Transient
    private BigDecimal stj7Rate;
    public BigDecimal getStj7Rate() {
        return stj7Rate;
    }

    public void setStj7Rate(BigDecimal stj7Rate) {
        this.stj7Rate = stj7Rate;
    }

    @Transient
    private BigDecimal stj7Setamt;
    public BigDecimal getStj7Setamt() {
        return stj7Setamt;
    }

    public void setStj7Setamt(BigDecimal stj7Setamt) {
        this.stj7Setamt = stj7Setamt;
    }

    @Transient
    private BigDecimal stj8Rate;
    public BigDecimal getStj8Rate() {
        return stj8Rate;
    }

    public void setStj8Rate(BigDecimal stj8Rate) {
        this.stj8Rate = stj8Rate;
    }

    @Transient
    private BigDecimal stj8Setamt;
    public BigDecimal getStj8Setamt() {
        return stj8Setamt;
    }

    public void setStj8Setamt(BigDecimal stj8Setamt) {
        this.stj8Setamt = stj8Setamt;
    }

    @Transient
    private BigDecimal stj9Rate;
    public BigDecimal getStj9Rate() {
        return stj9Rate;
    }

    public void setStj9Rate(BigDecimal stj9Rate) {
        this.stj9Rate = stj9Rate;
    }

    @Transient
    private BigDecimal stj9Setamt;
    public BigDecimal getStj9Setamt() {
        return stj9Setamt;
    }

    public void setStj9Setamt(BigDecimal stj9Setamt) {
        this.stj9Setamt = stj9Setamt;
    }

    @Transient
    private BigDecimal stj10Rate;
    public BigDecimal getStj10Rate() {
        return stj10Rate;
    }

    public void setStj10Rate(BigDecimal stj10Rate) {
        this.stj10Rate = stj10Rate;
    }

    @Transient
    private BigDecimal stj10Setamt;
    public BigDecimal getStj10Setamt() {
        return stj10Setamt;
    }

    public void setStj10Setamt(BigDecimal stj10Setamt) {
        this.stj10Setamt = stj10Setamt;
    }

    @Transient
    private BigDecimal combinedRate;
    public BigDecimal getCombinedRate() {
        return combinedRate;
    }

    public void setCombinedRate(BigDecimal combinedRate) {
        this.combinedRate = combinedRate;
    }

    @Transient
    private String countryTaxtypeCode;
    public String getCountryTaxtypeCode() {
        return countryTaxtypeCode;
    }

    public void setCountryTaxtypeCode(String countryTaxtypeCode) {
        this.countryTaxtypeCode = countryTaxtypeCode;
    }

    @Transient
    private String countryRatetypeCode;
    public String getCountryRatetypeCode() {
        return countryRatetypeCode;
    }

    public void setCountryRatetypeCode(String countryRatetypeCode) {
        this.countryRatetypeCode = countryRatetypeCode;
    }

    @Transient
    private String countryGoodSvcTypeCode;
    public String getCountryGoodSvcTypeCode() {
        return countryGoodSvcTypeCode;
    }

    public void setCountryGoodSvcTypeCode(String countryGoodSvcTypeCode) {
        this.countryGoodSvcTypeCode = countryGoodSvcTypeCode;
    }

    @Transient
    private String countryCitationRef;
    public String getCountryCitationRef() {
        return countryCitationRef;
    }

    public void setCountryCitationRef(String countryCitationRef) {
        this.countryCitationRef = countryCitationRef;
    }

    @Transient
    private String countryExemptReason;
    public String getCountryExemptReason() {
        return countryExemptReason;
    }

    public void setCountryExemptReason(String countryExemptReason) {
        this.countryExemptReason = countryExemptReason;
    }

    @Transient
    private String countryGroupByDriver;
    public String getCountryGroupByDriver() {
        return countryGroupByDriver;
    }

    public void setCountryGroupByDriver(String countryGroupByDriver) { this.countryGroupByDriver = countryGroupByDriver; }

    @Transient
    private String countryAllocBucket;
    public String getCountryAllocBucket() {
        return countryAllocBucket;
    }

    public void setCountryAllocBucket(String countryAllocBucket) {
        this.countryAllocBucket = countryAllocBucket;
    }

    @Transient
    private String countryAllocProrateByCode;
    public String getCountryAllocProrateByCode() {
        return countryAllocProrateByCode;
    }

    public void setCountryAllocProrateByCode(String countryAllocProrateByCode) { this.countryAllocProrateByCode = countryAllocProrateByCode; }

    @Transient
    private String countryAllocConditionCode;
    public String getCountryAllocConditionCode() {
        return countryAllocConditionCode;
    }

    public void setCountryAllocConditionCode(String countryAllocConditionCode) { this.countryAllocConditionCode = countryAllocConditionCode; }

    @Transient
    private String stateTaxtypeCode;
    public String getStateTaxtypeCode() {
        return stateTaxtypeCode;
    }

    public void setStateTaxtypeCode(String stateTaxtypeCode) {
        this.stateTaxtypeCode = stateTaxtypeCode;
    }

    @Transient
    private String stateRatetypeCode;
    public String getStateRatetypeCode() {
        return stateRatetypeCode;
    }

    public void setStateRatetypeCode(String stateRatetypeCode) {
        this.stateRatetypeCode = stateRatetypeCode;
    }

    @Transient
    private String stateGoodSvcTypeCode;
    public String getStateGoodSvcTypeCode() {
        return stateGoodSvcTypeCode;
    }

    public void setStateGoodSvcTypeCode(String stateGoodSvcTypeCode) {
        this.stateGoodSvcTypeCode = stateGoodSvcTypeCode;
    }

    @Transient
    private String stateCitationRef;
    public String getStateCountryCitationRef() {
        return stateCitationRef;
    }

    public void setStateCitationRef(String stateCitationRef) {
        this.stateCitationRef = stateCitationRef;
    }

    @Transient
    private String stateExemptReason;
    public String getStateExemptReason() {
        return stateExemptReason;
    }

    public void setStateExemptReason(String stateExemptReason) {
        this.stateExemptReason = stateExemptReason;
    }

    @Transient
    private String stateGroupByDriver;
    public String getStateGroupByDriver() {
        return stateGroupByDriver;
    }

    public void setStateGroupByDriver(String stateGroupByDriver) { this.stateGroupByDriver = stateGroupByDriver; }

    @Transient
    private String stateAllocBucket;
    public String getStateAllocBucket() {
        return stateAllocBucket;
    }

    public void setStateAllocBucket(String stateAllocBucket) {
        this.stateAllocBucket = stateAllocBucket;
    }

    @Transient
    private String stateAllocProrateByCode;
    public String getStateAllocProrateByCode() {
        return stateAllocProrateByCode;
    }

    public void setStateAllocProrateByCode(String stateAllocProrateByCode) { this.stateAllocProrateByCode = stateAllocProrateByCode; }

    @Transient
    private String stateAllocConditionCode;
    public String getStateAllocConditionCode() {
        return stateAllocConditionCode;
    }

    public void setStateAllocConditionCode(String stateAllocConditionCode) { this.stateAllocConditionCode = stateAllocConditionCode; }

    @Transient
    private String countyTaxtypeCode;
    public String getCountyTaxtypeCode() {
        return countyTaxtypeCode;
    }

    public void setCountyTaxtypeCode(String countyTaxtypeCode) {
        this.countyTaxtypeCode = countyTaxtypeCode;
    }

    @Transient
    private String countyRatetypeCode;
    public String getCountyRatetypeCode() {
        return countyRatetypeCode;
    }

    public void setCountyRatetypeCode(String countyRatetypeCode) {
        this.countyRatetypeCode = countyRatetypeCode;
    }

    @Transient
    private String countyGoodSvcTypeCode;
    public String getCountyGoodSvcTypeCode() {
        return countyGoodSvcTypeCode;
    }

    public void setCountyGoodSvcTypeCode(String countyGoodSvcTypeCode) {
        this.countyGoodSvcTypeCode = countyGoodSvcTypeCode;
    }

    @Transient
    private String countyCitationRef;
    public String getCountyCitationRef() {
        return countyCitationRef;
    }

    public void setCountyCitationRef(String countyCitationRef) {
        this.countyCitationRef = countyCitationRef;
    }

    @Transient
    private String countyExemptReason;
    public String getCountyExemptReason() {
        return countyExemptReason;
    }

    public void setCountyExemptReason(String countyExemptReason) {
        this.countyExemptReason = countyExemptReason;
    }

    @Transient
    private String countyGroupByDriver;
    public String getCountyGroupByDriver() {
        return countyGroupByDriver;
    }

    public void setCountyGroupByDriver(String countyGroupByDriver) { this.countyGroupByDriver = countyGroupByDriver; }

    @Transient
    private String countyAllocBucket;
    public String getCountyAllocBucket() {
        return countyAllocBucket;
    }

    public void setCountyAllocBucket(String countyAllocBucket) {
        this.countyAllocBucket = countyAllocBucket;
    }

    @Transient
    private String countyAllocProrateByCode;
    public String getCountyAllocProrateByCode() {
        return countyAllocProrateByCode;
    }

    public void setCountyAllocProrateByCode(String countyAllocProrateByCode) { this.countyAllocProrateByCode = countyAllocProrateByCode; }

    @Transient
    private String countyAllocConditionCode;
    public String getCountyAllocConditionCode() {
        return countyAllocConditionCode;
    }

    public void setCountyAllocConditionCode(String countyAllocConditionCode) { this.countyAllocConditionCode = countyAllocConditionCode; }

    @Transient
    private String cityTaxtypeCode;
    public String getCityTaxtypeCode() {
        return cityTaxtypeCode;
    }

    public void setCityTaxtypeCode(String cityTaxtypeCode) {
        this.cityTaxtypeCode = cityTaxtypeCode;
    }

    @Transient
    private String cityRatetypeCode;
    public String getCityRatetypeCode() {
        return cityRatetypeCode;
    }

    public void setCityRatetypeCode(String cityRatetypeCode) {
        this.cityRatetypeCode = cityRatetypeCode;
    }

    @Transient
    private String cityGoodSvcTypeCode;
    public String getCityGoodSvcTypeCode() {
        return cityGoodSvcTypeCode;
    }

    public void setCityGoodSvcTypeCode(String cityGoodSvcTypeCode) {
        this.cityGoodSvcTypeCode = cityGoodSvcTypeCode;
    }

    @Transient
    private String cityCitationRef;
    public String getCityCitationRef() {
        return cityCitationRef;
    }

    public void setCityCitationRef(String cityCitationRef) {
        this.cityCitationRef = cityCitationRef;
    }

    @Transient
    private String cityExemptReason;
    public String getCityExemptReason() {
        return cityExemptReason;
    }

    public void setCityExemptReason(String cityExemptReason) {
        this.cityExemptReason = cityExemptReason;
    }

    @Transient
    private String cityGroupByDriver;
    public String getCityGroupByDriver() {
        return cityGroupByDriver;
    }

    public void setCityGroupByDriver(String cityGroupByDriver) { this.cityGroupByDriver = cityGroupByDriver; }

    @Transient
    private String cityAllocBucket;
    public String getCityAllocBucket() {
        return cityAllocBucket;
    }

    public void setCityAllocBucket(String cityAllocBucket) {
        this.cityAllocBucket = cityAllocBucket;
    }

    @Transient
    private String cityAllocProrateByCode;
    public String getCityAllocProrateByCode() {
        return cityAllocProrateByCode;
    }

    public void setCityAllocProrateByCode(String cityAllocProrateByCode) { this.cityAllocProrateByCode = cityAllocProrateByCode; }

    @Transient
    private String cityAllocConditionCode;
    public String getCityAllocConditionCode() {
        return cityAllocConditionCode;
    }

    public void setCityAllocConditionCode(String cityAllocConditionCode) { this.cityAllocConditionCode = cityAllocConditionCode; }

    @Transient
    private String stj1TaxtypeCode;
    public String getStj1TaxtypeCode() {
        return stj1TaxtypeCode;
    }

    public void setStj1TaxtypeCode(String stj1TaxtypeCode) {
        this.stj1TaxtypeCode = stj1TaxtypeCode;
    }

    @Transient
    private String stj1RatetypeCode;
    public String getStj1RatetypeCode() {
        return stj1RatetypeCode;
    }

    public void setStj1RatetypeCode(String stj1RatetypeCode) {
        this.stj1RatetypeCode = stj1RatetypeCode;
    }

    @Transient
    private String stj1GoodSvcTypeCode;
    public String getStj1GoodSvcTypeCode() {
        return stj1GoodSvcTypeCode;
    }

    public void setStj1GoodSvcTypeCode(String stj1GoodSvcTypeCode) {
        this.stj1GoodSvcTypeCode = stj1GoodSvcTypeCode;
    }

    @Transient
    private String stj1CitationRef;
    public String getStj1CitationRef() {
        return stj1CitationRef;
    }

    public void setStj1CitationRef(String stj1CitationRef) {
        this.stj1CitationRef = stj1CitationRef;
    }

    @Transient
    private String stj1ExemptReason;
    public String getStj1ExemptReason() {
        return stj1ExemptReason;
    }

    public void setStj1ExemptReason(String stj1ExemptReason) {
        this.stj1ExemptReason = stj1ExemptReason;
    }

    @Transient
    private String stj1GroupByDriver;
    public String getStj1GroupByDriver() {
        return stj1GroupByDriver;
    }

    public void setStj1GroupByDriver(String stj1GroupByDriver) { this.stj1GroupByDriver = stj1GroupByDriver; }

    @Transient
    private String stj1AllocBucket;
    public String getStj1AllocBucket() {
        return stj1AllocBucket;
    }

    public void setStj1AllocBucket(String stj1AllocBucket) {
        this.stj1AllocBucket = stj1AllocBucket;
    }

    @Transient
    private String stj1AllocProrateByCode;
    public String getStj1AllocProrateByCode() {
        return stj1AllocProrateByCode;
    }

    public void setStj1AllocProrateByCode(String stj1AllocProrateByCode) { this.stj1AllocProrateByCode = stj1AllocProrateByCode; }

    @Transient
    private String stj1AllocConditionCode;
    public String getStj1AllocConditionCode() {
        return stj1AllocConditionCode;
    }

    public void setStj1AllocConditionCode(String stj1AllocConditionCode) { this.stj1AllocConditionCode = stj1AllocConditionCode; }

    @Transient
    private String stj2TaxtypeCode;
    public String getStj2TaxtypeCode() {
        return stj2TaxtypeCode;
    }

    public void setStj2TaxtypeCode(String stj2TaxtypeCode) {
        this.stj2TaxtypeCode = stj2TaxtypeCode;
    }

    @Transient
    private String stj2RatetypeCode;
    public String getStj2RatetypeCode() {
        return stj2RatetypeCode;
    }

    public void setStj2RatetypeCode(String stj2RatetypeCode) {
        this.stj2RatetypeCode = stj2RatetypeCode;
    }

    @Transient
    private String stj2GoodSvcTypeCode;
    public String getStj2GoodSvcTypeCode() {
        return stj2GoodSvcTypeCode;
    }

    public void setStj2GoodSvcTypeCode(String stj2GoodSvcTypeCode) {
        this.stj2GoodSvcTypeCode = stj2GoodSvcTypeCode;
    }

    @Transient
    private String stj2CitationRef;
    public String getStj2CitationRef() {
        return stj2CitationRef;
    }

    public void setStj2CitationRef(String stj2CitationRef) {
        this.stj2CitationRef = stj2CitationRef;
    }

    @Transient
    private String stj2ExemptReason;
    public String getStj2ExemptReason() {
        return stj2ExemptReason;
    }

    public void setStj2ExemptReason(String stj2ExemptReason) {
        this.stj2ExemptReason = stj2ExemptReason;
    }

    @Transient
    private String stj2GroupByDriver;
    public String getStj2GroupByDriver() {
        return stj2GroupByDriver;
    }

    public void setStj2GroupByDriver(String stj2GroupByDriver) { this.stj2GroupByDriver = stj2GroupByDriver; }

    @Transient
    private String stj2AllocBucket;
    public String getStj2AllocBucket() {
        return stj2AllocBucket;
    }

    public void setStj2AllocBucket(String stj2AllocBucket) {
        this.stj2AllocBucket = stj2AllocBucket;
    }

    @Transient
    private String stj2AllocProrateByCode;
    public String getStj2AllocProrateByCode() {
        return stj2AllocProrateByCode;
    }

    public void setStj2AllocProrateByCode(String stj2AllocProrateByCode) { this.stj2AllocProrateByCode = stj2AllocProrateByCode; }

    @Transient
    private String stj2AllocConditionCode;
    public String getStj2AllocConditionCode() { return stj2AllocConditionCode; }

    public void setStj2AllocConditionCode(String stj2AllocConditionCode) { this.stj2AllocConditionCode = stj2AllocConditionCode; }

    @Transient
    private String stj3TaxtypeCode;
    public String getStj3TaxtypeCode() {
        return stj3TaxtypeCode;
    }

    public void setStj3TaxtypeCode(String stj3TaxtypeCode) {
        this.stj3TaxtypeCode = stj3TaxtypeCode;
    }

    @Transient
    private String stj3RatetypeCode;
    public String getStj3RatetypeCode() {
        return stj3RatetypeCode;
    }

    public void setStj3RatetypeCode(String stj3RatetypeCode) {
        this.stj3RatetypeCode = stj3RatetypeCode;
    }

    @Transient
    private String stj3GoodSvcTypeCode;
    public String getStj3GoodSvcTypeCode() {
        return stj3GoodSvcTypeCode;
    }

    public void setStj3GoodSvcTypeCode(String stj3GoodSvcTypeCode) {
        this.stj3GoodSvcTypeCode = stj3GoodSvcTypeCode;
    }

    @Transient
    private String stj3CitationRef;
    public String getStj3CitationRef() {
        return stj3CitationRef;
    }

    public void setStj3CitationRef(String stj3CitationRef) {
        this.stj3CitationRef = stj3CitationRef;
    }

    @Transient
    private String stj3ExemptReason;
    public String getStj3ExemptReason() {
        return stj3ExemptReason;
    }

    public void setStj3ExemptReason(String stj3ExemptReason) {
        this.stj3ExemptReason = stj3ExemptReason;
    }

    @Transient
    private String stj3GroupByDriver;
    public String getStj3GroupByDriver() {
        return stj3GroupByDriver;
    }

    public void setStj3GroupByDriver(String stj3GroupByDriver) { this.stj3GroupByDriver = stj3GroupByDriver; }

    @Transient
    private String stj3AllocBucket;
    public String getStj3AllocBucket() {
        return stj3AllocBucket;
    }

    public void setStj3AllocBucket(String stj3AllocBucket) {
        this.stj3AllocBucket = stj3AllocBucket;
    }

    @Transient
    private String stj3AllocProrateByCode;
    public String getStj3AllocProrateByCode() {
        return stj3AllocProrateByCode;
    }

    public void setStj3AllocProrateByCode(String stj3AllocProrateByCode) { this.stj3AllocProrateByCode = stj3AllocProrateByCode; }

    @Transient
    private String stj3AllocConditionCode;
    public String getStj3AllocConditionCode() {
        return stj3AllocConditionCode;
    }

    public void setStj3AllocConditionCode(String stj3AllocConditionCode) { this.stj3AllocConditionCode = stj3AllocConditionCode; }

    @Transient
    private String stj4TaxtypeCode;
    public String getStj4TaxtypeCode() {
        return stj4TaxtypeCode;
    }

    public void setStj4TaxtypeCode(String stj4TaxtypeCode) {
        this.stj4TaxtypeCode = stj4TaxtypeCode;
    }

    @Transient
    private String stj4RatetypeCode;
    public String getStj4RatetypeCode() {
        return stj4RatetypeCode;
    }

    public void setStj4RatetypeCode(String stj4RatetypeCode) {
        this.stj4RatetypeCode = stj4RatetypeCode;
    }

    @Transient
    private String stj4GoodSvcTypeCode;
    public String getStj4GoodSvcTypeCode() {
        return stj4GoodSvcTypeCode;
    }

    public void setStj4GoodSvcTypeCode(String stj4GoodSvcTypeCode) {
        this.stj4GoodSvcTypeCode = stj4GoodSvcTypeCode;
    }

    @Transient
    private String stj4CitationRef;
    public String getStj4CitationRef() {
        return stj4CitationRef;
    }

    public void setStj4CitationRef(String stj4CitationRef) {
        this.stj4CitationRef = stj4CitationRef;
    }

    @Transient
    private String stj4ExemptReason;
    public String getStj4ExemptReason() {
        return stj4ExemptReason;
    }

    public void setStj4ExemptReason(String stj4ExemptReason) {
        this.stj4ExemptReason = stj4ExemptReason;
    }

    @Transient
    private String stj4GroupByDriver;
    public String getStj4GroupByDriver() {
        return stj4GroupByDriver;
    }

    public void setStj4GroupByDriver(String stj4GroupByDriver) { this.stj4GroupByDriver = stj4GroupByDriver; }

    @Transient
    private String stj4AllocBucket;
    public String getStj4AllocBucket() {
        return stj4AllocBucket;
    }

    public void setStj4AllocBucket(String stj4AllocBucket) {
        this.stj4AllocBucket = stj4AllocBucket;
    }

    @Transient
    private String stj4AllocProrateByCode;
    public String getStj4AllocProrateByCode() {
        return stj4AllocProrateByCode;
    }

    public void setStj4AllocProrateByCode(String stj4AllocProrateByCode) { this.stj4AllocProrateByCode = stj4AllocProrateByCode; }

    @Transient
    private String stj4AllocConditionCode;
    public String getStj4AllocConditionCode() {
        return stj4AllocConditionCode;
    }

    public void setStj4AllocConditionCode(String stj4AllocConditionCode) { this.stj4AllocConditionCode = stj4AllocConditionCode; }

    @Transient
    private String stj5TaxtypeCode;
    public String getStj5TaxtypeCode() {
        return stj5TaxtypeCode;
    }

    public void setStj5TaxtypeCode(String stj5TaxtypeCode) {
        this.stj5TaxtypeCode = stj5TaxtypeCode;
    }

    @Transient
    private String stj5RatetypeCode;
    public String getStj5RatetypeCode() {
        return stj5RatetypeCode;
    }

    public void setStj5RatetypeCode(String stj5RatetypeCode) {
        this.stj5RatetypeCode = stj5RatetypeCode;
    }

    @Transient
    private String stj5GoodSvcTypeCode;
    public String getStj5GoodSvcTypeCode() {
        return stj5GoodSvcTypeCode;
    }

    public void setStj5GoodSvcTypeCode(String stj5GoodSvcTypeCode) {
        this.stj5GoodSvcTypeCode = stj5GoodSvcTypeCode;
    }

    @Transient
    private String stj5CitationRef;
    public String getStj5CitationRef() {
        return stj5CitationRef;
    }

    public void setStj5CitationRef(String stj5CitationRef) {
        this.stj5CitationRef = stj5CitationRef;
    }

    @Transient
    private String stj5ExemptReason;
    public String getStj5ExemptReason() {
        return stj5ExemptReason;
    }

    public void setStj5ExemptReason(String stj5ExemptReason) {
        this.stj5ExemptReason = stj5ExemptReason;
    }

    @Transient
    private String stj5GroupByDriver;
    public String getStj5GroupByDriver() {
        return stj5GroupByDriver;
    }

    public void setStj5GroupByDriver(String stj5GroupByDriver) { this.stj5GroupByDriver = stj5GroupByDriver; }

    @Transient
    private String stj5AllocBucket;
    public String getStj5AllocBucket() {
        return stj5AllocBucket;
    }

    public void setStj5AllocBucket(String stj5AllocBucket) {
        this.stj5AllocBucket = stj5AllocBucket;
    }

    @Transient
    private String stj5AllocProrateByCode;
    public String getStj5AllocProrateByCode() {
        return stj5AllocProrateByCode;
    }

    public void setStj5AllocProrateByCode(String stj5AllocProrateByCode) { this.stj5AllocProrateByCode = stj5AllocProrateByCode; }

    @Transient
    private String stj5AllocConditionCode;
    public String getStj5AllocConditionCode() {
        return stj5AllocConditionCode;
    }

    public void setStj5AllocConditionCode(String stj5AllocConditionCode) { this.stj5AllocConditionCode = stj5AllocConditionCode; }

    @Transient
    private String stj6TaxtypeCode;
    public String getStj6TaxtypeCode() {
        return stj6TaxtypeCode;
    }

    public void setStj6TaxtypeCode(String stj6TaxtypeCode) {
        this.stj6TaxtypeCode = stj6TaxtypeCode;
    }

    @Transient
    private String stj6RatetypeCode;
    public String getStj6RatetypeCode() {
        return stj6RatetypeCode;
    }

    public void setStj6RatetypeCode(String stj6RatetypeCode) {
        this.stj6RatetypeCode = stj6RatetypeCode;
    }

    @Transient
    private String stj6GoodSvcTypeCode;
    public String getStj6GoodSvcTypeCode() {
        return stj6GoodSvcTypeCode;
    }

    public void setStj6GoodSvcTypeCode(String stj6GoodSvcTypeCode) {
        this.stj6GoodSvcTypeCode = stj6GoodSvcTypeCode;
    }

    @Transient
    private String stj6CitationRef;
    public String getStj6CitationRef() {
        return stj6CitationRef;
    }

    public void setStj6CitationRef(String stj6CitationRef) {
        this.stj6CitationRef = stj6CitationRef;
    }

    @Transient
    private String stj6ExemptReason;
    public String getStj6ExemptReason() {
        return stj6ExemptReason;
    }

    public void setStj6ExemptReason(String stj6ExemptReason) {
        this.stj6ExemptReason = stj6ExemptReason;
    }

    @Transient
    private String stj6GroupByDriver;
    public String getStj6GroupByDriver() {
        return stj6GroupByDriver;
    }

    public void setStj6GroupByDriver(String stj6GroupByDriver) { this.stj6GroupByDriver = stj6GroupByDriver; }

    @Transient
    private String stj6AllocBucket;
    public String getStj6AllocBucket() {
        return stj6AllocBucket;
    }

    public void setStj6AllocBucket(String stj6AllocBucket) {
        this.stj6AllocBucket = stj6AllocBucket;
    }

    @Transient
    private String stj6AllocProrateByCode;
    public String getStj6AllocProrateByCode() {
        return stj6AllocProrateByCode;
    }

    public void setStj6AllocProrateByCode(String stj6AllocProrateByCode) { this.stj6AllocProrateByCode = stj6AllocProrateByCode; }

    @Transient
    private String stj6AllocConditionCode;
    public String getStj6AllocConditionCode() {
        return stj6AllocConditionCode;
    }

    public void setStj6AllocConditionCode(String stj6AllocConditionCode) { this.stj6AllocConditionCode = stj6AllocConditionCode; }

    @Transient
    private String stj7TaxtypeCode;
    public String getStj7TaxtypeCode() {
        return stj7TaxtypeCode;
    }

    public void setStj7TaxtypeCode(String stj7TaxtypeCode) {
        this.stj7TaxtypeCode = stj7TaxtypeCode;
    }

    @Transient
    private String stj7RatetypeCode;
    public String getStj7RatetypeCode() {
        return stj7RatetypeCode;
    }

    public void setStj7RatetypeCode(String stj7RatetypeCode) {
        this.stj7RatetypeCode = stj7RatetypeCode;
    }

    @Transient
    private String stj7GoodSvcTypeCode;
    public String getStj7GoodSvcTypeCode() {
        return stj7GoodSvcTypeCode;
    }

    public void setStj7GoodSvcTypeCode(String stj7GoodSvcTypeCode) {
        this.stj7GoodSvcTypeCode = stj7GoodSvcTypeCode;
    }

    @Transient
    private String stj7CitationRef;
    public String getStj7CitationRef() {
        return stj7CitationRef;
    }

    public void setStj7CitationRef(String stj7CitationRef) {
        this.stj7CitationRef = stj7CitationRef;
    }

    @Transient
    private String stj7ExemptReason;
    public String getStj7ExemptReason() {
        return stj7ExemptReason;
    }

    public void setStj7ExemptReason(String stj7ExemptReason) {
        this.stj7ExemptReason = stj7ExemptReason;
    }

    @Transient
    private String stj7GroupByDriver;
    public String getStj7GroupByDriver() {
        return stj7GroupByDriver;
    }

    public void setStj7GroupByDriver(String stj7GroupByDriver) { this.stj7GroupByDriver = stj7GroupByDriver; }

    @Transient
    private String stj7AllocBucket;
    public String getStj7AllocBucket() {
        return stj7AllocBucket;
    }

    public void setStj7AllocBucket(String stj7AllocBucket) {
        this.stj7AllocBucket = stj7AllocBucket;
    }
    
    @Transient
    private String stj7AllocProrateByCode;
    public String getStj7AllocProrateByCode() {
        return stj7AllocProrateByCode;
    }

    public void setStj7AllocProrateByCode(String stj7AllocProrateByCode) { this.stj7AllocProrateByCode = stj7AllocProrateByCode; }

    @Transient
    private String stj7AllocConditionCode;
    public String getStj7AllocConditionCode() {
        return stj7AllocConditionCode;
    }

    public void setStj7AllocConditionCode(String stj7AllocConditionCode) { this.stj7AllocConditionCode = stj7AllocConditionCode; }

    @Transient
    private String stj8TaxtypeCode;
    public String getStj8TaxtypeCode() {
        return stj8TaxtypeCode;
    }

    public void setStj8TaxtypeCode(String stj8TaxtypeCode) {
        this.stj8TaxtypeCode = stj8TaxtypeCode;
    }

    @Transient
    private String stj8RatetypeCode;
    public String getStj8RatetypeCode() {
        return stj8RatetypeCode;
    }

    public void setStj8RatetypeCode(String stj8RatetypeCode) {
        this.stj8RatetypeCode = stj8RatetypeCode;
    }

    @Transient
    private String stj8GoodSvcTypeCode;
    public String getStj8GoodSvcTypeCode() {
        return stj8GoodSvcTypeCode;
    }

    public void setStj8GoodSvcTypeCode(String stj8GoodSvcTypeCode) {
        this.stj8GoodSvcTypeCode = stj8GoodSvcTypeCode;
    }

    @Transient
    private String stj8CitationRef;
    public String getStj8CitationRef() {
        return stj8CitationRef;
    }

    public void setStj8CitationRef(String stj8CitationRef) {
        this.stj8CitationRef = stj8CitationRef;
    }

    @Transient
    private String stj8ExemptReason;
    public String getStj8ExemptReason() {
        return stj8ExemptReason;
    }

    public void setStj8ExemptReason(String stj8ExemptReason) {
        this.stj8ExemptReason = stj8ExemptReason;
    }

    @Transient
    private String stj8GroupByDriver;
    public String getStj8GroupByDriver() {
        return stj8GroupByDriver;
    }

    public void setStj8GroupByDriver(String stj8GroupByDriver) { this.stj8GroupByDriver = stj8GroupByDriver; }

    @Transient
    private String stj8AllocBucket;
    public String getStj8AllocBucket() {
        return stj8AllocBucket;
    }

    public void setStj8AllocBucket(String stj8AllocBucket) {
        this.stj8AllocBucket = stj8AllocBucket;
    }

    @Transient
    private String stj8AllocProrateByCode;
    public String getStj8AllocProrateByCode() {
        return stj8AllocProrateByCode;
    }

    public void setStj8AllocProrateByCode(String stj8AllocProrateByCode) { this.stj8AllocProrateByCode = stj8AllocProrateByCode; }

    @Transient
    private String stj8AllocConditionCode;
    public String getStj8AllocConditionCode() {
        return stj8AllocConditionCode;
    }

    public void setStj8AllocConditionCode(String stj8AllocConditionCode) { this.stj8AllocConditionCode = stj8AllocConditionCode; }

    @Transient
    private String stj9TaxtypeCode;
    public String getStj9TaxtypeCode() {
        return stj9TaxtypeCode;
    }

    public void setStj9TaxtypeCode(String stj9TaxtypeCode) {
        this.stj9TaxtypeCode = stj9TaxtypeCode;
    }

    @Transient
    private String stj9RatetypeCode;
    public String getStj9RatetypeCode() {
        return stj9RatetypeCode;
    }

    public void setStj9RatetypeCode(String stj9RatetypeCode) {
        this.stj9RatetypeCode = stj9RatetypeCode;
    }

    @Transient
    private String stj9GoodSvcTypeCode;
    public String getStj9GoodSvcTypeCode() {
        return stj9GoodSvcTypeCode;
    }

    public void setStj9GoodSvcTypeCode(String stj9GoodSvcTypeCode) {
        this.stj9GoodSvcTypeCode = stj9GoodSvcTypeCode;
    }

    @Transient
    private String stj9CitationRef;
    public String getStj9CitationRef() {
        return stj9CitationRef;
    }

    public void setStj9CitationRef(String stj9CitationRef) {
        this.stj9CitationRef = stj9CitationRef;
    }

    @Transient
    private String stj9ExemptReason;
    public String getStj9ExemptReason() {
        return stj9ExemptReason;
    }

    public void setStj9ExemptReason(String stj9ExemptReason) {
        this.stj9ExemptReason = stj9ExemptReason;
    }

    @Transient
    private String stj9GroupByDriver;
    public String getStj9GroupByDriver() {
        return stj9GroupByDriver;
    }

    public void setStj9GroupByDriver(String stj9GroupByDriver) { this.stj9GroupByDriver = stj9GroupByDriver; }

    @Transient
    private String stj9AllocBucket;
    public String getStj9AllocBucket() {
        return stj9AllocBucket;
    }

    public void setStj9AllocBucket(String stj9AllocBucket) {
        this.stj9AllocBucket = stj9AllocBucket;
    }

    @Transient
    private String stj9AllocProrateByCode;
    public String getStj9AllocProrateByCode() {
        return stj9AllocProrateByCode;
    }

    public void setStj9AllocProrateByCode(String stj9AllocProrateByCode) { this.stj9AllocProrateByCode = stj9AllocProrateByCode; }

    @Transient
    private String stj9AllocConditionCode;
    public String getStj9AllocConditionCode() {
        return stj9AllocConditionCode;
    }

    public void setStj9AllocConditionCode(String stj9AllocConditionCode) { this.stj9AllocConditionCode = stj9AllocConditionCode; }

    @Transient
    private String stj10TaxtypeCode;
    public String getStj10TaxtypeCode() {
        return stj10TaxtypeCode;
    }

    public void setStj10TaxtypeCode(String stj10TaxtypeCode) {
        this.stj10TaxtypeCode = stj10TaxtypeCode;
    }

    @Transient
    private String stj10RatetypeCode;
    public String getStj10RatetypeCode() {
        return stj10RatetypeCode;
    }

    public void setStj10RatetypeCode(String stj10RatetypeCode) {
        this.stj10RatetypeCode = stj10RatetypeCode;
    }

    @Transient
    private String stj10GoodSvcTypeCode;
    public String getStj10GoodSvcTypeCode() {
        return stj10GoodSvcTypeCode;
    }

    public void setStj10GoodSvcTypeCode(String stj10GoodSvcTypeCode) {
        this.stj10GoodSvcTypeCode = stj10GoodSvcTypeCode;
    }

    @Transient
    private String stj10CitationRef;
    public String getStj10CitationRef() {
        return stj10CitationRef;
    }

    public void setStj10CitationRef(String stj10CitationRef) {
        this.stj10CitationRef = stj10CitationRef;
    }

    @Transient
    private String stj10ExemptReason;
    public String getStj10ExemptReason() {
        return stj10ExemptReason;
    }

    public void setStj10ExemptReason(String stj10ExemptReason) {
        this.stj10ExemptReason = stj10ExemptReason;
    }

    @Transient
    private String stj10GroupByDriver;
    public String getStj10GroupByDriver() {
        return stj10GroupByDriver;
    }

    public void setStj10GroupByDriver(String stj10GroupByDriver) { this.stj10GroupByDriver = stj10GroupByDriver; }

    @Transient
    private String stj10AllocBucket;
    public String getStj10AllocBucket() {
        return stj10AllocBucket;
    }

    public void setStj10AllocBucket(String stj10AllocBucket) {
        this.stj10AllocBucket = stj10AllocBucket;
    }

    @Transient
    private String stj10AllocProrateByCode;
    public String getStj10AllocProrateByCode() {
        return stj10AllocProrateByCode;
    }

    public void setStj10AllocProrateByCode(String stj10AllocProrateByCode) { this.stj10AllocProrateByCode = stj10AllocProrateByCode; }

    @Transient
    private String stj10AllocConditionCode;
    public String getStj10AllocConditionCode() {
        return stj10AllocConditionCode;
    }

    public void setStj10AllocConditionCode(String stj10AllocConditionCode) { this.stj10AllocConditionCode = stj10AllocConditionCode; }

    @Transient
    private String countryNexusTypeCode;
    public String getCountryNexusTypeCode() {
        return countryNexusTypeCode;
    }

    public void setCountryNexusTypeCode(String countryNexusTypeCode) {
        this.countryNexusTypeCode = countryNexusTypeCode;
    }

    @Transient
    private String stateNexusTypeCode;
    public String getStateNexusTypeCode() {
        return stateNexusTypeCode;
    }

    public void setStateNexusTypeCode(String stateNexusTypeCode) {
        this.stateNexusTypeCode = stateNexusTypeCode;
    }

    @Transient
    private String countyNexusTypeCode;
    public String getCountyNexusTypeCode() {
        return countyNexusTypeCode;
    }

    public void setCountyNexusTypeCode(String countyNexusTypeCode) {
        this.countyNexusTypeCode = countyNexusTypeCode;
    }

    @Transient
    private String cityNexusTypeCode;
    public String getCityNexusTypeCode() {
        return cityNexusTypeCode;
    }

    public void setCityNexusTypeCode(String cityNexusTypeCode) {
        this.cityNexusTypeCode = cityNexusTypeCode;
    }

    @Transient
    private String stj1NexusTypeCode;
    public String getStj1NexusTypeCode() {
        return stj1NexusTypeCode;
    }

    public void setStj1NexusTypeCode(String stj1NexusTypeCode) {
        this.stj1NexusTypeCode = stj1NexusTypeCode;
    }

    @Transient
    private String stj2NexusTypeCode;
    public String getStj2NexusTypeCode() {
        return stj2NexusTypeCode;
    }

    public void setStj2NexusTypeCode(String stj2NexusTypeCode) {
        this.stj2NexusTypeCode = stj2NexusTypeCode;
    }

    @Transient
    private String stj3NexusTypeCode;
    public String getStj3NexusTypeCode() {
        return stj3NexusTypeCode;
    }

    public void setStj3NexusTypeCode(String stj3NexusTypeCode) {
        this.stj3NexusTypeCode = stj3NexusTypeCode;
    }

    @Transient
    private String stj4NexusTypeCode;
    public String getStj4NexusTypeCode() {
        return stj4NexusTypeCode;
    }

    public void setStj4NexusTypeCode(String stj4NexusTypeCode) {
        this.stj4NexusTypeCode = stj4NexusTypeCode;
    }

    @Transient
    private String stj5NexusTypeCode;
    public String getStj5NexusTypeCode() {
        return stj5NexusTypeCode;
    }

    public void setStj5NexusTypeCode(String stj5NexusTypeCode) {
        this.stj5NexusTypeCode = stj5NexusTypeCode;
    }

    @Transient
    private String stj6NexusTypeCode;
    public String getStj6NexusTypeCode() {
        return stj6NexusTypeCode;
    }

    public void setStj6NexusTypeCode(String stj6NexusTypeCode) {
        this.stj6NexusTypeCode = stj6NexusTypeCode;
    }

    @Transient
    private String stj7NexusTypeCode;
    public String getStj7NexusTypeCode() {
        return stj7NexusTypeCode;
    }

    public void setStj7NexusTypeCode(String stj7NexusTypeCode) {
        this.stj7NexusTypeCode = stj7NexusTypeCode;
    }

    @Transient
    private String stj8NexusTypeCode;
    public String getStj8NexusTypeCode() {
        return stj8NexusTypeCode;
    }

    public void setStj8NexusTypeCode(String stj8NexusTypeCode) {
        this.stj8NexusTypeCode = stj8NexusTypeCode;
    }

    @Transient
    private String stj9NexusTypeCode;
    public String getStj9NexusTypeCode() {
        return stj9NexusTypeCode;
    }

    public void setStj9NexusTypeCode(String stj9NexusTypeCode) {
        this.stj9NexusTypeCode = stj9NexusTypeCode;
    }

    @Transient
    private String stj10NexusTypeCode;
    public String getStj10NexusTypeCode() {
        return stj10NexusTypeCode;
    }

    public void setStj10NexusTypeCode(String stj10NexusTypeCode) {
        this.stj10NexusTypeCode = stj10NexusTypeCode;
    }

    @Transient
    private String countryPrimarySitusCode;
    public String getCountryPrimarySitusCode() {
        return countryPrimarySitusCode;
    }

    public void setCountryPrimarySitusCode(String countryPrimarySitusCode) {
        this.countryPrimarySitusCode = countryPrimarySitusCode;
    }

    @Transient
    private String countryPrimaryTaxtypeCode;
    public String getCountryPrimaryTaxtypeCode() {
        return countryPrimaryTaxtypeCode;
    }

    public void setCountryPrimaryTaxtypeCode(String countryPrimaryTaxtypeCode) {
        this.countryPrimaryTaxtypeCode = countryPrimaryTaxtypeCode;
    }

    @Transient
    private String countrySecondarySitusCode;
    public String getCountrySecondarySitusCode() {
        return countrySecondarySitusCode;
    }

    public void setCountrySecondarySitusCode(String countrySecondarySitusCode) {
        this.countrySecondarySitusCode = countrySecondarySitusCode;
    }

    @Transient
    private String countrySecondaryTaxtypeCode;
    public String getCountrySecondaryTaxtypeCode() {
        return countrySecondaryTaxtypeCode;
    }

    public void setCountrySecondaryTaxtypeCode(String countrySecondaryTaxtypeCode) {
        this.countrySecondaryTaxtypeCode = countrySecondaryTaxtypeCode;
    }

    @Transient
    private String countryAllLevelsFlag;
    public String getCountryAllLevelsFlag() {
        return countryAllLevelsFlag;
    }

    public void setCountryAllLevelsFlag(String countryAllLevelsFlag) {
        this.countryAllLevelsFlag = countryAllLevelsFlag;
    }

    @Transient
    private String statePrimarySitusCode;
    public String getStatePrimarySitusCode() {
        return statePrimarySitusCode;
    }

    public void setStatePrimarySitusCode(String statePrimarySitusCode) {
        this.statePrimarySitusCode = statePrimarySitusCode;
    }

    @Transient
    private String statePrimaryTaxtypeCode;
    public String getStatePrimaryTaxtypeCode() {
        return statePrimaryTaxtypeCode;
    }

    public void setStatePrimaryTaxtypeCode(String statePrimaryTaxtypeCode) {
        this.statePrimaryTaxtypeCode = statePrimaryTaxtypeCode;
    }

    @Transient
    private String stateSecondarySitusCode;
    public String getStateSecondarySitusCode() {
        return stateSecondarySitusCode;
    }

    public void setStateSecondarySitusCode(String stateSecondarySitusCode) {
        this.stateSecondarySitusCode = stateSecondarySitusCode;
    }

    @Transient
    private String stateSecondaryTaxtypeCode;
    public String getStateSecondaryTaxtypeCode() {
        return stateSecondaryTaxtypeCode;
    }

    public void setStateSecondaryTaxtypeCode(String stateSecondaryTaxtypeCode) {
        this.stateSecondaryTaxtypeCode = stateSecondaryTaxtypeCode;
    }

    @Transient
    private String stateAllLevelsFlag;
    public String getStateAllLevelsFlag() {
        return stateAllLevelsFlag;
    }

    public void setStateAllLevelsFlag(String stateAllLevelsFlag) {
        this.stateAllLevelsFlag = stateAllLevelsFlag;
    }

    @Transient
    private String countyPrimarySitusCode;
    public String getCountyPrimarySitusCode() {
        return countyPrimarySitusCode;
    }

    public void setCountyPrimarySitusCode(String countyPrimarySitusCode) {
        this.countyPrimarySitusCode = countyPrimarySitusCode;
    }

    @Transient
    private String countyPrimaryTaxtypeCode;
    public String getCountyPrimaryTaxtypeCode() {
        return countyPrimaryTaxtypeCode;
    }

    public void setCountyPrimaryTaxtypeCode(String countyPrimaryTaxtypeCode) {
        this.countyPrimaryTaxtypeCode = countyPrimaryTaxtypeCode;
    }

    @Transient
    private String countySecondarySitusCode;
    public String getCountySecondarySitusCode() {
        return countySecondarySitusCode;
    }

    public void setCountySecondarySitusCode(String countySecondarySitusCode) {
        this.countySecondarySitusCode = countySecondarySitusCode;
    }

    @Transient
    private String countySecondaryTaxtypeCode;
    public String getCountySecondaryTaxtypeCode() {
        return countySecondaryTaxtypeCode;
    }

    public void setCountySecondaryTaxtypeCode(String countySecondaryTaxtypeCode) {
        this.countySecondaryTaxtypeCode = countySecondaryTaxtypeCode;
    }

    @Transient
    private String countyAllLevelsFlag;
    public String getCountyAllLevelsFlag() {
        return countyAllLevelsFlag;
    }

    public void setCountyAllLevelsFlag(String countyAllLevelsFlag) {
        this.countyAllLevelsFlag = countyAllLevelsFlag;
    }

    @Transient
    private String cityPrimarySitusCode;
    public String getCityPrimarySitusCode() {
        return cityPrimarySitusCode;
    }

    public void setCityPrimarySitusCode(String cityPrimarySitusCode) {
        this.cityPrimarySitusCode = cityPrimarySitusCode;
    }

    @Transient
    private String cityPrimaryTaxtypeCode;
    public String getCityPrimaryTaxtypeCode() {
        return cityPrimaryTaxtypeCode;
    }

    public void setCityPrimaryTaxtypeCode(String cityPrimaryTaxtypeCode) {
        this.cityPrimaryTaxtypeCode = cityPrimaryTaxtypeCode;
    }

    @Transient
    private String citySecondarySitusCode;
    public String getCitySecondarySitusCode() {
        return citySecondarySitusCode;
    }

    public void setCitySecondarySitusCode(String citySecondarySitusCode) {
        this.citySecondarySitusCode = citySecondarySitusCode;
    }

    @Transient
    private String citySecondaryTaxtypeCode;
    public String getCitySecondaryTaxtypeCode() {
        return citySecondaryTaxtypeCode;
    }

    public void setCitySecondaryTaxtypeCode(String citySecondaryTaxtypeCode) {
        this.citySecondaryTaxtypeCode = citySecondaryTaxtypeCode;
    }

    @Transient
    private String cityAllLevelsFlag;
    public String getCityAllLevelsFlag() {
        return cityAllLevelsFlag;
    }

    public void setCityAllLevelsFlag(String cityAllLevelsFlag) {
        this.cityAllLevelsFlag = cityAllLevelsFlag;
    }

    @Transient
    private String stj1PrimarySitusCode;
    public String getStj1PrimarySitusCode() {
        return stj1PrimarySitusCode;
    }

    public void setStj1PrimarySitusCode(String stj1PrimarySitusCode) {
        this.stj1PrimarySitusCode = stj1PrimarySitusCode;
    }

    @Transient
    private String stj1PrimaryTaxtypeCode;
    public String getStj1PrimaryTaxtypeCode() {
        return stj1PrimaryTaxtypeCode;
    }

    public void setStj1PrimaryTaxtypeCode(String stj1PrimaryTaxtypeCode) {
        this.stj1PrimaryTaxtypeCode = stj1PrimaryTaxtypeCode;
    }

    @Transient
    private String stj1SecondarySitusCode;
    public String getStj1SecondarySitusCode() {
        return stj1SecondarySitusCode;
    }

    public void setStj1SecondarySitusCode(String stj1SecondarySitusCode) {
        this.stj1SecondarySitusCode = stj1SecondarySitusCode;
    }

    @Transient
    private String stj1SecondaryTaxtypeCode;
    public String getStj1SecondaryTaxtypeCode() {
        return stj1SecondaryTaxtypeCode;
    }

    public void setStj1SecondaryTaxtypeCode(String stj1SecondaryTaxtypeCode) {
        this.stj1SecondaryTaxtypeCode = stj1SecondaryTaxtypeCode;
    }

    @Transient
    private String stj1AllLevelsFlag;
    public String getStj1AllLevelsFlag() {
        return stj1AllLevelsFlag;
    }

    public void setStj1AllLevelsFlag(String stj1AllLevelsFlag) {
        this.stj1AllLevelsFlag = stj1AllLevelsFlag;
    }

    @Transient
    private String stj2PrimarySitusCode;
    public String getStj2PrimarySitusCode() {
        return stj2PrimarySitusCode;
    }

    public void setStj2PrimarySitusCode(String stj2PrimarySitusCode) {
        this.stj2PrimarySitusCode = stj2PrimarySitusCode;
    }

    @Transient
    private String stj2PrimaryTaxtypeCode;
    public String getStj2PrimaryTaxtypeCode() {
        return stj2PrimaryTaxtypeCode;
    }

    public void setStj2PrimaryTaxtypeCode(String stj2PrimaryTaxtypeCode) {
        this.stj2PrimaryTaxtypeCode = stj2PrimaryTaxtypeCode;
    }

    @Transient
    private String stj2SecondarySitusCode;
    public String getStj2SecondarySitusCode() {
        return stj2SecondarySitusCode;
    }

    public void setStj2SecondarySitusCode(String stj2SecondarySitusCode) {
        this.stj2SecondarySitusCode = stj2SecondarySitusCode;
    }

    @Transient
    private String stj2SecondaryTaxtypeCode;
    public String getStj2SecondaryTaxtypeCode() {
        return stj2SecondaryTaxtypeCode;
    }

    public void setStj2SecondaryTaxtypeCode(String stj2SecondaryTaxtypeCode) {
        this.stj2SecondaryTaxtypeCode = stj2SecondaryTaxtypeCode;
    }

    @Transient
    private String stj2AllLevelsFlag;
    public String getStj2AllLevelsFlag() {
        return stj2AllLevelsFlag;
    }

    public void setStj2AllLevelsFlag(String stj2AllLevelsFlag) {
        this.stj2AllLevelsFlag = stj2AllLevelsFlag;
    }

    @Transient
    private String stj3PrimarySitusCode;
    public String getStj3PrimarySitusCode() {
        return stj3PrimarySitusCode;
    }

    public void setStj3PrimarySitusCode(String stj3PrimarySitusCode) {
        this.stj3PrimarySitusCode = stj3PrimarySitusCode;
    }

    @Transient
    private String stj3PrimaryTaxtypeCode;
    public String getStj3PrimaryTaxtypeCode() {
        return stj3PrimaryTaxtypeCode;
    }

    public void setStj3PrimaryTaxtypeCode(String stj3PrimaryTaxtypeCode) {
        this.stj3PrimaryTaxtypeCode = stj3PrimaryTaxtypeCode;
    }

    @Transient
    private String stj3SecondarySitusCode;
    public String getStj3SecondarySitusCode() {
        return stj3SecondarySitusCode;
    }

    public void setStj3SecondarySitusCode(String stj3SecondarySitusCode) {
        this.stj3SecondarySitusCode = stj3SecondarySitusCode;
    }

    @Transient
    private String stj3SecondaryTaxtypeCode;
    public String getStj3SecondaryTaxtypeCode() {
        return stj3SecondaryTaxtypeCode;
    }

    public void setStj3SecondaryTaxtypeCode(String stj3SecondaryTaxtypeCode) {
        this.stj3SecondaryTaxtypeCode = stj3SecondaryTaxtypeCode;
    }

    @Transient
    private String stj3AllLevelsFlag;
    public String getStj3AllLevelsFlag() {
        return stj3AllLevelsFlag;
    }

    public void setStj3AllLevelsFlag(String stj3AllLevelsFlag) {
        this.stj3AllLevelsFlag = stj3AllLevelsFlag;
    }

    @Transient
    private String stj4PrimarySitusCode;
    public String getStj4PrimarySitusCode() {
        return stj4PrimarySitusCode;
    }

    public void setStj4PrimarySitusCode(String stj4PrimarySitusCode) {
        this.stj4PrimarySitusCode = stj4PrimarySitusCode;
    }

    @Transient
    private String stj4PrimaryTaxtypeCode;
    public String getStj4PrimaryTaxtypeCode() {
        return stj4PrimaryTaxtypeCode;
    }

    public void setStj4PrimaryTaxtypeCode(String stj4PrimaryTaxtypeCode) {
        this.stj4PrimaryTaxtypeCode = stj4PrimaryTaxtypeCode;
    }

    @Transient
    private String stj4SecondarySitusCode;
    public String getStj4SecondarySitusCode() {
        return stj4SecondarySitusCode;
    }

    public void setStj4SecondarySitusCode(String stj4SecondarySitusCode) {
        this.stj4SecondarySitusCode = stj4SecondarySitusCode;
    }

    @Transient
    private String stj4SecondaryTaxtypeCode;
    public String getStj4SecondaryTaxtypeCode() {
        return stj4SecondaryTaxtypeCode;
    }

    public void setStj4SecondaryTaxtypeCode(String stj4SecondaryTaxtypeCode) {
        this.stj4SecondaryTaxtypeCode = stj4SecondaryTaxtypeCode;
    }

    @Transient
    private String stj4AllLevelsFlag;
    public String getStj4AllLevelsFlag() {
        return stj4AllLevelsFlag;
    }

    public void setStj4AllLevelsFlag(String stj4AllLevelsFlag) {
        this.stj4AllLevelsFlag = stj4AllLevelsFlag;
    }

    @Transient
    private String stj5PrimarySitusCode;
    public String getStj5PrimarySitusCode() {
        return stj5PrimarySitusCode;
    }

    public void setStj5PrimarySitusCode(String stj5PrimarySitusCode) {
        this.stj5PrimarySitusCode = stj5PrimarySitusCode;
    }

    @Transient
    private String stj5PrimaryTaxtypeCode;
    public String getStj5PrimaryTaxtypeCode() {
        return stj5PrimaryTaxtypeCode;
    }

    public void setStj5PrimaryTaxtypeCode(String stj5PrimaryTaxtypeCode) {
        this.stj5PrimaryTaxtypeCode = stj5PrimaryTaxtypeCode;
    }

    @Transient
    private String stj5SecondarySitusCode;
    public String getStj5SecondarySitusCode() {
        return stj5SecondarySitusCode;
    }

    public void setStj5SecondarySitusCode(String stj5SecondarySitusCode) {
        this.stj5SecondarySitusCode = stj5SecondarySitusCode;
    }

    @Transient
    private String stj5SecondaryTaxtypeCode;
    public String getStj5SecondaryTaxtypeCode() {
        return stj5SecondaryTaxtypeCode;
    }

    public void setStj5SecondaryTaxtypeCode(String stj5SecondaryTaxtypeCode) {
        this.stj5SecondaryTaxtypeCode = stj5SecondaryTaxtypeCode;
    }

    @Transient
    private String stj5AllLevelsFlag;
    public String getStj5AllLevelsFlag() {
        return stj5AllLevelsFlag;
    }

    public void setStj5AllLevelsFlag(String stj5AllLevelsFlag) {
        this.stj5AllLevelsFlag = stj5AllLevelsFlag;
    }

    @Transient
    private String stj6PrimarySitusCode;
    public String getStj6PrimarySitusCode() {
        return stj6PrimarySitusCode;
    }

    public void setStj6PrimarySitusCode(String stj6PrimarySitusCode) {
        this.stj6PrimarySitusCode = stj6PrimarySitusCode;
    }

    @Transient
    private String stj6PrimaryTaxtypeCode;
    public String getStj6PrimaryTaxtypeCode() {
        return stj6PrimaryTaxtypeCode;
    }

    public void setStj6PrimaryTaxtypeCode(String stj6PrimaryTaxtypeCode) {
        this.stj6PrimaryTaxtypeCode = stj6PrimaryTaxtypeCode;
    }

    @Transient
    private String stj6SecondarySitusCode;
    public String getStj6SecondarySitusCode() {
        return stj6SecondarySitusCode;
    }

    public void setStj6SecondarySitusCode(String stj6SecondarySitusCode) {
        this.stj6SecondarySitusCode = stj6SecondarySitusCode;
    }

    @Transient
    private String stj6SecondaryTaxtypeCode;
    public String getStj6SecondaryTaxtypeCode() {
        return stj6SecondaryTaxtypeCode;
    }

    public void setStj6SecondaryTaxtypeCode(String stj6SecondaryTaxtypeCode) {
        this.stj6SecondaryTaxtypeCode = stj6SecondaryTaxtypeCode;
    }

    @Transient
    private String stj6AllLevelsFlag;
    public String getStj6AllLevelsFlag() {
        return stj6AllLevelsFlag;
    }

    public void setStj6AllLevelsFlag(String stj6AllLevelsFlag) {
        this.stj6AllLevelsFlag = stj6AllLevelsFlag;
    }

    @Transient
    private String stj7PrimarySitusCode;
    public String getStj7PrimarySitusCode() {
        return stj7PrimarySitusCode;
    }

    public void setStj7PrimarySitusCode(String stj7PrimarySitusCode) {
        this.stj7PrimarySitusCode = stj7PrimarySitusCode;
    }

    @Transient
    private String stj7PrimaryTaxtypeCode;
    public String getStj7PrimaryTaxtypeCode() {
        return stj7PrimaryTaxtypeCode;
    }

    public void setStj7PrimaryTaxtypeCode(String stj7PrimaryTaxtypeCode) {
        this.stj7PrimaryTaxtypeCode = stj7PrimaryTaxtypeCode;
    }

    @Transient
    private String stj7SecondarySitusCode;
    public String getStj7SecondarySitusCode() {
        return stj7SecondarySitusCode;
    }

    public void setStj7SecondarySitusCode(String stj7SecondarySitusCode) {
        this.stj7SecondarySitusCode = stj7SecondarySitusCode;
    }

    @Transient
    private String stj7SecondaryTaxtypeCode;
    public String getStj7SecondaryTaxtypeCode() {
        return stj7SecondaryTaxtypeCode;
    }

    public void setStj7SecondaryTaxtypeCode(String stj7SecondaryTaxtypeCode) {
        this.stj7SecondaryTaxtypeCode = stj7SecondaryTaxtypeCode;
    }

    @Transient
    private String stj7AllLevelsFlag;
    public String getStj7AllLevelsFlag() {
        return stj7AllLevelsFlag;
    }

    public void setStj7AllLevelsFlag(String stj7AllLevelsFlag) {
        this.stj7AllLevelsFlag = stj7AllLevelsFlag;
    }

    @Transient
    private String stj8PrimarySitusCode;
    public String getStj8PrimarySitusCode() {
        return stj8PrimarySitusCode;
    }

    public void setStj8PrimarySitusCode(String stj8PrimarySitusCode) {
        this.stj8PrimarySitusCode = stj8PrimarySitusCode;
    }

    @Transient
    private String stj8PrimaryTaxtypeCode;
    public String getStj8PrimaryTaxtypeCode() {
        return stj8PrimaryTaxtypeCode;
    }

    public void setStj8PrimaryTaxtypeCode(String stj8PrimaryTaxtypeCode) {
        this.stj8PrimaryTaxtypeCode = stj8PrimaryTaxtypeCode;
    }

    @Transient
    private String stj8SecondarySitusCode;
    public String getStj8SecondarySitusCode() {
        return stj8SecondarySitusCode;
    }

    public void setStj8SecondarySitusCode(String stj8SecondarySitusCode) {
        this.stj8SecondarySitusCode = stj8SecondarySitusCode;
    }

    @Transient
    private String stj8SecondaryTaxtypeCode;
    public String getStj8SecondaryTaxtypeCode() {
        return stj8SecondaryTaxtypeCode;
    }

    public void setStj8SecondaryTaxtypeCode(String stj8SecondaryTaxtypeCode) {
        this.stj8SecondaryTaxtypeCode = stj8SecondaryTaxtypeCode;
    }

    @Transient
    private String stj8AllLevelsFlag;
    public String getStj8AllLevelsFlag() {
        return stj8AllLevelsFlag;
    }

    public void setStj8AllLevelsFlag(String stj8AllLevelsFlag) {
        this.stj8AllLevelsFlag = stj8AllLevelsFlag;
    }

    @Transient
    private String stj9PrimarySitusCode;
    public String getStj9PrimarySitusCode() {
        return stj9PrimarySitusCode;
    }

    public void setStj9PrimarySitusCode(String stj9PrimarySitusCode) {
        this.stj9PrimarySitusCode = stj9PrimarySitusCode;
    }

    @Transient
    private String stj9PrimaryTaxtypeCode;
    public String getStj9PrimaryTaxtypeCode() {
        return stj9PrimaryTaxtypeCode;
    }

    public void setStj9PrimaryTaxtypeCode(String stj9PrimaryTaxtypeCode) {
        this.stj9PrimaryTaxtypeCode = stj9PrimaryTaxtypeCode;
    }

    @Transient
    private String stj9SecondarySitusCode;
    public String getStj9SecondarySitusCode() {
        return stj9SecondarySitusCode;
    }

    public void setStj9SecondarySitusCode(String stj9SecondarySitusCode) {
        this.stj9SecondarySitusCode = stj9SecondarySitusCode;
    }

    @Transient
    private String stj9SecondaryTaxtypeCode;
    public String getStj9SecondaryTaxtypeCode() {
        return stj9SecondaryTaxtypeCode;
    }

    public void setStj9SecondaryTaxtypeCode(String stj9SecondaryTaxtypeCode) {
        this.stj9SecondaryTaxtypeCode = stj9SecondaryTaxtypeCode;
    }

    @Transient
    private String stj9AllLevelsFlag;
    public String getStj9AllLevelsFlag() {
        return stj9AllLevelsFlag;
    }

    public void setStj9AllLevelsFlag(String stj9AllLevelsFlag) {
        this.stj9AllLevelsFlag = stj9AllLevelsFlag;
    }

    @Transient
    private String stj10PrimarySitusCode;
    public String getStj10PrimarySitusCode() {
        return stj10PrimarySitusCode;
    }

    public void setStj10PrimarySitusCode(String stj10PrimarySitusCode) {
        this.stj10PrimarySitusCode = stj10PrimarySitusCode;
    }

    @Transient
    private String stj10PrimaryTaxtypeCode;
    public String getStj10PrimaryTaxtypeCode() {
        return stj10PrimaryTaxtypeCode;
    }

    public void setStj10PrimaryTaxtypeCode(String stj10PrimaryTaxtypeCode) {
        this.stj10PrimaryTaxtypeCode = stj10PrimaryTaxtypeCode;
    }

    @Transient
    private String stj10SecondarySitusCode;
    public String getStj10SecondarySitusCode() {
        return stj10SecondarySitusCode;
    }

    public void setStj10SecondarySitusCode(String stj10SecondarySitusCode) {
        this.stj10SecondarySitusCode = stj10SecondarySitusCode;
    }

    @Transient
    private String stj10SecondaryTaxtypeCode;
    public String getStj10SecondaryTaxtypeCode() {
        return stj10SecondaryTaxtypeCode;
    }

    public void setStj10SecondaryTaxtypeCode(String stj10SecondaryTaxtypeCode) {
        this.stj10SecondaryTaxtypeCode = stj10SecondaryTaxtypeCode;
    }

    @Transient
    private String stj10AllLevelsFlag;
    public String getStj10AllLevelsFlag() {
        return stj10AllLevelsFlag;
    }

    public void setStj10AllLevelsFlag(String stj10AllLevelsFlag) {
        this.stj10AllLevelsFlag = stj10AllLevelsFlag;
    }

    @Column(name="TRANSACTION_IND", insertable=false, updatable=false)
    @Transient
    private String processStatus;

  	public String getProcessStatus() {
  		if ("P".equalsIgnoreCase(getTransactionInd())) {
  			processStatus="Processed";
  			return processStatus;

  		}
  		else if ("S".equalsIgnoreCase(getTransactionInd())) {
  			if("T".equalsIgnoreCase(getSuspendInd())){
  				processStatus = "Suspended/No G&S Matrix";          // MBF
  			}else if ("L".equalsIgnoreCase(getSuspendInd())) {
  				processStatus = "Suspended/No Location Matrix";
  			}else if ("D".equalsIgnoreCase(getSuspendInd())) {
  				processStatus = "Suspended/No TaxCode Rule";
  			}else if ("R".equalsIgnoreCase(getSuspendInd())) {
  				processStatus = "Suspended/No Tax Rate";
  			}else if ("?".equalsIgnoreCase(getSuspendInd())) {
  				processStatus = "Unknown Suspend Error";
  			}else {
  				processStatus = "Suspend Ind. Error";
  			}
  			return processStatus;
  		}
  		else if("Q".equalsIgnoreCase(getTransactionInd())){
  			processStatus = "Question";
  			return processStatus;
  		}
  		else if("H".equalsIgnoreCase(getTransactionInd())){
  			processStatus = "Held";
  			if("FS".equalsIgnoreCase(getMultiTransCode())) {
          		processStatus += "/Future Split";
          	}
  			return processStatus;
  		}                                                      // MBF
  		else if(getTransactionInd() == null) {
  			processStatus = "Unprocessed";
  			return processStatus;
  		}
  		else if ("O".startsWith(getTransactionInd())) {            // MBF
            processStatus = "Original Transaction";                // MBF
            return processStatus;                                  // MBF     
        } 
  		else {
  			processStatus = "Error";
  			return processStatus;
  		}
  	}
  	
  	@Transient
  	@XmlTransient
  	private boolean isReprocessing = false;
  	
  	public boolean getIsReprocessing() {
		return isReprocessing;
	}
	
	public void setIsReprocessing(boolean isPreprocessing) {
		this.isReprocessing = isPreprocessing;
	}
  	
  	@Transient
	private String measureTypeCode;
  	public String getMeasureTypeCode() {
		return measureTypeCode;
	}

	public void setMeasureTypeCode(String measureTypeCode) {
		this.measureTypeCode = measureTypeCode;
	}
	
	@Transient
	private String transEntityName ;
	public String getTransEntityName() {
		return transEntityName;
	}

	public void setTransEntityName(String transEntityName) {
		this.transEntityName = transEntityName;
	}

	@Transient
	private String inheritEntityName;
	
	public String getInheritEntityName() {
		return inheritEntityName;
	}

	public void setInheritEntityName(String inheritEntityName) {
		this.inheritEntityName = inheritEntityName;
	}
	
	@Transient
    private String suspendCode;
	
	@Transient
    private String reprocessFlag;
	
	@Transient
    private String vendorNexusFlag;
	
	@Transient
    private Boolean invoiceVerificationFlag;

	@Transient
    private String defaultMatrixFlag;


    @Transient
    private BigDecimal prorateAmt;

    @Transient
    private BigDecimal pushToTrans;

    @Transient
    private BigDecimal pushToState;

    @Transient
    private BigDecimal pushToCounty;

    @Transient
    private BigDecimal pushToCity;

    @Transient
    private BigDecimal pushToStj1;

    @Transient
    private BigDecimal pushToStj2;

    @Transient
    private BigDecimal pushToStj3;

    @Transient
    private BigDecimal pushToStj4;

    @Transient
    private BigDecimal pushToStj5;

    @Transient
    private BigDecimal pushToStj6;

    @Transient
    private BigDecimal pushToStj7;

    @Transient
    private BigDecimal pushToStj8;

    @Transient
    private BigDecimal pushToStj9;

    @Transient
    private BigDecimal pushToStj10;

    @Transient
    private BigDecimal pushTotal;
    
    public String getSuspendCode() {
        return suspendCode;
    }

    public void setSuspendCode(String suspendCode) {
        this.suspendCode = suspendCode;
    }

    public String getReprocessFlag() {
        return reprocessFlag;
    }

    public void setReprocessFlag(String reprocessFlag) {
        this.reprocessFlag = reprocessFlag;
    }
    
    public String getDefaultMatrixFlag() {
        return defaultMatrixFlag;
    }

    public void setDefaultMatrixFlag(String defaultMatrixFlag) {
        this.defaultMatrixFlag = defaultMatrixFlag;
    }
    
    public String getVendorNexusFlag() {
        return vendorNexusFlag;
    }

    public void setVendorNexusFlag(String vendorNexusFlag) {
        this.vendorNexusFlag = vendorNexusFlag;
    }
    
    public Boolean getInvoiceVerificationFlag() {
        return invoiceVerificationFlag;
    }

    public void setInvoiceVerificationFlag(Boolean invoiceVerificationFlag) {
        this.invoiceVerificationFlag = invoiceVerificationFlag;
    }

	public TransactionDetailDTO getTransactionDetailDTO(){
		TransactionDetailDTO transactionDetailDTO = new TransactionDetailDTO();
		BeanUtils.copyProperties(this, transactionDetailDTO);
		return transactionDetailDTO;
	}
	
    @Transient
	@XmlTransient
	private Long instanceCreatedId;
    
    public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}
  
    public String getMultiTransCodeDesc() {
		String desc = "";
		if(this.multiTransCode != null) {
			if("OS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Split";
			}
			else if("S".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Split";
			}
			else if("OA".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Allocation";
			}
			else if("A".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Allocation";
			}
			else if("FS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Future Split";
			}
			else {
				desc = this.multiTransCode;
			}
		}
		
		return desc;
	}	
    
    public Boolean getCountryTier2EntamBooleanFlag() {
    	return "1".equals(countryTier2EntamFlag);
    }
    
    public Boolean getCountryTier3EntamBooleanFlag() {
    	return "1".equals(countryTier3EntamFlag);
    }
    
    public Boolean getStateTier2EntamBooleanFlag() {
    	return "1".equals(stateTier2EntamFlag);
    }
    
    public Boolean getStateTier3EntamBooleanFlag() {
    	return "1".equals(stateTier3EntamFlag);
    }
    
    public Boolean getCountyTier2EntamBooleanFlag() {
    	return "1".equals(countyTier2EntamFlag);
    }
    
    public Boolean getCountyTier3EntamBooleanFlag() {
    	return "1".equals(countyTier3EntamFlag);
    }
    
    public Boolean getCityTier2EntamBooleanFlag() {
    	return "1".equals(cityTier2EntamFlag);
    }
    
    public Boolean getCityTier3EntamBooleanFlag() {
    	return "1".equals(cityTier3EntamFlag);
    }
    
    public Boolean getCountryTaxcodeOverBooleanFlag() {
		return "1".equals(countryTaxcodeOverFlag);
	}
    
    public Boolean getStateTaxcodeOverBooleanFlag() {
		return "1".equals(stateTaxcodeOverFlag);
	}
    
    public Boolean getCountyTaxcodeOverBooleanFlag() {
		return "1".equals(countyTaxcodeOverFlag);
	}
    
    public Boolean getCityTaxcodeOverBooleanFlag() {
		return "1".equals(cityTaxcodeOverFlag);
	}
    
    public Boolean getStj1TaxcodeOverBooleanFlag() {
		return "1".equals(stj1TaxcodeOverFlag);
	}
	
	public Boolean getStj2TaxcodeOverBooleanFlag() {
		return "1".equals(stj2TaxcodeOverFlag);
	}
	
	public Boolean getStj3TaxcodeOverBooleanFlag() {
		return "1".equals(stj3TaxcodeOverFlag);
	}
	
	public Boolean getStj4TaxcodeOverBooleanFlag() {
		return "1".equals(stj4TaxcodeOverFlag);
	}
	
	public Boolean getStj5TaxcodeOverBooleanFlag() {
		return "1".equals(stj5TaxcodeOverFlag);
	}
	
	public Boolean getStj6TaxcodeOverBooleanFlag() {
		return "1".equals(stj6TaxcodeOverFlag);
	}
	
	public Boolean getStj7TaxcodeOverBooleanFlag() {
		return "1".equals(stj7TaxcodeOverFlag);
	}
	
	public Boolean getStj8TaxcodeOverBooleanFlag() {
		return "1".equals(stj8TaxcodeOverFlag);
	}
	
	public Boolean getStj9TaxcodeOverBooleanFlag() {
		return "1".equals(stj9TaxcodeOverFlag);
	}
	
	public Boolean getStj10TaxcodeOverBooleanFlag() {
		return "1".equals(stj10TaxcodeOverFlag);
	}
	
	public BigDecimal getProrateAmt() {
		return prorateAmt;
    }
	
	public void setProrateAmt(BigDecimal prorateAmt) {
	    this.prorateAmt = prorateAmt;
	 }
	 
	public BigDecimal getPushToTrans() {
	    return pushToTrans;
    }

    public void setPushToTrans(BigDecimal pushToTrans) {
        this.pushToTrans = pushToTrans;
    }

    public BigDecimal getPushToState() {
        return pushToState;
    }

    public void setPushToState(BigDecimal pushToState) {
        this.pushToState = pushToState;
    }

    public BigDecimal getPushToCounty() {
        return pushToCounty;
    }

    public void setPushToCounty(BigDecimal pushToCounty) {
        this.pushToCounty = pushToCounty;
    }

    public BigDecimal getPushToCity() {
        return pushToCity;
    }

    public void setPushToCity(BigDecimal pushToCity) {
        this.pushToCity = pushToCity;
    }

    public BigDecimal getPushToStj1() {
        return pushToStj1;
    }

    public void setPushToStj1(BigDecimal pushToStj1) {
        this.pushToStj1 = pushToStj1;
    }

    public BigDecimal getPushToStj2() {
        return pushToStj2;
    }

    public void setPushToStj2(BigDecimal pushToStj2) {
        this.pushToStj2 = pushToStj2;
    }

    public BigDecimal getPushToStj3() {
        return pushToStj3;
    }

    public void setPushToStj3(BigDecimal pushToStj3) {
        this.pushToStj3 = pushToStj3;
    }

    public BigDecimal getPushToStj4() {
        return pushToStj4;
    }

    public void setPushToStj4(BigDecimal pushToStj4) {
        this.pushToStj4 = pushToStj4;
    }

    public BigDecimal getPushToStj5() {
        return pushToStj5;
    }

    public void setPushToStj5(BigDecimal pushToStj5) {
        this.pushToStj5 = pushToStj5;
    }

    public BigDecimal getPushToStj6() {
        return pushToStj6;
    }

    public void setPushToStj6(BigDecimal pushToStj6) {
        this.pushToStj6 = pushToStj6;
    }

    public BigDecimal getPushToStj7() {
        return pushToStj7;
    }

    public void setPushToStj7(BigDecimal pushToStj7) {
        this.pushToStj7 = pushToStj7;
    }

    public BigDecimal getPushToStj8() {
        return pushToStj8;
    }

    public void setPushToStj8(BigDecimal pushToStj8) {
        this.pushToStj8 = pushToStj8;
    }

    public BigDecimal getPushToStj9() {
        return pushToStj9;
    }

    public void setPushToStj9(BigDecimal pushToStj9) {
        this.pushToStj9 = pushToStj9;
    }

    public BigDecimal getPushToStj10() {
        return pushToStj10;
    }

    public void setPushToStj10(BigDecimal pushToStj10) {
        this.pushToStj10 = pushToStj10;
    }

    public BigDecimal getPushTotal() {
        return pushTotal;
    }

    public void setPushTotal(BigDecimal pushTotal) {
        this.pushTotal = pushTotal;
    }

	@Transient
    private BigDecimal stj1JurisdTaxRate;
    
    @Transient
    private BigDecimal stj1JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj2JurisdTaxRate;
    
    @Transient
    private BigDecimal stj2JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj3JurisdTaxRate;
    
    @Transient
    private BigDecimal stj3JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj4JurisdTaxRate;
    
    @Transient
    private BigDecimal stj4JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj5JurisdTaxRate;
    
    @Transient
    private BigDecimal stj5JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj6JurisdTaxRate;
    
    @Transient
    private BigDecimal stj6JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj7JurisdTaxRate;
    
    @Transient
    private BigDecimal stj7JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj8JurisdTaxRate;
    
    @Transient
    private BigDecimal stj8JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj9JurisdTaxRate;
    
    @Transient
    private BigDecimal stj9JurisdTaxSetAmount;
    
    @Transient
    private BigDecimal stj10JurisdTaxRate;
    
    @Transient
    private BigDecimal stj10JurisdTaxSetAmount;
    
    @Transient
	private String result;

	public BigDecimal getStj1JurisdTaxRate() {
		return stj1JurisdTaxRate;
	}

	public BigDecimal getStj1JurisdTaxSetAmount() {
		return stj1JurisdTaxSetAmount;
	}

	public void setStj1JurisdTaxRate(BigDecimal stj1JurisdTaxRate) {
		this.stj1JurisdTaxRate = stj1JurisdTaxRate;
	}

	public void setStj1JurisdTaxSetAmount(BigDecimal stj1JurisdTaxSetAmount) {
		this.stj1JurisdTaxSetAmount = stj1JurisdTaxSetAmount;
	}

	public BigDecimal getStj2JurisdTaxRate() {
		return stj2JurisdTaxRate;
	}

	public BigDecimal getStj2JurisdTaxSetAmount() {
		return stj2JurisdTaxSetAmount;
	}

	public BigDecimal getStj3JurisdTaxRate() {
		return stj3JurisdTaxRate;
	}

	public BigDecimal getStj3JurisdTaxSetAmount() {
		return stj3JurisdTaxSetAmount;
	}

	public BigDecimal getStj4JurisdTaxRate() {
		return stj4JurisdTaxRate;
	}

	public BigDecimal getStj4JurisdTaxSetAmount() {
		return stj4JurisdTaxSetAmount;
	}

	public BigDecimal getStj5JurisdTaxRate() {
		return stj5JurisdTaxRate;
	}

	public BigDecimal getStj5JurisdTaxSetAmount() {
		return stj5JurisdTaxSetAmount;
	}

	public BigDecimal getStj6JurisdTaxRate() {
		return stj6JurisdTaxRate;
	}

	public BigDecimal getStj6JurisdTaxSetAmount() {
		return stj6JurisdTaxSetAmount;
	}

	public BigDecimal getStj7JurisdTaxRate() {
		return stj7JurisdTaxRate;
	}

	public BigDecimal getStj7JurisdTaxSetAmount() {
		return stj7JurisdTaxSetAmount;
	}

	public BigDecimal getStj8JurisdTaxRate() {
		return stj8JurisdTaxRate;
	}

	public BigDecimal getStj8JurisdTaxSetAmount() {
		return stj8JurisdTaxSetAmount;
	}

	public BigDecimal getStj9JurisdTaxRate() {
		return stj9JurisdTaxRate;
	}

	public BigDecimal getStj9JurisdTaxSetAmount() {
		return stj9JurisdTaxSetAmount;
	}

	public BigDecimal getStj10JurisdTaxRate() {
		return stj10JurisdTaxRate;
	}

	public BigDecimal getStj10JurisdTaxSetAmount() {
		return stj10JurisdTaxSetAmount;
	}

	public void setStj2JurisdTaxRate(BigDecimal stj2JurisdTaxRate) {
		this.stj2JurisdTaxRate = stj2JurisdTaxRate;
	}

	public void setStj2JurisdTaxSetAmount(BigDecimal stj2JurisdTaxSetAmount) {
		this.stj2JurisdTaxSetAmount = stj2JurisdTaxSetAmount;
	}

	public void setStj3JurisdTaxRate(BigDecimal stj3JurisdTaxRate) {
		this.stj3JurisdTaxRate = stj3JurisdTaxRate;
	}

	public void setStj3JurisdTaxSetAmount(BigDecimal stj3JurisdTaxSetAmount) {
		this.stj3JurisdTaxSetAmount = stj3JurisdTaxSetAmount;
	}

	public void setStj4JurisdTaxRate(BigDecimal stj4JurisdTaxRate) {
		this.stj4JurisdTaxRate = stj4JurisdTaxRate;
	}

	public void setStj4JurisdTaxSetAmount(BigDecimal stj4JurisdTaxSetAmount) {
		this.stj4JurisdTaxSetAmount = stj4JurisdTaxSetAmount;
	}

	public void setStj5JurisdTaxRate(BigDecimal stj5JurisdTaxRate) {
		this.stj5JurisdTaxRate = stj5JurisdTaxRate;
	}

	public void setStj5JurisdTaxSetAmount(BigDecimal stj5JurisdTaxSetAmount) {
		this.stj5JurisdTaxSetAmount = stj5JurisdTaxSetAmount;
	}

	public void setStj6JurisdTaxRate(BigDecimal stj6JurisdTaxRate) {
		this.stj6JurisdTaxRate = stj6JurisdTaxRate;
	}

	public void setStj6JurisdTaxSetAmount(BigDecimal stj6JurisdTaxSetAmount) {
		this.stj6JurisdTaxSetAmount = stj6JurisdTaxSetAmount;
	}

	public void setStj7JurisdTaxRate(BigDecimal stj7JurisdTaxRate) {
		this.stj7JurisdTaxRate = stj7JurisdTaxRate;
	}

	public void setStj7JurisdTaxSetAmount(BigDecimal stj7JurisdTaxSetAmount) {
		this.stj7JurisdTaxSetAmount = stj7JurisdTaxSetAmount;
	}

	public void setStj8JurisdTaxRate(BigDecimal stj8JurisdTaxRate) {
		this.stj8JurisdTaxRate = stj8JurisdTaxRate;
	}

	public void setStj8JurisdTaxSetAmount(BigDecimal stj8JurisdTaxSetAmount) {
		this.stj8JurisdTaxSetAmount = stj8JurisdTaxSetAmount;
	}

	public void setStj9JurisdTaxRate(BigDecimal stj9JurisdTaxRate) {
		this.stj9JurisdTaxRate = stj9JurisdTaxRate;
	}

	public void setStj9JurisdTaxSetAmount(BigDecimal stj9JurisdTaxSetAmount) {
		this.stj9JurisdTaxSetAmount = stj9JurisdTaxSetAmount;
	}

	public void setStj10JurisdTaxRate(BigDecimal stj10JurisdTaxRate) {
		this.stj10JurisdTaxRate = stj10JurisdTaxRate;
	}

	public void setStj10JurisdTaxSetAmount(BigDecimal stj10JurisdTaxSetAmount) {
		this.stj10JurisdTaxSetAmount = stj10JurisdTaxSetAmount;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
    public boolean getIsCountrySalesTaxtypeUsedCode() {
		return  "S".equals(countryTaxtypeUsedCode);
    }
	public boolean getIsStateSalesTaxtypeUsedCode() {
			return  "S".equals(stateTaxtypeUsedCode);
    }
	public boolean getIsCountySalesTaxtypeUsedCode() {
			return  "S".equals(countyTaxtypeUsedCode);
	}
	public boolean getIsCitySalesTaxtypeUsedCode() {
			return  "S".equals(cityTaxtypeUsedCode);
	}
	public boolean getIsStj1SalesTaxtypeUsedCode() {
			return  "S".equals(stj1TaxtypeUsedCode);
	}
	public boolean getIsStj2SalesTaxtypeUsedCode() {
			return  "S".equals(stj2TaxtypeUsedCode);
	}
	public boolean getIsStj3SalesTaxtypeUsedCode() {
			return  "S".equals(stj3TaxtypeUsedCode);
	}
	public boolean getIsStj4SalesTaxtypeUsedCode() {
			return  "S".equals(stj4TaxtypeUsedCode);
	}
	public boolean getIsStj5SalesTaxtypeUsedCode() {
			return  "S".equals(stj5TaxtypeUsedCode);
	}
	public boolean getIsStj6SalesTaxtypeUsedCode() {
			return  "S".equals(stj6TaxtypeUsedCode);
	}
	public boolean getIsStj7SalesTaxtypeUsedCode() {
			return  "S".equals(stj7TaxtypeUsedCode);
	}
	public boolean getIsStj8SalesTaxtypeUsedCode() {
			return  "S".equals(stj8TaxtypeUsedCode);
	}
	public boolean getIsStj9SalesTaxtypeUsedCode() {
			return  "S".equals(stj9TaxtypeUsedCode);
	}
	public boolean getIsStj10SalesTaxtypeUsedCode() {
			return  "S".equals(stj10TaxtypeUsedCode);
	}
	
	public boolean getIsDisplayStj1Line() {
		if(stj1TaxAmt!=null && stj1TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj2Line() {
		if(stj2TaxAmt!=null && stj2TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj3Line() {
		if(stj3TaxAmt!=null && stj3TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj4Line() {
		if(stj4TaxAmt!=null && stj4TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj5Line() {
		if(stj5TaxAmt!=null && stj5TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj6Line() {
		if(stj6TaxAmt!=null && stj6TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj7Line() {
		if(stj7TaxAmt!=null && stj7TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj8Line() {
		if(stj8TaxAmt!=null && stj8TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj9Line() {	
		if(stj9TaxAmt!=null && stj9TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
	
	public boolean getIsDisplayStj10Line() {	
		if(stj10TaxAmt!=null && stj10TaxAmt.compareTo(BigDecimal.ZERO)!=0){
			return true;
		}
		
		return false;
	}
    
	
	@Transient
	BigDecimal lineItemTestAmt;
	
	public BigDecimal getLineItemTestAmt(){
		return this.getInvoiceLineAmt();
	}
	
	public void setLineItemTestAmt(BigDecimal lineItemTestAmt) {
		this.lineItemTestAmt = lineItemTestAmt;
		this.invoiceLineAmt = lineItemTestAmt;
		this.glLineItmDistAmt = lineItemTestAmt;
	}
}
