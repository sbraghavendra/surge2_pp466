
package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.dto.ListCodesDTO;

/**
 *  @author Muneer
 */


@Entity
@Table (name="TB_LIST_CODE")
public class ListCodes extends Auditable implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * EmbeddedId primary key field
	 */
	 @EmbeddedId
	 ListCodesPK listCodesPK;
	 
/*	@Id
    @Column(name="CODE_TYPE_CODE")
	private String codeTypeCode;
  
	@Column(name="CODE_CODE")
    private String codeCode;*/
    
    @Column(name="DESCRIPTION" )
    private String description;

    @Column(name="MESSAGE" )
    private String message;
    
    @Column(name="EXPLANATION" )
    private String explanation;

    @Column(name="SEVERITY_LEVEL" )
    private String severityLevel;

    @Column(name="WRITE_IMPORT_LINE_FLAG" )
    private String writeImportLineFlag;
 
    @Column(name="ABORT_IMPORT_FLAG" )
    private String abortImportFlag;
    
	@Transient	
	ListCodesDTO listCodesDTO;
	


	    
    // Constructors

    /** default constructor */
    public ListCodes() {
    }
    
    public ListCodes(ListCodesPK listCodesPK) { 
    	this.listCodesPK = listCodesPK;
    }
    
    public ListCodesDTO getListCodesDTO() {
    	ListCodesDTO listCodesDTO= new ListCodesDTO();
    	BeanUtils.copyProperties(this,listCodesDTO);
    	return listCodesDTO;
    }

	/**
	 * @param ListCodesDTO the ListCodesDTO to set
	 */
	public void setListCodesDTO(ListCodesDTO listCodesDTO) {
		this.listCodesDTO = listCodesDTO;
	}

	public ListCodesPK getListCodesPK() {
		return listCodesPK;
	}

	public void setListCodesPK(ListCodesPK listCodesPK) {
		this.listCodesPK = listCodesPK;
	}

	/**
	 * @return the tranDetailColName
	 */
	public String getCodeTypeCode() {
		return listCodesPK.getCodeTypeCode();
	}
	
	/**
	 * @return the driverValue
	 */
	public String getCodeCode() {
		return listCodesPK.getCodeCode();
	}
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String getSeverityLevel() {
		return severityLevel;
	}
	
	public int getSeverityLevelAsInt() {
		try {
			return Integer.parseInt(severityLevel);
		} catch (Exception e) {
			return 0;
		}
	}
	
	public boolean isWarning() {
		return getSeverityLevelAsInt() <= 10;
	}

	public void setSeverityLevel(String severityLevel) {
		this.severityLevel = severityLevel;
	}

	public String getWriteImportLineFlag() {
		return writeImportLineFlag;
	}

	public void setWriteImportLineFlag(String writeImportLineFlag) {
		this.writeImportLineFlag = writeImportLineFlag;
	}
	public boolean isWriteImportLineEnabled() {
		return "1".equalsIgnoreCase(writeImportLineFlag);
	}
	
	public String getAbortImportFlag() {
		return abortImportFlag;
	}
	public boolean isAbortEnabled() {
		return ("1".equals(abortImportFlag) || "T".equalsIgnoreCase(abortImportFlag) || "T".equalsIgnoreCase(abortImportFlag) );
	}

	public void setAbortImportFlag(String abortImportFlag) {
		this.abortImportFlag = abortImportFlag;
	}


   public String toString(){
	   StringBuffer sb = new StringBuffer();
		 
	     sb.append(" Code Type Code :\t"+listCodesPK.getCodeTypeCode());
	     sb.append(" Code Code :\t"+listCodesPK.getCodeCode()+"\n");
	     sb.append(" description :\t"+description+"\n");
	     sb.append(" message :\t"+message+"\n");
	     sb.append(" explanation :\t"+explanation+"\n");
	     sb.append(" severityLevel :\t"+severityLevel+"\n");
	     sb.append(" writeImportLineFlag :\t"+writeImportLineFlag+"\n");
	     sb.append(" abortImportFlag :\t"+abortImportFlag+"\n");
	     //sb.append(super.toString());
	     
	     return sb.toString();
   }
}

