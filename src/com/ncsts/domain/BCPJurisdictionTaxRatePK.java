package com.ncsts.domain;

/*
 * Author: Jim M. Wilson
 * Created: Aug 22, 2008
 * $Date: 2009-02-28 00:41:27 -0600 (Sat, 28 Feb 2009) $: - $Revision: 4015 $: 
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Embeddable
public class BCPJurisdictionTaxRatePK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "BATCH_ID")
	private Long batchId;

	@Column(name = "GEOCODE")
	private String geocode;

	@Column(name = "ZIP")
	private String zip;

	@Column(name="CITY")
	private String city;

	@Column(name="COUNTY")
	private String county;

	@Column(name = "JUR_EFFECTIVE_DATE")
	private Date jurEffectiveDate;
	
	@Column(name = "BCP_JURISDICTION_TAXRATE_ID")
	private Long bcpJurisdictionTaxRateId;

	public BCPJurisdictionTaxRatePK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if ((other == null) || !(other instanceof BCPJurisdictionTaxRatePK)) {
			return false;
		}

		final BCPJurisdictionTaxRatePK revision = (BCPJurisdictionTaxRatePK) other;

		return new EqualsBuilder().append(batchId, revision.batchId).append(geocode, revision.geocode)
			.append(zip, revision.zip).append(city, revision.city).append(jurEffectiveDate, revision.jurEffectiveDate).isEquals();
//		return compare(getBatchId(), revision.getBatchId()) && 
//		  compare(getGeocode(), revision.getGeocode()) &&
//		  compare(getInOut(),	revision.getInOut()) && 
//		  compare(getZip(), revision.getZip()) &&
//		  compare(getCity(),	revision.getCity()) &&
//		  compare(getEffectiveDate(), revision.getEffectiveDate());
//	}
//
//	private boolean compare(Object a, Object b) {
//		return ((a == b) || ((a != null) && a.equals(b)));
}

	/**
	 * 
	 */
	public int hashCode() {
	 return	new HashCodeBuilder().append(batchId).append(geocode).append(zip).append(city).append(jurEffectiveDate).toHashCode();
	}

	/**
	 * 
	 * @return a string representation of this object.
	 */
	public String toString() {
		return this.batchId + "::" + this.geocode + "::" + 
		  this.zip + "::" + "::" + this.city + "::" + this.jurEffectiveDate.getTime();
	}

	public Long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getGeocode() {
		return this.geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public Date getJurEffectiveDate() {
		return this.jurEffectiveDate;
	}

	public void setJurEffectiveDate(Date jurEffectiveDate) {
		this.jurEffectiveDate = jurEffectiveDate;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {return this.city;}
	public void setCity(String city) {this.city = city;}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Long getBcpJurisdictionTaxRateId() {
		return bcpJurisdictionTaxRateId;
	}

	public void setBcpJurisdictionTaxRateId(Long bcpJurisdictionTaxRateId) {
		this.bcpJurisdictionTaxRateId = bcpJurisdictionTaxRateId;
	}


}
