package com.ncsts.domain;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.ncsts.view.util.ArithmeticUtils;


@Entity
@Table(name="TB_BCP_TRANSACTIONS")
public class BCPTransactionDetail extends Auditable implements java.io.Serializable, Idable<Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="BCP_TRANSACTION_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="tr_bcp_detail_sequence")
	@SequenceGenerator(name="tr_bcp_detail_sequence" , allocationSize = 1, sequenceName="SQ_TB_BCP_TRANSACTIONS")	
	private Long bcpTransactionId;
	
	@Column(name="TRANSACTION_DETAIL_ID")
	  private Long transactionDetailId;

	@Column(name="PROCESS_BATCH_NO")
	  private Long processBatchNo;

	@Column(name="SOURCE_TRANSACTION_ID")
     private String sourceTransactionId;
	
	@Column(name="GL_EXTRACT_BATCH_NO")
     private Long glExtractBatchNo;
	
	@Column(name="ARCHIVE_BATCH_NO")
     private Long archiveBatchNo;
	
	@Column(name="ALLOCATION_MATRIX_ID")
     private Long allocationMatrixId;
	
	@Column(name="ALLOCATION_SUBTRANS_ID")
     private Long allocationSubtransId;
	
	@Column(name="ENTERED_DATE")
     private Date enteredDate;
	
	@Column(name="TRANSACTION_STATUS")
     private String transactionStatus;
	
	@Column(name="GL_DATE")
     private Date glDate;
	
	@Column(name="GL_COMPANY_NBR")
     private String glCompanyNbr;
	
	@Column(name="GL_COMPANY_NAME")
     private String glCompanyName;
	
	@Column(name="GL_DIVISION_NBR")
     private String glDivisionNbr;
	
	@Column(name="GL_DIVISION_NAME")
     private String glDivisionName;
	
	@Column(name="GL_CC_NBR_DEPT_ID")
     private String glCcNbrDeptId;
	
	@Column(name="GL_CC_NBR_DEPT_NAME")
     private String glCcNbrDeptName;
	
	@Column(name="GL_LOCAL_ACCT_NBR")
     private String glLocalAcctNbr;
	
	@Column(name="GL_LOCAL_ACCT_NAME")
     private String glLocalAcctName;
	
	@Column(name="GL_LOCAL_SUB_ACCT_NBR")
     private String glLocalSubAcctNbr;
	
	@Column(name="GL_LOCAL_SUB_ACCT_NAME")
     private String glLocalSubAcctName;
	
	@Column(name="GL_FULL_ACCT_NBR")
     private String glFullAcctNbr;
	
	@Column(name="GL_FULL_ACCT_NAME")
     private String glFullAcctName;
	
	@Column(name="GL_LINE_ITM_DIST_AMT")
     private BigDecimal glLineItmDistAmt;
	
	@Column(name="ORIG_GL_LINE_ITM_DIST_AMT")
     private BigDecimal origGlLineItmDistAmt;
	
	@Column(name="VENDOR_NBR")
     private String vendorNbr;
	
	@Column(name="VENDOR_NAME")
     private String vendorName;
	
	@Column(name="VENDOR_ADDRESS_LINE_1")
     private String vendorAddressLine1;
	
	@Column(name="VENDOR_ADDRESS_LINE_2")
     private String vendorAddressLine2;
	
	@Column(name="VENDOR_ADDRESS_LINE_3")
     private String vendorAddressLine3;
	
	@Column(name="VENDOR_ADDRESS_LINE_4")
     private String vendorAddressLine4;
	
	@Column(name="VENDOR_ADDRESS_CITY")
     private String vendorAddressCity;
	
	@Column(name="VENDOR_ADDRESS_COUNTY")
     private String vendorAddressCounty;
	
	@Column(name="VENDOR_ADDRESS_STATE")
     private String vendorAddressState;
	
	@Column(name="VENDOR_ADDRESS_ZIP")
     private String vendorAddressZip;
	
	@Column(name="VENDOR_ADDRESS_COUNTRY")
     private String vendorAddressCountry;
	
	@Column(name="VENDOR_TYPE")
     private String vendorType;
	
	@Column(name="VENDOR_TYPE_NAME")
     private String vendorTypeName;
	
	@Column(name="INVOICE_NBR")
     private String invoiceNbr;
	
	@Column(name="INVOICE_DESC")
     private String invoiceDesc;
	
	@Column(name="INVOICE_DATE")
     private Date invoiceDate;
	
	@Column(name="INVOICE_FREIGHT_AMT")
     private BigDecimal invoiceFreightAmt;
	
	@Column(name="INVOICE_DISCOUNT_AMT")
     private BigDecimal invoiceDiscountAmt;
	
	@Column(name="INVOICE_TAX_AMT")
     private BigDecimal invoiceTaxAmt;
	
	@Column(name="INVOICE_TOTAL_AMT")
     private BigDecimal invoiceTotalAmt;
	
	@Column(name="INVOICE_TAX_FLG")
     private String invoiceTaxFlg;
	
	@Column(name="INVOICE_LINE_NBR")
     private String invoiceLineNbr;
	
	@Column(name="INVOICE_LINE_NAME")
     private String invoiceLineName;
	
	@Column(name="INVOICE_LINE_TYPE")
     private String invoiceLineType;
	
	@Column(name="INVOICE_LINE_TYPE_NAME")
     private String invoiceLineTypeName;
	
	@Column(name="INVOICE_LINE_AMT")
     private BigDecimal invoiceLineAmt;
	
	@Column(name="INVOICE_LINE_TAX")
     private BigDecimal invoiceLineTax;
	
	@Column(name="AFE_PROJECT_NBR")
     private String afeProjectNbr;
	
	@Column(name="AFE_PROJECT_NAME")
     private String afeProjectName;
	
	@Column(name="AFE_CATEGORY_NBR")
     private String afeCategoryNbr;
	
	@Column(name="AFE_CATEGORY_NAME")
     private String afeCategoryName;
	
	@Column(name="AFE_SUB_CAT_NBR")
     private String afeSubCatNbr;
	
	@Column(name="AFE_SUB_CAT_NAME")
     private String afeSubCatName;
	
	@Column(name="AFE_USE")
     private String afeUse;
	
	@Column(name="AFE_CONTRACT_TYPE")
     private String afeContractType;
	
	@Column(name="AFE_CONTRACT_STRUCTURE")
     private String afeContractStructure;
	
	@Column(name="AFE_PROPERTY_CAT")
     private String afePropertyCat;
	
	@Column(name="INVENTORY_NBR")
     private String inventoryNbr;
	
	@Column(name="INVENTORY_NAME")
     private String inventoryName;
	
	@Column(name="INVENTORY_CLASS")
     private String inventoryClass;
	
	@Column(name="INVENTORY_CLASS_NAME")
     private String inventoryClassName;
	
	@Column(name="PO_NBR")
     private String poNbr;
	
	@Column(name="PO_NAME")
     private String poName;
	
	@Column(name="PO_DATE")
     private Date poDate;
	
	@Column(name="PO_LINE_NBR")
     private String poLineNbr;
	
	@Column(name="PO_LINE_NAME")
     private String poLineName;
	
	@Column(name="PO_LINE_TYPE")
     private String poLineType;
	
	@Column(name="PO_LINE_TYPE_NAME")
     private String poLineTypeName;
	
	@Column(name="SHIP_TO_LOCATION")
     private String shipToLocation;
	
	@Column(name="SHIP_TO_LOCATION_NAME")
     private String shipToLocationName;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_1")
     private String shipToAddressLine1;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_2")
     private String shipToAddressLine2;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_3")
     private String shipToAddressLine3;
	
	@Column(name="SHIP_TO_ADDRESS_LINE_4")
     private String shipToAddressLine4;
	
	@Column(name="SHIP_TO_ADDRESS_CITY")
     private String shipToAddressCity;
	
	@Column(name="SHIP_TO_ADDRESS_COUNTY")
     private String shipToAddressCounty;
	
	@Column(name="SHIP_TO_ADDRESS_STATE")
     private String shipToAddressState;
	
	@Column(name="SHIP_TO_ADDRESS_ZIP")
     private String shipToAddressZip;
	
	@Column(name="SHIP_TO_ADDRESS_COUNTRY")
     private String shipToAddressCountry;
	
	@Column(name="WO_NBR")
     private String woNbr;
	
	@Column(name="WO_NAME")
     private String woName;
	
	@Column(name="WO_DATE")
     private Date woDate;
	
	@Column(name="WO_TYPE")
     private String woType;
	
	@Column(name="WO_TYPE_DESC")
     private String woTypeDesc;
	
	@Column(name="WO_CLASS")
     private String woClass;
	
	@Column(name="WO_CLASS_DESC")
     private String woClassDesc;
	
	@Column(name="WO_ENTITY")
     private String woEntity;
	
	@Column(name="WO_ENTITY_DESC")
     private String woEntityDesc;
	
	@Column(name="WO_LINE_NBR")
     private String woLineNbr;
	
	@Column(name="WO_LINE_NAME")
     private String woLineName;
	
	@Column(name="WO_LINE_TYPE")
     private String woLineType;
	
	@Column(name="WO_LINE_TYPE_DESC")
     private String woLineTypeDesc;
	
	@Column(name="WO_SHUT_DOWN_CD")
     private String woShutDownCd;
	
	@Column(name="WO_SHUT_DOWN_CD_DESC")
     private String woShutDownCdDesc;
	
	@Column(name="VOUCHER_ID")
     private String voucherId;
	
	@Column(name="VOUCHER_NAME")
     private String voucherName;
	
	
	@Column(name="VOUCHER_DATE")
     private Date voucherDate;
	
	
	@Column(name="VOUCHER_LINE_NBR")
     private String voucherLineNbr;
	
	@Column(name="VOUCHER_LINE_DESC")
     private String voucherLineDesc;
	
	@Column(name="CHECK_NBR")
     private String checkNbr;
	
	@Column(name="CHECK_NO")
     private Long checkNo;
	
	@Column(name="CHECK_DATE")
     private Date checkDate;
	
	@Column(name="CHECK_AMT")
     private BigDecimal checkAmt;
	
	@Column(name="CHECK_DESC")
     private String checkDesc;
	
	@Column(name="USER_TEXT_01")
     private String userText01;
	
	@Column(name="USER_TEXT_02")
     private String userText02;
	
	@Column(name="USER_TEXT_03")
     private String userText03;
	
	@Column(name="USER_TEXT_04")
     private String userText04;
	
	@Column(name="USER_TEXT_05")
     private String userText05;
	
	@Column(name="USER_TEXT_06")
     private String userText06;
	
	@Column(name="USER_TEXT_07")
     private String userText07;
	
	@Column(name="USER_TEXT_08")
     private String userText08;
	
	@Column(name="USER_TEXT_09")
     private String userText09;
	
	@Column(name="USER_TEXT_10")
     private String userText10;
	
	@Column(name="USER_TEXT_11")
     private String userText11;
	
	@Column(name="USER_TEXT_12")
     private String userText12;
	
	@Column(name="USER_TEXT_13")
     private String userText13;
	
	@Column(name="USER_TEXT_14")
     private String userText14;
	
	@Column(name="USER_TEXT_15")
     private String userText15;
	
	@Column(name="USER_TEXT_16")
     private String userText16;
	
	@Column(name="USER_TEXT_17")
     private String userText17;
	
	@Column(name="USER_TEXT_18")
     private String userText18;
	
	@Column(name="USER_TEXT_19")
     private String userText19;
	
	@Column(name="USER_TEXT_20")
     private String userText20;
	
	@Column(name="USER_TEXT_21")
     private String userText21;
	
	@Column(name="USER_TEXT_22")
     private String userText22;
	
	@Column(name="USER_TEXT_23")
     private String userText23;
	
	@Column(name="USER_TEXT_24")
     private String userText24;
	
	@Column(name="USER_TEXT_25")
     private String userText25;
	
	@Column(name="USER_TEXT_26")
     private String userText26;
	
	@Column(name="USER_TEXT_27")
     private String userText27;
	
	@Column(name="USER_TEXT_28")
     private String userText28;
	
	@Column(name="USER_TEXT_29")
     private String userText29;
	
	@Column(name="USER_TEXT_30")
     private String userText30;
	
	@Column(name="USER_NUMBER_01")
     private BigDecimal userNumber01;
	
	@Column(name="USER_NUMBER_02")
     private BigDecimal userNumber02;
	
	@Column(name="USER_NUMBER_03")
     private BigDecimal userNumber03;
	
	@Column(name="USER_NUMBER_04")
     private BigDecimal userNumber04;
	
	@Column(name="USER_NUMBER_05")
     private BigDecimal userNumber05;
	
	@Column(name="USER_NUMBER_06")
     private BigDecimal userNumber06;
	
	@Column(name="USER_NUMBER_07")
     private BigDecimal userNumber07;
	
	@Column(name="USER_NUMBER_08")
     private BigDecimal userNumber08;
	
	@Column(name="USER_NUMBER_09")
     private BigDecimal userNumber09;
	
	@Column(name="USER_NUMBER_10")
     private BigDecimal userNumber10;
	
	@Column(name="USER_DATE_01")
     private Date userDate01;
	
	@Column(name="USER_DATE_02")
     private Date userDate02;
	
	@Column(name="USER_DATE_03")
     private Date userDate03;
	
	@Column(name="USER_DATE_04")
     private Date userDate04;
	
	@Column(name="USER_DATE_05")
     private Date userDate05;
	
	@Column(name="USER_DATE_06")
     private Date userDate06;
	
	@Column(name="USER_DATE_07")
     private Date userDate07;
	
	@Column(name="USER_DATE_08")
     private Date userDate08;
	
	@Column(name="USER_DATE_09")
     private Date userDate09;
	
	@Column(name="USER_DATE_10")
     private Date userDate10;
	
	@Column(name="COMMENTS")
     private String comments;
	
	@Column(name="TB_CALC_TAX_AMT")
     private BigDecimal tbCalcTaxAmt;
	
	@Column(name="STATE_USE_AMOUNT")
     private BigDecimal stateUseAmount;
	
	@Column(name="STATE_USE_TIER2_AMOUNT")
     private BigDecimal stateUseTier2Amount;
	
	@Column(name="STATE_USE_TIER3_AMOUNT")
     private BigDecimal stateUseTier3Amount;
	
	@Column(name="COUNTY_USE_AMOUNT")
     private BigDecimal countyUseAmount;
	
	@Column(name="COUNTY_LOCAL_USE_AMOUNT")
     private BigDecimal countyLocalUseAmount;
	
	@Column(name="CITY_USE_AMOUNT")
     private BigDecimal cityUseAmount;
	
	@Column(name="CITY_LOCAL_USE_AMOUNT")
     private BigDecimal cityLocalUseAmount;
	
	@Column(name="TRANSACTION_STATE_CODE")
     private String transactionStateCode;
	
	@Column(name="AUTO_TRANSACTION_STATE_CODE")
     private String autoTransactionStateCode;
	
	@Column(name="TRANSACTION_IND")
     private String transactionInd;
	
	@Column(name="SUSPEND_IND")
     private String suspendInd;
	
	@Column(name="TAXCODE_CODE")
     private String taxcodeCode;
	
	//CCH	@Column(name="CCH_TAXCAT_CODE")
	//CCH     private String cchTaxcatCode;
	
	//CCH	@Column(name="CCH_GROUP_CODE")
	//CCH     private String cchGroupCode;
	
	//CCH	@Column(name="CCH_ITEM_CODE")
	//CCH    private String cchItemCode;
	
	@Column(name="MANUAL_TAXCODE_IND")
     private String manualTaxcodeInd;
	
	@Column(name="TAX_MATRIX_ID")
     private Long taxMatrixId;
	
	@Column(name="LOCATION_MATRIX_ID")
     private Long locationMatrixId;
	
	@Column(name="JURISDICTION_ID")
     private Long jurisdictionId;
	
	@Column(name="JURISDICTION_TAXRATE_ID")
     private Long jurisdictionTaxrateId;
	
	@Column(name="MANUAL_JURISDICTION_IND")
     private String manualJurisdictionInd;
	
	@Column(name="STATE_USE_RATE")
     private BigDecimal stateUseRate;
	
	@Column(name="STATE_USE_TIER2_RATE")
     private BigDecimal stateUseTier2Rate;
	
	@Column(name="STATE_USE_TIER3_RATE")
     private BigDecimal stateUseTier3Rate;
	
	@Column(name="STATE_SPLIT_AMOUNT")
     private BigDecimal stateSplitAmount;
	
	@Column(name="STATE_TIER2_MIN_AMOUNT")
     private BigDecimal stateTier2MinAmount;
	
	@Column(name="STATE_TIER2_MAX_AMOUNT")
     private BigDecimal stateTier2MaxAmount;
	
	@Column(name="STATE_MAXTAX_AMOUNT")
     private BigDecimal stateMaxtaxAmount;
	
	@Column(name="COUNTY_USE_RATE")
     private BigDecimal countyUseRate;
	
	@Column(name="COUNTY_SPLIT_AMOUNT")
     private BigDecimal countySplitAmount;
	
	@Column(name="COUNTY_MAXTAX_AMOUNT")
     private BigDecimal countyMaxtaxAmount;
	
	@Column(name="COUNTY_SINGLE_FLAG")
     private String countySingleFlag;
	
	@Column(name="COUNTY_DEFAULT_FLAG")
     private String countyDefaultFlag;
	
	@Column(name="CITY_USE_RATE")
     private BigDecimal cityUseRate;
	
	@Column(name="CITY_LOCAL_USE_RATE")
     private BigDecimal cityLocalUseRate;
	
	@Column(name="CITY_SPLIT_AMOUNT")
     private BigDecimal citySplitAmount;
	
	@Column(name="CITY_SPLIT_USE_RATE")
     private BigDecimal citySplitUseRate;
	
	@Column(name="CITY_SINGLE_FLAG")
     private String citySingleFlag;
	
	@Column(name="CITY_DEFAULT_FLAG")
     private String cityDefaultFlag;
	
	@Column(name="COMBINED_USE_RATE")
     private BigDecimal combinedUseRate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LOAD_TIMESTAMP")
     private Date loadTimestamp;
	
	@Column(name="GL_EXTRACT_UPDATER")
     private String glExtractUpdater;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GL_EXTRACT_TIMESTAMP")
     private Date glExtractTimestamp;
	
	@Column(name="GL_EXTRACT_FLAG")
     private Long glExtractFlag;
	
	@Column(name="GL_LOG_FLAG")
     private Long glLogFlag;
	
	@Column(name="GL_EXTRACT_AMT")
     private BigDecimal glExtractAmt;
	
	@Column(name="AUDIT_FLAG")
     private String auditFlag;
	
	@Column(name="AUDIT_USER_ID")
     private String auditUserId;
	
	@Column(name="COUNTRY_USE_AMOUNT")
	private BigDecimal countryUseAmount;
	
	@Column(name="TRANSACTION_COUNTRY_CODE")
    private String transactionCountryCode;
	
	@Column(name="AUTO_TRANSACTION_COUNTRY_CODE")
    private String autoTransactionCountryCode;
	
	@Column(name="COUNTRY_USE_RATE")
	private BigDecimal countryUseRate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AUDIT_TIMESTAMP")
     private Date auditTimestamp;
	
	@Column(name="MODIFY_USER_ID")
     private String modifyUserId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_TIMESTAMP")
     private Date modifyTimestamp;
	
	@Column(name="SPLIT_SUBTRANS_ID")
    private Long splitSubtransId;
	
	@Column(name="MULTI_TRANS_CODE")
    private String multiTransCode;
	
	@Column(name="TAX_ALLOC_MATRIX_ID")
    private Long taxAllocMatrixId;
	
	@Column(name="STATE_TAXCODE_DETAIL_ID")
	private Long stateTaxcodeDetailId;
	
	@Column(name="COUNTY_TAXCODE_DETAIL_ID")
	private Long countyTaxcodeDetailId;
	
	@Column(name="CITY_TAXCODE_DETAIL_ID")
	private Long  cityTaxcodeDetailId;
	
	@Column(name="TAXTYPE_USED_CODE")
	private String taxtypeUsedCode;
	
	@Column(name="STATE_TAXABLE_AMT")
	private BigDecimal stateTaxableAmt;
	
	@Column(name="COUNTY_TAXABLE_AMT")
	private BigDecimal  countyTaxableAmt;
	
	@Column(name="CITY_TAXABLE_AMT")
	private BigDecimal cityTaxableAmt;
	
	@Column(name="ENTITY_CODE")
    private String entityCode;
	
	@Column(name="STJ1_RATE")
	private BigDecimal stj1Rate;

	@Column(name="STJ2_RATE")
	private BigDecimal stj2Rate;

	@Column(name="STJ3_RATE")
	private BigDecimal stj3Rate;

	@Column(name="STJ4_RATE")
	private BigDecimal stj4Rate;

	@Column(name="STJ5_RATE")
	private BigDecimal stj5Rate;
	
	@Column(name="STJ1_AMOUNT")
	private BigDecimal stj1Amount;

	@Column(name="STJ2_AMOUNT")
	private BigDecimal stj2Amount;

	@Column(name="STJ3_AMOUNT")
	private BigDecimal stj3Amount;

	@Column(name="STJ4_AMOUNT")
	private BigDecimal stj4Amount;

	@Column(name="STJ5_AMOUNT")
	private BigDecimal stj5Amount;
	
	@Column(name="CITY_TAXCODE_TYPE_CODE")
	private String cityTaxcodeTypeCode;

	@Column(name="COUNTY_LOCAL_USE_RATE")
	private BigDecimal countyLocalUseRate;

	@Column(name="COUNTY_TAXCODE_TYPE_CODE")
	private String countyTaxcodeTypeCode;

	@Column(name="MEASURE_TYPE_CODE")
	private String measureTypeCode;

	@Column(name="PRODUCT_CODE")
	private String productCode;

	@Column(name="REJECT_FLAG")
	private String rejectFlag;

	@Column(name="STATE_TAXCODE_TYPE_CODE")
	private String stateTaxcodeTypeCode;

	@Column(name="STATE_USE_TIER1_SETAMT")
	private Double stateUseTier1Setamt;

	@Column(name="STATE_USE_TIER2_SETAMT")
	private Double stateUseTier2Setamt;

	@Column(name="STATE_USE_TIER3_SETAMT")
	private Double stateUseTier3Setamt;

	@Column(name="TAXCODE_COUNTRY_CODE")
	private String taxcodeCountryCode;

	@Column(name="TAXCODE_DETAIL_ID")
	private Long taxcodeDetailId;

	@Column(name="TAXCODE_STATE_CODE")
	private String taxcodeStateCode;

	@Column(name="TAXCODE_TYPE_CODE")
	private String taxcodeTypeCode;

	@Column(name="USER_DATE_11")
	private Date userDate11;

	@Column(name="USER_DATE_12")
	private Date userDate12;

	@Column(name="USER_DATE_13")
	private Date userDate13;

	@Column(name="USER_DATE_14")
	private Date userDate14;

	@Column(name="USER_DATE_15")
	private Date userDate15;

	@Column(name="USER_DATE_16")
	private Date userDate16;

	@Column(name="USER_DATE_17")
	private Date userDate17;

	@Column(name="USER_DATE_18")
	private Date userDate18;

	@Column(name="USER_DATE_19")
	private Date userDate19;

	@Column(name="USER_DATE_20")
	private Date userDate20;

	@Column(name="USER_DATE_21")
	private Date userDate21;

	@Column(name="USER_DATE_22")
	private Date userDate22;

	@Column(name="USER_DATE_23")
	private Date userDate23;

	@Column(name="USER_DATE_24")
	private Date userDate24;

	@Column(name="USER_DATE_25")
	private Date userDate25;

	@Column(name="USER_NUMBER_11")
	private BigDecimal userNumber11;

	@Column(name="USER_NUMBER_12")
	private BigDecimal userNumber12;

	@Column(name="USER_NUMBER_13")
	private BigDecimal userNumber13;

	@Column(name="USER_NUMBER_14")
	private BigDecimal userNumber14;

	@Column(name="USER_NUMBER_15")
	private BigDecimal userNumber15;

	@Column(name="USER_NUMBER_16")
	private BigDecimal userNumber16;

	@Column(name="USER_NUMBER_17")
	private BigDecimal userNumber17;

	@Column(name="USER_NUMBER_18")
	private BigDecimal userNumber18;

	@Column(name="USER_NUMBER_19")
	private BigDecimal userNumber19;

	@Column(name="USER_NUMBER_20")
	private BigDecimal userNumber20;

	@Column(name="USER_NUMBER_21")
	private BigDecimal userNumber21;

	@Column(name="USER_NUMBER_22")
	private BigDecimal userNumber22;

	@Column(name="USER_NUMBER_23")
	private BigDecimal userNumber23;

	@Column(name="USER_NUMBER_24")
	private BigDecimal userNumber24;

	@Column(name="USER_NUMBER_25")
	private BigDecimal userNumber25;

	@Column(name="USER_TEXT_100")
	private String userText100;

	@Column(name="USER_TEXT_31")
	private String userText31;

	@Column(name="USER_TEXT_32")
	private String userText32;

	@Column(name="USER_TEXT_33")
	private String userText33;

	@Column(name="USER_TEXT_34")
	private String userText34;

	@Column(name="USER_TEXT_35")
	private String userText35;

	@Column(name="USER_TEXT_36")
	private String userText36;

	@Column(name="USER_TEXT_37")
	private String userText37;

	@Column(name="USER_TEXT_38")
	private String userText38;

	@Column(name="USER_TEXT_39")
	private String userText39;

	@Column(name="USER_TEXT_40")
	private String userText40;

	@Column(name="USER_TEXT_41")
	private String userText41;

	@Column(name="USER_TEXT_42")
	private String userText42;

	@Column(name="USER_TEXT_43")
	private String userText43;

	@Column(name="USER_TEXT_44")
	private String userText44;

	@Column(name="USER_TEXT_45")
	private String userText45;

	@Column(name="USER_TEXT_46")
	private String userText46;

	@Column(name="USER_TEXT_47")
	private String userText47;

	@Column(name="USER_TEXT_48")
	private String userText48;

	@Column(name="USER_TEXT_49")
	private String userText49;

	@Column(name="USER_TEXT_50")
	private String userText50;

	@Column(name="USER_TEXT_51")
	private String userText51;

	@Column(name="USER_TEXT_52")
	private String userText52;

	@Column(name="USER_TEXT_53")
	private String userText53;

	@Column(name="USER_TEXT_54")
	private String userText54;

	@Column(name="USER_TEXT_55")
	private String userText55;

	@Column(name="USER_TEXT_56")
	private String userText56;

	@Column(name="USER_TEXT_57")
	private String userText57;

	@Column(name="USER_TEXT_58")
	private String userText58;

	@Column(name="USER_TEXT_59")
	private String userText59;

	@Column(name="USER_TEXT_60")
	private String userText60;

	@Column(name="USER_TEXT_61")
	private String userText61;

	@Column(name="USER_TEXT_62")
	private String userText62;

	@Column(name="USER_TEXT_63")
	private String userText63;

	@Column(name="USER_TEXT_64")
	private String userText64;

	@Column(name="USER_TEXT_65")
	private String userText65;

	@Column(name="USER_TEXT_66")
	private String userText66;

	@Column(name="USER_TEXT_67")
	private String userText67;

	@Column(name="USER_TEXT_68")
	private String userText68;

	@Column(name="USER_TEXT_69")
	private String userText69;

	@Column(name="USER_TEXT_70")
	private String userText70;

	@Column(name="USER_TEXT_71")
	private String userText71;

	@Column(name="USER_TEXT_72")
	private String userText72;

	@Column(name="USER_TEXT_73")
	private String userText73;

	@Column(name="USER_TEXT_74")
	private String userText74;

	@Column(name="USER_TEXT_75")
	private String userText75;

	@Column(name="USER_TEXT_76")
	private String userText76;

	@Column(name="USER_TEXT_77")
	private String userText77;

	@Column(name="USER_TEXT_78")
	private String userText78;

	@Column(name="USER_TEXT_79")
	private String userText79;

	@Column(name="USER_TEXT_80")
	private String userText80;

	@Column(name="USER_TEXT_81")
	private String userText81;

	@Column(name="USER_TEXT_82")
	private String userText82;

	@Column(name="USER_TEXT_83")
	private String userText83;

	@Column(name="USER_TEXT_84")
	private String userText84;

	@Column(name="USER_TEXT_85")
	private String userText85;

	@Column(name="USER_TEXT_86")
	private String userText86;

	@Column(name="USER_TEXT_87")
	private String userText87;

	@Column(name="USER_TEXT_88")
	private String userText88;

	@Column(name="USER_TEXT_89")
	private String userText89;

	@Column(name="USER_TEXT_90")
	private String userText90;

	@Column(name="USER_TEXT_91")
	private String userText91;

	@Column(name="USER_TEXT_92")
	private String userText92;

	@Column(name="USER_TEXT_93")
	private String userText93;

	@Column(name="USER_TEXT_94")
	private String userText94;

	@Column(name="USER_TEXT_95")
	private String userText95;

	@Column(name="USER_TEXT_96")
	private String userText96;

	@Column(name="USER_TEXT_97")
	private String userText97;

	@Column(name="USER_TEXT_98")
	private String userText98;

	@Column(name="USER_TEXT_99")
	private String userText99;
	
	@Column(name="CP_FILING_ENTITY_CODE")
	private String cpFilingEntityCode;
	
	@Transient
	private Long instanceCreatedId; //bug 5090
	
	private static long nextId = 0L;
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
	/** default constructor */
	public BCPTransactionDetail(){
		instanceCreatedId = new Long(getNextId());
	}
	
	public BCPTransactionDetail(int id){
		setTransactionDetailId(new Long(id));
	}
	
	public Long getId() {
    	return bcpTransactionId == null ? instanceCreatedId : bcpTransactionId;
    }
    
    public void setId(Long id) {
    	this.bcpTransactionId = id;
    }

    public String getIdPropertyName() {
    	return "bcpTransactionId";
    }
    
	public Long getBcpTransactionId() {
		return bcpTransactionId;
	}

	public void setBcpTransactionId(Long bcpTransactionId) {
		this.bcpTransactionId = bcpTransactionId;
	}
	
	public Long getTransactionDetailId() {
		return this.transactionDetailId;
	}

	public void setTransactionDetailId(Long transactionDetailId) {
		this.transactionDetailId = transactionDetailId;
	}

	public Long getProcessBatchNo() {
		return this.processBatchNo;
	}

	public void setProcessBatchNo(Long processBatchNo) {
		this.processBatchNo = processBatchNo;
	}

	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}

	public Long getGlExtractBatchNo() {
		return glExtractBatchNo;
	}

	public void setGlExtractBatchNo(Long glExtractBatchNo) {
		this.glExtractBatchNo = glExtractBatchNo;
	}

	public Long getArchiveBatchNo() {
		return archiveBatchNo;
	}

	public void setArchiveBatchNo(Long archiveBatchNo) {
		this.archiveBatchNo = archiveBatchNo;
	}

	public Long getAllocationMatrixId() {
		return allocationMatrixId;
	}

	public void setAllocationMatrixId(Long allocationMatrixId) {
		this.allocationMatrixId = allocationMatrixId;
	}

	public Long getAllocationSubtransId() {
		return allocationSubtransId;
	}

	public void setAllocationSubtransId(Long allocationSubtransId) {
		this.allocationSubtransId = allocationSubtransId;
	}

	public Date getEnteredDate() {
		return enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Date getGlDate() {
		return glDate;
	}

	public void setGlDate(Date glDate) {
		this.glDate = glDate;
	}

	public String getGlCompanyNbr() {
		return glCompanyNbr;
	}

	public void setGlCompanyNbr(String glCompanyNbr) {
		this.glCompanyNbr = glCompanyNbr;
	}

	public String getGlCompanyName() {
		return glCompanyName;
	}

	public void setGlCompanyName(String glCompanyName) {
		this.glCompanyName = glCompanyName;
	}

	public String getGlDivisionNbr() {
		return glDivisionNbr;
	}

	public void setGlDivisionNbr(String glDivisionNbr) {
		this.glDivisionNbr = glDivisionNbr;
	}

	public String getGlDivisionName() {
		return glDivisionName;
	}

	public void setGlDivisionName(String glDivisionName) {
		this.glDivisionName = glDivisionName;
	}

	public String getGlCcNbrDeptId() {
		return glCcNbrDeptId;
	}

	public void setGlCcNbrDeptId(String glCcNbrDeptId) {
		this.glCcNbrDeptId = glCcNbrDeptId;
	}

	public String getGlCcNbrDeptName() {
		return glCcNbrDeptName;
	}

	public void setGlCcNbrDeptName(String glCcNbrDeptName) {
		this.glCcNbrDeptName = glCcNbrDeptName;
	}

	public String getGlLocalAcctNbr() {
		return glLocalAcctNbr;
	}

	public void setGlLocalAcctNbr(String glLocalAcctNbr) {
		this.glLocalAcctNbr = glLocalAcctNbr;
	}

	public String getGlLocalAcctName() {
		return glLocalAcctName;
	}

	public void setGlLocalAcctName(String glLocalAcctName) {
		this.glLocalAcctName = glLocalAcctName;
	}

	public String getGlLocalSubAcctNbr() {
		return glLocalSubAcctNbr;
	}

	public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr) {
		this.glLocalSubAcctNbr = glLocalSubAcctNbr;
	}

	public String getGlLocalSubAcctName() {
		return glLocalSubAcctName;
	}

	public void setGlLocalSubAcctName(String glLocalSubAcctName) {
		this.glLocalSubAcctName = glLocalSubAcctName;
	}

	public String getGlFullAcctNbr() {
		return glFullAcctNbr;
	}

	public void setGlFullAcctNbr(String glFullAcctNbr) {
		this.glFullAcctNbr = glFullAcctNbr;
	}

	public String getGlFullAcctName() {
		return glFullAcctName;
	}

	public void setGlFullAcctName(String glFullAcctName) {
		this.glFullAcctName = glFullAcctName;
	}

	public String getVendorNbr() {
		return vendorNbr;
	}

	public void setVendorNbr(String vendorNbr) {
		this.vendorNbr = vendorNbr;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	public String getVendorAddressLine4() {
		return vendorAddressLine4;
	}

	public void setVendorAddressLine4(String vendorAddressLine4) {
		this.vendorAddressLine4 = vendorAddressLine4;
	}

	public String getVendorAddressCity() {
		return vendorAddressCity;
	}

	public void setVendorAddressCity(String vendorAddressCity) {
		this.vendorAddressCity = vendorAddressCity;
	}

	public String getVendorAddressCounty() {
		return vendorAddressCounty;
	}

	public void setVendorAddressCounty(String vendorAddressCounty) {
		this.vendorAddressCounty = vendorAddressCounty;
	}

	public String getVendorAddressState() {
		return vendorAddressState;
	}

	public void setVendorAddressState(String vendorAddressState) {
		this.vendorAddressState = vendorAddressState;
	}

	public String getVendorAddressZip() {
		return vendorAddressZip;
	}

	public void setVendorAddressZip(String vendorAddressZip) {
		this.vendorAddressZip = vendorAddressZip;
	}

	public String getVendorAddressCountry() {
		return vendorAddressCountry;
	}

	public void setVendorAddressCountry(String vendorAddressCountry) {
		this.vendorAddressCountry = vendorAddressCountry;
	}

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	public String getVendorTypeName() {
		return vendorTypeName;
	}

	public void setVendorTypeName(String vendorTypeName) {
		this.vendorTypeName = vendorTypeName;
	}

	public String getInvoiceNbr() {
		return invoiceNbr;
	}

	public void setInvoiceNbr(String invoiceNbr) {
		this.invoiceNbr = invoiceNbr;
	}

	public String getInvoiceDesc() {
		return invoiceDesc;
	}

	public void setInvoiceDesc(String invoiceDesc) {
		this.invoiceDesc = invoiceDesc;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceTaxFlg() {
		return invoiceTaxFlg;
	}

	public void setInvoiceTaxFlg(String invoiceTaxFlg) {
		this.invoiceTaxFlg = invoiceTaxFlg;
	}

	public String getInvoiceLineNbr() {
		return invoiceLineNbr;
	}

	public void setInvoiceLineNbr(String invoiceLineNbr) {
		this.invoiceLineNbr = invoiceLineNbr;
	}

	public String getInvoiceLineName() {
		return invoiceLineName;
	}

	public void setInvoiceLineName(String invoiceLineName) {
		this.invoiceLineName = invoiceLineName;
	}

	public String getInvoiceLineType() {
		return invoiceLineType;
	}

	public void setInvoiceLineType(String invoiceLineType) {
		this.invoiceLineType = invoiceLineType;
	}

	public String getInvoiceLineTypeName() {
		return invoiceLineTypeName;
	}

	public void setInvoiceLineTypeName(String invoiceLineTypeName) {
		this.invoiceLineTypeName = invoiceLineTypeName;
	}

	public String getAfeProjectNbr() {
		return afeProjectNbr;
	}

	public void setAfeProjectNbr(String afeProjectNbr) {
		this.afeProjectNbr = afeProjectNbr;
	}

	public String getAfeProjectName() {
		return afeProjectName;
	}

	public void setAfeProjectName(String afeProjectName) {
		this.afeProjectName = afeProjectName;
	}

	public String getAfeCategoryNbr() {
		return afeCategoryNbr;
	}

	public void setAfeCategoryNbr(String afeCategoryNbr) {
		this.afeCategoryNbr = afeCategoryNbr;
	}

	public String getAfeCategoryName() {
		return afeCategoryName;
	}

	public void setAfeCategoryName(String afeCategoryName) {
		this.afeCategoryName = afeCategoryName;
	}

	public String getAfeSubCatNbr() {
		return afeSubCatNbr;
	}

	public void setAfeSubCatNbr(String afeSubCatNbr) {
		this.afeSubCatNbr = afeSubCatNbr;
	}

	public String getAfeSubCatName() {
		return afeSubCatName;
	}

	public void setAfeSubCatName(String afeSubCatName) {
		this.afeSubCatName = afeSubCatName;
	}

	public String getAfeUse() {
		return afeUse;
	}

	public void setAfeUse(String afeUse) {
		this.afeUse = afeUse;
	}

	public String getAfeContractType() {
		return afeContractType;
	}

	public void setAfeContractType(String afeContractType) {
		this.afeContractType = afeContractType;
	}

	public String getAfeContractStructure() {
		return afeContractStructure;
	}

	public void setAfeContractStructure(String afeContractStructure) {
		this.afeContractStructure = afeContractStructure;
	}

	public String getAfePropertyCat() {
		return afePropertyCat;
	}

	public void setAfePropertyCat(String afePropertyCat) {
		this.afePropertyCat = afePropertyCat;
	}

	public String getInventoryNbr() {
		return inventoryNbr;
	}

	public void setInventoryNbr(String inventoryNbr) {
		this.inventoryNbr = inventoryNbr;
	}

	public String getInventoryName() {
		return inventoryName;
	}

	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}

	public String getInventoryClass() {
		return inventoryClass;
	}

	public void setInventoryClass(String inventoryClass) {
		this.inventoryClass = inventoryClass;
	}

	public String getInventoryClassName() {
		return inventoryClassName;
	}

	public void setInventoryClassName(String inventoryClassName) {
		this.inventoryClassName = inventoryClassName;
	}

	public String getPoNbr() {
		return poNbr;
	}

	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}

	public String getPoName() {
		return poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getPoLineNbr() {
		return poLineNbr;
	}

	public void setPoLineNbr(String poLineNbr) {
		this.poLineNbr = poLineNbr;
	}

	public String getPoLineName() {
		return poLineName;
	}

	public void setPoLineName(String poLineName) {
		this.poLineName = poLineName;
	}

	public String getPoLineType() {
		return poLineType;
	}

	public void setPoLineType(String poLineType) {
		this.poLineType = poLineType;
	}

	public String getPoLineTypeName() {
		return poLineTypeName;
	}

	public void setPoLineTypeName(String poLineTypeName) {
		this.poLineTypeName = poLineTypeName;
	}

	public String getShipToLocation() {
		return shipToLocation;
	}

	public void setShipToLocation(String shipToLocation) {
		this.shipToLocation = shipToLocation;
	}

	public String getShipToLocationName() {
		return shipToLocationName;
	}

	public void setShipToLocationName(String shipToLocationName) {
		this.shipToLocationName = shipToLocationName;
	}

	public String getShipToAddressLine1() {
		return shipToAddressLine1;
	}

	public void setShipToAddressLine1(String shipToAddressLine1) {
		this.shipToAddressLine1 = shipToAddressLine1;
	}

	public String getShipToAddressLine2() {
		return shipToAddressLine2;
	}

	public void setShipToAddressLine2(String shipToAddressLine2) {
		this.shipToAddressLine2 = shipToAddressLine2;
	}

	public String getShipToAddressLine3() {
		return shipToAddressLine3;
	}

	public void setShipToAddressLine3(String shipToAddressLine3) {
		this.shipToAddressLine3 = shipToAddressLine3;
	}

	public String getShipToAddressLine4() {
		return shipToAddressLine4;
	}

	public void setShipToAddressLine4(String shipToAddressLine4) {
		this.shipToAddressLine4 = shipToAddressLine4;
	}

	public String getShipToAddressCity() {
		return shipToAddressCity;
	}

	public void setShipToAddressCity(String shipToAddressCity) {
		this.shipToAddressCity = shipToAddressCity;
	}

	public String getShipToAddressCounty() {
		return shipToAddressCounty;
	}

	public void setShipToAddressCounty(String shipToAddressCounty) {
		this.shipToAddressCounty = shipToAddressCounty;
	}

	public String getShipToAddressState() {
		return shipToAddressState;
	}

	public void setShipToAddressState(String shipToAddressState) {
		this.shipToAddressState = shipToAddressState;
	}

	public String getShipToAddressZip() {
		return shipToAddressZip;
	}

	public void setShipToAddressZip(String shipToAddressZip) {
		this.shipToAddressZip = shipToAddressZip;
	}

	public String getShipToAddressCountry() {
		return shipToAddressCountry;
	}

	public void setShipToAddressCountry(String shipToAddressCountry) {
		this.shipToAddressCountry = shipToAddressCountry;
	}

	public String getWoNbr() {
		return woNbr;
	}

	public void setWoNbr(String woNbr) {
		this.woNbr = woNbr;
	}

	public String getWoName() {
		return woName;
	}

	public void setWoName(String woName) {
		this.woName = woName;
	}

	public Date getWoDate() {
		return woDate;
	}

	public void setWoDate(Date woDate) {
		this.woDate = woDate;
	}

	public String getWoType() {
		return woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public String getWoTypeDesc() {
		return woTypeDesc;
	}

	public void setWoTypeDesc(String woTypeDesc) {
		this.woTypeDesc = woTypeDesc;
	}

	public String getWoClass() {
		return woClass;
	}

	public void setWoClass(String woClass) {
		this.woClass = woClass;
	}

	public String getWoClassDesc() {
		return woClassDesc;
	}

	public void setWoClassDesc(String woClassDesc) {
		this.woClassDesc = woClassDesc;
	}

	public String getWoEntity() {
		return woEntity;
	}

	public void setWoEntity(String woEntity) {
		this.woEntity = woEntity;
	}

	public String getWoEntityDesc() {
		return woEntityDesc;
	}

	public void setWoEntityDesc(String woEntityDesc) {
		this.woEntityDesc = woEntityDesc;
	}

	public String getWoLineNbr() {
		return woLineNbr;
	}

	public void setWoLineNbr(String woLineNbr) {
		this.woLineNbr = woLineNbr;
	}

	public String getWoLineName() {
		return woLineName;
	}

	public void setWoLineName(String woLineName) {
		this.woLineName = woLineName;
	}

	public String getWoLineType() {
		return woLineType;
	}

	public void setWoLineType(String woLineType) {
		this.woLineType = woLineType;
	}

	public String getWoLineTypeDesc() {
		return woLineTypeDesc;
	}

	public void setWoLineTypeDesc(String woLineTypeDesc) {
		this.woLineTypeDesc = woLineTypeDesc;
	}

	public String getWoShutDownCd() {
		return woShutDownCd;
	}

	public void setWoShutDownCd(String woShutDownCd) {
		this.woShutDownCd = woShutDownCd;
	}

	public String getWoShutDownCdDesc() {
		return woShutDownCdDesc;
	}

	public void setWoShutDownCdDesc(String woShutDownCdDesc) {
		this.woShutDownCdDesc = woShutDownCdDesc;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherLineNbr() {
		return voucherLineNbr;
	}

	public void setVoucherLineNbr(String voucherLineNbr) {
		this.voucherLineNbr = voucherLineNbr;
	}

	public String getVoucherLineDesc() {
		return voucherLineDesc;
	}

	public void setVoucherLineDesc(String voucherLineDesc) {
		this.voucherLineDesc = voucherLineDesc;
	}

	public String getCheckNbr() {
		return checkNbr;
	}

	public void setCheckNbr(String checkNbr) {
		this.checkNbr = checkNbr;
	}

	public Long getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(Long checkNo) {
		this.checkNo = checkNo;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public String getCheckDesc() {
		return checkDesc;
	}

	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}

	public String getUserText01() {
		return userText01;
	}

	public void setUserText01(String userText01) {
		this.userText01 = userText01;
	}

	public String getUserText02() {
		return userText02;
	}

	public void setUserText02(String userText02) {
		this.userText02 = userText02;
	}

	public String getUserText03() {
		return userText03;
	}

	public void setUserText03(String userText03) {
		this.userText03 = userText03;
	}

	public String getUserText04() {
		return userText04;
	}

	public void setUserText04(String userText04) {
		this.userText04 = userText04;
	}

	public String getUserText05() {
		return userText05;
	}

	public void setUserText05(String userText05) {
		this.userText05 = userText05;
	}

	public String getUserText06() {
		return userText06;
	}

	public void setUserText06(String userText06) {
		this.userText06 = userText06;
	}

	public String getUserText07() {
		return userText07;
	}

	public void setUserText07(String userText07) {
		this.userText07 = userText07;
	}

	public String getUserText08() {
		return userText08;
	}

	public void setUserText08(String userText08) {
		this.userText08 = userText08;
	}

	public String getUserText09() {
		return userText09;
	}

	public void setUserText09(String userText09) {
		this.userText09 = userText09;
	}

	public String getUserText10() {
		return userText10;
	}

	public void setUserText10(String userText10) {
		this.userText10 = userText10;
	}

	public String getUserText11() {
		return userText11;
	}

	public void setUserText11(String userText11) {
		this.userText11 = userText11;
	}

	public String getUserText12() {
		return userText12;
	}

	public void setUserText12(String userText12) {
		this.userText12 = userText12;
	}

	public String getUserText13() {
		return userText13;
	}

	public void setUserText13(String userText13) {
		this.userText13 = userText13;
	}

	public String getUserText14() {
		return userText14;
	}

	public void setUserText14(String userText14) {
		this.userText14 = userText14;
	}

	public String getUserText15() {
		return userText15;
	}

	public void setUserText15(String userText15) {
		this.userText15 = userText15;
	}

	public String getUserText16() {
		return userText16;
	}

	public void setUserText16(String userText16) {
		this.userText16 = userText16;
	}

	public String getUserText17() {
		return userText17;
	}

	public void setUserText17(String userText17) {
		this.userText17 = userText17;
	}

	public String getUserText18() {
		return userText18;
	}

	public void setUserText18(String userText18) {
		this.userText18 = userText18;
	}

	public String getUserText19() {
		return userText19;
	}

	public void setUserText19(String userText19) {
		this.userText19 = userText19;
	}

	public String getUserText20() {
		return userText20;
	}

	public void setUserText20(String userText20) {
		this.userText20 = userText20;
	}

	public String getUserText21() {
		return userText21;
	}

	public void setUserText21(String userText21) {
		this.userText21 = userText21;
	}

	public String getUserText22() {
		return userText22;
	}

	public void setUserText22(String userText22) {
		this.userText22 = userText22;
	}

	public String getUserText23() {
		return userText23;
	}

	public void setUserText23(String userText23) {
		this.userText23 = userText23;
	}

	public String getUserText24() {
		return userText24;
	}

	public void setUserText24(String userText24) {
		this.userText24 = userText24;
	}

	public String getUserText25() {
		return userText25;
	}

	public void setUserText25(String userText25) {
		this.userText25 = userText25;
	}

	public String getUserText26() {
		return userText26;
	}

	public void setUserText26(String userText26) {
		this.userText26 = userText26;
	}

	public String getUserText27() {
		return userText27;
	}

	public void setUserText27(String userText27) {
		this.userText27 = userText27;
	}

	public String getUserText28() {
		return userText28;
	}

	public void setUserText28(String userText28) {
		this.userText28 = userText28;
	}

	public String getUserText29() {
		return userText29;
	}

	public void setUserText29(String userText29) {
		this.userText29 = userText29;
	}

	public String getUserText30() {
		return userText30;
	}

	public void setUserText30(String userText30) {
		this.userText30 = userText30;
	}

	public Date getUserDate01() {
		return userDate01;
	}

	public void setUserDate01(Date userDate01) {
		this.userDate01 = userDate01;
	}

	public Date getUserDate02() {
		return userDate02;
	}

	public void setUserDate02(Date userDate02) {
		this.userDate02 = userDate02;
	}

	public Date getUserDate03() {
		return userDate03;
	}

	public void setUserDate03(Date userDate03) {
		this.userDate03 = userDate03;
	}

	public Date getUserDate04() {
		return userDate04;
	}

	public void setUserDate04(Date userDate04) {
		this.userDate04 = userDate04;
	}

	public Date getUserDate05() {
		return userDate05;
	}

	public void setUserDate05(Date userDate05) {
		this.userDate05 = userDate05;
	}

	public Date getUserDate06() {
		return userDate06;
	}

	public void setUserDate06(Date userDate06) {
		this.userDate06 = userDate06;
	}

	public Date getUserDate07() {
		return userDate07;
	}

	public void setUserDate07(Date userDate07) {
		this.userDate07 = userDate07;
	}

	public Date getUserDate08() {
		return userDate08;
	}

	public void setUserDate08(Date userDate08) {
		this.userDate08 = userDate08;
	}

	public Date getUserDate09() {
		return userDate09;
	}

	public void setUserDate09(Date userDate09) {
		this.userDate09 = userDate09;
	}

	public Date getUserDate10() {
		return userDate10;
	}

	public void setUserDate10(Date userDate10) {
		this.userDate10 = userDate10;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}


	public String getTransactionStateCode() {
		return transactionStateCode;
	}

	public void setTransactionStateCode(String transactionStateCode) {
		this.transactionStateCode = transactionStateCode;
	}

	public String getAutoTransactionStateCode() {
		return autoTransactionStateCode;
	}

	public void setAutoTransactionStateCode(String autoTransactionStateCode) {
		this.autoTransactionStateCode = autoTransactionStateCode;
	}

	public String getTransactionInd() {
		return transactionInd;
	}

	public void setTransactionInd(String transactionInd) {
		this.transactionInd = transactionInd;
	}

	public String getSuspendInd() {
		return suspendInd;
	}

	public void setSuspendInd(String suspendInd) {
		this.suspendInd = suspendInd;
	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	//CCH	public String getCchTaxcatCode() {
	//CCH		return cchTaxcatCode;
	//CCH	}

	//CCH	public void setCchTaxcatCode(String cchTaxcatCode) {
	//CCH		this.cchTaxcatCode = cchTaxcatCode;
	//CCH	}

	//CCH	public String getCchGroupCode() {
	//CCH		return cchGroupCode;
	//CCH	}

	//CCH	public void setCchGroupCode(String cchGroupCode) {
	//CCH		this.cchGroupCode = cchGroupCode;
	//CCH	}

	//CCH	public String getCchItemCode() {
	//CCH		return cchItemCode;
	//CCH	}

	//CCH	public void setCchItemCode(String cchItemCode) {
	//CCH		this.cchItemCode = cchItemCode;
	//CCH	}

	public String getManualTaxcodeInd() {
		return manualTaxcodeInd;
	}

	public void setManualTaxcodeInd(String manualTaxcodeInd) {
		this.manualTaxcodeInd = manualTaxcodeInd;
	}

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getLocationMatrixId() {
		return locationMatrixId;
	}

	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public Long getJurisdictionTaxrateId() {
		return jurisdictionTaxrateId;
	}

	public void setJurisdictionTaxrateId(Long jurisdictionTaxrateId) {
		this.jurisdictionTaxrateId = jurisdictionTaxrateId;
	}

	public String getManualJurisdictionInd() {
		return manualJurisdictionInd;
	}

	public void setManualJurisdictionInd(String manualJurisdictionInd) {
		this.manualJurisdictionInd = manualJurisdictionInd;
	}

	public String getCountySingleFlag() {
		return countySingleFlag;
	}

	public void setCountySingleFlag(String countySingleFlag) {
		this.countySingleFlag = countySingleFlag;
	}

	public String getCountyDefaultFlag() {
		return countyDefaultFlag;
	}

	public void setCountyDefaultFlag(String countyDefaultFlag) {
		this.countyDefaultFlag = countyDefaultFlag;
	}

	public String getCitySingleFlag() {
		return citySingleFlag;
	}

	public void setCitySingleFlag(String citySingleFlag) {
		this.citySingleFlag = citySingleFlag;
	}

	public String getCityDefaultFlag() {
		return cityDefaultFlag;
	}

	public void setCityDefaultFlag(String cityDefaultFlag) {
		this.cityDefaultFlag = cityDefaultFlag;
	}

	public Date getLoadTimestamp() {
		return loadTimestamp;
	}

	public void setLoadTimestamp(Date loadTimestamp) {
		this.loadTimestamp = loadTimestamp;
	}

	public String getGlExtractUpdater() {
		return glExtractUpdater;
	}

	public void setGlExtractUpdater(String glExtractUpdater) {
		this.glExtractUpdater = glExtractUpdater;
	}

	public Date getGlExtractTimestamp() {
		return glExtractTimestamp;
	}

	public void setGlExtractTimestamp(Date glExtractTimestamp) {
		this.glExtractTimestamp = glExtractTimestamp;
	}

	public Long getGlExtractFlag() {
		return glExtractFlag;
	}

	public void setGlExtractFlag(Long glExtractFlag) {
		this.glExtractFlag = glExtractFlag;
	}

	public Long getGlLogFlag() {
		return glLogFlag;
	}

	public void setGlLogFlag(Long glLogFlag) {
		this.glLogFlag = glLogFlag;
	}

	public String getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}

	public String getAuditUserId() {
		return auditUserId;
	}

	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}

	public Date getAuditTimestamp() {
		return auditTimestamp;
	}

	public void setAuditTimestamp(Date auditTimestamp) {
		this.auditTimestamp = auditTimestamp;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public Date getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(Date modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}

	public BigDecimal getGlLineItmDistAmt() {
		return this.glLineItmDistAmt;
	}

	public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt) {
		this.glLineItmDistAmt = glLineItmDistAmt;
	}

	public BigDecimal getOrigGlLineItmDistAmt() {
		return this.origGlLineItmDistAmt;
	}

	public void setOrigGlLineItmDistAmt(BigDecimal origGlLineItmDistAmt) {
		this.origGlLineItmDistAmt = origGlLineItmDistAmt;
	}

	public BigDecimal getInvoiceFreightAmt() {
		return this.invoiceFreightAmt;
	}

	public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt) {
		this.invoiceFreightAmt = invoiceFreightAmt;
	}

	public BigDecimal getInvoiceDiscountAmt() {
		return this.invoiceDiscountAmt;
	}

	public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt) {
		this.invoiceDiscountAmt = invoiceDiscountAmt;
	}

	public BigDecimal getInvoiceTaxAmt() {
		return this.invoiceTaxAmt;
	}

	public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt) {
		this.invoiceTaxAmt = invoiceTaxAmt;
	}

	public BigDecimal getInvoiceTotalAmt() {
		return this.invoiceTotalAmt;
	}

	public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt) {
		this.invoiceTotalAmt = invoiceTotalAmt;
	}

	public BigDecimal getInvoiceLineAmt() {
		return this.invoiceLineAmt;
	}

	public void setInvoiceLineAmt(BigDecimal invoiceLineAmt) {
		this.invoiceLineAmt = invoiceLineAmt;
	}

	public BigDecimal getInvoiceLineTax() {
		return this.invoiceLineTax;
	}

	public void setInvoiceLineTax(BigDecimal invoiceLineTax) {
		this.invoiceLineTax = invoiceLineTax;
	}

	public BigDecimal getCheckAmt() {
		return this.checkAmt;
	}

	public void setCheckAmt(BigDecimal checkAmt) {
		this.checkAmt = checkAmt;
	}

	public BigDecimal getUserNumber01() {
		return this.userNumber01;
	}

	public void setUserNumber01(BigDecimal userNumber01) {
		this.userNumber01 = userNumber01;
	}

	public BigDecimal getUserNumber02() {
		return this.userNumber02;
	}

	public void setUserNumber02(BigDecimal userNumber02) {
		this.userNumber02 = userNumber02;
	}

	public BigDecimal getUserNumber03() {
		return this.userNumber03;
	}

	public void setUserNumber03(BigDecimal userNumber03) {
		this.userNumber03 = userNumber03;
	}

	public BigDecimal getUserNumber04() {
		return this.userNumber04;
	}

	public void setUserNumber04(BigDecimal userNumber04) {
		this.userNumber04 = userNumber04;
	}

	public BigDecimal getUserNumber05() {
		return this.userNumber05;
	}

	public void setUserNumber05(BigDecimal userNumber05) {
		this.userNumber05 = userNumber05;
	}

	public BigDecimal getUserNumber06() {
		return this.userNumber06;
	}

	public void setUserNumber06(BigDecimal userNumber06) {
		this.userNumber06 = userNumber06;
	}

	public BigDecimal getUserNumber07() {
		return this.userNumber07;
	}

	public void setUserNumber07(BigDecimal userNumber07) {
		this.userNumber07 = userNumber07;
	}

	public BigDecimal getUserNumber08() {
		return this.userNumber08;
	}

	public void setUserNumber08(BigDecimal userNumber08) {
		this.userNumber08 = userNumber08;
	}

	public BigDecimal getUserNumber09() {
		return this.userNumber09;
	}

	public void setUserNumber09(BigDecimal userNumber09) {
		this.userNumber09 = userNumber09;
	}

	public BigDecimal getUserNumber10() {
		return this.userNumber10;
	}

	public void setUserNumber10(BigDecimal userNumber10) {
		this.userNumber10 = userNumber10;
	}

	public BigDecimal getTbCalcTaxAmt() {
		return this.tbCalcTaxAmt;
	}

	public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt) {
		this.tbCalcTaxAmt = tbCalcTaxAmt;
	}

	public BigDecimal getStateUseAmount() {
		return this.stateUseAmount;
	}

	public void setStateUseAmount(BigDecimal stateUseAmount) {
		this.stateUseAmount = stateUseAmount;
	}

	public BigDecimal getStateUseTier2Amount() {
		return this.stateUseTier2Amount;
	}

	public void setStateUseTier2Amount(BigDecimal stateUseTier2Amount) {
		this.stateUseTier2Amount = stateUseTier2Amount;
	}

	public BigDecimal getStateUseTier3Amount() {
		return this.stateUseTier3Amount;
	}

	public void setStateUseTier3Amount(BigDecimal stateUseTier3Amount) {
		this.stateUseTier3Amount = stateUseTier3Amount;
	}

	public BigDecimal getCountyUseAmount() {
		return this.countyUseAmount;
	}

	public void setCountyUseAmount(BigDecimal countyUseAmount) {
		this.countyUseAmount = countyUseAmount;
	}

	public BigDecimal getCountyLocalUseAmount() {
		return this.countyLocalUseAmount;
	}

	public void setCountyLocalUseAmount(BigDecimal countyLocalUseAmount) {
		this.countyLocalUseAmount = countyLocalUseAmount;
	}

	public BigDecimal getCityUseAmount() {
		return this.cityUseAmount;
	}

	public void setCityUseAmount(BigDecimal cityUseAmount) {
		this.cityUseAmount = cityUseAmount;
	}

	public BigDecimal getCityLocalUseAmount() {
		return this.cityLocalUseAmount;
	}

	public void setCityLocalUseAmount(BigDecimal cityLocalUseAmount) {
		this.cityLocalUseAmount = cityLocalUseAmount;
	}

	public BigDecimal getStateUseRate() {
		return this.stateUseRate;
	}

	public void setStateUseRate(BigDecimal stateUseRate) {
		this.stateUseRate = stateUseRate;
	}

	public BigDecimal getStateUseTier2Rate() {
		return this.stateUseTier2Rate;
	}

	public void setStateUseTier2Rate(BigDecimal stateUseTier2Rate) {
		this.stateUseTier2Rate = stateUseTier2Rate;
	}

	public BigDecimal getStateUseTier3Rate() {
		return this.stateUseTier3Rate;
	}

	public void setStateUseTier3Rate(BigDecimal stateUseTier3Rate) {
		this.stateUseTier3Rate = stateUseTier3Rate;
	}

	public BigDecimal getStateSplitAmount() {
		return this.stateSplitAmount;
	}

	public void setStateSplitAmount(BigDecimal stateSplitAmount) {
		this.stateSplitAmount = stateSplitAmount;
	}

	public BigDecimal getStateTier2MinAmount() {
		return this.stateTier2MinAmount;
	}

	public void setStateTier2MinAmount(BigDecimal stateTier2MinAmount) {
		this.stateTier2MinAmount = stateTier2MinAmount;
	}

	public BigDecimal getStateTier2MaxAmount() {
		return this.stateTier2MaxAmount;
	}

	public void setStateTier2MaxAmount(BigDecimal stateTier2MaxAmount) {
		this.stateTier2MaxAmount = stateTier2MaxAmount;
	}

	public BigDecimal getStateMaxtaxAmount() {
		return this.stateMaxtaxAmount;
	}

	public void setStateMaxtaxAmount(BigDecimal stateMaxtaxAmount) {
		this.stateMaxtaxAmount = stateMaxtaxAmount;
	}

	public BigDecimal getCountyUseRate() {
		return this.countyUseRate;
	}

	public void setCountyUseRate(BigDecimal countyUseRate) {
		this.countyUseRate = countyUseRate;
	}

	public BigDecimal getCountySplitAmount() {
		return this.countySplitAmount;
	}

	public void setCountySplitAmount(BigDecimal countySplitAmount) {
		this.countySplitAmount = countySplitAmount;
	}

	public BigDecimal getCountyMaxtaxAmount() {
		return this.countyMaxtaxAmount;
	}

	public void setCountyMaxtaxAmount(BigDecimal countyMaxtaxAmount) {
		this.countyMaxtaxAmount = countyMaxtaxAmount;
	}

	public BigDecimal getCityUseRate() {
		return this.cityUseRate;
	}

	public void setCityUseRate(BigDecimal cityUseRate) {
		this.cityUseRate = cityUseRate;
	}

	public BigDecimal getCityLocalUseRate() {
		return this.cityLocalUseRate;
	}

	public void setCityLocalUseRate(BigDecimal cityLocalUseRate) {
		this.cityLocalUseRate = cityLocalUseRate;
	}

	public BigDecimal getCitySplitAmount() {
		return this.citySplitAmount;
	}

	public void setCitySplitAmount(BigDecimal citySplitAmount) {
		this.citySplitAmount = citySplitAmount;
	}

	public BigDecimal getCitySplitUseRate() {
		return this.citySplitUseRate;
	}

	public void setCitySplitUseRate(BigDecimal citySplitUseRate) {
		this.citySplitUseRate = citySplitUseRate;
	}

	public BigDecimal getCombinedUseRate() {
		return this.combinedUseRate;
	}

	public void setCombinedUseRate(BigDecimal combinedUseRate) {
		this.combinedUseRate = combinedUseRate;
	}

	public BigDecimal getGlExtractAmt() {
		return this.glExtractAmt;
	}

	public void setGlExtractAmt(BigDecimal glExtractAmt) {
		this.glExtractAmt = glExtractAmt;
	}
	
	public Long getSplitSubtransId() {
		return splitSubtransId;
	}

	public void setSplitSubtransId(Long splitSubtransId) {
		this.splitSubtransId = splitSubtransId;
	}

	public String getMultiTransCode() {
		return multiTransCode;
	}

	public void setMultiTransCode(String multiTransCode) {
		this.multiTransCode = multiTransCode;
	}

	public BigDecimal getCountryUseAmount() {
		return countryUseAmount;
	}

	public void setCountryUseAmount(BigDecimal countryUseAmount) {
		this.countryUseAmount= countryUseAmount;
	}

	public String getTransactionCountryCode() {
		return transactionCountryCode;
	}

	public void setTransactionCountryCode(String transactionCountryCode) {
		this.transactionCountryCode= transactionCountryCode;
	}

	public String getAutoTransactionCountryCode() {
		return autoTransactionCountryCode;
	}

	public void setAutoTransactionCountryCode(String autoTransactionCountryCode) {
		this.autoTransactionCountryCode= autoTransactionCountryCode;
	}

	public BigDecimal getCountryUseRate() {
		return countryUseRate;
	}

	public void setCountryUseRate(BigDecimal countryUseRate) {
		this.countryUseRate= countryUseRate;
	}
	
	public Long getStateTaxcodeDetailId(){
		return this.stateTaxcodeDetailId;
	}
	
	public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId){
		this.stateTaxcodeDetailId = stateTaxcodeDetailId;
	}
	
	public Long getCountyTaxcodeDetailId(){
		return this.countyTaxcodeDetailId;
	}
	
	public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId){
		this.countyTaxcodeDetailId = countyTaxcodeDetailId;
	}

	public Long getCityTaxcodeDetailId(){
		return this.cityTaxcodeDetailId;
	}
	
	public void setCityTaxcodeDetailId(Long  cityTaxcodeDetailId){
		this.cityTaxcodeDetailId = cityTaxcodeDetailId;
	}

	public String getTaxtypeUsedCode(){
		return this.taxtypeUsedCode;
	}
	
	public void setTaxtypeUsedCode(String taxtypeUsedCode){
		this.taxtypeUsedCode = taxtypeUsedCode;
	}

	public BigDecimal getStateTaxableAmt(){
		return this.stateTaxableAmt;
	}
	
	public void setStateTaxableAmt(BigDecimal stateTaxableAmt){
		this.stateTaxableAmt = stateTaxableAmt;
	}

	public BigDecimal getCountyTaxableAmt(){
		return this.countyTaxableAmt;
	}
	
	public void setCountyTaxableAmt(BigDecimal countyTaxableAmt){
		this.countyTaxableAmt = countyTaxableAmt;
	}

	public BigDecimal getCityTaxableAmt(){
		return this.cityTaxableAmt;
	}

	public void setCityTaxableAmt(BigDecimal cityTaxableAmt){
		this.cityTaxableAmt = cityTaxableAmt;
	}
	
	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode= entityCode;
	}
	
	public BigDecimal getStj1Rate() {
		return stj1Rate;
	}

	public void setStj1Rate(BigDecimal stj1Rate) {
		this.stj1Rate = stj1Rate;
	}

	public BigDecimal getStj2Rate() {
		return stj2Rate;
	}

	public void setStj2Rate(BigDecimal stj2Rate) {
		this.stj2Rate = stj2Rate;
	}

	public BigDecimal getStj3Rate() {
		return stj3Rate;
	}

	public void setStj3Rate(BigDecimal stj3Rate) {
		this.stj3Rate = stj3Rate;
	}

	public BigDecimal getStj4Rate() {
		return stj4Rate;
	}

	public void setStj4Rate(BigDecimal stj4Rate) {
		this.stj4Rate = stj4Rate;
	}

	public BigDecimal getStj5Rate() {
		return stj5Rate;
	}

	public void setStj5Rate(BigDecimal stj5Rate) {
		this.stj5Rate = stj5Rate;
	}
	
	public BigDecimal getStj1Amount() {
		return stj1Amount;
	}

	public void setStj1Amount(BigDecimal stj1Amount) {
		this.stj1Amount = stj1Amount;
	}

	public BigDecimal getStj2Amount() {
		return stj2Amount;
	}

	public void setStj2Amount(BigDecimal stj2Amount) {
		this.stj2Amount = stj2Amount;
	}

	public BigDecimal getStj3Amount() {
		return stj3Amount;
	}

	public void setStj3Amount(BigDecimal stj3Amount) {
		this.stj3Amount = stj3Amount;
	}

	public BigDecimal getStj4Amount() {
		return stj4Amount;
	}

	public void setStj4Amount(BigDecimal stj4Amount) {
		this.stj4Amount = stj4Amount;
	}

	public BigDecimal getStj5Amount() {
		return stj5Amount;
	}

	public void setStj5Amount(BigDecimal stj5Amount) {
		this.stj5Amount = stj5Amount;
	}
	
	public String getCityTaxcodeTypeCode() {
	    return cityTaxcodeTypeCode;
	}

	public void setCityTaxcodeTypeCode(String cityTaxcodeTypeCode) {
	    this.cityTaxcodeTypeCode = cityTaxcodeTypeCode;
	}

	public BigDecimal getCountyLocalUseRate() {
	    return countyLocalUseRate;
	}

	public void setCountyLocalUseRate(BigDecimal countyLocalUseRate) {
	    this.countyLocalUseRate = countyLocalUseRate;
	}

	public String getCountyTaxcodeTypeCode() {
	    return countyTaxcodeTypeCode;
	}

	public void setCountyTaxcodeTypeCode(String countyTaxcodeTypeCode) {
	    this.countyTaxcodeTypeCode = countyTaxcodeTypeCode;
	}

	public String getMeasureTypeCode() {
	    return measureTypeCode;
	}

	public void setMeasureTypeCode(String measureTypeCode) {
	    this.measureTypeCode = measureTypeCode;
	}

	public String getProductCode() {
	    return productCode;
	}

	public void setProductCode(String productCode) {
	    this.productCode = productCode;
	}

	public String getRejectFlag() {
	    return rejectFlag;
	}

	public void setRejectFlag(String rejectFlag) {
	    this.rejectFlag = rejectFlag;
	}

	public String getStateTaxcodeTypeCode() {
	    return stateTaxcodeTypeCode;
	}

	public void setStateTaxcodeTypeCode(String stateTaxcodeTypeCode) {
	    this.stateTaxcodeTypeCode = stateTaxcodeTypeCode;
	}

	public Double getStateUseTier1Setamt() {
	    return stateUseTier1Setamt;
	}

	public void setStateUseTier1Setamt(Double stateUseTier1Setamt) {
	    this.stateUseTier1Setamt = stateUseTier1Setamt;
	}

	public Double getStateUseTier2Setamt() {
	    return stateUseTier2Setamt;
	}

	public void setStateUseTier2Setamt(Double stateUseTier2Setamt) {
	    this.stateUseTier2Setamt = stateUseTier2Setamt;
	}

	public Double getStateUseTier3Setamt() {
	    return stateUseTier3Setamt;
	}

	public void setStateUseTier3Setamt(Double stateUseTier3Setamt) {
	    this.stateUseTier3Setamt = stateUseTier3Setamt;
	}

	public String getTaxcodeCountryCode() {
	    return taxcodeCountryCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
	    this.taxcodeCountryCode = taxcodeCountryCode;
	}

	public Long getTaxcodeDetailId() {
	    return taxcodeDetailId;
	}

	public void setTaxcodeDetailId(Long taxcodeDetailId) {
	    this.taxcodeDetailId = taxcodeDetailId;
	}

	public String getTaxcodeStateCode() {
	    return taxcodeStateCode;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
	    this.taxcodeStateCode = taxcodeStateCode;
	}

	public String getTaxcodeTypeCode() {
	    return taxcodeTypeCode;
	}

	public void setTaxcodeTypeCode(String taxcodeTypeCode) {
	    this.taxcodeTypeCode = taxcodeTypeCode;
	}

	public Date getUserDate11() {
	    return userDate11;
	}

	public void setUserDate11(Date userDate11) {
	    this.userDate11 = userDate11;
	}

	public Date getUserDate12() {
	    return userDate12;
	}

	public void setUserDate12(Date userDate12) {
	    this.userDate12 = userDate12;
	}

	public Date getUserDate13() {
	    return userDate13;
	}

	public void setUserDate13(Date userDate13) {
	    this.userDate13 = userDate13;
	}

	public Date getUserDate14() {
	    return userDate14;
	}

	public void setUserDate14(Date userDate14) {
	    this.userDate14 = userDate14;
	}

	public Date getUserDate15() {
	    return userDate15;
	}

	public void setUserDate15(Date userDate15) {
	    this.userDate15 = userDate15;
	}

	public Date getUserDate16() {
	    return userDate16;
	}

	public void setUserDate16(Date userDate16) {
	    this.userDate16 = userDate16;
	}

	public Date getUserDate17() {
	    return userDate17;
	}

	public void setUserDate17(Date userDate17) {
	    this.userDate17 = userDate17;
	}

	public Date getUserDate18() {
	    return userDate18;
	}

	public void setUserDate18(Date userDate18) {
	    this.userDate18 = userDate18;
	}

	public Date getUserDate19() {
	    return userDate19;
	}

	public void setUserDate19(Date userDate19) {
	    this.userDate19 = userDate19;
	}

	public Date getUserDate20() {
	    return userDate20;
	}

	public void setUserDate20(Date userDate20) {
	    this.userDate20 = userDate20;
	}

	public Date getUserDate21() {
	    return userDate21;
	}

	public void setUserDate21(Date userDate21) {
	    this.userDate21 = userDate21;
	}

	public Date getUserDate22() {
	    return userDate22;
	}

	public void setUserDate22(Date userDate22) {
	    this.userDate22 = userDate22;
	}

	public Date getUserDate23() {
	    return userDate23;
	}

	public void setUserDate23(Date userDate23) {
	    this.userDate23 = userDate23;
	}

	public Date getUserDate24() {
	    return userDate24;
	}

	public void setUserDate24(Date userDate24) {
	    this.userDate24 = userDate24;
	}

	public Date getUserDate25() {
	    return userDate25;
	}

	public void setUserDate25(Date userDate25) {
	    this.userDate25 = userDate25;
	}

	public BigDecimal getUserNumber11() {
	    return userNumber11;
	}

	public void setUserNumber11(BigDecimal userNumber11) {
	    this.userNumber11 = userNumber11;
	}

	public BigDecimal getUserNumber12() {
	    return userNumber12;
	}

	public void setUserNumber12(BigDecimal userNumber12) {
	    this.userNumber12 = userNumber12;
	}

	public BigDecimal getUserNumber13() {
	    return userNumber13;
	}

	public void setUserNumber13(BigDecimal userNumber13) {
	    this.userNumber13 = userNumber13;
	}

	public BigDecimal getUserNumber14() {
	    return userNumber14;
	}

	public void setUserNumber14(BigDecimal userNumber14) {
	    this.userNumber14 = userNumber14;
	}

	public BigDecimal getUserNumber15() {
	    return userNumber15;
	}

	public void setUserNumber15(BigDecimal userNumber15) {
	    this.userNumber15 = userNumber15;
	}

	public BigDecimal getUserNumber16() {
	    return userNumber16;
	}

	public void setUserNumber16(BigDecimal userNumber16) {
	    this.userNumber16 = userNumber16;
	}

	public BigDecimal getUserNumber17() {
	    return userNumber17;
	}

	public void setUserNumber17(BigDecimal userNumber17) {
	    this.userNumber17 = userNumber17;
	}

	public BigDecimal getUserNumber18() {
	    return userNumber18;
	}

	public void setUserNumber18(BigDecimal userNumber18) {
	    this.userNumber18 = userNumber18;
	}

	public BigDecimal getUserNumber19() {
	    return userNumber19;
	}

	public void setUserNumber19(BigDecimal userNumber19) {
	    this.userNumber19 = userNumber19;
	}

	public BigDecimal getUserNumber20() {
	    return userNumber20;
	}

	public void setUserNumber20(BigDecimal userNumber20) {
	    this.userNumber20 = userNumber20;
	}

	public BigDecimal getUserNumber21() {
	    return userNumber21;
	}

	public void setUserNumber21(BigDecimal userNumber21) {
	    this.userNumber21 = userNumber21;
	}

	public BigDecimal getUserNumber22() {
	    return userNumber22;
	}

	public void setUserNumber22(BigDecimal userNumber22) {
	    this.userNumber22 = userNumber22;
	}

	public BigDecimal getUserNumber23() {
	    return userNumber23;
	}

	public void setUserNumber23(BigDecimal userNumber23) {
	    this.userNumber23 = userNumber23;
	}

	public BigDecimal getUserNumber24() {
	    return userNumber24;
	}

	public void setUserNumber24(BigDecimal userNumber24) {
	    this.userNumber24 = userNumber24;
	}

	public BigDecimal getUserNumber25() {
	    return userNumber25;
	}

	public void setUserNumber25(BigDecimal userNumber25) {
	    this.userNumber25 = userNumber25;
	}

	public String getUserText100() {
	    return userText100;
	}

	public void setUserText100(String userText100) {
	    this.userText100 = userText100;
	}

	public String getUserText31() {
	    return userText31;
	}

	public void setUserText31(String userText31) {
	    this.userText31 = userText31;
	}

	public String getUserText32() {
	    return userText32;
	}

	public void setUserText32(String userText32) {
	    this.userText32 = userText32;
	}

	public String getUserText33() {
	    return userText33;
	}

	public void setUserText33(String userText33) {
	    this.userText33 = userText33;
	}

	public String getUserText34() {
	    return userText34;
	}

	public void setUserText34(String userText34) {
	    this.userText34 = userText34;
	}

	public String getUserText35() {
	    return userText35;
	}

	public void setUserText35(String userText35) {
	    this.userText35 = userText35;
	}

	public String getUserText36() {
	    return userText36;
	}

	public void setUserText36(String userText36) {
	    this.userText36 = userText36;
	}

	public String getUserText37() {
	    return userText37;
	}

	public void setUserText37(String userText37) {
	    this.userText37 = userText37;
	}

	public String getUserText38() {
	    return userText38;
	}

	public void setUserText38(String userText38) {
	    this.userText38 = userText38;
	}

	public String getUserText39() {
	    return userText39;
	}

	public void setUserText39(String userText39) {
	    this.userText39 = userText39;
	}

	public String getUserText40() {
	    return userText40;
	}

	public void setUserText40(String userText40) {
	    this.userText40 = userText40;
	}

	public String getUserText41() {
	    return userText41;
	}

	public void setUserText41(String userText41) {
	    this.userText41 = userText41;
	}

	public String getUserText42() {
	    return userText42;
	}

	public void setUserText42(String userText42) {
	    this.userText42 = userText42;
	}

	public String getUserText43() {
	    return userText43;
	}

	public void setUserText43(String userText43) {
	    this.userText43 = userText43;
	}

	public String getUserText44() {
	    return userText44;
	}

	public void setUserText44(String userText44) {
	    this.userText44 = userText44;
	}

	public String getUserText45() {
	    return userText45;
	}

	public void setUserText45(String userText45) {
	    this.userText45 = userText45;
	}

	public String getUserText46() {
	    return userText46;
	}

	public void setUserText46(String userText46) {
	    this.userText46 = userText46;
	}

	public String getUserText47() {
	    return userText47;
	}

	public void setUserText47(String userText47) {
	    this.userText47 = userText47;
	}

	public String getUserText48() {
	    return userText48;
	}

	public void setUserText48(String userText48) {
	    this.userText48 = userText48;
	}

	public String getUserText49() {
	    return userText49;
	}

	public void setUserText49(String userText49) {
	    this.userText49 = userText49;
	}

	public String getUserText50() {
	    return userText50;
	}

	public void setUserText50(String userText50) {
	    this.userText50 = userText50;
	}

	public String getUserText51() {
	    return userText51;
	}

	public void setUserText51(String userText51) {
	    this.userText51 = userText51;
	}

	public String getUserText52() {
	    return userText52;
	}

	public void setUserText52(String userText52) {
	    this.userText52 = userText52;
	}

	public String getUserText53() {
	    return userText53;
	}

	public void setUserText53(String userText53) {
	    this.userText53 = userText53;
	}

	public String getUserText54() {
	    return userText54;
	}

	public void setUserText54(String userText54) {
	    this.userText54 = userText54;
	}

	public String getUserText55() {
	    return userText55;
	}

	public void setUserText55(String userText55) {
	    this.userText55 = userText55;
	}

	public String getUserText56() {
	    return userText56;
	}

	public void setUserText56(String userText56) {
	    this.userText56 = userText56;
	}

	public String getUserText57() {
	    return userText57;
	}

	public void setUserText57(String userText57) {
	    this.userText57 = userText57;
	}

	public String getUserText58() {
	    return userText58;
	}

	public void setUserText58(String userText58) {
	    this.userText58 = userText58;
	}

	public String getUserText59() {
	    return userText59;
	}

	public void setUserText59(String userText59) {
	    this.userText59 = userText59;
	}

	public String getUserText60() {
	    return userText60;
	}

	public void setUserText60(String userText60) {
	    this.userText60 = userText60;
	}

	public String getUserText61() {
	    return userText61;
	}

	public void setUserText61(String userText61) {
	    this.userText61 = userText61;
	}

	public String getUserText62() {
	    return userText62;
	}

	public void setUserText62(String userText62) {
	    this.userText62 = userText62;
	}

	public String getUserText63() {
	    return userText63;
	}

	public void setUserText63(String userText63) {
	    this.userText63 = userText63;
	}

	public String getUserText64() {
	    return userText64;
	}

	public void setUserText64(String userText64) {
	    this.userText64 = userText64;
	}

	public String getUserText65() {
	    return userText65;
	}

	public void setUserText65(String userText65) {
	    this.userText65 = userText65;
	}

	public String getUserText66() {
	    return userText66;
	}

	public void setUserText66(String userText66) {
	    this.userText66 = userText66;
	}

	public String getUserText67() {
	    return userText67;
	}

	public void setUserText67(String userText67) {
	    this.userText67 = userText67;
	}

	public String getUserText68() {
	    return userText68;
	}

	public void setUserText68(String userText68) {
	    this.userText68 = userText68;
	}

	public String getUserText69() {
	    return userText69;
	}

	public void setUserText69(String userText69) {
	    this.userText69 = userText69;
	}

	public String getUserText70() {
	    return userText70;
	}

	public void setUserText70(String userText70) {
	    this.userText70 = userText70;
	}

	public String getUserText71() {
	    return userText71;
	}

	public void setUserText71(String userText71) {
	    this.userText71 = userText71;
	}

	public String getUserText72() {
	    return userText72;
	}

	public void setUserText72(String userText72) {
	    this.userText72 = userText72;
	}

	public String getUserText73() {
	    return userText73;
	}

	public void setUserText73(String userText73) {
	    this.userText73 = userText73;
	}

	public String getUserText74() {
	    return userText74;
	}

	public void setUserText74(String userText74) {
	    this.userText74 = userText74;
	}

	public String getUserText75() {
	    return userText75;
	}

	public void setUserText75(String userText75) {
	    this.userText75 = userText75;
	}

	public String getUserText76() {
	    return userText76;
	}

	public void setUserText76(String userText76) {
	    this.userText76 = userText76;
	}

	public String getUserText77() {
	    return userText77;
	}

	public void setUserText77(String userText77) {
	    this.userText77 = userText77;
	}

	public String getUserText78() {
	    return userText78;
	}

	public void setUserText78(String userText78) {
	    this.userText78 = userText78;
	}

	public String getUserText79() {
	    return userText79;
	}

	public void setUserText79(String userText79) {
	    this.userText79 = userText79;
	}

	public String getUserText80() {
	    return userText80;
	}

	public void setUserText80(String userText80) {
	    this.userText80 = userText80;
	}

	public String getUserText81() {
	    return userText81;
	}

	public void setUserText81(String userText81) {
	    this.userText81 = userText81;
	}

	public String getUserText82() {
	    return userText82;
	}

	public void setUserText82(String userText82) {
	    this.userText82 = userText82;
	}

	public String getUserText83() {
	    return userText83;
	}

	public void setUserText83(String userText83) {
	    this.userText83 = userText83;
	}

	public String getUserText84() {
	    return userText84;
	}

	public void setUserText84(String userText84) {
	    this.userText84 = userText84;
	}

	public String getUserText85() {
	    return userText85;
	}

	public void setUserText85(String userText85) {
	    this.userText85 = userText85;
	}

	public String getUserText86() {
	    return userText86;
	}

	public void setUserText86(String userText86) {
	    this.userText86 = userText86;
	}

	public String getUserText87() {
	    return userText87;
	}

	public void setUserText87(String userText87) {
	    this.userText87 = userText87;
	}

	public String getUserText88() {
	    return userText88;
	}

	public void setUserText88(String userText88) {
	    this.userText88 = userText88;
	}

	public String getUserText89() {
	    return userText89;
	}

	public void setUserText89(String userText89) {
	    this.userText89 = userText89;
	}

	public String getUserText90() {
	    return userText90;
	}

	public void setUserText90(String userText90) {
	    this.userText90 = userText90;
	}

	public String getUserText91() {
	    return userText91;
	}

	public void setUserText91(String userText91) {
	    this.userText91 = userText91;
	}

	public String getUserText92() {
	    return userText92;
	}

	public void setUserText92(String userText92) {
	    this.userText92 = userText92;
	}

	public String getUserText93() {
	    return userText93;
	}

	public void setUserText93(String userText93) {
	    this.userText93 = userText93;
	}

	public String getUserText94() {
	    return userText94;
	}

	public void setUserText94(String userText94) {
	    this.userText94 = userText94;
	}

	public String getUserText95() {
	    return userText95;
	}

	public void setUserText95(String userText95) {
	    this.userText95 = userText95;
	}

	public String getUserText96() {
	    return userText96;
	}

	public void setUserText96(String userText96) {
	    this.userText96 = userText96;
	}

	public String getUserText97() {
	    return userText97;
	}

	public void setUserText97(String userText97) {
	    this.userText97 = userText97;
	}

	public String getUserText98() {
	    return userText98;
	}

	public void setUserText98(String userText98) {
	    this.userText98 = userText98;
	}

	public String getUserText99() {
	    return userText99;
	}

	public void setUserText99(String userText99) {
	    this.userText99 = userText99;
	}
	
	public String getCpFilingEntityCode() {
		return cpFilingEntityCode;
	}

	public void setCpFilingEntityCode(String cpFilingEntityCode) {
		this.cpFilingEntityCode = cpFilingEntityCode;
	}

	//Fix bugs 4381, 4380,4377, and 3921
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	  static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.DECIMAL);
	}
	
	public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPTransactionDetail.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into tb_bcp_transactions (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPTransactionDetail.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){
					s.setNull(idx, sqlTypes.get(f.getType()));
				}
				else{
					if("LOAD_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getLoadTimestamp().getTime()));
					}
					else if("GL_EXTRACT_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getGlExtractTimestamp().getTime()));
					}
					else if("AUDIT_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getAuditTimestamp().getTime()));
					}
					else if("MODIFY_TIMESTAMP".equalsIgnoreCase(col.name())){
						//Use java.sql.Timestamp when using PreparedStatement
						s.setTimestamp(idx, new java.sql.Timestamp(this.getModifyTimestamp().getTime()));
					}
					else if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DECIMAL){
						//Make sure it is float instead of Float object
						s.setBigDecimal(idx, ArithmeticUtils.toBigDecimal(fld));
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){					
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{					
						s.setObject(idx, fld);
					}
				}
			}
		}
	}

	public Long getTaxAllocMatrixId() {
		return taxAllocMatrixId;
	}

	public void setTaxAllocMatrixId(Long taxAllocMatrixId) {
		this.taxAllocMatrixId = taxAllocMatrixId;
	}

	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}
	
	public String getMultiTransCodeDesc() {
		String desc = "";
		if(this.multiTransCode != null) {
			if("OS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Split";
			}
			else if("S".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Split";
			}
			else if("OA".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Original Allocation";
			}
			else if("A".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Allocation";
			}
			else if("FS".equalsIgnoreCase(this.multiTransCode)) {
				desc = "Future Split";
			}
			else {
				desc = this.multiTransCode;
			}
		}
		
		return desc;
	}
	
	public String getRawInsertStatement(boolean powerBuilderVersion) throws Exception {
		Class<?> cls = BCPTransactionDetail.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into tb_bcp_transactions (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				if(!(powerBuilderVersion && col.name().equalsIgnoreCase("BCP_TRANSACTION_ID"))){		
					idx++;
					if (first)
						first = false;
					else {
						colBuf.append(", ");
						valBuf.append(", ");
					}
					colBuf.append(col.name());
					valBuf.append("?");		
				}
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s, boolean powerBuilderVersion) throws Exception {
		Class<?> cls = BCPTransactionDetail.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				if(!(powerBuilderVersion && col.name().equalsIgnoreCase("BCP_TRANSACTION_ID"))){
				
					Object fld = f.get(this);
					idx++;
					if (fld == null){
						s.setNull(idx, sqlTypes.get(f.getType()));
					}
					else{
						if("LOAD_TIMESTAMP".equalsIgnoreCase(col.name())){
							//Use java.sql.Timestamp when using PreparedStatement
							s.setTimestamp(idx, new java.sql.Timestamp(this.getLoadTimestamp().getTime()));
						}
						else if("GL_EXTRACT_TIMESTAMP".equalsIgnoreCase(col.name())){
							//Use java.sql.Timestamp when using PreparedStatement
							s.setTimestamp(idx, new java.sql.Timestamp(this.getGlExtractTimestamp().getTime()));
						}
						else if("AUDIT_TIMESTAMP".equalsIgnoreCase(col.name())){
							//Use java.sql.Timestamp when using PreparedStatement
							s.setTimestamp(idx, new java.sql.Timestamp(this.getAuditTimestamp().getTime()));
						}
						else if("MODIFY_TIMESTAMP".equalsIgnoreCase(col.name())){
							//Use java.sql.Timestamp when using PreparedStatement
							s.setTimestamp(idx, new java.sql.Timestamp(this.getModifyTimestamp().getTime()));
						}
						else if(sqlTypes.get(f.getType()) == Types.FLOAT){
							//Make sure it is float instead of Float object
							s.setFloat(idx, ((Float)fld).floatValue());
						}
						else if(sqlTypes.get(f.getType()) == Types.DOUBLE){
							s.setDouble(idx, ((Double)fld).doubleValue());
						}
						else if(sqlTypes.get(f.getType()) == Types.DATE){					
							//Make sure it is sql date instead of java util date
							s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
						}
						else{					
							s.setObject(idx, fld);
						}
					}
				
				}
			}
		}
	}
}

