package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the TB_CUST_CERT database table.
 * 
 */
@Entity
@Table(name="TB_CUST_CERT")
public class CustCert extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TB_CUST_CERT_CUSTCERTID_GENERATOR" , allocationSize = 1, sequenceName="SQ_TB_CUST_CERT_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CUST_CERT_CUSTCERTID_GENERATOR")
	@Column(name="CUST_CERT_ID")
	private Long custCertId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	@Column(name="ADDRESS_LINE_1")
	private String addressLine1;

	@Column(name="ADDRESS_LINE_2")
	private String addressLine2;

	@Column(name="APPLY_TO_STATE_FLAG")
	private String applyToStateFlag;

	@Column(name="BLANKET_FLAG")
	private String blanketFlag;

	@Column(name="CERT_CUST_NAME")
	private String certCustName;

    @Lob()
	@Column(name="CERT_IMAGE")
	private byte[] certImage;

	@Column(name="CERT_IMAGE_URL")
	private String certImageUrl;

	@Column(name="CERT_NBR")
	private String certNbr;

	@Column(name="CERT_STATE")
	private String certState;

	@Column(name="CERT_TYPE_CODE")
	private String certTypeCode;

	private String city;

	@Column(name="CITY_BASE_CHG_PCT")
	private Double cityBaseChgPct;

	@Column(name="CITY_FLAG")
	private String cityFlag;

	@Column(name="CITY_SPECIAL_RATE")
	private Double citySpecialRate;

	@Column(name="COUNTRY")
	private String country;

	@Column(name="COUNTRY_BASE_CHG_PCT")
	private Double countryBaseChgPct;

	@Column(name="COUNTRY_FLAG")
	private String countryFlag;

	@Column(name="COUNTRY_SPECIAL_RATE")
	private Double countrySpecialRate;

	private String county;

	@Column(name="COUNTY_BASE_CHG_PCT")
	private Double countyBaseChgPct;

	@Column(name="COUNTY_FLAG")
	private String countyFlag;

	@Column(name="COUNTY_SPECIAL_RATE")
	private Double countySpecialRate;

	@Column(name="CUST_LOCN_ID")
	private Long custLocnId;

	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="ENTITY_ID")
	private Long entityId;

	@Column(name="EXEMPT_REASON_CODE")
	private String exemptReasonCode;

	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	private String geocode;

	@Column(name="INVOICE_NBR")
	private String invoiceNbr;

	@Column(name="STATE")
	private String state;

	@Column(name="STATE_BASE_CHG_PCT")
	private Double stateBaseChgPct;

	@Column(name="STATE_FLAG")
	private String stateFlag;

	@Column(name="STATE_SPECIAL_RATE")
	private Double stateSpecialRate;

	@Column(name="STJ1_BASE_CHG_PCT")
	private Double stj1BaseChgPct;

	@Column(name="STJ1_FLAG")
	private String stj1Flag;

	@Column(name="STJ1_SPECIAL_RATE")
	private Double stj1SpecialRate;

	@Column(name="STJ2_BASE_CHG_PCT")
	private Double stj2BaseChgPct;

	@Column(name="STJ2_FLAG")
	private String stj2Flag;

	@Column(name="STJ2_SPECIAL_RATE")
	private Double stj2SpecialRate;

	@Column(name="STJ3_BASE_CHG_PCT")
	private Double stj3BaseChgPct;

	@Column(name="STJ3_FLAG")
	private String stj3Flag;

	@Column(name="STJ3_SPECIAL_RATE")
	private Double stj3SpecialRate;

	@Column(name="STJ4_BASE_CHG_PCT")
	private Double stj4BaseChgPct;

	@Column(name="STJ4_FLAG")
	private String stj4Flag;

	@Column(name="STJ4_SPECIAL_RATE")
	private Double stj4SpecialRate;

	@Column(name="STJ5_BASE_CHG_PCT")
	private Double stj5BaseChgPct;

	@Column(name="STJ5_FLAG")
	private String stj5Flag;

	@Column(name="STJ5_SPECIAL_RATE")
	private Double stj5SpecialRate;

	@Column(name="ZIP")
	private String zip;

	@Column(name="ZIPPLUS4")
	private String zipplus4;
	
	@Transient
	private Long trueCustCertId;
	
	@Transient
	private Date effectiveDateThru;
	
	@Transient
	private Long custLocnExId;
	
	@Transient
	private Long custId;
	
	@Transient
	private String custNbr;
	
	@Transient
	private String custName;
	
	@Transient
	private String custActiveFlag;
	
	@Transient
	private String custLocnCode;
	
	@Transient
	private String custLocnName;
	
	@Transient
	private String custLocnCountry;
	
	@Transient
	private String custLocnState;
	
	@Transient
	private String custLocnCity;
	
	@Transient
	private String custLocnActiveFlag;
	
	@Transient
	private String entityCode; 
	
	@Transient
	private String entityName; 
	
	@Transient
	private String exemptionTypeCode;


    public CustCert() {
    }
    
    public Long getId() {
		return getCustCertId();
	}

	public String getIdPropertyName() {
		return "custCertId";
	}
	
	public void setId(Long id) {
		setCustCertId(id);
	}

	public Long getCustCertId() {
		return this.custCertId;
	}

	public void setCustCertId(Long custCertId) {
		this.custCertId = custCertId;
	}
	
	public Long getTrueCustCertId() {
		return this.trueCustCertId;
	}

	public void setTrueCustCertId(Long trueCustCertId) {
		this.trueCustCertId = trueCustCertId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getApplyToStateFlag() {
		return this.applyToStateFlag;
	}

	public void setApplyToStateFlag(String applyToStateFlag) {
		this.applyToStateFlag = applyToStateFlag;
	}

	public String getBlanketFlag() {
		return this.blanketFlag;
	}

	public void setBlanketFlag(String blanketFlag) {
		this.blanketFlag = blanketFlag;
	}

	public String getCertCustName() {
		return this.certCustName;
	}

	public void setCertCustName(String certCustName) {
		this.certCustName = certCustName;
	}

	public byte[] getCertImage() {
		return this.certImage;
	}

	public void setCertImage(byte[] certImage) {
		this.certImage = certImage;
	}

	public String getCertImageUrl() {
		return this.certImageUrl;
	}

	public void setCertImageUrl(String certImageUrl) {
		this.certImageUrl = certImageUrl;
	}

	public String getCertNbr() {
		return this.certNbr;
	}

	public void setCertNbr(String certNbr) {
		this.certNbr = certNbr;
	}

	public String getCertState() {
		return this.certState;
	}

	public void setCertState(String certState) {
		this.certState = certState;
	}

	public String getCertTypeCode() {
		return this.certTypeCode;
	}

	public void setCertTypeCode(String certTypeCode) {
		this.certTypeCode = certTypeCode;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Double getCityBaseChgPct() {
		return this.cityBaseChgPct;
	}

	public void setCityBaseChgPct(Double cityBaseChgPct) {
		this.cityBaseChgPct = cityBaseChgPct;
	}

	public String getCityFlag() {
		return this.cityFlag;
	}

	public void setCityFlag(String cityFlag) {
		this.cityFlag = cityFlag;
	}

	public Double getCitySpecialRate() {
		return this.citySpecialRate;
	}

	public void setCitySpecialRate(Double citySpecialRate) {
		this.citySpecialRate = citySpecialRate;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Double getCountryBaseChgPct() {
		return this.countryBaseChgPct;
	}

	public void setCountryBaseChgPct(Double countryBaseChgPct) {
		this.countryBaseChgPct = countryBaseChgPct;
	}

	public String getCountryFlag() {
		return this.countryFlag;
	}

	public void setCountryFlag(String countryFlag) {
		this.countryFlag = countryFlag;
	}

	public Double getCountrySpecialRate() {
		return this.countrySpecialRate;
	}

	public void setCountrySpecialRate(Double countrySpecialRate) {
		this.countrySpecialRate = countrySpecialRate;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Double getCountyBaseChgPct() {
		return this.countyBaseChgPct;
	}

	public void setCountyBaseChgPct(Double countyBaseChgPct) {
		this.countyBaseChgPct = countyBaseChgPct;
	}

	public String getCountyFlag() {
		return this.countyFlag;
	}

	public void setCountyFlag(String countyFlag) {
		this.countyFlag = countyFlag;
	}

	public Double getCountySpecialRate() {
		return this.countySpecialRate;
	}

	public void setCountySpecialRate(Double countySpecialRate) {
		this.countySpecialRate = countySpecialRate;
	}

	public Long getCustLocnId() {
		return this.custLocnId;
	}

	public void setCustLocnId(Long custLocnId) {
		this.custLocnId = custLocnId;
	}
	
	public Long getCustId() {
		return this.custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}
	
	public Long getCustLocnExId() {
		return this.custLocnExId;
	}

	public void setCustLocnExId(Long custLocnExId) {
		this.custLocnExId = custLocnExId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public Date getEffectiveDateThru() {
		return this.effectiveDateThru;
	}

	public void setEffectiveDateThru(Date effectiveDateThru) {
		this.effectiveDateThru = effectiveDateThru;
	}

	public Long getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getExemptReasonCode() {
		return this.exemptReasonCode;
	}

	public void setExemptReasonCode(String exemptReasonCode) {
		this.exemptReasonCode = exemptReasonCode;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getGeocode() {
		return this.geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public String getInvoiceNbr() {
		return this.invoiceNbr;
	}

	public void setInvoiceNbr(String invoiceNbr) {
		this.invoiceNbr = invoiceNbr;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Double getStateBaseChgPct() {
		return this.stateBaseChgPct;
	}

	public void setStateBaseChgPct(Double stateBaseChgPct) {
		this.stateBaseChgPct = stateBaseChgPct;
	}

	public String getStateFlag() {
		return this.stateFlag;
	}

	public void setStateFlag(String stateFlag) {
		this.stateFlag = stateFlag;
	}

	public Double getStateSpecialRate() {
		return this.stateSpecialRate;
	}

	public void setStateSpecialRate(Double stateSpecialRate) {
		this.stateSpecialRate = stateSpecialRate;
	}

	public Double getStj1BaseChgPct() {
		return this.stj1BaseChgPct;
	}

	public void setStj1BaseChgPct(Double stj1BaseChgPct) {
		this.stj1BaseChgPct = stj1BaseChgPct;
	}

	public String getStj1Flag() {
		return this.stj1Flag;
	}

	public void setStj1Flag(String stj1Flag) {
		this.stj1Flag = stj1Flag;
	}

	public Double getStj1SpecialRate() {
		return this.stj1SpecialRate;
	}

	public void setStj1SpecialRate(Double stj1SpecialRate) {
		this.stj1SpecialRate = stj1SpecialRate;
	}

	public Double getStj2BaseChgPct() {
		return this.stj2BaseChgPct;
	}

	public void setStj2BaseChgPct(Double stj2BaseChgPct) {
		this.stj2BaseChgPct = stj2BaseChgPct;
	}

	public String getStj2Flag() {
		return this.stj2Flag;
	}

	public void setStj2Flag(String stj2Flag) {
		this.stj2Flag = stj2Flag;
	}

	public Double getStj2SpecialRate() {
		return this.stj2SpecialRate;
	}

	public void setStj2SpecialRate(Double stj2SpecialRate) {
		this.stj2SpecialRate = stj2SpecialRate;
	}

	public Double getStj3BaseChgPct() {
		return this.stj3BaseChgPct;
	}

	public void setStj3BaseChgPct(Double stj3BaseChgPct) {
		this.stj3BaseChgPct = stj3BaseChgPct;
	}

	public String getStj3Flag() {
		return this.stj3Flag;
	}

	public void setStj3Flag(String stj3Flag) {
		this.stj3Flag = stj3Flag;
	}

	public Double getStj3SpecialRate() {
		return this.stj3SpecialRate;
	}

	public void setStj3SpecialRate(Double stj3SpecialRate) {
		this.stj3SpecialRate = stj3SpecialRate;
	}

	public Double getStj4BaseChgPct() {
		return this.stj4BaseChgPct;
	}

	public void setStj4BaseChgPct(Double stj4BaseChgPct) {
		this.stj4BaseChgPct = stj4BaseChgPct;
	}

	public String getStj4Flag() {
		return this.stj4Flag;
	}

	public void setStj4Flag(String stj4Flag) {
		this.stj4Flag = stj4Flag;
	}

	public Double getStj4SpecialRate() {
		return this.stj4SpecialRate;
	}

	public void setStj4SpecialRate(Double stj4SpecialRate) {
		this.stj4SpecialRate = stj4SpecialRate;
	}

	public Double getStj5BaseChgPct() {
		return this.stj5BaseChgPct;
	}

	public void setStj5BaseChgPct(Double stj5BaseChgPct) {
		this.stj5BaseChgPct = stj5BaseChgPct;
	}

	public String getStj5Flag() {
		return this.stj5Flag;
	}

	public void setStj5Flag(String stj5Flag) {
		this.stj5Flag = stj5Flag;
	}

	public Double getStj5SpecialRate() {
		return this.stj5SpecialRate;
	}

	public void setStj5SpecialRate(Double stj5SpecialRate) {
		this.stj5SpecialRate = stj5SpecialRate;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getZipplus4() {
		return this.zipplus4;
	}

	public void setZipplus4(String zipplus4) {
		this.zipplus4 = zipplus4;
	}
	
	public String getCustNbr() {
		return this.custNbr;
	}

	public void setCustNbr(String custNbr) {
		this.custNbr = custNbr;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustLocnCode() {
		return this.custLocnCode;
	}

	public void setCustLocnCode(String custLocnCode) {
		this.custLocnCode = custLocnCode;
	}

	public String getCustLocnName() {
		return this.custLocnName;
	}

	public void setCustLocnName(String custLocnName) {
		this.custLocnName = custLocnName;
	}

	public String getEntityCode() {
        return this.entityCode;
    }
    
    public void setEntityCode(String entityCode) {
    	this.entityCode = entityCode;
    }

    public String getEntityName() {
        return this.entityName;
    }
    
    public void setEntityName(String entityName) {
    	this.entityName = entityName;
    }

    public String getExemptionTypeCode() {
		return this.exemptionTypeCode;
	}

	public void setExemptionTypeCode(String exemptionTypeCode) {
		this.exemptionTypeCode = exemptionTypeCode;
	}
	
	public String getCustActiveFlag() {
		return this.custActiveFlag;
	}

	public void setCustActiveFlag(String custActiveFlag) {
		this.custActiveFlag = custActiveFlag;
	}

	public String getCustLocnActiveFlag() {
		return this.custLocnActiveFlag;
	}

	public void setCustLocnActiveFlag(String custLocnActiveFlag) {
		this.custLocnActiveFlag = custLocnActiveFlag;
	}
	
	public String getCustLocnCountry() {
		return this.custLocnCountry;
	}

	public void setCustLocnCountry(String custLocnCountry) {
		this.custLocnCountry = custLocnCountry;
	}
	
	public String getCustLocnState() {
		return this.custLocnState;
	}

	public void setCustLocnState(String custLocnState) {
		this.custLocnState = custLocnState;
	}
	
	public String getCustLocnCity() {
		return this.custLocnCity;
	}

	public void setCustLocnCity(String custLocnCity) {
		this.custLocnCity = custLocnCity;
	}
	
	public Boolean getStj1BooleanFlag() {
    	return "1".equals(this.stj1Flag);
	}

	public void setStj1BooleanFlag(Boolean stj1Flag) {
		this.stj1Flag = stj1Flag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getStj2BooleanFlag() {
    	return "1".equals(this.stj2Flag);
	}

	public void setStj2BooleanFlag(Boolean stj2Flag) {
		this.stj2Flag = stj2Flag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getStj3BooleanFlag() {
    	return "1".equals(this.stj3Flag);
	}

	public void setStj3BooleanFlag(Boolean stj3Flag) {
		this.stj3Flag = stj3Flag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getStj4BooleanFlag() {
    	return "1".equals(this.stj4Flag);
	}

	public void setStj4BooleanFlag(Boolean stj4Flag) {
		this.stj4Flag = stj4Flag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getStj5BooleanFlag() {
    	return "1".equals(this.stj5Flag);
	}


	public void setStj5BooleanFlag(Boolean stj5Flag) {
		this.stj5Flag = stj5Flag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getCountryBooleanFlag() {
    	return "1".equals(this.countryFlag);
	}

	public void setCountryBooleanFlag(Boolean countryFlag) {
		this.countryFlag = countryFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getStateBooleanFlag() {
    	return "1".equals(this.stateFlag);
	}

	public void setStateBooleanFlag(Boolean stateFlag) {
		this.stateFlag = stateFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getCountyBooleanFlag() {
    	return "1".equals(this.countyFlag);
	}

	public void setCountyBooleanFlag(Boolean countyFlag) {
		this.countyFlag = countyFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getCityBooleanFlag() {
    	return "1".equals(this.cityFlag);
	}

	public void setCityBooleanFlag(Boolean cityFlag) {
		this.cityFlag = cityFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getApplyToStateBooleanFlag() {
    	return "1".equals(this.applyToStateFlag);
	}

	public void setApplyToStateBooleanFlag(Boolean applyToStateFlag) {
		this.applyToStateFlag = applyToStateFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Boolean getBlanketBooleanFlag() {
    	return "1".equals(this.blanketFlag);
	}

	public void setBlanketBooleanFlag(Boolean blanketFlag) {
		this.blanketFlag = blanketFlag == Boolean.TRUE ? "1" : "0";
	}
}