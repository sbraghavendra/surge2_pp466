package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TB_NEXUS_DEF")
public class NexusDefinition extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="NEXUS_DEF_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="nexus_definition_sequence")
	@SequenceGenerator(name="nexus_definition_sequence" , allocationSize = 1, sequenceName="sq_tb_nexus_def_id")
	private Long nexusDefId;
	
	@Column(name="ENTITY_ID")
	private Long entityId;
	
	@Column(name="NEXUS_COUNTRY_CODE")
	private String nexusCountryCode;
	
	@Column(name="NEXUS_STATE_CODE")
	private String nexusStateCode;
	
	@Column(name="NEXUS_COUNTY")
	private String nexusCounty;
	
	@Column(name="NEXUS_CITY")
	private String nexusCity;
	
	@Column(name="NEXUS_STJ")
	private String nexusStj;
	
	@Column(name="NEXUS_FLAG")
	private String nexusFlag;
	
	@Column(name="AUTO_USER_ID")
	private String autoUserId;
	
	@Transient
	private Boolean newItemFlag = new Boolean(false);
	
	public Boolean getNewItemFlag() {
        return this.newItemFlag;
    }
    
    public void setNewItemFlag(Boolean newItemFlag) {
    	this.newItemFlag = newItemFlag;
    }

	public Long getNexusDefId() {
		return nexusDefId;
	}

	public void setNexusDefId(Long nexusDefId) {
		this.nexusDefId = nexusDefId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getNexusCountryCode() {
		return nexusCountryCode;
	}

	public void setNexusCountryCode(String nexusCountryCode) {
		this.nexusCountryCode = nexusCountryCode;
	}

	public String getNexusStateCode() {
		return nexusStateCode;
	}

	public void setNexusStateCode(String nexusStateCode) {
		this.nexusStateCode = nexusStateCode;
	}

	public String getNexusCounty() {
		return nexusCounty;
	}

	public void setNexusCounty(String nexusCounty) {
		this.nexusCounty = nexusCounty;
	}

	public String getNexusCity() {
		return nexusCity;
	}

	public void setNexusCity(String nexusCity) {
		this.nexusCity = nexusCity;
	}

	public String getNexusStj() {
		return nexusStj;
	}

	public void setNexusStj(String nexusStj) {
		this.nexusStj = nexusStj;
	}

	public String getNexusFlag() {
		return nexusFlag;
	}

	public void setNexusFlag(String nexusFlag) {
		this.nexusFlag = nexusFlag;
	}
	
	public Boolean getNexusBooleanFlag() {
		return "1".equals(nexusFlag);
	}
	
	public void setNexusBooleanFlag(Boolean nexusFlag) {
		this.nexusFlag = nexusFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return this.nexusDefId;
	}

	@Override
	public void setId(Long id) {
		this.nexusDefId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "nexusDefId";
	}

	public String getAutoUserId() {
		return autoUserId;
	}

	public void setAutoUserId(String autoUserId) {
		this.autoUserId = autoUserId;
	}
}
