package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * @author Jeesmon Jacob
 *
 */

@Embeddable
public class TaxAllocationMatrixDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	--- @GeneratedValue work only with @Id. So it will be set in the Dao on save and update
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="tax_allocation_detail_sequence")
	@SequenceGenerator(name="tax_allocation_detail_sequence" , allocationSize = 1, sequenceName="SQ_TB_TAX_ALLOC_MATRIX_DTL_ID")
	*/
	@Column(name="TAX_ALLOC_MATRIX_DTL_ID")
	private Long taxAllocationMatrixDetailId;
	
//	@Column(name="TAX_ALLOC_MATRIX_ID")
//	private Long taxAllocationMatrixId;
	
	@Column(name="TAXCODE_CODE")  	
	private String taxcodeCode;	
    
    @Column(name="COMMENTS")
    private String comments;
    
    @Column(name="ALLOCATION_PERCENT", nullable = false, scale = 7)
	private BigDecimal allocationPercent;
    
    @Transient
	private Long instanceCreatedId; //bug 5090
    
    @Transient
    private BigDecimal glLineItmDistAmt;
    
    @Transient
    private BigDecimal invoiceFreightAmt;
    
    @Transient
    private BigDecimal invoiceDiscountAmt;
    
    @Transient
	private String description;
	
	private static long nextId = 0L;
	
	public static long getNextId() {
		if(nextId >= 1000000L){nextId = 0L;}
		nextId++;
		return nextId;
	}
	
	public TaxAllocationMatrixDetail() {
		instanceCreatedId = new Long(getNextId());
	}

	public Long getTaxAllocationMatrixDetailId() {
		return taxAllocationMatrixDetailId;
	}

	public void setTaxAllocationMatrixDetailId(Long taxAllocationMatrixDetailId) {
		this.taxAllocationMatrixDetailId = taxAllocationMatrixDetailId;
	}
	
//	public Long getTaxAllocationMatrixId() {
//		return taxAllocationMatrixId;
//	}

//	public void setTaxAllocationMatrixId(Long taxAllocationMatrixId) {
//		this.taxAllocationMatrixId = taxAllocationMatrixId;
//	}

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public BigDecimal getAllocationPercent() {
		return allocationPercent;
	}

	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}

	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}
	
	public BigDecimal getGlLineItmDistAmt() {
		return glLineItmDistAmt;
	}

	public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt) {
		this.glLineItmDistAmt = glLineItmDistAmt;
	}
	
	public BigDecimal getInvoiceFreightAmt() {
		return invoiceFreightAmt;
	}

	public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt) {
		this.invoiceFreightAmt = invoiceFreightAmt;
	}

	public BigDecimal getInvoiceDiscountAmt() {
		return invoiceDiscountAmt;
	}

	public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt) {
		this.invoiceDiscountAmt = invoiceDiscountAmt;
	}
}
