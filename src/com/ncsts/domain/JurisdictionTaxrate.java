package com.ncsts.domain;
// default package

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import com.seconddecimal.billing.util.ArithmeticUtils;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_JURISDICTION_TAXRATE")
public class JurisdictionTaxrate extends Auditable implements java.io.Serializable, Idable<Long> {
	
	private static final long serialVersionUID = 1L;
	
	public static final String MAX_STRING = "*MAX";
	public static final long MAX_AMOUNT = 999999999;
	public static final String NEW_JURISDICTION_TAXRATE_ID_STRING = "*New";

	public static final int JURISDICTIONTAXRATEID_SIZE = 31;
	public static final int JURISDICTIONID_SIZE = 31;
	public static final int RATETYPECODE_SIZE = 40;
	public static final int EFFECTIVEDATE_SIZE = 19;
	public static final int EXPIRATIONDATE_SIZE = 19;
	public static final int CUSTOMFLAG_SIZE = 1;
	public static final int MODIFIEDFLAG_SIZE = 1;
	public static final int STATESALESRATE_SIZE = 31;
	public static final int STATEUSERATE_SIZE = 31;
	public static final int STATEUSETIER2RATE_SIZE = 31;
	public static final int STATEUSETIER3RATE_SIZE = 31;
	public static final int STATESALESTIER2RATE_SIZE = 31;
	public static final int STATESALESTIER3RATE_SIZE = 31;
	public static final int STATESPLITAMOUNT_SIZE = 31;
	public static final int STATETIER2MINAMOUNT_SIZE = 31;
	public static final int STATETIER2MAXAMOUNT_SIZE = 31;
	public static final int STATEMAXTAXAMOUNT_SIZE = 31;
	public static final int COUNTYSALESRATE_SIZE = 31;
	public static final int COUNTYUSERATE_SIZE = 31;
	public static final int COUNTYSPLITAMOUNT_SIZE = 31;
	public static final int COUNTYMAXTAXAMOUNT_SIZE = 31;
	public static final int COUNTYSINGLEFLAG_SIZE = 1;
	public static final int COUNTYDEFAULTFLAG_SIZE = 1;
	public static final int CITYSALESRATE_SIZE = 31;
	public static final int CITYUSERATE_SIZE = 31;
	public static final int CITYSPLITAMOUNT_SIZE = 31;
//	public static final int CITYSPLITSALESRATE_SIZE = 31;
	public static final int CITYSPLITUSERATE_SIZE = 31;
	public static final int CITYSINGLEFLAG_SIZE = 1;
	public static final int CITYDEFAULTFLAG_SIZE = 1;
	
	public static final int STJ1USERATE_SIZE = 31;
	public static final int STJ2USERATE_SIZE = 31;
	public static final int STJ3USERATE_SIZE = 31;
	public static final int STJ4USERATE_SIZE = 31;
	public static final int STJ5USERATE_SIZE = 31;
	
	public static final int STJ1SALESRATE_SIZE = 31;
	public static final int STJ2SALESRATE_SIZE = 31;
	public static final int STJ3SALESRATE_SIZE = 31;
	public static final int STJ4SALESRATE_SIZE = 31;
	public static final int STJ5SALESRATE_SIZE = 31;
	
	//COUNTRY
	public static final int COUNTRYSALESTIER1RATE = 31;
    public static final int COUNTRYSALESTIER1SETAMT = 31;
    public static final int COUNTRYSALESTIER1MAXAMT = 31;
    public static final int COUNTRYSALESTIER2MINAMT = 31;
    public static final int COUNTRYSALESTIER2RATE = 31;
    public static final int COUNTRYSALESTIER2SETAMT = 31;
    public static final int COUNTRYSALESTIER2MAXAMT = 31;
    public static final int COUNTRYSALESTIER2ENTAMFLAG = 31;
    public static final int COUNTRYSALESTIER3RATE = 31;
    public static final int COUNTRYSALESTIER3SETAMT = 31;
    public static final int COUNTRYSALESTIER3ENTAMFLAG = 31;  
    public static final int COUNTRYSALESMAXTAXAMT = 31;
    
    public static final int COUNTRYUSETIER1RATE = 31;
    public static final int COUNTRYUSETIER1SETAMT = 31;
    public static final int COUNTRYUSETIER1MAXAMT = 31;
    public static final int COUNTRYUSETIER2MINAMT = 31;
    public static final int COUNTRYUSETIER2RATE = 31;
    public static final int COUNTRYUSETIER2SETAMT = 31;
    public static final int COUNTRYUSETIER2MAXAMT = 31;
    public static final int COUNTRYUSETIER2ENTAMFLAG = 31;
    public static final int COUNTRYUSETIER3RATE = 31;
    public static final int COUNTRYUSETIER3SETAMT = 31;
    public static final int COUNTRYUSETIER3ENTAMFLAG = 31;
    public static final int COUNTRYUSEMAXTAXAMT = 31;
    
    //STATE
  	public static final int STATESALESTIER1RATE = 31;
    public static final int STATESALESTIER1SETAMT = 31;
    public static final int STATESALESTIER1MAXAMT = 31;
    public static final int STATESALESTIER2MINAMT = 31;
    public static final int STATESALESTIER2RATE = 31;
    public static final int STATESALESTIER2SETAMT = 31;
    public static final int STATESALESTIER2MAXAMT = 31;
    public static final int STATESALESTIER2ENTAMFLAG = 31;
    public static final int STATESALESTIER3RATE = 31;
    public static final int STATESALESTIER3SETAMT = 31;
    public static final int STATESALESTIER3ENTAMFLAG = 31;
    public static final int STATESALESMAXTAXAMT = 31;
      
    public static final int STATEUSETIER1RATE = 31;
    public static final int STATEUSETIER1SETAMT = 31;
    public static final int STATEUSETIER1MAXAMT = 31;
    public static final int STATEUSETIER2MINAMT = 31;
    public static final int STATEUSETIER2RATE = 31;
    public static final int STATEUSETIER2SETAMT = 31;
    public static final int STATEUSETIER2MAXAMT = 31;
    public static final int STATEUSETIER2ENTAMFLAG = 31;
    public static final int STATEUSETIER3RATE = 31;
    public static final int STATEUSETIER3SETAMT = 31;
    public static final int STATEUSETIER3ENTAMFLAG = 31;
    public static final int STATEUSEMAXTAXAMT = 31;
    
    //COUNTY
  	public static final int COUNTYSALESTIER1RATE = 31;
    public static final int COUNTYSALESTIER1SETAMT = 31;
    public static final int COUNTYSALESTIER1MAXAMT = 31;
    public static final int COUNTYSALESTIER2MINAMT = 31;
    public static final int COUNTYSALESTIER2RATE = 31;
    public static final int COUNTYSALESTIER2SETAMT = 31;
    public static final int COUNTYSALESTIER2MAXAMT = 31;
    public static final int COUNTYSALESTIER2ENTAMFLAG = 31;
    public static final int COUNTYSALESTIER3RATE = 31;
    public static final int COUNTYSALESTIER3SETAMT = 31;
    public static final int COUNTYSALESTIER3ENTAMFLAG = 31;
    public static final int COUNTYSALESMAXTAXAMT = 31;
      
    public static final int COUNTYUSETIER1RATE = 31;
    public static final int COUNTYUSETIER1SETAMT = 31;
    public static final int COUNTYUSETIER1MAXAMT = 31;
    public static final int COUNTYUSETIER2MINAMT = 31;
    public static final int COUNTYUSETIER2RATE = 31;
    public static final int COUNTYUSETIER2SETAMT = 31;
    public static final int COUNTYUSETIER2MAXAMT = 31;
    public static final int COUNTYUSETIER2ENTAMFLAG = 31;
    public static final int COUNTYUSETIER3RATE = 31;
    public static final int COUNTYUSETIER3SETAMT = 31;
    public static final int COUNTYUSETIER3ENTAMFLAG = 31;
    public static final int COUNTYUSEMAXTAXAMT = 31;
    
    //CITY
  	public static final int CITYSALESTIER1RATE = 31;
    public static final int CITYSALESTIER1SETAMT = 31;
    public static final int CITYSALESTIER1MAXAMT = 31;
    public static final int CITYSALESTIER2MINAMT = 31;
    public static final int CITYSALESTIER2RATE = 31;
    public static final int CITYSALESTIER2SETAMT = 31;
    public static final int CITYSALESTIER2MAXAMT = 31;
    public static final int CITYSALESTIER2ENTAMFLAG = 31;
    public static final int CITYSALESTIER3RATE = 31;
    public static final int CITYSALESTIER3SETAMT = 31;
    public static final int CITYSALESTIER3ENTAMFLAG = 31;
    public static final int CITYSALESMAXTAXAMT = 31;
      
    public static final int CITYUSETIER1RATE = 31;
    public static final int CITYUSETIER1SETAMT = 31;
    public static final int CITYUSETIER1MAXAMT = 31;
    public static final int CITYUSETIER2MINAMT = 31;
    public static final int CITYUSETIER2RATE = 31;
    public static final int CITYUSETIER2SETAMT = 31;
    public static final int CITYUSETIER2MAXAMT = 31;
    public static final int CITYUSETIER2ENTAMFLAG = 31;
    public static final int CITYUSETIER3RATE = 31;
    public static final int CITYUSETIER3SETAMT = 31;
    public static final int CITYUSETIER3ENTAMFLAG = 31;
    public static final int CITYUSEMAXTAXAMT = 31;
    

	// Fields    
	@Id
	@Column(name="JURISDICTION_TAXRATE_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="jurisdiction_tr_sequence")
	@SequenceGenerator(name="jurisdiction_tr_sequence" , allocationSize = 1, sequenceName="sq_tb_jurisdiction_taxrate_id")	
	private Long jurisdictionTaxrateId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "JURISDICTION_ID", nullable = false)
	private Jurisdiction jurisdiction;

	@Column(name="RATETYPE_CODE")
	private String ratetypeCode;

	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

	@Column(name="CUSTOM_FLAG")
	private String customFlag;

	@Column(name="MODIFIED_FLAG")
	private String modifiedFlag;

	@Column(name="COUNTRY_SALES_TIER1_RATE")
	private BigDecimal countrySalesTier1Rate;

	@Column(name="COUNTRY_SALES_TIER2_RATE")
	private BigDecimal countrySalesTier2Rate;

	@Column(name="COUNTRY_SALES_TIER3_RATE")
	private BigDecimal countrySalesTier3Rate;

	@Column(name="COUNTRY_SALES_TIER1_SETAMT")
	private BigDecimal countrySalesTier1Setamt;

	@Column(name="COUNTRY_SALES_TIER2_SETAMT")
	private BigDecimal countrySalesTier2Setamt;

	@Column(name="COUNTRY_SALES_TIER3_SETAMT")
	private BigDecimal countrySalesTier3Setamt;

	@Column(name="COUNTRY_SALES_TIER1_MAX_AMT")
	private BigDecimal countrySalesTier1MaxAmt;

	@Column(name="COUNTRY_SALES_TIER2_MIN_AMT")
	private BigDecimal countrySalesTier2MinAmt;

	@Column(name="COUNTRY_SALES_TIER2_MAX_AMT")
	private BigDecimal countrySalesTier2MaxAmt;

	@Column(name="COUNTRY_SALES_TIER2_ENTAM_FLAG")
	private String countrySalesTier2EntamFlag;

	@Column(name="COUNTRY_SALES_TIER3_ENTAM_FLAG")
	private String countrySalesTier3EntamFlag;

	@Column(name="COUNTRY_SALES_MAXTAX_AMT")
	private BigDecimal countrySalesMaxtaxAmt;

	@Column(name="COUNTRY_USE_TIER1_RATE")
	private BigDecimal countryUseTier1Rate;

	@Column(name="COUNTRY_USE_TIER2_RATE")
	private BigDecimal countryUseTier2Rate;

	@Column(name="COUNTRY_USE_TIER3_RATE")
	private BigDecimal countryUseTier3Rate;

	@Column(name="COUNTRY_USE_TIER1_SETAMT")
	private BigDecimal countryUseTier1Setamt;

	@Column(name="COUNTRY_USE_TIER2_SETAMT")
	private BigDecimal countryUseTier2Setamt;

	@Column(name="COUNTRY_USE_TIER3_SETAMT")
	private BigDecimal countryUseTier3Setamt;

	@Column(name="COUNTRY_USE_TIER1_MAX_AMT")
	private BigDecimal countryUseTier1MaxAmt;

	@Column(name="COUNTRY_USE_TIER2_MIN_AMT")
	private BigDecimal countryUseTier2MinAmt;

	@Column(name="COUNTRY_USE_TIER2_MAX_AMT")
	private BigDecimal countryUseTier2MaxAmt;

	@Column(name="COUNTRY_USE_TIER2_ENTAM_FLAG")
	private String countryUseTier2EntamFlag;

	@Column(name="COUNTRY_USE_TIER3_ENTAM_FLAG")
	private String countryUseTier3EntamFlag;

	@Column(name="COUNTRY_USE_MAXTAX_AMT")
	private BigDecimal countryUseMaxtaxAmt;

	@Column(name="STATE_SALES_TIER1_RATE")
	private BigDecimal stateSalesTier1Rate;

	@Column(name="STATE_SALES_TIER2_RATE")
	private BigDecimal stateSalesTier2Rate;

	@Column(name="STATE_SALES_TIER3_RATE")
	private BigDecimal stateSalesTier3Rate;

	@Column(name="STATE_SALES_TIER1_SETAMT")
	private BigDecimal stateSalesTier1Setamt;

	@Column(name="STATE_SALES_TIER2_SETAMT")
	private BigDecimal stateSalesTier2Setamt;

	@Column(name="STATE_SALES_TIER3_SETAMT")
	private BigDecimal stateSalesTier3Setamt;

	@Column(name="STATE_SALES_TIER1_MAX_AMT")
	private BigDecimal stateSalesTier1MaxAmt;

	@Column(name="STATE_SALES_TIER2_MIN_AMT")
	private BigDecimal stateSalesTier2MinAmt;

	@Column(name="STATE_SALES_TIER2_MAX_AMT")
	private BigDecimal stateSalesTier2MaxAmt;

	@Column(name="STATE_SALES_TIER2_ENTAM_FLAG")
	private String stateSalesTier2EntamFlag;

	@Column(name="STATE_SALES_TIER3_ENTAM_FLAG")
	private String stateSalesTier3EntamFlag;

	@Column(name="STATE_SALES_MAXTAX_AMT")
	private BigDecimal stateSalesMaxtaxAmt;

	@Column(name="STATE_USE_TIER1_RATE")
	private BigDecimal stateUseTier1Rate;

	@Column(name="STATE_USE_TIER2_RATE")
	private BigDecimal stateUseTier2Rate;

	@Column(name="STATE_USE_TIER3_RATE")
	private BigDecimal stateUseTier3Rate;

	@Column(name="STATE_USE_TIER1_SETAMT")
	private BigDecimal stateUseTier1Setamt;

	@Column(name="STATE_USE_TIER2_SETAMT")
	private BigDecimal stateUseTier2Setamt;

	@Column(name="STATE_USE_TIER3_SETAMT")
	private BigDecimal stateUseTier3Setamt;

	@Column(name="STATE_USE_TIER1_MAX_AMT")
	private BigDecimal stateUseTier1MaxAmt;

	@Column(name="STATE_USE_TIER2_MIN_AMT")
	private BigDecimal stateUseTier2MinAmt;

	@Column(name="STATE_USE_TIER2_MAX_AMT")
	private BigDecimal stateUseTier2MaxAmt;

	@Column(name="STATE_USE_TIER2_ENTAM_FLAG")
	private String stateUseTier2EntamFlag;

	@Column(name="STATE_USE_TIER3_ENTAM_FLAG")
	private String stateUseTier3EntamFlag;

	@Column(name="STATE_USE_MAXTAX_AMT")
	private BigDecimal stateUseMaxtaxAmt;

	@Column(name="COUNTY_SALES_TIER1_RATE")
	private BigDecimal countySalesTier1Rate;

	@Column(name="COUNTY_SALES_TIER2_RATE")
	private BigDecimal countySalesTier2Rate;

	@Column(name="COUNTY_SALES_TIER3_RATE")
	private BigDecimal countySalesTier3Rate;

	@Column(name="COUNTY_SALES_TIER1_SETAMT")
	private BigDecimal countySalesTier1Setamt;

	@Column(name="COUNTY_SALES_TIER2_SETAMT")
	private BigDecimal countySalesTier2Setamt;

	@Column(name="COUNTY_SALES_TIER3_SETAMT")
	private BigDecimal countySalesTier3Setamt;

	@Column(name="COUNTY_SALES_TIER1_MAX_AMT")
	private BigDecimal countySalesTier1MaxAmt;

	@Column(name="COUNTY_SALES_TIER2_MIN_AMT")
	private BigDecimal countySalesTier2MinAmt;

	@Column(name="COUNTY_SALES_TIER2_MAX_AMT")
	private BigDecimal countySalesTier2MaxAmt;

	@Column(name="COUNTY_SALES_TIER2_ENTAM_FLAG")
	private String countySalesTier2EntamFlag;

	@Column(name="COUNTY_SALES_TIER3_ENTAM_FLAG")
	private String countySalesTier3EntamFlag;

	@Column(name="COUNTY_SALES_MAXTAX_AMT")
	private BigDecimal countySalesMaxtaxAmt;

	@Column(name="COUNTY_USE_TIER1_RATE")
	private BigDecimal countyUseTier1Rate;

	@Column(name="COUNTY_USE_TIER2_RATE")
	private BigDecimal countyUseTier2Rate;

	@Column(name="COUNTY_USE_TIER3_RATE")
	private BigDecimal countyUseTier3Rate;

	@Column(name="COUNTY_USE_TIER1_SETAMT")
	private BigDecimal countyUseTier1Setamt;

	@Column(name="COUNTY_USE_TIER2_SETAMT")
	private BigDecimal countyUseTier2Setamt;

	@Column(name="COUNTY_USE_TIER3_SETAMT")
	private BigDecimal countyUseTier3Setamt;

	@Column(name="COUNTY_USE_TIER1_MAX_AMT")
	private BigDecimal countyUseTier1MaxAmt;

	@Column(name="COUNTY_USE_TIER2_MIN_AMT")
	private BigDecimal countyUseTier2MinAmt;

	@Column(name="COUNTY_USE_TIER2_MAX_AMT")
	private BigDecimal countyUseTier2MaxAmt;

	@Column(name="COUNTY_USE_TIER2_ENTAM_FLAG")
	private String countyUseTier2EntamFlag;

	@Column(name="COUNTY_USE_TIER3_ENTAM_FLAG")
	private String countyUseTier3EntamFlag;

	@Column(name="COUNTY_USE_MAXTAX_AMT")
	private BigDecimal countyUseMaxtaxAmt;

	@Column(name="CITY_SALES_TIER1_RATE")
	private BigDecimal citySalesTier1Rate;

	@Column(name="CITY_SALES_TIER2_RATE")
	private BigDecimal citySalesTier2Rate;

	@Column(name="CITY_SALES_TIER3_RATE")
	private BigDecimal citySalesTier3Rate;

	@Column(name="CITY_SALES_TIER1_SETAMT")
	private BigDecimal citySalesTier1Setamt;

	@Column(name="CITY_SALES_TIER2_SETAMT")
	private BigDecimal citySalesTier2Setamt;

	@Column(name="CITY_SALES_TIER3_SETAMT")
	private BigDecimal citySalesTier3Setamt;

	@Column(name="CITY_SALES_TIER1_MAX_AMT")
	private BigDecimal citySalesTier1MaxAmt;

	@Column(name="CITY_SALES_TIER2_MIN_AMT")
	private BigDecimal citySalesTier2MinAmt;

	@Column(name="CITY_SALES_TIER2_MAX_AMT")
	private BigDecimal citySalesTier2MaxAmt;

	@Column(name="CITY_SALES_TIER2_ENTAM_FLAG")
	private String citySalesTier2EntamFlag;

	@Column(name="CITY_SALES_TIER3_ENTAM_FLAG")
	private String citySalesTier3EntamFlag;

	@Column(name="CITY_SALES_MAXTAX_AMT")
	private BigDecimal citySalesMaxtaxAmt;

	@Column(name="CITY_USE_TIER1_RATE")
	private BigDecimal cityUseTier1Rate;

	@Column(name="CITY_USE_TIER2_RATE")
	private BigDecimal cityUseTier2Rate;

	@Column(name="CITY_USE_TIER3_RATE")
	private BigDecimal cityUseTier3Rate;

	@Column(name="CITY_USE_TIER1_SETAMT")
	private BigDecimal cityUseTier1Setamt;

	@Column(name="CITY_USE_TIER2_SETAMT")
	private BigDecimal cityUseTier2Setamt;

	@Column(name="CITY_USE_TIER3_SETAMT")
	private BigDecimal cityUseTier3Setamt;

	@Column(name="CITY_USE_TIER1_MAX_AMT")
	private BigDecimal cityUseTier1MaxAmt;

	@Column(name="CITY_USE_TIER2_MIN_AMT")
	private BigDecimal cityUseTier2MinAmt;

	@Column(name="CITY_USE_TIER2_MAX_AMT")
	private BigDecimal cityUseTier2MaxAmt;

	@Column(name="CITY_USE_TIER2_ENTAM_FLAG")
	private String cityUseTier2EntamFlag;

	@Column(name="CITY_USE_TIER3_ENTAM_FLAG")
	private String cityUseTier3EntamFlag;

	@Column(name="CITY_USE_MAXTAX_AMT")
	private BigDecimal cityUseMaxtaxAmt;

	@Column(name="STJ1_SALES_RATE")
	private BigDecimal stj1SalesRate;

	@Column(name="STJ1_SALES_SETAMT")
	private BigDecimal stj1SalesSetamt;

	@Column(name="STJ2_SALES_RATE")
	private BigDecimal stj2SalesRate;

	@Column(name="STJ2_SALES_SETAMT")
	private BigDecimal stj2SalesSetamt;

	@Column(name="STJ3_SALES_RATE")
	private BigDecimal stj3SalesRate;

	@Column(name="STJ3_SALES_SETAMT")
	private BigDecimal stj3SalesSetamt;

	@Column(name="STJ4_SALES_RATE")
	private BigDecimal stj4SalesRate;

	@Column(name="STJ4_SALES_SETAMT")
	private BigDecimal stj4SalesSetamt;

	@Column(name="STJ5_SALES_RATE")
	private BigDecimal stj5SalesRate;

	@Column(name="STJ5_SALES_SETAMT")
	private BigDecimal stj5SalesSetamt;

	@Column(name="STJ1_USE_RATE")
	private BigDecimal stj1UseRate;

	@Column(name="STJ1_USE_SETAMT")
	private BigDecimal stj1UseSetamt;

	@Column(name="STJ2_USE_RATE")
	private BigDecimal stj2UseRate;

	@Column(name="STJ2_USE_SETAMT")
	private BigDecimal stj2UseSetamt;

	@Column(name="STJ3_USE_RATE")
	private BigDecimal stj3UseRate;

	@Column(name="STJ3_USE_SETAMT")
	private BigDecimal stj3UseSetamt;

	@Column(name="STJ4_USE_RATE")
	private BigDecimal stj4UseRate;

	@Column(name="STJ4_USE_SETAMT")
	private BigDecimal stj4UseSetamt;

	@Column(name="STJ5_USE_RATE")
	private BigDecimal stj5UseRate;

	@Column(name="STJ5_USE_SETAMT")
	private BigDecimal stj5UseSetamt;
	
	@Column(name="COUNTRY_USE_SAME_SALES")
	private String countryUseSameSales;
	
	@Column(name="STATE_USE_SAME_SALES")
	private String stateUseSameSales;
	
	@Column(name="COUNTY_USE_SAME_SALES")
	private String countyUseSameSales;
	
	@Column(name="CITY_USE_SAME_SALES")
	private String cityUseSameSales;
	
	@Column(name="STJ_USE_SAME_SALES")
	private String stjUseSameSales;

	
	@Column(name="COMBINED_USE_RATE")
	private BigDecimal combinedUseRate;
	
	@Column(name="COMBINED_SALES_RATE")
	private BigDecimal combinedSalesRate;
	
	// Transient properties
	@Transient
	private String jurisdictionTaxrateIdString;

	// Calculated fields
	@Transient
	Long stateTier1MinAmount;
	@Transient
	String stateSplitAmountString;
	@Transient
	Boolean stateTier1MaxFlag;
	@Transient
	String stateTier2MaxAmountString;
	@Transient
	Boolean stateTier2MaxFlag;
	@Transient
	String stateTier3MaxAmount;

	// Constructors

    /** default constructor */
    public JurisdictionTaxrate() {
    }

	/** minimal constructor */
    public JurisdictionTaxrate(Long jurisdictionTaxrateId) {
        this.jurisdictionTaxrateId = jurisdictionTaxrateId;
    }
    
    // Copy constructor
	public JurisdictionTaxrate(JurisdictionTaxrate jurisdictionTaxrate) {
		if (jurisdictionTaxrate != null) {
			BeanUtils.copyProperties(jurisdictionTaxrate, this);
		}
	}

	// Idable interface
	public Long getId() {
		return jurisdictionTaxrateId;
	}
	
    public void setId(Long id) {
    	setJurisdictionTaxrateId(id);
    }
    
	public String getIdPropertyName() {
		return "jurisdictionTaxrateId";
	}

	public Long getJurisdictionTaxrateId() {
		return jurisdictionTaxrateId;
	}

	public void setJurisdictionTaxrateId(Long jurisdictionTaxrateId) {
		// Empty values get converted to 0, change back to null
		if ((jurisdictionTaxrateId != null) && (jurisdictionTaxrateId <= 0L)) {
			jurisdictionTaxrateId = null;
		}
		this.jurisdictionTaxrateId = jurisdictionTaxrateId;
		this.jurisdictionTaxrateIdString = ((jurisdictionTaxrateId == null) ? null : jurisdictionTaxrateId.toString());
	}

	public String getJurisdictionTaxrateIdString() {
		return jurisdictionTaxrateIdString;
	}

	public void setJurisdictionTaxrateIdString(String jurisdictionTaxrateIdString) {
		if (StringUtils.isBlank(jurisdictionTaxrateIdString)) {
			jurisdictionTaxrateIdString = null;
		} else {
			jurisdictionTaxrateIdString = jurisdictionTaxrateIdString.trim();
		}
		Long l;
		try {
			l = new Long(jurisdictionTaxrateIdString);
		} catch (Exception e) {
			l = null;
		}
		this.jurisdictionTaxrateId = l;
		this.jurisdictionTaxrateIdString = jurisdictionTaxrateIdString;
	}

    public Jurisdiction getJurisdiction() {
    	return jurisdiction;
	}
	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

//	public Long getJurisdictionId() {
//		return jurisdictionId;
//	}
//
//	public void setJurisdictionId(Long jurisdictionId) {
//		this.jurisdictionId = jurisdictionId;
//	}

	public String getRatetypeCode() {
		return ratetypeCode;
	}

	public void setRatetypeCode(String ratetypeCode) {
		this.ratetypeCode = ratetypeCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public Boolean getCustomFlagBoolean() {
		return (customFlag == null) ? null : customFlag.equals("1");
	}

	public void setCustomFlagBoolean(Boolean customFlag) {
		setCustomFlag((customFlag == null) ?  null : ((customFlag) ? "1" : "0"));
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}

	public Boolean getModifiedFlagBoolean() {
		return (modifiedFlag == null) ? null : modifiedFlag.equals("1");
	}

	public void setModifiedFlagBoolean(Boolean modifiedFlag) {
		setModifiedFlag((modifiedFlag == null) ?  null : ((modifiedFlag) ? "1" : "0"));
	}

	

	// Calculated fields
	public Long getStateTier1MinAmount() {
		return stateTier1MinAmount;
	}

	public void setStateTier1MinAmount(Long stateTier1MinAmount) {
		this.stateTier1MinAmount = stateTier1MinAmount;
	}

	public String getStateSplitAmountString() {
		return stateSplitAmountString;
	}

	public void setStateSplitAmountString(String stateSplitAmountString) {
		this.stateSplitAmountString = stateSplitAmountString;
	}

	public Boolean getStateTier1MaxFlag() {
		return stateTier1MaxFlag;
	}

	public void setStateTier1MaxFlag(Boolean stateTier1MaxFlag) {
		this.stateTier1MaxFlag = stateTier1MaxFlag;
	}

	public String getStateTier2MaxAmountString() {
		return stateTier2MaxAmountString;
	}

	public void setStateTier2MaxAmountString(String stateTier2MaxAmountString) {
		this.stateTier2MaxAmountString = stateTier2MaxAmountString;
	}

	public Boolean getStateTier2MaxFlag() {
		return stateTier2MaxFlag;
	}

	public void setStateTier2MaxFlag(Boolean stateTier2MaxFlag) {
		this.stateTier2MaxFlag = stateTier2MaxFlag;
	}

	public String getStateTier3MaxAmount() {
		return stateTier3MaxAmount;
	}

	public void setStateTier3MaxAmount(String stateTier3MaxAmount) {
		this.stateTier3MaxAmount = stateTier3MaxAmount;
	}
	
	public boolean isRateAndAmountZero() {
		BigDecimal userInputRateTotal = ArithmeticUtils.ZERO;	
		BigDecimal userInputAmountTotal = ArithmeticUtils.ZERO;
		
		//country
		if (this.countrySalesTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countrySalesTier1Rate);
		}
		
		if (this.countryUseTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countryUseTier1Rate);
		}
		
		if (this.countrySalesTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countrySalesTier2Rate);
		}
		
		if (this.countryUseTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countryUseTier2Rate);
		}
		
		if (this.countrySalesTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countrySalesTier3Rate);
		}
		
		if (this.countryUseTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countryUseTier3Rate);
		}
		
		//state		
		if (this.stateSalesTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateSalesTier1Rate);
		}
		
		if (this.stateUseTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateUseTier1Rate);
		}
		
		if (this.stateSalesTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateSalesTier2Rate);
		}
		
		if (this.stateUseTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateUseTier2Rate);
		}
		
		if (this.stateSalesTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateSalesTier3Rate);
		}
		
		if (this.stateUseTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stateUseTier3Rate);
		}
		
		//county
		if (this.countySalesTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countySalesTier1Rate);
		}
		
		if (this.countyUseTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countyUseTier1Rate);
		}
		
		if (this.countySalesTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countySalesTier2Rate);
		}
		
		if (this.countyUseTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countyUseTier2Rate);
		}
		
		if (this.countySalesTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countySalesTier3Rate);
		}
		
		if (this.countyUseTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.countyUseTier3Rate);
		}
		
		//city
		if (this.citySalesTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.citySalesTier1Rate);
		}
		
		if (this.cityUseTier1Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.cityUseTier1Rate);
		}
		
		if (this.citySalesTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.citySalesTier2Rate);
		}
		
		if (this.cityUseTier2Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.cityUseTier2Rate);
		}
		
		if (this.citySalesTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.citySalesTier3Rate);
		}
		
		if (this.cityUseTier3Rate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.cityUseTier3Rate);
		}
	
		//stj
		if(this.stj1SalesRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj1SalesRate);
		}
		if(this.stj2SalesRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj2SalesRate);
		}
		if(this.stj3SalesRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj3SalesRate);
		}
		if(this.stj4SalesRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj4SalesRate);
		}
		if(this.stj5SalesRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj5SalesRate);
		}	
		if(this.stj1UseRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj1UseRate);
		}
		if(this.stj2UseRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj2UseRate);
		}
		if(this.stj3UseRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj3UseRate);
		}
		if(this.stj4UseRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj4UseRate);
		}
		if(this.stj5UseRate != null) {
			userInputRateTotal  = ArithmeticUtils.add(userInputRateTotal, this.stj5UseRate);
		}
		
		//country
		if (this.countrySalesTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countrySalesTier1Setamt);
		}
		
		if (this.countryUseTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countryUseTier1Setamt);
		}
		
		if (this.countrySalesTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countrySalesTier2Setamt);
		}
		
		if (this.countryUseTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countryUseTier2Setamt);
		}
		
		if (this.countrySalesTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countrySalesTier3Setamt);
		}
		
		if (this.countryUseTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countryUseTier3Setamt);
		}
		
		//state		
		if (this.stateSalesTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateSalesTier1Setamt);
		}
		
		if (this.stateUseTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateUseTier1Setamt);
		}
		
		if (this.stateSalesTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateSalesTier2Setamt);
		}
		
		if (this.stateUseTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateUseTier2Setamt);
		}
		
		if (this.stateSalesTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateSalesTier3Setamt);
		}
		
		if (this.stateUseTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stateUseTier3Setamt);
		}
		
		//county
		if (this.countySalesTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countySalesTier1Setamt);
		}
		
		if (this.countyUseTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countyUseTier1Setamt);
		}
		
		if (this.countySalesTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countySalesTier2Setamt);
		}
		
		if (this.countyUseTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countyUseTier2Setamt);
		}
		
		if (this.countySalesTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countySalesTier3Setamt);
		}
		
		if (this.countyUseTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.countyUseTier3Setamt);
		}
		
		//city
		if (this.citySalesTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.citySalesTier1Setamt);
		}
		
		if (this.cityUseTier1Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.cityUseTier1Setamt);
		}
		
		if (this.citySalesTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.citySalesTier2Setamt);
		}
		
		if (this.cityUseTier2Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.cityUseTier2Setamt);
		}
		
		if (this.citySalesTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.citySalesTier3Setamt);
		}
		
		if (this.cityUseTier3Setamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.cityUseTier3Setamt);
		}
	
		//stj
		if(this.stj1SalesSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj1SalesSetamt);
		}
		if(this.stj2SalesSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj2SalesSetamt);
		}
		if(this.stj3SalesSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj3SalesSetamt);
		}
		if(this.stj4SalesSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj4SalesSetamt);
		}
		if(this.stj5SalesSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj5SalesSetamt);
		}	
		if(this.stj1UseSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj1UseSetamt);
		}
		if(this.stj2UseSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj2UseSetamt);
		}
		if(this.stj3UseSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj3UseSetamt);
		}
		if(this.stj4UseSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj4UseSetamt);
		}
		if(this.stj5UseSetamt != null) {
			userInputAmountTotal  = ArithmeticUtils.add(userInputAmountTotal, this.stj5UseSetamt);
		}
		
		if (!(userInputRateTotal.doubleValue() > 0.0) && !(userInputAmountTotal.doubleValue() > 0.0)) {
			return true;
		}
		else{
			return false;
		}
	}

	public void init() {
		setJurisdictionTaxrateIdString(NEW_JURISDICTION_TAXRATE_ID_STRING);
		this.jurisdiction = null;
//		this.jurisdictionId = null;
		//this.measureTypeCode = null;
		this.effectiveDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();
		Calendar effDate = Calendar.getInstance();
		effDate.set(9999, Calendar.DECEMBER, 31);
		this.expirationDate = effDate.getTime();
		this.customFlag = "1";
		this.modifiedFlag = "0";
		//this.stateSalesRate = ArithmeticUtils.ZERO;
		//this.stateUseRate = ArithmeticUtils.ZERO;
		this.stateUseTier2Rate = ArithmeticUtils.ZERO;
		this.stateUseTier3Rate = ArithmeticUtils.ZERO;
		//this.stateSplitAmount = 0L;
		//this.stateTier2MinAmount = 0L;
		//this.stateTier2MaxAmount = 0L;
		//this.stateMaxtaxAmount = null;
		//this.countySalesRate = ArithmeticUtils.ZERO;
		//this.countyUseRate = ArithmeticUtils.ZERO;
//		this.countySplitAmount = 0L;
//		this.countyMaxtaxAmount = ArithmeticUtils.ZERO;
//		this.countySingleFlag = "0";
//	this.countyDefaultFlag = "0";
//		this.citySalesRate = ArithmeticUtils.ZERO;
//		this.cityUseRate = ArithmeticUtils.ZERO;
//		this.citySplitAmount = 0L;
//		this.citySplitSalesRate = ArithmeticUtils.ZERO;
//		this.citySplitUseRate = ArithmeticUtils.ZERO;
//		this.citySingleFlag = "0";
//		this.cityDefaultFlag = "0";
		setUpdateUserId(null);
		setUpdateTimestamp(null);

		// Calculated fields
		stateTier1MinAmount = 0L;
		stateSplitAmountString = "0";
		stateTier1MaxFlag = false;
		stateTier2MaxAmountString = "0";
		stateTier2MaxFlag = false;
		stateTier3MaxAmount = MAX_STRING;
	}

	// Fix bugs 4381, 4380,4377, and 3921
	private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.DECIMAL);
	}
	
	public void replaceNullValue() throws Exception {
		Class<?> cls = JurisdictionTaxrate.class;
		Field [] fields = cls.getDeclaredFields();
	
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null && !"JURISDICTION_TAXRATE_ID".equalsIgnoreCase(col.name())){
					if(sqlTypes.get(f.getType()) == Types.DOUBLE){
						String methodName = "set" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1); 
				        Method m = cls.getMethod(methodName, new Class[] {Double.class});
						if (m != null) {
				            m.invoke(this, new Object[] {new Double(0.0)});
						}
					}
					if(sqlTypes.get(f.getType()) == Types.DECIMAL){
						String methodName = "set" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1); 
				        Method m = cls.getMethod(methodName, new Class[] {BigDecimal.class});
						if (m != null) {
				            m.invoke(this, new Object[] {new BigDecimal(0.0)});
						}
					}
					else if(sqlTypes.get(f.getType()) == Types.BIGINT){
						String methodName = "set" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1); 
					    Method m = cls.getMethod(methodName, new Class[] {Long.class});
						if (m != null) {
				            m.invoke(this, new Object[] {new Long(0L)});
						}		
					}
				}
			}
		}
	}
	
	public void setCountrySalesTier1Rate(BigDecimal countrySalesTier1Rate) {
	    this.countrySalesTier1Rate = countrySalesTier1Rate;
	}

	public BigDecimal getCountrySalesTier1Rate() {
	    return countrySalesTier1Rate;
	}

	public void setCountrySalesTier2Rate(BigDecimal countrySalesTier2Rate) {
	    this.countrySalesTier2Rate = countrySalesTier2Rate;
	}

	public BigDecimal getCountrySalesTier2Rate() {
	    return countrySalesTier2Rate;
	}

	public void setCountrySalesTier3Rate(BigDecimal countrySalesTier3Rate) {
	    this.countrySalesTier3Rate = countrySalesTier3Rate;
	}

	public BigDecimal getCountrySalesTier3Rate() {
	    return countrySalesTier3Rate;
	}

	public void setCountrySalesTier1Setamt(BigDecimal countrySalesTier1Setamt) {
	    this.countrySalesTier1Setamt = countrySalesTier1Setamt;
	}

	public BigDecimal getCountrySalesTier1Setamt() {
	    return countrySalesTier1Setamt;
	}

	public void setCountrySalesTier2Setamt(BigDecimal countrySalesTier2Setamt) {
	    this.countrySalesTier2Setamt = countrySalesTier2Setamt;
	}

	public BigDecimal getCountrySalesTier2Setamt() {
	    return countrySalesTier2Setamt;
	}

	public void setCountrySalesTier3Setamt(BigDecimal countrySalesTier3Setamt) {
	    this.countrySalesTier3Setamt = countrySalesTier3Setamt;
	}

	public BigDecimal getCountrySalesTier3Setamt() {
	    return countrySalesTier3Setamt;
	}

	public void setCountrySalesTier1MaxAmt(BigDecimal countrySalesTier1MaxAmt) {
	    this.countrySalesTier1MaxAmt = countrySalesTier1MaxAmt;
	}

	public BigDecimal getCountrySalesTier1MaxAmt() {
	    return countrySalesTier1MaxAmt;
	}

	public void setCountrySalesTier2MinAmt(BigDecimal countrySalesTier2MinAmt) {
	    this.countrySalesTier2MinAmt = countrySalesTier2MinAmt;
	}

	public BigDecimal getCountrySalesTier2MinAmt() {
	    return countrySalesTier2MinAmt;
	}

	public void setCountrySalesTier2MaxAmt(BigDecimal countrySalesTier2MaxAmt) {
	    this.countrySalesTier2MaxAmt = countrySalesTier2MaxAmt;
	}

	public BigDecimal getCountrySalesTier2MaxAmt() {
	    return countrySalesTier2MaxAmt;
	}

	public void setCountrySalesTier2EntamFlag(String countrySalesTier2EntamFlag) {
	    this.countrySalesTier2EntamFlag = countrySalesTier2EntamFlag;
	}

	public String getCountrySalesTier2EntamFlag() {
	    return countrySalesTier2EntamFlag;
	}

	public void setCountrySalesTier3EntamFlag(String countrySalesTier3EntamFlag) {
	    this.countrySalesTier3EntamFlag = countrySalesTier3EntamFlag;
	}

	public String getCountrySalesTier3EntamFlag() {
	    return countrySalesTier3EntamFlag;
	}

	public void setCountrySalesMaxtaxAmt(BigDecimal countrySalesMaxtaxAmt) {
	    this.countrySalesMaxtaxAmt = countrySalesMaxtaxAmt;
	}

	public BigDecimal getCountrySalesMaxtaxAmt() {
	    return countrySalesMaxtaxAmt;
	}

	public void setCountryUseTier1Rate(BigDecimal countryUseTier1Rate) {
	    this.countryUseTier1Rate = countryUseTier1Rate;
	}

	public BigDecimal getCountryUseTier1Rate() {
	    return countryUseTier1Rate;
	}

	public void setCountryUseTier2Rate(BigDecimal countryUseTier2Rate) {
	    this.countryUseTier2Rate = countryUseTier2Rate;
	}

	public BigDecimal getCountryUseTier2Rate() {
	    return countryUseTier2Rate;
	}

	public void setCountryUseTier3Rate(BigDecimal countryUseTier3Rate) {
	    this.countryUseTier3Rate = countryUseTier3Rate;
	}

	public BigDecimal getCountryUseTier3Rate() {
	    return countryUseTier3Rate;
	}

	public void setCountryUseTier1Setamt(BigDecimal countryUseTier1Setamt) {
	    this.countryUseTier1Setamt = countryUseTier1Setamt;
	}

	public BigDecimal getCountryUseTier1Setamt() {
	    return countryUseTier1Setamt;
	}

	public void setCountryUseTier2Setamt(BigDecimal countryUseTier2Setamt) {
	    this.countryUseTier2Setamt = countryUseTier2Setamt;
	}

	public BigDecimal getCountryUseTier2Setamt() {
	    return countryUseTier2Setamt;
	}

	public void setCountryUseTier3Setamt(BigDecimal countryUseTier3Setamt) {
	    this.countryUseTier3Setamt = countryUseTier3Setamt;
	}

	public BigDecimal getCountryUseTier3Setamt() {
	    return countryUseTier3Setamt;
	}

	public void setCountryUseTier1MaxAmt(BigDecimal countryUseTier1MaxAmt) {
	    this.countryUseTier1MaxAmt = countryUseTier1MaxAmt;
	}

	public BigDecimal getCountryUseTier1MaxAmt() {
	    return countryUseTier1MaxAmt;
	}

	public void setCountryUseTier2MinAmt(BigDecimal countryUseTier2MinAmt) {
	    this.countryUseTier2MinAmt = countryUseTier2MinAmt;
	}

	public BigDecimal getCountryUseTier2MinAmt() {
	    return countryUseTier2MinAmt;
	}

	public void setCountryUseTier2MaxAmt(BigDecimal countryUseTier2MaxAmt) {
	    this.countryUseTier2MaxAmt = countryUseTier2MaxAmt;
	}

	public BigDecimal getCountryUseTier2MaxAmt() {
	    return countryUseTier2MaxAmt;
	}

	public void setCountryUseTier2EntamFlag(String countryUseTier2EntamFlag) {
	    this.countryUseTier2EntamFlag = countryUseTier2EntamFlag;
	}

	public String getCountryUseTier2EntamFlag() {
	    return countryUseTier2EntamFlag;
	}

	public void setCountryUseTier3EntamFlag(String countryUseTier3EntamFlag) {
	    this.countryUseTier3EntamFlag = countryUseTier3EntamFlag;
	}

	public String getCountryUseTier3EntamFlag() {
	    return countryUseTier3EntamFlag;
	}

	public void setCountryUseMaxtaxAmt(BigDecimal countryUseMaxtaxAmt) {
	    this.countryUseMaxtaxAmt = countryUseMaxtaxAmt;
	}

	public BigDecimal getCountryUseMaxtaxAmt() {
	    return countryUseMaxtaxAmt;
	}

	public void setStateSalesTier1Rate(BigDecimal stateSalesTier1Rate) {
	    this.stateSalesTier1Rate = stateSalesTier1Rate;
	}

	public BigDecimal getStateSalesTier1Rate() {
	    return stateSalesTier1Rate;
	}

	public void setStateSalesTier2Rate(BigDecimal stateSalesTier2Rate) {
	    this.stateSalesTier2Rate = stateSalesTier2Rate;
	}

	public BigDecimal getStateSalesTier2Rate() {
	    return stateSalesTier2Rate;
	}

	public void setStateSalesTier3Rate(BigDecimal stateSalesTier3Rate) {
	    this.stateSalesTier3Rate = stateSalesTier3Rate;
	}

	public BigDecimal getStateSalesTier3Rate() {
	    return stateSalesTier3Rate;
	}

	public void setStateSalesTier1Setamt(BigDecimal stateSalesTier1Setamt) {
	    this.stateSalesTier1Setamt = stateSalesTier1Setamt;
	}

	public BigDecimal getStateSalesTier1Setamt() {
	    return stateSalesTier1Setamt;
	}

	public void setStateSalesTier2Setamt(BigDecimal stateSalesTier2Setamt) {
	    this.stateSalesTier2Setamt = stateSalesTier2Setamt;
	}

	public BigDecimal getStateSalesTier2Setamt() {
	    return stateSalesTier2Setamt;
	}

	public void setStateSalesTier3Setamt(BigDecimal stateSalesTier3Setamt) {
	    this.stateSalesTier3Setamt = stateSalesTier3Setamt;
	}

	public BigDecimal getStateSalesTier3Setamt() {
	    return stateSalesTier3Setamt;
	}

	public void setStateSalesTier1MaxAmt(BigDecimal stateSalesTier1MaxAmt) {
	    this.stateSalesTier1MaxAmt = stateSalesTier1MaxAmt;
	}

	public BigDecimal getStateSalesTier1MaxAmt() {
	    return stateSalesTier1MaxAmt;
	}

	public void setStateSalesTier2MinAmt(BigDecimal stateSalesTier2MinAmt) {
	    this.stateSalesTier2MinAmt = stateSalesTier2MinAmt;
	}

	public BigDecimal getStateSalesTier2MinAmt() {
	    return stateSalesTier2MinAmt;
	}

	public void setStateSalesTier2MaxAmt(BigDecimal stateSalesTier2MaxAmt) {
	    this.stateSalesTier2MaxAmt = stateSalesTier2MaxAmt;
	}

	public BigDecimal getStateSalesTier2MaxAmt() {
	    return stateSalesTier2MaxAmt;
	}

	public void setStateSalesTier2EntamFlag(String stateSalesTier2EntamFlag) {
	    this.stateSalesTier2EntamFlag = stateSalesTier2EntamFlag;
	}

	public String getStateSalesTier2EntamFlag() {
	    return stateSalesTier2EntamFlag;
	}

	public void setStateSalesTier3EntamFlag(String stateSalesTier3EntamFlag) {
	    this.stateSalesTier3EntamFlag = stateSalesTier3EntamFlag;
	}

	public String getStateSalesTier3EntamFlag() {
	    return stateSalesTier3EntamFlag;
	}

	public void setStateSalesMaxtaxAmt(BigDecimal stateSalesMaxtaxAmt) {
	    this.stateSalesMaxtaxAmt = stateSalesMaxtaxAmt;
	}

	public BigDecimal getStateSalesMaxtaxAmt() {
	    return stateSalesMaxtaxAmt;
	}

	public void setStateUseTier1Rate(BigDecimal stateUseTier1Rate) {
	    this.stateUseTier1Rate = stateUseTier1Rate;
	}

	public BigDecimal getStateUseTier1Rate() {
	    return stateUseTier1Rate;
	}

	public void setStateUseTier2Rate(BigDecimal stateUseTier2Rate) {
	    this.stateUseTier2Rate = stateUseTier2Rate;
	}

	public BigDecimal getStateUseTier2Rate() {
	    return stateUseTier2Rate;
	}

	public void setStateUseTier3Rate(BigDecimal stateUseTier3Rate) {
	    this.stateUseTier3Rate = stateUseTier3Rate;
	}

	public BigDecimal getStateUseTier3Rate() {
	    return stateUseTier3Rate;
	}

	public void setStateUseTier1Setamt(BigDecimal stateUseTier1Setamt) {
	    this.stateUseTier1Setamt = stateUseTier1Setamt;
	}

	public BigDecimal getStateUseTier1Setamt() {
	    return stateUseTier1Setamt;
	}

	public void setStateUseTier2Setamt(BigDecimal stateUseTier2Setamt) {
	    this.stateUseTier2Setamt = stateUseTier2Setamt;
	}

	public BigDecimal getStateUseTier2Setamt() {
	    return stateUseTier2Setamt;
	}

	public void setStateUseTier3Setamt(BigDecimal stateUseTier3Setamt) {
	    this.stateUseTier3Setamt = stateUseTier3Setamt;
	}

	public BigDecimal getStateUseTier3Setamt() {
	    return stateUseTier3Setamt;
	}

	public void setStateUseTier1MaxAmt(BigDecimal stateUseTier1MaxAmt) {
	    this.stateUseTier1MaxAmt = stateUseTier1MaxAmt;
	}

	public BigDecimal getStateUseTier1MaxAmt() {
	    return stateUseTier1MaxAmt;
	}

	public void setStateUseTier2MinAmt(BigDecimal stateUseTier2MinAmt) {
	    this.stateUseTier2MinAmt = stateUseTier2MinAmt;
	}

	public BigDecimal getStateUseTier2MinAmt() {
	    return stateUseTier2MinAmt;
	}

	public void setStateUseTier2MaxAmt(BigDecimal stateUseTier2MaxAmt) {
	    this.stateUseTier2MaxAmt = stateUseTier2MaxAmt;
	}

	public BigDecimal getStateUseTier2MaxAmt() {
	    return stateUseTier2MaxAmt;
	}

	public void setStateUseTier2EntamFlag(String stateUseTier2EntamFlag) {
	    this.stateUseTier2EntamFlag = stateUseTier2EntamFlag;
	}

	public String getStateUseTier2EntamFlag() {
	    return stateUseTier2EntamFlag;
	}

	public void setStateUseTier3EntamFlag(String stateUseTier3EntamFlag) {
	    this.stateUseTier3EntamFlag = stateUseTier3EntamFlag;
	}

	public String getStateUseTier3EntamFlag() {
	    return stateUseTier3EntamFlag;
	}

	public void setStateUseMaxtaxAmt(BigDecimal stateUseMaxtaxAmt) {
	    this.stateUseMaxtaxAmt = stateUseMaxtaxAmt;
	}

	public BigDecimal getStateUseMaxtaxAmt() {
	    return stateUseMaxtaxAmt;
	}

	public void setCountySalesTier1Rate(BigDecimal countySalesTier1Rate) {
	    this.countySalesTier1Rate = countySalesTier1Rate;
	}

	public BigDecimal getCountySalesTier1Rate() {
	    return countySalesTier1Rate;
	}

	public void setCountySalesTier2Rate(BigDecimal countySalesTier2Rate) {
	    this.countySalesTier2Rate = countySalesTier2Rate;
	}

	public BigDecimal getCountySalesTier2Rate() {
	    return countySalesTier2Rate;
	}

	public void setCountySalesTier3Rate(BigDecimal countySalesTier3Rate) {
	    this.countySalesTier3Rate = countySalesTier3Rate;
	}

	public BigDecimal getCountySalesTier3Rate() {
	    return countySalesTier3Rate;
	}

	public void setCountySalesTier1Setamt(BigDecimal countySalesTier1Setamt) {
	    this.countySalesTier1Setamt = countySalesTier1Setamt;
	}

	public BigDecimal getCountySalesTier1Setamt() {
	    return countySalesTier1Setamt;
	}

	public void setCountySalesTier2Setamt(BigDecimal countySalesTier2Setamt) {
	    this.countySalesTier2Setamt = countySalesTier2Setamt;
	}

	public BigDecimal getCountySalesTier2Setamt() {
	    return countySalesTier2Setamt;
	}

	public void setCountySalesTier3Setamt(BigDecimal countySalesTier3Setamt) {
	    this.countySalesTier3Setamt = countySalesTier3Setamt;
	}

	public BigDecimal getCountySalesTier3Setamt() {
	    return countySalesTier3Setamt;
	}

	public void setCountySalesTier1MaxAmt(BigDecimal countySalesTier1MaxAmt) {
	    this.countySalesTier1MaxAmt = countySalesTier1MaxAmt;
	}

	public BigDecimal getCountySalesTier1MaxAmt() {
	    return countySalesTier1MaxAmt;
	}

	public void setCountySalesTier2MinAmt(BigDecimal countySalesTier2MinAmt) {
	    this.countySalesTier2MinAmt = countySalesTier2MinAmt;
	}

	public BigDecimal getCountySalesTier2MinAmt() {
	    return countySalesTier2MinAmt;
	}

	public void setCountySalesTier2MaxAmt(BigDecimal countySalesTier2MaxAmt) {
	    this.countySalesTier2MaxAmt = countySalesTier2MaxAmt;
	}

	public BigDecimal getCountySalesTier2MaxAmt() {
	    return countySalesTier2MaxAmt;
	}

	public void setCountySalesTier2EntamFlag(String countySalesTier2EntamFlag) {
	    this.countySalesTier2EntamFlag = countySalesTier2EntamFlag;
	}

	public String getCountySalesTier2EntamFlag() {
	    return countySalesTier2EntamFlag;
	}

	public void setCountySalesTier3EntamFlag(String countySalesTier3EntamFlag) {
	    this.countySalesTier3EntamFlag = countySalesTier3EntamFlag;
	}

	public String getCountySalesTier3EntamFlag() {
	    return countySalesTier3EntamFlag;
	}

	public void setCountySalesMaxtaxAmt(BigDecimal countySalesMaxtaxAmt) {
	    this.countySalesMaxtaxAmt = countySalesMaxtaxAmt;
	}

	public BigDecimal getCountySalesMaxtaxAmt() {
	    return countySalesMaxtaxAmt;
	}

	public void setCountyUseTier1Rate(BigDecimal countyUseTier1Rate) {
	    this.countyUseTier1Rate = countyUseTier1Rate;
	}

	public BigDecimal getCountyUseTier1Rate() {
	    return countyUseTier1Rate;
	}

	public void setCountyUseTier2Rate(BigDecimal countyUseTier2Rate) {
	    this.countyUseTier2Rate = countyUseTier2Rate;
	}

	public BigDecimal getCountyUseTier2Rate() {
	    return countyUseTier2Rate;
	}

	public void setCountyUseTier3Rate(BigDecimal countyUseTier3Rate) {
	    this.countyUseTier3Rate = countyUseTier3Rate;
	}

	public BigDecimal getCountyUseTier3Rate() {
	    return countyUseTier3Rate;
	}

	public void setCountyUseTier1Setamt(BigDecimal countyUseTier1Setamt) {
	    this.countyUseTier1Setamt = countyUseTier1Setamt;
	}

	public BigDecimal getCountyUseTier1Setamt() {
	    return countyUseTier1Setamt;
	}

	public void setCountyUseTier2Setamt(BigDecimal countyUseTier2Setamt) {
	    this.countyUseTier2Setamt = countyUseTier2Setamt;
	}

	public BigDecimal getCountyUseTier2Setamt() {
	    return countyUseTier2Setamt;
	}

	public void setCountyUseTier3Setamt(BigDecimal countyUseTier3Setamt) {
	    this.countyUseTier3Setamt = countyUseTier3Setamt;
	}

	public BigDecimal getCountyUseTier3Setamt() {
	    return countyUseTier3Setamt;
	}

	public void setCountyUseTier1MaxAmt(BigDecimal countyUseTier1MaxAmt) {
	    this.countyUseTier1MaxAmt = countyUseTier1MaxAmt;
	}

	public BigDecimal getCountyUseTier1MaxAmt() {
	    return countyUseTier1MaxAmt;
	}

	public void setCountyUseTier2MinAmt(BigDecimal countyUseTier2MinAmt) {
	    this.countyUseTier2MinAmt = countyUseTier2MinAmt;
	}

	public BigDecimal getCountyUseTier2MinAmt() {
	    return countyUseTier2MinAmt;
	}

	public void setCountyUseTier2MaxAmt(BigDecimal countyUseTier2MaxAmt) {
	    this.countyUseTier2MaxAmt = countyUseTier2MaxAmt;
	}

	public BigDecimal getCountyUseTier2MaxAmt() {
	    return countyUseTier2MaxAmt;
	}

	public void setCountyUseTier2EntamFlag(String countyUseTier2EntamFlag) {
	    this.countyUseTier2EntamFlag = countyUseTier2EntamFlag;
	}

	public String getCountyUseTier2EntamFlag() {
	    return countyUseTier2EntamFlag;
	}

	public void setCountyUseTier3EntamFlag(String countyUseTier3EntamFlag) {
	    this.countyUseTier3EntamFlag = countyUseTier3EntamFlag;
	}

	public String getCountyUseTier3EntamFlag() {
	    return countyUseTier3EntamFlag;
	}

	public void setCountyUseMaxtaxAmt(BigDecimal countyUseMaxtaxAmt) {
	    this.countyUseMaxtaxAmt = countyUseMaxtaxAmt;
	}

	public BigDecimal getCountyUseMaxtaxAmt() {
	    return countyUseMaxtaxAmt;
	}

	public void setCitySalesTier1Rate(BigDecimal citySalesTier1Rate) {
	    this.citySalesTier1Rate = citySalesTier1Rate;
	}

	public BigDecimal getCitySalesTier1Rate() {
	    return citySalesTier1Rate;
	}

	public void setCitySalesTier2Rate(BigDecimal citySalesTier2Rate) {
	    this.citySalesTier2Rate = citySalesTier2Rate;
	}

	public BigDecimal getCitySalesTier2Rate() {
	    return citySalesTier2Rate;
	}

	public void setCitySalesTier3Rate(BigDecimal citySalesTier3Rate) {
	    this.citySalesTier3Rate = citySalesTier3Rate;
	}

	public BigDecimal getCitySalesTier3Rate() {
	    return citySalesTier3Rate;
	}

	public void setCitySalesTier1Setamt(BigDecimal citySalesTier1Setamt) {
	    this.citySalesTier1Setamt = citySalesTier1Setamt;
	}

	public BigDecimal getCitySalesTier1Setamt() {
	    return citySalesTier1Setamt;
	}

	public void setCitySalesTier2Setamt(BigDecimal citySalesTier2Setamt) {
	    this.citySalesTier2Setamt = citySalesTier2Setamt;
	}

	public BigDecimal getCitySalesTier2Setamt() {
	    return citySalesTier2Setamt;
	}

	public void setCitySalesTier3Setamt(BigDecimal citySalesTier3Setamt) {
	    this.citySalesTier3Setamt = citySalesTier3Setamt;
	}

	public BigDecimal getCitySalesTier3Setamt() {
	    return citySalesTier3Setamt;
	}

	public void setCitySalesTier1MaxAmt(BigDecimal citySalesTier1MaxAmt) {
	    this.citySalesTier1MaxAmt = citySalesTier1MaxAmt;
	}

	public BigDecimal getCitySalesTier1MaxAmt() {
	    return citySalesTier1MaxAmt;
	}

	public void setCitySalesTier2MinAmt(BigDecimal citySalesTier2MinAmt) {
	    this.citySalesTier2MinAmt = citySalesTier2MinAmt;
	}

	public BigDecimal getCitySalesTier2MinAmt() {
	    return citySalesTier2MinAmt;
	}

	public void setCitySalesTier2MaxAmt(BigDecimal citySalesTier2MaxAmt) {
	    this.citySalesTier2MaxAmt = citySalesTier2MaxAmt;
	}

	public BigDecimal getCitySalesTier2MaxAmt() {
	    return citySalesTier2MaxAmt;
	}

	public void setCitySalesTier2EntamFlag(String citySalesTier2EntamFlag) {
	    this.citySalesTier2EntamFlag = citySalesTier2EntamFlag;
	}

	public String getCitySalesTier2EntamFlag() {
	    return citySalesTier2EntamFlag;
	}

	public void setCitySalesTier3EntamFlag(String citySalesTier3EntamFlag) {
	    this.citySalesTier3EntamFlag = citySalesTier3EntamFlag;
	}

	public String getCitySalesTier3EntamFlag() {
	    return citySalesTier3EntamFlag;
	}

	public void setCitySalesMaxtaxAmt(BigDecimal citySalesMaxtaxAmt) {
	    this.citySalesMaxtaxAmt = citySalesMaxtaxAmt;
	}

	public BigDecimal getCitySalesMaxtaxAmt() {
	    return citySalesMaxtaxAmt;
	}

	public void setCityUseTier1Rate(BigDecimal cityUseTier1Rate) {
	    this.cityUseTier1Rate = cityUseTier1Rate;
	}

	public BigDecimal getCityUseTier1Rate() {
	    return cityUseTier1Rate;
	}

	public void setCityUseTier2Rate(BigDecimal cityUseTier2Rate) {
	    this.cityUseTier2Rate = cityUseTier2Rate;
	}

	public BigDecimal getCityUseTier2Rate() {
	    return cityUseTier2Rate;
	}

	public void setCityUseTier3Rate(BigDecimal cityUseTier3Rate) {
	    this.cityUseTier3Rate = cityUseTier3Rate;
	}

	public BigDecimal getCityUseTier3Rate() {
	    return cityUseTier3Rate;
	}

	public void setCityUseTier1Setamt(BigDecimal cityUseTier1Setamt) {
	    this.cityUseTier1Setamt = cityUseTier1Setamt;
	}

	public BigDecimal getCityUseTier1Setamt() {
	    return cityUseTier1Setamt;
	}

	public void setCityUseTier2Setamt(BigDecimal cityUseTier2Setamt) {
	    this.cityUseTier2Setamt = cityUseTier2Setamt;
	}

	public BigDecimal getCityUseTier2Setamt() {
	    return cityUseTier2Setamt;
	}

	public void setCityUseTier3Setamt(BigDecimal cityUseTier3Setamt) {
	    this.cityUseTier3Setamt = cityUseTier3Setamt;
	}

	public BigDecimal getCityUseTier3Setamt() {
	    return cityUseTier3Setamt;
	}

	public void setCityUseTier1MaxAmt(BigDecimal cityUseTier1MaxAmt) {
	    this.cityUseTier1MaxAmt = cityUseTier1MaxAmt;
	}

	public BigDecimal getCityUseTier1MaxAmt() {
	    return cityUseTier1MaxAmt;
	}

	public void setCityUseTier2MinAmt(BigDecimal cityUseTier2MinAmt) {
	    this.cityUseTier2MinAmt = cityUseTier2MinAmt;
	}

	public BigDecimal getCityUseTier2MinAmt() {
	    return cityUseTier2MinAmt;
	}

	public void setCityUseTier2MaxAmt(BigDecimal cityUseTier2MaxAmt) {
	    this.cityUseTier2MaxAmt = cityUseTier2MaxAmt;
	}

	public BigDecimal getCityUseTier2MaxAmt() {
	    return cityUseTier2MaxAmt;
	}

	public void setCityUseTier2EntamFlag(String cityUseTier2EntamFlag) {
	    this.cityUseTier2EntamFlag = cityUseTier2EntamFlag;
	}

	public String getCityUseTier2EntamFlag() {
	    return cityUseTier2EntamFlag;
	}

	public void setCityUseTier3EntamFlag(String cityUseTier3EntamFlag) {
	    this.cityUseTier3EntamFlag = cityUseTier3EntamFlag;
	}

	public String getCityUseTier3EntamFlag() {
	    return cityUseTier3EntamFlag;
	}

	public void setCityUseMaxtaxAmt(BigDecimal cityUseMaxtaxAmt) {
	    this.cityUseMaxtaxAmt = cityUseMaxtaxAmt;
	}

	public BigDecimal getCityUseMaxtaxAmt() {
	    return cityUseMaxtaxAmt;
	}

	public void setStj1SalesRate(BigDecimal stj1SalesRate) {
	    this.stj1SalesRate = stj1SalesRate;
	}

	public BigDecimal getStj1SalesRate() {
	    return stj1SalesRate;
	}

	public void setStj1SalesSetamt(BigDecimal stj1SalesSetamt) {
	    this.stj1SalesSetamt = stj1SalesSetamt;
	}

	public BigDecimal getStj1SalesSetamt() {
	    return stj1SalesSetamt;
	}

	public void setStj2SalesRate(BigDecimal stj2SalesRate) {
	    this.stj2SalesRate = stj2SalesRate;
	}

	public BigDecimal getStj2SalesRate() {
	    return stj2SalesRate;
	}

	public void setStj2SalesSetamt(BigDecimal stj2SalesSetamt) {
	    this.stj2SalesSetamt = stj2SalesSetamt;
	}

	public BigDecimal getStj2SalesSetamt() {
	    return stj2SalesSetamt;
	}

	public void setStj3SalesRate(BigDecimal stj3SalesRate) {
	    this.stj3SalesRate = stj3SalesRate;
	}

	public BigDecimal getStj3SalesRate() {
	    return stj3SalesRate;
	}

	public void setStj3SalesSetamt(BigDecimal stj3SalesSetamt) {
	    this.stj3SalesSetamt = stj3SalesSetamt;
	}

	public BigDecimal getStj3SalesSetamt() {
	    return stj3SalesSetamt;
	}

	public void setStj4SalesRate(BigDecimal stj4SalesRate) {
	    this.stj4SalesRate = stj4SalesRate;
	}

	public BigDecimal getStj4SalesRate() {
	    return stj4SalesRate;
	}

	public void setStj4SalesSetamt(BigDecimal stj4SalesSetamt) {
	    this.stj4SalesSetamt = stj4SalesSetamt;
	}

	public BigDecimal getStj4SalesSetamt() {
	    return stj4SalesSetamt;
	}

	public void setStj5SalesRate(BigDecimal stj5SalesRate) {
	    this.stj5SalesRate = stj5SalesRate;
	}

	public BigDecimal getStj5SalesRate() {
	    return stj5SalesRate;
	}

	public void setStj5SalesSetamt(BigDecimal stj5SalesSetamt) {
	    this.stj5SalesSetamt = stj5SalesSetamt;
	}

	public BigDecimal getStj5SalesSetamt() {
	    return stj5SalesSetamt;
	}

	public void setStj1UseRate(BigDecimal stj1UseRate) {
	    this.stj1UseRate = stj1UseRate;
	}

	public BigDecimal getStj1UseRate() {
	    return stj1UseRate;
	}

	public void setStj1UseSetamt(BigDecimal stj1UseSetamt) {
	    this.stj1UseSetamt = stj1UseSetamt;
	}

	public BigDecimal getStj1UseSetamt() {
	    return stj1UseSetamt;
	}

	public void setStj2UseRate(BigDecimal stj2UseRate) {
	    this.stj2UseRate = stj2UseRate;
	}

	public BigDecimal getStj2UseRate() {
	    return stj2UseRate;
	}

	public void setStj2UseSetamt(BigDecimal stj2UseSetamt) {
	    this.stj2UseSetamt = stj2UseSetamt;
	}

	public BigDecimal getStj2UseSetamt() {
	    return stj2UseSetamt;
	}

	public void setStj3UseRate(BigDecimal stj3UseRate) {
	    this.stj3UseRate = stj3UseRate;
	}

	public BigDecimal getStj3UseRate() {
	    return stj3UseRate;
	}

	public void setStj3UseSetamt(BigDecimal stj3UseSetamt) {
	    this.stj3UseSetamt = stj3UseSetamt;
	}

	public BigDecimal getStj3UseSetamt() {
	    return stj3UseSetamt;
	}

	public void setStj4UseRate(BigDecimal stj4UseRate) {
	    this.stj4UseRate = stj4UseRate;
	}

	public BigDecimal getStj4UseRate() {
	    return stj4UseRate;
	}

	public void setStj4UseSetamt(BigDecimal stj4UseSetamt) {
	    this.stj4UseSetamt = stj4UseSetamt;
	}

	public BigDecimal getStj4UseSetamt() {
	    return stj4UseSetamt;
	}

	public void setStj5UseRate(BigDecimal stj5UseRate) {
	    this.stj5UseRate = stj5UseRate;
	}

	public BigDecimal getStj5UseRate() {
	    return stj5UseRate;
	}

	public void setStj5UseSetamt(BigDecimal stj5UseSetamt) {
	    this.stj5UseSetamt = stj5UseSetamt;
	}

	public BigDecimal getStj5UseSetamt() {
	    return stj5UseSetamt;
	}
	
	public String getCountryUseSameSales() {
		return countryUseSameSales;
	}

	public void setCountryUseSameSales(String countryUseSameSales) {
		this.countryUseSameSales = countryUseSameSales;
	}
	
	public String getStateUseSameSales() {
		return stateUseSameSales;
	}

	public void setStateUseSameSales(String stateUseSameSales) {
		this.stateUseSameSales = stateUseSameSales;
	}
	
	public String getCountyUseSameSales() {
		return countyUseSameSales;
	}

	public void setCountyUseSameSales(String countyUseSameSales) {
		this.countyUseSameSales = countyUseSameSales;
	}
	
	public String getCityUseSameSales() {
		return cityUseSameSales;
	}

	public void setCityUseSameSales(String cityUseSameSales) {
		this.cityUseSameSales = cityUseSameSales;
	}
	
	public String getStjUseSameSales() {
		return stjUseSameSales;
	}

	public void setStjUseSameSales(String stjUseSameSales) {
		this.stjUseSameSales = stjUseSameSales;
	}

	public BigDecimal getCombinedUseRate() {
		return combinedUseRate;
	}

	public void setCombinedUseRate(BigDecimal combinedUseRate) {
		this.combinedUseRate = combinedUseRate;
	}

	public BigDecimal getCombinedSalesRate() {
		return combinedSalesRate;
	}

	public void setCombinedSalesRate(BigDecimal combinedSalesRate) {
		this.combinedSalesRate = combinedSalesRate;
	}
	
}