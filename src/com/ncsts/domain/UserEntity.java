package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.UserEntityDTO;

@Entity
@Table(name="TB_USER_ENTITY")
public class UserEntity extends Auditable implements Serializable, Idable<UserEntityPK> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -621997319406268142L;

	@EmbeddedId
	UserEntityPK userEntityPK; 
	
	@Column(name="ROLE_CODE")
	private String roleCode;
	
	//Midtier project code added - january 2009
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USER_CODE",insertable=false, updatable=false)
	@ForeignKey(name="FK_TB_USER_ENTITY_1")
	private User user;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ENTITY_ID",insertable=false, updatable=false)
	@ForeignKey(name="FK_TB_USER_ENTITY_2")
	private EntityItem entity;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_CODE",insertable=false, updatable=false)
	@ForeignKey(name="FK_TB_USER_ENTITY_3")
	private Role role;
	
    @OneToMany (mappedBy  = "userEntity")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<UserMenuEx> userMenuEx = new HashSet<UserMenuEx>();
	
	@Transient	
	UserEntityDTO userEntityDTO;
	
    public UserEntity() { 
    	
    }
	
    public UserEntity(UserEntityPK userEntityPK) { 
    	this.userEntityPK = userEntityPK;
    }
	/**
	 * @return the userEntityDTO
	*/
	public UserEntityDTO getUserEntityDTO() {
		UserEntityDTO userEntityDTO = new UserEntityDTO();
		BeanUtils.copyProperties(this, userEntityDTO);
		return userEntityDTO;
	}

	public void setUserEntityDTO(UserEntityDTO userEntityDTO) {
		this.userEntityDTO = userEntityDTO;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EntityItem getEntity() {
		return entity;
	}

	public void setEntity(EntityItem entity) {
		this.entity = entity;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public UserEntityPK getId() {
		return userEntityPK;
	}

	public void setId(UserEntityPK id) {
		this.userEntityPK = id;
	}
	
    public String getIdPropertyName() {
    	return "userEntityPK";
    }

	public UserEntityPK getUserEntityPK() {
		return userEntityPK;
	}

	public void setUserEntityPK(UserEntityPK userEntityPK) {
		this.userEntityPK = userEntityPK;
	}

	public String getUserCode() {
		return userEntityPK.getUserCode();
	}
	
	public Long getEntityId() {
		return userEntityPK.getEntityId();
	}	

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
    	userEntityPK.equals(other);
        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		return userEntityPK.hashCode();
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.userEntityPK.toString();
    }

	public Set<UserMenuEx> getUserMenuEx() {
		return userMenuEx;
	}

	public void setUserMenuEx(Set<UserMenuEx> userMenuEx) {
		this.userMenuEx = userMenuEx;
	}
	
}

