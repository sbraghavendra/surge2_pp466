package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CCHCodePK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="FILENAME")  	
	private String fileName;
	@Column(name="FIELDNAME")  	
	private String fieldName;
	@Column(name="CODE")  	
	private String code;
	
	
	public CCHCodePK() {
		
	}
	public CCHCodePK(String fileName, String fieldName, String code) {
		this.fileName = fileName;
		this.fieldName = fieldName;
		this.code = code;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof CCHCodePK)) {
            return false;
        }

        final CCHCodePK revision = (CCHCodePK)other;

        String f1 = getFileName();
        String f2 = revision.getFileName();
        if (!((f1 == f2) || (f1 != null && f1.equals(f2)))) {
            return false;
        }

        String f3 = getFieldName();
        String f4 = revision.getFieldName();
        if (!((f3 == f4) || (f3 != null && f3.equals(f4)))) {
            return false;
        }
        
        String c1 = getCode();
        String c2 = revision.getCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String f1 = getFileName();
		hash = 31 * hash + (null == f1 ? 0 : f1.hashCode());
        String f3 = getFieldName();
		hash = 31 * hash + (null == f3 ? 0 : f3.hashCode());
        String c1 = getCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.fileName + "::" + this.fieldName + "::" + this.code;
    }
}
