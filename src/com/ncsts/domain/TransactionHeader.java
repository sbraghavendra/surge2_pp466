package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

// Added for column re order - 3987
@Entity
@Table(name="TB_DW_COLUMN")
public class TransactionHeader implements Serializable{
	private static final long serialVersionUID = 8544536838077762806L;
	
	@EmbeddedId
	DwColumnPK dwColumnPK; 

	@Column(name="COLUMN_ORDER")
	private int columnId;
	
	@Column(name="COLUMN_WIDTH")
	private int columnWidth;
	
	@Column(name="COLUMN_X")
	private int columnX;
	
	@Column(name="ALL_COLUMNS")
	private String allColumns;
	
	
	@Transient
	private String columnLabel;


	public TransactionHeader(String columnName, String columnLabel) {
		this.dwColumnPK = new DwColumnPK();		
		this.dwColumnPK.setColumnName(columnName);
		this.columnLabel = columnLabel;
	}

	public TransactionHeader() {
		this.dwColumnPK = new DwColumnPK();	
	}

	public String getWindowName() {
		return dwColumnPK.getWindowName();
	}

	public void setWindowName(String windowName) {
		dwColumnPK.setWindowName(windowName);
	}

	public String getDataWindowName() {
		return dwColumnPK.getDataWindowName();
	}

	public void setDataWindowName(String dataWindowName) {
		dwColumnPK.setDataWindowName(dataWindowName);
	}

	public String getUserCode() {
		return dwColumnPK.getUserCode();
	}

	public void setUserCode(String userCode) {
		dwColumnPK.setUserCode(userCode);
	}

	public int getColumnId() {
		return columnId;
	}

	public void setColumnId(int columnId) {
		this.columnId = columnId;
	}

	public int getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(int columnWidth) {
		this.columnWidth = columnWidth;
	}

	public int getColumnX() {
		return columnX;
	}

	public void setColumnX(int columnX) {
		this.columnX = columnX;
	}

	public String getColumnName() {
		return dwColumnPK.getColumnName();
	}

	public void setColumnName(String columnName) {
		dwColumnPK.setColumnName(columnName);
	}

	public String getColumnLabel() {
		return columnLabel;
	}

	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}
	
	public String getAllColumns() {
		return allColumns;
	}

	public void setAllColumns(String allColumns) {
		this.allColumns = allColumns;
	}
}
