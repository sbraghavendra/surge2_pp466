package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.UserDTO;

/**
 * @author Paul Govindan
 *
 */

@Entity
@Table(name="TB_USER")
public class User extends Auditable implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    @Id
    @Column(name="USER_CODE")	
    private String userCode;
    
    @Column(name="USER_NAME")    
    private String userName;
    
    @Column(name="LAST_USED_ENTITY_ID")    
    private Long lastUsedEntityId;   
    
    @Column(name="GLOBAL_BU_FLAG")    
    private String globalBuFlag;  
    
    @Column(name="DEFAULT_MATRIX_LINE_FLAG")    
    private String defaultMatrixLineFlag;      
    
    @Column(name="ADMIN_FLAG")    
    private String adminFlag; 
    
    @Column(name="ACTIVE_FLAG")    
    private String activeFlag;  
    
    @Column(name="ADD_NEXUS_FLAG")    
    private String addNexusFlag;
    
    @Column(name="LAST_MODULE")
    private String lastModule;
    
    @Column(name="GLOBAL_VIEW_FLAG")
    private String globalViewFlag;
    
    @Column(name="LAST_PURCH_VIEW")
    private String lastPurchView;
    
    //Midtier project code added - january 2009
    @OneToMany(cascade = {CascadeType.REMOVE},mappedBy  = "user")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<UserEntity> userEntity = new HashSet<UserEntity>();
    
    @OneToMany(mappedBy="users")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<ImportSpecUser> importSpecUser = new HashSet<ImportSpecUser>();
    
	@Column(name="VIEW_ONLY_FLAG")
	private String viewOnlyFlag;
     
    @Transient
    private boolean globalBooleanFlag = false;
    
    @Transient
    private boolean addNexusBooleanFlag = false;
    
    @Transient
    private boolean activeBooleanFlag = true;	
    
    @Transient
    private boolean adminBooleanFlag = false;	
    
    @Transient
    private boolean defaultMatrixLineBooleanFlag = false;	
    
    @Transient
    private boolean globalViewBooleanFlag = false;	

    @Transient
    private String lastUsedEntityName;
    @Transient
    private String lastUsedEntityCode;
	
	@Transient	
	UserDTO userDTO;
	
	@Transient
	private String userActiveDescription;
	
	@Transient
	private String userSTSUserCode = "STSCORP";
	
	@Transient
	private boolean userMappedBooleanFag = false;
	

	public boolean getUserMappedBooleanFag() {
		return userMappedBooleanFag;
	}

	public void setUserMappedBooleanFag(boolean userMappedBooleanFag) {
		this.userMappedBooleanFag = userMappedBooleanFag;
	}

	public String getUserActiveDescription() {
		
		if("1".equals(this.activeFlag)) {
			return "Yes" ;
		}
		else if("0".equals(this.activeFlag)) {
			return "No" ;
		}
		else {
			return "New User" ;
		}
	}

	public void setUserActiveDescription(String userActiveDescription) {
		this.userActiveDescription = userActiveDescription;
	}


	public String getUserSTSUserCode(){
		return this.userSTSUserCode;
	}
	
    public User() { 
    	
    }
    
    public User(String userCode , String userName) { 
    	this.userCode= userCode;
    	this.userName = userName;
    }
	
	public UserDTO getUserDTO() {
		UserDTO userDTO = new UserDTO();
		BeanUtils.copyProperties(this, userDTO);
		return userDTO;
	}
    
    public String getUserCode() {
        return this.userCode;
    }
    
    public void setUserCode(String userCode) {
    	this.userCode = userCode;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
    	this.userName = userName;
    }
    
    public Long getLastUsedEntityId() {
        return this.lastUsedEntityId;
    }
    
    public void setLastUsedEntityId(Long lastUsedEntityId) {
    	this.lastUsedEntityId = lastUsedEntityId;
    }  

    public void setLastUsedEntityName(String name) {
    	this.lastUsedEntityName = name;
    }  
    public String getLastUsedEntityName() {
        return this.lastUsedEntityName;
    }  

    public void setLastUsedEntityCode(String code) {
    	this.lastUsedEntityCode = code;
    }  
    public String getLastUsedEntityCode() {
        return this.lastUsedEntityCode;
    }  
    public String getLastUsedEntityString() {
        String s = "";
        if (this.lastUsedEntityName != null) s += this.lastUsedEntityName;
        if (s.length() > 0) {
            return s + "(" + this.lastUsedEntityCode + ")";
        } else {
            return this.lastUsedEntityCode;
        }
    }  
    
    public String getGlobalBuFlag() {
        return this.globalBuFlag;
    }
    
    public void setGlobalBuFlag(String globalBuFlag) {
    	this.globalBuFlag = globalBuFlag;
    } 
    
    public String getAddNexusFlag() {
        return this.addNexusFlag;
    }
    
    public void setAddNexusFlag(String addNexusFlag) {
    	this.addNexusFlag = addNexusFlag;
    }   

    public String getAdminFlag() {
        return this.adminFlag;
    }
    
    public void setAdminFlag(String adminFlag) {
    	this.adminFlag = adminFlag;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  
    
    public String getDefaultMatrixLineFlag() {
        return this.defaultMatrixLineFlag;
    }
    
    public void setDefaultMatrixLineFlag(String defaultMatrixLineFlag) {
    	this.defaultMatrixLineFlag = defaultMatrixLineFlag;
    }     
        
	public boolean getGlobalBooleanFlag() {
		if ("1".equalsIgnoreCase(this.globalBuFlag)) return true;		
		if ("0".equalsIgnoreCase(this.globalBuFlag)) return false;		
		return this.globalBooleanFlag;
	}
	
	public void setGlobalBooleanFlag (boolean globalBooleanFlag) {
		this.globalBooleanFlag = globalBooleanFlag;
		if (globalBooleanFlag) {
			this.globalBuFlag = "1";
		} else {
			this.globalBuFlag = "0";
		}
	}
	
	public boolean getAddNexusBooleanFlag() {
		if ("1".equalsIgnoreCase(this.addNexusFlag)) return true;		
		if ("0".equalsIgnoreCase(this.addNexusFlag)) return false;		
		return this.addNexusBooleanFlag;
	}
	
	public void setAddNexusBooleanFlag (boolean addNexusBooleanFlag) {
		this.addNexusBooleanFlag = addNexusBooleanFlag;
		if (addNexusBooleanFlag) {
			this.addNexusFlag = "1";
		} else {
			this.addNexusFlag = "0";
		}
	}
	
	public boolean getActiveBooleanFlag() {
		//if ("1".equalsIgnoreCase(this.activeFlag)) return true;
		//if ("0".equalsIgnoreCase(this.activeFlag)) return false;
		//return this.activeBooleanFlag;
		return "1".equalsIgnoreCase(this.activeFlag);
	}
	
	public void setActiveBooleanFlag (boolean activeBooleanFlag) {
		this.activeBooleanFlag = activeBooleanFlag;
		if (activeBooleanFlag) {
			this.activeFlag = "1";
		} else {
			this.activeFlag = "0";
		}		
	}	
	
	public boolean getAdminBooleanFlag() {
		//if ("1".equalsIgnoreCase(this.adminFlag)) return true;
		//if ("0".equalsIgnoreCase(this.adminFlag)) return false;
		//return this.adminBooleanFlag;
		return "1".equalsIgnoreCase(this.adminFlag);
	}
	
	public void setAdminBooleanFlag (boolean adminBooleanFlag) {
		this.adminBooleanFlag = adminBooleanFlag;
		if (adminBooleanFlag) {
			this.adminFlag = "1";
		} else {
			this.adminFlag = "0";
		}		
	}	
	
	public boolean getDefaultMatrixLineBooleanFlag() {
		//if ("1".equalsIgnoreCase(this.defaultMatrixLineFlag)) return true;
		//if ("0".equalsIgnoreCase(this.defaultMatrixLineFlag)) return false;
		//return this.defaultMatrixLineBooleanFlag;
		return "1".equalsIgnoreCase(this.defaultMatrixLineFlag);
	}
	
	public void setDefaultMatrixLineBooleanFlag (boolean defaultMatrixLineBooleanFlag) {
		this.defaultMatrixLineBooleanFlag = defaultMatrixLineBooleanFlag;
		if (defaultMatrixLineBooleanFlag) {
			this.defaultMatrixLineFlag = "1";
		} else {
			this.defaultMatrixLineFlag = "0";
		}		
	}
	
	 /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof User)) {
            return false;
        }

        final User revision = (User)other;

        String c1 = getUserCode();
        String c2 = revision.getUserCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }
        
        String e1 = getUserName();
        String e2 = revision.getUserName();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		String c1 = getUserCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());  
		String dnc1 = getUserName();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
		return hash;
    }
	
	/**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.userCode + "::" + this.userName;
    }

	public Set<UserEntity> getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(Set<UserEntity> userEntity) {
		this.userEntity = userEntity;
	}

	public Set<ImportSpecUser> getImportSpecUser() {
		return importSpecUser;
	}

	public void setImportSpecUser(Set<ImportSpecUser> importSpecUser) {
		this.importSpecUser = importSpecUser;
	}

	public String getLastModule() {
		return lastModule;
	}

	public void setLastModule(String lastModule) {
		this.lastModule = lastModule;
	}

	public String getGlobalViewFlag() {
		return globalViewFlag;
	}

	public void setGlobalViewFlag(String globalViewFlag) {
		this.globalViewFlag = globalViewFlag;
	}
	
	public String getLastPurchView() {
		return lastPurchView;
	}

	public void setLastPurchView(String lastPurchView) {
		this.lastPurchView = lastPurchView;
	}
	
	public boolean getGlobalViewBooleanFlag() {
		return "1".equalsIgnoreCase(this.globalViewFlag);
	}
	
	public void setGlobalViewBooleanFlag (boolean globalViewBooleanFlag) {
		this.globalViewBooleanFlag = globalViewBooleanFlag;
		if (globalViewBooleanFlag) {
			this.globalViewFlag = "1";
		} else {
			this.globalViewFlag = "0";
		}		
	}

	public boolean getViewOnlyBooleanFlag() {
		return "1".equals(viewOnlyFlag);
	}

	public void setViewOnlyBooleanFlag(boolean viewOnlyFlag) {
		this.viewOnlyFlag = (viewOnlyFlag)? "1":"0";
	}

	public String getViewOnlyFlag() {
		return viewOnlyFlag;
	}

	public void setViewOnlyFlag(String viewOnlyFlag) {
		this.viewOnlyFlag = viewOnlyFlag;
	}
}

