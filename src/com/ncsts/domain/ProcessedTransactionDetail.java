package com.ncsts.domain;

import java.util.Date;

public class ProcessedTransactionDetail {
	private Long matrixId;
	private Date asOfDate;

	private Long totalTransactions;
	private Long beforeAsOfDateTransactions;
	private Long extractedGlTransactions;
	private Long nonExtractedGlTransactions;
	private Long manuallyChangedTransactions;
	
	public Long getMatrixId() {
		return matrixId;
	}
	
	public void setMatrixId(Long matrixId) {
		this.matrixId = matrixId;
	}
	
	public Date getAsOfDate() {
		return asOfDate;
	}
	
	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}
	
	public Long getTotalTransactions() {
		return totalTransactions;
	}
	
	public void setTotalTransactions(Long totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	
	public Long getBeforeAsOfDateTransactions() {
		return beforeAsOfDateTransactions;
	}
	
	public void setBeforeAsOfDateTransactions(Long beforeAsOfDateTransactions) {
		this.beforeAsOfDateTransactions = beforeAsOfDateTransactions;
	}
	
	public Long getExtractedGlTransactions() {
		return extractedGlTransactions;
	}
	
	public void setExtractedGlTransactions(Long extractedGlTransactions) {
		this.extractedGlTransactions = extractedGlTransactions;
	}
	
	public Long getNonExtractedGlTransactions() {
		return nonExtractedGlTransactions;
	}
	
	public void setNonExtractedGlTransactions(Long nonExtractedGlTransactions) {
		this.nonExtractedGlTransactions = nonExtractedGlTransactions;
	}
	
	public Long getManuallyChangedTransactions() {
		return manuallyChangedTransactions;
	}

	public void setManuallyChangedTransactions(Long manuallyChangedTransactions) {
		this.manuallyChangedTransactions = manuallyChangedTransactions;
	}
}
