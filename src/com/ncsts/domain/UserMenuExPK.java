package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserMenuExPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="USER_CODE")  	
	private String userCode; 
	   
	@Column(name="ENTITY_ID") 	
	private Long entityId;
	
	
	@Column(name="MENU_CODE")  	
	private String menuCode; 
	
	public UserMenuExPK(){
		
	}
	
	public UserMenuExPK(String userCode, Long entityId, String menuCode){
		this.userCode = userCode;
		this.entityId = entityId;
		this.menuCode = menuCode;
	}
	
	@Column(name="userCode")
	public String getUserCode() {
		return userCode;
	}

	@Column(name="entityId")
	public Long getEntityId() {
		return entityId;
	}

	@Column(name="menuCode")
	public String getMenuCode() {
		return menuCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof UserMenuExPK)) {
            return false;
        }

        final UserMenuExPK revision = (UserMenuExPK)other;

        String dnc1 = getUserCode();
        String dnc2 = revision.getUserCode();
        if (!((dnc1 == dnc2) || (dnc1 != null && dnc1.equals(dnc2)))) {
            return false;
        }

        Long d1 = getEntityId();
        Long d2 = revision.getEntityId();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }
        
        String dc1 = getMenuCode();
        String dc2 = revision.getMenuCode();
        if (!((dc1 == dc2) || (dc1 != null && dc1.equals(dc2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String dnc1 = getUserCode();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
        Long d1 = getEntityId();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		String dc1 = getMenuCode();
		hash = 31 * hash + (null == dc1 ? 0 : dc1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.userCode + "::" + this.entityId +"::" + this.menuCode;
    }
	
}
