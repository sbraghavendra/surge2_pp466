package com.ncsts.domain;

/*
 * Author: Jim M. Wilson
 * Created: Aug 22, 2008
 * $Date: 2008-09-16 17:05:12 -0500 (Tue, 16 Sep 2008) $: - $Revision: 2249 $: 
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Embeddable
public class BCPTransactionDetailPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="TRANSACTION_DETAIL_ID")
  private Long transactionDetailId;

@Column(name="PROCESS_BATCH_NO")
  private Long processBatchNo;

	public BCPTransactionDetailPK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if ((other == null) || !(other instanceof BCPTransactionDetailPK)) {
			return false;
		}

		final BCPTransactionDetailPK revision = (BCPTransactionDetailPK) other;

		return new EqualsBuilder().append(processBatchNo, revision.processBatchNo).append(transactionDetailId, revision.transactionDetailId).isEquals();
}

	/**
	 * 
	 */
	public int hashCode() {
	 return	new HashCodeBuilder().append(processBatchNo).append(transactionDetailId).toHashCode();
	}

	/**
	 * 
	 * @return a string representation of this object.
	 */
	public String toString() {
		return this.processBatchNo+ "::" + this.transactionDetailId; 
	}

	public Long getTransactionDetailId() {
		return this.transactionDetailId;
	}

	public void setTransactionDetailId(Long transactionDetailId) {
		this.transactionDetailId = transactionDetailId;
	}

	public Long getProcessBatchNo() {
		return this.processBatchNo;
	}

	public void setProcessBatchNo(Long processBatchNo) {
		this.processBatchNo = processBatchNo;
	}

}
