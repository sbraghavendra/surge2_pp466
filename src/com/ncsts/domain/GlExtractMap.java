package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

import com.ncsts.dto.GlExtractMapDTO;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="TB_GL_EXTRACT_MAP")
public class GlExtractMap extends Matrix implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final long DRIVER_COUNT = 10L;
	public static final String DRIVER_CODE = "E";

	@Id
	@Column(name="GL_EXTRACT_MAP_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE, generator="gl_sequence")
	@SequenceGenerator(name="gl_sequence" , allocationSize = 1, sequenceName="SQ_TB_GL_EXTRACT_MAP_ID")
	private Long glExtractMapId;
	
	@Column(name="TAXCODE_STATE_CODE")
	private String taxCodeStateCode;
	
	@Column(name="TAXCODE_TYPE_CODE")
	private String taxCodeTypeCode;
	
	@Column(name="TAXCODE_CODE")
	private String taxCodeCode;
	
	@Column(name="TAX_JURIS_TYPE_CODE")
	private String taxJurisTypeCode;
	
	@Column(name="DRIVER_01")
	private String driver01; 
	
	@Column(name="DRIVER_02")
	private String driver02;
	
	@Column(name="DRIVER_03")
	private String driver03;
	
	@Column(name="DRIVER_04")
	private String driver04;
	
	@Column(name="DRIVER_05")
	private String driver05;
	
	@Column(name="DRIVER_06")
	private String driver06;
	
	@Column(name="DRIVER_07")
	private String driver07;
	
	@Column(name="DRIVER_08")
	private String driver08;
	
	@Column(name="DRIVER_09")
	private String driver09;
	
	@Column(name="DRIVER_10")
	private String driver10;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="TAXCODE_COUNTRY_CODE")
	private String taxcodeCountryCode;

	
	//Midtier project code added - january 2009
	//@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="TAXCODE_STATE_CODE",insertable=false, updatable=false)
	//@ForeignKey(name="tb_gl_extract_map_fk01")
	//private TaxCodeState taxCodeState;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns( {
		@JoinColumn(name="TAXCODE_STATE_CODE", referencedColumnName="TAXCODE_STATE_CODE", insertable=false, updatable=false),
		@JoinColumn(name="TAXCODE_COUNTRY_CODE", referencedColumnName="TAXCODE_COUNTRY_CODE", insertable=false, updatable=false) } )
		@ForeignKey(name="tb_gl_extract_map_fk01")
	private TaxCodeState taxCodeState;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TAXCODE_CODE", insertable=false, updatable=false)
	@ForeignKey(name="tb_gl_extract_map_fk02")
	private TaxCode taxCode;		
	
	@Transient	
	GlExtractMapDTO glExtractMapDTO;
	
	
	public GlExtractMapDTO getGlExtractMapDTO() {
		GlExtractMapDTO gl = new GlExtractMapDTO();
		BeanUtils.copyProperties(this, gl);
		return gl;
	}
	public void setGlExtractMapDTO(GlExtractMapDTO glExtractMapDTO) {
		this.glExtractMapDTO = glExtractMapDTO;
	}
	public Long getGlExtractMapId() {
		return glExtractMapId;
	}
	public void setGlExtractMapId(Long glExtractMapId) {
		this.glExtractMapId = glExtractMapId;
	}
	public String getTaxCodeStateCode() {
		return taxCodeStateCode;
	}
	public void setTaxCodeStateCode(String taxCodeStateCode) {
		this.taxCodeStateCode = taxCodeStateCode;
	}
	public String getTaxCodeTypeCode() {
		return taxCodeTypeCode;
	}
	public void setTaxCodeTypeCode(String taxCodeTypeCode) {
		this.taxCodeTypeCode = taxCodeTypeCode;
	}
	public String getTaxCodeCode() {
		return taxCodeCode;
	}
	public void setTaxCodeCode(String taxCodeCode) {
		this.taxCodeCode = taxCodeCode;
	}
	public String getTaxJurisTypeCode() {
		return taxJurisTypeCode;
	}
	public void setTaxJurisTypeCode(String taxJurisTypeCode) {
		this.taxJurisTypeCode = taxJurisTypeCode;
	}

	public String getDriver01() {
		return driver01;
	}
	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}
	public String getDriver02() {
		return driver02;
	}
	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}
	public String getDriver03() {
		return driver03;
	}
	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}
	public String getDriver04() {
		return driver04;
	}
	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}
	public String getDriver05() {
		return driver05;
	}
	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}
	public String getDriver06() {
		return driver06;
	}
	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}
	public String getDriver07() {
		return driver07;
	}
	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}
	public String getDriver08() {
		return driver08;
	}
	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}
	public String getDriver09() {
		return driver09;
	}
	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}
	public String getDriver10() {
		return driver10;
	}
	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getTaxcodeCountryCode() {
		return taxcodeCountryCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
		this.taxcodeCountryCode = taxcodeCountryCode;
	}

    // Idable interface
    public Long getId() {
    	return glExtractMapId;
    }
    
    public void setId(Long id) {
    	this.glExtractMapId = id;
    }
    
    public String getIdPropertyName() {
    	return "glExtractMapId";
    }
    // Matrix overrides
    @Override
    public Long getDriverCount() {
    	return DRIVER_COUNT;
    }
    
    @Override
    public String getDriverCode() {
    	return DRIVER_CODE;
    }

    @Override
    public boolean getHasDescriptions() {
    	return false;
    }
	@Override
	public Date getEffectiveDate() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Date getExpirationDate() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setEffectiveDate(Date effectiveDate) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setExpirationDate(Date expirationDate) {
		// TODO Auto-generated method stub
		
	}
	public boolean hasDriverSet() {
		if ((driver01 != null) && (driver01.trim().length() > 0)) return true;
		if ((driver02 != null) && (driver02.trim().length() > 0)) return true;
		if ((driver03 != null) && (driver03.trim().length() > 0)) return true;
		if ((driver04 != null) && (driver04.trim().length() > 0)) return true;
		if ((driver05 != null) && (driver05.trim().length() > 0)) return true;
		if ((driver06 != null) && (driver06.trim().length() > 0)) return true;
		if ((driver07 != null) && (driver07.trim().length() > 0)) return true;
		if ((driver08 != null) && (driver08.trim().length() > 0)) return true;
		if ((driver09 != null) && (driver09.trim().length() > 0)) return true;
		if ((driver10 != null) && (driver10.trim().length() > 0)) return true;
		return false;
	}
	public TaxCodeState getTaxCodeState() {
		return taxCodeState;
	}
	public void setTaxCodeState(TaxCodeState taxCodeState) {
		this.taxCodeState = taxCodeState;
	}
	public TaxCode getTaxCode() {
		return taxCode;
	}
	public void setTaxCode(TaxCode taxCode) {
		this.taxCode = taxCode;
	}

}
