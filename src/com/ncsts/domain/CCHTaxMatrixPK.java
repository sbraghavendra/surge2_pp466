package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CCHTaxMatrixPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="GROUPCODE")
	private String groupCode;
	@Column(name="RECTYPE")
	private String recType;
	@Column(name="ITEM")
	private String item;
	
	
	public CCHTaxMatrixPK() {
		
	}

	public CCHTaxMatrixPK(String groupCode, String recType, String item) {
		this.groupCode = groupCode;
		this.recType = recType;
		this.item = item;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getRecType() {
		return recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof CCHTaxMatrixPK)) {
            return false;
        }

        final CCHTaxMatrixPK revision = (CCHTaxMatrixPK)other;

        String r1 = getRecType();
        String r2 = revision.getRecType();
        if (!((r1 == r2) || (r1 != null && r1.equals(r2)))) {
            return false;
        }

        String g1 = getGroupCode();
        String g2 = revision.getGroupCode();
        if (!((g1 == g2) || (g1 != null && g1.equals(g2)))) {
            return false;
        }
        
        String i1 = getItem();
        String i2 = revision.getItem();
        if (!((i1 == i2) || (i1 != null && i1.equals(i2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String r1 = getRecType();
		hash = 31 * hash + (null == r1 ? 0 : r1.hashCode());
        String g1 = getGroupCode();
		hash = 31 * hash + (null == g1 ? 0 : g1.hashCode());
        String i1 = getItem();
		hash = 31 * hash + (null == i1 ? 0 : i1.hashCode());
		return hash;
    }

}
