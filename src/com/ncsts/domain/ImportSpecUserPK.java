package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
/**
 * 
 * @author Muneer Basha
 *
 */
@Embeddable
public class ImportSpecUserPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

  @Column(name="IMPORT_SPEC_TYPE")  	
	private String importSpecType; 
	   
	@Column(name="IMPORT_SPEC_CODE") 	
	private String importSpecCode;
	
	@Column(name="USER_CODE") 	
	private String userCode;
	
	public ImportSpecUserPK(){
		
	}
	
	public ImportSpecUserPK(String importSpecType, String importSpecCode,String userCode) {
		this.importSpecType = importSpecType;
		this.importSpecCode = importSpecCode;
		this.userCode = userCode;
	}

	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof ImportSpecUserPK)) {
            return false;
        }

        final ImportSpecUserPK revision = (ImportSpecUserPK)other;

        String t1 = getImportSpecType();
        String t2 = revision.getImportSpecType();
        if (!((t1 == t2) || (t1 != null && t1.equals(t2)))) {
            return false;
        }

        String c1 = getImportSpecCode();
        String c2 = revision.getImportSpecCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        String u1 = getUserCode();
        String u2 = revision.getUserCode();
        if (!((u1 == u2) || (u1 != null && u1.equals(u2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String t1 = getImportSpecType();
		hash = 31 * hash + (null == t1 ? 0 : t1.hashCode());
        String c1 = getImportSpecCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
        String u1 = getUserCode();
		hash = 31 * hash + (null == u1 ? 0 : u1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.importSpecType + "::" + this.importSpecCode+"::"+this.userCode;
    }

	public String getImportSpecType() {
		return importSpecType;
	}

	public void setImportSpecType(String importSpecType) {
		this.importSpecType = importSpecType;
	}

	public String getImportSpecCode() {
		return importSpecCode;
	}

	public void setImportSpecCode(String importSpecCode) {
		this.importSpecCode = importSpecCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

}
