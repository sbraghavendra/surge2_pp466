package com.ncsts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TB_TMP_TAX_MTRX_DRIVERS_COUNT")
public class TempTaxMatrixCount {
	
	@Id
	@Column(name="DRIVER_01")
    private Long driver01;
    
    @Column(name="DRIVER_02" )
    private Long driver02;
    
    @Column(name="DRIVER_03" )
    private Long driver03;
    
    @Column(name="DRIVER_04" )
    private Long driver04;
    
    @Column(name="DRIVER_05" )
    private Long driver05;
  
    @Column(name="DRIVER_06" )
    private Long driver06;
    
    @Column(name="DRIVER_07" )
    private Long driver07;
    
    @Column(name="DRIVER_08" )
    private Long driver08;
    
    @Column(name="DRIVER_09" )
    private Long driver09;
    
    @Column(name="DRIVER_10" )
    private Long driver10;
   
    @Column(name="DRIVER_11" )
    private Long driver11;
   
    @Column(name="DRIVER_12" )
    private Long driver12;
    
    @Column(name="DRIVER_13" )
    private Long driver13;
    
    @Column(name="DRIVER_14" )
    private Long driver14;
   
    @Column(name="DRIVER_15" )
    private Long driver15;
    
    @Column(name="DRIVER_16" )
    private Long driver16;
        
    @Column(name="DRIVER_17" )
	private Long driver17;
            
	@Column(name="DRIVER_18")
	private Long driver18;
           
	@Column(name="DRIVER_19")
	private Long driver19;
	
	@Column(name="DRIVER_20")
	private Long driver20;
	
	@Column(name="DRIVER_21")
	private Long driver21;
	
	@Column(name="DRIVER_22")
	private Long driver22;
	
	@Column(name="DRIVER_23")
	private Long driver23;
	
	@Column(name="DRIVER_24")
	private Long driver24;
	
	@Column(name="DRIVER_25")
	private Long driver25;
	
	@Column(name="DRIVER_26")
	private Long driver26;
	
	@Column(name="DRIVER_27")
	private Long driver27;
	
	@Column(name="DRIVER_28")
	private Long driver28;
	
	@Column(name="DRIVER_29")
	private Long driver29;
	
	@Column(name="DRIVER_30")
	private Long driver30;

	public Long getDriver01() {
		return driver01;
	}

	public Long getDriver02() {
		return driver02;
	}

	public Long getDriver03() {
		return driver03;
	}

	public Long getDriver04() {
		return driver04;
	}

	public Long getDriver05() {
		return driver05;
	}

	public Long getDriver06() {
		return driver06;
	}

	public Long getDriver07() {
		return driver07;
	}

	public Long getDriver08() {
		return driver08;
	}

	public Long getDriver09() {
		return driver09;
	}

	public Long getDriver10() {
		return driver10;
	}

	public Long getDriver11() {
		return driver11;
	}

	public Long getDriver12() {
		return driver12;
	}

	public Long getDriver13() {
		return driver13;
	}

	public Long getDriver14() {
		return driver14;
	}

	public Long getDriver15() {
		return driver15;
	}

	public Long getDriver16() {
		return driver16;
	}

	public Long getDriver17() {
		return driver17;
	}

	public Long getDriver18() {
		return driver18;
	}

	public Long getDriver19() {
		return driver19;
	}

	public Long getDriver20() {
		return driver20;
	}

	public Long getDriver21() {
		return driver21;
	}

	public Long getDriver22() {
		return driver22;
	}

	public Long getDriver23() {
		return driver23;
	}

	public Long getDriver24() {
		return driver24;
	}

	public Long getDriver25() {
		return driver25;
	}

	public Long getDriver26() {
		return driver26;
	}

	public Long getDriver27() {
		return driver27;
	}

	public Long getDriver28() {
		return driver28;
	}

	public Long getDriver29() {
		return driver29;
	}

	public Long getDriver30() {
		return driver30;
	}

	public void setDriver01(Long driver01) {
		this.driver01 = driver01;
	}

	public void setDriver02(Long driver02) {
		this.driver02 = driver02;
	}

	public void setDriver03(Long driver03) {
		this.driver03 = driver03;
	}

	public void setDriver04(Long driver04) {
		this.driver04 = driver04;
	}

	public void setDriver05(Long driver05) {
		this.driver05 = driver05;
	}

	public void setDriver06(Long driver06) {
		this.driver06 = driver06;
	}

	public void setDriver07(Long driver07) {
		this.driver07 = driver07;
	}

	public void setDriver08(Long driver08) {
		this.driver08 = driver08;
	}

	public void setDriver09(Long driver09) {
		this.driver09 = driver09;
	}

	public void setDriver10(Long driver10) {
		this.driver10 = driver10;
	}

	public void setDriver11(Long driver11) {
		this.driver11 = driver11;
	}

	public void setDriver12(Long driver12) {
		this.driver12 = driver12;
	}

	public void setDriver13(Long driver13) {
		this.driver13 = driver13;
	}

	public void setDriver14(Long driver14) {
		this.driver14 = driver14;
	}

	public void setDriver15(Long driver15) {
		this.driver15 = driver15;
	}

	public void setDriver16(Long driver16) {
		this.driver16 = driver16;
	}

	public void setDriver17(Long driver17) {
		this.driver17 = driver17;
	}

	public void setDriver18(Long driver18) {
		this.driver18 = driver18;
	}

	public void setDriver19(Long driver19) {
		this.driver19 = driver19;
	}

	public void setDriver20(Long driver20) {
		this.driver20 = driver20;
	}

	public void setDriver21(Long driver21) {
		this.driver21 = driver21;
	}

	public void setDriver22(Long driver22) {
		this.driver22 = driver22;
	}

	public void setDriver23(Long driver23) {
		this.driver23 = driver23;
	}

	public void setDriver24(Long driver24) {
		this.driver24 = driver24;
	}

	public void setDriver25(Long driver25) {
		this.driver25 = driver25;
	}

	public void setDriver26(Long driver26) {
		this.driver26 = driver26;
	}

	public void setDriver27(Long driver27) {
		this.driver27 = driver27;
	}

	public void setDriver28(Long driver28) {
		this.driver28 = driver28;
	}

	public void setDriver29(Long driver29) {
		this.driver29 = driver29;
	}

	public void setDriver30(Long driver30) {
		this.driver30 = driver30;
	}

}
