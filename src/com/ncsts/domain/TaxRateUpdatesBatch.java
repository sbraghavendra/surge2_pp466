/*
 * Author: Jim M. Wilson
 * Created: Sep 24, 2008
 * $Date: 2009-11-25 11:43:58 -0600 (Wed, 25 Nov 2009) $: - $Revision: 4428 $: 
 */

package com.ncsts.domain;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 * A useful class for dealing with tax rate updates batches
 */
@SuppressWarnings("serial")
public class TaxRateUpdatesBatch extends BatchMaintenance 
implements Comparable<TaxRateUpdatesBatch> {
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(TaxRateUpdatesBatch.class);

	public static final String IMPORTING = "IX";
	public static final String IMPORTED= "I";
	public static final String PROCESS = "FP";
	public static final String PROCESSING = "XTR";
	public static final String BATCH_CODE = "TR";
	public static final String IMPORT= "I";
	//Fixed for issue 0002743
	public static final String ABORTED="ABI";

	/* a convenient place to cache an error */
	private String error;
	
	public TaxRateUpdatesBatch() {
		super();
	}
	public TaxRateUpdatesBatch(BatchMaintenance batch) {
		this();
		if (batch != null) BeanUtils.copyProperties(batch, this);
	}
	
	public int getReleaseVersion() {
		if (getNu01() == null) return 0;
		return getNu01().intValue();
	}
	public void setReleaseVersion(int version) {
		setNu01(new Double(version));
	}
	
	/* force some Timestamps to be converted to Date's */
	public Date getReleaseDate() {
		if (getTs01() != null)
			return new Date(getTs01().getTime());
		return null;
	}
	public void setReleaseDate(Date d) {
		setTs01(d);
	}
		
	public boolean isFullType() {
		return "Full".equalsIgnoreCase(getVc02());
	}
	public boolean isUpdateType() {
		return "Update".equalsIgnoreCase(getVc02());
	}
	public boolean isImported() {
		return IMPORTED.equalsIgnoreCase(getBatchStatusCode());
	}
	public boolean canBeProcessed() {
		return (IMPORTED.equalsIgnoreCase(getBatchStatusCode())
  			&& !isHeld());
  }
	public static boolean canBeProcessed(BatchMaintenance batch) {
		return (IMPORTED.equalsIgnoreCase(batch.getBatchStatusCode())
  			&& !batch.isHeld());
	}
	
	/* is this batch the next in sequence by date */
	public boolean isNextInSequence(TaxRateUpdatesBatch previous) {
		// Must have both dates
		if ((getReleaseDate() == null) || (previous.getReleaseDate() == null)) return false;
		// This has to be later than previous
		if (getReleaseDate().getTime() < previous.getReleaseDate().getTime())
			return false;
		
		Calendar thisCal = Calendar.getInstance();
		thisCal.setTime(getReleaseDate());
		Calendar prevCal = Calendar.getInstance();
		prevCal.setTime(previous.getReleaseDate());
		switch(thisCal.get(Calendar.YEAR) - prevCal.get(Calendar.YEAR)) {
		case 1: // Previous was last year - has to be Dec/Jan
			return thisCal.get(Calendar.MONTH) == Calendar.JANUARY
					&& prevCal.get(Calendar.MONTH) == Calendar.DECEMBER;
		case 0: // Previous was during this year
			switch(thisCal.get(Calendar.MONTH) - prevCal.get(Calendar.MONTH)) {
			case 1: // Previous month
				return getReleaseVersion() == 1;
			case 0: // Same month
				return getReleaseVersion() - previous.getReleaseVersion() == 1;
			default: // Anything else
				return false;
			}
		default:
			return false;
		}
	}

	public boolean isRunning() {
		return "X".equals(getBatchStatusCode().substring(0, 1));
	}

	public boolean isFlagged() {
		return "F".equals(getBatchStatusCode().substring(0, 1));
	}
	
	public String getError() {
		return this.error;
	}
	
	public void setError(String error) {
		this.error = error;
	}
	
	/**
	 * Must be a better way to do this in hibernate
	 */
	public BatchMaintenance getBatchMaintenance() {
		return new BatchMaintenance((BatchMaintenance)this);
	}

	/**
	 * impart an ordering on batches
	 * 1) First by date
	 * 2) then by release 
	 */
	public int compareTo(TaxRateUpdatesBatch other) {
		if (getReleaseDate().before(other.getReleaseDate())) 
			return -1;
		if (getReleaseDate().after(other.getReleaseDate())) 
			return 1;
		if (getReleaseVersion() < other.getReleaseVersion()) 
			return -1;
		if (getReleaseVersion() > other.getReleaseVersion()) 
			return 1;
		return 0;
	}
	
	public boolean equals(Object batch) {
		return (batch instanceof BatchMaintenance) && this.getBatchId().equals(((BatchMaintenance)batch).getBatchId());
	}
	public int hashCode() {
		return getBatchId().hashCode();
	}

}
