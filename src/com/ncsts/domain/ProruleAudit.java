package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;


@Entity
@Table(name="TB_PROCRULE_AUDIT")
public class ProruleAudit extends Auditable implements Serializable, Idable<Long>  {
	
	
	@Id
	@Column(name="PROCRULE_AUDIT_ID")
	@GeneratedValue (strategy = GenerationType.SEQUENCE , generator="procaudit_sequence")
	@SequenceGenerator(name="procaudit_sequence" , allocationSize = 1, sequenceName="sq_tb_procaudit_id")
	private Long procauditId;

	//@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="PROCRULE_ID", referencedColumnName="PROCRULE_ID", insertable=false, updatable=false)
	//@ForeignKey(name="tb_procrule_audit_fk01")
	@XmlTransient
	//private Procrule procruleId;
	
	@Column(name="PROCRULE_ID")
	private Procrule procruleId;
	
	@Column(name="PROCRULE_DETAIL_ID")
	private Long procruledetailId;
	
	@Column(name="LINE_NO")
	private Long lineNo;
	
	@Column(name="TRANSACTION_ID")
	private Long transactionId;
	
	@Column(name="COLUMN_NAME")
	private String columnName;
	
	@Column(name="BEFORE_VALUE")
	private String beforeValue;

	@Column(name="AFTER_VALUE")
	private String afterValue;
	
	@Column(name="ANON_SQL")
	private String anonSql;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PROCESS_TIMESTAMP")
	private Date processTimestamp;
	
	@Column(name="UPDATE_FLAG")
	private String updateFlag;
	
	@Column(name="SESSION_ID")
	private String sessionID;
	
	@Column(name="TABLE_NAME")
	private String tableName;
	
	@Transient	
	private String listcodedescription;
	
	@Transient
	private String procruleName;
	
	@Transient	
	private String proruledescription;
	
	@Transient	
	private String datadefdescription;

	public Long getId() {
		
		return getProcauditId();
	}


	public void setId(Long id) {
		
		setProcauditId(id);
	}

	public String getProruledescription() {
		return proruledescription;
	}


	public void setProruledescription(String proruledescription) {
		this.proruledescription = proruledescription;
	}


	public String getIdPropertyName() {
		
		return "procauditId";
	}


	public Long getProcauditId() {
		return procauditId;
	}


	public void setProcauditId(Long procauditId) {
		this.procauditId = procauditId;
	}

	public String getListcodedescription() {
		return listcodedescription;
	}


	public void setListcodedescription(String listcodedescription) {
		this.listcodedescription = listcodedescription;
	}





	public Procrule getProcruleId() {
		return procruleId;
	}


	public void setProcruleId(Procrule procruleId) {
		this.procruleId = procruleId;
	}


	public Long getProcruledetailId() {
		return procruledetailId;
	}


	public void setProcruledetailId(Long procruledetailId) {
		this.procruledetailId = procruledetailId;
	}


	public Long getLineNo() {
		return lineNo;
	}


	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}


	public Long getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}


	public String getColumnName() {
		return columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public String getBeforeValue() {
		return beforeValue;
	}


	public void setBeforeValue(String beforeValue) {
		this.beforeValue = beforeValue;
	}


	public String getAfterValue() {
		return afterValue;
	}


	public void setAfterValue(String afterValue) {
		this.afterValue = afterValue;
	}


	public String getAnonSql() {
		return anonSql;
	}


	public void setAnonSql(String anonSql) {
		this.anonSql = anonSql;
	}


	public Date getProcessTimestamp() {
		return processTimestamp;
	}


	public void setProcessTimestamp(Date processTimestamp) {
		this.processTimestamp = processTimestamp;
	}


	public String getUpdateFlag() {
		return updateFlag;
	}


	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}


	public String getSessionID() {
		return sessionID;
	}


	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}


	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getDatadefdescription() {
		return datadefdescription;
	}


	public void setDatadefdescription(String datadefdescription) {
		this.datadefdescription = datadefdescription;
	}
	
	public String getProcruleName() {
		return this.procruleName;
	}
	
	public void setProcruleName(String procruleName) {
		this.procruleName = procruleName;
	}
}
