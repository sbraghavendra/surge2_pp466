
package com.ncsts.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "TB_PURCHTRANS_LOG")
public class PurchaseTransactionLog extends Auditable implements Serializable, Idable<Long> {

    private static final long serialVersionUID = 1L;

    @Transient
	private Long rownumber;	
    
    @Id
    @Column(name = "PURCHTRANS_LOG_ID", nullable = false)
    @GeneratedValue (strategy = GenerationType.AUTO , generator="purch_trans_log_sequence")
    @SequenceGenerator(name="purch_trans_log_sequence" , allocationSize = 1, sequenceName="sq_tb_purchtrans_log_id")
    private Long purchtransLogId;

    @Column(name = "PURCHTRANS_ID")
    private Long purchtransId;

    @Column(name = "LOG_SOURCE", length = 40)
    private String logSource;

    @Column(name = "GL_EXTRACT_BATCH_NO")
    private Long glExtractBatchNo;

    @Temporal(TemporalType.DATE)
    @Column(name = "GL_EXTRACT_TIMESTAMP")
    private Date glExtractTimestamp;

    @Column(name = "DIRECT_PAY_PERMIT_FLAG", length = 1)
    private String directPayPermitFlag;

    @Column(name = "CALCULATE_IND", length = 10)
    private String calculateInd;

    @Temporal(TemporalType.DATE)
    @Column(name = "GL_DATE")
    private Date glDate;

    @Column(name = "GL_LINE_ITM_DIST_AMT")
    private BigDecimal glLineItmDistAmt;

    @Column(name = "ALLOCATION_MATRIX_ID")
    private Long allocationMatrixId;

    @Column(name = "ALLOCATION_SUBTRANS_ID")
    private Long allocationSubtransId;

    @Column(name = "TAX_ALLOC_MATRIX_ID")
    private Long taxAllocMatrixId;

    @Column(name = "SPLIT_SUBTRANS_ID")
    private Long splitSubtransId;

    @Column(name = "MULTI_TRANS_CODE", length = 10)
    private String multiTransCode;

 /*   @Column(name = "LOCATION_MATRIX_ID")
    private Long locationMatrixId;*/

/*
    @Column(name = "MANUAL_JURISDICTION_IND", length = 10)
    private String manualJurisdictionInd;
*/

    @Column(name = "TAXCODE_CODE", length = 40)
    private String taxcodeCode;

    @Column(name = "MANUAL_TAXCODE_IND", length = 10)
    private String manualTaxcodeInd;

    @Column(name = "TAX_MATRIX_ID")
    private Long taxMatrixId;

    @Column(name = "COMMENTS", length = 4000)
    private String comments;

    @Column(name = "PROCESSING_NOTES", length = 4000)
    private String processingNotes;

    @Column(name = "TRANSACTION_STATE_CODE", length = 10)
    private String transactionStateCode;

    @Column(name = "AUTO_TRANSACTION_STATE_CODE", length = 10)
    private String autoTransactionStateCode;

    @Column(name = "TRANSACTION_COUNTRY_CODE", length = 10)
    private String transactionCountryCode;

    @Column(name = "AUTO_TRANSACTION_COUNTRY_CODE", length = 10)
    private String autoTransactionCountryCode;

    @Column(name = "TRANSACTION_IND", length = 10)
    private String transactionInd;

    @Column(name = "SUSPEND_IND", length = 10)
    private String suspendInd;

    @Column(name = "REJECT_FLAG", length = 1)
    private String rejectFlag;

    @Column(name = "COUNTRY_TIER1_TAXABLE_AMT")
    private BigDecimal countryTier1TaxableAmt;

    @Column(name = "COUNTRY_TIER2_TAXABLE_AMT")
    private BigDecimal countryTier2TaxableAmt;

    @Column(name = "COUNTRY_TIER3_TAXABLE_AMT")
    private BigDecimal countryTier3TaxableAmt;

    @Column(name = "STATE_TIER1_TAXABLE_AMT")
    private BigDecimal stateTier1TaxableAmt;

    @Column(name = "STATE_TIER2_TAXABLE_AMT")
    private BigDecimal stateTier2TaxableAmt;

    @Column(name = "STATE_TIER3_TAXABLE_AMT")
    private BigDecimal stateTier3TaxableAmt;

    @Column(name = "COUNTY_TIER1_TAXABLE_AMT")
    private BigDecimal countyTier1TaxableAmt;

    @Column(name = "COUNTY_TIER2_TAXABLE_AMT")
    private BigDecimal countyTier2TaxableAmt;

    @Column(name = "COUNTY_TIER3_TAXABLE_AMT")
    private BigDecimal countyTier3TaxableAmt;

    @Column(name = "CITY_TIER1_TAXABLE_AMT")
    private BigDecimal cityTier1TaxableAmt;

    @Column(name = "CITY_TIER2_TAXABLE_AMT")
    private BigDecimal cityTier2TaxableAmt;

    @Column(name = "CITY_TIER3_TAXABLE_AMT")
    private BigDecimal cityTier3TaxableAmt;

    @Column(name = "STJ1_TAXABLE_AMT")
    private BigDecimal stj1TaxableAmt;

    @Column(name = "STJ2_TAXABLE_AMT")
    private BigDecimal stj2TaxableAmt;

    @Column(name = "STJ3_TAXABLE_AMT")
    private BigDecimal stj3TaxableAmt;

    @Column(name = "STJ4_TAXABLE_AMT")
    private BigDecimal stj4TaxableAmt;

    @Column(name = "STJ5_TAXABLE_AMT")
    private BigDecimal stj5TaxableAmt;

    @Column(name = "STJ6_TAXABLE_AMT")
    private BigDecimal stj6TaxableAmt;

    @Column(name = "STJ7_TAXABLE_AMT")
    private BigDecimal stj7TaxableAmt;

    @Column(name = "STJ8_TAXABLE_AMT")
    private BigDecimal stj8TaxableAmt;

    @Column(name = "STJ9_TAXABLE_AMT")
    private BigDecimal stj9TaxableAmt;

    @Column(name = "STJ10_TAXABLE_AMT")
    private BigDecimal stj10TaxableAmt;


    @Column(name = "B_COUNTRY_TIER1_TAX_AMT")
    private BigDecimal bCountryTier1TaxAmt;

    @Column(name = "B_COUNTRY_TIER2_TAX_AMT")
    private BigDecimal bCountryTier2TaxAmt;

    @Column(name = "B_COUNTRY_TIER3_TAX_AMT")
    private BigDecimal bCountryTier3TaxAmt;

    @Column(name = "B_STATE_TIER1_TAX_AMT")
    private BigDecimal bStateTier1TaxAmt;

    @Column(name = "B_STATE_TIER2_TAX_AMT")
    private BigDecimal bStateTier2TaxAmt;

    @Column(name = "B_STATE_TIER3_TAX_AMT")
    private BigDecimal bStateTier3TaxAmt;

    @Column(name = "B_COUNTY_TIER1_TAX_AMT")
    private BigDecimal bCountyTier1TaxAmt;

    @Column(name = "B_COUNTY_TIER2_TAX_AMT")
    private BigDecimal bCountyTier2TaxAmt;

    @Column(name = "B_COUNTY_TIER3_TAX_AMT")
    private BigDecimal bCountyTier3TaxAmt;

    @Column(name = "B_CITY_TIER1_TAX_AMT")
    private BigDecimal bCityTier1TaxAmt;

    @Column(name = "B_CITY_TIER2_TAX_AMT")
    private BigDecimal bCityTier2TaxAmt;

    @Column(name = "B_CITY_TIER3_TAX_AMT")
    private BigDecimal bCityTier3TaxAmt;

    @Column(name = "B_STJ1_TAX_AMT")
    private BigDecimal bStj1TaxAmt;

    @Column(name = "B_STJ2_TAX_AMT")
    private BigDecimal bStj2TaxAmt;

    @Column(name = "B_STJ3_TAX_AMT")
    private BigDecimal bStj3TaxAmt;

    @Column(name = "B_STJ4_TAX_AMT")
    private BigDecimal bStj4TaxAmt;

    @Column(name = "B_STJ5_TAX_AMT")
    private BigDecimal bStj5TaxAmt;

    @Column(name = "B_STJ6_TAX_AMT")
    private BigDecimal bStj6TaxAmt;

    @Column(name = "B_STJ7_TAX_AMT")
    private BigDecimal bStj7TaxAmt;

    @Column(name = "B_STJ8_TAX_AMT")
    private BigDecimal bStj8TaxAmt;

    @Column(name = "B_STJ9_TAX_AMT")
    private BigDecimal bStj9TaxAmt;

    @Column(name = "B_STJ10_TAX_AMT")
    private BigDecimal bStj10TaxAmt;

    @Column(name = "B_COUNTY_LOCAL_TAX_AMT")
    private BigDecimal bCountyLocalTaxAmt;

    @Column(name = "B_CITY_LOCAL_TAX_AMT")
    private BigDecimal bCityLocalTaxAmt;

    @Column(name = "B_TB_CALC_TAX_AMT")
    private BigDecimal bTbCalcTaxAmt;


    @Column(name = "A_COUNTRY_TIER1_TAX_AMT")
    private BigDecimal aCountryTier1TaxAmt;

    @Column(name = "A_COUNTRY_TIER2_TAX_AMT")
    private BigDecimal aCountryTier2TaxAmt;

    @Column(name = "A_COUNTRY_TIER3_TAX_AMT")
    private BigDecimal aCountryTier3TaxAmt;

    @Column(name = "A_STATE_TIER1_TAX_AMT")
    private BigDecimal aStateTier1TaxAmt;

    @Column(name = "A_STATE_TIER2_TAX_AMT")
    private BigDecimal aStateTier2TaxAmt;

    @Column(name = "A_STATE_TIER3_TAX_AMT")
    private BigDecimal aStateTier3TaxAmt;

    @Column(name = "A_COUNTY_TIER1_TAX_AMT")
    private BigDecimal aCountyTier1TaxAmt;

    @Column(name = "A_COUNTY_TIER2_TAX_AMT")
    private BigDecimal aCountyTier2TaxAmt;

    @Column(name = "A_COUNTY_TIER3_TAX_AMT")
    private BigDecimal aCountyTier3TaxAmt;

    @Column(name = "A_CITY_TIER1_TAX_AMT")
    private BigDecimal aCityTier1TaxAmt;

    @Column(name = "A_CITY_TIER2_TAX_AMT")
    private BigDecimal aCityTier2TaxAmt;

    @Column(name = "A_CITY_TIER3_TAX_AMT")
    private BigDecimal aCityTier3TaxAmt;

    @Column(name = "A_STJ1_TAX_AMT")
    private BigDecimal aStj1TaxAmt;

    @Column(name = "A_STJ2_TAX_AMT")
    private BigDecimal aStj2TaxAmt;

    @Column(name = "A_STJ3_TAX_AMT")
    private BigDecimal aStj3TaxAmt;

    @Column(name = "A_STJ4_TAX_AMT")
    private BigDecimal aStj4TaxAmt;

    @Column(name = "A_STJ5_TAX_AMT")
    private BigDecimal aStj5TaxAmt;

    @Column(name = "A_STJ6_TAX_AMT")
    private BigDecimal aStj6TaxAmt;

    @Column(name = "A_STJ7_TAX_AMT")
    private BigDecimal aStj7TaxAmt;

    @Column(name = "A_STJ8_TAX_AMT")
    private BigDecimal aStj8TaxAmt;

    @Column(name = "A_STJ9_TAX_AMT")
    private BigDecimal aStj9TaxAmt;

    @Column(name = "A_STJ10_TAX_AMT")
    private BigDecimal aStj10TaxAmt;

    @Column(name = "A_COUNTY_LOCAL_TAX_AMT")
    private BigDecimal aCountyLocalTaxAmt;

    @Column(name = "A_CITY_LOCAL_TAX_AMT")
    private BigDecimal aCityLocalTaxAmt;

    @Column(name = "A_TB_CALC_TAX_AMT")
    private BigDecimal aTbCalcTaxAmt;


    @Column(name = "DISTR_01_AMT")
    private BigDecimal distr01Amt;

    @Column(name = "DISTR_02_AMT")
    private BigDecimal distr02Amt;

    @Column(name = "DISTR_03_AMT")
    private BigDecimal distr03Amt;

    @Column(name = "DISTR_04_AMT")
    private BigDecimal distr04Amt;

    @Column(name = "DISTR_05_AMT")
    private BigDecimal distr05Amt;

    @Column(name = "DISTR_06_AMT")
    private BigDecimal distr06Amt;

    @Column(name = "DISTR_07_AMT")
    private BigDecimal distr07Amt;

    @Column(name = "DISTR_08_AMT")
    private BigDecimal distr08Amt;

    @Column(name = "DISTR_09_AMT")
    private BigDecimal distr09Amt;

    @Column(name = "DISTR_10_AMT")
    private BigDecimal distr10Amt;

    @Column(name = "COUNTRY_SITUS_MATRIX_ID")
    private Long countrySitusMatrixId;

    @Column(name = "STATE_SITUS_MATRIX_ID")
    private Long stateSitusMatrixId;

    @Column(name = "COUNTY_SITUS_MATRIX_ID")
    private Long countySitusMatrixId;

    @Column(name = "CITY_SITUS_MATRIX_ID")
    private Long citySitusMatrixId;

    @Column(name = "STJ1_SITUS_MATRIX_ID")
    private Long stj1SitusMatrixId;

    @Column(name = "STJ2_SITUS_MATRIX_ID")
    private Long stj2SitusMatrixId;

    @Column(name = "STJ3_SITUS_MATRIX_ID")
    private Long stj3SitusMatrixId;

    @Column(name = "STJ4_SITUS_MATRIX_ID")
    private Long stj4SitusMatrixId;

    @Column(name = "STJ5_SITUS_MATRIX_ID")
    private Long stj5SitusMatrixId;

    @Column(name = "SHIPTO_LOCN_MATRIX_ID")
    private Long shiptoLocnMatrixId;

    @Column(name = "SHIPFROM_LOCN_MATRIX_ID")
    private Long shipfromLocnMatrixId;

    @Column(name = "ORDRACPT_LOCN_MATRIX_ID")
    private Long ordracptLocnMatrixId;

    @Column(name = "ORDRORGN_LOCN_MATRIX_ID")
    private Long ordrorgnLocnMatrixId;
    
    @Column(name = "FIRSTUSE_LOCN_MATRIX_ID")
    private Long firstuseLocnMatrixId;

    @Column(name = "BILLTO_LOCN_MATRIX_ID")
    private Long billtoLocnMatrixId;

    @Column(name = "TTLXFR_LOCN_MATRIX_ID")
    private Long ttlxfrLocnMatrixId;

    @Column(name = "SHIPTO_MANUAL_JUR_IND")
    private String shiptoManualJurInd;

    @Column(name = "COUNTRY_CODE", length = 10)
    private String countryCode;

    @Column(name = "STATE_CODE", length = 10)
    private String stateCode;

    @Column(name = "COUNTY_NAME", length = 50)
    private String countyName;

    @Column(name = "CITY_NAME", length = 50)
    private String cityName;

    @Column(name = "STJ1_NAME", length = 255)
    private String stj1Name;

    @Column(name = "STJ2_NAME", length = 255)
    private String stj2Name;

    @Column(name = "STJ3_NAME", length = 255)
    private String stj3Name;

    @Column(name = "STJ4_NAME", length = 255)
    private String stj4Name;

    @Column(name = "STJ5_NAME", length = 255)
    private String stj5Name;

    @Column(name = "STJ6_NAME", length = 255)
    private String stj6Name;

    @Column(name = "STJ7_NAME", length = 255)
    private String stj7Name;

    @Column(name = "STJ8_NAME", length = 255)
    private String stj8Name;

    @Column(name = "STJ9_NAME", length = 255)
    private String stj9Name;

    @Column(name = "STJ10_NAME", length = 255)
    private String stj10Name;

    @Column(name = "COUNTRY_VENDOR_NEXUS_DTL_ID")
    private Long countryVendorNexusDtlId;

    @Column(name = "STATE_VENDOR_NEXUS_DTL_ID")
    private Long stateVendorNexusDtlId;

    @Column(name = "COUNTY_VENDOR_NEXUS_DTL_ID")
    private Long countyVendorNexusDtlId;

    @Column(name = "CITY_VENDOR_NEXUS_DTL_ID")
    private Long cityVendorNexusDtlId;

    @Column(name = "COUNTRY_TAXCODE_DETAIL_ID")
    private Long countryTaxcodeDetailId;

    @Column(name = "STATE_TAXCODE_DETAIL_ID")
    private Long stateTaxcodeDetailId;

    @Column(name = "COUNTY_TAXCODE_DETAIL_ID")
    private Long countyTaxcodeDetailId;

    @Column(name = "CITY_TAXCODE_DETAIL_ID")
    private Long cityTaxcodeDetailId;

    @Column(name = "STJ1_TAXCODE_DETAIL_ID")
    private Long stj1TaxcodeDetailId;

    @Column(name = "STJ2_TAXCODE_DETAIL_ID")
    private Long stj2TaxcodeDetailId;

    @Column(name = "STJ3_TAXCODE_DETAIL_ID")
    private Long stj3TaxcodeDetailId;

    @Column(name = "STJ4_TAXCODE_DETAIL_ID")
    private Long stj4TaxcodeDetailId;

    @Column(name = "STJ5_TAXCODE_DETAIL_ID")
    private Long stj5TaxcodeDetailId;

    @Column(name = "STJ6_TAXCODE_DETAIL_ID")
    private Long stj6TaxcodeDetailId;

    @Column(name = "STJ7_TAXCODE_DETAIL_ID")
    private Long stj7TaxcodeDetailId;

    @Column(name = "STJ8_TAXCODE_DETAIL_ID")
    private Long stj8TaxcodeDetailId;

    @Column(name = "STJ9_TAXCODE_DETAIL_ID")
    private Long stj9TaxcodeDetailId;

    @Column(name = "STJ10_TAXCODE_DETAIL_ID")
    private Long stj10TaxcodeDetailId;

    @Column(name = "COUNTRY_TAXTYPE_CODE", length = 10)
    private String countryTaxtypeCode;

    @Column(name = "STATE_TAXTYPE_CODE", length = 10)
    private String stateTaxtypeCode;

    @Column(name = "COUNTY_TAXTYPE_CODE", length = 10)
    private String countyTaxtypeCode;

    @Column(name = "CITY_TAXTYPE_CODE", length = 10)
    private String cityTaxtypeCode;

    @Column(name = "STJ1_TAXTYPE_CODE", length = 10)
    private String stj1TaxtypeCode;

    @Column(name = "STJ2_TAXTYPE_CODE", length = 10)
    private String stj2TaxtypeCode;

    @Column(name = "STJ3_TAXTYPE_CODE", length = 10)
    private String stj3TaxtypeCode;

    @Column(name = "STJ4_TAXTYPE_CODE", length = 10)
    private String stj4TaxtypeCode;

    @Column(name = "STJ5_TAXTYPE_CODE", length = 10)
    private String stj5TaxtypeCode;

    @Column(name = "STJ6_TAXTYPE_CODE", length = 10)
    private String stj6TaxtypeCode;

    @Column(name = "STJ7_TAXTYPE_CODE", length = 10)
    private String stj7TaxtypeCode;

    @Column(name = "STJ8_TAXTYPE_CODE", length = 10)
    private String stj8TaxtypeCode;

    @Column(name = "STJ9_TAXTYPE_CODE", length = 10)
    private String stj9TaxtypeCode;

    @Column(name = "STJ10_TAXTYPE_CODE", length = 10)
    private String stj10TaxtypeCode;

    @Column(name = "COUNTRY_TAXRATE_ID")
    private Long countryTaxrateId;

    @Column(name = "STATE_TAXRATE_ID")
    private Long stateTaxrateId;

    @Column(name = "COUNTY_TAXRATE_ID")
    private Long countyTaxrateId;

    @Column(name = "CITY_TAXRATE_ID")
    private Long cityTaxrateId;

    @Column(name = "STJ1_TAXRATE_ID")
    private Long stj1TaxrateId;

    @Column(name = "STJ2_TAXRATE_ID")
    private Long stj2TaxrateId;

    @Column(name = "STJ3_TAXRATE_ID")
    private Long stj3TaxrateId;

    @Column(name = "STJ4_TAXRATE_ID")
    private Long stj4TaxrateId;

    @Column(name = "STJ5_TAXRATE_ID")
    private Long stj5TaxrateId;

    @Column(name = "STJ6_TAXRATE_ID")
    private Long stj6TaxrateId;

    @Column(name = "STJ7_TAXRATE_ID")
    private Long stj7TaxrateId;

    @Column(name = "STJ8_TAXRATE_ID")
    private Long stj8TaxrateId;

    @Column(name = "STJ9_TAXRATE_ID")
    private Long stj9TaxrateId;

    @Column(name = "STJ10_TAXRATE_ID")
    private Long stj10TaxrateId;

    @Column(name = "COUNTRY_TAXCODE_OVER_FLAG", length = 1)
    private String countryTaxcodeOverFlag;

    @Column(name = "STATE_TAXCODE_OVER_FLAG", length = 1)
    private String stateTaxcodeOverFlag;

    @Column(name = "COUNTY_TAXCODE_OVER_FLAG", length = 1)
    private String countyTaxcodeOverFlag;

    @Column(name = "CITY_TAXCODE_OVER_FLAG", length = 1)
    private String cityTaxcodeOverFlag;

    @Column(name = "STJ1_TAXCODE_OVER_FLAG", length = 1)
    private String stj1TaxcodeOverFlag;

    @Column(name = "STJ2_TAXCODE_OVER_FLAG", length = 1)
    private String stj2TaxcodeOverFlag;

    @Column(name = "STJ3_TAXCODE_OVER_FLAG", length = 1)
    private String stj3TaxcodeOverFlag;

    @Column(name = "STJ4_TAXCODE_OVER_FLAG", length = 1)
    private String stj4TaxcodeOverFlag;

    @Column(name = "STJ5_TAXCODE_OVER_FLAG", length = 1)
    private String stj5TaxcodeOverFlag;

    @Column(name = "STJ6_TAXCODE_OVER_FLAG", length = 1)
    private String stj6TaxcodeOverFlag;

    @Column(name = "STJ7_TAXCODE_OVER_FLAG", length = 1)
    private String stj7TaxcodeOverFlag;

    @Column(name = "STJ8_TAXCODE_OVER_FLAG", length = 1)
    private String stj8TaxcodeOverFlag;

    @Column(name = "STJ9_TAXCODE_OVER_FLAG", length = 1)
    private String stj9TaxcodeOverFlag;

    @Column(name = "STJ10_TAXCODE_OVER_FLAG", length = 1)
    private String stj10TaxcodeOverFlag;

    @Column(name = "SHIPTO_JURISDICTION_ID")
    private Long shiptoJurisdictionId;

    @Column(name = "SHIPFROM_JURISDICTION_ID")
    private Long shipfromJurisdictionId;

    @Column(name = "ORDRACPT_JURISDICTION_ID")
    private Long ordracptJurisdictionId;

    @Column(name = "ORDRORGN_JURISDICTION_ID")
    private Long ordrorgnJurisdictionId;

    @Column(name = "FIRSTUSE_JURISDICTION_ID")
    private Long firstuseJurisdictionId;

    @Column(name = "BILLTO_JURISDICTION_ID")
    private Long billtoJurisdictionId;

    @Column(name = "TTLXFR_JURISDICTION_ID")
    private Long ttlxfrJurisdictionId;

    @Column(name = "COUNTRY_TIER1_RATE")
    private BigDecimal countryTier1Rate;

    @Column(name = "COUNTRY_TIER2_RATE")
    private BigDecimal countryTier2Rate;

    @Column(name = "COUNTRY_TIER3_RATE")
    private BigDecimal countryTier3Rate;

    @Column(name = "COUNTRY_TIER1_SETAMT")
    private BigDecimal countryTier1Setamt;

    @Column(name = "COUNTRY_TIER2_SETAMT")
    private BigDecimal countryTier2Setamt;

    @Column(name = "COUNTRY_TIER3_SETAMT")
    private BigDecimal countryTier3Setamt;

    @Column(name = "COUNTRY_TIER1_MAX_AMT")
    private BigDecimal countryTier1MaxAmt;

    @Column(name = "COUNTRY_TIER2_MIN_AMT")
    private BigDecimal countryTier2MinAmt;

    @Column(name = "COUNTRY_TIER2_MAX_AMT")
    private BigDecimal countryTier2MaxAmt;

    @Column(name = "COUNTRY_TIER2_ENTAM_FLAG", length = 1)
    private String countryTier2EntamFlag;

    @Column(name = "COUNTRY_TIER3_ENTAM_FLAG", length = 1)
    private String countryTier3EntamFlag;

    @Column(name = "COUNTRY_MAXTAX_AMT")
    private BigDecimal countryMaxtaxAmt;

    @Column(name = "COUNTRY_TAXABLE_THRESHOLD_AMT")
    private BigDecimal countryTaxableThresholdAmt;

    @Column(name = "COUNTRY_MINIMUM_TAXABLE_AMT")
    private BigDecimal countryMinimumTaxableAmt;

    @Column(name = "COUNTRY_MAXIMUM_TAXABLE_AMT")
    private BigDecimal countryMaximumTaxableAmt;

    @Column(name = "COUNTRY_MAXIMUM_TAX_AMT")
    private BigDecimal countryMaximumTaxAmt;

    @Column(name = "COUNTRY_BASE_CHANGE_PCT")
    private BigDecimal countryBaseChangePct;

    @Column(name = "COUNTRY_SPECIAL_RATE")
    private BigDecimal countrySpecialRate;

    @Column(name = "COUNTRY_SPECIAL_SETAMT")
    private BigDecimal countrySpecialSetamt;

    @Column(name = "COUNTRY_TAX_PROCESS_TYPE_CODE", length = 10)
    private String countryTaxProcessTypeCode;

    @Column(name = "STATE_TIER1_RATE")
    private BigDecimal stateTier1Rate;

    @Column(name = "STATE_TIER2_RATE")
    private BigDecimal stateTier2Rate;

    @Column(name = "STATE_TIER3_RATE")
    private BigDecimal stateTier3Rate;

    @Column(name = "STATE_TIER1_SETAMT")
    private BigDecimal stateTier1Setamt;

    @Column(name = "STATE_TIER2_SETAMT")
    private BigDecimal stateTier2Setamt;

    @Column(name = "STATE_TIER3_SETAMT")
    private BigDecimal stateTier3Setamt;

    @Column(name = "STATE_TIER1_MAX_AMT")
    private BigDecimal stateTier1MaxAmt;

    @Column(name = "STATE_TIER2_MIN_AMT")
    private BigDecimal stateTier2MinAmt;

    @Column(name = "STATE_TIER2_MAX_AMT")
    private BigDecimal stateTier2MaxAmt;

    @Column(name = "STATE_TIER2_ENTAM_FLAG", length = 1)
    private String stateTier2EntamFlag;

    @Column(name = "STATE_TIER3_ENTAM_FLAG", length = 1)
    private String stateTier3EntamFlag;

    @Column(name = "STATE_MAXTAX_AMT")
    private BigDecimal stateMaxtaxAmt;

    @Column(name = "STATE_TAXABLE_THRESHOLD_AMT")
    private BigDecimal stateTaxableThresholdAmt;

    @Column(name = "STATE_MINIMUM_TAXABLE_AMT")
    private BigDecimal stateMinimumTaxableAmt;

    @Column(name = "STATE_MAXIMUM_TAXABLE_AMT")
    private BigDecimal stateMaximumTaxableAmt;

    @Column(name = "STATE_MAXIMUM_TAX_AMT")
    private BigDecimal stateMaximumTaxAmt;

    @Column(name = "STATE_BASE_CHANGE_PCT")
    private BigDecimal stateBaseChangePct;

    @Column(name = "STATE_SPECIAL_RATE")
    private BigDecimal stateSpecialRate;

    @Column(name = "STATE_SPECIAL_SETAMT")
    private BigDecimal stateSpecialSetamt;

    @Column(name = "STATE_TAX_PROCESS_TYPE_CODE", length = 10)
    private String stateTaxProcessTypeCode;

    @Column(name = "COUNTY_TIER1_RATE")
    private BigDecimal countyTier1Rate;

    @Column(name = "COUNTY_TIER2_RATE")
    private BigDecimal countyTier2Rate;

    @Column(name = "COUNTY_TIER3_RATE")
    private BigDecimal countyTier3Rate;

    @Column(name = "COUNTY_TIER1_SETAMT")
    private BigDecimal countyTier1Setamt;

    @Column(name = "COUNTY_TIER2_SETAMT")
    private BigDecimal countyTier2Setamt;

    @Column(name = "COUNTY_TIER3_SETAMT")
    private BigDecimal countyTier3Setamt;

    @Column(name = "COUNTY_TIER1_MAX_AMT")
    private BigDecimal countyTier1MaxAmt;

    @Column(name = "COUNTY_TIER2_MIN_AMT")
    private BigDecimal countyTier2MinAmt;

    @Column(name = "COUNTY_TIER2_MAX_AMT")
    private BigDecimal countyTier2MaxAmt;

    @Column(name = "COUNTY_TIER2_ENTAM_FLAG", length = 1)
    private String countyTier2EntamFlag;

    @Column(name = "COUNTY_TIER3_ENTAM_FLAG", length = 1)
    private String countyTier3EntamFlag;

    @Column(name = "COUNTY_MAXTAX_AMT")
    private BigDecimal countyMaxtaxAmt;

    @Column(name = "COUNTY_TAXABLE_THRESHOLD_AMT")
    private BigDecimal countyTaxableThresholdAmt;

    @Column(name = "COUNTY_MINIMUM_TAXABLE_AMT")
    private BigDecimal countyMinimumTaxableAmt;

    @Column(name = "COUNTY_MAXIMUM_TAXABLE_AMT")
    private BigDecimal countyMaximumTaxableAmt;

    @Column(name = "COUNTY_MAXIMUM_TAX_AMT")
    private BigDecimal countyMaximumTaxAmt;

    @Column(name = "COUNTY_BASE_CHANGE_PCT")
    private BigDecimal countyBaseChangePct;

    @Column(name = "COUNTY_SPECIAL_RATE")
    private BigDecimal countySpecialRate;

    @Column(name = "COUNTY_SPECIAL_SETAMT")
    private BigDecimal countySpecialSetamt;

    @Column(name = "COUNTY_TAX_PROCESS_TYPE_CODE", length = 10)
    private String countyTaxProcessTypeCode;

    @Column(name = "CITY_TIER1_RATE")
    private BigDecimal cityTier1Rate;

    @Column(name = "CITY_TIER2_RATE")
    private BigDecimal cityTier2Rate;

    @Column(name = "CITY_TIER3_RATE")
    private BigDecimal cityTier3Rate;

    @Column(name = "CITY_TIER1_SETAMT")
    private BigDecimal cityTier1Setamt;

    @Column(name = "CITY_TIER2_SETAMT")
    private BigDecimal cityTier2Setamt;

    @Column(name = "CITY_TIER3_SETAMT")
    private BigDecimal cityTier3Setamt;

    @Column(name = "CITY_TIER1_MAX_AMT")
    private BigDecimal cityTier1MaxAmt;

    @Column(name = "CITY_TIER2_MIN_AMT")
    private BigDecimal cityTier2MinAmt;

    @Column(name = "CITY_TIER2_MAX_AMT")
    private BigDecimal cityTier2MaxAmt;

    @Column(name = "CITY_TIER2_ENTAM_FLAG", length = 1)
    private String cityTier2EntamFlag;

    @Column(name = "CITY_TIER3_ENTAM_FLAG", length = 1)
    private String cityTier3EntamFlag;

    @Column(name = "CITY_MAXTAX_AMT")
    private BigDecimal cityMaxtaxAmt;

    @Column(name = "CITY_TAXABLE_THRESHOLD_AMT")
    private BigDecimal cityTaxableThresholdAmt;

    @Column(name = "CITY_MINIMUM_TAXABLE_AMT")
    private BigDecimal cityMinimumTaxableAmt;

    @Column(name = "CITY_MAXIMUM_TAXABLE_AMT")
    private BigDecimal cityMaximumTaxableAmt;

    @Column(name = "CITY_MAXIMUM_TAX_AMT")
    private BigDecimal cityMaximumTaxAmt;

    @Column(name = "CITY_BASE_CHANGE_PCT")
    private BigDecimal cityBaseChangePct;

    @Column(name = "CITY_SPECIAL_RATE")
    private BigDecimal citySpecialRate;

    @Column(name = "CITY_SPECIAL_SETAMT")
    private BigDecimal citySpecialSetamt;

    @Column(name = "CITY_TAX_PROCESS_TYPE_CODE", length = 10)
    private String cityTaxProcessTypeCode;

    @Column(name = "STJ1_RATE")
    private BigDecimal stj1Rate;

    @Column(name = "STJ1_SETAMT")
    private BigDecimal stj1Setamt;

    @Column(name = "STJ2_RATE")
    private BigDecimal stj2Rate;

    @Column(name = "STJ2_SETAMT")
    private BigDecimal stj2Setamt;

    @Column(name = "STJ3_RATE")
    private BigDecimal stj3Rate;

    @Column(name = "STJ3_SETAMT")
    private BigDecimal stj3Setamt;

    @Column(name = "STJ4_RATE")
    private BigDecimal stj4Rate;

    @Column(name = "STJ4_SETAMT")
    private BigDecimal stj4Setamt;

    @Column(name = "STJ5_RATE")
    private BigDecimal stj5Rate;

    @Column(name = "STJ5_SETAMT")
    private BigDecimal stj5Setamt;

    @Column(name = "STJ6_RATE")
    private BigDecimal stj6Rate;

    @Column(name = "STJ6_SETAMT")
    private BigDecimal stj6Setamt;

    @Column(name = "STJ7_RATE")
    private BigDecimal stj7Rate;

    @Column(name = "STJ7_SETAMT")
    private BigDecimal stj7Setamt;

    @Column(name = "STJ8_RATE")
    private BigDecimal stj8Rate;

    @Column(name = "STJ8_SETAMT")
    private BigDecimal stj8Setamt;

    @Column(name = "STJ9_RATE")
    private BigDecimal stj9Rate;

    @Column(name = "STJ9_SETAMT")
    private BigDecimal stj9Setamt;

    @Column(name = "STJ10_RATE")
    private BigDecimal stj10Rate;

    @Column(name = "STJ10_SETAMT")
    private BigDecimal stj10Setamt;

    @Column(name = "COMBINED_RATE")
    private BigDecimal combinedRate;

    @Column(name = "COUNTRY_TAXTYPE_USED_CODE", length = 10)
    private String countryTaxtypeUsedCode;

    @Column(name = "STATE_TAXTYPE_USED_CODE", length = 10)
    private String stateTaxtypeUsedCode;

    @Column(name = "COUNTY_TAXTYPE_USED_CODE", length = 10)
    private String countyTaxtypeUsedCode;

    @Column(name = "CITY_TAXTYPE_USED_CODE", length = 10)
    private String cityTaxtypeUsedCode;

    @Column(name = "STJ1_TAXTYPE_USED_CODE", length = 10)
    private String stj1TaxtypeUsedCode;

    @Column(name = "STJ2_TAXTYPE_USED_CODE", length = 10)
    private String stj2TaxtypeUsedCode;

    @Column(name = "STJ3_TAXTYPE_USED_CODE", length = 10)
    private String stj3TaxtypeUsedCode;

    @Column(name = "STJ4_TAXTYPE_USED_CODE", length = 10)
    private String stj4TaxtypeUsedCode;

    @Column(name = "STJ5_TAXTYPE_USED_CODE", length = 10)
    private String stj5TaxtypeUsedCode;

    @Column(name = "STJ6_TAXTYPE_USED_CODE", length = 10)
    private String stj6TaxtypeUsedCode;

    @Column(name = "STJ7_TAXTYPE_USED_CODE", length = 10)
    private String stj7TaxtypeUsedCode;

    @Column(name = "STJ8_TAXTYPE_USED_CODE", length = 10)
    private String stj8TaxtypeUsedCode;

    @Column(name = "STJ9_TAXTYPE_USED_CODE", length = 10)
    private String stj9TaxtypeUsedCode;

    @Column(name = "STJ10_TAXTYPE_USED_CODE", length = 10)
    private String stj10TaxtypeUsedCode;

    @Column(name = "MODIFY_USER_ID", length = 40)
    private String modifyUserId;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFY_TIMESTAMP")
    private Date modifyTimestamp;

	@Transient
	private String transIndDesc;
	
	@Transient
	private String logSourceDesc; 
	
	@Transient
	private String suspendDesc;
	
	public String getTransIndDesc() {
		return transIndDesc;
	}

	public void setTransIndDesc(String transIndDesc) {
		this.transIndDesc = transIndDesc;
	}

	public String getLogSourceDesc() {
		return logSourceDesc;
	}

	public void setLogSourceDesc(String logSourceDesc) {
		this.logSourceDesc = logSourceDesc;
	}

	
	public String getSuspendDesc() {
		return suspendDesc;
	}

	public void setSuspendDesc(String suspendDesc) {
		this.suspendDesc = suspendDesc;
	}
	
	@Transient
    private BigDecimal deltaTaxAmt;
	    
    public BigDecimal getDeltaTaxAmt() {
		return deltaTaxAmt;
    }
    
	public void setDeltaTaxAmt(BigDecimal deltaTaxAmt) {
		this.deltaTaxAmt = deltaTaxAmt;
	}

	public Long getId() {
		return rownumber;
	}

	public void setId(Long id) {
    	this.rownumber = id;
    }

     public String getIdPropertyName() {
    	return "rownumber";
     }

    public Long getRownumber() {
		return rownumber;
	}

	public void setRownumber(Long rownumber) {
		this.rownumber = rownumber;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getPurchtransLogId() {
        return purchtransLogId;
    }

    public void setPurchtransLogId(Long purchtransLogId) {
        this.purchtransLogId = purchtransLogId;
    }

    public Long getPurchtransId() {
        return purchtransId;
    }

    public void setPurchtransId(Long purchtransId) {
        this.purchtransId = purchtransId;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }

    public Long getGlExtractBatchNo() {
        return glExtractBatchNo;
    }

    public void setGlExtractBatchNo(Long glExtractBatchNo) {
        this.glExtractBatchNo = glExtractBatchNo;
    }

    public Date getGlExtractTimestamp() {
        return glExtractTimestamp;
    }

    public void setGlExtractTimestamp(Date glExtractTimestamp) {
        this.glExtractTimestamp = glExtractTimestamp;
    }

    public String getDirectPayPermitFlag() {
        return directPayPermitFlag;
    }

    public void setDirectPayPermitFlag(String directPayPermitFlag) {
        this.directPayPermitFlag = directPayPermitFlag;
    }

    public String getCalculateInd() {
        return calculateInd;
    }

    public void setCalculateInd(String calculateInd) {
        this.calculateInd = calculateInd;
    }

    public Long getAllocationMatrixId() {
        return allocationMatrixId;
    }

    public void setAllocationMatrixId(Long allocationMatrixId) {
        this.allocationMatrixId = allocationMatrixId;
    }

    public Long getAllocationSubtransId() {
        return allocationSubtransId;
    }

    public void setAllocationSubtransId(Long allocationSubtransId) {
        this.allocationSubtransId = allocationSubtransId;
    }

    public Long getTaxAllocMatrixId() {
        return taxAllocMatrixId;
    }

    public void setTaxAllocMatrixId(Long taxAllocMatrixId) {
        this.taxAllocMatrixId = taxAllocMatrixId;
    }

    public Long getSplitSubtransId() {
        return splitSubtransId;
    }

    public void setSplitSubtransId(Long splitSubtransId) {
        this.splitSubtransId = splitSubtransId;
    }

    public String getMultiTransCode() {
        return multiTransCode;
    }

    public void setMultiTransCode(String multiTransCode) {
        this.multiTransCode = multiTransCode;
    }

    public String getTaxcodeCode() {
        return taxcodeCode;
    }

    public void setTaxcodeCode(String taxcodeCode) {
        this.taxcodeCode = taxcodeCode;
    }

    public String getManualTaxcodeInd() {
        return manualTaxcodeInd;
    }

    public void setManualTaxcodeInd(String manualTaxcodeInd) {
        this.manualTaxcodeInd = manualTaxcodeInd;
    }

    public Long getTaxMatrixId() {
        return taxMatrixId;
    }

    public void setTaxMatrixId(Long taxMatrixId) {
        this.taxMatrixId = taxMatrixId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getProcessingNotes() {
        return processingNotes;
    }

    public void setProcessingNotes(String processingNotes) {
        this.processingNotes = processingNotes;
    }

    public String getTransactionStateCode() {
        return transactionStateCode;
    }

    public void setTransactionStateCode(String transactionStateCode) {
        this.transactionStateCode = transactionStateCode;
    }

    public String getAutoTransactionStateCode() {
        return autoTransactionStateCode;
    }

    public void setAutoTransactionStateCode(String autoTransactionStateCode) {
        this.autoTransactionStateCode = autoTransactionStateCode;
    }

    public String getTransactionCountryCode() {
        return transactionCountryCode;
    }

    public void setTransactionCountryCode(String transactionCountryCode) {
        this.transactionCountryCode = transactionCountryCode;
    }

    public String getAutoTransactionCountryCode() {
        return autoTransactionCountryCode;
    }

    public void setAutoTransactionCountryCode(String autoTransactionCountryCode) {
        this.autoTransactionCountryCode = autoTransactionCountryCode;
    }

    public String getTransactionInd() {
        return transactionInd;
    }

    public void setTransactionInd(String transactionInd) {
        this.transactionInd = transactionInd;
    }

    public String getSuspendInd() {
        return suspendInd;
    }

    public void setSuspendInd(String suspendInd) {
        this.suspendInd = suspendInd;
    }

    public String getRejectFlag() {
        return rejectFlag;
    }

    public void setRejectFlag(String rejectFlag) {
        this.rejectFlag = rejectFlag;
    }

    public BigDecimal getCountryTier1TaxableAmt() {
        return countryTier1TaxableAmt;
    }

    public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt) {
        this.countryTier1TaxableAmt = countryTier1TaxableAmt;
    }

    public BigDecimal getCountryTier2TaxableAmt() {
        return countryTier2TaxableAmt;
    }

    public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt) {
        this.countryTier2TaxableAmt = countryTier2TaxableAmt;
    }

    public BigDecimal getCountryTier3TaxableAmt() {
        return countryTier3TaxableAmt;
    }

    public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt) {
        this.countryTier3TaxableAmt = countryTier3TaxableAmt;
    }

    public BigDecimal getStateTier1TaxableAmt() {
        return stateTier1TaxableAmt;
    }

    public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt) {
        this.stateTier1TaxableAmt = stateTier1TaxableAmt;
    }

    public BigDecimal getStateTier2TaxableAmt() {
        return stateTier2TaxableAmt;
    }

    public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt) {
        this.stateTier2TaxableAmt = stateTier2TaxableAmt;
    }

    public BigDecimal getStateTier3TaxableAmt() {
        return stateTier3TaxableAmt;
    }

    public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt) {
        this.stateTier3TaxableAmt = stateTier3TaxableAmt;
    }

    public BigDecimal getCountyTier1TaxableAmt() {
        return countyTier1TaxableAmt;
    }

    public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt) {
        this.countyTier1TaxableAmt = countyTier1TaxableAmt;
    }

    public BigDecimal getCountyTier2TaxableAmt() {
        return countyTier2TaxableAmt;
    }

    public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt) {
        this.countyTier2TaxableAmt = countyTier2TaxableAmt;
    }

    public BigDecimal getCountyTier3TaxableAmt() {
        return countyTier3TaxableAmt;
    }

    public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt) {
        this.countyTier3TaxableAmt = countyTier3TaxableAmt;
    }

    public BigDecimal getCityTier1TaxableAmt() {
        return cityTier1TaxableAmt;
    }

    public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt) {
        this.cityTier1TaxableAmt = cityTier1TaxableAmt;
    }

    public BigDecimal getCityTier2TaxableAmt() {
        return cityTier2TaxableAmt;
    }

    public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt) {
        this.cityTier2TaxableAmt = cityTier2TaxableAmt;
    }

    public BigDecimal getCityTier3TaxableAmt() {
        return cityTier3TaxableAmt;
    }

    public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt) {
        this.cityTier3TaxableAmt = cityTier3TaxableAmt;
    }

    public BigDecimal getBCountyLocalTaxAmt() {
        return bCountyLocalTaxAmt;
    }

    public void setBCountyLocalTaxAmt(BigDecimal bCountyLocalTaxAmt) {
        this.bCountyLocalTaxAmt = bCountyLocalTaxAmt;
    }

    public BigDecimal getBCityLocalTaxAmt() {
        return bCityLocalTaxAmt;
    }

    public void setBCityLocalTaxAmt(BigDecimal bCityLocalTaxAmt) {
        this.bCityLocalTaxAmt = bCityLocalTaxAmt;
    }


    public BigDecimal getaCountryTier1TaxAmt() {
		return aCountryTier1TaxAmt;
	}

	public void setaCountryTier1TaxAmt(BigDecimal aCountryTier1TaxAmt) {
		this.aCountryTier1TaxAmt = aCountryTier1TaxAmt;
	}

    public BigDecimal getbCountryTier1TaxAmt() {
		return bCountryTier1TaxAmt;
	}

	public BigDecimal getbCountryTier2TaxAmt() {
		return bCountryTier2TaxAmt;
	}

	public BigDecimal getbCountryTier3TaxAmt() {
		return bCountryTier3TaxAmt;
	}

	public BigDecimal getbStateTier1TaxAmt() {
		return bStateTier1TaxAmt;
	}

	public BigDecimal getbStateTier2TaxAmt() {
		return bStateTier2TaxAmt;
	}

	public BigDecimal getbStateTier3TaxAmt() {
		return bStateTier3TaxAmt;
	}

	public BigDecimal getbCountyTier1TaxAmt() {
		return bCountyTier1TaxAmt;
	}

	public BigDecimal getbCountyTier2TaxAmt() {
		return bCountyTier2TaxAmt;
	}

	public BigDecimal getbCountyTier3TaxAmt() {
		return bCountyTier3TaxAmt;
	}

	public BigDecimal getbCityTier1TaxAmt() {
		return bCityTier1TaxAmt;
	}

	public BigDecimal getbCityTier2TaxAmt() {
		return bCityTier2TaxAmt;
	}

	public BigDecimal getbCityTier3TaxAmt() {
		return bCityTier3TaxAmt;
	}

	public BigDecimal getbStj1TaxAmt() {
		return bStj1TaxAmt;
	}

	public BigDecimal getbStj2TaxAmt() {
		return bStj2TaxAmt;
	}

	public BigDecimal getbStj3TaxAmt() {
		return bStj3TaxAmt;
	}

	public BigDecimal getbStj4TaxAmt() {
		return bStj4TaxAmt;
	}

	public BigDecimal getbStj5TaxAmt() {
		return bStj5TaxAmt;
	}

	public BigDecimal getbStj6TaxAmt() {
		return bStj6TaxAmt;
	}

	public BigDecimal getbStj7TaxAmt() {
		return bStj7TaxAmt;
	}

	public BigDecimal getbStj8TaxAmt() {
		return bStj8TaxAmt;
	}

	public BigDecimal getbStj9TaxAmt() {
		return bStj9TaxAmt;
	}

	public BigDecimal getbStj10TaxAmt() {
		return bStj10TaxAmt;
	}

	public BigDecimal getbTbCalcTaxAmt() {
		return bTbCalcTaxAmt;
	}

	public BigDecimal getaCountryTier2TaxAmt() {
		return aCountryTier2TaxAmt;
	}

	public BigDecimal getaCountryTier3TaxAmt() {
		return aCountryTier3TaxAmt;
	}

	public BigDecimal getaStateTier1TaxAmt() {
		return aStateTier1TaxAmt;
	}

	public BigDecimal getaStateTier2TaxAmt() {
		return aStateTier2TaxAmt;
	}

	public BigDecimal getaStateTier3TaxAmt() {
		return aStateTier3TaxAmt;
	}

	public BigDecimal getaCountyTier1TaxAmt() {
		return aCountyTier1TaxAmt;
	}

	public BigDecimal getaCountyTier2TaxAmt() {
		return aCountyTier2TaxAmt;
	}

	public BigDecimal getaCountyTier3TaxAmt() {
		return aCountyTier3TaxAmt;
	}

	public BigDecimal getaCityTier1TaxAmt() {
		return aCityTier1TaxAmt;
	}

	public BigDecimal getaCityTier2TaxAmt() {
		return aCityTier2TaxAmt;
	}

	public BigDecimal getaCityTier3TaxAmt() {
		return aCityTier3TaxAmt;
	}

	public BigDecimal getaStj1TaxAmt() {
		return aStj1TaxAmt;
	}

	public BigDecimal getaStj2TaxAmt() {
		return aStj2TaxAmt;
	}

	public BigDecimal getaStj3TaxAmt() {
		return aStj3TaxAmt;
	}

	public BigDecimal getaStj4TaxAmt() {
		return aStj4TaxAmt;
	}

	public BigDecimal getaStj5TaxAmt() {
		return aStj5TaxAmt;
	}

	public BigDecimal getaStj6TaxAmt() {
		return aStj6TaxAmt;
	}

	public BigDecimal getaStj7TaxAmt() {
		return aStj7TaxAmt;
	}

	public BigDecimal getaStj8TaxAmt() {
		return aStj8TaxAmt;
	}

	public BigDecimal getaStj9TaxAmt() {
		return aStj9TaxAmt;
	}

	public BigDecimal getaStj10TaxAmt() {
		return aStj10TaxAmt;
	}

	public void setbCountryTier1TaxAmt(BigDecimal bCountryTier1TaxAmt) {
		this.bCountryTier1TaxAmt = bCountryTier1TaxAmt;
	}

	public void setbCountryTier2TaxAmt(BigDecimal bCountryTier2TaxAmt) {
		this.bCountryTier2TaxAmt = bCountryTier2TaxAmt;
	}

	public void setbCountryTier3TaxAmt(BigDecimal bCountryTier3TaxAmt) {
		this.bCountryTier3TaxAmt = bCountryTier3TaxAmt;
	}

	public void setbStateTier1TaxAmt(BigDecimal bStateTier1TaxAmt) {
		this.bStateTier1TaxAmt = bStateTier1TaxAmt;
	}

	public void setbStateTier2TaxAmt(BigDecimal bStateTier2TaxAmt) {
		this.bStateTier2TaxAmt = bStateTier2TaxAmt;
	}

	public void setbStateTier3TaxAmt(BigDecimal bStateTier3TaxAmt) {
		this.bStateTier3TaxAmt = bStateTier3TaxAmt;
	}

	public void setbCountyTier1TaxAmt(BigDecimal bCountyTier1TaxAmt) {
		this.bCountyTier1TaxAmt = bCountyTier1TaxAmt;
	}

	public void setbCountyTier2TaxAmt(BigDecimal bCountyTier2TaxAmt) {
		this.bCountyTier2TaxAmt = bCountyTier2TaxAmt;
	}

	public void setbCountyTier3TaxAmt(BigDecimal bCountyTier3TaxAmt) {
		this.bCountyTier3TaxAmt = bCountyTier3TaxAmt;
	}

	public void setbCityTier1TaxAmt(BigDecimal bCityTier1TaxAmt) {
		this.bCityTier1TaxAmt = bCityTier1TaxAmt;
	}

	public void setbCityTier2TaxAmt(BigDecimal bCityTier2TaxAmt) {
		this.bCityTier2TaxAmt = bCityTier2TaxAmt;
	}

	public void setbCityTier3TaxAmt(BigDecimal bCityTier3TaxAmt) {
		this.bCityTier3TaxAmt = bCityTier3TaxAmt;
	}

	public void setbStj1TaxAmt(BigDecimal bStj1TaxAmt) {
		this.bStj1TaxAmt = bStj1TaxAmt;
	}

	public void setbStj2TaxAmt(BigDecimal bStj2TaxAmt) {
		this.bStj2TaxAmt = bStj2TaxAmt;
	}

	public void setbStj3TaxAmt(BigDecimal bStj3TaxAmt) {
		this.bStj3TaxAmt = bStj3TaxAmt;
	}

	public void setbStj4TaxAmt(BigDecimal bStj4TaxAmt) {
		this.bStj4TaxAmt = bStj4TaxAmt;
	}

	public void setbStj5TaxAmt(BigDecimal bStj5TaxAmt) {
		this.bStj5TaxAmt = bStj5TaxAmt;
	}

	public void setbStj6TaxAmt(BigDecimal bStj6TaxAmt) {
		this.bStj6TaxAmt = bStj6TaxAmt;
	}

	public void setbStj7TaxAmt(BigDecimal bStj7TaxAmt) {
		this.bStj7TaxAmt = bStj7TaxAmt;
	}

	public void setbStj8TaxAmt(BigDecimal bStj8TaxAmt) {
		this.bStj8TaxAmt = bStj8TaxAmt;
	}

	public void setbStj9TaxAmt(BigDecimal bStj9TaxAmt) {
		this.bStj9TaxAmt = bStj9TaxAmt;
	}

	public void setbStj10TaxAmt(BigDecimal bStj10TaxAmt) {
		this.bStj10TaxAmt = bStj10TaxAmt;
	}

	public void setbTbCalcTaxAmt(BigDecimal bTbCalcTaxAmt) {
		this.bTbCalcTaxAmt = bTbCalcTaxAmt;
	}

	public void setaCountryTier2TaxAmt(BigDecimal aCountryTier2TaxAmt) {
		this.aCountryTier2TaxAmt = aCountryTier2TaxAmt;
	}

	public void setaCountryTier3TaxAmt(BigDecimal aCountryTier3TaxAmt) {
		this.aCountryTier3TaxAmt = aCountryTier3TaxAmt;
	}

	public void setaStateTier1TaxAmt(BigDecimal aStateTier1TaxAmt) {
		this.aStateTier1TaxAmt = aStateTier1TaxAmt;
	}

	public void setaStateTier2TaxAmt(BigDecimal aStateTier2TaxAmt) {
		this.aStateTier2TaxAmt = aStateTier2TaxAmt;
	}

	public void setaStateTier3TaxAmt(BigDecimal aStateTier3TaxAmt) {
		this.aStateTier3TaxAmt = aStateTier3TaxAmt;
	}

	public void setaCountyTier1TaxAmt(BigDecimal aCountyTier1TaxAmt) {
		this.aCountyTier1TaxAmt = aCountyTier1TaxAmt;
	}

	public void setaCountyTier2TaxAmt(BigDecimal aCountyTier2TaxAmt) {
		this.aCountyTier2TaxAmt = aCountyTier2TaxAmt;
	}

	public void setaCountyTier3TaxAmt(BigDecimal aCountyTier3TaxAmt) {
		this.aCountyTier3TaxAmt = aCountyTier3TaxAmt;
	}

	public void setaCityTier1TaxAmt(BigDecimal aCityTier1TaxAmt) {
		this.aCityTier1TaxAmt = aCityTier1TaxAmt;
	}

	public void setaCityTier2TaxAmt(BigDecimal aCityTier2TaxAmt) {
		this.aCityTier2TaxAmt = aCityTier2TaxAmt;
	}

	public void setaCityTier3TaxAmt(BigDecimal aCityTier3TaxAmt) {
		this.aCityTier3TaxAmt = aCityTier3TaxAmt;
	}

	public void setaStj1TaxAmt(BigDecimal aStj1TaxAmt) {
		this.aStj1TaxAmt = aStj1TaxAmt;
	}

	public void setaStj2TaxAmt(BigDecimal aStj2TaxAmt) {
		this.aStj2TaxAmt = aStj2TaxAmt;
	}

	public void setaStj3TaxAmt(BigDecimal aStj3TaxAmt) {
		this.aStj3TaxAmt = aStj3TaxAmt;
	}

	public void setaStj4TaxAmt(BigDecimal aStj4TaxAmt) {
		this.aStj4TaxAmt = aStj4TaxAmt;
	}

	public void setaStj5TaxAmt(BigDecimal aStj5TaxAmt) {
		this.aStj5TaxAmt = aStj5TaxAmt;
	}

	public void setaStj6TaxAmt(BigDecimal aStj6TaxAmt) {
		this.aStj6TaxAmt = aStj6TaxAmt;
	}

	public void setaStj7TaxAmt(BigDecimal aStj7TaxAmt) {
		this.aStj7TaxAmt = aStj7TaxAmt;
	}

	public void setaStj8TaxAmt(BigDecimal aStj8TaxAmt) {
		this.aStj8TaxAmt = aStj8TaxAmt;
	}

	public void setaStj9TaxAmt(BigDecimal aStj9TaxAmt) {
		this.aStj9TaxAmt = aStj9TaxAmt;
	}

	public void setaStj10TaxAmt(BigDecimal aStj10TaxAmt) {
		this.aStj10TaxAmt = aStj10TaxAmt;
	}

    public BigDecimal getACountyLocalTaxAmt() {
        return aCountyLocalTaxAmt;
    }

    public void setACountyLocalTaxAmt(BigDecimal aCountyLocalTaxAmt) {
        this.aCountyLocalTaxAmt = aCountyLocalTaxAmt;
    }

    public BigDecimal getACityLocalTaxAmt() {
        return aCityLocalTaxAmt;
    }

    public void setACityLocalTaxAmt(BigDecimal aCityLocalTaxAmt) {
        this.aCityLocalTaxAmt = aCityLocalTaxAmt;
    }

    public BigDecimal getDistr01Amt() {
        return distr01Amt;
    }

    public BigDecimal getaTbCalcTaxAmt() {
		return aTbCalcTaxAmt;
	}

	public void setaTbCalcTaxAmt(BigDecimal aTbCalcTaxAmt) {
		this.aTbCalcTaxAmt = aTbCalcTaxAmt;
	}

	public void setDistr01Amt(BigDecimal distr01Amt) {
        this.distr01Amt = distr01Amt;
    }

    public BigDecimal getDistr02Amt() {
        return distr02Amt;
    }

    public void setDistr02Amt(BigDecimal distr02Amt) {
        this.distr02Amt = distr02Amt;
    }

    public BigDecimal getDistr03Amt() {
        return distr03Amt;
    }

    public void setDistr03Amt(BigDecimal distr03Amt) {
        this.distr03Amt = distr03Amt;
    }

    public BigDecimal getDistr04Amt() {
        return distr04Amt;
    }

    public void setDistr04Amt(BigDecimal distr04Amt) {
        this.distr04Amt = distr04Amt;
    }

    public BigDecimal getDistr05Amt() {
        return distr05Amt;
    }

    public void setDistr05Amt(BigDecimal distr05Amt) {
        this.distr05Amt = distr05Amt;
    }

    public BigDecimal getDistr06Amt() {
        return distr06Amt;
    }

    public void setDistr06Amt(BigDecimal distr06Amt) {
        this.distr06Amt = distr06Amt;
    }

    public BigDecimal getDistr07Amt() {
        return distr07Amt;
    }

    public void setDistr07Amt(BigDecimal distr07Amt) {
        this.distr07Amt = distr07Amt;
    }

    public BigDecimal getDistr08Amt() {
        return distr08Amt;
    }

    public void setDistr08Amt(BigDecimal distr08Amt) {
        this.distr08Amt = distr08Amt;
    }

    public BigDecimal getDistr09Amt() {
        return distr09Amt;
    }

    public void setDistr09Amt(BigDecimal distr09Amt) {
        this.distr09Amt = distr09Amt;
    }

    public BigDecimal getDistr10Amt() {
        return distr10Amt;
    }

    public void setDistr10Amt(BigDecimal distr10Amt) {
        this.distr10Amt = distr10Amt;
    }

    public Long getCountrySitusMatrixId() {
        return countrySitusMatrixId;
    }

    public void setCountrySitusMatrixId(Long countrySitusMatrixId) {
        this.countrySitusMatrixId = countrySitusMatrixId;
    }

    public Long getStateSitusMatrixId() {
        return stateSitusMatrixId;
    }

    public void setStateSitusMatrixId(Long stateSitusMatrixId) {
        this.stateSitusMatrixId = stateSitusMatrixId;
    }

    public Long getCountySitusMatrixId() {
        return countySitusMatrixId;
    }

    public void setCountySitusMatrixId(Long countySitusMatrixId) {
        this.countySitusMatrixId = countySitusMatrixId;
    }

    public Long getCitySitusMatrixId() {
        return citySitusMatrixId;
    }

    public void setCitySitusMatrixId(Long citySitusMatrixId) {
        this.citySitusMatrixId = citySitusMatrixId;
    }

    public Long getStj1SitusMatrixId() {
        return stj1SitusMatrixId;
    }

    public void setStj1SitusMatrixId(Long stj1SitusMatrixId) {
        this.stj1SitusMatrixId = stj1SitusMatrixId;
    }

    public Long getStj2SitusMatrixId() {
        return stj2SitusMatrixId;
    }

    public void setStj2SitusMatrixId(Long stj2SitusMatrixId) {
        this.stj2SitusMatrixId = stj2SitusMatrixId;
    }

    public Long getStj3SitusMatrixId() {
        return stj3SitusMatrixId;
    }

    public void setStj3SitusMatrixId(Long stj3SitusMatrixId) {
        this.stj3SitusMatrixId = stj3SitusMatrixId;
    }

    public Long getStj4SitusMatrixId() {
        return stj4SitusMatrixId;
    }

    public void setStj4SitusMatrixId(Long stj4SitusMatrixId) {
        this.stj4SitusMatrixId = stj4SitusMatrixId;
    }

    public Long getStj5SitusMatrixId() {
        return stj5SitusMatrixId;
    }

    public void setStj5SitusMatrixId(Long stj5SitusMatrixId) {
        this.stj5SitusMatrixId = stj5SitusMatrixId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStj1Name() {
        return stj1Name;
    }

    public void setStj1Name(String stj1Name) {
        this.stj1Name = stj1Name;
    }

    public String getStj2Name() {
        return stj2Name;
    }

    public void setStj2Name(String stj2Name) {
        this.stj2Name = stj2Name;
    }

    public String getStj3Name() {
        return stj3Name;
    }

    public void setStj3Name(String stj3Name) {
        this.stj3Name = stj3Name;
    }

    public String getStj4Name() {
        return stj4Name;
    }

    public void setStj4Name(String stj4Name) {
        this.stj4Name = stj4Name;
    }

    public String getStj5Name() {
        return stj5Name;
    }

    public void setStj5Name(String stj5Name) {
        this.stj5Name = stj5Name;
    }

    public String getStj6Name() {
        return stj6Name;
    }

    public void setStj6Name(String stj6Name) {
        this.stj6Name = stj6Name;
    }

    public String getStj7Name() {
        return stj7Name;
    }

    public void setStj7Name(String stj7Name) {
        this.stj7Name = stj7Name;
    }

    public String getStj8Name() {
        return stj8Name;
    }

    public void setStj8Name(String stj8Name) {
        this.stj8Name = stj8Name;
    }

    public String getStj9Name() {
        return stj9Name;
    }

    public void setStj9Name(String stj9Name) {
        this.stj9Name = stj9Name;
    }

    public String getStj10Name() {
        return stj10Name;
    }

    public void setStj10Name(String stj10Name) {
        this.stj10Name = stj10Name;
    }

    public Long getCountryVendorNexusDtlId() {
        return countryVendorNexusDtlId;
    }

    public void setCountryVendorNexusDtlId(Long countryVendorNexusDtlId) {
        this.countryVendorNexusDtlId = countryVendorNexusDtlId;
    }

    public Long getStateVendorNexusDtlId() {
        return stateVendorNexusDtlId;
    }

    public void setStateVendorNexusDtlId(Long stateVendorNexusDtlId) {
        this.stateVendorNexusDtlId = stateVendorNexusDtlId;
    }

    public Long getCountyVendorNexusDtlId() {
        return countyVendorNexusDtlId;
    }

    public void setCountyVendorNexusDtlId(Long countyVendorNexusDtlId) {
        this.countyVendorNexusDtlId = countyVendorNexusDtlId;
    }

    public Long getCityVendorNexusDtlId() {
        return cityVendorNexusDtlId;
    }

    public void setCityVendorNexusDtlId(Long cityVendorNexusDtlId) {
        this.cityVendorNexusDtlId = cityVendorNexusDtlId;
    }

    public Long getCountryTaxcodeDetailId() {
        return countryTaxcodeDetailId;
    }

    public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId) {
        this.countryTaxcodeDetailId = countryTaxcodeDetailId;
    }

    public Long getStateTaxcodeDetailId() {
        return stateTaxcodeDetailId;
    }

    public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId) {
        this.stateTaxcodeDetailId = stateTaxcodeDetailId;
    }

    public Long getCountyTaxcodeDetailId() {
        return countyTaxcodeDetailId;
    }

    public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId) {
        this.countyTaxcodeDetailId = countyTaxcodeDetailId;
    }

    public Long getCityTaxcodeDetailId() {
        return cityTaxcodeDetailId;
    }

    public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId) {
        this.cityTaxcodeDetailId = cityTaxcodeDetailId;
    }

    public Long getStj1TaxcodeDetailId() {
        return stj1TaxcodeDetailId;
    }

    public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId) {
        this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
    }

    public Long getStj2TaxcodeDetailId() {
        return stj2TaxcodeDetailId;
    }

    public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId) {
        this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
    }

    public Long getStj3TaxcodeDetailId() {
        return stj3TaxcodeDetailId;
    }

    public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId) {
        this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
    }

    public Long getStj4TaxcodeDetailId() {
        return stj4TaxcodeDetailId;
    }

    public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId) {
        this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
    }

    public Long getStj5TaxcodeDetailId() {
        return stj5TaxcodeDetailId;
    }

    public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId) {
        this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
    }

    public Long getStj6TaxcodeDetailId() {
        return stj6TaxcodeDetailId;
    }

    public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId) {
        this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
    }

    public Long getStj7TaxcodeDetailId() {
        return stj7TaxcodeDetailId;
    }

    public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId) {
        this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
    }

    public Long getStj8TaxcodeDetailId() {
        return stj8TaxcodeDetailId;
    }

    public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId) {
        this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
    }

    public Long getStj9TaxcodeDetailId() {
        return stj9TaxcodeDetailId;
    }

    public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId) {
        this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
    }

    public Long getStj10TaxcodeDetailId() {
        return stj10TaxcodeDetailId;
    }

    public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId) {
        this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
    }

    public String getCountryTaxtypeCode() {
        return countryTaxtypeCode;
    }

    public void setCountryTaxtypeCode(String countryTaxtypeCode) {
        this.countryTaxtypeCode = countryTaxtypeCode;
    }

    public String getStateTaxtypeCode() {
        return stateTaxtypeCode;
    }

    public void setStateTaxtypeCode(String stateTaxtypeCode) {
        this.stateTaxtypeCode = stateTaxtypeCode;
    }

    public String getCountyTaxtypeCode() {
        return countyTaxtypeCode;
    }

    public void setCountyTaxtypeCode(String countyTaxtypeCode) {
        this.countyTaxtypeCode = countyTaxtypeCode;
    }

    public String getCityTaxtypeCode() {
        return cityTaxtypeCode;
    }

    public void setCityTaxtypeCode(String cityTaxtypeCode) {
        this.cityTaxtypeCode = cityTaxtypeCode;
    }

    public String getStj1TaxtypeCode() {
        return stj1TaxtypeCode;
    }

    public void setStj1TaxtypeCode(String stj1TaxtypeCode) {
        this.stj1TaxtypeCode = stj1TaxtypeCode;
    }

    public String getStj2TaxtypeCode() {
        return stj2TaxtypeCode;
    }

    public void setStj2TaxtypeCode(String stj2TaxtypeCode) {
        this.stj2TaxtypeCode = stj2TaxtypeCode;
    }

    public String getStj3TaxtypeCode() {
        return stj3TaxtypeCode;
    }

    public void setStj3TaxtypeCode(String stj3TaxtypeCode) {
        this.stj3TaxtypeCode = stj3TaxtypeCode;
    }

    public String getStj4TaxtypeCode() {
        return stj4TaxtypeCode;
    }

    public void setStj4TaxtypeCode(String stj4TaxtypeCode) {
        this.stj4TaxtypeCode = stj4TaxtypeCode;
    }

    public String getStj5TaxtypeCode() {
        return stj5TaxtypeCode;
    }

    public void setStj5TaxtypeCode(String stj5TaxtypeCode) {
        this.stj5TaxtypeCode = stj5TaxtypeCode;
    }

    public String getStj6TaxtypeCode() {
        return stj6TaxtypeCode;
    }

    public void setStj6TaxtypeCode(String stj6TaxtypeCode) {
        this.stj6TaxtypeCode = stj6TaxtypeCode;
    }

    public String getStj7TaxtypeCode() {
        return stj7TaxtypeCode;
    }

    public void setStj7TaxtypeCode(String stj7TaxtypeCode) {
        this.stj7TaxtypeCode = stj7TaxtypeCode;
    }

    public String getStj8TaxtypeCode() {
        return stj8TaxtypeCode;
    }

    public void setStj8TaxtypeCode(String stj8TaxtypeCode) {
        this.stj8TaxtypeCode = stj8TaxtypeCode;
    }

    public String getStj9TaxtypeCode() {
        return stj9TaxtypeCode;
    }

    public void setStj9TaxtypeCode(String stj9TaxtypeCode) {
        this.stj9TaxtypeCode = stj9TaxtypeCode;
    }

    public String getStj10TaxtypeCode() {
        return stj10TaxtypeCode;
    }

    public void setStj10TaxtypeCode(String stj10TaxtypeCode) {
        this.stj10TaxtypeCode = stj10TaxtypeCode;
    }

    public Long getCountryTaxrateId() {
        return countryTaxrateId;
    }

    public void setCountryTaxrateId(Long countryTaxrateId) {
        this.countryTaxrateId = countryTaxrateId;
    }

    public Long getStateTaxrateId() {
        return stateTaxrateId;
    }

    public void setStateTaxrateId(Long stateTaxrateId) {
        this.stateTaxrateId = stateTaxrateId;
    }

    public Long getCountyTaxrateId() {
        return countyTaxrateId;
    }

    public void setCountyTaxrateId(Long countyTaxrateId) {
        this.countyTaxrateId = countyTaxrateId;
    }

    public Long getCityTaxrateId() {
        return cityTaxrateId;
    }

    public void setCityTaxrateId(Long cityTaxrateId) {
        this.cityTaxrateId = cityTaxrateId;
    }

    public Long getStj1TaxrateId() {
        return stj1TaxrateId;
    }

    public void setStj1TaxrateId(Long stj1TaxrateId) {
        this.stj1TaxrateId = stj1TaxrateId;
    }

    public Long getStj2TaxrateId() {
        return stj2TaxrateId;
    }

    public void setStj2TaxrateId(Long stj2TaxrateId) {
        this.stj2TaxrateId = stj2TaxrateId;
    }

    public Long getStj3TaxrateId() {
        return stj3TaxrateId;
    }

    public void setStj3TaxrateId(Long stj3TaxrateId) {
        this.stj3TaxrateId = stj3TaxrateId;
    }

    public Long getStj4TaxrateId() {
        return stj4TaxrateId;
    }

    public void setStj4TaxrateId(Long stj4TaxrateId) {
        this.stj4TaxrateId = stj4TaxrateId;
    }

    public Long getStj5TaxrateId() {
        return stj5TaxrateId;
    }

    public void setStj5TaxrateId(Long stj5TaxrateId) {
        this.stj5TaxrateId = stj5TaxrateId;
    }

    public Long getStj6TaxrateId() {
        return stj6TaxrateId;
    }

    public void setStj6TaxrateId(Long stj6TaxrateId) {
        this.stj6TaxrateId = stj6TaxrateId;
    }

    public Long getStj7TaxrateId() {
        return stj7TaxrateId;
    }

    public void setStj7TaxrateId(Long stj7TaxrateId) {
        this.stj7TaxrateId = stj7TaxrateId;
    }

    public Long getStj8TaxrateId() {
        return stj8TaxrateId;
    }

    public void setStj8TaxrateId(Long stj8TaxrateId) {
        this.stj8TaxrateId = stj8TaxrateId;
    }

    public Long getStj9TaxrateId() {
        return stj9TaxrateId;
    }

    public void setStj9TaxrateId(Long stj9TaxrateId) {
        this.stj9TaxrateId = stj9TaxrateId;
    }

    public Long getStj10TaxrateId() {
        return stj10TaxrateId;
    }

    public void setStj10TaxrateId(Long stj10TaxrateId) {
        this.stj10TaxrateId = stj10TaxrateId;
    }

    public String getCountryTaxcodeOverFlag() {
        return countryTaxcodeOverFlag;
    }

    public void setCountryTaxcodeOverFlag(String countryTaxcodeOverFlag) {
        this.countryTaxcodeOverFlag = countryTaxcodeOverFlag;
    }

    public String getStateTaxcodeOverFlag() {
        return stateTaxcodeOverFlag;
    }

    public void setStateTaxcodeOverFlag(String stateTaxcodeOverFlag) {
        this.stateTaxcodeOverFlag = stateTaxcodeOverFlag;
    }

    public String getCountyTaxcodeOverFlag() {
        return countyTaxcodeOverFlag;
    }

    public void setCountyTaxcodeOverFlag(String countyTaxcodeOverFlag) {
        this.countyTaxcodeOverFlag = countyTaxcodeOverFlag;
    }

    public String getCityTaxcodeOverFlag() {
        return cityTaxcodeOverFlag;
    }

    public void setCityTaxcodeOverFlag(String cityTaxcodeOverFlag) {
        this.cityTaxcodeOverFlag = cityTaxcodeOverFlag;
    }

    public String getStj1TaxcodeOverFlag() {
        return stj1TaxcodeOverFlag;
    }

    public void setStj1TaxcodeOverFlag(String stj1TaxcodeOverFlag) {
        this.stj1TaxcodeOverFlag = stj1TaxcodeOverFlag;
    }

    public String getStj2TaxcodeOverFlag() {
        return stj2TaxcodeOverFlag;
    }

    public void setStj2TaxcodeOverFlag(String stj2TaxcodeOverFlag) {
        this.stj2TaxcodeOverFlag = stj2TaxcodeOverFlag;
    }

    public String getStj3TaxcodeOverFlag() {
        return stj3TaxcodeOverFlag;
    }

    public void setStj3TaxcodeOverFlag(String stj3TaxcodeOverFlag) {
        this.stj3TaxcodeOverFlag = stj3TaxcodeOverFlag;
    }

    public String getStj4TaxcodeOverFlag() {
        return stj4TaxcodeOverFlag;
    }

    public void setStj4TaxcodeOverFlag(String stj4TaxcodeOverFlag) {
        this.stj4TaxcodeOverFlag = stj4TaxcodeOverFlag;
    }

    public String getStj5TaxcodeOverFlag() {
        return stj5TaxcodeOverFlag;
    }

    public void setStj5TaxcodeOverFlag(String stj5TaxcodeOverFlag) {
        this.stj5TaxcodeOverFlag = stj5TaxcodeOverFlag;
    }

    public String getStj6TaxcodeOverFlag() {
        return stj6TaxcodeOverFlag;
    }

    public void setStj6TaxcodeOverFlag(String stj6TaxcodeOverFlag) {
        this.stj6TaxcodeOverFlag = stj6TaxcodeOverFlag;
    }

    public String getStj7TaxcodeOverFlag() {
        return stj7TaxcodeOverFlag;
    }

    public void setStj7TaxcodeOverFlag(String stj7TaxcodeOverFlag) {
        this.stj7TaxcodeOverFlag = stj7TaxcodeOverFlag;
    }

    public String getStj8TaxcodeOverFlag() {
        return stj8TaxcodeOverFlag;
    }

    public void setStj8TaxcodeOverFlag(String stj8TaxcodeOverFlag) {
        this.stj8TaxcodeOverFlag = stj8TaxcodeOverFlag;
    }

    public String getStj9TaxcodeOverFlag() {
        return stj9TaxcodeOverFlag;
    }

    public void setStj9TaxcodeOverFlag(String stj9TaxcodeOverFlag) {
        this.stj9TaxcodeOverFlag = stj9TaxcodeOverFlag;
    }

    public String getStj10TaxcodeOverFlag() {
        return stj10TaxcodeOverFlag;
    }

    public void setStj10TaxcodeOverFlag(String stj10TaxcodeOverFlag) {
        this.stj10TaxcodeOverFlag = stj10TaxcodeOverFlag;
    }

    public Long getShiptoJurisdictionId() {
        return shiptoJurisdictionId;
    }

    public void setShiptoJurisdictionId(Long shiptoJurisdictionId) {
        this.shiptoJurisdictionId = shiptoJurisdictionId;
    }

    public Long getShipfromJurisdictionId() {
        return shipfromJurisdictionId;
    }

    public void setShipfromJurisdictionId(Long shipfromJurisdictionId) {
        this.shipfromJurisdictionId = shipfromJurisdictionId;
    }

    public Long getOrdracptJurisdictionId() {
        return ordracptJurisdictionId;
    }

    public void setOrdracptJurisdictionId(Long ordracptJurisdictionId) {
        this.ordracptJurisdictionId = ordracptJurisdictionId;
    }

    public Long getOrdrorgnJurisdictionId() {
        return ordrorgnJurisdictionId;
    }

    public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId) {
        this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
    }

    public Long getFirstuseJurisdictionId() {
        return firstuseJurisdictionId;
    }

    public void setFirstuseJurisdictionId(Long firstuseJurisdictionId) {
        this.firstuseJurisdictionId = firstuseJurisdictionId;
    }

    public Long getBilltoJurisdictionId() {
        return billtoJurisdictionId;
    }

    public void setBilltoJurisdictionId(Long billtoJurisdictionId) {
        this.billtoJurisdictionId = billtoJurisdictionId;
    }

    public Long getTtlxfrJurisdictionId() {
        return ttlxfrJurisdictionId;
    }

    public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId) {
        this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
    }

    public BigDecimal getCountryTier1Rate() {
        return countryTier1Rate;
    }

    public void setCountryTier1Rate(BigDecimal countryTier1Rate) {
        this.countryTier1Rate = countryTier1Rate;
    }

    public BigDecimal getCountryTier2Rate() {
        return countryTier2Rate;
    }

    public void setCountryTier2Rate(BigDecimal countryTier2Rate) {
        this.countryTier2Rate = countryTier2Rate;
    }

    public BigDecimal getCountryTier3Rate() {
        return countryTier3Rate;
    }

    public void setCountryTier3Rate(BigDecimal countryTier3Rate) {
        this.countryTier3Rate = countryTier3Rate;
    }

    public BigDecimal getCountryTier1Setamt() {
        return countryTier1Setamt;
    }

    public void setCountryTier1Setamt(BigDecimal countryTier1Setamt) {
        this.countryTier1Setamt = countryTier1Setamt;
    }

    public BigDecimal getCountryTier2Setamt() {
        return countryTier2Setamt;
    }

    public void setCountryTier2Setamt(BigDecimal countryTier2Setamt) {
        this.countryTier2Setamt = countryTier2Setamt;
    }

    public BigDecimal getCountryTier3Setamt() {
        return countryTier3Setamt;
    }

    public void setCountryTier3Setamt(BigDecimal countryTier3Setamt) {
        this.countryTier3Setamt = countryTier3Setamt;
    }

    public BigDecimal getCountryTier1MaxAmt() {
        return countryTier1MaxAmt;
    }

    public void setCountryTier1MaxAmt(BigDecimal countryTier1MaxAmt) {
        this.countryTier1MaxAmt = countryTier1MaxAmt;
    }

    public BigDecimal getCountryTier2MinAmt() {
        return countryTier2MinAmt;
    }

    public void setCountryTier2MinAmt(BigDecimal countryTier2MinAmt) {
        this.countryTier2MinAmt = countryTier2MinAmt;
    }

    public BigDecimal getCountryTier2MaxAmt() {
        return countryTier2MaxAmt;
    }

    public void setCountryTier2MaxAmt(BigDecimal countryTier2MaxAmt) {
        this.countryTier2MaxAmt = countryTier2MaxAmt;
    }

    public String getCountryTier2EntamFlag() {
        return countryTier2EntamFlag;
    }

    public void setCountryTier2EntamFlag(String countryTier2EntamFlag) {
        this.countryTier2EntamFlag = countryTier2EntamFlag;
    }

    public String getCountryTier3EntamFlag() {
        return countryTier3EntamFlag;
    }

    public void setCountryTier3EntamFlag(String countryTier3EntamFlag) {
        this.countryTier3EntamFlag = countryTier3EntamFlag;
    }

    public BigDecimal getCountryMaxtaxAmt() {
        return countryMaxtaxAmt;
    }

    public void setCountryMaxtaxAmt(BigDecimal countryMaxtaxAmt) {
        this.countryMaxtaxAmt = countryMaxtaxAmt;
    }

    public BigDecimal getCountryTaxableThresholdAmt() {
        return countryTaxableThresholdAmt;
    }

    public void setCountryTaxableThresholdAmt(BigDecimal countryTaxableThresholdAmt) {
        this.countryTaxableThresholdAmt = countryTaxableThresholdAmt;
    }

    public BigDecimal getCountryMinimumTaxableAmt() {
        return countryMinimumTaxableAmt;
    }

    public void setCountryMinimumTaxableAmt(BigDecimal countryMinimumTaxableAmt) {
        this.countryMinimumTaxableAmt = countryMinimumTaxableAmt;
    }

    public BigDecimal getCountryMaximumTaxableAmt() {
        return countryMaximumTaxableAmt;
    }

    public void setCountryMaximumTaxableAmt(BigDecimal countryMaximumTaxableAmt) {
        this.countryMaximumTaxableAmt = countryMaximumTaxableAmt;
    }

    public BigDecimal getCountryMaximumTaxAmt() {
        return countryMaximumTaxAmt;
    }

    public void setCountryMaximumTaxAmt(BigDecimal countryMaximumTaxAmt) {
        this.countryMaximumTaxAmt = countryMaximumTaxAmt;
    }

    public BigDecimal getCountryBaseChangePct() {
        return countryBaseChangePct;
    }

    public void setCountryBaseChangePct(BigDecimal countryBaseChangePct) {
        this.countryBaseChangePct = countryBaseChangePct;
    }

    public BigDecimal getCountrySpecialRate() {
        return countrySpecialRate;
    }

    public void setCountrySpecialRate(BigDecimal countrySpecialRate) {
        this.countrySpecialRate = countrySpecialRate;
    }

    public BigDecimal getCountrySpecialSetamt() {
        return countrySpecialSetamt;
    }

    public void setCountrySpecialSetamt(BigDecimal countrySpecialSetamt) {
        this.countrySpecialSetamt = countrySpecialSetamt;
    }

    public String getCountryTaxProcessTypeCode() {
        return countryTaxProcessTypeCode;
    }

    public void setCountryTaxProcessTypeCode(String countryTaxProcessTypeCode) {
        this.countryTaxProcessTypeCode = countryTaxProcessTypeCode;
    }

    public BigDecimal getStateTier1Rate() {
        return stateTier1Rate;
    }

    public void setStateTier1Rate(BigDecimal stateTier1Rate) {
        this.stateTier1Rate = stateTier1Rate;
    }

    public BigDecimal getStateTier2Rate() {
        return stateTier2Rate;
    }

    public void setStateTier2Rate(BigDecimal stateTier2Rate) {
        this.stateTier2Rate = stateTier2Rate;
    }

    public BigDecimal getStateTier3Rate() {
        return stateTier3Rate;
    }

    public void setStateTier3Rate(BigDecimal stateTier3Rate) {
        this.stateTier3Rate = stateTier3Rate;
    }

    public BigDecimal getStateTier1Setamt() {
        return stateTier1Setamt;
    }

    public void setStateTier1Setamt(BigDecimal stateTier1Setamt) {
        this.stateTier1Setamt = stateTier1Setamt;
    }

    public BigDecimal getStateTier2Setamt() {
        return stateTier2Setamt;
    }

    public void setStateTier2Setamt(BigDecimal stateTier2Setamt) {
        this.stateTier2Setamt = stateTier2Setamt;
    }

    public BigDecimal getStateTier3Setamt() {
        return stateTier3Setamt;
    }

    public void setStateTier3Setamt(BigDecimal stateTier3Setamt) {
        this.stateTier3Setamt = stateTier3Setamt;
    }

    public BigDecimal getStateTier1MaxAmt() {
        return stateTier1MaxAmt;
    }

    public void setStateTier1MaxAmt(BigDecimal stateTier1MaxAmt) {
        this.stateTier1MaxAmt = stateTier1MaxAmt;
    }

    public BigDecimal getStateTier2MinAmt() {
        return stateTier2MinAmt;
    }

    public void setStateTier2MinAmt(BigDecimal stateTier2MinAmt) {
        this.stateTier2MinAmt = stateTier2MinAmt;
    }

    public BigDecimal getStateTier2MaxAmt() {
        return stateTier2MaxAmt;
    }

    public void setStateTier2MaxAmt(BigDecimal stateTier2MaxAmt) {
        this.stateTier2MaxAmt = stateTier2MaxAmt;
    }

    public String getStateTier2EntamFlag() {
        return stateTier2EntamFlag;
    }

    public void setStateTier2EntamFlag(String stateTier2EntamFlag) {
        this.stateTier2EntamFlag = stateTier2EntamFlag;
    }

    public String getStateTier3EntamFlag() {
        return stateTier3EntamFlag;
    }

    public void setStateTier3EntamFlag(String stateTier3EntamFlag) {
        this.stateTier3EntamFlag = stateTier3EntamFlag;
    }

    public BigDecimal getStateMaxtaxAmt() {
        return stateMaxtaxAmt;
    }

    public void setStateMaxtaxAmt(BigDecimal stateMaxtaxAmt) {
        this.stateMaxtaxAmt = stateMaxtaxAmt;
    }

    public BigDecimal getStateTaxableThresholdAmt() {
        return stateTaxableThresholdAmt;
    }

    public void setStateTaxableThresholdAmt(BigDecimal stateTaxableThresholdAmt) {
        this.stateTaxableThresholdAmt = stateTaxableThresholdAmt;
    }

    public BigDecimal getStateMinimumTaxableAmt() {
        return stateMinimumTaxableAmt;
    }

    public void setStateMinimumTaxableAmt(BigDecimal stateMinimumTaxableAmt) {
        this.stateMinimumTaxableAmt = stateMinimumTaxableAmt;
    }

    public BigDecimal getStateMaximumTaxableAmt() {
        return stateMaximumTaxableAmt;
    }

    public void setStateMaximumTaxableAmt(BigDecimal stateMaximumTaxableAmt) {
        this.stateMaximumTaxableAmt = stateMaximumTaxableAmt;
    }

    public BigDecimal getStateMaximumTaxAmt() {
        return stateMaximumTaxAmt;
    }

    public void setStateMaximumTaxAmt(BigDecimal stateMaximumTaxAmt) {
        this.stateMaximumTaxAmt = stateMaximumTaxAmt;
    }

    public BigDecimal getStateBaseChangePct() {
        return stateBaseChangePct;
    }

    public void setStateBaseChangePct(BigDecimal stateBaseChangePct) {
        this.stateBaseChangePct = stateBaseChangePct;
    }

    public BigDecimal getStateSpecialRate() {
        return stateSpecialRate;
    }

    public void setStateSpecialRate(BigDecimal stateSpecialRate) {
        this.stateSpecialRate = stateSpecialRate;
    }

    public BigDecimal getStateSpecialSetamt() {
        return stateSpecialSetamt;
    }

    public void setStateSpecialSetamt(BigDecimal stateSpecialSetamt) {
        this.stateSpecialSetamt = stateSpecialSetamt;
    }

    public String getStateTaxProcessTypeCode() {
        return stateTaxProcessTypeCode;
    }

    public void setStateTaxProcessTypeCode(String stateTaxProcessTypeCode) {
        this.stateTaxProcessTypeCode = stateTaxProcessTypeCode;
    }

    public BigDecimal getCountyTier1Rate() {
        return countyTier1Rate;
    }

    public void setCountyTier1Rate(BigDecimal countyTier1Rate) {
        this.countyTier1Rate = countyTier1Rate;
    }

    public BigDecimal getCountyTier2Rate() {
        return countyTier2Rate;
    }

    public void setCountyTier2Rate(BigDecimal countyTier2Rate) {
        this.countyTier2Rate = countyTier2Rate;
    }

    public BigDecimal getCountyTier3Rate() {
        return countyTier3Rate;
    }

    public void setCountyTier3Rate(BigDecimal countyTier3Rate) {
        this.countyTier3Rate = countyTier3Rate;
    }

    public BigDecimal getCountyTier1Setamt() {
        return countyTier1Setamt;
    }

    public void setCountyTier1Setamt(BigDecimal countyTier1Setamt) {
        this.countyTier1Setamt = countyTier1Setamt;
    }

    public BigDecimal getCountyTier2Setamt() {
        return countyTier2Setamt;
    }

    public void setCountyTier2Setamt(BigDecimal countyTier2Setamt) {
        this.countyTier2Setamt = countyTier2Setamt;
    }

    public BigDecimal getCountyTier3Setamt() {
        return countyTier3Setamt;
    }

    public void setCountyTier3Setamt(BigDecimal countyTier3Setamt) {
        this.countyTier3Setamt = countyTier3Setamt;
    }

    public BigDecimal getCountyTier1MaxAmt() {
        return countyTier1MaxAmt;
    }

    public void setCountyTier1MaxAmt(BigDecimal countyTier1MaxAmt) {
        this.countyTier1MaxAmt = countyTier1MaxAmt;
    }

    public BigDecimal getCountyTier2MinAmt() {
        return countyTier2MinAmt;
    }

    public void setCountyTier2MinAmt(BigDecimal countyTier2MinAmt) {
        this.countyTier2MinAmt = countyTier2MinAmt;
    }

    public BigDecimal getCountyTier2MaxAmt() {
        return countyTier2MaxAmt;
    }

    public void setCountyTier2MaxAmt(BigDecimal countyTier2MaxAmt) {
        this.countyTier2MaxAmt = countyTier2MaxAmt;
    }

    public String getCountyTier2EntamFlag() {
        return countyTier2EntamFlag;
    }

    public void setCountyTier2EntamFlag(String countyTier2EntamFlag) {
        this.countyTier2EntamFlag = countyTier2EntamFlag;
    }

    public String getCountyTier3EntamFlag() {
        return countyTier3EntamFlag;
    }

    public void setCountyTier3EntamFlag(String countyTier3EntamFlag) {
        this.countyTier3EntamFlag = countyTier3EntamFlag;
    }

    public BigDecimal getCountyMaxtaxAmt() {
        return countyMaxtaxAmt;
    }

    public void setCountyMaxtaxAmt(BigDecimal countyMaxtaxAmt) {
        this.countyMaxtaxAmt = countyMaxtaxAmt;
    }

    public BigDecimal getCountyTaxableThresholdAmt() {
        return countyTaxableThresholdAmt;
    }

    public void setCountyTaxableThresholdAmt(BigDecimal countyTaxableThresholdAmt) {
        this.countyTaxableThresholdAmt = countyTaxableThresholdAmt;
    }

    public BigDecimal getCountyMinimumTaxableAmt() {
        return countyMinimumTaxableAmt;
    }

    public void setCountyMinimumTaxableAmt(BigDecimal countyMinimumTaxableAmt) {
        this.countyMinimumTaxableAmt = countyMinimumTaxableAmt;
    }

    public BigDecimal getCountyMaximumTaxableAmt() {
        return countyMaximumTaxableAmt;
    }

    public void setCountyMaximumTaxableAmt(BigDecimal countyMaximumTaxableAmt) {
        this.countyMaximumTaxableAmt = countyMaximumTaxableAmt;
    }

    public BigDecimal getCountyMaximumTaxAmt() {
        return countyMaximumTaxAmt;
    }

    public void setCountyMaximumTaxAmt(BigDecimal countyMaximumTaxAmt) {
        this.countyMaximumTaxAmt = countyMaximumTaxAmt;
    }

    public BigDecimal getCountyBaseChangePct() {
        return countyBaseChangePct;
    }

    public void setCountyBaseChangePct(BigDecimal countyBaseChangePct) {
        this.countyBaseChangePct = countyBaseChangePct;
    }

    public BigDecimal getCountySpecialRate() {
        return countySpecialRate;
    }

    public void setCountySpecialRate(BigDecimal countySpecialRate) {
        this.countySpecialRate = countySpecialRate;
    }

    public BigDecimal getCountySpecialSetamt() {
        return countySpecialSetamt;
    }

    public void setCountySpecialSetamt(BigDecimal countySpecialSetamt) {
        this.countySpecialSetamt = countySpecialSetamt;
    }

    public String getCountyTaxProcessTypeCode() {
        return countyTaxProcessTypeCode;
    }

    public void setCountyTaxProcessTypeCode(String countyTaxProcessTypeCode) {
        this.countyTaxProcessTypeCode = countyTaxProcessTypeCode;
    }

    public BigDecimal getCityTier1Rate() {
        return cityTier1Rate;
    }

    public void setCityTier1Rate(BigDecimal cityTier1Rate) {
        this.cityTier1Rate = cityTier1Rate;
    }

    public BigDecimal getCityTier2Rate() {
        return cityTier2Rate;
    }

    public void setCityTier2Rate(BigDecimal cityTier2Rate) {
        this.cityTier2Rate = cityTier2Rate;
    }

    public BigDecimal getCityTier3Rate() {
        return cityTier3Rate;
    }

    public void setCityTier3Rate(BigDecimal cityTier3Rate) {
        this.cityTier3Rate = cityTier3Rate;
    }

    public BigDecimal getCityTier1Setamt() {
        return cityTier1Setamt;
    }

    public void setCityTier1Setamt(BigDecimal cityTier1Setamt) {
        this.cityTier1Setamt = cityTier1Setamt;
    }

    public BigDecimal getCityTier2Setamt() {
        return cityTier2Setamt;
    }

    public void setCityTier2Setamt(BigDecimal cityTier2Setamt) {
        this.cityTier2Setamt = cityTier2Setamt;
    }

    public BigDecimal getCityTier3Setamt() {
        return cityTier3Setamt;
    }

    public void setCityTier3Setamt(BigDecimal cityTier3Setamt) {
        this.cityTier3Setamt = cityTier3Setamt;
    }

    public BigDecimal getCityTier1MaxAmt() {
        return cityTier1MaxAmt;
    }

    public void setCityTier1MaxAmt(BigDecimal cityTier1MaxAmt) {
        this.cityTier1MaxAmt = cityTier1MaxAmt;
    }

    public BigDecimal getCityTier2MinAmt() {
        return cityTier2MinAmt;
    }

    public void setCityTier2MinAmt(BigDecimal cityTier2MinAmt) {
        this.cityTier2MinAmt = cityTier2MinAmt;
    }

    public BigDecimal getCityTier2MaxAmt() {
        return cityTier2MaxAmt;
    }

    public void setCityTier2MaxAmt(BigDecimal cityTier2MaxAmt) {
        this.cityTier2MaxAmt = cityTier2MaxAmt;
    }

    public String getCityTier2EntamFlag() {
        return cityTier2EntamFlag;
    }

    public void setCityTier2EntamFlag(String cityTier2EntamFlag) {
        this.cityTier2EntamFlag = cityTier2EntamFlag;
    }

    public String getCityTier3EntamFlag() {
        return cityTier3EntamFlag;
    }

    public void setCityTier3EntamFlag(String cityTier3EntamFlag) {
        this.cityTier3EntamFlag = cityTier3EntamFlag;
    }

    public BigDecimal getCityMaxtaxAmt() {
        return cityMaxtaxAmt;
    }

    public void setCityMaxtaxAmt(BigDecimal cityMaxtaxAmt) {
        this.cityMaxtaxAmt = cityMaxtaxAmt;
    }

    public BigDecimal getCityTaxableThresholdAmt() {
        return cityTaxableThresholdAmt;
    }

    public void setCityTaxableThresholdAmt(BigDecimal cityTaxableThresholdAmt) {
        this.cityTaxableThresholdAmt = cityTaxableThresholdAmt;
    }

    public BigDecimal getCityMinimumTaxableAmt() {
        return cityMinimumTaxableAmt;
    }

    public void setCityMinimumTaxableAmt(BigDecimal cityMinimumTaxableAmt) {
        this.cityMinimumTaxableAmt = cityMinimumTaxableAmt;
    }

    public BigDecimal getCityMaximumTaxableAmt() {
        return cityMaximumTaxableAmt;
    }

    public void setCityMaximumTaxableAmt(BigDecimal cityMaximumTaxableAmt) {
        this.cityMaximumTaxableAmt = cityMaximumTaxableAmt;
    }

    public BigDecimal getCityMaximumTaxAmt() {
        return cityMaximumTaxAmt;
    }

    public void setCityMaximumTaxAmt(BigDecimal cityMaximumTaxAmt) {
        this.cityMaximumTaxAmt = cityMaximumTaxAmt;
    }

    public BigDecimal getCityBaseChangePct() {
        return cityBaseChangePct;
    }

    public void setCityBaseChangePct(BigDecimal cityBaseChangePct) {
        this.cityBaseChangePct = cityBaseChangePct;
    }

    public BigDecimal getCitySpecialRate() {
        return citySpecialRate;
    }

    public void setCitySpecialRate(BigDecimal citySpecialRate) {
        this.citySpecialRate = citySpecialRate;
    }

    public BigDecimal getCitySpecialSetamt() {
        return citySpecialSetamt;
    }

    public void setCitySpecialSetamt(BigDecimal citySpecialSetamt) {
        this.citySpecialSetamt = citySpecialSetamt;
    }

    public String getCityTaxProcessTypeCode() {
        return cityTaxProcessTypeCode;
    }

    public void setCityTaxProcessTypeCode(String cityTaxProcessTypeCode) {
        this.cityTaxProcessTypeCode = cityTaxProcessTypeCode;
    }

    public BigDecimal getStj1Rate() {
        return stj1Rate;
    }

    public void setStj1Rate(BigDecimal stj1Rate) {
        this.stj1Rate = stj1Rate;
    }

    public BigDecimal getStj1Setamt() {
        return stj1Setamt;
    }

    public void setStj1Setamt(BigDecimal stj1Setamt) {
        this.stj1Setamt = stj1Setamt;
    }

    public BigDecimal getStj2Rate() {
        return stj2Rate;
    }

    public void setStj2Rate(BigDecimal stj2Rate) {
        this.stj2Rate = stj2Rate;
    }

    public BigDecimal getStj2Setamt() {
        return stj2Setamt;
    }

    public void setStj2Setamt(BigDecimal stj2Setamt) {
        this.stj2Setamt = stj2Setamt;
    }

    public BigDecimal getStj3Rate() {
        return stj3Rate;
    }

    public void setStj3Rate(BigDecimal stj3Rate) {
        this.stj3Rate = stj3Rate;
    }

    public BigDecimal getStj3Setamt() {
        return stj3Setamt;
    }

    public void setStj3Setamt(BigDecimal stj3Setamt) {
        this.stj3Setamt = stj3Setamt;
    }

    public BigDecimal getStj4Rate() {
        return stj4Rate;
    }

    public void setStj4Rate(BigDecimal stj4Rate) {
        this.stj4Rate = stj4Rate;
    }

    public BigDecimal getStj4Setamt() {
        return stj4Setamt;
    }

    public void setStj4Setamt(BigDecimal stj4Setamt) {
        this.stj4Setamt = stj4Setamt;
    }

    public BigDecimal getStj5Rate() {
        return stj5Rate;
    }

    public void setStj5Rate(BigDecimal stj5Rate) {
        this.stj5Rate = stj5Rate;
    }

    public BigDecimal getStj5Setamt() {
        return stj5Setamt;
    }

    public void setStj5Setamt(BigDecimal stj5Setamt) {
        this.stj5Setamt = stj5Setamt;
    }

    public BigDecimal getStj6Rate() {
        return stj6Rate;
    }

    public void setStj6Rate(BigDecimal stj6Rate) {
        this.stj6Rate = stj6Rate;
    }

    public BigDecimal getStj6Setamt() {
        return stj6Setamt;
    }

    public void setStj6Setamt(BigDecimal stj6Setamt) {
        this.stj6Setamt = stj6Setamt;
    }

    public BigDecimal getStj7Rate() {
        return stj7Rate;
    }

    public void setStj7Rate(BigDecimal stj7Rate) {
        this.stj7Rate = stj7Rate;
    }

    public BigDecimal getStj7Setamt() {
        return stj7Setamt;
    }

    public void setStj7Setamt(BigDecimal stj7Setamt) {
        this.stj7Setamt = stj7Setamt;
    }

    public BigDecimal getStj8Rate() {
        return stj8Rate;
    }

    public void setStj8Rate(BigDecimal stj8Rate) {
        this.stj8Rate = stj8Rate;
    }

    public BigDecimal getStj8Setamt() {
        return stj8Setamt;
    }

    public void setStj8Setamt(BigDecimal stj8Setamt) {
        this.stj8Setamt = stj8Setamt;
    }

    public BigDecimal getStj9Rate() {
        return stj9Rate;
    }

    public void setStj9Rate(BigDecimal stj9Rate) {
        this.stj9Rate = stj9Rate;
    }

    public BigDecimal getStj9Setamt() {
        return stj9Setamt;
    }

    public void setStj9Setamt(BigDecimal stj9Setamt) {
        this.stj9Setamt = stj9Setamt;
    }

    public BigDecimal getStj10Rate() {
        return stj10Rate;
    }

    public void setStj10Rate(BigDecimal stj10Rate) {
        this.stj10Rate = stj10Rate;
    }

    public BigDecimal getStj10Setamt() {
        return stj10Setamt;
    }

    public void setStj10Setamt(BigDecimal stj10Setamt) {
        this.stj10Setamt = stj10Setamt;
    }

    public BigDecimal getCombinedRate() {
        return combinedRate;
    }

    public void setCombinedRate(BigDecimal combinedRate) {
        this.combinedRate = combinedRate;
    }

    public String getCountryTaxtypeUsedCode() {
        return countryTaxtypeUsedCode;
    }

    public void setCountryTaxtypeUsedCode(String countryTaxtypeUsedCode) {
        this.countryTaxtypeUsedCode = countryTaxtypeUsedCode;
    }

    public String getStateTaxtypeUsedCode() {
        return stateTaxtypeUsedCode;
    }

    public void setStateTaxtypeUsedCode(String stateTaxtypeUsedCode) {
        this.stateTaxtypeUsedCode = stateTaxtypeUsedCode;
    }

    public String getCountyTaxtypeUsedCode() {
        return countyTaxtypeUsedCode;
    }

    public void setCountyTaxtypeUsedCode(String countyTaxtypeUsedCode) {
        this.countyTaxtypeUsedCode = countyTaxtypeUsedCode;
    }

    public String getCityTaxtypeUsedCode() {
        return cityTaxtypeUsedCode;
    }

    public void setCityTaxtypeUsedCode(String cityTaxtypeUsedCode) {
        this.cityTaxtypeUsedCode = cityTaxtypeUsedCode;
    }

    public String getStj1TaxtypeUsedCode() {
        return stj1TaxtypeUsedCode;
    }

    public void setStj1TaxtypeUsedCode(String stj1TaxtypeUsedCode) {
        this.stj1TaxtypeUsedCode = stj1TaxtypeUsedCode;
    }

    public String getStj2TaxtypeUsedCode() {
        return stj2TaxtypeUsedCode;
    }

    public void setStj2TaxtypeUsedCode(String stj2TaxtypeUsedCode) {
        this.stj2TaxtypeUsedCode = stj2TaxtypeUsedCode;
    }

    public String getStj3TaxtypeUsedCode() {
        return stj3TaxtypeUsedCode;
    }

    public void setStj3TaxtypeUsedCode(String stj3TaxtypeUsedCode) {
        this.stj3TaxtypeUsedCode = stj3TaxtypeUsedCode;
    }

    public String getStj4TaxtypeUsedCode() {
        return stj4TaxtypeUsedCode;
    }

    public void setStj4TaxtypeUsedCode(String stj4TaxtypeUsedCode) {
        this.stj4TaxtypeUsedCode = stj4TaxtypeUsedCode;
    }

    public String getStj5TaxtypeUsedCode() {
        return stj5TaxtypeUsedCode;
    }

    public void setStj5TaxtypeUsedCode(String stj5TaxtypeUsedCode) {
        this.stj5TaxtypeUsedCode = stj5TaxtypeUsedCode;
    }

    public String getStj6TaxtypeUsedCode() {
        return stj6TaxtypeUsedCode;
    }

    public void setStj6TaxtypeUsedCode(String stj6TaxtypeUsedCode) {
        this.stj6TaxtypeUsedCode = stj6TaxtypeUsedCode;
    }

    public String getStj7TaxtypeUsedCode() {
        return stj7TaxtypeUsedCode;
    }

    public void setStj7TaxtypeUsedCode(String stj7TaxtypeUsedCode) {
        this.stj7TaxtypeUsedCode = stj7TaxtypeUsedCode;
    }

    public String getStj8TaxtypeUsedCode() {
        return stj8TaxtypeUsedCode;
    }

    public void setStj8TaxtypeUsedCode(String stj8TaxtypeUsedCode) {
        this.stj8TaxtypeUsedCode = stj8TaxtypeUsedCode;
    }

    public String getStj9TaxtypeUsedCode() {
        return stj9TaxtypeUsedCode;
    }

    public void setStj9TaxtypeUsedCode(String stj9TaxtypeUsedCode) {
        this.stj9TaxtypeUsedCode = stj9TaxtypeUsedCode;
    }

    public String getStj10TaxtypeUsedCode() {
        return stj10TaxtypeUsedCode;
    }

    public void setStj10TaxtypeUsedCode(String stj10TaxtypeUsedCode) {
        this.stj10TaxtypeUsedCode = stj10TaxtypeUsedCode;
    }

    public BigDecimal getStj1TaxableAmt() {
        return stj1TaxableAmt;
    }

    public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt) {
        this.stj1TaxableAmt = stj1TaxableAmt;
    }

    public BigDecimal getStj2TaxableAmt() {
        return stj2TaxableAmt;
    }

    public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt) {
        this.stj2TaxableAmt = stj2TaxableAmt;
    }

    public BigDecimal getStj3TaxableAmt() {
        return stj3TaxableAmt;
    }

    public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt) {
        this.stj3TaxableAmt = stj3TaxableAmt;
    }

    public BigDecimal getStj4TaxableAmt() {
        return stj4TaxableAmt;
    }

    public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt) {
        this.stj4TaxableAmt = stj4TaxableAmt;
    }

    public BigDecimal getStj5TaxableAmt() {
        return stj5TaxableAmt;
    }

    public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt) {
        this.stj5TaxableAmt = stj5TaxableAmt;
    }

    public BigDecimal getStj6TaxableAmt() {
        return stj6TaxableAmt;
    }

    public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt) {
        this.stj6TaxableAmt = stj6TaxableAmt;
    }

    public BigDecimal getStj7TaxableAmt() {
        return stj7TaxableAmt;
    }

    public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt) {
        this.stj7TaxableAmt = stj7TaxableAmt;
    }

    public BigDecimal getStj8TaxableAmt() {
        return stj8TaxableAmt;
    }

    public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt) {
        this.stj8TaxableAmt = stj8TaxableAmt;
    }

    public BigDecimal getStj9TaxableAmt() {
        return stj9TaxableAmt;
    }

    public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt) {
        this.stj9TaxableAmt = stj9TaxableAmt;
    }

    public BigDecimal getStj10TaxableAmt() {
        return stj10TaxableAmt;
    }

    public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt) {
        this.stj10TaxableAmt = stj10TaxableAmt;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public Date getModifyTimestamp() {
        return modifyTimestamp;
    }

    public void setModifyTimestamp(Date modifyTimestamp) {
        this.modifyTimestamp = modifyTimestamp;
    }

   /* public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }
*/
    public Boolean getCountryTier2EntamBooleanFlag() {
        return "1".equals(countryTier2EntamFlag) || "T".equalsIgnoreCase(countryTier2EntamFlag) || "Y".equalsIgnoreCase(countryTier2EntamFlag);
    }

    public Boolean getCountryTier3EntamBooleanFlag() {
        return "1".equals(countryTier3EntamFlag) || "T".equalsIgnoreCase(countryTier3EntamFlag) || "Y".equalsIgnoreCase(countryTier3EntamFlag);
    }

    public Boolean getStateTier2EntamBooleanFlag() {
        return "1".equals(stateTier2EntamFlag) || "T".equalsIgnoreCase(stateTier2EntamFlag) || "Y".equalsIgnoreCase(stateTier2EntamFlag);
    }

    public Boolean getStateTier3EntamBooleanFlag() {
        return "1".equals(stateTier3EntamFlag) || "T".equalsIgnoreCase(stateTier3EntamFlag) || "Y".equalsIgnoreCase(stateTier3EntamFlag);
    }

    public Boolean getCountyTier2EntamBooleanFlag() {
        return "1".equals(countyTier2EntamFlag) || "T".equalsIgnoreCase(countyTier2EntamFlag) || "Y".equalsIgnoreCase(countyTier2EntamFlag);
    }

    public Boolean getCountyTier3EntamBooleanFlag() {
        return "1".equals(countyTier3EntamFlag) || "T".equalsIgnoreCase(countyTier3EntamFlag) || "Y".equalsIgnoreCase(countyTier3EntamFlag);
    }

    public Boolean getCityTier2EntamBooleanFlag() {
        return "1".equals(cityTier2EntamFlag) || "T".equalsIgnoreCase(cityTier2EntamFlag) || "Y".equalsIgnoreCase(cityTier2EntamFlag);
    }

    public Boolean getCityTier3EntamBooleanFlag() {
        return "1".equals(cityTier3EntamFlag) || "T".equalsIgnoreCase(cityTier3EntamFlag) | "Y".equalsIgnoreCase(cityTier3EntamFlag);
    }

    public Date getGlDate() {
        return glDate;
    }

    public void setGlDate(Date glDate) {
        this.glDate = glDate;
    }

    public BigDecimal getGlLineItmDistAmt() {
        return glLineItmDistAmt;
    }

    public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt) {
        this.glLineItmDistAmt = glLineItmDistAmt;
    }

    public Long getShiptoLocnMatrixId() {
        return shiptoLocnMatrixId;
    }

    public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId) {
        this.shiptoLocnMatrixId = shiptoLocnMatrixId;
    }

    public Long getShipfromLocnMatrixId() {
        return shipfromLocnMatrixId;
    }

    public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId) {
        this.shipfromLocnMatrixId = shipfromLocnMatrixId;
    }

    public Long getOrdracptLocnMatrixId() {
        return ordracptLocnMatrixId;
    }

    public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId) {
        this.ordracptLocnMatrixId = ordracptLocnMatrixId;
    }
    
    public Long getOrdrorgnLocnMatrixId() {
        return ordrorgnLocnMatrixId;
    }

    public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId) {
        this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
    }

    public Long getFirstuseLocnMatrixId() {
        return firstuseLocnMatrixId;
    }

    public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId) {
        this.firstuseLocnMatrixId = firstuseLocnMatrixId;
    }

    public Long getBilltoLocnMatrixId() {
        return billtoLocnMatrixId;
    }

    public void setBilltoLocnMatrixId(Long billtoLocnMatrixId) {
        this.billtoLocnMatrixId = billtoLocnMatrixId;
    }

    public Long getTtlxfrLocnMatrixId() {
        return ttlxfrLocnMatrixId;
    }

    public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId) {
        this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
    }

    public String getShiptoManualJurInd() {
        return shiptoManualJurInd;
    }

    public void setShiptoManualJurInd(String shiptoManualJurInd) {
        this.shiptoManualJurInd = shiptoManualJurInd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PurchaseTransactionLog that = (PurchaseTransactionLog) o;

        if (purchtransLogId != null ? !purchtransLogId.equals(that.purchtransLogId) : that.purchtransLogId != null)
            return false;
        if (purchtransId != null ? !purchtransId.equals(that.purchtransId) : that.purchtransId != null) return false;
        if (logSource != null ? !logSource.equals(that.logSource) : that.logSource != null) return false;
        if (glExtractBatchNo != null ? !glExtractBatchNo.equals(that.glExtractBatchNo) : that.glExtractBatchNo != null)
            return false;
        if (glExtractTimestamp != null ? !glExtractTimestamp.equals(that.glExtractTimestamp) : that.glExtractTimestamp != null)
            return false;
        if (directPayPermitFlag != null ? !directPayPermitFlag.equals(that.directPayPermitFlag) : that.directPayPermitFlag != null)
            return false;
        if (calculateInd != null ? !calculateInd.equals(that.calculateInd) : that.calculateInd != null) return false;
        if (glDate != null ? !glDate.equals(that.glDate) : that.glDate != null) return false;
        if (glLineItmDistAmt != null ? !glLineItmDistAmt.equals(that.glLineItmDistAmt) : that.glLineItmDistAmt != null)
            return false;
        if (allocationMatrixId != null ? !allocationMatrixId.equals(that.allocationMatrixId) : that.allocationMatrixId != null)
            return false;
        if (allocationSubtransId != null ? !allocationSubtransId.equals(that.allocationSubtransId) : that.allocationSubtransId != null)
            return false;
        if (taxAllocMatrixId != null ? !taxAllocMatrixId.equals(that.taxAllocMatrixId) : that.taxAllocMatrixId != null)
            return false;
        if (splitSubtransId != null ? !splitSubtransId.equals(that.splitSubtransId) : that.splitSubtransId != null)
            return false;
        if (multiTransCode != null ? !multiTransCode.equals(that.multiTransCode) : that.multiTransCode != null)
            return false;
        if (taxcodeCode != null ? !taxcodeCode.equals(that.taxcodeCode) : that.taxcodeCode != null) return false;
        if (manualTaxcodeInd != null ? !manualTaxcodeInd.equals(that.manualTaxcodeInd) : that.manualTaxcodeInd != null)
            return false;
        if (taxMatrixId != null ? !taxMatrixId.equals(that.taxMatrixId) : that.taxMatrixId != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (processingNotes != null ? !processingNotes.equals(that.processingNotes) : that.processingNotes != null)
            return false;
        if (transactionStateCode != null ? !transactionStateCode.equals(that.transactionStateCode) : that.transactionStateCode != null)
            return false;
        if (autoTransactionStateCode != null ? !autoTransactionStateCode.equals(that.autoTransactionStateCode) : that.autoTransactionStateCode != null)
            return false;
        if (transactionCountryCode != null ? !transactionCountryCode.equals(that.transactionCountryCode) : that.transactionCountryCode != null)
            return false;
        if (autoTransactionCountryCode != null ? !autoTransactionCountryCode.equals(that.autoTransactionCountryCode) : that.autoTransactionCountryCode != null)
            return false;
        if (transactionInd != null ? !transactionInd.equals(that.transactionInd) : that.transactionInd != null)
            return false;
        if (suspendInd != null ? !suspendInd.equals(that.suspendInd) : that.suspendInd != null) return false;
        if (rejectFlag != null ? !rejectFlag.equals(that.rejectFlag) : that.rejectFlag != null) return false;
        if (countryTier1TaxableAmt != null ? !countryTier1TaxableAmt.equals(that.countryTier1TaxableAmt) : that.countryTier1TaxableAmt != null)
            return false;
        if (countryTier2TaxableAmt != null ? !countryTier2TaxableAmt.equals(that.countryTier2TaxableAmt) : that.countryTier2TaxableAmt != null)
            return false;
        if (countryTier3TaxableAmt != null ? !countryTier3TaxableAmt.equals(that.countryTier3TaxableAmt) : that.countryTier3TaxableAmt != null)
            return false;
        if (stateTier1TaxableAmt != null ? !stateTier1TaxableAmt.equals(that.stateTier1TaxableAmt) : that.stateTier1TaxableAmt != null)
            return false;
        if (stateTier2TaxableAmt != null ? !stateTier2TaxableAmt.equals(that.stateTier2TaxableAmt) : that.stateTier2TaxableAmt != null)
            return false;
        if (stateTier3TaxableAmt != null ? !stateTier3TaxableAmt.equals(that.stateTier3TaxableAmt) : that.stateTier3TaxableAmt != null)
            return false;
        if (countyTier1TaxableAmt != null ? !countyTier1TaxableAmt.equals(that.countyTier1TaxableAmt) : that.countyTier1TaxableAmt != null)
            return false;
        if (countyTier2TaxableAmt != null ? !countyTier2TaxableAmt.equals(that.countyTier2TaxableAmt) : that.countyTier2TaxableAmt != null)
            return false;
        if (countyTier3TaxableAmt != null ? !countyTier3TaxableAmt.equals(that.countyTier3TaxableAmt) : that.countyTier3TaxableAmt != null)
            return false;
        if (cityTier1TaxableAmt != null ? !cityTier1TaxableAmt.equals(that.cityTier1TaxableAmt) : that.cityTier1TaxableAmt != null)
            return false;
        if (cityTier2TaxableAmt != null ? !cityTier2TaxableAmt.equals(that.cityTier2TaxableAmt) : that.cityTier2TaxableAmt != null)
            return false;
        if (cityTier3TaxableAmt != null ? !cityTier3TaxableAmt.equals(that.cityTier3TaxableAmt) : that.cityTier3TaxableAmt != null)
            return false;
        if (stj1TaxableAmt != null ? !stj1TaxableAmt.equals(that.stj1TaxableAmt) : that.stj1TaxableAmt != null)
            return false;
        if (stj2TaxableAmt != null ? !stj2TaxableAmt.equals(that.stj2TaxableAmt) : that.stj2TaxableAmt != null)
            return false;
        if (stj3TaxableAmt != null ? !stj3TaxableAmt.equals(that.stj3TaxableAmt) : that.stj3TaxableAmt != null)
            return false;
        if (stj4TaxableAmt != null ? !stj4TaxableAmt.equals(that.stj4TaxableAmt) : that.stj4TaxableAmt != null)
            return false;
        if (stj5TaxableAmt != null ? !stj5TaxableAmt.equals(that.stj5TaxableAmt) : that.stj5TaxableAmt != null)
            return false;
        if (stj6TaxableAmt != null ? !stj6TaxableAmt.equals(that.stj6TaxableAmt) : that.stj6TaxableAmt != null)
            return false;
        if (stj7TaxableAmt != null ? !stj7TaxableAmt.equals(that.stj7TaxableAmt) : that.stj7TaxableAmt != null)
            return false;
        if (stj8TaxableAmt != null ? !stj8TaxableAmt.equals(that.stj8TaxableAmt) : that.stj8TaxableAmt != null)
            return false;
        if (stj9TaxableAmt != null ? !stj9TaxableAmt.equals(that.stj9TaxableAmt) : that.stj9TaxableAmt != null)
            return false;
        if (stj10TaxableAmt != null ? !stj10TaxableAmt.equals(that.stj10TaxableAmt) : that.stj10TaxableAmt != null)
            return false;
        if (bCountryTier1TaxAmt != null ? !bCountryTier1TaxAmt.equals(that.bCountryTier1TaxAmt) : that.bCountryTier1TaxAmt != null)
            return false;
        if (bCountryTier2TaxAmt != null ? !bCountryTier2TaxAmt.equals(that.bCountryTier2TaxAmt) : that.bCountryTier2TaxAmt != null)
            return false;
        if (bCountryTier3TaxAmt != null ? !bCountryTier3TaxAmt.equals(that.bCountryTier3TaxAmt) : that.bCountryTier3TaxAmt != null)
            return false;
        if (bStateTier1TaxAmt != null ? !bStateTier1TaxAmt.equals(that.bStateTier1TaxAmt) : that.bStateTier1TaxAmt != null)
            return false;
        if (bStateTier2TaxAmt != null ? !bStateTier2TaxAmt.equals(that.bStateTier2TaxAmt) : that.bStateTier2TaxAmt != null)
            return false;
        if (bStateTier3TaxAmt != null ? !bStateTier3TaxAmt.equals(that.bStateTier3TaxAmt) : that.bStateTier3TaxAmt != null)
            return false;
        if (bCountyTier1TaxAmt != null ? !bCountyTier1TaxAmt.equals(that.bCountyTier1TaxAmt) : that.bCountyTier1TaxAmt != null)
            return false;
        if (bCountyTier2TaxAmt != null ? !bCountyTier2TaxAmt.equals(that.bCountyTier2TaxAmt) : that.bCountyTier2TaxAmt != null)
            return false;
        if (bCountyTier3TaxAmt != null ? !bCountyTier3TaxAmt.equals(that.bCountyTier3TaxAmt) : that.bCountyTier3TaxAmt != null)
            return false;
        if (bCityTier1TaxAmt != null ? !bCityTier1TaxAmt.equals(that.bCityTier1TaxAmt) : that.bCityTier1TaxAmt != null)
            return false;
        if (bCityTier2TaxAmt != null ? !bCityTier2TaxAmt.equals(that.bCityTier2TaxAmt) : that.bCityTier2TaxAmt != null)
            return false;
        if (bCityTier3TaxAmt != null ? !bCityTier3TaxAmt.equals(that.bCityTier3TaxAmt) : that.bCityTier3TaxAmt != null)
            return false;
        if (bStj1TaxAmt != null ? !bStj1TaxAmt.equals(that.bStj1TaxAmt) : that.bStj1TaxAmt != null) return false;
        if (bStj2TaxAmt != null ? !bStj2TaxAmt.equals(that.bStj2TaxAmt) : that.bStj2TaxAmt != null) return false;
        if (bStj3TaxAmt != null ? !bStj3TaxAmt.equals(that.bStj3TaxAmt) : that.bStj3TaxAmt != null) return false;
        if (bStj4TaxAmt != null ? !bStj4TaxAmt.equals(that.bStj4TaxAmt) : that.bStj4TaxAmt != null) return false;
        if (bStj5TaxAmt != null ? !bStj5TaxAmt.equals(that.bStj5TaxAmt) : that.bStj5TaxAmt != null) return false;
        if (bStj6TaxAmt != null ? !bStj6TaxAmt.equals(that.bStj6TaxAmt) : that.bStj6TaxAmt != null) return false;
        if (bStj7TaxAmt != null ? !bStj7TaxAmt.equals(that.bStj7TaxAmt) : that.bStj7TaxAmt != null) return false;
        if (bStj8TaxAmt != null ? !bStj8TaxAmt.equals(that.bStj8TaxAmt) : that.bStj8TaxAmt != null) return false;
        if (bStj9TaxAmt != null ? !bStj9TaxAmt.equals(that.bStj9TaxAmt) : that.bStj9TaxAmt != null) return false;
        if (bStj10TaxAmt != null ? !bStj10TaxAmt.equals(that.bStj10TaxAmt) : that.bStj10TaxAmt != null) return false;
        if (bCountyLocalTaxAmt != null ? !bCountyLocalTaxAmt.equals(that.bCountyLocalTaxAmt) : that.bCountyLocalTaxAmt != null)
            return false;
        if (bCityLocalTaxAmt != null ? !bCityLocalTaxAmt.equals(that.bCityLocalTaxAmt) : that.bCityLocalTaxAmt != null)
            return false;
        if (bTbCalcTaxAmt != null ? !bTbCalcTaxAmt.equals(that.bTbCalcTaxAmt) : that.bTbCalcTaxAmt != null)
            return false;
        if (aCountryTier1TaxAmt != null ? !aCountryTier1TaxAmt.equals(that.aCountryTier1TaxAmt) : that.aCountryTier1TaxAmt != null)
            return false;
        if (aCountryTier2TaxAmt != null ? !aCountryTier2TaxAmt.equals(that.aCountryTier2TaxAmt) : that.aCountryTier2TaxAmt != null)
            return false;
        if (aCountryTier3TaxAmt != null ? !aCountryTier3TaxAmt.equals(that.aCountryTier3TaxAmt) : that.aCountryTier3TaxAmt != null)
            return false;
        if (aStateTier1TaxAmt != null ? !aStateTier1TaxAmt.equals(that.aStateTier1TaxAmt) : that.aStateTier1TaxAmt != null)
            return false;
        if (aStateTier2TaxAmt != null ? !aStateTier2TaxAmt.equals(that.aStateTier2TaxAmt) : that.aStateTier2TaxAmt != null)
            return false;
        if (aStateTier3TaxAmt != null ? !aStateTier3TaxAmt.equals(that.aStateTier3TaxAmt) : that.aStateTier3TaxAmt != null)
            return false;
        if (aCountyTier1TaxAmt != null ? !aCountyTier1TaxAmt.equals(that.aCountyTier1TaxAmt) : that.aCountyTier1TaxAmt != null)
            return false;
        if (aCountyTier2TaxAmt != null ? !aCountyTier2TaxAmt.equals(that.aCountyTier2TaxAmt) : that.aCountyTier2TaxAmt != null)
            return false;
        if (aCountyTier3TaxAmt != null ? !aCountyTier3TaxAmt.equals(that.aCountyTier3TaxAmt) : that.aCountyTier3TaxAmt != null)
            return false;
        if (aCityTier1TaxAmt != null ? !aCityTier1TaxAmt.equals(that.aCityTier1TaxAmt) : that.aCityTier1TaxAmt != null)
            return false;
        if (aCityTier2TaxAmt != null ? !aCityTier2TaxAmt.equals(that.aCityTier2TaxAmt) : that.aCityTier2TaxAmt != null)
            return false;
        if (aCityTier3TaxAmt != null ? !aCityTier3TaxAmt.equals(that.aCityTier3TaxAmt) : that.aCityTier3TaxAmt != null)
            return false;
        if (aStj1TaxAmt != null ? !aStj1TaxAmt.equals(that.aStj1TaxAmt) : that.aStj1TaxAmt != null) return false;
        if (aStj2TaxAmt != null ? !aStj2TaxAmt.equals(that.aStj2TaxAmt) : that.aStj2TaxAmt != null) return false;
        if (aStj3TaxAmt != null ? !aStj3TaxAmt.equals(that.aStj3TaxAmt) : that.aStj3TaxAmt != null) return false;
        if (aStj4TaxAmt != null ? !aStj4TaxAmt.equals(that.aStj4TaxAmt) : that.aStj4TaxAmt != null) return false;
        if (aStj5TaxAmt != null ? !aStj5TaxAmt.equals(that.aStj5TaxAmt) : that.aStj5TaxAmt != null) return false;
        if (aStj6TaxAmt != null ? !aStj6TaxAmt.equals(that.aStj6TaxAmt) : that.aStj6TaxAmt != null) return false;
        if (aStj7TaxAmt != null ? !aStj7TaxAmt.equals(that.aStj7TaxAmt) : that.aStj7TaxAmt != null) return false;
        if (aStj8TaxAmt != null ? !aStj8TaxAmt.equals(that.aStj8TaxAmt) : that.aStj8TaxAmt != null) return false;
        if (aStj9TaxAmt != null ? !aStj9TaxAmt.equals(that.aStj9TaxAmt) : that.aStj9TaxAmt != null) return false;
        if (aStj10TaxAmt != null ? !aStj10TaxAmt.equals(that.aStj10TaxAmt) : that.aStj10TaxAmt != null) return false;
        if (aCountyLocalTaxAmt != null ? !aCountyLocalTaxAmt.equals(that.aCountyLocalTaxAmt) : that.aCountyLocalTaxAmt != null)
            return false;
        if (aCityLocalTaxAmt != null ? !aCityLocalTaxAmt.equals(that.aCityLocalTaxAmt) : that.aCityLocalTaxAmt != null)
            return false;
        if (aTbCalcTaxAmt != null ? !aTbCalcTaxAmt.equals(that.aTbCalcTaxAmt) : that.aTbCalcTaxAmt != null)
            return false;
        if (distr01Amt != null ? !distr01Amt.equals(that.distr01Amt) : that.distr01Amt != null) return false;
        if (distr02Amt != null ? !distr02Amt.equals(that.distr02Amt) : that.distr02Amt != null) return false;
        if (distr03Amt != null ? !distr03Amt.equals(that.distr03Amt) : that.distr03Amt != null) return false;
        if (distr04Amt != null ? !distr04Amt.equals(that.distr04Amt) : that.distr04Amt != null) return false;
        if (distr05Amt != null ? !distr05Amt.equals(that.distr05Amt) : that.distr05Amt != null) return false;
        if (distr06Amt != null ? !distr06Amt.equals(that.distr06Amt) : that.distr06Amt != null) return false;
        if (distr07Amt != null ? !distr07Amt.equals(that.distr07Amt) : that.distr07Amt != null) return false;
        if (distr08Amt != null ? !distr08Amt.equals(that.distr08Amt) : that.distr08Amt != null) return false;
        if (distr09Amt != null ? !distr09Amt.equals(that.distr09Amt) : that.distr09Amt != null) return false;
        if (distr10Amt != null ? !distr10Amt.equals(that.distr10Amt) : that.distr10Amt != null) return false;
        if (countrySitusMatrixId != null ? !countrySitusMatrixId.equals(that.countrySitusMatrixId) : that.countrySitusMatrixId != null)
            return false;
        if (stateSitusMatrixId != null ? !stateSitusMatrixId.equals(that.stateSitusMatrixId) : that.stateSitusMatrixId != null)
            return false;
        if (countySitusMatrixId != null ? !countySitusMatrixId.equals(that.countySitusMatrixId) : that.countySitusMatrixId != null)
            return false;
        if (citySitusMatrixId != null ? !citySitusMatrixId.equals(that.citySitusMatrixId) : that.citySitusMatrixId != null)
            return false;
        if (stj1SitusMatrixId != null ? !stj1SitusMatrixId.equals(that.stj1SitusMatrixId) : that.stj1SitusMatrixId != null)
            return false;
        if (stj2SitusMatrixId != null ? !stj2SitusMatrixId.equals(that.stj2SitusMatrixId) : that.stj2SitusMatrixId != null)
            return false;
        if (stj3SitusMatrixId != null ? !stj3SitusMatrixId.equals(that.stj3SitusMatrixId) : that.stj3SitusMatrixId != null)
            return false;
        if (stj4SitusMatrixId != null ? !stj4SitusMatrixId.equals(that.stj4SitusMatrixId) : that.stj4SitusMatrixId != null)
            return false;
        if (stj5SitusMatrixId != null ? !stj5SitusMatrixId.equals(that.stj5SitusMatrixId) : that.stj5SitusMatrixId != null)
            return false;
        if (shiptoLocnMatrixId != null ? !shiptoLocnMatrixId.equals(that.shiptoLocnMatrixId) : that.shiptoLocnMatrixId != null)
            return false;
        if (shipfromLocnMatrixId != null ? !shipfromLocnMatrixId.equals(that.shipfromLocnMatrixId) : that.shipfromLocnMatrixId != null)
            return false;
        if (ordracptLocnMatrixId != null ? !ordracptLocnMatrixId.equals(that.ordracptLocnMatrixId) : that.ordracptLocnMatrixId != null)
            return false;
        if (ordrorgnLocnMatrixId != null ? !ordrorgnLocnMatrixId.equals(that.ordrorgnLocnMatrixId) : that.ordrorgnLocnMatrixId != null)
            return false;
        if (firstuseLocnMatrixId != null ? !firstuseLocnMatrixId.equals(that.firstuseLocnMatrixId) : that.firstuseLocnMatrixId != null)
            return false;
        if (billtoLocnMatrixId != null ? !billtoLocnMatrixId.equals(that.billtoLocnMatrixId) : that.billtoLocnMatrixId != null)
            return false;
        if (ttlxfrLocnMatrixId != null ? !ttlxfrLocnMatrixId.equals(that.ttlxfrLocnMatrixId) : that.ttlxfrLocnMatrixId != null)
            return false;
        if (shiptoManualJurInd != null ? !shiptoManualJurInd.equals(that.shiptoManualJurInd) : that.shiptoManualJurInd != null)
            return false;

        if (countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) return false;
        if (stateCode != null ? !stateCode.equals(that.stateCode) : that.stateCode != null) return false;
        if (countyName != null ? !countyName.equals(that.countyName) : that.countyName != null) return false;
        if (cityName != null ? !cityName.equals(that.cityName) : that.cityName != null) return false;
        if (stj1Name != null ? !stj1Name.equals(that.stj1Name) : that.stj1Name != null) return false;
        if (stj2Name != null ? !stj2Name.equals(that.stj2Name) : that.stj2Name != null) return false;
        if (stj3Name != null ? !stj3Name.equals(that.stj3Name) : that.stj3Name != null) return false;
        if (stj4Name != null ? !stj4Name.equals(that.stj4Name) : that.stj4Name != null) return false;
        if (stj5Name != null ? !stj5Name.equals(that.stj5Name) : that.stj5Name != null) return false;
        if (stj6Name != null ? !stj6Name.equals(that.stj6Name) : that.stj6Name != null) return false;
        if (stj7Name != null ? !stj7Name.equals(that.stj7Name) : that.stj7Name != null) return false;
        if (stj8Name != null ? !stj8Name.equals(that.stj8Name) : that.stj8Name != null) return false;
        if (stj9Name != null ? !stj9Name.equals(that.stj9Name) : that.stj9Name != null) return false;
        if (stj10Name != null ? !stj10Name.equals(that.stj10Name) : that.stj10Name != null) return false;
        if (countryVendorNexusDtlId != null ? !countryVendorNexusDtlId.equals(that.countryVendorNexusDtlId) : that.countryVendorNexusDtlId != null)
            return false;
        if (stateVendorNexusDtlId != null ? !stateVendorNexusDtlId.equals(that.stateVendorNexusDtlId) : that.stateVendorNexusDtlId != null)
            return false;
        if (countyVendorNexusDtlId != null ? !countyVendorNexusDtlId.equals(that.countyVendorNexusDtlId) : that.countyVendorNexusDtlId != null)
            return false;
        if (cityVendorNexusDtlId != null ? !cityVendorNexusDtlId.equals(that.cityVendorNexusDtlId) : that.cityVendorNexusDtlId != null)
            return false;
        if (countryTaxcodeDetailId != null ? !countryTaxcodeDetailId.equals(that.countryTaxcodeDetailId) : that.countryTaxcodeDetailId != null)
            return false;
        if (stateTaxcodeDetailId != null ? !stateTaxcodeDetailId.equals(that.stateTaxcodeDetailId) : that.stateTaxcodeDetailId != null)
            return false;
        if (countyTaxcodeDetailId != null ? !countyTaxcodeDetailId.equals(that.countyTaxcodeDetailId) : that.countyTaxcodeDetailId != null)
            return false;
        if (cityTaxcodeDetailId != null ? !cityTaxcodeDetailId.equals(that.cityTaxcodeDetailId) : that.cityTaxcodeDetailId != null)
            return false;
        if (stj1TaxcodeDetailId != null ? !stj1TaxcodeDetailId.equals(that.stj1TaxcodeDetailId) : that.stj1TaxcodeDetailId != null)
            return false;
        if (stj2TaxcodeDetailId != null ? !stj2TaxcodeDetailId.equals(that.stj2TaxcodeDetailId) : that.stj2TaxcodeDetailId != null)
            return false;
        if (stj3TaxcodeDetailId != null ? !stj3TaxcodeDetailId.equals(that.stj3TaxcodeDetailId) : that.stj3TaxcodeDetailId != null)
            return false;
        if (stj4TaxcodeDetailId != null ? !stj4TaxcodeDetailId.equals(that.stj4TaxcodeDetailId) : that.stj4TaxcodeDetailId != null)
            return false;
        if (stj5TaxcodeDetailId != null ? !stj5TaxcodeDetailId.equals(that.stj5TaxcodeDetailId) : that.stj5TaxcodeDetailId != null)
            return false;
        if (stj6TaxcodeDetailId != null ? !stj6TaxcodeDetailId.equals(that.stj6TaxcodeDetailId) : that.stj6TaxcodeDetailId != null)
            return false;
        if (stj7TaxcodeDetailId != null ? !stj7TaxcodeDetailId.equals(that.stj7TaxcodeDetailId) : that.stj7TaxcodeDetailId != null)
            return false;
        if (stj8TaxcodeDetailId != null ? !stj8TaxcodeDetailId.equals(that.stj8TaxcodeDetailId) : that.stj8TaxcodeDetailId != null)
            return false;
        if (stj9TaxcodeDetailId != null ? !stj9TaxcodeDetailId.equals(that.stj9TaxcodeDetailId) : that.stj9TaxcodeDetailId != null)
            return false;
        if (stj10TaxcodeDetailId != null ? !stj10TaxcodeDetailId.equals(that.stj10TaxcodeDetailId) : that.stj10TaxcodeDetailId != null)
            return false;
        if (countryTaxtypeCode != null ? !countryTaxtypeCode.equals(that.countryTaxtypeCode) : that.countryTaxtypeCode != null)
            return false;
        if (stateTaxtypeCode != null ? !stateTaxtypeCode.equals(that.stateTaxtypeCode) : that.stateTaxtypeCode != null)
            return false;
        if (countyTaxtypeCode != null ? !countyTaxtypeCode.equals(that.countyTaxtypeCode) : that.countyTaxtypeCode != null)
            return false;
        if (cityTaxtypeCode != null ? !cityTaxtypeCode.equals(that.cityTaxtypeCode) : that.cityTaxtypeCode != null)
            return false;
        if (stj1TaxtypeCode != null ? !stj1TaxtypeCode.equals(that.stj1TaxtypeCode) : that.stj1TaxtypeCode != null)
            return false;
        if (stj2TaxtypeCode != null ? !stj2TaxtypeCode.equals(that.stj2TaxtypeCode) : that.stj2TaxtypeCode != null)
            return false;
        if (stj3TaxtypeCode != null ? !stj3TaxtypeCode.equals(that.stj3TaxtypeCode) : that.stj3TaxtypeCode != null)
            return false;
        if (stj4TaxtypeCode != null ? !stj4TaxtypeCode.equals(that.stj4TaxtypeCode) : that.stj4TaxtypeCode != null)
            return false;
        if (stj5TaxtypeCode != null ? !stj5TaxtypeCode.equals(that.stj5TaxtypeCode) : that.stj5TaxtypeCode != null)
            return false;
        if (stj6TaxtypeCode != null ? !stj6TaxtypeCode.equals(that.stj6TaxtypeCode) : that.stj6TaxtypeCode != null)
            return false;
        if (stj7TaxtypeCode != null ? !stj7TaxtypeCode.equals(that.stj7TaxtypeCode) : that.stj7TaxtypeCode != null)
            return false;
        if (stj8TaxtypeCode != null ? !stj8TaxtypeCode.equals(that.stj8TaxtypeCode) : that.stj8TaxtypeCode != null)
            return false;
        if (stj9TaxtypeCode != null ? !stj9TaxtypeCode.equals(that.stj9TaxtypeCode) : that.stj9TaxtypeCode != null)
            return false;
        if (stj10TaxtypeCode != null ? !stj10TaxtypeCode.equals(that.stj10TaxtypeCode) : that.stj10TaxtypeCode != null)
            return false;
        if (countryTaxrateId != null ? !countryTaxrateId.equals(that.countryTaxrateId) : that.countryTaxrateId != null)
            return false;
        if (stateTaxrateId != null ? !stateTaxrateId.equals(that.stateTaxrateId) : that.stateTaxrateId != null)
            return false;
        if (countyTaxrateId != null ? !countyTaxrateId.equals(that.countyTaxrateId) : that.countyTaxrateId != null)
            return false;
        if (cityTaxrateId != null ? !cityTaxrateId.equals(that.cityTaxrateId) : that.cityTaxrateId != null)
            return false;
        if (stj1TaxrateId != null ? !stj1TaxrateId.equals(that.stj1TaxrateId) : that.stj1TaxrateId != null)
            return false;
        if (stj2TaxrateId != null ? !stj2TaxrateId.equals(that.stj2TaxrateId) : that.stj2TaxrateId != null)
            return false;
        if (stj3TaxrateId != null ? !stj3TaxrateId.equals(that.stj3TaxrateId) : that.stj3TaxrateId != null)
            return false;
        if (stj4TaxrateId != null ? !stj4TaxrateId.equals(that.stj4TaxrateId) : that.stj4TaxrateId != null)
            return false;
        if (stj5TaxrateId != null ? !stj5TaxrateId.equals(that.stj5TaxrateId) : that.stj5TaxrateId != null)
            return false;
        if (stj6TaxrateId != null ? !stj6TaxrateId.equals(that.stj6TaxrateId) : that.stj6TaxrateId != null)
            return false;
        if (stj7TaxrateId != null ? !stj7TaxrateId.equals(that.stj7TaxrateId) : that.stj7TaxrateId != null)
            return false;
        if (stj8TaxrateId != null ? !stj8TaxrateId.equals(that.stj8TaxrateId) : that.stj8TaxrateId != null)
            return false;
        if (stj9TaxrateId != null ? !stj9TaxrateId.equals(that.stj9TaxrateId) : that.stj9TaxrateId != null)
            return false;
        if (stj10TaxrateId != null ? !stj10TaxrateId.equals(that.stj10TaxrateId) : that.stj10TaxrateId != null)
            return false;
        if (countryTaxcodeOverFlag != null ? !countryTaxcodeOverFlag.equals(that.countryTaxcodeOverFlag) : that.countryTaxcodeOverFlag != null)
            return false;
        if (stateTaxcodeOverFlag != null ? !stateTaxcodeOverFlag.equals(that.stateTaxcodeOverFlag) : that.stateTaxcodeOverFlag != null)
            return false;
        if (countyTaxcodeOverFlag != null ? !countyTaxcodeOverFlag.equals(that.countyTaxcodeOverFlag) : that.countyTaxcodeOverFlag != null)
            return false;
        if (cityTaxcodeOverFlag != null ? !cityTaxcodeOverFlag.equals(that.cityTaxcodeOverFlag) : that.cityTaxcodeOverFlag != null)
            return false;
        if (stj1TaxcodeOverFlag != null ? !stj1TaxcodeOverFlag.equals(that.stj1TaxcodeOverFlag) : that.stj1TaxcodeOverFlag != null)
            return false;
        if (stj2TaxcodeOverFlag != null ? !stj2TaxcodeOverFlag.equals(that.stj2TaxcodeOverFlag) : that.stj2TaxcodeOverFlag != null)
            return false;
        if (stj3TaxcodeOverFlag != null ? !stj3TaxcodeOverFlag.equals(that.stj3TaxcodeOverFlag) : that.stj3TaxcodeOverFlag != null)
            return false;
        if (stj4TaxcodeOverFlag != null ? !stj4TaxcodeOverFlag.equals(that.stj4TaxcodeOverFlag) : that.stj4TaxcodeOverFlag != null)
            return false;
        if (stj5TaxcodeOverFlag != null ? !stj5TaxcodeOverFlag.equals(that.stj5TaxcodeOverFlag) : that.stj5TaxcodeOverFlag != null)
            return false;
        if (stj6TaxcodeOverFlag != null ? !stj6TaxcodeOverFlag.equals(that.stj6TaxcodeOverFlag) : that.stj6TaxcodeOverFlag != null)
            return false;
        if (stj7TaxcodeOverFlag != null ? !stj7TaxcodeOverFlag.equals(that.stj7TaxcodeOverFlag) : that.stj7TaxcodeOverFlag != null)
            return false;
        if (stj8TaxcodeOverFlag != null ? !stj8TaxcodeOverFlag.equals(that.stj8TaxcodeOverFlag) : that.stj8TaxcodeOverFlag != null)
            return false;
        if (stj9TaxcodeOverFlag != null ? !stj9TaxcodeOverFlag.equals(that.stj9TaxcodeOverFlag) : that.stj9TaxcodeOverFlag != null)
            return false;
        if (stj10TaxcodeOverFlag != null ? !stj10TaxcodeOverFlag.equals(that.stj10TaxcodeOverFlag) : that.stj10TaxcodeOverFlag != null)
            return false;
        if (shiptoJurisdictionId != null ? !shiptoJurisdictionId.equals(that.shiptoJurisdictionId) : that.shiptoJurisdictionId != null)
            return false;
        if (shipfromJurisdictionId != null ? !shipfromJurisdictionId.equals(that.shipfromJurisdictionId) : that.shipfromJurisdictionId != null)
            return false;
        if (ordracptJurisdictionId != null ? !ordracptJurisdictionId.equals(that.ordracptJurisdictionId) : that.ordracptJurisdictionId != null)
            return false;
        if (ordrorgnJurisdictionId != null ? !ordrorgnJurisdictionId.equals(that.ordrorgnJurisdictionId) : that.ordrorgnJurisdictionId != null)
            return false;
        if (firstuseJurisdictionId != null ? !firstuseJurisdictionId.equals(that.firstuseJurisdictionId) : that.firstuseJurisdictionId != null)
            return false;
        if (billtoJurisdictionId != null ? !billtoJurisdictionId.equals(that.billtoJurisdictionId) : that.billtoJurisdictionId != null)
            return false;
        if (ttlxfrJurisdictionId != null ? !ttlxfrJurisdictionId.equals(that.ttlxfrJurisdictionId) : that.ttlxfrJurisdictionId != null)
            return false;
        if (countryTier1Rate != null ? !countryTier1Rate.equals(that.countryTier1Rate) : that.countryTier1Rate != null)
            return false;
        if (countryTier2Rate != null ? !countryTier2Rate.equals(that.countryTier2Rate) : that.countryTier2Rate != null)
            return false;
        if (countryTier3Rate != null ? !countryTier3Rate.equals(that.countryTier3Rate) : that.countryTier3Rate != null)
            return false;
        if (countryTier1Setamt != null ? !countryTier1Setamt.equals(that.countryTier1Setamt) : that.countryTier1Setamt != null)
            return false;
        if (countryTier2Setamt != null ? !countryTier2Setamt.equals(that.countryTier2Setamt) : that.countryTier2Setamt != null)
            return false;
        if (countryTier3Setamt != null ? !countryTier3Setamt.equals(that.countryTier3Setamt) : that.countryTier3Setamt != null)
            return false;
        if (countryTier1MaxAmt != null ? !countryTier1MaxAmt.equals(that.countryTier1MaxAmt) : that.countryTier1MaxAmt != null)
            return false;
        if (countryTier2MinAmt != null ? !countryTier2MinAmt.equals(that.countryTier2MinAmt) : that.countryTier2MinAmt != null)
            return false;
        if (countryTier2MaxAmt != null ? !countryTier2MaxAmt.equals(that.countryTier2MaxAmt) : that.countryTier2MaxAmt != null)
            return false;
        if (countryTier2EntamFlag != null ? !countryTier2EntamFlag.equals(that.countryTier2EntamFlag) : that.countryTier2EntamFlag != null)
            return false;
        if (countryTier3EntamFlag != null ? !countryTier3EntamFlag.equals(that.countryTier3EntamFlag) : that.countryTier3EntamFlag != null)
            return false;
        if (countryMaxtaxAmt != null ? !countryMaxtaxAmt.equals(that.countryMaxtaxAmt) : that.countryMaxtaxAmt != null)
            return false;
        if (countryTaxableThresholdAmt != null ? !countryTaxableThresholdAmt.equals(that.countryTaxableThresholdAmt) : that.countryTaxableThresholdAmt != null)
            return false;
        if (countryMinimumTaxableAmt != null ? !countryMinimumTaxableAmt.equals(that.countryMinimumTaxableAmt) : that.countryMinimumTaxableAmt != null)
            return false;
        if (countryMaximumTaxableAmt != null ? !countryMaximumTaxableAmt.equals(that.countryMaximumTaxableAmt) : that.countryMaximumTaxableAmt != null)
            return false;
        if (countryMaximumTaxAmt != null ? !countryMaximumTaxAmt.equals(that.countryMaximumTaxAmt) : that.countryMaximumTaxAmt != null)
            return false;
        if (countryBaseChangePct != null ? !countryBaseChangePct.equals(that.countryBaseChangePct) : that.countryBaseChangePct != null)
            return false;
        if (countrySpecialRate != null ? !countrySpecialRate.equals(that.countrySpecialRate) : that.countrySpecialRate != null)
            return false;
        if (countrySpecialSetamt != null ? !countrySpecialSetamt.equals(that.countrySpecialSetamt) : that.countrySpecialSetamt != null)
            return false;
        if (countryTaxProcessTypeCode != null ? !countryTaxProcessTypeCode.equals(that.countryTaxProcessTypeCode) : that.countryTaxProcessTypeCode != null)
            return false;
        if (stateTier1Rate != null ? !stateTier1Rate.equals(that.stateTier1Rate) : that.stateTier1Rate != null)
            return false;
        if (stateTier2Rate != null ? !stateTier2Rate.equals(that.stateTier2Rate) : that.stateTier2Rate != null)
            return false;
        if (stateTier3Rate != null ? !stateTier3Rate.equals(that.stateTier3Rate) : that.stateTier3Rate != null)
            return false;
        if (stateTier1Setamt != null ? !stateTier1Setamt.equals(that.stateTier1Setamt) : that.stateTier1Setamt != null)
            return false;
        if (stateTier2Setamt != null ? !stateTier2Setamt.equals(that.stateTier2Setamt) : that.stateTier2Setamt != null)
            return false;
        if (stateTier3Setamt != null ? !stateTier3Setamt.equals(that.stateTier3Setamt) : that.stateTier3Setamt != null)
            return false;
        if (stateTier1MaxAmt != null ? !stateTier1MaxAmt.equals(that.stateTier1MaxAmt) : that.stateTier1MaxAmt != null)
            return false;
        if (stateTier2MinAmt != null ? !stateTier2MinAmt.equals(that.stateTier2MinAmt) : that.stateTier2MinAmt != null)
            return false;
        if (stateTier2MaxAmt != null ? !stateTier2MaxAmt.equals(that.stateTier2MaxAmt) : that.stateTier2MaxAmt != null)
            return false;
        if (stateTier2EntamFlag != null ? !stateTier2EntamFlag.equals(that.stateTier2EntamFlag) : that.stateTier2EntamFlag != null)
            return false;
        if (stateTier3EntamFlag != null ? !stateTier3EntamFlag.equals(that.stateTier3EntamFlag) : that.stateTier3EntamFlag != null)
            return false;
        if (stateMaxtaxAmt != null ? !stateMaxtaxAmt.equals(that.stateMaxtaxAmt) : that.stateMaxtaxAmt != null)
            return false;
        if (stateTaxableThresholdAmt != null ? !stateTaxableThresholdAmt.equals(that.stateTaxableThresholdAmt) : that.stateTaxableThresholdAmt != null)
            return false;
        if (stateMinimumTaxableAmt != null ? !stateMinimumTaxableAmt.equals(that.stateMinimumTaxableAmt) : that.stateMinimumTaxableAmt != null)
            return false;
        if (stateMaximumTaxableAmt != null ? !stateMaximumTaxableAmt.equals(that.stateMaximumTaxableAmt) : that.stateMaximumTaxableAmt != null)
            return false;
        if (stateMaximumTaxAmt != null ? !stateMaximumTaxAmt.equals(that.stateMaximumTaxAmt) : that.stateMaximumTaxAmt != null)
            return false;
        if (stateBaseChangePct != null ? !stateBaseChangePct.equals(that.stateBaseChangePct) : that.stateBaseChangePct != null)
            return false;
        if (stateSpecialRate != null ? !stateSpecialRate.equals(that.stateSpecialRate) : that.stateSpecialRate != null)
            return false;
        if (stateSpecialSetamt != null ? !stateSpecialSetamt.equals(that.stateSpecialSetamt) : that.stateSpecialSetamt != null)
            return false;
        if (stateTaxProcessTypeCode != null ? !stateTaxProcessTypeCode.equals(that.stateTaxProcessTypeCode) : that.stateTaxProcessTypeCode != null)
            return false;
        if (countyTier1Rate != null ? !countyTier1Rate.equals(that.countyTier1Rate) : that.countyTier1Rate != null)
            return false;
        if (countyTier2Rate != null ? !countyTier2Rate.equals(that.countyTier2Rate) : that.countyTier2Rate != null)
            return false;
        if (countyTier3Rate != null ? !countyTier3Rate.equals(that.countyTier3Rate) : that.countyTier3Rate != null)
            return false;
        if (countyTier1Setamt != null ? !countyTier1Setamt.equals(that.countyTier1Setamt) : that.countyTier1Setamt != null)
            return false;
        if (countyTier2Setamt != null ? !countyTier2Setamt.equals(that.countyTier2Setamt) : that.countyTier2Setamt != null)
            return false;
        if (countyTier3Setamt != null ? !countyTier3Setamt.equals(that.countyTier3Setamt) : that.countyTier3Setamt != null)
            return false;
        if (countyTier1MaxAmt != null ? !countyTier1MaxAmt.equals(that.countyTier1MaxAmt) : that.countyTier1MaxAmt != null)
            return false;
        if (countyTier2MinAmt != null ? !countyTier2MinAmt.equals(that.countyTier2MinAmt) : that.countyTier2MinAmt != null)
            return false;
        if (countyTier2MaxAmt != null ? !countyTier2MaxAmt.equals(that.countyTier2MaxAmt) : that.countyTier2MaxAmt != null)
            return false;
        if (countyTier2EntamFlag != null ? !countyTier2EntamFlag.equals(that.countyTier2EntamFlag) : that.countyTier2EntamFlag != null)
            return false;
        if (countyTier3EntamFlag != null ? !countyTier3EntamFlag.equals(that.countyTier3EntamFlag) : that.countyTier3EntamFlag != null)
            return false;
        if (countyMaxtaxAmt != null ? !countyMaxtaxAmt.equals(that.countyMaxtaxAmt) : that.countyMaxtaxAmt != null)
            return false;
        if (countyTaxableThresholdAmt != null ? !countyTaxableThresholdAmt.equals(that.countyTaxableThresholdAmt) : that.countyTaxableThresholdAmt != null)
            return false;
        if (countyMinimumTaxableAmt != null ? !countyMinimumTaxableAmt.equals(that.countyMinimumTaxableAmt) : that.countyMinimumTaxableAmt != null)
            return false;
        if (countyMaximumTaxableAmt != null ? !countyMaximumTaxableAmt.equals(that.countyMaximumTaxableAmt) : that.countyMaximumTaxableAmt != null)
            return false;
        if (countyMaximumTaxAmt != null ? !countyMaximumTaxAmt.equals(that.countyMaximumTaxAmt) : that.countyMaximumTaxAmt != null)
            return false;
        if (countyBaseChangePct != null ? !countyBaseChangePct.equals(that.countyBaseChangePct) : that.countyBaseChangePct != null)
            return false;
        if (countySpecialRate != null ? !countySpecialRate.equals(that.countySpecialRate) : that.countySpecialRate != null)
            return false;
        if (countySpecialSetamt != null ? !countySpecialSetamt.equals(that.countySpecialSetamt) : that.countySpecialSetamt != null)
            return false;
        if (countyTaxProcessTypeCode != null ? !countyTaxProcessTypeCode.equals(that.countyTaxProcessTypeCode) : that.countyTaxProcessTypeCode != null)
            return false;
        if (cityTier1Rate != null ? !cityTier1Rate.equals(that.cityTier1Rate) : that.cityTier1Rate != null)
            return false;
        if (cityTier2Rate != null ? !cityTier2Rate.equals(that.cityTier2Rate) : that.cityTier2Rate != null)
            return false;
        if (cityTier3Rate != null ? !cityTier3Rate.equals(that.cityTier3Rate) : that.cityTier3Rate != null)
            return false;
        if (cityTier1Setamt != null ? !cityTier1Setamt.equals(that.cityTier1Setamt) : that.cityTier1Setamt != null)
            return false;
        if (cityTier2Setamt != null ? !cityTier2Setamt.equals(that.cityTier2Setamt) : that.cityTier2Setamt != null)
            return false;
        if (cityTier3Setamt != null ? !cityTier3Setamt.equals(that.cityTier3Setamt) : that.cityTier3Setamt != null)
            return false;
        if (cityTier1MaxAmt != null ? !cityTier1MaxAmt.equals(that.cityTier1MaxAmt) : that.cityTier1MaxAmt != null)
            return false;
        if (cityTier2MinAmt != null ? !cityTier2MinAmt.equals(that.cityTier2MinAmt) : that.cityTier2MinAmt != null)
            return false;
        if (cityTier2MaxAmt != null ? !cityTier2MaxAmt.equals(that.cityTier2MaxAmt) : that.cityTier2MaxAmt != null)
            return false;
        if (cityTier2EntamFlag != null ? !cityTier2EntamFlag.equals(that.cityTier2EntamFlag) : that.cityTier2EntamFlag != null)
            return false;
        if (cityTier3EntamFlag != null ? !cityTier3EntamFlag.equals(that.cityTier3EntamFlag) : that.cityTier3EntamFlag != null)
            return false;
        if (cityMaxtaxAmt != null ? !cityMaxtaxAmt.equals(that.cityMaxtaxAmt) : that.cityMaxtaxAmt != null)
            return false;
        if (cityTaxableThresholdAmt != null ? !cityTaxableThresholdAmt.equals(that.cityTaxableThresholdAmt) : that.cityTaxableThresholdAmt != null)
            return false;
        if (cityMinimumTaxableAmt != null ? !cityMinimumTaxableAmt.equals(that.cityMinimumTaxableAmt) : that.cityMinimumTaxableAmt != null)
            return false;
        if (cityMaximumTaxableAmt != null ? !cityMaximumTaxableAmt.equals(that.cityMaximumTaxableAmt) : that.cityMaximumTaxableAmt != null)
            return false;
        if (cityMaximumTaxAmt != null ? !cityMaximumTaxAmt.equals(that.cityMaximumTaxAmt) : that.cityMaximumTaxAmt != null)
            return false;
        if (cityBaseChangePct != null ? !cityBaseChangePct.equals(that.cityBaseChangePct) : that.cityBaseChangePct != null)
            return false;
        if (citySpecialRate != null ? !citySpecialRate.equals(that.citySpecialRate) : that.citySpecialRate != null)
            return false;
        if (citySpecialSetamt != null ? !citySpecialSetamt.equals(that.citySpecialSetamt) : that.citySpecialSetamt != null)
            return false;
        if (cityTaxProcessTypeCode != null ? !cityTaxProcessTypeCode.equals(that.cityTaxProcessTypeCode) : that.cityTaxProcessTypeCode != null)
            return false;
        if (stj1Rate != null ? !stj1Rate.equals(that.stj1Rate) : that.stj1Rate != null) return false;
        if (stj1Setamt != null ? !stj1Setamt.equals(that.stj1Setamt) : that.stj1Setamt != null) return false;
        if (stj2Rate != null ? !stj2Rate.equals(that.stj2Rate) : that.stj2Rate != null) return false;
        if (stj2Setamt != null ? !stj2Setamt.equals(that.stj2Setamt) : that.stj2Setamt != null) return false;
        if (stj3Rate != null ? !stj3Rate.equals(that.stj3Rate) : that.stj3Rate != null) return false;
        if (stj3Setamt != null ? !stj3Setamt.equals(that.stj3Setamt) : that.stj3Setamt != null) return false;
        if (stj4Rate != null ? !stj4Rate.equals(that.stj4Rate) : that.stj4Rate != null) return false;
        if (stj4Setamt != null ? !stj4Setamt.equals(that.stj4Setamt) : that.stj4Setamt != null) return false;
        if (stj5Rate != null ? !stj5Rate.equals(that.stj5Rate) : that.stj5Rate != null) return false;
        if (stj5Setamt != null ? !stj5Setamt.equals(that.stj5Setamt) : that.stj5Setamt != null) return false;
        if (stj6Rate != null ? !stj6Rate.equals(that.stj6Rate) : that.stj6Rate != null) return false;
        if (stj6Setamt != null ? !stj6Setamt.equals(that.stj6Setamt) : that.stj6Setamt != null) return false;
        if (stj7Rate != null ? !stj7Rate.equals(that.stj7Rate) : that.stj7Rate != null) return false;
        if (stj7Setamt != null ? !stj7Setamt.equals(that.stj7Setamt) : that.stj7Setamt != null) return false;
        if (stj8Rate != null ? !stj8Rate.equals(that.stj8Rate) : that.stj8Rate != null) return false;
        if (stj8Setamt != null ? !stj8Setamt.equals(that.stj8Setamt) : that.stj8Setamt != null) return false;
        if (stj9Rate != null ? !stj9Rate.equals(that.stj9Rate) : that.stj9Rate != null) return false;
        if (stj9Setamt != null ? !stj9Setamt.equals(that.stj9Setamt) : that.stj9Setamt != null) return false;
        if (stj10Rate != null ? !stj10Rate.equals(that.stj10Rate) : that.stj10Rate != null) return false;
        if (stj10Setamt != null ? !stj10Setamt.equals(that.stj10Setamt) : that.stj10Setamt != null) return false;
        if (combinedRate != null ? !combinedRate.equals(that.combinedRate) : that.combinedRate != null) return false;
        if (countryTaxtypeUsedCode != null ? !countryTaxtypeUsedCode.equals(that.countryTaxtypeUsedCode) : that.countryTaxtypeUsedCode != null)
            return false;
        if (stateTaxtypeUsedCode != null ? !stateTaxtypeUsedCode.equals(that.stateTaxtypeUsedCode) : that.stateTaxtypeUsedCode != null)
            return false;
        if (countyTaxtypeUsedCode != null ? !countyTaxtypeUsedCode.equals(that.countyTaxtypeUsedCode) : that.countyTaxtypeUsedCode != null)
            return false;
        if (cityTaxtypeUsedCode != null ? !cityTaxtypeUsedCode.equals(that.cityTaxtypeUsedCode) : that.cityTaxtypeUsedCode != null)
            return false;
        if (stj1TaxtypeUsedCode != null ? !stj1TaxtypeUsedCode.equals(that.stj1TaxtypeUsedCode) : that.stj1TaxtypeUsedCode != null)
            return false;
        if (stj2TaxtypeUsedCode != null ? !stj2TaxtypeUsedCode.equals(that.stj2TaxtypeUsedCode) : that.stj2TaxtypeUsedCode != null)
            return false;
        if (stj3TaxtypeUsedCode != null ? !stj3TaxtypeUsedCode.equals(that.stj3TaxtypeUsedCode) : that.stj3TaxtypeUsedCode != null)
            return false;
        if (stj4TaxtypeUsedCode != null ? !stj4TaxtypeUsedCode.equals(that.stj4TaxtypeUsedCode) : that.stj4TaxtypeUsedCode != null)
            return false;
        if (stj5TaxtypeUsedCode != null ? !stj5TaxtypeUsedCode.equals(that.stj5TaxtypeUsedCode) : that.stj5TaxtypeUsedCode != null)
            return false;
        if (stj6TaxtypeUsedCode != null ? !stj6TaxtypeUsedCode.equals(that.stj6TaxtypeUsedCode) : that.stj6TaxtypeUsedCode != null)
            return false;
        if (stj7TaxtypeUsedCode != null ? !stj7TaxtypeUsedCode.equals(that.stj7TaxtypeUsedCode) : that.stj7TaxtypeUsedCode != null)
            return false;
        if (stj8TaxtypeUsedCode != null ? !stj8TaxtypeUsedCode.equals(that.stj8TaxtypeUsedCode) : that.stj8TaxtypeUsedCode != null)
            return false;
        if (stj9TaxtypeUsedCode != null ? !stj9TaxtypeUsedCode.equals(that.stj9TaxtypeUsedCode) : that.stj9TaxtypeUsedCode != null)
            return false;
        if (stj10TaxtypeUsedCode != null ? !stj10TaxtypeUsedCode.equals(that.stj10TaxtypeUsedCode) : that.stj10TaxtypeUsedCode != null)
            return false;
        if (modifyUserId != null ? !modifyUserId.equals(that.modifyUserId) : that.modifyUserId != null) return false;
        return modifyTimestamp != null ? modifyTimestamp.equals(that.modifyTimestamp) : that.modifyTimestamp == null;

    }

    @Override
    public int hashCode() {
        int result = purchtransLogId != null ? purchtransLogId.hashCode() : 0;
        result = 31 * result + (purchtransId != null ? purchtransId.hashCode() : 0);
        result = 31 * result + (logSource != null ? logSource.hashCode() : 0);
        result = 31 * result + (glExtractBatchNo != null ? glExtractBatchNo.hashCode() : 0);
        result = 31 * result + (glExtractTimestamp != null ? glExtractTimestamp.hashCode() : 0);
        result = 31 * result + (directPayPermitFlag != null ? directPayPermitFlag.hashCode() : 0);
        result = 31 * result + (calculateInd != null ? calculateInd.hashCode() : 0);
        result = 31 * result + (glDate != null ? glDate.hashCode() : 0);
        result = 31 * result + (glLineItmDistAmt != null ? glLineItmDistAmt.hashCode() : 0);
        result = 31 * result + (allocationMatrixId != null ? allocationMatrixId.hashCode() : 0);
        result = 31 * result + (allocationSubtransId != null ? allocationSubtransId.hashCode() : 0);
        result = 31 * result + (taxAllocMatrixId != null ? taxAllocMatrixId.hashCode() : 0);
        result = 31 * result + (splitSubtransId != null ? splitSubtransId.hashCode() : 0);
        result = 31 * result + (multiTransCode != null ? multiTransCode.hashCode() : 0);
        result = 31 * result + (taxcodeCode != null ? taxcodeCode.hashCode() : 0);
        result = 31 * result + (manualTaxcodeInd != null ? manualTaxcodeInd.hashCode() : 0);
        result = 31 * result + (taxMatrixId != null ? taxMatrixId.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (processingNotes != null ? processingNotes.hashCode() : 0);
        result = 31 * result + (transactionStateCode != null ? transactionStateCode.hashCode() : 0);
        result = 31 * result + (autoTransactionStateCode != null ? autoTransactionStateCode.hashCode() : 0);
        result = 31 * result + (transactionCountryCode != null ? transactionCountryCode.hashCode() : 0);
        result = 31 * result + (autoTransactionCountryCode != null ? autoTransactionCountryCode.hashCode() : 0);
        result = 31 * result + (transactionInd != null ? transactionInd.hashCode() : 0);
        result = 31 * result + (suspendInd != null ? suspendInd.hashCode() : 0);
        result = 31 * result + (rejectFlag != null ? rejectFlag.hashCode() : 0);
        result = 31 * result + (countryTier1TaxableAmt != null ? countryTier1TaxableAmt.hashCode() : 0);
        result = 31 * result + (countryTier2TaxableAmt != null ? countryTier2TaxableAmt.hashCode() : 0);
        result = 31 * result + (countryTier3TaxableAmt != null ? countryTier3TaxableAmt.hashCode() : 0);
        result = 31 * result + (stateTier1TaxableAmt != null ? stateTier1TaxableAmt.hashCode() : 0);
        result = 31 * result + (stateTier2TaxableAmt != null ? stateTier2TaxableAmt.hashCode() : 0);
        result = 31 * result + (stateTier3TaxableAmt != null ? stateTier3TaxableAmt.hashCode() : 0);
        result = 31 * result + (countyTier1TaxableAmt != null ? countyTier1TaxableAmt.hashCode() : 0);
        result = 31 * result + (countyTier2TaxableAmt != null ? countyTier2TaxableAmt.hashCode() : 0);
        result = 31 * result + (countyTier3TaxableAmt != null ? countyTier3TaxableAmt.hashCode() : 0);
        result = 31 * result + (cityTier1TaxableAmt != null ? cityTier1TaxableAmt.hashCode() : 0);
        result = 31 * result + (cityTier2TaxableAmt != null ? cityTier2TaxableAmt.hashCode() : 0);
        result = 31 * result + (cityTier3TaxableAmt != null ? cityTier3TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj1TaxableAmt != null ? stj1TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj2TaxableAmt != null ? stj2TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj3TaxableAmt != null ? stj3TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj4TaxableAmt != null ? stj4TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj5TaxableAmt != null ? stj5TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj6TaxableAmt != null ? stj6TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj7TaxableAmt != null ? stj7TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj8TaxableAmt != null ? stj8TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj9TaxableAmt != null ? stj9TaxableAmt.hashCode() : 0);
        result = 31 * result + (stj10TaxableAmt != null ? stj10TaxableAmt.hashCode() : 0);
        result = 31 * result + (bCountryTier1TaxAmt != null ? bCountryTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountryTier2TaxAmt != null ? bCountryTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountryTier3TaxAmt != null ? bCountryTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (bStateTier1TaxAmt != null ? bStateTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (bStateTier2TaxAmt != null ? bStateTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (bStateTier3TaxAmt != null ? bStateTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountyTier1TaxAmt != null ? bCountyTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountyTier2TaxAmt != null ? bCountyTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountyTier3TaxAmt != null ? bCountyTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (bCityTier1TaxAmt != null ? bCityTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (bCityTier2TaxAmt != null ? bCityTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (bCityTier3TaxAmt != null ? bCityTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj1TaxAmt != null ? bStj1TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj2TaxAmt != null ? bStj2TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj3TaxAmt != null ? bStj3TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj4TaxAmt != null ? bStj4TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj5TaxAmt != null ? bStj5TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj6TaxAmt != null ? bStj6TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj7TaxAmt != null ? bStj7TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj8TaxAmt != null ? bStj8TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj9TaxAmt != null ? bStj9TaxAmt.hashCode() : 0);
        result = 31 * result + (bStj10TaxAmt != null ? bStj10TaxAmt.hashCode() : 0);
        result = 31 * result + (bCountyLocalTaxAmt != null ? bCountyLocalTaxAmt.hashCode() : 0);
        result = 31 * result + (bCityLocalTaxAmt != null ? bCityLocalTaxAmt.hashCode() : 0);
        result = 31 * result + (bTbCalcTaxAmt != null ? bTbCalcTaxAmt.hashCode() : 0);
        result = 31 * result + (aCountryTier1TaxAmt != null ? aCountryTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountryTier2TaxAmt != null ? aCountryTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountryTier3TaxAmt != null ? aCountryTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (aStateTier1TaxAmt != null ? aStateTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (aStateTier2TaxAmt != null ? aStateTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (aStateTier3TaxAmt != null ? aStateTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountyTier1TaxAmt != null ? aCountyTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountyTier2TaxAmt != null ? aCountyTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountyTier3TaxAmt != null ? aCountyTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (aCityTier1TaxAmt != null ? aCityTier1TaxAmt.hashCode() : 0);
        result = 31 * result + (aCityTier2TaxAmt != null ? aCityTier2TaxAmt.hashCode() : 0);
        result = 31 * result + (aCityTier3TaxAmt != null ? aCityTier3TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj1TaxAmt != null ? aStj1TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj2TaxAmt != null ? aStj2TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj3TaxAmt != null ? aStj3TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj4TaxAmt != null ? aStj4TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj5TaxAmt != null ? aStj5TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj6TaxAmt != null ? aStj6TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj7TaxAmt != null ? aStj7TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj8TaxAmt != null ? aStj8TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj9TaxAmt != null ? aStj9TaxAmt.hashCode() : 0);
        result = 31 * result + (aStj10TaxAmt != null ? aStj10TaxAmt.hashCode() : 0);
        result = 31 * result + (aCountyLocalTaxAmt != null ? aCountyLocalTaxAmt.hashCode() : 0);
        result = 31 * result + (aCityLocalTaxAmt != null ? aCityLocalTaxAmt.hashCode() : 0);
        result = 31 * result + (aTbCalcTaxAmt != null ? aTbCalcTaxAmt.hashCode() : 0);
        result = 31 * result + (distr01Amt != null ? distr01Amt.hashCode() : 0);
        result = 31 * result + (distr02Amt != null ? distr02Amt.hashCode() : 0);
        result = 31 * result + (distr03Amt != null ? distr03Amt.hashCode() : 0);
        result = 31 * result + (distr04Amt != null ? distr04Amt.hashCode() : 0);
        result = 31 * result + (distr05Amt != null ? distr05Amt.hashCode() : 0);
        result = 31 * result + (distr06Amt != null ? distr06Amt.hashCode() : 0);
        result = 31 * result + (distr07Amt != null ? distr07Amt.hashCode() : 0);
        result = 31 * result + (distr08Amt != null ? distr08Amt.hashCode() : 0);
        result = 31 * result + (distr09Amt != null ? distr09Amt.hashCode() : 0);
        result = 31 * result + (distr10Amt != null ? distr10Amt.hashCode() : 0);
        result = 31 * result + (countrySitusMatrixId != null ? countrySitusMatrixId.hashCode() : 0);
        result = 31 * result + (stateSitusMatrixId != null ? stateSitusMatrixId.hashCode() : 0);
        result = 31 * result + (countySitusMatrixId != null ? countySitusMatrixId.hashCode() : 0);
        result = 31 * result + (citySitusMatrixId != null ? citySitusMatrixId.hashCode() : 0);
        result = 31 * result + (stj1SitusMatrixId != null ? stj1SitusMatrixId.hashCode() : 0);
        result = 31 * result + (stj2SitusMatrixId != null ? stj2SitusMatrixId.hashCode() : 0);
        result = 31 * result + (stj3SitusMatrixId != null ? stj3SitusMatrixId.hashCode() : 0);
        result = 31 * result + (stj4SitusMatrixId != null ? stj4SitusMatrixId.hashCode() : 0);
        result = 31 * result + (stj5SitusMatrixId != null ? stj5SitusMatrixId.hashCode() : 0);
        result = 31 * result + (shiptoLocnMatrixId != null ? shiptoLocnMatrixId.hashCode() : 0);
        result = 31 * result + (shipfromLocnMatrixId != null ? shipfromLocnMatrixId.hashCode() : 0);
        result = 31 * result + (ordracptLocnMatrixId != null ? ordracptLocnMatrixId.hashCode() : 0);
        result = 31 * result + (ordrorgnLocnMatrixId != null ? ordrorgnLocnMatrixId.hashCode() : 0);
        result = 31 * result + (firstuseLocnMatrixId != null ? firstuseLocnMatrixId.hashCode() : 0);
        result = 31 * result + (billtoLocnMatrixId != null ? billtoLocnMatrixId.hashCode() : 0);
        result = 31 * result + (ttlxfrLocnMatrixId != null ? ttlxfrLocnMatrixId.hashCode() : 0);
        result = 31 * result + (shiptoManualJurInd != null ? shiptoManualJurInd.hashCode() : 0);
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        result = 31 * result + (stateCode != null ? stateCode.hashCode() : 0);
        result = 31 * result + (countyName != null ? countyName.hashCode() : 0);
        result = 31 * result + (cityName != null ? cityName.hashCode() : 0);
        result = 31 * result + (stj1Name != null ? stj1Name.hashCode() : 0);
        result = 31 * result + (stj2Name != null ? stj2Name.hashCode() : 0);
        result = 31 * result + (stj3Name != null ? stj3Name.hashCode() : 0);
        result = 31 * result + (stj4Name != null ? stj4Name.hashCode() : 0);
        result = 31 * result + (stj5Name != null ? stj5Name.hashCode() : 0);
        result = 31 * result + (stj6Name != null ? stj6Name.hashCode() : 0);
        result = 31 * result + (stj7Name != null ? stj7Name.hashCode() : 0);
        result = 31 * result + (stj8Name != null ? stj8Name.hashCode() : 0);
        result = 31 * result + (stj9Name != null ? stj9Name.hashCode() : 0);
        result = 31 * result + (stj10Name != null ? stj10Name.hashCode() : 0);
        result = 31 * result + (countryVendorNexusDtlId != null ? countryVendorNexusDtlId.hashCode() : 0);
        result = 31 * result + (stateVendorNexusDtlId != null ? stateVendorNexusDtlId.hashCode() : 0);
        result = 31 * result + (countyVendorNexusDtlId != null ? countyVendorNexusDtlId.hashCode() : 0);
        result = 31 * result + (cityVendorNexusDtlId != null ? cityVendorNexusDtlId.hashCode() : 0);
        result = 31 * result + (countryTaxcodeDetailId != null ? countryTaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stateTaxcodeDetailId != null ? stateTaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (countyTaxcodeDetailId != null ? countyTaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (cityTaxcodeDetailId != null ? cityTaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj1TaxcodeDetailId != null ? stj1TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj2TaxcodeDetailId != null ? stj2TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj3TaxcodeDetailId != null ? stj3TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj4TaxcodeDetailId != null ? stj4TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj5TaxcodeDetailId != null ? stj5TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj6TaxcodeDetailId != null ? stj6TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj7TaxcodeDetailId != null ? stj7TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj8TaxcodeDetailId != null ? stj8TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj9TaxcodeDetailId != null ? stj9TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (stj10TaxcodeDetailId != null ? stj10TaxcodeDetailId.hashCode() : 0);
        result = 31 * result + (countryTaxtypeCode != null ? countryTaxtypeCode.hashCode() : 0);
        result = 31 * result + (stateTaxtypeCode != null ? stateTaxtypeCode.hashCode() : 0);
        result = 31 * result + (countyTaxtypeCode != null ? countyTaxtypeCode.hashCode() : 0);
        result = 31 * result + (cityTaxtypeCode != null ? cityTaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj1TaxtypeCode != null ? stj1TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj2TaxtypeCode != null ? stj2TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj3TaxtypeCode != null ? stj3TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj4TaxtypeCode != null ? stj4TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj5TaxtypeCode != null ? stj5TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj6TaxtypeCode != null ? stj6TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj7TaxtypeCode != null ? stj7TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj8TaxtypeCode != null ? stj8TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj9TaxtypeCode != null ? stj9TaxtypeCode.hashCode() : 0);
        result = 31 * result + (stj10TaxtypeCode != null ? stj10TaxtypeCode.hashCode() : 0);
        result = 31 * result + (countryTaxrateId != null ? countryTaxrateId.hashCode() : 0);
        result = 31 * result + (stateTaxrateId != null ? stateTaxrateId.hashCode() : 0);
        result = 31 * result + (countyTaxrateId != null ? countyTaxrateId.hashCode() : 0);
        result = 31 * result + (cityTaxrateId != null ? cityTaxrateId.hashCode() : 0);
        result = 31 * result + (stj1TaxrateId != null ? stj1TaxrateId.hashCode() : 0);
        result = 31 * result + (stj2TaxrateId != null ? stj2TaxrateId.hashCode() : 0);
        result = 31 * result + (stj3TaxrateId != null ? stj3TaxrateId.hashCode() : 0);
        result = 31 * result + (stj4TaxrateId != null ? stj4TaxrateId.hashCode() : 0);
        result = 31 * result + (stj5TaxrateId != null ? stj5TaxrateId.hashCode() : 0);
        result = 31 * result + (stj6TaxrateId != null ? stj6TaxrateId.hashCode() : 0);
        result = 31 * result + (stj7TaxrateId != null ? stj7TaxrateId.hashCode() : 0);
        result = 31 * result + (stj8TaxrateId != null ? stj8TaxrateId.hashCode() : 0);
        result = 31 * result + (stj9TaxrateId != null ? stj9TaxrateId.hashCode() : 0);
        result = 31 * result + (stj10TaxrateId != null ? stj10TaxrateId.hashCode() : 0);
        result = 31 * result + (countryTaxcodeOverFlag != null ? countryTaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stateTaxcodeOverFlag != null ? stateTaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (countyTaxcodeOverFlag != null ? countyTaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (cityTaxcodeOverFlag != null ? cityTaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj1TaxcodeOverFlag != null ? stj1TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj2TaxcodeOverFlag != null ? stj2TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj3TaxcodeOverFlag != null ? stj3TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj4TaxcodeOverFlag != null ? stj4TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj5TaxcodeOverFlag != null ? stj5TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj6TaxcodeOverFlag != null ? stj6TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj7TaxcodeOverFlag != null ? stj7TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj8TaxcodeOverFlag != null ? stj8TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj9TaxcodeOverFlag != null ? stj9TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (stj10TaxcodeOverFlag != null ? stj10TaxcodeOverFlag.hashCode() : 0);
        result = 31 * result + (shiptoJurisdictionId != null ? shiptoJurisdictionId.hashCode() : 0);
        result = 31 * result + (shipfromJurisdictionId != null ? shipfromJurisdictionId.hashCode() : 0);
        result = 31 * result + (ordracptJurisdictionId != null ? ordracptJurisdictionId.hashCode() : 0);
        result = 31 * result + (ordrorgnJurisdictionId != null ? ordrorgnJurisdictionId.hashCode() : 0);
        result = 31 * result + (firstuseJurisdictionId != null ? firstuseJurisdictionId.hashCode() : 0);
        result = 31 * result + (billtoJurisdictionId != null ? billtoJurisdictionId.hashCode() : 0);
        result = 31 * result + (ttlxfrJurisdictionId != null ? ttlxfrJurisdictionId.hashCode() : 0);
        result = 31 * result + (countryTier1Rate != null ? countryTier1Rate.hashCode() : 0);
        result = 31 * result + (countryTier2Rate != null ? countryTier2Rate.hashCode() : 0);
        result = 31 * result + (countryTier3Rate != null ? countryTier3Rate.hashCode() : 0);
        result = 31 * result + (countryTier1Setamt != null ? countryTier1Setamt.hashCode() : 0);
        result = 31 * result + (countryTier2Setamt != null ? countryTier2Setamt.hashCode() : 0);
        result = 31 * result + (countryTier3Setamt != null ? countryTier3Setamt.hashCode() : 0);
        result = 31 * result + (countryTier1MaxAmt != null ? countryTier1MaxAmt.hashCode() : 0);
        result = 31 * result + (countryTier2MinAmt != null ? countryTier2MinAmt.hashCode() : 0);
        result = 31 * result + (countryTier2MaxAmt != null ? countryTier2MaxAmt.hashCode() : 0);
        result = 31 * result + (countryTier2EntamFlag != null ? countryTier2EntamFlag.hashCode() : 0);
        result = 31 * result + (countryTier3EntamFlag != null ? countryTier3EntamFlag.hashCode() : 0);
        result = 31 * result + (countryMaxtaxAmt != null ? countryMaxtaxAmt.hashCode() : 0);
        result = 31 * result + (countryTaxableThresholdAmt != null ? countryTaxableThresholdAmt.hashCode() : 0);
        result = 31 * result + (countryMinimumTaxableAmt != null ? countryMinimumTaxableAmt.hashCode() : 0);
        result = 31 * result + (countryMaximumTaxableAmt != null ? countryMaximumTaxableAmt.hashCode() : 0);
        result = 31 * result + (countryMaximumTaxAmt != null ? countryMaximumTaxAmt.hashCode() : 0);
        result = 31 * result + (countryBaseChangePct != null ? countryBaseChangePct.hashCode() : 0);
        result = 31 * result + (countrySpecialRate != null ? countrySpecialRate.hashCode() : 0);
        result = 31 * result + (countrySpecialSetamt != null ? countrySpecialSetamt.hashCode() : 0);
        result = 31 * result + (countryTaxProcessTypeCode != null ? countryTaxProcessTypeCode.hashCode() : 0);
        result = 31 * result + (stateTier1Rate != null ? stateTier1Rate.hashCode() : 0);
        result = 31 * result + (stateTier2Rate != null ? stateTier2Rate.hashCode() : 0);
        result = 31 * result + (stateTier3Rate != null ? stateTier3Rate.hashCode() : 0);
        result = 31 * result + (stateTier1Setamt != null ? stateTier1Setamt.hashCode() : 0);
        result = 31 * result + (stateTier2Setamt != null ? stateTier2Setamt.hashCode() : 0);
        result = 31 * result + (stateTier3Setamt != null ? stateTier3Setamt.hashCode() : 0);
        result = 31 * result + (stateTier1MaxAmt != null ? stateTier1MaxAmt.hashCode() : 0);
        result = 31 * result + (stateTier2MinAmt != null ? stateTier2MinAmt.hashCode() : 0);
        result = 31 * result + (stateTier2MaxAmt != null ? stateTier2MaxAmt.hashCode() : 0);
        result = 31 * result + (stateTier2EntamFlag != null ? stateTier2EntamFlag.hashCode() : 0);
        result = 31 * result + (stateTier3EntamFlag != null ? stateTier3EntamFlag.hashCode() : 0);
        result = 31 * result + (stateMaxtaxAmt != null ? stateMaxtaxAmt.hashCode() : 0);
        result = 31 * result + (stateTaxableThresholdAmt != null ? stateTaxableThresholdAmt.hashCode() : 0);
        result = 31 * result + (stateMinimumTaxableAmt != null ? stateMinimumTaxableAmt.hashCode() : 0);
        result = 31 * result + (stateMaximumTaxableAmt != null ? stateMaximumTaxableAmt.hashCode() : 0);
        result = 31 * result + (stateMaximumTaxAmt != null ? stateMaximumTaxAmt.hashCode() : 0);
        result = 31 * result + (stateBaseChangePct != null ? stateBaseChangePct.hashCode() : 0);
        result = 31 * result + (stateSpecialRate != null ? stateSpecialRate.hashCode() : 0);
        result = 31 * result + (stateSpecialSetamt != null ? stateSpecialSetamt.hashCode() : 0);
        result = 31 * result + (stateTaxProcessTypeCode != null ? stateTaxProcessTypeCode.hashCode() : 0);
        result = 31 * result + (countyTier1Rate != null ? countyTier1Rate.hashCode() : 0);
        result = 31 * result + (countyTier2Rate != null ? countyTier2Rate.hashCode() : 0);
        result = 31 * result + (countyTier3Rate != null ? countyTier3Rate.hashCode() : 0);
        result = 31 * result + (countyTier1Setamt != null ? countyTier1Setamt.hashCode() : 0);
        result = 31 * result + (countyTier2Setamt != null ? countyTier2Setamt.hashCode() : 0);
        result = 31 * result + (countyTier3Setamt != null ? countyTier3Setamt.hashCode() : 0);
        result = 31 * result + (countyTier1MaxAmt != null ? countyTier1MaxAmt.hashCode() : 0);
        result = 31 * result + (countyTier2MinAmt != null ? countyTier2MinAmt.hashCode() : 0);
        result = 31 * result + (countyTier2MaxAmt != null ? countyTier2MaxAmt.hashCode() : 0);
        result = 31 * result + (countyTier2EntamFlag != null ? countyTier2EntamFlag.hashCode() : 0);
        result = 31 * result + (countyTier3EntamFlag != null ? countyTier3EntamFlag.hashCode() : 0);
        result = 31 * result + (countyMaxtaxAmt != null ? countyMaxtaxAmt.hashCode() : 0);
        result = 31 * result + (countyTaxableThresholdAmt != null ? countyTaxableThresholdAmt.hashCode() : 0);
        result = 31 * result + (countyMinimumTaxableAmt != null ? countyMinimumTaxableAmt.hashCode() : 0);
        result = 31 * result + (countyMaximumTaxableAmt != null ? countyMaximumTaxableAmt.hashCode() : 0);
        result = 31 * result + (countyMaximumTaxAmt != null ? countyMaximumTaxAmt.hashCode() : 0);
        result = 31 * result + (countyBaseChangePct != null ? countyBaseChangePct.hashCode() : 0);
        result = 31 * result + (countySpecialRate != null ? countySpecialRate.hashCode() : 0);
        result = 31 * result + (countySpecialSetamt != null ? countySpecialSetamt.hashCode() : 0);
        result = 31 * result + (countyTaxProcessTypeCode != null ? countyTaxProcessTypeCode.hashCode() : 0);
        result = 31 * result + (cityTier1Rate != null ? cityTier1Rate.hashCode() : 0);
        result = 31 * result + (cityTier2Rate != null ? cityTier2Rate.hashCode() : 0);
        result = 31 * result + (cityTier3Rate != null ? cityTier3Rate.hashCode() : 0);
        result = 31 * result + (cityTier1Setamt != null ? cityTier1Setamt.hashCode() : 0);
        result = 31 * result + (cityTier2Setamt != null ? cityTier2Setamt.hashCode() : 0);
        result = 31 * result + (cityTier3Setamt != null ? cityTier3Setamt.hashCode() : 0);
        result = 31 * result + (cityTier1MaxAmt != null ? cityTier1MaxAmt.hashCode() : 0);
        result = 31 * result + (cityTier2MinAmt != null ? cityTier2MinAmt.hashCode() : 0);
        result = 31 * result + (cityTier2MaxAmt != null ? cityTier2MaxAmt.hashCode() : 0);
        result = 31 * result + (cityTier2EntamFlag != null ? cityTier2EntamFlag.hashCode() : 0);
        result = 31 * result + (cityTier3EntamFlag != null ? cityTier3EntamFlag.hashCode() : 0);
        result = 31 * result + (cityMaxtaxAmt != null ? cityMaxtaxAmt.hashCode() : 0);
        result = 31 * result + (cityTaxableThresholdAmt != null ? cityTaxableThresholdAmt.hashCode() : 0);
        result = 31 * result + (cityMinimumTaxableAmt != null ? cityMinimumTaxableAmt.hashCode() : 0);
        result = 31 * result + (cityMaximumTaxableAmt != null ? cityMaximumTaxableAmt.hashCode() : 0);
        result = 31 * result + (cityMaximumTaxAmt != null ? cityMaximumTaxAmt.hashCode() : 0);
        result = 31 * result + (cityBaseChangePct != null ? cityBaseChangePct.hashCode() : 0);
        result = 31 * result + (citySpecialRate != null ? citySpecialRate.hashCode() : 0);
        result = 31 * result + (citySpecialSetamt != null ? citySpecialSetamt.hashCode() : 0);
        result = 31 * result + (cityTaxProcessTypeCode != null ? cityTaxProcessTypeCode.hashCode() : 0);
        result = 31 * result + (stj1Rate != null ? stj1Rate.hashCode() : 0);
        result = 31 * result + (stj1Setamt != null ? stj1Setamt.hashCode() : 0);
        result = 31 * result + (stj2Rate != null ? stj2Rate.hashCode() : 0);
        result = 31 * result + (stj2Setamt != null ? stj2Setamt.hashCode() : 0);
        result = 31 * result + (stj3Rate != null ? stj3Rate.hashCode() : 0);
        result = 31 * result + (stj3Setamt != null ? stj3Setamt.hashCode() : 0);
        result = 31 * result + (stj4Rate != null ? stj4Rate.hashCode() : 0);
        result = 31 * result + (stj4Setamt != null ? stj4Setamt.hashCode() : 0);
        result = 31 * result + (stj5Rate != null ? stj5Rate.hashCode() : 0);
        result = 31 * result + (stj5Setamt != null ? stj5Setamt.hashCode() : 0);
        result = 31 * result + (stj6Rate != null ? stj6Rate.hashCode() : 0);
        result = 31 * result + (stj6Setamt != null ? stj6Setamt.hashCode() : 0);
        result = 31 * result + (stj7Rate != null ? stj7Rate.hashCode() : 0);
        result = 31 * result + (stj7Setamt != null ? stj7Setamt.hashCode() : 0);
        result = 31 * result + (stj8Rate != null ? stj8Rate.hashCode() : 0);
        result = 31 * result + (stj8Setamt != null ? stj8Setamt.hashCode() : 0);
        result = 31 * result + (stj9Rate != null ? stj9Rate.hashCode() : 0);
        result = 31 * result + (stj9Setamt != null ? stj9Setamt.hashCode() : 0);
        result = 31 * result + (stj10Rate != null ? stj10Rate.hashCode() : 0);
        result = 31 * result + (stj10Setamt != null ? stj10Setamt.hashCode() : 0);
        result = 31 * result + (combinedRate != null ? combinedRate.hashCode() : 0);
        result = 31 * result + (countryTaxtypeUsedCode != null ? countryTaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stateTaxtypeUsedCode != null ? stateTaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (countyTaxtypeUsedCode != null ? countyTaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (cityTaxtypeUsedCode != null ? cityTaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj1TaxtypeUsedCode != null ? stj1TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj2TaxtypeUsedCode != null ? stj2TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj3TaxtypeUsedCode != null ? stj3TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj4TaxtypeUsedCode != null ? stj4TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj5TaxtypeUsedCode != null ? stj5TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj6TaxtypeUsedCode != null ? stj6TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj7TaxtypeUsedCode != null ? stj7TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj8TaxtypeUsedCode != null ? stj8TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj9TaxtypeUsedCode != null ? stj9TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (stj10TaxtypeUsedCode != null ? stj10TaxtypeUsedCode.hashCode() : 0);
        result = 31 * result + (modifyUserId != null ? modifyUserId.hashCode() : 0);
        result = 31 * result + (modifyTimestamp != null ? modifyTimestamp.hashCode() : 0);
        return result;
    }
    
    public Boolean getCountryTaxcodeOverBooleanFlag() {
  		return "1".equals(countryTaxcodeOverFlag);
  	}
  	  
  	public Boolean getStateTaxcodeOverBooleanFlag() {
  		return "1".equals(stateTaxcodeOverFlag);
  	}
  	    
  	public Boolean getCountyTaxcodeOverBooleanFlag() {
  		return "1".equals(countyTaxcodeOverFlag);
  	}
  	    
  	public Boolean getCityTaxcodeOverBooleanFlag() {
  		return "1".equals(cityTaxcodeOverFlag);
  	}
  	
  	public Boolean getStj1TaxcodeOverBooleanFlag() {
  		return "1".equals(stj1TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj2TaxcodeOverBooleanFlag() {
  		return "1".equals(stj2TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj3TaxcodeOverBooleanFlag() {
  		return "1".equals(stj3TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj4TaxcodeOverBooleanFlag() {
  		return "1".equals(stj4TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj5TaxcodeOverBooleanFlag() {
  		return "1".equals(stj5TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj6TaxcodeOverBooleanFlag() {
  		return "1".equals(stj6TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj7TaxcodeOverBooleanFlag() {
  		return "1".equals(stj7TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj8TaxcodeOverBooleanFlag() {
  		return "1".equals(stj8TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj9TaxcodeOverBooleanFlag() {
  		return "1".equals(stj9TaxcodeOverFlag);
  	}
  	
  	public Boolean getStj10TaxcodeOverBooleanFlag() {
  		return "1".equals(stj10TaxcodeOverFlag);
  	}
  	
  	public Boolean getShiptoManualJurIndBooleanFlag() {
  		return "1".equals(shiptoManualJurInd);
  	}
  	
}
