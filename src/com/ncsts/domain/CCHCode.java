package com.ncsts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TB_CCH_CODE")
public class CCHCode extends Auditable implements Serializable,Idable<CCHCodePK> {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CCHCodePK codePK;
	
	@Column(name="FIELDPOS")
	private String fldPos;
	
	@Column(name="FIELDLEN")
	private String fldLen;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="CUSTOM_FLAG")
	private String customFlag;
	
	@Column(name="MODIFIED_FLAG")
	private String modifiedFlag;
	
	public CCHCode() {
		
	}
	public CCHCode(CCHCodePK codePK) {
		this.codePK = codePK;
	}

	// Idable interface
	public CCHCodePK getId() {
		return codePK;
	}
	
    public void setId(CCHCodePK id) {
    	this.codePK = id;
    }
    
	public String getIdPropertyName() {
		return "codePK";
	}

	public CCHCodePK getCodePK() {
		return codePK;
	}
	public void setCodePK(CCHCodePK codePK) {
		this.codePK = codePK;
	}
	public String getFldPos() {
		return fldPos;
	}
	public void setFldPos(String fldPos) {
		this.fldPos = fldPos;
	}
	public String getFldLen() {
		return fldLen;
	}
	public void setFldLen(String fldLen) {
		this.fldLen = fldLen;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCustomFlag() {
		return customFlag;
	}
	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}
	public String getModifiedFlag() {
		return modifiedFlag;
	}
	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}
}
