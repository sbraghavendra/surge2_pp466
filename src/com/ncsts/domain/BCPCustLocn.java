package com.ncsts.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

import javax.persistence.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;


/**
 * The persistent class for the TB_BCP_CUST_LOCN database table.
 * 
 */
@Entity
@Table(name="TB_BCP_CUST_LOCN")
public class BCPCustLocn implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TB_BCP_CUST_LOCN_CUSTLOCNID_GENERATOR" , allocationSize = 1, sequenceName="SQ_TB_BCP_CUST_LOCN_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_BCP_CUST_LOCN_CUSTLOCNID_GENERATOR")
	@Column(name="BCP_CUST_LOCN_ID")
	private Long bcpCustLocnId;
	
	@Column(name="BATCH_ID")
	private Long batchId;
	
	@Column(name="RECORD_TYPE")
	private String recordType;
	
	@Column(name="CUST_NBR")
	private String custNbr;
	
	@Column(name="CUST_NAME")
	private String custName;
	
	@Column(name="CUST_LOCN_CODE")
	private String custLocnCode;
	
	@Column(name="LOCATION_NAME")
	private String locationName;
	
	@Column(name="GEOCODE")
	private String geocode;
	
	@Column(name="ADDRESS_LINE_1")
	private String addressLine1;

	@Column(name="ADDRESS_LINE_2")
	private String addressLine2;

	@Column(name="CITY")
	private String city;
	
	@Column(name="COUNTY")
	private String county;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="ZIP")
	private String zip;

	@Column(name="ZIPPLUS4")
	private String zipplus4;

	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Column(name="ENTITY_CODE")    
	private String entityCode;  
	
	@Column(name="UPDATE_CODE")    
	private String updateCode;  
	
	@Transient
	private String text;
	
	@Transient
	private Long line;
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getLine() {
		return line;
	}

	public void setLine(Long line) {
		this.line = line;
	}

    public BCPCustLocn() {
    }
    
    public Long getId() {
		return getBcpCustLocnId();
	}

	public String getIdPropertyName() {
		return "custLocnId";
	}
	
	public void setId(Long id) {
		setBcpCustLocnId(id);
	}

	public Long getBcpCustLocnId() {
		return this.bcpCustLocnId;
	}

	public void setBcpCustLocnId(Long bcpCustLocnId) {
		this.bcpCustLocnId = bcpCustLocnId;
	}
	
	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNbr() {
		return this.custNbr;
	}

	public void setCustNbr(String custNbr) {
		this.custNbr = custNbr;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCustLocnCode() {
		return this.custLocnCode;
	}

	public void setCustLocnCode(String custLocnCode) {
		this.custLocnCode = custLocnCode;
	}

	public String getGeocode() {
		return this.geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getZipplus4() {
		return this.zipplus4;
	}

	public void setZipplus4(String zipplus4) {
		this.zipplus4 = zipplus4;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getEntityCode() {
        return this.entityCode;
    }
    
    public void setEntityCode(String entityCode) {
    	this.entityCode = entityCode;
    }
    
    public String getUpdateCode() {
        return this.updateCode;
    }
    
    public void setUpdateCode(String updateCode) {
    	this.updateCode = updateCode;
    }
    
    private static HashMap<Class<?>, Integer> sqlTypes = new HashMap<Class<?>, Integer>();
	static {
		sqlTypes.put(Long.class, Types.BIGINT);
		sqlTypes.put(String.class, Types.VARCHAR);
		sqlTypes.put(Date.class, Types.DATE);
		sqlTypes.put(Float.class, Types.FLOAT);
		sqlTypes.put(Boolean.class, Types.BOOLEAN);
		sqlTypes.put(Double.class, Types.DOUBLE);
		sqlTypes.put(BigDecimal.class, Types.NUMERIC);
	}
    
    public String getRawInsertStatement() throws Exception {
		Class<?> cls = BCPCustLocn.class;
		Field [] fields = cls.getDeclaredFields();
		StringBuffer colBuf = new StringBuffer("insert into TB_BCP_CUST_LOCN (");
		StringBuffer valBuf = new StringBuffer(" values (");
		boolean first = true;
		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				idx++;
				if (first)
					first = false;
				else {
					colBuf.append(", ");
					valBuf.append(", ");
				}
				colBuf.append(col.name());
				valBuf.append("?");
			}
		}
		colBuf.append(") " + valBuf + ")");
		String sql = colBuf.toString();
		System.out.println(sql);
		
		return sql;
	}
	
	public void populatePreparedStatement(Connection con, PreparedStatement s) throws Exception {
		Class<?> cls = BCPCustLocn.class;
		Field [] fields = cls.getDeclaredFields();

		int idx = 0;
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				Object fld = f.get(this);
				idx++;
				if (fld == null){					
					s.setNull(idx, sqlTypes.get(f.getType()));	
				}
				else{
					if(sqlTypes.get(f.getType()) == Types.FLOAT){
						//Make sure it is float instead of Float object
						s.setFloat(idx, ((Float)fld).floatValue());
					}
					else if(sqlTypes.get(f.getType()) == Types.DATE){
						//Make sure it is sql date instead of java util date
						s.setDate(idx, new java.sql.Date(((Date)fld).getTime()));
					}
					else{
						s.setObject(idx, fld);
					}
				}
			}
		}		
	}
	
}