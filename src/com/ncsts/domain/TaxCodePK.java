package com.ncsts.domain;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author Vinay Singh & Paul Govindan
 *
 */
@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
public class TaxCodePK implements Serializable{

	private static final long serialVersionUID = 1318470152133640111L;
	
	@Column(name="TAXCODE_CODE")  	
	private String taxcodeCode; 
	   
	public TaxCodePK(){
		
	}
	
	public TaxCodePK(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}
	
	@Column(name="taxcodeCode")
	public String getTaxcodeCode() {
		return taxcodeCode;
	}
	
	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}
	
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxCodePK)) {
            return false;
        }

        final TaxCodePK revision = (TaxCodePK)other;

        String c1 = getTaxcodeCode();
        String c2 = revision.getTaxcodeCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getTaxcodeCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.taxcodeCode;
    }
	
}
