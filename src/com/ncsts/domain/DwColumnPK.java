package com.ncsts.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

@Embeddable
public class DwColumnPK implements Serializable {
	
	private static final long serialVersionUID = 1L;	

	@Column(name="WINDOW_NAME")
	private String windowName;
	
	@Column(name="DATAWINDOW_NAME")
	private String dataWindowName;
	
	@Column(name="USER_CODE")
	private String userCode;
	
	@Column(name="COLUMN_NAME")
	private String columnName;
    
    public DwColumnPK(){	
    }

    public String getWindowName() {
        return this.windowName;
    }
    
    public void setWindowName(String windowName) {
    	this.windowName = windowName;
    }
    
    public String getDataWindowName() {
		return dataWindowName;
	}

	public void setDataWindowName(String dataWindowName) {
		this.dataWindowName = dataWindowName;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	

    
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof DwColumnPK)) {
            return false;
        }

        final DwColumnPK revision = (DwColumnPK)other;

        String w1 = getWindowName();
        String w2 = revision.getWindowName();
        if (!((w1 == w2) || (w1 != null && w1.equals(w2)))) {
            return false;
        }

        String f1 = getDataWindowName();
        String f2 = revision.getDataWindowName();
        if (!((f1 == f2) || (f1 != null && f1.equals(f2)))) {
            return false;
        }
        
        String u1 = getUserCode();
        String u2 = revision.getUserCode();
        if (!((u1 == u2) || (u1 != null && u1.equals(u2)))) {
            return false;
        }
        
        String c1 = getColumnName();
        String c2 = revision.getColumnName();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
    
    public int hashCode() {
		int hash = 7;
        String f1 = getWindowName();
		hash = 31 * hash + (null == f1 ? 0 : f1.hashCode());
        String f2 = getDataWindowName();
		hash = 31 * hash + (null == f2 ? 0 : f2.hashCode());
        String f3 = getUserCode();
		hash = 31 * hash + (null == f3 ? 0 : f3.hashCode());
		String f4 = getColumnName();
		hash = 31 * hash + (null == f4 ? 0 : f4.hashCode());

		return hash;
    }
}
