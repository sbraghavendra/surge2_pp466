package com.ncsts.domain;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author pmarquis
 *
 */
@Entity
@Table(name = "TB_BATCH_METADATA")
public class BatchMetadata extends Auditable implements java.io.Serializable {
	
	@Retention(RetentionPolicy.RUNTIME)
    public @interface Order {
        int value();
    }
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BATCH_TYPE_CODE")
	private String batchTypeCode;

	@Column(name = "VC01_DESC")
	private String vc01Desc;

	@Column(name = "VC02_DESC")
	private String vc02Desc;

	@Column(name = "VC03_DESC")
	private String vc03Desc;

	@Column(name = "VC04_DESC")
	private String vc04Desc;

	@Column(name = "VC05_DESC")
	private String vc05Desc;

	@Column(name = "VC06_DESC")
	private String vc06Desc;

	@Column(name = "VC07_DESC")
	private String vc07Desc;

	@Column(name = "VC08_DESC")
	private String vc08Desc;

	@Column(name = "VC09_DESC")
	private String vc09Desc;

	@Column(name = "VC10_DESC")
	private String vc10Desc;

	@Column(name = "NU01_DESC")
	private String nu01Desc;

	@Column(name = "NU02_DESC")
	private String nu02Desc;

	@Column(name = "NU03_DESC")
	private String nu03Desc;

	@Column(name = "NU04_DESC")
	private String nu04Desc;

	@Column(name = "NU05_DESC")
	private String nu05Desc;

	@Column(name = "NU06_DESC")
	private String nu06Desc;

	@Column(name = "NU07_DESC")
	private String nu07Desc;

	@Column(name = "NU08_DESC")
	private String nu08Desc;

	@Column(name = "NU09_DESC")
	private String nu09Desc;

	@Column(name = "NU10_DESC")
	private String nu10Desc;

	@Column(name = "TS01_DESC")
	private String ts01Desc;

	@Column(name = "TS02_DESC")
	private String ts02Desc;

	@Column(name = "TS03_DESC")
	private String ts03Desc;

	@Column(name = "TS04_DESC")
	private String ts04Desc;

	@Column(name = "TS05_DESC")
	private String ts05Desc;

	@Column(name = "TS06_DESC")
	private String ts06Desc;

	@Column(name = "TS07_DESC")
	private String ts07Desc;

	@Column(name = "TS08_DESC")
	private String ts08Desc;

	@Column(name = "TS09_DESC")
	private String ts09Desc;

	@Column(name = "TS10_DESC")
	private String ts10Desc;



	@Column(name = "UVC01_DESC")
	private String uvc01Desc;

	@Column(name = "UVC02_DESC")
	private String uvc02Desc;

	@Column(name = "UVC03_DESC")
	private String uvc03Desc;

	@Column(name = "UVC04_DESC")
	private String uvc04Desc;

	@Column(name = "UVC05_DESC")
	private String uvc05Desc;

	@Column(name = "UVC06_DESC")
	private String uvc06Desc;

	@Column(name = "UVC07_DESC")
	private String uvc07Desc;

	@Column(name = "UVC08_DESC")
	private String uvc08Desc;

	@Column(name = "UVC09_DESC")
	private String uvc09Desc;

	@Column(name = "UVC10_DESC")
	private String uvc10Desc;


	@Column(name = "UNU01_DESC")
	private String unu01Desc;

	@Column(name = "UNU02_DESC")
	private String unu02Desc;

	@Column(name = "UNU03_DESC")
	private String unu03Desc;

	@Column(name = "UNU04_DESC")
	private String unu04Desc;

	@Column(name = "UNU05_DESC")
	private String unu05Desc;

	@Column(name = "UNU06_DESC")
	private String unu06Desc;

	@Column(name = "UNU07_DESC")
	private String unu07Desc;

	@Column(name = "UNU08_DESC")
	private String unu08Desc;

	@Column(name = "UNU09_DESC")
	private String unu09Desc;

	@Column(name = "UNU10_DESC")
	private String unu10Desc;


	@Column(name = "UTS01_DESC")
	private String uts01Desc;

	@Column(name = "UTS02_DESC")
	private String uts02Desc;

	@Column(name = "UTS03_DESC")
	private String uts03Desc;

	@Column(name = "UTS04_DESC")
	private String uts04Desc;

	@Column(name = "UTS05_DESC")
	private String uts05Desc;

	@Column(name = "UTS06_DESC")
	private String uts06Desc;

	@Column(name = "UTS07_DESC")
	private String uts07Desc;

	@Column(name = "UTS08_DESC")
	private String uts08Desc;

	@Column(name = "UTS09_DESC")
	private String uts09Desc;

	@Column(name = "UTS10_DESC")
	private String uts10Desc;


	//Midtier project code added - january 2009
	@OneToMany(mappedBy="batchMetadata")
	private Set<BatchMaintenance> batch = new HashSet<BatchMaintenance>();

	/** default constructor */
	public BatchMetadata() {
	}

	/** minimal constructor */
	public BatchMetadata(String batchTypeCode) {
		this.batchTypeCode = batchTypeCode;
	}

	public String getBatchTypeCode() {
		return batchTypeCode;
	}

	public void setBatchTypeCode(String batchTypeCode) {
		this.batchTypeCode = batchTypeCode;
	}

	public String getVc01Desc() {
		return vc01Desc;
	}

	public void setVc01Desc(String vc01Desc) {
		this.vc01Desc = vc01Desc;
	}

	public String getVc02Desc() {
		return vc02Desc;
	}

	public void setVc02Desc(String vc02Desc) {
		this.vc02Desc = vc02Desc;
	}

	public String getVc03Desc() {
		return vc03Desc;
	}

	public void setVc03Desc(String vc03Desc) {
		this.vc03Desc = vc03Desc;
	}

	public String getVc04Desc() {
		return vc04Desc;
	}

	public void setVc04Desc(String vc04Desc) {
		this.vc04Desc = vc04Desc;
	}

	public String getVc05Desc() {
		return vc05Desc;
	}

	public void setVc05Desc(String vc05Desc) {
		this.vc05Desc = vc05Desc;
	}

	public String getVc06Desc() {
		return vc06Desc;
	}

	public void setVc06Desc(String vc06Desc) {
		this.vc06Desc = vc06Desc;
	}

	public String getVc07Desc() {
		return vc07Desc;
	}

	public void setVc07Desc(String vc07Desc) {
		this.vc07Desc = vc07Desc;
	}

	public String getVc08Desc() {
		return vc08Desc;
	}

	public void setVc08Desc(String vc08Desc) {
		this.vc08Desc = vc08Desc;
	}

	public String getVc09Desc() {
		return vc09Desc;
	}

	public void setVc09Desc(String vc09Desc) {
		this.vc09Desc = vc09Desc;
	}

	public String getVc10Desc() {
		return vc10Desc;
	}

	public void setVc10Desc(String vc10Desc) {
		this.vc10Desc = vc10Desc;
	}

	public String getNu01Desc() {
		return nu01Desc;
	}

	public void setNu01Desc(String nu01Desc) {
		this.nu01Desc = nu01Desc;
	}

	public String getNu02Desc() {
		return nu02Desc;
	}

	public void setNu02Desc(String nu02Desc) {
		this.nu02Desc = nu02Desc;
	}

	public String getNu03Desc() {
		return nu03Desc;
	}

	public void setNu03Desc(String nu03Desc) {
		this.nu03Desc = nu03Desc;
	}

	public String getNu04Desc() {
		return nu04Desc;
	}

	public void setNu04Desc(String nu04Desc) {
		this.nu04Desc = nu04Desc;
	}

	public String getNu05Desc() {
		return nu05Desc;
	}

	public void setNu05Desc(String nu05Desc) {
		this.nu05Desc = nu05Desc;
	}

	public String getNu06Desc() {
		return nu06Desc;
	}

	public void setNu06Desc(String nu06Desc) {
		this.nu06Desc = nu06Desc;
	}

	public String getNu07Desc() {
		return nu07Desc;
	}

	public void setNu07Desc(String nu07Desc) {
		this.nu07Desc = nu07Desc;
	}

	public String getNu08Desc() {
		return nu08Desc;
	}

	public void setNu08Desc(String nu08Desc) {
		this.nu08Desc = nu08Desc;
	}

	public String getNu09Desc() {
		return nu09Desc;
	}

	public void setNu09Desc(String nu09Desc) {
		this.nu09Desc = nu09Desc;
	}

	public String getNu10Desc() {
		return nu10Desc;
	}

	public void setNu10Desc(String nu10Desc) {
		this.nu10Desc = nu10Desc;
	}

	public String getTs01Desc() {
		return ts01Desc;
	}

	public void setTs01Desc(String ts01Desc) {
		this.ts01Desc = ts01Desc;
	}

	public String getTs02Desc() {
		return ts02Desc;
	}

	public void setTs02Desc(String ts02Desc) {
		this.ts02Desc = ts02Desc;
	}

	public String getTs03Desc() {
		return ts03Desc;
	}

	public void setTs03Desc(String ts03Desc) {
		this.ts03Desc = ts03Desc;
	}

	public String getTs04Desc() {
		return ts04Desc;
	}

	public void setTs04Desc(String ts04Desc) {
		this.ts04Desc = ts04Desc;
	}

	public String getTs05Desc() {
		return ts05Desc;
	}

	public void setTs05Desc(String ts05Desc) {
		this.ts05Desc = ts05Desc;
	}

	public String getTs06Desc() {
		return ts06Desc;
	}

	public void setTs06Desc(String ts06Desc) {
		this.ts06Desc = ts06Desc;
	}

	public String getTs07Desc() {
		return ts07Desc;
	}

	public void setTs07Desc(String ts07Desc) {
		this.ts07Desc = ts07Desc;
	}

	public String getTs08Desc() {
		return ts08Desc;
	}

	public void setTs08Desc(String ts08Desc) {
		this.ts08Desc = ts08Desc;
	}

	public String getTs09Desc() {
		return ts09Desc;
	}

	public void setTs09Desc(String ts09Desc) {
		this.ts09Desc = ts09Desc;
	}

	public String getTs10Desc() {
		return ts10Desc;
	}

	public void setTs10Desc(String ts10Desc) {
		this.ts10Desc = ts10Desc;
	}

	public Set<BatchMaintenance> getBatch() {
		return batch;
	}

	public void setBatch(Set<BatchMaintenance> batch) {
		this.batch = batch;
	}


	public String getUvc01Desc() {
		return uvc01Desc;
	}

	public void setUvc01Desc(String uvc01Desc) {
		this.uvc01Desc = uvc01Desc;
	}

	public String getUvc02Desc() {
		return uvc02Desc;
	}

	public void setUvc02Desc(String uvc02Desc) {
		this.uvc02Desc = uvc02Desc;
	}

	public String getUvc03Desc() {
		return uvc03Desc;
	}

	public void setUvc03Desc(String uvc03Desc) {
		this.uvc03Desc = uvc03Desc;
	}

	public String getUvc04Desc() {
		return uvc04Desc;
	}

	public void setUvc04Desc(String uvc04Desc) {
		this.uvc04Desc = uvc04Desc;
	}

	public String getUvc05Desc() {
		return uvc05Desc;
	}

	public void setUvc05Desc(String uvc05Desc) {
		this.uvc05Desc = uvc05Desc;
	}

	public String getUvc06Desc() {
		return uvc06Desc;
	}

	public void setUvc06Desc(String uvc06Desc) {
		this.uvc06Desc = uvc06Desc;
	}

	public String getUvc07Desc() {
		return uvc07Desc;
	}

	public void setUvc07Desc(String uvc07Desc) {
		this.uvc07Desc = uvc07Desc;
	}

	public String getUvc08Desc() {
		return uvc08Desc;
	}

	public void setUvc08Desc(String uvc08Desc) {
		this.uvc08Desc = uvc08Desc;
	}

	public String getUvc09Desc() {
		return uvc09Desc;
	}

	public void setUvc09Desc(String uvc09Desc) {
		this.uvc09Desc = uvc09Desc;
	}

	public String getUvc10Desc() {
		return uvc10Desc;
	}

	public void setUvc10Desc(String uvc10Desc) {
		this.uvc10Desc = uvc10Desc;
	}

	public String getUnu01Desc() {
		return unu01Desc;
	}

	public void setUnu01Desc(String unu01Desc) {
		this.unu01Desc = unu01Desc;
	}

	public String getUnu02Desc() {
		return unu02Desc;
	}

	public void setUnu02Desc(String unu02Desc) {
		this.unu02Desc = unu02Desc;
	}

	public String getUnu03Desc() {
		return unu03Desc;
	}

	public void setUnu03Desc(String unu03Desc) {
		this.unu03Desc = unu03Desc;
	}

	public String getUnu04Desc() {
		return unu04Desc;
	}

	public void setUnu04Desc(String unu04Desc) {
		this.unu04Desc = unu04Desc;
	}

	public String getUnu05Desc() {
		return unu05Desc;
	}

	public void setUnu05Desc(String unu05Desc) {
		this.unu05Desc = unu05Desc;
	}

	public String getUnu06Desc() {
		return unu06Desc;
	}

	public void setUnu06Desc(String unu06Desc) {
		this.unu06Desc = unu06Desc;
	}

	public String getUnu07Desc() {
		return unu07Desc;
	}

	public void setUnu07Desc(String unu07Desc) {
		this.unu07Desc = unu07Desc;
	}

	public String getUnu08Desc() {
		return unu08Desc;
	}

	public void setUnu08Desc(String unu08Desc) {
		this.unu08Desc = unu08Desc;
	}

	public String getUnu09Desc() {
		return unu09Desc;
	}

	public void setUnu09Desc(String unu09Desc) {
		this.unu09Desc = unu09Desc;
	}

	public String getUnu10Desc() {
		return unu10Desc;
	}

	public void setUnu10Desc(String unu10Desc) {
		this.unu10Desc = unu10Desc;
	}

	public String getUts01Desc() {
		return uts01Desc;
	}

	public void setUts01Desc(String uts01Desc) {
		this.uts01Desc = uts01Desc;
	}

	public String getUts02Desc() {
		return uts02Desc;
	}

	public void setUts02Desc(String uts02Desc) {
		this.uts02Desc = uts02Desc;
	}

	public String getUts03Desc() {
		return uts03Desc;
	}

	public void setUts03Desc(String uts03Desc) {
		this.uts03Desc = uts03Desc;
	}

	public String getUts04Desc() {
		return uts04Desc;
	}

	public void setUts04Desc(String uts04Desc) {
		this.uts04Desc = uts04Desc;
	}

	public String getUts05Desc() {
		return uts05Desc;
	}

	public void setUts05Desc(String uts05Desc) {
		this.uts05Desc = uts05Desc;
	}

	public String getUts06Desc() {
		return uts06Desc;
	}

	public void setUts06Desc(String uts06Desc) {
		this.uts06Desc = uts06Desc;
	}

	public String getUts07Desc() {
		return uts07Desc;
	}

	public void setUts07Desc(String uts07Desc) {
		this.uts07Desc = uts07Desc;
	}

	public String getUts08Desc() {
		return uts08Desc;
	}

	public void setUts08Desc(String uts08Desc) {
		this.uts08Desc = uts08Desc;
	}

	public String getUts09Desc() {
		return uts09Desc;
	}

	public void setUts09Desc(String uts09Desc) {
		this.uts09Desc = uts09Desc;
	}

	public String getUts10Desc() {
		return uts10Desc;
	}

	public void setUts10Desc(String uts10Desc) {
		this.uts10Desc = uts10Desc;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("BatchMetadata{");
		sb.append("batchTypeCode='").append(batchTypeCode).append('\'');
		sb.append(", vc01Desc='").append(vc01Desc).append('\'');
		sb.append(", vc02Desc='").append(vc02Desc).append('\'');
		sb.append(", vc03Desc='").append(vc03Desc).append('\'');
		sb.append(", vc04Desc='").append(vc04Desc).append('\'');
		sb.append(", vc05Desc='").append(vc05Desc).append('\'');
		sb.append(", vc06Desc='").append(vc06Desc).append('\'');
		sb.append(", vc07Desc='").append(vc07Desc).append('\'');
		sb.append(", vc08Desc='").append(vc08Desc).append('\'');
		sb.append(", vc09Desc='").append(vc09Desc).append('\'');
		sb.append(", vc10Desc='").append(vc10Desc).append('\'');
		sb.append(", nu01Desc='").append(nu01Desc).append('\'');
		sb.append(", nu02Desc='").append(nu02Desc).append('\'');
		sb.append(", nu03Desc='").append(nu03Desc).append('\'');
		sb.append(", nu04Desc='").append(nu04Desc).append('\'');
		sb.append(", nu05Desc='").append(nu05Desc).append('\'');
		sb.append(", nu06Desc='").append(nu06Desc).append('\'');
		sb.append(", nu07Desc='").append(nu07Desc).append('\'');
		sb.append(", nu08Desc='").append(nu08Desc).append('\'');
		sb.append(", nu09Desc='").append(nu09Desc).append('\'');
		sb.append(", nu10Desc='").append(nu10Desc).append('\'');
		sb.append(", ts01Desc='").append(ts01Desc).append('\'');
		sb.append(", ts02Desc='").append(ts02Desc).append('\'');
		sb.append(", ts03Desc='").append(ts03Desc).append('\'');
		sb.append(", ts04Desc='").append(ts04Desc).append('\'');
		sb.append(", ts05Desc='").append(ts05Desc).append('\'');
		sb.append(", ts06Desc='").append(ts06Desc).append('\'');
		sb.append(", ts07Desc='").append(ts07Desc).append('\'');
		sb.append(", ts08Desc='").append(ts08Desc).append('\'');
		sb.append(", ts09Desc='").append(ts09Desc).append('\'');
		sb.append(", ts10Desc='").append(ts10Desc).append('\'');
		sb.append(", uvc01Desc='").append(uvc01Desc).append('\'');
		sb.append(", uvc02Desc='").append(uvc02Desc).append('\'');
		sb.append(", uvc03Desc='").append(uvc03Desc).append('\'');
		sb.append(", uvc04Desc='").append(uvc04Desc).append('\'');
		sb.append(", uvc05Desc='").append(uvc05Desc).append('\'');
		sb.append(", uvc06Desc='").append(uvc06Desc).append('\'');
		sb.append(", uvc07Desc='").append(uvc07Desc).append('\'');
		sb.append(", uvc08Desc='").append(uvc08Desc).append('\'');
		sb.append(", uvc09Desc='").append(uvc09Desc).append('\'');
		sb.append(", uvc10Desc='").append(uvc10Desc).append('\'');
		sb.append(", unu01Desc='").append(unu01Desc).append('\'');
		sb.append(", unu02Desc='").append(unu02Desc).append('\'');
		sb.append(", unu03Desc='").append(unu03Desc).append('\'');
		sb.append(", unu04Desc='").append(unu04Desc).append('\'');
		sb.append(", unu05Desc='").append(unu05Desc).append('\'');
		sb.append(", unu06Desc='").append(unu06Desc).append('\'');
		sb.append(", unu07Desc='").append(unu07Desc).append('\'');
		sb.append(", unu08Desc='").append(unu08Desc).append('\'');
		sb.append(", unu09Desc='").append(unu09Desc).append('\'');
		sb.append(", unu10Desc='").append(unu10Desc).append('\'');
		sb.append(", uts01Desc='").append(uts01Desc).append('\'');
		sb.append(", uts02Desc='").append(uts02Desc).append('\'');
		sb.append(", uts03Desc='").append(uts03Desc).append('\'');
		sb.append(", uts04Desc='").append(uts04Desc).append('\'');
		sb.append(", uts05Desc='").append(uts05Desc).append('\'');
		sb.append(", uts06Desc='").append(uts06Desc).append('\'');
		sb.append(", uts07Desc='").append(uts07Desc).append('\'');
		sb.append(", uts08Desc='").append(uts08Desc).append('\'');
		sb.append(", uts09Desc='").append(uts09Desc).append('\'');
		sb.append(", uts10Desc='").append(uts10Desc).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
