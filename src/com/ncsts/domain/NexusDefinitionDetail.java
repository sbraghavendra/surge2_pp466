package com.ncsts.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TB_NEXUS_DEF_DETAIL")
public class NexusDefinitionDetail extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="NEXUS_DEF_DETAIL_ID")
    @GeneratedValue (strategy = GenerationType.SEQUENCE , generator="nexus_definition_detail_sequence")
	@SequenceGenerator(name="nexus_definition_detail_sequence" , allocationSize = 1, sequenceName="sq_tb_nexus_def_detail_id")
	private Long nexusDefDetailId;
	
	@Column(name="NEXUS_DEF_ID")
	private Long nexusDefId;
	
	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;
	
	@Column(name="NEXUS_TYPE_CODE")
	private String nexusTypeCode;
	
	@Column(name="ACTIVE_FLAG")
	private String activeFlag;
	
	@Column(name="AUTO_USER_ID")
	private String autoUserId;

	public Long getNexusDefDetailId() {
		return nexusDefDetailId;
	}

	public void setNexusDefDetailId(Long nexusDefDetailId) {
		this.nexusDefDetailId = nexusDefDetailId;
	}

	public Long getNexusDefId() {
		return nexusDefId;
	}

	public void setNexusDefId(Long nexusDefId) {
		this.nexusDefId = nexusDefId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getNexusTypeCode() {
		return nexusTypeCode;
	}

	public void setNexusTypeCode(String nexusTypeCode) {
		this.nexusTypeCode = nexusTypeCode;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	@Override
	public Long getId() {
		return this.nexusDefDetailId;
	}

	@Override
	public void setId(Long id) {
		this.nexusDefDetailId = id;
	}

	@Override
	public String getIdPropertyName() {
		return "nexusDefDetailId";
	}

	public String getAutoUserId() {
		return autoUserId;
	}

	public void setAutoUserId(String autoUserId) {
		this.autoUserId = autoUserId;
	}
}
