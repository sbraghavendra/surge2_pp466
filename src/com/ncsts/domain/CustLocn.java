package com.ncsts.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the TB_CUST_LOCN database table.
 * 
 */
@Entity
@Table(name="TB_CUST_LOCN")
public class CustLocn extends Auditable implements Serializable, Idable<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TB_CUST_LOCN_CUSTLOCNID_GENERATOR" , allocationSize = 1, sequenceName="SQ_TB_CUST_LOCN_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CUST_LOCN_CUSTLOCNID_GENERATOR")
	@Column(name="CUST_LOCN_ID")
	private Long custLocnId;
	
	@Column(name="CUST_ID")
	private Long custId;

	@Column(name="ACTIVE_FLAG")
	private String activeFlag;

	@Column(name="ADDRESS_LINE_1")
	private String addressLine1;

	@Column(name="ADDRESS_LINE_2")
	private String addressLine2;

	@Column(name="CITY")
	private String city;

	@Column(name="COUNTRY")
	private String country;

	@Column(name="COUNTY")
	private String county;

	@Column(name="CUST_LOCN_CODE")
	private String custLocnCode;

	@Column(name="GEOCODE")
	private String geocode;

	@Column(name="LOCATION_NAME")
	private String locationName;

	@Column(name="STATE")
	private String state;

	@Column(name="ZIP")
	private String zip;

	@Column(name="ZIPPLUS4")
	private String zipplus4;

	//bi-directional many-to-one association to Cust
    //@ManyToOne
	//@JoinColumn(name="CUST_ID")
	//private Cust cust;

	//bi-directional many-to-one association to CustLocnEx
	//@OneToMany(mappedBy="custLocn")
	//private Set<CustLocnEx> custLocnExs;

    public CustLocn() {
    }
    
    public Long getId() {
		return getCustLocnId();
	}

	public String getIdPropertyName() {
		return "custLocnId";
	}
	
	public void setId(Long id) {
		setCustLocnId(id);
	}

	public Long getCustLocnId() {
		return this.custLocnId;
	}

	public void setCustLocnId(Long custLocnId) {
		this.custLocnId = custLocnId;
	}
	
	public Long getCustId() {
		return this.custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCustLocnCode() {
		return this.custLocnCode;
	}

	public void setCustLocnCode(String custLocnCode) {
		this.custLocnCode = custLocnCode;
	}

	public String getGeocode() {
		return this.geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getZipplus4() {
		return this.zipplus4;
	}

	public void setZipplus4(String zipplus4) {
		this.zipplus4 = zipplus4;
	}
	
	public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	//public Cust getCust() {
	//	return this.cust;
	//}

	//public void setCust(Cust cust) {
	//	this.cust = cust;
	//}
	
	//public Set<CustLocnEx> getCustLocnExs() {
	//	return this.custLocnExs;
	//}

	//public void setCustLocnExs(Set<CustLocnEx> custLocnExs) {
	//	this.custLocnExs = custLocnExs;
	//}
	
}