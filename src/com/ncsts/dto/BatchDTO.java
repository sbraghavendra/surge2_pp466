package com.ncsts.dto;

import java.util.Date;

public class BatchDTO {
	
	private Long batchId;
	
	private String batchTypeCode;
	private Date entryTimestamp;
	private String batchStatusCode;
	private String heldFlag;
	private String errorSevCode;
	private Long totalBytes;
	private Long totalRows;
	private Long startRow;
	private Long processedBytes;
	private Long processedRows;
	private Date schedStartTimestamp;
	private Date actualStartTimestamp;
	private Date actualEndTimestamp;
	private String statusUpdateUserId;
	private Date statusUpdateTimestamp;
	private String updateUserId;
	private Date updateTimestamp;
	private String yearPeriod;
	private Long relSeqn;
	
	
	public Long getBatchId() {
		return batchId;
	}
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	public String getBatchTypeCode() {
		return batchTypeCode;
	}
	public void setBatchTypeCode(String batchTypeCode) {
		this.batchTypeCode = batchTypeCode;
	}
	public Date getEntryTimestamp() {
		return entryTimestamp;
	}
	public void setEntryTimestamp(Date entryTimestamp) {
		this.entryTimestamp = entryTimestamp;
	}
	public String getBatchStatusCode() {
		return batchStatusCode;
	}
	public void setBatchStatusCode(String batchStatusCode) {
		this.batchStatusCode = batchStatusCode;
	}
	public String getHeldFlag() {
		return heldFlag;
	}
    public Boolean getHeldFlagBoolean() {
        return "1".equals(heldFlag);
    }
	public void setHeldFlag(String heldFlag) {
		this.heldFlag = heldFlag;
	}
	public String getErrorSevCode() {
		return errorSevCode;
	}
	public void setErrorSevCode(String errorSevCode) {
		this.errorSevCode = errorSevCode;
	}
	public Long getTotalBytes() {
		return totalBytes;
	}
	public void setTotalBytes(Long totalBytes) {
		this.totalBytes = totalBytes;
	}
	public Long getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}
	public Long getStartRow() {
		return startRow;
	}
	public void setStartRow(Long startRow) {
		this.startRow = startRow;
	}
	public Long getProcessedBytes() {
		return processedBytes;
	}
	public void setProcessedBytes(Long processedBytes) {
		this.processedBytes = processedBytes;
	}
	public Long getProcessedRows() {
		return processedRows;
	}
	public void setProcessedRows(Long processedRows) {
		this.processedRows = processedRows;
	}
	public Date getSchedStartTimestamp() {
		return schedStartTimestamp;
	}
	public void setSchedStartTimestamp(Date schedStartTimestamp) {
		this.schedStartTimestamp = schedStartTimestamp;
	}
	public Date getActualStartTimestamp() {
		return actualStartTimestamp;
	}
	public void setActualStartTimestamp(Date actualStartTimestamp) {
		this.actualStartTimestamp = actualStartTimestamp;
	}
	public Date getActualEndTimestamp() {
		return actualEndTimestamp;
	}
	public void setActualEndTimestamp(Date actualEndTimestamp) {
		this.actualEndTimestamp = actualEndTimestamp;
	}
	public String getStatusUpdateUserId() {
		return statusUpdateUserId;
	}
	public void setStatusUpdateUserId(String statusUpdateUserId) {
		this.statusUpdateUserId = statusUpdateUserId;
	}
	public Date getStatusUpdateTimestamp() {
		return statusUpdateTimestamp;
	}
	public void setStatusUpdateTimestamp(Date statusUpdateTimestamp) {
		this.statusUpdateTimestamp = statusUpdateTimestamp;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	public String getYearPeriod() {
		return yearPeriod;
	}
	public void setYearPeriod(String yearPeriod) {
		this.yearPeriod = yearPeriod;
	}
	public Long getRelSeqn() {
		return relSeqn;
	}
	public void setRelSeqn(Long relSeqn) {
		this.relSeqn = relSeqn;
	}
	

}
