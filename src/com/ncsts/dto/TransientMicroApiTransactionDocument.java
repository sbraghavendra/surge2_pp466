package com.ncsts.dto;

import com.ncsts.domain.MicroApiTransaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransientMicroApiTransactionDocument {

    private final Map<TaxCodeRuleGroupBy, List<MicroApiTransaction>> groupedDocument = new HashMap<TaxCodeRuleGroupBy, List<MicroApiTransaction>>();

    public Map<TaxCodeRuleGroupBy, List<MicroApiTransaction>> getGroupedDocument() {
        return groupedDocument;
    }



}
