package com.ncsts.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ncsts.view.validator.TimeValidator;
import java.util.TimeZone;

public class BatchPreferenceDTO implements Serializable {
private static Logger log = Logger.getLogger(BatchPreferenceDTO.class);


	private static final long serialVersionUID = 1L;
	
	private String systemDefTime;
	private boolean systemUserFlag;
	private String systemRecalTime;
	private boolean systemAutoCheck;
	private String systemAutoCheckTime;
	private boolean userCompletionPopUp;
	private String adminDefTime;
	private boolean adminUserFlag;
	private String adminRecalTime;
	private boolean adminRecalUserFlag;
	private String adminIntervalTime;
	private boolean showRecalc;
	private String timeZoneName;
	private String delimiter;
	private String exportFileExt;
	private String textQualifier;
	
	// If the Admin-Allow User to Change flag is not checked, then the Admin time is used. 
	// If it is checked and the System time is not blank/null, then the System time is used. 
	// If the System-Allow User to Change flag is checked then the start date/time allows the user to change it, 
	// otherwise it is protected.
	public boolean getCanChangeBatchStartTime() {
		if (! adminUserFlag) return false;
//		if ((systemDefTime != null) && (systemDefTime.trim().length() > 0)) return false;
		return systemUserFlag;
	}
	public Date getDefaultBatchStartTime () {
		String t;
		if ((systemDefTime != null) && (systemDefTime.trim().length() > 0) && (adminUserFlag)) {
			t  = systemDefTime;
		} else {
			t = adminDefTime;
		}
		if ((t != null) && (t.trim().length() > 0) && (! "*NOW".equalsIgnoreCase(t))) {
			Calendar scheduleTime = Calendar.getInstance();
			Calendar d = Calendar.getInstance();
			Calendar userTime = Calendar.getInstance();
			scheduleTime.setTimeInMillis(com.ncsts.view.util.DateHelper.getUserTimeInMillis(timeZoneName));

			userTime.setTimeInMillis(com.ncsts.view.util.DateHelper.getUserTimeInMillis(timeZoneName));
			
			
			Date date = formatTime(t, timeZoneName);
			if (date != null) {
				d.setTimeInMillis(date.getTime());
				
				//Already in user timezone
				scheduleTime.set(Calendar.HOUR_OF_DAY, d.get(Calendar.HOUR_OF_DAY));
				scheduleTime.set(Calendar.MINUTE, d.get(Calendar.MINUTE));
				scheduleTime.set(Calendar.SECOND, d.get(Calendar.SECOND));
				if (scheduleTime.before(userTime)) {
					scheduleTime.add(Calendar.DAY_OF_MONTH, 1);
				}
				
				return new Date(scheduleTime.getTimeInMillis());
			}
		}
		
		if("*NOW".equalsIgnoreCase(t)) {
			return new Date();
		}
		
		return new Date(com.ncsts.view.util.DateHelper.getUserTimeInMillis(timeZoneName));
	}
	
	public long getUserTimeInMillis(String userTimeZoneName){
		TimeZone userTimeZone = TimeZone.getTimeZone(userTimeZoneName);//User Timezone
		TimeZone tomcatTimeZone = TimeZone.getDefault();//Tomcat Timezone
		
		Calendar currentTime = Calendar.getInstance();
		long tomcatTime = currentTime.getTimeInMillis();
		long userTime = tomcatTime + (userTimeZone.getOffset(tomcatTime)-tomcatTimeZone.getOffset(tomcatTime));
		
		return userTime;
	}
	
	/* return interval in ms */
	public int getBatchRefreshInterval() {
		int val=0;
		if (adminRecalUserFlag) {
			try {
				val = Integer.parseInt(systemRecalTime);
			} catch (NumberFormatException e) {
				log.error("Unable to parse systemRecalTime: " + systemRecalTime);
			}
		} else {
			try {
				val = Integer.parseInt(adminRecalTime);
			} catch (NumberFormatException e) {
				log.error("Unable to parse adminRecalTime: " + adminRecalTime);
			}
		}
		return val*1000;
	}
	
	public boolean getHasRefreshInterval() {
		return getBatchRefreshInterval() > 0;
	}
	
	public String getSystemDefTime() {
		return systemDefTime;
	}
	public String getAdminDefTime() {
		return adminDefTime;
	}

	public void setAdminDefTime(String adminDefTime) {
		this.adminDefTime = adminDefTime;
	}
	public void setSystemDefTime(String systemDefTime) {
		this.systemDefTime = systemDefTime;
	}

	public Date getSystemDefTimeAsDate() {
		return formatTime(this.systemDefTime);
	}
	public Date getAdminDefTimeAsDate() {
		return formatTime(this.adminDefTime);
	}
	private Date formatTime(String s) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return new TimeValidator().parse(s);
	}
	private Date formatTime(String s, String timeZoneString) {
		if ((s == null) || (s.trim().length() == 0)) return null;
		return new TimeValidator().parse(s, timeZoneString);
	}

	public boolean isSystemUserFlag() {
		return systemUserFlag;
	}
	public void setSystemUserFlag(boolean systemUserFlag) {
		this.systemUserFlag = systemUserFlag;
	}
	public String getSystemRecalTime() {
		return systemRecalTime;
	}
	public void setSystemRecalTime(String systemRecalTime) {
		this.systemRecalTime = systemRecalTime;
	}
	public String getDelimiter() {
		return delimiter;
	}
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	public String getExportFileExt() {
		return exportFileExt;
	}
	public void setExportFileExt(String exportFileExt) {
		this.exportFileExt = exportFileExt;
	}

	public String getTextQualifier() {
		return textQualifier;
	}
	public void setTextQualifier(String textQualifier) {
		this.textQualifier = textQualifier;
	}

	public boolean isSystemAutoCheck() {
		return systemAutoCheck;
	}
	public void setSystemAutoCheck(boolean systemAutoCheck) {
		this.systemAutoCheck = systemAutoCheck;
	}
	public String getSystemAutoCheckTime() {
		return systemAutoCheckTime;
	}
	public void setSystemAutoCheckTime(String systemAutoCheckTime) {
		this.systemAutoCheckTime = systemAutoCheckTime;
	}
	public boolean isUserCompletionPopUp() {
		return userCompletionPopUp;
	}
	public void setUserCompletionPopUp(boolean userCompletionPopUp) {
		this.userCompletionPopUp = userCompletionPopUp;
	}
	public boolean isAdminUserFlag() {
		return adminUserFlag;
	}
	public void setAdminUserFlag(boolean adminUserFlag) {
		this.adminUserFlag = adminUserFlag;
	}
	public String getAdminRecalTime() {
		return adminRecalTime;
	}
	public void setAdminRecalTime(String adminRecalTime) {
		this.adminRecalTime = adminRecalTime;
	}
	public String getAdminIntervalTime() {
		return adminIntervalTime;
	}
	public void setAdminIntervalTime(String adminIntervalTime) {
		this.adminIntervalTime = adminIntervalTime;
	}
	public boolean isAdminRecalUserFlag() {
		return adminRecalUserFlag;
	}
	public void setAdminRecalUserFlag(boolean adminRecalUserFlag) {
		this.adminRecalUserFlag = adminRecalUserFlag;
	}
	public boolean isShowRecalc() {
		return showRecalc;
	}
	public void setShowRecalc(boolean showRecalc) {
		this.showRecalc = showRecalc;
	}
	public String getTimeZoneName() {
		return timeZoneName;
	}
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}
}
