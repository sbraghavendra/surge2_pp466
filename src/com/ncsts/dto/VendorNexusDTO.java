package com.ncsts.dto;

import java.io.Serializable;

public class VendorNexusDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long vendorNexusId;
    private Long vendorId;  
    private String nexusCountryCode;   
    private String nexusStateCode;   
    private String nexusCounty;   
    private String nexusCity;   
    private String nexusStj;   
    private String nexusFlag; 

    public VendorNexusDTO(){
    }

	public Long getVendorNexusId() {
        return this.vendorNexusId;
    }
    
    public void setVendorNexusId(Long vendorNexusId) {
    	this.vendorNexusId = vendorNexusId;
    }
    
    public Long getVendorId() {
        return this.vendorId;
    }
    
    public void setVendorId(Long vendorId) {
    	this.vendorId = vendorId;
    }
    
    public String getNexusCountryCode() {
        return this.nexusCountryCode;
    }
    
    public void setNexusCountryCode(String nexusCountryCode) {
    	this.nexusCountryCode = nexusCountryCode;
    }  
    
    public String getNexusStateCode() {
        return this.nexusStateCode;
    }
    
    public void setNexusStateCode(String nexusStateCode) {
    	this.nexusStateCode = nexusStateCode;
    }
    
    public String getNexusCounty() {
        return this.nexusCounty;
    }
    
    public void setNexusCounty(String nexusCounty) {
    	this.nexusCounty = nexusCounty;
    }  
     
    public String getNexusCity() {
        return this.nexusCity;
    }
    
    public void setNexusCity(String nexusCity) {
    	this.nexusCity = nexusCity;
    }
    
    public String getNexusStj() {
        return this.nexusStj;
    }
    
    public void setNexusStj(String nexusStj) {
    	this.nexusStj = nexusStj;
    }
    
    public String getNexusFlag() {
        return this.nexusFlag;
    }
    
    public void setNexusFlag(String nexusFlag) {
    	this.nexusFlag = nexusFlag;
    }  

    public Boolean getNexusBooleanFlag() {
    	return "1".equals(this.nexusFlag);
	}

	public void setNexusBooleanFlag(Boolean nexusFlag) {
		this.nexusFlag = nexusFlag == Boolean.TRUE ? "1" : "0";
	}
}