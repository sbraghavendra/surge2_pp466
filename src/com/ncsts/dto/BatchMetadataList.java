/*
 * Author: Jim M. Wilson
 * Created: Sep 26, 2008
 * $Date: 2009-02-01 22:47:28 -0600 (Sun, 01 Feb 2009) $: - $Revision: 3932 $: 
 */

package com.ncsts.dto;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;

/**
 *
 */
public class BatchMetadataList {
	public class NameValuePair {
		private String name;
		private Object value;
		protected String property;
		public NameValuePair(String name, Object value, String property) {
			this.name = name;
			this.value = value;
			this.property = property;
		}
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Object getValue() {
			return this.value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
	}

	private static Logger log = Logger.getLogger(BatchMetadataList.class);

	private BatchMaintenance batchMaintenance;
	private BatchMetadata batchMetadata;
	private List<NameValuePair> numberList;
	private List<NameValuePair> stringList;
	private List<NameValuePair> dateList;
	private List<NameValuePair> userNumberList;
	private List<NameValuePair> userStringList;
	private List<NameValuePair> userDateList;


	private NumberFormat nf = NumberFormat.getIntegerInstance();
	
	public BatchMetadataList(BatchMetadata batchMetadata, BatchMaintenance batchMaintenance) {
		this.batchMaintenance = batchMaintenance;
		this.batchMetadata = batchMetadata;
		nf.setMinimumIntegerDigits(2);
	}
	
  private void add(List<NameValuePair> list, String property) {
  	property = property.toLowerCase();
    try {
    	String name = (String)PropertyUtils.getProperty(batchMetadata, property+"Desc");
    	if ((name != null) && (name.trim().length() > 0)) {
      	Object val = PropertyUtils.getProperty(batchMaintenance, property);
        list.add(new NameValuePair(name, val, property));
    	}
    } catch (Exception e) {
    	log.error(e);
    }
  }
  
  /** Faces will update the list contents (if inputText), so we need to populate
   * change into the original.
   */
  public BatchMaintenance update() {
	try {
		if (numberList!=null) {
			for (NameValuePair nvp : numberList) {
				if (nvp.value != null) {
					Double value = Double.valueOf(nvp.value.toString());
					PropertyUtils.setProperty(batchMaintenance, nvp.property, value);
				} else {
					PropertyUtils.setProperty(batchMaintenance, nvp.property, null);
				}
			}
		}
		if (stringList!=null) {
			for (NameValuePair nvp : stringList) {
				PropertyUtils.setProperty(batchMaintenance, nvp.property, (String) nvp.value);
			}
		}
		if (dateList!=null) {
			for (NameValuePair nvp : dateList) {
				PropertyUtils.setProperty(batchMaintenance, nvp.property, (Date) nvp.value);
			}
		}

		if (userNumberList!=null) {
			for (NameValuePair nvp : userNumberList) {
				if (nvp.value != null) {
					BigDecimal value = new BigDecimal(nvp.value.toString());
					//Double value = Double.valueOf(nvp.value.toString());
					PropertyUtils.setProperty(batchMaintenance, nvp.property, value);
				} else {
					PropertyUtils.setProperty(batchMaintenance, nvp.property, null);
				}
			}
		}

		if (userStringList!=null) {
			for (NameValuePair nvp : userStringList) {
				PropertyUtils.setProperty(batchMaintenance, nvp.property, (String) nvp.value);
			}
		}

		if (userDateList!=null){
			for (NameValuePair nvp : userDateList) {
				PropertyUtils.setProperty(batchMaintenance, nvp.property, (Date) nvp.value);
			}
		}

	} catch (Exception iae) {
		iae.printStackTrace();
		log.error(iae);
  	}
  	return batchMaintenance;
  }
	
  private List<NameValuePair> buildList(String name) {
  	List<NameValuePair> 	list = new ArrayList<NameValuePair>();
  	for (int i = 1; i <= 10; i++ ) {
  		add(list, name + nf.format(i));
  	}
  	return list;
  }
  
	public List<NameValuePair> getNumerics() {
		return numberList = buildList("Nu");
	}
	public List<NameValuePair> getStrings() {
		return stringList = buildList("Vc");
	}
	public List<NameValuePair> getDates() {
		return dateList = buildList("Ts");
	}
	public List<NameValuePair> getUserNumbers() {
		return userNumberList = buildList("Unu");
	}

	public List<NameValuePair> getUserStrings() {
		return userStringList = buildList("Uvc");
	}

	public List<NameValuePair> getUserDates() {
		return userDateList = buildList("Uts");
	}
}
