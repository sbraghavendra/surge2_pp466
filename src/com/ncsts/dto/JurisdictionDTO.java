package com.ncsts.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.ForeignKey;
import org.springframework.beans.BeanUtils;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class JurisdictionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    private Long jurisdictionId;
	private String geocode;
    private String state;
    private String county;
    private String city;
	private String reportingCity;
	private String reportingCityFips;
	private String stj1Name;
	private String stj2Name;
	private String stj3Name;
	private String stj4Name;
	private String stj5Name;
    private String zip;
    private String zipplus4;
    private String inOut;
	private Date effectiveDate;
	private Date expirationDate;
    private String customFlag;
	private String modifiedFlag;
    private String description;
    private String clientGeocode;
    private String compGeocode;
	private String countryNexusindCode;
	private String stateNexusindCode;
	private String countyNexusindCode;
	private String cityNexusindCode;
	private String stj1NexusindCode;
	private String stj2NexusindCode;
	private String stj3NexusindCode;
	private String stj4NexusindCode;
	private String stj5NexusindCode;
	private String stj1LocalCode;
	private String stj2LocalCode;
	private String stj3LocalCode;
	private String stj4LocalCode;
	private String stj5LocalCode;
    private String country;
	private String updateUserId;
    private Date updateTimestamp;

	// Default constructor
	public JurisdictionDTO() {
		// Do nothing
	}

	public Long getJurisdictionId() {
        return this.jurisdictionId;
    }
    
    public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
    }

	public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
        this.geocode = geocode;
    }

    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
        this.county = county;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getZipplus4() {
        return this.zipplus4;
    }
    
    public void setZipplus4(String zipplus4) {
        this.zipplus4 = zipplus4;
    }

    public String getInOut() {
        return this.inOut;
    }
    
    public void setInOut(String inOut) {
        this.inOut = inOut;
    }

    public String getCustomFlag() {
        return this.customFlag;
    }
    
    public void setCustomFlag(String customFlag) {
        this.customFlag = customFlag;
    }

	public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getClientGeocode() {
        return this.clientGeocode;
    }
    
    public void setClientGeocode(String clientGeocode) {
        this.clientGeocode = clientGeocode;
    }

    public String getCompGeocode() {
        return this.compGeocode;
    }
    
    public void setCompGeocode(String compGeocode) {
        this.compGeocode = compGeocode;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
	public String getReportingCity() {
		return reportingCity;
	}

	public void setReportingCity(String reportingCity) {
		this.reportingCity = reportingCity;
	}

	public String getReportingCityFips() {
		return reportingCityFips;
	}

	public void setReportingCityFips(String reportingCityFips) {
		this.reportingCityFips = reportingCityFips;
	}

	public String getStj1Name() {
		return stj1Name;
	}

	public void setStj1Name(String stj1Name) {
		this.stj1Name = stj1Name;
	}

	public String getStj2Name() {
		return stj2Name;
	}

	public void setStj2Name(String stj2Name) {
		this.stj2Name = stj2Name;
	}

	public String getStj3Name() {
		return stj3Name;
	}

	public void setStj3Name(String stj3Name) {
		this.stj3Name = stj3Name;
	}

	public String getStj4Name() {
		return stj4Name;
	}

	public void setStj4Name(String stj4Name) {
		this.stj4Name = stj4Name;
	}

	public String getStj5Name() {
		return stj5Name;
	}

	public void setStj5Name(String stj5Name) {
		this.stj5Name = stj5Name;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}

	public String getCountryNexusindCode() {
		return countryNexusindCode;
	}

	public void setCountryNexusindCode(String countryNexusindCode) {
		this.countryNexusindCode = countryNexusindCode;
	}

	public String getStateNexusindCode() {
		return stateNexusindCode;
	}

	public void setStateNexusindCode(String stateNexusindCode) {
		this.stateNexusindCode = stateNexusindCode;
	}

	public String getCountyNexusindCode() {
		return countyNexusindCode;
	}

	public void setCountyNexusindCode(String countyNexusindCode) {
		this.countyNexusindCode = countyNexusindCode;
	}

	public String getCityNexusindCode() {
		return cityNexusindCode;
	}

	public void setCityNexusindCode(String cityNexusindCode) {
		this.cityNexusindCode = cityNexusindCode;
	}

	public String getStj1NexusindCode() {
		return stj1NexusindCode;
	}

	public void setStj1NexusindCode(String stj1NexusindCode) {
		this.stj1NexusindCode = stj1NexusindCode;
	}

	public String getStj2NexusindCode() {
		return stj2NexusindCode;
	}

	public void setStj2NexusindCode(String stj2NexusindCode) {
		this.stj2NexusindCode = stj2NexusindCode;
	}

	public String getStj3NexusindCode() {
		return stj3NexusindCode;
	}

	public void setStj3NexusindCode(String stj3NexusindCode) {
		this.stj3NexusindCode = stj3NexusindCode;
	}

	public String getStj4NexusindCode() {
		return stj4NexusindCode;
	}

	public void setStj4NexusindCode(String stj4NexusindCode) {
		this.stj4NexusindCode = stj4NexusindCode;
	}

	public String getStj5NexusindCode() {
		return stj5NexusindCode;
	}

	public void setStj5NexusindCode(String stj5NexusindCode) {
		this.stj5NexusindCode = stj5NexusindCode;
	}
	
	public String getStj1LocalCode() {
		return stj1LocalCode;
	}

	public void setStj1LocalCode(String stj1LocalCode) {
		this.stj1LocalCode = stj1LocalCode;
	}

	public String getStj2LocalCode() {
		return stj2LocalCode;
	}

	public void setStj2LocalCode(String stj2LocalCode) {
		this.stj2LocalCode = stj2LocalCode;
	}

	public String getStj3LocalCode() {
		return stj3LocalCode;
	}

	public void setStj3LocalCode(String stj3LocalCode) {
		this.stj3LocalCode = stj3LocalCode;
	}

	public String getStj4LocalCode() {
		return stj4LocalCode;
	}

	public void setStj4LocalCode(String stj4LocalCode) {
		this.stj4LocalCode = stj4LocalCode;
	}

	public String getStj5LocalCode() {
		return stj5LocalCode;
	}

	public void setStj5LocalCode(String stj5LocalCode) {
		this.stj5LocalCode = stj5LocalCode;
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	
	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}
