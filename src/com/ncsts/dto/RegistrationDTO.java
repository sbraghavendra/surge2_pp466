package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

public class RegistrationDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
	private Long registrationId;
	private Long entityId;
	private String regCountryCode;
	private String regStateCode;
	private String regCounty;
	private String regCity;
	private String regStj;

    private String updateUserId;
    private Date updateTimestamp; 
        
	public RegistrationDTO() {   	
    }
	
	public Long getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(Long registrationId) {
		this.registrationId = registrationId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getRegCountryCode() {
		return regCountryCode;
	}

	public void setRegCountryCode(String regCountryCode) {
		this.regCountryCode = regCountryCode;
	}
	
	public String getRegStateCode() {
		return regStateCode;
	}

	public void setRegStateCode(String regStateCode) {
		this.regStateCode = regStateCode;
	}
	
	public String getRegCounty() {
		return regCounty;
	}

	public void setRegCounty(String regCounty) {
		this.regCounty = regCounty;
	}
	
	public String getRegCity() {
		return regCity;
	}

	public void setRegCity(String regCity) {
		this.regCity = regCity;
	}
	
	public String getRegStj() {
		return regStj;
	}

	public void setRegStj(String regStj) {
		this.regStj = regStj;
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}

