package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.UserEntityPK;

public class UserEntityDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	UserEntityPK userEntityPK; 
	
	private String userName;
	
	private String roleCode;
	
	private String roleName;
	
    private String updateUserId;
	
    private Date updateTimestamp; 
    
    private EntityItem entity;
    
    private EntityLevel entityLevel;
	
    public UserEntityDTO() { 
    	
    }
	
	public UserEntityPK getUserEntityPK() {
		return userEntityPK;
	}

	public void setUserEntityPK(UserEntityPK userEntityPK) {
		this.userEntityPK = userEntityPK;
	}

	public String getUserCode() {
		return userEntityPK.getUserCode();
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getEntityId() {
		return userEntityPK.getEntityId();
	}	

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public EntityItem getEntity() {
		return entity;
	}

	public EntityLevel getEntityLevel() {
		return entityLevel;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public void setEntity(EntityItem entity) {
		this.entity = entity;
	}

	public void setEntityLevel(EntityLevel entityLevel) {
		this.entityLevel = entityLevel;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	
}


