/**
 * 
 */
package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DriverNamesPK;


/**
 * @author Anand
 *
 */
public class DriverNamesDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getInstance().getLogger(DriverNamesDTO.class);

	private String drvNamesCode;
	
	private String transDtlColName;
	
	private String matrixColName;
	
	private DriverNamesPK driverNamesPK;
	
	// Lookup value from transDtlColName to TB_ENTITY_LEVEL.description
	private String description;

	private String businessUnitFlag;

	private String mandatoryFlag; 

	private String nullDriverFlag;

	private String wildcardFlag; 

	private String rangeFlag;
	
	private String toNumberFlag;
	
	private String activeFlag;
	
	private Double driverWeight;
	
	private String updateUserId;
	
	private Date updateTimestamp;
	
	private Long driverCount;
	
	private String tableName;//Fixed for issue 0004622
	
	private static final double DRIVER_WEIGHT_TAX = 30.0;
	private static final double DRIVER_WEIGHT_LOCATION = 10.0;
	private static final double DRIVER_WEIGHT_ALLOCATION = 30.0;
	private static final double DRIVER_WEIGHT_EXTRACT = 10.0;

	public String getDrvNamesCode() {
		return drvNamesCode;
	}

	public void setDrvNamesCode(String drvNamesCode) {
		this.drvNamesCode = drvNamesCode;

		calculateDriverWeight();
	}

	public String getTransDtlColName() {
		return transDtlColName;
	}

	public void setTransDtlColName(String transDtlColName) {
		this.transDtlColName = transDtlColName;
	}

	public String getMatrixColName() {
		return matrixColName;
	}

	public void setMatrixColName(String matrixColName) {
		this.matrixColName = matrixColName;
	}

	public DriverNamesPK getDriverNamesPK() {
		return driverNamesPK;
	}

	public void setDriverNamesPK(DriverNamesPK driverNamesPK) {
		this.driverNamesPK = driverNamesPK;
		
		calculateDriverWeight();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	// BUSINESS_UNIT_FLAG properties
	public String getBusinessUnitFlag() {
		return businessUnitFlag;
	}
	
	public void setBusinessUnitFlag(String businessUnitFlag) {
		this.businessUnitFlag = businessUnitFlag;
	}
	
	public Boolean getBusinessUnitFlagBoolean() {
		return (businessUnitFlag == null) ? false : businessUnitFlag.equals("1");
	}
	
	public void setBusinessUnitFlagBoolean(Boolean businessUnitFlagBoolean) {
		setBusinessUnitFlag((businessUnitFlagBoolean == null) ? null : (businessUnitFlagBoolean ? "1" : "0"));
	}

	// MANDATORY_FLAG properties
	public String getMandatoryFlag() {
		return mandatoryFlag;
	}
	
	public void setMandatoryFlag(String mandatoryFlag) {
		this.mandatoryFlag = mandatoryFlag;
	}

	public Boolean getMandatoryFlagBoolean() {
		return (mandatoryFlag == null) ? null : mandatoryFlag.equals("1");
	}
	
	public void setMandatoryFlagBoolean(Boolean mandatoryFlagBoolean) {
		setMandatoryFlag((mandatoryFlagBoolean == null) ? null : (mandatoryFlagBoolean ? "1" : "0"));
	}

	// NULL_DRIVER_FLAG properties
	public String getNullDriverFlag() {
		return nullDriverFlag;
	}
	
	public void setNullDriverFlag(String nullDriverFlag) {
		this.nullDriverFlag = nullDriverFlag;
	}

	public Boolean getNullDriverFlagBoolean() {
		return (nullDriverFlag == null) ? null : nullDriverFlag.equals("1");
	}
	
	public void setNullDriverFlagBoolean(Boolean nullDriverFlagBoolean) {
		setNullDriverFlag((nullDriverFlagBoolean == null) ? null : (nullDriverFlagBoolean ? "1" : "0"));
	}

	// WILDCARD_FLAG properties
	public String getWildcardFlag() {
		return wildcardFlag;
	}
	
	public void setWildcardFlag(String wildcardFlag) {
		this.wildcardFlag = wildcardFlag;
	}
	
	public Boolean getWildcardFlagBoolean() {
		return (wildcardFlag == null)? null : wildcardFlag.equals("1");
	}
	
	public void setWildcardFlagBoolean(Boolean wildcardFlagBoolean) {
		setWildcardFlag((wildcardFlagBoolean == null) ? null : (wildcardFlagBoolean ? "1" : "0"));
	}
	
	// RANGE_FLAG properties
	public String getRangeFlag() {
		return rangeFlag;
	}
	
	public void setRangeFlag(String rangeFlag) {
		this.rangeFlag = rangeFlag;
	}

	public Boolean getRangeFlagBoolean() {
		return (rangeFlag == null)? null : rangeFlag.equals("1");
	}
	
	public void setRangeFlagBoolean(Boolean rangeFlagBoolean) {
		setRangeFlag((rangeFlagBoolean == null) ? null : (rangeFlagBoolean ? "1" : "0"));
	}
	
	// TO_NUMBER_FLAG properties
	public String getToNumberFlag() {
		return toNumberFlag;
	}
	
	public void setToNumberFlag(String toNumberFlag) {
		this.toNumberFlag = toNumberFlag;
	}

	public Boolean getToNumberFlagBoolean() {
		return (toNumberFlag == null)? null : toNumberFlag.equals("1");
	}
	
	public void setToNumberFlagBoolean(Boolean toNumberFlagBoolean) {
		setToNumberFlag((toNumberFlagBoolean == null) ? null : (toNumberFlagBoolean ? "1" : "0"));
	}
	
	// ACTIVE_FLAG properties
	public String getActiveFlag() {
		return activeFlag;
	}
	
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Boolean getActiveFlagBoolean() {
		return (activeFlag == null)? null : activeFlag.equals("1");
	}
	
	public void setActiveFlagBoolean(Boolean toActiveFlagBoolean) {
		setActiveFlag((toActiveFlagBoolean == null) ? null : (toActiveFlagBoolean ? "1" : "0"));
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public Double getDriverWeight() {
		if (driverWeight == null) {
			calculateDriverWeight();
		}
		
		return driverWeight;
	}
	
	public void setDriverWeight(Double driverWeight) {
		this.driverWeight = driverWeight;
	}
	
	private void calculateDriverWeight() {
		driverWeight = null;

		if ((driverNamesPK != null) && (driverNamesPK.getDriverId() != null) && (drvNamesCode != null)) {
			try {
				if ("T".equalsIgnoreCase(drvNamesCode)) {
					driverWeight = java.lang.Math.pow(2.0, DRIVER_WEIGHT_TAX - driverNamesPK.getDriverId().doubleValue());
				} else if ("L".equalsIgnoreCase(drvNamesCode)) {
					driverWeight = java.lang.Math.pow(2.0, DRIVER_WEIGHT_LOCATION - driverNamesPK.getDriverId().doubleValue());
				} else if ("A".equalsIgnoreCase(drvNamesCode)) {
					driverWeight = java.lang.Math.pow(2.0, DRIVER_WEIGHT_ALLOCATION - driverNamesPK.getDriverId().doubleValue());
				} else if ("E".equalsIgnoreCase(drvNamesCode)) {
					driverWeight = java.lang.Math.pow(2.0, DRIVER_WEIGHT_EXTRACT - driverNamesPK.getDriverId().doubleValue());
				}else if ("GSB".equalsIgnoreCase(drvNamesCode)) {
					driverWeight = java.lang.Math.pow(2.0, DRIVER_WEIGHT_TAX - driverNamesPK.getDriverId().doubleValue());
				}
			} catch (NumberFormatException e) {
				logger.warn("Failed to calculate driver weight for " + driverNamesPK.getDriverId());
			}
		}
	}

	public Long getDriverCount() {
		return driverCount;
	}

	public void setDriverCount(Long driverCount) {
		this.driverCount = driverCount;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	
	
}
