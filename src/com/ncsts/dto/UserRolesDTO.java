package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class UserRolesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer roleId;
    private String roleName;   
 
    private Integer passwordHistory;
    private Integer invalidAttempts;
    private Integer notificationDays;
    private Integer expirationDays;
    private Integer admin; 
    
    
    private boolean adminDisplay; 
    
    
    public boolean getAdminDisplay() {
		if (this.admin!=null && this.admin.intValue()==1) {this.adminDisplay = true;}
		else{this.adminDisplay = false;}
		
		return adminDisplay;
	}
    
    public void setAdminDisplay(boolean adminDisplay) {
		this.adminDisplay = adminDisplay;
		if (adminDisplay) {this.admin = 1;} 
		else {this.admin = 0;}
	} 
	
    public UserRolesDTO() { 
    	
    }

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public Integer getPasswordHistory() {
		return passwordHistory;
	}

	public void setPasswordHistory(Integer passwordHistory) {
		this.passwordHistory = passwordHistory;
	}
	
	public Integer getInvalidAttempts() {
		return invalidAttempts;
	}

	public void setInvalidAttempts(Integer invalidAttempts) {
		this.invalidAttempts = invalidAttempts;
	}
	
	public Integer getNotificationDays() {
		return notificationDays;
	}

	public void setNotificationDays(Integer notificationDays) {
		this.notificationDays = notificationDays;
	}

	public Integer getExpirationDays() {
		return expirationDays;
	}

	public void setExpirationDays(Integer expirationDays) {
		this.expirationDays = expirationDays;
	}
	
	public Integer getAdmin() {
		return admin;
	}

	public void setAdmin(Integer admin) {
		this.admin = admin;
	}

    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("roleId :\t").append(roleId);
    	sb.append("roleName :\t").append(roleName);

    	return sb.toString();
    }
    
}







