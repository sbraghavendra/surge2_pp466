package com.ncsts.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;

public class TaxHolidayDTO implements Serializable {
	private static final long serialVersionUID = 1318470152133640111L;	
	
	private String taxHolidayCode;  
    private String country;     
    private String state;     
    private String description;   
    private String notes;    
    private Date effectiveDate;
    private Date expirationDate;
    private String executeFlag;
    private String updateUserId;
    private Date updateTimestamp; 
    private String taxcodeTypeCode;  
    private String overrideTaxtypeCode;    
    private String ratetypeCode;  
    private BigDecimal taxableThresholdAmt;
    private BigDecimal taxableLimitationAmt;
    private BigDecimal taxLimitationAmt; 
    private BigDecimal capAmt;
    private BigDecimal baseChangePct;
    private BigDecimal specialRate;
    
    public TaxHolidayDTO() { 
    }
    
    public String getTaxHolidayCode() {
        return this.taxHolidayCode;
    }
    
    public void setTaxHolidayCode(String taxHolidayCode) {
    	this.taxHolidayCode = taxHolidayCode;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }    
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }      
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }    
    
    public String getNotes() {
        return this.notes;
    }
    
    public void setNotes(String notes) {
    	this.notes = notes;
    }  
    
    public String getExecuteFlag() {
		return executeFlag;
	}

	public void setExecuteFlag(String executeFlag) {
		this.executeFlag= executeFlag;
	}

	public Boolean getExecuteBooleanFlag() {
		return (executeFlag == null)? null:executeFlag.equals("1");
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}  
	
	
	public String getTaxcodeTypeCode() {
        return this.taxcodeTypeCode;
    }
    
    public void setTaxcodeTypeCode(String taxcodeTypeCode) {
    	this.taxcodeTypeCode = taxcodeTypeCode;
    }
    
    public String getOverrideTaxtypeCode() {
        return this.overrideTaxtypeCode;
    }
    
    public void setOverrideTaxtypeCode(String overrideTaxtypeCode) {
    	this.overrideTaxtypeCode = overrideTaxtypeCode;
    }
    
    public String getRatetypeCode() {
        return this.ratetypeCode;
    }
    
    public void setRatetypeCode(String ratetypeCode) {
    	this.ratetypeCode = ratetypeCode;
    }
    
    public BigDecimal getTaxableThresholdAmt() {
		return this.taxableThresholdAmt;
	}

	public void setTaxableThresholdAmt(BigDecimal taxableThresholdAmt) {
		this.taxableThresholdAmt = taxableThresholdAmt;
	}
	
	public BigDecimal getTaxableLimitationAmt() {
		return taxableLimitationAmt;
	}

	public void setTaxableLimitationAmt(BigDecimal taxableLimitationAmt) {
		this.taxableLimitationAmt = taxableLimitationAmt;
	}
	
	public BigDecimal getTaxLimitationAmt() {
		return this.taxLimitationAmt;
	}

	public void setTaxLimitationAmt(BigDecimal taxLimitationAmt) {
		this.taxLimitationAmt = taxLimitationAmt;
	}
	
	public BigDecimal getCapAmt() {
		return this.capAmt;
	}

	public void setCapAmt(BigDecimal capAmt) {
		this.capAmt = capAmt;
	}
	
	public BigDecimal getBaseChangePct() {
		return this.baseChangePct;
	}

	public void setBaseChangePct(BigDecimal baseChangePct) {
		this.baseChangePct = baseChangePct;
	}
	
	public BigDecimal getSpecialRate() {
		return this.specialRate;
	}

	public void setSpecialRate(BigDecimal specialRate) {
		this.specialRate = specialRate;
	}
	
	/**
	 * 
	 */
	public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxHolidayDTO)) {
            return false;
        }

        final TaxHolidayDTO revision = (TaxHolidayDTO)other;

        String c1 = getTaxHolidayCode();
        String c2 = revision.getTaxHolidayCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getTaxHolidayCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.taxHolidayCode + "::" + this.description;
    }
    
}




