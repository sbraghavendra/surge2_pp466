package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import com.ncsts.domain.DataDefinitionColumnPK;

/**
 * 
 * @author Muneer Basha
 *
 */
	public class DataDefinitionColumnDTO implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		DataDefinitionColumnPK dataDefinitionColumnPK;
		
		private String columnName;
		private String dataType;
	    private Long datalength;
	    private String description;
	    private String abbrDesc;
	    private String descColumnName;
	    private String definition;
	    private String mapFlag;	
	    private String minimumViewFlag;	
	    private String updateUserId;		
	    private Date updateTimestamp;
	    
	    private boolean mapFlagDisplay;
	    private boolean minimumViewFlagDisplay;
	    
	    public boolean getMapFlagDisplay() {
			if (this.mapFlag!=null && this.mapFlag.equalsIgnoreCase("1")) {this.mapFlagDisplay = true;}
			else{this.mapFlagDisplay = false;}
			
			return mapFlagDisplay;
		}

	    public void setMapFlagDisplay(boolean mapFlagDisplay) {
			this.mapFlagDisplay = mapFlagDisplay;
			if (mapFlagDisplay) {this.mapFlag = "1";} 
			else {this.mapFlag = "0";}
		}
   
	    public boolean getMinimumViewFlagDisplay() {
			if (this.minimumViewFlag!=null && this.minimumViewFlag.equalsIgnoreCase("1")) {this.minimumViewFlagDisplay = true;}
			else{this.minimumViewFlagDisplay = false;}
			
			return minimumViewFlagDisplay;
		}

	    public void setMinimumViewFlagDisplay(boolean minimumViewFlagDisplay) {
			this.minimumViewFlagDisplay = minimumViewFlagDisplay;
			if (minimumViewFlagDisplay) {this.minimumViewFlag = "1";} 
			else {this.minimumViewFlag = "0";}
		}
	    
	    public String getTableName() {
			return dataDefinitionColumnPK.getTableName();
		}
		
		public DataDefinitionColumnPK getDataDefinitionColumnPK() {
			return dataDefinitionColumnPK;
		}
		public void setDataDefinitionColumnPK(DataDefinitionColumnPK dataDefinitionColumnPK) {
			this.dataDefinitionColumnPK = dataDefinitionColumnPK;
		}
	    
		public String getDataType() {
			return dataType;
		}
		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
				
		public Long getDatalength() {
			return datalength;
		}
		public void setDatalength(Long datalength) {
			this.datalength = datalength;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getAbbrDesc() {
			return abbrDesc;
		}
		public void setAbbrDesc(String abbrDesc) {
			this.abbrDesc = abbrDesc;
		}
		public String getDescColumnName() {
			return descColumnName;
		}
		public void setDescColumnName(String descColumnName) {
			this.descColumnName = descColumnName;
		}
		public String getDefinition() {
			return definition;
		}
		public void setDefinition(String definition) {
			this.definition = definition;
		}
		public String getMapFlag() {
			return mapFlag;
		}
		public void setMapFlag(String mapFlag) {
			this.mapFlag = mapFlag;
		}
		public String getMinimumViewFlag() {
			return minimumViewFlag;
		}
		public void setMinimumViewFlag(String minimumViewFlag) {
			this.minimumViewFlag = minimumViewFlag;
		}
		public String getUpdateUserId() {
			return updateUserId;
		}
		public void setUpdateUserId(String updateUserId) {
			this.updateUserId = updateUserId;
		}
		public Date getUpdateTimestamp() {
			return updateTimestamp;
		}
		public void setUpdateTimestamp(Date updateTimestamp) {
			this.updateTimestamp = updateTimestamp;
		}
		public String getColumnName() {
			return columnName;
		}
		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}
				
	}

