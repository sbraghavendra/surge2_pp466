package com.ncsts.dto;

import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.persistence.Column;
import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/*General storage for keeping purchase transaction processing variables*/

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TransientMicroApiTransaction extends TransientPurchaseTransaction{
}
