package com.ncsts.dto;

import java.io.Serializable;

public class MatrixDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long sysMinDriver;
	private Long sysMinLocationDriver;
	private Long sysMinAllocationDriver;	
	private String sysLimitedThreshold;
	private String sysFullThreshold;
	private String sysLimitedThresholdPages;
	private String sysFullThresholdPages;
	private String sysDriverRef;
	private String userTransInd;
	private String userTaxCode;
	private String userCountry;
	private String sysTaxCodeDropDownThreshold;

	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public Long getSysMinDriver() {
		return sysMinDriver;
	}
	public void setSysMinDriver(Long sysMinDriver) {
		this.sysMinDriver = sysMinDriver;
	}
	public String getSysLimitedThreshold() {
		return sysLimitedThreshold;
	}
	public void setSysLimitedThreshold(String sysLimitedThreshold) {
		this.sysLimitedThreshold = sysLimitedThreshold;
	}
	public String getSysFullThreshold() {
		return sysFullThreshold;
	}
	public void setSysFullThreshold(String sysFullThreshold) {
		this.sysFullThreshold = sysFullThreshold;
	}
	public String getSysLimitedThresholdPages() {
		return sysLimitedThresholdPages;
	}
	public void setSysLimitedThresholdPages(String sysLimitedThresholdPages) {
		this.sysLimitedThresholdPages = sysLimitedThresholdPages;
	}
	public String getSysFullThresholdPages() {
		return sysFullThresholdPages;
	}
	public void setSysFullThresholdPages(String sysFullThresholdPages) {
		this.sysFullThresholdPages = sysFullThresholdPages;
	}
	public String getSysDriverRef() {
		return sysDriverRef;
	}
	public void setSysDriverRef(String sysDriverRef) {
		this.sysDriverRef = sysDriverRef;
	}
	public String getUserTransInd() {
		return userTransInd;
	}
	public void setUserTransInd(String userTransInd) {
		this.userTransInd = userTransInd;
	}
	public String getUserTaxCode() {
		return userTaxCode;
	}
	public void setUserTaxCode(String userTaxCode) {
		this.userTaxCode = userTaxCode;
	}
	public Long getSysMinLocationDriver() {
		return sysMinLocationDriver;
	}
	public void setSysMinLocationDriver(Long sysMinLocationDriver) {
		this.sysMinLocationDriver = sysMinLocationDriver;
	}
	public Long getSysMinAllocationDriver() {
		return sysMinAllocationDriver;
	}
	public void setSysMinAllocationDriver(Long sysMinAllocationDriver) {
		this.sysMinAllocationDriver = sysMinAllocationDriver;
	}
	
	public String getSysTaxCodeDropDownThreshold() {
		return sysTaxCodeDropDownThreshold;
	}
	public void setSysTaxCodeDropDownThreshold(String sysTaxCodeDropDownThreshold) {
		this.sysTaxCodeDropDownThreshold = sysTaxCodeDropDownThreshold;
	}
}
