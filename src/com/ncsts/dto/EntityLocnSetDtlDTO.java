package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

public class EntityLocnSetDtlDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long entityLocnSetDtlId;
	private Long entityLocnSetId;
	private String description;
	private Long sfEntityId;
	private Long pooEntityId;
	private Long poaEntityId;
    private Date effectiveDate;

    private String updateUserId;
    private Date updateTimestamp; 
        
	public EntityLocnSetDtlDTO() {   	
    }

	public Long getEntityLocnSetDtlId() {
		return entityLocnSetDtlId;
	}

	public void setEntityLocnSetDtlId(Long entityLocnSetDtlId) {
		this.entityLocnSetDtlId = entityLocnSetDtlId;
	}

	public Long getEntityLocnSetId() {
		return entityLocnSetId;
	}

	public void setEntityLocnSetId(Long entityLocnSetId) {
		this.entityLocnSetId = entityLocnSetId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSfEntityId() {
		return sfEntityId;
	}

	public void setSfEntityId(Long sfEntityId) {
		this.sfEntityId = sfEntityId;
	}

	public Long getPooEntityId() {
		return pooEntityId;
	}

	public void setPooEntityId(Long pooEntityId) {
		this.pooEntityId = pooEntityId;
	}
	
	public Long getPoaEntityId() {
		return poaEntityId;
	}

	public void setPoaEntityId(Long poaEntityId) {
		this.poaEntityId = poaEntityId;
	}
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}