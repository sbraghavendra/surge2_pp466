package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Paul Govindan
 *
 */

public class EntityLevelDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    private Long entityLevelId;
      
    private String transDetailColumnName;   
       
    private String description;      
    
    private String updateUserId;
	
    private Date updateTimestamp;   
    
    private String businessUnitFlag;
	
    public EntityLevelDTO() { 
    	
    }   

	
    public String getBusinessUnitFlag() {
		return businessUnitFlag;
	}

	public void setBusinessUnitFlag(String businessUnitFlag) {
		this.businessUnitFlag = businessUnitFlag;
	}
    
    public String getTransDetailColumnName() {
        return this.transDetailColumnName;
    }
    
    public void setTransDetailColumnName(String entityName) {
    	this.transDetailColumnName = entityName;
    }      
    
    public Long getEntityLevelId() {
        return this.entityLevelId;
    }
    
    public void setEntityLevelId(Long entityLevelId) {
    	this.entityLevelId = entityLevelId;
    }    
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }         
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}    
    
}







