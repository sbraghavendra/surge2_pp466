package com.ncsts.dto;

import java.io.Serializable;

public class CompanyDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String logoUrl;
	private String stsTimeZone;
	private String defaultTimeZone;
	private boolean stsDST;
	private boolean defaultDST;
	private String version;
	private boolean aspCustomer;
	private boolean pcoCustomer;
	private String stsAdminTimeZone;
	private boolean stsAdminDST;
	private boolean splashscreen;
	private boolean sound;
	private String userTimeZone;
	private boolean userDST;
	private String gridLayout;
	private String tbSpace;
	private String autoAddNexus;
	private String autoAddNexusNoRights;
	private String defaultNexusType;
	private boolean ratepointEnabled;
	private String ratepointServer;
	private String ratepointUserId;
	private String ratepointPassword;
	private boolean controlPointEnabled;
	private String controlPointServer;
	private String controlPointUserId;
	private String controlPointPassword;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getStsTimeZone() {
		return stsTimeZone;
	}
	public void setStsTimeZone(String stsTimeZone) {
		this.stsTimeZone = stsTimeZone;
	}
	public String getDefaultTimeZone() {
		return defaultTimeZone;
	}
	public void setDefaultTimeZone(String defaultTimeZone) {
		this.defaultTimeZone = defaultTimeZone;
	}
	public boolean isStsDST() {
		return stsDST;
	}
	public void setStsDST(boolean stsDST) {
		this.stsDST = stsDST;
	}
	public boolean isDefaultDST() {
		return defaultDST;
	}
	public void setDefaultDST(boolean defaultDST) {
		this.defaultDST = defaultDST;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public boolean isAspCustomer() {
		return aspCustomer;
	}
	public void setAspCustomer(boolean aspCustomer) {
		this.aspCustomer = aspCustomer;
	}
	
	
	public boolean isPcoCustomer() {
		return pcoCustomer;
	}
	public void setPcoCustomer(boolean pcoCustomer) {
		this.pcoCustomer = pcoCustomer;
	}
	public String getStsAdminTimeZone() {
		return stsAdminTimeZone;
	}
	public void setStsAdminTimeZone(String stsAdminTimeZone) {
		this.stsAdminTimeZone = stsAdminTimeZone;
	}
	public boolean isStsAdminDST() {
		return stsAdminDST;
	}
	public void setStsAdminDST(boolean stsAdminDST) {
		this.stsAdminDST = stsAdminDST;
	}
	public boolean isSplashscreen() {
		return splashscreen;
	}
	public void setSplashscreen(boolean splashscreen) {
		this.splashscreen = splashscreen;
	}
	public boolean isSound() {
		return sound;
	}
	public void setSound(boolean sound) {
		this.sound = sound;
	}
	public String getUserTimeZone() {
		return userTimeZone;
	}
	public void setUserTimeZone(String userTimeZone) {
		this.userTimeZone = userTimeZone;
	}
	public String getGridLayout() {
		return gridLayout;
	}
	public void setGridLayout(String gridLayout) {
		this.gridLayout = gridLayout;
	}
	public boolean isUserDST() {
		return userDST;
	}
	public void setUserDST(boolean userDST) {
		this.userDST = userDST;
	}
	public String getTbSpace() {
		return tbSpace;
	}
	public void setTbSpace(String tbSpace) {
		this.tbSpace = tbSpace;
	}
	public String getAutoAddNexus() {
		return autoAddNexus;
	}
	public void setAutoAddNexus(String autoAddNexus) {
		this.autoAddNexus = autoAddNexus;
	}
	public String getAutoAddNexusNoRights() {
		return autoAddNexusNoRights;
	}
	public void setAutoAddNexusNoRights(String autoAddNexusNoRights) {
		this.autoAddNexusNoRights = autoAddNexusNoRights;
	}
	public String getDefaultNexusType() {
		return defaultNexusType;
	}
	public void setDefaultNexusType(String defaultNexusType) {
		this.defaultNexusType = defaultNexusType;
	}
	
	public String getRatepointServer() {
		return ratepointServer;
	}
	public void setRatepointServer(String ratepointServer) {
		this.ratepointServer = ratepointServer;
	}
	public String getRatepointUserId() {
		return ratepointUserId;
	}
	public void setRatepointUserId(String ratepointUserId) {
		this.ratepointUserId = ratepointUserId;
	}
	public String getRatepointPassword() {
		return ratepointPassword;
	}
	public void setRatepointPassword(String ratepointPassword) {
		this.ratepointPassword = ratepointPassword;
	}

	public boolean isRatepointEnabled() {
		return ratepointEnabled;
	}
	public void setRatepointEnabled(boolean ratepointEnabled) {
		this.ratepointEnabled = ratepointEnabled;
	}
	public boolean isControlPointEnabled() {
		return controlPointEnabled;
	}
	public void setControlPointEnabled(boolean controlPointEnabled) {
		this.controlPointEnabled = controlPointEnabled;
	}
	public String getControlPointServer() {
		return controlPointServer;
	}
	public void setControlPointServer(String controlPointServer) {
		this.controlPointServer = controlPointServer;
	}
	public String getControlPointUserId() {
		return controlPointUserId;
	}
	public void setControlPointUserId(String controlPointUserId) {
		this.controlPointUserId = controlPointUserId;
	}
	public String getControlPointPassword() {
		return controlPointPassword;
	}
	public void setControlPointPassword(String controlPointPassword) {
		this.controlPointPassword = controlPointPassword;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
