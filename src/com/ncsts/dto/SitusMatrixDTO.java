package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class SitusMatrixDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
	private Long situsMatrixId; 
    private String transactionTypeCode;   
    private String ratetypeCode;   
    private String methodDeliveryCode;  
	private String situsCountryCode;
	private String situsStateCode;
	private String jurLevel;
    private String customFlag; 
    private String driver01;
    private String driver02;
    private String driver03;
    private String driver04;
    private String driver05;
    private String driver06;
    private String driver07;
    private String driver08;
    private String driver09;
    private String driver10;
    private String driver11;
    private String driver12;
    private String driver13;
    private String driver14;
    private String driver15;
    private String driver16;
	private String driver17;
	private String driver18;
	private String driver19;
	private String driver20;
	private String driver21;
	private String driver22;
	private String driver23;
	private String driver24;
	private String driver25;
	private String driver26;
	private String driver27;
	private String driver28;
	private String driver29;
	private String driver30;
    private Long binaryWeight;
    private Date effectiveDate;
    private Date expirationDate;
	private String primarySitusCode;
	private String primaryTaxtypeCode;
	private String secondarySitusCode;
	private String secondaryTaxtypeCode;  
	private String comments;
    private String allLevelsFlag; 
    private String activeFlag; 
    private String updateUserId;
    private Date updateTimestamp; 

	public SitusMatrixDTO() {   	
    }
	
	public Long getSitusMatrixId() {
        return this.situsMatrixId;
    }
    
    public void setSitusMatrixId(Long situsMatrixId) {
    	this.situsMatrixId = situsMatrixId;
    }
    
    public String getTransactionTypeCode() {
        return this.transactionTypeCode;
    }
    
    public void setTransactionTypeCode(String transactionTypeCode) {
    	this.transactionTypeCode = transactionTypeCode;
    }    
    
    public String getRatetypeCode() {
        return this.ratetypeCode;
    }
    
    public void setRatetypeCode(String ratetypeCode) {
    	this.ratetypeCode = ratetypeCode;
    }      
    
    public String getMethodDeliveryCode() {
        return this.methodDeliveryCode;
    }
    
    public void setMethodDeliveryCode(String methodDeliveryCode) {
    	this.methodDeliveryCode = methodDeliveryCode;
    }   
    
    public String getSitusCountryCode() {
		return situsCountryCode;
	}

	public void setSitusCountryCode(String situsCountryCode) {
		this.situsCountryCode = situsCountryCode;
	}

	public String getSitusStateCode() {
		return situsStateCode;
	}

	public void setSitusStateCode(String situsStateCode) {
		this.situsStateCode = situsStateCode;
	}
	
	public String getJurLevel() {
		return jurLevel;
	}

	public void setJurLevel(String jurLevel) {
		this.jurLevel = jurLevel;
	}
    
    public String getCustomFlag() {
        return this.customFlag;
    }
    
    public void setCustomFlag(String customFlag) {
    	this.customFlag = customFlag;
    }  
    
    public Boolean getCustomBooleanFlag() {
    	return "1".equals(this.customFlag);
	}

	public void setCustomBooleanFlag(Boolean customFlag) {
		this.customFlag = customFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getAllLevelsFlag() {
        return this.allLevelsFlag;
    }
    
    public void setAllLevelsFlag(String allLevelsFlag) {
    	this.allLevelsFlag = allLevelsFlag;
    }  
    
    public Boolean getAllLevelsBooleanFlag() {
    	return "1".equals(this.allLevelsFlag);
	}

	public void setAllLevelsBooleanFlag(Boolean allLevelsFlag) {
		this.customFlag = allLevelsFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public Long getBinaryWeight() {
		return binaryWeight;
	}

	public void setBinaryWeight(Long binaryWeight) {
		this.binaryWeight = binaryWeight;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getPrimarySitusCode() {
		return primarySitusCode;
	}

	public void setPrimarySitusCode(String primarySitusCode) {
		this.primarySitusCode = primarySitusCode;
	}
	
	public String getPrimaryTaxtypeCode() {
		return primaryTaxtypeCode;
	}

	public void setPrimaryTaxtypeCode(String primaryTaxtypeCode) {
		this.primaryTaxtypeCode = primaryTaxtypeCode;
	}
	
	public String getSecondarySitusCode() {
		return secondarySitusCode;
	}

	public void setSecondarySitusCode(String secondarySitusCode) {
		this.secondarySitusCode = secondarySitusCode;
	}
	
	public String getSecondaryTaxtypeCode() {
		return secondaryTaxtypeCode;
	}

	public void setSecondaryTaxtypeCode(String secondaryTaxtypeCode) {
		this.secondaryTaxtypeCode = secondaryTaxtypeCode;
	}
	
	public String getComments() {
	    return this.comments;
	}
	 
	public void setComments(String comments) {
	 	this.comments = comments;
	}
	
	public String getDriver01() {
		return driver01;
	}

	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}

	public String getDriver02() {
		return driver02;
	}

	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}

	public String getDriver03() {
		return driver03;
	}

	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}

	public String getDriver04() {
		return driver04;
	}

	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}

	public String getDriver05() {
		return driver05;
	}

	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}

	public String getDriver06() {
		return driver06;
	}

	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}

	public String getDriver07() {
		return driver07;
	}

	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}

	public String getDriver08() {
		return driver08;
	}

	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}

	public String getDriver09() {
		return driver09;
	}

	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}

	public String getDriver10() {
		return driver10;
	}

	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}

	public String getDriver11() {
		return driver11;
	}

	public void setDriver11(String driver11) {
		this.driver11 = driver11;
	}

	public String getDriver12() {
		return driver12;
	}

	public void setDriver12(String driver12) {
		this.driver12 = driver12;
	}

	public String getDriver13() {
		return driver13;
	}

	public void setDriver13(String driver13) {
		this.driver13 = driver13;
	}

	public String getDriver14() {
		return driver14;
	}

	public void setDriver14(String driver14) {
		this.driver14 = driver14;
	}

	public String getDriver15() {
		return driver15;
	}

	public void setDriver15(String driver15) {
		this.driver15 = driver15;
	}

	public String getDriver16() {
		return driver16;
	}

	public void setDriver16(String driver16) {
		this.driver16 = driver16;
	}

	public String getDriver17() {
		return driver17;
	}

	public String getDriver18() {
		return driver18;
	}

	public void setDriver18(String driver18) {
		this.driver18 = driver18;
	}

	public String getDriver19() {
		return driver19;
	}

	public void setDriver19(String driver19) {
		this.driver19 = driver19;
	}

	public String getDriver20() {
		return driver20;
	}

	public void setDriver20(String driver20) {
		this.driver20 = driver20;
	}

	public String getDriver21() {
		return driver21;
	}

	public void setDriver21(String driver21) {
		this.driver21 = driver21;
	}

	public String getDriver22() {
		return driver22;
	}

	public void setDriver22(String driver22) {
		this.driver22 = driver22;
	}

	public String getDriver23() {
		return driver23;
	}

	public void setDriver23(String driver23) {
		this.driver23 = driver23;
	}

	public String getDriver24() {
		return driver24;
	}

	public void setDriver24(String driver24) {
		this.driver24 = driver24;
	}
	
	public String getDriver25() {
		return driver25;
	}

	public void setDriver25(String driver25) {
		this.driver25 = driver25;
	}

	public String getDriver26() {
		return driver26;
	}

	public void setDriver26(String driver26) {
		this.driver26 = driver26;
	}

	public String getDriver27() {
		return driver27;
	}

	public void setDriver27(String driver27) {
		this.driver27 = driver27;
	}

	public String getDriver28() {
		return driver28;
	}

	public void setDriver28(String driver28) {
		this.driver28 = driver28;
	}

	public String getDriver29() {
		return driver29;
	}

	public void setDriver29(String driver29) {
		this.driver29 = driver29;
	}

	public String getDriver30() {
		return driver30;
	}

	public void setDriver30(String driver30) {
		this.driver30 = driver30;
	}
	
	public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  
    
    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}
