package com.ncsts.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class ImportMapProcessPreferenceDTO implements Serializable {
	private static Logger log = Logger.getLogger(ImportMapProcessPreferenceDTO.class);

	
	private static final long serialVersionUID = 1L;
	
	public enum DuplicationRestriction {
		Full("0"), 
		Partial("1"), 
		None("2");
		private String code;
		private DuplicationRestriction(String code) {
			this.code = code;
		}
		public static DuplicationRestriction get(String code) {
			for (DuplicationRestriction dr : DuplicationRestriction.values()) {
				if (dr.code.equalsIgnoreCase(code)) return dr;
			}
			return null;
		}
	}
	
	private String fileNameDuplicateRestriction;
	private String fileSizeDuplicateRestriction;
	private String terminalErrorsBeforeAbortImport;
	private String defaultImportDateMap;
	private String rowsToImportMapBeforeSave;
	private String endOfFileMarker;
	private String batchCountMarker;
	private String batchTotalMarker;
	private boolean emailProcessStatusFlag;
	private boolean deleteTransactionWithZeroAmountFlag;
	private boolean holdTransactionAmountFlag;
	private boolean processTransactionAmountFlag;
	private boolean killProcessingProcedureCountFlag;
	private boolean lookupVendorNexusFlag;
	private String holdTransactionAmount;
	private String processTransactionAmount;
	private String killProcessingProcedureCount;
	private String applyTaxCodeAmount;
    private boolean allocationsEnabled;	
    private boolean processrulesEnabled;
    private boolean taxCodeAllocationsEnabled;	
    private String doNotImportMarkers;
    private String replaceWithMarkers;
    private String batchTotalTolerance;
      
    private String glExtractFilePath; //GLEXTRACTFILEPATH
    private String glExtractFilePrefix; //GLEXTRACTPREFIX
    private String glExtractFileExtension; //GLEXTRACTEXT
    private boolean glExtractEnabled; //GLEXTRACTENABLED
    private String glExtractWindowName; //GLEXTRACTWIN
    private boolean glExtractSetMark; //GLEXTRACTMARK   
    

    private String pwdExpirationWarningDays; //PWEXPDAYS
    private boolean chgPassword; //PWUSERCHG
    private String denyPasswordChangeMessage; //PWDENYMSG    
    private boolean activateExtendedSecurity; //EXTENDSEC
    private boolean activateDebugExtendedSecurity; //DEBUGSEC    
    
    private String sqlFilePath; //SQLFILEPATH
    private String sqlFileExt;// SQLFILEEXT
    private String impExpFilePath; //IMPEXPFILEPATH
    private String impExpFileExt; //IMPEXPFILEEXT
    private String refDocFilePath; //REFDOCFILEPATH
    private String refDocFileExt;//REFDOCFILEEXT
    private String defaultDirCrystal; //DEFAULTDIRCRYSTAL
    private String refDocFilePathURL;
    
    private String importFilePath; //IMPORTFILEPATH
    private String importFileExt; //IMPORTFILEEXT
    private String userDirCrystal; //USERDIRCRYSTAL 
    
    private String lastTaxRateDateFull; //LASTTAXRATEDATEFULL
    private String lastTaxRateRelFull; //LASTTAXRATERELFULL
    private String lastTaxRateDateUpdate; //LASTTAXRATEDATEUPDATE
    private String lastTaxRateRelUpdate; //LASTTAXRATERELUPDATE
    private String taxWarnRate; //TAXWARNRATE    
    
    private String defCCHTaxCatCode; //DEFCCHTAXCATCODE
    private String defCCHGroupCode; //DEFCCHGROUPCODE
    private String tpExtractFilePath; //TPEXTRACTEFILEPATH
    private String tpExtractFilePrefix; //TPEXTRACTPREFIX
    private String tpExtractExt; //TPEXTRACTEXT
    private boolean tpExtractEnabled; //TPEXTRACTENABLED
    
    private String lastTaxCodeRulesDateFull; //LASTRULEDATEFULL
    private String lastTaxCodeRulesRelFull; //LASTRULERELFULL
    private String lastTaxCodeRulesDateUpdate; //LASTRULEDATEUPDATE
    private String lastTaxCodeRulesRelUpdate; //LASTRULERELUPDATE
    
    private String lastSitusRulesDateFull; //LASTSITUSRULEDATEFULL
    private String lastSitusRulesRelFull; //LASTSITUSRULERELFULL
    private String lastSitusRulesDateUpdate; //LASTSITUSRULEDATEUPDATE
    private String lastSitusRulesRelUpdate; //LASTSITUSRULERELUPDATE
    
    private boolean ratepointLatlongLookup; //LATLONGLOOKUP
    private String ratepointDefaultJurisdiction; //DEFAULTJURISDICTION
    private String ratepointDefaultTaxCode; //DEFAULTTAXCODE
    private String genericJurApi; //GENERICJURAPI
    
    //For AID email notification
    private String emailHost; //ADMIN/ADMIN/EMAILHOST
    private String emailPort; //ADMIN/ADMIN/EMAILPORT
    private String emailStart; //SYSTEM/SYSTEM/EMAILSTART
    private String emailStop; //SYSTEM/SYSTEM/EMAILSTOP
    private String emailTo; //SYSTEM/SYSTEM/EMAILTO
    private String emailCc; //SYSTEM/SYSTEM/EMAILCC
    private String emailFrom; //ADMIN/ADMIN/EMAILFROM 
    
    private String downloadFilePath;
    private String downloadUserId;
    private String downloadPassword;
    private String saleApiDatabase; //SALEAPIDATABASE
    private String saleApiServer;   //SALEAPISERVER
    private String saleApiUserId;  //SALEAPIUSERID
    private String saleApiPassword;//SALEAPIPASSWORD
    private String lastRuleDateUpdate; //LASTRULEDATEUPDATE
    private String lastRuleRelUpdate; //LASTRULERELUPDATE
    private String lastSitusDateUpdate; //LASTSITUSDATEUPDATE
    private String lastSitusRelUpdate; //LASTSITUSRELUPDATE
    
    private boolean postJurisdtoSalesAddressEnabled;
    
    private String postJurisdictiontoSalesAddress;
    
    private boolean runPostProcessrulesForheldTransFlag;
    
    private boolean customTaxCodeOverrideTaxionaryFlag;
    
	public boolean isRunPostProcessrulesForheldTransFlag() {
		return runPostProcessrulesForheldTransFlag;
	}

	public void setRunPostProcessrulesForheldTransFlag(
			boolean runPostProcessrulesForheldTransFlag) {
		this.runPostProcessrulesForheldTransFlag = runPostProcessrulesForheldTransFlag;
	}
	
	public boolean isCustomTaxCodeOverrideTaxionaryFlag() {
		return customTaxCodeOverrideTaxionaryFlag;
	}

	public void setCustomTaxCodeOverrideTaxionaryFlag(
			boolean customTaxCodeOverrideTaxionaryFlag) {
		this.customTaxCodeOverrideTaxionaryFlag = customTaxCodeOverrideTaxionaryFlag;
	}

	public String getPostJurisdictiontoSalesAddress() {
		return postJurisdictiontoSalesAddress;
	}

	public void setPostJurisdictiontoSalesAddress(
			String postJurisdictiontoSalesAddress) {
		this.postJurisdictiontoSalesAddress = postJurisdictiontoSalesAddress;
	}

	public void setPostJurisdtoSalesAddressEnabled(
			boolean postJurisdtoSalesAddressEnabled) {
		this.postJurisdtoSalesAddressEnabled = postJurisdtoSalesAddressEnabled;
	}

	public boolean isPostJurisdtoSalesAddressEnabled() {
		return postJurisdtoSalesAddressEnabled;
	}
	
	public boolean getIsPostJurisdtoSalesAddressEnabled() {
		return postJurisdtoSalesAddressEnabled;
	}
	

	public String getLastSitusDateUpdate() {
		return lastSitusDateUpdate;
	}

	public void setLastSitusDateUpdate(String lastSitusDateUpdate) {
		this.lastSitusDateUpdate = lastSitusDateUpdate;
	}

	public String getLastSitusRelUpdate() {
		return lastSitusRelUpdate;
	}

	public void setLastSitusRelUpdate(String lastSitusRelUpdate) {
		this.lastSitusRelUpdate = lastSitusRelUpdate;
	}

	public String getLastRuleDateUpdate() {
		return lastRuleDateUpdate;
	}

	public void setLastRuleDateUpdate(String lastRuleDateUpdate) {
		this.lastRuleDateUpdate = lastRuleDateUpdate;
	}

	public String getLastRuleRelUpdate() {
		return lastRuleRelUpdate;
	}

	public void setLastRuleRelUpdate(String lastRuleRelUpdate) {
		this.lastRuleRelUpdate = lastRuleRelUpdate;
	}

	public String getSaleApiDatabase() {
		return saleApiDatabase;
	}

	public void setSaleApiDatabase(String saleApiDatabase) {
		this.saleApiDatabase = saleApiDatabase;
	}

	public String getSaleApiServer() {
		return saleApiServer;
	}

	public void setSaleApiServer(String saleApiServer) {
		this.saleApiServer = saleApiServer;
	}

	public String getSaleApiUserId() {
		return saleApiUserId;
	}

	public void setSaleApiUserId(String saleApiUserId) {
		this.saleApiUserId = saleApiUserId;
	}

	public String getSaleApiPassword() {
		return saleApiPassword;
	}

	public void setSaleApiPassword(String saleApiPassword) {
		this.saleApiPassword = saleApiPassword;
	}

	public String getDownloadFilePath()
    {
      return this.downloadFilePath;
    }
    
    public void setDownloadFilePath(String downloadFilePath)
    {
      this.downloadFilePath = downloadFilePath;
    }
    
    public String getDownloadUserId()
    {
      return this.downloadUserId;
    }
    
    public void setDownloadUserId(String downloadUserId)
    {
      this.downloadUserId = downloadUserId;
    }
    
    public String getDownloadPassword()
    {
      return this.downloadPassword;
    }
    
    public void setDownloadPassword(String downloadPassword)
    {
      this.downloadPassword = downloadPassword;
    }
    
    
    public boolean isRatepointLatlongLookup() {
		return ratepointLatlongLookup;
	}
    
	public void setRatepointLatlongLookup(boolean ratepointLatlongLookup) {
		this.ratepointLatlongLookup = ratepointLatlongLookup;
	}
	
	public String getRatepointDefaultJurisdiction() {
		return ratepointDefaultJurisdiction;
	}
	
	public void setRatepointDefaultJurisdiction(String ratepointDefaultJurisdiction) {
		this.ratepointDefaultJurisdiction = ratepointDefaultJurisdiction;
	}
	
	public String getRatepointDefaultTaxCode() {
		return ratepointDefaultTaxCode;
	}
	
	public void setRatepointDefaultTaxCode(String ratepointDefaultTaxCode) {
		this.ratepointDefaultTaxCode = ratepointDefaultTaxCode;
	}
	
	public String getGenericJurApi() {
		return genericJurApi;
	}
	
	public void setGenericJurApi(String genericJurApi) {
		this.genericJurApi = genericJurApi;
	}
	
	public String getEmailHost() {
		return emailHost;
	}
	
	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}
	
	public String getEmailFrom() {
		return emailFrom;
	}
	
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	
	public String getEmailPort() {
		return emailPort;
	}
	
	public void setEmailPort(String emailPort) {
		this.emailPort = emailPort;
	}
	
	public String getEmailStart() {
		return emailStart;
	}
	
	public void setEmailStart(String emailStart) {
		this.emailStart = emailStart;
	}
	
	public String getEmailStop() {
		return emailStop;
	}
	
	public void setEmailStop(String emailStop) {
		this.emailStop = emailStop;
	}
	
	public String getEmailTo() {
		return emailTo;
	}
	
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	public String getEmailCc() {
		return emailCc;
	}
	
	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}
    
	public String getDoNotImportMarkers (){
		return this.doNotImportMarkers;
	}
	
	public void setDoNotImportMarkers (String doNotImportMarkers){
		this.doNotImportMarkers = doNotImportMarkers;
	}
    
	public String getReplaceWithMarkers (){
		return this.replaceWithMarkers;
	}
	
	public void setReplaceWithMarkers (String replaceWithMarkers){
		this.replaceWithMarkers = replaceWithMarkers;
	}
  
	public DuplicationRestriction getFileNameDuplicateRestrictionType() {
		return DuplicationRestriction.get(fileNameDuplicateRestriction);
	}
	public String getFileNameDuplicateRestriction() {
		return fileNameDuplicateRestriction;
	}
	public void setFileNameDuplicateRestriction(String fileNameDuplicateRestriction) {
		this.fileNameDuplicateRestriction = fileNameDuplicateRestriction;
	}
	public DuplicationRestriction getFileSizeDuplicateRestrictionType() {
		return DuplicationRestriction.get(fileSizeDuplicateRestriction);
	}
	public String getFileSizeDuplicateRestriction() {
		return fileSizeDuplicateRestriction;
	}
	public void setFileSizeDuplicateRestriction(String fileSizeDuplicateRestriction) {
		this.fileSizeDuplicateRestriction = fileSizeDuplicateRestriction;
	}
	public String getTerminalErrorsBeforeAbortImport() {
		return terminalErrorsBeforeAbortImport;
	}
	public Integer getMaxErrors() {
		//0001701: Import errors make the system crash
		int maxError = 100;
		try {
			maxError = Integer.parseInt(terminalErrorsBeforeAbortImport);
			
			if(maxError>100 || maxError<1){
				maxError = 100;
			}
		} catch (NumberFormatException e) {
		}
		
		return maxError;
	}
	public void setTerminalErrorsBeforeAbortImport(
			String terminalErrorsBeforeAbortImport) {
		this.terminalErrorsBeforeAbortImport = terminalErrorsBeforeAbortImport;
	}
	public String getDefaultImportDateMap() {
		return defaultImportDateMap;
	}
	public void setDefaultImportDateMap(String defaultImportDateMap) {
		this.defaultImportDateMap = defaultImportDateMap;
	}
	public String getRowsToImportMapBeforeSave() {
		return rowsToImportMapBeforeSave;
	}
	public void setRowsToImportMapBeforeSave(String rowsToImportMapBeforeSave) {
		this.rowsToImportMapBeforeSave = rowsToImportMapBeforeSave;
	}
	
	public String getBatchTotalTolerance() {
		return batchTotalTolerance;
	}
	public void setBatchTotalTolerance(String batchTotalTolerance) {
		this.batchTotalTolerance = batchTotalTolerance;
	}
	
	public String getEndOfFileMarker() {
		return endOfFileMarker;
	}
	public void setEndOfFileMarker(String endOfFileMarker) {
		this.endOfFileMarker = endOfFileMarker;
	}
	public String getBatchCountMarker() {
		return batchCountMarker;
	}
	public void setBatchCountMarker(String batchCountMarker) {
		this.batchCountMarker = batchCountMarker;
	}
	public String getBatchTotalMarker() {
		return batchTotalMarker;
	}
	public void setBatchTotalMarker(String batchTotalMarker) {
		this.batchTotalMarker = batchTotalMarker;
	}
	public boolean isEmailProcessStatusFlag() {
		return emailProcessStatusFlag;
	}
	public void setEmailProcessStatusFlag(
			boolean emailProcessStatusFlag) {
		this.emailProcessStatusFlag = emailProcessStatusFlag;
	}
	public boolean isDeleteTransactionWithZeroAmountFlag() {
		return deleteTransactionWithZeroAmountFlag;
	}
	public void setDeleteTransactionWithZeroAmountFlag(
			boolean deleteTransactionWithZeroAmountFlag) {
		this.deleteTransactionWithZeroAmountFlag = deleteTransactionWithZeroAmountFlag;
	}
	public boolean isHoldTransactionAmountFlag() {
		return holdTransactionAmountFlag;
	}
	public void setHoldTransactionAmountFlag(boolean holdTransactionAmountFlag) {
		this.holdTransactionAmountFlag = holdTransactionAmountFlag;
	}
	public boolean isProcessTransactionAmountFlag() {
		return processTransactionAmountFlag;
	}
	public void setProcessTransactionAmountFlag(boolean processTransactionAmountFlag) {
		this.processTransactionAmountFlag = processTransactionAmountFlag;
	}
	public boolean isKillProcessingProcedureCountFlag() {
		return killProcessingProcedureCountFlag;
	}
	public void setKillProcessingProcedureCountFlag(
			boolean killProcessingProcedureCountFlag) {
		this.killProcessingProcedureCountFlag = killProcessingProcedureCountFlag;
	}
	public boolean isLookupVendorNexusFlag() {
		return lookupVendorNexusFlag;
	}
	public void setLookupVendorNexusFlag(boolean lookupVendorNexusFlag) {
		this.lookupVendorNexusFlag = lookupVendorNexusFlag;
	}
	public String getHoldTransactionAmount() {
		return holdTransactionAmount;
	}
	public void setHoldTransactionAmount(String holdTransactionAmount) {
		this.holdTransactionAmount = holdTransactionAmount;
	}
	public String getProcessTransactionAmount() {
		return processTransactionAmount;
	}
	public void setProcessTransactionAmount(String processTransactionAmount) {
		this.processTransactionAmount = processTransactionAmount;
	}
	public String getKillProcessingProcedureCount() {
		return killProcessingProcedureCount;
	}
	public void setKillProcessingProcedureCount(String killProcessingProcedureCount) {
		this.killProcessingProcedureCount = killProcessingProcedureCount;
	}
	public String getApplyTaxCodeAmount() {
		return applyTaxCodeAmount;
	}
	public void setApplyTaxCodeAmount(String applyTaxCodeAmount) {
		this.applyTaxCodeAmount = applyTaxCodeAmount;
	}
	public boolean isAllocationsEnabled() {
		return allocationsEnabled;
	}
	public void setAllocationsEnabled(boolean allocationsEnabled) {
		this.allocationsEnabled = allocationsEnabled;
	}
	
	public boolean isProcessrulesEnabled() {
		return processrulesEnabled;
	}

	public void setProcessrulesEnabled(boolean processrulesEnabled) {
		this.processrulesEnabled = processrulesEnabled;
	}
	
	public boolean isTaxCodeAllocationsEnabled() {
		return taxCodeAllocationsEnabled;
	}
	public void setTaxCodeAllocationsEnabled(boolean taxCodeAllocationsEnabled) {
		this.taxCodeAllocationsEnabled = taxCodeAllocationsEnabled;
	}

	public String getGlExtractFilePath() {
		return glExtractFilePath;
	}
	public void setGlExtractFilePath(String glExtractFilePath) {
		this.glExtractFilePath = glExtractFilePath;
	}
	public String getGlExtractFilePrefix() {
		return glExtractFilePrefix;
	}
	public void setGlExtractFilePrefix(String glExtractFilePrefix) {
		this.glExtractFilePrefix = glExtractFilePrefix;
	}
	public String getGlExtractFileExtension() {
		return glExtractFileExtension;
	}
	public void setGlExtractFileExtension(String glExtractFileExtension) {
		this.glExtractFileExtension = glExtractFileExtension;
	}
	public boolean isGlExtractEnabled() {
		return glExtractEnabled;
	}
	public void setGlExtractEnabled(boolean glExtractEnabled) {
		this.glExtractEnabled = glExtractEnabled;
	}
	public String getGlExtractWindowName() {
		return glExtractWindowName;
	}
	public void setGlExtractWindowName(String glExtractWindowName) {
		this.glExtractWindowName = glExtractWindowName;
	}
	public boolean isGlExtractSetMark() {
		return glExtractSetMark;
	}
	public void setGlExtractSetMark(boolean glExtractSetMark) {
		this.glExtractSetMark = glExtractSetMark;
	}
	public String getPwdExpirationWarningDays() {
		return pwdExpirationWarningDays;
	}
	public void setPwdExpirationWarningDays(String pwdExpirationWarningDays) {
		this.pwdExpirationWarningDays = pwdExpirationWarningDays;
	}
	public boolean isChgPassword() {
		return chgPassword;
	}
	public void setChgPassword(boolean chgPassword) {
		this.chgPassword = chgPassword;
	}
	public String getDenyPasswordChangeMessage() {
		return denyPasswordChangeMessage;
	}
	public void setDenyPasswordChangeMessage(String denyPasswordChangeMessage) {
		this.denyPasswordChangeMessage = denyPasswordChangeMessage;
	}
	public boolean isActivateExtendedSecurity() {
		return activateExtendedSecurity;
	}
	public void setActivateExtendedSecurity(boolean activateExtendedSecurity) {
		this.activateExtendedSecurity = activateExtendedSecurity;
	}
	public boolean isActivateDebugExtendedSecurity() {
		return activateDebugExtendedSecurity;
	}
	public void setActivateDebugExtendedSecurity(
			boolean activateDebugExtendedSecurity) {
		this.activateDebugExtendedSecurity = activateDebugExtendedSecurity;
	}
	public String getSqlFilePath() {
		return sqlFilePath;
	}
	public void setSqlFilePath(String sqlFilePath) {
		this.sqlFilePath = sqlFilePath;
	}
	public String getSqlFileExt() {
		return sqlFileExt;
	}
	public void setSqlFileExt(String sqlFileExt) {
		this.sqlFileExt = sqlFileExt;
	}
	public String getImpExpFilePath() {
		return impExpFilePath;
	}
	public void setImpExpFilePath(String impExpFilePath) {
		this.impExpFilePath = impExpFilePath;
	}
	public String getImpExpFileExt() {
		return impExpFileExt;
	}
	public void setImpExpFileExt(String impExpFileExt) {
		this.impExpFileExt = impExpFileExt;
	}
	public String getRefDocFilePath() {
		return refDocFilePath;
	}
	public void setRefDocFilePath(String refDocFilePath) {
		this.refDocFilePath = refDocFilePath;
	}
	//Added to fix the bug 0003958(line 325 t0 333)
	public String getRefDocFilePathURL() {
		
		if(this.getRefDocFilePath().endsWith("*STATE")){
			refDocFilePathURL=getRefDocFilePath().substring(0,getRefDocFilePath().lastIndexOf("\\*")+1);
			log.info("##################Inside RefDocFilePathURL get method ##########################"+refDocFilePathURL);
		}
		return refDocFilePathURL;
	}
	public void setRefDocFilePathURL(String refDocFilePath) {
		this.refDocFilePathURL = refDocFilePath;
	}
	public String getRefDocFileExt() {
		return refDocFileExt;
	}
	public void setRefDocFileExt(String refDocFileExt) {
		this.refDocFileExt = refDocFileExt;
	}
	public String getDefaultDirCrystal() {
		return defaultDirCrystal;
	}
	public void setDefaultDirCrystal(String defaultDirCrystal) {
		this.defaultDirCrystal = defaultDirCrystal;
	}
	public String getImportFilePath() {
		return importFilePath;
	}
	public void setImportFilePath(String importFilePath) {
		this.importFilePath = importFilePath;
	}
	public String getImportFileExt() {
		return importFileExt;
	}
	public void setImportFileExt(String importFileExt) {
		this.importFileExt = importFileExt;
	}
	public String getUserDirCrystal() {
		return userDirCrystal;
	}
	public void setUserDirCrystal(String userDirCrystal) {
		this.userDirCrystal = userDirCrystal;
	}
	public String getLastTaxRateDateFull() {
		return lastTaxRateDateFull;
	}
	public void setLastTaxRateDateFull(String lastTaxRateDateFull) {
		this.lastTaxRateDateFull = lastTaxRateDateFull;
	}
	public String getLastTaxRateRelFull() {
		return lastTaxRateRelFull;
	}
	public void setLastTaxRateRelFull(String lastTaxRateRelFull) {
		this.lastTaxRateRelFull = lastTaxRateRelFull;
	}
	public String getLastTaxRateDateUpdate() {
		return lastTaxRateDateUpdate;
	}
	public Date getLastTaxRateDateUpdateAsDate() {
		try {
		return new SimpleDateFormat("yyyy_MM").parse(lastTaxRateDateUpdate);
		} catch (ParseException pe) {
			log.error("Failed to parse: " + lastTaxRateDateUpdate);
			return null;
		}
	}
	public void setLastTaxRateDateUpdate(String lastTaxRateDateUpdate) {
		this.lastTaxRateDateUpdate = lastTaxRateDateUpdate;
	}
	public String getLastTaxRateRelUpdate() {
		return lastTaxRateRelUpdate;
	}
	public int getLastTaxRateRelUpdateAsInt() {
		try {
			return Integer.parseInt(lastTaxRateRelUpdate);
		} catch (NumberFormatException e) {
			log.error("Cannot parse lastTaxRateRelUpdate: " + lastTaxRateRelUpdate);
			return 0;
		}
	}
	public void setLastTaxRateRelUpdate(String lastTaxRateRelUpdate) {
		this.lastTaxRateRelUpdate = lastTaxRateRelUpdate;
	}
	public String getTaxWarnRate() {
		return taxWarnRate;
	}
	public void setTaxWarnRate(String taxWarnRate) {
		this.taxWarnRate = taxWarnRate;
	}
	public String getDefCCHTaxCatCode() {
		return defCCHTaxCatCode;
	}
	public void setDefCCHTaxCatCode(String defCCHTaxCatCode) {
		this.defCCHTaxCatCode = defCCHTaxCatCode;
	}
	public String getDefCCHGroupCode() {
		return defCCHGroupCode;
	}
	public void setDefCCHGroupCode(String defCCHGroupCode) {
		this.defCCHGroupCode = defCCHGroupCode;
	}
	public String getTpExtractFilePath() {
		return tpExtractFilePath;
	}
	public void setTpExtractFilePath(String tpExtractFilePath) {
		this.tpExtractFilePath = tpExtractFilePath;
	}
	public String getTpExtractFilePrefix() {
		return tpExtractFilePrefix;
	}
	public void setTpExtractFilePrefix(String tpExtractFilePrefix) {
		this.tpExtractFilePrefix = tpExtractFilePrefix;
	}
	public String getTpExtractExt() {
		return tpExtractExt;
	}
	public void setTpExtractExt(String tpExtractExt) {
		this.tpExtractExt = tpExtractExt;
	}
	public boolean isTpExtractEnabled() {
		return tpExtractEnabled;
	}
	public void setTpExtractEnabled(boolean tpExtractEnabled) {
		this.tpExtractEnabled = tpExtractEnabled;
	}

	//Tax code rules
	public String getLastTaxCodeRulesDateFull() {
		return lastTaxCodeRulesDateFull;
	}
	public void setLastTaxCodeRulesDateFull(String lastTaxCodeRulesDateFull) {
		this.lastTaxCodeRulesDateFull = lastTaxCodeRulesDateFull;
	}
	public String getLastTaxCodeRulesRelFull() {
		return lastTaxCodeRulesRelFull;
	}
	public void setLastTaxCodeRulesRelFull(String lastTaxCodeRulesRelFull) {
		this.lastTaxCodeRulesRelFull = lastTaxCodeRulesRelFull;
	}
	public String getLastTaxCodeRulesDateUpdate() {
		return lastTaxCodeRulesDateUpdate;
	}
	public Date getLastTaxCodeRulesDateUpdateAsDate() {
		try {
			if(lastTaxCodeRulesDateUpdate != null) {
				return new SimpleDateFormat("yyyy_MM").parse(lastTaxCodeRulesDateUpdate);
			}
			return null;
		
		} catch (ParseException pe) {
			log.error("Failed to parse: " + lastTaxCodeRulesDateUpdate);
			return null;
		}
	}
	public void setLastTaxCodeRulesDateUpdate(String lastTaxCodeRulesDateUpdate) {
		this.lastTaxCodeRulesDateUpdate = lastTaxCodeRulesDateUpdate;
	}
	public String getLastTaxCodeRulesRelUpdate() {
		return lastTaxCodeRulesRelUpdate;
	}
	public int getLastTaxCodeRulesRelUpdateAsInt() {
		try {
			return Integer.parseInt(lastTaxCodeRulesRelUpdate);
		} catch (NumberFormatException e) {
			log.error("Cannot parse lastTaxCodeRulesRelUpdate: " + lastTaxCodeRulesRelUpdate);
			return 0;
		}
	}
	public void setLastTaxCodeRulesRelUpdate(String lastTaxCodeRulesRelUpdate) {
		this.lastTaxCodeRulesRelUpdate = lastTaxCodeRulesRelUpdate;
	}
	
	//Sutus rules
	public String getLastSitusRulesDateFull() {
		return lastSitusRulesDateFull;
	}
	public void setLastSitusRulesDateFull(String lastSitusRulesDateFull) {
		this.lastSitusRulesDateFull = lastSitusRulesDateFull;
	}
	public String getLastSitusRulesRelFull() {
		return lastSitusRulesRelFull;
	}
	public void setLastSitusRulesRelFull(String lastSitusRulesRelFull) {
		this.lastSitusRulesRelFull = lastSitusRulesRelFull;
	}
	public String getLastSitusRulesDateUpdate() {
		return lastSitusRulesDateUpdate;
	}
	public Date getLastSitusRulesDateUpdateAsDate() {
		try {
			if(lastSitusRulesDateUpdate==null || lastSitusRulesDateUpdate.length()==0){
				lastSitusRulesDateUpdate = "1970_01";
			}
			return new SimpleDateFormat("yyyy_MM").parse(lastSitusRulesDateUpdate);
		} catch (ParseException pe) {
			log.error("Failed to parse: " + lastSitusRulesDateUpdate);
			return null;
		}
	}
	public void setLastSitusRulesDateUpdate(String lastSitusRulesDateUpdate) {
		this.lastSitusRulesDateUpdate = lastSitusRulesDateUpdate;
	}
	public String getLastSitusRulesRelUpdate() {
		return lastSitusRulesRelUpdate;
	}
	public int getLastSitusRulesRelUpdateAsInt() {
		try {
			if(lastSitusRulesRelUpdate==null || lastSitusRulesRelUpdate.length()==0){
				lastSitusRulesRelUpdate = "1";
			}
			return Integer.parseInt(lastSitusRulesRelUpdate);
		} catch (NumberFormatException e) {
			log.error("Cannot parse lastSitusRulesRelUpdate: " + lastSitusRulesRelUpdate);
			return 0;
		}
	}
	public void setLastSitusRulesRelUpdate(String lastSitusRulesRelUpdate) {
		this.lastSitusRulesRelUpdate = lastTaxCodeRulesRelUpdate;
	}
	
}
