package com.ncsts.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.ncsts.domain.Idable;

public class ErrorLogDTO implements Idable<Long>{
	
	private Long batchErrorLogId;
	private Long batchId;
	private String processType;
	private Timestamp processTimestamp;
	private Long rowNumber;
	private Long columnNumber;
	private String errorDefCode;
	private String errorDefDesc;
	private String importHeaderColumn;
	private String importColumnValue;
	private String transDtlColumnName;
	private String transDtlColumnDesc;
	private String transDtlDatatype;
	private String importRow;
	
	private String errorDefExplanation;//ERROR_DEF_EXPL of TB_BATCH_ERROR_LOG
	private String sevLevel;//SEVERITY_LEVEL of TB_BATCH_ERROR_LOG
	private String sevLevelDesc;//SEVERITY_LEVEL_DESC of TB_BATCH_ERROR_LOG
	
	
    final static SimpleDateFormat   DATE_TIME =  new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    /**
     * Return "standard" date/time formatted string
     * @param t
     * @return
     */
    private static String toString(Timestamp t) {
      if (t != null) {
        return DATE_TIME.format(t);
      } else {
        return "";
      }
    }

		public String getProcessType() {
		return processType;
	}
		
	public Long getId() {
		return getBatchErrorLogId();
	}

	public String getIdPropertyName() {
		return "batchErrorLogId";
	}
	
	public void setId(Long id) {
		setBatchErrorLogId(id);
	}
		
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getErrorDefCode() {
		return errorDefCode;
	}
	public void setErrorDefCode(String errorDefCode) {
		this.errorDefCode = errorDefCode;
	}
	public String getErrorDefDesc() {
		return errorDefDesc;
	}
	public void setErrorDefDesc(String errorDefDesc) {
		this.errorDefDesc = errorDefDesc;
	}
	public String getImportHeaderColumn() {
		return importHeaderColumn;
	}
	public void setImportHeaderColumn(String importHeaderColumn) {
		this.importHeaderColumn = importHeaderColumn;
	}
	public String getImportColumnValue() {
		return importColumnValue;
	}
	public void setImportColumnValue(String importColumnValue) {
		this.importColumnValue = importColumnValue;
	}
	public String getTransDtlColumnName() {
		return transDtlColumnName;
	}
	public void setTransDtlColumnName(String transDtlColumnName) {
		this.transDtlColumnName = transDtlColumnName;
	}
	public String getTransDtlColumnDesc() {
		return transDtlColumnDesc;
	}
	public void setTransDtlColumnDesc(String transDtlColumnDesc) {
		this.transDtlColumnDesc = transDtlColumnDesc;
	}
	public String getTransDtlDatatype() {
		return transDtlDatatype;
	}
	public void setTransDtlDatatype(String transDtlDatatype) {
		this.transDtlDatatype = transDtlDatatype;
	}
	public String getImportRow() {
		return importRow;
	}
	public String getImportRowAbbreviated() {
		if ((importRow != null) && (importRow.length() > 32)) {
			return importRow.substring(0, 30) + "...";
		} else {
			return importRow;
		}
	}
	public void setImportRow(String importRow) {
		this.importRow = importRow;
	}
	public String getErrorDefExplanation() {
		return errorDefExplanation;
	}
	public String getSevLevel() {
		return sevLevel;
	}
	public String getSevLevelDesc() {
		return sevLevelDesc;
	}
	public void setErrorDefExplanation(String errorDefExplanation) {
		this.errorDefExplanation = errorDefExplanation;
	}
	public void setSevLevel(String sevLevel) {
		this.sevLevel = sevLevel;
	}
	public void setSevLevelDesc(String sevLevelDesc) {
		this.sevLevelDesc = sevLevelDesc;
	}
	
    /**
	 * @return the batchErrorLogId
	 */
	public Long getBatchErrorLogId() {
		return this.batchErrorLogId;
	}

	/**
	 * @param batchErrorLogId the batchErrorLogId to set
	 */
	public void setBatchErrorLogId(Long batchErrorLogId) {
		this.batchErrorLogId = batchErrorLogId;
	}

	/**
	 * @return the batchId
	 */
	public Long getBatchId() {
		return this.batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the processTimestamp
	 */
	public Timestamp getProcessTimestamp() {
		return this.processTimestamp;
	}

	/**
	 * @param processTimestamp the processTimestamp to set
	 */
	public void setProcessTimestamp(Timestamp processTimestamp) {
		this.processTimestamp = processTimestamp;
	}

	/**
	 * @return the rowNumber
	 */
	public Long getRowNumber() {
		return this.rowNumber;
	}

	/**
	 * @param rowNumber the rowNumber to set
	 */
	public void setRowNumber(Long rowNumber) {
		this.rowNumber = rowNumber;
	}
	public void setRowNo(Long row) {
		this.rowNumber = row;
	}

	/**
	 * @return the columnNumber
	 */
	public Long getColumnNumber() {
		return this.columnNumber;
	}

	/**
	 * @param columnNumber the columnNumber to set
	 */
	public void setColumnNumber(Long columnNumber) {
		this.columnNumber = columnNumber;
	}
	public void setColumnNo(Long columnNumber) {
		this.columnNumber = columnNumber;
	}

		/**
     * Used for saveAs 
     * @return a string representation of this object.
     */
    public String toString() {
      return this.batchErrorLogId + "\t" + this.batchId+ "\t" + this.processType+"\t" 
      + toString(this.processTimestamp) +"\t" + this.rowNumber +"\t" + this.columnNumber + "\t"
      + this.errorDefCode + "\t" + this.errorDefDesc + "\t" + this.importHeaderColumn + "\t"
      + this.importColumnValue + "\t" + this.transDtlColumnName + "\t" + this.transDtlColumnDesc + "\t"
      + this.transDtlDatatype + "\t" + this.importRow;
    }
	
}
