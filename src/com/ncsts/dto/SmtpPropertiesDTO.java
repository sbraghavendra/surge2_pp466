package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class SmtpPropertiesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;  
    private Integer smtpPort;
    
    private String action; 
    private String smtpSubject; 
    private String smtpFrom; 
    private String smtpContent; 
    private String smtpHostName; 

    public SmtpPropertiesDTO() { 
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getSmtpSubject() {
		return smtpSubject;
	}
	
	public void setSmtpSubject(String smtpSubject) {
		this.smtpSubject = smtpSubject;
	}

	public String getSmtpFrom() {
		return smtpFrom;
	}
	
	public void setSmtpFrom(String smtpFrom) {
		this.smtpFrom = smtpFrom;
	}
	
	public String getSmtpContent() {
		return smtpContent;
	}
	
	public void setSmtpContent(String smtpContent) {
		this.smtpContent = smtpContent;
	}
	
	public String getSmtpHostName() {
		return smtpHostName;
	}
	
	public void setSmtpHostName(String smtpHostName) {
		this.smtpHostName = smtpHostName;
	}
	
    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("id :\t").append(id);

    	return sb.toString();
    }
    
}







