package com.ncsts.dto;

import com.ncsts.domain.PurchaseTransaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by RC05200 on 2/28/2017.
 */
public class TransientPurchaseTransactionDocument {

    private final Map<TaxCodeRuleGroupBy, List<PurchaseTransaction>> groupedDocument = new HashMap<TaxCodeRuleGroupBy, List<PurchaseTransaction>>();

    public Map<TaxCodeRuleGroupBy, List<PurchaseTransaction>> getGroupedDocument() {
        return groupedDocument;
    }



}
