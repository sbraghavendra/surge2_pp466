package com.ncsts.dto;

import java.util.Date;

public class ImportDefDTO {
	private String importDefinitionCode;
	private String importDefinitionType;
	private String description;
	private String importFileTypeCode;
	private String importLoader;
	private String flatDelChar;
	private String flatDelTextQual;
	private String flatDelLine1HeadFlag;
	private Boolean flatDelLine1HeadFlagUI;
	private Long flatFixedRecordLength;
	private String dbTableName;
	private String xmlDomName;
	private String storedProcName;
	private String comments;
	private String sampleFileName;
	private String mapUpdateUserId;
	private Date mapUpdateTimestamp;
	private String updateUserId;
	private Date updateTimestamp;
	private String importConnectionCode;
	private String encoding;
	private boolean copyMapFlag;  
	
	
    public boolean getCopyMapFlag() {
		return copyMapFlag;
	}

	public void setCopyMapFlag(boolean copyMapFlag) {
		this.copyMapFlag = copyMapFlag;
	}
	
	public String getImportDefinitionCode() {
		return importDefinitionCode;
	}
	public void setImportDefinitionCode(String importDefinitionCode) {
		this.importDefinitionCode = importDefinitionCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImportFileTypeCode() {
		return importFileTypeCode;
	}
	public void setImportFileTypeCode(String importFileTypeCode) {
		this.importFileTypeCode = importFileTypeCode;
	}
	public String getImportLoader() {
		return importLoader;
	}
	public void setImportLoader(String importLoader) {
		this.importLoader = importLoader;
	}
	public String getFlatDelChar() {
		return flatDelChar;
	}
	public void setFlatDelChar(String flatDelChar) {
		this.flatDelChar = flatDelChar;
	}
	public String getFlatDelTextQual() {
		return flatDelTextQual;
	}
	public void setFlatDelTextQual(String flatDelTextQual) {
		this.flatDelTextQual = flatDelTextQual;
	}
	public Boolean getFlatDelLine1HeadFlagUI() {
		if (flatDelLine1HeadFlag == null)
		{
			return false;
		}else {
			if (flatDelLine1HeadFlag.equals("1"))
			{
				return true;
			}else{
				return false;
			}
		}
	}
	public void setFlatDelLine1HeadFlagUI(Boolean flatDelLine1HeadFlagUI) {
		if (flatDelLine1HeadFlagUI == true)
		{
			flatDelLine1HeadFlag = "1";
		}else {
			flatDelLine1HeadFlag = "0";
		}
	}
	public String getFlatDelLine1HeadFlag() {
		return flatDelLine1HeadFlag; 
	}
	public void setFlatDelLine1HeadFlag(String flatDelLine1HeadFlag) {
		this.flatDelLine1HeadFlag = flatDelLine1HeadFlag;
	}
	public Long getFlatFixedRecordLength() {
		return flatFixedRecordLength;
	}
	public void setFlatFixedRecordLength(Long flatFixedRecordLength) {
		this.flatFixedRecordLength = flatFixedRecordLength;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getXmlDomName() {
		return xmlDomName;
	}
	public void setXmlDomName(String xmlDomName) {
		this.xmlDomName = xmlDomName;
	}
	public String getStoredProcName() {
		return storedProcName;
	}
	public void setStoredProcName(String storedProcName) {
		this.storedProcName = storedProcName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getSampleFileName() {
		return sampleFileName;
	}
	public void setSampleFileName(String sampleFileName) {
		this.sampleFileName = sampleFileName;
	}
	public String getMapUpdateUserId() {
		return mapUpdateUserId;
	}
	public void setMapUpdateUserId(String mapUpdateUserId) {
		this.mapUpdateUserId = mapUpdateUserId;
	}
	public Date getMapUpdateTimestamp() {
		return mapUpdateTimestamp;
	}
	public void setMapUpdateTimestamp(Date mapUpdateTimestamp) {
		this.mapUpdateTimestamp = mapUpdateTimestamp;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	public String getImportDefinitionType() {
		return importDefinitionType;
	}
	public void setImportDefinitionType(String importDefinitionType) {
		this.importDefinitionType = importDefinitionType;
	}

	public String getImportConnectionCode() {
		return importConnectionCode;
	}

	public void setImportConnectionCode(String importConnectionCode) {
		this.importConnectionCode = importConnectionCode;
	}
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
