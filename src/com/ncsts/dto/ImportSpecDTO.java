package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Muneer
 *
 */
	public class ImportSpecDTO implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		private String importSpecType; 
		private String importSpecCode;		
		private String description;
	    private String importDefinitionCode;
	    private String defaultDirectory;
	    private String lastImportFileName;
	    /*private String submitJobFlag;*/
	    private String whereClause;	
	    private String comments;	
	    private String updateUserId;		
	    private Date updateTimestamp;
	    private String filesizedupres;
	    private String filenamedupres;
	    private String importdatemap;
	    private String donotimport;
	    private String replacewith;
	    private String batcheofmarker;
	    private String batchcountmarker;
	    private String batchtotalmarker;
	    private String delete0amtflag;
	    private String holdtransflag;
	    private Long holdtransamt;
	    private String autoprocflag;
	    private Long autoprocamt;
	    private String autoprocid;	
	    private String autoprocessFlag;

	    
	    private ImportDefDTO importDefDTO;
		public String getImportSpecType() {
			return importSpecType;
		}
		public void setImportSpecType(String importSpecType) {
			this.importSpecType = importSpecType;
		}
		public String getImportSpecCode() {
			return importSpecCode;
		}
		public void setImportSpecCode(String importSpecCode) {
			this.importSpecCode = importSpecCode;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getImportDefinitionCode() {
			return importDefinitionCode;
		}
		public void setImportDefinitionCode(String importDefinitionCode) {
			this.importDefinitionCode = importDefinitionCode;
		}
		public String getDefaultDirectory() {
			return defaultDirectory;
		}
		public void setDefaultDirectory(String defaultDirectory) {
			this.defaultDirectory = defaultDirectory;
		}
		public String getLastImportFileName() {
			return lastImportFileName;
		}
		public void setLastImportFileName(String lastImportFileName) {
			this.lastImportFileName = lastImportFileName;
		}
		/*public String getSubmitJobFlag() {
			return submitJobFlag;
		}
		public void setSubmitJobFlag(String submitJobFlag) {
			this.submitJobFlag = submitJobFlag;
		}*/
		public String getWhereClause() {
			return whereClause;
		}
		public void setWhereClause(String whereClause) {
			this.whereClause = whereClause;
		}
		public String getComments() {
			return comments;
		}
		public void setComments(String comments) {
			this.comments = comments;
		}
		public String getUpdateUserId() {
			return updateUserId;
		}
		public void setUpdateUserId(String updatedUserId) {
			this.updateUserId = updatedUserId;
		}
		public Date getUpdateTimestamp() {
			return updateTimestamp;
		}
		public void setUpdateTimestamp(Date updateTimestamp) {
			this.updateTimestamp = updateTimestamp;
		}
		
		public void setImportDefDTO(ImportDefDTO importDefDTO) {
			this.importDefDTO = importDefDTO;
		}
		
		public ImportDefDTO getImportDefDTO () {
			return this.importDefDTO;
		}
		public String getFilesizedupres() {
			return filesizedupres;
		}
		public void setFilesizedupres(String filesizedupres) {
			this.filesizedupres = filesizedupres;
		}
		public String getFilenamedupres() {
			return filenamedupres;
		}
		public void setFilenamedupres(String filenamedupres) {
			this.filenamedupres = filenamedupres;
		}
		public String getImportdatemap() {
			return importdatemap;
		}
		public void setImportdatemap(String importdatemap) {
			this.importdatemap = importdatemap;
		}
		public String getDonotimport() {
			return donotimport;
		}
		public void setDonotimport(String donotimport) {
			this.donotimport = donotimport;
		}
		public String getReplacewith() {
			return replacewith;
		}
		public void setReplacewith(String replacewith) {
			this.replacewith = replacewith;
		}
		public String getBatcheofmarker() {
			return batcheofmarker;
		}
		public void setBatcheofmarker(String batcheofmarker) {
			this.batcheofmarker = batcheofmarker;
		}
		public String getBatchcountmarker() {
			return batchcountmarker;
		}
		public void setBatchcountmarker(String batchcountmarker) {
			this.batchcountmarker = batchcountmarker;
		}
		public String getBatchtotalmarker() {
			return batchtotalmarker;
		}
		public void setBatchtotalmarker(String batchtotalmarker) {
			this.batchtotalmarker = batchtotalmarker;
		}
		public String getDelete0amtflag() {
			return delete0amtflag;
		}
		public void setDelete0amtflag(String delete0amtflag) {
			this.delete0amtflag = delete0amtflag;
		}
		public String getHoldtransflag() {
			return holdtransflag;
		}
		public void setHoldtransflag(String holdtransflag) {
			this.holdtransflag = holdtransflag;
		}
		public Long getHoldtransamt() {
			return holdtransamt;
		}
		public void setHoldtransamt(Long holdtransamt) {
			this.holdtransamt = holdtransamt;
		}
		public String getAutoprocflag() {
			return autoprocflag;
		}
		public void setAutoprocflag(String autoprocflag) {
			this.autoprocflag = autoprocflag;
		}
		public Long getAutoprocamt() {
			return autoprocamt;
		}
		public void setAutoprocamt(Long autoprocamt) {
			this.autoprocamt = autoprocamt;
		}
		public String getAutoprocid() {
			return autoprocid;
		}
		public void setAutoprocid(String autoprocid) {
			this.autoprocid = autoprocid;
		}

		public String getAutoprocessFlag() {
			return autoprocessFlag;
		}

		public void setAutoprocessFlag(String autoprocessFlag) {
			this.autoprocessFlag = autoprocessFlag;
		} 
		
		public Boolean getAutoprocessBooleanFlag() {
	    	return "1".equals(this.autoprocessFlag);
		}

		public void setAutoprocessBooleanFlag(Boolean autoprocessFlag) {
			this.autoprocessFlag = autoprocessFlag == Boolean.TRUE ? "1" : "0";
		}
	
	}

