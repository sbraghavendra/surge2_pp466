package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class VendorNexusDtlDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long vendorNexusDtlId;
    private Long vendorNexusId;
    private Date effectiveDate;
    private Date expirationDate;   
    private String nexusTypeCode; 
    private String activeFlag; 

    public VendorNexusDtlDTO(){
    }

    public Long getVendorNexusDtlId() {
        return this.vendorNexusDtlId;
    }
    
    public void setVendorNexusDtlId(Long vendorNexusDtlId) {
    	this.vendorNexusDtlId = vendorNexusDtlId;
    }
	
	public Long getVendorNexusId() {
        return this.vendorNexusId;
    }
    
    public void setVendorNexusId(Long vendorNexusId) {
    	this.vendorNexusId = vendorNexusId;
    }
    
    public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
    
    public String getNexusTypeCode() {
        return this.nexusTypeCode;
    }
    
    public void setNexusTypeCode(String nexusTypeCode) {
    	this.nexusTypeCode = nexusTypeCode;
    }
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  

    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
}