package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class DbPropertiesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer dbId;
    private String userName;   
    private String password; 
    
    private String databaseName;  
    private String driverClassName;  
    private String url;  

	
    public DbPropertiesDTO() { 
    	
    }

	public Integer getDbId() {
		return dbId;
	}

	public void setDbId(Integer dbId) {
		this.dbId = dbId;
	}

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("dbId :\t").append(dbId);
    	sb.append("userName :\t").append(userName);
    	sb.append("password :\t").append(password);

    	return sb.toString();
    }
    
}







