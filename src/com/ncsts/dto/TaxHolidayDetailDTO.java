package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Transient;

import org.springframework.beans.BeanUtils;

import com.ncsts.domain.TaxHolidayDetail;

public class TaxHolidayDetailDTO implements Serializable {
	private static final long serialVersionUID = 1318470152133640111L;	
		
    private String taxHolidayCode;    
    private String taxcodeCode;     
    private String taxcodeCounty;     
    private String taxcodeCity;  

   
    public TaxHolidayDetailDTO() { 
    }
    
    // Idable interface
	public String getId() {
		return taxHolidayCode;
	}

    public void setId(String id) {
    	this.taxHolidayCode = id;
    }
    
	public String getIdPropertyName() {
    	return "taxHolidayCode";
    }
    
	public TaxHolidayDetailDTO getTaxHolidayDetailDTO() {
		TaxHolidayDetailDTO taxHolidayDetailDTO = new TaxHolidayDetailDTO();
		BeanUtils.copyProperties(this, taxHolidayDetailDTO);
		return taxHolidayDetailDTO;
	}
    
    public String getTaxHolidayCode() {
        return this.taxHolidayCode;
    }
    
    public void setTaxHolidayCode(String taxHolidayCode) {
    	this.taxHolidayCode = taxHolidayCode;
    }
    
    public String getTaxcodeCode() {
        return this.taxcodeCode;
    }
    
    public void setTaxcodeCode(String taxcodeCode) {
    	this.taxcodeCode = taxcodeCode;
    }  
    
    public String getTaxcodeCounty() {
        return this.taxcodeCounty;
    }
    
    public void setTaxcodeCounty(String taxcodeCounty) {
    	this.taxcodeCounty = taxcodeCounty;
    }    
    
    public String getTaxcodeCity() {
        return this.taxcodeCity;
    }
    
    public void setTaxcodeCity(String taxcodeCity) {
    	this.taxcodeCity = taxcodeCity;
    }      
    
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof TaxHolidayDetailDTO)) {
            return false;
        }

        final TaxHolidayDetailDTO revision = (TaxHolidayDetailDTO)other;

        String c1 = getTaxHolidayCode();
        String c2 = revision.getTaxHolidayCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getTaxHolidayCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.taxHolidayCode + "::";
    }
    
}




