package com.ncsts.dto;

public class ErrorCodeDTO {

	private String av_severity_level;
	
	private char av_write_import_line_flag;
	
	private char av_abort_import_flag;
	
	public ErrorCodeDTO(String av_severity_level, char av_write_import_line_flag, char av_abort_import_flag) {
		this.av_severity_level = av_severity_level;
		this.av_write_import_line_flag = av_write_import_line_flag;
		this.av_abort_import_flag = av_abort_import_flag;
	}
	
	public Integer getSeverityLevelAsInt() {
		Integer result = Integer.valueOf(this.av_severity_level);
		return (null==result)?0:result;
	}
	
	public String getAv_severity_level() {
		return av_severity_level;
	}
	
	public void setAv_severity_level(String av_severity_level) {
		this.av_severity_level = av_severity_level;
	}
	
	public char getAv_write_import_line_flag() {
		return av_write_import_line_flag;
	}
	
	public void setAv_write_import_line_flag(char av_write_import_line_flag) {
		this.av_write_import_line_flag = av_write_import_line_flag;
	}
	
	public char getAv_abort_import_flag() {
		return av_abort_import_flag;
	}
	
	public void setAv_abort_import_flag(char av_abort_import_flag) {
		this.av_abort_import_flag = av_abort_import_flag;
	}
	
}
