package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class SitusDriverDetailDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
private Long situsDriverDetailId;
    	
    private Long situsDriverId;
    private String locationTypeCode;  
    private Long driverId;  
    private String mandatoryFlag;    
    private String customMandatoryFlag;  
    private String updateUserId;
    private Date updateTimestamp; 
        
	public SitusDriverDetailDTO() {   	
    }
	
	public Long getSitusDriverDetailId() {
        return this.situsDriverDetailId;
    }
    
    public void setSitusDriverDetailId(Long situsDriverDetailId) {
    	this.situsDriverDetailId = situsDriverDetailId;
    }
    
    public Long getSitusDriverId() {
        return this.situsDriverId;
    }
    
    public void setSitusDriverId(Long situsDriverId) {
    	this.situsDriverId = situsDriverId;
    }
    
    public String getLocationTypeCode() {
        return this.locationTypeCode;
    }
    
    public void setLocationTypeCode(String locationTypeCode) {
    	this.locationTypeCode = locationTypeCode;
    } 
    
    public Long getDriverId() {
        return this.driverId;
    }
    
    public void setDriverId(Long driverId) {
    	this.driverId = driverId;
    }

    public String getMandatoryFlag() {
        return this.mandatoryFlag;
    }
    
    public void setMandatoryFlag(String mandatoryFlag) {
    	this.mandatoryFlag = mandatoryFlag;
    }  
    
    public Boolean getMandatoryBooleanFlag() {
    	return "1".equals(this.mandatoryFlag);
	}

	public void setMandatoryBooleanFlag(Boolean mandatoryFlag) {
		this.mandatoryFlag = mandatoryFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getCustomMandatoryFlag() {
        return this.customMandatoryFlag;
    }
    
    public void setCustomMandatoryFlag(String customMandatoryFlag) {
    	this.customMandatoryFlag = customMandatoryFlag;
    }  
    
    public Boolean getCustomMandatoryBooleanFlag() {
    	return "1".equals(this.customMandatoryFlag);
	}

	public void setCustomMandatoryBooleanFlag(Boolean customMandatoryFlag) {
		this.customMandatoryFlag = customMandatoryFlag == Boolean.TRUE ? "1" : "0";
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}

