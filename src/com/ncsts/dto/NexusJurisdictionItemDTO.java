package com.ncsts.dto;

public class NexusJurisdictionItemDTO {
	private String name;	
	private boolean selected;
	private boolean hasNexus;
	private boolean enabled;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isHasNexus() {
		return hasNexus;
	}

	public void setHasNexus(boolean hasNexus) {
		this.hasNexus = hasNexus;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
