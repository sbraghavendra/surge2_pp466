package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class EntityDefaultDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
	private Long entityDefaultId;
	private Long entityId;
	private String defaultCode;
	private String defaultValue;
    private String updateUserId;
    private Date updateTimestamp; 
        

	public EntityDefaultDTO() {   	
    }
	
	public Long getEntityDefaultId() {
		return entityDefaultId;
	}

	public void setEntityDefaultId(Long entityDefaultId) {
		this.entityDefaultId = entityDefaultId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getDefaultCode() {
		return defaultCode;
	}

	public void setDefaultCode(String defaultCode) {
		this.defaultCode = defaultCode;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}






