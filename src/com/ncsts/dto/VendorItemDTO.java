package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class VendorItemDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long vendorId; 
    private String vendorCode;    
    private String vendorName;     
    private String geocode;    
    private String addressLine1;   
    private String addressLine2;  
    private String city;   
    private String county;  
    private String state; 
    private String zip;  
    private String zipplus4; 
    private String country;   
    private String activeFlag; 
    private String updateUserId;
    private Date updateTimestamp;
    
	public VendorItemDTO() {  	
    }
	
	public Long getVendorId() {
        return this.vendorId;
    }
    
    public void setVendorId(Long vendorId) {
    	this.vendorId = vendorId;
    }
    
    public String getVendorCode() {
        return this.vendorCode;
    }
    
    public void setVendorCode(String vendorCode) {
    	this.vendorCode = vendorCode;
    }    
    
    public String getVendorName() {
        return this.vendorName;
    }
    
    public void setVendorName(String vendorName) {
    	this.vendorName = vendorName;
    }      
    
    public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
    	this.geocode = geocode;
    }  
    
    public String getAddressLine1() {
        return this.addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
    	this.addressLine1 = addressLine1;
    }  
    
    public String getAddressLine2() {
        return this.addressLine2;
    }
    
    public void setAddressLine2(String addressLine2) {
    	this.addressLine2 = addressLine2;
    }  
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
    	this.city = city;
    }  
    
    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
    	this.county = county;
    }  
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }  
    
    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
    	this.zip = zip;
    }  
    
    public String getZipplus4() {
        return this.zipplus4;
    }
    
    public void setZipplus4(String zipplus4) {
    	this.zipplus4 = zipplus4;
    }  
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  

    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}
