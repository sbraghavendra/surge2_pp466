package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class SitusDriverDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
    private Long situsDriverId;  
    private String transactionTypeCode;     
    private String ratetypeCode;    
    private String methodDeliveryCode;   
    private String customFlag; 
    private String updateUserId;
    private Date updateTimestamp; 
        

	public SitusDriverDTO() {   	
    }
	
	public Long getSitusDriverId() {
        return this.situsDriverId;
    }
    
    public void setSitusDriverId(Long situsDriverId) {
    	this.situsDriverId = situsDriverId;
    }
    
    public String getTransactionTypeCode() {
        return this.transactionTypeCode;
    }
    
    public void setTransactionTypeCode(String transactionTypeCode) {
    	this.transactionTypeCode = transactionTypeCode;
    }    
    
    public String getRatetypeCode() {
        return this.ratetypeCode;
    }
    
    public void setRatetypeCode(String ratetypeCode) {
    	this.ratetypeCode = ratetypeCode;
    }      
    
    public String getMethodDeliveryCode() {
        return this.methodDeliveryCode;
    }
    
    public void setMethodDeliveryCode(String methodDeliveryCode) {
    	this.methodDeliveryCode = methodDeliveryCode;
    }   
    
    public String getCustomFlag() {
        return this.customFlag;
    }
    
    public void setCustomFlag(String customFlag) {
    	this.customFlag = customFlag;
    }  

    public Boolean getCustomBooleanFlag() {
    	return "1".equals(this.customFlag);
	}

	public void setCustomBooleanFlag(Boolean customFlag) {
		this.customFlag = customFlag == Boolean.TRUE ? "1" : "0";
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}






