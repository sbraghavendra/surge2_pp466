package com.ncsts.dto;

import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Role;
import com.ncsts.domain.User;

public class UserEntityRoleDTO {
	
	private static final long serialVersionUID = 1L;

	private EntityItem entityItem;
	
	private User user;
	
	private Role role;
	
	public UserEntityRoleDTO(){
		
	}
	
	public UserEntityRoleDTO(EntityItem entityItem, User user, Role role){
		this.entityItem = entityItem;
		this.user = user;
		this.role = role;
	}

	public EntityItem getEntityItem() {
		return entityItem;
	}

	public User getUser() {
		return user;
	}

	public Role getRole() {
		return role;
	}

	public void setEntityItem(EntityItem entityItem) {
		this.entityItem = entityItem;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	 /**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof UserEntityRoleDTO)) {
            return false;
        }

        final UserEntityRoleDTO revision = (UserEntityRoleDTO)other;

        User c1 = getUser();
        User c2 = revision.getUser();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }
        
        Role d1 = getRole();
        Role d2 = revision.getRole();
        if (!((d1 == d2) || (d1 != null && d1.equals(d2)))) {
            return false;
        }
        
        EntityItem e1 = getEntityItem();
        EntityItem e2 = revision.getEntityItem();
        if (!((e1 == e2) || (e1 != null && e1.equals(e2)))) {
            return false;
        }
        
        

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
		User c1 = getUser();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());    
		Role dnc1 = getRole();
		hash = 31 * hash + (null == dnc1 ? 0 : dnc1.hashCode());
		EntityItem d1 = getEntityItem();
		hash = 31 * hash + (null == d1 ? 0 : d1.hashCode());
		return hash;
    }
	
	public String toString(){		
		return this.getClass()
        .getName() + entityItem.toString() + "-" + role.toString() + "-" + user.toString();
	}
	
}
