package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Muneer Basha
 *
 */

public class DataStatisticsDTO implements Serializable {
	
		
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long dataStatisticsId;
    private String rowTitle;   
    private String rowWhere;      
    private String groupByOnDrilldown;
    private String updateUserId; 
    private Date updateTimestamp;    
	
    public DataStatisticsDTO() { 
    	
    }

	public Long getDataStatisticsId() {
		return dataStatisticsId;
	}

	public void setDataStatisticsId(Long dataStatisticsId) {
		this.dataStatisticsId = dataStatisticsId;
	}

	public String getRowTitle() {
		return rowTitle;
	}
	
	public String getRowTitleHTML() {
		return (rowTitle != null)? rowTitle.replace(" ", "&#160;"):null;
	}

	public void setRowTitle(String rowTitle) {
		this.rowTitle = rowTitle;
	}

	public String getRowWhere() {
		return rowWhere;
	}

	public void setRowWhere(String rowWhere) {
		this.rowWhere = rowWhere;
	}

	public String getGroupByOnDrilldown() {
		return groupByOnDrilldown;
	}

	public void setGroupByOnDrilldown(String groupByOnDrilldown) {
		this.groupByOnDrilldown = groupByOnDrilldown;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}   
    
    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
 
    	sb.append("dataStatisticsId :\t").append(dataStatisticsId);
    	sb.append("rowTitle :\t").append(rowTitle);
    	sb.append("rowWhere :\t").append(rowWhere);
    	sb.append("groupByOnDrilldown :\t").append(groupByOnDrilldown);
    	sb.append("updateUserId :\t").append(updateUserId);
    	//sb.append("updateTimestamp :\t").append(updateTimestamp);
    	
    	return sb.toString();
    }
    
}







