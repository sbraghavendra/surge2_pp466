package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Paul Govindan
 *
 */
public class UserDTO implements Serializable{
	
	private static final long serialVersionUID = 8953767585322548149L;	
	
    private String userCode;
    private String userName;  
    private String userRole;
    private Long lastUsedEntityId;     
    private String defaultMatrixLineFlag;
    private String globalBuFlag;     
    private String adminFlag;     
    private String activeFlag;    
    private String addNexusFlag;
    private String updateUserId;
    private Date updateTimestamp;    
    private String lastModule;   
    private String globalViewFlag;
    private String lastPurchView;
    
	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserRole() {
		return userRole;
	}
	
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
    public Long getLastUsedEntityId() {
        return this.lastUsedEntityId;
    }
    
    public void setLastUsedEntityId(Long lastUsedEntityId) {
    	this.lastUsedEntityId = lastUsedEntityId;
    }    
    
    public String getDefaultMatrixLineFlag() {
        return this.defaultMatrixLineFlag;
    }
    
    public void setDefaultMatrixLineFlag(String defaultMatrixLineFlag) {
    	this.defaultMatrixLineFlag = defaultMatrixLineFlag;
    }     

    public String getGlobalBuFlag() {
        return this.globalBuFlag;
    }
    
    public void setGlobalBuFlag(String globalBuFlag) {
    	this.globalBuFlag = globalBuFlag;
    }  
    
    public String getAddNexusFlag() {
        return this.addNexusFlag;
    }
    
    public void setAddNexusFlag(String addNexusFlag) {
    	this.addNexusFlag = addNexusFlag;
    }
    
    public String getAdminFlag() {
        return this.adminFlag;
    }
    
    public void setAdminFlag(String adminFlag) {
    	this.adminFlag = adminFlag;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    } 	

    public Boolean getActiveFlagBoolean() {
		//return (activeFlag == null)? null : activeFlag.equals("1");
		return "1".equalsIgnoreCase(activeFlag);
	}

	public void setActiveFlagBoolean(Boolean activeFlag) {
		//setActiveFlag((activeFlag == null) ?  null : ((activeFlag) ? "1" : "0"));
		setActiveFlag(activeFlag ? "1" : "0");
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public String getLastModule() {
		return lastModule;
	}

	public void setLastModule(String lastModule) {
		this.lastModule = lastModule;
	}
	
    public String getGlobalViewFlag() {
		return globalViewFlag;
	}

	public void setGlobalViewFlag(String globalViewFlag) {
		this.globalViewFlag = globalViewFlag;
	}
	
	public String getLastPurchView() {
		return lastPurchView;
	}

	public void setLastPurchView(String lastPurchView) {
		this.lastPurchView = lastPurchView;
	}
}
