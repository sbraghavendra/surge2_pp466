package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class TransactionBlobDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long fileId;
	private String fileName;
	private String userName;
	private Date createDate;
	private Long rowCount;
	private Double fileSize;

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public Long getRowCount() {
		return rowCount;
	}

	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	public void setFileSize(Double fileSize) {
		this.fileSize = fileSize;
	}

	public Double getFileSize() {
		return fileSize;
	}

}

