package com.ncsts.dto;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class SitusInfoDTO {

    private String situsDriver;
    private String countryJur;
    private String stateJur;
    private String countyJur;
    private String cityJur;
    private String stj1Jur;
    private String stj2Jur;
    private String stj3Jur;
    private String stj4Jur;
    private String stj5Jur;


    public String getSitusDriver() {
        return situsDriver;
    }

    public void setSitusDriver(String situsDriver) {
        this.situsDriver = situsDriver;
    }

    public String getCountryJur() {
        return countryJur;
    }

    public void setCountryJur(String countryJur) {
        this.countryJur = countryJur;
    }

    public String getStateJur() {
        return stateJur;
    }

    public void setStateJur(String stateJur) {
        this.stateJur = stateJur;
    }

    public String getCountyJur() {
        return countyJur;
    }

    public void setCountyJur(String countyJur) {
        this.countyJur = countyJur;
    }

    public String getCityJur() {
        return cityJur;
    }

    public void setCityJur(String cityJur) {
        this.cityJur = cityJur;
    }

    public String getStj1Jur() {
        return stj1Jur;
    }

    public void setStj1Jur(String stj1Jur) {
        this.stj1Jur = stj1Jur;
    }

    public String getStj2Jur() {
        return stj2Jur;
    }

    public void setStj2Jur(String stj2Jur) {
        this.stj2Jur = stj2Jur;
    }

    public String getStj3Jur() {
        return stj3Jur;
    }

    public void setStj3Jur(String stj3Jur) {
        this.stj3Jur = stj3Jur;
    }

    public String getStj4Jur() {
        return stj4Jur;
    }

    public void setStj4Jur(String stj4Jur) {
        this.stj4Jur = stj4Jur;
    }

    public String getStj5Jur() {
        return stj5Jur;
    }

    public void setStj5Jur(String stj5Jur) {
        this.stj5Jur = stj5Jur;
    }
}
