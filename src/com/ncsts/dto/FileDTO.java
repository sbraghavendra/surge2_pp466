package com.ncsts.dto;

public class FileDTO {
	
	private String filepath;
	private String filename;
	private String parentdir;
	private String filetype;
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public String getParentdir() {
		return parentdir;
	}
	public void setParentdir(String parentdir) {
		this.parentdir = parentdir;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

}
