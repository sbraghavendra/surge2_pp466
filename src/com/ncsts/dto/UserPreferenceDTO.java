package com.ncsts.dto;

import java.io.Serializable;

public class UserPreferenceDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String gridLayout;
	private String userTransInd;
	private String userTaxCode;
	private boolean aolEnabled;
	private String aolWhen;
	private String aolWhich;
	private String userTimeZoneID;
	private String userCountry;
	private boolean userCountryFilter;
	private String userEntity;
	private boolean userEntityFilter;
	private String userState;
	private boolean userStateFilter;
	private String userTaxType;
	private String userRateType;
	private String module;
	private String maxdecimalplace;
	private boolean useTaxCodeLookup;
	
	public String getMaxdecimalplace() {
		return maxdecimalplace;
	}
	public void setMaxdecimalplace(String maxdecimalplace) {
		this.maxdecimalplace = maxdecimalplace;
	}
	
	public String getUserTransInd() {
		return userTransInd;
	}
	public void setUserTransInd(String userTransInd) {
		this.userTransInd = userTransInd;
	}
	public String getUserTaxCode() {
		return userTaxCode;
	}
	public void setUserTaxCode(String userTaxCode) {
		this.userTaxCode = userTaxCode;
	}
	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public boolean getUserCountryFilter() {
		return userCountryFilter;
	}
	public void setUserCountryFilter(boolean userCountryFilter) {
		this.userCountryFilter = userCountryFilter;
	}
	public String getUserEntity() {
		return userEntity;
	}
	public void setUserEntity(String userEntity) {
		this.userEntity = userEntity;
	}
	public boolean getUserEntityFilter() {
		return userEntityFilter;
	}
	public void setUserEntityFilter(boolean userEntityFilter) {
		this.userEntityFilter = userEntityFilter;
	}
	public String getUserState() {
		return userState;
	}
	public void setUserState(String userState) {
		this.userState = userState;
	}
	public boolean getUserStateFilter() {
		return userStateFilter;
	}
	public void setUserStateFilter(boolean userStateFilter) {
		this.userStateFilter = userStateFilter;
	}
	public String getGridLayout() {
		return gridLayout;
	}
	public void setGridLayout(String gridLayout) {
		this.gridLayout = gridLayout;
	}
	public boolean isAolEnabled() {
		return aolEnabled;
	}
	public void setAolEnabled(boolean aolEnabled) {
		this.aolEnabled = aolEnabled;
	}
	public String getAolWhen() {
		return aolWhen;
	}
	public void setAolWhen(String aolWhen) {
		this.aolWhen = aolWhen;
	}
	public String getAolWhich() {
		return aolWhich;
	}
	public void setAolWhich(String aolWhich) {
		this.aolWhich = aolWhich;
	}
	public String getUserTimeZoneID() {
		return userTimeZoneID;
	}
	public void setUserTimeZoneID(String userTimeZoneID) {
		this.userTimeZoneID = userTimeZoneID;
	}
	public String getUserTaxType() {
		return userTaxType;
	}
	public void setUserTaxType(String userTaxType) {
		this.userTaxType = userTaxType;
	}
	public String getUserRateType() {
		return userRateType;
	}
	public void setUserRateType(String userRateType) {
		this.userRateType = userRateType;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public boolean getUseTaxCodeLookup() {
		return this.useTaxCodeLookup;
	}
	public void setUseTaxCodeLookup(boolean useTaxCodeLookup) {
		this.useTaxCodeLookup = useTaxCodeLookup;
	}
}

