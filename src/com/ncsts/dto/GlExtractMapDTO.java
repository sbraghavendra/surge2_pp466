package com.ncsts.dto;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.io.Serializable;
import java.util.Date;
import javax.faces.context.FacesContext;
import javax.faces.component.UIInput;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class GlExtractMapDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long glExtractMapId;
	private String taxCodeStateCode;
	private String taxCodeTypeCode;
	private String taxCodeCode;
	private String taxJurisTypeCode;
	private String driver01; 
	private String driver02;
	private String driver03;
	private String driver04;
	private String driver05;
	private String driver06;
	private String driver07;
	private String driver08;
	private String driver09;
	private String driver10;
	private String comments;
	private String updateUserId;
	private Date updateTimeStamp;
	
	public Long getGlExtractMapId() {
		return glExtractMapId;
	}
	public void setGlExtractMapId(Long glExtractMapId) {
		this.glExtractMapId = glExtractMapId;
	}
	public String getTaxCodeStateCode() {
		return taxCodeStateCode;
	}
	public void setTaxCodeStateCode(String taxCodeStateCode) {
		this.taxCodeStateCode = taxCodeStateCode;
	}
	public String getTaxCodeTypeCode() {
		return taxCodeTypeCode;
	}
	public void setTaxCodeTypeCode(String taxCodeTypeCode) {
		this.taxCodeTypeCode = taxCodeTypeCode;
	}
	public String getTaxCodeCode() {
		return taxCodeCode;
	}
	public void setTaxCodeCode(String taxCodeCode) {
		this.taxCodeCode = taxCodeCode;
	}
	public String getTaxJurisTypeCode() {
		return taxJurisTypeCode;
	}
	public void setTaxJurisTypeCode(String taxJurisTypeCode) {
		this.taxJurisTypeCode = taxJurisTypeCode;
	}
	
	public String getDriver01() {
		return driver01;
	}
	public void setDriver01(String driver01) {
		this.driver01 = driver01;
	}
	public String getDriver02() {
		return driver02;
	}
	public void setDriver02(String driver02) {
		this.driver02 = driver02;
	}
	public String getDriver03() {
		return driver03;
	}
	public void setDriver03(String driver03) {
		this.driver03 = driver03;
	}
	public String getDriver04() {
		return driver04;
	}
	public void setDriver04(String driver04) {
		this.driver04 = driver04;
	}
	public String getDriver05() {
		return driver05;
	}
	public void setDriver05(String driver05) {
		this.driver05 = driver05;
	}
	public String getDriver06() {
		return driver06;
	}
	public void setDriver06(String driver06) {
		this.driver06 = driver06;
	}
	public String getDriver07() {
		return driver07;
	}
	public void setDriver07(String driver07) {
		this.driver07 = driver07;
	}
	public String getDriver08() {
		return driver08;
	}
	public void setDriver08(String driver08) {
		this.driver08 = driver08;
	}
	public String getDriver09() {
		return driver09;
	}
	public void setDriver09(String driver09) {
		this.driver09 = driver09;
	}
	public String getDriver10() {
		return driver10;
	}
	public void setDriver10(String driver10) {
		this.driver10 = driver10;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateTimeStamp() {
		return updateTimeStamp;
	}
	public void setUpdateTimeStamp(Date updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}
	public void validateStateTaxCodeValue(FacesContext context, UIComponent toValidate, Object value) {
		
		/* Get the string value of the current field*/ 
		String stateTaxCodeValue  = (String)value; 
			 	
	    if (stateTaxCodeValue.equals( "-1")){
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("State is required. Please select a state.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context),message);
	    }else{
	    	setTaxCodeStateCode(stateTaxCodeValue);
	    }
	}
	
	public void validateJurisdictionTypeCodeValue(FacesContext context, UIComponent toValidate, Object value) {
		
		 /*Get the string value of the current field */
		String jurisdictionTypeCodeValue  = (String)value; 
			 	
	    if (jurisdictionTypeCodeValue.equals( "-1")){
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Jurisdiction Type Code is required. Please select a Jurisdiction Type Code.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context),message);
	    }else{
	    	if (taxCodeStateCode != null){
		    	if (!taxCodeStateCode.equals("*ALL")){
		    		if(!jurisdictionTypeCodeValue.equals("*ALL")){
		    			((UIInput)toValidate).setValid(false);
		    			FacesMessage message = new FacesMessage("Jurisdiction Type Code must be All when only a state is selected. Please select All as the Jurisdiction Type Code.");
		    			message.setSeverity(FacesMessage.SEVERITY_ERROR);
		    			context.addMessage(toValidate.getClientId(context),message);
		    		}
		    	}
	    	}
	    }
	}
}
