package com.ncsts.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.jsf.model.TransactionDataModel;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.jsf.model.BaseExtendedDataModel;

public class ColumnItem {
	private String field = "";
	private String label = "";
	private boolean ascending = true;
	
	public ColumnItem(String field, String label, boolean ascending) {
		this.field = field;
		this.label = label;
		this.ascending = ascending;
	}
	
	public ColumnItem(String field, String label) {
		this.field = field;
		this.label = label;
	}
	
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public boolean getAscending() {
		return ascending;
	}
	
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}
	
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof ColumnItem)) {
            return false;
        }

        final ColumnItem revision = (ColumnItem)other;

        String c1 = getField();
        String c2 = revision.getField();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
}