package com.ncsts.dto;

import java.io.Serializable;
/**
 * 
 * @author Muneer
 *
 */
	public class ImportSpecUserDTO implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		private String importSpecType; 
		private String importSpecCode;		
		private String userCode;

		public String getImportSpecType() {
			return importSpecType;
		}
		public void setImportSpecType(String importSpecType) {
			this.importSpecType = importSpecType;
		}
		public String getImportSpecCode() {
			return importSpecCode;
		}
		public void setImportSpecCode(String importSpecCode) {
			this.importSpecCode = importSpecCode;
		}
		public String getUserCode() {
			return userCode;
		}
		public void setUserCode(String userCode) {
			this.userCode = userCode;
		}
		
	
	}

