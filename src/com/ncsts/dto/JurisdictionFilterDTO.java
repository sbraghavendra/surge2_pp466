package com.ncsts.dto;

public class JurisdictionFilterDTO {
    private String country;
    private String state;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getJurLevel() {
        return jurLevel;
    }

    public void setJurLevel(String jurLevel) {
        this.jurLevel = jurLevel;
    }

    private String jurLevel;
}
