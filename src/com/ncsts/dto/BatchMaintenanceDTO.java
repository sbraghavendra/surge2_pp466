package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Muneer
 *
 */
	public class BatchMaintenanceDTO implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		private Long batchId;
		private String heldFlag;
		private String someThingElse;
		private String batchTypeDesc;
		private String batchStatusDesc;
		
		private String batchTypeCode;
	    private Date entryTimestamp;
	    private String batchStatusCode;
	    private String heldFlagTOTAL_BYTES;
	    private String errorSevCode;
	    private Long totalBytes;
	    private Long totalRows;
	    private Long startRow;
	    private Long processedBytes;
	    private Long processedRows;
	    private String driver11;
	    private Date schedStartTimestamp;
	    private Date actualStartTimestamp;
	    private Date actualEndTimestamp;
	    private String vc01;
	    private String vc02;
	    private String vc03;
	    private String vc04;
	    private String vc05;
	    private String vc06;
	    private String vc07;
	    private String vc08;
	    private String vc09;
	    private String vc10;
	    private Double nu01;
	    private Double nu02;     
	    private Double nu03;
	    private Double nu04;
	    private Double nu05;
	    private Double nu06;
	    private Double nu07;
	    private Double nu08;
	    private Double nu09;
	    private Double nu10;
	    private Date ts01;
	    private Date ts02;
	    private Date ts03;
	    private Date ts04;
	    private Date ts05;
	    private Date ts06;
	    private Date ts07;
	    private Date ts08;
	    private Date ts09;
	    private Date ts10;
	    private String statusUpdateUserId;
	    private Date statusUpdateTimestamp;
	    private String updateUserId;
	    private Date updateTimestamp;
	    
	    private String heldCode;
	    private String yearPeriod;
		
	    public Long getBatchId() {
			return batchId;
		}

		public void setBatchId(Long batchId) {
			this.batchId = batchId;
		}


		public String getHeldFlag() {
			return heldFlag;
		}

		public void setHeldFlag(String heldFlag) {
			this.heldFlag = heldFlag;
		}

		public Boolean getHeldFlagBoolean() {
			return (heldFlag == null)? null : heldFlag.equals("1");
		}

		public void setHeldFlagBoolean(Boolean heldFlag) {
			setHeldFlag((heldFlag == null) ?  null : ((heldFlag) ? "1" : "0"));
		}

		public String getSomeThingElse() {
			return someThingElse;
		}

		public void setSomeThingElse(String someThingElse) {
			this.someThingElse = someThingElse;
		}

		public String getBatchTypeCode() {
			return batchTypeCode;
		}

		public void setBatchTypeCode(String batchTypeCode) {
			this.batchTypeCode = batchTypeCode;
		}

		public Date getEntryTimestamp() {
			return entryTimestamp;
		}

		public void setEntryTimestamp(Date entryTimestamp) {
			this.entryTimestamp = entryTimestamp;
		}

		public String getBatchStatusCode() {
			return batchStatusCode;
		}

		public void setBatchStatusCode(String batchStatusCode) {
			this.batchStatusCode = batchStatusCode;
		}

		public String getHeldFlagTOTAL_BYTES() {
			return heldFlagTOTAL_BYTES;
		}

		public void setHeldFlagTOTAL_BYTES(String heldFlagTOTAL_BYTES) {
			this.heldFlagTOTAL_BYTES = heldFlagTOTAL_BYTES;
		}

		public String getErrorSevCode() {
			return errorSevCode;
		}

		public void setErrorSevCode(String errorSevCode) {
			this.errorSevCode = errorSevCode;
		}

		public Long getTotalBytes() {
			return totalBytes;
		}

		public void setTotalBytes(Long totalBytes) {
			this.totalBytes = totalBytes;
		}

		public Long getTotalRows() {
			return totalRows;
		}

		public void setTotalRows(Long totalRows) {
			this.totalRows = totalRows;
		}

		public Long getStartRow() {
			return startRow;
		}

		public void setStartRow(Long startRow) {
			this.startRow = startRow;
		}

		public Long getProcessedBytes() {
			return processedBytes;
		}

		public void setProcessedBytes(Long processedBytes) {
			this.processedBytes = processedBytes;
		}

		public Long getProcessedRows() {
			return processedRows;
		}

		public void setProcessedRows(Long processedRows) {
			this.processedRows = processedRows;
		}

		public String getDriver11() {
			return driver11;
		}

		public void setDriver11(String driver11) {
			this.driver11 = driver11;
		}

		public Date getSchedStartTimestamp() {
			return schedStartTimestamp;
		}

		public void setSchedStartTimestamp(Date schedStartTimestamp) {
			this.schedStartTimestamp = schedStartTimestamp;
		}

		public Date getActualStartTimestamp() {
			return actualStartTimestamp;
		}

		public void setActualStartTimestamp(Date actualStartTimestamp) {
			this.actualStartTimestamp = actualStartTimestamp;
		}

		public Date getActualEndTimestamp() {
			return actualEndTimestamp;
		}

		public void setActualEndTimestamp(Date actualEndTimestamp) {
			this.actualEndTimestamp = actualEndTimestamp;
		}

		public String getVc01() {
			return vc01;
		}

		public void setVc01(String vc01) {
			this.vc01 = vc01;
		}

		public String getVc02() {
			return vc02;
		}

		public void setVc02(String vc02) {
			this.vc02 = vc02;
		}

		public String getVc03() {
			return vc03;
		}

		public void setVc03(String vc03) {
			this.vc03 = vc03;
		}

		public String getVc04() {
			return vc04;
		}

		public void setVc04(String vc04) {
			this.vc04 = vc04;
		}

		public String getVc05() {
			return vc05;
		}

		public void setVc05(String vc05) {
			this.vc05 = vc05;
		}

		public String getVc06() {
			return vc06;
		}

		public void setVc06(String vc06) {
			this.vc06 = vc06;
		}

		public String getVc07() {
			return vc07;
		}

		public void setVc07(String vc07) {
			this.vc07 = vc07;
		}

		public String getVc08() {
			return vc08;
		}

		public void setVc08(String vc08) {
			this.vc08 = vc08;
		}

		public String getVc09() {
			return vc09;
		}

		public void setVc09(String vc09) {
			this.vc09 = vc09;
		}

		public String getVc10() {
			return vc10;
		}

		public void setVc10(String vc10) {
			this.vc10 = vc10;
		}
		

		public Double getNu01() {
			return nu01;
		}

		public void setNu01(Double nu01) {
			this.nu01 = nu01;
		}

		public Double getNu02() {
			return nu02;
		}

		public void setNu02(Double nu02) {
			this.nu02 = nu02;
		}

		public Double getNu03() {
			return nu03;
		}

		public void setNu03(Double nu03) {
			this.nu03 = nu03;
		}

		public Double getNu04() {
			return nu04;
		}

		public void setNu04(Double nu04) {
			this.nu04 = nu04;
		}

		public Double getNu05() {
			return nu05;
		}

		public void setNu05(Double nu05) {
			this.nu05 = nu05;
		}

		public Double getNu06() {
			return nu06;
		}

		public void setNu06(Double nu06) {
			this.nu06 = nu06;
		}

		public Double getNu07() {
			return nu07;
		}

		public void setNu07(Double nu07) {
			this.nu07 = nu07;
		}

		public Double getNu08() {
			return nu08;
		}

		public void setNu08(Double nu08) {
			this.nu08 = nu08;
		}

		public Double getNu09() {
			return nu09;
		}

		public void setNu09(Double nu09) {
			this.nu09 = nu09;
		}

		public Double getNu10() {
			return nu10;
		}

		public void setNu10(Double nu10) {
			this.nu10 = nu10;
		}

		public Date getTs01() {
			return ts01;
		}

		public void setTs01(Date ts01) {
			this.ts01 = ts01;
		}

		public Date getTs02() {
			return ts02;
		}

		public void setTs02(Date ts02) {
			this.ts02 = ts02;
		}

		public Date getTs03() {
			return ts03;
		}

		public void setTs03(Date ts03) {
			this.ts03 = ts03;
		}

		public Date getTs04() {
			return ts04;
		}

		public void setTs04(Date ts04) {
			this.ts04 = ts04;
		}

		public Date getTs05() {
			return ts05;
		}

		public void setTs05(Date ts05) {
			this.ts05 = ts05;
		}

		public Date getTs06() {
			return ts06;
		}

		public void setTs06(Date ts06) {
			this.ts06 = ts06;
		}

		public Date getTs07() {
			return ts07;
		}

		public void setTs07(Date ts07) {
			this.ts07 = ts07;
		}

		public Date getTs08() {
			return ts08;
		}

		public void setTs08(Date ts08) {
			this.ts08 = ts08;
		}

		public Date getTs09() {
			return ts09;
		}

		public void setTs09(Date ts09) {
			this.ts09 = ts09;
		}

		public Date getTs10() {
			return ts10;
		}

		public void setTs10(Date ts10) {
			this.ts10 = ts10;
		}

		public String getStatusUpdateUserId() {
			return statusUpdateUserId;
		}

		public void setStatusUpdateUserId(String statusUpdateUserId) {
			this.statusUpdateUserId = statusUpdateUserId;
		}

		public Date getStatusUpdateTimestamp() {
			return statusUpdateTimestamp;
		}

		public void setStatusUpdateTimestamp(Date statusUpdateTimestamp) {
			this.statusUpdateTimestamp = statusUpdateTimestamp;
		}

		public String getUpdateUserId() {
			return updateUserId;
		}

		public void setUpdateUserId(String updateUserId) {
			this.updateUserId = updateUserId;
		}

		public Date getUpdateTimestamp() {
			return updateTimestamp;
		}

		public void setUpdateTimestamp(Date updateTimestamp) {
			this.updateTimestamp = updateTimestamp;
		}

		public String getBatchTypeDesc() {
			return batchTypeDesc;
		}

		public void setBatchTypeDesc(String batchTypeDesc) {
			this.batchTypeDesc = batchTypeDesc;
		}

		public String getBatchStatusDesc() {
			return batchStatusDesc;
		}

		public void setBatchStatusDesc(String batchStatusDesc) {
			this.batchStatusDesc = batchStatusDesc;
		}

		public String getHeldCode() {
			return heldCode;
		}

		public void setHeldCode(String heldCode) {
			this.heldCode = heldCode;
		}

		public String getYearPeriod() {
			return yearPeriod;
		}

		public void setYearPeriod(String yearPeriod) {
			this.yearPeriod = yearPeriod;
		}
		
	    //private Jurisdiction jurisdiction;
		
	}

