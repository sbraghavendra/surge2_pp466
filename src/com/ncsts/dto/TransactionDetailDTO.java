package com.ncsts.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionDetailDTO {
	
	/** default constructor */
	public TransactionDetailDTO(){
		
	}

	private Long purchtransId;

	public Long getPurchtransId() {
		return purchtransId;
	}

	public void setPurchtransId(Long purchtransId) {
		this.purchtransId = purchtransId;
	}
	
	 private String processStatus;

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}
	

	private String processtypeCode;

	public String getProcesstypeCode() {
		return processtypeCode;
	}

	public void setProcesstypeCode(String processtypeCode) {
		this.processtypeCode = processtypeCode;
	}

	private Long bcpTransactionId;

	public Long getBcpTransactionId() {
		return bcpTransactionId;
	}

	public void setBcpTransactionId(Long bcpTransactionId) {
		this.bcpTransactionId = bcpTransactionId;
	}

	private Long processBatchNo;

	public Long getProcessBatchNo() {
		return processBatchNo;
	}

	public void setProcessBatchNo(Long processBatchNo) {
		this.processBatchNo = processBatchNo;
	}

	private Long glExtractBatchNo;

	public Long getGlExtractBatchNo() {
		return glExtractBatchNo;
	}

	public void setGlExtractBatchNo(Long glExtractBatchNo) {
		this.glExtractBatchNo = glExtractBatchNo;
	}

	private Date enteredDate;

	public Date getEnteredDate() {
		return enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	private Date loadTimestamp;

	public Date getLoadTimestamp() {
		return loadTimestamp;
	}

	public void setLoadTimestamp(Date loadTimestamp) {
		this.loadTimestamp = loadTimestamp;
	}

	private String transactionStateCode;

	public String getTransactionStateCode() {
		return transactionStateCode;
	}

	public void setTransactionStateCode(String transactionStateCode) {
		this.transactionStateCode = transactionStateCode;
	}

	private String autoTransactionStateCode;

	public String getAutoTransactionStateCode() {
		return autoTransactionStateCode;
	}

	public void setAutoTransactionStateCode(String autoTransactionStateCode) {
		this.autoTransactionStateCode = autoTransactionStateCode;
	}

	private String transactionCountryCode;

	public String getTransactionCountryCode() {
		return transactionCountryCode;
	}

	public void setTransactionCountryCode(String transactionCountryCode) {
		this.transactionCountryCode = transactionCountryCode;
	}

	private String autoTransactionCountryCode;

	public String getAutoTransactionCountryCode() {
		return autoTransactionCountryCode;
	}

	public void setAutoTransactionCountryCode(String autoTransactionCountryCode) {
		this.autoTransactionCountryCode = autoTransactionCountryCode;
	}

	private String transactionInd;

	public String getTransactionInd() {
		return transactionInd;
	}

	public void setTransactionInd(String transactionInd) {
		this.transactionInd = transactionInd;
	}

	private String suspendInd;

	public String getSuspendInd() {
		return suspendInd;
	}

	public void setSuspendInd(String suspendInd) {
		this.suspendInd = suspendInd;
	}

	private String directPayPermitFlag;

	public String getDirectPayPermitFlag() {
		return directPayPermitFlag;
	}

	public void setDirectPayPermitFlag(String directPayPermitFlag) {
		this.directPayPermitFlag = directPayPermitFlag;
	}

	private String calculateInd;

	public String getCalculateInd() {
		return calculateInd;
	}

	public void setCalculateInd(String calculateInd) {
		this.calculateInd = calculateInd;
	}

	private Long entityId;

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	private String entityCode;

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	private Long unlockEntityId;

	public Long getUnlockEntityId() {
		return unlockEntityId;
	}

	public void setUnlockEntityId(Long unlockEntityId) {
		this.unlockEntityId = unlockEntityId;
	}

	private String cpFilingEntityCode;

	public String getCpFilingEntityCode() {
		return cpFilingEntityCode;
	}

	public void setCpFilingEntityCode(String cpFilingEntityCode) {
		this.cpFilingEntityCode = cpFilingEntityCode;
	}

	private Long allocationMatrixId;

	public Long getAllocationMatrixId() {
		return allocationMatrixId;
	}

	public void setAllocationMatrixId(Long allocationMatrixId) {
		this.allocationMatrixId = allocationMatrixId;
	}

	private Long allocationSubtransId;

	public Long getAllocationSubtransId() {
		return allocationSubtransId;
	}

	public void setAllocationSubtransId(Long allocationSubtransId) {
		this.allocationSubtransId = allocationSubtransId;
	}

	private Long taxAllocMatrixId;

	public Long getTaxAllocMatrixId() {
		return taxAllocMatrixId;
	}

	public void setTaxAllocMatrixId(Long taxAllocMatrixId) {
		this.taxAllocMatrixId = taxAllocMatrixId;
	}

	private Long splitSubtransId;

	public Long getSplitSubtransId() {
		return splitSubtransId;
	}

	public void setSplitSubtransId(Long splitSubtransId) {
		this.splitSubtransId = splitSubtransId;
	}

	private String multiTransCode;

	public String getMultiTransCode() {
		return multiTransCode;
	}

	public void setMultiTransCode(String multiTransCode) {
		this.multiTransCode = multiTransCode;
	}

	private String taxcodeCode;

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	private String manualTaxcodeInd;

	public String getManualTaxcodeInd() {
		return manualTaxcodeInd;
	}

	public void setManualTaxcodeInd(String manualTaxcodeInd) {
		this.manualTaxcodeInd = manualTaxcodeInd;
	}

	private Long taxMatrixId;

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	private String procruleSessionId;

	public String getProcruleSessionId() {
		return procruleSessionId;
	}

	public void setProcruleSessionId(String procruleSessionId) {
		this.procruleSessionId = procruleSessionId;
	}

	private String sourceTransactionId;

	public String getSourceTransactionId() {
		return sourceTransactionId;
	}

	public void setSourceTransactionId(String sourceTransactionId) {
		this.sourceTransactionId = sourceTransactionId;
	}

	private String transactionStatus;

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	private String comments;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	private String urlLink;

	public String getUrlLink() {
		return urlLink;
	}

	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}

	private String processingNotes;

	public String getProcessingNotes() {
		return processingNotes;
	}

	public void setProcessingNotes(String processingNotes) {
		this.processingNotes = processingNotes;
	}

	private String rejectFlag;

	public String getRejectFlag() {
		return rejectFlag;
	}

	public void setRejectFlag(String rejectFlag) {
		this.rejectFlag = rejectFlag;
	}

	private Date glDate;

	public Date getGlDate() {
		return glDate;
	}

	public void setGlDate(Date glDate) {
		this.glDate = glDate;
	}

	private String glCompanyNbr;

	public String getGlCompanyNbr() {
		return glCompanyNbr;
	}

	public void setGlCompanyNbr(String glCompanyNbr) {
		this.glCompanyNbr = glCompanyNbr;
	}

	private String glCompanyName;

	public String getGlCompanyName() {
		return glCompanyName;
	}

	public void setGlCompanyName(String glCompanyName) {
		this.glCompanyName = glCompanyName;
	}

	private String glDivisionNbr;

	public String getGlDivisionNbr() {
		return glDivisionNbr;
	}

	public void setGlDivisionNbr(String glDivisionNbr) {
		this.glDivisionNbr = glDivisionNbr;
	}

	private String glDivisionName;

	public String getGlDivisionName() {
		return glDivisionName;
	}

	public void setGlDivisionName(String glDivisionName) {
		this.glDivisionName = glDivisionName;
	}

	private String glCcNbrDeptId;

	public String getGlCcNbrDeptId() {
		return glCcNbrDeptId;
	}

	public void setGlCcNbrDeptId(String glCcNbrDeptId) {
		this.glCcNbrDeptId = glCcNbrDeptId;
	}

	private String glCcNbrDeptName;

	public String getGlCcNbrDeptName() {
		return glCcNbrDeptName;
	}

	public void setGlCcNbrDeptName(String glCcNbrDeptName) {
		this.glCcNbrDeptName = glCcNbrDeptName;
	}

	private String glLocalAcctNbr;

	public String getGlLocalAcctNbr() {
		return glLocalAcctNbr;
	}

	public void setGlLocalAcctNbr(String glLocalAcctNbr) {
		this.glLocalAcctNbr = glLocalAcctNbr;
	}

	private String glLocalAcctName;

	public String getGlLocalAcctName() {
		return glLocalAcctName;
	}

	public void setGlLocalAcctName(String glLocalAcctName) {
		this.glLocalAcctName = glLocalAcctName;
	}

	private String glLocalSubAcctNbr;

	public String getGlLocalSubAcctNbr() {
		return glLocalSubAcctNbr;
	}

	public void setGlLocalSubAcctNbr(String glLocalSubAcctNbr) {
		this.glLocalSubAcctNbr = glLocalSubAcctNbr;
	}

	private String glLocalSubAcctName;

	public String getGlLocalSubAcctName() {
		return glLocalSubAcctName;
	}

	public void setGlLocalSubAcctName(String glLocalSubAcctName) {
		this.glLocalSubAcctName = glLocalSubAcctName;
	}

	private String glFullAcctNbr;

	public String getGlFullAcctNbr() {
		return glFullAcctNbr;
	}

	public void setGlFullAcctNbr(String glFullAcctNbr) {
		this.glFullAcctNbr = glFullAcctNbr;
	}

	private String glFullAcctName;

	public String getGlFullAcctName() {
		return glFullAcctName;
	}

	public void setGlFullAcctName(String glFullAcctName) {
		this.glFullAcctName = glFullAcctName;
	}

	private BigDecimal glLineItmDistAmt;

	public BigDecimal getGlLineItmDistAmt() {
		return glLineItmDistAmt;
	}

	public void setGlLineItmDistAmt(BigDecimal glLineItmDistAmt) {
		this.glLineItmDistAmt = glLineItmDistAmt;
	}

	private String productCode;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	private String productName;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productCode = productName;
	}

	private String vendorNbr;

	public String getVendorNbr() {
		return vendorNbr;
	}

	public void setVendorNbr(String vendorNbr) {
		this.vendorNbr = vendorNbr;
	}

	private String vendorName;

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	private String vendorAddressLine1;

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	private String vendorAddressLine2;

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	private String vendorAddressLine3;

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	private String vendorAddressLine4;

	public String getVendorAddressLine4() {
		return vendorAddressLine4;
	}

	public void setVendorAddressLine4(String vendorAddressLine4) {
		this.vendorAddressLine4 = vendorAddressLine4;
	}

	private String vendorAddressCity;

	public String getVendorAddressCity() {
		return vendorAddressCity;
	}

	public void setVendorAddressCity(String vendorAddressCity) {
		this.vendorAddressCity = vendorAddressCity;
	}

	private String vendorAddressCounty;

	public String getVendorAddressCounty() {
		return vendorAddressCounty;
	}

	public void setVendorAddressCounty(String vendorAddressCounty) {
		this.vendorAddressCounty = vendorAddressCounty;
	}

	private String vendorAddressState;

	public String getVendorAddressState() {
		return vendorAddressState;
	}

	public void setVendorAddressState(String vendorAddressState) {
		this.vendorAddressState = vendorAddressState;
	}

	private String vendorAddressZip;

	public String getVendorAddressZip() {
		return vendorAddressZip;
	}

	public void setVendorAddressZip(String vendorAddressZip) {
		this.vendorAddressZip = vendorAddressZip;
	}

	private String vendorAddressCountry;

	public String getVendorAddressCountry() {
		return vendorAddressCountry;
	}

	public void setVendorAddressCountry(String vendorAddressCountry) {
		this.vendorAddressCountry = vendorAddressCountry;
	}

	private String vendorType;

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	private String vendorTypeName;

	public String getVendorTypeName() {
		return vendorTypeName;
	}

	public void setVendorTypeName(String vendorTypeName) {
		this.vendorTypeName = vendorTypeName;
	}

	private String invoiceNbr;

	public String getInvoiceNbr() {
		return invoiceNbr;
	}

	public void setInvoiceNbr(String invoiceNbr) {
		this.invoiceNbr = invoiceNbr;
	}

	private String invoiceDesc;

	public String getInvoiceDesc() {
		return invoiceDesc;
	}

	public void setInvoiceDesc(String invoiceDesc) {
		this.invoiceDesc = invoiceDesc;
	}

	private Date invoiceDate;

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	private BigDecimal invoiceFreightAmt;

	public BigDecimal getInvoiceFreightAmt() {
		return invoiceFreightAmt;
	}

	public void setInvoiceFreightAmt(BigDecimal invoiceFreightAmt) {
		this.invoiceFreightAmt = invoiceFreightAmt;
	}

	private BigDecimal invoiceDiscountAmt;

	public BigDecimal getInvoiceDiscountAmt() {
		return invoiceDiscountAmt;
	}

	public void setInvoiceDiscountAmt(BigDecimal invoiceDiscountAmt) {
		this.invoiceDiscountAmt = invoiceDiscountAmt;
	}

	private BigDecimal invoiceTaxAmt;

	public BigDecimal getInvoiceTaxAmt() {
		return invoiceTaxAmt;
	}

	public void setInvoiceTaxAmt(BigDecimal invoiceTaxAmt) {
		this.invoiceTaxAmt = invoiceTaxAmt;
	}

	private BigDecimal invoiceTotalAmt;

	public BigDecimal getInvoiceTotalAmt() {
		return invoiceTotalAmt;
	}

	public void setInvoiceTotalAmt(BigDecimal invoiceTotalAmt) {
		this.invoiceTotalAmt = invoiceTotalAmt;
	}

	private String invoiceTaxFlg;

	public String getInvoiceTaxFlg() {
		return invoiceTaxFlg;
	}

	public void setInvoiceTaxFlg(String invoiceTaxFlg) {
		this.invoiceTaxFlg = invoiceTaxFlg;
	}

	private String invoiceLineNbr;

	public String getInvoiceLineNbr() {
		return invoiceLineNbr;
	}

	public void setInvoiceLineNbr(String invoiceLineNbr) {
		this.invoiceLineNbr = invoiceLineNbr;
	}

	private String invoiceLineName;

	public String getInvoiceLineName() {
		return invoiceLineName;
	}

	public void setInvoiceLineName(String invoiceLineName) {
		this.invoiceLineName = invoiceLineName;
	}

	private String invoiceLineType;

	public String getInvoiceLineType() {
		return invoiceLineType;
	}

	public void setInvoiceLineType(String invoiceLineType) {
		this.invoiceLineType = invoiceLineType;
	}

	private String invoiceLineTypeName;

	public String getInvoiceLineTypeName() {
		return invoiceLineTypeName;
	}

	public void setInvoiceLineTypeName(String invoiceLineTypeName) {
		this.invoiceLineTypeName = invoiceLineTypeName;
	}

	private BigDecimal invoiceLineAmt;

	public BigDecimal getInvoiceLineAmt() {
		return invoiceLineAmt;
	}

	public void setInvoiceLineAmt(BigDecimal invoiceLineAmt) {
		this.invoiceLineAmt = invoiceLineAmt;
	}

	private BigDecimal invoiceLineTax;

	public BigDecimal getInvoiceLineTax() {
		return invoiceLineTax;
	}

	public void setInvoiceLineTax(BigDecimal invoiceLineTax) {
		this.invoiceLineTax = invoiceLineTax;
	}

	private Long invoiceLineCount;

	public Long getInvoiceLineCount() {
		return invoiceLineCount;
	}

	public void setInvoiceLineCount(Long invoiceLineCount) {
		this.invoiceLineCount = invoiceLineCount;
	}

	private Long invoiceLineWeight;

	public Long getInvoiceLineWeight() {
		return invoiceLineWeight;
	}

	public void setInvoiceLineWeight(Long invoiceLineWeight) {
		this.invoiceLineWeight = invoiceLineWeight;
	}

	private BigDecimal invoiceLineFreightAmt;

	public BigDecimal getInvoiceLineFreightAmt() {
		return invoiceLineFreightAmt;
	}

	public void setInvoiceLineFreightAmt(BigDecimal invoiceLineFreightAmt) {
		this.invoiceLineFreightAmt = invoiceLineFreightAmt;
	}

	private BigDecimal invoiceLineDiscAmt;

	public BigDecimal getInvoiceLineDiscAmt() {
		return invoiceLineDiscAmt;
	}

	public void setInvoiceLineDiscAmt(BigDecimal invoiceLineDiscAmt) {
		this.invoiceLineDiscAmt = invoiceLineDiscAmt;
	}

	private String invoiceLineDiscTypeCode;

	public String getInvoiceLineDiscTypeCode() {
		return invoiceLineDiscTypeCode;
	}

	public void setInvoiceLineDiscTypeCode(String invoiceLineDiscTypeCode) {
		this.invoiceLineDiscTypeCode = invoiceLineDiscTypeCode;
	}

	private String afeProjectNbr;

	public String getAfeProjectNbr() {
		return afeProjectNbr;
	}

	public void setAfeProjectNbr(String afeProjectNbr) {
		this.afeProjectNbr = afeProjectNbr;
	}

	private String afeProjectName;

	public String getAfeProjectName() {
		return afeProjectName;
	}

	public void setAfeProjectName(String afeProjectName) {
		this.afeProjectName = afeProjectName;
	}

	private String afeCategoryNbr;

	public String getAfeCategoryNbr() {
		return afeCategoryNbr;
	}

	public void setAfeCategoryNbr(String afeCategoryNbr) {
		this.afeCategoryNbr = afeCategoryNbr;
	}

	private String afeCategoryName;

	public String getAfeCategoryName() {
		return afeCategoryName;
	}

	public void setAfeCategoryName(String afeCategoryName) {
		this.afeCategoryName = afeCategoryName;
	}

	private String afeSubCatNbr;

	public String getAfeSubCatNbr() {
		return afeSubCatNbr;
	}

	public void setAfeSubCatNbr(String afeSubCatNbr) {
		this.afeSubCatNbr = afeSubCatNbr;
	}

	private String afeSubCatName;

	public String getAfeSubCatName() {
		return afeSubCatName;
	}

	public void setAfeSubCatName(String afeSubCatName) {
		this.afeSubCatName = afeSubCatName;
	}

	private String afeUse;

	public String getAfeUse() {
		return afeUse;
	}

	public void setAfeUse(String afeUse) {
		this.afeUse = afeUse;
	}

	private String afeContractType;

	public String getAfeContractType() {
		return afeContractType;
	}

	public void setAfeContractType(String afeContractType) {
		this.afeContractType = afeContractType;
	}

	private String afeContractStructure;

	public String getAfeContractStructure() {
		return afeContractStructure;
	}

	public void setAfeContractStructure(String afeContractStructure) {
		this.afeContractStructure = afeContractStructure;
	}

	private String afePropertyCat;

	public String getAfePropertyCat() {
		return afePropertyCat;
	}

	public void setAfePropertyCat(String afePropertyCat) {
		this.afePropertyCat = afePropertyCat;
	}

	private String inventoryNbr;

	public String getInventoryNbr() {
		return inventoryNbr;
	}

	public void setInventoryNbr(String inventoryNbr) {
		this.inventoryNbr = inventoryNbr;
	}

	private String inventoryName;

	public String getInventoryName() {
		return inventoryName;
	}

	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}

	private String inventoryClass;

	public String getInventoryClass() {
		return inventoryClass;
	}

	public void setInventoryClass(String inventoryClass) {
		this.inventoryClass = inventoryClass;
	}

	private String inventoryClassName;

	public String getInventoryClassName() {
		return inventoryClassName;
	}

	public void setInventoryClassName(String inventoryClassName) {
		this.inventoryClassName = inventoryClassName;
	}

	private String poNbr;

	public String getPoNbr() {
		return poNbr;
	}

	public void setPoNbr(String poNbr) {
		this.poNbr = poNbr;
	}

	private String poName;

	public String getPoName() {
		return poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	private Date poDate;

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	private String poLineNbr;

	public String getPoLineNbr() {
		return poLineNbr;
	}

	public void setPoLineNbr(String poLineNbr) {
		this.poLineNbr = poLineNbr;
	}

	private String poLineName;

	public String getPoLineName() {
		return poLineName;
	}

	public void setPoLineName(String poLineName) {
		this.poLineName = poLineName;
	}

	private String poLineType;

	public String getPoLineType() {
		return poLineType;
	}

	public void setPoLineType(String poLineType) {
		this.poLineType = poLineType;
	}

	private String poLineTypeName;

	public String getPoLineTypeName() {
		return poLineTypeName;
	}

	public void setPoLineTypeName(String poLineTypeName) {
		this.poLineTypeName = poLineTypeName;
	}

	private String woNbr;

	public String getWoNbr() {
		return woNbr;
	}

	public void setWoNbr(String woNbr) {
		this.woNbr = woNbr;
	}

	private String woName;

	public String getWoName() {
		return woName;
	}

	public void setWoName(String woName) {
		this.woName = woName;
	}

	private Date woDate;

	public Date getWoDate() {
		return woDate;
	}

	public void setWoDate(Date woDate) {
		this.woDate = woDate;
	}

	private String woType;

	public String getWoType() {
		return woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	private String woTypeDesc;

	public String getWoTypeDesc() {
		return woTypeDesc;
	}

	public void setWoTypeDesc(String woTypeDesc) {
		this.woTypeDesc = woTypeDesc;
	}

	private String woClass;

	public String getWoClass() {
		return woClass;
	}

	public void setWoClass(String woClass) {
		this.woClass = woClass;
	}

	private String woClassDesc;

	public String getWoClassDesc() {
		return woClassDesc;
	}

	public void setWoClassDesc(String woClassDesc) {
		this.woClassDesc = woClassDesc;
	}

	private String woEntity;

	public String getWoEntity() {
		return woEntity;
	}

	public void setWoEntity(String woEntity) {
		this.woEntity = woEntity;
	}

	private String woEntityDesc;

	public String getWoEntityDesc() {
		return woEntityDesc;
	}

	public void setWoEntityDesc(String woEntityDesc) {
		this.woEntityDesc = woEntityDesc;
	}

	private String woLineNbr;

	public String getWoLineNbr() {
		return woLineNbr;
	}

	public void setWoLineNbr(String woLineNbr) {
		this.woLineNbr = woLineNbr;
	}

	private String woLineName;

	public String getWoLineName() {
		return woLineName;
	}

	public void setWoLineName(String woLineName) {
		this.woLineName = woLineName;
	}

	private String woLineType;

	public String getWoLineType() {
		return woLineType;
	}

	public void setWoLineType(String woLineType) {
		this.woLineType = woLineType;
	}

	private String woLineTypeDesc;

	public String getWoLineTypeDesc() {
		return woLineTypeDesc;
	}

	public void setWoLineTypeDesc(String woLineTypeDesc) {
		this.woLineTypeDesc = woLineTypeDesc;
	}

	private String woShutDownCd;

	public String getWoShutDownCd() {
		return woShutDownCd;
	}

	public void setWoShutDownCd(String woShutDownCd) {
		this.woShutDownCd = woShutDownCd;
	}

	private String woShutDownCdDesc;

	public String getWoShutDownCdDesc() {
		return woShutDownCdDesc;
	}

	public void setWoShutDownCdDesc(String woShutDownCdDesc) {
		this.woShutDownCdDesc = woShutDownCdDesc;
	}

	private String voucherId;

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	private String voucherName;

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	private Date voucherDate;

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	private String voucherLineNbr;

	public String getVoucherLineNbr() {
		return voucherLineNbr;
	}

	public void setVoucherLineNbr(String voucherLineNbr) {
		this.voucherLineNbr = voucherLineNbr;
	}

	private String voucherLineDesc;

	public String getVoucherLineDesc() {
		return voucherLineDesc;
	}

	public void setVoucherLineDesc(String voucherLineDesc) {
		this.voucherLineDesc = voucherLineDesc;
	}

	private String checkNbr;

	public String getCheckNbr() {
		return checkNbr;
	}

	public void setCheckNbr(String checkNbr) {
		this.checkNbr = checkNbr;
	}

	private Long checkNo;

	public Long getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(Long checkNo) {
		this.checkNo = checkNo;
	}

	private Date checkDate;

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	private BigDecimal checkAmt;

	public BigDecimal getCheckAmt() {
		return checkAmt;
	}

	public void setCheckAmt(BigDecimal checkAmt) {
		this.checkAmt = checkAmt;
	}

	private String checkDesc;

	public String getCheckDesc() {
		return checkDesc;
	}

	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}

	private String glExtractFlag;

	public String getGlExtractFlag() {
		return glExtractFlag;
	}

	public void setGlExtractFlag(String glExtractFlag) {
		this.glExtractFlag = glExtractFlag;
	}

	private String glExtractUpdater;

	public String getGlExtractUpdater() {
		return glExtractUpdater;
	}

	public void setGlExtractUpdater(String glExtractUpdater) {
		this.glExtractUpdater = glExtractUpdater;
	}

	private Date glExtractTimestamp;

	public Date getGlExtractTimestamp() {
		return glExtractTimestamp;
	}

	public void setGlExtractTimestamp(Date glExtractTimestamp) {
		this.glExtractTimestamp = glExtractTimestamp;
	}

	private BigDecimal taxableAmt;

	public BigDecimal getTaxableAmt() {
		return taxableAmt;
	}

	public void setTaxableAmt(BigDecimal taxableAmt) {
		this.taxableAmt = taxableAmt;
	}

	private BigDecimal countryTier1TaxableAmt;

	public BigDecimal getCountryTier1TaxableAmt() {
		return countryTier1TaxableAmt;
	}

	public void setCountryTier1TaxableAmt(BigDecimal countryTier1TaxableAmt) {
		this.countryTier1TaxableAmt = countryTier1TaxableAmt;
	}

	private BigDecimal countryTier2TaxableAmt;

	public BigDecimal getCountryTier2TaxableAmt() {
		return countryTier2TaxableAmt;
	}

	public void setCountryTier2TaxableAmt(BigDecimal countryTier2TaxableAmt) {
		this.countryTier2TaxableAmt = countryTier2TaxableAmt;
	}

	private BigDecimal countryTier3TaxableAmt;

	public BigDecimal getCountryTier3TaxableAmt() {
		return countryTier3TaxableAmt;
	}

	public void setCountryTier3TaxableAmt(BigDecimal countryTier3TaxableAmt) {
		this.countryTier3TaxableAmt = countryTier3TaxableAmt;
	}

	private BigDecimal stateTier1TaxableAmt;

	public BigDecimal getStateTier1TaxableAmt() {
		return stateTier1TaxableAmt;
	}

	public void setStateTier1TaxableAmt(BigDecimal stateTier1TaxableAmt) {
		this.stateTier1TaxableAmt = stateTier1TaxableAmt;
	}

	private BigDecimal stateTier2TaxableAmt;

	public BigDecimal getStateTier2TaxableAmt() {
		return stateTier2TaxableAmt;
	}

	public void setStateTier2TaxableAmt(BigDecimal stateTier2TaxableAmt) {
		this.stateTier2TaxableAmt = stateTier2TaxableAmt;
	}

	private BigDecimal stateTier3TaxableAmt;

	public BigDecimal getStateTier3TaxableAmt() {
		return stateTier3TaxableAmt;
	}

	public void setStateTier3TaxableAmt(BigDecimal stateTier3TaxableAmt) {
		this.stateTier3TaxableAmt = stateTier3TaxableAmt;
	}

	private BigDecimal countyTier1TaxableAmt;

	public BigDecimal getCountyTier1TaxableAmt() {
		return countyTier1TaxableAmt;
	}

	public void setCountyTier1TaxableAmt(BigDecimal countyTier1TaxableAmt) {
		this.countyTier1TaxableAmt = countyTier1TaxableAmt;
	}

	private BigDecimal countyTier2TaxableAmt;

	public BigDecimal getCountyTier2TaxableAmt() {
		return countyTier2TaxableAmt;
	}

	public void setCountyTier2TaxableAmt(BigDecimal countyTier2TaxableAmt) {
		this.countyTier2TaxableAmt = countyTier2TaxableAmt;
	}

	private BigDecimal countyTier3TaxableAmt;

	public BigDecimal getCountyTier3TaxableAmt() {
		return countyTier3TaxableAmt;
	}

	public void setCountyTier3TaxableAmt(BigDecimal countyTier3TaxableAmt) {
		this.countyTier3TaxableAmt = countyTier3TaxableAmt;
	}

	private BigDecimal cityTier1TaxableAmt;

	public BigDecimal getCityTier1TaxableAmt() {
		return cityTier1TaxableAmt;
	}

	public void setCityTier1TaxableAmt(BigDecimal cityTier1TaxableAmt) {
		this.cityTier1TaxableAmt = cityTier1TaxableAmt;
	}

	private BigDecimal cityTier2TaxableAmt;

	public BigDecimal getCityTier2TaxableAmt() {
		return cityTier2TaxableAmt;
	}

	public void setCityTier2TaxableAmt(BigDecimal cityTier2TaxableAmt) {
		this.cityTier2TaxableAmt = cityTier2TaxableAmt;
	}

	private BigDecimal cityTier3TaxableAmt;

	public BigDecimal getCityTier3TaxableAmt() {
		return cityTier3TaxableAmt;
	}

	public void setCityTier3TaxableAmt(BigDecimal cityTier3TaxableAmt) {
		this.cityTier3TaxableAmt = cityTier3TaxableAmt;
	}

	private BigDecimal stj1TaxableAmt;

	public BigDecimal getStj1TaxableAmt() {
		return stj1TaxableAmt;
	}

	public void setStj1TaxableAmt(BigDecimal stj1TaxableAmt) {
		this.stj1TaxableAmt = stj1TaxableAmt;
	}

	private BigDecimal stj2TaxableAmt;

	public BigDecimal getStj2TaxableAmt() {
		return stj2TaxableAmt;
	}

	public void setStj2TaxableAmt(BigDecimal stj2TaxableAmt) {
		this.stj2TaxableAmt = stj2TaxableAmt;
	}

	private BigDecimal stj3TaxableAmt;

	public BigDecimal getStj3TaxableAmt() {
		return stj3TaxableAmt;
	}

	public void setStj3TaxableAmt(BigDecimal stj3TaxableAmt) {
		this.stj3TaxableAmt = stj3TaxableAmt;
	}

	private BigDecimal stj4TaxableAmt;

	public BigDecimal getStj4TaxableAmt() {
		return stj4TaxableAmt;
	}

	public void setStj4TaxableAmt(BigDecimal stj4TaxableAmt) {
		this.stj4TaxableAmt = stj4TaxableAmt;
	}

	private BigDecimal stj5TaxableAmt;

	public BigDecimal getStj5TaxableAmt() {
		return stj5TaxableAmt;
	}

	public void setStj5TaxableAmt(BigDecimal stj5TaxableAmt) {
		this.stj5TaxableAmt = stj5TaxableAmt;
	}

	private BigDecimal stj6TaxableAmt;

	public BigDecimal getStj6TaxableAmt() {
		return stj6TaxableAmt;
	}

	public void setStj6TaxableAmt(BigDecimal stj6TaxableAmt) {
		this.stj6TaxableAmt = stj6TaxableAmt;
	}

	private BigDecimal stj7TaxableAmt;

	public BigDecimal getStj7TaxableAmt() {
		return stj7TaxableAmt;
	}

	public void setStj7TaxableAmt(BigDecimal stj7TaxableAmt) {
		this.stj7TaxableAmt = stj7TaxableAmt;
	}

	private BigDecimal stj8TaxableAmt;

	public BigDecimal getStj8TaxableAmt() {
		return stj8TaxableAmt;
	}

	public void setStj8TaxableAmt(BigDecimal stj8TaxableAmt) {
		this.stj8TaxableAmt = stj8TaxableAmt;
	}

	private BigDecimal stj9TaxableAmt;

	public BigDecimal getStj9TaxableAmt() {
		return stj9TaxableAmt;
	}

	public void setStj9TaxableAmt(BigDecimal stj9TaxableAmt) {
		this.stj9TaxableAmt = stj9TaxableAmt;
	}

	private BigDecimal stj10TaxableAmt;

	public BigDecimal getStj10TaxableAmt() {
		return stj10TaxableAmt;
	}

	public void setStj10TaxableAmt(BigDecimal stj10TaxableAmt) {
		this.stj10TaxableAmt = stj10TaxableAmt;
	}

	private BigDecimal tbCalcTaxAmt;

	public BigDecimal getTbCalcTaxAmt() {
		return tbCalcTaxAmt;
	}

	public void setTbCalcTaxAmt(BigDecimal tbCalcTaxAmt) {
		this.tbCalcTaxAmt = tbCalcTaxAmt;
	}

	private BigDecimal countryTier1TaxAmt;

	public BigDecimal getCountryTier1TaxAmt() {
		return countryTier1TaxAmt;
	}

	public void setCountryTier1TaxAmt(BigDecimal countryTier1TaxAmt) {
		this.countryTier1TaxAmt = countryTier1TaxAmt;
	}

	private BigDecimal countryTier2TaxAmt;

	public BigDecimal getCountryTier2TaxAmt() {
		return countryTier2TaxAmt;
	}

	public void setCountryTier2TaxAmt(BigDecimal countryTier2TaxAmt) {
		this.countryTier2TaxAmt = countryTier2TaxAmt;
	}

	private BigDecimal countryTier3TaxAmt;

	public BigDecimal getCountryTier3TaxAmt() {
		return countryTier3TaxAmt;
	}

	public void setCountryTier3TaxAmt(BigDecimal countryTier3TaxAmt) {
		this.countryTier3TaxAmt = countryTier3TaxAmt;
	}

	private BigDecimal stateTier1TaxAmt;

	public BigDecimal getStateTier1TaxAmt() {
		return stateTier1TaxAmt;
	}

	public void setStateTier1TaxAmt(BigDecimal stateTier1TaxAmt) {
		this.stateTier1TaxAmt = stateTier1TaxAmt;
	}

	private BigDecimal stateTier2TaxAmt;

	public BigDecimal getStateTier2TaxAmt() {
		return stateTier2TaxAmt;
	}

	public void setStateTier2TaxAmt(BigDecimal stateTier2TaxAmt) {
		this.stateTier2TaxAmt = stateTier2TaxAmt;
	}

	private BigDecimal stateTier3TaxAmt;

	public BigDecimal getStateTier3TaxAmt() {
		return stateTier3TaxAmt;
	}

	public void setStateTier3TaxAmt(BigDecimal stateTier3TaxAmt) {
		this.stateTier3TaxAmt = stateTier3TaxAmt;
	}

	private BigDecimal countyTier1TaxAmt;

	public BigDecimal getCountyTier1TaxAmt() {
		return countyTier1TaxAmt;
	}

	public void setCountyTier1TaxAmt(BigDecimal countyTier1TaxAmt) {
		this.countyTier1TaxAmt = countyTier1TaxAmt;
	}

	private BigDecimal countyTier2TaxAmt;

	public BigDecimal getCountyTier2TaxAmt() {
		return countyTier2TaxAmt;
	}

	public void setCountyTier2TaxAmt(BigDecimal countyTier2TaxAmt) {
		this.countyTier2TaxAmt = countyTier2TaxAmt;
	}

	private BigDecimal countyTier3TaxAmt;

	public BigDecimal getCountyTier3TaxAmt() {
		return countyTier3TaxAmt;
	}

	public void setCountyTier3TaxAmt(BigDecimal countyTier3TaxAmt) {
		this.countyTier3TaxAmt = countyTier3TaxAmt;
	}

	private BigDecimal cityTier1TaxAmt;

	public BigDecimal getCityTier1TaxAmt() {
		return cityTier1TaxAmt;
	}

	public void setCityTier1TaxAmt(BigDecimal cityTier1TaxAmt) {
		this.cityTier1TaxAmt = cityTier1TaxAmt;
	}

	private BigDecimal cityTier2TaxAmt;

	public BigDecimal getCityTier2TaxAmt() {
		return cityTier2TaxAmt;
	}

	public void setCityTier2TaxAmt(BigDecimal cityTier2TaxAmt) {
		this.cityTier2TaxAmt = cityTier2TaxAmt;
	}

	private BigDecimal cityTier3TaxAmt;

	public BigDecimal getCityTier3TaxAmt() {
		return cityTier3TaxAmt;
	}

	public void setCityTier3TaxAmt(BigDecimal cityTier3TaxAmt) {
		this.cityTier3TaxAmt = cityTier3TaxAmt;
	}

	private BigDecimal stj1TaxAmt;

	public BigDecimal getStj1TaxAmt() {
		return stj1TaxAmt;
	}

	public void setStj1TaxAmt(BigDecimal stj1TaxAmt) {
		this.stj1TaxAmt = stj1TaxAmt;
	}

	private BigDecimal stj2TaxAmt;

	public BigDecimal getStj2TaxAmt() {
		return stj2TaxAmt;
	}

	public void setStj2TaxAmt(BigDecimal stj2TaxAmt) {
		this.stj2TaxAmt = stj2TaxAmt;
	}

	private BigDecimal stj3TaxAmt;

	public BigDecimal getStj3TaxAmt() {
		return stj3TaxAmt;
	}

	public void setStj3TaxAmt(BigDecimal stj3TaxAmt) {
		this.stj3TaxAmt = stj3TaxAmt;
	}

	private BigDecimal stj4TaxAmt;

	public BigDecimal getStj4TaxAmt() {
		return stj4TaxAmt;
	}

	public void setStj4TaxAmt(BigDecimal stj4TaxAmt) {
		this.stj4TaxAmt = stj4TaxAmt;
	}

	private BigDecimal stj5TaxAmt;

	public BigDecimal getStj5TaxAmt() {
		return stj5TaxAmt;
	}

	public void setStj5TaxAmt(BigDecimal stj5TaxAmt) {
		this.stj5TaxAmt = stj5TaxAmt;
	}

	private BigDecimal stj6TaxAmt;

	public BigDecimal getStj6TaxAmt() {
		return stj6TaxAmt;
	}

	public void setStj6TaxAmt(BigDecimal stj6TaxAmt) {
		this.stj6TaxAmt = stj6TaxAmt;
	}

	private BigDecimal stj7TaxAmt;

	public BigDecimal getStj7TaxAmt() {
		return stj7TaxAmt;
	}

	public void setStj7TaxAmt(BigDecimal stj7TaxAmt) {
		this.stj7TaxAmt = stj7TaxAmt;
	}

	private BigDecimal stj8TaxAmt;

	public BigDecimal getStj8TaxAmt() {
		return stj8TaxAmt;
	}

	public void setStj8TaxAmt(BigDecimal stj8TaxAmt) {
		this.stj8TaxAmt = stj8TaxAmt;
	}

	private BigDecimal stj9TaxAmt;

	public BigDecimal getStj9TaxAmt() {
		return stj9TaxAmt;
	}

	public void setStj9TaxAmt(BigDecimal stj9TaxAmt) {
		this.stj9TaxAmt = stj9TaxAmt;
	}

	private BigDecimal stj10TaxAmt;

	public BigDecimal getStj10TaxAmt() {
		return stj10TaxAmt;
	}

	public void setStj10TaxAmt(BigDecimal stj10TaxAmt) {
		this.stj10TaxAmt = stj10TaxAmt;
	}

	private BigDecimal countyLocalTaxAmt;

	public BigDecimal getCountyLocalTaxAmt() {
		return countyLocalTaxAmt;
	}

	public void setCountyLocalTaxAmt(BigDecimal countyLocalTaxAmt) {
		this.countyLocalTaxAmt = countyLocalTaxAmt;
	}

	private BigDecimal cityLocalTaxAmt;

	public BigDecimal getCityLocalTaxAmt() {
		return cityLocalTaxAmt;
	}

	public void setCityLocalTaxAmt(BigDecimal cityLocalTaxAmt) {
		this.cityLocalTaxAmt = cityLocalTaxAmt;
	}

	private BigDecimal distr01Amt;

	public BigDecimal getDistr01Amt() {
		return distr01Amt;
	}

	public void setDistr01Amt(BigDecimal distr01Amt) {
		this.distr01Amt = distr01Amt;
	}

	private BigDecimal distr02Amt;

	public BigDecimal getDistr02Amt() {
		return distr02Amt;
	}

	public void setDistr02Amt(BigDecimal distr02Amt) {
		this.distr02Amt = distr02Amt;
	}

	private BigDecimal distr03Amt;

	public BigDecimal getDistr03Amt() {
		return distr03Amt;
	}

	public void setDistr03Amt(BigDecimal distr03Amt) {
		this.distr03Amt = distr03Amt;
	}

	private BigDecimal distr04Amt;

	public BigDecimal getDistr04Amt() {
		return distr04Amt;
	}

	public void setDistr04Amt(BigDecimal distr04Amt) {
		this.distr04Amt = distr04Amt;
	}

	private BigDecimal distr05Amt;

	public BigDecimal getDistr05Amt() {
		return distr05Amt;
	}

	public void setDistr05Amt(BigDecimal distr05Amt) {
		this.distr05Amt = distr05Amt;
	}

	private BigDecimal distr06Amt;

	public BigDecimal getDistr06Amt() {
		return distr06Amt;
	}

	public void setDistr06Amt(BigDecimal distr06Amt) {
		this.distr06Amt = distr06Amt;
	}

	private BigDecimal distr07Amt;

	public BigDecimal getDistr07Amt() {
		return distr07Amt;
	}

	public void setDistr07Amt(BigDecimal distr07Amt) {
		this.distr07Amt = distr07Amt;
	}

	private BigDecimal distr08Amt;

	public BigDecimal getDistr08Amt() {
		return distr08Amt;
	}

	public void setDistr08Amt(BigDecimal distr08Amt) {
		this.distr08Amt = distr08Amt;
	}

	private BigDecimal distr09Amt;

	public BigDecimal getDistr09Amt() {
		return distr09Amt;
	}

	public void setDistr09Amt(BigDecimal distr09Amt) {
		this.distr09Amt = distr09Amt;
	}

	private BigDecimal distr10Amt;

	public BigDecimal getDistr10Amt() {
		return distr10Amt;
	}

	public void setDistr10Amt(BigDecimal distr10Amt) {
		this.distr10Amt = distr10Amt;
	}

	private String locationDriver01;

	public String getLocationDriver01() {
		return locationDriver01;
	}

	public void setLocationDriver01(String locationDriver01) {
		this.locationDriver01 = locationDriver01;
	}

	private String locationDriver02;

	public String getLocationDriver02() {
		return locationDriver02;
	}

	public void setLocationDriver02(String locationDriver02) {
		this.locationDriver02 = locationDriver02;
	}

	private String locationDriver03;

	public String getLocationDriver03() {
		return locationDriver03;
	}

	public void setLocationDriver03(String locationDriver03) {
		this.locationDriver03 = locationDriver03;
	}

	private String locationDriver04;

	public String getLocationDriver04() {
		return locationDriver04;
	}

	public void setLocationDriver04(String locationDriver04) {
		this.locationDriver04 = locationDriver04;
	}

	private String locationDriver05;

	public String getLocationDriver05() {
		return locationDriver05;
	}

	public void setLocationDriver05(String locationDriver05) {
		this.locationDriver05 = locationDriver05;
	}

	private String locationDriver06;

	public String getLocationDriver06() {
		return locationDriver06;
	}

	public void setLocationDriver06(String locationDriver06) {
		this.locationDriver06 = locationDriver06;
	}

	private String locationDriver07;

	public String getLocationDriver07() {
		return locationDriver07;
	}

	public void setLocationDriver07(String locationDriver07) {
		this.locationDriver07 = locationDriver07;
	}

	private String locationDriver08;

	public String getLocationDriver08() {
		return locationDriver08;
	}

	public void setLocationDriver08(String locationDriver08) {
		this.locationDriver08 = locationDriver08;
	}

	private String locationDriver09;

	public String getLocationDriver09() {
		return locationDriver09;
	}

	public void setLocationDriver09(String locationDriver09) {
		this.locationDriver09 = locationDriver09;
	}

	private String locationDriver10;

	public String getLocationDriver10() {
		return locationDriver10;
	}

	public void setLocationDriver10(String locationDriver10) {
		this.locationDriver10 = locationDriver10;
	}

	private String taxDriver01;

	public String getTaxDriver01() {
		return taxDriver01;
	}

	public void setTaxDriver01(String taxDriver01) {
		this.taxDriver01 = taxDriver01;
	}

	private String taxDriver02;

	public String getTaxDriver02() {
		return taxDriver02;
	}

	public void setTaxDriver02(String taxDriver02) {
		this.taxDriver02 = taxDriver02;
	}

	private String taxDriver03;

	public String getTaxDriver03() {
		return taxDriver03;
	}

	public void setTaxDriver03(String taxDriver03) {
		this.taxDriver03 = taxDriver03;
	}

	private String taxDriver04;

	public String getTaxDriver04() {
		return taxDriver04;
	}

	public void setTaxDriver04(String taxDriver04) {
		this.taxDriver04 = taxDriver04;
	}

	private String taxDriver05;

	public String getTaxDriver05() {
		return taxDriver05;
	}

	public void setTaxDriver05(String taxDriver05) {
		this.taxDriver05 = taxDriver05;
	}

	private String taxDriver06;

	public String getTaxDriver06() {
		return taxDriver06;
	}

	public void setTaxDriver06(String taxDriver06) {
		this.taxDriver06 = taxDriver06;
	}

	private String taxDriver07;

	public String getTaxDriver07() {
		return taxDriver07;
	}

	public void setTaxDriver07(String taxDriver07) {
		this.taxDriver07 = taxDriver07;
	}

	private String taxDriver08;

	public String getTaxDriver08() {
		return taxDriver08;
	}

	public void setTaxDriver08(String taxDriver08) {
		this.taxDriver08 = taxDriver08;
	}

	private String taxDriver09;

	public String getTaxDriver09() {
		return taxDriver09;
	}

	public void setTaxDriver09(String taxDriver09) {
		this.taxDriver09 = taxDriver09;
	}

	private String taxDriver10;

	public String getTaxDriver10() {
		return taxDriver10;
	}

	public void setTaxDriver10(String taxDriver10) {
		this.taxDriver10 = taxDriver10;
	}

	private String taxDriver11;

	public String getTaxDriver11() {
		return taxDriver11;
	}

	public void setTaxDriver11(String taxDriver11) {
		this.taxDriver11 = taxDriver11;
	}

	private String taxDriver12;

	public String getTaxDriver12() {
		return taxDriver12;
	}

	public void setTaxDriver12(String taxDriver12) {
		this.taxDriver12 = taxDriver12;
	}

	private String taxDriver13;

	public String getTaxDriver13() {
		return taxDriver13;
	}

	public void setTaxDriver13(String taxDriver13) {
		this.taxDriver13 = taxDriver13;
	}

	private String taxDriver14;

	public String getTaxDriver14() {
		return taxDriver14;
	}

	public void setTaxDriver14(String taxDriver14) {
		this.taxDriver14 = taxDriver14;
	}

	private String taxDriver15;

	public String getTaxDriver15() {
		return taxDriver15;
	}

	public void setTaxDriver15(String taxDriver15) {
		this.taxDriver15 = taxDriver15;
	}

	private String taxDriver16;

	public String getTaxDriver16() {
		return taxDriver16;
	}

	public void setTaxDriver16(String taxDriver16) {
		this.taxDriver16 = taxDriver16;
	}

	private String taxDriver17;

	public String getTaxDriver17() {
		return taxDriver17;
	}

	public void setTaxDriver17(String taxDriver17) {
		this.taxDriver17 = taxDriver17;
	}

	private String taxDriver18;

	public String getTaxDriver18() {
		return taxDriver18;
	}

	public void setTaxDriver18(String taxDriver18) {
		this.taxDriver18 = taxDriver18;
	}

	private String taxDriver19;

	public String getTaxDriver19() {
		return taxDriver19;
	}

	public void setTaxDriver19(String taxDriver19) {
		this.taxDriver19 = taxDriver19;
	}

	private String taxDriver20;

	public String getTaxDriver20() {
		return taxDriver20;
	}

	public void setTaxDriver20(String taxDriver20) {
		this.taxDriver20 = taxDriver20;
	}

	private String taxDriver21;

	public String getTaxDriver21() {
		return taxDriver21;
	}

	public void setTaxDriver21(String taxDriver21) {
		this.taxDriver21 = taxDriver21;
	}

	private String taxDriver22;

	public String getTaxDriver22() {
		return taxDriver22;
	}

	public void setTaxDriver22(String taxDriver22) {
		this.taxDriver22 = taxDriver22;
	}

	private String taxDriver23;

	public String getTaxDriver23() {
		return taxDriver23;
	}

	public void setTaxDriver23(String taxDriver23) {
		this.taxDriver23 = taxDriver23;
	}

	private String taxDriver24;

	public String getTaxDriver24() {
		return taxDriver24;
	}

	public void setTaxDriver24(String taxDriver24) {
		this.taxDriver24 = taxDriver24;
	}

	private String taxDriver25;

	public String getTaxDriver25() {
		return taxDriver25;
	}

	public void setTaxDriver25(String taxDriver25) {
		this.taxDriver25 = taxDriver25;
	}

	private String taxDriver26;

	public String getTaxDriver26() {
		return taxDriver26;
	}

	public void setTaxDriver26(String taxDriver26) {
		this.taxDriver26 = taxDriver26;
	}

	private String taxDriver27;

	public String getTaxDriver27() {
		return taxDriver27;
	}

	public void setTaxDriver27(String taxDriver27) {
		this.taxDriver27 = taxDriver27;
	}

	private String taxDriver28;

	public String getTaxDriver28() {
		return taxDriver28;
	}

	public void setTaxDriver28(String taxDriver28) {
		this.taxDriver28 = taxDriver28;
	}

	private String taxDriver29;

	public String getTaxDriver29() {
		return taxDriver29;
	}

	public void setTaxDriver29(String taxDriver29) {
		this.taxDriver29 = taxDriver29;
	}

	private String taxDriver30;

	public String getTaxDriver30() {
		return taxDriver30;
	}

	public void setTaxDriver30(String taxDriver30) {
		this.taxDriver30 = taxDriver30;
	}

	private String auditFlag;

	public String getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}

	private String auditUserId;

	public String getAuditUserId() {
		return auditUserId;
	}

	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}

	private Date auditTimestamp;

	public Date getAuditTimestamp() {
		return auditTimestamp;
	}

	public void setAuditTimestamp(Date auditTimestamp) {
		this.auditTimestamp = auditTimestamp;
	}

	private String modifyUserId;

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	private Date modifyTimestamp;

	public Date getModifyTimestamp() {
		return modifyTimestamp;
	}

	public void setModifyTimestamp(Date modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}

	private String userText01;

	public String getUserText01() {
		return userText01;
	}

	public void setUserText01(String userText01) {
		this.userText01 = userText01;
	}

	private String userText02;

	public String getUserText02() {
		return userText02;
	}

	public void setUserText02(String userText02) {
		this.userText02 = userText02;
	}

	private String userText03;

	public String getUserText03() {
		return userText03;
	}

	public void setUserText03(String userText03) {
		this.userText03 = userText03;
	}

	private String userText04;

	public String getUserText04() {
		return userText04;
	}

	public void setUserText04(String userText04) {
		this.userText04 = userText04;
	}

	private String userText05;

	public String getUserText05() {
		return userText05;
	}

	public void setUserText05(String userText05) {
		this.userText05 = userText05;
	}

	private String userText06;

	public String getUserText06() {
		return userText06;
	}

	public void setUserText06(String userText06) {
		this.userText06 = userText06;
	}

	private String userText07;

	public String getUserText07() {
		return userText07;
	}

	public void setUserText07(String userText07) {
		this.userText07 = userText07;
	}

	private String userText08;

	public String getUserText08() {
		return userText08;
	}

	public void setUserText08(String userText08) {
		this.userText08 = userText08;
	}

	private String userText09;

	public String getUserText09() {
		return userText09;
	}

	public void setUserText09(String userText09) {
		this.userText09 = userText09;
	}

	private String userText10;

	public String getUserText10() {
		return userText10;
	}

	public void setUserText10(String userText10) {
		this.userText10 = userText10;
	}

	private String userText11;

	public String getUserText11() {
		return userText11;
	}

	public void setUserText11(String userText11) {
		this.userText11 = userText11;
	}

	private String userText12;

	public String getUserText12() {
		return userText12;
	}

	public void setUserText12(String userText12) {
		this.userText12 = userText12;
	}

	private String userText13;

	public String getUserText13() {
		return userText13;
	}

	public void setUserText13(String userText13) {
		this.userText13 = userText13;
	}

	private String userText14;

	public String getUserText14() {
		return userText14;
	}

	public void setUserText14(String userText14) {
		this.userText14 = userText14;
	}

	private String userText15;

	public String getUserText15() {
		return userText15;
	}

	public void setUserText15(String userText15) {
		this.userText15 = userText15;
	}

	private String userText16;

	public String getUserText16() {
		return userText16;
	}

	public void setUserText16(String userText16) {
		this.userText16 = userText16;
	}

	private String userText17;

	public String getUserText17() {
		return userText17;
	}

	public void setUserText17(String userText17) {
		this.userText17 = userText17;
	}

	private String userText18;

	public String getUserText18() {
		return userText18;
	}

	public void setUserText18(String userText18) {
		this.userText18 = userText18;
	}

	private String userText19;

	public String getUserText19() {
		return userText19;
	}

	public void setUserText19(String userText19) {
		this.userText19 = userText19;
	}

	private String userText20;

	public String getUserText20() {
		return userText20;
	}

	public void setUserText20(String userText20) {
		this.userText20 = userText20;
	}

	private String userText21;

	public String getUserText21() {
		return userText21;
	}

	public void setUserText21(String userText21) {
		this.userText21 = userText21;
	}

	private String userText22;

	public String getUserText22() {
		return userText22;
	}

	public void setUserText22(String userText22) {
		this.userText22 = userText22;
	}

	private String userText23;

	public String getUserText23() {
		return userText23;
	}

	public void setUserText23(String userText23) {
		this.userText23 = userText23;
	}

	private String userText24;

	public String getUserText24() {
		return userText24;
	}

	public void setUserText24(String userText24) {
		this.userText24 = userText24;
	}

	private String userText25;

	public String getUserText25() {
		return userText25;
	}

	public void setUserText25(String userText25) {
		this.userText25 = userText25;
	}

	private String userText26;

	public String getUserText26() {
		return userText26;
	}

	public void setUserText26(String userText26) {
		this.userText26 = userText26;
	}

	private String userText27;

	public String getUserText27() {
		return userText27;
	}

	public void setUserText27(String userText27) {
		this.userText27 = userText27;
	}

	private String userText28;

	public String getUserText28() {
		return userText28;
	}

	public void setUserText28(String userText28) {
		this.userText28 = userText28;
	}

	private String userText29;

	public String getUserText29() {
		return userText29;
	}

	public void setUserText29(String userText29) {
		this.userText29 = userText29;
	}

	private String userText30;

	public String getUserText30() {
		return userText30;
	}

	public void setUserText30(String userText30) {
		this.userText30 = userText30;
	}

	private String userText31;

	public String getUserText31() {
		return userText31;
	}

	public void setUserText31(String userText31) {
		this.userText31 = userText31;
	}

	private String userText32;

	public String getUserText32() {
		return userText32;
	}

	public void setUserText32(String userText32) {
		this.userText32 = userText32;
	}

	private String userText33;

	public String getUserText33() {
		return userText33;
	}

	public void setUserText33(String userText33) {
		this.userText33 = userText33;
	}

	private String userText34;

	public String getUserText34() {
		return userText34;
	}

	public void setUserText34(String userText34) {
		this.userText34 = userText34;
	}

	private String userText35;

	public String getUserText35() {
		return userText35;
	}

	public void setUserText35(String userText35) {
		this.userText35 = userText35;
	}

	private String userText36;

	public String getUserText36() {
		return userText36;
	}

	public void setUserText36(String userText36) {
		this.userText36 = userText36;
	}

	private String userText37;

	public String getUserText37() {
		return userText37;
	}

	public void setUserText37(String userText37) {
		this.userText37 = userText37;
	}

	private String userText38;

	public String getUserText38() {
		return userText38;
	}

	public void setUserText38(String userText38) {
		this.userText38 = userText38;
	}

	private String userText39;

	public String getUserText39() {
		return userText39;
	}

	public void setUserText39(String userText39) {
		this.userText39 = userText39;
	}

	private String userText40;

	public String getUserText40() {
		return userText40;
	}

	public void setUserText40(String userText40) {
		this.userText40 = userText40;
	}

	private String userText41;

	public String getUserText41() {
		return userText41;
	}

	public void setUserText41(String userText41) {
		this.userText41 = userText41;
	}

	private String userText42;

	public String getUserText42() {
		return userText42;
	}

	public void setUserText42(String userText42) {
		this.userText42 = userText42;
	}

	private String userText43;

	public String getUserText43() {
		return userText43;
	}

	public void setUserText43(String userText43) {
		this.userText43 = userText43;
	}

	private String userText44;

	public String getUserText44() {
		return userText44;
	}

	public void setUserText44(String userText44) {
		this.userText44 = userText44;
	}

	private String userText45;

	public String getUserText45() {
		return userText45;
	}

	public void setUserText45(String userText45) {
		this.userText45 = userText45;
	}

	private String userText46;

	public String getUserText46() {
		return userText46;
	}

	public void setUserText46(String userText46) {
		this.userText46 = userText46;
	}

	private String userText47;

	public String getUserText47() {
		return userText47;
	}

	public void setUserText47(String userText47) {
		this.userText47 = userText47;
	}

	private String userText48;

	public String getUserText48() {
		return userText48;
	}

	public void setUserText48(String userText48) {
		this.userText48 = userText48;
	}

	private String userText49;

	public String getUserText49() {
		return userText49;
	}

	public void setUserText49(String userText49) {
		this.userText49 = userText49;
	}

	private String userText50;

	public String getUserText50() {
		return userText50;
	}

	public void setUserText50(String userText50) {
		this.userText50 = userText50;
	}

	private String userText51;

	public String getUserText51() {
		return userText51;
	}

	public void setUserText51(String userText51) {
		this.userText51 = userText51;
	}

	private String userText52;

	public String getUserText52() {
		return userText52;
	}

	public void setUserText52(String userText52) {
		this.userText52 = userText52;
	}

	private String userText53;

	public String getUserText53() {
		return userText53;
	}

	public void setUserText53(String userText53) {
		this.userText53 = userText53;
	}

	private String userText54;

	public String getUserText54() {
		return userText54;
	}

	public void setUserText54(String userText54) {
		this.userText54 = userText54;
	}

	private String userText55;

	public String getUserText55() {
		return userText55;
	}

	public void setUserText55(String userText55) {
		this.userText55 = userText55;
	}

	private String userText56;

	public String getUserText56() {
		return userText56;
	}

	public void setUserText56(String userText56) {
		this.userText56 = userText56;
	}

	private String userText57;

	public String getUserText57() {
		return userText57;
	}

	public void setUserText57(String userText57) {
		this.userText57 = userText57;
	}

	private String userText58;

	public String getUserText58() {
		return userText58;
	}

	public void setUserText58(String userText58) {
		this.userText58 = userText58;
	}

	private String userText59;

	public String getUserText59() {
		return userText59;
	}

	public void setUserText59(String userText59) {
		this.userText59 = userText59;
	}

	private String userText60;

	public String getUserText60() {
		return userText60;
	}

	public void setUserText60(String userText60) {
		this.userText60 = userText60;
	}

	private String userText61;

	public String getUserText61() {
		return userText61;
	}

	public void setUserText61(String userText61) {
		this.userText61 = userText61;
	}

	private String userText62;

	public String getUserText62() {
		return userText62;
	}

	public void setUserText62(String userText62) {
		this.userText62 = userText62;
	}

	private String userText63;

	public String getUserText63() {
		return userText63;
	}

	public void setUserText63(String userText63) {
		this.userText63 = userText63;
	}

	private String userText64;

	public String getUserText64() {
		return userText64;
	}

	public void setUserText64(String userText64) {
		this.userText64 = userText64;
	}

	private String userText65;

	public String getUserText65() {
		return userText65;
	}

	public void setUserText65(String userText65) {
		this.userText65 = userText65;
	}

	private String userText66;

	public String getUserText66() {
		return userText66;
	}

	public void setUserText66(String userText66) {
		this.userText66 = userText66;
	}

	private String userText67;

	public String getUserText67() {
		return userText67;
	}

	public void setUserText67(String userText67) {
		this.userText67 = userText67;
	}

	private String userText68;

	public String getUserText68() {
		return userText68;
	}

	public void setUserText68(String userText68) {
		this.userText68 = userText68;
	}

	private String userText69;

	public String getUserText69() {
		return userText69;
	}

	public void setUserText69(String userText69) {
		this.userText69 = userText69;
	}

	private String userText70;

	public String getUserText70() {
		return userText70;
	}

	public void setUserText70(String userText70) {
		this.userText70 = userText70;
	}

	private String userText71;

	public String getUserText71() {
		return userText71;
	}

	public void setUserText71(String userText71) {
		this.userText71 = userText71;
	}

	private String userText72;

	public String getUserText72() {
		return userText72;
	}

	public void setUserText72(String userText72) {
		this.userText72 = userText72;
	}

	private String userText73;

	public String getUserText73() {
		return userText73;
	}

	public void setUserText73(String userText73) {
		this.userText73 = userText73;
	}

	private String userText74;

	public String getUserText74() {
		return userText74;
	}

	public void setUserText74(String userText74) {
		this.userText74 = userText74;
	}

	private String userText75;

	public String getUserText75() {
		return userText75;
	}

	public void setUserText75(String userText75) {
		this.userText75 = userText75;
	}

	private String userText76;

	public String getUserText76() {
		return userText76;
	}

	public void setUserText76(String userText76) {
		this.userText76 = userText76;
	}

	private String userText77;

	public String getUserText77() {
		return userText77;
	}

	public void setUserText77(String userText77) {
		this.userText77 = userText77;
	}

	private String userText78;

	public String getUserText78() {
		return userText78;
	}

	public void setUserText78(String userText78) {
		this.userText78 = userText78;
	}

	private String userText79;

	public String getUserText79() {
		return userText79;
	}

	public void setUserText79(String userText79) {
		this.userText79 = userText79;
	}

	private String userText80;

	public String getUserText80() {
		return userText80;
	}

	public void setUserText80(String userText80) {
		this.userText80 = userText80;
	}

	private String userText81;

	public String getUserText81() {
		return userText81;
	}

	public void setUserText81(String userText81) {
		this.userText81 = userText81;
	}

	private String userText82;

	public String getUserText82() {
		return userText82;
	}

	public void setUserText82(String userText82) {
		this.userText82 = userText82;
	}

	private String userText83;

	public String getUserText83() {
		return userText83;
	}

	public void setUserText83(String userText83) {
		this.userText83 = userText83;
	}

	private String userText84;

	public String getUserText84() {
		return userText84;
	}

	public void setUserText84(String userText84) {
		this.userText84 = userText84;
	}

	private String userText85;

	public String getUserText85() {
		return userText85;
	}

	public void setUserText85(String userText85) {
		this.userText85 = userText85;
	}

	private String userText86;

	public String getUserText86() {
		return userText86;
	}

	public void setUserText86(String userText86) {
		this.userText86 = userText86;
	}

	private String userText87;

	public String getUserText87() {
		return userText87;
	}

	public void setUserText87(String userText87) {
		this.userText87 = userText87;
	}

	private String userText88;

	public String getUserText88() {
		return userText88;
	}

	public void setUserText88(String userText88) {
		this.userText88 = userText88;
	}

	private String userText89;

	public String getUserText89() {
		return userText89;
	}

	public void setUserText89(String userText89) {
		this.userText89 = userText89;
	}

	private String userText90;

	public String getUserText90() {
		return userText90;
	}

	public void setUserText90(String userText90) {
		this.userText90 = userText90;
	}

	private String userText91;

	public String getUserText91() {
		return userText91;
	}

	public void setUserText91(String userText91) {
		this.userText91 = userText91;
	}

	private String userText92;

	public String getUserText92() {
		return userText92;
	}

	public void setUserText92(String userText92) {
		this.userText92 = userText92;
	}

	private String userText93;

	public String getUserText93() {
		return userText93;
	}

	public void setUserText93(String userText93) {
		this.userText93 = userText93;
	}

	private String userText94;

	public String getUserText94() {
		return userText94;
	}

	public void setUserText94(String userText94) {
		this.userText94 = userText94;
	}

	private String userText95;

	public String getUserText95() {
		return userText95;
	}

	public void setUserText95(String userText95) {
		this.userText95 = userText95;
	}

	private String userText96;

	public String getUserText96() {
		return userText96;
	}

	public void setUserText96(String userText96) {
		this.userText96 = userText96;
	}

	private String userText97;

	public String getUserText97() {
		return userText97;
	}

	public void setUserText97(String userText97) {
		this.userText97 = userText97;
	}

	private String userText98;

	public String getUserText98() {
		return userText98;
	}

	public void setUserText98(String userText98) {
		this.userText98 = userText98;
	}

	private String userText99;

	public String getUserText99() {
		return userText99;
	}

	public void setUserText99(String userText99) {
		this.userText99 = userText99;
	}

	private String userText100;

	public String getUserText100() {
		return userText100;
	}

	public void setUserText100(String userText100) {
		this.userText100 = userText100;
	}

	private BigDecimal userNumber01;

	public BigDecimal getUserNumber01() {
		return userNumber01;
	}

	public void setUserNumber01(BigDecimal userNumber01) {
		this.userNumber01 = userNumber01;
	}

	private BigDecimal userNumber02;

	public BigDecimal getUserNumber02() {
		return userNumber02;
	}

	public void setUserNumber02(BigDecimal userNumber02) {
		this.userNumber02 = userNumber02;
	}

	private BigDecimal userNumber03;

	public BigDecimal getUserNumber03() {
		return userNumber03;
	}

	public void setUserNumber03(BigDecimal userNumber03) {
		this.userNumber03 = userNumber03;
	}

	private BigDecimal userNumber04;

	public BigDecimal getUserNumber04() {
		return userNumber04;
	}

	public void setUserNumber04(BigDecimal userNumber04) {
		this.userNumber04 = userNumber04;
	}

	private BigDecimal userNumber05;

	public BigDecimal getUserNumber05() {
		return userNumber05;
	}

	public void setUserNumber05(BigDecimal userNumber05) {
		this.userNumber05 = userNumber05;
	}

	private BigDecimal userNumber06;

	public BigDecimal getUserNumber06() {
		return userNumber06;
	}

	public void setUserNumber06(BigDecimal userNumber06) {
		this.userNumber06 = userNumber06;
	}

	private BigDecimal userNumber07;

	public BigDecimal getUserNumber07() {
		return userNumber07;
	}

	public void setUserNumber07(BigDecimal userNumber07) {
		this.userNumber07 = userNumber07;
	}

	private BigDecimal userNumber08;

	public BigDecimal getUserNumber08() {
		return userNumber08;
	}

	public void setUserNumber08(BigDecimal userNumber08) {
		this.userNumber08 = userNumber08;
	}

	private BigDecimal userNumber09;

	public BigDecimal getUserNumber09() {
		return userNumber09;
	}

	public void setUserNumber09(BigDecimal userNumber09) {
		this.userNumber09 = userNumber09;
	}

	private BigDecimal userNumber10;

	public BigDecimal getUserNumber10() {
		return userNumber10;
	}

	public void setUserNumber10(BigDecimal userNumber10) {
		this.userNumber10 = userNumber10;
	}

	private BigDecimal userNumber11;

	public BigDecimal getUserNumber11() {
		return userNumber11;
	}

	public void setUserNumber11(BigDecimal userNumber11) {
		this.userNumber11 = userNumber11;
	}

	private BigDecimal userNumber12;

	public BigDecimal getUserNumber12() {
		return userNumber12;
	}

	public void setUserNumber12(BigDecimal userNumber12) {
		this.userNumber12 = userNumber12;
	}

	private BigDecimal userNumber13;

	public BigDecimal getUserNumber13() {
		return userNumber13;
	}

	public void setUserNumber13(BigDecimal userNumber13) {
		this.userNumber13 = userNumber13;
	}

	private BigDecimal userNumber14;

	public BigDecimal getUserNumber14() {
		return userNumber14;
	}

	public void setUserNumber14(BigDecimal userNumber14) {
		this.userNumber14 = userNumber14;
	}

	private BigDecimal userNumber15;

	public BigDecimal getUserNumber15() {
		return userNumber15;
	}

	public void setUserNumber15(BigDecimal userNumber15) {
		this.userNumber15 = userNumber15;
	}

	private BigDecimal userNumber16;

	public BigDecimal getUserNumber16() {
		return userNumber16;
	}

	public void setUserNumber16(BigDecimal userNumber16) {
		this.userNumber16 = userNumber16;
	}

	private BigDecimal userNumber17;

	public BigDecimal getUserNumber17() {
		return userNumber17;
	}

	public void setUserNumber17(BigDecimal userNumber17) {
		this.userNumber17 = userNumber17;
	}

	private BigDecimal userNumber18;

	public BigDecimal getUserNumber18() {
		return userNumber18;
	}

	public void setUserNumber18(BigDecimal userNumber18) {
		this.userNumber18 = userNumber18;
	}

	private BigDecimal userNumber19;

	public BigDecimal getUserNumber19() {
		return userNumber19;
	}

	public void setUserNumber19(BigDecimal userNumber19) {
		this.userNumber19 = userNumber19;
	}

	private BigDecimal userNumber20;

	public BigDecimal getUserNumber20() {
		return userNumber20;
	}

	public void setUserNumber20(BigDecimal userNumber20) {
		this.userNumber20 = userNumber20;
	}

	private BigDecimal userNumber21;

	public BigDecimal getUserNumber21() {
		return userNumber21;
	}

	public void setUserNumber21(BigDecimal userNumber21) {
		this.userNumber21 = userNumber21;
	}

	private BigDecimal userNumber22;

	public BigDecimal getUserNumber22() {
		return userNumber22;
	}

	public void setUserNumber22(BigDecimal userNumber22) {
		this.userNumber22 = userNumber22;
	}

	private BigDecimal userNumber23;

	public BigDecimal getUserNumber23() {
		return userNumber23;
	}

	public void setUserNumber23(BigDecimal userNumber23) {
		this.userNumber23 = userNumber23;
	}

	private BigDecimal userNumber24;

	public BigDecimal getUserNumber24() {
		return userNumber24;
	}

	public void setUserNumber24(BigDecimal userNumber24) {
		this.userNumber24 = userNumber24;
	}

	private BigDecimal userNumber25;

	public BigDecimal getUserNumber25() {
		return userNumber25;
	}

	public void setUserNumber25(BigDecimal userNumber25) {
		this.userNumber25 = userNumber25;
	}

	private Date userDate01;

	public Date getUserDate01() {
		return userDate01;
	}

	public void setUserDate01(Date userDate01) {
		this.userDate01 = userDate01;
	}

	private Date userDate02;

	public Date getUserDate02() {
		return userDate02;
	}

	public void setUserDate02(Date userDate02) {
		this.userDate02 = userDate02;
	}

	private Date userDate03;

	public Date getUserDate03() {
		return userDate03;
	}

	public void setUserDate03(Date userDate03) {
		this.userDate03 = userDate03;
	}

	private Date userDate04;

	public Date getUserDate04() {
		return userDate04;
	}

	public void setUserDate04(Date userDate04) {
		this.userDate04 = userDate04;
	}

	private Date userDate05;

	public Date getUserDate05() {
		return userDate05;
	}

	public void setUserDate05(Date userDate05) {
		this.userDate05 = userDate05;
	}

	private Date userDate06;

	public Date getUserDate06() {
		return userDate06;
	}

	public void setUserDate06(Date userDate06) {
		this.userDate06 = userDate06;
	}

	private Date userDate07;

	public Date getUserDate07() {
		return userDate07;
	}

	public void setUserDate07(Date userDate07) {
		this.userDate07 = userDate07;
	}

	private Date userDate08;

	public Date getUserDate08() {
		return userDate08;
	}

	public void setUserDate08(Date userDate08) {
		this.userDate08 = userDate08;
	}

	private Date userDate09;

	public Date getUserDate09() {
		return userDate09;
	}

	public void setUserDate09(Date userDate09) {
		this.userDate09 = userDate09;
	}

	private Date userDate10;

	public Date getUserDate10() {
		return userDate10;
	}

	public void setUserDate10(Date userDate10) {
		this.userDate10 = userDate10;
	}

	private Date userDate11;

	public Date getUserDate11() {
		return userDate11;
	}

	public void setUserDate11(Date userDate11) {
		this.userDate11 = userDate11;
	}

	private Date userDate12;

	public Date getUserDate12() {
		return userDate12;
	}

	public void setUserDate12(Date userDate12) {
		this.userDate12 = userDate12;
	}

	private Date userDate13;

	public Date getUserDate13() {
		return userDate13;
	}

	public void setUserDate13(Date userDate13) {
		this.userDate13 = userDate13;
	}

	private Date userDate14;

	public Date getUserDate14() {
		return userDate14;
	}

	public void setUserDate14(Date userDate14) {
		this.userDate14 = userDate14;
	}

	private Date userDate15;

	public Date getUserDate15() {
		return userDate15;
	}

	public void setUserDate15(Date userDate15) {
		this.userDate15 = userDate15;
	}

	private Date userDate16;

	public Date getUserDate16() {
		return userDate16;
	}

	public void setUserDate16(Date userDate16) {
		this.userDate16 = userDate16;
	}

	private Date userDate17;

	public Date getUserDate17() {
		return userDate17;
	}

	public void setUserDate17(Date userDate17) {
		this.userDate17 = userDate17;
	}

	private Date userDate18;

	public Date getUserDate18() {
		return userDate18;
	}

	public void setUserDate18(Date userDate18) {
		this.userDate18 = userDate18;
	}

	private Date userDate19;

	public Date getUserDate19() {
		return userDate19;
	}

	public void setUserDate19(Date userDate19) {
		this.userDate19 = userDate19;
	}

	private Date userDate20;

	public Date getUserDate20() {
		return userDate20;
	}

	public void setUserDate20(Date userDate20) {
		this.userDate20 = userDate20;
	}

	private Date userDate21;

	public Date getUserDate21() {
		return userDate21;
	}

	public void setUserDate21(Date userDate21) {
		this.userDate21 = userDate21;
	}

	private Date userDate22;

	public Date getUserDate22() {
		return userDate22;
	}

	public void setUserDate22(Date userDate22) {
		this.userDate22 = userDate22;
	}

	private Date userDate23;

	public Date getUserDate23() {
		return userDate23;
	}

	public void setUserDate23(Date userDate23) {
		this.userDate23 = userDate23;
	}

	private Date userDate24;

	public Date getUserDate24() {
		return userDate24;
	}

	public void setUserDate24(Date userDate24) {
		this.userDate24 = userDate24;
	}

	private Date userDate25;

	public Date getUserDate25() {
		return userDate25;
	}

	public void setUserDate25(Date userDate25) {
		this.userDate25 = userDate25;
	}

	private String shiptoLocation;

	public String getShiptoLocation() {
		return shiptoLocation;
	}

	public void setShiptoLocation(String shiptoLocation) {
		this.shiptoLocation = shiptoLocation;
	}

	private String shiptoLocationName;

	public String getShiptoLocationName() {
		return shiptoLocationName;
	}

	public void setShiptoLocationName(String shiptoLocationName) {
		this.shiptoLocationName = shiptoLocationName;
	}

	private String shiptoManualJurInd;

	public String getShiptoManualJurInd() {
		return shiptoManualJurInd;
	}

	public void setShiptoManualJurInd(String shiptoManualJurInd) {
		this.shiptoManualJurInd = shiptoManualJurInd;
	}

	private Long shiptoEntityId;

	public Long getShiptoEntityId() {
		return shiptoEntityId;
	}

	public void setShiptoEntityId(Long shiptoEntityId) {
		this.shiptoEntityId = shiptoEntityId;
	}

	private String shiptoEntityCode;

	public String getShiptoEntityCode() {
		return shiptoEntityCode;
	}

	public void setShiptoEntityCode(String shiptoEntityCode) {
		this.shiptoEntityCode = shiptoEntityCode;
	}

	private String shiptoLat;

	public String getShiptoLat() {
		return shiptoLat;
	}

	public void setShiptoLat(String shiptoLat) {
		this.shiptoLat = shiptoLat;
	}

	;

	private String shiptoLong;

	public String getShiptoLong() {
		return shiptoLong;
	}

	public void setShiptoLong(String shiptoLong) {
		this.shiptoLong = shiptoLong;
	}

	private Long shiptoLocnMatrixId;

	public Long getShiptoLocnMatrixId() {
		return shiptoLocnMatrixId;
	}

	public void setShiptoLocnMatrixId(Long shiptoLocnMatrixId) {
		this.shiptoLocnMatrixId = shiptoLocnMatrixId;
	}

	private Long shiptoJurisdictionId;

	public Long getShiptoJurisdictionId() {
		return shiptoJurisdictionId;
	}

	public void setShiptoJurisdictionId(Long shiptoJurisdictionId) {
		this.shiptoJurisdictionId = shiptoJurisdictionId;
	}

	private String shiptoGeocode;

	public String getShiptoGeocode() {
		return shiptoGeocode;
	}

	public void setShiptoGeocode(String shiptoGeocode) {
		this.shiptoGeocode = shiptoGeocode;
	}

	private String shiptoAddressLine1;

	public String getShiptoAddressLine1() {
		return shiptoAddressLine1;
	}

	public void setShiptoAddressLine1(String shiptoAddressLine1) {
		this.shiptoAddressLine1 = shiptoAddressLine1;
	}

	private String shiptoAddressLine2;

	public String getShiptoAddressLine2() {
		return shiptoAddressLine2;
	}

	public void setShiptoAddressLine2(String shiptoAddressLine2) {
		this.shiptoAddressLine2 = shiptoAddressLine2;
	}

	private String shiptoCity;

	public String getShiptoCity() {
		return shiptoCity;
	}

	public void setShiptoCity(String shiptoCity) {
		this.shiptoCity = shiptoCity;
	}

	private String shiptoCounty;

	public String getShiptoCounty() {
		return shiptoCounty;
	}

	public void setShiptoCounty(String shiptoCounty) {
		this.shiptoCounty = shiptoCounty;
	}

	private String shiptoStateCode;

	public String getShiptoStateCode() {
		return shiptoStateCode;
	}

	public void setShiptoStateCode(String shiptoStateCode) {
		this.shiptoStateCode = shiptoStateCode;
	}

	private String shiptoZip;

	public String getShiptoZip() {
		return shiptoZip;
	}

	public void setShiptoZip(String shiptoZip) {
		this.shiptoZip = shiptoZip;
	}

	private String shiptoZipplus4;

	public String getShiptoZipplus4() {
		return shiptoZipplus4;
	}

	public void setShiptoZipplus4(String shiptoZipplus4) {
		this.shiptoZipplus4 = shiptoZipplus4;
	}

	private String shiptoCountryCode;

	public String getShiptoCountryCode() {
		return shiptoCountryCode;
	}

	public void setShiptoCountryCode(String shiptoCountryCode) {
		this.shiptoCountryCode = shiptoCountryCode;
	}

	private String shiptoStj1Name;

	public String getShiptoStj1Name() {
		return shiptoStj1Name;
	}

	public void setShiptoStj1Name(String shiptoStj1Name) {
		this.shiptoStj1Name = shiptoStj1Name;
	}

	private String shiptoStj2Name;

	public String getShiptoStj2Name() {
		return shiptoStj2Name;
	}

	public void setShiptoStj2Name(String shiptoStj2Name) {
		this.shiptoStj2Name = shiptoStj2Name;
	}

	private String shiptoStj3Name;

	public String getShiptoStj3Name() {
		return shiptoStj3Name;
	}

	public void setShiptoStj3Name(String shiptoStj3Name) {
		this.shiptoStj3Name = shiptoStj3Name;
	}

	private String shiptoStj4Name;

	public String getShiptoStj4Name() {
		return shiptoStj4Name;
	}

	public void setShiptoStj4Name(String shiptoStj4Name) {
		this.shiptoStj4Name = shiptoStj4Name;
	}

	private String shiptoStj5Name;

	public String getShiptoStj5Name() {
		return shiptoStj5Name;
	}

	public void setShiptoStj5Name(String shiptoStj5Name) {
		this.shiptoStj5Name = shiptoStj5Name;
	}

	private Long shipfromEntityId;

	public Long getShipfromEntityId() {
		return shipfromEntityId;
	}

	public void setShipfromEntityId(Long shipfromEntityId) {
		this.shipfromEntityId = shipfromEntityId;
	}

	private String shipfromEntityCode;

	public String getShipfromEntityCode() {
		return shipfromEntityCode;
	}

	public void setShipfromEntityCode(String shipfromEntityCode) {
		this.shipfromEntityCode = shipfromEntityCode;
	}

	private String shipfromLat;

	public String getShipfromLat() {
		return shipfromLat;
	}

	public void setShipfromLat(String shipfromLat) {
		this.shipfromLat = shipfromLat;
	}

	private String shipfromLong;

	public String getShipfromLong() {
		return shipfromLong;
	}

	public void setShipfromLong(String shipfromLong) {
		this.shipfromLong = shipfromLong;
	}

	private Long shipfromLocnMatrixId;

	public Long getShipfromLocnMatrixId() {
		return shipfromLocnMatrixId;
	}

	public void setShipfromLocnMatrixId(Long shipfromLocnMatrixId) {
		this.shipfromLocnMatrixId = shipfromLocnMatrixId;
	}

	private Long shipfromJurisdictionId;

	public Long getShipfromJurisdictionId() {
		return shipfromJurisdictionId;
	}

	public void setShipfromJurisdictionId(Long shipfromJurisdictionId) {
		this.shipfromJurisdictionId = shipfromJurisdictionId;
	}

	private String shipfromGeocode;

	public String getShipfromGeocode() {
		return shipfromGeocode;
	}

	public void setShipfromGeocode(String shipfromGeocode) {
		this.shipfromGeocode = shipfromGeocode;
	}

	private String shipfromAddressLine1;

	public String getShipfromAddressLine1() {
		return shipfromAddressLine1;
	}

	public void setShipfromAddressLine1(String shipfromAddressLine1) {
		this.shipfromAddressLine1 = shipfromAddressLine1;
	}

	private String shipfromAddressLine2;

	public String getShipfromAddressLine2() {
		return shipfromAddressLine2;
	}

	public void setShipfromAddressLine2(String shipfromAddressLine2) {
		this.shipfromAddressLine2 = shipfromAddressLine2;
	}

	private String shipfromCity;

	public String getShipfromCity() {
		return shipfromCity;
	}

	public void setShipfromCity(String shipfromCity) {
		this.shipfromCity = shipfromCity;
	}

	private String shipfromCounty;

	public String getShipfromCounty() {
		return shipfromCounty;
	}

	public void setShipfromCounty(String shipfromCounty) {
		this.shipfromCounty = shipfromCounty;
	}

	private String shipfromStateCode;

	public String getShipfromStateCode() {
		return shipfromStateCode;
	}

	public void setShipfromStateCode(String shipfromStateCode) {
		this.shipfromStateCode = shipfromStateCode;
	}

	private String shipfromZip;

	public String getShipfromZip() {
		return shipfromZip;
	}

	public void setShipfromZip(String shipfromZip) {
		this.shipfromZip = shipfromZip;
	}

	private String shipfromZipplus4;

	public String getShipfromZipplus4() {
		return shipfromZipplus4;
	}

	public void setShipfromZipplus4(String shipfromZipplus4) {
		this.shipfromZipplus4 = shipfromZipplus4;
	}

	private String shipfromCountryCode;

	public String getShipfromCountryCode() {
		return shipfromCountryCode;
	}

	public void setShipfromCountryCode(String shipfromCountryCode) {
		this.shipfromCountryCode = shipfromCountryCode;
	}

	private String shipfromStj1Name;

	public String getShipfromStj1Name() {
		return shipfromStj1Name;
	}

	public void setShipfromStj1Name(String shipfromStj1Name) {
		this.shipfromStj1Name = shipfromStj1Name;
	}

	private String shipfromStj2Name;

	public String getShipfromStj2Name() {
		return shipfromStj2Name;
	}

	public void setShipfromStj2Name(String shipfromStj2Name) {
		this.shipfromStj2Name = shipfromStj2Name;
	}

	private String shipfromStj3Name;

	public String getShipfromStj3Name() {
		return shipfromStj3Name;
	}

	public void setShipfromStj3Name(String shipfromStj3Name) {
		this.shipfromStj3Name = shipfromStj3Name;
	}

	private String shipfromStj4Name;

	public String getShipfromStj4Name() {
		return shipfromStj4Name;
	}

	public void setShipfromStj4Name(String shipfromStj4Name) {
		this.shipfromStj4Name = shipfromStj4Name;
	}

	private String shipfromStj5Name;

	public String getShipfromStj5Name() {
		return shipfromStj5Name;
	}

	public void setShipfromStj5Name(String shipfromStj5Name) {
		this.shipfromStj5Name = shipfromStj5Name;
	}

	private Long ordracptEntityId;

	public Long getOrdracptEntityId() {
		return ordracptEntityId;
	}

	public void setOrdracptEntityId(Long ordracptEntityId) {
		this.ordracptEntityId = ordracptEntityId;
	}

	private String ordracptEntityCode;

	public String getOrdracptEntityCode() {
		return ordracptEntityCode;
	}

	public void setOrdracptEntityCode(String ordracptEntityCode) {
		this.ordracptEntityCode = ordracptEntityCode;
	}

	private String ordracptLat;

	public String getOrdracptLat() {
		return ordracptLat;
	}

	public void setOrdracptLat(String ordracptLat) {
		this.ordracptLat = ordracptLat;
	}

	private String ordracptLong;

	public String getOrdracptLong() {
		return ordracptLong;
	}

	public void setOrdracptLong(String ordracptLong) {
		this.ordracptLong = ordracptLong;
	}

	private Long ordracptLocnMatrixId;

	public Long getOrdracptLocnMatrixId() {
		return ordracptLocnMatrixId;
	}

	public void setOrdracptLocnMatrixId(Long ordracptLocnMatrixId) {
		this.ordracptLocnMatrixId = ordracptLocnMatrixId;
	}

	private Long ordracptJurisdictionId;

	public Long getOrdracptJurisdictionId() {
		return ordracptJurisdictionId;
	}

	public void setOrdracptJurisdictionId(Long ordracptJurisdictionId) {
		this.ordracptJurisdictionId = ordracptJurisdictionId;
	}

	private String ordracptGeocode;

	public String getOrdracptGeocode() {
		return ordracptGeocode;
	}

	public void setOrdracptGeocode(String ordracptGeocode) {
		this.ordracptGeocode = ordracptGeocode;
	}

	private String ordracptAddressLine1;

	public String getOrdracptAddressLine1() {
		return ordracptAddressLine1;
	}

	public void setOrdracptAddressLine1(String ordracptAddressLine1) {
		this.ordracptAddressLine1 = ordracptAddressLine1;
	}

	private String ordracptAddressLine2;

	public String getOrdracptAddressLine2() {
		return ordracptAddressLine2;
	}

	public void setOrdracptAddressLine2(String ordracptAddressLine2) {
		this.ordracptAddressLine2 = ordracptAddressLine2;
	}

	private String ordracptCity;

	public String getOrdracptCity() {
		return ordracptCity;
	}

	public void setOrdracptCity(String ordracptCity) {
		this.ordracptCity = ordracptCity;
	}

	private String ordracptCounty;

	public String getOrdracptCounty() {
		return ordracptCounty;
	}

	public void setOrdracptCounty(String ordracptCounty) {
		this.ordracptCounty = ordracptCounty;
	}

	private String ordracptStateCode;

	public String getOrdracptStateCode() {
		return ordracptStateCode;
	}

	public void setOrdracptStateCode(String ordracptStateCode) {
		this.ordracptStateCode = ordracptStateCode;
	}

	private String ordracptZip;

	public String getOrdracptZip() {
		return ordracptZip;
	}

	public void setOrdracptZip(String ordracptZip) {
		this.ordracptZip = ordracptZip;
	}

	private String ordracptZipplus4;

	public String getOrdracptZipplus4() {
		return ordracptZipplus4;
	}

	public void setOrdracptZipplus4(String ordracptZipplus4) {
		this.ordracptZipplus4 = ordracptZipplus4;
	}

	private String ordracptCountryCode;

	public String getOrdracptCountryCode() {
		return ordracptCountryCode;
	}

	public void setOrdracptCountryCode(String ordracptCountryCode) {
		this.ordracptCountryCode = ordracptCountryCode;
	}

	private String ordracptStj1Name;

	public String getOrdracptStj1Name() {
		return ordracptStj1Name;
	}

	public void setOrdracptStj1Name(String ordracptStj1Name) {
		this.ordracptStj1Name = ordracptStj1Name;
	}

	private String ordracptStj2Name;

	public String getOrdracptStj2Name() {
		return ordracptStj2Name;
	}

	public void setOrdracptStj2Name(String ordracptStj2Name) {
		this.ordracptStj2Name = ordracptStj2Name;
	}

	private String ordracptStj3Name;

	public String getOrdracptStj3Name() {
		return ordracptStj3Name;
	}

	public void setOrdracptStj3Name(String ordracptStj3Name) {
		this.ordracptStj3Name = ordracptStj3Name;
	}

	private String ordracptStj4Name;

	public String getOrdracptStj4Name() {
		return ordracptStj4Name;
	}

	public void setOrdracptStj4Name(String ordracptStj4Name) {
		this.ordracptStj4Name = ordracptStj4Name;
	}

	private String ordracptStj5Name;

	public String getOrdracptStj5Name() {
		return ordracptStj5Name;
	}

	public void setOrdracptStj5Name(String ordracptStj5Name) {
		this.ordracptStj5Name = ordracptStj5Name;
	}

	private Long ordrorgnEntityId;

	public Long getOrdrorgnEntityId() {
		return ordrorgnEntityId;
	}

	public void setOrdrorgnEntityId(Long ordrorgnEntityId) {
		this.ordrorgnEntityId = ordrorgnEntityId;
	}

	private String ordrorgnEntityCode;

	public String getOrdrorgnEntityCode() {
		return ordrorgnEntityCode;
	}

	public void setOrdrorgnEntityCode(String ordrorgnEntityCode) {
		this.ordrorgnEntityCode = ordrorgnEntityCode;
	}

	private String ordrorgnLat;

	public String getOrdrorgnLat() {
		return ordrorgnLat;
	}

	public void setOrdrorgnLat(String ordrorgnLat) {
		this.ordrorgnLat = ordrorgnLat;
	}

	private String ordrorgnLong;

	public String getOrdrorgnLong() {
		return ordrorgnLong;
	}

	public void setOrdrorgnLong(String ordrorgnLong) {
		this.ordrorgnLong = ordrorgnLong;
	}

	private Long ordrorgnLocnMatrixId;

	public Long getOrdrorgnLocnMatrixId() {
		return ordrorgnLocnMatrixId;
	}

	public void setOrdrorgnLocnMatrixId(Long ordrorgnLocnMatrixId) {
		this.ordrorgnLocnMatrixId = ordrorgnLocnMatrixId;
	}

	private Long ordrorgnJurisdictionId;

	public Long getOrdrorgnJurisdictionId() {
		return ordrorgnJurisdictionId;
	}

	public void setOrdrorgnJurisdictionId(Long ordrorgnJurisdictionId) {
		this.ordrorgnJurisdictionId = ordrorgnJurisdictionId;
	}

	private String ordrorgnGeocode;

	public String getOrdrorgnGeocode() {
		return ordrorgnGeocode;
	}

	public void setOrdrorgnGeocode(String ordrorgnGeocode) {
		this.ordrorgnGeocode = ordrorgnGeocode;
	}

	private String ordrorgnAddressLine1;

	public String getOrdrorgnAddressLine1() {
		return ordrorgnAddressLine1;
	}

	public void setOrdrorgnAddressLine1(String ordrorgnAddressLine1) {
		this.ordrorgnAddressLine1 = ordrorgnAddressLine1;
	}

	private String ordrorgnAddressLine2;

	public String getOrdrorgnAddressLine2() {
		return ordrorgnAddressLine2;
	}

	public void setOrdrorgnAddressLine2(String ordrorgnAddressLine2) {
		this.ordrorgnAddressLine2 = ordrorgnAddressLine2;
	}

	private String ordrorgnCity;

	public String getOrdrorgnCity() {
		return ordrorgnCity;
	}

	public void setOrdrorgnCity(String ordrorgnCity) {
		this.ordrorgnCity = ordrorgnCity;
	}

	private String ordrorgnCounty;

	public String getOrdrorgnCounty() {
		return ordrorgnCounty;
	}

	public void setOrdrorgnCounty(String ordrorgnCounty) {
		this.ordrorgnCounty = ordrorgnCounty;
	}

	private String ordrorgnStateCode;

	public String getOrdrorgnStateCode() {
		return ordrorgnStateCode;
	}

	public void setOrdrorgnStateCode(String ordrorgnStateCode) {
		this.ordrorgnStateCode = ordrorgnStateCode;
	}

	private String ordrorgnZip;

	public String getOrdrorgnZip() {
		return ordrorgnZip;
	}

	public void setOrdrorgnZip(String ordrorgnZip) {
		this.ordrorgnZip = ordrorgnZip;
	}

	private String ordrorgnZipplus4;

	public String getOrdrorgnZipplus4() {
		return ordrorgnZipplus4;
	}

	public void setOrdrorgnZipplus4(String ordrorgnZipplus4) {
		this.ordrorgnZipplus4 = ordrorgnZipplus4;
	}

	private String ordrorgnCountryCode;

	public String getOrdrorgnCountryCode() {
		return ordrorgnCountryCode;
	}

	public void setOrdrorgnCountryCode(String ordrorgnCountryCode) {
		this.ordrorgnCountryCode = ordrorgnCountryCode;
	}

	private String ordrorgnStj1Name;

	public String getOrdrorgnStj1Name() {
		return ordrorgnStj1Name;
	}

	public void setOrdrorgnStj1Name(String ordrorgnStj1Name) {
		this.ordrorgnStj1Name = ordrorgnStj1Name;
	}

	private String ordrorgnStj2Name;

	public String getOrdrorgnStj2Name() {
		return ordrorgnStj2Name;
	}

	public void setOrdrorgnStj2Name(String ordrorgnStj2Name) {
		this.ordrorgnStj2Name = ordrorgnStj2Name;
	}

	private String ordrorgnStj3Name;

	public String getOrdrorgnStj3Name() {
		return ordrorgnStj3Name;
	}

	public void setOrdrorgnStj3Name(String ordrorgnStj3Name) {
		this.ordrorgnStj3Name = ordrorgnStj3Name;
	}

	private String ordrorgnStj4Name;

	public String getOrdrorgnStj4Name() {
		return ordrorgnStj4Name;
	}

	public void setOrdrorgnStj4Name(String ordrorgnStj4Name) {
		this.ordrorgnStj4Name = ordrorgnStj4Name;
	}

	private String ordrorgnStj5Name;

	public String getOrdrorgnStj5Name() {
		return ordrorgnStj5Name;
	}

	public void setOrdrorgnStj5Name(String ordrorgnStj5Name) {
		this.ordrorgnStj5Name = ordrorgnStj5Name;
	}

	private Long firstuseEntityId;

	public Long getFirstuseEntityId() {
		return firstuseEntityId;
	}

	public void setFirstuseEntityId(Long firstuseEntityId) {
		this.firstuseEntityId = firstuseEntityId;
	}

	private String firstuseEntityCode;

	public String getFirstuseEntityCode() {
		return firstuseEntityCode;
	}

	public void setFirstuseEntityCode(String firstuseEntityCode) {
		this.firstuseEntityCode = firstuseEntityCode;
	}

	private String firstuseLat;

	public String getFirstuseLat() {
		return firstuseLat;
	}

	public void setFirstuseLat(String firstuseLat) {
		this.firstuseLat = firstuseLat;
	}

	private String firstuseLong;

	public String getFirstuseLong() {
		return firstuseLong;
	}

	public void setFirstuseLong(String firstuseLong) {
		this.firstuseLong = firstuseLong;
	}

	private Long firstuseLocnMatrixId;

	public Long getFirstuseLocnMatrixId() {
		return firstuseLocnMatrixId;
	}

	public void setFirstuseLocnMatrixId(Long firstuseLocnMatrixId) {
		this.firstuseLocnMatrixId = firstuseLocnMatrixId;
	}

	private Long firstuseJurisdictionId;

	public Long getFirstuseJurisdictionId() {
		return firstuseJurisdictionId;
	}

	public void setFirstuseJurisdictionId(Long firstuseJurisdictionId) {
		this.firstuseJurisdictionId = firstuseJurisdictionId;
	}

	private String firstuseGeocode;

	public String getFirstuseGeocode() {
		return firstuseGeocode;
	}

	public void setFirstuseGeocode(String firstuseGeocode) {
		this.firstuseGeocode = firstuseGeocode;
	}

	private String firstuseAddressLine1;

	public String getFirstuseAddressLine1() {
		return firstuseAddressLine1;
	}

	public void setFirstuseAddressLine1(String firstuseAddressLine1) {
		this.firstuseAddressLine1 = firstuseAddressLine1;
	}

	private String firstuseAddressLine2;

	public String getFirstuseAddressLine2() {
		return firstuseAddressLine2;
	}

	public void setFirstuseAddressLine2(String firstuseAddressLine2) {
		this.firstuseAddressLine2 = firstuseAddressLine2;
	}

	private String firstuseCity;

	public String getFirstuseCity() {
		return firstuseCity;
	}

	public void setFirstuseCity(String firstuseCity) {
		this.firstuseCity = firstuseCity;
	}

	private String firstuseCounty;

	public String getFirstuseCounty() {
		return firstuseCounty;
	}

	public void setFirstuseCounty(String firstuseCounty) {
		this.firstuseCounty = firstuseCounty;
	}

	private String firstuseStateCode;

	public String getFirstuseStateCode() {
		return firstuseStateCode;
	}

	public void setFirstuseStateCode(String firstuseStateCode) {
		this.firstuseStateCode = firstuseStateCode;
	}

	private String firstuseZip;

	public String getFirstuseZip() {
		return firstuseZip;
	}

	public void setFirstuseZip(String firstuseZip) {
		this.firstuseZip = firstuseZip;
	}

	private String firstuseZipplus4;

	public String getFirstuseZipplus4() {
		return firstuseZipplus4;
	}

	public void setFirstuseZipplus4(String firstuseZipplus4) {
		this.firstuseZipplus4 = firstuseZipplus4;
	}

	private String firstuseCountryCode;

	public String getFirstuseCountryCode() {
		return firstuseCountryCode;
	}

	public void setFirstuseCountryCode(String firstuseCountryCode) {
		this.firstuseCountryCode = firstuseCountryCode;
	}

	private String firstuseStj1Name;

	public String getFirstuseStj1Name() {
		return firstuseStj1Name;
	}

	public void setFirstuseStj1Name(String firstuseStj1Name) {
		this.firstuseStj1Name = firstuseStj1Name;
	}

	private String firstuseStj2Name;

	public String getFirstuseStj2Name() {
		return firstuseStj2Name;
	}

	public void setFirstuseStj2Name(String firstuseStj2Name) {
		this.firstuseStj2Name = firstuseStj2Name;
	}

	private String firstuseStj3Name;

	public String getFirstuseStj3Name() {
		return firstuseStj3Name;
	}

	public void setFirstuseStj3Name(String firstuseStj3Name) {
		this.firstuseStj3Name = firstuseStj3Name;
	}

	private String firstuseStj4Name;

	public String getFirstuseStj4Name() {
		return firstuseStj4Name;
	}

	public void setFirstuseStj4Name(String firstuseStj4Name) {
		this.firstuseStj4Name = firstuseStj4Name;
	}

	private String firstuseStj5Name;

	public String getFirstuseStj5Name() {
		return firstuseStj5Name;
	}

	public void setFirstuseStj5Name(String firstuseStj5Name) {
		this.firstuseStj5Name = firstuseStj5Name;
	}

	private Long billtoEntityId;

	public Long getBilltoEntityId() {
		return billtoEntityId;
	}

	public void setBilltoEntityId(Long billtoEntityId) {
		this.billtoEntityId = billtoEntityId;
	}

	private String billtoEntityCode;

	public String getBilltoEntityCode() {
		return billtoEntityCode;
	}

	public void setBilltoEntityCode(String billtoEntityCode) {
		this.billtoEntityCode = billtoEntityCode;
	}

	private String billtoLat;

	public String getBilltoLat() {
		return billtoLat;
	}

	public void setBilltoLat(String billtoLat) {
		this.billtoLat = billtoLat;
	}

	private String billtoLong;

	public String getBilltoLong() {
		return billtoLong;
	}

	public void setBilltoLong(String billtoLong) {
		this.billtoLong = billtoLong;
	}

	private Long billtoLocnMatrixId;

	public Long getBilltoLocnMatrixId() {
		return billtoLocnMatrixId;
	}

	public void setBilltoLocnMatrixId(Long billtoLocnMatrixId) {
		this.billtoLocnMatrixId = billtoLocnMatrixId;
	}

	private Long billtoJurisdictionId;

	public Long getBilltoJurisdictionId() {
		return billtoJurisdictionId;
	}

	public void setBilltoJurisdictionId(Long billtoJurisdictionId) {
		this.billtoJurisdictionId = billtoJurisdictionId;
	}

	private String billtoGeocode;

	public String getBilltoGeocode() {
		return billtoGeocode;
	}

	public void setBilltoGeocode(String billtoGeocode) {
		this.billtoGeocode = billtoGeocode;
	}

	private String billtoAddressLine1;

	public String getBilltoAddressLine1() {
		return billtoAddressLine1;
	}

	public void setBilltoAddressLine1(String billtoAddressLine1) {
		this.billtoAddressLine1 = billtoAddressLine1;
	}

	private String billtoAddressLine2;

	public String getBilltoAddressLine2() {
		return billtoAddressLine2;
	}

	public void setBilltoAddressLine2(String billtoAddressLine2) {
		this.billtoAddressLine2 = billtoAddressLine2;
	}

	private String billtoCity;

	public String getBilltoCity() {
		return billtoCity;
	}

	public void setBilltoCity(String billtoCity) {
		this.billtoCity = billtoCity;
	}

	private String billtoCounty;

	public String getBilltoCounty() {
		return billtoCounty;
	}

	public void setBilltoCounty(String billtoCounty) {
		this.billtoCounty = billtoCounty;
	}

	private String billtoStateCode;

	public String getBilltoStateCode() {
		return billtoStateCode;
	}

	public void setBilltoStateCode(String billtoStateCode) {
		this.billtoStateCode = billtoStateCode;
	}

	private String billtoZip;

	public String getBilltoZip() {
		return billtoZip;
	}

	public void setBilltoZip(String billtoZip) {
		this.billtoZip = billtoZip;
	}

	private String billtoZipplus4;

	public String getBilltoZipplus4() {
		return billtoZipplus4;
	}

	public void setBilltoZipplus4(String billtoZipplus4) {
		this.billtoZipplus4 = billtoZipplus4;
	}

	private String billtoCountryCode;

	public String getBilltoCountryCode() {
		return billtoCountryCode;
	}

	public void setBilltoCountryCode(String billtoCountryCode) {
		this.billtoCountryCode = billtoCountryCode;
	}

	private String billtoStj1Name;

	public String getBilltoStj1Name() {
		return billtoStj1Name;
	}

	public void setBilltoStj1Name(String billtoStj1Name) {
		this.billtoStj1Name = billtoStj1Name;
	}

	private String billtoStj2Name;

	public String getBilltoStj2Name() {
		return billtoStj2Name;
	}

	public void setBilltoStj2Name(String billtoStj2Name) {
		this.billtoStj2Name = billtoStj2Name;
	}

	private String billtoStj3Name;

	public String getBilltoStj3Name() {
		return billtoStj3Name;
	}

	public void setBilltoStj3Name(String billtoStj3Name) {
		this.billtoStj3Name = billtoStj3Name;
	}

	private String billtoStj4Name;

	public String getBilltoStj4Name() {
		return billtoStj4Name;
	}

	public void setBilltoStj4Name(String billtoStj4Name) {
		this.billtoStj4Name = billtoStj4Name;
	}

	private String billtoStj5Name;

	public String getBilltoStj5Name() {
		return billtoStj5Name;
	}

	public void setBilltoStj5Name(String billtoStj5Name) {
		this.billtoStj5Name = billtoStj5Name;
	}

	private Long ttlxfrEntityId;

	public Long getTtlxfrEntityId() {
		return ttlxfrEntityId;
	}

	public void setTtlxfrEntityId(Long ttlxfrEntityId) {
		this.ttlxfrEntityId = ttlxfrEntityId;
	}

	private String ttlxfrEntityCode;

	public String getTtlxfrEntityCode() {
		return ttlxfrEntityCode;
	}

	public void setTtlxfrEntityCode(String ttlxfrEntityCode) {
		this.ttlxfrEntityCode = ttlxfrEntityCode;
	}

	private String ttlxfrLat;

	public String getTtlxfrLat() {
		return ttlxfrLat;
	}

	public void setTtlxfrLat(String ttlxfrLat) {
		this.ttlxfrLat = ttlxfrLat;
	}

	private String ttlxfrLong;

	public String getTtlxfrLong() {
		return ttlxfrLong;
	}

	public void setTtlxfrLong(String ttlxfrLong) {
		this.ttlxfrLong = ttlxfrLong;
	}

	private Long ttlxfrLocnMatrixId;

	public Long getTtlxfrLocnMatrixId() {
		return ttlxfrLocnMatrixId;
	}

	public void setTtlxfrLocnMatrixId(Long ttlxfrLocnMatrixId) {
		this.ttlxfrLocnMatrixId = ttlxfrLocnMatrixId;
	}

	private Long ttlxfrJurisdictionId;

	public Long getTtlxfrJurisdictionId() {
		return ttlxfrJurisdictionId;
	}

	public void setTtlxfrJurisdictionId(Long ttlxfrJurisdictionId) {
		this.ttlxfrJurisdictionId = ttlxfrJurisdictionId;
	}

	private String ttlxfrGeocode;

	public String getTtlxfrGeocode() {
		return ttlxfrGeocode;
	}

	public void setTtlxfrGeocode(String ttlxfrGeocode) {
		this.ttlxfrGeocode = ttlxfrGeocode;
	}

	private String ttlxfrAddressLine1;

	public String getTtlxfrAddressLine1() {
		return ttlxfrAddressLine1;
	}

	public void setTtlxfrAddressLine1(String ttlxfrAddressLine1) {
		this.ttlxfrAddressLine1 = ttlxfrAddressLine1;
	}

	private String ttlxfrAddressLine2;

	public String getTtlxfrAddressLine2() {
		return ttlxfrAddressLine2;
	}

	public void setTtlxfrAddressLine2(String ttlxfrAddressLine2) {
		this.ttlxfrAddressLine2 = ttlxfrAddressLine2;
	}

	private String ttlxfrCity;

	public String getTtlxfrCity() {
		return ttlxfrCity;
	}

	public void setTtlxfrCity(String ttlxfrCity) {
		this.ttlxfrCity = ttlxfrCity;
	}

	private String ttlxfrCounty;

	public String getTtlxfrCounty() {
		return ttlxfrCounty;
	}

	public void setTtlxfrCounty(String ttlxfrCounty) {
		this.ttlxfrCounty = ttlxfrCounty;
	}

	private String ttlxfrStateCode;

	public String getTtlxfrStateCode() {
		return ttlxfrStateCode;
	}

	public void setTtlxfrStateCode(String ttlxfrStateCode) {
		this.ttlxfrStateCode = ttlxfrStateCode;
	}

	private String ttlxfrZip;

	public String getTtlxfrZip() {
		return ttlxfrZip;
	}

	public void setTtlxfrZip(String ttlxfrZip) {
		this.ttlxfrZip = ttlxfrZip;
	}

	private String ttlxfrZipplus4;

	public String getTtlxfrZipplus4() {
		return ttlxfrZipplus4;
	}

	public void setTtlxfrZipplus4(String ttlxfrZipplus4) {
		this.ttlxfrZipplus4 = ttlxfrZipplus4;
	}

	private String ttlxfrCountryCode;

	public String getTtlxfrCountryCode() {
		return ttlxfrCountryCode;
	}

	public void setTtlxfrCountryCode(String ttlxfrCountryCode) {
		this.ttlxfrCountryCode = ttlxfrCountryCode;
	}

	private String ttlxfrStj1Name;

	public String getTtlxfrStj1Name() {
		return ttlxfrStj1Name;
	}

	public void setTtlxfrStj1Name(String ttlxfrStj1Name) {
		this.ttlxfrStj1Name = ttlxfrStj1Name;
	}

	private String ttlxfrStj2Name;

	public String getTtlxfrStj2Name() {
		return ttlxfrStj2Name;
	}

	public void setTtlxfrStj2Name(String ttlxfrStj2Name) {
		this.ttlxfrStj2Name = ttlxfrStj2Name;
	}

	private String ttlxfrStj3Name;

	public String getTtlxfrStj3Name() {
		return ttlxfrStj3Name;
	}

	public void setTtlxfrStj3Name(String ttlxfrStj3Name) {
		this.ttlxfrStj3Name = ttlxfrStj3Name;
	}

	private String ttlxfrStj4Name;

	public String getTtlxfrStj4Name() {
		return ttlxfrStj4Name;
	}

	public void setTtlxfrStj4Name(String ttlxfrStj4Name) {
		this.ttlxfrStj4Name = ttlxfrStj4Name;
	}

	private String ttlxfrStj5Name;

	public String getTtlxfrStj5Name() {
		return ttlxfrStj5Name;
	}

	public void setTtlxfrStj5Name(String ttlxfrStj5Name) {
		this.ttlxfrStj5Name = ttlxfrStj5Name;
	}

	/* TB_PURCHTRANS_JURDTL */

	private Long countrySitusMatrixId;

	public Long getCountrySitusMatrixId() {
		return countrySitusMatrixId;
	}

	public void setCountrySitusMatrixId(Long countrySitusMatrixId) {
		this.countrySitusMatrixId = countrySitusMatrixId;
	}

	private Long stateSitusMatrixId;

	public Long getStateSitusMatrixId() {
		return stateSitusMatrixId;
	}

	public void setStateSitusMatrixId(Long stateSitusMatrixId) {
		this.stateSitusMatrixId = stateSitusMatrixId;
	}

	private Long countySitusMatrixId;

	public Long getCountySitusMatrixId() {
		return countySitusMatrixId;
	}

	public void setCountySitusMatrixId(Long countySitusMatrixId) {
		this.countySitusMatrixId = countySitusMatrixId;
	}

	private Long citySitusMatrixId;

	public Long getCitySitusMatrixId() {
		return citySitusMatrixId;
	}

	public void setCitySitusMatrixId(Long citySitusMatrixId) {
		this.citySitusMatrixId = citySitusMatrixId;
	}

	private Long stj1SitusMatrixId;

	public Long getStj1SitusMatrixId() {
		return stj1SitusMatrixId;
	}

	public void setStj1SitusMatrixId(Long stj1SitusMatrixId) {
		this.stj1SitusMatrixId = stj1SitusMatrixId;
	}

	private Long stj2SitusMatrixId;

	public Long getStj2SitusMatrixId() {
		return stj2SitusMatrixId;
	}

	public void setStj2SitusMatrixId(Long stj2SitusMatrixId) {
		this.stj2SitusMatrixId = stj2SitusMatrixId;
	}

	private Long stj3SitusMatrixId;

	public Long getStj3SitusMatrixId() {
		return stj3SitusMatrixId;
	}

	public void setStj3SitusMatrixId(Long stj3SitusMatrixId) {
		this.stj3SitusMatrixId = stj3SitusMatrixId;
	}

	private Long stj4SitusMatrixId;

	public Long getStj4SitusMatrixId() {
		return stj4SitusMatrixId;
	}

	public void setStj4SitusMatrixId(Long stj4SitusMatrixId) {
		this.stj4SitusMatrixId = stj4SitusMatrixId;
	}

	private Long stj5SitusMatrixId;

	public Long getStj5SitusMatrixId() {
		return stj5SitusMatrixId;
	}

	public void setStj5SitusMatrixId(Long stj5SitusMatrixId) {
		this.stj5SitusMatrixId = stj5SitusMatrixId;
	}

	private String countryCode;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	private String stateCode;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	private String countyName;

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	private String cityName;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	private String stj1Name;

	public String getStj1Name() {
		return stj1Name;
	}

	public void setStj1Name(String stj1Name) {
		this.stj1Name = stj1Name;
	}

	private String stj2Name;

	public String getStj2Name() {
		return stj2Name;
	}

	public void setStj2Name(String stj2Name) {
		this.stj2Name = stj2Name;
	}

	private String stj3Name;

	public String getStj3Name() {
		return stj3Name;
	}

	public void setStj3Name(String stj3Name) {
		this.stj3Name = stj3Name;
	}

	private String stj4Name;

	public String getStj4Name() {
		return stj4Name;
	}

	public void setStj4Name(String stj4Name) {
		this.stj4Name = stj4Name;
	}

	private String stj5Name;

	public String getStj5Name() {
		return stj5Name;
	}

	public void setStj5Name(String stj5Name) {
		this.stj5Name = stj5Name;
	}

	private String stj6Name;

	public String getStj6Name() {
		return stj6Name;
	}

	public void setStj6Name(String stj6Name) {
		this.stj6Name = stj6Name;
	}

	private String stj7Name;

	public String getStj7Name() {
		return stj7Name;
	}

	public void setStj7Name(String stj7Name) {
		this.stj7Name = stj7Name;
	}

	private String stj8Name;

	public String getStj8Name() {
		return stj8Name;
	}

	public void setStj8Name(String stj8Name) {
		this.stj8Name = stj8Name;
	}

	private String stj9Name;

	public String getStj9Name() {
		return stj9Name;
	}

	public void setStj9Name(String stj9Name) {
		this.stj9Name = stj9Name;
	}

	private String stj10Name;

	public String getStj10Name() {
		return stj10Name;
	}

	public void setStj10Name(String stj10Name) {
		this.stj10Name = stj10Name;
	}

	private Long countryVendorNexusDtlId;

	public Long getCountryVendorNexusDtlId() {
		return countryVendorNexusDtlId;
	}

	public void setCountryVendorNexusDtlId(Long countryVendorNexusDtlId) {
		this.countryVendorNexusDtlId = countryVendorNexusDtlId;
	}

	private Long stateVendorNexusDtlId;

	public Long getStateVendorNexusDtlId() {
		return stateVendorNexusDtlId;
	}

	public void setStateVendorNexusDtlId(Long stateVendorNexusDtlId) {
		this.stateVendorNexusDtlId = stateVendorNexusDtlId;
	}

	private Long countyVendorNexusDtlId;

	public Long getCountyVendorNexusDtlId() {
		return countyVendorNexusDtlId;
	}

	public void setCountyVendorNexusDtlId(Long countyVendorNexusDtlId) {
		this.countyVendorNexusDtlId = countyVendorNexusDtlId;
	}

	private Long cityVendorNexusDtlId;

	public Long getCityVendorNexusDtlId() {
		return cityVendorNexusDtlId;
	}

	public void setCityVendorNexusDtlId(Long cityVendorNexusDtlId) {
		this.cityVendorNexusDtlId = cityVendorNexusDtlId;
	}

	private Long countryTaxcodeDetailId;

	public Long getCountryTaxcodeDetailId() {
		return countryTaxcodeDetailId;
	}

	public void setCountryTaxcodeDetailId(Long countryTaxcodeDetailId) {
		this.countryTaxcodeDetailId = countryTaxcodeDetailId;
	}

	private Long stateTaxcodeDetailId;

	public Long getStateTaxcodeDetailId() {
		return stateTaxcodeDetailId;
	}

	public void setStateTaxcodeDetailId(Long stateTaxcodeDetailId) {
		this.stateTaxcodeDetailId = stateTaxcodeDetailId;
	}

	private Long countyTaxcodeDetailId;

	public Long getCountyTaxcodeDetailId() {
		return countyTaxcodeDetailId;
	}

	public void setCountyTaxcodeDetailId(Long countyTaxcodeDetailId) {
		this.countyTaxcodeDetailId = countyTaxcodeDetailId;
	}

	private Long cityTaxcodeDetailId;

	public Long getCityTaxcodeDetailId() {
		return cityTaxcodeDetailId;
	}

	public void setCityTaxcodeDetailId(Long cityTaxcodeDetailId) {
		this.cityTaxcodeDetailId = cityTaxcodeDetailId;
	}

	private Long stj1TaxcodeDetailId;

	public Long getStj1TaxcodeDetailId() {
		return stj1TaxcodeDetailId;
	}

	public void setStj1TaxcodeDetailId(Long stj1TaxcodeDetailId) {
		this.stj1TaxcodeDetailId = stj1TaxcodeDetailId;
	}

	private Long stj2TaxcodeDetailId;

	public Long getStj2TaxcodeDetailId() {
		return stj2TaxcodeDetailId;
	}

	public void setStj2TaxcodeDetailId(Long stj2TaxcodeDetailId) {
		this.stj2TaxcodeDetailId = stj2TaxcodeDetailId;
	}

	private Long stj3TaxcodeDetailId;

	public Long getStj3TaxcodeDetailId() {
		return stj3TaxcodeDetailId;
	}

	public void setStj3TaxcodeDetailId(Long stj3TaxcodeDetailId) {
		this.stj3TaxcodeDetailId = stj3TaxcodeDetailId;
	}

	private Long stj4TaxcodeDetailId;

	public Long getStj4TaxcodeDetailId() {
		return stj4TaxcodeDetailId;
	}

	public void setStj4TaxcodeDetailId(Long stj4TaxcodeDetailId) {
		this.stj4TaxcodeDetailId = stj4TaxcodeDetailId;
	}

	private Long stj5TaxcodeDetailId;

	public Long getStj5TaxcodeDetailId() {
		return stj5TaxcodeDetailId;
	}

	public void setStj5TaxcodeDetailId(Long stj5TaxcodeDetailId) {
		this.stj5TaxcodeDetailId = stj5TaxcodeDetailId;
	}

	private Long stj6TaxcodeDetailId;

	public Long getStj6TaxcodeDetailId() {
		return stj6TaxcodeDetailId;
	}

	public void setStj6TaxcodeDetailId(Long stj6TaxcodeDetailId) {
		this.stj6TaxcodeDetailId = stj6TaxcodeDetailId;
	}

	private Long stj7TaxcodeDetailId;

	public Long getStj7TaxcodeDetailId() {
		return stj7TaxcodeDetailId;
	}

	public void setStj7TaxcodeDetailId(Long stj7TaxcodeDetailId) {
		this.stj7TaxcodeDetailId = stj7TaxcodeDetailId;
	}

	private Long stj8TaxcodeDetailId;

	public Long getStj8TaxcodeDetailId() {
		return stj8TaxcodeDetailId;
	}

	public void setStj8TaxcodeDetailId(Long stj8TaxcodeDetailId) {
		this.stj8TaxcodeDetailId = stj8TaxcodeDetailId;
	}

	private Long stj9TaxcodeDetailId;

	public Long getStj9TaxcodeDetailId() {
		return stj9TaxcodeDetailId;
	}

	public void setStj9TaxcodeDetailId(Long stj9TaxcodeDetailId) {
		this.stj9TaxcodeDetailId = stj9TaxcodeDetailId;
	}

	private Long stj10TaxcodeDetailId;

	public Long getStj10TaxcodeDetailId() {
		return stj10TaxcodeDetailId;
	}

	public void setStj10TaxcodeDetailId(Long stj10TaxcodeDetailId) {
		this.stj10TaxcodeDetailId = stj10TaxcodeDetailId;
	}

	private String countryTaxtypeCode;

	public String getCountryTaxtypeCode() {
		return countryTaxtypeCode;
	}

	public void setCountryTaxtypeCode(String countryTaxtypeCode) {
		this.countryTaxtypeCode = countryTaxtypeCode;
	}

	private String stateTaxtypeCode;

	public String getStateTaxtypeCode() {
		return stateTaxtypeCode;
	}

	public void setStateTaxtypeCode(String stateTaxtypeCode) {
		this.stateTaxtypeCode = stateTaxtypeCode;
	}

	private String countyTaxtypeCode;

	public String getCountyTaxtypeCode() {
		return countyTaxtypeCode;
	}

	public void setCountyTaxtypeCode(String countyTaxtypeCode) {
		this.countyTaxtypeCode = countyTaxtypeCode;
	}

	private String cityTaxtypeCode;

	public String getCityTaxtypeCode() {
		return cityTaxtypeCode;
	}

	public void setCityTaxtypeCode(String cityTaxtypeCode) {
		this.cityTaxtypeCode = cityTaxtypeCode;
	}

	private String stj1TaxtypeCode;

	public String getStj1TaxtypeCode() {
		return stj1TaxtypeCode;
	}

	public void setStj1TaxtypeCode(String stj1TaxtypeCode) {
		this.stj1TaxtypeCode = stj1TaxtypeCode;
	}

	private String stj2TaxtypeCode;

	public String getStj2TaxtypeCode() {
		return stj2TaxtypeCode;
	}

	public void setStj2TaxtypeCode(String stj2TaxtypeCode) {
		this.stj2TaxtypeCode = stj2TaxtypeCode;
	}

	private String stj3TaxtypeCode;

	public String getStj3TaxtypeCode() {
		return stj3TaxtypeCode;
	}

	public void setStj3TaxtypeCode(String stj3TaxtypeCode) {
		this.stj3TaxtypeCode = stj3TaxtypeCode;
	}

	private String stj4TaxtypeCode;

	public String getStj4TaxtypeCode() {
		return stj4TaxtypeCode;
	}

	public void setStj4TaxtypeCode(String stj4TaxtypeCode) {
		this.stj4TaxtypeCode = stj4TaxtypeCode;
	}

	private String stj5TaxtypeCode;

	public String getStj5TaxtypeCode() {
		return stj5TaxtypeCode;
	}

	public void setStj5TaxtypeCode(String stj5TaxtypeCode) {
		this.stj5TaxtypeCode = stj5TaxtypeCode;
	}

	private String stj6TaxtypeCode;

	public String getStj6TaxtypeCode() {
		return stj6TaxtypeCode;
	}

	public void setStj6TaxtypeCode(String stj6TaxtypeCode) {
		this.stj6TaxtypeCode = stj6TaxtypeCode;
	}

	private String stj7TaxtypeCode;

	public String getStj7TaxtypeCode() {
		return stj7TaxtypeCode;
	}

	public void setStj7TaxtypeCode(String stj7TaxtypeCode) {
		this.stj7TaxtypeCode = stj7TaxtypeCode;
	}

	private String stj8TaxtypeCode;

	public String getStj8TaxtypeCode() {
		return stj8TaxtypeCode;
	}

	public void setStj8TaxtypeCode(String stj8TaxtypeCode) {
		this.stj8TaxtypeCode = stj8TaxtypeCode;
	}

	private String stj9TaxtypeCode;

	public String getStj9TaxtypeCode() {
		return stj9TaxtypeCode;
	}

	public void setStj9TaxtypeCode(String stj9TaxtypeCode) {
		this.stj9TaxtypeCode = stj9TaxtypeCode;
	}

	private String stj10TaxtypeCode;

	public String getStj10TaxtypeCode() {
		return stj10TaxtypeCode;
	}

	public void setStj10TaxtypeCode(String stj10TaxtypeCode) {
		this.stj10TaxtypeCode = stj10TaxtypeCode;
	}

	private Long countryTaxrateId;

	public Long getCountryTaxrateId() {
		return countryTaxrateId;
	}

	public void setCountryTaxrateId(Long countryTaxrateId) {
		this.countryTaxrateId = countryTaxrateId;
	}

	private Long stateTaxrateId;

	public Long getStateTaxrateId() {
		return stateTaxrateId;
	}

	public void setStateTaxrateId(Long stateTaxrateId) {
		this.stateTaxrateId = stateTaxrateId;
	}

	private Long countyTaxrateId;

	public Long getCountyTaxrateId() {
		return countyTaxrateId;
	}

	public void setCountyTaxrateId(Long countyTaxrateId) {
		this.countyTaxrateId = countyTaxrateId;
	}

	private Long cityTaxrateId;

	public Long getCityTaxrateId() {
		return cityTaxrateId;
	}

	public void setCityTaxrateId(Long cityTaxrateId) {
		this.cityTaxrateId = cityTaxrateId;
	}

	private Long stj1TaxrateId;

	public Long getStj1TaxrateId() {
		return stj1TaxrateId;
	}

	public void setStj1TaxrateId(Long stj1TaxrateId) {
		this.stj1TaxrateId = stj1TaxrateId;
	}

	private Long stj2TaxrateId;

	public Long getStj2TaxrateId() {
		return stj2TaxrateId;
	}

	public void setStj2TaxrateId(Long stj2TaxrateId) {
		this.stj2TaxrateId = stj2TaxrateId;
	}

	private Long stj3TaxrateId;

	public Long getStj3TaxrateId() {
		return stj3TaxrateId;
	}

	public void setStj3TaxrateId(Long stj3TaxrateId) {
		this.stj3TaxrateId = stj3TaxrateId;
	}

	private Long stj4TaxrateId;

	public Long getStj4TaxrateId() {
		return stj4TaxrateId;
	}

	public void setStj4TaxrateId(Long stj4TaxrateId) {
		this.stj4TaxrateId = stj4TaxrateId;
	}

	private Long stj5TaxrateId;

	public Long getStj5TaxrateId() {
		return stj5TaxrateId;
	}

	public void setStj5TaxrateId(Long stj5TaxrateId) {
		this.stj5TaxrateId = stj5TaxrateId;
	}

	private Long stj6TaxrateId;

	public Long getStj6TaxrateId() {
		return stj6TaxrateId;
	}

	public void setStj6TaxrateId(Long stj6TaxrateId) {
		this.stj6TaxrateId = stj6TaxrateId;
	}

	private Long stj7TaxrateId;

	public Long getStj7TaxrateId() {
		return stj7TaxrateId;
	}

	public void setStj7TaxrateId(Long stj7TaxrateId) {
		this.stj7TaxrateId = stj7TaxrateId;
	}

	private Long stj8TaxrateId;

	public Long getStj8TaxrateId() {
		return stj8TaxrateId;
	}

	public void setStj8TaxrateId(Long stj8TaxrateId) {
		this.stj8TaxrateId = stj8TaxrateId;
	}

	private Long stj9TaxrateId;

	public Long getStj9TaxrateId() {
		return stj9TaxrateId;
	}

	public void setStj9TaxrateId(Long stj9TaxrateId) {
		this.stj9TaxrateId = stj9TaxrateId;
	}

	private Long stj10TaxrateId;

	public Long getStj10TaxrateId() {
		return stj10TaxrateId;
	}

	public void setStj10TaxrateId(Long stj10TaxrateId) {
		this.stj10TaxrateId = stj10TaxrateId;
	}

	private String countryTaxcodeOverFlag;

	public String getCountryTaxcodeOverFlag() {
		return countryTaxcodeOverFlag;
	}

	public void setCountryTaxcodeOverFlag(String countryTaxcodeOverFlag) {
		this.countryTaxcodeOverFlag = countryTaxcodeOverFlag;
	}

	private String stateTaxcodeOverFlag;

	public String getStateTaxcodeOverFlag() {
		return stateTaxcodeOverFlag;
	}

	public void setStateTaxcodeOverFlag(String stateTaxcodeOverFlag) {
		this.stateTaxcodeOverFlag = stateTaxcodeOverFlag;
	}

	private String countyTaxcodeOverFlag;

	public String getCountyTaxcodeOverFlag() {
		return countyTaxcodeOverFlag;
	}

	public void setCountyTaxcodeOverFlag(String countyTaxcodeOverFlag) {
		this.countyTaxcodeOverFlag = countyTaxcodeOverFlag;
	}

	private String cityTaxcodeOverFlag;

	public String getCityTaxcodeOverFlag() {
		return cityTaxcodeOverFlag;
	}

	public void setCityTaxcodeOverFlag(String cityTaxcodeOverFlag) {
		this.cityTaxcodeOverFlag = cityTaxcodeOverFlag;
	}

	private String stj1TaxcodeOverFlag;

	public String getStj1TaxcodeOverFlag() {
		return stj1TaxcodeOverFlag;
	}

	public void setStj1TaxcodeOverFlag(String stj1TaxcodeOverFlag) {
		this.stj1TaxcodeOverFlag = stj1TaxcodeOverFlag;
	}

	private String stj2TaxcodeOverFlag;

	public String getStj2TaxcodeOverFlag() {
		return stj2TaxcodeOverFlag;
	}

	public void setStj2TaxcodeOverFlag(String stj2TaxcodeOverFlag) {
		this.stj2TaxcodeOverFlag = stj2TaxcodeOverFlag;
	}

	private String stj3TaxcodeOverFlag;

	public String getStj3TaxcodeOverFlag() {
		return stj3TaxcodeOverFlag;
	}

	public void setStj3TaxcodeOverFlag(String stj3TaxcodeOverFlag) {
		this.stj3TaxcodeOverFlag = stj3TaxcodeOverFlag;
	}

	private String stj4TaxcodeOverFlag;

	public String getStj4TaxcodeOverFlag() {
		return stj4TaxcodeOverFlag;
	}

	public void setStj4TaxcodeOverFlag(String stj4TaxcodeOverFlag) {
		this.stj4TaxcodeOverFlag = stj4TaxcodeOverFlag;
	}

	private String stj5TaxcodeOverFlag;

	public String getStj5TaxcodeOverFlag() {
		return stj5TaxcodeOverFlag;
	}

	public void setStj5TaxcodeOverFlag(String stj5TaxcodeOverFlag) {
		this.stj5TaxcodeOverFlag = stj5TaxcodeOverFlag;
	}

	private String stj6TaxcodeOverFlag;

	public String getStj6TaxcodeOverFlag() {
		return stj6TaxcodeOverFlag;
	}

	public void setStj6TaxcodeOverFlag(String stj6TaxcodeOverFlag) {
		this.stj6TaxcodeOverFlag = stj6TaxcodeOverFlag;
	}

	private String stj7TaxcodeOverFlag;

	public String getStj7TaxcodeOverFlag() {
		return stj7TaxcodeOverFlag;
	}

	public void setStj7TaxcodeOverFlag(String stj7TaxcodeOverFlag) {
		this.stj7TaxcodeOverFlag = stj7TaxcodeOverFlag;
	}

	private String stj8TaxcodeOverFlag;

	public String getStj8TaxcodeOverFlag() {
		return stj8TaxcodeOverFlag;
	}

	public void setStj8TaxcodeOverFlag(String stj8TaxcodeOverFlag) {
		this.stj8TaxcodeOverFlag = stj8TaxcodeOverFlag;
	}

	private String stj9TaxcodeOverFlag;

	public String getStj9TaxcodeOverFlag() {
		return stj9TaxcodeOverFlag;
	}

	public void setStj9TaxcodeOverFlag(String stj9TaxcodeOverFlag) {
		this.stj9TaxcodeOverFlag = stj9TaxcodeOverFlag;
	}

	private String stj10TaxcodeOverFlag;

	public String getStj10TaxcodeOverFlag() {
		return stj10TaxcodeOverFlag;
	}

	public void setStj10TaxcodeOverFlag(String stj10TaxcodeOverFlag) {
		this.stj10TaxcodeOverFlag = stj10TaxcodeOverFlag;
	}

	/* TB_PURCHTRANS_RATE fields */
	/*
	 * ---------------------------- FOR MICHAEL TO WORK ON
	 * ----------------------------------
	 */

	private BigDecimal countryTier1Rate;

	public BigDecimal getCountryTier1Rate() {
		return countryTier1Rate;
	}

	public void setCountryTier1Rate(BigDecimal countryTier1Rate) {
		this.countryTier1Rate = countryTier1Rate;
	}

	private BigDecimal countryTier2Rate;

	public BigDecimal getCountryTier2Rate() {
		return countryTier2Rate;
	}

	public void setCountryTier2Rate(BigDecimal countryTier2Rate) {
		this.countryTier2Rate = countryTier2Rate;
	}

	private BigDecimal countryTier3Rate;

	public BigDecimal getCountryTier3Rate() {
		return countryTier3Rate;
	}

	public void setCountryTier3Rate(BigDecimal countryTier3Rate) {
		this.countryTier3Rate = countryTier3Rate;
	}

	private BigDecimal countryTier1Setamt;

	public BigDecimal getCountryTier1Setamt() {
		return countryTier1Setamt;
	}

	public void setCountryTier1Setamt(BigDecimal countryTier1Setamt) {
		this.countryTier1Setamt = countryTier1Setamt;
	}

	private BigDecimal countryTier2Setamt;

	public BigDecimal getCountryTier2Setamt() {
		return countryTier2Setamt;
	}

	public void setCountryTier2Setamt(BigDecimal countryTier2Setamt) {
		this.countryTier2Setamt = countryTier2Setamt;
	}

	private BigDecimal countryTier3Setamt;

	public BigDecimal getCountryTier3Setamt() {
		return countryTier3Setamt;
	}

	public void setCountryTier3Setamt(BigDecimal countryTier3Setamt) {
		this.countryTier3Setamt = countryTier3Setamt;
	}

	private BigDecimal countryTier1MaxAmt;

	public BigDecimal getCountryTier1MaxAmt() {
		return countryTier1MaxAmt;
	}

	public void setCountryTier1MaxAmt(BigDecimal countryTier1MaxAmt) {
		this.countryTier1MaxAmt = countryTier1MaxAmt;
	}

	private BigDecimal countryTier2MinAmt;

	public BigDecimal getCountryTier2MinAmt() {
		return countryTier2MinAmt;
	}

	public void setCountryTier2MinAmt(BigDecimal countryTier2MinAmt) {
		this.countryTier2MinAmt = countryTier2MinAmt;
	}

	private BigDecimal countryTier2MaxAmt;

	public BigDecimal getCountryTier2MaxAmt() {
		return countryTier2MaxAmt;
	}

	public void setCountryTier2MaxAmt(BigDecimal countryTier2MaxAmt) {
		this.countryTier2MaxAmt = countryTier2MaxAmt;
	}

	private String countryTier2EntamFlag;

	public String getCountryTier2EntamFlag() {
		return countryTier2EntamFlag;
	}

	public void setCountryTier2EntamFlag(String countryTier2EntamFlag) {
		this.countryTier2EntamFlag = countryTier2EntamFlag;
	}

	private String countryTier3EntamFlag;

	public String getCountryTier3EntamFlag() {
		return countryTier3EntamFlag;
	}

	public void setCountryTier3EntamFlag(String countryTier3EntamFlag) {
		this.countryTier3EntamFlag = countryTier3EntamFlag;
	}

	private BigDecimal countryMaxtaxAmt;

	public BigDecimal getCountryMaxtaxAmt() {
		return countryMaxtaxAmt;
	}

	public void setCountryMaxtaxAmt(BigDecimal countryMaxtaxAmt) {
		this.countryMaxtaxAmt = countryMaxtaxAmt;
	}

	private BigDecimal countryTaxableThresholdAmt;

	public BigDecimal getCountryTaxableThresholdAmt() {
		return countryTaxableThresholdAmt;
	}

	public void setCountryTaxableThresholdAmt(
			BigDecimal countryTaxableThresholdAmt) {
		this.countryTaxableThresholdAmt = countryTaxableThresholdAmt;
	}

	private BigDecimal countryMinimumTaxableAmt;

	public BigDecimal getCountryMinimumTaxableAmt() {
		return countryMinimumTaxableAmt;
	}

	public void setCountryMinimumTaxableAmt(BigDecimal countryMinimumTaxableAmt) {
		this.countryMinimumTaxableAmt = countryMinimumTaxableAmt;
	}

	private BigDecimal countryMaximumTaxableAmt;

	public BigDecimal getCountryMaximumTaxableAmt() {
		return countryMaximumTaxableAmt;
	}

	public void setCountryMaximumTaxableAmt(BigDecimal countryMaximumTaxableAmt) {
		this.countryMaximumTaxableAmt = countryMaximumTaxableAmt;
	}

	private BigDecimal countryMaximumTaxAmt;

	public BigDecimal getCountryMaximumTaxAmt() {
		return countryMaximumTaxAmt;
	}

	public void setCountryMaximumTaxAmt(BigDecimal countryMaximumTaxAmt) {
		this.countryMaximumTaxAmt = countryMaximumTaxAmt;
	}

	private BigDecimal countryBaseChangePct;

	public BigDecimal getCountryBaseChangePct() {
		return countryBaseChangePct;
	}

	public void setCountryBaseChangePct(BigDecimal countryBaseChangePct) {
		this.countryBaseChangePct = countryBaseChangePct;
	}

	private String countryTaxProcessTypeCode;

	public String getCountryTaxProcessTypeCode() {
		return countryTaxProcessTypeCode;
	}

	public void setCountryTaxProcessTypeCode(String countryTaxProcessTypeCode) {
		this.countryTaxProcessTypeCode = countryTaxProcessTypeCode;
	}

	private BigDecimal stateTier1Rate;

	public BigDecimal getStateTier1Rate() {
		return stateTier1Rate;
	}

	public void setStateTier1Rate(BigDecimal stateTier1Rate) {
		this.stateTier1Rate = stateTier1Rate;
	}

	private BigDecimal stateTier2Rate;

	public BigDecimal getStateTier2Rate() {
		return stateTier2Rate;
	}

	public void setStateTier2Rate(BigDecimal stateTier2Rate) {
		this.stateTier2Rate = stateTier2Rate;
	}

	private BigDecimal stateTier3Rate;

	public BigDecimal getStateTier3Rate() {
		return stateTier3Rate;
	}

	public void setStateTier3Rate(BigDecimal stateTier3Rate) {
		this.stateTier3Rate = stateTier3Rate;
	}

	private BigDecimal stateTier1Setamt;

	public BigDecimal getStateTier1Setamt() {
		return stateTier1Setamt;
	}

	public void setStateTier1Setamt(BigDecimal stateTier1Setamt) {
		this.stateTier1Setamt = stateTier1Setamt;
	}

	private BigDecimal stateTier2Setamt;

	public BigDecimal getStateTier2Setamt() {
		return stateTier2Setamt;
	}

	public void setStateTier2Setamt(BigDecimal stateTier2Setamt) {
		this.stateTier2Setamt = stateTier2Setamt;
	}

	private BigDecimal stateTier3Setamt;

	public BigDecimal getStateTier3Setamt() {
		return stateTier3Setamt;
	}

	public void setStateTier3Setamt(BigDecimal stateTier3Setamt) {
		this.stateTier3Setamt = stateTier3Setamt;
	}

	private BigDecimal stateTier1MaxAmt;

	public BigDecimal getStateTier1MaxAmt() {
		return stateTier1MaxAmt;
	}

	public void setStateTier1MaxAmt(BigDecimal stateTier1MaxAmt) {
		this.stateTier1MaxAmt = stateTier1MaxAmt;
	}

	private BigDecimal stateTier2MinAmt;

	public BigDecimal getStateTier2MinAmt() {
		return stateTier2MinAmt;
	}

	public void setStateTier2MinAmt(BigDecimal stateTier2MinAmt) {
		this.stateTier2MinAmt = stateTier2MinAmt;
	}

	private BigDecimal stateTier2MaxAmt;

	public BigDecimal getStateTier2MaxAmt() {
		return stateTier2MaxAmt;
	}

	public void setStateTier2MaxAmt(BigDecimal stateTier2MaxAmt) {
		this.stateTier2MaxAmt = stateTier2MaxAmt;
	}

	private String stateTier2EntamFlag;

	public String getStateTier2EntamFlag() {
		return stateTier2EntamFlag;
	}

	public void setStateTier2EntamFlag(String stateTier2EntamFlag) {
		this.stateTier2EntamFlag = stateTier2EntamFlag;
	}

	private String stateTier3EntamFlag;

	public String getStateTier3EntamFlag() {
		return stateTier3EntamFlag;
	}

	public void setStateTier3EntamFlag(String stateTier3EntamFlag) {
		this.stateTier3EntamFlag = stateTier3EntamFlag;
	}

	private BigDecimal stateMaxtaxAmt;

	public BigDecimal getStateMaxtaxAmt() {
		return stateMaxtaxAmt;
	}

	public void setStateMaxtaxAmt(BigDecimal stateMaxtaxAmt) {
		this.stateMaxtaxAmt = stateMaxtaxAmt;
	}

	private BigDecimal stateTaxableThresholdAmt;

	public BigDecimal getStateTaxableThresholdAmt() {
		return stateTaxableThresholdAmt;
	}

	public void setStateTaxableThresholdAmt(BigDecimal stateTaxableThresholdAmt) {
		this.stateTaxableThresholdAmt = stateTaxableThresholdAmt;
	}

	private BigDecimal stateMinimumTaxableAmt;

	public BigDecimal getStateMinimumTaxableAmt() {
		return stateMinimumTaxableAmt;
	}

	public void setStateMinimumTaxableAmt(BigDecimal stateMinimumTaxableAmt) {
		this.stateMinimumTaxableAmt = stateMinimumTaxableAmt;
	}

	private BigDecimal stateMaximumTaxableAmt;

	public BigDecimal getStateMaximumTaxableAmt() {
		return stateMaximumTaxableAmt;
	}

	public void setStateMaximumTaxableAmt(BigDecimal stateMaximumTaxableAmt) {
		this.stateMaximumTaxableAmt = stateMaximumTaxableAmt;
	}

	private BigDecimal stateMaximumTaxAmt;

	public BigDecimal getStateMaximumTaxAmt() {
		return stateMaximumTaxAmt;
	}

	public void setStateMaximumTaxAmt(BigDecimal stateMaximumTaxAmt) {
		this.stateMaximumTaxAmt = stateMaximumTaxAmt;
	}

	private BigDecimal stateBaseChangePct;

	public BigDecimal getStateBaseChangePct() {
		return stateBaseChangePct;
	}

	public void setStateBaseChangePct(BigDecimal stateBaseChangePct) {
		this.stateBaseChangePct = stateBaseChangePct;
	}

	private String stateTaxProcessTypeCode;

	public String getStateTaxProcessTypeCode() {
		return stateTaxProcessTypeCode;
	}

	public void setStateTaxProcessTypeCode(String stateTaxProcessTypeCode) {
		this.stateTaxProcessTypeCode = stateTaxProcessTypeCode;
	}

	private BigDecimal countyTier1Rate;

	public BigDecimal getCountyTier1Rate() {
		return countyTier1Rate;
	}

	public void setCountyTier1Rate(BigDecimal countyTier1Rate) {
		this.countyTier1Rate = countyTier1Rate;
	}

	private BigDecimal countyTier2Rate;

	public BigDecimal getCountyTier2Rate() {
		return countyTier2Rate;
	}

	public void setCountyTier2Rate(BigDecimal countyTier2Rate) {
		this.countyTier2Rate = countyTier2Rate;
	}

	private BigDecimal countyTier3Rate;

	public BigDecimal getCountyTier3Rate() {
		return countyTier3Rate;
	}

	public void setCountyTier3Rate(BigDecimal countyTier3Rate) {
		this.countyTier3Rate = countyTier3Rate;
	}

	private BigDecimal countyTier1Setamt;

	public BigDecimal getCountyTier1Setamt() {
		return countyTier1Setamt;
	}

	public void setCountyTier1Setamt(BigDecimal countyTier1Setamt) {
		this.countyTier1Setamt = countyTier1Setamt;
	}

	private BigDecimal countyTier2Setamt;

	public BigDecimal getCountyTier2Setamt() {
		return countyTier2Setamt;
	}

	public void setCountyTier2Setamt(BigDecimal countyTier2Setamt) {
		this.countyTier2Setamt = countyTier2Setamt;
	}

	private BigDecimal countyTier3Setamt;

	public BigDecimal getCountyTier3Setamt() {
		return countyTier3Setamt;
	}

	public void setCountyTier3Setamt(BigDecimal countyTier3Setamt) {
		this.countyTier3Setamt = countyTier3Setamt;
	}

	private BigDecimal countyTier1MaxAmt;

	public BigDecimal getCountyTier1MaxAmt() {
		return countyTier1MaxAmt;
	}

	public void setCountyTier1MaxAmt(BigDecimal countyTier1MaxAmt) {
		this.countyTier1MaxAmt = countyTier1MaxAmt;
	}

	private BigDecimal countyTier2MinAmt;

	public BigDecimal getCountyTier2MinAmt() {
		return countyTier2MinAmt;
	}

	public void setCountyTier2MinAmt(BigDecimal countyTier2MinAmt) {
		this.countyTier2MinAmt = countyTier2MinAmt;
	}

	private BigDecimal countyTier2MaxAmt;

	public BigDecimal getCountyTier2MaxAmt() {
		return countyTier2MaxAmt;
	}

	public void setCountyTier2MaxAmt(BigDecimal countyTier2MaxAmt) {
		this.countyTier2MaxAmt = countyTier2MaxAmt;
	}

	private String countyTier2EntamFlag;

	public String getCountyTier2EntamFlag() {
		return countyTier2EntamFlag;
	}

	public void setCountyTier2EntamFlag(String countyTier2EntamFlag) {
		this.countyTier2EntamFlag = countyTier2EntamFlag;
	}

	private String countyTier3EntamFlag;

	public String getCountyTier3EntamFlag() {
		return countyTier3EntamFlag;
	}

	public void setCountyTier3EntamFlag(String countyTier3EntamFlag) {
		this.countyTier3EntamFlag = countyTier3EntamFlag;
	}

	private BigDecimal countyMaxtaxAmt;

	public BigDecimal getCountyMaxtaxAmt() {
		return countyMaxtaxAmt;
	}

	public void setCountyMaxtaxAmt(BigDecimal countyMaxtaxAmt) {
		this.countyMaxtaxAmt = countyMaxtaxAmt;
	}

	private BigDecimal countyTaxableThresholdAmt;

	public BigDecimal getCountyTaxableThresholdAmt() {
		return countyTaxableThresholdAmt;
	}

	public void setCountyTaxableThresholdAmt(
			BigDecimal countyTaxableThresholdAmt) {
		this.countyTaxableThresholdAmt = countyTaxableThresholdAmt;
	}

	private BigDecimal countyMinimumTaxableAmt;

	public BigDecimal getCountyMinimumTaxableAmt() {
		return countyMinimumTaxableAmt;
	}

	public void setCountyMinimumTaxableAmt(BigDecimal countyMinimumTaxableAmt) {
		this.countyMinimumTaxableAmt = countyMinimumTaxableAmt;
	}

	private BigDecimal countyMaximumTaxableAmt;

	public BigDecimal getCountyMaximumTaxableAmt() {
		return countyMaximumTaxableAmt;
	}

	public void setCountyMaximumTaxableAmt(BigDecimal countyMaximumTaxableAmt) {
		this.countyMaximumTaxableAmt = countyMaximumTaxableAmt;
	}

	private BigDecimal countyMaximumTaxAmt;

	public BigDecimal getCountyMaximumTaxAmt() {
		return countyMaximumTaxAmt;
	}

	public void setCountyMaximumTaxAmt(BigDecimal countyMaximumTaxAmt) {
		this.countyMaximumTaxAmt = countyMaximumTaxAmt;
	}

	private BigDecimal countyBaseChangePct;

	public BigDecimal getCountyBaseChangePct() {
		return countyBaseChangePct;
	}

	public void setCountyBaseChangePct(BigDecimal countyBaseChangePct) {
		this.countyBaseChangePct = countyBaseChangePct;
	}

	private String countyTaxProcessTypeCode;

	public String getCountyTaxProcessTypeCode() {
		return countyTaxProcessTypeCode;
	}

	public void setCountyTaxProcessTypeCode(String countyTaxProcessTypeCode) {
		this.countyTaxProcessTypeCode = countyTaxProcessTypeCode;
	}

	private BigDecimal cityTier1Rate;

	public BigDecimal getCityTier1Rate() {
		return cityTier1Rate;
	}

	public void setCityTier1Rate(BigDecimal cityTier1Rate) {
		this.cityTier1Rate = cityTier1Rate;
	}

	private BigDecimal cityTier2Rate;

	public BigDecimal getCityTier2Rate() {
		return cityTier2Rate;
	}

	public void setCityTier2Rate(BigDecimal cityTier2Rate) {
		this.cityTier2Rate = cityTier2Rate;
	}

	private BigDecimal cityTier3Rate;

	public BigDecimal getCityTier3Rate() {
		return cityTier3Rate;
	}

	public void setCityTier3Rate(BigDecimal cityTier3Rate) {
		this.cityTier3Rate = cityTier3Rate;
	}

	private BigDecimal cityTier1Setamt;

	public BigDecimal getCityTier1Setamt() {
		return cityTier1Setamt;
	}

	public void setCityTier1Setamt(BigDecimal cityTier1Setamt) {
		this.cityTier1Setamt = cityTier1Setamt;
	}

	private BigDecimal cityTier2Setamt;

	public BigDecimal getCityTier2Setamt() {
		return cityTier2Setamt;
	}

	public void setCityTier2Setamt(BigDecimal cityTier2Setamt) {
		this.cityTier2Setamt = cityTier2Setamt;
	}

	private BigDecimal cityTier3Setamt;

	public BigDecimal getCityTier3Setamt() {
		return cityTier3Setamt;
	}

	public void setCityTier3Setamt(BigDecimal cityTier3Setamt) {
		this.cityTier3Setamt = cityTier3Setamt;
	}

	private BigDecimal cityTier1MaxAmt;

	public BigDecimal getCityTier1MaxAmt() {
		return cityTier1MaxAmt;
	}

	public void setCityTier1MaxAmt(BigDecimal cityTier1MaxAmt) {
		this.cityTier1MaxAmt = cityTier1MaxAmt;
	}

	private BigDecimal cityTier2MinAmt;

	public BigDecimal getCityTier2MinAmt() {
		return cityTier2MinAmt;
	}

	public void setCityTier2MinAmt(BigDecimal cityTier2MinAmt) {
		this.cityTier2MinAmt = cityTier2MinAmt;
	}

	private BigDecimal cityTier2MaxAmt;

	public BigDecimal getCityTier2MaxAmt() {
		return cityTier2MaxAmt;
	}

	public void setCityTier2MaxAmt(BigDecimal cityTier2MaxAmt) {
		this.cityTier2MaxAmt = cityTier2MaxAmt;
	}

	private String cityTier2EntamFlag;

	public String getCityTier2EntamFlag() {
		return cityTier2EntamFlag;
	}

	public void setCityTier2EntamFlag(String cityTier2EntamFlag) {
		this.cityTier2EntamFlag = cityTier2EntamFlag;
	}

	private String cityTier3EntamFlag;

	public String getCityTier3EntamFlag() {
		return cityTier3EntamFlag;
	}

	public void setCityTier3EntamFlag(String cityTier3EntamFlag) {
		this.cityTier3EntamFlag = cityTier3EntamFlag;
	}

	private BigDecimal cityMaxtaxAmt;

	public BigDecimal getCityMaxtaxAmt() {
		return cityMaxtaxAmt;
	}

	public void setCityMaxtaxAmt(BigDecimal cityMaxtaxAmt) {
		this.cityMaxtaxAmt = cityMaxtaxAmt;
	}

	private BigDecimal cityTaxableThresholdAmt;

	public BigDecimal getCityTaxableThresholdAmt() {
		return cityTaxableThresholdAmt;
	}

	public void setCityTaxableThresholdAmt(BigDecimal cityTaxableThresholdAmt) {
		this.cityTaxableThresholdAmt = cityTaxableThresholdAmt;
	}

	private BigDecimal cityMinimumTaxableAmt;

	public BigDecimal getCityMinimumTaxableAmt() {
		return cityMinimumTaxableAmt;
	}

	public void setCityMinimumTaxableAmt(BigDecimal cityMinimumTaxableAmt) {
		this.cityMinimumTaxableAmt = cityMinimumTaxableAmt;
	}

	private BigDecimal cityMaximumTaxableAmt;

	public BigDecimal getCityMaximumTaxableAmt() {
		return cityMaximumTaxableAmt;
	}

	public void setCityMaximumTaxableAmt(BigDecimal cityMaximumTaxableAmt) {
		this.cityMaximumTaxableAmt = cityMaximumTaxableAmt;
	}

	private BigDecimal cityMaximumTaxAmt;

	public BigDecimal getCityMaximumTaxAmt() {
		return cityMaximumTaxAmt;
	}

	public void setCityMaximumTaxAmt(BigDecimal cityMaximumTaxAmt) {
		this.cityMaximumTaxAmt = cityMaximumTaxAmt;
	}

	private BigDecimal cityBaseChangePct;

	public BigDecimal getCityBaseChangePct() {
		return cityBaseChangePct;
	}

	public void setCityBaseChangePct(BigDecimal cityBaseChangePct) {
		this.cityBaseChangePct = cityBaseChangePct;
	}

	private String cityTaxProcessTypeCode;

	public String getCityTaxProcessTypeCode() {
		return cityTaxProcessTypeCode;
	}

	public void setCityTaxProcessTypeCode(String cityTaxProcessTypeCode) {
		this.cityTaxProcessTypeCode = cityTaxProcessTypeCode;
	}

	private BigDecimal stj1Rate;

	public BigDecimal getStj1Rate() {
		return stj1Rate;
	}

	public void setStj1Rate(BigDecimal stj1Rate) {
		this.stj1Rate = stj1Rate;
	}

	private BigDecimal stj1Setamt;

	public BigDecimal getStj1Setamt() {
		return stj1Setamt;
	}

	public void setStj1Setamt(BigDecimal stj1Setamt) {
		this.stj1Setamt = stj1Setamt;
	}

	private BigDecimal stj2Rate;

	public BigDecimal getStj2Rate() {
		return stj2Rate;
	}

	public void setStj2Rate(BigDecimal stj2Rate) {
		this.stj2Rate = stj2Rate;
	}

	private BigDecimal stj2Setamt;

	public BigDecimal getStj2Setamt() {
		return stj2Setamt;
	}

	public void setStj2Setamt(BigDecimal stj2Setamt) {
		this.stj2Setamt = stj2Setamt;
	}

	private BigDecimal stj3Rate;

	public BigDecimal getStj3Rate() {
		return stj3Rate;
	}

	public void setStj3Rate(BigDecimal stj3Rate) {
		this.stj3Rate = stj3Rate;
	}

	private BigDecimal stj3Setamt;

	public BigDecimal getStj3Setamt() {
		return stj3Setamt;
	}

	public void setStj3Setamt(BigDecimal stj3Setamt) {
		this.stj3Setamt = stj3Setamt;
	}

	private BigDecimal stj4Rate;

	public BigDecimal getStj4Rate() {
		return stj4Rate;
	}

	public void setStj4Rate(BigDecimal stj4Rate) {
		this.stj4Rate = stj4Rate;
	}

	private BigDecimal stj4Setamt;

	public BigDecimal getStj4Setamt() {
		return stj4Setamt;
	}

	public void setStj4Setamt(BigDecimal stj4Setamt) {
		this.stj4Setamt = stj4Setamt;
	}

	private BigDecimal stj5Rate;

	public BigDecimal getStj5Rate() {
		return stj5Rate;
	}

	public void setStj5Rate(BigDecimal stj5Rate) {
		this.stj5Rate = stj5Rate;
	}

	private BigDecimal stj5Setamt;

	public BigDecimal getStj5Setamt() {
		return stj5Setamt;
	}

	public void setStj5Setamt(BigDecimal stj5Setamt) {
		this.stj5Setamt = stj5Setamt;
	}

	private BigDecimal stj6Rate;

	public BigDecimal getStj6Rate() {
		return stj6Rate;
	}

	public void setStj6Rate(BigDecimal stj6Rate) {
		this.stj6Rate = stj6Rate;
	}

	private BigDecimal stj6Setamt;

	public BigDecimal getStj6Setamt() {
		return stj6Setamt;
	}

	public void setStj6Setamt(BigDecimal stj6Setamt) {
		this.stj6Setamt = stj6Setamt;
	}

	private BigDecimal stj7Rate;

	public BigDecimal getStj7Rate() {
		return stj7Rate;
	}

	public void setStj7Rate(BigDecimal stj7Rate) {
		this.stj7Rate = stj7Rate;
	}

	private BigDecimal stj7Setamt;

	public BigDecimal getStj7Setamt() {
		return stj7Setamt;
	}

	public void setStj7Setamt(BigDecimal stj7Setamt) {
		this.stj7Setamt = stj7Setamt;
	}

	private BigDecimal stj8Rate;

	public BigDecimal getStj8Rate() {
		return stj8Rate;
	}

	public void setStj8Rate(BigDecimal stj8Rate) {
		this.stj8Rate = stj8Rate;
	}

	private BigDecimal stj8Setamt;

	public BigDecimal getStj8Setamt() {
		return stj8Setamt;
	}

	public void setStj8Setamt(BigDecimal stj8Setamt) {
		this.stj8Setamt = stj8Setamt;
	}

	private BigDecimal stj9Rate;

	public BigDecimal getStj9Rate() {
		return stj9Rate;
	}

	public void setStj9Rate(BigDecimal stj9Rate) {
		this.stj9Rate = stj9Rate;
	}

	private BigDecimal stj9Setamt;

	public BigDecimal getStj9Setamt() {
		return stj9Setamt;
	}

	public void setStj9Setamt(BigDecimal stj9Setamt) {
		this.stj9Setamt = stj9Setamt;
	}

	private BigDecimal stj10Rate;

	public BigDecimal getStj10Rate() {
		return stj10Rate;
	}

	public void setStj10Rate(BigDecimal stj10Rate) {
		this.stj10Rate = stj10Rate;
	}

	private BigDecimal stj10Setamt;

	public BigDecimal getStj10Setamt() {
		return stj10Setamt;
	}

	public void setStj10Setamt(BigDecimal stj10Setamt) {
		this.stj10Setamt = stj10Setamt;
	}

	private BigDecimal combinedRate;

	public BigDecimal getCombinedRate() {
		return combinedRate;
	}

	public void setCombinedRate(BigDecimal combinedRate) {
		this.combinedRate = combinedRate;
	}
	
	private Boolean hasComments;

	public Boolean getHasComments() {
		return hasComments;
	}

	public void setHasComments(Boolean hasComments) {
		this.hasComments = hasComments;
	}
	
	
}
