package com.ncsts.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.ncsts.domain.Jurisdiction;

public class TaxCodeDetailDTO {
	private String taxcodeCode;
	private String taxcodeTypeCode;
	private String taxcodeTypeDesc;
	private String description;
	private String erpTaxcode;
	private String taxtypeCode;
	private String taxtypeDesc;
	private String comments;
	private Long  taxcodeDetailId;
	private String taxcodeStateCode;
	private String taxcodeCountryCode;
	private String activeFlag;
	private String customFlag;
	private String name;
	private Long sortNo;
    private String citationRef;
    private String exemptReason;
    private String goodSvcTypeCode;
    
    private BigDecimal specialSetAmt;
    private BigDecimal minimumTaxableAmt;
    private BigDecimal maximumTaxableAmt;
    private BigDecimal maximumTaxAmt;
    private String groupByDriver;
    private String taxProcessTypeCode;
    private String allocBucket;
    private String allocProrateByCode;
    private String allocConditionCode;
	
	//new columns from taxcode restructure
	private String taxCodeCounty;
	private String taxCodeCity;
	private String taxCodeStj;
	private String jurLevel;
	private String taxCodeCountryName;
	private String localTaxabilityCode;
	private boolean haveRulesFlag;
	private int ruleCount;
	private Date effectiveDate;
	private String rateTypeCode;
	private String rateTypeDesc;
	private BigDecimal taxableThresholdAmt;
	private BigDecimal taxLimitationAmt;
	private BigDecimal baseChangePct;
	private BigDecimal specialRate;
	private Date expirationDate;
	private String updateUserId;
	private Date updateTimestamp;
	private BigDecimal refdoccount;
	
	//for supporting checkbox selection
	private boolean selected;

	private TaxCodeStateDTO taxCodeStateDTO;
	
	private Jurisdiction jurisdiction;

	public String getTaxcodeCode() {
		return taxcodeCode;
	}

	public String getTaxcodeTypeCode() {
		return taxcodeTypeCode;
	}

	public String getTaxcodeTypeDesc() {
		return taxcodeTypeDesc;
	}

	public void setTaxcodeTypeDesc(String taxcodeTypeDesc) {
		this.taxcodeTypeDesc = taxcodeTypeDesc;
	}

	public String getDescription() {
		return description;
	}

	public String getErpTaxcode() {
		return erpTaxcode;
	}

	public String getTaxtypeCode() {
		return taxtypeCode;
	}

	public String getTaxtypeDesc() {
		return taxtypeDesc;
	}

	public String getName() {
		return name;
	}

	public void setTaxtypeDesc(String taxtypeDesc) {
		this.taxtypeDesc = taxtypeDesc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComments() {
		return comments;
	}

	public Long getTaxcodeDetailId() {
		return taxcodeDetailId;
	}

	public String getTaxcodeStateCode() {
		return taxcodeStateCode;
	}

	public String getTaxcodeCountryCode() {
		return taxcodeCountryCode;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public TaxCodeStateDTO getTaxCodeStateDTO() {
		return taxCodeStateDTO;
	}

	public void setTaxcodeCode(String taxcodeCode) {
		this.taxcodeCode = taxcodeCode;
	}

	public void setTaxcodeTypeCode(String taxcodeTypeCode) {
		this.taxcodeTypeCode = taxcodeTypeCode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setErpTaxcode(String erpTaxcode) {
		this.erpTaxcode = erpTaxcode;
	}

	public void setTaxtypeCode(String taxtypeCode) {
		this.taxtypeCode = taxtypeCode;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setTaxcodeDetailId(Long taxcodeDetailId) {
		this.taxcodeDetailId = taxcodeDetailId;
	}

	public void setTaxcodeStateCode(String taxcodeStateCode) {
		this.taxcodeStateCode = taxcodeStateCode;
	}

	public void setTaxcodeCountryCode(String taxcodeCountryCode) {
		this.taxcodeCountryCode = taxcodeCountryCode;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public void setTaxCodeStateDTO(TaxCodeStateDTO taxCodeStateDTO) {
		this.taxCodeStateDTO = taxCodeStateDTO;
	}

	public String getTaxCodeCounty() {
		return taxCodeCounty;
	}

	public void setTaxCodeCounty(String taxCodeCounty) {
		this.taxCodeCounty = taxCodeCounty;
	}

	public String getTaxCodeCity() {
		return taxCodeCity;
	}

	public void setTaxCodeCity(String taxCodeCity) {
		this.taxCodeCity = taxCodeCity;
	}
	
	public String getTaxCodeStj() {
		return taxCodeStj;
	}

	public void setTaxCodeStj(String taxCodeStj) {
		this.taxCodeStj = taxCodeStj;
	}

	public String getJurLevel() {
		return jurLevel;
	}

	public void setJurLevel(String jurLevel) {
		this.jurLevel = jurLevel;
	}

	public String getTaxCodeCountryName() {
		return taxCodeCountryName;
	}

	public void setTaxCodeCountryName(String taxCodeCountryName) {
		this.taxCodeCountryName = taxCodeCountryName;
	}

	public String getLocalTaxabilityCode() {
		return localTaxabilityCode;
	}

	public void setLocalTaxabilityCode(String localTaxabilityCode) {
		this.localTaxabilityCode = localTaxabilityCode;
	}

	public boolean isHaveRulesFlag() {
		return haveRulesFlag;
	}

	public void setHaveRulesFlag(boolean haveRulesFlag) {
		this.haveRulesFlag = haveRulesFlag;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getRateTypeDesc() {
		return rateTypeDesc;
	}

	public void setRateTypeDesc(String rateTypeDesc) {
		this.rateTypeDesc = rateTypeDesc;
	}

	public BigDecimal getTaxableThresholdAmt() {
		return taxableThresholdAmt;
	}

	public void setTaxableThresholdAmt(BigDecimal taxableThresholdAmt) {
		this.taxableThresholdAmt = taxableThresholdAmt;
	}

	public BigDecimal getTaxLimitationAmt() {
		return taxLimitationAmt;
	}

	public void setTaxLimitationAmt(BigDecimal taxLimitationAmt) {
		this.taxLimitationAmt = taxLimitationAmt;
	}

	public BigDecimal getBaseChangePct() {
		return baseChangePct;
	}

	public void setBaseChangePct(BigDecimal baseChangePct) {
		this.baseChangePct = baseChangePct;
	}

	public BigDecimal getSpecialRate() {
		return specialRate;
	}

	public void setSpecialRate(BigDecimal specialRate) {
		this.specialRate = specialRate;
	}
	
	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getRateTypeCode() {
		return rateTypeCode;
	}

	public void setRateTypeCode(String rateTypeCode) {
		this.rateTypeCode = rateTypeCode;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public Boolean getActiveBooleanFlag() {
		return "1".equals(activeFlag);
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}

	public int getRuleCount() {
		return ruleCount;
	}

	public void setRuleCount(int ruleCount) {
		this.ruleCount = ruleCount;
	}
	
	public Long getSortNo() {
        return this.sortNo;
    }
    
    public void setSortNo(Long sortNo) {
    	this.sortNo = sortNo;
    }

	public Jurisdiction getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Jurisdiction jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getCitationRef() {
		return citationRef;
	}

	public void setCitationRef(String citationRef) {
		this.citationRef = citationRef;
	}
	
	public String getExemptReason() {
		return exemptReason;
	}

	public void setExemptReason(String exemptReason) {
		this.exemptReason = exemptReason;
	}
	
	public String getGoodSvcTypeCode() {
		return goodSvcTypeCode;
	}

	public void setGoodSvcTypeCode(String goodSvcTypeCode) {
		this.goodSvcTypeCode = goodSvcTypeCode;
	}
	
	public BigDecimal getSpecialSetAmt() {
		return specialSetAmt;
	}

	public void setSpecialSetAmt(BigDecimal specialSetAmt) {
		this.specialSetAmt = specialSetAmt;
	}
	
	public BigDecimal getMinimumTaxableAmt() {
		return minimumTaxableAmt;
	}

	public void setMinimumTaxableAmt(BigDecimal minimumTaxableAmt) {
		this.minimumTaxableAmt = minimumTaxableAmt;
	}
	
	public BigDecimal getMaximumTaxableAmt() {
		return maximumTaxableAmt;
	}

	public void setMaximumTaxableAmt(BigDecimal maximumTaxableAmt) {
		this.maximumTaxableAmt = maximumTaxableAmt;
	}
	
	public BigDecimal getMaximumTaxAmt() {
		return maximumTaxAmt;
	}

	public void setMaximumTaxAmt(BigDecimal maximumTaxAmt) {
		this.maximumTaxAmt = maximumTaxAmt;
	}
	
	public String getGroupByDriver() {
		return groupByDriver;
	}

	public void setGroupByDriver(String groupByDriver) {
		this.groupByDriver = groupByDriver;
	}
	
	public String getTaxProcessTypeCode() {
		return taxProcessTypeCode;
	}

	public void setTaxProcessTypeCode(String taxProcessTypeCode) {
		this.taxProcessTypeCode = taxProcessTypeCode;
	}
	
	public String getAllocBucket() {
		return allocBucket;
	}

	public void setAllocBucket(String allocBucket) {
		this.allocBucket = allocBucket;
	}
	
	public String getAllocProrateByCode() {
		return allocProrateByCode;
	}

	public void setAllocProrateByCode(String allocProrateByCode) {
		this.allocProrateByCode = allocProrateByCode;
	}
	
	public String getAllocConditionCode() {
		return allocConditionCode;
	}

	public void setAllocConditionCode(String allocConditionCode) {
		this.allocConditionCode = allocConditionCode;
	}
	
	public BigDecimal getRefdoccount() {
		return refdoccount;
	}

	public void setRefdoccount(BigDecimal refdoccount) {
		this.refdoccount = refdoccount;
	}
	
	public Boolean getRefdoccountBooleanFlag() {
		return (refdoccount!=null && refdoccount.compareTo(new BigDecimal("0")) > 0);
	}


}
