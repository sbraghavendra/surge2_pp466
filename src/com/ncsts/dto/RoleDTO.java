package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Paul Govindan
 *
 */

public class RoleDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    private String roleCode;
      
    private String roleName;  
      
    private String globalBuFlag;  
      
    private String activeFlag;  
    
    private String updateUserId;
	
    private Date updateTimestamp;    
    
    public String getRoleCode() {
        return this.roleCode;
    }
    
    public void setRoleCode(String roleCode) {
    	this.roleCode = roleCode;
    }
    
    public String getRoleName() {
        return this.roleName;
    }
    
    public void setRoleName(String roleName) {
    	this.roleName = roleName;
    }    
    
    public String getGlobalBuFlag() {
        return this.globalBuFlag;
    }
    
    public void setGlobalBuFlag(String globalBuFlag) {
    	this.globalBuFlag = globalBuFlag;
    }      
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }    
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}    
    
}



