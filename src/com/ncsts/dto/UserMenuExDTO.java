package com.ncsts.dto;

import java.io.Serializable;


public class UserMenuExDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String userCode; 
	   
	private Long entityId;	
	
	private String menuCode; 
	
	private String exceptionType;
	
	private String optionName;

	public String getUserCode() {
		return userCode;
	}

	public Long getEntityId() {
		return entityId;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	
	

}
