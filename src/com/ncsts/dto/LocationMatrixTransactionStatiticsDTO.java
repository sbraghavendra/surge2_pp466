package com.ncsts.dto;

import com.ncsts.domain.TempLocationMatrixCount;
/*
 * Midtier project code modified - january 2009 */

public class LocationMatrixTransactionStatiticsDTO {
	
	private String total_location_matrix_lines;
	private String active_location_matrix_lines;
	private String distinct_location_matrix_lines;
	
	private TempLocationMatrixCount tempLocationMatrixCount;

	public String getTotal_location_matrix_lines() {
		return total_location_matrix_lines;
	}
	public void setTotal_location_matrix_lines(String total_location_matrix_lines) {
		this.total_location_matrix_lines = total_location_matrix_lines;
	}
	
	public String getActive_location_matrix_lines() {
		return active_location_matrix_lines;
	}
	public void setActive_location_matrix_lines(String active_location_matrix_lines) {
		this.active_location_matrix_lines = active_location_matrix_lines;
	}
	public String getDistinct_location_matrix_lines() {
		return distinct_location_matrix_lines;
	}
	public void setDistinct_location_matrix_lines(
			String distinct_location_matrix_lines) {
		this.distinct_location_matrix_lines = distinct_location_matrix_lines;
	}
	
	public TempLocationMatrixCount getTempLocationMatrixCount() {
		return tempLocationMatrixCount;
	}
	public void setTempLocationMatrixCount(
			TempLocationMatrixCount tempLocationMatrixCount) {
		this.tempLocationMatrixCount = tempLocationMatrixCount;
	}
	
}
