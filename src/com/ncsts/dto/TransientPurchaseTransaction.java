package com.ncsts.dto;

import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TransactionLog;
import com.ncsts.domain.BillTransactionLog;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.persistence.Column;
import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/*General storage for keeping purchase transaction processing variables*/

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TransientPurchaseTransaction {

    private String locationPreference;             // use "ratepoint" or "locnmatrix" first?
    private String locationMatrixSql;              // dynamically created SQL to find matching Location Matrix rows
    private String ratepointServer;                // the server name where RatePoint is installed
    private String ratepointUserId;                // the User ID for the RatePoint instance
    private String ratepointPassword;              // the Password for the RatePoint instance
    private String genericJurApi;                  // indicates if the genericJur API call will be used 0=off, H=high, L=low
    private String getRateForLatLonURL;            // the URL for the RatePoint getRateForLatLon API
    private String getStreetLevelRateURL;          // the URL for the RatePoint getStreetLevelRate API
    private String getJurGenericURL;               // the URL for the RatePoint getJurGeneric API
    private String goodSvcMatrixSql;               // dynamically create SQL to find matching Goods and Services Matrix rows
    private String autoProcId;                     // the TaxCode to use when AutoProcessing is turned on
    private String holdCodeFlag;                   // used in IF_THEN_ELSE tax_matrix calculation

    private String locationMatrixSQL;
    private String taxMatrixSQL;


    private String frtIncItem;
    private String discIncItem;

    private String countryLocalCode;
    private String stateLocalCode;
    private String countyLocalCode;
    private String cityLocalCode;
    private String stj1LocalCode;
    private String stj2LocalCode;
    private String stj3LocalCode;
    private String stj4LocalCode;
    private String stj5LocalCode;
    private String stj6LocalCode;
    private String stj7LocalCode;
    private String stj8LocalCode;
    private String stj9LocalCode;
    private String stj10LocalCode;

    private String countryNexusIndCode;
    private String stateNexusIndCode;
    private String countyNexusIndCode;
    private String cityNexusIndCode;
    private String stj1NexusIndCode;
    private String stj2NexusIndCode;
    private String stj3NexusIndCode;
    private String stj4NexusIndCode;
    private String stj5NexusIndCode;
    private String stj6NexusIndCode;
    private String stj7NexusIndCode;
    private String stj8NexusIndCode;
    private String stj9NexusIndCode;
    private String stj10NexusIndCode;


    private String countryPrimaryCode;
    private String statePrimaryCode;
    private String countyPrimaryName;
    private String cityPrimaryName;
    private String stj1PrimaryName;
    private String stj2PrimaryName;
    private String stj3PrimaryName;
    private String stj4PrimaryName;
    private String stj5PrimaryName;

    private String stj1PrimaryType;
    private String stj2PrimaryType;
    private String stj3PrimaryType;
    private String stj4PrimaryType;
    private String stj5PrimaryType;

    private String stj1SecondaryType;
    private String stj2SecondaryType;
    private String stj3SecondaryType;
    private String stj4SecondaryType;
    private String stj5SecondaryType;

    private String citySecondaryName;
    private String countySecondaryName;
    private String stj1SecondaryName;
    private String stj2SecondaryName;
    private String stj3SecondaryName;
    private String stj4SecondaryName;
    private String stj5SecondaryName;

    private String countryPrimaryNexusindCode;
    private String statePrimaryNexusindCode;
    private String countyPrimaryNexusindCode;
    private String cityPrimaryNexusindCode;
    private String stj1PrimaryNexusindCode;
    private String stj2PrimaryNexusindCode;
    private String stj3PrimaryNexusindCode;
    private String stj4PrimaryNexusindCode;
    private String stj5PrimaryNexusindCode;

    private String citySecondaryNexusindCode;
    private String countySecondaryNexusindCode;
    private String stj1SecondaryNexusindCode;
    private String stj2SecondaryNexusindCode;
    private String stj3SecondaryNexusindCode;
    private String stj4SecondaryNexusindCode;
    private String stj5SecondaryNexusindCode;

    private String lookupMaxCountryCode;
    private String lookupMaxStateCode;

    private Long countryPrimarySitusJurId;
    private Long statePrimarySitusJurId;
    private Long countyPrimarySitusJurId;
    private Long cityPrimarySitusJurId;
    private Long stj1PrimarySitusJurId;
    private Long stj2PrimarySitusJurId;
    private Long stj3PrimarySitusJurId;
    private Long stj4PrimarySitusJurId;
    private Long stj5PrimarySitusJurId;

    private Long citySecondarySitusJurId;
    private Long countySecondarySitusJurId;
    private Long stj1SecondarySitusJurId;
    private Long stj2SecondarySitusJurId;
    private Long stj3SecondarySitusJurId;
    private Long stj4SecondarySitusJurId;
    private Long stj5SecondarySitusJurId;


    private BigDecimal invoiceGoodCnt;
    private BigDecimal invoiceServiceCnt;
    private BigDecimal invoiceGoodWgt;
    private BigDecimal invoiceServiceWgt;

    private String processingLog = "";

//    private String uuid = UUID.randomUUID().toString();

    @XmlElement(nillable = false)
    private String  situsDebugMatrix;


    private BigDecimal autoProcAmt;                // the amt for comparison when AutoProcessing is turned on

    // fields for holding taxcode detail information
    private BigDecimal countryTcdtlSpecialRate;
    private BigDecimal countryTcdtlSpecialSetAmt;
    private BigDecimal countryTcdtlTaxableThresholdAmt;
    private BigDecimal countryTcdtlMinimumTaxableAmt;
    private BigDecimal countryTcdtlMaximumTaxableAmt;
    private BigDecimal countryTcdtlMaximumTaxAmt;
    private BigDecimal countryTaxAmt;

    private BigDecimal stateTcdtlSpecialRate;
    private BigDecimal stateTcdtlSpecialSetAmt;
    private BigDecimal stateTcdtlTaxableThresholdAmt;
    private BigDecimal stateTcdtlMinimumTaxableAmt;
    private BigDecimal stateTcdtlMaximumTaxableAmt;
    private BigDecimal stateTcdtlMaximumTaxAmt;
    private BigDecimal stateTaxAmt;

    private BigDecimal countyTcdtlSpecialRate;
    private BigDecimal countyTcdtlSpecialSetAmt;
    private BigDecimal countyTcdtlTaxableThresholdAmt;
    private BigDecimal countyTcdtlMinimumTaxableAmt;
    private BigDecimal countyTcdtlMaximumTaxableAmt;
    private BigDecimal countyTcdtlMaximumTaxAmt;
    private BigDecimal countyTaxAmt;

    private BigDecimal cityTcdtlSpecialRate;
    private BigDecimal cityTcdtlSpecialSetAmt;
    private BigDecimal cityTcdtlTaxableThresholdAmt;
    private BigDecimal cityTcdtlMinimumTaxableAmt;
    private BigDecimal cityTcdtlMaximumTaxableAmt;
    private BigDecimal cityTcdtlMaximumTaxAmt;
    private BigDecimal cityTaxAmt;

    private BigDecimal stj1TcdtlSpecialRate;
    private BigDecimal stj1TcdtlSpecialSetAmt;
    private BigDecimal stj1TcdtlTaxableThresholdAmt;
    private BigDecimal stj1TcdtlMinimumTaxableAmt;
    private BigDecimal stj1TcdtlMaximumTaxableAmt;
    private BigDecimal stj1TcdtlMaximumTaxAmt;

    private BigDecimal stj2TcdtlSpecialRate;
    private BigDecimal stj2TcdtlSpecialSetAmt;
    private BigDecimal stj2TcdtlTaxableThresholdAmt;
    private BigDecimal stj2TcdtlMinimumTaxableAmt;
    private BigDecimal stj2TcdtlMaximumTaxableAmt;
    private BigDecimal stj2TcdtlMaximumTaxAmt;

    private BigDecimal stj3TcdtlSpecialRate;
    private BigDecimal stj3TcdtlSpecialSetAmt;
    private BigDecimal stj3TcdtlTaxableThresholdAmt;
    private BigDecimal stj3TcdtlMinimumTaxableAmt;
    private BigDecimal stj3TcdtlMaximumTaxableAmt;
    private BigDecimal stj3TcdtlMaximumTaxAmt;

    private BigDecimal stj4TcdtlSpecialRate;
    private BigDecimal stj4TcdtlSpecialSetAmt;
    private BigDecimal stj4TcdtlTaxableThresholdAmt;
    private BigDecimal stj4TcdtlMinimumTaxableAmt;
    private BigDecimal stj4TcdtlMaximumTaxableAmt;
    private BigDecimal stj4TcdtlMaximumTaxAmt;

    private BigDecimal stj5TcdtlSpecialRate;
    private BigDecimal stj5TcdtlSpecialSetAmt;
    private BigDecimal stj5TcdtlTaxableThresholdAmt;
    private BigDecimal stj5TcdtlMinimumTaxableAmt;
    private BigDecimal stj5TcdtlMaximumTaxableAmt;
    private BigDecimal stj5TcdtlMaximumTaxAmt;

    private BigDecimal stj6TcdtlSpecialRate;
    private BigDecimal stj6TcdtlSpecialSetAmt;
    private BigDecimal stj6TcdtlTaxableThresholdAmt;
    private BigDecimal stj6TcdtlMinimumTaxableAmt;
    private BigDecimal stj6TcdtlMaximumTaxableAmt;
    private BigDecimal stj6TcdtlMaximumTaxAmt;

    private BigDecimal stj7TcdtlSpecialRate;
    private BigDecimal stj7TcdtlSpecialSetAmt;
    private BigDecimal stj7TcdtlTaxableThresholdAmt;
    private BigDecimal stj7TcdtlMinimumTaxableAmt;
    private BigDecimal stj7TcdtlMaximumTaxableAmt;
    private BigDecimal stj7TcdtlMaximumTaxAmt;

    private BigDecimal stj8TcdtlSpecialRate;
    private BigDecimal stj8TcdtlSpecialSetAmt;
    private BigDecimal stj8TcdtlTaxableThresholdAmt;
    private BigDecimal stj8TcdtlMinimumTaxableAmt;
    private BigDecimal stj8TcdtlMaximumTaxableAmt;
    private BigDecimal stj8TcdtlMaximumTaxAmt;

    private BigDecimal stj9TcdtlSpecialRate;
    private BigDecimal stj9TcdtlSpecialSetAmt;
    private BigDecimal stj9TcdtlTaxableThresholdAmt;
    private BigDecimal stj9TcdtlMinimumTaxableAmt;
    private BigDecimal stj9TcdtlMaximumTaxableAmt;
    private BigDecimal stj9TcdtlMaximumTaxAmt;

    private BigDecimal stj10TcdtlSpecialRate;
    private BigDecimal stj10TcdtlSpecialSetAmt;
    private BigDecimal stj10TcdtlTaxableThresholdAmt;
    private BigDecimal stj10TcdtlMinimumTaxableAmt;
    private BigDecimal stj10TcdtlMaximumTaxableAmt;
    private BigDecimal stj10TcdtlMaximumTaxAmt;
    private BigDecimal subTotalGrossAmt;

    private BigDecimal maxLocalRate;
    private BigDecimal combinedLocalRate;

    private BigDecimal invoiceGoodAmt;
    private BigDecimal invoiceServiceAmt;

    private BigDecimal countrySubTotalTaxAmt;
    private BigDecimal stateSubTotalTaxAmt;
    private BigDecimal countySubTotalTaxAmt;
    private BigDecimal citySubTotalTaxAmt;
    private BigDecimal stj1SubTotalTaxAmt;
    private BigDecimal stj2SubTotalTaxAmt;
    private BigDecimal stj3SubTotalTaxAmt;
    private BigDecimal stj4SubTotalTaxAmt;
    private BigDecimal stj5SubTotalTaxAmt;
    private BigDecimal stj6SubTotalTaxAmt;
    private BigDecimal stj7SubTotalTaxAmt;
    private BigDecimal stj8SubTotalTaxAmt;
    private BigDecimal stj9SubTotalTaxAmt;
    private BigDecimal stj10SubTotalTaxAmt;


    private Boolean latLongLookup;                 // indicates if RatePoint getRateForLatLon API is to be used
    private Boolean ratepointEnabled;              // indicates if any RatePoint APIs will be used
    private Boolean autoProcFlag;                  // indicates if AutoProcessing is to be used

    private Boolean countryCalcFlag;                   // indicates if Country taxes are to be calculated
    private Boolean stateCalcFlag;                     // indicates if State taxes are to be calculated
    private Boolean countyCalcFlag;                    // indicates if County taxes are to be calculated
    private Boolean cityCalcFlag;                      // indicates if City taxes are to be calculated
    private Boolean stj1CalcFlag;                      // indicates if STJ1 taxes are to be calculated
    private Boolean stj2CalcFlag;                      // indicates if STJ2 taxes are to be calculated
    private Boolean stj3CalcFlag;                      // indicates if STJ3 taxes are to be calculated
    private Boolean stj4CalcFlag;                      // indicates if STJ4 taxes are to be calculated
    private Boolean stj5CalcFlag;                      // indicates if STJ5 taxes are to be calculated
    private Boolean stj6CalcFlag;                      // indicates if STJ6 taxes are to be calculated
    private Boolean stj7CalcFlag;                      // indicates if STJ7 taxes are to be calculated
    private Boolean stj8CalcFlag;                      // indicates if STJ8 taxes are to be calculated
    private Boolean stj9CalcFlag;                      // indicates if STJ9 taxes are to be calculated
    private Boolean stj10CalcFlag;                     // indicates if STJ10 taxes are to be calculated

    private Boolean countryRateSetFlag;
    private Boolean stateRateSetFlag;
    private Boolean countyRateSetFlag;
    private Boolean cityRateSetFlag;
    private Boolean stj1RateSetFlag;
    private Boolean stj2RateSetFlag;
    private Boolean stj3RateSetFlag;
    private Boolean stj4RateSetFlag;
    private Boolean stj5RateSetFlag;
    private Boolean stj6RateSetFlag;
    private Boolean stj7RateSetFlag;
    private Boolean stj8RateSetFlag;
    private Boolean stj9RateSetFlag;
    private Boolean stj10RateSetFlag;

    private Boolean refreshCacheFlag;
    private Boolean distributedFlag;
    private Boolean documentFlag;
    private Boolean markedForGroupby = Boolean.FALSE;
    private Boolean subtotalGrossDistributed;
    private Boolean delete0Amount;
    private Boolean procRuleEnabled;
    private Boolean suspendFlag;
    private Boolean suspendCodeFlag;
    private Boolean preProcessFlag;
    private Boolean postProcessFlag;

    private Boolean primarySpdFlag = Boolean.FALSE;
    private Boolean primaryMtaFlag = Boolean.FALSE;

    @Transient
    @XmlTransient
    private Boolean localWrite = Boolean.TRUE;

    @Transient
    @XmlTransient
    private Boolean stopProcessing = Boolean.FALSE;

    @Transient
    @XmlTransient
    private Boolean localCountyAmtSet = Boolean.FALSE;

    @Transient
    @XmlTransient
    private Boolean localCityAmtSet = Boolean.FALSE;

    private Date stj1EffectiveDate;
    private Date stj2EffectiveDate;
    private Date stj3EffectiveDate;
    private Date stj4EffectiveDate;
    private Date stj5EffectiveDate;
    private Date stj6EffectiveDate;
    private Date stj7EffectiveDate;
    private Date stj8EffectiveDate;
    private Date stj9EffectiveDate;
    private Date stj10EffectiveDate;
    
    private String shiptoCountryNexusindCode;
    private String shiptoStateNexusindCode;
    private String shiptoCountyNexusindCode;
    private String shiptoCityNexusindCode;
    private String shiptoStj1NexusindCode;
    private String shiptoStj2NexusindCode;
    private String shiptoStj3NexusindCode;
    private String shiptoStj4NexusindCode;
    private String shiptoStj5NexusindCode;
    private String shiptoStj1LocalCode;
    private String shiptoStj2LocalCode;
    private String shiptoStj3LocalCode;
    private String shiptoStj4LocalCode;
    private String shiptoStj5LocalCode;
    private String billtoCountryNexusindCode;
    private String billtoStateNexusindCode;
    private String billtoCountyNexusindCode;
    private String billtoCityNexusindCode;
    private String billtoStj1NexusindCode;
    private String billtoStj2NexusindCode;
    private String billtoStj3NexusindCode;
    private String billtoStj4NexusindCode;
    private String billtoStj5NexusindCode;
    private String billtoStj1LocalCode;
    private String billtoStj2LocalCode;
    private String billtoStj3LocalCode;
    private String billtoStj4LocalCode;
    private String billtoStj5LocalCode;
    private String ttlxfrCountryNexusindCode;
    private String ttlxfrStateNexusindCode;
    private String ttlxfrCountyNexusindCode;
    private String ttlxfrCityNexusindCode;
    private String ttlxfrStj1NexusindCode;
    private String ttlxfrStj2NexusindCode;
    private String ttlxfrStj3NexusindCode;
    private String ttlxfrStj4NexusindCode;
    private String ttlxfrStj5NexusindCode;
    private String ttlxfrStj1LocalCode;
    private String ttlxfrStj2LocalCode;
    private String ttlxfrStj3LocalCode;
    private String ttlxfrStj4LocalCode;
    private String ttlxfrStj5LocalCode;
    private String firstuseCountryNexusindCode;
    private String firstuseStateNexusindCode;
    private String firstuseCountyNexusindCode;
    private String firstuseCityNexusindCode;
    private String firstuseStj1NexusindCode;
    private String firstuseStj2NexusindCode;
    private String firstuseStj3NexusindCode;
    private String firstuseStj4NexusindCode;
    private String firstuseStj5NexusindCode;
    private String firstuseStj1LocalCode;
    private String firstuseStj2LocalCode;
    private String firstuseStj3LocalCode;
    private String firstuseStj4LocalCode;
    private String firstuseStj5LocalCode;
    private String shipfromCountryNexusindCode;
    private String shipfromStateNexusindCode;
    private String shipfromCountyNexusindCode;
    private String shipfromCityNexusindCode;
    private String shipfromStj1NexusindCode;
    private String shipfromStj2NexusindCode;
    private String shipfromStj3NexusindCode;
    private String shipfromStj4NexusindCode;
    private String shipfromStj5NexusindCode;
    private String shipfromStj1LocalCode;
    private String shipfromStj2LocalCode;
    private String shipfromStj3LocalCode;
    private String shipfromStj4LocalCode;
    private String shipfromStj5LocalCode;
    private String ordrorgnCountryNexusindCode;
    private String ordrorgnStateNexusindCode;
    private String ordrorgnCountyNexusindCode;
    private String ordrorgnCityNexusindCode;
    private String ordrorgnStj1NexusindCode;
    private String ordrorgnStj2NexusindCode;
    private String ordrorgnStj3NexusindCode;
    private String ordrorgnStj4NexusindCode;
    private String ordrorgnStj5NexusindCode;
    private String ordrorgnStj1LocalCode;
    private String ordrorgnStj2LocalCode;
    private String ordrorgnStj3LocalCode;
    private String ordrorgnStj4LocalCode;
    private String ordrorgnStj5LocalCode;
    private String ordracptCountryNexusindCode;
    private String ordracptStateNexusindCode;
    private String ordracptCountyNexusindCode;
    private String ordracptCityNexusindCode;
    private String ordracptStj1NexusindCode;
    private String ordracptStj2NexusindCode;
    private String ordracptStj3NexusindCode;
    private String ordracptStj4NexusindCode;
    private String ordracptStj5NexusindCode;
    private String ordracptStj1LocalCode;
    private String ordracptStj2LocalCode;
    private String ordracptStj3LocalCode;
    private String ordracptStj4LocalCode;
    private String ordracptStj5LocalCode;
    
   

    private String stj1PrimaryLocalCode;
    private String stj2PrimaryLocalCode;
    private String stj3PrimaryLocalCode;
    private String stj4PrimaryLocalCode;
    private String stj5PrimaryLocalCode;
    private String stj6PrimaryLocalCode;
    private String stj7PrimaryLocalCode;
    private String stj8PrimaryLocalCode;
    private String stj9PrimaryLocalCode;
    private String stj10PrimaryLocalCode;

    private String stj1SecondaryLocalCode;
    private String stj2SecondaryLocalCode;
    private String stj3SecondaryLocalCode;
    private String stj4SecondaryLocalCode;
    private String stj5SecondaryLocalCode;
    private String stj6SecondaryLocalCode;
    private String stj7SecondaryLocalCode;
    private String stj8SecondaryLocalCode;
    private String stj9SecondaryLocalCode;
    private String stj10SecondaryLocalCode;

    @Transient
    @XmlTransient
    private TransactionLog transactionLog;

    @XmlElement(nillable = false)
    private String[] situsStjFinalSitusCode;
    @XmlElement(nillable = false)
    private String[] situsStjFinalTxtypCd;
    @XmlElement(nillable = false)
    private Long[] situsStjFinalJurId;
    @XmlElement(nillable = false)
    private BigDecimal[] situsStjFinalRate;
    @XmlElement(nillable = false)
    private String[] situsStjFinalName;
    @XmlElement(nillable = false)
    private String[] situsStjFinalNexusindCode;
    @XmlElement(nillable = false)
    private Long[] situsStjFinalTaxrateId;

    List<SitusInfoDTO> situsInfoList;


    private Map<OptionCodePK, String> purchasingOptions;
    public static Map<String, String> transProperties;

    private HashMap<String, String> entityDefaults;

    private TaxCodeRuleGroupBy taxCodeRuleGroupByKey;





    static {
        if (transProperties==null) {
        	transProperties = new HashMap<String, String>();
            Class<?> cls = PurchaseTransaction.class;
            Field[] fields = cls.getDeclaredFields();
            for (Field field: fields) {
                Column col = field.getAnnotation(Column.class);
                if (col !=null && col.name()!=null)
                    transProperties.put(col.name(), field.getName());
            }
        }
    }


    public static String KEY_DISINCITEM = "DISINCITEM";
    public static String KEY_DEFFRTTAX = "DEFFRTTAX";
    public static String KEY_FRTINCITEM = "FRTINCITEM";
    public static String KEY_RATETYPE = "RATETYPE";
    public static String KEY_TRANSTYPE = "TRANSTYPE";
    public static String KEY_FREIGHTTC  = "FREIGHTTC";
    public static String KEY_MOD = "MOD";
    public static String KEY_GEORECON = "GEORECON";
    public static String KEY_LOCNPREF = "LOCNPREF";


    public TransientPurchaseTransaction() {


        //defaulting entityDefaults
        entityDefaults = new HashMap<String, String>();


/*
        entityDefaults.put(KEY_DISINCITEM, "0");
        entityDefaults.put(KEY_FRTINCITEM, "0");
        entityDefaults.put(KEY_RATETYPE, "0");
        entityDefaults.put(KEY_TRANSTYPE, "SALE");
        entityDefaults.put(KEY_MOD, "1");
        entityDefaults.put(KEY_GEORECON, "1");
*/

    }


    public String getLocationPreference() {
        return locationPreference;
    }

    public void setLocationPreference(String locationPreference) {
        this.locationPreference = locationPreference;
    }

    public String getLocationMatrixSql() {
        return locationMatrixSql;
    }

    public void setLocationMatrixSql(String locationMatrixSql) {
        this.locationMatrixSql = locationMatrixSql;
    }

    public Boolean getRatepointEnabled() {
        return ratepointEnabled;
    }

    public void setRatepointEnabled(Boolean ratepointEnabled) {
        this.ratepointEnabled = ratepointEnabled;
    }

    public String getRatepointServer() {
        return ratepointServer;
    }

    public void setRatepointServer(String ratepointServer) {
        this.ratepointServer = ratepointServer;
    }

    public String getRatepointUserId() {
        return ratepointUserId;
    }

    public void setRatepointUserId(String ratepointUserId) {
        this.ratepointUserId = ratepointUserId;
    }

    public String getRatepointPassword() {
        return ratepointPassword;
    }

    public void setRatepointPassword(String ratepointPassword) {
        this.ratepointPassword = ratepointPassword;
    }

    public Boolean getLatLongLookup() {
        return latLongLookup;
    }

    public void setLatLongLookup(Boolean latLongLookup) {
        this.latLongLookup = latLongLookup;
    }

    public String getGenericJurApi() {
        return genericJurApi;
    }

    public void setGenericJurApi(String genericJurApi) {
        this.genericJurApi = genericJurApi;
    }

    public String getGetRateForLatLonURL() {
        return getRateForLatLonURL;
    }

    public void setGetRateForLatLonURL(String getRateForLatLonURL) {
        this.getRateForLatLonURL = getRateForLatLonURL;
    }

    public String getGetStreetLevelRateURL() {
        return getStreetLevelRateURL;
    }

    public void setGetStreetLevelRateURL(String getStreetLevelRateURL) {
        this.getStreetLevelRateURL = getStreetLevelRateURL;
    }

    public String getGetJurGenericURL() {
        return getJurGenericURL;
    }

    public void setGetJurGenericURL(String getJurGenericURL) {
        this.getJurGenericURL = getJurGenericURL;
    }

    public String getGoodSvcMatrixSql() {
        return goodSvcMatrixSql;
    }

    public void setGoodSvcMatrixSql(String goodSvcMatrixSql) {
        this.goodSvcMatrixSql = goodSvcMatrixSql;
    }

    public String getAutoProcId() {
        return autoProcId;
    }

    public void setAutoProcId(String autoProcId) {
        this.autoProcId = autoProcId;
    }

    public BigDecimal getAutoProcAmt() {
        return autoProcAmt;
    }

    public void setAutoProcAmt(BigDecimal autoProcAmt) {
        this.autoProcAmt = autoProcAmt;
    }

    public Boolean getAutoProcFlag() {
        return autoProcFlag;
    }

    public void setAutoProcFlag(Boolean autoProcFlag) {
        this.autoProcFlag = autoProcFlag;
    }

    public Boolean getCountryCalcFlag() {
        return countryCalcFlag;
    }

    public void setCountryCalcFlag(Boolean countryCalcFlag) {
        this.countryCalcFlag = countryCalcFlag;
    }

    public Boolean getStateCalcFlag() {
        return stateCalcFlag;
    }

    public void setStateCalcFlag(Boolean stateCalcFlag) {
        this.stateCalcFlag = stateCalcFlag;
    }

    public Boolean getCountyCalcFlag() {
        return countyCalcFlag;
    }

    public void setCountyCalcFlag(Boolean countyCalcFlag) {
        this.countyCalcFlag = countyCalcFlag;
    }

    public Boolean getCityCalcFlag() {
        return cityCalcFlag;
    }

    public void setCityCalcFlag(Boolean cityCalcFlag) {
        this.cityCalcFlag = cityCalcFlag;
    }

    public Boolean getStj1CalcFlag() {
        return stj1CalcFlag;
    }

    public void setStj1CalcFlag(Boolean stj1CalcFlag) {
        this.stj1CalcFlag = stj1CalcFlag;
    }

    public Boolean getStj2CalcFlag() {
        return stj2CalcFlag;
    }

    public void setStj2CalcFlag(Boolean stj2CalcFlag) {
        this.stj2CalcFlag = stj2CalcFlag;
    }

    public Boolean getStj3CalcFlag() {
        return stj3CalcFlag;
    }

    public void setStj3CalcFlag(Boolean stj3CalcFlag) {
        this.stj3CalcFlag = stj3CalcFlag;
    }

    public Boolean getStj4CalcFlag() {
        return stj4CalcFlag;
    }

    public void setStj4CalcFlag(Boolean stj4CalcFlag) {
        this.stj4CalcFlag = stj4CalcFlag;
    }

    public Boolean getStj5CalcFlag() {
        return stj5CalcFlag;
    }

    public void setStj5CalcFlag(Boolean stj5CalcFlag) {
        this.stj5CalcFlag = stj5CalcFlag;
    }

    public Boolean getStj6CalcFlag() {
        return stj6CalcFlag;
    }

    public void setStj6CalcFlag(Boolean stj6CalcFlag) {
        this.stj6CalcFlag = stj6CalcFlag;
    }

    public Boolean getStj7CalcFlag() {
        return stj7CalcFlag;
    }

    public void setStj7CalcFlag(Boolean stj7CalcFlag) {
        this.stj7CalcFlag = stj7CalcFlag;
    }

    public Boolean getStj8CalcFlag() {
        return stj8CalcFlag;
    }

    public void setStj8CalcFlag(Boolean stj8CalcFlag) {
        this.stj8CalcFlag = stj8CalcFlag;
    }

    public Boolean getStj9CalcFlag() {
        return stj9CalcFlag;
    }

    public void setStj9CalcFlag(Boolean stj9CalcFlag) {
        this.stj9CalcFlag = stj9CalcFlag;
    }

    public Boolean getStj10CalcFlag() {
        return stj10CalcFlag;
    }

    public void setStj10CalcFlag(Boolean stj10CalcFlag) {
        this.stj10CalcFlag = stj10CalcFlag;
    }



    public Map<OptionCodePK, String> getPurchasingOptions() {
        return purchasingOptions;
    }

    public void setPurchasingOptions(Map<OptionCodePK, String> purchasingOptions) {
        this.purchasingOptions = purchasingOptions;
    }

    public String getLocationMatrixSQL() {
        return locationMatrixSQL;
    }

    public void setLocationMatrixSQL(String locationMatrixSQL) {
        this.locationMatrixSQL = locationMatrixSQL;
    }


    public String getHoldCodeFlag() {
        return holdCodeFlag;
    }

    public void setHoldCodeFlag(String holdCodeFlag) {
        this.holdCodeFlag = holdCodeFlag;
    }
    
    public String getTaxMatrixSQL() {
        return taxMatrixSQL;
    }

    public void setTaxMatrixSQL(String taxMatrixSQL) {
        this.taxMatrixSQL = taxMatrixSQL;
    }

    public Boolean getRefreshCacheFlag() {
        return refreshCacheFlag;
    }

    public void setRefreshCacheFlag(Boolean refreshCacheFlag) {
        this.refreshCacheFlag = refreshCacheFlag;
    }


    public Boolean getDistributedFlag() {
        return distributedFlag;
    }

    public void setDistributedFlag(Boolean distributedFlag) {
        this.distributedFlag = distributedFlag;
    }

    public Boolean getDocumentFlag() {
        return documentFlag;
    }

    public void setDocumentFlag(Boolean documentFlag) {
        this.documentFlag = documentFlag;
    }

    public Boolean getCountryRateSetFlag() {
        return countryRateSetFlag;
    }

    public void setCountryRateSetFlag(Boolean countryRateSetFlag) {
        this.countryRateSetFlag = countryRateSetFlag;
    }


    public Boolean getStateRateSetFlag() {
        return stateRateSetFlag;
    }

    public void setStateRateSetFlag(Boolean stateRateSetFlag) {
        this.stateRateSetFlag = stateRateSetFlag;
    }

    public Boolean getCountyRateSetFlag() {
        return countyRateSetFlag;
    }

    public void setCountyRateSetFlag(Boolean countyRateSetFlag) {
        this.countyRateSetFlag = countyRateSetFlag;
    }

    public Boolean getCityRateSetFlag() {
        return cityRateSetFlag;
    }

    public void setCityRateSetFlag(Boolean cityRateSetFlag) {
        this.cityRateSetFlag = cityRateSetFlag;
    }

    public Boolean getStj1RateSetFlag() {
        return stj1RateSetFlag;
    }

    public void setStj1RateSetFlag(Boolean stj1RateSetFlag) {
        this.stj1RateSetFlag = stj1RateSetFlag;
    }

    public Boolean getStj2RateSetFlag() {
        return stj2RateSetFlag;
    }

    public void setStj2RateSetFlag(Boolean stj2RateSetFlag) {
        this.stj2RateSetFlag = stj2RateSetFlag;
    }

    public Boolean getStj3RateSetFlag() {
        return stj3RateSetFlag;
    }

    public void setStj3RateSetFlag(Boolean stj3RateSetFlag) {
        this.stj3RateSetFlag = stj3RateSetFlag;
    }

    public Boolean getStj4RateSetFlag() {
        return stj4RateSetFlag;
    }

    public void setStj4RateSetFlag(Boolean stj4RateSetFlag) {
        this.stj4RateSetFlag = stj4RateSetFlag;
    }

    public Boolean getStj5RateSetFlag() {
        return stj5RateSetFlag;
    }

    public void setStj5RateSetFlag(Boolean stj5RateSetFlag) {
        this.stj5RateSetFlag = stj5RateSetFlag;
    }

    public Boolean getStj6RateSetFlag() {
        return stj6RateSetFlag;
    }

    public void setStj6RateSetFlag(Boolean stj6RateSetFlag) {
        this.stj6RateSetFlag = stj6RateSetFlag;
    }

    public Boolean getStj7RateSetFlag() {
        return stj7RateSetFlag;
    }

    public void setStj7RateSetFlag(Boolean stj7RateSetFlag) {
        this.stj7RateSetFlag = stj7RateSetFlag;
    }

    public Boolean getStj8RateSetFlag() {
        return stj8RateSetFlag;
    }

    public void setStj8RateSetFlag(Boolean stj8RsteSetFlag) {
        this.stj8RateSetFlag = stj8RsteSetFlag;
    }

    public Boolean getStj9RateSetFlag() {
        return stj9RateSetFlag;
    }

    public void setStj9RateSetFlag(Boolean stj9RateSetFlag) {
        this.stj9RateSetFlag = stj9RateSetFlag;
    }

    public Boolean getStj10RateSetFlag() {
        return stj10RateSetFlag;
    }

    public void setStj10RateSetFlag(Boolean stj10RateSetFlag) {
        this.stj10RateSetFlag = stj10RateSetFlag;
    }


    public BigDecimal getCountryTcdtlSpecialRate() {
        return countryTcdtlSpecialRate;
    }

    public void setCountryTcdtlSpecialRate(BigDecimal countryTcdtlSpecialRate) {
        this.countryTcdtlSpecialRate = countryTcdtlSpecialRate;
    }

    public BigDecimal getCountryTcdtlSpecialSetAmt() {
        return countryTcdtlSpecialSetAmt;
    }

    public void setCountryTcdtlSpecialSetAmt(BigDecimal countryTcdtlSpecialSetAmt) {
        this.countryTcdtlSpecialSetAmt = countryTcdtlSpecialSetAmt;
    }

    public BigDecimal getCountryTcdtlTaxableThresholdAmt() {
        return countryTcdtlTaxableThresholdAmt;
    }

    public void setCountryTcdtlTaxableThresholdAmt(BigDecimal countryTcdtlTaxableThresholdAmt) {
        this.countryTcdtlTaxableThresholdAmt = countryTcdtlTaxableThresholdAmt;
    }

    public BigDecimal getCountryTcdtlMinimumTaxableAmt() {
        return countryTcdtlMinimumTaxableAmt;
    }

    public void setCountryTcdtlMinimumTaxableAmt(BigDecimal countryTcdtlMinimumTaxableAmt) {
        this.countryTcdtlMinimumTaxableAmt = countryTcdtlMinimumTaxableAmt;
    }

    public BigDecimal getCountryTcdtlMaximumTaxableAmt() {
        return countryTcdtlMaximumTaxableAmt;
    }

    public void setCountryTcdtlMaximumTaxableAmt(BigDecimal countryTcdtlMaximumTaxableAmt) {
        this.countryTcdtlMaximumTaxableAmt = countryTcdtlMaximumTaxableAmt;
    }

    public BigDecimal getCountryTcdtlMaximumTaxAmt() {
        return countryTcdtlMaximumTaxAmt;
    }

    public void setCountryTcdtlMaximumTaxAmt(BigDecimal countryTcdtlMaximumTaxAmt) {
        this.countryTcdtlMaximumTaxAmt = countryTcdtlMaximumTaxAmt;
    }
    
    public BigDecimal getCountryTaxAmt() {
        return countryTaxAmt;
    }

    public void setCountryTaxAmt(BigDecimal countryTaxAmt) {
        this.countryTaxAmt = countryTaxAmt;
    }

    public BigDecimal getStateTcdtlSpecialRate() {
        return stateTcdtlSpecialRate;
    }

    public void setStateTcdtlSpecialRate(BigDecimal stateTcdtlSpecialRate) {
        this.stateTcdtlSpecialRate = stateTcdtlSpecialRate;
    }

    public BigDecimal getStateTcdtlSpecialSetAmt() {
        return stateTcdtlSpecialSetAmt;
    }

    public void setStateTcdtlSpecialSetAmt(BigDecimal stateTcdtlSpecialSetAmt) {
        this.stateTcdtlSpecialSetAmt = stateTcdtlSpecialSetAmt;
    }

    public BigDecimal getStateTcdtlTaxableThresholdAmt() {
        return stateTcdtlTaxableThresholdAmt;
    }

    public void setStateTcdtlTaxableThresholdAmt(BigDecimal stateTcdtlTaxableThresholdAmt) {
        this.stateTcdtlTaxableThresholdAmt = stateTcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStateTcdtlMinimumTaxableAmt() {
        return stateTcdtlMinimumTaxableAmt;
    }

    public void setStateTcdtlMinimumTaxableAmt(BigDecimal stateTcdtlMinimumTaxableAmt) {
        this.stateTcdtlMinimumTaxableAmt = stateTcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStateTcdtlMaximumTaxableAmt() {
        return stateTcdtlMaximumTaxableAmt;
    }

    public void setStateTcdtlMaximumTaxableAmt(BigDecimal stateTcdtlMaximumTaxableAmt) {
        this.stateTcdtlMaximumTaxableAmt = stateTcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStateTcdtlMaximumTaxAmt() {
        return stateTcdtlMaximumTaxAmt;
    }

    public void setStateTcdtlMaximumTaxAmt(BigDecimal stateTcdtlMaximumTaxAmt) {
        this.stateTcdtlMaximumTaxAmt = stateTcdtlMaximumTaxAmt;
    }

    public BigDecimal getStateTaxAmt() {
        return stateTaxAmt;
    }

    public void setStateTaxAmt(BigDecimal stateTaxAmt) {
        this.stateTaxAmt = stateTaxAmt;
    }

    public BigDecimal getCountyTcdtlSpecialRate() {
        return countyTcdtlSpecialRate;
    }

    public void setCountyTcdtlSpecialRate(BigDecimal countyTcdtlSpecialRate) {
        this.countyTcdtlSpecialRate = countyTcdtlSpecialRate;
    }

    public BigDecimal getCountyTcdtlSpecialSetAmt() {
        return countyTcdtlSpecialSetAmt;
    }

    public void setCountyTcdtlSpecialSetAmt(BigDecimal countyTcdtlSpecialSetAmt) {
        this.countyTcdtlSpecialSetAmt = countyTcdtlSpecialSetAmt;
    }

    public BigDecimal getCountyTcdtlTaxableThresholdAmt() {
        return countyTcdtlTaxableThresholdAmt;
    }

    public void setCountyTcdtlTaxableThresholdAmt(BigDecimal countyTcdtlTaxableThresholdAmt) {
        this.countyTcdtlTaxableThresholdAmt = countyTcdtlTaxableThresholdAmt;
    }

    public BigDecimal getCountyTcdtlMinimumTaxableAmt() {
        return countyTcdtlMinimumTaxableAmt;
    }

    public void setCountyTcdtlMinimumTaxableAmt(BigDecimal countyTcdtlMinimumTaxableAmt) {
        this.countyTcdtlMinimumTaxableAmt = countyTcdtlMinimumTaxableAmt;
    }

    public BigDecimal getCountyTcdtlMaximumTaxableAmt() {
        return countyTcdtlMaximumTaxableAmt;
    }

    public void setCountyTcdtlMaximumTaxableAmt(BigDecimal countyTcdtlMaximumTaxableAmt) {
        this.countyTcdtlMaximumTaxableAmt = countyTcdtlMaximumTaxableAmt;
    }

    public BigDecimal getCountyTcdtlMaximumTaxAmt() {
        return countyTcdtlMaximumTaxAmt;
    }

    public void setCountyTcdtlMaximumTaxAmt(BigDecimal countyTcdtlMaximumTaxAmt) {
        this.countyTcdtlMaximumTaxAmt = countyTcdtlMaximumTaxAmt;
    }

    public BigDecimal getCountyTaxAmt() {
        return countyTaxAmt;
    }

    public void setCountyTaxAmt(BigDecimal countyTaxAmt) {
        this.countyTaxAmt = countyTaxAmt;
    }

    public BigDecimal getCityTcdtlSpecialRate() {
        return cityTcdtlSpecialRate;
    }

    public void setCityTcdtlSpecialRate(BigDecimal cityTcdtlSpecialRate) {
        this.cityTcdtlSpecialRate = cityTcdtlSpecialRate;
    }

    public BigDecimal getCityTcdtlSpecialSetAmt() {
        return cityTcdtlSpecialSetAmt;
    }

    public void setCityTcdtlSpecialSetAmt(BigDecimal cityTcdtlSpecialSetAmt) {
        this.cityTcdtlSpecialSetAmt = cityTcdtlSpecialSetAmt;
    }

    public BigDecimal getCityTcdtlTaxableThresholdAmt() {
        return cityTcdtlTaxableThresholdAmt;
    }

    public void setCityTcdtlTaxableThresholdAmt(BigDecimal cityTcdtlTaxableThresholdAmt) {
        this.cityTcdtlTaxableThresholdAmt = cityTcdtlTaxableThresholdAmt;
    }

    public BigDecimal getCityTcdtlMinimumTaxableAmt() {
        return cityTcdtlMinimumTaxableAmt;
    }

    public void setCityTcdtlMinimumTaxableAmt(BigDecimal cityTcdtlMinimumTaxableAmt) {
        this.cityTcdtlMinimumTaxableAmt = cityTcdtlMinimumTaxableAmt;
    }

    public BigDecimal getCityTcdtlMaximumTaxableAmt() {
        return cityTcdtlMaximumTaxableAmt;
    }

    public void setCityTcdtlMaximumTaxableAmt(BigDecimal cityTcdtlMaximumTaxableAmt) {
        this.cityTcdtlMaximumTaxableAmt = cityTcdtlMaximumTaxableAmt;
    }

    public BigDecimal getCityTcdtlMaximumTaxAmt() {
        return cityTcdtlMaximumTaxAmt;
    }

    public void setCityTcdtlMaximumTaxAmt(BigDecimal cityTcdtlMaximumTaxAmt) {
        this.cityTcdtlMaximumTaxAmt = cityTcdtlMaximumTaxAmt;
    }
    
    public BigDecimal getCityTaxAmt() {
        return cityTaxAmt;
    }

    public void setCityTaxAmt(BigDecimal cityTaxAmt) {
        this.cityTaxAmt = cityTaxAmt;
    }

    public BigDecimal getStj1TcdtlSpecialRate() {
        return stj1TcdtlSpecialRate;
    }

    public void setStj1TcdtlSpecialRate(BigDecimal stj1TcdtlSpecialRate) {
        this.stj1TcdtlSpecialRate = stj1TcdtlSpecialRate;
    }

    public BigDecimal getStj1TcdtlSpecialSetAmt() {
        return stj1TcdtlSpecialSetAmt;
    }

    public void setStj1TcdtlSpecialSetAmt(BigDecimal stj1TcdtlSpecialSetAmt) {
        this.stj1TcdtlSpecialSetAmt = stj1TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj1TcdtlTaxableThresholdAmt() {
        return stj1TcdtlTaxableThresholdAmt;
    }

    public void setStj1TcdtlTaxableThresholdAmt(BigDecimal stj1TcdtlTaxableThresholdAmt) {
        this.stj1TcdtlTaxableThresholdAmt = stj1TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj1TcdtlMinimumTaxableAmt() {
        return stj1TcdtlMinimumTaxableAmt;
    }

    public void setStj1TcdtlMinimumTaxableAmt(BigDecimal stj1TcdtlMinimumTaxableAmt) {
        this.stj1TcdtlMinimumTaxableAmt = stj1TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj1TcdtlMaximumTaxableAmt() {
        return stj1TcdtlMaximumTaxableAmt;
    }

    public void setStj1TcdtlMaximumTaxableAmt(BigDecimal stj1TcdtlMaximumTaxableAmt) {
        this.stj1TcdtlMaximumTaxableAmt = stj1TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj1TcdtlMaximumTaxAmt() {
        return stj1TcdtlMaximumTaxAmt;
    }

    public void setStj1TcdtlMaximumTaxAmt(BigDecimal stj1TcdtlMaximumTaxAmt) {
        this.stj1TcdtlMaximumTaxAmt = stj1TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj2TcdtlSpecialRate() {
        return stj2TcdtlSpecialRate;
    }

    public void setStj2TcdtlSpecialRate(BigDecimal stj2TcdtlSpecialRate) {
        this.stj2TcdtlSpecialRate = stj2TcdtlSpecialRate;
    }

    public BigDecimal getStj2TcdtlSpecialSetAmt() {
        return stj2TcdtlSpecialSetAmt;
    }

    public void setStj2TcdtlSpecialSetAmt(BigDecimal stj2TcdtlSpecialSetAmt) {
        this.stj2TcdtlSpecialSetAmt = stj2TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj2TcdtlTaxableThresholdAmt() {
        return stj2TcdtlTaxableThresholdAmt;
    }

    public void setStj2TcdtlTaxableThresholdAmt(BigDecimal stj2TcdtlTaxableThresholdAmt) {
        this.stj2TcdtlTaxableThresholdAmt = stj2TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj2TcdtlMinimumTaxableAmt() {
        return stj2TcdtlMinimumTaxableAmt;
    }

    public void setStj2TcdtlMinimumTaxableAmt(BigDecimal stj2TcdtlMinimumTaxableAmt) {
        this.stj2TcdtlMinimumTaxableAmt = stj2TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj2TcdtlMaximumTaxableAmt() {
        return stj2TcdtlMaximumTaxableAmt;
    }

    public void setStj2TcdtlMaximumTaxableAmt(BigDecimal stj2TcdtlMaximumTaxableAmt) {
        this.stj2TcdtlMaximumTaxableAmt = stj2TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj2TcdtlMaximumTaxAmt() {
        return stj2TcdtlMaximumTaxAmt;
    }

    public void setStj2TcdtlMaximumTaxAmt(BigDecimal stj2TcdtlMaximumTaxAmt) {
        this.stj2TcdtlMaximumTaxAmt = stj2TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj3TcdtlSpecialRate() {
        return stj3TcdtlSpecialRate;
    }

    public void setStj3TcdtlSpecialRate(BigDecimal stj3TcdtlSpecialRate) {
        this.stj3TcdtlSpecialRate = stj3TcdtlSpecialRate;
    }

    public BigDecimal getStj3TcdtlSpecialSetAmt() {
        return stj3TcdtlSpecialSetAmt;
    }

    public void setStj3TcdtlSpecialSetAmt(BigDecimal stj3TcdtlSpecialSetAmt) {
        this.stj3TcdtlSpecialSetAmt = stj3TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj3TcdtlTaxableThresholdAmt() {
        return stj3TcdtlTaxableThresholdAmt;
    }

    public void setStj3TcdtlTaxableThresholdAmt(BigDecimal stj3TcdtlTaxableThresholdAmt) {
        this.stj3TcdtlTaxableThresholdAmt = stj3TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj3TcdtlMinimumTaxableAmt() {
        return stj3TcdtlMinimumTaxableAmt;
    }

    public void setStj3TcdtlMinimumTaxableAmt(BigDecimal stj3TcdtlMinimumTaxableAmt) {
        this.stj3TcdtlMinimumTaxableAmt = stj3TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj3TcdtlMaximumTaxableAmt() {
        return stj3TcdtlMaximumTaxableAmt;
    }

    public void setStj3TcdtlMaximumTaxableAmt(BigDecimal stj3TcdtlMaximumTaxableAmt) {
        this.stj3TcdtlMaximumTaxableAmt = stj3TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj3TcdtlMaximumTaxAmt() {
        return stj3TcdtlMaximumTaxAmt;
    }

    public void setStj3TcdtlMaximumTaxAmt(BigDecimal stj3TcdtlMaximumTaxAmt) {
        this.stj3TcdtlMaximumTaxAmt = stj3TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj4TcdtlSpecialRate() {
        return stj4TcdtlSpecialRate;
    }

    public void setStj4TcdtlSpecialRate(BigDecimal stj4TcdtlSpecialRate) {
        this.stj4TcdtlSpecialRate = stj4TcdtlSpecialRate;
    }

    public BigDecimal getStj4TcdtlSpecialSetAmt() {
        return stj4TcdtlSpecialSetAmt;
    }

    public void setStj4TcdtlSpecialSetAmt(BigDecimal stj4TcdtlSpecialSetAmt) {
        this.stj4TcdtlSpecialSetAmt = stj4TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj4TcdtlTaxableThresholdAmt() {
        return stj4TcdtlTaxableThresholdAmt;
    }

    public void setStj4TcdtlTaxableThresholdAmt(BigDecimal stj4TcdtlTaxableThresholdAmt) {
        this.stj4TcdtlTaxableThresholdAmt = stj4TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj4TcdtlMinimumTaxableAmt() {
        return stj4TcdtlMinimumTaxableAmt;
    }

    public void setStj4TcdtlMinimumTaxableAmt(BigDecimal stj4TcdtlMinimumTaxableAmt) {
        this.stj4TcdtlMinimumTaxableAmt = stj4TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj4TcdtlMaximumTaxableAmt() {
        return stj4TcdtlMaximumTaxableAmt;
    }

    public void setStj4TcdtlMaximumTaxableAmt(BigDecimal stj4TcdtlMaximumTaxableAmt) {
        this.stj4TcdtlMaximumTaxableAmt = stj4TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj4TcdtlMaximumTaxAmt() {
        return stj4TcdtlMaximumTaxAmt;
    }

    public void setStj4TcdtlMaximumTaxAmt(BigDecimal stj4TcdtlMaximumTaxAmt) {
        this.stj4TcdtlMaximumTaxAmt = stj4TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj5TcdtlSpecialRate() {
        return stj5TcdtlSpecialRate;
    }

    public void setStj5TcdtlSpecialRate(BigDecimal stj5TcdtlSpecialRate) {
        this.stj5TcdtlSpecialRate = stj5TcdtlSpecialRate;
    }

    public BigDecimal getStj5TcdtlSpecialSetAmt() {
        return stj5TcdtlSpecialSetAmt;
    }

    public void setStj5TcdtlSpecialSetAmt(BigDecimal stj5TcdtlSpecialSetAmt) {
        this.stj5TcdtlSpecialSetAmt = stj5TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj5TcdtlTaxableThresholdAmt() {
        return stj5TcdtlTaxableThresholdAmt;
    }

    public void setStj5TcdtlTaxableThresholdAmt(BigDecimal stj5TcdtlTaxableThresholdAmt) {
        this.stj5TcdtlTaxableThresholdAmt = stj5TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj5TcdtlMinimumTaxableAmt() {
        return stj5TcdtlMinimumTaxableAmt;
    }

    public void setStj5TcdtlMinimumTaxableAmt(BigDecimal stj5TcdtlMinimumTaxableAmt) {
        this.stj5TcdtlMinimumTaxableAmt = stj5TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj5TcdtlMaximumTaxableAmt() {
        return stj5TcdtlMaximumTaxableAmt;
    }

    public void setStj5TcdtlMaximumTaxableAmt(BigDecimal stj5TcdtlMaximumTaxableAmt) {
        this.stj5TcdtlMaximumTaxableAmt = stj5TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj5TcdtlMaximumTaxAmt() {
        return stj5TcdtlMaximumTaxAmt;
    }

    public void setStj5TcdtlMaximumTaxAmt(BigDecimal stj5TcdtlMaximumTaxAmt) {
        this.stj5TcdtlMaximumTaxAmt = stj5TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj6TcdtlSpecialRate() {
        return stj6TcdtlSpecialRate;
    }

    public void setStj6TcdtlSpecialRate(BigDecimal stj6TcdtlSpecialRate) {
        this.stj6TcdtlSpecialRate = stj6TcdtlSpecialRate;
    }

    public BigDecimal getStj6TcdtlSpecialSetAmt() {
        return stj6TcdtlSpecialSetAmt;
    }

    public void setStj6TcdtlSpecialSetAmt(BigDecimal stj6TcdtlSpecialSetAmt) {
        this.stj6TcdtlSpecialSetAmt = stj6TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj6TcdtlTaxableThresholdAmt() {
        return stj6TcdtlTaxableThresholdAmt;
    }

    public void setStj6TcdtlTaxableThresholdAmt(BigDecimal stj6TcdtlTaxableThresholdAmt) {
        this.stj6TcdtlTaxableThresholdAmt = stj6TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj6TcdtlMinimumTaxableAmt() {
        return stj6TcdtlMinimumTaxableAmt;
    }

    public void setStj6TcdtlMinimumTaxableAmt(BigDecimal stj6TcdtlMinimumTaxableAmt) {
        this.stj6TcdtlMinimumTaxableAmt = stj6TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj6TcdtlMaximumTaxableAmt() {
        return stj6TcdtlMaximumTaxableAmt;
    }

    public void setStj6TcdtlMaximumTaxableAmt(BigDecimal stj6TcdtlMaximumTaxableAmt) {
        this.stj6TcdtlMaximumTaxableAmt = stj6TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj6TcdtlMaximumTaxAmt() {
        return stj6TcdtlMaximumTaxAmt;
    }

    public void setStj6TcdtlMaximumTaxAmt(BigDecimal stj6TcdtlMaximumTaxAmt) {
        this.stj6TcdtlMaximumTaxAmt = stj6TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj7TcdtlSpecialRate() {
        return stj7TcdtlSpecialRate;
    }

    public void setStj7TcdtlSpecialRate(BigDecimal stj7TcdtlSpecialRate) {
        this.stj7TcdtlSpecialRate = stj7TcdtlSpecialRate;
    }

    public BigDecimal getStj7TcdtlSpecialSetAmt() {
        return stj7TcdtlSpecialSetAmt;
    }

    public void setStj7TcdtlSpecialSetAmt(BigDecimal stj7TcdtlSpecialSetAmt) {
        this.stj7TcdtlSpecialSetAmt = stj7TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj7TcdtlTaxableThresholdAmt() {
        return stj7TcdtlTaxableThresholdAmt;
    }

    public void setStj7TcdtlTaxableThresholdAmt(BigDecimal stj7TcdtlTaxableThresholdAmt) {
        this.stj7TcdtlTaxableThresholdAmt = stj7TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj7TcdtlMinimumTaxableAmt() {
        return stj7TcdtlMinimumTaxableAmt;
    }

    public void setStj7TcdtlMinimumTaxableAmt(BigDecimal stj7TcdtlMinimumTaxableAmt) {
        this.stj7TcdtlMinimumTaxableAmt = stj7TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj7TcdtlMaximumTaxableAmt() {
        return stj7TcdtlMaximumTaxableAmt;
    }

    public void setStj7TcdtlMaximumTaxableAmt(BigDecimal stj7TcdtlMaximumTaxableAmt) {
        this.stj7TcdtlMaximumTaxableAmt = stj7TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj7TcdtlMaximumTaxAmt() {
        return stj7TcdtlMaximumTaxAmt;
    }

    public void setStj7TcdtlMaximumTaxAmt(BigDecimal stj7TcdtlMaximumTaxAmt) {
        this.stj7TcdtlMaximumTaxAmt = stj7TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj8TcdtlSpecialRate() {
        return stj8TcdtlSpecialRate;
    }

    public void setStj8TcdtlSpecialRate(BigDecimal stj8TcdtlSpecialRate) {
        this.stj8TcdtlSpecialRate = stj8TcdtlSpecialRate;
    }

    public BigDecimal getStj8TcdtlSpecialSetAmt() {
        return stj8TcdtlSpecialSetAmt;
    }

    public void setStj8TcdtlSpecialSetAmt(BigDecimal stj8TcdtlSpecialSetAmt) {
        this.stj8TcdtlSpecialSetAmt = stj8TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj8TcdtlTaxableThresholdAmt() {
        return stj8TcdtlTaxableThresholdAmt;
    }

    public void setStj8TcdtlTaxableThresholdAmt(BigDecimal stj8TcdtlTaxableThresholdAmt) {
        this.stj8TcdtlTaxableThresholdAmt = stj8TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj8TcdtlMinimumTaxableAmt() {
        return stj8TcdtlMinimumTaxableAmt;
    }

    public void setStj8TcdtlMinimumTaxableAmt(BigDecimal stj8TcdtlMinimumTaxableAmt) {
        this.stj8TcdtlMinimumTaxableAmt = stj8TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj8TcdtlMaximumTaxableAmt() {
        return stj8TcdtlMaximumTaxableAmt;
    }

    public void setStj8TcdtlMaximumTaxableAmt(BigDecimal stj8TcdtlMaximumTaxableAmt) {
        this.stj8TcdtlMaximumTaxableAmt = stj8TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj8TcdtlMaximumTaxAmt() {
        return stj8TcdtlMaximumTaxAmt;
    }

    public void setStj8TcdtlMaximumTaxAmt(BigDecimal stj8TcdtlMaximumTaxAmt) {
        this.stj8TcdtlMaximumTaxAmt = stj8TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj9TcdtlSpecialRate() {
        return stj9TcdtlSpecialRate;
    }

    public void setStj9TcdtlSpecialRate(BigDecimal stj9TcdtlSpecialRate) {
        this.stj9TcdtlSpecialRate = stj9TcdtlSpecialRate;
    }

    public BigDecimal getStj9TcdtlSpecialSetAmt() {
        return stj9TcdtlSpecialSetAmt;
    }

    public void setStj9TcdtlSpecialSetAmt(BigDecimal stj9TcdtlSpecialSetAmt) {
        this.stj9TcdtlSpecialSetAmt = stj9TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj9TcdtlTaxableThresholdAmt() {
        return stj9TcdtlTaxableThresholdAmt;
    }

    public void setStj9TcdtlTaxableThresholdAmt(BigDecimal stj9TcdtlTaxableThresholdAmt) {
        this.stj9TcdtlTaxableThresholdAmt = stj9TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj9TcdtlMinimumTaxableAmt() {
        return stj9TcdtlMinimumTaxableAmt;
    }

    public void setStj9TcdtlMinimumTaxableAmt(BigDecimal stj9TcdtlMinimumTaxableAmt) {
        this.stj9TcdtlMinimumTaxableAmt = stj9TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj9TcdtlMaximumTaxableAmt() {
        return stj9TcdtlMaximumTaxableAmt;
    }

    public void setStj9TcdtlMaximumTaxableAmt(BigDecimal stj9TcdtlMaximumTaxableAmt) {
        this.stj9TcdtlMaximumTaxableAmt = stj9TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj9TcdtlMaximumTaxAmt() {
        return stj9TcdtlMaximumTaxAmt;
    }

    public void setStj9TcdtlMaximumTaxAmt(BigDecimal stj9TcdtlMaximumTaxAmt) {
        this.stj9TcdtlMaximumTaxAmt = stj9TcdtlMaximumTaxAmt;
    }

    public BigDecimal getStj10TcdtlSpecialRate() {
        return stj10TcdtlSpecialRate;
    }

    public void setStj10TcdtlSpecialRate(BigDecimal stj10TcdtlSpecialRate) {
        this.stj10TcdtlSpecialRate = stj10TcdtlSpecialRate;
    }

    public BigDecimal getStj10TcdtlSpecialSetAmt() {
        return stj10TcdtlSpecialSetAmt;
    }

    public void setStj10TcdtlSpecialSetAmt(BigDecimal stj10TcdtlSpecialSetAmt) {
        this.stj10TcdtlSpecialSetAmt = stj10TcdtlSpecialSetAmt;
    }

    public BigDecimal getStj10TcdtlTaxableThresholdAmt() {
        return stj10TcdtlTaxableThresholdAmt;
    }

    public void setStj10TcdtlTaxableThresholdAmt(BigDecimal stj10TcdtlTaxableThresholdAmt) {
        this.stj10TcdtlTaxableThresholdAmt = stj10TcdtlTaxableThresholdAmt;
    }

    public BigDecimal getStj10TcdtlMinimumTaxableAmt() {
        return stj10TcdtlMinimumTaxableAmt;
    }

    public void setStj10TcdtlMinimumTaxableAmt(BigDecimal stj10TcdtlMinimumTaxableAmt) {
        this.stj10TcdtlMinimumTaxableAmt = stj10TcdtlMinimumTaxableAmt;
    }

    public BigDecimal getStj10TcdtlMaximumTaxableAmt() {
        return stj10TcdtlMaximumTaxableAmt;
    }

    public void setStj10TcdtlMaximumTaxableAmt(BigDecimal stj10TcdtlMaximumTaxableAmt) {
        this.stj10TcdtlMaximumTaxableAmt = stj10TcdtlMaximumTaxableAmt;
    }

    public BigDecimal getStj10TcdtlMaximumTaxAmt() {
        return stj10TcdtlMaximumTaxAmt;
    }

    public void setStj10TcdtlMaximumTaxAmt(BigDecimal stj10TcdtlMaximumTaxAmt) {
        this.stj10TcdtlMaximumTaxAmt = stj10TcdtlMaximumTaxAmt;
    }

    public HashMap<String, String> getEntityDefaults() {
        return entityDefaults;
    }

    public void setEntityDefaults(HashMap<String, String> entityDefaults) {
        this.entityDefaults = entityDefaults;
    }


    public String getStj1LocalCode() {
        return stj1LocalCode;
    }

    public void setStj1LocalCode(String stj1LocalCode) {
        this.stj1LocalCode = stj1LocalCode;
    }

    public String getStj2LocalCode() {
        return stj2LocalCode;
    }

    public void setStj2LocalCode(String stj2LocalCode) {
        this.stj2LocalCode = stj2LocalCode;
    }

    public String getStj3LocalCode() {
        return stj3LocalCode;
    }

    public void setStj3LocalCode(String stj3LocalCode) {
        this.stj3LocalCode = stj3LocalCode;
    }

    public String getStj4LocalCode() {
        return stj4LocalCode;
    }

    public void setStj4LocalCode(String stj4LocalCode) {
        this.stj4LocalCode = stj4LocalCode;
    }

    public String getStj5LocalCode() {
        return stj5LocalCode;
    }

    public void setStj5LocalCode(String stj5LocalCode) {
        this.stj5LocalCode = stj5LocalCode;
    }

    public String getStj6LocalCode() {
        return stj6LocalCode;
    }

    public void setStj6LocalCode(String stj6LocalCode) {
        this.stj6LocalCode = stj6LocalCode;
    }

    public String getStj7LocalCode() {
        return stj7LocalCode;
    }

    public void setStj7LocalCode(String stj7LocalCode) {
        this.stj7LocalCode = stj7LocalCode;
    }

    public String getStj8LocalCode() {
        return stj8LocalCode;
    }

    public void setStj8LocalCode(String stj8LocalCode) {
        this.stj8LocalCode = stj8LocalCode;
    }

    public String getStj9LocalCode() {
        return stj9LocalCode;
    }

    public void setStj9LocalCode(String stj9LocalCode) {
        this.stj9LocalCode = stj9LocalCode;
    }

    public String getStj10LocalCode() {
        return stj10LocalCode;
    }

    public void setStj10LocalCode(String stj10LocalCode) {
        this.stj10LocalCode = stj10LocalCode;
    }

    public Boolean getMarkedForGroupby() {
        return markedForGroupby;
    }

    public void setMarkedForGroupby(Boolean markedForGroupby) {
        this.markedForGroupby = markedForGroupby;
    }

    public BigDecimal getSubTotalGrossAmt() {
        return subTotalGrossAmt;
    }

    public void setSubTotalGrossAmt(BigDecimal subTotalGrossAmt) {
        this.subTotalGrossAmt = subTotalGrossAmt;
    }

    public TaxCodeRuleGroupBy getTaxCodeRuleGroupByKey() {
        return taxCodeRuleGroupByKey;
    }

    public void setTaxCodeRuleGroupByKey(TaxCodeRuleGroupBy taxCodeRuleGroupByKey) {
        this.taxCodeRuleGroupByKey = taxCodeRuleGroupByKey;
    }

    public Boolean getSubtotalGrossDistributed() {
        return subtotalGrossDistributed;
    }

    public void setSubtotalGrossDistributed(Boolean subtotalGrossDistributed) {
        this.subtotalGrossDistributed = subtotalGrossDistributed;
    }

    public String getCountryLocalCode() {
        return countryLocalCode;
    }

    public void setCountryLocalCode(String countryLocalCode) {
        this.countryLocalCode = countryLocalCode;
    }

    public String getStateLocalCode() {
        return stateLocalCode;
    }

    public void setStateLocalCode(String stateLocalCode) {
        this.stateLocalCode = stateLocalCode;
    }

    public String getCountyLocalCode() {
        return countyLocalCode;
    }

    public void setCountyLocalCode(String countyLocalCode) {
        this.countyLocalCode = countyLocalCode;
    }

    public String getCityLocalCode() {
        return cityLocalCode;
    }

    public void setCityLocalCode(String cityLocalCode) {
        this.cityLocalCode = cityLocalCode;
    }

    public String getCountryNexusIndCode() {
        return countryNexusIndCode;
    }

    public void setCountryNexusIndCode(String countryNexusIndCode) {
        this.countryNexusIndCode = countryNexusIndCode;
    }

    public String getStateNexusIndCode() {
        return stateNexusIndCode;
    }

    public void setStateNexusIndCode(String stateNexusIndCode) {
        this.stateNexusIndCode = stateNexusIndCode;
    }

    public String getCountyNexusIndCode() {
        return countyNexusIndCode;
    }

    public void setCountyNexusIndCode(String countyNexusIndCode) {
        this.countyNexusIndCode = countyNexusIndCode;
    }

    public String getCityNexusIndCode() {
        return cityNexusIndCode;
    }

    public void setCityNexusIndCode(String cityNexusIndCode) {
        this.cityNexusIndCode = cityNexusIndCode;
    }

    public String getStj1NexusIndCode() {
        return stj1NexusIndCode;
    }

    public void setStj1NexusIndCode(String stj1NexusIndCode) {
        this.stj1NexusIndCode = stj1NexusIndCode;
    }

    public String getStj2NexusIndCode() {
        return stj2NexusIndCode;
    }

    public void setStj2NexusIndCode(String stj2NexusIndCode) {
        this.stj2NexusIndCode = stj2NexusIndCode;
    }

    public String getStj3NexusIndCode() {
        return stj3NexusIndCode;
    }

    public void setStj3NexusIndCode(String stj3NexusIndCode) {
        this.stj3NexusIndCode = stj3NexusIndCode;
    }

    public String getStj4NexusIndCode() {
        return stj4NexusIndCode;
    }

    public void setStj4NexusIndCode(String stj4NexusIndCode) {
        this.stj4NexusIndCode = stj4NexusIndCode;
    }

    public String getStj5NexusIndCode() {
        return stj5NexusIndCode;
    }

    public void setStj5NexusIndCode(String stj5NexusIndCode) {
        this.stj5NexusIndCode = stj5NexusIndCode;
    }

    public String getStj6NexusIndCode() {
        return stj6NexusIndCode;
    }

    public void setStj6NexusIndCode(String stj6NexusIndCode) {
        this.stj6NexusIndCode = stj6NexusIndCode;
    }

    public String getStj7NexusIndCode() {
        return stj7NexusIndCode;
    }

    public void setStj7NexusIndCode(String stj7NexusIndCode) {
        this.stj7NexusIndCode = stj7NexusIndCode;
    }

    public String getStj8NexusIndCode() {
        return stj8NexusIndCode;
    }

    public void setStj8NexusIndCode(String stj8NexusIndCode) {
        this.stj8NexusIndCode = stj8NexusIndCode;
    }

    public String getStj9NexusIndCode() {
        return stj9NexusIndCode;
    }

    public void setStj9NexusIndCode(String stj9NexusIndCode) {
        this.stj9NexusIndCode = stj9NexusIndCode;
    }

    public String getStj10NexusIndCode() {
        return stj10NexusIndCode;
    }

    public void setStj10NexusIndCode(String stj10NexusIndCode) {
        this.stj10NexusIndCode = stj10NexusIndCode;
    }

    public String getProcessingLog() {
        return processingLog;
    }

    public void setProcessingLog(String processingLog) {
        this.processingLog = processingLog;
    }


    public Boolean getDelete0Amount() {
        return delete0Amount;
    }

    public void setDelete0Amount(Boolean delete0Amount) {
        this.delete0Amount = delete0Amount;
    }

    public Boolean getProcRuleEnabled() {
        return procRuleEnabled;
    }

    public void setProcRuleEnabled(Boolean procRuleEnabled) {
        this.procRuleEnabled = procRuleEnabled;
    }

    public Boolean getSuspendFlag() {
        return suspendFlag;
    }

    public void setSuspendFlag(Boolean suspendFlag) {
        this.suspendFlag = suspendFlag;
    }

    public String getCountryPrimaryCode() {
        return countryPrimaryCode;
    }

    public void setCountryPrimaryCode(String countryPrimaryCode) {
        this.countryPrimaryCode = countryPrimaryCode;
    }

    public String getStatePrimaryCode() {
        return statePrimaryCode;
    }

    public void setStatePrimaryCode(String statePrimaryCode) {
        this.statePrimaryCode = statePrimaryCode;
    }

    public String getCountyPrimaryName() {
        return countyPrimaryName;
    }

    public void setCountyPrimaryName(String countyPrimaryName) {
        this.countyPrimaryName = countyPrimaryName;
    }

    public String getCityPrimaryName() {
        return cityPrimaryName;
    }

    public void setCityPrimaryName(String cityPrimaryName) {
        this.cityPrimaryName = cityPrimaryName;
    }

    public String getStj1PrimaryName() {
        return stj1PrimaryName;
    }

    public void setStj1PrimaryName(String stj1PrimaryName) {
        this.stj1PrimaryName = stj1PrimaryName;
    }

    public String getStj2PrimaryName() {
        return stj2PrimaryName;
    }

    public void setStj2PrimaryName(String stj2PrimaryName) {
        this.stj2PrimaryName = stj2PrimaryName;
    }

    public String getStj3PrimaryName() {
        return stj3PrimaryName;
    }

    public void setStj3PrimaryName(String stj3PrimaryName) {
        this.stj3PrimaryName = stj3PrimaryName;
    }

    public String getStj4PrimaryName() {
        return stj4PrimaryName;
    }

    public void setStj4PrimaryName(String stj4PrimaryName) {
        this.stj4PrimaryName = stj4PrimaryName;
    }

    public String getStj5PrimaryName() {
        return stj5PrimaryName;
    }

    public void setStj5PrimaryName(String stj5PrimaryName) {
        this.stj5PrimaryName = stj5PrimaryName;
    }

    public String getCitySecondaryName() {
        return citySecondaryName;
    }

    public void setCitySecondaryName(String citySecondaryName) {
        this.citySecondaryName = citySecondaryName;
    }

    public String getCountySecondaryName() {
        return countySecondaryName;
    }

    public void setCountySecondaryName(String countySecondaryName) {
        this.countySecondaryName = countySecondaryName;
    }

    public String getStj1SecondaryName() {
        return stj1SecondaryName;
    }

    public void setStj1SecondaryName(String stj1SecondaryName) {
        this.stj1SecondaryName = stj1SecondaryName;
    }

    public String getStj2SecondaryName() {
        return stj2SecondaryName;
    }

    public void setStj2SecondaryName(String stj2SecondaryName) {
        this.stj2SecondaryName = stj2SecondaryName;
    }

    public String getStj3SecondaryName() {
        return stj3SecondaryName;
    }

    public void setStj3SecondaryName(String stj3SecondaryName) {
        this.stj3SecondaryName = stj3SecondaryName;
    }

    public String getStj4SecondaryName() {
        return stj4SecondaryName;
    }

    public void setStj4SecondaryName(String stj4SecondaryName) {
        this.stj4SecondaryName = stj4SecondaryName;
    }

    public String getStj5SecondaryName() {
        return stj5SecondaryName;
    }

    public void setStj5SecondaryName(String stj5SecondaryName) {
        this.stj5SecondaryName = stj5SecondaryName;
    }

    public String getCountryPrimaryNexusindCode() {
        return countryPrimaryNexusindCode;
    }

    public void setCountryPrimaryNexusindCode(String countryPrimaryNexusindCode) {
        this.countryPrimaryNexusindCode = countryPrimaryNexusindCode;
    }

    public String getStatePrimaryNexusindCode() {
        return statePrimaryNexusindCode;
    }

    public void setStatePrimaryNexusindCode(String statePrimaryNexusindCode) {
        this.statePrimaryNexusindCode = statePrimaryNexusindCode;
    }

    public String getCountyPrimaryNexusindCode() {
        return countyPrimaryNexusindCode;
    }

    public void setCountyPrimaryNexusindCode(String countyPrimaryNexusindCode) {
        this.countyPrimaryNexusindCode = countyPrimaryNexusindCode;
    }

    public String getCityPrimaryNexusindCode() {
        return cityPrimaryNexusindCode;
    }

    public void setCityPrimaryNexusindCode(String cityPrimaryNexusindCode) {
        this.cityPrimaryNexusindCode = cityPrimaryNexusindCode;
    }

    public String getStj1PrimaryNexusindCode() {
        return stj1PrimaryNexusindCode;
    }

    public void setStj1PrimaryNexusindCode(String stj1PrimaryNexusindCode) {
        this.stj1PrimaryNexusindCode = stj1PrimaryNexusindCode;
    }

    public String getStj2PrimaryNexusindCode() {
        return stj2PrimaryNexusindCode;
    }

    public void setStj2PrimaryNexusindCode(String stj2PrimaryNexusindCode) {
        this.stj2PrimaryNexusindCode = stj2PrimaryNexusindCode;
    }

    public String getStj3PrimaryNexusindCode() {
        return stj3PrimaryNexusindCode;
    }

    public void setStj3PrimaryNexusindCode(String stj3PrimaryNexusindCode) {
        this.stj3PrimaryNexusindCode = stj3PrimaryNexusindCode;
    }

    public String getStj4PrimaryNexusindCode() {
        return stj4PrimaryNexusindCode;
    }

    public void setStj4PrimaryNexusindCode(String stj4PrimaryNexusindCode) {
        this.stj4PrimaryNexusindCode = stj4PrimaryNexusindCode;
    }

    public String getStj5PrimaryNexusindCode() {
        return stj5PrimaryNexusindCode;
    }

    public void setStj5PrimaryNexusindCode(String stj5PrimaryNexusindCode) {
        this.stj5PrimaryNexusindCode = stj5PrimaryNexusindCode;
    }

    public String getCitySecondaryNexusindCode() {
        return citySecondaryNexusindCode;
    }

    public void setCitySecondaryNexusindCode(String citySecondaryNexusindCode) {
        this.citySecondaryNexusindCode = citySecondaryNexusindCode;
    }

    public String getCountySecondaryNexusindCode() {
        return countySecondaryNexusindCode;
    }

    public void setCountySecondaryNexusindCode(String countySecondaryNexusindCode) {
        this.countySecondaryNexusindCode = countySecondaryNexusindCode;
    }

    public String getStj1SecondaryNexusindCode() {
        return stj1SecondaryNexusindCode;
    }

    public void setStj1SecondaryNexusindCode(String stj1SecondaryNexusindCode) {
        this.stj1SecondaryNexusindCode = stj1SecondaryNexusindCode;
    }

    public String getStj2SecondaryNexusindCode() {
        return stj2SecondaryNexusindCode;
    }

    public void setStj2SecondaryNexusindCode(String stj2SecondaryNexusindCode) {
        this.stj2SecondaryNexusindCode = stj2SecondaryNexusindCode;
    }

    public String getStj3SecondaryNexusindCode() {
        return stj3SecondaryNexusindCode;
    }

    public void setStj3SecondaryNexusindCode(String stj3SecondaryNexusindCode) {
        this.stj3SecondaryNexusindCode = stj3SecondaryNexusindCode;
    }

    public String getStj4SecondaryNexusindCode() {
        return stj4SecondaryNexusindCode;
    }

    public void setStj4SecondaryNexusindCode(String stj4SecondaryNexusindCode) {
        this.stj4SecondaryNexusindCode = stj4SecondaryNexusindCode;
    }

    public String getStj5SecondaryNexusindCode() {
        return stj5SecondaryNexusindCode;
    }

    public void setStj5SecondaryNexusindCode(String stj5SecondaryNexusindCode) {
        this.stj5SecondaryNexusindCode = stj5SecondaryNexusindCode;
    }

    public String getLookupMaxCountryCode() {
        return lookupMaxCountryCode;
    }

    public void setLookupMaxCountryCode(String lookupMaxCountryCode) {
        this.lookupMaxCountryCode = lookupMaxCountryCode;
    }

    public String getLookupMaxStateCode() {
        return lookupMaxStateCode;
    }

    public void setLookupMaxStateCode(String lookupMaxStateCode) {
        this.lookupMaxStateCode = lookupMaxStateCode;
    }

    public Long getCountryPrimarySitusJurId() {
        return countryPrimarySitusJurId;
    }

    public void setCountryPrimarySitusJurId(Long countryPrimarySitusJurId) {
        this.countryPrimarySitusJurId = countryPrimarySitusJurId;
    }

    public Long getStatePrimarySitusJurId() {
        return statePrimarySitusJurId;
    }

    public void setStatePrimarySitusJurId(Long statePrimarySitusJurId) {
        this.statePrimarySitusJurId = statePrimarySitusJurId;
    }

    public Long getCountyPrimarySitusJurId() {
        return countyPrimarySitusJurId;
    }

    public void setCountyPrimarySitusJurId(Long countyPrimarySitusJurId) {
        this.countyPrimarySitusJurId = countyPrimarySitusJurId;
    }

    public Long getCityPrimarySitusJurId() {
        return cityPrimarySitusJurId;
    }

    public void setCityPrimarySitusJurId(Long cityPrimarySitusJurId) {
        this.cityPrimarySitusJurId = cityPrimarySitusJurId;
    }

    public Long getStj1PrimarySitusJurId() {
        return stj1PrimarySitusJurId;
    }

    public void setStj1PrimarySitusJurId(Long stj1PrimarySitusJurId) {
        this.stj1PrimarySitusJurId = stj1PrimarySitusJurId;
    }

    public Long getStj2PrimarySitusJurId() {
        return stj2PrimarySitusJurId;
    }

    public void setStj2PrimarySitusJurId(Long stj2PrimarySitusJurId) {
        this.stj2PrimarySitusJurId = stj2PrimarySitusJurId;
    }

    public Long getStj3PrimarySitusJurId() {
        return stj3PrimarySitusJurId;
    }

    public void setStj3PrimarySitusJurId(Long stj3PrimarySitusJurId) {
        this.stj3PrimarySitusJurId = stj3PrimarySitusJurId;
    }

    public Long getStj4PrimarySitusJurId() {
        return stj4PrimarySitusJurId;
    }

    public void setStj4PrimarySitusJurId(Long stj4PrimarySitusJurId) {
        this.stj4PrimarySitusJurId = stj4PrimarySitusJurId;
    }

    public Long getStj5PrimarySitusJurId() {
        return stj5PrimarySitusJurId;
    }

    public void setStj5PrimarySitusJurId(Long stj5PrimarySitusJurId) {
        this.stj5PrimarySitusJurId = stj5PrimarySitusJurId;
    }

    public Long getCitySecondarySitusJurId() {
        return citySecondarySitusJurId;
    }

    public void setCitySecondarySitusJurId(Long citySecondarySitusJurId) {
        this.citySecondarySitusJurId = citySecondarySitusJurId;
    }

    public Long getCountySecondarySitusJurId() {
        return countySecondarySitusJurId;
    }

    public void setCountySecondarySitusJurId(Long countySecondarySitusJurId) {
        this.countySecondarySitusJurId = countySecondarySitusJurId;
    }

    public Long getStj1SecondarySitusJurId() {
        return stj1SecondarySitusJurId;
    }

    public void setStj1SecondarySitusJurId(Long stj1SecondarySitusJurId) {
        this.stj1SecondarySitusJurId = stj1SecondarySitusJurId;
    }

    public Long getStj2SecondarySitusJurId() {
        return stj2SecondarySitusJurId;
    }

    public void setStj2SecondarySitusJurId(Long stj2SecondarySitusJurId) {
        this.stj2SecondarySitusJurId = stj2SecondarySitusJurId;
    }

    public Long getStj3SecondarySitusJurId() {
        return stj3SecondarySitusJurId;
    }

    public void setStj3SecondarySitusJurId(Long stj3SecondarySitusJurId) {
        this.stj3SecondarySitusJurId = stj3SecondarySitusJurId;
    }

    public Long getStj4SecondarySitusJurId() {
        return stj4SecondarySitusJurId;
    }

    public void setStj4SecondarySitusJurId(Long stj4SecondarySitusJurId) {
        this.stj4SecondarySitusJurId = stj4SecondarySitusJurId;
    }

    public Long getStj5SecondarySitusJurId() {
        return stj5SecondarySitusJurId;
    }

    public void setStj5SecondarySitusJurId(Long stj5SecondarySitusJurId) {
        this.stj5SecondarySitusJurId = stj5SecondarySitusJurId;
    }

    public BigDecimal getMaxLocalRate() {
        return maxLocalRate;
    }

    public void setMaxLocalRate(BigDecimal maxLocalRate) {
        this.maxLocalRate = maxLocalRate;
    }

    public String getStj1PrimaryType() {
        return stj1PrimaryType;
    }

    public void setStj1PrimaryType(String stj1PrimaryType) {
        this.stj1PrimaryType = stj1PrimaryType;
    }

    public String getStj2PrimaryType() {
        return stj2PrimaryType;
    }

    public void setStj2PrimaryType(String stj2PrimaryType) {
        this.stj2PrimaryType = stj2PrimaryType;
    }

    public String getStj3PrimaryType() {
        return stj3PrimaryType;
    }

    public void setStj3PrimaryType(String stj3PrimaryType) {
        this.stj3PrimaryType = stj3PrimaryType;
    }

    public String getStj4PrimaryType() {
        return stj4PrimaryType;
    }

    public void setStj4PrimaryType(String stj4PrimaryType) {
        this.stj4PrimaryType = stj4PrimaryType;
    }

    public String getStj5PrimaryType() {
        return stj5PrimaryType;
    }

    public void setStj5PrimaryType(String stj5PrimaryType) {
        this.stj5PrimaryType = stj5PrimaryType;
    }

    public String getStj1SecondaryType() {
        return stj1SecondaryType;
    }

    public void setStj1SecondaryType(String stj1SecondaryType) {
        this.stj1SecondaryType = stj1SecondaryType;
    }

    public String getStj2SecondaryType() {
        return stj2SecondaryType;
    }

    public void setStj2SecondaryType(String stj2SecondaryType) {
        this.stj2SecondaryType = stj2SecondaryType;
    }

    public String getStj3SecondaryType() {
        return stj3SecondaryType;
    }

    public void setStj3SecondaryType(String stj3SecondaryType) {
        this.stj3SecondaryType = stj3SecondaryType;
    }

    public String getStj4SecondaryType() {
        return stj4SecondaryType;
    }

    public void setStj4SecondaryType(String stj4SecondaryType) {
        this.stj4SecondaryType = stj4SecondaryType;
    }

    public String getStj5SecondaryType() {
        return stj5SecondaryType;
    }

    public void setStj5SecondaryType(String stj5SecondaryType) {
        this.stj5SecondaryType = stj5SecondaryType;
    }

    public Boolean getPrimarySpdFlag() {
        return primarySpdFlag;
    }

    public void setPrimarySpdFlag(Boolean primarySpdFlag) {
        this.primarySpdFlag = primarySpdFlag;
    }

    public Boolean getPrimaryMtaFlag() {
        return primaryMtaFlag;
    }

    public void setPrimaryMtaFlag(Boolean primaryMtaFlag) {
        this.primaryMtaFlag = primaryMtaFlag;
    }

    public BigDecimal getCombinedLocalRate() {
        return combinedLocalRate;
    }

    public void setCombinedLocalRate(BigDecimal combinedLocalRate) {
        this.combinedLocalRate = combinedLocalRate;
    }

    public Date getStj1EffectiveDate() {
        return stj1EffectiveDate;
    }

    public void setStj1EffectiveDate(Date stj1EffectiveDate) {
        this.stj1EffectiveDate = stj1EffectiveDate;
    }

    public Date getStj2EffectiveDate() {
        return stj2EffectiveDate;
    }

    public void setStj2EffectiveDate(Date stj2EffectiveDate) {
        this.stj2EffectiveDate = stj2EffectiveDate;
    }

    public Date getStj3EffectiveDate() {
        return stj3EffectiveDate;
    }

    public void setStj3EffectiveDate(Date stj3EffectiveDate) {
        this.stj3EffectiveDate = stj3EffectiveDate;
    }

    public Date getStj4EffectiveDate() {
        return stj4EffectiveDate;
    }

    public void setStj4EffectiveDate(Date stj4EffectiveDate) {
        this.stj4EffectiveDate = stj4EffectiveDate;
    }

    public Date getStj5EffectiveDate() {
        return stj5EffectiveDate;
    }

    public void setStj5EffectiveDate(Date stj5EffectiveDate) {
        this.stj5EffectiveDate = stj5EffectiveDate;
    }

    public Date getStj6EffectiveDate() {
        return stj6EffectiveDate;
    }

    public void setStj6EffectiveDate(Date stj6EffectiveDate) {
        this.stj6EffectiveDate = stj6EffectiveDate;
    }

    public Date getStj7EffectiveDate() {
        return stj7EffectiveDate;
    }

    public void setStj7EffectiveDate(Date stj7EffectiveDate) {
        this.stj7EffectiveDate = stj7EffectiveDate;
    }

    public Date getStj8EffectiveDate() {
        return stj8EffectiveDate;
    }

    public void setStj8EffectiveDate(Date stj8EffectiveDate) {
        this.stj8EffectiveDate = stj8EffectiveDate;
    }

    public Date getStj9EffectiveDate() {
        return stj9EffectiveDate;
    }

    public void setStj9EffectiveDate(Date stj9EffectiveDate) {
        this.stj9EffectiveDate = stj9EffectiveDate;
    }

    public Date getStj10EffectiveDate() {
        return stj10EffectiveDate;
    }

    public void setStj10EffectiveDate(Date stj10EffectiveDate) {
        this.stj10EffectiveDate = stj10EffectiveDate;
    }
 
	public void setShiptoCountryNexusindCode(String shiptoCountryNexusindCode){
	    this.shiptoCountryNexusindCode = shiptoCountryNexusindCode;
	}
	
	public void setShiptoStateNexusindCode(String shiptoStateNexusindCode){
	    this.shiptoStateNexusindCode = shiptoStateNexusindCode;
	}
	
	public void setShiptoCountyNexusindCode(String shiptoCountyNexusindCode){
	    this.shiptoCountyNexusindCode = shiptoCountyNexusindCode;
	}
	
	public void setShiptoCityNexusindCode(String shiptoCityNexusindCode){
	    this.shiptoCityNexusindCode = shiptoCityNexusindCode;
	}
	
	public void setShiptoStj1NexusindCode(String shiptoStj1NexusindCode){
	    this.shiptoStj1NexusindCode = shiptoStj1NexusindCode;
	}
	
	public void setShiptoStj2NexusindCode(String shiptoStj2NexusindCode){
	    this.shiptoStj2NexusindCode = shiptoStj2NexusindCode;
	}
	
	public void setShiptoStj3NexusindCode(String shiptoStj3NexusindCode){
	    this.shiptoStj3NexusindCode = shiptoStj3NexusindCode;
	}
	
	public void setShiptoStj4NexusindCode(String shiptoStj4NexusindCode){
	    this.shiptoStj4NexusindCode = shiptoStj4NexusindCode;
	}
	
	public void setShiptoStj5NexusindCode(String shiptoStj5NexusindCode){
	    this.shiptoStj5NexusindCode = shiptoStj5NexusindCode;
	}
	
	public void setShiptoStj1LocalCode(String shiptoStj1LocalCode){
	    this.shiptoStj1LocalCode = shiptoStj1LocalCode;
	}
	
	public void setShiptoStj2LocalCode(String shiptoStj2LocalCode){
	    this.shiptoStj2LocalCode = shiptoStj2LocalCode;
	}
	
	public void setShiptoStj3LocalCode(String shiptoStj3LocalCode){
	    this.shiptoStj3LocalCode = shiptoStj3LocalCode;
	}
	
	public void setShiptoStj4LocalCode(String shiptoStj4LocalCode){
	    this.shiptoStj4LocalCode = shiptoStj4LocalCode;
	}
	
	public void setShiptoStj5LocalCode(String shiptoStj5LocalCode){
	    this.shiptoStj5LocalCode = shiptoStj5LocalCode;
	}
	
	public String getShiptoCountryNexusindCode(){
	    return shiptoCountryNexusindCode;
	}
	
	public String getShiptoStateNexusindCode(){
	    return shiptoStateNexusindCode;
	}
	
	public String getShiptoCountyNexusindCode(){
	    return shiptoCountyNexusindCode;
	}
	
	public String getShiptoCityNexusindCode(){
	    return shiptoCityNexusindCode;
	}
	
	public String getShiptoStj1NexusindCode(){
	    return shiptoStj1NexusindCode;
	}
	
	public String getShiptoStj2NexusindCode(){
	    return shiptoStj2NexusindCode;
	}
	
	public String getShiptoStj3NexusindCode(){
	    return shiptoStj3NexusindCode;
	}
	
	public String getShiptoStj4NexusindCode(){
	    return shiptoStj4NexusindCode;
	}
	
	public String getShiptoStj5NexusindCode(){
	    return shiptoStj5NexusindCode;
	}
	
	public String getShiptoStj1LocalCode(){
	    return shiptoStj1LocalCode;
	}
	
	public String getShiptoStj2LocalCode(){
	    return shiptoStj2LocalCode;
	}
	
	public String getShiptoStj3LocalCode(){
	    return shiptoStj3LocalCode;
	}
	
	public String getShiptoStj4LocalCode(){
	    return shiptoStj4LocalCode;
	}
	
	public String getShiptoStj5LocalCode(){
	    return shiptoStj5LocalCode;
	}
	
	
	public void setBilltoCountryNexusindCode(String billtoCountryNexusindCode){
	    this.billtoCountryNexusindCode = billtoCountryNexusindCode;
	}
	
	public void setBilltoStateNexusindCode(String billtoStateNexusindCode){
	    this.billtoStateNexusindCode = billtoStateNexusindCode;
	}
	
	public void setBilltoCountyNexusindCode(String billtoCountyNexusindCode){
	    this.billtoCountyNexusindCode = billtoCountyNexusindCode;
	}
	
	public void setBilltoCityNexusindCode(String billtoCityNexusindCode){
	    this.billtoCityNexusindCode = billtoCityNexusindCode;
	}
	
	public void setBilltoStj1NexusindCode(String billtoStj1NexusindCode){
	    this.billtoStj1NexusindCode = billtoStj1NexusindCode;
	}
	
	public void setBilltoStj2NexusindCode(String billtoStj2NexusindCode){
	    this.billtoStj2NexusindCode = billtoStj2NexusindCode;
	}
	
	public void setBilltoStj3NexusindCode(String billtoStj3NexusindCode){
	    this.billtoStj3NexusindCode = billtoStj3NexusindCode;
	}
	
	public void setBilltoStj4NexusindCode(String billtoStj4NexusindCode){
	    this.billtoStj4NexusindCode = billtoStj4NexusindCode;
	}
	
	public void setBilltoStj5NexusindCode(String billtoStj5NexusindCode){
	    this.billtoStj5NexusindCode = billtoStj5NexusindCode;
	}
	
	public void setBilltoStj1LocalCode(String billtoStj1LocalCode){
	    this.billtoStj1LocalCode = billtoStj1LocalCode;
	}
	
	public void setBilltoStj2LocalCode(String billtoStj2LocalCode){
	    this.billtoStj2LocalCode = billtoStj2LocalCode;
	}
	
	public void setBilltoStj3LocalCode(String billtoStj3LocalCode){
	    this.billtoStj3LocalCode = billtoStj3LocalCode;
	}
	
	public void setBilltoStj4LocalCode(String billtoStj4LocalCode){
	    this.billtoStj4LocalCode = billtoStj4LocalCode;
	}
	
	public void setBilltoStj5LocalCode(String billtoStj5LocalCode){
	    this.billtoStj5LocalCode = billtoStj5LocalCode;
	}
	
	public String getBilltoCountryNexusindCode(){
	    return billtoCountryNexusindCode;
	}
	
	public String getBilltoStateNexusindCode(){
	    return billtoStateNexusindCode;
	}
	
	public String getBilltoCountyNexusindCode(){
	    return billtoCountyNexusindCode;
	}
	
	public String getBilltoCityNexusindCode(){
	    return billtoCityNexusindCode;
	}

	public String getBilltoStj1NexusindCode(){
	    return billtoStj1NexusindCode;
	}
	
	public String getBilltoStj2NexusindCode(){
	    return billtoStj2NexusindCode;
	}
	
	public String getBilltoStj3NexusindCode(){
	    return billtoStj3NexusindCode;
	}
	
	public String getBilltoStj4NexusindCode(){
	    return billtoStj4NexusindCode;
	}
	
	public String getBilltoStj5NexusindCode(){
	    return billtoStj5NexusindCode;
	}
	
	public String getBilltoStj1LocalCode(){
	    return billtoStj1LocalCode;
	}
	
	public String getBilltoStj2LocalCode(){
	    return billtoStj2LocalCode;
	}
	
	public String getBilltoStj3LocalCode(){
	    return billtoStj3LocalCode;
	}
	
	public String getBilltoStj4LocalCode(){
	    return billtoStj4LocalCode;
	}
	
	public String getBilltoStj5LocalCode(){
	    return billtoStj5LocalCode;
	}
	
	public void setTtlxfrCountryNexusindCode(String ttlxfrCountryNexusindCode){
	    this.ttlxfrCountryNexusindCode = ttlxfrCountryNexusindCode;
	}
	
	public void setTtlxfrStateNexusindCode(String ttlxfrStateNexusindCode){
	    this.ttlxfrStateNexusindCode = ttlxfrStateNexusindCode;
	}
	
	public void setTtlxfrCountyNexusindCode(String ttlxfrCountyNexusindCode){
	    this.ttlxfrCountyNexusindCode = ttlxfrCountyNexusindCode;
	}
	
	public void setTtlxfrCityNexusindCode(String ttlxfrCityNexusindCode){
	    this.ttlxfrCityNexusindCode = ttlxfrCityNexusindCode;
	}

	public void setTtlxfrStj1NexusindCode(String ttlxfrStj1NexusindCode){
	    this.ttlxfrStj1NexusindCode = ttlxfrStj1NexusindCode;
	}
	
	public void setTtlxfrStj2NexusindCode(String ttlxfrStj2NexusindCode){
	    this.ttlxfrStj2NexusindCode = ttlxfrStj2NexusindCode;
	}
	
	public void setTtlxfrStj3NexusindCode(String ttlxfrStj3NexusindCode){
	    this.ttlxfrStj3NexusindCode = ttlxfrStj3NexusindCode;
	}
	
	public void setTtlxfrStj4NexusindCode(String ttlxfrStj4NexusindCode){
	    this.ttlxfrStj4NexusindCode = ttlxfrStj4NexusindCode;
	}
	
	public void setTtlxfrStj5NexusindCode(String ttlxfrStj5NexusindCode){
	    this.ttlxfrStj5NexusindCode = ttlxfrStj5NexusindCode;
	}
	
	public void setTtlxfrStj1LocalCode(String ttlxfrStj1LocalCode){
	    this.ttlxfrStj1LocalCode = ttlxfrStj1LocalCode;
	}
	
	public void setTtlxfrStj2LocalCode(String ttlxfrStj2LocalCode){
	    this.ttlxfrStj2LocalCode = ttlxfrStj2LocalCode;
	}
	
	public void setTtlxfrStj3LocalCode(String ttlxfrStj3LocalCode){
	    this.ttlxfrStj3LocalCode = ttlxfrStj3LocalCode;
	}
	
	public void setTtlxfrStj4LocalCode(String ttlxfrStj4LocalCode){
	    this.ttlxfrStj4LocalCode = ttlxfrStj4LocalCode;
	}
	
	public void setTtlxfrStj5LocalCode(String ttlxfrStj5LocalCode){
	    this.ttlxfrStj5LocalCode = ttlxfrStj5LocalCode;
	}
	
	public String getTtlxfrCountryNexusindCode(){
	    return ttlxfrCountryNexusindCode;
	}
	
	public String getTtlxfrStateNexusindCode(){
	    return ttlxfrStateNexusindCode;
	}
	
	public String getTtlxfrCountyNexusindCode(){
	    return ttlxfrCountyNexusindCode;
	}
	
	public String getTtlxfrCityNexusindCode(){
	    return ttlxfrCityNexusindCode;
	}

	public String getTtlxfrStj1NexusindCode(){
	    return ttlxfrStj1NexusindCode;
	}
	
	public String getTtlxfrStj2NexusindCode(){
	    return ttlxfrStj2NexusindCode;
	}
	
	public String getTtlxfrStj3NexusindCode(){
	    return ttlxfrStj3NexusindCode;
	}
	
	public String getTtlxfrStj4NexusindCode(){
	    return ttlxfrStj4NexusindCode;
	}
	
	public String getTtlxfrStj5NexusindCode(){
	    return ttlxfrStj5NexusindCode;
	}
	
	public String getTtlxfrStj1LocalCode(){
	    return ttlxfrStj1LocalCode;
	}
	
	public String getTtlxfrStj2LocalCode(){
	    return ttlxfrStj2LocalCode;
	}
	
	public String getTtlxfrStj3LocalCode(){
	    return ttlxfrStj3LocalCode;
	}
	
	public String getTtlxfrStj4LocalCode(){
	    return ttlxfrStj4LocalCode;
	}
	
	public String getTtlxfrStj5LocalCode(){
	    return ttlxfrStj5LocalCode;
	}
	
	public void setFirstuseCountryNexusindCode(String firstuseCountryNexusindCode){
	    this.firstuseCountryNexusindCode = firstuseCountryNexusindCode;
	}
	
	public void setFirstuseStateNexusindCode(String firstuseStateNexusindCode){
	    this.firstuseStateNexusindCode = firstuseStateNexusindCode;
	}
	
	public void setFirstuseCountyNexusindCode(String firstuseCountyNexusindCode){
	    this.firstuseCountyNexusindCode = firstuseCountyNexusindCode;
	}
	
	public void setFirstuseCityNexusindCode(String firstuseCityNexusindCode){
	    this.firstuseCityNexusindCode = firstuseCityNexusindCode;
	}

	public void setFirstuseStj1NexusindCode(String firstuseStj1NexusindCode){
	    this.firstuseStj1NexusindCode = firstuseStj1NexusindCode;
	}
	
	public void setFirstuseStj2NexusindCode(String firstuseStj2NexusindCode){
	    this.firstuseStj2NexusindCode = firstuseStj2NexusindCode;
	}
	
	public void setFirstuseStj3NexusindCode(String firstuseStj3NexusindCode){
	    this.firstuseStj3NexusindCode = firstuseStj3NexusindCode;
	}
	
	public void setFirstuseStj4NexusindCode(String firstuseStj4NexusindCode){
	    this.firstuseStj4NexusindCode = firstuseStj4NexusindCode;
	}
	
	public void setFirstuseStj5NexusindCode(String firstuseStj5NexusindCode){
	    this.firstuseStj5NexusindCode = firstuseStj5NexusindCode;
	}
	
	public void setFirstuseStj1LocalCode(String firstuseStj1LocalCode){
	    this.firstuseStj1LocalCode = firstuseStj1LocalCode;
	}
	
	public void setFirstuseStj2LocalCode(String firstuseStj2LocalCode){
	    this.firstuseStj2LocalCode = firstuseStj2LocalCode;
	}
	
	public void setFirstuseStj3LocalCode(String firstuseStj3LocalCode){
	    this.firstuseStj3LocalCode = firstuseStj3LocalCode;
	}
	
	public void setFirstuseStj4LocalCode(String firstuseStj4LocalCode){
	    this.firstuseStj4LocalCode = firstuseStj4LocalCode;
	}
	
	public void setFirstuseStj5LocalCode(String firstuseStj5LocalCode){
	    this.firstuseStj5LocalCode = firstuseStj5LocalCode;
	}
	
	public String getFirstuseCountryNexusindCode(){
	    return firstuseCountryNexusindCode;
	}
	
	public String getFirstuseStateNexusindCode(){
	    return firstuseStateNexusindCode;
	}
	
	public String getFirstuseCountyNexusindCode(){
	    return firstuseCountyNexusindCode;
	}
	
	public String getFirstuseCityNexusindCode(){
	    return firstuseCityNexusindCode;
	}
	
	public String getFirstuseStj1NexusindCode(){
	    return firstuseStj1NexusindCode;
	}
	
	public String getFirstuseStj2NexusindCode(){
	    return firstuseStj2NexusindCode;
	}
	
	public String getFirstuseStj3NexusindCode(){
	    return firstuseStj3NexusindCode;
	}
	
	public String getFirstuseStj4NexusindCode(){
	    return firstuseStj4NexusindCode;
	}
	
	public String getFirstuseStj5NexusindCode(){
	    return firstuseStj5NexusindCode;
	}
	
	public String getFirstuseStj1LocalCode(){
	    return firstuseStj1LocalCode;
	}
	
	public String getFirstuseStj2LocalCode(){
	    return firstuseStj2LocalCode;
	}
	
	public String getFirstuseStj3LocalCode(){
	    return firstuseStj3LocalCode;
	}
	
	public String getFirstuseStj4LocalCode(){
	    return firstuseStj4LocalCode;
	}
	
	public String getFirstuseStj5LocalCode(){
	    return firstuseStj5LocalCode;
	}
	
	public void setShipfromCountryNexusindCode(String shipfromCountryNexusindCode){
	    this.shipfromCountryNexusindCode = shipfromCountryNexusindCode;
	}
	
	public void setShipfromStateNexusindCode(String shipfromStateNexusindCode){
	    this.shipfromStateNexusindCode = shipfromStateNexusindCode;
	}
	
	public void setShipfromCountyNexusindCode(String shipfromCountyNexusindCode){
	    this.shipfromCountyNexusindCode = shipfromCountyNexusindCode;
	}
	
	public void setShipfromCityNexusindCode(String shipfromCityNexusindCode){
	    this.shipfromCityNexusindCode = shipfromCityNexusindCode;
	}

	public void setShipfromStj1NexusindCode(String shipfromStj1NexusindCode){
	    this.shipfromStj1NexusindCode = shipfromStj1NexusindCode;
	}
	
	public void setShipfromStj2NexusindCode(String shipfromStj2NexusindCode){
	    this.shipfromStj2NexusindCode = shipfromStj2NexusindCode;
	}
	
	public void setShipfromStj3NexusindCode(String shipfromStj3NexusindCode){
	    this.shipfromStj3NexusindCode = shipfromStj3NexusindCode;
	}
	
	public void setShipfromStj4NexusindCode(String shipfromStj4NexusindCode){
	    this.shipfromStj4NexusindCode = shipfromStj4NexusindCode;
	}
	
	public void setShipfromStj5NexusindCode(String shipfromStj5NexusindCode){
	    this.shipfromStj5NexusindCode = shipfromStj5NexusindCode;
	}
	
	public void setShipfromStj1LocalCode(String shipfromStj1LocalCode){
	    this.shipfromStj1LocalCode = shipfromStj1LocalCode;
	}
	
	public void setShipfromStj2LocalCode(String shipfromStj2LocalCode){
	    this.shipfromStj2LocalCode = shipfromStj2LocalCode;
	}
	
	public void setShipfromStj3LocalCode(String shipfromStj3LocalCode){
	    this.shipfromStj3LocalCode = shipfromStj3LocalCode;
	}
	
	public void setShipfromStj4LocalCode(String shipfromStj4LocalCode){
	    this.shipfromStj4LocalCode = shipfromStj4LocalCode;
	}
	
	public void setShipfromStj5LocalCode(String shipfromStj5LocalCode){
	    this.shipfromStj5LocalCode = shipfromStj5LocalCode;
	}
	
	public String getShipfromCountryNexusindCode(){
	    return shipfromCountryNexusindCode;
	}
	
	public String getShipfromStateNexusindCode(){
	    return shipfromStateNexusindCode;
	}
	
	public String getShipfromCountyNexusindCode(){
	    return shipfromCountyNexusindCode;
	}
	
	public String getShipfromCityNexusindCode(){
	    return shipfromCityNexusindCode;
	}

	public String getShipfromStj1NexusindCode(){
	    return shipfromStj1NexusindCode;
	}
	
	public String getShipfromStj2NexusindCode(){
	    return shipfromStj2NexusindCode;
	}
	
	public String getShipfromStj3NexusindCode(){
	    return shipfromStj3NexusindCode;
	}
	
	public String getShipfromStj4NexusindCode(){
	    return shipfromStj4NexusindCode;
	}
	
	public String getShipfromStj5NexusindCode(){
	    return shipfromStj5NexusindCode;
	}
	
	public String getShipfromStj1LocalCode(){
	    return shipfromStj1LocalCode;
	}
	
	public String getShipfromStj2LocalCode(){
	    return shipfromStj2LocalCode;
	}
	
	public String getShipfromStj3LocalCode(){
	    return shipfromStj3LocalCode;
	}
	
	public String getShipfromStj4LocalCode(){
	    return shipfromStj4LocalCode;
	}
	
	public String getShipfromStj5LocalCode(){
	    return shipfromStj5LocalCode;
	}
	
	public void setOrdrorgnCountryNexusindCode(String ordrorgnCountryNexusindCode){
	    this.ordrorgnCountryNexusindCode = ordrorgnCountryNexusindCode;
	}
	
	public void setOrdrorgnStateNexusindCode(String ordrorgnStateNexusindCode){
	    this.ordrorgnStateNexusindCode = ordrorgnStateNexusindCode;
	}
	
	public void setOrdrorgnCountyNexusindCode(String ordrorgnCountyNexusindCode){
	    this.ordrorgnCountyNexusindCode = ordrorgnCountyNexusindCode;
	}
	
	public void setOrdrorgnCityNexusindCode(String ordrorgnCityNexusindCode){
	    this.ordrorgnCityNexusindCode = ordrorgnCityNexusindCode;
	}
	
	public void setOrdrorgnStj1NexusindCode(String ordrorgnStj1NexusindCode){
	    this.ordrorgnStj1NexusindCode = ordrorgnStj1NexusindCode;
	}
	
	public void setOrdrorgnStj2NexusindCode(String ordrorgnStj2NexusindCode){
	    this.ordrorgnStj2NexusindCode = ordrorgnStj2NexusindCode;
	}
	
	public void setOrdrorgnStj3NexusindCode(String ordrorgnStj3NexusindCode){
	    this.ordrorgnStj3NexusindCode = ordrorgnStj3NexusindCode;
	}
	
	public void setOrdrorgnStj4NexusindCode(String ordrorgnStj4NexusindCode){
	    this.ordrorgnStj4NexusindCode = ordrorgnStj4NexusindCode;
	}
	
	public void setOrdrorgnStj5NexusindCode(String ordrorgnStj5NexusindCode){
	    this.ordrorgnStj5NexusindCode = ordrorgnStj5NexusindCode;
	}
	
	public void setOrdrorgnStj1LocalCode(String ordrorgnStj1LocalCode){
	    this.ordrorgnStj1LocalCode = ordrorgnStj1LocalCode;
	}
	
	public void setOrdrorgnStj2LocalCode(String ordrorgnStj2LocalCode){
	    this.ordrorgnStj2LocalCode = ordrorgnStj2LocalCode;
	}
	
	public void setOrdrorgnStj3LocalCode(String ordrorgnStj3LocalCode){
	    this.ordrorgnStj3LocalCode = ordrorgnStj3LocalCode;
	}
	
	public void setOrdrorgnStj4LocalCode(String ordrorgnStj4LocalCode){
	    this.ordrorgnStj4LocalCode = ordrorgnStj4LocalCode;
	}
	
	public void setOrdrorgnStj5LocalCode(String ordrorgnStj5LocalCode){
	    this.ordrorgnStj5LocalCode = ordrorgnStj5LocalCode;
	}
	
	public String getOrdrorgnCountryNexusindCode(){
	    return ordrorgnCountryNexusindCode;
	}
	
	public String getOrdrorgnStateNexusindCode(){
	    return ordrorgnStateNexusindCode;
	}
	
	public String getOrdrorgnCountyNexusindCode(){
	    return ordrorgnCountyNexusindCode;
	}
	
	public String getOrdrorgnCityNexusindCode(){
	    return ordrorgnCityNexusindCode;
	}

	public String getOrdrorgnStj1NexusindCode(){
	    return ordrorgnStj1NexusindCode;
	}
	
	public String getOrdrorgnStj2NexusindCode(){
	    return ordrorgnStj2NexusindCode;
	}
	
	public String getOrdrorgnStj3NexusindCode(){
	    return ordrorgnStj3NexusindCode;
	}
	
	public String getOrdrorgnStj4NexusindCode(){
	    return ordrorgnStj4NexusindCode;
	}
	
	public String getOrdrorgnStj5NexusindCode(){
	    return ordrorgnStj5NexusindCode;
	}
	
	public String getOrdrorgnStj1LocalCode(){
	    return ordrorgnStj1LocalCode;
	}
	
	public String getOrdrorgnStj2LocalCode(){
	    return ordrorgnStj2LocalCode;
	}
	
	public String getOrdrorgnStj3LocalCode(){
	    return ordrorgnStj3LocalCode;
	}
	
	public String getOrdrorgnStj4LocalCode(){
	    return ordrorgnStj4LocalCode;
	}
	
	public String getOrdrorgnStj5LocalCode(){
	    return ordrorgnStj5LocalCode;
	}
	
	public void setOrdracptCountryNexusindCode(String ordracptCountryNexusindCode){
	    this.ordracptCountryNexusindCode = ordracptCountryNexusindCode;
	}
	
	public void setOrdracptStateNexusindCode(String ordracptStateNexusindCode){
	    this.ordracptStateNexusindCode = ordracptStateNexusindCode;
	}
	
	public void setOrdracptCountyNexusindCode(String ordracptCountyNexusindCode){
	    this.ordracptCountyNexusindCode = ordracptCountyNexusindCode;
	}
	
	public void setOrdracptCityNexusindCode(String ordracptCityNexusindCode){
	    this.ordracptCityNexusindCode = ordracptCityNexusindCode;
	}
	
	public void setOrdracptStj1NexusindCode(String ordracptStj1NexusindCode){
	    this.ordracptStj1NexusindCode = ordracptStj1NexusindCode;
	}
	
	public void setOrdracptStj2NexusindCode(String ordracptStj2NexusindCode){
	    this.ordracptStj2NexusindCode = ordracptStj2NexusindCode;
	}
	
	public void setOrdracptStj3NexusindCode(String ordracptStj3NexusindCode){
	    this.ordracptStj3NexusindCode = ordracptStj3NexusindCode;
	}
	
	public void setOrdracptStj4NexusindCode(String ordracptStj4NexusindCode){
	    this.ordracptStj4NexusindCode = ordracptStj4NexusindCode;
	}
	
	public void setOrdracptStj5NexusindCode(String ordracptStj5NexusindCode){
	    this.ordracptStj5NexusindCode = ordracptStj5NexusindCode;
	}
	
	public void setOrdracptStj1LocalCode(String ordracptStj1LocalCode){
	    this.ordracptStj1LocalCode = ordracptStj1LocalCode;
	}
	
	public void setOrdracptStj2LocalCode(String ordracptStj2LocalCode){
	    this.ordracptStj2LocalCode = ordracptStj2LocalCode;
	}
	
	public void setOrdracptStj3LocalCode(String ordracptStj3LocalCode){
	    this.ordracptStj3LocalCode = ordracptStj3LocalCode;
	}
	
	public void setOrdracptStj4LocalCode(String ordracptStj4LocalCode){
	    this.ordracptStj4LocalCode = ordracptStj4LocalCode;
	}
	
	public void setOrdracptStj5LocalCode(String ordracptStj5LocalCode){
	    this.ordracptStj5LocalCode = ordracptStj5LocalCode;
	}
	
	public String getOrdracptCountryNexusindCode(){
	    return ordracptCountryNexusindCode;
	}
	
	public String getOrdracptStateNexusindCode(){
	    return ordracptStateNexusindCode;
	}
	
	public String getOrdracptCountyNexusindCode(){
	    return ordracptCountyNexusindCode;
	}
	
	public String getOrdracptCityNexusindCode(){
	    return ordracptCityNexusindCode;
	}
	
	public String getOrdracptStj1NexusindCode(){
	    return ordracptStj1NexusindCode;
	}
	
	public String getOrdracptStj2NexusindCode(){
	    return ordracptStj2NexusindCode;
	}
	
	public String getOrdracptStj3NexusindCode(){
	    return ordracptStj3NexusindCode;
	}
	
	public String getOrdracptStj4NexusindCode(){
	    return ordracptStj4NexusindCode;
	}
	
	public String getOrdracptStj5NexusindCode(){
	    return ordracptStj5NexusindCode;
	}
	
	public String getOrdracptStj1LocalCode(){
	    return ordracptStj1LocalCode;
	}
	
	public String getOrdracptStj2LocalCode(){
	    return ordracptStj2LocalCode;
	}
	
	public String getOrdracptStj3LocalCode(){
	    return ordracptStj3LocalCode;
	}
	
	public String getOrdracptStj4LocalCode(){
	    return ordracptStj4LocalCode;
	}
	
	public String getOrdracptStj5LocalCode(){
	    return ordracptStj5LocalCode;
	}

    public List<SitusInfoDTO> getSitusInfoList() {
        return situsInfoList;
    }

    public void setSitusInfoList(List<SitusInfoDTO> situsInfoList) {
        this.situsInfoList = situsInfoList;
    }



    public String[] getSitusStjFinalSitusCode() {
        return situsStjFinalSitusCode;
    }

    public void setSitusStjFinalSitusCode(String[] situsStjFinalSitusCode) {
        this.situsStjFinalSitusCode = situsStjFinalSitusCode;
    }

    public String[] getSitusStjFinalTxtypCd() {
        return situsStjFinalTxtypCd;
    }

    public void setSitusStjFinalTxtypCd(String[] situsStjFinalTxtypCd) {
        this.situsStjFinalTxtypCd = situsStjFinalTxtypCd;
    }

    public Long[] getSitusStjFinalJurId() {
        return situsStjFinalJurId;
    }

    public void setSitusStjFinalJurId(Long[] situsStjFinalJurId) {
        this.situsStjFinalJurId = situsStjFinalJurId;
    }

    public BigDecimal[] getSitusStjFinalRate() {
        return situsStjFinalRate;
    }

    public void setSitusStjFinalRate(BigDecimal[] situsStjFinalRate) {
        this.situsStjFinalRate = situsStjFinalRate;
    }

    public String[] getSitusStjFinalName() {
        return situsStjFinalName;
    }

    public void setSitusStjFinalName(String[] situsStjFinalName) {
        this.situsStjFinalName = situsStjFinalName;
    }

    public String[] getSitusStjFinalNexusindCode() {
        return situsStjFinalNexusindCode;
    }

    public void setSitusStjFinalNexusindCode(String[] situsStjFinalNexusindCode) {
        this.situsStjFinalNexusindCode = situsStjFinalNexusindCode;
    }

    public Long[] getSitusStjFinalTaxrateId() {
        return situsStjFinalTaxrateId;
    }

    public void setSitusStjFinalTaxrateId(Long[] situsStjFinalTaxrateId) {
        this.situsStjFinalTaxrateId = situsStjFinalTaxrateId;
    }

    public String getSitusDebugMatrix() {
        return situsDebugMatrix;
    }

    public void setSitusDebugMatrix(String situsDebugMatrix) {
        this.situsDebugMatrix = situsDebugMatrix;
    }

    public Boolean getPreProcessFlag() {
        return preProcessFlag;
    }

    public void setPreProcessFlag(Boolean preProcessFlag) {
        this.preProcessFlag = preProcessFlag;
    }

    public Boolean getPostProcessFlag() {
        return postProcessFlag;
    }

    public void setPostProcessFlag(Boolean postProcessFlag) {
        this.postProcessFlag = postProcessFlag;
    }


    public String getStj1PrimaryLocalCode() {
        return stj1PrimaryLocalCode;
    }

    public void setStj1PrimaryLocalCode(String stj1PrimaryLocalCode) {
        this.stj1PrimaryLocalCode = stj1PrimaryLocalCode;
    }

    public String getStj2PrimaryLocalCode() {
        return stj2PrimaryLocalCode;
    }

    public void setStj2PrimaryLocalCode(String stj2PrimaryLocalCode) {
        this.stj2PrimaryLocalCode = stj2PrimaryLocalCode;
    }

    public String getStj3PrimaryLocalCode() {
        return stj3PrimaryLocalCode;
    }

    public void setStj3PrimaryLocalCode(String stj3PrimaryLocalCode) {
        this.stj3PrimaryLocalCode = stj3PrimaryLocalCode;
    }

    public String getStj4PrimaryLocalCode() {
        return stj4PrimaryLocalCode;
    }

    public void setStj4PrimaryLocalCode(String stj4PrimaryLocalCode) {
        this.stj4PrimaryLocalCode = stj4PrimaryLocalCode;
    }

    public String getStj5PrimaryLocalCode() {
        return stj5PrimaryLocalCode;
    }

    public void setStj5PrimaryLocalCode(String stj5PrimaryLocalCode) {
        this.stj5PrimaryLocalCode = stj5PrimaryLocalCode;
    }

    public String getStj1SecondaryLocalCode() {
        return stj1SecondaryLocalCode;
    }

    public void setStj1SecondaryLocalCode(String stj1SecondaryLocalCode) {
        this.stj1SecondaryLocalCode = stj1SecondaryLocalCode;
    }

    public String getStj2SecondaryLocalCode() {
        return stj2SecondaryLocalCode;
    }

    public void setStj2SecondaryLocalCode(String stj2SecondaryLocalCode) {
        this.stj2SecondaryLocalCode = stj2SecondaryLocalCode;
    }

    public String getStj3SecondaryLocalCode() {
        return stj3SecondaryLocalCode;
    }

    public void setStj3SecondaryLocalCode(String stj3SecondaryLocalCode) {
        this.stj3SecondaryLocalCode = stj3SecondaryLocalCode;
    }

    public String getStj4SecondaryLocalCode() {
        return stj4SecondaryLocalCode;
    }

    public void setStj4SecondaryLocalCode(String stj4SecondaryLocalCode) {
        this.stj4SecondaryLocalCode = stj4SecondaryLocalCode;
    }

    public String getStj5SecondaryLocalCode() {
        return stj5SecondaryLocalCode;
    }

    public void setStj5SecondaryLocalCode(String stj5SecondaryLocalCode) {
        this.stj5SecondaryLocalCode = stj5SecondaryLocalCode;
    }

    public String getStj6PrimaryLocalCode() {
        return stj6PrimaryLocalCode;
    }

    public void setStj6PrimaryLocalCode(String stj6PrimaryLocalCode) {
        this.stj6PrimaryLocalCode = stj6PrimaryLocalCode;
    }

    public String getStj7PrimaryLocalCode() {
        return stj7PrimaryLocalCode;
    }

    public void setStj7PrimaryLocalCode(String stj7PrimaryLocalCode) {
        this.stj7PrimaryLocalCode = stj7PrimaryLocalCode;
    }

    public String getStj8PrimaryLocalCode() {
        return stj8PrimaryLocalCode;
    }

    public void setStj8PrimaryLocalCode(String stj8PrimaryLocalCode) {
        this.stj8PrimaryLocalCode = stj8PrimaryLocalCode;
    }

    public String getStj9PrimaryLocalCode() {
        return stj9PrimaryLocalCode;
    }

    public void setStj9PrimaryLocalCode(String stj9PrimaryLocalCode) {
        this.stj9PrimaryLocalCode = stj9PrimaryLocalCode;
    }

    public String getStj10PrimaryLocalCode() {
        return stj10PrimaryLocalCode;
    }

    public void setStj10PrimaryLocalCode(String stj10PrimaryLocalCode) {
        this.stj10PrimaryLocalCode = stj10PrimaryLocalCode;
    }

    public String getStj6SecondaryLocalCode() {
        return stj6SecondaryLocalCode;
    }

    public void setStj6SecondaryLocalCode(String stj6SecondaryLocalCode) {
        this.stj6SecondaryLocalCode = stj6SecondaryLocalCode;
    }

    public String getStj7SecondaryLocalCode() {
        return stj7SecondaryLocalCode;
    }

    public void setStj7SecondaryLocalCode(String stj7SecondaryLocalCode) {
        this.stj7SecondaryLocalCode = stj7SecondaryLocalCode;
    }

    public String getStj8SecondaryLocalCode() {
        return stj8SecondaryLocalCode;
    }

    public void setStj8SecondaryLocalCode(String stj8SecondaryLocalCode) {
        this.stj8SecondaryLocalCode = stj8SecondaryLocalCode;
    }

    public String getStj9SecondaryLocalCode() {
        return stj9SecondaryLocalCode;
    }

    public void setStj9SecondaryLocalCode(String stj9SecondaryLocalCode) {
        this.stj9SecondaryLocalCode = stj9SecondaryLocalCode;
    }

    public String getStj10SecondaryLocalCode() {
        return stj10SecondaryLocalCode;
    }

    public void setStj10SecondaryLocalCode(String stj10SecondaryLocalCode) {
        this.stj10SecondaryLocalCode = stj10SecondaryLocalCode;
    }

    public String getFrtIncItem() {
        return frtIncItem;
    }

    public void setFrtIncItem(String frtIncItem) {
        this.frtIncItem = frtIncItem;
    }

    public String getDiscIncItem() {
        return discIncItem;
    }

    public void setDiscIncItem(String discIncItem) {
        this.discIncItem = discIncItem;
    }


    public BigDecimal getInvoiceGoodAmt() {
        return invoiceGoodAmt;
    }

    public void setInvoiceGoodAmt(BigDecimal invoiceGoodAmt) {
        this.invoiceGoodAmt = invoiceGoodAmt;
    }

    public BigDecimal getInvoiceServiceAmt() {
        return invoiceServiceAmt;
    }

    public void setInvoiceServiceAmt(BigDecimal invoiceServiceAmt) {
        this.invoiceServiceAmt = invoiceServiceAmt;
    }

    public BigDecimal getInvoiceGoodCnt() {
        return invoiceGoodCnt;
    }

    public void setInvoiceGoodCnt(BigDecimal invoiceGoodCnt) {
        this.invoiceGoodCnt = invoiceGoodCnt;
    }

    public BigDecimal getInvoiceServiceCnt() {
        return invoiceServiceCnt;
    }

    public void setInvoiceServiceCnt(BigDecimal invoiceServiceCnt) {
        this.invoiceServiceCnt = invoiceServiceCnt;
    }

    public BigDecimal getInvoiceGoodWgt() {
        return invoiceGoodWgt;
    }

    public void setInvoiceGoodWgt(BigDecimal invoiceGoodWgt) {
        this.invoiceGoodWgt = invoiceGoodWgt;
    }

    public BigDecimal getInvoiceServiceWgt() {
        return invoiceServiceWgt;
    }

    public void setInvoiceServiceWgt(BigDecimal invoiceServiceWgt) {
        this.invoiceServiceWgt = invoiceServiceWgt;
    }

    public Boolean getLocalWrite() {
        return localWrite;
    }

    public void setLocalWrite(Boolean localWrite) {
        this.localWrite = localWrite;
    }

    public Boolean getStopProcessing() {
        return stopProcessing;
    }

    public void setStopProcessing(Boolean stopProcessing) {
        this.stopProcessing = stopProcessing;
    }

    public TransactionLog getTransactionLog() {
        return transactionLog;
    }

    public void setTransactionLog(TransactionLog transactionLog) {
        this.transactionLog = transactionLog;
    }

    public Boolean getLocalCountyAmtSet() {
        return localCountyAmtSet;
    }

    public void setLocalCountyAmtSet(Boolean localCountyAmtSet) {
        this.localCountyAmtSet = localCountyAmtSet;
    }

    public Boolean getLocalCityAmtSet() {
        return localCityAmtSet;
    }

    public void setLocalCityAmtSet(Boolean localCityAmtSet) {
        this.localCityAmtSet = localCityAmtSet;
    }

    public BigDecimal getCountrySubTotalTaxAmt() {
        return countrySubTotalTaxAmt;
    }

    public void setCountrySubTotalTaxAmt(BigDecimal countrySubTotalTaxAmt) {
        this.countrySubTotalTaxAmt = countrySubTotalTaxAmt;
    }

    public BigDecimal getStateSubTotalTaxAmt() {
        return stateSubTotalTaxAmt;
    }

    public void setStateSubTotalTaxAmt(BigDecimal stateSubTotalTaxAmt) {
        this.stateSubTotalTaxAmt = stateSubTotalTaxAmt;
    }

    public BigDecimal getCountySubTotalTaxAmt() {
        return countySubTotalTaxAmt;
    }

    public void setCountySubTotalTaxAmt(BigDecimal countySubTotalTaxAmt) {
        this.countySubTotalTaxAmt = countySubTotalTaxAmt;
    }

    public BigDecimal getCitySubTotalTaxAmt() {
        return citySubTotalTaxAmt;
    }

    public void setCitySubTotalTaxAmt(BigDecimal citySubTotalTaxAmt) {
        this.citySubTotalTaxAmt = citySubTotalTaxAmt;
    }

    public BigDecimal getStj1SubTotalTaxAmt() {
        return stj1SubTotalTaxAmt;
    }

    public void setStj1SubTotalTaxAmt(BigDecimal stj1SubTotalTaxAmt) {
        this.stj1SubTotalTaxAmt = stj1SubTotalTaxAmt;
    }

    public BigDecimal getStj2SubTotalTaxAmt() {
        return stj2SubTotalTaxAmt;
    }

    public void setStj2SubTotalTaxAmt(BigDecimal stj2SubTotalTaxAmt) {
        this.stj2SubTotalTaxAmt = stj2SubTotalTaxAmt;
    }

    public BigDecimal getStj3SubTotalTaxAmt() {
        return stj3SubTotalTaxAmt;
    }

    public void setStj3SubTotalTaxAmt(BigDecimal stj3SubTotalTaxAmt) {
        this.stj3SubTotalTaxAmt = stj3SubTotalTaxAmt;
    }

    public BigDecimal getStj4SubTotalTaxAmt() {
        return stj4SubTotalTaxAmt;
    }

    public void setStj4SubTotalTaxAmt(BigDecimal stj4SubTotalTaxAmt) {
        this.stj4SubTotalTaxAmt = stj4SubTotalTaxAmt;
    }

    public BigDecimal getStj5SubTotalTaxAmt() {
        return stj5SubTotalTaxAmt;
    }

    public void setStj5SubTotalTaxAmt(BigDecimal stj5SubTotalTaxAmt) {
        this.stj5SubTotalTaxAmt = stj5SubTotalTaxAmt;
    }

    public BigDecimal getStj6SubTotalTaxAmt() {
        return stj6SubTotalTaxAmt;
    }

    public void setStj6SubTotalTaxAmt(BigDecimal stj6SubTotalTaxAmt) {
        this.stj6SubTotalTaxAmt = stj6SubTotalTaxAmt;
    }

    public BigDecimal getStj7SubTotalTaxAmt() {
        return stj7SubTotalTaxAmt;
    }

    public void setStj7SubTotalTaxAmt(BigDecimal stj7SubTotalTaxAmt) {
        this.stj7SubTotalTaxAmt = stj7SubTotalTaxAmt;
    }

    public BigDecimal getStj8SubTotalTaxAmt() {
        return stj8SubTotalTaxAmt;
    }

    public void setStj8SubTotalTaxAmt(BigDecimal stj8SubTotalTaxAmt) {
        this.stj8SubTotalTaxAmt = stj8SubTotalTaxAmt;
    }

    public BigDecimal getStj9SubTotalTaxAmt() {
        return stj9SubTotalTaxAmt;
    }

    public void setStj9SubTotalTaxAmt(BigDecimal stj9SubTotalTaxAmt) {
        this.stj9SubTotalTaxAmt = stj9SubTotalTaxAmt;
    }

    public BigDecimal getStj10SubTotalTaxAmt() {
        return stj10SubTotalTaxAmt;
    }

    public void setStj10SubTotalTaxAmt(BigDecimal stj10SubTotalTaxAmt) {
        this.stj10SubTotalTaxAmt = stj10SubTotalTaxAmt;
    }


	public Boolean getSuspendCodeFlag() {
		return suspendCodeFlag;
	}


	public void setSuspendCodeFlag(Boolean suspendCodeFlag) {
		this.suspendCodeFlag = suspendCodeFlag;
	}
    
    
}
