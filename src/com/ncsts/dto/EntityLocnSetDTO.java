package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

public class EntityLocnSetDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long entityLocnSetId;
	private Long entityId;
	private String locnSetCode;
    private String activeFlag; 
	private String name;
    private String updateUserId;
    private Date updateTimestamp; 
        

	public EntityLocnSetDTO() {   	
    }

	public Long getEntityLocnSetId() {
		return entityLocnSetId;
	}

	public void setEntityLocnSetId(Long entityLocnSetId) {
		this.entityLocnSetId = entityLocnSetId;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getLocnSetCode() {
		return locnSetCode;
	}

	public void setLocnSetCode(String locnSetCode) {
		this.locnSetCode = locnSetCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }  
    
    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}