package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class UserSecurityDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer userId;
    private String userName;   
    private String password;      
    private String lastUpdateDate; 
    private String createDate; 
    private Integer roleId;
    private String roleName; 
    
    private Integer currentAttempts;  
    private Integer lock; 
    private Integer change;
    
    private String email; 
    
    public Integer getCurrentAttempts() {
		return currentAttempts;
	}

	public void setCurrentAttempts(Integer currentAttempts) {
		this.currentAttempts = currentAttempts;
	}
	
	public Integer getLock() {
		return lock;
	}

	public void setLock(Integer lock) {
		this.lock = lock;
	}
	
	public Integer getChange() {
		return change;
	}

	public void setChange(Integer change) {
		this.change = change;
	}

    public UserSecurityDTO() { 	
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("userId :\t").append(userId);
    	sb.append("userName :\t").append(userName);
    	sb.append("password :\t").append(password);

    	return sb.toString();
    }
    
}







