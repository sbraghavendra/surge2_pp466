package com.ncsts.dto;


import java.io.Serializable;
import java.util.Date;

public class SitusDriverSitusDriverDetailDTO implements Serializable {

    private Long situsDriverId;
    private String transactionTypeCode;
    private String ratetypeCode;
    private String methodDeliveryCode;
    private String customFlag;

    private String locationTypeCode;
    private Long driverId;
    private String mandatoryFlag;
    private String customMandatoryFlag;
    private String updateUserId;
    private Date updateTimestamp;


    public Long getSitusDriverId() {
        return situsDriverId;

    }


    public void setSitusDriverId(Long situsDriverId) {
        this.situsDriverId = situsDriverId;
    }

    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public String getRatetypeCode() {
        return ratetypeCode;
    }

    public void setRatetypeCode(String ratetypeCode) {
        this.ratetypeCode = ratetypeCode;
    }

    public String getMethodDeliveryCode() {
        return methodDeliveryCode;
    }

    public void setMethodDeliveryCode(String methodDeliveryCode) {
        this.methodDeliveryCode = methodDeliveryCode;
    }

    public String getCustomFlag() {
        return customFlag;
    }

    public void setCustomFlag(String customFlag) {
        this.customFlag = customFlag;
    }

    public String getLocationTypeCode() {
        return locationTypeCode;
    }

    public void setLocationTypeCode(String locationTypeCode) {
        this.locationTypeCode = locationTypeCode;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getMandatoryFlag() {
        return mandatoryFlag;
    }

    public void setMandatoryFlag(String mandatoryFlag) {
        this.mandatoryFlag = mandatoryFlag;
    }

    public String getCustomMandatoryFlag() {
        return customMandatoryFlag;
    }

    public void setCustomMandatoryFlag(String customMandatoryFlag) {
        this.customMandatoryFlag = customMandatoryFlag;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }
}
