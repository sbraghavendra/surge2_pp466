/**
 * 
 */
package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.ncsts.domain.DriverReference;
import com.ncsts.domain.DriverReferencePK;

/**
 * @author Anand
 *
 */
public class DriverReferenceDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String transDtlColName;
	private String driverValue;
	private String driverDesc;
	private String userValue;
	private String updateUserId;
	private Date updateTimestamp;
	private String transDtlColDesc;
	
	public DriverReferenceDTO() {
		super();
	}
	
	public DriverReferenceDTO(DriverReferenceDTO driverReferenceDTO) {
		BeanUtils.copyProperties(driverReferenceDTO, this);
	}
	
	public String getTransDtlColName() {
		return transDtlColName;
	}
	public void setTransDtlColName(String transDetailColName) {
		this.transDtlColName = transDetailColName;
	}
	public String getDriverValue() {
		return driverValue;
	}
	public void setDriverValue(String driverValue) {
		this.driverValue = driverValue;
	}
	public String getDriverDesc() {
		return driverDesc;
	}
	public void setDriverDesc(String driverDesc) {
		this.driverDesc = driverDesc;
	}
	public String getUserValue() {
		return userValue;
	}
	public void setUserValue(String userValue) {
		this.userValue = userValue;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	public void setUpdateTimestamp(Date lastUpdateTimeStamp) {
		this.updateTimestamp = lastUpdateTimeStamp;
	}
	public String getTransDtlColDesc() {
		return transDtlColDesc;
	}
	public void setTransDtlColDesc(String transDtlColDesc) {
		this.transDtlColDesc = transDtlColDesc;
	}

	public DriverReference getDriverReference() {
		DriverReference driverReference = new DriverReference(new DriverReferencePK(transDtlColName, driverValue));
		BeanUtils.copyProperties(this, driverReference);
		return driverReference;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";
	    
	    String retValue = "";
	    
	    retValue = "DriverReferenceDTO ( "
	        + super.toString() + TAB
	        + "transDtlColName = " + this.transDtlColName + TAB
	        + "driverValue = " + this.driverValue + TAB
	        + "driverDesc = " + this.driverDesc + TAB
	        + "userValue = " + this.userValue + TAB
	        + "updateUserId = " + this.updateUserId + TAB
	        + "updateTimestamp = " + this.updateTimestamp + TAB
	        + "transDtlColDesc = " + this.transDtlColDesc + TAB
	        + " )";
	
	    return retValue;
	}
	
}
