package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Paul Govindan
 *
 */

public class EntityItemDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
		
    private Long entityId;
       
    private String entityCode;  
       
    private String entityName;  
        
    private Long entityLevelId;  
    private String entityLevel;
    private String entityColumn;
    private String entityLevelDescription;
    private Long parentLevel;//Entity level of child
       
    private Long parentEntityId;
    private String parentCode;
    private String parentName;
    private String parentDescription;
    private Long parentEntityLevel;//Entity level of parent
    private String parentLevelDescription;
    private String parentActiveFlag;
    
    private String updateUserId;
    private Date updateTimestamp; 
        
    private String fein;      
    private String description;    
    private Long jurisdictionId;      
    private String geocode;      
    private String addressLine1;    
    private String addressLine2;     
    private String city; 
    private String county;     
    private String state;     
    private String zip;     
    private String zipPlus4;     
    private String country;    
    private String lockFlag; 
    private String activeFlag; 
    private Long exemptGraceDays;
    
    public Long getParentLevel() {
		return parentLevel;
	}

	public String getParentCode() {
		return parentCode;
	}

	public String getParentName() {
		return parentName;
	}
	
	public String getParentDescription() {
		return parentDescription;
	}

	public void setParentLevel(Long parentLevel) {
		this.parentLevel = parentLevel;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	
	public void setParentDescription(String parentDescription) {
		this.parentDescription = parentDescription;
	}

	public String getEntityLevel() {
		return entityLevel;
	}

	public String getEntityColumn() {
		return entityColumn;
	}

	public void setEntityLevel(String entityLevel) {
		this.entityLevel = entityLevel;
	}

	public void setEntityColumn(String entityColumn) {
		this.entityColumn = entityColumn;
	}

	public EntityItemDTO() { 
    	
    }
    
    public Long getEntityId() {
        return this.entityId;
    }
    
    public void setEntityId(Long entityId) {
    	this.entityId = entityId;
    }
    
    public String getEntityCode() {
        return this.entityCode;
    }
    public boolean isEntityCodeAll() {
    	return "*ALL".equalsIgnoreCase(this.entityCode);
    }
    
    public void setEntityCode(String entityCode) {
    	this.entityCode = entityCode;
    }    
    
    public String getEntityName() {
        return this.entityName;
    }
    
    public void setEntityName(String entityName) {
    	this.entityName = entityName;
    }      
    
    public Long getEntityLevelId() {
        return this.entityLevelId;
    }
    
    public void setEntityLevelId(Long entityLevelId) {
    	this.entityLevelId = entityLevelId;
    }    
    
    public Long getParentEntityId() {
        return this.parentEntityId;
    }
    
    public void setParentEntityId(Long parentEntityId) {
    	this.parentEntityId = parentEntityId;
    }         
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public String getEntityLevelDescription() {
		return entityLevelDescription;
	}

	public void setEntityLevelDescription(String entityLevelDescription) {
		this.entityLevelDescription = entityLevelDescription;
	}

	public String getParentLevelDescription() {
		return parentLevelDescription;
	}

	public void setParentLevelDescription(String parentLevelDescription) {
		this.parentLevelDescription = parentLevelDescription;
	}

	public Long getParentEntityLevel() {
		return parentEntityLevel;
	}

	public void setParentEntityLevel(Long parentEntityLevel) {
		this.parentEntityLevel = parentEntityLevel;
	}    
	
	public String getFein() {
        return this.fein;
    }
    
    public void setFein(String fein) {
    	this.fein = fein;
    }   
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
    	this.description = description;
    }  
    
    public Long getJurisdictionId() {
        return this.jurisdictionId;
    }
    
    public void setJurisdictionId(Long jurisdictionId) {
    	this.jurisdictionId = jurisdictionId;
    }   
    
    public String getGeocode() {
        return this.geocode;
    }
    
    public void setGeocode(String geocode) {
    	this.geocode = geocode;
    }  
    
    public String getAddressLine1() {
        return this.addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
    	this.addressLine1 = addressLine1;
    }  
    
    public String getAddressLine2() {
        return this.addressLine2;
    }
    
    public void setAddressLine2(String addressLine2) {
    	this.addressLine2 = addressLine2;
    }  
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
    	this.city = city;
    }  
    
    public String getCounty() {
        return this.county;
    }
    
    public void setCounty(String county) {
    	this.county = county;
    }  
    
    public String getState() {
        return this.state;
    }
    
    public void setState(String state) {
    	this.state = state;
    }  
    
    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
    	this.zip = zip;
    }  
    
    public String getZipPlus4() {
        return this.zipPlus4;
    }
    
    public void setZipPlus4(String zipPlus4) {
    	this.zipPlus4 = zipPlus4;
    }  
    
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
    	this.country = country;
    }  
    
    public String getLockFlag() {
        return this.lockFlag;
    }
    
    public void setLockFlag(String lockFlag) {
    	this.lockFlag = lockFlag;
    }  
    
    public String getActiveFlag() {
        return this.activeFlag;
    }
    
    public void setActiveFlag(String activeFlag) {
    	this.activeFlag = activeFlag;
    }
    
    public String getParentActiveFlag() {
        return this.parentActiveFlag;
    }
    
    public void setParentActiveFlag(String parentActiveFlag) {
    	this.parentActiveFlag = parentActiveFlag;
    }
	
    public Boolean getLockBooleanFlag() {
    	return "1".equals(this.lockFlag);
	}

	public void setLockBooleanFlag(Boolean lockFlag) {
		this.lockFlag = lockFlag == Boolean.TRUE ? "1" : "0";
	}
	
    public Boolean getActiveBooleanFlag() {
    	return "1".equals(this.activeFlag);
	}

	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag == Boolean.TRUE ? "1" : "0";
	}
    
	public Long getExemptGraceDays() {
        return this.exemptGraceDays;
    }
    
    public void setExemptGraceDays(Long exemptGraceDays) {
    	this.exemptGraceDays = exemptGraceDays;
    }
}






