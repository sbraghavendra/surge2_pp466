package com.ncsts.dto;

import com.ncsts.domain.OptionCodePK;

import java.util.HashSet;

public class PurchaseTransactionOptions {
    public static final OptionCodePK PK_RATEPOINT_ENABLED = new OptionCodePK("RATEPOINTENABLED","ADMIN","ADMIN");
    public static final OptionCodePK PK_RATEPOINT_SERVER = new OptionCodePK("RATEPOINTSERVER","ADMIN","ADMIN");
    public static final OptionCodePK PK_RATEPOINT_USER_ID = new OptionCodePK("RATEPOINTUSERID","ADMIN","ADMIN");
    public static final OptionCodePK PK_RATEPOINT_PASSWORD = new OptionCodePK("RATEPOINTPASSWORD","ADMIN","ADMIN");
    public static final OptionCodePK PK_RATEPOINT_USELATLONLOOKUP = new OptionCodePK("LATLONGLOOKUP","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_RATEPOINT_USEGENERICJURAPI = new OptionCodePK("GENERICJURAPI","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_RATEPOINT_LATLONLOOKUP_URL = new OptionCodePK("LATLONLOOKUPURL","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_RATEPOINT_GENERICJURAPI_URL = new OptionCodePK("GENERICJURAPIURL","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_RATEPOINT_STREETLEVELRATE_URL = new OptionCodePK("STREETLEVELURL","SYSTEM","SYSTEM");

    public static final OptionCodePK PK_LOCATIONPREFERENCE = new OptionCodePK("LOCATIONPREFERENCE","SYSTEM","SYSTEM");

    public static final OptionCodePK PK_AUTOPROCFLAG = new OptionCodePK("AUTOPROCFLAG","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_AUTOPROCAMT = new OptionCodePK("AUTOPROCAMT","SYSTEM","SYSTEM");
    public static final OptionCodePK PK_AUTOPROCID = new OptionCodePK("AUTOPROCID","SYSTEM","SYSTEM");

    public static final OptionCodePK PK_HOLDTRANSFLAG = new OptionCodePK("HOLDTRANSFLAG", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_HOLDTRANSAMT = new OptionCodePK("HOLDTRANSAMT", "SYSTEM", "SYSTEM");

    public static final OptionCodePK PK_DELETE0AMT = new OptionCodePK("DELETE0AMT", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_PROCRULEENABLED = new OptionCodePK("PROCRULEENABLED", "ADMIN", "ADMIN");

    public static final OptionCodePK PK_DEFAULTRATETYPE = new OptionCodePK("DEFAULTRATETYPE", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_DEFAULTMOD = new OptionCodePK("DEFAULTMOD", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_DEFAULTGEORECON = new OptionCodePK("DEFAULTGEORECON", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_DEFAULTFRTINCITEM = new OptionCodePK("DEFAULTFRTINCITEM", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_DEFAULTDISCINCITEM = new OptionCodePK("DEFAULTDISCINCITEM", "SYSTEM", "SYSTEM");
    public static final OptionCodePK PK_DEFAULTTRANSTYPE = new OptionCodePK("DEFAULTTRANSTYPE", "SYSTEM", "SYSTEM");

}
