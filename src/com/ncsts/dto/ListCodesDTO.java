package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

import com.ncsts.domain.ListCodesPK;
/**
 * 
 * @author Muneer
 *
 */
public class ListCodesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private ListCodesPK listCodesPK;
	private String description;
    private String message;
    private String explanation;
    private String severityLevel;
    private String writeImportLineFlag;
    private String abortImportFlag;
    private String updateUserId;
    private Date updateTimestamp;  
    
    private String codeCode;
    
    private boolean writeImportLineFlagDisplay;
    private boolean abortImportFlagDisplay;
    private boolean messageSelect;
    private boolean explanationSelect;
    private boolean severityLevelSelect;
    private boolean writeImportLineFlagSelect;
    private boolean abortImportFlagSelect;
    
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public String getSeverityLevel() {
		return severityLevel;
	}
	public void setSeverityLevel(String severityLevel) {
		this.severityLevel = severityLevel;
	}
	public String getWriteImportLineFlag() {
		return writeImportLineFlag;
	}
	public void setWriteImportLineFlag(String writeImportLineFlag) {
		this.writeImportLineFlag = writeImportLineFlag;
	}
	public String getAbortImportFlag() {
		return abortImportFlag;
	}
	public void setAbortImportFlag(String abortImportFlag) {
		this.abortImportFlag = abortImportFlag;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	public String getCodeTypeCode() {
		return listCodesPK.getCodeTypeCode();
	}
	
	public String getCodeCode() {
		return codeCode;
	}

	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}
	
	public ListCodesPK getListCodesPK() {
		return listCodesPK;
	}
	public void setListCodesPK(ListCodesPK listCodesPK) {
		this.listCodesPK = listCodesPK;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getWriteImportLineFlagDisplay() {
		if (this.writeImportLineFlag!=null && this.writeImportLineFlag.equalsIgnoreCase("1")) {this.writeImportLineFlagDisplay = true;}
		else{this.writeImportLineFlagDisplay = false;}
		
		return writeImportLineFlagDisplay;
	}
	
	public void setWriteImportLineFlagDisplay(boolean writeImportLineFlagDisplay) {
		this.writeImportLineFlagDisplay = writeImportLineFlagDisplay;
		if (writeImportLineFlagDisplay) {this.writeImportLineFlag = "1";} 
		else {this.writeImportLineFlag = "0";}
	}
	
	public boolean getAbortImportFlagDisplay() {
		if (this.abortImportFlag!=null && this.abortImportFlag.equalsIgnoreCase("1")) {this.abortImportFlagDisplay = true;}
		else{this.abortImportFlagDisplay = false;}
		
		return abortImportFlagDisplay;
	}
	
	public void setAbortImportFlagDisplay(boolean abortImportFlagDisplay) {
		this.abortImportFlagDisplay = abortImportFlagDisplay;
		if (abortImportFlagDisplay) {this.abortImportFlag = "1";} 
		else {this.abortImportFlag = "0";}
	}
	
	public boolean getMessageSelect() {
		if (this.message!=null && this.message.equalsIgnoreCase("1")) {this.messageSelect = true;}
		else{this.messageSelect = false;}
		
		return messageSelect;
	}
	
	public void setMessageSelect(boolean messageSelect) {
		this.messageSelect = messageSelect;
		if (messageSelect) {this.message = "1";} 
		else {this.message = "0";}
	}
	
	public boolean getExplanationSelect() {
		if (this.explanation!=null && this.explanation.equalsIgnoreCase("1")) {this.explanationSelect = true;}
		else{this.explanationSelect = false;}
		
		return explanationSelect;
	}
	
	public void setExplanationSelect(boolean explanationSelect) {
		this.explanationSelect = explanationSelect;
		if (explanationSelect) {this.explanation = "1";} 
		else {this.explanation = "0";}
	}
	
	public boolean getSeverityLevelSelect() {
		if (this.severityLevel!=null && this.severityLevel.equalsIgnoreCase("1")) {this.severityLevelSelect = true;}
		else{this.severityLevelSelect = false;}
		
		return severityLevelSelect;
	}
	
	public void setSeverityLevelSelect(boolean severityLevelSelect) {
		this.severityLevelSelect = severityLevelSelect;
		if (severityLevelSelect) {this.severityLevel = "1";} 
		else {this.severityLevel = "0";}
	}
	
	public boolean getWriteImportLineFlagSelect() {
		if (this.writeImportLineFlag!=null && this.writeImportLineFlag.equalsIgnoreCase("1")) {this.writeImportLineFlagSelect = true;}
		else{this.writeImportLineFlagSelect = false;}
		
		return writeImportLineFlagSelect;
	}
	
	public void setWriteImportLineFlagSelect(boolean writeImportLineFlagSelect) {
		this.writeImportLineFlagSelect = writeImportLineFlagSelect;
		if (writeImportLineFlagSelect) {this.writeImportLineFlag = "1";} 
		else {this.writeImportLineFlag = "0";}
	}
	
	public boolean getAbortImportFlagSelect() {
		if (this.abortImportFlag!=null && this.abortImportFlag.equalsIgnoreCase("1")) {this.abortImportFlagSelect = true;}
		else{this.abortImportFlagSelect = false;}
		
		return abortImportFlagSelect;
	}
	
	public void setAbortImportFlagSelect(boolean abortImportFlagSelect) {
		this.abortImportFlagSelect = abortImportFlagSelect;
		if (abortImportFlagSelect) {this.abortImportFlag = "1";} 
		else {this.abortImportFlag = "0";}
	}
	
	public boolean isDEF(){
		return listCodesPK.getCodeTypeCode().equalsIgnoreCase("*DEF");
	}
	
	public boolean getDisableMessage() {
		if (isDEF() && this.message!=null && this.message.equalsIgnoreCase("1")) {return false;}
		else{return true;}
	}
	
	public boolean getDisableExplanation() {
		if (isDEF() && this.explanation!=null && this.explanation.equalsIgnoreCase("1")) {return false;}
		else{return true;}
	}
	
	public boolean getDisableSeverityLevel() {
		if (isDEF() && this.severityLevel!=null && this.severityLevel.equalsIgnoreCase("1")) {return false;}
		else{return true;}
	}
	
	public boolean getDisableWriteImportLineFlag() {
		if (isDEF() && this.writeImportLineFlag!=null && this.writeImportLineFlag.equalsIgnoreCase("1")) {return false;}
		else{return true;}
	}
	
	public boolean getDisableAbortImportFlag() {
		if (isDEF() && this.abortImportFlag!=null && this.abortImportFlag.equalsIgnoreCase("1")) {return false;}
		else{return true;}
	}
}

