package com.ncsts.dto;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.ncsts.domain.TempStatistics;


public class DatabaseTransactionStatisticsDTO {

	// Fields
	private String title;
	private Long count;
	
	private Double countPct;
	private Double sum;
	private Double sumPct;
	private Double taxAmt;
	private Double max;
	private Double avg;
	
	//for drill down
	private Long distCount;
	private Double distAmt;
	private Double maxDistAmt;
	private Double avgDistAmt;
	
	private String whereClause;
	private String groupByOnDrilldown;
	private String updateUserId;
	private Date updateTimestamp;
	private Double effTaxRate;
	
	private List<String> dynamicCols;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public Double getCountPct() {
		return countPct;
	}
	public void setCountPct(Double countPct) {
		this.countPct = countPct;
	}
	public Double getSum() {
		return sum;
	}
	public void setSum(Double sum) {
		this.sum = sum;
	}
	public Double getSumPct() {
		return sumPct;
	}
	public void setSumPct(Double sumPct) {
		this.sumPct = sumPct;
	}
	public Double getTaxAmt() {
		return taxAmt;
	}
	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}
	public Double getMax() {
		return max;
	}
	public void setMax(Double max) {
		this.max = max;
	}
	public Double getAvg() {
		return avg;
	}
	public void setAvg(Double avg) {
		this.avg = avg;
	}
	public String getWhereClause() {
		return whereClause;
	}
	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}
	public String getGroupByOnDrilldown() {
		return groupByOnDrilldown;
	}
	public void setGroupByOnDrilldown(String groupByOnDrilldown) {
		this.groupByOnDrilldown = groupByOnDrilldown;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}
	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	public Double getEffTaxRate() {
		return effTaxRate;
	}
	public void setEffTaxRate(Double effTaxRate) {
		this.effTaxRate = effTaxRate;
	}
	public Long getDistCount() {
		return distCount;
	}
	public Double getDistAmt() {
		return distAmt;
	}
	public Double getMaxDistAmt() {
		return maxDistAmt;
	}
	public Double getAvgDistAmt() {
		return avgDistAmt;
	}
	public void setDistCount(Long distCount) {
		this.distCount = distCount;
	}
	public void setDistAmt(Double distAmt) {
		this.distAmt = distAmt;
	}
	public void setMaxDistAmt(Double maxDistAmt) {
		this.maxDistAmt = maxDistAmt;
	}
	public void setAvgDistAmt(Double avgDistAmt) {
		this.avgDistAmt = avgDistAmt;
	}

	public void setDynamicCols(List<String> dynamicCols) {
		this.dynamicCols = dynamicCols;
	}
	public List<String> getDynamicCols() {
		return dynamicCols;
	}
	
	public TempStatistics getTempStatistics() {
		TempStatistics tempStatistics = new TempStatistics();
    	BeanUtils.copyProperties(this,tempStatistics);
    	return tempStatistics;
    }
}
