package com.ncsts.dto;

import com.ncsts.domain.TempTaxMatrixCount;
/*
 * Midtier project code modified - january 2009
 */
public class TaxMatrixTransactionStatiticsDTO {
	
	private String total_matrix_lines;
	private String active_matrix_lines;
	private String distinct_matrix_lines;
	private TempTaxMatrixCount tempTaxMatrixCount;
	
	public String getTotal_matrix_lines() {
		return total_matrix_lines;
	}

	public void setTotal_matrix_lines(String total_matrix_lines) {
		this.total_matrix_lines = total_matrix_lines;
	}

		public String getActive_matrix_lines() {
		return active_matrix_lines;
	}

	public void setActive_matrix_lines(String active_matrix_lines) {
		this.active_matrix_lines = active_matrix_lines;
	}

	public String getDistinct_matrix_lines() {
		return distinct_matrix_lines;
	}

	public void setDistinct_matrix_lines(String distinct_matrix_lines) {
		this.distinct_matrix_lines = distinct_matrix_lines;
	}

	public TempTaxMatrixCount getTempTaxMatrixCount() {
		return tempTaxMatrixCount;
	}

	public void setTempTaxMatrixCount(TempTaxMatrixCount tempTaxMatrixCount) {
		this.tempTaxMatrixCount = tempTaxMatrixCount;
	}

}
