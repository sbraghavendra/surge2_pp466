package com.ncsts.dto;

import com.ncsts.domain.BatchMetadata;
import org.apache.commons.beanutils.BeanUtils;

import java.util.Date;

/**
 * Created by RC05200 on 2/17/2016.
 */


public class BatchMetadataDTO  {

    private String batchTypeCode;
    private String vc01Desc;
    private String vc02Desc;
    private String vc03Desc;
    private String vc04Desc;
    private String vc05Desc;
    private String vc06Desc;
    private String vc07Desc;
    private String vc08Desc;
    private String vc09Desc;
    private String vc10Desc;
    private String nu01Desc;
    private String nu02Desc;
    private String nu03Desc;
    private String nu04Desc;
    private String nu05Desc;
    private String nu06Desc;
    private String nu07Desc;
    private String nu08Desc;
    private String nu09Desc;
    private String nu10Desc;
    private String ts01Desc;
    private String ts02Desc;
    private String ts03Desc;
    private String ts04Desc;
    private String ts05Desc;
    private String ts06Desc;
    private String ts07Desc;
    private String ts08Desc;
    private String ts09Desc;
    private String ts10Desc;

    private String uvc01Desc;
    private String uvc02Desc;
    private String uvc03Desc;
    private String uvc04Desc;
    private String uvc05Desc;
    private String uvc06Desc;
    private String uvc07Desc;
    private String uvc08Desc;
    private String uvc09Desc;
    private String uvc10Desc;
    private String unu01Desc;
    private String unu02Desc;
    private String unu03Desc;
    private String unu04Desc;
    private String unu05Desc;
    private String unu06Desc;
    private String unu07Desc;
    private String unu08Desc;
    private String unu09Desc;
    private String unu10Desc;
    private String uts01Desc;
    private String uts02Desc;
    private String uts03Desc;
    private String uts04Desc;
    private String uts05Desc;
    private String uts06Desc;
    private String uts07Desc;
    private String uts08Desc;
    private String uts09Desc;
    private String uts10Desc;

    private String updateUserId;
    private Date updateTimestamp;



    /** default constructor */
    public BatchMetadataDTO() {
    }

    /** minimal constructor */
    public BatchMetadataDTO(String batchTypeCode) {
        this.batchTypeCode = batchTypeCode;
    }

    public String getBatchTypeCode() {
        return batchTypeCode;
    }

    public void setBatchTypeCode(String batchTypeCode) {
        this.batchTypeCode = batchTypeCode;
    }

    public String getVc01Desc() {
        return vc01Desc;
    }

    public void setVc01Desc(String vc01Desc) {
        this.vc01Desc = vc01Desc;
    }

    public String getVc02Desc() {
        return vc02Desc;
    }

    public void setVc02Desc(String vc02Desc) {
        this.vc02Desc = vc02Desc;
    }

    public String getVc03Desc() {
        return vc03Desc;
    }

    public void setVc03Desc(String vc03Desc) {
        this.vc03Desc = vc03Desc;
    }

    public String getVc04Desc() {
        return vc04Desc;
    }

    public void setVc04Desc(String vc04Desc) {
        this.vc04Desc = vc04Desc;
    }

    public String getVc05Desc() {
        return vc05Desc;
    }

    public void setVc05Desc(String vc05Desc) {
        this.vc05Desc = vc05Desc;
    }

    public String getVc06Desc() {
        return vc06Desc;
    }

    public void setVc06Desc(String vc06Desc) {
        this.vc06Desc = vc06Desc;
    }

    public String getVc07Desc() {
        return vc07Desc;
    }

    public void setVc07Desc(String vc07Desc) {
        this.vc07Desc = vc07Desc;
    }

    public String getVc08Desc() {
        return vc08Desc;
    }

    public void setVc08Desc(String vc08Desc) {
        this.vc08Desc = vc08Desc;
    }

    public String getVc09Desc() {
        return vc09Desc;
    }

    public void setVc09Desc(String vc09Desc) {
        this.vc09Desc = vc09Desc;
    }

    public String getVc10Desc() {
        return vc10Desc;
    }

    public void setVc10Desc(String vc10Desc) {
        this.vc10Desc = vc10Desc;
    }

    public String getNu01Desc() {
        return nu01Desc;
    }

    public void setNu01Desc(String nu01Desc) {
        this.nu01Desc = nu01Desc;
    }

    public String getNu02Desc() {
        return nu02Desc;
    }

    public void setNu02Desc(String nu02Desc) {
        this.nu02Desc = nu02Desc;
    }

    public String getNu03Desc() {
        return nu03Desc;
    }

    public void setNu03Desc(String nu03Desc) {
        this.nu03Desc = nu03Desc;
    }

    public String getNu04Desc() {
        return nu04Desc;
    }

    public void setNu04Desc(String nu04Desc) {
        this.nu04Desc = nu04Desc;
    }

    public String getNu05Desc() {
        return nu05Desc;
    }

    public void setNu05Desc(String nu05Desc) {
        this.nu05Desc = nu05Desc;
    }

    public String getNu06Desc() {
        return nu06Desc;
    }

    public void setNu06Desc(String nu06Desc) {
        this.nu06Desc = nu06Desc;
    }

    public String getNu07Desc() {
        return nu07Desc;
    }

    public void setNu07Desc(String nu07Desc) {
        this.nu07Desc = nu07Desc;
    }

    public String getNu08Desc() {
        return nu08Desc;
    }

    public void setNu08Desc(String nu08Desc) {
        this.nu08Desc = nu08Desc;
    }

    public String getNu09Desc() {
        return nu09Desc;
    }

    public void setNu09Desc(String nu09Desc) {
        this.nu09Desc = nu09Desc;
    }

    public String getNu10Desc() {
        return nu10Desc;
    }

    public void setNu10Desc(String nu10Desc) {
        this.nu10Desc = nu10Desc;
    }

    public String getTs01Desc() {
        return ts01Desc;
    }

    public void setTs01Desc(String ts01Desc) {
        this.ts01Desc = ts01Desc;
    }

    public String getTs02Desc() {
        return ts02Desc;
    }

    public void setTs02Desc(String ts02Desc) {
        this.ts02Desc = ts02Desc;
    }

    public String getTs03Desc() {
        return ts03Desc;
    }

    public void setTs03Desc(String ts03Desc) {
        this.ts03Desc = ts03Desc;
    }

    public String getTs04Desc() {
        return ts04Desc;
    }

    public void setTs04Desc(String ts04Desc) {
        this.ts04Desc = ts04Desc;
    }

    public String getTs05Desc() {
        return ts05Desc;
    }

    public void setTs05Desc(String ts05Desc) {
        this.ts05Desc = ts05Desc;
    }

    public String getTs06Desc() {
        return ts06Desc;
    }

    public void setTs06Desc(String ts06Desc) {
        this.ts06Desc = ts06Desc;
    }

    public String getTs07Desc() {
        return ts07Desc;
    }

    public void setTs07Desc(String ts07Desc) {
        this.ts07Desc = ts07Desc;
    }

    public String getTs08Desc() {
        return ts08Desc;
    }

    public void setTs08Desc(String ts08Desc) {
        this.ts08Desc = ts08Desc;
    }

    public String getTs09Desc() {
        return ts09Desc;
    }

    public void setTs09Desc(String ts09Desc) {
        this.ts09Desc = ts09Desc;
    }

    public String getTs10Desc() {
        return ts10Desc;
    }

    public void setTs10Desc(String ts10Desc) {
        this.ts10Desc = ts10Desc;
    }

    public String getUvc01Desc() {
        return uvc01Desc;
    }

    public void setUvc01Desc(String uvc01Desc) {
        this.uvc01Desc = uvc01Desc;
    }

    public String getUvc02Desc() {
        return uvc02Desc;
    }

    public void setUvc02Desc(String uvc02Desc) {
        this.uvc02Desc = uvc02Desc;
    }

    public String getUvc03Desc() {
        return uvc03Desc;
    }

    public void setUvc03Desc(String uvc03Desc) {
        this.uvc03Desc = uvc03Desc;
    }

    public String getUvc04Desc() {
        return uvc04Desc;
    }

    public void setUvc04Desc(String uvc04Desc) {
        this.uvc04Desc = uvc04Desc;
    }

    public String getUvc05Desc() {
        return uvc05Desc;
    }

    public void setUvc05Desc(String uvc05Desc) {
        this.uvc05Desc = uvc05Desc;
    }

    public String getUvc06Desc() {
        return uvc06Desc;
    }

    public void setUvc06Desc(String uvc06Desc) {
        this.uvc06Desc = uvc06Desc;
    }

    public String getUvc07Desc() {
        return uvc07Desc;
    }

    public void setUvc07Desc(String uvc07Desc) {
        this.uvc07Desc = uvc07Desc;
    }

    public String getUvc08Desc() {
        return uvc08Desc;
    }

    public void setUvc08Desc(String uvc08Desc) {
        this.uvc08Desc = uvc08Desc;
    }

    public String getUvc09Desc() {
        return uvc09Desc;
    }

    public void setUvc09Desc(String uvc09Desc) {
        this.uvc09Desc = uvc09Desc;
    }

    public String getUvc10Desc() {
        return uvc10Desc;
    }

    public void setUvc10Desc(String uvc10Desc) {
        this.uvc10Desc = uvc10Desc;
    }

    public String getUnu01Desc() {
        return unu01Desc;
    }

    public void setUnu01Desc(String unu01Desc) {
        this.unu01Desc = unu01Desc;
    }

    public String getUnu02Desc() {
        return unu02Desc;
    }

    public void setUnu02Desc(String unu02Desc) {
        this.unu02Desc = unu02Desc;
    }

    public String getUnu03Desc() {
        return unu03Desc;
    }

    public void setUnu03Desc(String unu03Desc) {
        this.unu03Desc = unu03Desc;
    }

    public String getUnu04Desc() {
        return unu04Desc;
    }

    public void setUnu04Desc(String unu04Desc) {
        this.unu04Desc = unu04Desc;
    }

    public String getUnu05Desc() {
        return unu05Desc;
    }

    public void setUnu05Desc(String unu05Desc) {
        this.unu05Desc = unu05Desc;
    }

    public String getUnu06Desc() {
        return unu06Desc;
    }

    public void setUnu06Desc(String unu06Desc) {
        this.unu06Desc = unu06Desc;
    }

    public String getUnu07Desc() {
        return unu07Desc;
    }

    public void setUnu07Desc(String unu07Desc) {
        this.unu07Desc = unu07Desc;
    }

    public String getUnu08Desc() {
        return unu08Desc;
    }

    public void setUnu08Desc(String unu08Desc) {
        this.unu08Desc = unu08Desc;
    }

    public String getUnu09Desc() {
        return unu09Desc;
    }

    public void setUnu09Desc(String unu09Desc) {
        this.unu09Desc = unu09Desc;
    }

    public String getUnu10Desc() {
        return unu10Desc;
    }

    public void setUnu10Desc(String unu10Desc) {
        this.unu10Desc = unu10Desc;
    }

    public String getUts01Desc() {
        return uts01Desc;
    }

    public void setUts01Desc(String uts01Desc) {
        this.uts01Desc = uts01Desc;
    }

    public String getUts02Desc() {
        return uts02Desc;
    }

    public void setUts02Desc(String uts02Desc) {
        this.uts02Desc = uts02Desc;
    }

    public String getUts03Desc() {
        return uts03Desc;
    }

    public void setUts03Desc(String uts03Desc) {
        this.uts03Desc = uts03Desc;
    }

    public String getUts04Desc() {
        return uts04Desc;
    }

    public void setUts04Desc(String uts04Desc) {
        this.uts04Desc = uts04Desc;
    }

    public String getUts05Desc() {
        return uts05Desc;
    }

    public void setUts05Desc(String uts05Desc) {
        this.uts05Desc = uts05Desc;
    }

    public String getUts06Desc() {
        return uts06Desc;
    }

    public void setUts06Desc(String uts06Desc) {
        this.uts06Desc = uts06Desc;
    }

    public String getUts07Desc() {
        return uts07Desc;
    }

    public void setUts07Desc(String uts07Desc) {
        this.uts07Desc = uts07Desc;
    }

    public String getUts08Desc() {
        return uts08Desc;
    }

    public void setUts08Desc(String uts08Desc) {
        this.uts08Desc = uts08Desc;
    }

    public String getUts09Desc() {
        return uts09Desc;
    }

    public void setUts09Desc(String uts09Desc) {
        this.uts09Desc = uts09Desc;
    }

    public String getUts10Desc() {
        return uts10Desc;
    }

    public void setUts10Desc(String uts10Desc) {
        this.uts10Desc = uts10Desc;
    }


    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }



    public static BatchMetadataDTO createCopyFrom(BatchMetadata batchMetadata) {
        BatchMetadataDTO result = new BatchMetadataDTO();
        try {
            BeanUtils.copyProperties(result, batchMetadata);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    public static BatchMetadata createBatchMetadata(BatchMetadataDTO batchMetadataDTO){
        BatchMetadata batchMetadata = new BatchMetadata();
        try {
            BeanUtils.copyProperties(batchMetadata, batchMetadataDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return batchMetadata;
    }

}
