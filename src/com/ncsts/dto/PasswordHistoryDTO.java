package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


public class PasswordHistoryDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String lastSuccessfulLogin;   
    private String passwordHistory; 
    private Integer userId;  

    public PasswordHistoryDTO() { 
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastSuccessfulLogin() {
		return lastSuccessfulLogin;
	}
	
	public void setLastSuccessfulLogin(String lastSuccessfulLogin) {
		this.lastSuccessfulLogin = lastSuccessfulLogin;
	}

	public String getPasswordHistory() {
		return passwordHistory;
	}

	public void setPasswordHistory(String passwordHistory) {
		this.passwordHistory = passwordHistory;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("id :\t").append(id);
    	sb.append("lastSuccessfulLogin :\t").append(lastSuccessfulLogin);
    	sb.append("passwordHistory :\t").append(passwordHistory);

    	return sb.toString();
    }
    
}







