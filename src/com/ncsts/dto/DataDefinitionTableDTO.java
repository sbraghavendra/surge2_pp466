package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Muneer
 *
 */
public class DataDefinitionTableDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String tableName;
    private String description;
    private String definition;
    private String updateUserId;
    private Date updateTimestamp;
    private String configFileTypeCode;


    private String importFlag;
    private String exportFlag;
    private String importInsertSql;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public String getConfigFileTypeCode() {
        return configFileTypeCode;
    }

    public void setConfigFileTypeCode(String configFileTypeCode) {
        this.configFileTypeCode = configFileTypeCode;
    }

    public String getImportInsertSql() {
        return importInsertSql;
    }

    public void setImportInsertSql(String importInsertSql) {
        this.importInsertSql = importInsertSql;
    }


    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DataDefinitionTableDTO{");
        sb.append("tableName='").append(tableName).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", definition='").append(definition).append('\'');
        sb.append(", updateUserId='").append(updateUserId).append('\'');
        sb.append(", updateTimestamp=").append(updateTimestamp);
        sb.append(", configFileTypeCode='").append(configFileTypeCode).append('\'');
        sb.append(", importFlag='").append(importFlag).append('\'');
        sb.append(", exportFlag='").append(exportFlag).append('\'');
        sb.append(", importInsertSql='").append(importInsertSql).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

