package com.ncsts.dto;

import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.HashSet;
import java.util.Set;

import static com.ncsts.ws.PinPointWebServiceConstants.*;

@XmlRootElement(namespace= MESSAGES_NAMESPACE)
@XmlType(namespace= MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TaxCodeRuleGroupBy {
    private String state;
    private String taxCode;
    private String groupByColumnName;
    private String groupByColumnValue;

    public TaxCodeRuleGroupBy() {

    }

    public TaxCodeRuleGroupBy(String state, String taxCode, String groupByColumnName, String groupByColumnValue) {
        this.state = state;
        this.taxCode = taxCode;
        this.groupByColumnName = groupByColumnName;
        this.groupByColumnValue = groupByColumnValue;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getGroupByColumnName() {
        return groupByColumnName;
    }

    public void setGroupByColumnName(String groupByColumnName) {
        this.groupByColumnName = groupByColumnName;
    }

    public String getGroupByColumnValue() {
        return groupByColumnValue;
    }

    public void setGroupByColumnValue(String groupByColumnValue) {
        this.groupByColumnValue = groupByColumnValue;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxCodeRuleGroupBy that = (TaxCodeRuleGroupBy) o;

        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (taxCode != null ? !taxCode.equals(that.taxCode) : that.taxCode != null) return false;
        if (groupByColumnName != null ? !groupByColumnName.equals(that.groupByColumnName) : that.groupByColumnName != null)
            return false;
        return groupByColumnValue != null ? groupByColumnValue.equals(that.groupByColumnValue) : that.groupByColumnValue == null;

    }

    @Override
    public int hashCode() {
        int result = state != null ? state.hashCode() : "00".hashCode();
        result = 31 ^ result + (taxCode != null ? taxCode.hashCode() : 0);
        result = 31 ^ result + (groupByColumnName != null ? groupByColumnName.hashCode() : 0);
        result = 31 ^ result + (groupByColumnValue != null ? groupByColumnValue.hashCode() : 0);
        return result;
    }


    public boolean transactionMatches(PurchaseTransaction trans) {
        if (trans == null) return false;
        boolean result = false;
        //if (trans.stateCode)


        return result;
    }


    public static void main(String[] args) {



    }



}
