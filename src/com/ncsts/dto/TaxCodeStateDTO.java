package com.ncsts.dto;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;
/**
 * 
 * @author Anand
 *
 */

@XmlRootElement(namespace= PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TaxCodeStateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String taxCodeState;
	
	private String geoCode;
	
	private String name;
	
	private String country;
	
	private String localTaxabilityCode;
	
    private String activeFlag;
    
    private String activeTempFlag;
    
    private String customFlag;
    
    private String modifiedFlag;

	private String updateUserId;
	
	private Date updateTimestamp;
	
	private String localTaxabilityCodeDesc;
	
	private String cpFormId;
	
	private String stateTaxabilityFlag;
	
	private String cpCertLevelCode;
	private String cpCertLevelCodeDesc;
	
	public String getStateCountryKey() {
		return taxCodeState+"-"+country;
	}
	
	public String getTaxCodeState() {
		return taxCodeState;
	}

	public void setTaxCodeState(String taxCodeState) {
		this.taxCodeState = taxCodeState;
	}

	public String getGeoCode() {
		return geoCode;
	}

	public void setGeoCode(String geoCode) {
		this.geoCode = geoCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public Boolean getActiveBooleanFlag() {
		return (activeFlag == null)? null:activeFlag.equals("1");
	}
	
	public void setActiveBooleanFlag(Boolean activeFlag) {
		this.activeFlag = (activeFlag == null)? null:((activeFlag)? "1":"0");
	}
	
	public String getActiveTempFlag() {
		return activeTempFlag;
	}

	public void setActiveTempFlag(String activeTempFlag) {
		this.activeTempFlag = activeTempFlag;
	}
	
	public Boolean getActiveTempBooleanFlag() {
		return (activeTempFlag == null)? null:activeTempFlag.equals("1");
	}
	
	public void setActiveTempBooleanFlag(Boolean activeTempFlag) {
		this.activeTempFlag = (activeTempFlag == null)? null:((activeTempFlag)? "1":"0");
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public String getLocalTaxabilityCode() {
		return localTaxabilityCode;
	}

	public void setLocalTaxabilityCode(String localTaxabilityCode) {
		this.localTaxabilityCode = localTaxabilityCode;
	}

	public String getLocalTaxabilityCodeDesc() {
		return localTaxabilityCodeDesc;
	}

	public void setLocalTaxabilityCodeDesc(String localTaxabilityCodeDesc) {
		this.localTaxabilityCodeDesc = localTaxabilityCodeDesc;
	}

	public String getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(String customFlag) {
		this.customFlag = customFlag;
	}

	public String getModifiedFlag() {
		return modifiedFlag;
	}

	public void setModifiedFlag(String modifiedFlag) {
		this.modifiedFlag = modifiedFlag;
	}

	public String getCpFormId() {
		return cpFormId;
	}

	public void setCpFormId(String cpFormid) {
		this.cpFormId = cpFormid;
	}

	public String getStateTaxabilityFlag() {
		return stateTaxabilityFlag; 
	}
	
	public void setStateTaxabilityFlag(String stateTaxabilityFlag) {
		this.stateTaxabilityFlag = stateTaxabilityFlag; 
	}
	
	
	
	public Boolean getStateTaxabilityBooleanFlag() {
		
		return (this.stateTaxabilityFlag == null)? null:this.stateTaxabilityFlag.equals("1");
	}

	public void setStateTaxabilityBooleanFlag(Boolean stateTaxabilityFlag) {
		this.stateTaxabilityFlag = (stateTaxabilityFlag == null)? null:((stateTaxabilityFlag)? "1":"0");
	}

	public String getCpCertLevelCode() {
		return cpCertLevelCode;
	}

	public void setCpCertLevelCode(String cpCertLevelCode) {
		this.cpCertLevelCode = cpCertLevelCode;
	}
	
	public String getCpCertLevelCodeDesc() {
		return cpCertLevelCodeDesc;
	}

	public void setCpCertLevelCodeDesc(String cpCertLevelCodeDesc) {
		this.cpCertLevelCodeDesc = cpCertLevelCodeDesc;
	}
	

	
}
