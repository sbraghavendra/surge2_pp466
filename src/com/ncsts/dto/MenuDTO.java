package com.ncsts.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Paul Govindan
 *
 */

public class MenuDTO implements Serializable {
	
	private static final long serialVersionUID = 1318470152133640111L;	
	
    private String menuCode;
       
    private String mainMenuCode;  
       
    private Long menuSequence;  
       
    private String optionName;  
        
    private String commandType;    
        
    private String commandLine;     
       
    private String keyValues; 
       
    private String panelClassName;    
    
    private String updateUserId;
	
    private Date updateTimestamp;  
    
    //for UserMenuEx
    private String exceptionType;
    
    public MenuDTO() { 
    	
    }
    
    public MenuDTO(String menuCode,String optionName) { 
    	setMenuCode(menuCode);
    	setOptionName(optionName);
    }
    
    public String getMenuCode() {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode) {
    	this.menuCode = menuCode;
    }
    
    public String getMainMenuCode() {
        return this.mainMenuCode;
    }
    
    public void setMainMenuCode(String mainMenuCode) {
    	this.mainMenuCode = mainMenuCode;
    }    
    
    public Long getMenuSequence() {
        return this.menuSequence;
    }
    
    public void setMenuSequence(Long menuSequence) {
    	this.menuSequence = menuSequence;
    }      
    
    public String getOptionName() {
        return this.optionName;
    }
    
    public void setOptionName(String optionName) {
    	this.optionName = optionName;
    }    
    
    public String getCommandType() {
        return this.commandType;
    }
    
    public void setCommandType(String commandType) {
    	this.commandType = commandType;
    }    
    
    public String getCommandLine() {
        return this.commandLine;
    }
    
    public void setCommandLine(String commandLine) {
    	this.commandLine = commandLine;
    }    
    
    public String getKeyValues() {
        return this.keyValues;
    }
    
    public void setKeyValues(String keyValues) {
    	this.keyValues = keyValues;
    }     
    
    public String getPanelClassName() {
        return this.panelClassName;
    }
    
    public void setPanelClassName(String panelClassName) {
    	this.panelClassName = panelClassName;
    }     
    
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}    
	
	/**
	 * 
	 */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if ((other == null) || !(other instanceof MenuDTO)) {
            return false;
        }

        final MenuDTO revision = (MenuDTO)other;

        String c1 = getMenuCode();
        String c2 = revision.getMenuCode();
        if (!((c1 == c2) || (c1 != null && c1.equals(c2)))) {
            return false;
        }

        return true;
    }
	
	 /**
     * 
     */
    public int hashCode() {
		int hash = 7;
        String c1 = getMenuCode();
		hash = 31 * hash + (null == c1 ? 0 : c1.hashCode());        
		return hash;
    }

    /**
     * 
     * @return a string representation of this object.
     */
    public String toString() {
        return this.getClass()
                   .getName() + "::" + this.menuCode + "::" + this.optionName + "::" + this.exceptionType;
    }
    
}




