package com.ncsts.dto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public enum FlagValue {
	All(null),
	True("1"),
	False("0");
	
	private String flag;
	
	private static List<SelectItem> flagMenuItems;
	
	static {
		flagMenuItems = new ArrayList<SelectItem>();
		for (FlagValue flagValue : FlagValue.values()) {
			flagMenuItems.add(new SelectItem(flagValue.toString(), flagValue.toString()));
		}
	}

	FlagValue(String flag) {
		this.flag = flag;
	}

	public String getFlag() {
		return flag;
	}
	
	public static List<SelectItem> getFlagMenuItems() {
		return flagMenuItems;
	}

}
