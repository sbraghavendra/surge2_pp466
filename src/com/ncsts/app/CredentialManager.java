package com.ncsts.app;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import com.ncsts.domain.License;
import com.ncsts.view.util.DesEncrypter;

public class CredentialManager {

	private JFrame frmCredentialManager;
	
	private ButtonGroup groupLicenseAction;
	private ButtonGroup groupLicenseType;
	
	private JTextField textCompanyName;
	private JTextField textExpirationDate;
	private JTextField textLicenseKey;
	
	private JRadioButton rdbtnCreate;
	private JRadioButton rdbtnView;
	
	private JRadioButton rdbtnPurchase;
	private JRadioButton rdbtnSale;
	private JRadioButton rdbtnBoth;
	
	private SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	private JTextField textPassword;
	private JTextField textEncryptedPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CredentialManager window = new CredentialManager();
					window.frmCredentialManager.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CredentialManager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCredentialManager = new JFrame();
		frmCredentialManager.setResizable(false);
		frmCredentialManager.setTitle("Credential Manager");
		frmCredentialManager.setBounds(100, 100, 465, 286);
		frmCredentialManager.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCredentialManager.setLocationRelativeTo(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmCredentialManager.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelLicense = new JPanel();
		tabbedPane.addTab("License", null, panelLicense, null);
		panelLicense.setLayout(null);
		
		JLabel lblPinpointLicense = new JLabel("PinPoint License Key");
		lblPinpointLicense.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPinpointLicense.setBounds(10, 11, 196, 30);
		panelLicense.add(lblPinpointLicense);
		
		rdbtnCreate = new JRadioButton("Create");
		rdbtnCreate.setBounds(6, 48, 78, 23);
		rdbtnCreate.setSelected(true);
		panelLicense.add(rdbtnCreate);
		
		rdbtnView = new JRadioButton("View");
		rdbtnView.setBounds(86, 48, 109, 23);
		panelLicense.add(rdbtnView);
		
		groupLicenseAction = new ButtonGroup();
		groupLicenseAction.add(rdbtnCreate);
		groupLicenseAction.add(rdbtnView);
		
		JLabel lblCompanyName = new JLabel("Company Name");
		lblCompanyName.setBounds(10, 78, 114, 14);
		panelLicense.add(lblCompanyName);
		
		textCompanyName = new JTextField();
		textCompanyName.setBounds(141, 75, 274, 20);
		panelLicense.add(textCompanyName);
		textCompanyName.setColumns(10);
		
		JLabel lblLicenseType = new JLabel("License Type");
		lblLicenseType.setBounds(10, 103, 114, 14);
		panelLicense.add(lblLicenseType);
		
		rdbtnPurchase = new JRadioButton("Purchase");
		rdbtnPurchase.setBounds(141, 99, 84, 23);
		panelLicense.add(rdbtnPurchase);
		
		rdbtnSale = new JRadioButton("Sale");
		rdbtnSale.setBounds(235, 99, 62, 23);
		panelLicense.add(rdbtnSale);
		
		rdbtnBoth = new JRadioButton("Both");
		rdbtnBoth.setBounds(306, 99, 54, 23);
		rdbtnBoth.setSelected(true);
		panelLicense.add(rdbtnBoth);
		
		groupLicenseType = new ButtonGroup();
		groupLicenseType.add(rdbtnPurchase);
		groupLicenseType.add(rdbtnSale);
		groupLicenseType.add(rdbtnBoth);
		
		JLabel lblExpirationDate = new JLabel("Expiration Date");
		lblExpirationDate.setBounds(10, 128, 114, 14);
		panelLicense.add(lblExpirationDate);
		
		textExpirationDate = new JTextField();
		textExpirationDate.setBounds(141, 125, 126, 20);
		panelLicense.add(textExpirationDate);
		textExpirationDate.setColumns(10);
		
		JLabel lblmmddyyyy = new JLabel("(mm/dd/yyyy)");
		lblmmddyyyy.setBounds(277, 128, 109, 14);
		panelLicense.add(lblmmddyyyy);
		
		JLabel lblLicenseKey = new JLabel("License Key");
		lblLicenseKey.setBounds(10, 153, 114, 14);
		panelLicense.add(lblLicenseKey);
		
		textLicenseKey = new JTextField();
		textLicenseKey.setBounds(141, 150, 196, 20);
		panelLicense.add(textLicenseKey);
		textLicenseKey.setColumns(10);
		
		JButton btnClear = new JButton("Clear");
		btnClear.setBounds(141, 181, 89, 23);
		panelLicense.add(btnClear);
		
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				rdbtnCreate.setSelected(true);
				textCompanyName.setText("");
				rdbtnBoth.setSelected(true);
				textExpirationDate.setText("");
				textLicenseKey.setText("");
			}
		});
		
		JButton btnGo = new JButton("Go");
		btnGo.setBounds(235, 181, 89, 23);
		panelLicense.add(btnGo);
		
		JButton btnCopy = new JButton("Copy");
		btnCopy.setBounds(347, 149, 68, 23);
		panelLicense.add(btnCopy);
		
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(rdbtnCreate.isSelected()) {
					if(!hasText(textCompanyName.getText())) {
						JOptionPane.showMessageDialog(frmCredentialManager, "Company Name is required");
						textCompanyName.requestFocusInWindow();
						return;
					}
					
					if(hasText(textExpirationDate.getText())) {
						Date date = null;
						try {
							date = format.parse(textExpirationDate.getText());
						}
						catch(Exception e) {
							e.printStackTrace();
						}
						
						if(date == null) {
							JOptionPane.showMessageDialog(frmCredentialManager, "Invalid Expiry Date");
							textExpirationDate.requestFocusInWindow();
							return;
						}
					}
					
					textLicenseKey.setText("");
					
					String licenseType = "";
					if(rdbtnPurchase.isSelected()) {
						licenseType = "1";
					}
					else if(rdbtnSale.isSelected()) {
						licenseType = "2";
					}
					else if(rdbtnBoth.isSelected()) {
						licenseType = "3";
					}
					
					String key = "";
					try {
						key = License.encrypt(textCompanyName.getText(), licenseType, textExpirationDate.getText());
					}
					catch(Exception e) {
						JOptionPane.showMessageDialog(frmCredentialManager, "Error: " + e.getMessage());
						return;
					}
					
					textLicenseKey.setText(key);
				}
				else {
					if(!hasText(textLicenseKey.getText())) {
						JOptionPane.showMessageDialog(frmCredentialManager, "License Key is required");
						textLicenseKey.requestFocusInWindow();
						return;
					}
					
					textCompanyName.setText("");
					textExpirationDate.setText("");
					rdbtnBoth.setSelected(false);
					rdbtnPurchase.setSelected(false);
					rdbtnSale.setSelected(false);
					
					try {
						License lic = new License(textLicenseKey.getText());
						if(lic.isValidLicense()) {
							textCompanyName.setText(lic.getCompany());
							if(lic.getExpiration() != null) {
								textExpirationDate.setText(format.format(lic.getExpiration()));
							}
							if(lic.hasPurchaseLicense() && lic.hasSaleLicense()) {
								rdbtnBoth.setSelected(true);
							}
							else if(lic.hasPurchaseLicense()) {
								rdbtnPurchase.setSelected(true);
							}
							else if(lic.hasSaleLicense()) {
								rdbtnSale.setSelected(true);
							}
							else {
								rdbtnBoth.setSelected(false);
								rdbtnPurchase.setSelected(false);
								rdbtnSale.setSelected(false);
							}
						}
						else {
							JOptionPane.showMessageDialog(frmCredentialManager, "Invalid License Key");
						}
					}
					catch(Exception e) {
						JOptionPane.showMessageDialog(frmCredentialManager, "Invalid License Key");
						e.printStackTrace();
					}
				}
			}
		});
		
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(!hasText(textLicenseKey.getText())) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Nothing to copy");
				}
				else {
					StringSelection selection = new StringSelection(textLicenseKey.getText());
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(selection, selection);
					JOptionPane.showMessageDialog(frmCredentialManager, "License Key copied to clipboard");
				}
			}
		});
		
		JPanel panelPassword = new JPanel();
		tabbedPane.addTab("Password", null, panelPassword, null);
		panelPassword.setLayout(null);
		
		JLabel lblPinpointPasswordEndecrypter = new JLabel("PinPoint Password \"En/De\"crypter");
		lblPinpointPasswordEndecrypter.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPinpointPasswordEndecrypter.setBounds(10, 11, 283, 30);
		panelPassword.add(lblPinpointPasswordEndecrypter);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 66, 129, 14);
		panelPassword.add(lblPassword);
		
		textPassword = new JTextField();
		textPassword.setBounds(149, 66, 206, 20);
		panelPassword.add(textPassword);
		textPassword.setColumns(10);
		
		JLabel lblEncryptedPassword = new JLabel("Encrypted Password");
		lblEncryptedPassword.setBounds(10, 100, 129, 14);
		panelPassword.add(lblEncryptedPassword);
		
		textEncryptedPassword = new JTextField();
		textEncryptedPassword.setBounds(149, 97, 206, 20);
		panelPassword.add(textEncryptedPassword);
		textEncryptedPassword.setColumns(10);
		
		JButton btnPasswordClear = new JButton("Clear");
		btnPasswordClear.setBounds(68, 139, 89, 23);
		panelPassword.add(btnPasswordClear);
		
		btnPasswordClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textPassword.setText("");
				textEncryptedPassword.setText("");
			}
		});
		
		JButton btnEncrypt = new JButton("Encrypt");
		btnEncrypt.setBounds(167, 139, 89, 23);
		panelPassword.add(btnEncrypt);
		
		btnEncrypt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!hasText(textPassword.getText())) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Password is required");
					textPassword.requestFocusInWindow();
					return;
				}
				
				textEncryptedPassword.setText("");
				try {
					DesEncrypter enc = DesEncrypter.getInstance();
					textEncryptedPassword.setText(enc.encrypt(textPassword.getText()));
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Error: " + ex.getMessage());
				}
			}
		});
		
		JButton btnDecrypt = new JButton("Decrypt");
		btnDecrypt.setBounds(266, 139, 89, 23);
		panelPassword.add(btnDecrypt);
		
		JButton btnPasswordCopy = new JButton("Copy");
		btnPasswordCopy.setBounds(365, 62, 71, 23);
		panelPassword.add(btnPasswordCopy);
		
		JButton btnEncPasswordCopy = new JButton("Copy");
		btnEncPasswordCopy.setBounds(365, 96, 71, 23);
		panelPassword.add(btnEncPasswordCopy);
		
		btnDecrypt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!hasText(textEncryptedPassword.getText())) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Encrypted Password is required");
					textEncryptedPassword.requestFocusInWindow();
					return;
				}
				
				textPassword.setText("");
				try {
					DesEncrypter enc = DesEncrypter.getInstance();
					textPassword.setText(enc.decrypt(textEncryptedPassword.getText()));
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Error: " + ex.getMessage());
				}
			}
		});
		
		btnPasswordCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(!hasText(textPassword.getText())) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Nothing to copy");
				}
				else {
					StringSelection selection = new StringSelection(textPassword.getText());
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(selection, selection);
					JOptionPane.showMessageDialog(frmCredentialManager, "Password copied to clipboard");
				}
			}
		});
		
		btnEncPasswordCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(!hasText(textEncryptedPassword.getText())) {
					JOptionPane.showMessageDialog(frmCredentialManager, "Nothing to copy");
				}
				else {
					StringSelection selection = new StringSelection(textEncryptedPassword.getText());
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(selection, selection);
					JOptionPane.showMessageDialog(frmCredentialManager, "Encrypted Password copied to clipboard");
				}
			}
		});
	}
	
	private boolean hasText(String s) {
		return s != null && s.trim().length() > 0;
	}
}
