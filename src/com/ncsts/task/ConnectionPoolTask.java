package com.ncsts.task;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import com.ncsts.view.bean.RoutingDataSource;

public class ConnectionPoolTask {
	private static final Logger logger = Logger.getLogger(ConnectionPoolTask.class);
	
	@Autowired
    private RoutingDataSource dataSource;
	
	private StdScheduler connectionPoolScheduler;	
	private SimpleTrigger connectionPoolTrigger;
	private boolean initialized = false;
   
	public ConnectionPoolTask(){   
	}
   
	public StdScheduler getConnectionPoolScheduler() {
		return connectionPoolScheduler;
	}

	public void setConnectionPoolScheduler(StdScheduler connectionPoolScheduler) {
		this.connectionPoolScheduler = connectionPoolScheduler;
	}

	public SimpleTrigger getConnectionPoolTrigger() {
		return connectionPoolTrigger;
	}

	public void setConnectionPoolTrigger(SimpleTrigger connectionPoolTrigger) {
		this.connectionPoolTrigger = connectionPoolTrigger;
	}

	public void process() {
		if(!initialized) {
			long defaultMinutes = 45;//in minutes			
			String repeatStr = System.getProperty("repeat.interval");
			if(repeatStr!=null && repeatStr.trim().length()>0){
				logger.info("-Drepeat.interval=" + System.getProperty("repeat.interval"));
				try{
					long temp = Long.valueOf(repeatStr.trim());
					if(temp>0){
						defaultMinutes = temp;
					}
				}
				catch(Exception e){}
			}
					
			long repeatInterval = defaultMinutes * 60 * 1000;//Refresh every defaultMinutes minutes = defaultMinutes * 60 * 1000
			reschedule(repeatInterval);
			this.initialized = true;
			return;
		}
		
		poll();
	}
   
	public void poll() {		
		refresh();
	}
   
	public void reschedule(long repeatInterval) {
		SimpleTrigger newTrigger = new SimpleTrigger();
		newTrigger.setRepeatInterval(repeatInterval);
		newTrigger.setName(connectionPoolTrigger.getName());
		newTrigger.setJobName(connectionPoolTrigger.getJobName());
		newTrigger.setStartTime(new Date());
		newTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);

		// Re-schedule Job with new trigger
		try {
			Date date = connectionPoolScheduler.rescheduleJob(connectionPoolTrigger.getName(), connectionPoolTrigger.getGroup(), newTrigger);
			connectionPoolTrigger = newTrigger;
			logger.info("ConnectionPoolTask re-scheduled: repeatInterval - " + repeatInterval + " milliseconds, " + date);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
   
	public void refresh() {
		dataSource.connectionValidation();
	}
}
