package com.ncsts.task;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ncsts.domain.*;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdScheduler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ncsts.app.CredentialManager;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.BCPCustLocnDAO;
import com.ncsts.dao.BatchDAO;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.dao.BcpSaleTransactionExDAO;
import com.ncsts.dao.CustDAO;
import com.ncsts.dao.CustLocnDAO;
import com.ncsts.dao.EntityItemDAO;
import com.ncsts.dao.SaleTransactionExDAO;
import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.CustLocnBatch;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.dto.DatabaseTransactionStatisticsDTO;
import com.ncsts.services.DatabaseStatisticsService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.services.SaleTransactionLogService;
import com.ncsts.view.bean.ImportMapAddBean;
import com.seconddecimal.billing.domain.SaleTransaction;
import com.seconddecimal.billing.domain.SaleTransactionDocument;
import com.seconddecimal.billing.domain.SaleTransactionLog;
import com.seconddecimal.billing.dto.SaleTransactionLogUtil;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;
import com.seconddecimal.billing.service.SaleTransactionProcess;
import com.seconddecimal.billing.util.ArithmeticUtils;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.domain.BCPBillTransaction;


@Service
public class BatchProcessTask {
	private static final Logger logger = Logger.getLogger(BatchProcessTask.class);
	private static final String BATCH_TYPE_SALE = "IPS";
	private static final String BATCH_STATUS_SALE_FLAGGED_FOR_PROCESS = "FP";
	private static final String BATCH_STATUS_SALE_PROCESSING = "XPS";
	private static final String BATCH_STATUS_SALE_FLAGGED_FOR_DELETE = "FD";
	private static final String BATCH_STATUS_SALE_DELETING = "XD";
	private static final String BATCH_STATUS_SALE_DELETED = "D";
	private static final String BATCH_PROCESS_TYPE_SALE = "PS";
	private static final String BATCH_TYPE_PURCHASING = "IP";
	private static final String BATCH_TYPE_PURCHASING_COMPANY = "PCO";
	private static final String BATCH_STATUS_FLAGGED_FOR_IMPORT = "FI";
	private static final String BATCH_TYPE_TAXRATE = "TR";
	private static final String BATCH_TYPE_FLAGFORPROCESS = "FP";
	private static final String BATCH_STATUS_DELETE_ABORTED = "ABD";
	
	private static final int FETCH_SIZE = 1000;
	
	@Autowired
	private OptionService optionService;
	
	@Autowired
	private BatchMaintenanceDAO batchMaintenanceDAO;
	
	@Autowired
	private BcpSaleTransactionExDAO bcpSaleTransactionExDAO;
	
	@Autowired
	private SaleTransactionExDAO saleTransactionExDAO;
	
	@Autowired
	private SaleTransactionProcess saleTransactionProcess;
	
	@Autowired
	private ListCodesService listCodesService;
	
	@Autowired
	private DatabaseStatisticsService databaseStatisticsService;
	
	@Autowired
	private BCPCustLocnDAO bcpCustLocnDAO;
	
	@Autowired
	private CustDAO custDAO;
	
	@Autowired
	private CustLocnDAO custLocnDAO;
	
	@Autowired
	private EntityItemDAO entityItemDAO;
	
	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	@Autowired
	private BatchDAO batchDAO;
	@Autowired
	private SaleTransactionLogService saleTransactionLogService;	private StdScheduler batchProcessScheduler;	
	private SimpleTrigger batchProcessTrigger;
	
	private boolean initialized = false;

	private long currentRepeatInterval = 0;

	//Call this only from scheduler. Not from any other place to avoid
	//concurrent execution
	public void process() {
		if(!initialized) {
			init();
			return;
		}
		
		long repeatInterval = getRepeatIntervalFromPreference();
		if(repeatInterval==0){
			if(repeatInterval==currentRepeatInterval){
				//No change, default interval 30 * 1000
				return;
			}
			else{
				logger.info("Batch process stopped. Run every 30 * 1000 milliseconds for scheduler: " + new Date().toString());
				reschedule(30*1000);
				currentRepeatInterval=0;		
				return;
			}
		}
		else{
			if(repeatInterval!=currentRepeatInterval){
				//Change to new interval
				reschedule(repeatInterval);
				currentRepeatInterval=repeatInterval;
				return;
			}
		}

		poll();
	}
	
	public void poll() {

		logger.debug("process called at " + new Date());
		
		if(logger.isDebugEnabled()) {
			logger.debug(batchMaintenanceDAO.getDBProperties());
		}
		if(!batchMaintenanceDAO.checkRunningBatches()) {
		//Process Sale Batch
		List<BatchMaintenance> batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_SALE, BATCH_STATUS_SALE_FLAGGED_FOR_PROCESS);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					try {
						purchaseTransactionService.getBillTransactionProcessBatch(b, "1", LogSourceEnum.BillTransactionProcessBatch);
					}
					catch(PurchaseTransactionProcessingException e) {
					}

					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for sale process");
		}
		
		//PP-134, Implement Transactions Batch Processing 
		//Process Transaction Batch
		batches = batchMaintenanceDAO.findFlaggedBatches("IP", BATCH_TYPE_FLAGFORPROCESS);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					try {
						purchaseTransactionService.getPurchaseTransactionProcessBatch(b, "1");
					}
					catch(PurchaseTransactionProcessingException e) {
					}

					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for transaction process");
		}	

		//Process Customer import
		BatchMaintenance bm = new BatchMaintenance();
		bm.setBatchTypeCode(CustLocnBatch.BATCH_CODE);
		bm.setBatchStatusCode(CustLocnBatch.PROCESS);
		
		batches = batchMaintenanceDAO.findByExample(bm, 0);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					Date now = new Date();
					Date schedTime = b.getSchedStartTimestamp();
					if(schedTime == null || schedTime.getTime() <= now.getTime()) {
						logger.info(batchMaintenanceDAO.getDBProperties());
						logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
						processCustomerBatch(b);
						break; //process only one a time
					}
				}
			}
		}
		else {
			logger.debug("No customer batch available for process");
		}
		
		//Delete Customer Batch
		batches = batchMaintenanceDAO.findFlaggedBatches(CustLocnBatch.BATCH_CODE, CustLocnBatch.FLAGGED_FOR_DELETE);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					deleteCustomerBatch(b);
					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for process");
		}
		
		//Delete Sale Batch
		batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_SALE, BATCH_STATUS_SALE_FLAGGED_FOR_DELETE);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					deleteSaleBatch(b);
					break; //process only one a time
					
//					//Test Code Start
//					System.out.println("Found batch to delete");
//					System.out.println(b.getbatchId());
//					b.setBatchStatusCode(BATCH_STATUS_DELETE_ABORTED);
//					batchMaintenanceDAO.update(b);
				}
			}
		}
		else {
			logger.debug("No batch available for delete");
		}
		
		logger.debug("process finished at " + new Date());
		
		//Process Taxrate Update
		//PP-135
		batches = batchMaintenanceDAO.findFlaggedProcessTaxRateBatches();	
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					Date now = new Date();
					Date schedTime = b.getSchedStartTimestamp();
					if(schedTime == null || schedTime.getTime() <= now.getTime()) {
						logger.info(batchMaintenanceDAO.getDBProperties());
						logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
						processTaxrateUpdate(b);
						break; //process only one a time
					}
				}
			}
		}
		else {
			logger.debug("No customer batch available for process");
		}
		
		//Automated Database Table Import for batch_type_code IN (�IP�, �IPS�, �PCO�) with batch_status_code = �FI�
		batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_PURCHASING, BATCH_STATUS_FLAGGED_FOR_IMPORT);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					importDatabaseTable(b);
					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for database table import of " + BATCH_TYPE_PURCHASING);
		}
		
		batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_SALE, BATCH_STATUS_FLAGGED_FOR_IMPORT);
		//batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_PURCHASING_COMPANY, BATCH_STATUS_FLAGGED_FOR_IMPORT);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					importDatabaseTable(b);
					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for database table import of " + BATCH_TYPE_SALE);
		}
		
		batches = batchMaintenanceDAO.findFlaggedBatches(BATCH_TYPE_PURCHASING_COMPANY, BATCH_STATUS_FLAGGED_FOR_IMPORT);
		if(batches != null && batches.size() > 0) {
			for(BatchMaintenance b : batches) {
				if(b != null) {
					logger.info(batchMaintenanceDAO.getDBProperties());
					logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
					importDatabaseTable(b);
					break; //process only one a time
				}
			}
		}
		else {
			logger.debug("No batch available for database table import of " + BATCH_TYPE_PURCHASING_COMPANY);
		}
		
		//Send an email when I/M/P batch processing is complete
		Option emailProcessStatusOption = optionService.findByPK(new OptionCodePK ("EMAILPROC", "SYSTEM", "SYSTEM"));   
        if(emailProcessStatusOption!=null && emailProcessStatusOption.getValue()!=null && emailProcessStatusOption.getValue().equals("1")){
	        batches = batchMaintenanceDAO.findProcessedBatchesNotNotified();

			if(batches != null && batches.size() > 0) {
				Option emailToOption = optionService.findByPK(new OptionCodePK("EMAILTO", "SYSTEM",  "SYSTEM"));
		  		Option emailCcOption = optionService.findByPK(new OptionCodePK("EMAILCC", "SYSTEM", "SYSTEM"));
		  		Option emailHostOption = optionService.findByPK(new OptionCodePK("EMAILHOST", "ADMIN", "ADMIN"));
		  		Option emailPortOption = optionService.findByPK(new OptionCodePK("EMAILPORT", "ADMIN","ADMIN"));
		  		Option emailFromOption = optionService.findByPK(new OptionCodePK("EMAILFROM", "ADMIN","ADMIN"));
			
				String emailTo = null;
				String emailCc = null;
				String emailHost = null;
				String emailPort = null;
				String emailFrom = null;
				
				if(emailToOption!=null){
					emailTo = emailToOption.getValue();
				}
				if(emailCcOption!=null){
					emailCc = emailCcOption.getValue();
				}
				if(emailHostOption!=null){
					emailHost = emailHostOption.getValue();
				}
				if(emailPortOption!=null){
					emailPort = emailPortOption.getValue();
				}
				if(emailFromOption!=null){
					emailFrom = emailFromOption.getValue();
				}
	
				if (emailHost == null || emailHost.length() == 0 || emailPort == null || emailPort.length() == 0) {
					logger.info("ProcessedBatchesNotNotified: SMTP Settings must be configured");
					return;
				}
	
				if (emailTo == null || emailTo.length() == 0) {
					logger.info("ProcessedBatchesNotNotified: At least one email address must be entered");
					return;
				}
				
				if (emailFrom == null || emailFrom.length() == 0) {
					emailFrom = "pinpoint.support@ryan.com";
				}
				
	
				for(BatchMaintenance b : batches) {
					if(b != null) {
						logger.info(batchMaintenanceDAO.getDBProperties());
						logger.info(String.format("Batch id: %d, type: %s, status: %s, held flag: %s", b.getBatchId(), b.getBatchTypeCode(), b.getBatchStatusCode(), b.getHeldFlag()));
						
						String subject = String.format("Batch ID: %d - Process Complete", b.getBatchId());
						String content = getStatisticsContent(b.getBatchId());//emailContent.toString();
						
						if(!notifyProcessedBatches(b, emailHost, emailPort, emailFrom, emailTo, emailCc, subject, content)){
							break;//Send email failed, no need to keep going;
						}
					}
				}
			}
			else {
				logger.debug("No batch available for batches of IP, PCO not notified");
			}	
        }
		}
	}
	
	public String getStatisticsContent(Long batchId){
		List<DatabaseTransactionStatisticsDTO> tempStatisticsData = new ArrayList<DatabaseTransactionStatisticsDTO>();
		StringBuilder statisticsBuilder = new StringBuilder(); 
		try{
			tempStatisticsData = databaseStatisticsService.getTransactionsStatistics("  WHERE process_batch_no = " + batchId);
		}
		catch(SQLException e){		
		}
		
		DecimalFormat df = new DecimalFormat("###,###,###.00");
		DecimalFormat df10 = new DecimalFormat("##.##########");
		
		statisticsBuilder.append("<table border='1' align='left'>");
		statisticsBuilder.append("<tr><td align='left'>" + "&#160;&#160;" + "</td><td align='right'>" + "&#160;&#160;" + 
				String.format("%10s","Count")  + "</td><td align='right'>" + "&#160;&#160;" + String.format("%10s","% Total")  + "</td><td align='right'>" + "&#160;&#160;" + 
				String.format("%13s","$ Amount")  + "</td><td align='right'>" + "&#160;&#160;" + String.format("%10s","% Sum")  + "</td><td align='right'>" + "&#160;&#160;" + 
				String.format("%12s","Tax Amount")  + "</td><td align='right'>" + "&#160;&#160;" + String.format("%15s","Eff. Tax Rate")  + "</td><td align='right'>" + "&#160;&#160;" + 
				String.format("%18s","$ Highest Amount")  + "</td><td align='right'>" + "&#160;&#160;" + String.format("%18s","$ Average Amount") + "</td></tr>");
		for(DatabaseTransactionStatisticsDTO statisticsData : tempStatisticsData) {
			statisticsBuilder.append("<tr>");	
			statisticsBuilder.append("<td align='left'>" + ((statisticsData.getTitle()==null) ? "" : statisticsData.getTitle().replace(" ", "&#160;")) + "&#160;&#160;</td>");
			
			statisticsBuilder.append("<td align='right'>" + String.format("%10s",(statisticsData.getCount()==null) ? "0" : statisticsData.getCount()) + "</td>");
			statisticsBuilder.append("<td align='right'>" + String.format("%10s",((statisticsData.getCountPct()==null || statisticsData.getCountPct()==0) ? "" : df.format(statisticsData.getCountPct()))) + "</td>"); 
			statisticsBuilder.append("<td align='right'>" + String.format("%13s",((statisticsData.getSum()==null || statisticsData.getSum()==0) ? "" : df.format(statisticsData.getSum()))) + "</td>");
			statisticsBuilder.append("<td align='right'>" + String.format("%10s",((statisticsData.getSumPct()==null || statisticsData.getSumPct()==0) ? "" : df.format(statisticsData.getSumPct()))) + "</td>");
			statisticsBuilder.append("<td align='right'>" + String.format("%12s",((statisticsData.getTaxAmt()==null || statisticsData.getTaxAmt()==0) ? "" : df.format(statisticsData.getTaxAmt()))) + "</td>");  
			statisticsBuilder.append("<td align='right'>" + String.format("%15s",((statisticsData.getEffTaxRate()==null || statisticsData.getEffTaxRate()==0) ? "" : df10.format(statisticsData.getEffTaxRate()))) + "</td>");
			statisticsBuilder.append("<td align='right'>" + String.format("%18s",((statisticsData.getMax()==null || statisticsData.getMax()==0) ? "" : df.format(statisticsData.getMax()))) + "</td>");  
			statisticsBuilder.append("<td align='right'>" + String.format("%18s",((statisticsData.getAvg()==null || statisticsData.getAvg()==0) ? "" : df.format(statisticsData.getAvg()))) + "</td>");	
			statisticsBuilder.append("</tr>");
		}		
		statisticsBuilder.append("</table>");

		System.out.println(statisticsBuilder.toString());
		return statisticsBuilder.toString();
	}
	
	private void processTaxrateUpdate(BatchMaintenance b) {
		try {
			batchDAO.processTaxrateUpdate(b.getBatchId());
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}
	}
	
	private void importDatabaseTable(BatchMaintenance b) { //Automated Database Table Import 

		Date now = new Date();
		
		b.setBatchStatusCode("IX");
		b.setActualStartTimestamp(now);
		try {
     		batchMaintenanceDAO.update(b);
			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return;
		}
		
		Connection conn = null;
		try {
			conn = batchMaintenanceDAO.getDBConnection();
			System.out.println("conn.getMetaData().getURL() : " + conn.getMetaData().getURL());
			
			ImportMapAddBean importMapAddBean = new ImportMapAddBean();
			if(importMapAddBean.automatedInitVerification(conn, b.getVc01())){			
			}
			
			boolean imported = importMapAddBean.automatedDatabaseAction(b);
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}
		finally{
			System.out.println("conn.close() : ");
			if (conn != null) {
				try { conn.close(); } catch (SQLException sqe) { sqe.printStackTrace(); }
			}
		}
	}
	
	private boolean notifyProcessedBatches(BatchMaintenance b, String emailHost, String emailPort, String emailFrom, 
			String emailTo, String emailCc, String subject, String content) { 
		try {
			sendEmail(emailHost, emailPort,emailFrom, emailTo, emailCc, subject, content);
			b.setEmailSentFlag("1");
     		batchMaintenanceDAO.update(b);	
			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), "email notified"));
		}
		catch(Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return false;
		}
		
		return true;
	}

	public void sendEmail(String smtpHost, String smtpPort, String from,
			String to, String cc, String subject, String content)
			throws AddressException, MessagingException {

		// Create a mail session
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		javax.mail.Session session = javax.mail.Session.getInstance(props, null);

		// Construct the message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));

		InternetAddress[] addressTo = InternetAddress.parse(
				to.replaceAll(";", ","), true);
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		if (cc != null && cc.length() > 0) {
			InternetAddress[] addressCc = InternetAddress.parse(
					cc.replaceAll(";", ","), true);
			msg.setRecipients(Message.RecipientType.CC, addressCc);
		}
		msg.setSubject(subject);
		//msg.setText(content);
		msg.setContent(content, "text/html");

		// Send the message
		Transport.send(msg);
	}
	
	private void deleteCustomerBatch(BatchMaintenance b) {
		Date now = new Date();
		
		b.setBatchStatusCode(CustLocnBatch.DELETING);
		b.setActualStartTimestamp(now);
		try {
			batchMaintenanceDAO.update(b);
			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}
		
		try {
			boolean deleted = custLocnDAO.deleteBatch(b.getBatchId());
			if(deleted) {
				now = new Date();
				b.setBatchStatusCode(CustLocnBatch.DELETED);
				b.setActualEndTimestamp(now);
				batchMaintenanceDAO.update(b);
				logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
			}
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}
	}

	private void deleteSaleBatch(BatchMaintenance b) {
		Date now = new Date();
		
		
		b.setBatchStatusCode(BATCH_STATUS_SALE_DELETING);
		b.setActualStartTimestamp(now);
		try {
			batchMaintenanceDAO.update(b);
			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			return;
		}
		
		try {
			boolean deleted = saleTransactionExDAO.deleteBatch(b.getBatchId());
			if(deleted) {
				now = new Date();
				b.setBatchStatusCode(BATCH_STATUS_SALE_DELETED);
				b.setActualEndTimestamp(now);
				batchMaintenanceDAO.update(b);
				logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
			} else {
				b.setBatchStatusCode(BATCH_STATUS_DELETE_ABORTED);
				batchMaintenanceDAO.update(b);
			}
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			//b.setBatchStatusCode(Batch)
			return;
		}
	}

	private void processCustomerBatch(BatchMaintenance b) {
		BatchProcessTransient bpt = new BatchProcessTransient();
		
		Date now = new Date();
		
		b.setBatchStatusCode(CustLocnBatch.PROCESSING);
		b.setErrorSevCode("");
		b.setActualStartTimestamp(now);
		try {
			batchMaintenanceDAO.update(b);
			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			
			logError(b, bpt, "CL1", CustLocnBatch.BATCH_CODE, now, 0L);
			if("1".equals(bpt.getAbortImportFlag())) {
				try {
					b = batchMaintenanceDAO.findByBatchId(b.getBatchId());
					b.setBatchStatusCode("ABP");
					batchMaintenanceDAO.update(b);
				}
				catch(Exception ee) {
					logger.error(ee.getMessage(), ee);
				}
				return;
			}
		}
		
		try{
			Long count = bcpCustLocnDAO.count(b);
			if(count > 0) {
				for(int i=0; i<count; i+=FETCH_SIZE) {
					List<BCPCustLocn> list = bcpCustLocnDAO.getPage(b, i, FETCH_SIZE, true);
					if(list != null) {
						for(BCPCustLocn cl : list) {
							bpt.setWriteImportLineFlag("1");
							bpt.setActualRowno(bpt.getActualRowno() + 1);
							logger.trace(cl);
							
							if(cl.getRecordType()!=null && cl.getRecordType().equalsIgnoreCase("A")){
								//Customer/Location
								if(cl.getCustNbr()==null || cl.getCustNbr().length()==0){
									logError(b, bpt, "CL25", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
								}
								else if(cl.getUpdateCode()!=null && cl.getUpdateCode().equalsIgnoreCase("I")){
									Cust aCust = custDAO.getCustByNumber(cl.getCustNbr());		
									if(aCust!=null){
										aCust.setCustName(cl.getCustName());
										custDAO.update(aCust);
									}
									else{
										aCust = new Cust();
										aCust.setCustNbr(cl.getCustNbr());
										aCust.setCustName(cl.getCustName());
										custDAO.save(aCust);
									}
									
									if(cl.getCustLocnCode()!=null && cl.getCustLocnCode().length()>0){
										CustLocn aCustLocn = custLocnDAO.getCustLocnByCustId(aCust.getCustId(), cl.getCustLocnCode());
										if(aCustLocn!=null){
											aCustLocn.setLocationName(cl.getLocationName());
											aCustLocn.setGeocode(cl.getGeocode());
											aCustLocn.setAddressLine1(cl.getAddressLine1());
											aCustLocn.setAddressLine2(cl.getAddressLine2());
											aCustLocn.setCity(cl.getCity());
											aCustLocn.setCounty(cl.getCounty());
											aCustLocn.setState(cl.getState());
											aCustLocn.setZip(cl.getZip());
											aCustLocn.setZipplus4(cl.getZipplus4());
											aCustLocn.setCountry(cl.getCountry());
											aCustLocn.setActiveFlag(cl.getActiveFlag());
											custLocnDAO.update(aCustLocn);
										}
										else{
											aCustLocn = new CustLocn();
											aCustLocn.setCustId(aCust.getCustId());
											aCustLocn.setCustLocnCode(cl.getCustLocnCode());
											
											aCustLocn.setLocationName(cl.getLocationName());
											aCustLocn.setGeocode(cl.getGeocode());
											aCustLocn.setAddressLine1(cl.getAddressLine1());
											aCustLocn.setAddressLine2(cl.getAddressLine2());
											aCustLocn.setCity(cl.getCity());
											aCustLocn.setCounty(cl.getCounty());
											aCustLocn.setState(cl.getState());
											aCustLocn.setZip(cl.getZip());
											aCustLocn.setZipplus4(cl.getZipplus4());
											aCustLocn.setCountry(cl.getCountry());
											aCustLocn.setActiveFlag(cl.getActiveFlag());
											custLocnDAO.save(aCustLocn);
										}		
									}
									
									bpt.setProcessedRows(bpt.getProcessedRows() + 1);
								}
								else if(cl.getUpdateCode()!=null && cl.getUpdateCode().equalsIgnoreCase("D")){
									if(cl.getCustLocnCode()!=null && cl.getCustLocnCode().length()>0){
										CustLocn aCustLocn = custLocnDAO.getCustLocnByCustLocnCode(cl.getCustLocnCode());
										if(aCustLocn!=null){
											if(custLocnDAO.isAllowDeleteCustLocn(cl.getCustLocnCode())){
												custLocnDAO.remove(aCustLocn);
											}
											else{
												aCustLocn.setActiveBooleanFlag(false);
												custLocnDAO.update(aCustLocn);
											}
										}
										else{
											logError(b, bpt, "CL26", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
										}
									}
									else{
										Cust aCust = custDAO.getCustByNumber(cl.getCustNbr());										
										if(aCust!=null){
											if(custDAO.isAllowDeleteCustomer(aCust.getCustNbr())){
												custDAO.deleteCustEntityList(aCust);
											}
											else{
												custDAO.disableActiveCustomer(aCust.getCustNbr());
											}
										}									
									}
										
									bpt.setProcessedRows(bpt.getProcessedRows() + 1);		
								}
								else{
									logError(b, bpt, "CL27", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
								}	
							}
							else if(cl.getRecordType()!=null && cl.getRecordType().equalsIgnoreCase("B")){	
								//Entity
								if(cl.getCustNbr()==null || cl.getCustNbr().length()==0){
									logError(b, bpt, "CL25", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
								}
								else if(cl.getUpdateCode()!=null && cl.getUpdateCode().equalsIgnoreCase("I")){
									Cust aCust = custDAO.getCustByNumber(cl.getCustNbr());								
									if(aCust!=null){
										EntityItem aEntityItem = entityItemDAO.findByCode(cl.getEntityCode());
										if(aEntityItem!=null){
											CustEntity aCustEntity = custDAO.findCustEntity(aCust.getCustId(), aEntityItem.getEntityId());
											
											if(aCustEntity==null){
												custDAO.addCustEntity(aCust.getCustId(), aEntityItem.getEntityId());
											}
											bpt.setProcessedRows(bpt.getProcessedRows() + 1);
										}
										else{
											logError(b, bpt, "CL28", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
										}
									}
									else{
										logError(b, bpt, "CL25", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
									}
								}
								else if(cl.getUpdateCode()!=null && cl.getUpdateCode().equalsIgnoreCase("D")){
									Cust aCust = custDAO.getCustByNumber(cl.getCustNbr());										
									if(aCust!=null){
										EntityItem aEntityItem = entityItemDAO.findByCode(cl.getEntityCode());
										if(aEntityItem!=null){
											CustEntity aCustEntity = custDAO.findCustEntity(aCust.getCustId(), aEntityItem.getEntityId());						
											if(aCustEntity==null){
												custDAO.deleteCustEntity(aCust.getCustId(), aEntityItem.getEntityId());
											}
											bpt.setProcessedRows(bpt.getProcessedRows() + 1);
										}
										else{
											logError(b, bpt, "CL28", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
										}
									}
									else{
										logError(b, bpt, "CL25", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
									}									
								}
								else{
									logError(b, bpt, "CL27", CustLocnBatch.BATCH_CODE, now, bpt.getActualRowno());
								}	
							}
						}
					}
				}
				
				b = batchMaintenanceDAO.findByBatchId(b.getBatchId());
				b.setBatchStatusCode("P");
				b.setErrorSevCode(bpt.getErrorSevCode() + "");
				b.setProcessedRows(bpt.getProcessedRows());
				b.setActualEndTimestamp(new Date());
				b.setNu02(new Double(bpt.getActualRowno()));
				b.setNu03(bpt.getTransactionAmount().doubleValue());
				batchMaintenanceDAO.update(b);
			}
		}
		catch(Exception e) {
			logger.error("Failed to update batch status for process", e);
		}
	}
	
//	private void processSaleBatch(BatchMaintenance b) {
//		BatchProcessTransient bpt = new BatchProcessTransient();
//		System.out.println("proc sb!");
//		
//		Date now = new Date();
//		
//		b.setBatchStatusCode(BATCH_STATUS_SALE_PROCESSING);
//		b.setErrorSevCode("");
//		b.setActualStartTimestamp(now);
//		try {
//			batchMaintenanceDAO.update(b);
//			logger.info(String.format("Batch id: %d update with status %s", b.getBatchId(), b.getBatchStatusCode()));
//		}
//		catch(Exception e) {
//			logger.error(e.getMessage(), e);
//			
//			logError(b, bpt, "PS1", BATCH_PROCESS_TYPE_SALE, now, 0L);
//			if("1".equals(bpt.getAbortImportFlag())) {
//				try {
//					b = batchMaintenanceDAO.findByBatchId(b.getBatchId());
//					b.setBatchStatusCode("ABP");
//					batchMaintenanceDAO.update(b);
//				}
//				catch(Exception ee) {
//					logger.error(ee.getMessage(), ee);
//				}
//				return;
//			}
//		}
//		
//		try{
//			BCPBillTransaction bcpTrans = new BCPBillTransaction();
//			bcpTrans.setProcessBatchNo(b.getbatchId());
//			Long count = bcpSaleTransactionExDAO.count(bcpTrans);
//			if(count > 0) {
//
//				SaleTransactionDocument doc = null;
//				SaleTransaction pt = null;
//				boolean doInvoiceProcessing = true; //TODO: set this flag from TB_OPTION
//
//				for(int i=0; i<count; i+=FETCH_SIZE) {
//					List<BCPBillTransaction> list = bcpSaleTransactionExDAO.findByExample(bcpTrans, new String[]{"invoiceNbr", "bcpSaletransId"}, new boolean[]{true, true}, i, FETCH_SIZE);
//					if(list != null) {
//						for(BCPBillTransaction t : list) {
//							bpt.setWriteImportLineFlag("1");
//							bpt.setActualRowno(bpt.getActualRowno() + 1);
//							logger.trace(t);
//							SaleTransaction st = new SaleTransaction();
//							BeanUtils.copyProperties(t, st, new String[]{"bcpSaletransId", "saletransId"});
//							logger.trace(st);
//							bpt.setTransactionAmount(ArithmeticUtils.add(bpt.getTransactionAmount(), st.getInvoiceLineItemAmt()));
//							st.setProcessMethod("BATCH");
//							String currentUser = Auditable.currentUserCode();
//							if (pt == null) {
//								doc = new SaleTransactionDocument();
//								List<SaleTransaction> lineItems = new ArrayList<SaleTransaction>();
//								lineItems.add(st);
//								doc.setSaleTransaction(lineItems);
//								pt = st;
//							} else {
//								if (doInvoiceProcessing && StringUtils.hasText(pt.getInvoiceNbr())
//										&& pt.getInvoiceNbr().equals(st.getInvoiceNbr())) {
//									doc.getSaleTransaction().add(st);
//									pt = st;
//								} else {
//									if(doc.getSaleTransaction() != null && doc.getSaleTransaction().size() > 0) {
//									//	LinkedList<SaleTransactionLog> logs = new LinkedList<SaleTransactionLog>();
//										for(SaleTransaction saleTransaction: doc.getSaleTransaction()) {
//									/*		SaleTransactionLog newLog = new SaleTransactionLog();
//											SaleTransactionLogUtil.copyState(saleTransaction, newLog, SaleTransactionLogUtil.BEFORE_STATE);
//											logs.add(newLog);*/
//										}
//										
//										try {
//											saleTransactionProcess.process(doc, true, currentUser);
//											bpt.setProcessedRows(bpt.getProcessedRows() + doc.getSaleTransaction().size());
//										}
//										catch(ProcessAbortedException e) {
//											logError(b, bpt, e.getErrorCode(), BATCH_PROCESS_TYPE_SALE, now, bpt.getActualRowno());
//										}
//										
//										if("1".equals(bpt.getWriteImportLineFlag())) {
//											for(SaleTransaction dt : doc.getSaleTransaction()) {
//												try {
//													dt.setSaletransId(null);
//													saleTransactionExDAO.save(dt);
//													
//													SaleTransactionLog savedLog = dt.getTransactionLog();
//													//SaleTransactionLogUtil.copyState(dt, savedLog, SaleTransactionLogUtil.AFTER_STATE);
//													savedLog.setUpdateUserId(currentUser);
//													savedLog.setUpdateTimestamp(new Date());
//													savedLog.setSaletransId(dt.getSaletransId());
//													savedLog.setLogSource("Batch process Task");
//													saleTransactionLogService.save(savedLog);
//													
//													
//												}
//												catch(Exception we) {
//													logError(b, bpt, "PS24", BATCH_PROCESS_TYPE_SALE, now, bpt.getActualRowno());
//												}
//											}
//										}
//									}
//
//									doc = new SaleTransactionDocument();
//									List<SaleTransaction> lineItems = new ArrayList<SaleTransaction>();
//									lineItems.add(st);
//									doc.setSaleTransaction(lineItems);
//									pt = st;
//								}
//							}
//						}
//					}
//				}
//				
//				if(doc.getSaleTransaction() != null && doc.getSaleTransaction().size() > 0) {
//					String currentUser = Auditable.currentUserCode();
////					LinkedList<SaleTransactionLog> logs = new LinkedList<SaleTransactionLog>();
//
//					try {
//						saleTransactionProcess.process(doc, true, currentUser);
//						bpt.setProcessedRows(bpt.getProcessedRows() + doc.getSaleTransaction().size());
//					}
//					catch(ProcessAbortedException e) {
//						logError(b, bpt, e.getErrorCode(), BATCH_PROCESS_TYPE_SALE, now, bpt.getActualRowno());
//					}
//					
//					if("1".equals(bpt.getWriteImportLineFlag())) {
//						for(SaleTransaction dt : doc.getSaleTransaction()) {
//							try {
//								dt.setSaletransId(null);
//								saleTransactionExDAO.save(dt);
//								
///*								SaleTransactionLog savedLog = logs.poll();
//								SaleTransactionLogUtil.copyState(dt, savedLog, SaleTransactionLogUtil.AFTER_STATE);*/
//								SaleTransactionLog savedLog = dt.getTransactionLog();
//								savedLog.setSaletransId(dt.getSaletransId());
//								savedLog.setLogSource("Batch process Task");
//								saleTransactionLogService.save(savedLog);
//								
//								
//							}
//							catch(Exception we) {
//								logError(b, bpt, "PS24", BATCH_PROCESS_TYPE_SALE, now, bpt.getActualRowno());
//							}
//						}
//					}
//				}
//				
//				b = batchMaintenanceDAO.findByBatchId(b.getBatchId());
//				b.setBatchStatusCode("P");
//				b.setErrorSevCode(bpt.getErrorSevCode() + "");
//				b.setProcessedRows(bpt.getProcessedRows());
//				b.setActualEndTimestamp(new Date());
//				b.setNu02(new Double(bpt.getActualRowno()));
//				b.setNu03(bpt.getTransactionAmount().doubleValue());
//				batchMaintenanceDAO.update(b);
//			}
//		}
//		catch(Exception e) {
//			logger.error("Failed to update batch status for process", e);
//		}
//	}

	public void logError(BatchMaintenance b, BatchProcessTransient bpt, String errorCode, String processType, Date processTimestamp, long rowNum) {
		try {
			ListCodesPK pk = new ListCodesPK("ERRORCODE", errorCode);
			ListCodes errCode = listCodesService.findByPK(pk);
			ListCodes errSev = null;
			if(errCode != null) {
				pk = new ListCodesPK("ERRORSEV", errCode.getSeverityLevel());
				errSev = listCodesService.findByPK(pk);
			}
			
			bpt.setErrorCount(bpt.getErrorCount() + 1);
			bpt.setSevLevel(errCode == null ? "90" : errCode.getSeverityLevel());
			bpt.setWriteImportLineFlag(errSev == null ? "0" : errSev.getWriteImportLineFlag());
			bpt.setAbortImportFlag(errSev == null ? "1" : errSev.getAbortImportFlag());
			bpt.setErrorSevCode(bpt.getSevLevel());
			
			BatchErrorLog error = new BatchErrorLog();
			error.setBatchId(b.getbatchId());
			error.setProcessType(processType);
			error.setProcessTimestamp(processTimestamp);
			error.setRowNo(rowNum);
			error.setColumnNo(0L);
			error.setErrorDefCode(errorCode);
			batchMaintenanceDAO.save(error);
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void reschedule(long repeatInterval) {
		SimpleTrigger newTrigger = new SimpleTrigger();
		newTrigger.setRepeatInterval(repeatInterval);
		newTrigger.setName(batchProcessTrigger.getName());
		newTrigger.setJobName(batchProcessTrigger.getJobName());
		newTrigger.setStartTime(new Date());
		newTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);

		// Re-schedule Job with new trigger
		try {
			Date date = batchProcessScheduler.rescheduleJob(batchProcessTrigger.getName(), batchProcessTrigger.getGroup(), newTrigger);
			batchProcessTrigger = newTrigger;		
			logger.info("Batch process re-scheduled to interval " + repeatInterval + " milliseconds: " + date);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public StdScheduler getBatchProcessScheduler() {
		return batchProcessScheduler;
	}

	public void setBatchProcessScheduler(StdScheduler batchProcessScheduler) {
		this.batchProcessScheduler = batchProcessScheduler;
	}

	public SimpleTrigger getBatchProcessTrigger() {
		return batchProcessTrigger;
	}

	public void setBatchProcessTrigger(SimpleTrigger batchProcessTrigger) {
		this.batchProcessTrigger = batchProcessTrigger;
	}

	public long getRepeatIntervalFromPreference(){
		long repeatInterval = 30 * 1000;
		Option option = optionService.findByPK(new OptionCodePK("BATCHPROCESSINTERVALSECS", "SYSTEM", "SYSTEM"));
		if(option != null && option.getValue()!=null) {
			try {
				repeatInterval = Long.parseLong(option.getValue()) * 1000;
				if(repeatInterval<0){
					repeatInterval = 30 * 1000;
				}
			}
			catch(Exception e){
			}
		}
		
		return repeatInterval;
	}

	public void init() {
		currentRepeatInterval = getRepeatIntervalFromPreference();
		if(currentRepeatInterval==0){
			logger.info("Batch process stopped. Run every 30 * 1000 milliseconds for scheduler: " + new Date().toString());
			reschedule(30*1000);
		}
		else{
			reschedule(currentRepeatInterval);
		}
		this.initialized = true;
	}

	public SaleTransactionExDAO getSaleTransactionExDAO() {
		return saleTransactionExDAO;
	}

	public void setSaleTransactionExDAO(SaleTransactionExDAO saleTransactionExDAO) {
		this.saleTransactionExDAO = saleTransactionExDAO;
	}
}
