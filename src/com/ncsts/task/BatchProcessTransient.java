package com.ncsts.task;

import java.math.BigDecimal;

import com.seconddecimal.billing.util.ArithmeticUtils;

public class BatchProcessTransient {
	private int errorCount = 0;
	private String sevLevel;
	private String writeImportLineFlag;
	private String abortImportFlag;
	private int errorSevCode = 0;
	private long actualRowno;
	private long processedRows = 0;
	private BigDecimal transactionAmount = ArithmeticUtils.ZERO;

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public String getSevLevel() {
		return sevLevel;
	}

	public void setSevLevel(String sevLevel) {
		this.sevLevel = sevLevel;
	}

	public String getWriteImportLineFlag() {
		return writeImportLineFlag;
	}

	public void setWriteImportLineFlag(String writeImportLineFlag) {
		this.writeImportLineFlag = writeImportLineFlag;
	}

	public String getAbortImportFlag() {
		return abortImportFlag;
	}

	public void setAbortImportFlag(String abortImportFlag) {
		this.abortImportFlag = abortImportFlag;
	}

	public int getErrorSevCode() {
		return errorSevCode;
	}

	public void setErrorSevCode(int errorSevCode) {
		this.errorSevCode = errorSevCode;
	}
	
	public void setErrorSevCode(String sevLevel) {
		int level = 0;
		try {
			level = Integer.parseInt(sevLevel);
		}
		catch(Exception e){}
		
		if(level > this.errorSevCode) {
			this.errorSevCode = level;
		}
	}

	public long getActualRowno() {
		return actualRowno;
	}

	public void setActualRowno(long actualRowno) {
		this.actualRowno = actualRowno;
	}

	public long getProcessedRows() {
		return processedRows;
	}

	public void setProcessedRows(long processedRows) {
		this.processedRows = processedRows;
	}

	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
}
