package com.ncsts.task;


import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.ProcessStep;

public class PurchaseTransactionTask {
    public enum TaskStatus {READY, IN_PROGRESS, COMPLETE};
    private TaskStatus status;
    private final PurchaseTransaction purchaseTransaction;
    private ProcessStep processStep;
    private long timeStart;
    private long timeEnd;


    public PurchaseTransactionTask(PurchaseTransaction purchaseTransaction, ProcessStep processStep) {
        this.purchaseTransaction = purchaseTransaction;
        this.processStep = processStep;
        status = TaskStatus.READY;
    }

    public void markStart() {
        timeStart = System.currentTimeMillis();
        status = TaskStatus.IN_PROGRESS;
    }
    public void markComplete() {
        timeEnd = System.currentTimeMillis();
        status = TaskStatus.COMPLETE;
    }

    public long executionTime() {
        if(status == TaskStatus.COMPLETE)
            return timeEnd - timeStart;
        return -1;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public PurchaseTransaction getPurchaseTransaction() {
        return purchaseTransaction;
    }

    public ProcessStep getProcessStep() {
        return processStep;
    }
}
