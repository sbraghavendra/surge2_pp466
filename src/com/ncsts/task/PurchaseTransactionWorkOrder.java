package com.ncsts.task;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants.ProcessStep;
import com.ncsts.domain.PurchaseTransaction;

import java.util.LinkedList;

/**
 * Created by RC05200 on 8/25/2017.
 */
public class PurchaseTransactionWorkOrder {
    private final LinkedList<PurchaseTransactionTask> tasks = new LinkedList<>();
    private final PurchaseTransaction purchaseTransaction;

    public PurchaseTransactionWorkOrder(PurchaseTransaction purchaseTransaction) {
        this.purchaseTransaction = purchaseTransaction;
    }

    public PurchaseTransactionWorkOrder addTask(PurchaseTransactionTask processTask) {
        tasks.add(processTask);
        return this;
    }

    public LinkedList<PurchaseTransactionTask> getTasks() {
        return tasks;
    }
}
