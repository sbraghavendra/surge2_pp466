package com.ncsts.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletFileDownload extends HttpServlet{ 
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

		//Check if session is still valid
		HttpSession session = (HttpSession) request.getSession(false);
		if(session==null){
			createWarningPage(response, "HttpSession is invalid: PPERR-00100", null,null);			
			return;
		}
		
		String sessionId = session.getId();
		
		//Check url uid
		String requesturi = request.getParameter("uri");
		if(requesturi==null || requesturi.length()==0){
			createWarningPage(response, "Request uri parameter is invalid: PPERR-00101", null,null);
			return;
		}
		String decodedUri = java.net.URLDecoder.decode(requesturi, "UTF-8");
				
		//Check url uid
		String urluid = request.getParameter("uid");
		if(urluid==null || urluid.length()==0){
			createWarningPage(response, "Request uid parameter is invalid: PPERR-00102", sessionId, decodedUri);
			return;
		}
		
		//Check session uid
		String keyfilePath = (String) session.getAttribute("downloadFilename");		  
		if(keyfilePath==null || keyfilePath.length()==0){
			createWarningPage(response, "An unexpected error occurred: PPERR-00103", sessionId, decodedUri);
			return;
		}
		
		String keyuid = urluid + "::";
		if(!keyfilePath.startsWith(keyuid)){
			//session uid doesn't match url uid
			createWarningPage(response, "An unexpected error occurred: PPERR-00104", sessionId, decodedUri);
			return;
		}
		
		if(keyfilePath.length() == keyuid.length()){
			createWarningPage(response, "An unexpected error occurred: PPERR-00105", sessionId, decodedUri);
			return;
		}
		
		//Got everything, remove downloadFilename. 
		session.removeAttribute("downloadFilename");
		
		String path = keyfilePath.substring(keyuid.length(), keyfilePath.length());
		File file = new File(path);			
		if (file != null && file.canRead()) {
			long contentLength = file.length();		
			String fileName = file.getName();
  			response.setContentType("text/plain");
  			response.setContentLength((int) contentLength);
  			response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
  			response.addHeader("Cache-Control", "no-cache");  
  			response.flushBuffer();			
  			System.out.println("File downloading : " + fileName + " " + sdf.format(new Date()));
	  			
  			ServletOutputStream out = response.getOutputStream();
  			FileInputStream reader = null;	
  			try
  			{
  				reader = new FileInputStream(file);
  				byte[] buffer = new byte[1024];
  				int bytesread = 0, bytesBuffered = 0;	
  				while ((bytesread = reader.read(buffer)) != -1) {
  					out.write(buffer, 0, bytesread);	
  					bytesBuffered += bytesread;
  			        if (bytesBuffered > 1024 * 1024) { //flush after 1MB
  			            bytesBuffered = 0;
  			            out.flush();
  			        }
  				}
  			}
  			catch (Exception e) {
  				e.printStackTrace();
  			} 
  			finally {	
  				System.out.println("File downloaded  : " + fileName + " " + sdf.format(new Date()));
  				
  				if(reader!=null){
  					reader.close();
  					reader = null;
  				}
  			    if (out != null) {
  			        out.flush();
  			        out.close();
  			        out = null;
  			    }
  			}
  		}
		else{
			createWarningPage(response, "File not found: PPERR-00106", sessionId, decodedUri);
		}
	}
	
	public void createWarningPage(HttpServletResponse response, String message, String sessionid, String requesturi){
		try{
			PrintWriter out = response.getWriter(); 
		    response.setContentType("text/html"); 
		    out.println("<H1>PinPoint File Download</h2>"); 
		    out.println("<P>This servlet ran at "); 
		    out.println(new Date().toString()); 
		    out.println("<P>Warning: " + message);
		    out.println("<P>Please contact Ryan Support.");
		    out.println("<P>");
		    if(sessionid!=null && requesturi!=null){
		    	out.println("<P><a href=\"" + requesturi + ";jsessionid=" + sessionid + "\">Go Back</a> ");
		    }
		    out.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		} 	
	}
}
