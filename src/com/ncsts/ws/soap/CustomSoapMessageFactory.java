package com.ncsts.ws.soap;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.SoapMessageFactory;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.TransportInputStream;

public class CustomSoapMessageFactory implements SoapMessageFactory, InitializingBean {
	private static final String REQUEST_CONTEXT_ATTRIBUTE = "CustomSoapMessageFactory";
	
	private static final Log logger = LogFactory.getLog(CustomSoapMessageFactory.class);
	
	private SaajSoapMessageFactory soap11MessageFactory = new SaajSoapMessageFactory();
	private SaajSoapMessageFactory soap12MessageFactory = new SaajSoapMessageFactory();
	
	private SoapProtocolChooser soapProtocolChooser = new ContentTypeSoapProtocolChooser();
	
	private void setMessageFactoryForRequestContext(SaajSoapMessageFactory mf) {
		RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
		attrs.setAttribute(REQUEST_CONTEXT_ATTRIBUTE, mf, RequestAttributes.SCOPE_REQUEST);
	}
	
	private SaajSoapMessageFactory getMessageFactoryForRequestContext() {
		RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
		SaajSoapMessageFactory mf = (SaajSoapMessageFactory) attrs.getAttribute(REQUEST_CONTEXT_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST);
		return mf;
	}
	
	public SoapMessage createWebServiceMessage() {
		return getMessageFactoryForRequestContext().createWebServiceMessage();
	}

	public SoapMessage createWebServiceMessage(InputStream inputStream) throws IOException {
		setMessageFactoryForRequestContext(soap12MessageFactory);
		if (inputStream instanceof TransportInputStream) {
            TransportInputStream transportInputStream = (TransportInputStream) inputStream;
        	if (soapProtocolChooser.useSoap11(transportInputStream)) {
        		setMessageFactoryForRequestContext(soap11MessageFactory);
        	}
        }
		SaajSoapMessageFactory mf = getMessageFactoryForRequestContext();
		if (mf == soap11MessageFactory) {
			logger.debug("Final soapMessageFactory? " + soap11MessageFactory);
		} else {
			logger.debug("Final soapMessageFactory? " + soap12MessageFactory);
		}
		return mf.createWebServiceMessage(inputStream);
	}
	
	public void setSoapVersion(SoapVersion version) {
		logger.debug("setSoapVersion called with: " + version + " -- ignoring");
	}
	
	public void setSoapProtocolChooser(SoapProtocolChooser soapProtocolChooser) {
		this.soapProtocolChooser = soapProtocolChooser;
	}
	
	public void afterPropertiesSet() throws Exception {
		soap11MessageFactory.setSoapVersion(SoapVersion.SOAP_11);
		soap11MessageFactory.afterPropertiesSet();
		soap12MessageFactory.setSoapVersion(SoapVersion.SOAP_12);
		soap12MessageFactory.afterPropertiesSet();
	}
}
