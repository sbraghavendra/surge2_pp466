package com.ncsts.ws.soap;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ws.transport.TransportInputStream;

public class ContentTypeSoapProtocolChooser implements SoapProtocolChooser {
	private static final Log logger = LogFactory.getLog(ContentTypeSoapProtocolChooser.class);
	
	@SuppressWarnings("rawtypes")
	public boolean useSoap11(TransportInputStream transportInputStream) throws IOException {
        for(Iterator headerNames = transportInputStream.getHeaderNames(); headerNames.hasNext();) {
            String headerName = (String) headerNames.next();
            logger.debug("found headerName: " + headerName);
            for(Iterator headerValues = transportInputStream.getHeaders(headerName); headerValues.hasNext();) {
                String headerValue = (String) headerValues.next();
                logger.debug("     headerValue? " + headerValue);
                if(headerName.toLowerCase().contains("content-type")) {
                	logger.debug("Content Type  - " + headerValue);
            		if (headerValue.trim().toLowerCase().contains("text/xml")) {
            			logger.debug("Found text/xml in header.  Using SOAP 1.1");
        				return true;
            		}
            		break;
                }
            }
        }
        return false;
	}
}
