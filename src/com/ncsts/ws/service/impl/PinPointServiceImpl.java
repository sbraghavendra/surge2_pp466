package com.ncsts.ws.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LocationType;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants.LogSourceEnum;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.dao.SaleTransactionExDAO;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.dto.JurisdictionDTO;
import com.ncsts.exception.PurchaseTransactionProcessSuspendedException;
import com.ncsts.exception.PurchaseTransactionProcessingException;
import com.ncsts.exception.PurchaseTransactionSuspendedException;
import com.ncsts.services.EntityDefaultService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.SaleTransactionLogService;
import com.ncsts.ws.message.BatchProcessRequest;
import com.ncsts.ws.message.BatchProcessResponse;
import com.ncsts.ws.message.BatchStatusRequest;
import com.ncsts.ws.message.BatchStatusResponse;
import com.ncsts.ws.message.BillTransactionDocument;
import com.ncsts.ws.message.BillTransactionProcessRequest;
import com.ncsts.ws.message.BillTransactionProcessResponse;
import com.ncsts.ws.message.JurisdictionRequest;
import com.ncsts.ws.message.JurisdictionResponse;
import com.ncsts.ws.message.MicroApiTransactionDocument;
import com.ncsts.ws.message.MicroApiTransactionProcessRequest;
import com.ncsts.ws.message.MicroApiTransactionProcessResponse;
import com.ncsts.ws.message.ProcessSuspendedTransactionDocument;
import com.ncsts.ws.message.ProcessedTransactionIdentifier;
import com.ncsts.ws.message.PurchaseTransactionDocument;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualRequest;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualResponse;
import com.ncsts.ws.message.PurchaseTransactionGetLocationRequest;
import com.ncsts.ws.message.PurchaseTransactionGetLocationResponse;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationRequest;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedResponse;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceRequest;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceResponse;
import com.ncsts.ws.message.PurchaseTransactionPushProrateRequest;
import com.ncsts.ws.message.PurchaseTransactionPushProrateResponse;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificRequest;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificResponse;
import com.ncsts.ws.message.PurchaseTransactionReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionSuspendResponse;
import com.ncsts.ws.message.RealTimeTransactionProcessRequest;
import com.ncsts.ws.message.RealTimeTransactionProcessResponse;
import com.ncsts.ws.message.SaleTransactionDocumentProcessRequest;
import com.ncsts.ws.message.SaleTransactionDocumentProcessResponse;
import com.ncsts.ws.message.SaleTransactionProcessRequest;
import com.ncsts.ws.service.PinPointService;
import com.seconddecimal.billing.domain.SaleTransaction;
import com.seconddecimal.billing.domain.SaleTransactionDocument;
import com.seconddecimal.billing.domain.SaleTransactionLog;
import com.seconddecimal.billing.exceptions.ProcessAbortedException;
import com.seconddecimal.billing.service.SaleTransactionProcess;

@Service
public class PinPointServiceImpl implements PinPointService {
	private Logger logger = Logger.getLogger(PinPointService.class);

	@Autowired
	private BatchMaintenanceDAO batchMaintenanceDAO;

	@Autowired
	private TransactionDetailDAO transactionDetailDAO;

	@Autowired
	private SaleTransactionProcess saleTransactionProcess;

	@Autowired
	private SaleTransactionExDAO saleTransactionExDAO;

	@Autowired
	private SaleTransactionLogService saleTransactionLogService;

	@Autowired
	private PurchaseTransactionService purchaseTransactionService;

	@Autowired
	private JurisdictionDAO jurisdictionDAO;

	@Autowired
	private OptionService optionService;

	@Autowired
	private EntityDefaultService entityDefaultService;

	@Autowired
	private EntityItemService entityItemService;

	@Autowired
	protected ListCodesService listCodesService;

	@Override
	public BatchStatusResponse getBatchStatus(BatchStatusRequest request) {
		BatchMaintenance batchMaintenance = new BatchMaintenance();

		if (request.getEntryDate() != null) {
			batchMaintenance.setEntryTimestamp(request.getEntryDate());
		}

		if (request.getBatchType() != null) {
			batchMaintenance.setBatchTypeCode(request.getBatchType());
		}

		if (request.getBatchStatus() != null) {
			batchMaintenance.setBatchStatusCode(request.getBatchStatus());
		}

		OrderBy orderBy = new OrderBy();
		if (request.getOrderBy() != null) {
			String fieldName = request.getOrderBy();
			boolean ascending = true;
			int index = -1;
			if ((index = fieldName.indexOf(" ")) != -1 && index < fieldName.length() - 1) {
				fieldName = request.getOrderBy().substring(0, index).trim();
				ascending = request.getOrderBy().substring(index + 1).trim().toLowerCase().startsWith("desc") ? false
						: true;
			}
			orderBy.addFieldSortOrder(new FieldSortOrder(fieldName, 1, ascending));
		}

		List<BatchMaintenance> list = batchMaintenanceDAO.find(batchMaintenance, orderBy, request.getOffset(),
				request.getMaxResults());

		BatchStatusResponse response = new BatchStatusResponse();
		response.setBatchMaintenance(list);

		return response;
	}

	@Override
	public RealTimeTransactionProcessResponse getRealTimeTransactionProcess(RealTimeTransactionProcessRequest request) {
		PurchaseTransaction transactionDetail = request.getTransactionDetail();
		String errorCode = transactionDetailDAO.callRealTimeTransactionProcessProc(transactionDetail,
				request.getCalcFlag(), request.getWriteFlag());
		RealTimeTransactionProcessResponse response = new RealTimeTransactionProcessResponse();
		if (errorCode != null) {
			response.setErrorCode(errorCode);
		} else
			response.setErrorCode("");
		response.setTransactionDetail(transactionDetail);

		return response;
	}

	@Override
	public SaleTransactionDocumentProcessResponse getSaleTransactionProcess(SaleTransactionProcessRequest request) {
		SaleTransaction trans = request.getSaleTransaction();
		SaleTransactionDocumentProcessResponse response = new SaleTransactionDocumentProcessResponse();

		String currentUser = "";
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			currentUser = authentication.getName();
		SaleTransactionDocument document = new SaleTransactionDocument();
		document.setSaleTransaction(new LinkedList<SaleTransaction>());
		document.getSaleTransaction().add(trans);

		try {
			if (trans != null)
				trans.setProcessMethod("API");

			saleTransactionProcess.process(document, true, currentUser);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ProcessAbortedException) {
				response.setErrorCode(((ProcessAbortedException) e).getErrorCode());
			} else {
				response.setErrorCode("PS99");
			}
			response.setErrorMessage(e.getMessage());
			logger.error("getSaleTransactionProcess failed!\n" + e.getMessage());
		} finally {
			// 5728
			try {
				if (document != null && request.getWriteFlag() != null && request.getWriteFlag().equals(1)) {

					SaleTransaction emptyTrans = new SaleTransaction();
					for (SaleTransaction processedTrans : document.getSaleTransaction()) {

						if (processedTrans != null) {
							//saleTransactionExDAO.save(processedTrans);
						}

						SaleTransactionLog detachedLog = processedTrans.getTransactionLog();

						if (detachedLog != null) {
							detachedLog.setSaletransId(processedTrans.getSaletransId());
							detachedLog.setLogSource("Sale Transaction Process API");
							detachedLog.setUpdateUserId(processedTrans.getUpdateUserId());
							detachedLog.setUpdateTimestamp(new Date());
							saleTransactionLogService.save(detachedLog);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("failed while persisting saletransaction\n" + e.getMessage());
				response.setErrorCode("PS99");
				response.setErrorMessage(e.getMessage());
			}

		}

		response.setSaleTransactionDocument(document);
		return response;
	}

	@Override
	public SaleTransactionDocumentProcessResponse getSaleTransactionDocumentProcess(
			SaleTransactionDocumentProcessRequest request) {
		SaleTransactionDocument doc = request.getSaleTransactionDocument();
		SaleTransactionDocumentProcessResponse response = new SaleTransactionDocumentProcessResponse();

		String currentUser = "";
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			currentUser = authentication.getName();
		// LinkedList<SaleTransactionLog> logs = new LinkedList<SaleTransactionLog>();
		try {
			/*
			 * if(doc != null) { if(doc.getSaleTransaction() != null) { for(SaleTransaction
			 * trans : doc.getSaleTransaction()) { if(trans != null) {
			 * 
			 * SaleTransactionLog newLog = new SaleTransactionLog();
			 * saleTransactionLogService.copyState(trans, newLog,
			 * SaleTransactionLogService.BEFORE_STATE); logs.add(newLog); } } } }
			 */
			saleTransactionProcess.process(doc, true, currentUser);

			for (SaleTransaction trans : doc.getSaleTransaction())
				trans.setProcessMethod("API");
			response.setSaleTransactionDocument(doc);

			if (doc != null && Integer.valueOf(1).equals(request.getWriteFlag())) {
				if (doc.getSaleTransaction() != null) {
					for (SaleTransaction trans : doc.getSaleTransaction()) {
						if (trans != null) {
							if (trans.getSaletransId() != null && trans.getSaletransId() == 0l)
								trans.setSaletransId(null);
							// trans.setProcessMethod("API");
							//saleTransactionExDAO.save(trans);
							SaleTransactionLog savedLog = trans.getTransactionLog();
							if (savedLog != null) {
								savedLog.setSaletransId(trans.getSaletransId());
								savedLog.setLogSource("Sale Transaction Document Process API");
								savedLog.setUpdateUserId(currentUser);
								savedLog.setUpdateTimestamp(new Date());
								saleTransactionLogService.save(savedLog);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof ProcessAbortedException) {
				response.setErrorCode(((ProcessAbortedException) e).getErrorCode());
			} else {
				response.setErrorCode("PS99");
			}
			response.setErrorMessage(e.getMessage());
		}

		return response;
	}

	// TODO Join purchase calls to a single 'route' method to reduce redundant code

	public PurchaseTransactionProcessResponse getPurchaseTransactionProcess(PurchaseTransactionProcessRequest request) {
		return getPurchaseTransactionProcess(request, LogSourceEnum.TransactionProcess);
	}
	
	@Override
	public PurchaseTransactionProcessResponse getPurchaseTransactionProcess(PurchaseTransactionProcessRequest request, LogSourceEnum callingFromMethod) {
		PurchaseTransactionProcessResponse response = new PurchaseTransactionProcessResponse();
		// return response;
		if (request == null)
			return null;
		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();

		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag()))
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction())
					transaction.getTransientTransaction().setRefreshCacheFlag(true);

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.normalProcess(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in normal processing. " + e.getMessage());
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
	
	public PurchaseTransactionProcessResponse getPurchaseTransactionTaxEstimate(
			PurchaseTransactionProcessRequest request) {
		return getPurchaseTransactionTaxEstimate(request, LogSourceEnum.PurchaseTransactionTaxEstimate);
				
	}

	@Override
	public PurchaseTransactionProcessResponse getPurchaseTransactionTaxEstimate(
			PurchaseTransactionProcessRequest request, LogSourceEnum callingFromMethod) {
		PurchaseTransactionProcessResponse response = new PurchaseTransactionProcessResponse();
		// return response;
		if (request == null)
			return null;
		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag()))
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction())
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
//keep a copy of original document with 5 trans
			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.taxEstimate(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in normal processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransEntityNexus(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();

			boolean errorFound = false;
			if (microApiTransactionDocument.getMicroApiTransaction() != null) {
				for (MicroApiTransaction microApiTransaction : microApiTransactionDocument.getMicroApiTransaction()) {
					try {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);

						purchaseTransactionService.getTransEntityNexus(microApiTransaction);
						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					} catch (PurchaseTransactionProcessingException e) {
						e.printStackTrace();
					}
				}
			}

			MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
			ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

			ptr.setErrorFlag(errorFound);

			return ptr;
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransEntity(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();

			boolean errorFound = false;
			if (microApiTransactionDocument.getMicroApiTransaction() != null) {
				for (MicroApiTransaction microApiTransaction : microApiTransactionDocument.getMicroApiTransaction()) {
					try {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);

						purchaseTransactionService.getTransEntity(microApiTransaction);
						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					} catch (PurchaseTransactionProcessingException e) {
						e.printStackTrace();
					}
				}
			}

			MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
			ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

			ptr.setErrorFlag(errorFound);

			return ptr;
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransLocation(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction purchaseTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							String overrideFlag = request.getOverrideSitusNexusFlag();
							PurchaseTransactionGetLocationRequest.LocationType locationType = request.getLocationType();
							PurchaseTransactionServiceConstants.LocationType passinLt;
							switch (locationType) {

							case BILLTO:
								passinLt = PurchaseTransactionServiceConstants.LocationType.BILLTO;
								break;

							case SHIPTO:
								passinLt = PurchaseTransactionServiceConstants.LocationType.SHIPTO;
								break;

							case TITLETRANSFER:
								passinLt = PurchaseTransactionServiceConstants.LocationType.TITLETRANSFER;
								break;

							case FIRSTUSE:
								passinLt = PurchaseTransactionServiceConstants.LocationType.FIRSTUSE;
								break;

							case SHIPFROM:
								passinLt = PurchaseTransactionServiceConstants.LocationType.SHIPFROM;
								break;

							case ORDERORIGIN:
								passinLt = PurchaseTransactionServiceConstants.LocationType.ORDERORIGIN;
								break;

							case ORDERACCEPT:
								passinLt = PurchaseTransactionServiceConstants.LocationType.ORDERACCEPT;
								break;

							default:
								passinLt = null;
								break;
							}

							purchaseTransactionService.getTransLocation(purchaseTransaction, passinLt, overrideFlag);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransGoodSvc(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction purchaseTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransGoodSvc(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;

	}

	@Override
	public MicroApiTransactionProcessResponse getTransTaxCodeDtl(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction microApiTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransTaxCodeDtl(microApiTransaction,
									microApiTransactionDocument);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							microApiTransaction.setTransientTransaction(null);

						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransTaxRates(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction microApiTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransTaxRates(microApiTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							microApiTransaction.setTransientTransaction(null);

						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	public PurchaseTransactionGetLocationResponse getBasicLocation(PurchaseTransactionGetLocationRequest request) {
		if (request != null) {
			PurchaseTransactionGetLocationResponse response = new PurchaseTransactionGetLocationResponse();
			PurchaseTransactionDocument document = request.getPurchaseTransactionDocument();
			PurchaseTransactionGetLocationRequest.LocationType locationType = request.getLocationType();
			LocationType passinLt;
			switch (locationType) {

			// BILLTO, SHIPTO, TITLETRANSFER, FIRSTUSE, SHIPFROM, ORDERORIGIN, ORDERACCEPT
			case BILLTO:
				passinLt = PurchaseTransactionServiceConstants.LocationType.BILLTO;
				break;

			case SHIPTO:
				passinLt = PurchaseTransactionServiceConstants.LocationType.SHIPTO;
				break;

			case TITLETRANSFER:
				passinLt = PurchaseTransactionServiceConstants.LocationType.TITLETRANSFER;
				break;

			case FIRSTUSE:
				passinLt = PurchaseTransactionServiceConstants.LocationType.FIRSTUSE;
				break;

			case SHIPFROM:
				passinLt = PurchaseTransactionServiceConstants.LocationType.SHIPFROM;
				break;

			case ORDERORIGIN:
				passinLt = PurchaseTransactionServiceConstants.LocationType.ORDERORIGIN;
				break;

			case ORDERACCEPT:
				passinLt = PurchaseTransactionServiceConstants.LocationType.ORDERACCEPT;
				break;

			default:
				passinLt = null;
				break;
			}
			PurchaseTransactionGetLocationRequest.QueryType queryType = request.getQueryTypeType();
			PurchaseTransaction purchaseTransaction = document.getPurchaseTransaction().get(0);

			MicroApiTransaction microApiTransaction = new MicroApiTransaction(purchaseTransaction);

			Jurisdiction jurResponse = null;
			try {
				if (queryType == PurchaseTransactionGetLocationRequest.QueryType.RATEPOINT) {
					// jurResponse = purchaseTransactionService.getJurViaRatepoint(pt, passinLt);
				} else {
					jurResponse = purchaseTransactionService.getJurViaLocationMatrix(microApiTransaction, passinLt);
				}
			} catch (PurchaseTransactionSuspendedException e) {
				e.printStackTrace();
			}
			response.setJurisdiction(jurResponse);
		}

		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransCalcAllTaxes(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction microApiTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransCalcAllTaxes(microApiTransaction,
									microApiTransactionDocument);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							microApiTransaction.setTransientTransaction(null);

						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionInvoiceVerificationResponse getPurchaseTransactionInvoiceVerification(
			PurchaseTransactionInvoiceVerificationRequest request) {

		PurchaseTransactionInvoiceVerificationResponse response = new PurchaseTransactionInvoiceVerificationResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionInvoiceVerification(microApiTransactionDocument,
						writeFlag, LogSourceEnum.getPurchaseTransactionInvoiceVerification); 
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in normal processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionReverseResponse getPurchaseTransactionReverse(PurchaseTransactionReverseRequest request) {

		PurchaseTransactionReverseResponse response = new PurchaseTransactionReverseResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionReverse(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in getPurchaseTransactionReverse processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionLoadAndReverseResponse getPurchaseTransactionLoadAndReverse(
			PurchaseTransactionLoadAndReverseRequest request) {

		PurchaseTransactionLoadAndReverseResponse response = new PurchaseTransactionLoadAndReverseResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionLoadAndReverse(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in getPurchaseTransactionLoadAndReverse processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionEvaluateForAccrualResponse getPurchaseTransactionEvaluateForAccrual(
			PurchaseTransactionEvaluateForAccrualRequest request) {

		PurchaseTransactionEvaluateForAccrualResponse response = new PurchaseTransactionEvaluateForAccrualResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionEvaluateForAccrual(microApiTransactionDocument,
						writeFlag, LogSourceEnum.getPurchaseTransactionEvaluateForAccrual);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in getPurchaseTransactionEvaluateForAccrual processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
	
	public BillTransactionProcessResponse getBillTransactionProcess(BillTransactionProcessRequest request) {
		return getBillTransactionProcess(request, LogSourceEnum.BillTransactionProcess);
	}

	@Override
	public BillTransactionProcessResponse getBillTransactionProcess(BillTransactionProcessRequest request, LogSourceEnum callingFromMethod) {

		BillTransactionProcessResponse response = new BillTransactionProcessResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		BillTransactionDocument transactionDocument = request.getBillTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (BillTransaction transaction : transactionDocument.getBillTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument, "BillTransactionProcess");
				purchaseTransactionService.billTransactionProcess(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updateBillTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error occured in getPurchaseTransactionEvaluateForAccrual processing. " + e.getMessage());
			}

			boolean errorFound = false;

			for (BillTransaction transaction : transactionDocument.getBillTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound)
				response.setErrorFlag(errorFound);

			response.setBillTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	public MicroApiTransactionProcessResponse getTransStatus(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction microApiTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransStatus(microApiTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							microApiTransaction.setTransientTransaction(null);

						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	public MicroApiTransactionProcessResponse getTransFullExemption(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction microApiTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							microApiTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransFullExemption(microApiTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							microApiTransaction.setTransientTransaction(null);

						if (microApiTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionProcessResponse getTransSuspend(PurchaseTransactionProcessRequest request) {
		if (request != null) {
			try {
				PurchaseTransactionDocument purchaseTransactionDocument = request.getPurchaseTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (purchaseTransactionDocument.getPurchaseTransaction() != null) {
					for (PurchaseTransaction purchaseTransaction : purchaseTransactionDocument
							.getPurchaseTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransSuspend(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				PurchaseTransactionProcessResponse ptr = new PurchaseTransactionProcessResponse();
				ptr.setPurchaseTransactionDocument(purchaseTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransSitus(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction purchaseTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getSitus(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionProcessResponse getTransReprocess(PurchaseTransactionProcessRequest request) {
		if (request != null) {
			try {
				PurchaseTransactionDocument purchaseTransactionDocument = request.getPurchaseTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (purchaseTransactionDocument.getPurchaseTransaction() != null) {
					for (PurchaseTransaction purchaseTransaction : purchaseTransactionDocument
							.getPurchaseTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransReprocess(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				PurchaseTransactionProcessResponse ptr = new PurchaseTransactionProcessResponse();
				ptr.setPurchaseTransactionDocument(purchaseTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public BatchProcessResponse getPurchaseTransactionProcessBatch(BatchProcessRequest request) {
		if (request != null) {
			try {
				Long processBatchNo = request.getProcessBatchNo();
				String defaultFlag = request.getDefaultMatrixFlag();

				boolean errorFound = false;
				String errorCode = "";
				BatchMaintenance responseBatch = new BatchMaintenance(processBatchNo);
				try {
					purchaseTransactionService.getPurchaseTransactionProcessBatch(responseBatch, defaultFlag);
				} catch (Exception e) {
					e.printStackTrace();
					errorCode = e.getMessage();
					errorFound = true;
				}

				BatchProcessResponse bpr = new BatchProcessResponse();
				bpr.setErrorFlag(errorFound);
				bpr.setErrorCode(errorCode);

				String errorText = "";
				if (errorCode != null && errorCode.length() > 0) {
					List<ListCodes> listCodes = listCodesService.getListCodesByTypeAndCode("ERRORCODE", errorCode);
					if (listCodes != null && listCodes.size() > 0) {
						errorText = listCodes.get(0).getDescription();
					}
				}

				bpr.setErrorText(errorText); // P9 -> Batch not Flagged for Processing
				bpr.setBatchStatusCode(responseBatch.getBatchStatusCode());
				bpr.setTotalRows(responseBatch.getTotalRows());

				return bpr;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionProcessResponse getTransVariance(PurchaseTransactionProcessRequest request) {
		if (request != null) {
			try {
				PurchaseTransactionDocument purchaseTransactionDocument = request.getPurchaseTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (purchaseTransactionDocument.getPurchaseTransaction() != null) {
					for (PurchaseTransaction purchaseTransaction : purchaseTransactionDocument
							.getPurchaseTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransVariance(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				PurchaseTransactionProcessResponse ptr = new PurchaseTransactionProcessResponse();
				ptr.setPurchaseTransactionDocument(purchaseTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionProcessResponse getTransRelease(PurchaseTransactionProcessRequest request) {
		if (request != null) {
			try {
				PurchaseTransactionDocument purchaseTransactionDocument = request.getPurchaseTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (purchaseTransactionDocument.getPurchaseTransaction() != null) {
					for (PurchaseTransaction purchaseTransaction : purchaseTransactionDocument
							.getPurchaseTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransRelease(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				PurchaseTransactionProcessResponse ptr = new PurchaseTransactionProcessResponse();
				ptr.setPurchaseTransactionDocument(purchaseTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public PurchaseTransactionProcessResponse getTransReverse(PurchaseTransactionProcessRequest request) {
		if (request != null) {
			try {
				PurchaseTransactionDocument purchaseTransactionDocument = request.getPurchaseTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (purchaseTransactionDocument.getPurchaseTransaction() != null) {
					for (PurchaseTransaction purchaseTransaction : purchaseTransactionDocument
							.getPurchaseTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransReverse(purchaseTransaction);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				PurchaseTransactionProcessResponse ptr = new PurchaseTransactionProcessResponse();
				ptr.setPurchaseTransactionDocument(purchaseTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public MicroApiTransactionProcessResponse getTransVendorNexus(MicroApiTransactionProcessRequest request) {
		if (request != null) {
			try {
				MicroApiTransactionDocument microApiTransactionDocument = request.getMicroApiTransactionDocument();
				Integer debugMode = request.getDebugFlag();
				boolean errorFound = false;
				if (microApiTransactionDocument.getMicroApiTransaction() != null) {
					for (MicroApiTransaction purchaseTransaction : microApiTransactionDocument
							.getMicroApiTransaction()) {
						if ("1".equals(request.getRefreshCacheFlag()))
							purchaseTransaction.getTransientTransaction().setRefreshCacheFlag(true);
						try {
							purchaseTransactionService.getTransVendorNexus(purchaseTransaction, false);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
							errorFound = true;
						}
						if (debugMode == null || debugMode.equals(0))
							purchaseTransaction.setTransientTransaction(null);

						if (purchaseTransaction.getErrorCode() != null)
							errorFound = true;
					}
				}

				MicroApiTransactionProcessResponse ptr = new MicroApiTransactionProcessResponse();
				ptr.setMicroApiTransactionDocument(microApiTransactionDocument);

				ptr.setErrorFlag(errorFound);

				return ptr;
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return null;
	}

	@Override
	public JurisdictionResponse getJurisdiction(JurisdictionRequest request) {

		String geocode = request.getGeocode();
		String country = request.getCountry();
		String state = request.getState();
		String county = request.getCounty();
		String city = request.getCity();
		String zip = request.getZip();
		String zipplus4 = request.getZipplus4();
		String streetAddress1 = request.getStreetAddress1();
		String streetAddress2 = request.getStreetAddress2();
		String latitude = request.getLatitude();
		String longitude = request.getLongitude();
		String geoRecon = request.getGeoRecon();
		String entityCode = request.getEntityCode();

		String errorCode = "";

		List<Jurisdiction> jurResponse = null;
		try {
			jurResponse = purchaseTransactionService.getJurisdictionViaRatepoint(country, state, county, city, zip,
					streetAddress1, streetAddress2, latitude, longitude, null);
		} catch (PurchaseTransactionSuspendedException e) {
			e.printStackTrace();
		}

		JurisdictionResponse response = new JurisdictionResponse();
		String notes = "";
		if (jurResponse != null) {
			List<JurisdictionDTO> jurArrayReturn = new ArrayList<JurisdictionDTO>();
			for(Jurisdiction jurisdiction : jurResponse) {
				JurisdictionDTO jurReturn = new JurisdictionDTO();
				notes = jurisdiction.getNotes();
				BeanUtils.copyProperties(jurisdiction, jurReturn);
				jurArrayReturn.add(jurReturn);
				
			}
			response.setNotes(notes);
			response.setJurisdiction(jurArrayReturn);

			return response;
		}

		// 1. geoRecon
		// 2. entityCode
		// 3. GEORECON/SYSTEM/SYSTEM
		// 4. 1
		int geoReconInt = 0;
		notes = "Jurisdiction Via PinPoint-GeoRecon=";

		// geoRecon from request
		if (geoRecon != null && geoRecon.trim().length() > 0) {
			try {
				geoReconInt = Integer.parseInt(geoRecon.trim());
				if (geoReconInt >= 1 && geoReconInt <= 12) {
					notes = notes + "ValuePassed:" + geoReconInt;
				} else {
					geoReconInt = 0;
				}
			} catch (NumberFormatException e) {
			}
		}

		if (geoReconInt == 0 && entityCode != null && entityCode.trim().length() > 0) {
			// entityCode from request
			EntityItem entityItem = entityItemService.findByCode(entityCode.trim());
			if (entityItem != null) {
				Long entityId = entityItem.getEntityId();
				List<EntityDefault> entityDefaults = entityDefaultService.findEntityDefaultsByDefaultCode(entityId,
						"GEORECON");
				if (entityDefaults != null && entityDefaults.size() > 0) {
					EntityDefault entityDefault = (EntityDefault) entityDefaults.get(0);
					try {
						geoReconInt = Integer.parseInt(entityDefault.getDefaultValue());
						if (geoReconInt >= 1 && geoReconInt <= 12) {
							notes = notes + "EntityDefaultValue:" + geoReconInt;
						} else {
							geoReconInt = 0;
						}
					} catch (NumberFormatException e) {
					}
				}
			}
		}

		if (geoReconInt == 0) {
			String geoReconOption = optionService.findByPKSafe(new OptionCodePK("GEORECON", "SYSTEM", "SYSTEM"));
			if (geoReconOption != null && geoReconOption.trim().length() > 0) {
				try {
					geoReconInt = Integer.parseInt(geoReconOption.trim());
					if (geoReconInt >= 1 && geoReconInt <= 12) {
						notes = notes + "Preference/Options:" + geoReconInt;
					} else {
						geoReconInt = 0;
					}
				} catch (NumberFormatException e) {
				}
			}
		}

		if (geoReconInt >= 1 && geoReconInt <= 12) {
		} else {
			geoReconInt = 1;
			notes = notes + "Default:" + geoReconInt;
		}

		List<JurisdictionDTO> jurArrayReturn = null;
		try {
			if (StringUtils.hasText(geocode) || StringUtils.hasText(country) || StringUtils.hasText(state)
					|| StringUtils.hasText(county) || StringUtils.hasText(city) || StringUtils.hasText(zip)
					|| StringUtils.hasText(zipplus4)) {
				Jurisdiction jurisdiction = jurisdictionDAO.getJurisdictionFromAPI(geocode, country, state, county, city, zip,
						zipplus4, "" + geoReconInt);
				if (jurisdiction.getJurisdictionId() != null && jurisdiction.getJurisdictionId().longValue() != 0L) {
					jurArrayReturn = new ArrayList<JurisdictionDTO>();
					JurisdictionDTO jurReturn = new JurisdictionDTO();
					BeanUtils.copyProperties(jurResponse, jurReturn);
					jurArrayReturn.add(jurReturn);
				} else {
					notes = "Jurisdiction: not found";
				}
			} else {
				notes = "Jurisdiction: not found";
			}
		} catch (Exception e) {
			notes = "Jurisdiction: error";
			errorCode = e.getMessage();
			e.printStackTrace();
		}

		response.setJurisdiction(jurArrayReturn);
		response.setErrorCode(errorCode);
		response.setNotes(notes);

		return response;
	}
	
	public PurchaseTransactionPushProrateResponse getPurchaseTransactionPushProrate(
			PurchaseTransactionPushProrateRequest request) {
		return getPurchaseTransactionPushProrate(request, LogSourceEnum.PushProrate);
	}

	@Override
	public PurchaseTransactionPushProrateResponse getPurchaseTransactionPushProrate(
			PurchaseTransactionPushProrateRequest request, LogSourceEnum callingFromMethod) {
		PurchaseTransactionPushProrateResponse response = new PurchaseTransactionPushProrateResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			transactionDocument.setPushTaxInd("P1");
			if (transactionDocument.getPushTaxAmt() == null) {
				transactionDocument.setPushTaxAmt(new BigDecimal(0));
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.taxEstimate(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
	
	public PurchaseTransactionPushPreferenceResponse getPurchaseTransactionPushPreference(
			PurchaseTransactionPushPreferenceRequest request) {
		return getPurchaseTransactionPushPreference(request, LogSourceEnum.PushPreference);
	}
		 

	@Override
	public PurchaseTransactionPushPreferenceResponse getPurchaseTransactionPushPreference(
			PurchaseTransactionPushPreferenceRequest request, LogSourceEnum callingFromMethod) {
		PurchaseTransactionPushPreferenceResponse response = new PurchaseTransactionPushPreferenceResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			transactionDocument.setPushTaxInd("P2");
			if (transactionDocument.getPushTaxAmt() == null) {
				transactionDocument.setPushTaxAmt(new BigDecimal(0));
			}

			if (transactionDocument.getPushTaxPreference() == null) {
				transactionDocument.setPushTaxPreference(PurchaseTransactionProcessRequest.PushPreference.STATE);
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.taxEstimate(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
	
	public PurchaseTransactionPushSpecificResponse getPurchaseTransactionPushSpecific(
			PurchaseTransactionPushSpecificRequest request) {
		return getPurchaseTransactionPushSpecific(request, LogSourceEnum.PushSpecific); 
				
	}

	@Override
	public PurchaseTransactionPushSpecificResponse getPurchaseTransactionPushSpecific(
			PurchaseTransactionPushSpecificRequest request, LogSourceEnum callingFromMethod) {
		PurchaseTransactionPushSpecificResponse response = new PurchaseTransactionPushSpecificResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			transactionDocument.setPushTaxInd("P3");
			if (transactionDocument.getPushTaxAmt() == null) {
				transactionDocument.setPushTaxAmt(new BigDecimal(0));
			}

			if (transactionDocument.getPushTaxPreference() == null) {
				transactionDocument.setPushTaxPreference(PurchaseTransactionProcessRequest.PushPreference.STATE);
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.taxEstimate(microApiTransactionDocument, writeFlag, callingFromMethod);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionLoadAndReprocessResponse getPurchaseTransactionLoadAndReprocess(
			PurchaseTransactionLoadAndReprocessRequest request) {
		PurchaseTransactionLoadAndReprocessResponse response = new PurchaseTransactionLoadAndReprocessResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionLoadAndReprocess(microApiTransactionDocument,
						writeFlag, LogSourceEnum.getPurchaseTransactionLoadAndReprocess);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
	
	private List<Long> collectProcessedTransactionIds(List<PurchaseTransaction> transactions){
		List<Long> transactionIds = new ArrayList<Long>();
		
		for(PurchaseTransaction transaction : transactions) {
			transactionIds.add(transaction.getId());
		}
		
		return transactionIds;
	}

	@Override
	public PurchaseTransactionProcessSuspendedResponse getPurchaseTransactionProcessSuspended(
			PurchaseTransactionProcessSuspendedRequest request) {
		PurchaseTransactionProcessSuspendedResponse result = new PurchaseTransactionProcessSuspendedResponse();
		try {
			ProcessSuspendedTransactionDocument document = purchaseTransactionService.processSuspendedTransactions(
					request.getSuspendNewId(), request.getSuspendType(), request.getWriteFlag());
			
			result.setTransactionsProcessed(document.getProcessedTransactions());
			
			List<ProcessedTransactionIdentifier> processedTransactions = new ArrayList<ProcessedTransactionIdentifier>();
			Boolean debugFlag = request.getDebugFlag();
			
			//Run through list of PurchaseTransactions. 
			//Put the Ids of each one into the list
			//If errors, put those there too.
			
			for(PurchaseTransaction transaction : document.getPurchaseTransaction()) {
				ProcessedTransactionIdentifier processedTransaction = new ProcessedTransactionIdentifier();
				Boolean errorFlag = transaction.getErrorCode() != null;
				processedTransaction.setId(transaction.getId());

				if(debugFlag) processedTransaction.setPurchaseTransaction(transaction);

				
				if(errorFlag) {
					processedTransaction.setErrorFlag(true);
					processedTransaction.setErrorCode(transaction.getErrorCode());
					processedTransaction.setErrorText(transaction.getErrorText());
				}
				
				processedTransactions.add(processedTransaction);
			}
			
			result.setProcessedTransactionIds(processedTransactions);
			
			if(request.getDebugFlag()) {
				result.setSuspendedTransactionSql(document.getLocationTransSql());
			}
			
		} catch (PurchaseTransactionProcessSuspendedException e) {
			
			result.setErrorFlag(true);
			result.setErrorCode(e.getErrorCode().getCodeCode());
			result.setErrorText(e.getErrorCode().getDescription());
			if(request.getDebugFlag()) {
				result.setSuspendedTransactionSql(e.getMessage());
			}
		}
		
		catch (PurchaseTransactionProcessingException e) {
			result.setErrorFlag(true);
			result.setErrorText(e.getMessage());
		}
		return result;
	}

	@Override
	public PurchaseTransactionReprocessResponse getPurchaseTransactionReprocess(
			PurchaseTransactionReprocessRequest request) {
		PurchaseTransactionReprocessResponse response = new PurchaseTransactionReprocessResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionReprocess(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionReleaseResponse getPurchaseTransactionRelease(PurchaseTransactionReleaseRequest request) {
		PurchaseTransactionReleaseResponse response = new PurchaseTransactionReleaseResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionRelease(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionLoadAndReleaseResponse getPurchaseTransactionLoadAndRelease(
			PurchaseTransactionLoadAndReleaseRequest request) {
		PurchaseTransactionLoadAndReleaseResponse response = new PurchaseTransactionLoadAndReleaseResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionLoadAndRelease(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}

	@Override
	public PurchaseTransactionSuspendResponse getPurchaseTransactionSuspend(PurchaseTransactionSuspendRequest request) {
		if (request == null)
			return null;
		String errorCode = null;
		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionSuspend(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				errorCode = e.getMessage();
			}

			return buildSuspendResponse(transactionDocument, debugMode, errorCode);
		}
		return null;
	}

	@Override
	public PurchaseTransactionSuspendResponse getPurchaseTransactionLoadAndSuspend(
			PurchaseTransactionLoadAndSuspendRequest request) {
		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		String errorCode = null;
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		Long matrixId = request.getMatrixId();

		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				
				if(matrixId != null && microApiTransactionDocument.getMicroApiTransaction().get(0).getPurchtransId() == null) {
					purchaseTransactionService.suspendByMatrixId(matrixId, microApiTransactionDocument, writeFlag);
				} else {
					purchaseTransactionService.getPurchaseTransactionLoadAndSuspend(microApiTransactionDocument, writeFlag);

				}
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
				errorCode = e.getMessage();
			}

			

			return buildSuspendResponse(transactionDocument, debugMode, errorCode);
		}
		return null;
	}
	
	private PurchaseTransactionSuspendResponse buildSuspendResponse(PurchaseTransactionDocument transactionDocument, Integer debugMode, String errorCode) {
		PurchaseTransactionSuspendResponse response = new PurchaseTransactionSuspendResponse();

//		for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
//			if (debugMode == null || debugMode.equals(0))
//				transaction.setTransientTransaction(null);
//		}

		for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
			if (transaction.getErrorCode() != null) {
				errorCode = transaction.getErrorCode();
				break;
			}
		}

		if (errorCode != null) {

			List<ListCodes> listCodes = listCodesService.getListCodesByTypeAndCode("ERRORCODE", errorCode);
			String errorText = listCodes.get(0).getDescription();

			response.setErrorFlag(true);
			response.setErrorCode(errorCode);
			response.setErrorText(errorText);
		}
		
		List<ProcessedTransactionIdentifier> processedTransactions = new ArrayList<ProcessedTransactionIdentifier>();

		for(PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
			ProcessedTransactionIdentifier processedTransaction = new ProcessedTransactionIdentifier();
			Boolean errorFlag = transaction.getErrorCode() != null;
			processedTransaction.setId(transaction.getId());

			if(!(debugMode == null || debugMode.equals(0)))  
				processedTransaction.setPurchaseTransaction(transaction);

			
			if(errorFlag) {
				processedTransaction.setErrorFlag(true);
				processedTransaction.setErrorCode(transaction.getErrorCode());
				processedTransaction.setErrorText(transaction.getErrorText());
			}
			
			processedTransactions.add(processedTransaction);
		}
		
		response.setProcessedTransactionIds(processedTransactions);

//		response.setPurchaseTransactionDocument(transactionDocument);
		return response;
	}

	@Override
	public PurchaseTransactionLoadResponse getPurchaseTransactionLoad(PurchaseTransactionLoadRequest request) {
		PurchaseTransactionLoadResponse response = new PurchaseTransactionLoadResponse();

		if (request == null)
			return null;

		Boolean writeFlag = ((request.getWriteflag() == null || request.getWriteflag().equals(new Integer(0))))
				? Boolean.FALSE
				: Boolean.TRUE;
		Integer debugMode = request.getDebugFlag();
		PurchaseTransactionDocument transactionDocument = request.getPurchaseTransactionDocument();
		if (transactionDocument != null) {
			if ("1".equals(request.getRefreshCacheFlag())) {
				for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
					transaction.getTransientTransaction().setRefreshCacheFlag(true);
				}
			}

			try {
				MicroApiTransactionDocument microApiTransactionDocument = new MicroApiTransactionDocument(
						transactionDocument);
				purchaseTransactionService.getPurchaseTransactionLoad(microApiTransactionDocument, writeFlag);
				microApiTransactionDocument.updatePurchaseTransactionDocument(transactionDocument);
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean errorFound = false;
			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (debugMode == null || debugMode.equals(0))
					transaction.setTransientTransaction(null);
			}

			for (PurchaseTransaction transaction : transactionDocument.getPurchaseTransaction()) {
				if (transaction.getErrorCode() != null) {
					errorFound = true;
					break;
				}
			}

			if (errorFound) {
				response.setErrorFlag(errorFound);
			}

			response.setPurchaseTransactionDocument(transactionDocument);
			return response;
		}
		return null;
	}
}
