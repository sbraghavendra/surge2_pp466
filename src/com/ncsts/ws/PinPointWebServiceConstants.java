package com.ncsts.ws;

public interface PinPointWebServiceConstants {
	public static final String MESSAGES_NAMESPACE = "http://www.seconddecimal.com/ncsts/schemas/messages";
	public static final String MESSAGES_UNRESTRICTED_NAMESPACE = "http://www.seconddecimal.com/ncsts/schemas/unrestricted/messages";
	
	public static final String PING_REQUEST = "PingRequest";
	public static final String PING_RESPONSE = "PingResponse";
	public static final String ECHO_REQUEST = "EchoRequest";
	public static final String ECHO_RESPONSE = "EchoResponse";
	public static final String VERSION_REQUEST = "VersionRequest";
	public static final String VERSION_RESPONSE = "VersionResponse";
	public static final String BATCH_STATUS_REQUEST = "BatchStatusRequest";
	public static final String BATCH_STATUS_RESPONSE = "BatchStatusResponse";
	public static final String REALTIME_TRANSACTION_PROCESS_REQUEST = "RealTimeTransactionProcessRequest";
	public static final String REALTIME_TRANSACTION_PROCESS_RESPONSE = "RealTimeTransactionProcessResponse";


	String STATUS_REQUEST = "StatusRequest";
	String STATUS_RESPONSE = "StatusResponse";

	//Billing API
	public static final String SALE_TRANSACTION_PROCESS_REQUEST = "SaleTransactionProcessRequest";
	public static final String SALE_TRANSACTION_PROCESS_RESPONSE = "SaleTransactionProcessResponse";
	public static final String SALE_TRANSACTION_DOCUMENT_PROCESS_REQUEST = "SaleTransactionDocumentProcessRequest";
	public static final String SALE_TRANSACTION_DOCUMENT_PROCESS_RESPONSE = "SaleTransactionDocumentProcessResponse";
	
	//Surge (new bill) API
	String MICROAPI_TRANSACTION_PROCESS_REQUEST = "MicroApiTransactionProcessRequest";
	String MICROAPI_TRANSACTION_PROCESS_RESPONSE = "MicroApiTransactionProcessResponse";
	String BILL_TRANSACTION_PROCESS_REQUEST = "BillTransactionProcessRequest";
	String BILL_TRANSACTION_PROCESS_RESPONSE = "BillTransactionProcessResponse";

	//Surge (new purchasing) API
	String PURCHASE_TRANSACTION_PROCESS_REQUEST = "PurchaseTransactionProcessRequest";
	String PURCHASE_TRANSACTION_PROCESS_RESPONSE = "PurchaseTransactionProcessResponse";
	
	String BATCH_PROCESS_REQUEST = "BatchProcessRequest";
	String BATCH_PROCESS_RESPONSE = "BatchProcessResponse";

	String PURCHASE_TRANSACTION_TAX_ESTIMATE_REQUEST = "PurchaseTransactionTaxEstimateRequest";
	String PURCHASE_TRANSACTION_TAX_ESTIMATE_RESPONSE = "PurchaseTransactionTaxEstimateResponse";

	String PURCHASE_TRANSACTION_GET_LOCATION_REQUEST = "PurchaseTransactionGetLocationRequest";
	String PURCHASE_TRANSACTION_GET_LOCATION_RESPONSE = "PurchaseTransactionGetLocationResponse";
	
	String JURISDICTION_REQUEST = "JurisdictionRequest";
	String JURISDICTION_RESPONSE = "JurisdictionResponse";

	String PURCHASE_TRANSACTION_REVERSE_REQUEST = "PurchaseTransactionReverseRequest";
	String PURCHASE_TRANSACTION_REVERSE_RESPONSE = "PurchaseTransactionReverseResponse";
	
	String PURCHASE_TRANSACTION_LOADANDREVERSE_REQUEST = "PurchaseTransactionLoadAndReverseRequest";
	String PURCHASE_TRANSACTION_LOADANDREVERSE_RESPONSE = "PurchaseTransactionLoadAndReverseResponse";
	
	String PURCHASE_TRANSACTION_VERIFICATION_REQUEST = "PurchaseTransactionInvoiceVerificationRequest";
	String PURCHASE_TRANSACTION_LVERIFICATION_RESPONSE = "PurchaseTransactionInvoiceVerificationResponse";
	
	String PURCHASE_TRANSACTION_EVALUATIONFORACCRUAL_REQUEST = "PurchaseTransactionEvaluateForAccrualRequest";
	String PURCHASE_TRANSACTION_EVALUATIONFORACCRUAL_RESPONSE = "PurchaseTransactionEvaluateForAccrualResponse";

	String PURCHASE_TRANSACTION_PUSH_PRORATE_REQUEST = "PurchaseTransactionPushProrateRequest";
	String PURCHASE_TRANSACTION_PUSH_PRORATE_RESPONSE = "PurchaseTransactionPushProrateResponse";
	
	String PURCHASE_TRANSACTION_PUSH_PREFERENCE_REQUEST = "PurchaseTransactionPushPreferenceRequest";
	String PURCHASE_TRANSACTION_PUSH_PREFERENCE_RESPONSE = "PurchaseTransactionPushPreferenceResponse";

	String PURCHASE_TRANSACTION_PUSH_SPECIFIC_REQUEST = "PurchaseTransactionPushSpecificRequest";
	String PURCHASE_TRANSACTION_PUSH_SPECIFIC_RESPONSE = "PurchaseTransactionPushSpecificResponse";

	String PURCHASE_TRANSACTION_LOADANDREPROCESS_REQUEST = "PurchaseTransactionLoadAndReprocessRequest";
	String PURCHASE_TRANSACTION_LOADANDREPROCESS_RESPONSE = "PurchaseTransactionLoadAndReprocessResponse";

	String PURCHASE_TRANSACTION_PROCESSSUSPENDED_REQUEST = "PurchaseTransactionProcessSuspendedRequest";
	String PURCHASE_TRANSACTION_PROCESSSUSPENDED_RESPONSE = "PurchaseTransactionProcessSuspendedResponse";
	
	String PURCHASE_TRANSACTION_REPROCESS_REQUEST = "PurchaseTransactionReprocessRequest";
	String PURCHASE_TRANSACTION_REPROCESS_RESPONSE = "PurchaseTransactionReprocessResponse";
	
	String PURCHASE_TRANSACTION_RELEASE_REQUEST = "PurchaseTransactionReleaseRequest";
	String PURCHASE_TRANSACTION_RELEASE_RESPONSE = "PurchaseTransactionReleaseResponse";
	
	String PURCHASE_TRANSACTION_LOADANDRELEASE_REQUEST = "PurchaseTransactionLoadAndReleaseRequest";
	String PURCHASE_TRANSACTION_LOADANDRELEASE_RESPONSE = "PurchaseTransactionLoadAndReleaseResponse";
	
	String PURCHASE_TRANSACTION_SUSPEND_REQUEST = "PurchaseTransactionSuspendRequest";
	String PURCHASE_TRANSACTION_SUSPEND_RESPONSE = "PurchaseTransactionSuspendResponse";
	
	String PURCHASE_TRANSACTION_LOADANDSUSPEND_REQUEST = "PurchaseTransactionLoadAndSuspendRequest";
	String PURCHASE_TRANSACTION_LOADANDSUSPEND_RESPONSE = "PurchaseTransactionLoadAndSuspendResponse";
	
	String PURCHASE_TRANSACTION_LOAD_REQUEST = "PurchaseTransactionLoadRequest";
	String PURCHASE_TRANSACTION_LOAD_RESPONSE = "PurchaseTransactionLoadResponse";
	
	
}
