package com.ncsts.ws.message;

import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESSSUSPENDED_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESSSUSPENDED_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionProcessSuspendedRequest {
    @XmlElement(required = true)
    private Long suspendNewId;
    @XmlElement(required = true)
    private PurchaseTransactionServiceConstants.SuspendReason suspendType;
    @XmlElement(required = false, defaultValue="true")
    private Boolean writeFlag = true;
    @XmlElement(required = false, defaultValue="false")
    private Boolean debugFlag = false;

    public Boolean getDebugFlag() {
		return debugFlag;
	}

	public void setDebugFlag(Boolean debugFlag) {
		this.debugFlag = debugFlag;
	}

	public Long getSuspendNewId() {
        return suspendNewId;
    }

    public void setSuspendNewId(Long suspendNewId) {
        this.suspendNewId = suspendNewId;
    }

    public PurchaseTransactionServiceConstants.SuspendReason getSuspendType() {
        return suspendType;
    }

    public void setSuspendType(PurchaseTransactionServiceConstants.SuspendReason suspendType) {
        this.suspendType = suspendType;
    }

    public Boolean getWriteFlag() {
        return writeFlag;
    }

    public void setWriteFlag(Boolean writeFlag) {
        this.writeFlag = writeFlag;
    }
}
