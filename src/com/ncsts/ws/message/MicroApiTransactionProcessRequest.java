package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.message.PurchaseTransactionGetLocationRequest.LocationType;
import javax.xml.bind.annotation.*;


@XmlRootElement(name = PinPointWebServiceConstants.MICROAPI_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.MICROAPI_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class MicroApiTransactionProcessRequest{

    @XmlElement(required = true)
    private MicroApiTransactionDocument microApiTransactionDocument;
    @XmlElement(required = false)
    private Integer writeFlag;
    @XmlElement(required = false)
    private Integer debugFlag;
    @XmlElement(required = false)
    private String refreshCacheFlag;

    @XmlElement(required = false)
    private String overrideSitusNexusFlag;
    
    @XmlElement(required = true)
    private LocationType locationType;

    //@XmlType(namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    //@XmlEnum(String.class)
    //public enum PushPreference {STATE, COUNTY, CITY, STJ1, STJ2, STJ3, STJ4, STJ5, STJ6, STJ7, STJ8, STJ9, STJ10}



    public Integer getWriteflag() {
        return writeFlag;
    }

    public void setWriteflag(Integer writeflag) {
        this.writeFlag = writeflag;
    }

    public MicroApiTransactionDocument getMicroApiTransactionDocument() {
        return microApiTransactionDocument;
    }

    public void setMicroApiTransactionDocument(MicroApiTransactionDocument microApiTransactionDocument) {
        this.microApiTransactionDocument = microApiTransactionDocument;
    }

    public Integer getDebugFlag() {
        return debugFlag;
    }

    public void setDebugFlag(Integer debugFlag) {
        this.debugFlag = debugFlag;
    }

    public String getRefreshCacheFlag() {
        return refreshCacheFlag;
    }

    public void setRefreshCacheFlag(String refreshCacheFlag) {
        this.refreshCacheFlag = refreshCacheFlag;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }
    
    public String getOverrideSitusNexusFlag() {
        return overrideSitusNexusFlag;
    }

    public void setOverrideSitusNexusFlag(String overrideSitusNexusFlag) {
        this.overrideSitusNexusFlag = overrideSitusNexusFlag;
    }



}
