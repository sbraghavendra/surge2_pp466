package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.domain.PurchaseTransaction;

import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.REALTIME_TRANSACTION_PROCESS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.REALTIME_TRANSACTION_PROCESS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class RealTimeTransactionProcessResponse {
	@XmlElement(required = true)
	private String errorCode;


	@XmlElement(required = false)
	private PurchaseTransaction transactionDetail;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public PurchaseTransaction getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(PurchaseTransaction transactionDetail) {
		this.transactionDetail = transactionDetail;
	}
}
