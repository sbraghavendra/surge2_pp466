package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name= PinPointWebServiceConstants.STATUS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.STATUS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusResponse {
    @XmlElement(required = true)
    private String applicationVersion;
    @XmlElement(required = true)
    private String salesModuleVersion;
    @XmlElement(required = true)
    private String databaseVersion;
    @XmlElement(required = true)
    private String uptime;



   /* public StatusResponse(String applicationVersion, String salesModuleVersion, String databaseVersion, String uptime) {
        this.applicationVersion = applicationVersion;
        this.salesModuleVersion = salesModuleVersion;
        this.databaseVersion = databaseVersion;
        this.uptime = uptime;
    }*/

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getSalesModuleVersion() {
        return salesModuleVersion;
    }

    public void setSalesModuleVersion(String salesModuleVersion) {
        this.salesModuleVersion = salesModuleVersion;
    }

    public String getDatabaseVersion() {
        return databaseVersion;
    }

    public void setDatabaseVersion(String databaseVersion) {
        this.databaseVersion = databaseVersion;
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }
}
