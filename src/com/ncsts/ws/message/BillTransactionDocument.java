package com.ncsts.ws.message;

import com.ncsts.domain.BillTransaction;
import com.ncsts.dto.TransientPurchaseTransactionDocument;

import com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

@XmlRootElement(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlType(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class BillTransactionDocument {
    private List<BillTransaction> billTransaction;
    
    private String pushTaxInd;

    private BigDecimal pushTaxAmt;

    private BigDecimal pushDelta;

    private PushPreference pushTaxPreference;

    public TransientPurchaseTransactionDocument getTransientPurchaseTransactionDocument() {
        return transientPurchaseTransactionDocument;
    }

    @Transient
    @XmlTransient
    private final TransientPurchaseTransactionDocument transientPurchaseTransactionDocument = new TransientPurchaseTransactionDocument();

    public List<BillTransaction> getBillTransaction() {
        return billTransaction;
    }

    public void setBillTransaction(List<BillTransaction> billTransaction) {
        this.billTransaction = billTransaction;
    }
    
    public String getPushTaxInd() {
        return pushTaxInd;
    }

    public void setPushTaxInd(String pushTaxInd) {
        this.pushTaxInd = pushTaxInd;
    }

    public BigDecimal getPushTaxAmt() {
        return pushTaxAmt;
    }

    public void setPushTaxAmt(BigDecimal pushTaxAmt) {
        this.pushTaxAmt = pushTaxAmt;
    }

    public BigDecimal getPushDelta() {
        return pushDelta;
    }

    public void setPushDelta(BigDecimal pushDelta) {
        this.pushDelta = pushDelta;
    }

    public PushPreference getPushTaxPreference() {
        return pushTaxPreference;
    }

    public void setPushTaxPreference(PushPreference pushTaxPreference) {
        this.pushTaxPreference = pushTaxPreference;
    }
}
