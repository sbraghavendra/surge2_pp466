package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ncsts.domain.PurchaseTransaction;

@XmlRootElement(name = "transactionId")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessedTransactionIdentifier {
	
	@XmlAttribute
	private Long id;
	
	@XmlElement(required = false)
	private String errorCode;
	
	@XmlElement(required = false)
	private Boolean errorFlag;
	
	@XmlElement(required = false)
	private String errorText;
	
	@XmlElement(required = false)
	private PurchaseTransaction purchaseTransaction;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Boolean getErrorFlag() {
		return errorFlag;
	}

	public void setErrorFlag(Boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public PurchaseTransaction getPurchaseTransaction() {
		return purchaseTransaction;
	}

	public void setPurchaseTransaction(PurchaseTransaction purchaseTransaction) {
		this.purchaseTransaction = purchaseTransaction;
	}
}
