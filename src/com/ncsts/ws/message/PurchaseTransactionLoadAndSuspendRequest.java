package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDSUSPEND_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDSUSPEND_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionLoadAndSuspendRequest extends PurchaseTransactionProcessRequest {
	
	@XmlElement(required = false)
	Long matrixId;

	public Long getMatrixId() {
		return matrixId;
	}

	public void setMatrixId(Long matrixId) {
		this.matrixId = matrixId;
	}
}
