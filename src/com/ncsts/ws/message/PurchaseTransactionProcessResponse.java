package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionProcessResponse {

    @XmlElement(required = true)
    private PurchaseTransactionDocument purchaseTransactionDocument;
    @XmlElement(required = false)
    private Boolean errorFlag;

    public PurchaseTransactionDocument getPurchaseTransactionDocument() {
        return purchaseTransactionDocument;
    }

    public void setPurchaseTransactionDocument(PurchaseTransactionDocument purchaseTransactionDocument) {
        this.purchaseTransactionDocument = purchaseTransactionDocument;
    }

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Boolean errorFlag) {
        this.errorFlag = errorFlag;
    }

}
