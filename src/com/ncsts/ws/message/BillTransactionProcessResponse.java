package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.BILL_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.BILL_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BillTransactionProcessResponse {

    @XmlElement(required = true)
    private BillTransactionDocument billTransactionDocument;
    @XmlElement(required = false)
    private Boolean errorFlag;

    public BillTransactionDocument getBillTransactionDocument() {
        return billTransactionDocument;
    }

    public void setBillTransactionDocument(BillTransactionDocument billTransactionDocument) {
        this.billTransactionDocument = billTransactionDocument;
    }

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Boolean errorFlag) {
        this.errorFlag = errorFlag;
    }

}
