package com.ncsts.ws.message;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.dto.JurisdictionDTO;
import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.JURISDICTION_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.JURISDICTION_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class JurisdictionResponse {
	
	@XmlElement(required = false)
	List<JurisdictionDTO> jurisdiction;
	
	@XmlElement(required = true)
	private String errorCode;
	
	@XmlElement(required = true)
	private String notes;
	
	public List<JurisdictionDTO> getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(List<JurisdictionDTO> jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	/*
	@XmlElement
	private Long jurisdictionId;
	
	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}*/
}
