package com.ncsts.ws.message;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.JURISDICTION_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.JURISDICTION_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class JurisdictionRequest {
	
	@XmlElement(required = false)
	private String geocode;
	
	@XmlElement(required = false)
	private String country;
	
	@XmlElement(required = false)
	private String state;
	
	@XmlElement(required = false)
	private String county;
	
	@XmlElement(required = false)
	private String city;
	
	@XmlElement(required = false)
	private String zip;
	
	@XmlElement(required = false)
	private String zipplus4;
	
	@XmlElement(required = false)
	private String streetAddress1;
	
	@XmlElement(required = false)
	private String streetAddress2;
	
	@XmlElement(required = false)
	private String latitude;
	
	@XmlElement(required = false)
	private String longitude;
	
	@XmlElement(required = false)
	private String geoRecon;
	
	@XmlElement(required = false)
	private String entityCode;
	
	
	public String getGeocode() {
		return geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public String getCity() {
		return city;
	}

	public void setcity(String city) {
		this.city = city;
	}
	
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getZipplus4() {
		return zipplus4;
	}

	public void setZipplus4(String zipplus4) {
		this.zipplus4 = zipplus4;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}
	
	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getGeoRecon() {
		return geoRecon;
	}

	public void setGeoRecon(String geoRecon) {
		this.geoRecon = geoRecon;
	}
	
	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
}
