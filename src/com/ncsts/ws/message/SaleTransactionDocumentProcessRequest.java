package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.seconddecimal.billing.domain.SaleTransactionDocument;

@XmlRootElement(name=PinPointWebServiceConstants.SALE_TRANSACTION_DOCUMENT_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.SALE_TRANSACTION_DOCUMENT_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class SaleTransactionDocumentProcessRequest {

	@XmlElement(required = false)
	private Integer writeFlag;

	@XmlElement(required = true)
	private SaleTransactionDocument saleTransactionDocument;


	public SaleTransactionDocument getSaleTransactionDocument() {
		return saleTransactionDocument;
	}

	public void setSaleTransactionDocument(
			SaleTransactionDocument saleTransactionDocument) {
		this.saleTransactionDocument = saleTransactionDocument;
	}

	public Integer getWriteFlag() {
		return writeFlag;
	}

	public void setWriteFlag(Integer writeFlag) {
		this.writeFlag = writeFlag;
	}
}
