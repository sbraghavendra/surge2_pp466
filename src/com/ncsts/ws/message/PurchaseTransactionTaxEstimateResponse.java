package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by RC05200 on 8/7/2017.
 */
@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_TAX_ESTIMATE_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.PURCHASE_TRANSACTION_TAX_ESTIMATE_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionTaxEstimateResponse extends PurchaseTransactionProcessResponse {
}
