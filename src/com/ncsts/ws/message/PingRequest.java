package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.PING_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_UNRESTRICTED_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.PING_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_UNRESTRICTED_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PingRequest {
}
