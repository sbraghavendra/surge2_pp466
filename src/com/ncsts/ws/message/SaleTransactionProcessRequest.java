package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.seconddecimal.billing.domain.SaleTransaction;

@XmlRootElement(name=PinPointWebServiceConstants.SALE_TRANSACTION_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.SALE_TRANSACTION_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class SaleTransactionProcessRequest {
	@XmlElement(required = true)
	private SaleTransaction saleTransaction;

	@XmlElement(required = false)
	private Integer writeFlag;
	
	public SaleTransaction getSaleTransaction() {
		return saleTransaction;
	}

	public void setSaleTransaction(SaleTransaction saleTransaction) {
		this.saleTransaction = saleTransaction;
	}

	public Integer getWriteFlag() {
		return writeFlag;
	}

	public void setWriteFlag(Integer writeFlag) {
		this.writeFlag = writeFlag;
	}	
}
