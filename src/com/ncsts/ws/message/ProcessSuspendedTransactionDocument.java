package com.ncsts.ws.message;

import java.util.List;

import com.ncsts.domain.PurchaseTransaction;

public class ProcessSuspendedTransactionDocument {
	
    private String locationTransSql;
    
    private long processedTransactions;
    
    private List<PurchaseTransaction> purchaseTransaction;
    
    
    public List<PurchaseTransaction> getPurchaseTransaction() {
		return purchaseTransaction;
	}

	public void setPurchaseTransaction(List<PurchaseTransaction> purchaseTransaction) {
		this.purchaseTransaction = purchaseTransaction;
	}

	public String getLocationTransSql() {
		return locationTransSql;
	}

	public void setLocationTransSql(String locationTransSql) {
		this.locationTransSql = locationTransSql;
	}

	public long getProcessedTransactions() {
		return processedTransactions;
	}

	public void setProcessedTransactions(long processedTransactions) {
		this.processedTransactions = processedTransactions;
	}


}
