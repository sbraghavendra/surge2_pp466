package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.ECHO_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.ECHO_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class EchoResponse {
	@XmlElement(required = true)
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
