package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.domain.PurchaseTransaction;

import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.REALTIME_TRANSACTION_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.REALTIME_TRANSACTION_PROCESS_REQUEST, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class RealTimeTransactionProcessRequest {
	@XmlElement(required = true)
	private String calcFlag;
	
	@XmlElement(required = true)
	private String writeFlag;

	@XmlElement(required = true)
	private PurchaseTransaction transactionDetail;
	
	public PurchaseTransaction getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(PurchaseTransaction transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getCalcFlag() {
		return calcFlag;
	}

	public void setCalcFlag(String calcFlag) {
		this.calcFlag = calcFlag;
	}

	public String getWriteFlag() {
		return writeFlag;
	}

	public void setWriteFlag(String writeFlag) {
		this.writeFlag = writeFlag;
	}
}
