package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.BATCH_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.BATCH_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BatchProcessResponse {

    @XmlElement(required = false)
    private Boolean errorFlag;
    
    @XmlElement(required = false)
    private String errorText;
    
    @XmlElement(required = false)
    private String errorCode;
    
    @XmlElement(required = false)
    private String batchStatusCode;
    
    @XmlElement(required = false)
    private Long totalRows;

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Boolean errorFlag) {
        this.errorFlag = errorFlag;
    }
    
    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    public String getBatchStatusCode() {
		return batchStatusCode;
	}

	public void setBatchStatusCode(String batchStatusCode) {
		this.batchStatusCode = batchStatusCode;
	}
	
	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

	public Long getTotalRows() {
		return totalRows;
	}
}
