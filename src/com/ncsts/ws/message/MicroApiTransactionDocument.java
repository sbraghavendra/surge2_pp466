package com.ncsts.ws.message;

import com.ncsts.domain.MicroApiTransaction;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.BillTransaction;
import com.ncsts.dto.TransientMicroApiTransactionDocument;

import com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

import org.springframework.beans.BeanUtils;

@XmlRootElement(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlType(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class MicroApiTransactionDocument {
	
	public MicroApiTransactionDocument() {	
	}
	
	//Purchase payload
	public MicroApiTransactionDocument(PurchaseTransactionDocument purchaseTransactionDocument) {	
		List<MicroApiTransaction> microApiTransactions = new ArrayList<MicroApiTransaction>();	
		for (PurchaseTransaction purchaseTransaction: purchaseTransactionDocument.getPurchaseTransaction()) {
			MicroApiTransaction microApiTransaction = new MicroApiTransaction(purchaseTransaction);
			microApiTransactions.add(microApiTransaction);
		}
		setMicroApiTransaction(microApiTransactions);
		
		setPushTaxInd(purchaseTransactionDocument.getPushTaxInd());
		setPushTaxAmt(purchaseTransactionDocument.getPushTaxAmt());
		setPushDelta(purchaseTransactionDocument.getPushDelta());
		setPushTaxPreference(purchaseTransactionDocument.getPushTaxPreference());
	}
	
	public void updatePurchaseTransactionDocument(PurchaseTransactionDocument purchaseTransactionDocument) {	
		List<PurchaseTransaction> purchaseTransactions = new ArrayList<PurchaseTransaction>();
		for (MicroApiTransaction microApiTransaction: this.getMicroApiTransaction()) {
			PurchaseTransaction purchaseTransaction = new PurchaseTransaction();
			microApiTransaction.updatePurchaseTransaction(purchaseTransaction);
			purchaseTransactions.add(purchaseTransaction);
		}
		purchaseTransactionDocument.setPurchaseTransaction(purchaseTransactions);
		
		purchaseTransactionDocument.setPushTaxInd(getPushTaxInd());
		purchaseTransactionDocument.setPushTaxAmt(getPushTaxAmt());
		purchaseTransactionDocument.setPushDelta(getPushDelta());
		purchaseTransactionDocument.setPushTaxPreference(getPushTaxPreference());
	}
	
	//Bill payload
	public MicroApiTransactionDocument(BillTransactionDocument billTransactionDocument) {
		this(billTransactionDocument, "");
	}
	
	public MicroApiTransactionDocument(BillTransactionDocument billTransactionDocument, String processMethod) {	
		List<MicroApiTransaction> microApiTransactions = new ArrayList<MicroApiTransaction>();	
		for (BillTransaction billeTransaction: billTransactionDocument.getBillTransaction()) {
			billeTransaction.setProcessMethod(processMethod);
			MicroApiTransaction microApiTransaction = new MicroApiTransaction(billeTransaction);
			microApiTransactions.add(microApiTransaction);
		}
		setMicroApiTransaction(microApiTransactions);
		
		setPushTaxInd(billTransactionDocument.getPushTaxInd());
		setPushTaxAmt(billTransactionDocument.getPushTaxAmt());
		setPushDelta(billTransactionDocument.getPushDelta());
		setPushTaxPreference(billTransactionDocument.getPushTaxPreference());
	}
	
	public void updateBillTransactionDocument(BillTransactionDocument billeTransactionDocument) {	
		List<BillTransaction> billTransactions = new ArrayList<BillTransaction>();
		for (MicroApiTransaction microApiTransaction: this.getMicroApiTransaction()) {
			BillTransaction billTransaction = new BillTransaction();
			microApiTransaction.updateBillTransaction(billTransaction);
			billTransactions.add(billTransaction);
		}
		billeTransactionDocument.setBillTransaction(billTransactions);
		
		billeTransactionDocument.setPushTaxInd(getPushTaxInd());
		billeTransactionDocument.setPushTaxAmt(getPushTaxAmt());
		billeTransactionDocument.setPushDelta(getPushDelta());
		billeTransactionDocument.setPushTaxPreference(getPushTaxPreference());
	}
	
    private List<MicroApiTransaction> microApiTransaction;
    private String pushTaxInd;

    private BigDecimal pushTaxAmt;

    private BigDecimal pushDelta;

    private PushPreference pushTaxPreference;

    public TransientMicroApiTransactionDocument getTransientMicroApiTransactionDocument() {
        return transientMicroApiTransactionDocument;
    }

    @Transient
    @XmlTransient
    private final TransientMicroApiTransactionDocument transientMicroApiTransactionDocument = new TransientMicroApiTransactionDocument();

    public List<MicroApiTransaction> getMicroApiTransaction() {
        return microApiTransaction;
    }

    public void setMicroApiTransaction(List<MicroApiTransaction> microApiTransaction) {
        this.microApiTransaction = microApiTransaction;
    }

    public String getPushTaxInd() {
        return pushTaxInd;
    }

    public void setPushTaxInd(String pushTaxInd) {
        this.pushTaxInd = pushTaxInd;
    }

    public BigDecimal getPushTaxAmt() {
        return pushTaxAmt;
    }

    public void setPushTaxAmt(BigDecimal pushTaxAmt) {
        this.pushTaxAmt = pushTaxAmt;
    }

    public BigDecimal getPushDelta() {
        return pushDelta;
    }

    public void setPushDelta(BigDecimal pushDelta) {
        this.pushDelta = pushDelta;
    }

    public PushPreference getPushTaxPreference() {
        return pushTaxPreference;
    }

    public void setPushTaxPreference(PushPreference pushTaxPreference) {
        this.pushTaxPreference = pushTaxPreference;
    }
}
