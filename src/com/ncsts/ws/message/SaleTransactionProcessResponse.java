package com.ncsts.ws.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.seconddecimal.billing.domain.SaleTransaction;

@XmlRootElement(name=PinPointWebServiceConstants.SALE_TRANSACTION_PROCESS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.SALE_TRANSACTION_PROCESS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class SaleTransactionProcessResponse {
	@XmlElement(required = false)
	private SaleTransaction saleTransaction;
	
	@XmlElement(required = false)
	private String errorCode;
	
	@XmlElement(required = false)
	private String errorMessage;

	public SaleTransaction getSaleTransaction() {
		return saleTransaction;
	}

	public void setSaleTransaction(SaleTransaction saleTransaction) {
		this.saleTransaction = saleTransaction;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
