package com.ncsts.ws.message;

import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.dto.TransientPurchaseTransactionDocument;

import com.ncsts.ws.message.PurchaseTransactionProcessRequest.PushPreference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Transient;
import javax.xml.bind.annotation.*;

@XmlRootElement(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlType(
        namespace = "http://www.seconddecimal.com/ncsts/schemas/messages"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionDocument {
    private List<PurchaseTransaction> purchaseTransaction;
    private String pushTaxInd;

    private BigDecimal pushTaxAmt;

    private BigDecimal pushDelta;

    private PushPreference pushTaxPreference;

    public TransientPurchaseTransactionDocument getTransientPurchaseTransactionDocument() {
        return transientPurchaseTransactionDocument;
    }

  /*  public void setTransientPurchaseTransactionDocument(TransientPurchaseTransactionDocument transientPurchaseTransactionDocument) {
        this.transientPurchaseTransactionDocument = transientPurchaseTransactionDocument;
    }*/

    @Transient
    @XmlTransient
    private final TransientPurchaseTransactionDocument transientPurchaseTransactionDocument = new TransientPurchaseTransactionDocument();

    public List<PurchaseTransaction> getPurchaseTransaction() {
        return purchaseTransaction;
    }

    public void setPurchaseTransaction(List<PurchaseTransaction> purchaseTransaction) {
        this.purchaseTransaction = purchaseTransaction;
    }
    
    public void setPurchaseTransaction(PurchaseTransaction purchaseTransaction) {
    	List<PurchaseTransaction> purchaseTransactions = new ArrayList<PurchaseTransaction>();
    	purchaseTransactions.add(purchaseTransaction);
    	this.purchaseTransaction = purchaseTransactions;
    }

    public String getPushTaxInd() {
        return pushTaxInd;
    }

    public void setPushTaxInd(String pushTaxInd) {
        this.pushTaxInd = pushTaxInd;
    }

    public BigDecimal getPushTaxAmt() {
        return pushTaxAmt;
    }

    public void setPushTaxAmt(BigDecimal pushTaxAmt) {
        this.pushTaxAmt = pushTaxAmt;
    }


    public BigDecimal getPushDelta() {
        return pushDelta;
    }

    public void setPushDelta(BigDecimal pushDelta) {
        this.pushDelta = pushDelta;
    }

    public PushPreference getPushTaxPreference() {
        return pushTaxPreference;
    }

    public void setPushTaxPreference(PushPreference pushTaxPreference) {
        this.pushTaxPreference = pushTaxPreference;
    }
}
