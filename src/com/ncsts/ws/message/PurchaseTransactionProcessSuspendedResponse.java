package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESSSUSPENDED_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESSSUSPENDED_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionProcessSuspendedResponse {
	
	
    @XmlElement(required = false)
    private Boolean debugFlag;
    
    @XmlElement(required = false)
    private String suspendedTransactionSql;
    
    @XmlElement(required = false)
    private Boolean errorFlag;
    
    @XmlElement(required = false)
    private String errorCode;
    
    @XmlElement(required = false)
    private String errorText;
    
    @XmlElement(required = false)
    private Long transactionsProcessed;
    
    @XmlElementWrapper
    @XmlElement(name="transactionId", required = false)
    private List<ProcessedTransactionIdentifier> processedTransactionIds;

	public List<ProcessedTransactionIdentifier> getProcessedTransactionIds() {
		return processedTransactionIds;
	}

	public void setProcessedTransactionIds(List<ProcessedTransactionIdentifier> transactionIds) {
		this.processedTransactionIds = transactionIds;
	}

	public Boolean getDebugFlag() {
		return debugFlag;
	}

	public void setDebugFlag(Boolean debugFlag) {
		this.debugFlag = debugFlag;
	}

	public Boolean getErrorFlag() {
		return errorFlag;
	}

	public void setErrorFlag(Boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getSuspendedTransactionSql() {
		return suspendedTransactionSql;
	}

	public void setSuspendedTransactionSql(String locationTransSql) {
		this.suspendedTransactionSql = locationTransSql;
	}

	public Long getTransactionsProcessed() {
		return transactionsProcessed;
	}

	public void setTransactionsProcessed(Long transactionsProcessed) {
		this.transactionsProcessed = transactionsProcessed;
	}
	
    

}
