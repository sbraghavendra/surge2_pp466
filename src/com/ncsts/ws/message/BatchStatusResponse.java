package com.ncsts.ws.message;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.ws.PinPointWebServiceConstants;

@XmlRootElement(name=PinPointWebServiceConstants.BATCH_STATUS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name=PinPointWebServiceConstants.BATCH_STATUS_RESPONSE, namespace=PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BatchStatusResponse {
	@XmlElement
	List<BatchMaintenance> batchMaintenance;

	public List<BatchMaintenance> getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(List<BatchMaintenance> batchMaintenance) {
		this.batchMaintenance = batchMaintenance;
	}
}
