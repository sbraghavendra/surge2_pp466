package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.MICROAPI_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.MICROAPI_TRANSACTION_PROCESS_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class MicroApiTransactionProcessResponse {

    @XmlElement(required = true)
    private MicroApiTransactionDocument microApiTransactionDocument;
    @XmlElement(required = false)
    private Boolean errorFlag;

    public MicroApiTransactionDocument getMicroApiTransactionDocument() {
        return microApiTransactionDocument;
    }

    public void setMicroApiTransactionDocument(MicroApiTransactionDocument microApiTransactionDocument) {
        this.microApiTransactionDocument = microApiTransactionDocument;
    }

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Boolean errorFlag) {
        this.errorFlag = errorFlag;
    }

}
