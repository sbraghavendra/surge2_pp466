package com.ncsts.ws.message;

import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_GET_LOCATION_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_GET_LOCATION_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionGetLocationRequest {
    @XmlElement(required = true)
    private PurchaseTransactionDocument purchaseTransactionDocument;

    @XmlType(namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    @XmlEnum(String.class)
    public enum  LocationType {BILLTO,	SHIPTO, TITLETRANSFER, FIRSTUSE, SHIPFROM, ORDERORIGIN, ORDERACCEPT} ;

    @XmlType(namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    @XmlEnum(String.class)
    public enum QueryType {RATEPOINT, LOCATION_MATRIX}

    @XmlElement(required = true)
    private LocationType locationType;

    @XmlElement(required = true)
    private QueryType queryTypeType;


    public PurchaseTransactionDocument getPurchaseTransactionDocument() {
        return purchaseTransactionDocument;
    }

    public void setPurchaseTransactionDocument(PurchaseTransactionDocument purchaseTransactionDocument) {
        this.purchaseTransactionDocument = purchaseTransactionDocument;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public QueryType getQueryTypeType() {
        return queryTypeType;
    }

    public void setQueryTypeType(QueryType queryTypeType) {
        this.queryTypeType = queryTypeType;
    }
}
