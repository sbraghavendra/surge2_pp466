package com.ncsts.ws.message;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.ws.PinPointWebServiceConstants;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_GET_LOCATION_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name = PinPointWebServiceConstants.PURCHASE_TRANSACTION_GET_LOCATION_RESPONSE, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTransactionGetLocationResponse {
    @XmlElement(required = true)
    private Jurisdiction jurisdiction;

    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }
}
