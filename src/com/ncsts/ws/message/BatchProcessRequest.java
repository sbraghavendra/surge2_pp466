package com.ncsts.ws.message;

import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.message.PurchaseTransactionGetLocationRequest.LocationType;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = PinPointWebServiceConstants.BATCH_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlType(name =PinPointWebServiceConstants.BATCH_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class BatchProcessRequest {

    @XmlElement(required = false)
    private Long processBatchNo;
    
    @XmlElement(required = false)
    private String defaultMatrixFlag;

    public Long getProcessBatchNo() {
        return processBatchNo;
    }

    public void setProcessBatchNo(Long processBatchNo) {
        this.processBatchNo = processBatchNo;
    }
    
    public String getDefaultMatrixFlag() {
        return defaultMatrixFlag;
    }

    public void setDefaultMatrixFlag(String defaultMatrixFlag) {
        this.defaultMatrixFlag = defaultMatrixFlag;
    }

}
