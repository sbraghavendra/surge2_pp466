package com.ncsts.ws.endpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ncsts.services.impl.DiagnosticService;
import com.ncsts.ws.PinPointWebServiceConstants;
import com.ncsts.ws.message.BatchProcessRequest;
import com.ncsts.ws.message.BatchProcessResponse;
import com.ncsts.ws.message.BatchStatusRequest;
import com.ncsts.ws.message.BatchStatusResponse;
import com.ncsts.ws.message.BillTransactionProcessRequest;
import com.ncsts.ws.message.BillTransactionProcessResponse;
import com.ncsts.ws.message.EchoRequest;
import com.ncsts.ws.message.EchoResponse;
import com.ncsts.ws.message.JurisdictionRequest;
import com.ncsts.ws.message.JurisdictionResponse;
import com.ncsts.ws.message.PingRequest;
import com.ncsts.ws.message.PingResponse;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualRequest;
import com.ncsts.ws.message.PurchaseTransactionEvaluateForAccrualResponse;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationRequest;
import com.ncsts.ws.message.PurchaseTransactionInvoiceVerificationResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadAndReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionLoadAndSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadRequest;
import com.ncsts.ws.message.PurchaseTransactionLoadResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessResponse;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedRequest;
import com.ncsts.ws.message.PurchaseTransactionProcessSuspendedResponse;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceRequest;
import com.ncsts.ws.message.PurchaseTransactionPushPreferenceResponse;
import com.ncsts.ws.message.PurchaseTransactionPushProrateRequest;
import com.ncsts.ws.message.PurchaseTransactionPushProrateResponse;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificRequest;
import com.ncsts.ws.message.PurchaseTransactionPushSpecificResponse;
import com.ncsts.ws.message.PurchaseTransactionReleaseRequest;
import com.ncsts.ws.message.PurchaseTransactionReleaseResponse;
import com.ncsts.ws.message.PurchaseTransactionReprocessRequest;
import com.ncsts.ws.message.PurchaseTransactionReprocessResponse;
import com.ncsts.ws.message.PurchaseTransactionReverseRequest;
import com.ncsts.ws.message.PurchaseTransactionReverseResponse;
import com.ncsts.ws.message.PurchaseTransactionSuspendRequest;
import com.ncsts.ws.message.PurchaseTransactionSuspendResponse;
import com.ncsts.ws.message.RealTimeTransactionProcessRequest;
import com.ncsts.ws.message.RealTimeTransactionProcessResponse;
import com.ncsts.ws.message.SaleTransactionDocumentProcessRequest;
import com.ncsts.ws.message.SaleTransactionDocumentProcessResponse;
import com.ncsts.ws.message.SaleTransactionProcessRequest;
import com.ncsts.ws.message.StatusRequest;
import com.ncsts.ws.message.StatusResponse;
import com.ncsts.ws.service.PinPointService;


@Endpoint
@SuppressWarnings("unused")
public class PinPointEndPoint {

	@Autowired
	private DiagnosticService diagnosticService;

	private static final Log logger = LogFactory.getLog(PinPointEndPoint.class);
	
	private final PinPointService pinPointService;
	
	@Autowired
	public PinPointEndPoint(PinPointService service) {
		this.pinPointService = service;
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PING_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_UNRESTRICTED_NAMESPACE)
    @ResponsePayload
    public PingResponse ping(PingRequest request) {
		PingResponse response = new PingResponse();
		response.setResponse("Alive");
		
		return response;
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.ECHO_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
    @ResponsePayload
    public EchoResponse getEcho(EchoRequest request) {
		EchoResponse response = new EchoResponse();
		response.setResponse(request.getRequest());
		
		return response;
	}

	@PayloadRoot(localPart = PinPointWebServiceConstants.STATUS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public StatusResponse getStatus(StatusRequest statusRequest) {
		StatusResponse statusResponse = diagnosticService.getCurrentStatus();

		return statusResponse;
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.BATCH_STATUS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public BatchStatusResponse getBatchStatus(BatchStatusRequest request) {
		return this.pinPointService.getBatchStatus(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.REALTIME_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public RealTimeTransactionProcessResponse getRealTimeTransactionProcess(RealTimeTransactionProcessRequest request) {
		return this.pinPointService.getRealTimeTransactionProcess(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.SALE_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public SaleTransactionDocumentProcessResponse getSaleTransactionProcess(SaleTransactionProcessRequest request) {
		return this.pinPointService.getSaleTransactionProcess(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.SALE_TRANSACTION_DOCUMENT_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public SaleTransactionDocumentProcessResponse getSaleTransactionDocumentProcess(SaleTransactionDocumentProcessRequest request) {
		return this.pinPointService.getSaleTransactionDocumentProcess(request);
	}

	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionProcessResponse getPurchaseTransactionProcess(PurchaseTransactionProcessRequest request) {
		return pinPointService.getPurchaseTransactionProcess(request);
	}

	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_TAX_ESTIMATE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionProcessResponse getPurchaseTransactionTaxEstimate(PurchaseTransactionProcessRequest request) {
		return pinPointService.getPurchaseTransactionTaxEstimate(request);
	}

	@PayloadRoot(localPart = PinPointWebServiceConstants.JURISDICTION_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public JurisdictionResponse getJurisdiction(JurisdictionRequest  request) {
		return this.pinPointService.getJurisdiction(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.BATCH_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public BatchProcessResponse getPurchaseTransactionProcessBatch(BatchProcessRequest  request) {
		return this.pinPointService.getPurchaseTransactionProcessBatch(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_REVERSE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionReverseResponse getPurchaseTransactionReverse(PurchaseTransactionReverseRequest request) {
		return this.pinPointService.getPurchaseTransactionReverse(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDREVERSE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionLoadAndReverseResponse getPurchaseTransactionLoadAndReverse(@RequestBody PurchaseTransactionLoadAndReverseRequest request) {
		return this.pinPointService.getPurchaseTransactionLoadAndReverse(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_VERIFICATION_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionInvoiceVerificationResponse getPurchaseTransactionInvoiceVerification(@RequestBody PurchaseTransactionInvoiceVerificationRequest request) {
		return this.pinPointService.getPurchaseTransactionInvoiceVerification(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_EVALUATIONFORACCRUAL_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionEvaluateForAccrualResponse getPurchaseTransactionEvaluateForAccrual(@RequestBody PurchaseTransactionEvaluateForAccrualRequest request) {
		return this.pinPointService.getPurchaseTransactionEvaluateForAccrual(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.BILL_TRANSACTION_PROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public BillTransactionProcessResponse getBillTransactionProcess(@RequestBody BillTransactionProcessRequest request) {
		return this.pinPointService.getBillTransactionProcess(request, null);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PUSH_PRORATE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionPushProrateResponse getPurchaseTransactionPushProrate(PurchaseTransactionPushProrateRequest request) {
		return pinPointService.getPurchaseTransactionPushProrate(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PUSH_PREFERENCE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionPushPreferenceResponse getPurchaseTransactionPushPreference(PurchaseTransactionPushPreferenceRequest request) {
		return pinPointService.getPurchaseTransactionPushPreference(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PUSH_SPECIFIC_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionPushSpecificResponse getPurchaseTransactionPushSpecific(PurchaseTransactionPushSpecificRequest request) {
		return pinPointService.getPurchaseTransactionPushSpecific(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDREPROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionLoadAndReprocessResponse getPurchaseTransactionLoadAndReprocess(PurchaseTransactionLoadAndReprocessRequest request) {
		return pinPointService.getPurchaseTransactionLoadAndReprocess(request);
	}

	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_PROCESSSUSPENDED_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionProcessSuspendedResponse getPurchaseTransactionProcessSuspended(PurchaseTransactionProcessSuspendedRequest request) {
		return pinPointService.getPurchaseTransactionProcessSuspended(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_REPROCESS_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionReprocessResponse getPurchaseTransactionReprocess(PurchaseTransactionReprocessRequest request) {
		return pinPointService.getPurchaseTransactionReprocess(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_RELEASE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionReleaseResponse getPurchaseTransactionRelease(PurchaseTransactionReleaseRequest request) {
		return pinPointService.getPurchaseTransactionRelease(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDRELEASE_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionLoadAndReleaseResponse getPurchaseTransactionLoadAndRelease(PurchaseTransactionLoadAndReleaseRequest request) {
		return pinPointService.getPurchaseTransactionLoadAndRelease(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_SUSPEND_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionSuspendResponse getPurchaseTransactionSuspend(PurchaseTransactionSuspendRequest request) {
		return pinPointService.getPurchaseTransactionSuspend(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOADANDSUSPEND_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionSuspendResponse getPurchaseTransactionLoadAndSuspend(PurchaseTransactionLoadAndSuspendRequest request) {
		return pinPointService.getPurchaseTransactionLoadAndSuspend(request);
	}
	
	@PayloadRoot(localPart = PinPointWebServiceConstants.PURCHASE_TRANSACTION_LOAD_REQUEST, namespace = PinPointWebServiceConstants.MESSAGES_NAMESPACE)
	@ResponsePayload
	public PurchaseTransactionLoadResponse getPurchaseTransactionLoad(PurchaseTransactionLoadRequest request) {
		return pinPointService.getPurchaseTransactionLoad(request);
	}

}
