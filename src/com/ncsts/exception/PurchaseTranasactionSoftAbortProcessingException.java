package com.ncsts.exception;

/**
 * Created by RC05200 on 8/1/2017.
 */
public class PurchaseTranasactionSoftAbortProcessingException  extends PurchaseTransactionProcessingException {
    public PurchaseTranasactionSoftAbortProcessingException(String message) {super(message);}
}
