package com.ncsts.exception;

/**
 * Created by RC05200 on 7/11/2016.
 */
public class PurchaseTransactionSuspendedException extends PurchaseTransactionProcessingException {
    public PurchaseTransactionSuspendedException(String message) {
        super(message);
    }
}
