package com.ncsts.exception;

public class InvalidSuspensionTypeException extends Exception{
	public InvalidSuspensionTypeException(String message) { super(message);}
}
