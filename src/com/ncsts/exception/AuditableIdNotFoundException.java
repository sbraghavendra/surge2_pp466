package com.ncsts.exception;

public class AuditableIdNotFoundException extends Exception{
	public AuditableIdNotFoundException(String message) {super(message);}
}
