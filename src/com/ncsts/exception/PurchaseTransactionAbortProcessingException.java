package com.ncsts.exception;


public class PurchaseTransactionAbortProcessingException extends PurchaseTransactionProcessingException {

    public PurchaseTransactionAbortProcessingException(String message) {
        super(message);
    }
}
