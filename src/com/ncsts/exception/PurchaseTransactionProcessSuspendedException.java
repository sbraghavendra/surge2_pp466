package com.ncsts.exception;

import com.ncsts.domain.ListCodes;

public class PurchaseTransactionProcessSuspendedException extends PurchaseTransactionProcessingException {
	
	private ListCodes errorCode;

	public PurchaseTransactionProcessSuspendedException(String message, ListCodes errorCode) {
		super(message);
		setErrorCode(errorCode);
		// TODO Auto-generated constructor stub
	}

	public PurchaseTransactionProcessSuspendedException(ListCodes errorCode) {
		// TODO Auto-generated constructor stub
		super(null);
		setErrorCode(errorCode);
	}

	public ListCodes getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ListCodes errorCode) {
		this.errorCode = errorCode;
	}

}
