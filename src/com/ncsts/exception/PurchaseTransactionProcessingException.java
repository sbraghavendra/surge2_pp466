package com.ncsts.exception;

import com.ncsts.domain.ListCodes;

public class PurchaseTransactionProcessingException extends Exception {
	
	
    public PurchaseTransactionProcessingException(String message) {
        super(message);

    }
    
}
