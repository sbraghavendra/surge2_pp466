/*
 * Author: Jim M. Wilson
 * Created: Sep 16, 2008
 * $Date: 2008-09-16 17:00:09 -0500 (Tue, 16 Sep 2008) $: - $Revision: 2243 $: 
 */

package com.ncsts.exception;

/**
 *
 */
public class DateFormatException extends Exception {
	public DateFormatException(String s) {
		super(s);
	}
}
