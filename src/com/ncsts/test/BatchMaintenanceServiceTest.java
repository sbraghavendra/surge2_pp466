package com.ncsts.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.ListCodesService;

@RunWith(TestClassRunner.class)
public class BatchMaintenanceServiceTest extends STSTestBase {
	
	
	BatchMaintenanceService batchMaintenanceService = 
		(BatchMaintenanceService) getAppContext().getBean("batchMaintenanceService");
	
	ListCodesService listCodesService = 
		(ListCodesService) getAppContext().getBean("listCodesService");	
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    //  FK and Constraints (test for  implementations of the conditional references)
    @Test
    public void updateBatch(){
    	
    	BatchMaintenance batchMaintenance = new BatchMaintenance();
    	batchMaintenance.setBatchId(180l);
    	batchMaintenance.setBatchStatusCode("P");
    	batchMaintenanceService.update(batchMaintenance);
    	
    }
    
    
/*   @Test
   public void testGetBatchMaintenanceByBatchStatusCode(){
	   
	   List<BatchMaintenance> listBatchMaintenance = batchMaintenanceService.getBatchMaintenanceByBatchStatusCode("I");
	   
	   System.out.println(" testGetBatchMaintenanceByBatchStatusCode \n");
	   
	   if(listBatchMaintenance != null)
		   System.out.println(" listBatchMaintenance size is :"+listBatchMaintenance.size());
	   else{
		   System.out.println(" listBatchMaintenance size is :"+listBatchMaintenance);
	   }
	   
   }*/
   
/*   
   @Test
   public void testGetBatchMaintenanceByBatchTypeCode(){
	   
	   List<BatchMaintenance> listBatchMaintenance = batchMaintenanceService.getBatchMaintenanceByBatchTypeCode("T1");
	   
	   System.out.println(" testGetBatchMaintenanceByBatchTypeCode \n");
	   
	   if(listBatchMaintenance != null)
		   System.out.println(" listBatchMaintenance size is :"+listBatchMaintenance.size());
	   else{
		   System.out.println(" listBatchMaintenance size is :"+listBatchMaintenance);
	   }
	   
   }	
	
*/
/*    @Test
    public void testGetAllBatchMaintenanceData(){
    	
    	System.out.println(" Start of Test case:");
    	List<BatchMaintenanceDTO> batchMaintenanceList = batchMaintenanceService.getAllBatchMaintenanceData();
    	System.out.println(" Test results from testGetAllBatchMaintenanceData\n");
    	if(batchMaintenanceList != null)
    		System.out.println(" Size of batchMaintenanceList is :"+batchMaintenanceList.size()+"\n");
    	else
    		System.out.println("  batchMaintenanceList is :"+batchMaintenanceList+"\n");
    	assertEquals(337, batchMaintenanceList.size());
    }*/
    
/*    @Test
    public void testDelete(){
    	batchMaintenanceService.delete(new Long(445));
    }
*/   
    
/*    @Test
    public void getBatchById()
    {
    	BatchMaintenance batchMaintenance = batchMaintenanceService.getBatchMaintananceData(482);
    	System.out.println("Entry time stamp "+batchMaintenance.getEntryTimestamp());
    }*/
   
	// Test Fix for Bug 2234    
/*   @Test
   public void testBinaryWeight() {
       List<String> list = getSelectionsForBinaryWeight(3);
       for (Iterator<String> it = list.iterator(); it.hasNext(); ) {
    	   System.out.println("description = " + it.next());
       }
   }
   
	// Fix for Bug 2234
	private List<String> getSelectionsForBinaryWeight(int weight) {
        List<String> selections = new ArrayList<String>();
		List<Integer> list = getBinaryCodeForBinaryWeight(weight);
		ListCodesPK pk;		
		for (Iterator<Integer> it = list.iterator(); it.hasNext(); ) {
		    Integer value = it.next();
		    pk = new ListCodesPK();
			pk.setCodeTypeCode("MEASURETYP");
			pk.setCodeCode(value.toString());
			ListCodes listCodes = listCodesService.findByPK(pk);
			selections.add(listCodes.getDescription());
		}
		pk = new ListCodesPK();
		pk.setCodeTypeCode("MEASURETYP");
		pk.setCodeCode("0");
		ListCodes listCodes = listCodesService.findByPK(pk);
		selections.add(listCodes.getDescription());
		return selections;
	}
	
	// Fix for Bug 2234
	private List<Integer> getBinaryCodeForBinaryWeight(int weight) {
	    List<Integer> list = new ArrayList<Integer>();
		String bin = Integer.toBinaryString(weight);	
		char[] charr = bin.toCharArray();
		for (int i = 0; i < charr.length ; i++) {
	        if ("1".equalsIgnoreCase(""+charr[i])) {
	            int pos = 1;
	            for (int j = 0; j < ((charr.length-1) - i); j++ ) {
	                pos = 2 * pos;	
	            }
	            list.add(new Integer(pos));
	        }
		}
		return list;
	}  */
    
}
