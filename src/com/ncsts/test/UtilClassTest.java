package com.ncsts.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.Util;

@RunWith(TestClassRunner.class)
public class UtilClassTest extends STSTestBase {

	@Before
	public void initialize() throws Exception {
		super.setUp();
	}

	@After
	public void cleanup() throws Exception {
		super.tearDown();
	}

	@Test
	public void testDouble2String() {
		Util daoUtil = new Util();
		Double d;
		d= 12.3/3;
		System.out.println("d=" + d);
		System.out.println("d=" + daoUtil.Double2String(d));
	}

}
