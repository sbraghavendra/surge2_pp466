package com.ncsts.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.services.CallGLExtractService;

@RunWith(TestClassRunner.class)
public class CallGLExtractServiceTest extends STSTestBase {
	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(com.ncsts.test.GlExtractMapServiceTest.class);
	
	CallGLExtractService callGLExtractService = 
		(CallGLExtractService) getAppContext().getBean("callGLExtractService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }   	          

    @Test
    public void testNothing(){
        
    }
    
    @Test
    public void testSP() {
    	callGLExtractService.callGlExtractProc("11/11/2008", "abc.txt", 0, 1);
    }
}
