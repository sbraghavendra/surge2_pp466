package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.TransactionDetail;

import org.apache.commons.logging.*;

@RunWith(TestClassRunner.class)
public class TransactionDataModelTest extends STSTestBase  {
	
	    private static final transient Log log = LogFactory.getLog(com.ncsts.test.TransactionDataModelTest.class);

	    /**default empty constructor*/
	    public TransactionDataModelTest() {
	    }
	    
	    private TransactionDetailDAO transactionDetailDAO = 
	    	(TransactionDetailDAO) getAppContext().getBean("transactionDetailDAO");
	    
	    
	    @Before
	    public void initialize() throws Exception {
	        super.setUp();
	    }

	    @After
	    public void cleanup() throws Exception {
	        super.tearDown();
	    }
	 
	    
	    /*@Test
	    public void testLoadNoTaxRate(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("S");	
	        detail.setSuspendInd("R");
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);
	    }
*/
		/*@Test
	    public void testLoadTaxMatrix(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("S");	
	        detail.setSuspendInd("T");	
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	    }*/

		/*@Test
	    public void testLoadLocationMatrix(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("S");	
	        detail.setSuspendInd("L");	
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	    }*/

		/*@Test
	    public void testLoadProcessed(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("P");	
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	    }*/

		/*@Test
	    public void testLoadSuspended(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("S");
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	    }
*/
		/*@Test
	    public void testLoadHeld(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("H");
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	        
	    }*/

		/*@Test
	    public void testLoadClosed(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("P");	
			detail.setGlExtractFlag(1l);
			 Long count = transactionDetailDAO.count(detail, null, null, null, false, null, false, null);
		     log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, false, null, null, 0, 500);	       
	    }*/

	    /*@Test
	    public void testLoadOpen(){
			TransactionDetail detail = new TransactionDetail();
	        detail.setTransactionInd("P");
	        Long count = transactionDetailDAO.count(detail, null, null, null, false, null, true, null);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<TransactionDetail> list = transactionDetailDAO.getAllRecords(detail, null, null, null, false, null, true, null, null, 0, 500);
	    }*/
}
