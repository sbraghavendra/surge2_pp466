package com.ncsts.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;
import com.ncsts.view.util.ArithmeticUtils;

@RunWith(TestClassRunner.class)
public class TaxRateServiceImplSpringJpaTest extends STSTestBase {

	JurisdictionTaxrateService taxRateService = 
		(JurisdictionTaxrateService) getAppContext().getBean("jurisdictionTaxrateService");
	/*JurisdictionService jurisdictionService = 
		(JurisdictionService) getAppContext().getBean("jurisdictionService");*/

	@Before
	public void initialize() throws Exception {
		super.setUp();
	}

	@After
	public void cleanup() throws Exception {
		super.tearDown();
	}

	/*@Test
	public void testGetAllTaxJurisdiction() {
		long startTime = System.currentTimeMillis(); // Set Timer
		Integer searchFor = 1000;
		List<Jurisdiction> jurisdictionList = jurisdictionService.findByExample(new Jurisdiction(), searchFor);
		System.out.println("Number of Tax Jurisdictions returned: " + jurisdictionList.size());
		assertEquals(searchFor.intValue(), jurisdictionList.size());

		// Get the next 1000
//		List<JurisdictionDTO> jurisdictionDTO2 = jurisdictionService.getAllTaxJurisdiction(searchFor, searchFor);
//		System.out.println("Number of Tax Jurisdictions returned: " + jurisdictionDTO2.size());
//		assertEquals(searchFor.intValue(), jurisdictionDTO2.size());

		System.out.println("Execution Time: " + (System.currentTimeMillis() - startTime) / 1000.0f + " sec");

		try {
			Runtime r = Runtime.getRuntime();
			System.gc();
			Thread.sleep(4000);
			System.out.println("Memory used (max) : " + ((r.totalMemory() - r.freeMemory()) / 1000000) + " MB");
		} catch (Exception ex) {
		}

	}

	@Test
	public void testTaxJurisdictionMethods() {
		//
		// Test performs the following actions with should call all
		// TaxJurisdiction methods
		// 1. Add / Create several new Tax Jurisdictions
		// 2. Copy and Add an existing Tax Jurisdiction
		// 3. Search for those Tax Jurisdictions
		// 4. Update a Tax Jurisdiction
		// 5. Get several Tax Jurisdictions
		// 6. Delete all created Tax Jurisdictions
		//

		long startTime = System.currentTimeMillis(); // Set Timer
		Long originalNumOfTaxJurisdictions;

		// 1. Add / Create several new Tax Jurisdictions
		Long prevNumTaxJur = 0L;
		Long newNumTaxJur = 0L;

		// Create the new TaxJurisdiction Object
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setGeocode("0123456789");
		jurisdiction.setState("MA");
		jurisdiction.setCounty("TEST-COUNTY");
		jurisdiction.setCity("TEST-CITY");
		jurisdiction.setZip("11111");
		jurisdiction.setZipplus4("1111");
		jurisdiction.setInOut("B");
		jurisdiction.setCustomFlag("1");
		jurisdiction.setDescription("Sample Jurisdiction");
		jurisdiction.setClientGeocode("01234567");
		jurisdiction.setCompGeocode("0123456789");

		// Count existing Tax Jurisdictions
		prevNumTaxJur = jurisdictionService.count();
		originalNumOfTaxJurisdictions = prevNumTaxJur;
		assertNotSame(prevNumTaxJur.longValue(), 0L);

		// Add the new TaxJurisdiction Object
		System.out.println("Adding the new TaxJurisdiction Object");
		jurisdictionService.save(jurisdiction);
		
		// See if we can retrieve it's JurisdictionId
		List<Jurisdiction> taxJurisdictions = jurisdictionService.findByExample(jurisdiction, 0);
		System.out.println("Trying to get JurisdictionId of the last Jurisdiction inserted");
		assertNotNull(taxJurisdictions);
		assertEquals(taxJurisdictions.size(), 1);
		assertNotNull(taxJurisdictions.get(0).getJurisdictionId());
		
		// Count existing Tax Jurisdictions
		newNumTaxJur = jurisdictionService.count();

		// Make sure newNumTaxJur is one larger than prevNumTaxJur
		System.out.println("Previous Tax Juridictin Count = (" + prevNumTaxJur + ") New Tax Jurisdiction Count = (" + newNumTaxJur + ")");
		assertNotSame(prevNumTaxJur.longValue(), newNumTaxJur.longValue());

		// 2. Copy and Add an existing Tax Jurisdiction
		// Search for newly created Tax Jurisdiction
		Jurisdiction searchTJ = new Jurisdiction();
		searchTJ.setClientGeocode("01234567");
		List<Jurisdiction> tJurs = jurisdictionService.findByExample(searchTJ, 0);
		assertNotNull(tJurs);
		assertEquals(1, tJurs.size());

		System.out.println("Searching for records with client_geocode of 01234567: " + tJurs.size());

		// Get the search count from the method
		Long searchCount = jurisdictionService.count(searchTJ);
		assertEquals(tJurs.size(), searchCount.intValue());
		System.out.println("Searching for records with client_geocode of 01234567. Array size (: " + tJurs.size() + ") Method = (" + searchCount + ")");

		// Check in the database if Tax Jurisdiction exists
		boolean status = (jurisdictionService.findById(tJurs.get(0).getJurisdictionId()) != null);
		System.out.println("Checking if Tax Jurisdiction (" + tJurs.get(0).getJurisdictionId() + ") exists: " + status);
		assertTrue(status);

		// Check to make sure invalid Tax Jurisdiction Ids return false
		Long badJurisdictionId = 2999999999L;
		status = (jurisdictionService.findById(badJurisdictionId) != null);
		System.out.println("Checking if Tax Jurisdiction (" + badJurisdictionId + ") exists: " + status);
		assertFalse(status);

		// Now copy and add a few new Tax Jurisdictions
		prevNumTaxJur = jurisdictionService.count();

		System.out.println("Creating 3 new Tax Jurisdictions");
		Jurisdiction copiedTJ;
		Long srcJurisdictionId = tJurs.get(0).getJurisdictionId();

		copiedTJ = jurisdictionService.copyToNew(srcJurisdictionId);
		copiedTJ.setClientGeocode("01234567-1"); // Change a value from the
		// original
		jurisdictionService.save(copiedTJ);

		copiedTJ = jurisdictionService.copyToNew(srcJurisdictionId);
		copiedTJ.setClientGeocode("01234567-2"); // Change a value from the
		// original
		jurisdictionService.save(copiedTJ);

		copiedTJ = jurisdictionService.copyToNew(srcJurisdictionId);
		copiedTJ.setClientGeocode("01234567-3"); // Change a value from the
		// original
		jurisdictionService.save(copiedTJ);

		newNumTaxJur = jurisdictionService.count();
		// Make sure newNumTaxJur is one larger than prevNumTaxJur
		System.out.println("Previous Tax Juridictin Count = (" + prevNumTaxJur + ") New Tax Jurisdiction Count = (" + newNumTaxJur + ")");
		assertNotSame(prevNumTaxJur.longValue(), newNumTaxJur.longValue());

		// 3. Search for those Tax Jurisdictions
		searchTJ = new Jurisdiction();
		searchTJ.setClientGeocode("01234567");
		tJurs = jurisdictionService.findByExample(searchTJ, 0);
		assertNotNull(tJurs);
		assertEquals(4, tJurs.size());
		System.out.println("Searching for records with client_geocode of 01234567: " + tJurs.size());

		// 4. Update a Tax Jurisdiction
		// Get a single Tax Jurisdiction
		jurisdiction = jurisdictionService.findById(tJurs.get(0).getJurisdictionId());

		// Modify the Tax Jurisdiction
		jurisdiction.setCompGeocode("TEST");

		// Update the Tax Jurisdiction to the database
		jurisdictionService.update(jurisdiction);

		// Get the Tax Jurisdiction back from database
		Jurisdiction updateTJ = jurisdictionService.findById(jurisdiction.getJurisdictionId());

		// Perform compare to see if they are the same
		assertEquals(jurisdiction.getCompGeocode(), updateTJ.getCompGeocode());

		// 5. Get several Tax Jurisdictions
		List<Long> jIds = new ArrayList<Long>();
		//
		// Get the jurisdictionIds of the 4 created Tax Jurisdictions and put
		// them into a List

		for (Jurisdiction fTJ : tJurs) {
			jIds.add(fTJ.getJurisdictionId());
		}

		// Get the list of Tax Jurisdictions
		List<Jurisdiction> listTJ = jurisdictionService.findById(jIds);
		assertEquals(jIds.size(), listTJ.size());
		System.out.println("Expecting to retrieve (" + jIds.size() + ") records and got back (" + listTJ.size() + ").");

		// 6. Delete all created Tax Jurisdictions
		prevNumTaxJur = jurisdictionService.count();
		for (Jurisdiction fTJ : tJurs) {
			jurisdictionService.remove(fTJ);
		}
		newNumTaxJur = jurisdictionService.count();

		// Make sure newNumTaxJur is one larger than prevNumTaxJur
		System.out.println("Previous Tax Juridictin Count = (" + prevNumTaxJur + ") New Tax Jurisdiction Count = (" + newNumTaxJur + ") Original Tax Jurisdiction Count = (" + originalNumOfTaxJurisdictions + ")");
		assertNotSame(prevNumTaxJur.longValue(), newNumTaxJur.longValue());
		assertEquals(originalNumOfTaxJurisdictions.longValue(), newNumTaxJur.longValue());

		// Search for invalid Tax Jurisdiction
		System.out.println("Checking to see if searching for bad DTO returns no results");
		Jurisdiction badJurisdiction = new Jurisdiction();
		badJurisdiction.setJurisdictionId(99999999L);
		List<Jurisdiction> badJurisdictionList = jurisdictionService.findByExample(badJurisdiction, 0);
		assertEquals(0, badJurisdictionList.size());
		
		System.out.println("Execution Time: " + (System.currentTimeMillis() - startTime) / 1000.0f + " sec");
	}*/

	@Test
	public void testTaxRateMethods() {
		//
		// Test performs the following actions with should call all TaxRate
		// methods
		// 1. Add / Create several new Tax Rates
		// 2. Copy and Add an existing Tax Rate
		// 3. Search for those Tax Rates
		// 4. Update a Tax Rate
		// 5. Get several Tax Rates
		// 6. Delete all created Tax Rates
		// 7. Create a new Tax Jurisdiction
		// 8. Search for Tax Rate by Jurisdiction
		// 9. delete Tax Rate by Jurisdiction Id
		//10. Search for Tax Rate and return JurisdictionIds
		//

		long startTime = System.currentTimeMillis(); // Set Timer
		Long originalNumOfTaxRates;

		// 1. Add / Create several new Tax Rates
		Long prevNumTaxRate = 0L;
		Long newNumTaxRate = 0L;

		// Create the new TaxRate Object
		JurisdictionTaxrate taxRate = new JurisdictionTaxrate();
		Date today = new Date();
		// Date now = new Date();
		// today.setTime(now);
		taxRate.setJurisdictionTaxrateId(1234l);
		taxRate.setJurisdiction(new Jurisdiction(99850l));
//ac 		taxRate.setMeasureTypeCode("8");
		taxRate.setEffectiveDate(today);
		taxRate.setExpirationDate(today);
		taxRate.setCustomFlag("1");
		taxRate.setModifiedFlag("1");
		//ac 		taxRate.setStateSalesRate(ArithmeticUtils.toBigDecimal(1234567890.1234));
		//ac 		taxRate.setStateUseRate(ArithmeticUtils.toBigDecimal(1234567890.1234));
		taxRate.setStateUseTier2Rate(ArithmeticUtils.toBigDecimal(1234567890.1234));
		taxRate.setStateUseTier3Rate(ArithmeticUtils.toBigDecimal(1234567890.1234));
		//ac 		taxRate.setStateSplitAmount(1234567890L);
		//ac 		taxRate.setStateTier2MaxAmount(1234567890L);
		//ac 		taxRate.setStateMaxtaxAmount(ArithmeticUtils.toBigDecimal(1234567890.1234));
		//ac 		taxRate.setCountySalesRate(ArithmeticUtils.toBigDecimal(1234567890.1234));
		//ac 		taxRate.setCountyUseRate(ArithmeticUtils.toBigDecimal(1234567890.1234));

		// Count existing Tax Rates
		prevNumTaxRate = taxRateService.count();
		originalNumOfTaxRates = prevNumTaxRate;
		assertNotSame(prevNumTaxRate.longValue(), 0L);

		// Add the new TaxRate Object
		System.out.println("Adding the new TaxRate Object");
		taxRateService.save(taxRate);

		/*// Count existing Tax Jurisdictions
		newNumTaxRate = taxRateService.count();

		// Make sure newNumTaxRate is one larger than prevNumTaxRate
		System.out.println("Previous Tax Rate Count = (" + prevNumTaxRate + ") New Tax Rate Count = (" + newNumTaxRate + ")");
		assertNotSame(prevNumTaxRate.longValue(), newNumTaxRate.longValue());

		// 2. Copy and Add an existing Tax Rate
		// Search for newly created Tax Rate
		JurisdictionTaxrate searchTR = new JurisdictionTaxrate();
		searchTR.setStateSalesRate(1234567890.1234);
		List<JurisdictionTaxrate> tRates = taxRateService.findByExample(searchTR, 0);
		assertNotNull(tRates);
		assertEquals(1, tRates.size());

		System.out.println("Searching for records with state_sales_rate of 1234567890.1234: " + tRates.size());

		// Get the search count from the method
		Long searchCount = taxRateService.count(searchTR);
		assertEquals(tRates.size(), searchCount.intValue());
		System.out.println("Searching for records with state_sales_rate of 1234567890.1234. Array size (: " + tRates.size() + ") Method = (" + searchCount + ")");

		// Check in the database if Tax Rate exists
		boolean status = (taxRateService.findById(tRates.get(0).getJurisdictionTaxrateId()) != null);
		System.out.println("Checking if Tax Rate (" + tRates.get(0).getJurisdictionTaxrateId() + ") exists: " + status);
		assertTrue(status);

		// Check to make sure invalid Tax Rate Ids return false
		Long badTaxRateId = 2999999999L;
		status = (taxRateService.findById(badTaxRateId) != null);
		System.out.println("Checking if Tax Rate (" + badTaxRateId + ") exists: " + status);
		assertFalse(status);

		// Now copy and add a few new Tax Rates
		prevNumTaxRate = taxRateService.count();

		System.out.println("Creating 3 new Tax Rates");
		JurisdictionTaxrate copiedTR = tRates.get(0);
		copiedTR.setJurisdictionTaxrateId(null);
		copiedTR.setEffectiveDate(null);
		copiedTR.setUpdateUserId(null);
		copiedTR.setStateSalesRate(1234567890.1); // Change a value from the original
		copiedTR.setMeasureTypeCode("2");
		taxRateService.save(copiedTR);
		copiedTR.setMeasureTypeCode("4");
		taxRateService.save(copiedTR);
		copiedTR.setMeasureTypeCode("0");
		taxRateService.save(copiedTR);

		newNumTaxRate = taxRateService.count();
		// Make sure newNumTaxRate is one larger than prevNumTaxRate
		System.out.println("Previous Tax Rate Count = (" + prevNumTaxRate + ") New Tax Rate Count = (" + newNumTaxRate + ")");
		assertNotSame(prevNumTaxRate.longValue(), newNumTaxRate.longValue());

		// 3. Search for those Tax Rates
		searchTR = new JurisdictionTaxrate();
		searchTR.setStateUseRate(1234567890.1234);
		tRates = taxRateService.findByExample(searchTR, 0);
		assertNotNull(tRates);
		assertEquals(4, tRates.size());
		System.out.println("Searching for records with state_use_rate of 1234567890.1234: " + tRates.size());

		//
		// 4. Update a Tax Rate
		//

		// Get a single Tax Rate
		taxRate = taxRateService.findById(tRates.get(0).getJurisdictionTaxrateId());

		// Modify the Tax Rate
		taxRate.setStateUseRate(1234567890.1);

		// Update the Tax Rate to the database
		taxRateService.update(taxRate);

		// Get the Tax Rate back from database
		JurisdictionTaxrate updateTR = taxRateService.findById(taxRate.getJurisdictionTaxrateId());

		// Perform compare to see if they are the same
		assertEquals(taxRate.getStateUseRate(), updateTR.getStateUseRate());

		//
		// 6. Delete all created Tax Rates
		// 
		prevNumTaxRate = taxRateService.count();
		for (JurisdictionTaxrate fTR : tRates) {
			taxRateService.remove(fTR);
		}
		newNumTaxRate = taxRateService.count();

		// Make sure newNumTaxRate is one larger than prevNumTaxRate
		System.out.println("Previous Tax Rate Count = (" + prevNumTaxRate + ") New Tax Rate Count = (" + newNumTaxRate + ") Original Tax Rate Count = (" + originalNumOfTaxRates + ")");
		assertNotSame(prevNumTaxRate.longValue(), newNumTaxRate.longValue());
		assertEquals(originalNumOfTaxRates.longValue(), newNumTaxRate.longValue());

		//
		// 7. Create a new Tax Jurisdiction and Tax Rate
		//

		// Create the new TaxJurisdiction Object
		Jurisdiction jurisdiction = new Jurisdiction();
		jurisdiction.setGeocode("0123456789");
		jurisdiction.setState("MA");
		jurisdiction.setCounty("TEST-COUNTY");
		jurisdiction.setCity("TEST-CITY");
		jurisdiction.setZip("11111");
		jurisdiction.setZipplus4("1111");
		jurisdiction.setInOut("B");
		jurisdiction.setCustomFlag("1");
		jurisdiction.setDescription("Sample Jurisdiction");
		jurisdiction.setClientGeocode("01234567");
		jurisdiction.setCompGeocode("0123456789");

		// Add the new Tax Jurisdiction to the database
		jurisdictionService.save(jurisdiction);

		// Now search for the newly created Tax Jurisdiction
		List<Jurisdiction> tJurs = jurisdictionService.findByExample(jurisdiction, 0);

		// Check that it exits
		assertTrue(jurisdictionService.findById(tJurs.get(0).getJurisdictionId()) != null);

		// Use previous reference to new Tax Rate and update with a reference to
		// the Tax Jurisdiction
		taxRate.setJurisdiction(new Jurisdiction(tJurs.get(0).getJurisdictionId()));
		// Add the Tax Rate to the database twice
		taxRate.setMeasureTypeCode("2");
		taxRateService.save(taxRate);
		taxRate.setMeasureTypeCode("4");
		taxRateService.save(taxRate);

		//
		// 8. Get Tax Rate by Jurisdiction Tax Rate
		//
		System.out.println ("Getting Tax Rate by Jurisdiction Tax Rate: " + taxRate.getJurisdiction().getJurisdictionId());
		List<JurisdictionTaxrate> newTaxRates = taxRateService.findByExample(taxRate, 0);
		assertEquals(2, newTaxRates.size());
		assertTrue(taxRateService.findById(newTaxRates.get(0).getJurisdictionTaxrateId()) != null);
		assertTrue(taxRateService.findById(newTaxRates.get(1).getJurisdictionTaxrateId()) != null);

		//
		// 9. delete Tax Rates by Jurisdiction Id than Delete Jurisdiction
		//
		Long rcnt = taxRateService.count();
		Long jcnt = jurisdictionService.count();
		taxRateService.remove(taxRate);
		Jurisdiction toRemoveTaxJurisdiction = new Jurisdiction();
		toRemoveTaxJurisdiction.setJurisdictionId(taxRate.getJurisdiction().getJurisdictionId());
		jurisdictionService.remove(toRemoveTaxJurisdiction);
		Long rcnt2 = taxRateService.count();
		Long jcnt2 = jurisdictionService.count();
		assertEquals(rcnt.longValue(), (rcnt2.longValue() + 2L));
		assertEquals(jcnt.longValue(), (jcnt2.longValue() + 1L));
		System.out.println("Inserted 2 new Tax Rates (Total=" + rcnt + ")");
		System.out.println("Deleted 2 Tax Rates (Total=" + rcnt2 + ")");
		System.out.println("Inserted 1 new Tax Jurisdiction (Total=" + jcnt + ")");
		System.out.println("Deleted 1 Tax Jurisdiction (Total=" + jcnt2 + ")");

		//10. Search for Tax Rate and return JurisdictionIds
		System.out.println("Searching for tax rates and returing jurisdictionIds.");
		JurisdictionTaxrate searchTaxRate = new JurisdictionTaxrate();
		Calendar effDate  = Calendar.getInstance (  ) ;
		
		effDate.set(1967, Calendar.OCTOBER, 1);
		searchTaxRate.setEffectiveDate(effDate.getTime());
		List<Long> sJids = new ArrayList<Long>();
		
		{
			List<JurisdictionTaxrate> foundTaxRates = taxRateService.findByExample(searchTaxRate, 0);
			for (JurisdictionTaxrate foundTaxRate : foundTaxRates) {
				Long foundJurisdictionId = foundTaxRate.getJurisdiction().getJurisdictionId();
				if ((foundJurisdictionId != null) && (!sJids.contains(foundJurisdictionId))) {
					sJids.add(foundJurisdictionId);
				}
			}
		}
		System.out.println("Searching for tax rates.  Found="+sJids.size());
		for (Long sJid : sJids) {
			System.out.println("Jurisdiction Id: " + sJid);
		}
		assertNotSame(0,sJids.size());
		
		System.out.println("Now search for the JurisdictionIds in the Jurisdiction Table.");
		List<Jurisdiction> sJurisdictions = jurisdictionService.findById(sJids);
		System.out.println("Should return the same amount as tax rates: " + sJurisdictions.size());
		assertEquals(sJids.size(), sJurisdictions.size());
		
		System.out.println("Execution Time: " + (System.currentTimeMillis() - startTime) / 1000.0f + " sec");*/
	}

	/*protected static String GenerateRandomString(int length) {
		char[] rstring = new char[length];
		int c = 'A';
		int r1 = 0;
		for (int i = 0; i < length; i++) {
			r1 = (int) (Math.random() * 3);
			switch (r1) {
			case 0:
				c = '0' + (int) (Math.random() * 10);
				break;
			case 1:
				c = 'a' + (int) (Math.random() * 26);
				break;
			case 2:
				c = 'A' + (int) (Math.random() * 26);
				break;
			}
			rstring[i] = (char) c;
		}
		return new String(rstring);
	}*/

}
