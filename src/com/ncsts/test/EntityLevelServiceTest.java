package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.EntityLevel;
import com.ncsts.dto.EntityLevelDTO;
import com.ncsts.services.EntityLevelService;

@RunWith(TestClassRunner.class)
public class EntityLevelServiceTest extends STSTestBase {
	
	EntityLevelService entityLevelService = 
		(EntityLevelService) getAppContext().getBean("entityLevelService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    // FK and Constraints check for non existent
    // trans dtl col and also duplicate col name (UPDATE)
/*    @Test
    public void testUpdate(){
    	EntityLevelDTO entityLevelDTO = new EntityLevelDTO();
    	entityLevelDTO.setEntityLevelId(3l);
    	entityLevelDTO.setTransDetailColumnName("AFE_CATEGORY_NAME");
    	entityLevelDTO.setDescription("AFE/Project Category Name");
    	entityLevelService.update(entityLevelDTO);
    }*/
    
    // FK and Constraints check for non existent
    // trans dtl col and also duplicate col name (SAVE)
    @Test
    public void testUpdate(){
    	EntityLevelDTO entityLevelDTO = new EntityLevelDTO();
    	entityLevelDTO.setEntityLevelId(5l);
    	entityLevelDTO.setTransDetailColumnName("SHIP_TO_ADDRESS_CITY");
    	entityLevelDTO.setDescription("test");
    	entityLevelService.save(entityLevelDTO);
    }
    
/*    @Test
    public void testFindById() {
        EntityLevel entityLevel = entityLevelService.findById(1L);
        System.out.println("description  = " + entityLevel.getDescription());     
    }    	          

    @Test
    public void testGetAllEntityItems() {
        List<EntityLevel> list = entityLevelService.findAllEntityLevels();
        for (EntityLevel entityLevel : list) {
        	System.out.println("id = " + entityLevel.getEntityLevelId());
        	System.out.println("description = " + entityLevel.getDescription());  
        }
        System.out.println("size = " + list.size());     

    }     */
    
} 






