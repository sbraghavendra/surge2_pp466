package com.ncsts.test;

import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.ejb.HibernateEntityManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.ncsts.dao.CommonDAO;
import com.ncsts.domain.TransactionDetail;

/**
 * <b>Created:</b> Mar 25, 2008<br>
 * <b>Title:</b> CommonDAOTest<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * @todo add class overview here
 */
@RunWith(TestClassRunner.class)
public class CommonDAOTest extends STSTestBase {
    private static final transient Log log = LogFactory.getLog(com.ncsts.test.CommonDAOTest.class);
    
    private CommonDAO commonDAO = 
    	(CommonDAO) getAppContext().getBean("commonDAO");
    
    // Some entities for testing
    private TransactionDetail detail1;
    private TransactionDetail detail2;
    private TransactionDetail detail3;
    
    private String uniqueCategoryName = null;
    private String uniqueDescription = null;
    
    /**default empty constructor*/
    public CommonDAOTest() {
        this.setDefaultRollback(true);
    }
    
    @Before
    public void initialize() throws Exception {
        if (log.isInfoEnabled()) { log.info("BEGIN initialize()"); }
        super.setUp();
        
        uniqueCategoryName = "NAME: " + new Random().nextInt();
        
        detail1 = new TransactionDetail();
        detail1.setTransactionDetailId(new Random().nextLong());
        detail1.setAfeCategoryName("test1");
        detail1.setInvoiceDesc(uniqueDescription);
        
        detail2 = new TransactionDetail();
        detail2.setTransactionDetailId(new Random().nextLong());
        detail2.setAfeCategoryName(uniqueCategoryName);
        
        detail3 = new TransactionDetail();
        detail3.setTransactionDetailId(new Random().nextLong());
        detail3.setAfeCategoryName("test3");
        
        commonDAO.persist(detail1);
        commonDAO.persist(detail2);
        commonDAO.persist(detail3);
        
        if (log.isInfoEnabled()) { log.info("END initialize()"); }
    }
    
    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }   
    
    @Test
    public void testCountAll(){
        Long count = commonDAO.getTotalCount(TransactionDetail.class);
        if (log.isInfoEnabled()) { log.info("count: " + count); }
        assertTrue("Count was: " + count, count.longValue() > 3);
    }
    @Test
    public void testExamplePaging(){
//        TestDAO dao = getTestDAO();
//        TransactionDetail fake = new TransactionDetail();
////        fake.setAfeCategoryName("TEST123");
//        fake.setInvoiceDesc("test3");
//        
//        List<Object> list = dao.getByExample(TransactionDetail.class, fake, new String[0]);
//        if (log.isInfoEnabled()) { log.info("list: " + list); }
        
        Long count = commonDAO.getTotalCount(TransactionDetail.class, detail2, new String[0]);
        if (log.isInfoEnabled()) { log.info("count: " + count); }
        assertEquals("Count was: " + count, count, new Long(1)); // Should just get 1 back based on uniqueDescription
    }

    protected TestDAO getTestDAO (){
        TestDAO dao = new TestDAO();
       
        EntityManagerFactory emFactory = (EntityManagerFactory)getApplicationContext().getBean("entityManagerFactory");
        dao.setEntityManagerFactory(emFactory);
        
        return dao;
    }

    protected class TestDAO extends JpaDaoSupport {
        @Transactional
        public void TestDAO(){
            getJpaTemplate().flush();
        }
        
        @SuppressWarnings("unchecked")
		public List<Object> getByExample(Class<?> clazz, Object exampleInstance, String[] excludeProperties){
            if (log.isInfoEnabled()) { log.info("BEGIN getTotalCount(Class, Object, String[]): " + clazz + " : " + exampleInstance + " | " + excludeProperties); }
            
            EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
            Session session = entityManager instanceof HibernateEntityManager 
                    ? ((HibernateEntityManager) entityManager).getSession()
                    : ((HibernateEntityManager) entityManager.getDelegate()).getSession();
                    
            Criteria crit = session.createCriteria(clazz);
            // Add the example and excludes
            Example example = Example.create(exampleInstance);
            for (String exclude : excludeProperties) {
                example.excludeProperty(exclude);
            }
            crit.add(example);
            
            return crit.list();
        }
    }
}
