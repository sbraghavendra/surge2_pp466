package com.ncsts.test;
/**
 * @author Muneer Basha
 */
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.dto.ImportDefDTO;
import com.ncsts.dto.ImportSpecDTO;
import com.ncsts.services.ImportDefAndSpecsService;

@RunWith(TestClassRunner.class)
public class ImportDefinitionsAndSpecsTest extends STSTestBase {
	
	ImportDefAndSpecsService importDefAndSpecsService = 
		(ImportDefAndSpecsService) getAppContext().getBean("importDefAndSpecsService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    // for FK and Constraints
/*    @Test
    public void testSave(){
    	ImportSpecDTO importSpecDTO = new ImportSpecDTO();
    	importSpecDTO.setImportSpecCode("testK");
    	importSpecDTO.setImportSpecType("testTYPE");
    	importSpecDTO.setImportDefinitionCode("TESTAGAIN");
    	importDefAndSpecsService.addImportSpecRecord(importSpecDTO);
    	
    	
    }*/
    //  FK and Constraints (test for  implementations of the conditional references)
    //'IMPFILETYP'
/*    @Test
    public void testUpdate(){
    	ImportDefDTO importDefDTO = new ImportDefDTO();
    	importDefDTO.setImportDefinitionCode("TESTFF");
    	importDefDTO.setImportFileTypeCode("FD");
    	
    	importDefAndSpecsService.update(importDefDTO);
    }*/
    
    //  FK and Constraints (test for  implementations of the conditional references)
    //'BATCHTYPE'
    @Test
    public void testUpdateSpec(){
    	ImportSpecDTO importSpecDTO = new ImportSpecDTO();
    	importSpecDTO.setImportSpecType("GE1");
    	importSpecDTO.setImportSpecCode("GE_TEST");
    	importSpecDTO.setComments("test 22-11-2008");
    	importDefAndSpecsService.updateImportSpec(importSpecDTO);
    }
    
    
    // for FK and Constraints
   /* @Test
    public void testDelete(){
  
    importDefAndSpecsService.delete("TESAGAIN");
    }*/
    // for FK and Constraints 
/*    @Test
    public void testDeleteImportSpec(){
    	logger.info("start delete action");
    	ImportSpecPK importSpecPK = new ImportSpecPK();
    	importSpecPK.setImportSpecCode("TEST-KK");
    	importSpecPK.setImportSpecType("IP");
    	importDefAndSpecsService.deleteImportSpec(importSpecPK);
    	logger.info("END");
    }*/

   /* @Test
    public void testGetAllImportDefinition(){
    	
    	System.out.println(" Start of Test case:");
    	List<ImportDefDTO> importDefinitionList = importDefAndSpecsService.getAllImportDefinition();
    	System.out.println(" Test results from testGetAllBatchMaintenanceData\n");
    	if(importDefinitionList != null){
    		System.out.println(" Size of importDefinitionList is :"+importDefinitionList.size()+"\n");
    		ImportDefDTO a = (ImportDefDTO)((Object)importDefinitionList.get(0));
    		System.out.println(" One Object values ::"+a.getImportDefinitionCode()+" ::"+a.getImportFileTypeCode());
          }
    	else
    		System.out.println("  importDefinitionList is :"+importDefinitionList+"\n");
    	assertEquals(23, importDefinitionList.size());
    }

    @Test
    public void testGetAllBySpec(){
    	List<ImportSpec> importDefinitionList = importDefAndSpecsService.getAllForUserByType("IP");
    	if(importDefinitionList != null){
    		System.out.println(" Size of importDefinitionList is :"+importDefinitionList.size()+"\n");
    		ImportSpec a = importDefinitionList.get(0);
    		System.out.println(" One Object values ::"+a.getImportDefinitionCode()+" ::"+a.getImportSpecCode());
    	}
    	else
    		System.out.println("  importDefinitionList is :"+importDefinitionList+"\n");
    	assertEquals(23, importDefinitionList.size());
    }*/
    

/*    @Test
    public void testUpdate(){

    	ImportDefDTO lc = new ImportDefDTO();
    	lc.setImportDefinitionCode("ANDREW_TEST");
    	lc.setDescription("Test by Muneer");
    	System.out.println("before calling update of service Bean");
    	importDefAndSpecsService.update(lc);
    	System.out.println("end of update in Junit test");
    }   

 
    @Test
    public void testAddImportDefRecord(){

    	ImportDefDTO lc = new ImportDefDTO();
    	lc.setImportDefinitionCode("MUNEER_TEST");
    	lc.setDescription("By Muneer");
    	lc.setDescription("some data found");    	
    	System.out.println("before calling add of service Bean in Test Case");
    	importDefAndSpecsService.addImportDefRecord(lc);
    	System.out.println("end of update in Junit test");
    }
      
    @Test
    public void testDelete(){

    	String pk= "MUNEER_TEST";
    	
    		importDefAndSpecsService.delete(pk);
    	logger.info("Delete completed ");
    }   
    
*/  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////    Test Case Methods related to Import Specs ////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    
/*    @Test
    public void testGetAllImportSpec(){
    	
    	System.out.println(" Start of Test case:");
    	List<ImportSpecDTO> importSpecList = importDefAndSpecsService.getAllImportSpec();
    	System.out.println(" Test results from testGetAllImportSpec\n");
    	if(importSpecList != null){
    		System.out.println(" Size of importSpecList is :"+importSpecList.size()+"\n");
    		ImportSpecDTO a = (ImportSpecDTO)((Object)importSpecList.get(0));
    		System.out.println(" One Object values ::"+a.getImportSpecType()+" ::"+a.getImportSpecCode()+" ::"+a.getDescription());
          }
    	else
    		System.out.println("  importSpecList is :"+importSpecList+"\n");
    	assertEquals(23, importSpecList.size());
    }*/

 
/*    @Test
    public void testAddImportSpecRecord(){

    	ImportSpecDTO lc = new ImportSpecDTO();
    	lc.setImportSpecCode("MUNEER_TEST");
    	lc.setImportSpecType("IP");
    	lc.setImportDefinitionCode("ANDREW_TEST");
    	lc.setComments("Test by Muneer1");
    	lc.setDefaultDirectory("C:\\temp\\test\\");
    	lc.setDescription("Test it for Add");
    	
    	System.out.println("before calling add of service Bean in Test Case");
    	importDefAndSpecsService.addImportSpecRecord(lc);
    	System.out.println("end of update in Junit test");
    }*/
       
/*    @Test
    public void testUpdateImportSpec(){

    	ImportSpecDTO lc = new ImportSpecDTO();
    	lc.setImportSpecCode("MUNEER_TEST");
    	lc.setImportSpecType("IP");
    	lc.setComments("Test by Muneer");
    	System.out.println("before calling update of service Bean");
    	importDefAndSpecsService.updateImportSpec(lc);
    	System.out.println("end of update in Junit test");
    }*/   
      
/*    @Test
    public void testDeleteImportSpec(){

    	String specCode = "MUNEER_TEST";
    	String specType = "IP";
    	ImportSpecPK pk = new ImportSpecPK();
    	pk.setImportSpecCode(specCode);
    	pk.setImportSpecType(specType);
    	
        importDefAndSpecsService.deleteImportSpec(pk);
        
    	logger.info("Delete completed ");
    } */  
    
/*    @Test
    public void testGetImportSpecUserBySpec(){
    	
    	System.out.println(" Start of Test case:testGetImportSpecUserBySpec ");
    	
    	ImportSpecDTO input = new ImportSpecDTO();
    	input.setImportSpecType("IP");
    	input.setImportSpecCode("ANDREW_TEST");
    	
    	List<UserDTO> userList = importDefAndSpecsService.getImportSpecUserBySpec(input);
    	System.out.println(" Test results from testGetImportSpecUserBySpec\n");
    	if(userList != null){
    		System.out.println(" Size of importSpecUserList is :"+userList.size()+"\n");
    		UserDTO a = (UserDTO)((Object)userList.get(0));
    		System.out.println(" One Object values ::"+a.getUserCode()+" ::"+a.getUserName()+" ::"+a.getActiveFlag());
          }
    	else
    		System.out.println("  userList is :"+userList+"\n");
    	assertEquals(3, userList.size());
    } */   
}
