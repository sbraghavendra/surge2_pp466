package com.ncsts.test;

import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustCert;
import com.ncsts.dao.CustDAO;


@RunWith(TestClassRunner.class)
public class CustServiceTest extends STSTestBase {
	
	CustDAO custDAO = (CustDAO) getAppContext().getBean("custDAO");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
  
    @Test
    public void testCodeUpdate(){
    	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    	
    	CustCert exampleInstance = new CustCert();
    	exampleInstance.setCustNbr("ABC");
    	exampleInstance.setCustName("ABC Corporation");
    	exampleInstance.setCustActiveFlag("1");
  //  	exampleInstance.setEntityId(51L);
    	exampleInstance.setExemptionTypeCode("1");

    	exampleInstance.setCustLocnCode("ABC 456");
    	exampleInstance.setCustLocnName("Location 456");
    	exampleInstance.setCustLocnCountry("US");
    	exampleInstance.setCustLocnState(null);
    	exampleInstance.setCustLocnCity(null);
    	exampleInstance.setCustLocnActiveFlag(null);


    	exampleInstance.setCountry("US");
 //   	exampleInstance.setState("IL");
    	try{
    		exampleInstance.setEffectiveDate(df.parse("07/08/2012"));
    		exampleInstance.setEffectiveDateThru(df.parse("07/20/2012"));
    		exampleInstance.setExpirationDate(df.parse("07/18/2012"));
    	}
    	catch(Exception e){
    		e.toString();
    	}
    	
    	List<CustCert> aList = custDAO.find(exampleInstance, null, 1, 10);
    	Long aCount = custDAO.count(exampleInstance);
    	
    }
}
