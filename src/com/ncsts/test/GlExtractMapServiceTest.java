package com.ncsts.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dto.GlExtractMapDTO;
import com.ncsts.services.GlExtractMapService;

@RunWith(TestClassRunner.class)
public class GlExtractMapServiceTest extends STSTestBase {
	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(com.ncsts.test.GlExtractMapServiceTest.class);
	
	GlExtractMapService glExtractMapService = 
		(GlExtractMapService) getAppContext().getBean("glExtractMapService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }   	          

    @Test
    public void testNothing(){
        
    }
    //  FK and Constraints (test for  implementations of the conditional references)
    @Test
    public void testSave() {
    	GlExtractMapDTO glExtractMapDTO = new GlExtractMapDTO();
    	//glExtractMapDTO.setGlExtractMapId(new Long(1));
    	glExtractMapDTO.setComments("11/22/2008 -- 1");
    	glExtractMapDTO.setTaxJurisTypeCode("ST_U");
    	glExtractMapService.save(glExtractMapDTO);
    }
/*    @Test
    public void testUpdate() {
    	GlExtractMapDTO glExtractMapDTO = new GlExtractMapDTO();
    	glExtractMapDTO.setGlExtractMapId(new Long(1));
    	glExtractMapDTO.setComments("updated by anand");
    	glExtractMapService.update(glExtractMapDTO);
    }
    @Test
    public void testRemove() {
    	glExtractMapService.remove(new Long(1));
    }*/
    
}
