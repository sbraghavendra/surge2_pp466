package com.ncsts.test;

import com.ncsts.dao.CommonDAO;
import com.ncsts.domain.DataDefinitionColumnDrivers;
import com.ncsts.domain.PurchaseTransaction;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.ejb.HibernateEntityManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(TestClassRunner.class)
public class PurchaseTransactionTest extends STSTestBase {
    private CommonDAO commonDAO =
            (CommonDAO) getAppContext().getBean("commonDAO");

    @Before
    public void setup() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    //@Test
    public void createRecord() {
        PurchaseTransaction pt = new PurchaseTransaction();
        pt.setComments("second one");
        pt.setUserText01("UserText1");
        pt.setShiptoLat("Lat");
        pt.setCountyName("Suffolk");
        //pt.setPurchtransId(99999);
        commonDAO.persist(pt);

    }
    //@Test
    public void testGetDDCDriver() {

        TestDAO testDao = new TestDAO();
        DataDefinitionColumnDrivers example = new DataDefinitionColumnDrivers();
       // example.setTableName("TB_PURCHTRANS");
        example.setDriverNamesCode("L");

        List l  = testDao.getByExample(DataDefinitionColumnDrivers.class, example, new String[0]);

        System.out.println("l = " + l);
    }



    protected class TestDAO extends JpaDaoSupport {
        @Transactional
        public void TestDAO(){
            getJpaTemplate().flush();
        }

        @SuppressWarnings("unchecked")
        public List<Object> getByExample(Class<?> clazz, Object exampleInstance, String[] excludeProperties){
            //if (log.isInfoEnabled()) { log.info("BEGIN getTotalCount(Class, Object, String[]): " + clazz + " : " + exampleInstance + " | " + excludeProperties); }

            EntityManager entityManager = getJpaTemplate().getEntityManagerFactory().createEntityManager();
            Session session = entityManager instanceof HibernateEntityManager
                    ? ((HibernateEntityManager) entityManager).getSession()
                    : ((HibernateEntityManager) entityManager.getDelegate()).getSession();

            Criteria crit = session.createCriteria(clazz);
            // Add the example and excludes
            Example example = Example.create(exampleInstance);
            for (String exclude : excludeProperties) {
                example.excludeProperty(exclude);
            }
            crit.add(example);

            return crit.list();
        }
    }


}
