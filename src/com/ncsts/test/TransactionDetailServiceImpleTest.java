package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Batch;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.services.TransactionDetailService;


@RunWith(TestClassRunner.class)
public class TransactionDetailServiceImpleTest extends STSTestBase {
	
	TransactionDetailService transactionDetailService = 
		(TransactionDetailService) getAppContext().getBean("transactionDetailService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
   /* //FK AND CONSTRAINT (TEST CONSTRAINT VIOLATION)
    @Test
   public void testSave(){
    	TransactionDetail transactionDetail = new TransactionDetail();
    	//transactionDetail.setProcessBatchNo(1455555l);
    	transactionDetail.setLocationMatrixId(98888888888l);
    	transactionDetailService.save(transactionDetail);
    }*/
    
/*    @Test
    public void testUpdate(){
     	TransactionDetail transactionDetail = new TransactionDetail();
     	transactionDetail.setTransactionDetailId(3163385l);
     	transactionDetail.setCchItemCode("tes");
     	transactionDetail.setCchTaxcatCode("01");
     	transactionDetail.setCchGroupCode("0000");
     	transactionDetail.setJurisdictionId(115527l);
     	transactionDetail.setTaxcodeDetailId(820l);
     	transactionDetail.setTaxcodeCode("LEASE");
     	transactionDetail.setTaxcodeTypeCode("E");
     	transactionDetail.setTaxcodeStateCode("*ALL");
     	transactionDetail.setComments("test");
     	transactionDetailService.update(transactionDetail);
     }*/

    /*
    @Test
    public void testJdbcQueryUsingJdbcTemplate() {
        int count = jdbcTemplate.queryForInt("select count(*) from TB_TAXCODE_DETAIL");
    	System.out.println("Count = " + count);        
        assertEquals(count,380);
    }
    */
    
/*    @Test
    public void testFindTaxCodeDetail() {
    	TransactionDetail transactionDetail = (TransactionDetail)transactionDetailService.findById(new Long(18596226));
    	String vendorAddressZip = transactionDetail.getVendorAddressZip();
        assertEquals(vendorAddressZip,"29405");
    }
    
    @Test
    public void testFindAllPlantNumber() {
    	List<TransactionDetail> transactionDetailList = transactionDetailService.findAllPlantNumber();
    	int listSize = transactionDetailList.size();
    	assertEquals(112, listSize);
    	//String vendorAddressZip = transactionDetail.getVendorAddressZip();
        //assertEquals(vendorAddressZip,"29405");
    }
    
    @Test
    public void testFindAllCostCenterNumber(){
    	List<TransactionDetail> transactionDetailList = transactionDetailService.findAllCostCenterNumber();
    	int listSize = transactionDetailList.size();
    	assertEquals(1438, listSize);
    	//String vendorAddressZip = transactionDetail.getVendorAddressZip();
        //assertEquals(vendorAddressZip,"29405");
    }
    
    @Test
    public void testfindAllVendorNumber(){
    	List<TransactionDetail> transactionDetailList = transactionDetailService.findAllVendorNumber();
    	int listSize = transactionDetailList.size();
    	assertEquals(13592, listSize);
    	//String vendorAddressZip = transactionDetail.getVendorAddressZip();
        //assertEquals(vendorAddressZip,"29405");
    }
    
    @Test
    public void testFindAllVendorZip(){
    	List<TransactionDetail> transactionDetailList = transactionDetailService.findAllVendorZip();
    	int listSize = transactionDetailList.size();
    	assertEquals(2795, listSize);
    	//String vendorAddressZip = transactionDetail.getVendorAddressZip();
        //assertEquals(vendorAddressZip,"29405");
    }*/
    
    
    @Test
    public void testFindAll(){
    	Long count = transactionDetailService.count();
    	System.out.println("count = " + count);
    }
    
/*    @Test
    public void testgetProcessedTransactionDetails(){
    	TransactionDetailDTO transaction=new TransactionDetailDTO();
    	transaction.setTaxMatrixId(25443L);
    	transactionDetailService.getProcessedTransactionDetails(transaction);
    }*/
   /* @Test
    public void getTransactionDetailRecordsWithDynamicWhere(){
    	String whereClause="WHERE ( tb_transaction_detail.jurisdiction_id = tb_jurisdiction.jurisdiction_id(+) ) AND transaction_ind='S' AND suspend_ind='L'";
    	List<TransactionDetail> list = transactionDetailService.getTransactionDetailRecordsWithDynamicWhere(whereClause);
    	
    }*/
    /*
    @Test
    public void testSuspendLM(){
    	transactionDetailService.suspendLM(new Long(174));
    	System.out.println("suspended transaction");
    	//assertEquals(63, list.size());
    }
    */
}
