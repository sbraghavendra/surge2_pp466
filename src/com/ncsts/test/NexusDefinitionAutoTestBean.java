package com.ncsts.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.view.bean.NexusDefinitionAutoBackingBean;

@Component
@Scope("session")
public class NexusDefinitionAutoTestBean {
	@Autowired
	private NexusDefinitionAutoBackingBean nexusDefinitionAutoBackingBean;
	
	public String autoAction() {
		//Test Data
		Jurisdiction j = new Jurisdiction();
		j.setCountry("US");
		j.setState("CT");
		j.setCounty("FAIRFIELD");
		j.setCity("GROTON");
		j.setStj1Name("MBTA");
		
		try {
			nexusDefinitionAutoBackingBean.setOkView("nexus_def_auto_test");
			String result = nexusDefinitionAutoBackingBean.process("EN", 1L, 1L, j);
			if(!"".equals(result)) {
				return result;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
