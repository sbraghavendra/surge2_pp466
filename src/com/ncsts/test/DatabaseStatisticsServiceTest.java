package com.ncsts.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dto.DatabaseTransactionStatisticsDTO;
import com.ncsts.dto.LocationMatrixTransactionStatiticsDTO;
import com.ncsts.dto.TaxMatrixTransactionStatiticsDTO;
import com.ncsts.services.DatabaseStatisticsService;

@RunWith(TestClassRunner.class)
public class DatabaseStatisticsServiceTest extends STSTestBase {
		
		DatabaseStatisticsService dbService = 
			(DatabaseStatisticsService) getAppContext().getBean("databaseStatisticsService");
		
	    @Before
	    public void initialize() throws Exception {
	        super.setUp();
	    }
	    

	    @After
	    public void cleanup() throws Exception {
	        super.tearDown();
	    }
	    
	    
	/*    @Test
	    public void testTransactionsStatistics() {
	    	try{
	    	List<DatabaseTransactionStatisticsDTO> dataList = dbService.getTransactionsStatistics(null);
	    	
	    	System.out.println("print data");
	    	int listSize = 0;
	    	if(dataList.size() > 0){
	    	DatabaseTransactionStatisticsDTO lm = dataList.get(0);
	    	System.out.println("title no == " + lm.getTitle());
	    	System.out.println("average ==" + lm.getAvg());
	    	System.out.println("count == " + lm.getCount());
	    	
	    	listSize = dataList.size();
	    	System.out.println("size="+listSize);
	    	}

	    	assertEquals(13, dataList.size());
	    	}catch(SQLException e){
	    		
	    	}
	    }
	    
	    @Test
	    public void testGetTaxMatrixStatistics(){
	    	TaxMatrixTransactionStatiticsDTO tx= dbService.getTaxMatrixStatistics("");
	    	System.out.println("total lines"+tx.getTotal_matrix_lines());
	    	System.out.println("active lines"+tx.getActive_matrix_lines());
	    	System.out.println("driver 15="+tx.getDriver_15());
	    }
	    
	    @Test
	    public void testGetLocationMatrixStatistics(){
	    	LocationMatrixTransactionStatiticsDTO tx= dbService.getLocationMatrixStatistics("");
	    	System.out.println("active lines"+tx.getActive_location_matrix_lines());
	    	System.out.println("driver 1="+tx.getDriver_01());
	    }*/
	    
	   /* @Test
	    public void testGetLocationMatrixStatistics(){
	    	TaxMatrixTransactionStatiticsDTO tx= dbService.getTaxMatrixStatistics(" and matrix_state_code = 'AR'");
	    	
	    }*/
	    
/*	    @Test
	    public void testGetLocationMatrixStatistics(){
	    	LocationMatrixTransactionStatiticsDTO tx= dbService.getLocationMatrixStatistics("");
	    	System.out.println("count" + tx.getActive_location_matrix_lines());
	    }*/	    
	    
	    @Test
	    public void testTransactionsStatistics() {
	    	try{
	    	java.util.List<DatabaseTransactionStatisticsDTO> dataList = dbService.getTransactionsStatistics("");
	    	System.out.println("print data");
	    	int listSize = 0;
	    	if(dataList.size() > 0){
	    	   DatabaseTransactionStatisticsDTO lm = dataList.get(0);
	    	   System.out.println("title no == " + lm.getTitle());
	    	   System.out.println("average ==" + lm.getAvg());
	    	   System.out.println("count == " + lm.getCount());
	    	   listSize = dataList.size();
	    	   System.out.println("size="+listSize);
	    	}
	    	}catch(java.sql.SQLException e){
	    	   System.out.println("exception e = " + e.getMessage());	
	    	}
	    }
	    
/*	    @Test
	    public void testGetTaxMatrixStatistics(){
	    	TaxMatrixTransactionStatiticsDTO tx= dbService.getTaxMatrixStatistics("");
	    	System.out.println("total lines"+tx.getTotal_matrix_lines());
	    	System.out.println("active lines"+tx.getActive_matrix_lines());
	    }	*/    
    	    
}