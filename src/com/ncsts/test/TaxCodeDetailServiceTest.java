package com.ncsts.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;

@RunWith(TestClassRunner.class)
public class TaxCodeDetailServiceTest extends STSTestBase {
	
	TaxCodeDetailService taxCodeDetailService = 
		(TaxCodeDetailService) getAppContext().getBean("taxCodeDetailService");
	
	TaxCodeService taxCodeService = 
		(TaxCodeService) getAppContext().getBean("taxCodeService");
	
	
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    // FK AND CONSTRAINTS (A TEST RECORD ALREADY ADDED WITH ID 100)
/*    @Test
    public void deleteTaxCodeDetail(){
    	TaxCodeDetail taxCodeDetail = new TaxCodeDetail();
    	taxCodeDetail.setTaxcodeDetailId(100l);
    	taxCodeDetailService.remove(taxCodeDetail);
    }*/
    
    //FK and Constraints
    
    /*@Test
    public void getList(){
    	List<String> list = taxCodeDetailService.findTaxCodeTypeForState("AK");
    	logger.info("TaxCodeDetail List Size "+list.size());
    	for(String code:list){
    		logger.info("CODE "+code);
    	}
    	
    }*/
    
/*    @Test
    public void testFindTaxCodeTypeForState(){
    	List<String> list = taxCodeDetailService.findTaxCodeTypeForState("AK");
    	assertEquals(1, list.size());
    	assertEquals("E", list.get(0));
    }
    @Test
    public void testFindTaxCodeCode(){
    	List<TaxCodeDetail> list = taxCodeDetailService.findTaxCodeCode("AK", "E");
    	assertEquals(3, list.size());
    	assertEquals("ANCHORAGE",list.get(0).getTaxcodeCode());
    }
    @Test
    public void testFindByTaxCodeCode1(){
    	List<TaxCode> list = taxCodeDetailService.findTaxCodeListByStateCode("AK", "E");
    	assertEquals(3, list.size());
    	assertEquals("ANCHORAGE", list.get(0).getTaxCodePK().getTaxcodeCode());
    }
    @Test
    public void testFindTaxCodeDetailList(){
    	List<TaxCodeDetail> list = taxCodeDetailService.findTaxCodeDetailList("AK", "E", "ANCHORAGE");
    	assertEquals(1, list.size());
    	assertEquals(706, list.get(0).getTaxcodeDetailId().intValue());
    }
    @Test
    public void testFindByTaxCodeCode(){
    	List<TaxCode> list = taxCodeDetailService.findByTaxCodeCode("E");
    	assertEquals(93, list.size());
    	assertEquals("3RDPARTYSHIP", list.get(0).getTaxCodePK().getTaxcodeCode());
    }*/
    /*
    @Test
    public void testGetAllTaxabilityTypes(){
    	List<ListCodesDTO> list = taxCodeDetailService.getAllTaxabilityTypes();
    	assertEquals(3, list.size());
    	//assertEquals("3RDPARTYSHIP", list.get(0).getTaxCodePK().getTaxcodeCode());
    }
    */
    
    
    //public abstract List<TaxCodeDTO> findByTaxCodeCode(String taxCodeTypeCode);
    
    /*@Test
    public void getList(){
    	List<TaxCodeDetailDTO> list = taxCodeDetailService.getTaxcodeDetails("AK","","","",null);
    	logger.info("TaxCodeDetail List Size "+list.size());
    	for(TaxCodeDetailDTO code:list){
    		logger.info("CODE "+code.getDescription());
    	}
    	
    }*/
    
    @Test
    public void testCodeUpdate(){
    	TaxCode tax = taxCodeService.findById(new TaxCodePK("MAINTENANCE"));
    	tax.setErpTaxcode("Testing agian");
    	tax.setComments("Testing");
    	taxCodeService.update(tax);
    }
    
    @Test
    public void testCodeDetailUpdate(){
    	TaxCodeDetail tax = taxCodeDetailService.findById(859l);
    	taxCodeDetailService.update(tax);
    }
    
    @Test
    public void testCodeDetailSave(){
    	TaxCodeDetail tax = new TaxCodeDetail();
    	tax.setId(56478l);
    	tax.setTaxcodeTypeCode("E");
    	tax.setTaxcodeCode("MAINTENANCE");
    	taxCodeDetailService.save(tax);
    }
}
