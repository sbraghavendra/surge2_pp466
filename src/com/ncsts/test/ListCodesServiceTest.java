package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.services.ListCodesService;

@RunWith(TestClassRunner.class)
public class ListCodesServiceTest extends STSTestBase {
	
	ListCodesService listCodesService = 
		(ListCodesService) getAppContext().getBean("listCodesService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    

    @Test
    public void testGetAllListCodes(){
    	
    	logger.debug(" Start of Test case:");
    	List<ListCodes> listCodesList = listCodesService.getAllListCodes();
    	logger.debug(" Test results from testGetAllBatchMaintenanceData\n");
    	if(listCodesList != null){
    		logger.debug(" Size of listCodesList is :"+listCodesList.size()+"\n");
    		ListCodes a = (ListCodes)((Object)listCodesList.get(0));
    		logger.debug(" One Object values ::"+a.getCodeCode()+" ::"+a.getDescription());
          }
    	else
    		logger.debug("  listCodesList is :"+listCodesList+"\n");
    	assertEquals(23, listCodesList.size());
    }
   
    @Test
    public void testFindByPK(){
    	logger.debug(" Start of Test case for testFindByPK:");
    	ListCodesPK pk = new ListCodesPK();
    	pk.setCodeTypeCode("ERRORCODE");
    	pk.setCodeCode("PTEST");
    	ListCodes listCodesList = listCodesService.findByPK(pk);
    	logger.debug(" Out put values of ListCodes are ::"+listCodesList.toString());
    }

    @Test
    public void testUpdate(){

    	ListCodesDTO lc = new ListCodesDTO();
    	ListCodesPK pk = new ListCodesPK();
    	pk.setCodeTypeCode("ERRORCODE");
    	pk.setCodeCode("PTEST");
    	lc.setListCodesPK(pk);
    	lc.setDescription("no data found");
    	lc.setMessage("Test by MB");
    	logger.debug("before calling update of service Bean");
    	listCodesService.update(lc);
    	logger.debug("end of update in Junit test");
    }    

 
    @Test
    public void testAddListCodeRecord(){

    	ListCodesDTO lc = new ListCodesDTO();
    	ListCodesPK pk = new ListCodesPK();
    	pk.setCodeTypeCode("LCTypeCD");
    	pk.setCodeCode("LCTEST");
    	lc.setListCodesPK(pk);
    	lc.setDescription("some data found");
    	lc.setMessage("Test by MB");
    	logger.debug("before calling add of service Bean in Test Case");
    	listCodesService.addListCodeRecord(lc);
    	logger.debug("end of update in Junit test");
    } 
    
    @Test
    public void testDelete(){
    	ListCodesPK pk = new ListCodesPK();
    	pk.setCodeTypeCode("LCTypeCD");
    	pk.setCodeCode("LCTEST");
    	
    	listCodesService.delete(pk);
    	logger.info("Delete completed ");
    }    
    
    @Test
    public void testUpdateGet(){
    	List<ListCodesDTO> lc = listCodesService.getListCodesByCodeCode("TR");
    	logger.debug("Size ==" +lc.size());
    }
}
