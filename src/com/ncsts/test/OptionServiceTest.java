package com.ncsts.test;

import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.services.OptionService;

@RunWith(TestClassRunner.class)
public class OptionServiceTest extends STSTestBase {
	
	OptionService optionService = 
		(OptionService) getAppContext().getBean("optionService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
       
    @Test
    public void getOptionByPK(){ 
    	System.out.println(" get optionServiceTest");
        OptionCodePK codePK = new OptionCodePK();
        codePK.setOptionCode("DONOTIMPORT");
        codePK.setOptionTypeCode("SYSTEM");
        codePK.setUserCode("SYSTEM");
        Option option = optionService.findByPK(codePK);   
    	System.out.println(" option desc =  " + option.getDescription());        
    }
    
    @Test
    public void getSystemPreferences(){ 
    	System.out.println(" getSystemPreferences ");
    	List<Option> list = optionService.getSystemPreferences();  
        System.out.println("list size = " + list.size()); 
        Map<String,String> map = new HashMap<String,String>();
        for (Option opt : list) {
        	map.put(opt.getCodePK().getOptionCode(), opt.getValue());
            System.out.println("option code = " + opt.getCodePK().getOptionCode()+ " ; value = " + opt.getValue());	
        } 
        System.out.println(map.get("COMPANYNAME"));
        System.out.println(map.get("LOGO"));  
        System.out.println(map.get("SYSTEMDST"));        
        System.out.println(map.get("SYSTEMTIMEZONE"));  
        System.out.println(map.get("SYSTEMDST"));  
        System.out.println(map.get("DEFUSERDST"));          
        
    }      
    
    @Test
    public void getAdminPreferences(){ 
    	System.out.println(" getAdminPreferences ");
    	List<Option> list = optionService.getAdminPreferences();  
        System.out.println("list size = " + list.size());    	
        Map<String,String> map = new HashMap<String,String>();
        for (Option opt : list) {
        	map.put(opt.getCodePK().getOptionCode(), opt.getValue());
            System.out.println("option code = " + opt.getCodePK().getOptionCode()+ " ; value = " + opt.getValue());	
        }     
        System.out.println("exit test");        
    }  
    
    @Test
    public void getUserPreferences(){ 
    	System.out.println(" getUserPreferences ");
    	List<Option> list = optionService.getUserPreferences();  
        System.out.println("list size = " + list.size());    	
        Map<String,String> map = new HashMap<String,String>();
        for (Option opt : list) {
        	map.put(opt.getCodePK().getOptionCode(), opt.getValue());
            System.out.println("option code = " + opt.getCodePK().getOptionCode()+ " ; value = " + opt.getValue());	
        }    
        System.out.println("exit test");        
    }    
}

