package com.ncsts.test;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.services.UserEntityService;

@RunWith(TestClassRunner.class)
public class  UserEntityServiceTest extends STSTestBase {
    @SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(com.ncsts.test.UserEntityServiceTest.class);
	
	UserEntityService userEntityService = 
		(UserEntityService) getAppContext().getBean("userEntityService");
	
    /**
     * An entity that is persisted in the setup.
     */
    UserEntity testEntity;
    
    @Before
    public void initialize() throws Exception {
        super.setUp();
        
        // Create an entity to use in test
        testEntity = new UserEntity();
        testEntity.setRoleCode("testRole");
        testEntity.setUpdateTimestamp(new Date());
        testEntity.setUpdateUserId("1");
        testEntity.setUserEntityDTO(null);
        Random random = new Random();
        //testEntity.setUserEntityPK(new UserEntityPK(Long.toString(random.nextLong()),"1"));
        
        //userEntityService.persist(testEntity);
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }   	 
    
    //FK and Constraints (Throw constraint violation exception when role_code not in the tb_role)
/*    @Test
    public void testSave(){
    	UserEntity userEntity = new UserEntity();
    	UserEntityPK userEntityPK = new UserEntityPK();
    	userEntityPK.setEntityId(0l);
    	userEntityPK.setUserCode("ATHIEL");
    	userEntity.setRoleCode("ADMIN");
    	userEntity.setUserEntityPK(userEntityPK);
    	userEntityService.saveOrUpdate(userEntity);
        
    }*/
    
  //FK and Constraints  (Cascade Delete)
    @Test
    public void testDelete(){
    	UserEntity userEntity = new UserEntity();
    	UserEntityPK userEntityPK = new UserEntityPK();
    	userEntityPK.setUserCode("ATHIEL");
    	userEntityPK.setEntityId(0l);
    	userEntity.setUserEntityPK(userEntityPK);
    	userEntityService.delete(userEntity);
    }
/*    @Test
    public void testGetAll() {
        List<UserEntity> list = userEntityService.findAll();
        for (UserEntity userEntity : list) {
            System.out.println("role code = " + userEntity.getRoleCode());
            System.out.println("entity id = " + userEntity.getEntityId());  
            System.out.println("user code = " + userEntity.getUserCode());
        }
        System.out.println("size = " + list.size());     
    }*/
    
/*    @Test
    public void testPagingAndCount(){
        Long count = userEntityService.getTotalCount();
        assertTrue (count > 0);
    }*/
    
/*    @Test
    public void testGetById(){
        UserEntity recoveredEntity = userEntityService.getById(testEntity.getUserEntityPK());
        assertNotNull(recoveredEntity);
        assertTrue(recoveredEntity.equals(testEntity));
    }*/
    
} 





