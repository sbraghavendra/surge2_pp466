package com.ncsts.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;


import com.ncsts.services.TransactionDetailSaleService;

import com.seconddecimal.billing.exceptions.ProcessAbortedException;

@RunWith(TestClassRunner.class)
public class SaleTransactionServiceImplTest extends STSTestBase {
	TransactionDetailSaleService transactionDetailService = 
			(TransactionDetailSaleService) getAppContext().getBean("transactionDetailSaleService");
		
	@Before
	public void initialize() throws Exception {
		super.setUp();
	}

	@After
	public void cleanup() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testSuspend() {
		///
		
		assertTrue(transactionDetailService.suspendAndLog(270l));
		assertFalse(transactionDetailService.suspendAndLog(2700000000000000000l));

		
		
	}
	
	    
	
}
