package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.AllocationMatrix;
import com.ncsts.services.AllocationMatrixService;
import com.ncsts.view.bean.AllocationMatrixBackingBean;

@RunWith(TestClassRunner.class)
public class AllocationMatrixServiceTest extends STSTestBase {
	
	AllocationMatrixService allocationMatrixService = 
		(AllocationMatrixService) getAppContext().getBean("allocationMatrixService");
	
	AllocationMatrixBackingBean allocationMatrixBackingBean = new AllocationMatrixBackingBean();
	
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
/*    @Test
    public void testRetrieveData() {
    	List<AllocationMatrix> amList= allocationMatrixService.retrieveData("17215");
    	int listSize = 0;
    	if(amList.size() > 0){
    	AllocationMatrix am = amList.get(0);
    	System.out.println("driver 6 = " + am.getDriver06());
    	System.out.println("driver 7 = " + am.getDriver07());    	
    	System.out.println("driver 8 = " + am.getDriver08());       	
    	listSize = amList.size();
    	}
    	assertEquals(1, listSize);
    }
    
    @Test
    public void testFindByLocationMatrix(){    	 
		AllocationMatrixDTO allocationMatrixDTO = new AllocationMatrixDTO();
		allocationMatrixDTO.setDriver05("17215");
		List<AllocationMatrixDTO> allocationMatrixList = allocationMatrixService.getAllocationMatrixData(allocationMatrixDTO);
		if(allocationMatrixList.size() > 0){
	    	AllocationMatrixDTO lmd = allocationMatrixList.get(0);
	    	System.out.println("driver05 == " + lmd.getDriver05());
	    	System.out.println("driver06 ==" + lmd.getDriver06());
	    	System.out.println("driver07 == " + lmd.getDriver07());
	    	System.out.println("driver08 == " + lmd.getDriver08());
	    	System.out.println("Binary Weight == " + lmd.getBinaryWeight());
	    }
		assertEquals(1, allocationMatrixList.size());

    }*/    

/*    @Test
    public void testSave(){ 
    	System.out.println(" save start");
		AllocationMatrixDTO allocationMatrixDTO = new AllocationMatrixDTO();
		allocationMatrixDTO.setDriver01("Y");
		allocationMatrixDTO.setDriver02("TAX");	
		allocationMatrixDTO.setDriver03("1819");		
        Serializable obj = allocationMatrixService.saveAllocationMatrixDTO(allocationMatrixDTO);
    	System.out.println(" save end obj = " + obj.toString());        
    } */
    
/*    @Test
    public void testSave(){ 
    	System.out.println(" save new start");
		AllocationMatrix allocationMatrix = new AllocationMatrix();
		allocationMatrix.setDriver01("Y");
		allocationMatrix.setDriver02("TAX");	
		allocationMatrix.setDriver03("2008");
        int key = allocationMatrixService.persist (allocationMatrix);   
    	System.out.println(" end save = " + key);        
    } */   
    
    /*@Test
    public void testAllocationMatrixDetailSave(){
    	System.out.println(" save start testAllocationMatrixDetailSave");    	
        AllocationMatrixDetailDTO amd = new AllocationMatrixDetailDTO();
        AllocationMatrixDetailPK pk = new AllocationMatrixDetailPK();
        pk.setAllocationMatrixId("89");
        pk.setJurisdictionId("122904");
        amd.setAllocationMatrixDetailPK(pk);
        allocationMatrixService.save(amd);
    	System.out.println(" save end testAllocationMatrixDetailSave");         
    }   */  

   /* @Test
    public void testFindById(){     
    	AllocationMatrix allocationMatrix = allocationMatrixService.findById(new Long("15"));
    	System.out.println("driver01 = " + allocationMatrix.getDriver01());
    }*/
    
    @Test
    public void testUpdate(){  
    
    	AllocationMatrix allocationMatrix = allocationMatrixService.findById(571l);
    	allocationMatrix.setDriver16("test");
    	allocationMatrix.setDriver16("abcd");
    	allocationMatrixBackingBean.setSelectedMatrix(allocationMatrix);
    	//Replace of trigger
    	allocationMatrix.setBinaryWeight(allocationMatrixBackingBean.CalcBinaryWeight());
    	allocationMatrix.setSignificantDigits(allocationMatrixBackingBean.CalcSignificantDigits());
     	//End of trigger 
		logger.debug("binaryWeight : " + allocationMatrix.getBinaryWeight());
		logger.debug("significantDigits : " + allocationMatrix.getSignificantDigits());
		
    	allocationMatrixService.update(allocationMatrix);
    	System.out.println("driver01 = " + allocationMatrix.getDriver01());
    } 
    
    /*@Test
    public void testSave(){ 
    	System.out.println(" save new start");
		AllocationMatrix allocationMatrix = new AllocationMatrix();
		allocationMatrix.setDriver01("Y");
		allocationMatrix.setDriver02("TAX");	
		allocationMatrix.setDriver03("2008");
		allocationMatrixBackingBean.setSelectedMatrix(allocationMatrix);
    	//Replace of trigger
    	allocationMatrix.setBinaryWeight(allocationMatrixBackingBean.CalcBinaryWeight());
    	allocationMatrix.setSignificantDigits(allocationMatrixBackingBean.CalcSignificantDigits());
    	//End of trigger 
		logger.debug("binaryWeight : " + allocationMatrix.getBinaryWeight());
		logger.debug("significantDigits : " + allocationMatrix.getSignificantDigits());
		
        allocationMatrixService.save(allocationMatrix);   
    	System.out.println(" end save = ");
    }*/
    
/*    @Test
    public void testFindAllocationMatrixDetailById(){     
    	AllocationMatrixDetail allocationMatrixDetail = allocationMatrixService.findAllocationMatrixDetailById(new Long("15"));
    	System.out.println("alloc percent = " + allocationMatrixDetail.getAllocationPercent());
    }  */ 
    
/*    @Test
    public void testFindAllocationMatrixDetails(){ 
    	//AllocationMatrixDetailPK allocationMatrixDetailPK = new AllocationMatrixDetailPK("15");    	
    	List<AllocationMatrixDetailDTO> list = allocationMatrixService.findByAllocationMatrixDetail("1");
    	System.out.println("list size = " + list.size());    	
    	AllocationMatrixDetailDTO amd = (AllocationMatrixDetailDTO) list.get(0);
    	System.out.println("alloc percent = " + amd.getAllocationPercent());
    }*/    
    
/*    @Test
    public void remove(){     
    	System.out.println(" start remove ");
    	allocationMatrixService.remove(new Long("101"));
    	System.out.println(" end remove ");    	
    }    
    
    @Test
    public void testAllocationMatrixDetailDelete(){
    	System.out.println("  start testAllocationMatrixDetailDelete");    	
        AllocationMatrixDetailPK pk = new AllocationMatrixDetailPK();
        pk.setAllocationMatrixId("89");
        pk.setJurisdictionId("122904");
        allocationMatrixService.removeAllocationMatrixDetail(pk);
    	System.out.println(" save end testAllocationMatrixDetailSave");         
    }*/     
    
/*    @Test
    public void getAllIds(){ 
    	System.out.println(" get all Ids");
        List<Long> list = allocationMatrixService.findAllAllocationMatrixIds();   
    	System.out.println(" end all Ids: list size =  " + list.size());        
    }   */ 
/*    
    @Test
    public void getAllocationMatrixList(){ 
    	System.out.println(" get allocationMatrixList");
    	AllocationMatrix dto = new AllocationMatrix();
    	dto.setAllocationMatrixId(new Long("5"));
        List<AllocationMatrix> list = allocationMatrixService.findByExample(dto, 0);   
    	System.out.println(" list size =  " + list.size());        
    }*/
    /*
   @Test
    public void getAllMatrixList(){
    	List<AllocationMatrix> list = allocationMatrixService.getAllocationMatrixData();
    	System.out.println("list size "+ list.size() );
    }
    */
}
