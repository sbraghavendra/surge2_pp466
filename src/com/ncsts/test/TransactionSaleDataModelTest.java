package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.TransactionDetailSaleDAO;
import com.ncsts.services.TransactionDetailSaleService;
import org.apache.commons.logging.*;
import com.seconddecimal.billing.domain.SaleTransaction;

@RunWith(TestClassRunner.class)
public class TransactionSaleDataModelTest extends STSTestBase  {
	
	    private static final transient Log log = LogFactory.getLog(com.ncsts.test.TransactionSaleDataModelTest.class);

	    /**default empty constructor*/
	    public TransactionSaleDataModelTest() {
	    }
	    
	    private TransactionDetailSaleService transactionDetailSaleService = 
	    	(TransactionDetailSaleService) getAppContext().getBean("transactionDetailSaleService");
	    
	    
	    @Before
	    public void initialize() throws Exception {
	        super.setUp();
	    }

	    @After
	    public void cleanup() throws Exception {
	        super.tearDown();
	    }
	 

		@Test
	    public void testLoadOpen(){
			SaleTransaction detail = new SaleTransaction();
			
	//		detail.setProcessBatchNo(1112L);
	//		detail.setSaletransId(270L);
			detail.setTransactionType("Use");

	        Long count = transactionDetailSaleService.count(detail);
	        log.debug("count : " + count);
	    	//transactionDetailDAO.getAllRecords(detail, effectiveDate, expirationDate, includeUnprocessed, includeProcessed, includeSuspended, glExtractFlagIsNull, advancedFilter, orderBy, 0, 500);
	        List<SaleTransaction> list = transactionDetailSaleService.getAllRecords(detail, null, 0, 500);
	        
	        int x = 0;
	    }
}
