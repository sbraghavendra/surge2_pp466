package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Role;
import com.ncsts.services.RoleService;

@RunWith(TestClassRunner.class)
public class  RoleServiceTest extends STSTestBase {
	
	RoleService roleService = (RoleService) getAppContext().getBean("roleService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
/*    @Test
    public void testFindById() {
        Role role = roleService.findById("ADMIN");
        System.out.println("role name  = " + role.getRoleName());     
    }    	          

    @Test
    public void testGetAllRoles() {
        List<Role> list = roleService.findAllRoles();
        for (Role role : list) {
        	System.out.println("code = " + role.getRoleCode());
        	System.out.println("name = " + role.getRoleName());  
        	System.out.println("active flag = " + role.getActiveFlag());
        }
        System.out.println("size = " + list.size());     

    }     
    
    @Test
    public void testAddRole() {
    	Role role = new Role();
    	role.setRoleCode("TEST1");
        // String id = roleService.persist(role);
        System.out.println("role id  = " + role.getRoleCode());     
    }
    
    @Test
    public void testAddUpdate() {
        Role role = roleService.findById("TEST1");
        role.setRoleName("testRole1");
        roleService.saveOrUpdate(role);
        System.out.println("role name  = " + role.getRoleName());     
    } */
    
    // FK and Constraints (test cascade delete)
    @Test
    public void testDelete() {
        roleService.remove("TEST4567");
        System.out.println("role deleted ");     
    }    
    
} 




