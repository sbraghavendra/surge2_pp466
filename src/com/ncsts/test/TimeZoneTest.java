package com.ncsts.test;

import java.util.Date;
import java.util.TimeZone;

public class TimeZoneTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("-Duser.timezone = " + System.getProperty("user.timezone"));
		System.out.println("new Date() = " + new Date());
		System.out.println("TimeZone.getDefault() = " + TimeZone.getDefault());
	}
}
