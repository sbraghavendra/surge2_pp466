package com.ncsts.test;

public class NullTest {
	public static void main(String[] args) {
		boolean test = "*NOW".equalsIgnoreCase(null);
		System.out.println(test);
		
		Boolean b = null;
		System.out.println(Boolean.TRUE.equals(b) ? "1" : 0);
	}
}
