package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dto.BatchPreferenceDTO;
import com.ncsts.dto.ListCodesDTO;
import com.ncsts.services.PreferenceService;

@RunWith(TestClassRunner.class)
public class PreferenceServiceTest extends STSTestBase {
	
	PreferenceService preferenceService = 
		(PreferenceService) getAppContext().getBean("preferenceService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }

//    @Test 
//    public void testGetCompany(){
//    	CompanyDTO cd = preferenceService.getCompany();
//    	String companyName = cd.getName();
//    	System.out.println("Company name == " + companyName + "timezone== " + cd.getStsTimeZone());
//    	assertEquals("Aetna", companyName);
//    }
//    @Test 
//    public void testGetBatchPreference(){
//    	BatchPreferenceDTO bp = preferenceService.getBatchPreference();
//    	String batchSysSceTime = bp.getSystemDefTime();
//    	System.out.println("System Sed Time == " + batchSysSceTime + "user flag == " + bp.isUserCompletionPopUp());
//    	assertEquals("*NOW", batchSysSceTime);
//    }
/*    @Test 
    public void testGetMatrixPreference(){
    	MatrixDTO bp = preferenceService.getMatrix();
    	String batchSysSceTime = bp.getUserThreshHold();
    	//System.out.println("System Sed Time == " + batchSysSceTime + "user flag == " + bp.isUserCompletionPopUp());
    	assertEquals("10000", batchSysSceTime);
    }*/
    
    @Test
    public void testGetTimeZoneList(){
    	List<ListCodesDTO> list = preferenceService.getTimeZoneList();
    	assertEquals(6, list.size());
    }
}
