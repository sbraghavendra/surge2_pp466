package com.ncsts.test;

import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.ImportDefinition;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.view.util.ImportMapUploadParser;

@RunWith(TestClassRunner.class)
public class ImportMapUploadParserTest extends TestCase {
	
	ImportMapUploadParser parser;
	ImportDefinition def;
	ImportMapProcessPreferenceDTO pref;
	
	@Before
	public void initialize() throws Exception {
		def = new ImportDefinition();
		pref = new ImportMapProcessPreferenceDTO();
		parser = new ImportMapUploadParser(def, pref);
	}

    /**
     * @throws Exception
     */
    @After
    public void cleanup() throws Exception {
    }
    
    @Test
    public void testReplace() {
    	System.out.println(parser.replacementCodes("~a"));
    	System.out.println(parser.replacementCodes("~b"));
    	System.out.println(parser.replacementCodes("~c"));
    	System.out.println(parser.replacementCodes("~d"));
    	System.out.println(parser.replacementCodes("~s"));
    }
    
    @Test
      public void testSplit() {
  	 def.setFlatDelChar("|");
  	 def.setFlatDelTextQual("'");
  	 List<String> list = parser.split("abc|'d|'|efg");
  	 assertEquals(3, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d|", list.get(1));
  	 assertEquals("efg", list.get(2));
  	 list = parser.split("abc|d|efg");
  	 assertEquals(3, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d", list.get(1));
  	 assertEquals("efg", list.get(2));
  	 list = parser.split("abc|d||efg|");
  	 assertEquals(5, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d", list.get(1));
  	 assertEquals(null, list.get(2));
  	 assertEquals("efg", list.get(3));
  	 assertEquals(null, list.get(4));

  	 def.setFlatDelChar("~t");
  	 def.setFlatDelTextQual(null);
  	 list = parser.split("abc\td\tefg");
  	 assertEquals(3, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d", list.get(1));
  	 assertEquals("efg", list.get(2));

  	 def.setFlatDelChar("~r");
  	 def.setFlatDelTextQual(null);
  	 list = parser.split("abc\rd\refg");
  	 assertEquals(3, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d", list.get(1));
  	 assertEquals("efg", list.get(2));

  	 def.setFlatDelChar("~v");
  	 def.setFlatDelTextQual(null);
  	 list = parser.split("abc\u000Bd\u000Befg");
  	 assertEquals(3, list.size());
  	 assertEquals("abc", list.get(0));
  	 assertEquals("d", list.get(1));
  	 assertEquals("efg", list.get(2));
}
    
}
