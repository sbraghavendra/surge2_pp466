package com.ncsts.test;

import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.dto.DataDefinitionColumnDTO;
import com.ncsts.dto.DataDefinitionTableDTO;
import com.ncsts.services.DataDefinitionService;

@RunWith(TestClassRunner.class)
public class DataDefinitionServiceTest extends STSTestBase {
	
	DataDefinitionService dataDefinitionService = 
		(DataDefinitionService) getAppContext().getBean("dataDefinitionService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    // For Constraints and PK
    @Test
    public void deleteFromDDC(){ 
    	System.out.println(" START DELETE ");
        //dataDefinitionService.delete("TB_TEST");
    	dataDefinitionService.delete("TB_TEST","TEST");
    	System.out.println(" END ");        
    }
    
    // For Constraints and PK
/*    @Test
    public void addDDC(){ 
    	System.out.println(" START ADD ");
    	DataDefinitionColumnDTO dataDefinitionColumnDTO = new DataDefinitionColumnDTO();
    	DataDefinitionColumnPK dataDefinitionColumnPK = new DataDefinitionColumnPK();
    	dataDefinitionColumnPK.setTableName("NEW_TB_TEST");
    	dataDefinitionColumnPK.setColumnName("new_column name");
    	dataDefinitionColumnDTO.setDataDefinitionColumnPK(dataDefinitionColumnPK);
        dataDefinitionService.addNewDataDefinitionColumn(dataDefinitionColumnDTO);
    	System.out.println(" END ");        
    }*/
    
       
/*    @Test
    public void getAllDataDefinitionTable(){ 
    	System.out.println(" getAllDataDefinitionTable ");
        List<DataDefinitionTableDTO> list = dataDefinitionService.getAllDataDefinitionTable();   
    	System.out.println(" list size =  " + list.size());        
    }
    
    //getAllDataDefinitionColumnByTable
    @Test
    public void getAllDataDefinitionColumnByTable(){ 
    	System.out.println(" getAllDataDefinitionColumnByTable ");
        List<DataDefinitionColumn> list = dataDefinitionService.getAllDataDefinitionColumnByTable("TB_TRANSACTION_DETAIL");   
    	System.out.println(" list size =  " + list.size());        
    }    */    
    
    
}


