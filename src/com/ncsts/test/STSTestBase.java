package com.ncsts.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.jpa.AbstractJpaTests;

public class STSTestBase extends AbstractJpaTests {
	
	private static final String[] configFiles = { "com/ncsts/context/applicationContext.xml" }; 
	
	private static ApplicationContext ctx = null;
	
	public STSTestBase(){
        super();
        this.setAutowireMode(AUTOWIRE_BY_NAME);
        this.setDependencyCheck(false);    
    }
	
	@Override 	
	protected String[] getConfigLocations() { 		
		return configFiles; 	
	}
	
	protected static ApplicationContext getAppContext() {
		if (ctx == null) {
			try {
				ctx = new ClassPathXmlApplicationContext(configFiles);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		
		return ctx;
	}
}
