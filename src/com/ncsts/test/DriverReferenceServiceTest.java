package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.services.DriverReferenceService;

@RunWith(TestClassRunner.class)
public class DriverReferenceServiceTest extends STSTestBase {
	
	//TransactionDetailService transactionDetailService = 
	//	(TransactionDetailService) getAppContext().getBean("transactionDetailService");
	
	DriverReferenceService driverReferenceService = 
		(DriverReferenceService) getAppContext().getBean("driverReferenceService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    /*
    @Test
    public void testfindByDrvCodeMatrixCol1() {
    	List<DriverReference> driverList= driverReferenceService.findByDrvCodeMatrixCol("T", "DRIVER_01", 100);
    	DriverReference d = driverList.get(0);
    	System.out.println(d.getDriverDesc());
    	System.out.println(d.getDriverValue());
    	System.out.println(d.getTransDetailColName());
    	int listSize = driverList.size();
    	assertEquals(100, listSize);
    }
  
    @Test
    public void testfindByDrvCodeMatrixCol() {
    	List<DriverReference> driverList= driverReferenceService.findByDrvCodeMatrixCol("L", "DRIVER_04", 200);
    	DriverReference d = driverList.get(0);
    	System.out.println(d.getDriverDesc());
    	System.out.println(d.getDriverValue());
    	System.out.println(d.getTransDetailColName());
    }
    */
    
    @Test
    public void testGetDriverNamebyDrvCodeMatrixCol() {
    	List<String> list= driverReferenceService.getDriverNamesByDrvCodeMatrixCol("T", "DRIVER_02");
    	String name = list.get(0);
    	System.out.println("name = " + name);
    	int listSize = list.size();
    	assertEquals(1, listSize);
    }    
}

