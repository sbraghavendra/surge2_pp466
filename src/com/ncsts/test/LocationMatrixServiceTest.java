package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.services.LocationMatrixService;
import com.ncsts.view.bean.LocationMatrixBackingBean;

@RunWith(TestClassRunner.class)
public class LocationMatrixServiceTest extends STSTestBase {
	
	//TransactionDetailService transactionDetailService = 
	//	(TransactionDetailService) getAppContext().getBean("transactionDetailService");
	
	LocationMatrixService locationMatrixService = 
		(LocationMatrixService) getAppContext().getBean("locationMatrixService");
	
	LocationMatrixBackingBean locationMatrixBackingBean = new LocationMatrixBackingBean();
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    /*
    @Test
    public void testRetrieveData() {
    	System.out.println("yes step 1");
    	List<LocationMatrix> lmList= locationMatrixService.retrieveData("2054");
    	System.out.println("yes step 2");
    	int listSize = 0;
    	if(lmList.size() > 0){
    	LocationMatrix lm = lmList.get(0);
    	System.out.println("plant no == " + lm.getDriver01());
    	System.out.println("cost center number ==" + lm.getDriver02());
    	System.out.println("vendor address zip == " + lm.getDriver03());
    	System.out.println("Vendor Number == " + lm.getDriver04());
    	System.out.println("Binary Weight == " + lm.getBinaryWt());
    	System.out.println("Default Binary Weight == " + lm.getDefltBinaryWt());
    	listSize = lmList.size();
    	}
    	assertEquals(1, listSize);
    }
    
    
    @Test
    public void testFindByLocationMatrix(){
    	 
		//TaxMatrix taxMatrix=new TaxMatrix();
		LocationMatrix lacationMatrix = new LocationMatrix();
		lacationMatrix.setDriver01("2054");
		List<LocationMatrix> locationMatrixList = locationMatrixService.findByExample(lacationMatrix, 0);
		if(locationMatrixList.size() > 0){
	    	LocationMatrix lmd = locationMatrixList.get(0);
	    	System.out.println("plant no == " + lmd.getDriver01());
	    	System.out.println("cost center number ==" + lmd.getDriver02());
	    	System.out.println("vendor address zip == " + lmd.getDriver03());
	    	System.out.println("Vendor Number == " + lmd.getDriver04());
	    	System.out.println("Binary Weight == " + lmd.getBinaryWt());
	    	System.out.println("Default Binary Weight == " + lmd.getDefltBinaryWt());
	    	}
		assertEquals(1, locationMatrixList.size());

    }
    
    @Test
    public void testFindByDriver01Zip(){
    	List<LocationMatrix> locationMatrixList = locationMatrixService.findByDriver01Zip("TXLIB%", "78642");		
		assertEquals(1, locationMatrixList.size());
    }
    
    
    @Test
    public void testFindByLocationMatrixByAllDriver(){
    	 
		//TaxMatrix taxMatrix=new TaxMatrix();
		LocationMatrix locationMatrix = new LocationMatrix();
		//locationMatrix.setJurisdictionId(new Long(124366));
		//locationMatrix.setDriver01("*NULL");
		//locationMatrix.setDriver02("0002046012");
		//locationMatrix.setDriver03("*ALL");
		//locationMatrix.setDriver04("*ALL");
		locationMatrix.setLocationMatrixId(new Long(246));
		//Jurisdiction jd = new Jurisdiction();
		//jd.setZip("78642");
		//jd.setJurisdictionId(new Long(124366));
		//locationMatrix.setJurisdiction(jd);
		List<LocationMatrix> locationMatrixList = locationMatrixService.findByExample(locationMatrix, 0);
		if(locationMatrixList.size() > 0){
	    	LocationMatrix lmd = locationMatrixList.get(0);
	    	Jurisdiction jd1 = lmd.getJurisdiction();
	    	System.out.println("plant no == " + lmd.getDriver01());
	    	System.out.println("cost center number ==" + lmd.getDriver02());
	    	System.out.println("vendor address zip == " + lmd.getDriver03());
	    	System.out.println("county == " + lmd.getJurisdiction().getCounty());
	    	System.out.println("geocode == " + jd1.getGeocode());
	    	//System.out.println("Vendor Number == " + lmd.getDriver04());
	    	//System.out.println("Binary Weight == " + lmd.getBinaryWt());
	    	System.out.println("Default Binary Weight == " + lmd.getDefltBinaryWt());
	    	System.out.println("Comment == " + lmd.getComments());
	    	System.out.println("Plant No Description == " + lmd.getDriver01Desc());
	    	}
		assertEquals(1, locationMatrixList.size());

    }*/
    
    @Test
    public void testSave(){
    	LocationMatrix locationMatrix = new LocationMatrix();
    	locationMatrix.setLocationMatrixId(32545l);
		locationMatrix.setDriver01("2046");
		locationMatrix.setDriver05("204k%l6");
		locationMatrix.setComments("-----added by junit----");
		locationMatrixBackingBean.setSelectedMatrix(locationMatrix);
		
		//Start of trigger 
		Long binaryWeight = locationMatrixBackingBean.CalcBinaryWeight();
		String significantDigits = locationMatrixBackingBean.CalcSignificantDigits();
		// Default Flag
		if(locationMatrix.getDefaultFlag() == null) {
			locationMatrix.setDefaultFlag("0");
		}
		if(locationMatrix.getDefaultFlag() != "1") {
			locationMatrix.setBinaryWt(binaryWeight);
			locationMatrix.setDefltBinaryWt(0L);
			
			locationMatrix.setSignificantDigits(significantDigits);
			locationMatrix.setDefaultSignificantDigits(null);
		} else {
			locationMatrix.setBinaryWt(0L);
			locationMatrix.setDefltBinaryWt(binaryWeight);
			
			locationMatrix.setSignificantDigits(null);
			locationMatrix.setDefaultSignificantDigits(significantDigits);
		}
		//End of trigger 
		
		logger.debug("binaryWeight : " + locationMatrix.getBinaryWt());
		logger.debug("significantDigits : " + locationMatrix.getSignificantDigits());
		
		locationMatrixService.save(locationMatrix);
    }
    
   /* @Test
    public void testUpdate(){
    	LocationMatrix locationMatrix = new LocationMatrix();
    	locationMatrix.setLocationMatrixId(new Long(471));
		locationMatrix.setDriver01("2046");
		locationMatrix.setComments("updated");
		locationMatrixBackingBean.setSelectedMatrix(locationMatrix);
		//Start of trigger 
		Long binaryWeight = locationMatrixBackingBean.CalcBinaryWeight();
		String significantDigits = locationMatrixBackingBean.CalcSignificantDigits();
		// Default Flag
		if(locationMatrix.getDefaultFlag() == null) {
			locationMatrix.setDefaultFlag("0");
		}
		if(locationMatrix.getDefaultFlag() != "1") {
			locationMatrix.setBinaryWt(binaryWeight);
			locationMatrix.setDefltBinaryWt(0L);
			
			locationMatrix.setSignificantDigits(significantDigits);
			locationMatrix.setDefaultSignificantDigits(null);
		} else {
			locationMatrix.setBinaryWt(0L);
			locationMatrix.setDefltBinaryWt(binaryWeight);
			
			locationMatrix.setSignificantDigits(null);
			locationMatrix.setDefaultSignificantDigits(significantDigits);
		}
		//End of trigger 
		
		logger.debug("binaryWeight : " + locationMatrix.getBinaryWt());
		logger.debug("significantDigits : " + locationMatrix.getSignificantDigits());
		
		locationMatrixService.update(locationMatrix);
    }*/
    
    /*@Test
    public void testGetAllLocationMatrix(){
    	List<LocationMatrix> locationMatrixList = locationMatrixService.findAll();
    	assertEquals(337, locationMatrixList.size());
    }
    
    @Test
    public void testRemove(){
    	//LocationMatrix locationMatrix = new LocationMatrix();
    	//locationMatrix.setLocationMatrixId(new Long(445));
    	LocationMatrix lm = locationMatrixService.findById(new Long(445));
    	//System.out.println(lm.getComments());
    	locationMatrixService.remove(lm);
    }*/
   
}
