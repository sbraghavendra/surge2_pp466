package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.TaxMatrixDAO;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.services.TaxMatrixService;
import com.ncsts.view.bean.TaxMatrixViewBean;

@RunWith(TestClassRunner.class)
public class TaxMatrixServiceTest extends STSTestBase {
	
	TaxMatrixDAO taxMatrixDAO = 
		(TaxMatrixDAO) getAppContext().getBean("taxMatrixDAO");
	
	TaxMatrixService taxMatrixService =
		(TaxMatrixService) getAppContext().getBean("taxMatrixService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }
    

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    
/*    @Test
    public void testSaveMatrix(){
    	TaxMatrix taxMatrix = new TaxMatrix();
    	//taxMatrix.setTaxMatrixId(10800l);
    	taxMatrix.setMatrixStateCode("TX55");
    	TaxCodeDetail elseTaxcodeDetail = new TaxCodeDetail(); 
    	elseTaxcodeDetail.setTaxcodeDetailId(833l);
    	
    	taxMatrix.setElseTaxcodeDetail(elseTaxcodeDetail);
    	taxMatrixService.save(taxMatrix);
    
    }
    */
    @Test

/*    public void testFindByExample2(){
            TaxMatrix taxMatrix=new TaxMatrix();
            //taxMatrix.setDriver03("1676");
            taxMatrix.setDriver03("6000");            
            List<TaxMatrix> taxMatrixList = taxMatrixDAO.getAllRecords(taxMatrix, "", "", "", "", "", "", "", "", 0l, null , 0, 500);
            System.out.println("taxMatrixList Size ->" taxMatrixList.size());
    }*/
    
    public void testFindByExample3(){
        TaxMatrix taxMatrix=new TaxMatrix();
        //taxMatrix.setDriver03("1676"); //postgres
        taxMatrix.setDriver03("6000");  //oracle          
        Long count= taxMatrixDAO.count(taxMatrix, "", "", "", "", "", "", "", "", "");
        System.out.println("taxMatrixList Size ->"  + count.toString());
    }     
    
/*    public void testFindByExample3(){
        System.out.println("taxMatrix save start");    	
        TaxMatrix taxMatrix=new TaxMatrix();   
        taxMatrix.setId(new Long(28531));
        taxMatrixDAO.save(taxMatrix);
        System.out.println("taxMatrix save end");
    }  */   
    
    
    
/*	    @Test
	    public void testFindAll() {
	    	TaxMatrix listDriver02=taxMatrixDAO.findById(22L);
	    	
	    	
	    	System.out.println("id ="
	    			+listDriver02.getDriver04()+"\n driver 04"+listDriver02.getDriver04Desc());
	    }*/
    
/*	    @Test
	    public void testFindMatrixByExample(){
	    	//List<TaxMatrix> taxMatrixList = taxMatrixDAO.findMatrixByExample("vendor_nbr", "Driver04");
	    	List<TaxMatrix> taxMatrixList = taxMatrixService.getTaxMatrix("0008116001");
	    	Iterator itr=taxMatrixList.iterator();
	    	while(itr.hasNext())
	    	{
	    		Object[] str=(Object[])itr.next();
	    		System.out.println(str[0]+"<-->"+str[1]);
	    		
	    	}
	    	System.out.println(taxMatrixList.size());
	    }
    
    
    @Test
    public void testFindByExample1(){
    	 
		TaxMatrix taxMatrix=new TaxMatrix();
		taxMatrix.setDriver04("0008116001");
		List<TaxMatrix> taxMatrixList = taxMatrixService.findByExample(taxMatrix, 100);
		assertEquals(29, taxMatrixList.size());

    }
    

    @Test
    public void testFindByExample2(){
    	 
		TaxMatrix taxMatrix=new TaxMatrix();
		taxMatrix.setDriver04("0008116001");
		taxMatrix.setDriver02("1414");
		List<TaxMatrix> taxMatrixList = taxMatrixService.findByExample(taxMatrix, 100);
		System.out.println("taxMatrixList Size ->"+taxMatrixList.size());
		assertEquals(19, taxMatrixList.size());
		
}
    
   */
    /*    
    @Test
    public void testSaveTaxMatrix(){
    	TaxMatrixViewBean taxMatrixViewBean = new TaxMatrixViewBean();
    	TaxMatrix tax = new TaxMatrix();
    	tax.setDriver01("test");
    	tax.setDriver02("norwood");
    	tax.setComments("jan28_comments23");
    	//tax.setTaxMatrixId(4321L);
    	tax.setDriver04("500%test");
    	//Start of trigger
    	taxMatrixViewBean.setSelectedMatrix(tax);
    	Long binaryWeight = taxMatrixViewBean.CalcBinaryWeight();
		String significantDigits = taxMatrixViewBean.CalcSignificantDigits();
		
		// Matrix State Code
		if (tax.getMatrixStateCode() == null) {
			tax.setMatrixStateCode("*ALL");
		}
		if (tax.getMatrixStateCode() != "*ALL") {
			Double mult = Math.pow(2,30);
			binaryWeight = binaryWeight + mult.longValue();
		}
		// Global Flag
		if (tax.getDriverGlobalFlag() == null) {
			tax.setDriverGlobalFlag("0");
		}
		if ("1".equalsIgnoreCase(tax.getDriverGlobalFlag())) {
			Double mult = Math.pow(2,31);
			binaryWeight = binaryWeight + + mult.longValue();
		}
		// Default Flag
		if(tax.getDefaultFlag() == null) {
			tax.setDefaultFlag("0");
		}
		if(tax.getDefaultFlag() != "1") {
			tax.setBinaryWeight(binaryWeight);
			tax.setDefaultBinaryWeight(0L);
			
			tax.setSignificantDigits(significantDigits);
			tax.setDefaultSignificantDigits(null);
		} else {
			tax.setBinaryWeight(0L);
			tax.setDefaultBinaryWeight(binaryWeight);
			
			tax.setSignificantDigits(null);
			tax.setDefaultSignificantDigits(significantDigits);
		}
		//End of trigger 
		logger.debug("binaryWeight : " + tax.getBinaryWeight());
		logger.debug("significantDigits : " + tax.getSignificantDigits());
		
		taxMatrixService.save(tax);
    	//System.out.println(taxMatrixList.size());
    }
    
    @Test
    public void testUpdate(){
    	TaxMatrixViewBean taxMatrixViewBean = new TaxMatrixViewBean();
    	TaxMatrix taxDTO = taxMatrixService.findById(28511l);
    	taxDTO.setTaxMatrixId(28511l);
		taxDTO.setDriver02("020111");
		taxDTO.setRelationSign("!");
		taxDTO.setComments("jan23_comments28");
		taxMatrixViewBean.setSelectedMatrix(taxDTO);
		//Start of trigger
    	Long binaryWeight = taxMatrixViewBean.CalcBinaryWeight();
		String significantDigits = taxMatrixViewBean.CalcSignificantDigits();
		
		// Matrix State Code
		if (taxDTO.getMatrixStateCode() == null) {
			taxDTO.setMatrixStateCode("*ALL");
		}
		if (taxDTO.getMatrixStateCode() != "*ALL") {
			Double mult = Math.pow(2,30);
			binaryWeight = binaryWeight + mult.longValue();
		}
		// Global Flag
		if (taxDTO.getDriverGlobalFlag() == null) {
			taxDTO.setDriverGlobalFlag("0");
		}
		if ("1".equalsIgnoreCase(taxDTO.getDriverGlobalFlag())) {
			Double mult = Math.pow(2,31);
			binaryWeight = binaryWeight + + mult.longValue();
		}
		// Default Flag
		if(taxDTO.getDefaultFlag() == null) {
			taxDTO.setDefaultFlag("0");
		}
		if(taxDTO.getDefaultFlag() != "1") {
			taxDTO.setBinaryWeight(binaryWeight);
			taxDTO.setDefaultBinaryWeight(0L);
			
			taxDTO.setSignificantDigits(significantDigits);
			taxDTO.setDefaultSignificantDigits(null);
		} else {
			taxDTO.setBinaryWeight(0L);
			taxDTO.setDefaultBinaryWeight(binaryWeight);
			
			taxDTO.setSignificantDigits(null);
			taxDTO.setDefaultSignificantDigits(significantDigits);
		}
		//End of trigger 
		logger.debug("binaryWeight : " + taxDTO.getBinaryWeight());
		logger.debug("significantDigits : " + taxDTO.getSignificantDigits());
		taxMatrixService.update(taxDTO);
		System.out.println("update done");
		TaxMatrix taxMatrixA=new TaxMatrix();
		taxMatrixA.setTaxMatrixId(28513L);
		assertEquals("jan23_comments28", taxMatrixService.findByExample(taxMatrixA, 100));
    }
    
  @Test
    public void testRemove(){
    	TaxMatrix taxDTO = new TaxMatrix();
    	taxDTO.setTaxMatrixId(22821L);
    	taxMatrixService.remove(taxDTO);
    }*/

}