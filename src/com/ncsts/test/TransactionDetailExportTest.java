package com.ncsts.test;

import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.Column;

import com.ncsts.domain.TransactionDetail;

public class TransactionDetailExportTest {
	public static void main(String[] args) {
		System.out.println(new Date());
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@10.1.106.11:1521:Cbimp"; //"jdbc:oracle:thin:@localhost:1521:xe";
		String user = "stscorp";
		String password = "STSCORP";
		String sql = "SELECT * FROM TB_TRANSACTION_DETAIL where rownum <= 300000";
		
		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, user, password);
			Statement stmt = conn.createStatement();
			stmt.setFetchSize(1000);
			
			String filename = System.getProperty("java.io.tmpdir") + "/output.zip";
			System.out.println(filename);
			
			ZipOutputStream os = new ZipOutputStream(new FileOutputStream(filename));
			ZipEntry entry = new ZipEntry("transactions.csv");
			os.putNextEntry(entry);
			
			List<String> columns = getColumnNames();
			
			StringBuffer sb = new StringBuffer();
			String comma = "";
			for(String c : columns) {
				sb.append(comma).append(c);
				if(comma.length() == 0) {
					comma = ",";
				}
			}
			sb.append("\n");
			os.write(sb.toString().getBytes());
			
			ResultSet rs = stmt.executeQuery(sql);
			Map<String, Integer> typeMap = getColumnTypeMap(rs.getMetaData());
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			while(rs.next()) {
				sb = new StringBuffer();
				comma = "";
				for(String c : columns) {
					if(!typeMap.containsKey(c)) {
						continue;
					}
					sb.append(comma);
					int t = typeMap.get(c).intValue(); 
					switch(t) {
					case Types.FLOAT:
						try {
							sb.append(rs.getFloat(c));
						}
						catch(Exception e) {
						}
						break;
					case Types.DOUBLE:
						try {
							sb.append(rs.getDouble(c));
						}
						catch(Exception e) {
						}
						break;
					case Types.DATE:
					case Types.TIMESTAMP:
						try {
							sb.append(df.format(rs.getDate(c)));
						}
						catch(Exception e) {
						}
						break;
					case Types.NUMERIC:
						try {
							sb.append(rs.getLong(c));
						}
						catch(Exception e) {
						}
						break;
					default:
						String s = rs.getString(c);
						if(s != null && s.length() > 0) {
							sb.append("\"").append(rs.getString(c)).append("\"");
						}
					}
					if(comma.length() == 0) {
						comma = ",";
					}
				}
				sb.append("\n");
				os.write(sb.toString().getBytes());
			}
			
			os.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(new Date());
	}
	
	private static List<String> getColumnNames() {
		List<String> columns = new ArrayList<String>();
		Class<?> clazz = TransactionDetail.class;
		Field [] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				columns.add(col.name());
			}
		}
		
		return columns;
	}
	
	private static Map<String, Integer> getColumnTypeMap(ResultSetMetaData meta) throws Exception {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(int i=1; i<=meta.getColumnCount(); i++) {
			map.put(meta.getColumnName(i), meta.getColumnType(i));
		}
		
		return map;
	}
}
