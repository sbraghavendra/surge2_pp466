package com.ncsts.test;

import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;

@RunWith(TestClassRunner.class)
public class TaxRateServiceTest extends STSTestBase {
	
	JurisdictionTaxrateService jurisdictionTaxrateService = 
		(JurisdictionTaxrateService) getAppContext().getBean("taxRateService");
	JurisdictionService jurisdictionService = 
		(JurisdictionService) getAppContext().getBean("jurisdictionService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    @Test
    public void testGetJurisdictionIdsBySearchTaxRate(){
    	
    	System.out.println("Searching for tax rates and returing jurisdictionIds.");
    	JurisdictionTaxrate searchTaxRate = new JurisdictionTaxrate();
    	Calendar effDate  = Calendar.getInstance (  ) ;
    	//2005-01-01 00:00:00
    	effDate.set(2008, Calendar.FEBRUARY, 26);
    	searchTaxRate.setEffectiveDate(effDate.getTime());
    	List<JurisdictionTaxrate> sTaxRates = jurisdictionTaxrateService.findByExample(searchTaxRate, 0);
    	
    	System.out.println("Searching for tax rates.  Found=" + sTaxRates.size());
    	for (JurisdictionTaxrate sTaxRate : sTaxRates) {
    		System.out.println("Jurisdiction Id: " + sTaxRate.getJurisdiction().getJurisdictionId());
    	}    	
    }
}
