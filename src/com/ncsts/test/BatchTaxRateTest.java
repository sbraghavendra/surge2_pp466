package com.ncsts.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;
import com.ncsts.dao.BatchDAO;

@RunWith(TestClassRunner.class)
public class BatchTaxRateTest extends STSTestBase {

	BatchDAO batchDAO = (BatchDAO) getAppContext().getBean("batchDAO");

	@Before
	public void initialize() throws Exception {
		super.setUp();
	}

	@After
	public void cleanup() throws Exception {
		super.tearDown();
	}

	@Test
	public void testCodeUpdate() {

		batchDAO.processTaxrateUpdate(1136l);

	}
}
