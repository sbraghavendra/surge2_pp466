package com.ncsts.test;

import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.dao.CommonDAO;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.jsf.model.DelegatingDataModel;
import com.ncsts.services.TransactionDetailService;

import org.apache.commons.logging.*;

/**
 * <b>Created:</b> Mar 27, 2008<br>
 * <b>Title:</b> DelegatingDataModelTest<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * @todo add class overview here
 */
@RunWith(TestClassRunner.class)
public class DelegatingDataModelTest extends STSTestBase {
    private static final transient Log log = LogFactory.getLog(com.ncsts.test.DelegatingDataModelTest.class);

    /**default empty constructor*/
    public DelegatingDataModelTest() {
    }
    
    TransactionDetailService transactionDetailService = 
    	(TransactionDetailService) getAppContext().getBean("transactionDetailService");
    
    private CommonDAO commonDAO = 
    	(CommonDAO) getAppContext().getBean("commonDAO");
    
    private DelegatingDataModel<TransactionDetail> dataModel = new DelegatingDataModel<TransactionDetail>(TransactionDetail.class);
    
    // Some entities for testing
    private TransactionDetail detail1;
    private TransactionDetail detail2;
    private TransactionDetail detail3;
    
    private String uniqueCategoryName = null;
    private String uniqueDescription = null;
    
    @Before
    public void initialize() throws Exception {
        if (log.isInfoEnabled()) { log.info("BEGIN initialize()"); }
        super.setUp();
        
        // Set up dataModel
        dataModel.setCommonDAO(commonDAO);
        
        uniqueCategoryName = "NAME: " + new Random().nextInt();
        
        detail1 = new TransactionDetail();
        detail1.setTransactionDetailId(new Random().nextLong());
        detail1.setAfeCategoryName("test1");
        detail1.setInvoiceDesc(uniqueDescription);
        
        detail2 = new TransactionDetail();
        detail2.setTransactionDetailId(new Random().nextLong());
        detail2.setAfeCategoryName(uniqueCategoryName);
        
        detail3 = new TransactionDetail();
        detail3.setTransactionDetailId(new Random().nextLong());
        detail3.setAfeCategoryName("test3");
        
        commonDAO.persist(detail1);
        commonDAO.persist(detail2);
        commonDAO.persist(detail3);
        
        if (log.isInfoEnabled()) { log.info("END initialize()"); }
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    @Test
    public void testLoadData(){
        List<TransactionDetail> list = dataModel.loadData(0, 10, null);
        if (log.isInfoEnabled()) { log.info("list: " + list); }
        assertTrue("list.size(): " + list.size(), list.size() > 0);
    }
    
    @Test
    public void testLoadDataByExample(){
        dataModel.setExampleInstance(detail2);
        List<TransactionDetail> list = dataModel.loadData(0, 10, null);
        if (log.isInfoEnabled()) { log.info("list: " + list); }
        assertTrue("list.size(): " + list.size(), list.size() > 0);
        assertTrue(list.contains(detail2));
    }
    
}
