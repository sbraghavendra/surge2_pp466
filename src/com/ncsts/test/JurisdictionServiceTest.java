package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;

@RunWith(TestClassRunner.class)
public class JurisdictionServiceTest extends STSTestBase {

	JurisdictionService jurisdictionService = 
		(JurisdictionService) getAppContext().getBean("jurisdictionService");
	JurisdictionTaxrateService jurisdictionTaxrateService = 
		(JurisdictionTaxrateService) getAppContext().getBean("jurisdictionTaxrateService");
	
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
/*    @Test
    public void testGetAllJurisdiction(){
    	List<Jurisdiction> jurisdictionList = jurisdictionService.findByExample(new Jurisdiction(), 199);
    	assertEquals(199, jurisdictionList.size());
    	for(Jurisdiction jurisdiction:jurisdictionList){
    		logger.info("jurisdiction.getCity() "+jurisdiction.getCity());
    		logger.info("jurisdiction.getCountry "+jurisdiction.getCounty());
    	}
    }*/
    // FK and Constraints,  for cascade delete test
    @Test
    public void deleteJuris(){
    	Jurisdiction jurisdiction = new Jurisdiction();
    	jurisdiction.setId(136588L);
    	jurisdictionService.remove(jurisdiction);
    }
    
   
}
