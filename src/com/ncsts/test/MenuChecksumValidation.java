package com.ncsts.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ncsts.security.MD5Util;

public class MenuChecksumValidation {
	public static void main(String[] args) throws Exception {
		String propFileName = "WebRoot/WEB-INF/classes/menu-permissions.properties";
		String javaFileName = "src/com/ncsts/security/ValidMenuOptions.java";
		
		File file = new File(propFileName);
		File javaFile = new File(javaFileName);
		String md5hex = MD5Util.computeChecksum(file);
		String sourceMD5hex = null;
		
		BufferedReader r = new BufferedReader(new FileReader(javaFile));
		String line = null;
		StringBuilder content = new StringBuilder();
		boolean checksumLineFound = false;
		while((line = r.readLine()) != null) {
			if(!checksumLineFound) {
				int idx = line.indexOf("CHECKSUM");
				if(idx > -1) {
					content.append(line);
					if(line.indexOf(";", idx) > -1) {
						break;
					}
				}
			}
			else {
				content.append(line);
				if(line.indexOf(";") > -1) {
					break;
				}
			}
		}
		
		Pattern p = Pattern.compile("CHECKSUM\\s+=\\s+\\\"(.*?)\\\"");
		Matcher m = p.matcher(content.toString());
		if(m.find()) {
			sourceMD5hex = m.group(1);
		}
		
		if(!md5hex.equalsIgnoreCase(sourceMD5hex)) {
			System.out.println("md5sum of " + propFileName + " doesn't match in " + javaFileName);
			System.out.println("Actual: " + md5hex + ", In source: " + sourceMD5hex);
			System.exit(50);
		}
		else {
			System.out.println("MD5sum matched: " + md5hex);
		}
	}
}
