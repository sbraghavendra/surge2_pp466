package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.BatchErrorLog;
import com.ncsts.dto.BatchMaintenanceDTO;
import com.ncsts.services.BatchMaintenanceService;

@RunWith(TestClassRunner.class)
public class BatchMaintenanceTest extends STSTestBase {

	BatchMaintenanceService service =
		(BatchMaintenanceService) getAppContext().getBean("batchMaintenanceService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }
    

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
 /*   @Test
    public void testgetAllGLExtractedBatches(){
    	List<BatchMaintenanceDTO> list = service.getAllBatchesByBatchTypeCode("GE");
    	assertEquals(37, list.size());
    }*/
    
    @Test
    public void batchErrorSave(){
    	BatchErrorLog error = new BatchErrorLog();
    	error.setBatchErrorLogId(null);
		error.setProcessType("IP");
		error.setBatchId(1234l);
    	service.save(error);
    }
/*    @Test
    public void testCompanyList(){
    	List<GLExtAetnaprod1DTO> list = service.getAllGLExtProd1();
    	assertEquals(2126, list.size());
    }*/
}
