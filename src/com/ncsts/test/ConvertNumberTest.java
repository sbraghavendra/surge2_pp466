package com.ncsts.test;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ConvertNumberTest {
	public static void main(String[] args) {
		DecimalFormat df = new DecimalFormat("0.00");
		for(String s : new String[]{"32.405", "32.415", "32.404", "32.406", "32.4049"}) {
			double d = new Float("25.585").doubleValue();
			System.out.println(d);
			double n = Double.parseDouble(s);
			df.setRoundingMode(RoundingMode.HALF_EVEN);
			System.out.println(df.getRoundingMode() + "->" + n + "->" + df.format(n));
			df.setRoundingMode(RoundingMode.HALF_UP);
			System.out.println(df.getRoundingMode() + "  ->" + n + "->" + df.format(n));
			df.setRoundingMode(RoundingMode.HALF_DOWN);
			System.out.println(df.getRoundingMode() + "->" + n + "->" + df.format(n));
			System.out.println("=====");
		}
	}
}
