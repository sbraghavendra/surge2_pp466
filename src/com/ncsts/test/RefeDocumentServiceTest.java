package com.ncsts.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.TestClassRunner;
import org.junit.runner.RunWith;

import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.services.AllocationMatrixService;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.ReferenceDocumentService;

@RunWith(TestClassRunner.class)
public class RefeDocumentServiceTest extends STSTestBase {
	
	ReferenceDocumentService referenceDocumentService = 
		(ReferenceDocumentService) getAppContext().getBean("referenceDocumentService");
	
    @Before
    public void initialize() throws Exception {
        super.setUp();
    }

    @After
    public void cleanup() throws Exception {
        super.tearDown();
    }
    
    // FK and Constraints 
/*   @Test
    public void deleteRefDoc(){
	   logger.info("Inside  delete method (test)");
	   ReferenceDocument referenceDocument = new ReferenceDocument();
	   referenceDocument.setRefDocCode("KY REF 003-1");
	   referenceDocumentService.remove(referenceDocument);
	   logger.info("^^^^^^^^^END^^^^^^^^^^");
    }*/
    

    @Test
    public void testUpdateCodes(){
    	ReferenceDocument example = new ReferenceDocument();
    	example.setId("GA REF 005-1test");
    	/*ListCodes listCode = new ListCodes();
    	listCode.setListCodesPK(new ListCodesPK("REFTYPE","ANNO"));
    	example.setListcodes(listCode);*/
    	example.setRefTypeCode("CASE200");
    	example.setDescription("Test1");
    	referenceDocumentService.update(example);
    }
    
}
