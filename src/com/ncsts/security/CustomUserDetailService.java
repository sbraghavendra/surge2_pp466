package com.ncsts.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ncsts.dto.UserRolesDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.view.bean.ChangeContextHolder;
import com.ncsts.view.bean.RoutingDataSource;
import com.ncsts.view.util.DesEncrypter;

@Component
public class CustomUserDetailService implements UserDetailsService, InitializingBean {
	@Autowired
	private RoutingDataSource dataSource;
	
	private UserSecurityBean userSecurityBean;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		
		User user = null;
		String[] fields = parseUsername(username);
		StringBuilder error = new StringBuilder("Bad Credentials");
		if(fields[0] != null) {
			UserSecurityDTO userSecurityDTO = userSecurityBean.getUserSecurity(fields[0]);
			if(userSecurityDTO != null) {
				List<String> dbPropertiesList = userSecurityBean.findUserDbPropertiesList(userSecurityDTO.getUserId());
				String dbName = null;
				if(dbPropertiesList.size() == 1) {
					if(fields[1] == null || fields[1].trim().length() == 0 || fields[1].trim().equalsIgnoreCase(dbPropertiesList.get(0))) {
						dbName = dbPropertiesList.get(0);
					}
				}
				else if(dbPropertiesList.size() > 1) {
					if(fields[1] != null) {
						for (String db : dbPropertiesList) {
							if(fields[1].trim().equalsIgnoreCase(db)) {
								dbName = db;
								break;
							}
						}
					}
				}
				
				if(dbName != null) {
					String password = DesEncrypter.getInstance().decrypt(userSecurityDTO.getPassword());
					
					Collection<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(1);
					UserRolesDTO userRolesDTO = userSecurityBean.getUserRoles(userSecurityDTO.getRoleId());
					if(userRolesDTO != null) {
						authList.add(new GrantedAuthorityImpl(userRolesDTO.getRoleName()));
					}
					
					boolean locked = userSecurityDTO.getLock() != null && userSecurityDTO.getLock().intValue() == 1;
					int iDays = userSecurityBean.passwordExpiredInDays(fields[0]);
					boolean credentialsExpired = iDays == -1;
					
					user = new User(fields[0], password, true, true, !credentialsExpired, !locked, authList);
					
					ChangeContextHolder.clearDataBase();
            		ChangeContextHolder.setDataBaseType(dbName);
				}
				else {
					error.append(" - Invalid Database");
				}
			}
		}
		
		if(user == null) {
			throw new UsernameNotFoundException(error.toString());
		}
		
		return user;
	}

	private String[] parseUsername(String username) {
		String[] fields = null;
		int index = -1;
		if(username == null || username.length() == 0) {
			fields = new String[]{null, null};
		}
		else if((index = username.indexOf('\\')) != -1 && index < username.length() - 1) {
			fields = new String[]{username.substring(0, index).toUpperCase(), username.substring(index + 1)};
		}
		else if((index = username.indexOf('@')) != -1 && index < username.length() - 1) {
			fields = new String[]{username.substring(0, index).toUpperCase(), username.substring(index + 1)};
		}
		else {
			fields = new String[]{username.toUpperCase(), null};
		}
		
		return fields;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.userSecurityBean = new UserSecurityBean();
		this.userSecurityBean.setDataSource(dataSource);
	}
}
