package com.ncsts.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;

public class MD5Util {
	public static String computeChecksum(File file) {
		String md5hex = null;
		try {
			InputStream is = new FileInputStream(file);
			MessageDigest md = MessageDigest.getInstance("MD5");
			InputStream dis = new DigestInputStream(is, md);
			byte[] b = new byte[1024];
			while (dis.read(b) > -1) {
				// nothing to do here other than reading until EOF
			}
			b = md.digest();
			dis.close();
			is.close();

			StringBuilder hex = new StringBuilder();
			for (int i = 0; i < b.length; i++) {
				hex.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));
			}
			
			md5hex = hex.toString();
		} catch (Exception e) {}
		
		return md5hex;
	}
	
	public static void main(String[] args) throws Exception {
		String md5hex = computeChecksum(new File(args[0]));
		System.out.println(md5hex + "\t" + args[0]);
	}
}
