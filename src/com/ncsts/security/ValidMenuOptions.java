package com.ncsts.security;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class ValidMenuOptions implements InitializingBean {
	private static final String CHECKSUM = "d5fa05be39d8addf1fdc58e8557394fa";
	
	@Value("classpath:menu-permissions.properties")
	private File permissions;
	
	private static Set<String> purchaseMenus;
	private static Set<String> saleMenus;

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.permissions);
		Assert.isTrue(isValidChecksum(), "menu-permissions.properties checksum doesn't match");
		
		Properties prop = new Properties();
		prop.load(new FileInputStream(permissions));
		
		purchaseMenus = new HashSet<String>();
		saleMenus = new HashSet<String>();
		
		for(Object k : prop.keySet()) {
			Object v = prop.get(k);
			try {
				String sk = k + "";
				String sv = (v + "").trim();
				int iv = Integer.parseInt(sv);
				if(iv == 1) {
					purchaseMenus.add(sk);
				}
				else if(iv == 2) {
					saleMenus.add(sk);
				}
				else if(iv == 3) {
					purchaseMenus.add(sk);
					saleMenus.add(sk);
				}
			}
			catch(Exception e){}
		}
	}
	
	public static Set<String> getPurchaseMenus() {
		return purchaseMenus;
	}
	
	public static Set<String> getSaleMenus() {
		return saleMenus;
	}
	
	private boolean isValidChecksum() {
		boolean valid = false;
		String md5hex = null;
		md5hex = MD5Util.computeChecksum(this.permissions);

		if(CHECKSUM.equalsIgnoreCase(md5hex)) {
			valid = true;
		}
		
		return valid;
	}
}
