package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.LocationMatrixDAO;
import com.ncsts.domain.LocationMatrix;

public class LocationMatrixDataModel extends BaseExtendedDataModel<LocationMatrix,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(LocationMatrixDataModel.class);
	
	private String geocode;
	private String city;
	private String county;
	
	private String state;
	private String country;
	private String zip;
	
	private String defaultFlag;
	private Long locationMatrixId;

	public LocationMatrixDataModel() {
	}
	
	public void setFilterCriteria(LocationMatrix locationMatrix,
    		String geocode, String city, String county,
    		String state, String country, String zip,
    		String defaultFlag, Long locationMatrixId) {
		setExampleInstance(locationMatrix);
		this.geocode = geocode;
		this.city = city;
		this.county = county;
		
		this.state = state;
		this.country = country;
		this.zip = zip;
		
		this.defaultFlag = defaultFlag;
		this.locationMatrixId = locationMatrixId;
	}

	public void resetFilterCriteria() {
		geocode = null;
		city = null;
		county = null;
		
		state = null;
		zip = null;
		
		defaultFlag = null;
		locationMatrixId = null;
		
		refreshData(false);
	}
	
	@Override
	protected List<LocationMatrix> fetchData(int firstRow, int maxResults) {
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		LocationMatrixDAO dao = (LocationMatrixDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), 
				geocode, city, county, state, country, zip,
				defaultFlag, locationMatrixId,
				getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		LocationMatrixDAO dao = (LocationMatrixDAO) getCommonDAO();
		count = dao.count(getExampleInstance(), 
				geocode, city, county, state, country, zip,
				defaultFlag, locationMatrixId);
		return count;
	}
}


