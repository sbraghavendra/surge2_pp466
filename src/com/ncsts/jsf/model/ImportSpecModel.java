package com.ncsts.jsf.model;

import java.util.List;

import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.dao.ImportSpecDAO;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;

public class ImportSpecModel extends BaseExtendedDataModel<ImportSpec, ImportSpecPK> {
	public ImportSpecModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
		setExampleInstance(new ImportSpec());
	}
	
	public void setFilterCriteria(ImportSpec importSpec) {
		setExampleInstance(importSpec);
		refreshData(false);
	}

	@Override
	protected List<ImportSpec> fetchData(int firstRow, int maxResults) {
		ImportSpecDAO dao = (ImportSpecDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		ImportSpecDAO dao = (ImportSpecDAO) getCommonDAO();
		count = dao.count(getExampleInstance());
		return count;
	}
}
