package com.ncsts.jsf.model;

import java.util.List;

import com.seconddecimal.billing.domain.SaleTransaction;

public class TransactionSaleDataMemoryModel extends BaseExtendedDataModel<SaleTransaction, Long> {
	private List<SaleTransaction> saleTransactions;
	
	@Override
	protected List<SaleTransaction> fetchData(int firstRow, int maxResults) {
		return this.saleTransactions;
	}

	@Override
	protected Long count() {
		return this.saleTransactions == null ? 0L : (long) this.saleTransactions.size();
	}

	public List<SaleTransaction> getSaleTransactions() {
		return saleTransactions;
	}

	public void setSaleTransactions(List<SaleTransaction> saleTransactions) {
		this.saleTransactions = saleTransactions;
	}

	@Override
	public void tooggleSelectAll(boolean value, int maxToFetch) {
		if(value) {
			getSelectionMap().clear();
			for(SaleTransaction t : this.saleTransactions) {
				getSelectionMap().put(t.getId(), Boolean.TRUE);
			}
		}
		else {
			getSelectionMap().clear();
		}
	}
}
