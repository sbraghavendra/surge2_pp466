package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailBillDAO;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.domain.BillTransaction;

public class TransactionBillDataModel extends BaseExtendedDataModel<BillTransaction,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionBillDataModel.class);
	
	private Date effectiveDate;
	private Date expirationDate;
	private Boolean includeUnprocessed;
	private Boolean includeProcessed;
	private String includeSuspended;
	private Boolean glExtractFlagIsNull;
	private boolean criteriaSet = false;
	private String advancedFilter = "";
	
	private BillTransaction exampleInstance;
	
	public BillTransaction getExampleInstance() {
        return this.exampleInstance;
    }

	public void setExampleInstance(BillTransaction exampleInstance) {
		this.exampleInstance = exampleInstance;
        refreshData(false);
    }


	public TransactionBillDataModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
	}
	
	public void setFilterCriteria(BillTransaction billTransactionFilter) {
		setExampleInstance(billTransactionFilter);
		this.criteriaSet = true;
	}

	public void resetFilterCriteria() {
		effectiveDate = null;
		expirationDate = null;
		includeUnprocessed = null;
		includeProcessed = null;
		includeSuspended = null;
		glExtractFlagIsNull = null;
		this.criteriaSet = false;
		this.advancedFilter = "";
		refreshData(false);
	}
	
	public OrderBy getSaveAsOrderBy() {
		//we probably shouldn't let "database ordering happen,
		//so if no ordering is picked put in a order by transaction detail id
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
		
    	return orderBy;
	}
	
	@Override
	protected List<BillTransaction> fetchData(int firstRow, int maxResults) {
		List<BillTransaction> results;
		logger.info("enter TransactionBillDataModel.fetchData()");
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			TransactionDetailBillDAO dao = (TransactionDetailBillDAO) getCommonDAO();
			logger.info("just before exit TransactionBillDataModel.fetchData()");
			long start = System.currentTimeMillis();
			
			//we probably shouldn't let "database ordering happen,
			//so if no ordering is picked put in a order by transaction detail id
			OrderBy orderBy = getOrderBy();
			if(orderBy.getSortOrderSize()==0){
				orderBy = getDefaultOrderBy();
			}
			
			BillTransaction td = new BillTransaction();
			org.springframework.beans.BeanUtils.copyProperties(getExampleInstance(), td);
			
			List<BillTransaction> list = dao.getAllRecords(td, orderBy, firstRow, maxResults);
			results = list;
		} else {
			results = new ArrayList<BillTransaction>(); 
		}
		
		return results;
	}
	
	OrderBy defaultOrderBy = null;
	
	//degault is to order on transaction detail id
	protected OrderBy getDefaultOrderBy() {
    		if(defaultOrderBy == null){
    				OrderBy o = new OrderBy();
    				o.addFieldSortOrder(new FieldSortOrder("billtransId",1,true));
    				defaultOrderBy = o;
    		}
    		return defaultOrderBy;
	}
	
	@Override
	public Long count() {
		logger.info("enter TransactionalDataModel.count()");
		Long count = 0L;
		if (criteriaSet) {
			TransactionDetailBillDAO dao = (TransactionDetailBillDAO) getCommonDAO();

			long start = System.currentTimeMillis();		
			
			BillTransaction td = new BillTransaction();
			org.springframework.beans.BeanUtils.copyProperties(getExampleInstance(), td);
			
			count = dao.count(td);
			long finish = System.currentTimeMillis();
			logger.info("TransactionBillDataModel.count() -> query time = " + (finish - start));			
		}
		logger.info("exit TransactionBillDataModel.count(); count = " + count);
		return count;
	}

}

