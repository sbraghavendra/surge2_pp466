package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.ReferenceDocumentDAO;
import com.ncsts.domain.ReferenceDocument;

public class RefDocLookupDataModel extends BaseExtendedDataModel<ReferenceDocument, String> {
	private Logger logger = LoggerFactory.getInstance().getLogger(RefDocLookupDataModel.class);

	private boolean criteriaSet = false;
	
	OrderBy defaultOrderBy = null;

	public RefDocLookupDataModel() {

	}

	public void setFilterCriteria(ReferenceDocument referenceDocument) {
		setExampleInstance(referenceDocument);
		this.criteriaSet = true;
	}

	public void resetFilterCriteria() {
		this.criteriaSet = false;
	}

	@Override
	protected List<ReferenceDocument> fetchData(int firstRow, int maxResults) {
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			ReferenceDocumentDAO dao = (ReferenceDocumentDAO) getCommonDAO();
			OrderBy orderBy = getOrderBy();
			if(orderBy.getSortOrderSize()==0){
				orderBy = getDefaultOrderBy();
			}
			
			return dao.search(getExampleInstance(),firstRow, maxResults,orderBy);
			
		}
		
		logger.debug("No criteria set");
		return new ArrayList<ReferenceDocument>();
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		if (criteriaSet) {
			ReferenceDocumentDAO dao = (ReferenceDocumentDAO) getCommonDAO();
			count = dao.count(getExampleInstance());
		}
		return count;
	}
	
	//degault is to order on document code
		protected OrderBy getDefaultOrderBy() {
	    		if(defaultOrderBy == null){
	    				OrderBy o = new OrderBy();
	    				o.addFieldSortOrder(new FieldSortOrder("refDocCode",1,true));
	    				defaultOrderBy = o;
	    		}
	    		return defaultOrderBy;
		}
}
