package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPJurisdictionTaxRateDAO;
import com.ncsts.dao.BCPJurisdictionTaxRateTextDAO;
import com.ncsts.domain.BCPJurisdictionTaxRateText;
import com.ncsts.domain.BCPJurisdictionTaxRateTextPK;
import com.ncsts.domain.BatchMaintenance;


public class BCPJurisdictionTaxRateTextDataModel  extends BaseExtendedDataModel<BCPJurisdictionTaxRateText, BCPJurisdictionTaxRateTextPK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPJurisdictionTaxRateTextDataModel.class);

	private BatchMaintenance batchMaintenance;
	
	public BCPJurisdictionTaxRateTextDataModel() {
	}

	@Override
	protected List<BCPJurisdictionTaxRateText> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPJurisdictionTaxRateTextDAO dao = (BCPJurisdictionTaxRateTextDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
		
	}

	@Override
	public Long count() {
		BCPJurisdictionTaxRateTextDAO dao = (BCPJurisdictionTaxRateTextDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}

	/**
	 * @return the batchMaintenance
	 */
	public BatchMaintenance getBatchMaintenance() {
		return this.batchMaintenance;
	}

	/**
	 * @param batchMaintenance the batchMaintenance to set
	 */
	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	public BCPJurisdictionTaxRateTextDAO getDAO() {
		return (BCPJurisdictionTaxRateTextDAO)getCommonDAO();
	}


}
