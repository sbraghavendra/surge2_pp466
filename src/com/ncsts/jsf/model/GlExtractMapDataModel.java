package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.GlExtractMapDAO;
import com.ncsts.domain.GlExtractMap;



public class GlExtractMapDataModel  extends BaseExtendedDataModel<GlExtractMap, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(GlExtractMapDataModel.class);
	
	private boolean criteriaSet = false;
	
	public GlExtractMapDataModel() {
//		setPageSize(10);	// attempt to speed up paging through transactions
//		setDbFetchMultiple(10);
	}
	
	public void refreshData(boolean retainCount) {
		this.criteriaSet = false;
		super.refreshData(retainCount);
	}
	
	public void setFilterCriteria(GlExtractMap glExtractMap) {
		setExampleInstance(glExtractMap);
		criteriaSet = true;
	}


	@Override
	protected List<GlExtractMap> fetchData(int firstRow, int maxResults) {
		GlExtractMapDAO dao = (GlExtractMapDAO) getCommonDAO();
		//if(criteriaSet){
		int count = 0;
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		
		return dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
		//}
		/*else {
			//return new ArrayList<GlExtractMap>();
			
		}*/
		
	}

	@Override
	public Long count() {
		Long count = 0L;
		if(criteriaSet){
		GlExtractMapDAO dao = (GlExtractMapDAO) getCommonDAO();
		return dao.count(getExampleInstance());
		}
		else {
			return count;
		}
	}

	public boolean isCriteriaSet() {
		return criteriaSet;
	}

	public void setCriteriaSet(boolean criteriaSet) {
		this.criteriaSet = criteriaSet;
		refreshData(false);
	}

	
	
}
