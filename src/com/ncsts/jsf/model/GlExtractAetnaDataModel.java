package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.GlExtractAetnaDAO;
import com.ncsts.domain.GlExtractAetna;


public class GlExtractAetnaDataModel  extends BaseExtendedDataModel<GlExtractAetna, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(GlExtractAetnaDataModel.class);

	private boolean enabled=false;
	
	public GlExtractAetnaDataModel() {
//		setPageSize(10);	// attempt to speed up paging through transactions
//		setDbFetchMultiple(10);
	}

	@Override
	protected List<GlExtractAetna> fetchData(int firstRow, int maxResults) {
		if (!enabled) return new ArrayList<GlExtractAetna>();
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		GlExtractAetnaDAO dao = (GlExtractAetnaDAO) getCommonDAO();
		return dao.getPage(firstRow, maxResults);
		
	}

	@Override
	public Long count() {
		if (! enabled) return 0l;
		GlExtractAetnaDAO dao = (GlExtractAetnaDAO) getCommonDAO();
		return dao.count();
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return this.enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		refreshData(false);
	}
	
}
