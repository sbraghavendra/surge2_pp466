package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailSaleDAO;
import com.ncsts.domain.TransactionDetail;
import com.seconddecimal.billing.domain.SaleTransaction;

public class TransactionSaleDataModel extends BaseExtendedDataModel<SaleTransaction,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionSaleDataModel.class);
	
	private Date effectiveDate;
	private Date expirationDate;
	private Boolean includeUnprocessed;
	private Boolean includeProcessed;
	private String includeSuspended;
	private Boolean glExtractFlagIsNull;
	private boolean criteriaSet = false;
	private String advancedFilter = "";
	
	private SaleTransaction exampleInstance;
	
	public SaleTransaction getExampleInstance() {
        return this.exampleInstance;
    }

	public void setExampleInstance(SaleTransaction exampleInstance) {
		this.exampleInstance = exampleInstance;
        refreshData(false);
    }


	public TransactionSaleDataModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
	}
	
	public void setFilterCriteria(SaleTransaction saleTransactionFilter) {
		setExampleInstance(saleTransactionFilter);
		this.criteriaSet = true;
	}

	public void resetFilterCriteria() {
		effectiveDate = null;
		expirationDate = null;
		includeUnprocessed = null;
		includeProcessed = null;
		includeSuspended = null;
		glExtractFlagIsNull = null;
		this.criteriaSet = false;
		this.advancedFilter = "";
		refreshData(false);
	}
	
	public OrderBy getSaveAsOrderBy() {
		//we probably shouldn't let "database ordering happen,
		//so if no ordering is picked put in a order by transaction detail id
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
		
    	return orderBy;
	}
	
	@Override
	protected List<SaleTransaction> fetchData(int firstRow, int maxResults) {
		List<SaleTransaction> results;
		logger.info("enter TransactionSaleDataModel.fetchData()");
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			TransactionDetailSaleDAO dao = (TransactionDetailSaleDAO) getCommonDAO();
			logger.info("just before exit TransactionSaleDataModel.fetchData()");
			long start = System.currentTimeMillis();
			
			//we probably shouldn't let "database ordering happen,
			//so if no ordering is picked put in a order by transaction detail id
			OrderBy orderBy = getOrderBy();
			if(orderBy.getSortOrderSize()==0){
				orderBy = getDefaultOrderBy();
			}
			
			SaleTransaction td = new SaleTransaction();
			org.springframework.beans.BeanUtils.copyProperties(getExampleInstance(), td);
			
			List<SaleTransaction> list = dao.getAllRecords(td, orderBy, firstRow, maxResults);
			results = list;
		} else {
			results = new ArrayList<SaleTransaction>(); 
		}
		
		return results;
	}
	
	OrderBy defaultOrderBy = null;
	
	//degault is to order on transaction detail id
	protected OrderBy getDefaultOrderBy() {
    		if(defaultOrderBy == null){
    				OrderBy o = new OrderBy();
    				o.addFieldSortOrder(new FieldSortOrder("saletransId",1,true));
    				defaultOrderBy = o;
    		}
    		return defaultOrderBy;
	}
	
	@Override
	public Long count() {
		logger.info("enter TransactionalDataModel.count()");
		Long count = 0L;
		if (criteriaSet) {
			TransactionDetailSaleDAO dao = (TransactionDetailSaleDAO) getCommonDAO();

			long start = System.currentTimeMillis();		
			
			SaleTransaction td = new SaleTransaction();
			org.springframework.beans.BeanUtils.copyProperties(getExampleInstance(), td);
			
			count = dao.count(td);
			long finish = System.currentTimeMillis();
			logger.info("TransactionSaleDataModel.count() -> query time = " + (finish - start));			
		}
		logger.info("exit TransactionSaleDataModel.count(); count = " + count);
		return count;
	}

}

