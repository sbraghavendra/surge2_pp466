package com.ncsts.jsf.model;


import org.apache.log4j.Logger;

import org.hibernate.criterion.DetachedCriteria;


import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;

import com.ncsts.domain.ProruleAudit;



public class ProruleAuditDataModel extends BaseExtendedDataModel<ProruleAudit, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(ProruleAuditDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public ProruleAuditDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(ProruleAudit audit) {
		setExampleInstance(audit);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
		public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
