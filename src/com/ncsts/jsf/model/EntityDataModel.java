package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.EntityItemDAO;
import com.ncsts.domain.EntityItem;


public class EntityDataModel extends BaseExtendedDataModel<EntityItem, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(EntityDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	private String whereClause = "";
	
	public EntityDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(EntityItem entityItem, String whereClause) {
		setExampleInstance(entityItem);
		this.whereClause = whereClause;
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<EntityItem> fetchData(int firstRow, int maxResults) {
		EntityItemDAO dao = (EntityItemDAO) getCommonDAO();
		
		//getAllRecords(EntityItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause)
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<EntityItem> list = dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults, whereClause);
			for (EntityItem bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<EntityItem>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			EntityItemDAO dao = (EntityItemDAO) getCommonDAO();
			return dao.count(getExampleInstance(), whereClause);
		} else{
			return 0l;
		} 
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public EntityItemDAO getCustomerLocationDAO() {
		return (EntityItemDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
