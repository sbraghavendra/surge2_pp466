package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.AllocationMatrixDAO;
import com.ncsts.domain.AllocationMatrix;

public class AllocationMatrixDataModel extends BaseExtendedDataModel<AllocationMatrix,Long> {

	static private Logger logger = LoggerFactory.getInstance().getLogger(AllocationMatrixDataModel.class);
	
	private String geocode;
    private String city;
    private String county;
    private String state;
    private String country;
    private String zip;
    
    private Long allocationMatrixId;

	public AllocationMatrixDataModel() {
	}
	
	public void setFilterCriteria(AllocationMatrix matrix,
    		String geocode, String city, String county, String state, String country, String zip,
    		Long allocationMatrixId) {
		setExampleInstance(matrix);
		this.geocode = geocode;
		this.city = city;
		this.county = county;
		this.state = state;
		this.country = country;
		this.zip = zip;
		this.allocationMatrixId = allocationMatrixId;
	}

	public void resetFilterCriteria() {
		this.geocode = null;
		this.city = null;
		this.county = null;
		this.state = null;
		this.country = null;
		this.zip = null;
		this.allocationMatrixId = null;
		
		refreshData(false);
	}
	
	@Override
	protected List<AllocationMatrix> fetchData(int firstRow, int maxResults) {
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		AllocationMatrixDAO dao = (AllocationMatrixDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), 
				geocode, city, county, state, country, zip, allocationMatrixId,
				getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		AllocationMatrixDAO dao = (AllocationMatrixDAO) getCommonDAO();
		count = dao.count(getExampleInstance(), geocode, city, county, state, country, zip, allocationMatrixId);
		return count;
	}
}

