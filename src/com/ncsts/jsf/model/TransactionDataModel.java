package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.PurchaseTransaction;

public class TransactionDataModel extends BaseExtendedDataModel<PurchaseTransaction,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionDataModel.class);
	
	private Date effectiveDate;
	private Date expirationDate;
	private Boolean includeUnprocessed;
	private Boolean includeProcessed;
	private String includeSuspended;
	private Boolean glExtractFlagIsNull;
	private boolean criteriaSet = false;
	private String advancedFilter = "";
	

	public TransactionDataModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
	}
	
	public void setFilterCriteria(PurchaseTransaction transactionDetail,
    		Date effectiveDate, Date expirationDate, 
    		Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
    		Boolean glExtractFlagIsNull) {
		setExampleInstance(transactionDetail);
		this.effectiveDate = effectiveDate;
		this.expirationDate = expirationDate;
		this.includeUnprocessed = includeUnprocessed;
		this.includeProcessed = includeProcessed;
		this.includeSuspended = includeSuspended;
		this.glExtractFlagIsNull = glExtractFlagIsNull;
		this.criteriaSet = true;
	}
	
	public void setFilterCriteriaAdvanced(PurchaseTransaction transactionDetail,
    		Date effectiveDate, Date expirationDate, 
    		Boolean includeUnprocessed, Boolean includeProcessed, String includeSuspended,
    		Boolean glExtractFlagIsNull, String advancedFilter) {
		logger.info("enter setFilterCriteriaAdvanced()");
		setExampleInstance(transactionDetail);
		this.effectiveDate = effectiveDate;
		this.expirationDate = expirationDate;
		this.includeUnprocessed = includeUnprocessed;
		this.includeProcessed = includeProcessed;
		this.includeSuspended = includeSuspended;
		this.glExtractFlagIsNull = glExtractFlagIsNull;
		this.advancedFilter = advancedFilter;
		
		this.criteriaSet = true;
		logger.info("exit setFilterCriteriaAdvanced()");		
	}

	public void resetFilterCriteria() {
		effectiveDate = null;
		expirationDate = null;
		includeUnprocessed = null;
		includeProcessed = null;
		includeSuspended = null;
		glExtractFlagIsNull = null;
		this.criteriaSet = false;
		this.advancedFilter = "";
		
		refreshData(false);
	}
	
	public Date getEffectiveDate(){
		return effectiveDate;
	}
	public Date getExpirationDate(){
		return expirationDate;
	}
	public Boolean getIncludeUnprocessed(){
		return includeUnprocessed;
	}
	public Boolean getIncludeProcessed(){
		return includeProcessed;
	}
	public String getIncludeSuspended(){
		return includeSuspended;
	}
	public Boolean getGlExtractFlagIsNull(){
		return glExtractFlagIsNull;
	}
	public String getAdvancedFilter(){
		return advancedFilter;
	}
	
	public OrderBy getSaveAsOrderBy() {
		//we probably shouldn't let "database ordering happen,
		//so if no ordering is picked put in a order by transaction detail id
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
		
    	return orderBy;
	}
	
	@Override
	public List<PurchaseTransaction> fetchData(int firstRow, int maxResults) {
		List<PurchaseTransaction> results;
		logger.info("enter TransactionDataModel.fetchData()");
		//maxResults = 100;
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			logger.info("just before exit TransactionDataModel.fetchData()");
			long start = System.currentTimeMillis();
			
			//we probably shouldn't let "database ordering happen,
			//so if no ordering is picked put in a order by transaction detail id
			OrderBy orderBy = getOrderBy();
			if(orderBy.getSortOrderSize()==0){
				orderBy = getDefaultOrderBy();
			}
			
			
			List<PurchaseTransaction> list = dao.getAllRecords(getExampleInstance(), 
					effectiveDate, expirationDate, 
					includeUnprocessed, includeProcessed, includeSuspended,
					glExtractFlagIsNull, advancedFilter,
					orderBy, firstRow, maxResults);
			
			// Force redisplay of page for transaction process search filter
			//AC, It causes to refresh whole page 3/5/10.
			//			UIViewRoot root = FacesContext.getCurrentInstance().getViewRoot();
			//			UIComponent cmp = root.findComponent("filterForm:redisplayTransactions");
			//			if (cmp != null)
			//				cmp.broadcast(new ActionEvent(cmp));
			
			results = list;
		} else {
			results = new ArrayList<PurchaseTransaction>(); 
		}
		
		return results;
	}
	
	OrderBy defaultOrderBy = null;
	
	//degault is to order on transaction detail id
	protected OrderBy getDefaultOrderBy() {
    		if(defaultOrderBy == null){
    				OrderBy o = new OrderBy();
    				o.addFieldSortOrder(new FieldSortOrder("purchtransId",1,true));
    				defaultOrderBy = o;
    		}
    		return defaultOrderBy;
	}
	
	@Override
	public Long count() {
		logger.info("enter TransactionalDataModel.count()");
		Long count = 0L;
		if (criteriaSet) {
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			logger.info("effectiveDate = " + effectiveDate);
			logger.info("expirationDate = " + expirationDate);		
			logger.info("includeUnprocessed = " + includeUnprocessed);	
			logger.info("includeProcessed = " + includeProcessed);	
			logger.info("includeSuspended = " + includeSuspended);	
			logger.info("glExtractFlagIsNull = " + glExtractFlagIsNull);			
			logger.info("advancedFilter = " + advancedFilter);	
			long start = System.currentTimeMillis();			
			count = dao.count(getExampleInstance(), 
					effectiveDate, expirationDate, 
					includeUnprocessed, includeProcessed, includeSuspended,
					glExtractFlagIsNull, advancedFilter);
			long finish = System.currentTimeMillis();
			logger.info("TransactionDataModel.count() -> query time = " + (finish - start));			
		}
		logger.info("exit TransactionalDataModel.count(); count = " + count);
		return count;
	}
	
	public Date minGLDate() {
		Date minDate = null;
		if (criteriaSet) {
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			minDate = dao.minGLDate(getExampleInstance(), 
					effectiveDate, expirationDate, 
					includeUnprocessed, includeProcessed, includeSuspended,
					glExtractFlagIsNull);
		}
		return minDate;
	}
	
	public boolean isValidAdvancedFilter(String advancedFilter) {
		if (advancedFilter!=null && advancedFilter.length()>0) {
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			return dao.isValidAdvancedFilter(advancedFilter);
		}
		else{
			return true;
		}
	}

}

