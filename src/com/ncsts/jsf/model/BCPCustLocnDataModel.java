package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPCustLocnDAO;
import com.ncsts.domain.BCPCustLocn;
import com.ncsts.domain.BatchMaintenance;


public class BCPCustLocnDataModel extends BaseExtendedDataModel<BCPCustLocn, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPCustLocnDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	@Override
	public List<BCPCustLocn> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPCustLocnDAO dao = (BCPCustLocnDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPCustLocnDAO dao = (BCPCustLocnDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
