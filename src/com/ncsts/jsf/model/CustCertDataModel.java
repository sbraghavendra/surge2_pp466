package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.CustCertDAO;
import com.ncsts.domain.CustCert;


public class CustCertDataModel extends BaseExtendedDataModel<CustCert, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(CustCertDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public CustCertDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(CustCert custCert) {
		setExampleInstance(custCert);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<CustCert> fetchData(int firstRow, int maxResults) {
		CustCertDAO dao = (CustCertDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<CustCert> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (CustCert bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<CustCert> list = dao.find(withOrder);
			for (CustCert bm : list) {
				//bm.setSelected(true);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<CustCert>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			CustCertDAO dao = (CustCertDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			CustCertDAO dao = (CustCertDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((CustCertDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public CustCertDAO getCustomerLocationDAO() {
		return (CustCertDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
