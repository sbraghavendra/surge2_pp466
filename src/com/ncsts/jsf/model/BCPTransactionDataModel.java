package com.ncsts.jsf.model;

import org.apache.log4j.Logger;

import com.ncsts.domain.BCPPurchaseTransaction;

public class BCPTransactionDataModel extends BaseExtendedDataModel<BCPPurchaseTransaction, Long> {
	private static final Logger logger = Logger.getLogger(BCPTransactionDataModel.class);
	
	public BCPTransactionDataModel() {
		setPageSize(25);
		setDbFetchMultiple(10);
	}
	
	public void setFilterCriteria(BCPPurchaseTransaction bcpTransactionDetail) {
		setExampleInstance(bcpTransactionDetail);
	}
}
