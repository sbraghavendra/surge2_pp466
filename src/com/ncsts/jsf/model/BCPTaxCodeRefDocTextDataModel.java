package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPTaxCodeRefDocTextDAO;
import com.ncsts.domain.BCPTaxCodeRefDocText;
import com.ncsts.domain.BCPTaxCodeRefDocTextPK;
import com.ncsts.domain.BatchMaintenance;


public class BCPTaxCodeRefDocTextDataModel extends BaseExtendedDataModel<BCPTaxCodeRefDocText, BCPTaxCodeRefDocTextPK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPTaxCodeRefDocTextDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}

	@Override
	public List<BCPTaxCodeRefDocText> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPTaxCodeRefDocTextDAO dao = (BCPTaxCodeRefDocTextDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPTaxCodeRefDocTextDAO dao = (BCPTaxCodeRefDocTextDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
