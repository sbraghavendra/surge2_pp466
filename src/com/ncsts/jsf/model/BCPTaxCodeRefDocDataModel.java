package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPTaxCodeRefDocDAO;
import com.ncsts.domain.BCPTaxCodeRefDoc;
import com.ncsts.domain.BatchMaintenance;


public class BCPTaxCodeRefDocDataModel extends BaseExtendedDataModel<BCPTaxCodeRefDoc, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPTaxCodeRefDocDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	@Override
	public List<BCPTaxCodeRefDoc> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPTaxCodeRefDocDAO dao = (BCPTaxCodeRefDocDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPTaxCodeRefDocDAO dao = (BCPTaxCodeRefDocDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
