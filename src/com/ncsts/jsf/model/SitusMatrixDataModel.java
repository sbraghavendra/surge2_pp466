package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.SitusMatrixDAO;
import com.ncsts.domain.SitusMatrix;


public class SitusMatrixDataModel extends BaseExtendedDataModel<SitusMatrix, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(SitusMatrixDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public SitusMatrixDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(SitusMatrix situsMatrix) {
		setExampleInstance(situsMatrix);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<SitusMatrix> fetchData(int firstRow, int maxResults) {
		SitusMatrixDAO dao = (SitusMatrixDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<SitusMatrix> list = dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (SitusMatrix bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		/*} 
		else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			//List<SitusMatrix> list = dao.find(withOrder);
			for (SitusMatrix bm : list) {
				//bm.setSelected(true);
				//setDescriptions(bm);
			}
			return list;*/
		} else {
			logger.debug("No criteria set");
			return new ArrayList<SitusMatrix>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			SitusMatrixDAO dao = (SitusMatrixDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		//} else if (criteria != null)  {
		//	SitusMatrixDAO dao = (SitusMatrixDAO) getCommonDAO();
		//	Criteria c = dao.create(criteria);
		//	return ((SitusMatrixDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public SitusMatrixDAO getSitusMatrixDAO() {
		return (SitusMatrixDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
