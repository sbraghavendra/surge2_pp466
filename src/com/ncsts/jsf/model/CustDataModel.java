package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.CustDAO;
import com.ncsts.domain.Cust;


public class CustDataModel extends BaseExtendedDataModel<Cust, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(CustDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public CustDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(Cust cust) {
		setExampleInstance(cust);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<Cust> fetchData(int firstRow, int maxResults) {
		CustDAO dao = (CustDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<Cust> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (Cust bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<Cust> list = dao.find(withOrder);
			for (Cust bm : list) {
				//bm.setSelected(true);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<Cust>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			CustDAO dao = (CustDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			CustDAO dao = (CustDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((CustDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public CustDAO getCustomerLocationDAO() {
		return (CustDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
