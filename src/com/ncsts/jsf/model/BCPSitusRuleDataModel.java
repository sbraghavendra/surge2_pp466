package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPSitusRuleDAO;
import com.ncsts.domain.BCPSitusRule;
import com.ncsts.domain.BatchMaintenance;


public class BCPSitusRuleDataModel extends BaseExtendedDataModel<BCPSitusRule, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPSitusRuleDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	@Override
	public List<BCPSitusRule> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPSitusRuleDAO dao = (BCPSitusRuleDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPSitusRuleDAO dao = (BCPSitusRuleDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
