package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TaxCodeDAO;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;

public class TaxCodeDataModel extends BaseExtendedDataModel<TaxCode, TaxCodePK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(TaxCodeDataModel.class);

	private boolean criteriaSet = false;
	
	

	public TaxCodeDataModel() {

	}

	public void setFilterCriteria(TaxCode taxcode) {
		setExampleInstance(taxcode);
		this.criteriaSet = true;
	}

	public void resetFilterCriteria() {
		this.criteriaSet = false;
	}

	@Override
	protected List<TaxCode> fetchData(int firstRow, int maxResults) {
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			TaxCodeDAO dao = (TaxCodeDAO) getCommonDAO();
			OrderBy orderBy = getOrderBy();
			
			return dao.getTaxCodeList(getExampleInstance(),firstRow, maxResults,orderBy);
			
		}
		
		logger.debug("No criteria set");
		return new ArrayList<TaxCode>();
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		if (criteriaSet) {
			TaxCodeDAO dao = (TaxCodeDAO) getCommonDAO();
			count = dao.count(getExampleInstance());
		}
		return count;
	}
}
