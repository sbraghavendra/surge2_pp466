package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailDAO;
import com.ncsts.domain.PurchaseTransaction;

public class StatisticsDataModel extends BaseExtendedDataModel<PurchaseTransaction,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(StatisticsDataModel.class);
	
	private String sqlWhere;
	private boolean criteriaSet = false;
	private List<String> nullFileds;

	public void setFilterCriteria(PurchaseTransaction transactionDetail,
    		String sqlWhere,List<String> nullFileds) {
		setExampleInstance(transactionDetail);
		this.sqlWhere = sqlWhere;
		this.criteriaSet = true;
		this.nullFileds = nullFileds;
	}

	public void resetFilterCriteria() {
		this.sqlWhere = null;
		this.criteriaSet = false;
		refreshData(false);
	}
	
	OrderBy defaultOrderBy = null;
	
	//default is to order on transaction detail id
	protected OrderBy getDefaultOrderBy() {
		if(defaultOrderBy == null){
				OrderBy o = new OrderBy();
				o.addFieldSortOrder(new FieldSortOrder("transactionDetailId",1,true));
				defaultOrderBy = o;
		}
		return defaultOrderBy;
	}

	
	public OrderBy getSaveAsOrderBy() {
		//we probably shouldn't let "database ordering happen,
		//so if no ordering is picked put in a order by transaction detail id
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
		
    	return orderBy;
	}
	
	@Override
	public List<PurchaseTransaction> fetchData(int firstRow, int maxResults) {
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			return dao.getAllStatisticsRecords(getExampleInstance(), 
					sqlWhere,getOrderBy(), firstRow, maxResults,nullFileds);
		}
		
		logger.debug("No criteria set");
		return new ArrayList<PurchaseTransaction>();
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		if (criteriaSet) {
			TransactionDetailDAO dao = (TransactionDetailDAO) getCommonDAO();
			count = dao.getStatisticsCount(getExampleInstance(),
					sqlWhere,nullFileds);
		}
		return count;
	}
}

