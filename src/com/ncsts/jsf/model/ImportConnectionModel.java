package com.ncsts.jsf.model;

import java.util.List;

import com.ncsts.dao.ImportConnectionDAO;
import com.ncsts.domain.ImportConnection;

public class ImportConnectionModel extends BaseExtendedDataModel<ImportConnection, String> {
	public ImportConnectionModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
		setExampleInstance(new ImportConnection());
	}
	
	public void setFilterCriteria(ImportConnection importConnection) {
		setExampleInstance(importConnection);
		refreshData(false);
	}

	@Override
	protected List<ImportConnection> fetchData(int firstRow, int maxResults) {
		ImportConnectionDAO dao = (ImportConnectionDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		ImportConnectionDAO dao = (ImportConnectionDAO) getCommonDAO();
		count = dao.count(getExampleInstance());
		return count;
	}
}
