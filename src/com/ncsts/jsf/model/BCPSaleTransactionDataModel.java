package com.ncsts.jsf.model;

import org.apache.log4j.Logger;

import com.ncsts.domain.BCPBillTransaction;

public class BCPSaleTransactionDataModel extends BaseExtendedDataModel<BCPBillTransaction, Long> {
	private static final Logger logger = Logger.getLogger(BCPSaleTransactionDataModel.class);
	
	public BCPSaleTransactionDataModel() {
		setPageSize(25);
		setDbFetchMultiple(10);
	}
	
	public void setFilterCriteria(BCPBillTransaction bcpBillTransaction) {
		setExampleInstance(bcpBillTransaction);
	}
}
