package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.dao.BatchErrorDAO;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.ListCodes;
import com.ncsts.dto.ErrorLogDTO;


public class BatchErrorDataModel  extends BaseExtendedDataModel<ErrorLogDTO, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BatchErrorDataModel.class);

	private BatchMaintenance batchMaintenance;
	private CacheManager cacheManager;
	private BatchErrorLog batchErrorLog;
	public BatchErrorDataModel() {
	}

	@Override
	protected List<ErrorLogDTO> fetchData(int firstRow, int maxResults) {
		BatchErrorDAO dao = (BatchErrorDAO) getCommonDAO();
		List<BatchErrorLog> list = dao.getPage(batchErrorLog, firstRow, maxResults);
		
		List<ErrorLogDTO> errorList  = new ArrayList<ErrorLogDTO>();
		for (BatchErrorLog e : list) {
			ErrorLogDTO dto = new ErrorLogDTO();
			BeanUtils.copyProperties(e, dto);
			errorList .add(dto);
		}
		
		for (ErrorLogDTO err : errorList) {
			if (err.getErrorDefCode() != null && err.getErrorDefCode().equalsIgnoreCase("IPXX")) {
  				err.setErrorDefDesc("IPXX");
  				err.setErrorDefExplanation("");
  				err.setSevLevel("30");
  			}
			else{		
	  			String prop = Util.columnToProperty(err.getTransDtlColumnName());
	  			DataDefinitionColumn c = cacheManager.getTransactionPropertyMap().get(prop);
	  			if (c == null) {
	  				err.setTransDtlColumnDesc(err.getTransDtlColumnName());
	  			} else {
	  				err.setTransDtlColumnDesc(c.getDescription());
	  			}
	  			Map<String, ListCodes> map = cacheManager.getListCodeMap().get("ERRORCODE");
	  			if (err.getErrorDefCode() != null) {
	  				ListCodes listCode = map.get(err.getErrorDefCode());
	  				if(listCode!=null){
		  				err.setErrorDefDesc(map.get(err.getErrorDefCode()).getDescription());
		  				err.setErrorDefExplanation(map.get(err.getErrorDefCode()).getExplanation());
		  				err.setSevLevel(map.get(err.getErrorDefCode()).getSeverityLevel());
		  				
		  				map = cacheManager.getListCodeMap().get("ERRORSEV");
			  			if (err.getSevLevel() != null) {
			  				err.setSevLevelDesc(map.get(err.getSevLevel()).getDescription());
			  			}
	  				}
	  				else{
	  					err.setErrorDefDesc("Error code, " + err.getErrorDefCode() + ", not found in database.");
	  				}
	  			}
			}
  		}

		return errorList ;	
	}

	@Override
	public Long count() {
		BatchErrorDAO dao = (BatchErrorDAO) getCommonDAO();
		return dao.count(batchErrorLog);
	}

	public BatchMaintenance getBatchMaintenance() {
		return this.batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		this.batchMaintenance = batchMaintenance;
		refreshData(false);
	}
	
	public BatchErrorDAO getDAO() {
		return (BatchErrorDAO)getCommonDAO();
	}
	
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public BatchErrorLog getBatchErrorLog() {
		return batchErrorLog;
	}

	public void setBatchErrorLog(BatchErrorLog batchErrorLog) {
		this.batchErrorLog = batchErrorLog;
		refreshData(false);
	}
	
}
