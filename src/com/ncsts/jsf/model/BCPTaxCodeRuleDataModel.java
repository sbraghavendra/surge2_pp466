package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPTaxCodeRuleDAO;
import com.ncsts.domain.BCPTaxCodeRule;
import com.ncsts.domain.BatchMaintenance;


public class BCPTaxCodeRuleDataModel extends BaseExtendedDataModel<BCPTaxCodeRule, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPTaxCodeRuleDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	@Override
	public List<BCPTaxCodeRule> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPTaxCodeRuleDAO dao = (BCPTaxCodeRuleDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPTaxCodeRuleDAO dao = (BCPTaxCodeRuleDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
