package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.CustLocnDAO;
import com.ncsts.domain.CustLocn;


public class CustLocnDataModel extends BaseExtendedDataModel<CustLocn, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(CustLocnDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public CustLocnDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(CustLocn custLocn) {
		setExampleInstance(custLocn);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<CustLocn> fetchData(int firstRow, int maxResults) {
		CustLocnDAO dao = (CustLocnDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<CustLocn> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (CustLocn bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<CustLocn> list = dao.find(withOrder);
			for (CustLocn bm : list) {
				//bm.setSelected(true);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<CustLocn>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			CustLocnDAO dao = (CustLocnDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			CustLocnDAO dao = (CustLocnDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((CustLocnDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public CustLocnDAO getCustLocnDAO() {
		return (CustLocnDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
	
	public Boolean getIsNoData() {
		if(count().intValue()==0){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
}
