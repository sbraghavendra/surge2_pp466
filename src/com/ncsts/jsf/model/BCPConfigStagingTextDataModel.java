package com.ncsts.jsf.model;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPConfigStagingTextDAO;
import com.ncsts.domain.BatchMaintenance;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

// Referenced classes of package com.ncsts.jsf.model:
//            BaseExtendedDataModel

public class BCPConfigStagingTextDataModel extends BaseExtendedDataModel
{

    public BCPConfigStagingTextDataModel()
    {
        logger = LoggerFactory.getInstance().getLogger(BCPConfigStagingTextDataModel.class);
    }

    protected List fetchData(int firstRow, int maxResults)
    {
        getSortOrder().put("batchId", "descending");
        logger.debug((new StringBuilder()).append("Fetching data from ").append(firstRow).append(" (").append(maxResults).append(" rows)").toString());
        BCPConfigStagingTextDAO dao = (BCPConfigStagingTextDAO)getCommonDAO();
        return dao.getPage(batchMaintenance, firstRow, maxResults);
    }

    public Long count()
    {
        BCPConfigStagingTextDAO dao = (BCPConfigStagingTextDAO)getCommonDAO();
        return dao.count(batchMaintenance);
    }

    public BatchMaintenance getBatchMaintenance()
    {
        return batchMaintenance;
    }

    public void setBatchMaintenance(BatchMaintenance batchMaintenance)
    {
        if(batchMaintenance != this.batchMaintenance)
        {
            this.batchMaintenance = batchMaintenance;
            refreshData(false);
        }
    }

    public BCPConfigStagingTextDAO getDAO()
    {
        return (BCPConfigStagingTextDAO)getCommonDAO();
    }

    private Logger logger;
    private BatchMaintenance batchMaintenance;
}
