package com.ncsts.jsf.model;



import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.FisYearDAO;
import com.ncsts.domain.FisYear;


public class FisYearDataModel extends BaseExtendedDataModel<FisYear, String> {
	private Logger logger = LoggerFactory.getInstance().getLogger(FisYearDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public FisYearDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}
	
	

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(FisYear fisYear) {
		setExampleInstance(fisYear);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<FisYear> fetchData(int firstRow, int maxResults) {
		FisYearDAO dao = (FisYearDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<FisYear> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<FisYear> list = dao.find(withOrder);
			
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<FisYear>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			FisYearDAO dao = (FisYearDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			FisYearDAO dao = (FisYearDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((FisYearDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public FisYearDAO getProcruleomerLocationDAO() {
		return (FisYearDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
