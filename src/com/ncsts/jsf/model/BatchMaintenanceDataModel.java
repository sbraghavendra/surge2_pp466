package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.ListCodes;


public class BatchMaintenanceDataModel extends BaseExtendedDataModel<BatchMaintenance, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BatchMaintenanceDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public BatchMaintenanceDataModel() {
//		setPageSize(10);	// attempt to speed up paging through transactions
//		setDbFetchMultiple(10);
	}

	
	/**
	 * Select all tax rate update batches ready for processing 
	 */
	public void setSelectAllTaxRateUpdates() {
		BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
  // WHERE (batch_status_code = 'I' Or batch_status_code = 'ITR')
	// AND (held_flag IS NULL Or held_flag <> '1')
		resetFilterCriteria();
		criteria = DetachedCriteria.forClass(BatchMaintenance.class);
		criteria.add(Expression.or(Expression.isNull("heldFlag"), 
				Expression.ne("heldFlag", "1")));
		/*criteria.add(Expression.or(Expression.eq("batchStatusCode", "I"),
				Expression.eq("batchStatusCode", "ITR")));*/
		//6042
		criteria.add(Expression.eq("batchStatusCode", "I"));
		criteria.add(Expression.eq("batchTypeCode", "TR"));
		setCriteria(criteria);
	}
	
	public void setSelectAllTaxCodeRulesUpdates() {
		BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
		resetFilterCriteria();
		criteria = DetachedCriteria.forClass(BatchMaintenance.class);
		criteria.add(Expression.or(Expression.isNull("heldFlag"), 
				Expression.ne("heldFlag", "1")));
		//6042
		/*criteria.add(Expression.or(Expression.eq("batchStatusCode", "I"),
				Expression.eq("batchStatusCode", "ITCR")));*/
		criteria.add(Expression.eq("batchStatusCode", "I"));
		criteria.add(Expression.eq("batchTypeCode", "TCR"));
		setCriteria(criteria);
	}
	
	public void setSelectAllSitusRulesUpdates() {
		BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
		resetFilterCriteria();
		criteria = DetachedCriteria.forClass(BatchMaintenance.class);
		criteria.add(Expression.or(Expression.isNull("heldFlag"),
				Expression.ne("heldFlag", "1")));
		// 6042
		/*
		 * criteria.add(Expression.or(Expression.eq("batchStatusCode", "I"),
		 * Expression.eq("batchStatusCode", "ISDM")));
		 */
		criteria.add(Expression.eq("batchStatusCode", "I"));
		criteria.add(Expression.eq("batchTypeCode", "SDM"));
		setCriteria(criteria);
	}
	
	public void setSelectAllCustLocnUpdates() {
		BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
		resetFilterCriteria();
		criteria = DetachedCriteria.forClass(BatchMaintenance.class);
		criteria.add(Expression.or(Expression.isNull("heldFlag"), 
				Expression.ne("heldFlag", "1")));
		criteria.add(Expression.or(Expression.eq("batchStatusCode", "I"),
				Expression.eq("batchStatusCode", "ICL")));
		criteria.add(Expression.eq("batchTypeCode", "CL"));
		setCriteria(criteria);
	}
	public void setSelectAllImportConfigs()
    {
        BatchMaintenanceDAO dao = (BatchMaintenanceDAO)getCommonDAO();
        resetFilterCriteria();
        criteria = DetachedCriteria.forClass(BatchMaintenance.class);
        criteria.add(Expression.or(Expression.isNull("heldFlag"), Expression.ne("heldFlag", "1")));
        criteria.add(Expression.eq("batchStatusCode", "I"));
        criteria.add(Expression.eq("batchTypeCode", "CI"));
        setCriteria(criteria);
    }

	
	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(BatchMaintenance batchMaintenance) {
		setExampleInstance(batchMaintenance);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<BatchMaintenance> fetchData(int firstRow, int maxResults) {
		BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<BatchMaintenance> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (BatchMaintenance bm : list) {
				setDescriptions(bm);
			}
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<BatchMaintenance> list = dao.find(withOrder);
			for (BatchMaintenance bm : list) {
				bm.setSelected(true);
				setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<BatchMaintenance>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			BatchMaintenanceDAO dao = (BatchMaintenanceDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((BatchMaintenanceDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	/**
	 * Map descriptions from code, ignore anything that doesn't exist
	 * @param bm
	 */
	private void setDescriptions(BatchMaintenance bm) {try
    {
        bm.setBatchStatusDesc(((ListCodes)((Map)cacheManager.getListCodeMap().get("BATCHSTAT")).get(bm.getBatchStatusCode())).getDescription());
    }
    catch(Exception e) { }
    try
    {
        bm.setBatchTypeDesc(((ListCodes)((Map)cacheManager.getListCodeMap().get("BATCHTYPE")).get(bm.getBatchTypeCode())).getDescription());
        try
        {
            if(bm.getBatchTypeCode().equals("CI"))
                bm.setVc01Desc(((ListCodes)((Map)cacheManager.getListCodeMap().get("CONFIGTYPE")).get(bm.getVc01())).getDescription());
        }
        catch(Exception e) { }
    }
    catch(Exception e) { }}

	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return (BatchMaintenanceDAO)getCommonDAO();
	}


	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
