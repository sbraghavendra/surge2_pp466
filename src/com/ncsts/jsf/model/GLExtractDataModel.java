package com.ncsts.jsf.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.PurchaseTransactionLogDAO;
import com.ncsts.domain.PurchaseTransactionLog;

public class GLExtractDataModel extends BaseExtendedDataModel<PurchaseTransactionLog,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(GLExtractDataModel.class);
	
	private String sqlWhere;
	private boolean criteriaSet = false;
	private List<String> nullFileds;

	public void setFilterCriteria(PurchaseTransactionLog transactionDetailLog,
    		String sqlWhere,List<String> nullFileds) {
		setExampleInstance(transactionDetailLog);
		this.sqlWhere = sqlWhere;
		this.criteriaSet = true;
		this.nullFileds = nullFileds;
	}

	public void resetFilterCriteria() {
		this.sqlWhere = null;
		this.criteriaSet = false;
		refreshData(false);
	}
	
	OrderBy defaultOrderBy = null;
	
	//default is to order on purchtransId
	protected OrderBy getDefaultOrderBy() {
		if(defaultOrderBy == null){
				OrderBy o = new OrderBy();
				o.addFieldSortOrder(new FieldSortOrder("purchtransId",1,true));
				defaultOrderBy = o;
		}
		return defaultOrderBy;
	}

	
	public OrderBy getSaveAsOrderBy() {
		//we probably shouldn't let "database ordering happen,
		//so if no ordering is picked put in a order by purchtrans id
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
		
    	return orderBy;
	}
	
	@Override
	public List<PurchaseTransactionLog> fetchData(int firstRow, int maxResults) {
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			PurchaseTransactionLogDAO dao = (PurchaseTransactionLogDAO) getCommonDAO();
			
			List<PurchaseTransactionLog> list = dao.getAllGLTransactions(getExampleInstance(), 
					sqlWhere,getOrderBy(), firstRow, maxResults,nullFileds);
			return list;
		}
		
		logger.debug("No criteria set");
		return new ArrayList<PurchaseTransactionLog>();
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		if (criteriaSet) {
			PurchaseTransactionLogDAO dao = (PurchaseTransactionLogDAO) getCommonDAO();
			count = dao.getTransactionCount(getExampleInstance(),
					sqlWhere,nullFileds);
		}
		return count;
	}
	
	@Override
	public Map<String,String> createSortOrder(){
    	Map<String,String> localSortOrder = new LinkedHashMap<String,String>();
    	Class<?> purchtrans = commonDAO.getObjectClass();
    	
    	 String name = null;

    	    for (Field f : purchtrans.getDeclaredFields()) {
    	        Column column = null;
    	        Annotation[] annotationArray = f.getAnnotations();
    	        for (Annotation a : annotationArray) {
    	            if (a.annotationType() == Column.class) 
    	                column = (Column) a;
    	        }
    	        if (column != null){
    	            name = column.name();
    	            localSortOrder.put(name, SORT_NONE);
    	        }
    	    }
    	    localSortOrder.put("TB_DELTA_TAX_AMT", SORT_NONE);
    	return localSortOrder;
    }
}

