package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.TaxAllocationMatrixDAO;
import com.ncsts.dao.TaxMatrixDAO;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxMatrix;

public class TaxAllocationMatrixDataModel extends BaseExtendedDataModel<TaxAllocationMatrix,Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(TaxAllocationMatrixDataModel.class);
	
	public void setFilterCriteria(TaxAllocationMatrix taxAllocationMatrix) {
		setExampleInstance(taxAllocationMatrix);
	}
	
	@Override
	protected List<TaxAllocationMatrix> fetchData(int firstRow, int maxResults) {
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		
		TaxAllocationMatrixDAO dao = (TaxAllocationMatrixDAO) getCommonDAO();	
		return dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		TaxAllocationMatrixDAO dao = (TaxAllocationMatrixDAO) getCommonDAO();
		return dao.count(this.getExampleInstance());
	}
}

