package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPTaxCodeRuleTextDAO;
import com.ncsts.domain.BCPTaxCodeRuleText;
import com.ncsts.domain.BCPTaxCodeRuleTextPK;
import com.ncsts.domain.BatchMaintenance;


public class BCPTaxCodeRuleTextDataModel extends BaseExtendedDataModel<BCPTaxCodeRuleText, BCPTaxCodeRuleTextPK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPTaxCodeRuleTextDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}

	@Override
	public List<BCPTaxCodeRuleText> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPTaxCodeRuleTextDAO dao = (BCPTaxCodeRuleTextDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPTaxCodeRuleTextDAO dao = (BCPTaxCodeRuleTextDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
