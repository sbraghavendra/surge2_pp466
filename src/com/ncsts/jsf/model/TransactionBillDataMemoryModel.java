package com.ncsts.jsf.model;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.Range;
import org.ajax4jsf.model.SequenceRange;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.TransactionDetailBillDAO;
import com.ncsts.domain.BillTransaction;
import com.seconddecimal.billing.domain.SaleTransaction;

public class TransactionBillDataMemoryModel extends BaseExtendedDataModel<BillTransaction, Long> {
	private List<BillTransaction> billTransactions;
	
	
	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionBillDataMemoryModel.class);
	OrderBy defaultOrderBy = null;

	protected OrderBy getDefaultOrderBy() {
		if(defaultOrderBy == null){
				OrderBy o = new OrderBy();
				o.addFieldSortOrder(new FieldSortOrder("billtransId",1,true));
				defaultOrderBy = o;
		}
		return defaultOrderBy;
}
	@Override
	protected List<BillTransaction> fetchData(int firstRow, int maxResults) {
		logger.info("enter TransactionBillDataMemoryModel.fetchData()");
		
		 
		
		
		return this.billTransactions;
	}

	@Override
	protected Long count() {
		return this.billTransactions == null ? 0L : (long) this.billTransactions.size();
	}

	public List<BillTransaction> getBillTransactions() {
		return billTransactions;
	}

	public void setBillTransactions(List<BillTransaction> BillTransactions) {
		this.billTransactions = BillTransactions;
	}

	@Override
	public void tooggleSelectAll(boolean value, int maxToFetch) {
		if(value) {
			getSelectionMap().clear();
			for(BillTransaction t : this.billTransactions) {
				getSelectionMap().put(t.getId(), Boolean.TRUE);
			}
		}
		else {
			getSelectionMap().clear();
		}
	}
	
	@Override
    public void toggleSortOrder(String field) {
    	Map<String,String> sortOrder = getSortOrder();
    	processToggleSortOrder(sortOrder, field);
		
		// Refresh data, but we can keep the count
		refreshDataTest(false);
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
			
		if(orderBy != null && billTransactions != null && billTransactions.size() > 1) {
				final String fieldName = orderBy.getFields().get(0).getName();
				final boolean ascendingOrder = orderBy.getFields().get(0).getAscending();
				Collections.sort(billTransactions, new Comparator<BillTransaction>() {
				
				@Override
				public int compare(BillTransaction b1, BillTransaction b2 ) {
					try {
						DecimalFormat deciFormat = new DecimalFormat();
						SimpleDateFormat format = new SimpleDateFormat("yyy-mm-dd");
						deciFormat.setParseBigDecimal(true);
						Object o1 = BeanUtils.getProperty(b1, fieldName);
						Object o2 = BeanUtils.getProperty(b2, fieldName);
						if(ascendingOrder) {
							if(o1 == null)
								return -1;
							if(o2 == null)
								return 1;
							
							try {
								if(format.parse(o1.toString()) != null){
									return (format.parse(o1.toString()).compareTo(format.parse(o2.toString())));
								}
							} catch(ParseException e) { }
							
							if(deciFormat.parse((String) o1) != null)
							{
								return (new BigDecimal(o1.toString())).compareTo(new BigDecimal(o2.toString()));
							}
							else {
								return ((String)o1).compareTo((String)o2);
							}
						} else {
							if(o1 == null)
								return 1;
							if(o2 == null)
								return -1;
							
							try {
								if(format.parse(o1.toString()) != null){
									return (format.parse(o2.toString()).compareTo(format.parse(o1.toString())));
								}
							} catch (ParseException e) { }
							
							if(deciFormat.parse((String) o1) != null)
							{
								return (new BigDecimal(o2.toString())).compareTo(new BigDecimal(o1.toString()));
							}
							else {
								return ((String)o2).compareTo((String)o1);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				}
			});
			
		}
    }
}