package com.ncsts.jsf.model;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.Range;
import org.ajax4jsf.model.SequenceRange;
import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.GenericDAO;
import com.ncsts.domain.Idable;

/**
 * 
 * @author ias
 * This is example class that intended to demonstrate use of ExtendedDataModel and SerializableDataModel.
 * This implementation intended to be used as a request scope bean. However, it actually provides serialized
 * state, so on a post-back we do not load data from the data provider. Instead we use data that was used 
 * during rendering.
 * This data model must be used together with Data Provider, which is responsible for actual data load 
 * from the database using specific filtering and sorting. Normally Data Provider must be in either session, or conversation
 * scope.
 */
public class BaseExtendedDataModel<T extends Idable<ID>, ID extends Serializable> extends org.ajax4jsf.model.ExtendedDataModel {

	private Logger logger = LoggerFactory.getInstance().getLogger(BaseExtendedDataModel.class);
	
	private class CacheEntry {
		private int firstRow;
		private int count;
		private List<T> data;
		private Date timestamp;
		
		public CacheEntry(int firstRow, int count, List<T> data) {
			this.firstRow = firstRow;
			this.count = count;
			this.data = data;
			this.timestamp = new Date();
		}
		
		public void clear() {
			data.clear();
			data = null;
			firstRow = -1;
			count = 0;
		}
		
		public void setData(List<T> data) {
			this.data = data;
		}
		
		public boolean update(T item) {
			if (item == null) {
				logger.error("Null item update");
				return false;
			}
			ID id = item.getId();
			for (int i = 0; i < data.size(); i++) {
				if (data.get(i).getId().equals(id)) {
					data.set(i, item);
					return true;
				}
			}
			return false;
		}
		
		public List<T> getData(int firstRow, int maxResults) {
			List<T> result = null;
			if ((firstRow >= this.firstRow) &&
				(firstRow + maxResults) <= (this.firstRow + this.count)) {
				// Got a hit, update timestamp
				timestamp = new Date();
				
				int start = firstRow - this.firstRow;
				
				if (start > data.size()) {
					// In our cache, but no data exists
					return new ArrayList<T>();
				}

				// Constrain result to actual range of data
				int end = Math.min(start+maxResults, data.size());
				result = data.subList(start, end);
				logger.debug("Cache hit from " + firstRow + " for " + maxResults + " rows.  Returning " + result.size() + " rows");
			}
			
			return result;
		}
		
		public T findItem(ID id) {
			for (T row : data) {
				if (row.getId().equals(id)) {
					return row;
				}
			}
			return null;
		}
		
		/*
		public List<ID> getKeys(int firstRow, int maxResults) {
			if ((firstRow >= this.firstRow) &&
				(firstRow + maxResults) <= (this.firstRow + this.count)) {
				int offset = firstRow - this.firstRow;
				return keys.subList(offset, offset+maxResults);
			}
			return null;
		}
		*/
	}
	
	private class Cache {
		private List<CacheEntry> cache = new ArrayList<CacheEntry>();
		
		public void clear() {
			for (CacheEntry e : cache) {
				e.clear();
			}
			cache.clear();
		}
		
		public CacheEntry add(int firstRow, int maxResults, List<T> data) {
			CacheEntry entry = new CacheEntry(firstRow, maxResults, data);
			cache.add(entry);
			return entry;
		}
		
		public void update(Collection<T> data) {
			for (T item : data) {
				update(item);
			}
		}
		
		public void update (T item) {
			for (CacheEntry e : cache) {
				if (e.update(item)) {
					break;
				}
			}
		}

		public List<T> getData(int firstRow, int maxResults) {
			List<T> result = null;
			for (CacheEntry e : cache) {
				result = e.getData(firstRow, maxResults);
				if (result != null) {
					break;
				}
			}
			return result;
		}
		
		public T findItem(ID id) {
			T result = null;
			for (CacheEntry e : cache) {
				result = e.findItem(id);
				if (result != null) {
					break;
				}
			}
			return result;
		}
	}

	public final static String SORT_NONE = "none";
	public final static String SORT_ASCENDING = "ascending";
	public final static String SORT_DESCENDING = "descending";
	
    protected GenericDAO<T,ID> commonDAO; 
    private String[] excludes;
    private T exampleInstance;
	private int curPage = 1;
	private int pageSize = 50;

	private Map<String,String> sortOrder;
	private Map<String,String> sortDetailOrder;
	
    private SelectionMap<ID> selectionMap = new SelectionMap<ID>();

    private List<T> listRow;
	
	private ID currentId;
	private Map<ID, T> wrappedData = new HashMap<ID, T>();
	private List<ID> wrappedKeys;
	private Integer rowCount; // better to buffer row count locally
	
	private int cacheFirstRow = -1;
	private int cacheMaxResults = -1;

	private Cache cache = new Cache();
	private int dbFetchMultiple = 1;
	
	public BaseExtendedDataModel() {
    }
    
    public Map<String,String> getSortOrder() {
    	if (sortOrder == null) {
        	sortOrder = createSortOrder();
    	}
    	return sortOrder;
    }
    
    public Map<String,String> getSortDetailOrder() {
    	if (sortDetailOrder == null) {
        	sortDetailOrder = createSortOrder();
    	}
    	return sortDetailOrder;
    }
    
    protected Map<String,String> createSortOrder(){
    	//Creating a Map That Retains Order-of-Insertion
    	Map<String,String> localSortOrder = new LinkedHashMap<String,String>();
    	for (Method m : commonDAO.getObjectClass().getMethods()) {
    		if (m.getName().startsWith("get")) {
    			String field = Util.lowerCaseFirst(m.getName().substring(3));
    			localSortOrder.put(field, SORT_NONE);
    		}
    	}
    	return localSortOrder;
    }
    
    public void setDefaultSortOrder(String field, String order) {
    	for (String value: getSortOrder().values()) {
    		if (! SORT_NONE.equalsIgnoreCase(value)) return;
    	}
    	getSortOrder().put(field, order);
    }

    public void toggleSortOrder(String field) {
    	Map<String,String> sortOrder = getSortOrder();
    	processToggleSortOrder(sortOrder, field);
		
		// Refresh data, but we can keep the count
		refreshData(false);
    }
    
    public void resetSortOrder() {
    	Map<String,String> resetOrder = getSortOrder();	
    	for (String key : resetOrder.keySet()) {
			resetOrder.put(key, SORT_NONE);
		}

		// Refresh data, but we can keep the count
		refreshData(true);
    }
    
    public void toggleSortDetailOrder(String field) {
    	Map<String,String> sortDetailOrder = getSortDetailOrder();
    	processToggleSortOrder(sortDetailOrder, field);
    }
    
    protected void processToggleSortOrder(Map<String,String> localSortOrder, String field) {
		if (localSortOrder.containsKey(field) && SORT_ASCENDING.equals(localSortOrder.get(field))) {
			localSortOrder.put(field, SORT_DESCENDING);
		} else {
			localSortOrder.put(field, SORT_ASCENDING);
		}
		System.out.println(localSortOrder.containsKey(field));
		
		// Only single column sort (for now) so we
		// set all the others back to none
		for (String key : localSortOrder.keySet()) {
			if (!key.equals(field)) {
				localSortOrder.put(key, SORT_NONE);
			}
			else
				System.out.println("Found key" + key);
		}
    }
    
	public void wrap(FacesContext context, DataVisitor visitor, Range range, Object argument, List<T> list) throws IOException {
		wrappedKeys = new ArrayList<ID>();
		wrappedData = new HashMap<ID, T>();
		for (T row : list) {
			ID id = row.getId();
			wrappedKeys.add(id);
			wrappedData.put(id, row);
			visitor.process(context, id, argument);
		}	
			
	}
	
	public Iterator<T> iterator() {
		return wrappedData.values().iterator();
	}
	
	public Set<ID> keySet() {
		return wrappedData.keySet();
	}	
	public boolean hasById(ID id)  {
		return wrappedData.containsKey(id);
	}
	
	@Override
	public void walk(FacesContext context, DataVisitor visitor, Range range, Object argument) throws IOException {		
		int firstRow = ((SequenceRange) range).getFirstRow();
		int maxResults = ((SequenceRange) range).getRows();
		if ((listRow != null) && (firstRow == cacheFirstRow) && ((maxResults == cacheMaxResults) || (maxResults == -1))) {
			//check for ConcurrentModificationException
			//0001955: Column Subtotals crashes after many tests
			try {
				for (ID id : wrappedKeys) {
					visitor.process(context, id, argument);
				}
			}
			catch(ConcurrentModificationException cme) {
				logger.error(cme.getMessage(), cme);
			}
		} else if (maxResults > 0) {
			// Check if we have it in the DB cache
			listRow = cache.getData(firstRow, maxResults);
			
			if (listRow == null) {
				// Normalize to DB cache pages
				int dbFetchSize = pageSize * dbFetchMultiple;
				int startRow = ((firstRow / dbFetchSize) * dbFetchSize);
				
				List<T> fetch = fetchData(startRow, dbFetchSize); 
				logger.debug("fetchData from " + startRow + " for " + dbFetchSize + " rows.  Returned " + ((fetch == null)? "NULL":(fetch.size() + " rows")));
				// sometimes this is resulting in a null, create empty list to be safe...
				if (fetch == null) fetch = new ArrayList<T>();
				listRow = cache.add(startRow, dbFetchSize, fetch).getData(firstRow, maxResults);
			}
			
			cacheFirstRow = firstRow;
			cacheMaxResults = maxResults;
			wrap(context, visitor, range, argument, listRow);
		}
	}
	
	public OrderBy getOrderBy() {
    	Map<String,String> sortOrder = getSortOrder();
    	return createOrderBy(sortOrder);
    	
	}
	
	public OrderBy getDetailOrderBy() {
    	Map<String,String> sortDetailOrder = getSortDetailOrder();
		return createOrderBy(sortDetailOrder);
	}
	
	private OrderBy createOrderBy(Map<String,String> localSortOrder){
		// Build sorting expression
		OrderBy orderBy = new OrderBy();
		int i = 1;
		for (String field : localSortOrder.keySet()) {
			if (SORT_ASCENDING.equals(localSortOrder.get(field))) {
				orderBy.getFields().add(new FieldSortOrder(field, i++, true));
			} else if (SORT_DESCENDING.equals(localSortOrder.get(field))) {
				orderBy.getFields().add(new FieldSortOrder(field, i++, false));
			}
		}
		
		return orderBy;
	}
	
	// Override in derived classes to implement custom filtering 
	protected List<T> fetchData(int firstRow, int maxResults) {
		return commonDAO.getPage(firstRow, firstRow+maxResults, getOrderBy(), getExampleInstance(), getExcludes());
	}
	
	// Override in derived classes to implement custom filtering 
	protected Long count() {
		logger.info("enter/exit BaseExtendedDataModel.count()");
		return this.commonDAO.count(this.getExampleInstance(), this.getExcludes());
	}
	
    /*
	 * This method normally called by Visitor before request Data Row.
	 */ 
	@SuppressWarnings("unchecked")
	@Override
	public void setRowKey(Object key) {
		this.currentId = (ID) key;
	}

	@Override 
	public Object getRowKey() { 
		return currentId; 
	}

	@Override
	public int getRowCount() {
		logger.info("enter getRowCount()");
        if (rowCount == null) {
            rowCount = count().intValue(); 
            logger.debug("Count: " + rowCount);
    		logger.info("exit getRowCount()");            
            return rowCount.intValue();
        } else {
    		logger.info("exit getRowCount()");        	
            return rowCount.intValue();
        }
	}
	
	@Override
	public boolean isRowAvailable() {
		if (currentId == null) {
			return false;
		} else {
			return hasById(currentId);
		}
	}
	
	/**
	 * This is main way to obtain data row. It is intensively used by framework.
	 * We strongly recommend use of local cache in that method.
	 */
	@Override
	public Object getRowData() {
		if (currentId == null) {
			return null;
		} else {
			return getById(currentId);
		}
	}
	
	@Override 
	public int getRowIndex() {
		if (currentId == null) {
			return -1;
		} else {
			return wrappedKeys.indexOf(currentId);
		}
	}
	
	@Override 
	public void setRowIndex(int rowIndex) {
		currentId = wrappedKeys.get(rowIndex);
	}
	
	public ID getRowID(int rowIndex) {
		return wrappedKeys.get(rowIndex);
	}
	
	/**
	 * Unused rudiment from old JSF staff. 
	 */
	@Override public Object getWrappedData() { throw new UnsupportedOperationException(); }
	@Override public void setWrappedData(Object data) { throw new UnsupportedOperationException(); }
	
	/**
	 * Unused update data. 
	 */
//	@Override public void update() { throw new UnsupportedOperationException(); }

	public T getById(ID id) {
		T ret = wrappedData.get(id);
		if ((ret == null) && (id != null)) {
			// Look in cache before going to DB
			ret = cache.findItem(id);
			if (ret == null) {
				ret = this.commonDAO.findById(id);
			}
			wrappedData.put(id, ret);
			return ret;
		} else {
			return ret;
		}
	}

	public T getExampleInstance() {
        return exampleInstance;
    }

    /**
     * An example instance that will be used to define the results.
     * @param exampleInstance
     */
    public void setExampleInstance(T exampleInstance) {
        this.exampleInstance = exampleInstance;
        refreshData(false);
    }

    public String[] getExcludes() {
        return excludes;
    }

    /**
     * Excludes that will be applied to the fetch to filter the results.
     * @param excludes
     */
    public void setExcludes(String[] excludes) {
        this.excludes = excludes;
        refreshData(false);
    }

	public Map<ID, Boolean> getSelectionMap() {
		return selectionMap;
	}

	public void setSelectionMap(Map<ID, Boolean> selectionMap) {
		this.selectionMap.clear();
		this.selectionMap.putAll(selectionMap);
	}
	
	public void refreshSelection() {
		// Check if there is a selection
		if (selectionMap.size() > 0) {
			List<T> data = commonDAO.findById(selectionMap.keySet());
			// Update the cache
			cache.update(data);
			
			// Check if any items are "in view", if so, we need to refresh current page
			for (T row : data) {
				if (wrappedData.containsKey(row.getId())) {
					logger.debug("Forcing refresh of page in view");
					listRow = null;		// Force current page to refetch from cache
					break;
				}
			}
		}
	}
	
	public void refreshItem(ID id) {
		cache.update(commonDAO.findById(id));
		if (wrappedData.containsKey(id)) {
			logger.debug("Forcing refresh of page in view");
			listRow = null;		// Force current page to refetch from cache
		}
	}
	
	public void refreshData(boolean retainCount) {
		// Eliminate cached data
		cache.clear();
		wrappedData = new HashMap<ID, T>();
		wrappedKeys = null;
		selectionMap.clear();
        listRow = null;
        curPage = 1;
        if (!retainCount) {
        	rowCount = null;
        }
	}
	
	public void refreshDataTest(boolean retainCount) {
		wrappedData = new HashMap<ID, T>();
		wrappedKeys = null;
		selectionMap.clear();
        listRow = null;
        curPage = 1;
        if (!retainCount) {
        	rowCount = null;
        }
	}

	private int getFirstRow() {
		return Math.max(0, Math.min(getRowCount(), ((curPage - 1) * pageSize) + 1));
	}

	private int getLastRow() {
		return Math.min(getRowCount(), (curPage * pageSize));
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getDbFetchMultiple() {
		return dbFetchMultiple;
	}

	public void setDbFetchMultiple(int dbFetchMultiple) {
		logger.info("enter setDbFetchMultiple()");
		this.dbFetchMultiple = dbFetchMultiple;
		logger.info("exit setDbFetchMultiple()");		
	}

	public String getPageDescription() {
		logger.info("enter getPageDescription()");		
		logger.debug("curPage: " + curPage);
		logger.debug("pageSize: " + pageSize);
		logger.info("exit getPageDescription()");			
		return String.format("Displaying rows %d through %d (%d matching rows)",
				getFirstRow(), getLastRow(), getRowCount());
	}
	
	public List<T> getItems(Collection<ID> ids) {
		List<T> result = new ArrayList<T>();
		for (ID id : ids) {
			result.add(getById(id));
		}
		return result;
	}

	public GenericDAO<T, ID> getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(GenericDAO<T, ID> commonDAO) {
		this.commonDAO = commonDAO;
	}
	
	//support for the "Select All" checkbox...
	//TRUE requires us to get all the IDs and add them into the selectionMap
	// --actually I (kirk) don't 100% know what I'm doing here :-)
	//  it seems like normally data is loaded into the cache when an item is selected individually
	//  so if we're selecting all without putting stuff into cache
	//   pretty soon all those items will be queried (possibly for something like getUpdateDisabled)
	//   one at a time with a very painful server round trip - so I just copied and pasted code from
	//   above - hopefully using 0 to the result of getRowCount for everything is ok 
	//FALSE is easier, just empty the selectionMap
	public void tooggleSelectAll(boolean value, int maxToFetch){
		if(value == true) {
			int fullRowCount = getRowCount();
			int howManyToFetch = Math.min(fullRowCount,maxToFetch);
			
			List<T> fetch = fetchData(0,howManyToFetch);
			// sometimes this is resulting in a null, create empty list to be safe...
			if (fetch == null) fetch = new ArrayList<T>();
			listRow = cache.add(0, howManyToFetch, fetch).getData(0, howManyToFetch);
			//now do the actual inclusion in the selection map
			for(T item : fetch){
				this.selectionMap.put(item.getId(), true);
			}
		} else { //unclear everything
			this.selectionMap.clear();
		}
		
	}

}

