package com.ncsts.jsf.model;

import java.util.List;
import com.ncsts.dao.ImportDefinitionDAO;
import com.ncsts.domain.ImportConnection;
import com.ncsts.domain.ImportDefinition;

public class ImportDefinitionModel extends BaseExtendedDataModel<ImportDefinition, String> {
	public ImportDefinitionModel() {
		setPageSize(25);	// attempt to speed up paging through transactions
		setDbFetchMultiple(10);
		setExampleInstance(new ImportDefinition());
	}
	
	public void setFilterCriteria(ImportDefinition importDefinition) {
		setExampleInstance(importDefinition);
		refreshData(false);
	}

	@Override
	protected List<ImportDefinition> fetchData(int firstRow, int maxResults) {
		ImportDefinitionDAO dao = (ImportDefinitionDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		ImportDefinitionDAO dao = (ImportDefinitionDAO) getCommonDAO();
		count = dao.count(getExampleInstance());
		return count;
	}
}
