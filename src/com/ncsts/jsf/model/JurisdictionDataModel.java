package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.JurisdictionDAO;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;

public class JurisdictionDataModel extends BaseExtendedDataModel<Jurisdiction, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(JurisdictionDataModel.class);

	private JurisdictionTaxrate jurisdictionTaxrate;
	private boolean criteriaSet = false;

	public JurisdictionDataModel() {
//		setPageSize(10);	// attempt to speed up paging through transactions
//		setDbFetchMultiple(10);
	}

	public void setFilterCriteria(Jurisdiction jurisdiction, JurisdictionTaxrate jurisdictionTaxrate) {
		setExampleInstance(jurisdiction);
		this.jurisdictionTaxrate = jurisdictionTaxrate;
		this.criteriaSet = true;
	}

	public void resetFilterCriteria() {
		this.jurisdictionTaxrate = null;
		this.criteriaSet = false;
	}

	@Override
	protected List<Jurisdiction> fetchData(int firstRow, int maxResults) {
		if (criteriaSet) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			JurisdictionDAO dao = (JurisdictionDAO) getCommonDAO();
			return dao.getAllRecords(getExampleInstance(), jurisdictionTaxrate, getOrderBy(), firstRow, maxResults);
		}
		
		logger.debug("No criteria set");
		return new ArrayList<Jurisdiction>();
	}

	@Override
	public Long count() {
		Long count = 0L;
		if (criteriaSet) {
			JurisdictionDAO dao = (JurisdictionDAO) getCommonDAO();
			count = dao.count(getExampleInstance(), jurisdictionTaxrate);
		}
		return count;
	}
}
