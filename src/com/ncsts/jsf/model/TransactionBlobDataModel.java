package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.TransactionBlobDAO;
import com.ncsts.domain.TransactionBlob;


public class TransactionBlobDataModel extends BaseExtendedDataModel<TransactionBlob, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionBlobDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public TransactionBlobDataModel() {
	}
	
	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(TransactionBlob transactionBlob) {
		setExampleInstance(transactionBlob);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<TransactionBlob> fetchData(int firstRow, int maxResults) {
		TransactionBlobDAO dao = (TransactionBlobDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			
			
			
			System.out.println(" xxxxxxxxxxxxxxxxxxx" + "Fetching data from " + firstRow + " (" + maxResults + " rows)");
			
			List<TransactionBlob> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			for (TransactionBlob bm : list) {
				//bm.setSelected(false);
			
			}
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<TransactionBlob> list = dao.find(withOrder);
			for (TransactionBlob bm : list) {
				//bm.setSelected(true);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<TransactionBlob>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			TransactionBlobDAO dao = (TransactionBlobDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			TransactionBlobDAO dao = (TransactionBlobDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((TransactionBlobDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}


	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public TransactionBlobDAO getTransactionBlobDAO() {
		return (TransactionBlobDAO)getCommonDAO();
	}


	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
