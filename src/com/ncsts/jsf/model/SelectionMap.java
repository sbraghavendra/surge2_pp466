package com.ncsts.jsf.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class SelectionMap<ID> implements Map<ID,Boolean> {

	private Set<ID> selection = new LinkedHashSet<ID>(); //a linked hashset should let us preserve insertion order, kind of a half-assed solution for 3922
	
	public void clear() {
		selection.clear();
	}

	// We pretend to contain all keys - not that everything is selected (i.e. true) 
	// but we pretend to "know" about whether its selected or not
	public boolean containsKey(Object key) {
		return true;
	}

	public boolean containsValue(Object value) {
		// Nonsense method for our purposes
		return true;
	}

	public Set<Map.Entry<ID, Boolean>> entrySet() {
		Map<ID,Boolean> map = new LinkedHashMap<ID,Boolean>();
		for (ID key : selection) {
			map.put(key, true);
		}
		return map.entrySet();
	}

	public Boolean get(Object key) {
		return selection.contains(key)? true:false;
	}

	public boolean isEmpty() {
		return selection.isEmpty();
	}

	public Set<ID> keySet() {
		Set<ID> result = new LinkedHashSet<ID>();
		result.addAll(selection);
		return result;
	}

	public Boolean put(ID key, Boolean value) {
		Boolean result = get(key);
		if ((value == null) || (value == false)) {
			selection.remove(key);
		} else {
			selection.add(key);
		}
		return result;
	}

	public void putAll(Map<? extends ID, ? extends Boolean> t) {
		for (ID key : t.keySet()) {
			if (t.get(key).equals(true)) {
				put(key, t.get(key));
			}
		}
	}

	public Boolean remove(Object key) {
		Boolean result = get(key);
		selection.remove(key); 
		return result;
	}

	public int size() {
		return selection.size();
	}

	public Collection<Boolean> values() {
		// Nonsense method for us
		return null;
	}
}
