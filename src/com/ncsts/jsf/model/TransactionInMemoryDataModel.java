package com.ncsts.jsf.model;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.Range;

import com.ncsts.common.Util;
import com.ncsts.domain.Idable;

public class TransactionInMemoryDataModel<T extends Idable<ID>, ID extends Serializable>
		extends BaseExtendedDataModel<T, ID> {
	private List<T> data = null;
	private Map<String, String> sortOrder;
	private Class<T> persistenceClass;

	private List<T> listRow;
	
	public TransactionInMemoryDataModel(Class<?> clazz) {
		persistenceClass = (Class<T>) clazz;
	}

	@Override
	protected List<T> fetchData(int firstRow, int maxResults) {
		return this.data;
	}

	@Override
	protected Long count() {
		return data == null ? 0L : data.size();
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public Map<String, String> getSortOrder() {
		if (sortOrder == null) {
			sortOrder = createSortOrder();
		}

		return sortOrder;
	}

	protected Map<String, String> createSortOrder() {		Map<String, String> localSortOrder = new LinkedHashMap<String, String>();
		for (Method m : persistenceClass.getMethods()) {
			if (m.getName().startsWith("get")) {
				String field = Util.lowerCaseFirst(m.getName().substring(3));
				localSortOrder.put(field, SORT_NONE);
			}
		}
		return localSortOrder;
	}
	
	@Override
	public void walk(FacesContext context, DataVisitor visitor, Range range, Object argument) throws IOException {		
		List<T> fetch = fetchData(1, 1); 
		if (fetch == null) {
			fetch = new ArrayList<T>();
		}
		listRow = fetch;
		wrap(context, visitor, range, argument, listRow);
	}
}
