package com.ncsts.jsf.model;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.TaxMatrixDAO;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.view.bean.FilePreferenceBackingBean;

public class TaxMatrixDataModel extends BaseExtendedDataModel<TaxMatrix,Long> {

	private Logger logger = LoggerFactory.getInstance().getLogger(TaxMatrixDataModel.class);
	
	private String taxcodeState;
	private String taxcodeType;
	private String taxcodeCode;
	
	private String cchGroup;
	private String cchTaxCat;
	
	private String defaultFlag;
	private String activeFlag;
	private String matrixStateCode;
	private String matrixCountryCode;
	private String driverGlobalFlag;
	private Long taxMatrixId;
	private String moduleCode;
	private Date effectiveDate;
	
	private FilePreferenceBackingBean filePreferenceBean;

	public TaxMatrixDataModel() {
	}
	
	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
		
		//Default country
		String scountry = filePreferenceBean.getUserPreferenceDTO().getUserCountry();
		matrixCountryCode = scountry;
	}
	
	public void setFilterCriteria(TaxMatrix taxMatrix,
    		String taxcodeState, String taxcodeType, String taxcodeCode,
    		String cchGroup, String cchTaxCat,
    		String defaultFlag, String matrixStateCode, String matrixCountryCode,Date effectiveDate, String driverGlobalFlag,String activeFlag, Long taxMatrixId,String moduleCode) {
		setExampleInstance(taxMatrix);
		this.taxcodeState = taxcodeState;
		this.taxcodeType = taxcodeType;
		this.taxcodeCode = taxcodeCode;
		
		this.cchGroup = cchGroup;
		this.cchTaxCat = cchTaxCat;
		
		this.defaultFlag = defaultFlag;
		this.activeFlag=activeFlag;
		this.matrixStateCode = matrixStateCode;
		this.matrixCountryCode = matrixCountryCode;
		this.driverGlobalFlag = driverGlobalFlag;
		this.taxMatrixId = taxMatrixId;
		this.moduleCode=moduleCode;
		this.effectiveDate=effectiveDate;
	}

	public void resetFilterCriteria() {
		taxcodeState = null;
		taxcodeType = null;
		taxcodeCode = null;
		
		cchGroup = null;
		cchTaxCat = null;
		
		defaultFlag = null;
		matrixStateCode = null;
		matrixCountryCode = null;
		driverGlobalFlag = null;
		activeFlag=null;
		taxMatrixId = null;
		moduleCode=null;
		effectiveDate=null;
		refreshData(false);
	}
	
	@Override
	protected List<TaxMatrix> fetchData(int firstRow, int maxResults) {
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		TaxMatrixDAO dao = (TaxMatrixDAO) getCommonDAO();
		return dao.getAllRecords(getExampleInstance(), 
				taxcodeState, taxcodeType, taxcodeCode, cchGroup, cchTaxCat,
				defaultFlag, matrixStateCode, matrixCountryCode, effectiveDate,driverGlobalFlag, activeFlag,taxMatrixId,
				getOrderBy(), firstRow, maxResults,moduleCode);
	}
	
	@Override
	public Long count() {
		Long count = 0L;
		TaxMatrixDAO dao = (TaxMatrixDAO) getCommonDAO();
		count = dao.count(getExampleInstance(), 
				taxcodeState, taxcodeType, taxcodeCode, cchGroup, cchTaxCat,
				defaultFlag, matrixStateCode, matrixCountryCode, effectiveDate,driverGlobalFlag,activeFlag, taxMatrixId,moduleCode);
		return count;
	}
}

