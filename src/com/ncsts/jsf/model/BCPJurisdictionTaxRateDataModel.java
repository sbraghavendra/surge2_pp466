package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPJurisdictionTaxRateDAO;
import com.ncsts.domain.BCPJurisdictionTaxRate;
import com.ncsts.domain.BCPJurisdictionTaxRatePK;
import com.ncsts.domain.BatchMaintenance;


public class BCPJurisdictionTaxRateDataModel  extends BaseExtendedDataModel<BCPJurisdictionTaxRate, BCPJurisdictionTaxRatePK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPJurisdictionTaxRateDataModel.class);

	private BatchMaintenance batchMaintenance;
	
	public BCPJurisdictionTaxRateDataModel() {
	}

	@Override
	protected List<BCPJurisdictionTaxRate> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPJurisdictionTaxRateDAO dao = (BCPJurisdictionTaxRateDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
		
	}

	@Override
	public Long count() {
		BCPJurisdictionTaxRateDAO dao = (BCPJurisdictionTaxRateDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}

	/**
	 * @return the batchMaintenance
	 */
	public BatchMaintenance getBatchMaintenance() {
		return this.batchMaintenance;
	}

	/**
	 * @param batchMaintenance the batchMaintenance to set
	 */
	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}
	
	public BCPJurisdictionTaxRateDAO getDAO() {
		return (BCPJurisdictionTaxRateDAO)getCommonDAO();
	}


}
