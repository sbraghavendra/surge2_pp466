package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.dao.ProcruleDAO;
import com.ncsts.domain.Procrule;


public class ProcruleDataModel extends BaseExtendedDataModel<Procrule, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(ProcruleDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	
	public ProcruleDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(Procrule procrule) {
		setExampleInstance(procrule);
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<Procrule> fetchData(int firstRow, int maxResults) {
		ProcruleDAO dao = (ProcruleDAO) getCommonDAO();
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<Procrule> list = dao.find(getExampleInstance(), getOrderBy(), firstRow, maxResults);
			return list;
		} else if (criteria != null) {
			Criteria withOrder = dao.create(criteria);
			withOrder.setFirstResult(firstRow);
			withOrder.setMaxResults(maxResults);
			withOrder.setProjection(null);
			if (getOrderBy() != null){
				for (FieldSortOrder field : getOrderBy().getFields()){
					if (field.getAscending()){
						withOrder.addOrder( Order.asc(field.getName()) );
					} else {
						withOrder.addOrder( Order.desc(field.getName()) );
					}
				}
			}
			List<Procrule> list = dao.find(withOrder);
			
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<Procrule>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			ProcruleDAO dao = (ProcruleDAO) getCommonDAO();
			return dao.count(getExampleInstance());
		} else if (criteria != null)  {
			ProcruleDAO dao = (ProcruleDAO) getCommonDAO();
			Criteria c = dao.create(criteria);
			return ((ProcruleDAO) getCommonDAO()).count(c);
		} 
		return 0l;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public ProcruleDAO getProcruleomerLocationDAO() {
		return (ProcruleDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
