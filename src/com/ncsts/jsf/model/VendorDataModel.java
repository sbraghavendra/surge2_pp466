package com.ncsts.jsf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.VendorItemDAO;
import com.ncsts.domain.VendorItem;


public class VendorDataModel extends BaseExtendedDataModel<VendorItem, Long> {
	private Logger logger = LoggerFactory.getInstance().getLogger(VendorDataModel.class);

	private CacheManager cacheManager;
	private DetachedCriteria criteria;
	private String whereClause = "";
	
	public VendorDataModel() {
		setPageSize(20);	// attempt to speed up paging through transactions
		setDbFetchMultiple(5);
	}

	public void setCriteria(DetachedCriteria criteria) {
		setExampleInstance(null);
		this.criteria = criteria;
		refreshData(false);
	}
	
	public void setCriteria(VendorItem vendorItem, String whereClause) {
		setExampleInstance(vendorItem);
		this.whereClause = whereClause;
		this.criteria = null;
		refreshData(false);
	}

	public void resetFilterCriteria() {
		this.criteria = null;
	}
	
	@Override
	protected List<VendorItem> fetchData(int firstRow, int maxResults) {
		VendorItemDAO dao = (VendorItemDAO) getCommonDAO();
		
		//getAllRecords(EntityItem exampleInstance, OrderBy orderBy, int firstRow, int maxResults, String whereClause)
		if (getExampleInstance() != null) {
			logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
			List<VendorItem> list = dao.getAllRecords(getExampleInstance(), getOrderBy(), firstRow, maxResults, whereClause);
			for (VendorItem bm : list) {
				//	bm.setSelected(false);
				//setDescriptions(bm);
			}
			return list;
		} else {
			logger.debug("No criteria set");
			return new ArrayList<VendorItem>();
		}
	}

	@Override
	public Long count() {
		if (getExampleInstance() != null) {
			VendorItemDAO dao = (VendorItemDAO) getCommonDAO();
			return dao.count(getExampleInstance(), whereClause);
		} else{
			return 0l;
		} 
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public VendorItemDAO getCustomerLocationDAO() {
		return (VendorItemDAO)getCommonDAO();
	}

	public DetachedCriteria getCriteria() {
		return this.criteria;
	}
}
