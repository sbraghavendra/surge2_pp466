package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;
import org.richfaces.model.ScrollableTableDataModel;
import org.richfaces.model.SortOrder;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.dao.CommonDAO;
import com.ncsts.domain.Idable;

/**
 * <b>Created:</b> Mar 15, 2008<br>
 * <b>Title:</b> DelegatingDataModel<br>
 * <b>Description:</b><br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * @author MTyson
 * <p>
 * A DataModel for use with ScrollableTable.  Allows delegating the data operations to the backend. 
 */

/**
 * An extension of the RichFaces datamodel that passes calls back to the DB, using the CommonDAO and a
 * className.  The className is used to know what entity to refer to when fulfulling the method calls, by delegating to the DAO. 
 */
public class DelegatingDataModel<T> extends ScrollableTableDataModel<T> {
	private Logger logger = LoggerFactory.getInstance().getLogger(DelegatingDataModel.class);
    
    private Class<?> clazz = null;
    private CommonDAO commonDAO;
    private String[] excludes;
    private T exampleInstance;
    private List<T> cache;
    private int cacheStartRow = -1;
    private int cacheEndRow = -1;
    private SortOrder cacheSortOrder = null;
    
    int rowCount = -1;
    
    /**
     * 
     * @param clazz The entity class that this DelegatingDataModel instance will act for.
     */
    public DelegatingDataModel(Class<?> clazz){
       this.clazz = clazz; 
    }
    
    /**
     * @param className Fully qualified className
     */
    public DelegatingDataModel(String className){
        try {
            this.clazz = Class.forName(className);
        } catch (ClassNotFoundException cnfe){
            logger.error("ClassNotFound: " + className, cnfe);
        }
    }
    
    public T getExampleInstance() {
        return exampleInstance;
    }

    /**
     * An example instance that will be used to define the results.
     * @param exampleInstance
     */
    public void setExampleInstance(T exampleInstance) {
        rowCount = -1; // reset rowcount
        this.exampleInstance = exampleInstance;
    }

    public String[] getExcludes() {
        return excludes;
    }

    /**
     * Excludes that will be applied to the fetch to filter the results.
     * @param excludes
     */
    public void setExcludes(String[] excludes) {
        rowCount = -1;
        this.excludes = excludes;
    }

    /**
     * @deprecated -- As per the RichFaces api
     */
    public int getRowIndex(){
        return -1;
//        throw new UnsupportedOperationException("This method is not implemented.");
    }
    /**
     * Not wrapping the data.  The data is requested from DB each time.
     */
    public Object getWrappedData(){
        throw new UnsupportedOperationException("This method is not implemented: getWrappedData");
    }
    /**
     * Not wrapping the data.  The data is requested from DB each time.
     */
    public void setWrappedData(Object o){
        throw new UnsupportedOperationException("This method is not implemented: setWrappedData");
    }

    public int getRowCount(){

        if (rowCount == -1){
            rowCount = this.commonDAO.getTotalCount(this.clazz, this.getExampleInstance(), this.getExcludes()).intValue();
        }
        
        return this.rowCount;
    }

    @SuppressWarnings("unchecked")
	public List<T> loadData(int startRow, int endRow, SortOrder sortOrder) {
    	if ((cache == null) || (startRow != cacheStartRow) || (endRow != cacheEndRow) ||
    		!((sortOrder == cacheSortOrder) || (cacheSortOrder != null && cacheSortOrder.equals(sortOrder)))) {
	        logger.debug("BEGIN loadData(): " + startRow + " | " + endRow + " | " + sortOrder);
	        if (sortOrder != null && sortOrder.getFields() != null){
	            logger.trace("sortOrder.getFields().length: " + sortOrder.getFields().length);
	        } else {
	            logger.trace("sortOrder or getFields is null.");
	        }
	        OrderBy orderBy = new OrderBy(sortOrder); // Create an orderBy to specify sorting
	        cache = (List<T>) this.commonDAO.getPage(clazz, startRow, endRow, orderBy, this.getExampleInstance(), this.getExcludes());
	        cacheStartRow = startRow;
	        cacheEndRow = endRow;
	        cacheSortOrder = sortOrder;
    	} else {
    		logger.debug("Returning cached data");
    	}
        
        return cache; 
    }
    
    @SuppressWarnings("unchecked")
	public T getObjectById(Object id){
        if (id != null){
            T entity = (T) commonDAO.getById(clazz, id);
            logger.trace("RETURNING entity: " + entity);
            return entity;
        } else {
            return null;
        }
    }
    
    public Object getId(Object o){
        logger.trace("BEGIN getId(): " + o);
        Object id = null;
		if ((o != null) && (o instanceof Idable)) {
			id = ((Idable<?>) o).getId();
		}

        logger.trace("RETURN id: " + id);
        return id;
    }

    public CommonDAO getCommonDAO() {
        return commonDAO;
    }

    public void setCommonDAO(CommonDAO commonDAO) {
        this.commonDAO = commonDAO;
    }
    
    public String toString(){
        return "DelegatingDataModel - size: " + this.rowCount;
    }
}
