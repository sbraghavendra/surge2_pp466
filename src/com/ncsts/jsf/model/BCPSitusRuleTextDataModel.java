package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPSitusRuleTextDAO;
import com.ncsts.domain.BCPSitusRuleText;
import com.ncsts.domain.BCPSitusRuleTextPK;
import com.ncsts.domain.BatchMaintenance;


public class BCPSitusRuleTextDataModel extends BaseExtendedDataModel<BCPSitusRuleText, BCPSitusRuleTextPK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPSitusRuleTextDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}

	@Override
	public List<BCPSitusRuleText> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPSitusRuleTextDAO dao = (BCPSitusRuleTextDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPSitusRuleTextDAO dao = (BCPSitusRuleTextDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
