package com.ncsts.jsf.model;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.dao.BCPCustLocnTextDAO;
import com.ncsts.domain.BCPCustLocnText;
import com.ncsts.domain.BCPCustLocnTextPK;
import com.ncsts.domain.BatchMaintenance;


public class BCPCustLocnTextDataModel extends BaseExtendedDataModel<BCPCustLocnText, BCPCustLocnTextPK> {
	private Logger logger = LoggerFactory.getInstance().getLogger(BCPCustLocnTextDataModel.class);
	
	private BatchMaintenance batchMaintenance;

	public BatchMaintenance getBatchMaintenance() {
		return batchMaintenance;
	}

	public void setBatchMaintenance(BatchMaintenance batchMaintenance) {
		if (batchMaintenance != this.batchMaintenance) {
			this.batchMaintenance = batchMaintenance;
			refreshData(false);
		}
	}

	@Override
	public List<BCPCustLocnText> fetchData(int firstRow, int maxResults) {
		this.getSortOrder().put("batchId", BaseExtendedDataModel.SORT_DESCENDING);
		logger.debug("Fetching data from " + firstRow + " (" + maxResults + " rows)");
		BCPCustLocnTextDAO dao = (BCPCustLocnTextDAO) getCommonDAO();
		return dao.getPage(batchMaintenance, firstRow, maxResults);
	}

	@Override
	public Long count() {
		BCPCustLocnTextDAO dao = (BCPCustLocnTextDAO) getCommonDAO();
		return dao.count(batchMaintenance);
	}
}
