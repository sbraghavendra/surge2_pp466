package com.ncsts.jsf.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.ncsts.common.LoggerFactory;
import com.ncsts.common.helper.FieldSortOrder;
import com.ncsts.common.helper.OrderBy;
import com.ncsts.domain.PurchaseTransaction;

public class TransactionDetailMemoryModel extends BaseExtendedDataModel<PurchaseTransaction, Long> {
	private List<PurchaseTransaction> purchaseTransactions;
	
	
	private Logger logger = LoggerFactory.getInstance().getLogger(TransactionBillDataMemoryModel.class);
	OrderBy defaultOrderBy = null;

	protected OrderBy getDefaultOrderBy() {
		if(defaultOrderBy == null){
				OrderBy o = new OrderBy();
				o.addFieldSortOrder(new FieldSortOrder("purchtransId",1,true));
				defaultOrderBy = o;
		}
		return defaultOrderBy;
}
	@Override
	protected List<PurchaseTransaction> fetchData(int firstRow, int maxResults) {
		logger.info("enter TransactionDetailMemoryModel.fetchData()");
		
		 
		
		
		return this.purchaseTransactions;
	}

	@Override
	protected Long count() {
		return this.purchaseTransactions == null ? 0L : (long) this.purchaseTransactions.size();
	}

	public List<PurchaseTransaction> getPurchaseTransactions() {
		return purchaseTransactions;
	}

	public void setPurchaseTransactions(List<PurchaseTransaction> purchaseTransactions) {
		this.purchaseTransactions = purchaseTransactions;
	}

	@Override
	public void tooggleSelectAll(boolean value, int maxToFetch) {
		if(value) {
			getSelectionMap().clear();
			for(PurchaseTransaction t : this.purchaseTransactions) {
				getSelectionMap().put(t.getId(), Boolean.TRUE);
			}
		}
		else {
			getSelectionMap().clear();
		}
	}
	
	@Override
    public void toggleSortOrder(String field) {
    	Map<String,String> sortOrder = getSortOrder();
    	processToggleSortOrder(sortOrder, field);
		
		// Refresh data, but we can keep the count
		refreshDataTest(false);
		OrderBy orderBy = getOrderBy();
		if(orderBy.getSortOrderSize()==0){
			orderBy = getDefaultOrderBy();
		}
			
		if(orderBy != null && purchaseTransactions != null && purchaseTransactions.size() > 1) {
				final String fieldName = orderBy.getFields().get(0).getName();
				final boolean ascendingOrder = orderBy.getFields().get(0).getAscending();
				Collections.sort(purchaseTransactions, new Comparator<PurchaseTransaction>() {
				
				@Override
				public int compare(PurchaseTransaction b1, PurchaseTransaction b2 ) {
					try {
						DecimalFormat deciFormat = new DecimalFormat();
						SimpleDateFormat format = new SimpleDateFormat("yyy-mm-dd");
						deciFormat.setParseBigDecimal(true);
						Object o1 = BeanUtils.getProperty(b1, fieldName);
						Object o2 = BeanUtils.getProperty(b2, fieldName);
						if(ascendingOrder) {
							if(o1 == null)
								return -1;
							if(o2 == null)
								return 1;
							
							try {
								if(format.parse(o1.toString()) != null){
									return (format.parse(o1.toString()).compareTo(format.parse(o2.toString())));
								}
							} catch(ParseException e) { }
							
							if(deciFormat.parse((String) o1) != null)
							{
								return (new BigDecimal(o1.toString())).compareTo(new BigDecimal(o2.toString()));
							}
							else {
								return ((String)o1).compareTo((String)o2);
							}
						} else {
							if(o1 == null)
								return 1;
							if(o2 == null)
								return -1;
							
							try {
								if(format.parse(o1.toString()) != null){
									return (format.parse(o2.toString()).compareTo(format.parse(o1.toString())));
								}
							} catch (ParseException e) { }
							
							if(deciFormat.parse((String) o1) != null)
							{
								return (new BigDecimal(o2.toString())).compareTo(new BigDecimal(o1.toString()));
							}
							else {
								return ((String)o2).compareTo((String)o1);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				}
			});
			
		}
    }
}