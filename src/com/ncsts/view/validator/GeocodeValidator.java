package com.ncsts.view.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

public class GeocodeValidator implements Validator {

	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(GeocodeValidator.class);
	private static final String Geocode_REGEXP = "^\\d*[a-zA-Z][a-zA-Z0-9]*$";

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {
		// TODO Auto-generated method stub
		String Geocode = (String) arg2;

		Pattern mask = null;

		mask = Pattern.compile(Geocode_REGEXP);

		Matcher matcher = mask.matcher(Geocode);

		if (!matcher.matches()) {

			FacesMessage message = new FacesMessage();

			message.setDetail("Geocode must contain at least one alpha-character and no special characters.");

			message.setSummary("Geocode must contain at least one alpha-character and no special characters.");

			message.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(message);

		}

	}

}
