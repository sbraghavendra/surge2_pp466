/*
 * Author: Jim M. Wilson
 * Created: Aug 8, 2008
 * $Date: 2006-09-21 22:51:26 $: - $Revision: 4320 $: 
 */

package com.ncsts.view.validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.ncsts.view.bean.UpdateFilesBackingBean;

/**
 * Validates format of taxcode rules update file 
 */
public class SitusRulesFileUploadValidator extends AbstractFileUploadValidator {
  private Logger logger = Logger.getLogger(SitusRulesFileUploadValidator.class);

  /*
   * (non-Javadoc)
   * 
   * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
   *      javax.faces.component.UIComponent, java.lang.Object)
   */
  public void validateContent(UploadedFile f, BufferedReader reader) throws IOException, ValidatorException {
	  String firstLine = reader.readLine();
      
      if(firstLine == null){
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " appears to be empty", ""));
      }
      
      String typeStr = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.TYPE_VALUE);
      if(typeStr==null) {
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " Invalid Full/Update Type Selected", ""));
      }
			
      String dateStr = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.DATE_VALUE);
      if(dateStr==null) {
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " Invalid Date Type Selected", ""));
      }
		
      String fileStr = UpdateFilesBackingBean.getHeaderValueOfType(firstLine, UpdateFilesBackingBean.FILE_VALUE);
      if(fileStr==null) {
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " - Invalid File Type Selected (should be \"SITUS\")", ""));
      }

      if (!(typeStr.equalsIgnoreCase("Full") || typeStr.equalsIgnoreCase("Update"))) {
    	  logger.debug("fileType : " + typeStr + "," + typeStr.length());
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " header type must be one of Update or Full", ""));
      }
        
      try {
    	  if ((!fileStr.trim().equalsIgnoreCase("situs") || StringUtils.isEmpty(fileStr))) {
    		  logger.debug("fileType code: " + fileStr + "," + fileStr.length());
    		  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " - Invalid File Type Selected (should be \"SITUS\")", ""));
          }
      }
      catch(ArrayIndexOutOfBoundsException aoe) {
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " - Invalid File Type Selected (should be \"SITUS\")", ""));
      }
      catch(Exception e) {
    	  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, f.getName() + " - Invalid File Type Selected (should be \"SITUS\")", ""));
      }
      
  }
  //4323 
  public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
	    logger.debug("Validating file upload");
	    UploadedFile f = (UploadedFile) arg2;
	    String fileExt = f.getContentType();
	    String test=f.getName();
	    BufferedReader din = null;
	    InputStream stream = null;
	    byte[] buffer = null;

	    try {
	      String types = "";
	      for (String s : validTypes) {
	      	if (types.length() > 0) types += ", ";
	      	types += s;
	      }
	      
	      if (! validTypes.contains(fileExt.toLowerCase())) {

	    	  String msg = "Invalid Directory path or Sample File";
	        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, ""));
	      }
	      stream = f.getInputStream();
	      din = new BufferedReader(new InputStreamReader(stream));
	      validateContent(f, din);
	    } catch (IOException e) {
	      logger.debug("Exception while validating file upload stream");
	    } finally {
	      try {
	        if (stream != null) stream.close();
	        if (din != null) din.close();
	      } catch (IOException e) {
	        e.printStackTrace();
	        logger.debug("Exception while closing file upload stream");
	      }

	    }
	  }
	
}
