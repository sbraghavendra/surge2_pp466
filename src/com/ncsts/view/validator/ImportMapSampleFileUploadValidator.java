/*
 * Author: Jim M. Wilson
 * Created: Sep 13, 2008
 * $Date: 2006-09-21 22:51:26 $: - $Revision: 3956 $: 
 */

package com.ncsts.view.validator;

import java.io.BufferedReader;
import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * Validates format of import map sample file
 */
public class ImportMapSampleFileUploadValidator extends AbstractFileUploadValidator {
  private Logger logger = Logger.getLogger(ImportMapSampleFileUploadValidator.class);

  /*
   * (non-Javadoc)
   * 
   * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
   *      javax.faces.component.UIComponent, java.lang.Object)
   */
  public void validateContent(UploadedFile f, BufferedReader reader) throws IOException, ValidatorException {

	  // the superclass validator is enough at this point
  }
}
