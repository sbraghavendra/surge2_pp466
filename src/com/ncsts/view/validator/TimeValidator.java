/*
 * Author: Jim M. Wilson
 * Created: Aug 8, 2008
 * $Date: 2010-01-05 18:47:40 -0600 (Tue, 05 Jan 2010) $: - $Revision: 4461 $: 
 */

package com.ncsts.view.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

/**
 * Validates a time string
 */
public class TimeValidator implements Validator {
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(TimeValidator.class);
  // formats to try
  private final String[] FORMATS = new String[] {"hh:mm:ss a", "HH:mm:ss", "hh:mm a", "HH:mm"};
  
  /*
   * (non-Javadoc)
   * 
   * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
   *      javax.faces.component.UIComponent, java.lang.Object)
   */
  public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
    logger.debug("Validating Time");
    String s = (String) arg2;
    if ("*now".equalsIgnoreCase(s)) {
    	return;
    }
    if (parse(s) == null) {
    	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    			"Invalid time format", ""));
    }
  }
  
  public Date parse(String s, String timeZoneString) {
	  return parse(s, TimeZone.getTimeZone(timeZoneString));
  }
  
  public Date parse(String s) {
	  return parse(s, TimeZone.getDefault());
  }
  
  public Date parse(String s, TimeZone timeZone) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String dateString = String.format("%s %s", sdf.format(new Date()), s);
  	
	for (String f : FORMATS) {
  		try {
  			String fs = String.format("%s %s", "yyyy-MM-dd", f);
  			
  			sdf = new SimpleDateFormat(fs);
  			sdf.setTimeZone(timeZone);
  			
  			Date d = sdf.parse(dateString);
  			logger.debug("Parsed date=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
  			return d;
  		} catch (ParseException e) {}
  	}
	return null;
  }
}