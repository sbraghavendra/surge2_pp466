/*
 * Author: Jim M. Wilson
 * Created: Sep 18, 2008
 * $Date: 2006-09-21 22:51:26 $: - $Revision: 4319 $: 
 */

package com.ncsts.view.validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * 
 */
public abstract class AbstractFileUploadValidator implements Validator {
  private Logger logger = Logger.getLogger(AbstractFileUploadValidator.class);
  
  protected abstract void validateContent(UploadedFile file, BufferedReader reader) throws IOException, ValidatorException;
//modified for fixing Bug 0003884
  protected List<String> validTypes = new ArrayList<String>(Arrays.asList( new String[] {"text/plain","text/html","application/msword"}));
   
  /*
   * (non-Javadoc)
   * 
   * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
   *      javax.faces.component.UIComponent, java.lang.Object)
   */
  public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
    logger.debug("Validating file upload");
    UploadedFile f = (UploadedFile) arg2;
    String fileExt = f.getContentType();
    String test=f.getName();
    BufferedReader din = null;
    InputStream stream = null;
    byte[] buffer = null;
 
    try {
      String types = "";
      for (String s : validTypes) {
      	if (types.length() > 0) types += ", ";
      	types += s;
      }

      if (! validTypes.contains(fileExt.toLowerCase())) {
    	  //3884 BB
/*      	String msg = "Invalid content type: " + fileExt + ", expecting" + 
      		((validTypes.size() > 1) ? " one of: " : ": ") + types;*/
    	  String msg = "Invalid Sample File Name";
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, ""));
      }
      stream = f.getInputStream();
      din = new BufferedReader(new InputStreamReader(stream));
      validateContent(f, din);
    } catch (IOException e) {
      logger.debug("Exception while validating file upload stream");
    } finally {
      try {
        if (stream != null) stream.close();
        if (din != null) din.close();
      } catch (IOException e) {
        e.printStackTrace();
        logger.debug("Exception while closing file upload stream");
      }

    }
  }
}
