package com.ncsts.view.validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

public class ImportConfigFileUploadValidator extends
		AbstractFileUploadValidator {
	private Logger logger = Logger.getLogger(ImportConfigFileUploadValidator.class);

	@Override
	protected void validateContent(UploadedFile file, BufferedReader reader)
			throws IOException, ValidatorException {
	      String header = reader.readLine();

	      if (header == null) {
	        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
	        		file.getName() + " appears to be empty", ""));
	      } else if (!header.contains("Type")) {
	        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
	        		file.getName() + " header line is missing Type column", ""));
	      } else if (!header.contains("Date")) {
	        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
	        		file.getName() + " header line is missing Date column", ""));
	      } else if (!header.contains("ConfigBatch")) {
		        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
		        		file.getName() + " header line is missing ConfigBatch column", ""));
		   }else if (!header.contains("Append")) {
		        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
		        		file.getName() + " header line is missing Append column", ""));
		   } else {
	        logger.debug("header : " + header);
	        String ar[] = header.split(",");
	       }
	  }

}
