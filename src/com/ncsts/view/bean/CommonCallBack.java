package com.ncsts.view.bean;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;

public class CommonCallBack implements BaseMatrixBackingBean.FindIdCallback,
		BaseMatrixBackingBean.SearchCallback,
		ViewTaxCodeBean.TaxCodeInfoCallback {

	private ViewTaxCodeBean viewTaxCodeBean;
	@Autowired
	private TaxMatrixViewBean taxMatrix;
	@Autowired
	private GSBMatrixViewBean gsbMatrixBean;
	@Autowired
	private TransactionDetailSaleSearchBean transactionDetailSaleSearchBean;
	@Autowired
	private CustomerLocationSearchBean customerLocationBean;
	@Autowired
	private ExemptionCertificateBean exemptionCertificateBean;
	@Autowired
	private LocationMatrixBackingBean locationMatrixBean;
	@Autowired
	private EntityBackingBean entityBackingBean;
	@Autowired
	private loginBean loginBean;
	@Autowired
	private EntityItemService entityItemService;
	@Autowired
	private TransactionViewBean transactionViewBean;
	@Autowired
	private TransactionDetailBean transactionDetailBean;
	@Autowired
	private JurisdictionService jurisdictionService;
	
	private EntityType txnLocnentitytype = null;
	
	private String entityType = null;
	
	private String entitySelected;

	public String getEntitySelected() {
		return entitySelected;
	}

	public void setEntitySelected(String entitySelected) {
		this.entitySelected = entitySelected;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@Override
	public void searchTaxCodeInfoCallback(String taxcodeCountry,
			String taxcodeState, String taxcodeType, String taxcodeCode,
			String callingPage) {
		viewTaxCodeBean.reset();
		viewTaxCodeBean.setTaxCode(taxcodeCountry, taxcodeState, taxcodeType,
				taxcodeCode);
		viewTaxCodeBean.setCallingPage(callingPage);
	}

	@Override
	public void searchCallback(String context) {
		if (context.equalsIgnoreCase("search")) {
			if (loginBean.getHasPurchaseLicense()
					&& loginBean.getHasBillingLicense()
					&& loginBean.getIsBillingSelected()) {
				gsbMatrixBean.updateMatrixList();
			} else {
				taxMatrix.updateMatrixList();
			}
		}

	}

	@Override
	public void findIdCallback(Long id, String action) {
		if (action.equalsIgnoreCase("entityId_Tax")) {
			taxMatrix.entityId = id;
			EntityItem entityItem = entityItemService.findById(id);
			if (entityItem != null) {
				taxMatrix.entityCode = entityItem.getEntityCode();
			}
		}
		else if (action.equalsIgnoreCase("entityId_Tax_Filter")) {
			taxMatrix.entityId = id;
			EntityItem entityItem = entityItemService.findById(id);
			if (entityItem != null) {
				taxMatrix.entityCodeFilter = entityItem.getEntityCode();
			}
		}
		else if (action.equalsIgnoreCase("entityId_Location")) {
			EntityItem entityItem = entityItemService.findById(id);
			if (entityItem != null) {
				locationMatrixBean.entityCode=entityItem.getEntityCode();
			}
		}
		else if (action.equalsIgnoreCase("entityId_Location_Filter")) {
			EntityItem entityItem = entityItemService.findById(id);
			if (entityItem != null) {
				locationMatrixBean.entityCodeFilter=entityItem.getEntityCode();
			}
		}
		else if (action.equalsIgnoreCase("entityId_Sale")) {
			if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
				EntityItem entityItem = entityItemService.findById(id);
				if (entityItem != null) {
					transactionDetailSaleSearchBean.setEntityCode(entityItem.getEntityCode());
				}
			}
		}
		else if (action.equalsIgnoreCase("entityId_Gsb")) {
			if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
				gsbMatrixBean.entityId = id;
				EntityItem entityItem = entityItemService.findById(id);
				if (entityItem != null) {
					gsbMatrixBean.entityCode = entityItem.getEntityCode();
				}
			}
		}
		else if (action.equalsIgnoreCase("entityId_Gsb_Filter")) {
			if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
				gsbMatrixBean.entityId = id;
				EntityItem entityItem = entityItemService.findById(id);
				if (entityItem != null) {
					gsbMatrixBean.entityCodeFilter = entityItem.getEntityCode();
				}
			}
		}
		else if (action.equalsIgnoreCase("entityId_CustLoc")) {
			if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
				EntityItem entityItem = entityItemService.findById(id);
				if (entityItem != null) {
					customerLocationBean.entityCode=entityItem.getEntityCode();
				}
			}
		}
		else if (action.equalsIgnoreCase("entityId_Exempt")) {
			if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
				EntityItem entityItem = entityItemService.findById(id);
				if (entityItem != null) {
					exemptionCertificateBean.entityCode=entityItem.getEntityCode();
				}
			}
		}
		else if (action.equalsIgnoreCase("entityId_trans_vendor_edit")) {
			EntityItem entityItem = entityItemService.findById(id);
			if (entityItem != null) {
				transactionViewBean.getUpdatedTransaction().setCpFilingEntityCode(entityItem.getEntityCode());
			}
		}

		else if (action.equalsIgnoreCase("entityId_txn_locn_edit")) {
			setJurisdicHandlerValues(txnLocnentitytype,id);
		}
		
	}
	
	public void setJurisdicHandlerValues(EntityType txnLocnentitytype, Long id) {
		switch (txnLocnentitytype) {
		case shipto:
			SearchJurisdictionHandler txnLocnShiptoJurisdictionHandler = transactionViewBean.getTxnLocnShiptoJurisdictionHandler();
			setJurisdiction(txnLocnShiptoJurisdictionHandler,id);
			break;
		case shipfrom:
			SearchJurisdictionHandler txnLocnShipfromJurisdictionHandler = transactionViewBean.getTxnLocnShipfromJurisdictionHandler();
			setJurisdiction(txnLocnShipfromJurisdictionHandler,id);
			break;
		case ordrorgn:
			SearchJurisdictionHandler txnLocnOrdrorgnJurisdictionHandler = transactionViewBean.getTxnLocnOrdrorgnJurisdictionHandler();
			setJurisdiction(txnLocnOrdrorgnJurisdictionHandler,id);
			break;
		case ordracpt:
			SearchJurisdictionHandler txnLocnOrdracptJurisdictionHandler = transactionViewBean.getTxnLocnOrdracptJurisdictionHandler();
			setJurisdiction(txnLocnOrdracptJurisdictionHandler,id);
			break;
		case firstuse:
			SearchJurisdictionHandler txnLocnFirstuseJurisdictionHandler = transactionViewBean.getTxnLocnFirstuseJurisdictionHandler();
			setJurisdiction(txnLocnFirstuseJurisdictionHandler,id);
			break;
		case billto:
			SearchJurisdictionHandler txnLocnBilltoJurisdictionHandler = transactionViewBean.getTxnLocnBilltoJurisdictionHandler();
			setJurisdiction(txnLocnBilltoJurisdictionHandler,id);
			break;
		case ttlxfr:
			SearchJurisdictionHandler txnLocnTtlxfrJurisdictionHandler = transactionViewBean.getTxnLocnTtlxfrJurisdictionHandler();
			setJurisdiction(txnLocnTtlxfrJurisdictionHandler,id);
			break;
	}
	}
	
	
	public void setJurisdiction(SearchJurisdictionHandler handler, Long id) {
		EntityItem entityItem = entityItemService.findById(id);
		if(entityItem !=null){
			Long entityItemJurId = entityItem.getJurisdictionId();
			if(entityItemJurId!=null && entityItemJurId!=0L){
				Jurisdiction jurisdiction = jurisdictionService.findById(entityItemJurId);
				if(jurisdiction !=null){
					handler.setEntityCode(entityItem.getEntityCode());
					handler.setJurisdictionId(jurisdiction.getJurisdictionId().toString()); 
					handler.setGeocode(jurisdiction.getGeocode());
					handler.setCountry(jurisdiction.getCountry());
					handler.setState(jurisdiction.getState());
					handler.setCounty(jurisdiction.getCounty());
					handler.setCity(jurisdiction.getCity());
					handler.setZip(jurisdiction.getZip());
					handler.setZipPlus4(jurisdiction.getZipplus4());
					handler.setStj1Name(jurisdiction.getStj1Name());
					handler.setStj2Name(jurisdiction.getStj2Name());
					handler.setStj3Name(jurisdiction.getStj3Name());
					handler.setStj4Name(jurisdiction.getStj4Name());
					handler.setStj5Name(jurisdiction.getStj5Name());
				}
				
				else {
					handler.setJurisdictionId(null);
					resetHandler(handler);
				}
				
			}
			else {
				handler.setEntityCode(entityItem.getEntityCode());
				handler.setJurisdictionId(null);
				handler.setGeocode(entityItem.getGeocode());
				handler.setCountry(entityItem.getCountry());
				handler.setState(entityItem.getState());
				handler.setCounty(entityItem.getCounty());
				handler.setCity(entityItem.getCity());
				handler.setZip(entityItem.getZip());
				handler.setZipPlus4(entityItem.getZipPlus4());
				handler.setStj1Name(null);
				handler.setStj2Name(null);
				handler.setStj3Name(null);
				handler.setStj4Name(null);
				handler.setStj5Name(null);
			}
		}else {
			resetHandler(handler);
		}
	}
	
	private void resetHandler(SearchJurisdictionHandler handler) {
		handler.setGeocode(null);
		handler.setCountry(null);
		handler.setState(null);
		handler.setCounty(null);
		handler.setCity(null);
		handler.setZip(null);
		handler.setZipPlus4(null);
		handler.setStj1Name(null);
		handler.setStj2Name(null);
		handler.setStj3Name(null);
		handler.setStj4Name(null);
		handler.setStj5Name(null);
	}

	public String findTaxMatrixIdAction() {
		EntityItem entityFilter = new EntityItem();
		if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
			entityFilter.setEntityId(gsbMatrixBean.entityId);
			entityFilter.setEntityCodeCallback(gsbMatrixBean.entityCodeFilter);
			entityBackingBean.findId(this, "entityId_Gsb_Filter", "tax_matrix_sales_main",entityFilter);
		} else {
			entityFilter.setEntityId(taxMatrix.entityId);
			entityFilter.setEntityCodeCallback(taxMatrix.entityCodeFilter);
			entityBackingBean.findId(this, "entityId_Tax_Filter", "tax_matrix_main",entityFilter);
		}
		
		return "entity_main";
	}
	
	public String findTaxMatrixIdUpdateAction() {
		EntityItem entityFilter = new EntityItem();
		if (loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense() && loginBean.getIsBillingSelected()) {
			entityFilter.setEntityId(gsbMatrixBean.entityId);
			entityFilter.setEntityCodeCallback(gsbMatrixBean.entityCode);
			entityBackingBean.findId(this, "entityId_Gsb", "tax_matrix_sales_details",entityFilter);
		} else {
			entityFilter.setEntityId(taxMatrix.entityId);
			entityFilter.setEntityCodeCallback(taxMatrix.entityCode);
			entityBackingBean.findId(this, "entityId_Tax", "tax_matrix_detail",entityFilter);
		}
		
		return "entity_main";
	}

	public String findLocationMatrixIdAction() {
		EntityItem entityFilter = new EntityItem();
		entityFilter.setEntityCodeCallback(locationMatrixBean.getEntityCodeFilter());
		entityBackingBean.findId(this, "entityId_Location_Filter", "location_matrix_main", entityFilter);
		return "entity_main";
	}
	
	public String findTxnDriversEntCodeEditAction() {
		EntityItem entityFilter = new EntityItem();
		entityBackingBean.findId(this, "entityId_txn_drivers_ent_edit", "trans_view", entityFilter);
		return "entity_main";
	}
	
	public String findTxnDriversInhEntcodeEditAction() {
		EntityItem entityFilter = new EntityItem();
		entityBackingBean.findId(this, "entityId_txn_drivers__inh_ent_edit", "trans_view", entityFilter);
		return "entity_main";
	}
	
	public String findLocationMatrixIdUpdateAction() {
		EntityItem entityFilter = new EntityItem();
		entityFilter.setEntityCodeCallback(locationMatrixBean.getEntityCode());
		entityBackingBean.findId(this, "entityId_Location", "location_matrix_detail", entityFilter);
		return "entity_main";
	}
	
	public String findTransactionVendorEditAction() {
		EntityItem entityFilter = new EntityItem();
		entityFilter.setEntityCodeCallback(transactionViewBean.getSelectedTransaction().getCpFilingEntityCode());
		entityBackingBean.findId(this, "entityId_trans_vendor_edit", "trans_view", entityFilter);
		return "entity_main";
	}
	
	public String findTxnLocnEditAction() {
		EntityItem entityFilter = new EntityItem();
		entityType = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("entityType");
		txnLocnentitytype = EntityType.valueOf(entityType);
		entityBackingBean.findId(this, "entityId_txn_locn_edit", "trans_view", entityFilter);
		return "entity_main";
	}
	
	public void entitySelectionChanged(ActionEvent e) {
	   entitySelected = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getValue();
		System.out.println("selected entity is  :  " +entitySelected);
	}
	public void getTxnLocnEditSelectedEntity(String entityType) {
		System.out.println("Entity type is when drop down selected is  : "+entityType); 
		txnLocnentitytype = EntityType.valueOf(entityType);
		EntityItem dropDownentityItem = entityBackingBean.findEntityByEntityCode(entitySelected);
		if(dropDownentityItem !=null){
			System.out.println("Dropdown entity selected id is : "+dropDownentityItem.getEntityId() + "    code is : "+dropDownentityItem.getEntityCode());
			setJurisdicHandlerValues(txnLocnentitytype, dropDownentityItem.getEntityId());
		}else {
			System.out.println("Dropdown entity selected id is :"+dropDownentityItem); 
			setJurisdicHandlerValues(txnLocnentitytype, -1L);
		}
	}
	
	public String findCustLocIdAction(){
		EntityItem entityFilter = new EntityItem();
		if(customerLocationBean!=null && customerLocationBean.getEntityCode()!=null && customerLocationBean.getEntityCode().length()>0 && customerLocationBean.getEntityCode()!=" "){
			EntityItem entityItem = entityItemService.findByCode(customerLocationBean.getEntityCode());    
			entityFilter.setEntityId(entityItem.getEntityId());
			entityFilter.setEntityCodeCallback(customerLocationBean.getEntityCode());
		}		
		entityBackingBean.findId(this, "entityId_CustLoc", "customerLocation_main", entityFilter);	
		return "entity_main";
	}
	
	public String findSaleTransactionIdAction() {
		EntityItem entityFilter = new EntityItem();
		entityFilter.setEntityCodeCallback(transactionDetailSaleSearchBean.getEntityCode());
		entityBackingBean.findId(this, "entityId_Sale", "transactions_sale", entityFilter);
		return "entity_main";
	}
	
	public String exmpCertIdAction(){	
		EntityItem entityFilter = new EntityItem();
		if(exemptionCertificateBean!=null && exemptionCertificateBean.getEntityCode()!=null&& exemptionCertificateBean.getEntityCode()!=" " && exemptionCertificateBean.getEntityCode().length()>0){
			EntityItem entityItem = entityItemService.findByCode(exemptionCertificateBean.getEntityCode());		    
			entityFilter.setEntityId(entityItem.getEntityId());
			entityFilter.setEntityCodeCallback(exemptionCertificateBean.getEntityCode());
		}	
		entityBackingBean.findId(this, "entityId_Exempt", "exemptionCertificate_main", entityFilter);
		return "entity_main";
	}
	
	public String exmpCertIdUpdateAction(){	
	   EntityItem entityFilter = new EntityItem();
	   if(exemptionCertificateBean!=null && exemptionCertificateBean.getEntityCode()!=null &&  exemptionCertificateBean.getEntityCode()!=" " && exemptionCertificateBean.getEntityCode().length()>0){
			EntityItem entityItem = entityItemService.findByCode(exemptionCertificateBean.getEntityCode());
			entityFilter.setEntityId(entityItem.getEntityId());
			entityFilter.setEntityCodeCallback(exemptionCertificateBean.getEntityCode());
		}		
		entityBackingBean.findId(this, "entityId_Exempt", "exemption_update", entityFilter);
		return "entity_main";
	}

	public ViewTaxCodeBean getViewTaxCodeBean() {
		return viewTaxCodeBean;
	}

	public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
		this.viewTaxCodeBean = viewTaxCodeBean;
	}
}
