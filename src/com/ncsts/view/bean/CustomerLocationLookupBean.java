package com.ncsts.view.bean;

import org.apache.log4j.Logger;

import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;

public class CustomerLocationLookupBean extends CustomerLocationBean {	
	
	private Logger logger = Logger.getLogger(CustomerLocationLookupBean.class);
	private ExemptionCertificateBean exemptionCertificateBean;
	private TransactionDetailSaleBean transactionDetailSaleBean;
	private BillingTestingToolBean billingTestingToolBean;
	private String beanName = "";
	private PurchaseTestingToolBean purchaseTestingToolBean;
	public void setBean(ExemptionCertificateBean exemptionCertificateBean, String beanName){
		this.exemptionCertificateBean = exemptionCertificateBean;
		this.beanName = beanName;
	}
	
	public void setBean(TransactionDetailSaleBean transactionDetailSaleBean, String beanName){
		this.transactionDetailSaleBean = transactionDetailSaleBean;
		this.beanName = beanName;
	}
	
	public void setBean(BillingTestingToolBean bean, String beanName){
		this.billingTestingToolBean = bean;
		this.beanName = beanName;
	}
	
	public void setBean(PurchaseTestingToolBean bean, String beanName){
		this.purchaseTestingToolBean = bean;
		this.beanName = beanName;
	}
	
	public String canceLookupAction() {
		if(beanName.equalsIgnoreCase("exemptionCertificateBean")){
			beanName = "";
			
			return "exemption_update";
		}
		else if(beanName.equalsIgnoreCase("transactionDetailSaleBean")){
			beanName = "";
			
			return "transactions_sale";
		}
		else if(beanName.equalsIgnoreCase("billingTestingToolBean")){
			beanName = "";
			
			return "billing_test_tool";
		}
		else if(beanName.equalsIgnoreCase("purchaseTestingToolBean")) {
			beanName = "";
	
			return "purchase_test_tool";
		}
		beanName = "";
		return null;
	}
	
	public String beginLookupAction(Cust aFilterCust, CustLocn aFilterCustLocn) {	
		resetFilterSearchAction();
		
		getFilterCust().setCustName(aFilterCust.getCustName());
		getFilterCust().setCustNbr(aFilterCust.getCustNbr());
		
		getFilterCustLocn().setCustLocnCode(aFilterCustLocn.getCustLocnCode());
		getFilterCustLocn().setLocationName(aFilterCustLocn.getLocationName());
		
		setLookupFlag("1");

		retrieveFilterCust();
		
		return "custLocnLookup_main";
	}
	
	public Boolean getDisplayCustLocnButtons() {
		if((super.getSelCust()!=null && super.getSelCust().getCustId()!=null) && 
				(beanName.equalsIgnoreCase("transactionDetailSaleBean") || beanName.equalsIgnoreCase("billingTestingToolBean"))){
			return true;
		}
		else{
			if(beanName.equalsIgnoreCase("exemptionCertificateBean") && super.getDisplayCustLocnButtons()){
				return true;
			}
			
			return false;
		}
	}
	
	public String selectLookupAction() {	
		Cust updateCust= new Cust();
		if(this.getSelCust()!=null){
			updateCust.setCustId(this.getSelCust().getCustId());
			updateCust.setCustName(this.getSelCust().getCustName());
			updateCust.setCustNbr(this.getSelCust().getCustNbr());
		}
		
		CustLocn updateCustLocn = new CustLocn();
		if(this.getSelCustLocn()!=null){
			updateCustLocn.setCustLocnId(this.getSelCustLocn().getCustLocnId());
			updateCustLocn.setCustId(this.getSelCustLocn().getCustId());
			updateCustLocn.setCustLocnCode(this.getSelCustLocn().getCustLocnCode());
			updateCustLocn.setLocationName(this.getSelCustLocn().getLocationName());
		}
		
		if(beanName.equalsIgnoreCase("exemptionCertificateBean")){
			exemptionCertificateBean.custLocnSelectionChanged(updateCust, updateCustLocn);
			beanName = "";
			
			return "exemption_update";
		}
		else if(beanName.equalsIgnoreCase("transactionDetailSaleBean")){
			transactionDetailSaleBean.custLocnSelectionChanged(updateCust, updateCustLocn);
			beanName = "";
			
			return "transactions_sale";
		}
		else if(beanName.equalsIgnoreCase("billingTestingToolBean")){
			updateCustLocn.setAddressLine1(this.getSelCustLocn().getAddressLine1());
			updateCustLocn.setAddressLine2(this.getSelCustLocn().getAddressLine2());
			updateCustLocn.setGeocode(this.getSelCustLocn().getGeocode());
			updateCustLocn.setCity(this.getSelCustLocn().getCity());
			updateCustLocn.setCounty(this.getSelCustLocn().getCounty());
			updateCustLocn.setState(this.getSelCustLocn().getState());
			updateCustLocn.setCountry(this.getSelCustLocn().getCountry());
			updateCustLocn.setZip(this.getSelCustLocn().getZip());
			updateCustLocn.setZipplus4(this.getSelCustLocn().getZipplus4());
			
			billingTestingToolBean.custLocnSelectionChanged(updateCust, updateCustLocn);
			beanName = "";
			
			return "billing_test_tool";
		}
		
		return null;
	}
}
