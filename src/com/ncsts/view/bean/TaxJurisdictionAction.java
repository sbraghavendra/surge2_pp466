package com.ncsts.view.bean;

public enum TaxJurisdictionAction {
	addJurisdiction("Add a Jurisdiction"),
	copyAddJurisdiction("Copy/Add a Jurisdiction"),
	updateJurisdiction("Update a Jurisdiction"),
	deleteJurisdiction("Delete a Jurisdiction"),
	viewJurisdiction("View a Jurisdiction"),
	addJurisdictionTaxrate("Add a Tax Rate to a Jurisdiction"),
	copyAddJurisdictionTaxrate("Copy/Add a Tax Rate to a Jurisdiction"),
	updateJurisdictionTaxrate("Update a Tax Rate"),
	copyUpdateJurisdictionTaxrate("Copy/Update a Tax Rate"),
	deleteJurisdictionTaxrate("Delete a Tax Rate"),
	addJurisdictionTaxrateFromTransaction("Add a Tax Rate to a Jurisdiction from a Transaction"),
	copyAddJurisdictionTaxrateFromTransaction("Copy/Add a Tax Rate to a Jurisdiction from a Transaction"),
	viewJurisdictionTaxrateFromTransactionLog("View a Jurisdiction and Tax Rate"),
	viewJurisdictionTaxrateFromTransaction("View a Jurisdiction and Tax Rate"),
	viewJurisdictionFromTransaction("View a Jurisdiction"),
	viewJurisdictionTaxrate("View a Jurisdiction and Tax Rate"),
	viewJurisdictionFromTransactionLogHistory("View a Jurisdiction"),
	viewJurisdictionFromTransactionLog("View a Jurisdiction");
	
	
	private String actionText;

	TaxJurisdictionAction(String actionText) {
		this.actionText = actionText;
	}

	public String getActionText() {
		return actionText;
	}
	
	public boolean isShortDetailAction() {
		return (this.equals(addJurisdiction) || this.equals(updateJurisdiction) || this.equals(deleteJurisdiction));
	}
}
