package com.ncsts.view.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.component.html.HtmlTree;
import org.richfaces.event.NodeSelectedEvent;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.EntityDefault;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.EntityLocnSetDtl;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Menu;
import com.ncsts.domain.Role;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.dto.EntityDefaultDTO;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.EntityLocnSetDTO;
import com.ncsts.dto.EntityLocnSetDtlDTO;
import com.ncsts.jsf.model.EntityDataModel;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.EntityDefaultService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.EntityLocnSetDtlService;
import com.ncsts.services.EntityLocnSetService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.RoleService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserService;
import com.ncsts.view.bean.BaseMatrixBackingBean.FindIdCallback;
import com.ncsts.view.util.ConfigSetting;
import com.ncsts.view.util.TogglePanelController;

public class EntityBackingBean {
    
    private Logger logger = LoggerFactory.getInstance().getLogger(EntityBackingBean.class);
    
    @Autowired
	private NexusDefinitionBackingBean nexusDefinitionBackingBean;
    
    @Autowired
    private NexusDefRegBackingBean nexusDefRegBackingBean;
   
    @Autowired
    private CustomerLocationSearchBean custLoc;
    @Autowired
    protected MatrixCommonBean matrixCommonBean;
    @Autowired
    private GSBMatrixViewBean gsbMatrixBean;
    @Autowired
  	private TaxMatrixViewBean taxMatrix;
    private EntityDataModel entityDataModel;
    
	private EditAction currentAction = EditAction.VIEW;
    
    public static String MAP_MAPPED = "mapped";
    public static String MAP_SELECTED = "selected";
    public static String MAP_NONE = "";
    
    public static final String ROLE_EXCEPTION_TYPE = "";
    public static final String GRANTED_EXCEPTION_TYPE = "G";
    public static final String DENIED_EXCEPTION_TYPE = "D";
    
	private List<User> usersList = new ArrayList<User>();
	private List<Role> rolesList = new ArrayList<Role>();
	private List<Menu> menusList = new ArrayList<Menu>();	
	private List<EntityItem> entityItemsList = new ArrayList<EntityItem>();	
	private List<EntityDefault> entityDefaultList = new ArrayList<EntityDefault>();	
	private List<EntityLocnSet> entityLocnSetList = new ArrayList<EntityLocnSet>();	
	private List<EntityLocnSetDtl> entityLocnSetDtlList = new ArrayList<EntityLocnSetDtl>();	
	
	private List<EntityLevel> entityLevelsList = new ArrayList<EntityLevel>();	
	private List <EntityLevel> selectedEntityLevelList = new ArrayList<EntityLevel>();
	private List<DataDefinitionColumn> dataDefinitionColList = new ArrayList<DataDefinitionColumn>();
	private CacheManager cacheManager;
    private UserService userService;       
    private RoleService roleService;
    private EntityItemService entityItemService; 
    private EntityDefaultService entityDefaultService; 
    private EntityLocnSetService entityLocnSetService; 
    private EntityLocnSetDtlService entityLocnSetDtlService; 
    private EntityLevelService entityLevelService;
    private UserEntityService userEntityService;
    private DataDefinitionService dataDefinitionService;
    private String rerender; 
    private EntityItem selectedEntity;
    private EntityLevel selectedEntityLevel;
    private UserEntity selectedUserEntity;    
    private String mode;
    private boolean transColDisplay=false;
    private boolean displayUpdateButton = false;
    private boolean displayAddButton = false;
    private boolean updateDisplay=false;
    private boolean disableDeleteButton=false;
    private boolean showUpdateButton=false;
    private boolean showOkButton = false;
	private EntityItemDTO entityItemDTO;
	private ConfigSetting userConfig;
	private int selectedRowIndex = -1;
	private int selectedEntityDefaultRowIndex = -1;
	private int selectedLocationListRowIndex = -1;
	private int selectedLocationListDetailRowIndex = -1;
	private Long currentAddChangeEntityId = 0L;
	private loginBean loginBean;
	private UserSecurityBean userSecurityBean;
    private TreeNode rootNode = null;
    private EntityItem selectedEntityItem = null;
    private EntityItem selectedGridEntityItem = null;
    private EntityDefaultDTO entityDefaultDTO;
    private EntityLocnSetDTO entityLocnSetDTO;
    private EntityLocnSetDtlDTO entityLocnSetDtlDTO;
    private EntityDefault selectedEntityDefault = null;
    private EntityLocnSet selectedEntityLocnSet = null;
    private EntityLocnSetDtl selectedEntityLocnSetDtl = null;  
    private Map<Long, EntityItem > globalEntityArray = new LinkedHashMap<Long, EntityItem >();
    private Map<Long, String > entityLevelMap = null;
    private Map<Long, String > entityItemMap = null;
    private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
    private SearchJurisdictionHandler selectedHandler = new SearchJurisdictionHandler();
    private JurisdictionService jurisdictionService;
    private boolean entityLocked = false;
    private Long entityCopyFromSelected = null;
    private Long entityParentSelected = null;
    private List<SelectItem> copyFromList = null;
    private List<SelectItem> entityCopyFromList = null;
    private List<SelectItem> entityParentList = null;
    private List<SelectItem> entityLevelList = null;
    private List<SelectItem> defaultTypeList = null; 
    private List<SelectItem> defaultCodeList = null;
    private List<SelectItem> defaultValueList = null;
    private List<SelectItem> pickComponentListOptions = null;
    private List<String> pickComponentListResult = new ArrayList<String>();
    private List<SelectItem> pickEntityListOptions = null;
    private List<String> pickEntityListResult = new ArrayList<String>();
    private List<String> pickComponentListUpdateResult = new ArrayList<String>();
    private List<SelectItem> pickComponentListUpdateOptions = null;
    private List<SelectItem> pickShipFromEntityListOptions = null;
    private List<SelectItem> pickPOOriginEntityListOptions = null;
    private List<SelectItem> pickPOAcceptanceEntityListOptions = null;
    private String showhide="";
	private FindIdCallback findIdCallback = null;
	private String findIdAction;
	private String findIdContext;
    private String selectedRowKey =null;
    private String returnView=null;
    private TaxJurisdictionBackingBean taxJurisdictionBean;
    
	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(
			TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}

	public String getReturnView() {
		return returnView;
	}

	public void setReturnView(String returnView) {
		this.returnView = returnView;
	}

	public String getSelectedRowKey(){
    	return selectedRowKey;
    }
    
    private enum EntityDefaultAction  {
		closeEntityDefaultMain,
		displayAddSingle,
		displayUpdateSingle,
		displayDeleteSingle,
		okSingle,
		cancelSingle,
		closeSingleMain,
		displayAddLocationSet,
		displayUpdateLocationSet,
		displayDeleteLocationSet,
		okLocationSet,
		cancelLocationSet,
		displayAddLocationSetDetail,
		displayUpdateLocationSetDetail,
		displayDeleteLocationSetDetail,
		okLocationSetDetail,
		cancelLocationSetDetail,
		closeLocationSetDetailMain
	}
    
    public void setEntityDataModel(EntityDataModel entityDataModel) {
		this.entityDataModel = entityDataModel;
	}
	
	public EntityDataModel getEntityDataModel() {
		return entityDataModel;
	}

	public void findId(FindIdCallback callback, String context, String action,
			EntityItem entityFilter) {
		findIdCallback = callback;
		findIdContext = context;
		findIdAction = action;
		if (entityFilter != null) {
			entityFilter.setEntityCodeCallback(entityFilter.getEntityCode());
					
		}
		resetFilterSearchAction();
		retrieveFilterSearchAction();
	}

	public String findAction() {
		 if(selectedGridEntityItem!=null && selectedGridEntityItem.getEntityId()!=null){
			findIdCallback.findIdCallback(selectedGridEntityItem.getEntityId(),findIdContext);
			}
			else{
			findIdCallback.findIdCallback(999999999L,findIdContext);
			}
	
		exitFindMode();
		return findIdAction;
	}

	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}

	public boolean getFindMode() {
		return (findIdCallback != null);
	}

	public void exitFindMode() {
		if (findIdCallback != null) {
			findIdCallback = null;
		}
	}

	public String navigateAction() {
		exitFindMode();
		return "entity_main";
	}
    public List<String> getPickComponentListResult(){
    	return this.pickComponentListResult;
    }
    
    public void setPickComponentListResult(List<String> pickComponentListResult){
    	this.pickComponentListResult = pickComponentListResult;
    }
    
    public List<String> getPickComponentListUpdateResult(){
    	return this.pickComponentListUpdateResult;
    }
    
    public void setPickComponentListUpdateResult(List<String> pickComponentListUpdateResult){
    	this.pickComponentListUpdateResult = pickComponentListUpdateResult;
    }
    
    public List<SelectItem> getPickComponentListOptions(){
    	if(pickComponentListOptions==null){
    		pickComponentListOptions = new ArrayList<SelectItem>();
    		pickComponentListOptions.add(new SelectItem(new Long(1L),"Defaults"));
    		pickComponentListOptions.add(new SelectItem(new Long(2L),"Nexus"));
    		pickComponentListOptions.add(new SelectItem(new Long(3L),"Registrations"));
    	}
    	return pickComponentListOptions;
    }
    
    public List<SelectItem> getPickComponentListUpdateOptions(){
    	if(pickComponentListUpdateOptions==null){
    		pickComponentListUpdateOptions = new ArrayList<SelectItem>();
    		pickComponentListUpdateOptions.add(new SelectItem(new Long(1L),"Defaults"));
    		pickComponentListUpdateOptions.add(new SelectItem(new Long(2L),"Nexus"));
    	}
    	return pickComponentListUpdateOptions;
    }
    
    public List<String> getPickEntityListResult(){
    	return this.pickEntityListResult;
    }
    
    public void setPickEntityListResult(List<String> pickEntityListResult){
    	this.pickEntityListResult = pickEntityListResult;
    }
    
    public List<SelectItem> getPickEntityListOptions(){
    	if(pickEntityListOptions==null){
    		pickEntityListOptions = new ArrayList<SelectItem>();
    		
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(!entityItem.getEntityId().equals(selectedGridEntityItem.getEntityId()) && entityItem.getLockBooleanFlag()==false){
    	    			pickEntityListOptions.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    		}
    	    	}
    	    } 		
    	}
    	return pickEntityListOptions;
    }
 
    public List<SelectItem> getPickShipFromEntityListOptions(){
    	if(pickShipFromEntityListOptions==null){
    		pickShipFromEntityListOptions = new ArrayList<SelectItem>();
    		pickShipFromEntityListOptions.add(new SelectItem(new Long(-1L),"Select a Ship From Entity"));
    	
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		pickShipFromEntityListOptions.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    	}
    	    } 		
    	}
    	return pickShipFromEntityListOptions;
    }
    
    public List<SelectItem> getPickPOOriginEntityListOptions(){
    	if(pickPOOriginEntityListOptions==null){
    		pickPOOriginEntityListOptions = new ArrayList<SelectItem>();	
    		pickPOOriginEntityListOptions.add(new SelectItem(new Long(-1L),"Select a Purchase Order Origin Entity"));
    		
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		pickPOOriginEntityListOptions.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    	}
    	    } 		
    	}
    	return pickPOOriginEntityListOptions;
    }
    
    public List<SelectItem> getPickPOAcceptanceEntityListOptions (){
    	if(pickPOAcceptanceEntityListOptions==null){
    		pickPOAcceptanceEntityListOptions = new ArrayList<SelectItem>();
    		pickPOAcceptanceEntityListOptions.add(new SelectItem(new Long(-1L),"Select a Purchase Order Acceptance Entity"));
    		
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		pickPOAcceptanceEntityListOptions.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    	}
    	    } 		
    	}
    	return pickPOAcceptanceEntityListOptions;
    }
    
    public void onpicklistchangedAction(ActionEvent e){
	}
    
    public EntityBackingBean() {
	}
    
    public void init() {
    	EntityItem entityItem = entityItemService.findById(new Long(0L));  
    	if(entityItem==null || !entityItem.getEntityId().equals(0L)){
    		//Create All entity
    		entityItem = new EntityItem();
    		
    		entityItem.setEntityId(0L);
			entityItem.setParentEntityId(0L);
			entityItem.setEntityLevelId(0L);
			entityItem.setEntityCode("*ALL");
			entityItem.setEntityName("All Entities");
			entityItem.setDescription("All Entities");
			entityItem.setAddressLine1("");
			entityItem.setAddressLine2("");
			entityItem.setLockFlag("0");
			entityItem.setActiveFlag("1");
			
			currentAddChangeEntityId = entityItemService.persist(entityItem);
    	}
    	
    	filterHandlerShipto.reset();
		filterHandlerShipto.setCountry(matrixCommonBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipto.setState("");

    }
    
    public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		filterHandlerShipto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerShipto.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerShipto.setTaxJurisdictionBean(taxJurisdictionBean);
		filterHandlerShipto.setCallbackScreen("entity_main");
		
		selectedHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		selectedHandler.setCacheManager(matrixCommonBean.getCacheManager());
		selectedHandler.setTaxJurisdictionBean(taxJurisdictionBean);
		selectedHandler.setCallbackScreen("entity_update");
	}
    
    public Map<Long, String > getEntityLevelMap(){
    	if(entityLevelMap==null){
    		entityLevelMap = new LinkedHashMap<Long, String >();

    		List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMap.put(entityLevel.getEntityLevelId(), entityLevel.getDescription());
			}
    	}
    	return entityLevelMap;
    }
    
    public Map<Long, String > getEntityItemMap(){
    	if(entityItemMap==null){
    		entityItemMap = new LinkedHashMap<Long, String >();
    		
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		entityItemMap.put(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName());
    	    	}
    	    } 
    	}
    	return entityItemMap;
    }
    
    public List<SelectItem> getEntityLevelList(){
    	return entityLevelList;
    }
    
    private Long selectedDefaultTypeList = 1L;
    
    public Long getSelectedDefaultTypeList(){
    	return this.selectedDefaultTypeList;
    }
    
    public void setSelectedDefaultTypeList(Long selectedDefaultTypeList){
    	this.selectedDefaultTypeList=selectedDefaultTypeList;
    }
    
    public List<SelectItem> getDefaultTypeList(){
    	if(defaultTypeList==null){
    		defaultTypeList = new ArrayList<SelectItem>();
    		//defaultTypeList.add(new SelectItem(new Long(0L),"Select a Default Type"));
    		defaultTypeList.add(new SelectItem(new Long(1L),"Single Values"));
    		defaultTypeList.add(new SelectItem(new Long(2L),"Location Sets"));
    	}
    	return defaultTypeList;
    }
    
    public List<SelectItem> getDefaultCodeList(){
    	return defaultCodeList;
    }
    
    public List<SelectItem> getDefaultValueList(){
    	if(defaultValueList==null){
    		defaultValueList = new ArrayList<SelectItem>();
    		//defaultValueList.add(new SelectItem(new Long(0L),"Select a Default Value"));
    		defaultValueList.add(new SelectItem(new Long(1L),"Single Values"));
    		defaultValueList.add(new SelectItem(new Long(2L),"Location Sets"));
    	}
    	return defaultValueList;
    }
    
    public String defaultTypeChange() {
		return null;
	}
    
    public boolean getIsEntityDefaultTableMainPanel(){
		if(selectedDefaultTypeList==0L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public boolean getIsEntityDefaultListTablePanel(){
		if(selectedDefaultTypeList==1L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public boolean getIsLocationListTablePanel(){
		if(selectedDefaultTypeList==2L){
			return true;
		}
		else{
			return false;
		}
	}
    
    public void rebuildEntityLevelList(Long parentEntityLevel, boolean isAll){
    	entityLevelList = new ArrayList<SelectItem>();
    	if(isAll){
    		entityLevelList.add(new SelectItem(new Long(0L),"ALL Entities"));
    	}
    	else if(parentEntityLevel.intValue()==0){
    		entityLevelList.add(new SelectItem(new Long(1L),"Company"));
    	}
    	else if(parentEntityLevel.intValue()==1){
    		entityLevelList.add(new SelectItem(new Long(2L),"Division"));
    		entityLevelList.add(new SelectItem(new Long(3L),"Location"));
    	}
    	else if(parentEntityLevel.intValue()==2){
    		entityLevelList.add(new SelectItem(new Long(3L),"Location"));
    	}
    }
    
    public List<SelectItem> getEntityCopyFromList(){
    	if(entityCopyFromList==null){
    		entityCopyFromList = new ArrayList<SelectItem>();
    		entityCopyFromList.add(new SelectItem(new Long(-1L),"Select Entity to Copy From"));
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		entityCopyFromList.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    	}
    	    } 		
    	}
    	return entityCopyFromList;
    }
    
    public List<SelectItem> getEntityParentList(){
    	if(entityParentList==null){
    		entityParentList = new ArrayList<SelectItem>();
    		List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityLevelId()<3L){
    	    			entityParentList.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    		}
    	    	}
    	    } 		
    	}
    	return entityParentList;
    }
    
    public List<SelectItem> getCopyFromList(){
    	if(copyFromList==null){
    		copyFromList = new ArrayList<SelectItem>();
    		copyFromList.add(new SelectItem(new Long(1L),"Defaults"));
    		copyFromList.add(new SelectItem(new Long(2L),"Nexus"));
    		copyFromList.add(new SelectItem(new Long(3L),"Registrations"));
    	}
    	return copyFromList;
    }
    
    public Long getEntityCopyFromSelected(){
    	return entityCopyFromSelected;
    }
    
    public void setEntityCopyFromSelected(Long entityCopyFromSelected){
    	this.entityCopyFromSelected = entityCopyFromSelected;
    }
    
    public Long getEntityParentSelected(){
    	return entityParentSelected;
    }
    
    public void setEntityParentSelected(Long entityParentSelected){
    	this.entityParentSelected = entityParentSelected;
    }
    
    public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
		
		filterHandlerShipto.setJurisdictionService(getJurisdictionService());
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}
	
	public void resetFilter() {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
	}

    public String getEntityNameText() {
    	if(selectedEntityItem!=null){
    		return selectedEntityItem.getEntityCode() + " - " + selectedEntityItem.getEntityName();
    	}
    	else{
    		return "";
    	}
	}
    
    public void selectedEntityRowChanged (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedRowIndex = table.getRowIndex();
		selectedGridEntityItem = (EntityItem)table.getRowData();
	}	
    
    public void selectedEntityDefaultRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityDefaultRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedEntityDefaultRowIndex = table.getRowIndex();
		selectedEntityDefault = (EntityDefault)table.getRowData();
	}	
    
    public void selectedLocationListRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedLocationListRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedLocationListRowIndex = table.getRowIndex();
		selectedEntityLocnSet = (EntityLocnSet)table.getRowData();
		
		displayLocationDtlAction();
	}	
    
    public void selectedLocationListDetailRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedLocationListDetailRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) .getParent(); 
		
		selectedLocationListDetailRowIndex = table.getRowIndex();
		selectedEntityLocnSetDtl = (EntityLocnSetDtl)table.getRowData();
	}
  
    private void addNodes(String path, TreeNode node, Long parentId) {
        //Add to local array
        Map<Long, EntityItem > localEntityArray = new LinkedHashMap<Long, EntityItem >();
    	for (Long entityKey : globalEntityArray.keySet()) {
    		EntityItem entityItem = (EntityItem)globalEntityArray.get(entityKey);
    		if(parentId.equals(entityItem.getParentEntityId())){
    			localEntityArray.put(entityKey, entityItem);
			}
    	}
    	
    	//Remove 
    	for (Long entityKey : localEntityArray.keySet()) {
    		globalEntityArray.remove(entityKey);
    	}
    	
        //Build
        for(EntityItem tcd : localEntityArray.values()) {
        	TreeNodeImpl nodeImpl = new TreeNodeImpl();
            nodeImpl.setData(tcd);
            node.addChild(tcd.getEntityId(), nodeImpl);

            addNodes(null, nodeImpl, tcd.getEntityId());		
		}
    }

    private void loadTree() {
        try {
        	loadEntity();
        	
        	EntityItem rootEntityItem = (EntityItem)globalEntityArray.get(new Long(0));
            
            rootNode = new TreeNodeImpl();
            rootNode.setData(rootEntityItem);
              
            TreeNodeImpl nodeImpl = new TreeNodeImpl();
            nodeImpl.setData(rootEntityItem);
            rootNode.addChild(rootEntityItem.getEntityId(), nodeImpl);
            
            globalEntityArray.remove(new Long(0));
            addNodes(null, nodeImpl, new Long(0));
            
        } catch (Exception e) {
            throw new FacesException(e.getMessage(), e);
        } finally {

        }
    }
    
    public void processSelection(NodeSelectedEvent event) {
        HtmlTree tree = (HtmlTree) event.getComponent();
        EntityItem entityItem = (EntityItem) tree.getRowData();
        if(rootNode!=null){
        	TreeNode treeNode = rootNode.getChild(entityItem.getEntityId());
        	if(treeNode!=null){
        		EntityItem aEntityItem = (EntityItem)treeNode.getData();
        	}	
        }
        
        if(entityItem.getType().equalsIgnoreCase(MAP_MAPPED) || entityItem.getType().equalsIgnoreCase(MAP_SELECTED)){
        }
        else{
        	if(selectedEntityItem!=null){
            	selectedEntityItem.setType(MAP_NONE);
            }
        	entityItem.setType(MAP_SELECTED);
        	
        	//Set rowKey only through tree selection
    		selectedEntityItem = entityItem;
    		Object rowKey = tree.getRowKey();
    		selectedRowKey = rowKey.toString();
        }
        
        loadEntityItemsList(selectedEntityItem.getEntityId());
    }
    
    public void setSelectedEntityItem(EntityItem selectedEntityItem){
		this.selectedEntityItem = selectedEntityItem;
		this.selectedRowKey = null;
	}
	
	public EntityItem getSelectedEntityItem(){
		return this.selectedEntityItem;
	}
    
    public EntityItem findEntityItemById(TreeNode parent, Long ID){
    	EntityItem entityItem = null;
    	Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
        while (it!=null &&it.hasNext()) {
            Map.Entry<Object, TreeNode> entry = it.next();
            
            if(ID.equals(entry.getKey())){
            	entityItem = (EntityItem)entry.getValue().getData();
            	break;
            }
            
            entityItem = findEntityItemById(entry.getValue(), ID);
            if(entityItem!=null){
            	break;
            }
        }
        
        return entityItem;
    }
    
    public TreeNode findTreeNodeById(TreeNode parent, Long ID){
    	TreeNode treeNode = null;
    	Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
        while (it!=null &&it.hasNext()) {
            Map.Entry<Object, TreeNode> entry = it.next();
            
            if(ID.equals(entry.getKey())){
            	treeNode = (TreeNode)entry.getValue();
            	break;
            }
            
            treeNode = findTreeNodeById(entry.getValue(), ID);
            if(treeNode!=null){
            	break;
            }
        }
        
        return treeNode;
    }
    
    private void loadEntity(){
    	List<EntityItem> eList = entityItemService.findAllEntityItems();//source list
	    if(globalEntityArray!=null){
	    	globalEntityArray.clear();
	    }
	    globalEntityArray = new LinkedHashMap<Long, EntityItem >();
	    
	    if(eList != null && eList.size() > 0){
	    	for(EntityItem entityItem:eList){
	    		entityItem.setType(MAP_NONE);//Map first, later unmap.
	    		globalEntityArray.put(entityItem.getEntityId(), entityItem);
	    	}
	    } 			   

	    selectedEntityItem = null;
		selectedRowKey = null;
    }
    
    public TreeNode getTreeNode() {
        if (rootNode == null) {
            loadTree();
        }
        
        return rootNode;
    }
    
    public boolean getAddAction() {
		return currentAction.equals(EditAction.ADD);
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getDisableActiveFlag() {
		return !selectedEntityItem.getActiveBooleanFlag();
	}
	
	public boolean getIsEntitlLevel3() {
		if(entityItemDTO!=null && entityItemDTO.getEntityLevelId()!=null && !entityItemDTO.getEntityLevelId().equals(3L)){
			return false;
		}
		else{
			return true;
		}
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	
	public boolean getCopyAddAction() {
		return currentAction.equals(EditAction.COPY_ADD);
	}
	
	public boolean getEntityLocked() {
		return this.entityLocked;
	}
	
	public void setEntityLocked(boolean entityLocked) {
		this.entityLocked = entityLocked;
	}
	
	public String getActionText() {
		if (currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
		}
		else if (currentAction.equals(EditAction.UPDATE)) {
			return "Update";
		}
		else if (currentAction.equals(EditAction.ADD)) {
			return "Add";
		}
		else if (currentAction.equals(EditAction.DELETE)) {
			return "Delete";
		}
		else if (currentAction.equals(EditAction.VIEW)) {
			return "View";
		}
		else{
			return "";
		}
	}
	
	public boolean getDisableAdd(){
		if(selectedEntityItem!=null && selectedEntityItem.getEntityLevelId()!=null && selectedEntityItem.getEntityLevelId().intValue()<3){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getSelectedEntityLocked(){
		if(selectedGridEntityItem!=null && selectedGridEntityItem.getLockBooleanFlag()==false){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableCopyAdd(){
		if(selectedGridEntityItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableUpdate(){
		if(selectedGridEntityItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableCopyTo(){
		if(selectedGridEntityItem!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDelete(){
		if(selectedGridEntityItem!=null){
			if((selectedGridEntityItem.getEntityId().equals(0L))){
				//All Entities. No delete.
				return true;
			}
			
			List<EntityItem> list = entityItemService.findAllEntityItemsByParent(selectedGridEntityItem.getEntityId());
			if(list!=null && list.size()>0){
				return true;
			}

			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDefaultUpdate(){
		if(selectedEntityDefault!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableDefaultDelete(){
		if(selectedEntityDefault!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationUpdate(){
		if(selectedEntityLocnSet!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationDtlAdd(){
		if(selectedEntityLocnSet!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisableLocationDtlUpdate(){
		if(selectedEntityLocnSetDtl!=null){
			return false;
		}
		else{
			return true;
		}
	}
	
	public String validateEntityCode(String code, Long parentId) {
	    // Ensure code is unique and not empty	
		String errorStr = "";
		if(code == null || code.length() <= 0){
			errorStr = "Entity Code cannot be empty.";
			
	    }else if (currentAction.isAddAction() && (entityItemService.findByCode(code) != null)) {
	    	errorStr = "Entity Code already exists.";
	    }
		return errorStr;
	}
	
	public String validateEntityName(String name, Long parentId) {
	    // Ensure code is unique and not empty	
		String errorStr = "";
		if(name == null || name.length() <= 0){
			errorStr = "Entity Name cannot be empty.";
			
	    }else if (currentAction.isAddAction() && (entityItemService.findByName(name) != null)) {
	    	errorStr = "Entity Name already exists.";
	    }
		return errorStr;
	}
	
	public void onParentEntityChange(ValueChangeEvent eventValue) {

		Long newValue = (Long) eventValue.getNewValue();

		entityParentSelected = newValue;
		
		selectedEntityItem = entityItemService.findById(entityParentSelected);
				
		if(selectedEntityItem!=null){
			entityItemDTO.setParentLevelDescription(entityLevelMap.get(selectedEntityItem.getEntityLevelId()));
			entityItemDTO.setParentCode(selectedEntityItem.getEntityCode());
			entityItemDTO.setParentName(selectedEntityItem.getEntityName());
			entityItemDTO.setParentDescription(selectedEntityItem.getDescription());
			entityItemDTO.setParentActiveFlag(selectedEntityItem.getActiveFlag());
		
			//This level
			entityItemDTO.setEntityLevelId(new Long(selectedEntityItem.getEntityLevelId().intValue()+1));
			entityItemDTO.setParentEntityId(selectedEntityItem.getEntityId());
			entityItemDTO.setActiveFlag(selectedEntityItem.getActiveFlag());
			entityItemDTO.setExemptGraceDays(null);
			
			//Rebuild entity level selection
			rebuildEntityLevelList(selectedEntityItem.getEntityLevelId(),false);
		}
	}
	
	public String displayAddAction() {
		return displayAddAction(null);
	}
	
	public String displayAddAction(EntityItem entityItem) {
		//0003704: Enhance Add an Entity
		selectedEntityItem = null;
		
		if(entityItem==null){	
			if(selectedGridEntityItem!=null && selectedGridEntityItem.getEntityLevelId()!=null && selectedGridEntityItem.getEntityLevelId().intValue()<3){
				selectedEntityItem = selectedGridEntityItem;
			}
			else{
				selectedEntityItem =  entityItemService.findById(0L);
			}
			
			if(selectedEntityItem==null){
				return "";
			}
		}
		else{
			//OK & Add Child 
			selectedEntityItem = entityItem;
		}
		
		//Default to parent
		entityParentSelected = selectedEntityItem.getEntityId();
		if(entityParentList!=null){
			entityParentList.clear();
			entityParentList = null;
		}
		
		if(entityCopyFromList!=null){
			entityCopyFromList.clear();
			entityCopyFromList = null;
		}
		
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = EditAction.ADD;
		
		entityItemDTO = new EntityItemDTO();
		
		//Parent level
		entityItemDTO.setParentLevelDescription(entityLevelMap.get(selectedEntityItem.getEntityLevelId()));
		entityItemDTO.setParentCode(selectedEntityItem.getEntityCode());
		entityItemDTO.setParentName(selectedEntityItem.getEntityName());
		entityItemDTO.setParentDescription(selectedEntityItem.getDescription());
		entityItemDTO.setParentActiveFlag(selectedEntityItem.getActiveFlag());
	
		//This level
		entityItemDTO.setEntityLevelId(new Long(selectedEntityItem.getEntityLevelId().intValue()+1));
		entityItemDTO.setParentEntityId(selectedEntityItem.getEntityId());
		
		//Rebuild entity level selection
		rebuildEntityLevelList(selectedEntityItem.getEntityLevelId(),false);
		
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		entityItemDTO.setJurisdictionId(null);
		entityItemDTO.setGeocode("");
		entityItemDTO.setCountry("");
		entityItemDTO.setState("");
		entityItemDTO.setCounty("");
		entityItemDTO.setCity("");
		entityItemDTO.setZip("");
		
		//When a new Entity is added to an Inactive Entity, the new Entity will be set to Inactive and the field protected.
		entityItemDTO.setActiveFlag(selectedEntityItem.getActiveFlag());
		
		filterHandler.setGeocode(filterHandlerShipto.getGeocode());
		filterHandler.setCountry(filterHandlerShipto.getCountry());
		filterHandler.setState(filterHandlerShipto.getState());
		filterHandler.setCounty(filterHandlerShipto.getCounty());
		filterHandler.setCity(filterHandlerShipto.getCity());
		filterHandler.setZip(filterHandlerShipto.getZip());
		filterHandler.setZipPlus4(filterHandlerShipto.getZipPlus4());
		
		entityLocked = false;
		
		//Reset copy from data
		//Default to parent
		entityCopyFromSelected = selectedEntityItem.getEntityId();
		
		pickComponentListUpdateResult.clear();


		return "entity_update";
	}
	
	public String addAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			String errorMessage = "";
			String errorTemp = "";
			
			//Validate fields
			errorTemp = validateEntityCode(entityItemDTO.getEntityCode(),entityItemDTO.getParentEntityId());
			if(errorTemp!=null && errorTemp.length()>0){
				errorMessage = errorMessage + " " + errorTemp;
			}
			
			errorTemp = validateEntityName(entityItemDTO.getEntityName(),entityItemDTO.getParentEntityId());
			if(errorTemp!=null && errorTemp.length()>0){
				errorMessage = errorMessage + " " + errorTemp;
			}
			
			//Validate full and partial Jurisdiction		
			boolean validJurisdiction = false;
			Jurisdiction aJurisdiction = null;
			if(filterHandler.getHasSomeInput()){
				aJurisdiction = filterHandler.getJurisdictionFromDB();
				if(aJurisdiction!=null){
					validJurisdiction = true;
				}
				else{
					validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
				}
				
				if(!validJurisdiction){
					errorMessage = errorMessage + " " + "An Invalid Jurisdiction was entered.";
				}
			}
			
			if(errorMessage!=null && errorMessage.length()>0){
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}

			EntityItem entityItem = new EntityItem();
			entityItem.setParentEntityId(entityItemDTO.getParentEntityId());
			entityItem.setEntityLevelId(entityItemDTO.getEntityLevelId());
			entityItem.setEntityCode(entityItemDTO.getEntityCode());
			entityItem.setFein(entityItemDTO.getFein());
			entityItem.setEntityName(entityItemDTO.getEntityName());
			
			entityItem.setDescription(entityItemDTO.getDescription());
			entityItem.setAddressLine1(entityItemDTO.getAddressLine1());
			entityItem.setAddressLine2(entityItemDTO.getAddressLine2());
			
			entityItem.setLockFlag(entityLocked ? "1" : "0");
			entityItem.setActiveFlag(entityItemDTO.getActiveFlag());
			entityItem.setExemptGraceDays(entityItemDTO.getExemptGraceDays());
			
			if(aJurisdiction!=null){
				entityItem.setJurisdictionId(aJurisdiction.getJurisdictionId());
			}
			else{
				entityItem.setJurisdictionId(null);
			}
			Jurisdiction localJurisdiction = filterHandler.getJurisdiction();
			entityItem.setGeocode(localJurisdiction.getGeocode());
			entityItem.setCountry(localJurisdiction.getCountry());
			entityItem.setState(localJurisdiction.getState());
			entityItem.setCounty(localJurisdiction.getCounty());
			entityItem.setCity(localJurisdiction.getCity());
			entityItem.setZip(localJurisdiction.getZip());

			currentAddChangeEntityId = entityItemService.persist(entityItem);
			entityItem.setEntityId(currentAddChangeEntityId);
			
			//Reload combox
			retrieveComboBoxes(entityItem.getEntityLevelId());
			
			//Reload datagrid
			retrieveFilterSearchAction();
			
			//Copy components from other entity
			Long fromEntityId = entityCopyFromSelected;
			Long toEntityId = currentAddChangeEntityId;
			
			//pickComponentListUpdateResult
			String selectedComponent = "00000000";
			if(entityLocked){
				entityItemService.copyEntityComponents(fromEntityId, 0L, selectedComponent);
			}
			else{
				if(pickComponentListUpdateResult!=null && pickComponentListUpdateResult.size()>0){
					for(int i=0; i<pickComponentListUpdateResult.size(); i++){
						String componentId = (String)pickComponentListUpdateResult.get(i);
						Long selectId = Long.valueOf(componentId);
						selectedComponent = selectedComponent.substring(0, selectId.intValue()-1) + "1" + selectedComponent.substring(selectId.intValue(), selectedComponent.length());
					}
					
					entityItemService.copyEntityComponents(fromEntityId, toEntityId, selectedComponent);
				}
			}
			
			selectedRowIndex = -1;
			selectedGridEntityItem = null;
			
		    result = "entity_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end addAction");	    
        return result;
	}	
	
	public String displayUpdateAction() {
		return displayUpdateAction(EditAction.UPDATE);
	}
	
	public String displayViewAction() {
		return displayUpdateAction(EditAction.VIEW);
	}
	
	public String displayUpdateAction(EditAction editAction) {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		if(entityCopyFromList!=null){
			entityCopyFromList.clear();
			entityCopyFromList = null;
		}
		
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = editAction;
		
		entityItemDTO = new EntityItemDTO();
		
		//Parent level
		entityItemDTO.setParentLevelDescription(entityLevelMap.get(selectedEntityItem.getEntityLevelId()));
		entityItemDTO.setParentCode(selectedEntityItem.getEntityCode());
		entityItemDTO.setParentName(selectedEntityItem.getEntityName());
		entityItemDTO.setParentDescription(selectedEntityItem.getDescription());
		entityItemDTO.setParentActiveFlag(selectedEntityItem.getActiveFlag());
	
		//This level
		entityItemDTO.setEntityLevelId(selectedGridEntityItem.getEntityLevelId());
		entityItemDTO.setParentEntityId(selectedGridEntityItem.getParentEntityId());
		
		//Rebuild entity level selection
		rebuildEntityLevelList(selectedEntityItem.getEntityLevelId(), (selectedGridEntityItem.getEntityId().equals(0L) ? true : false));
		
		if(currentAction.equals(EditAction.UPDATE) || currentAction.equals(EditAction.VIEW)){
			//If there is a location child under this entity, no location selection at entity level selections.
			if(selectedEntityItem.getEntityLevelId().intValue()==1){
				List<EntityItem> list = entityItemService.findAllEntityItemsByParent(selectedGridEntityItem.getEntityId());
				if(list!=null && list.size()>0){
					entityLevelList = new ArrayList<SelectItem>();
					entityLevelList.add(new SelectItem(new Long(2L),"Division"));
				}
			}
		}
		
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
		entityItemDTO.setFein(selectedGridEntityItem.getFein());
		entityItemDTO.setAddressLine1(selectedGridEntityItem.getAddressLine1());
		entityItemDTO.setAddressLine2(selectedGridEntityItem.getAddressLine2());
		entityItemDTO.setZipPlus4(selectedGridEntityItem.getZipPlus4());	
		entityItemDTO.setActiveFlag(selectedGridEntityItem.getActiveFlag());
		
		if(selectedGridEntityItem.getLockFlag()!=null && selectedGridEntityItem.getLockFlag().equals("1")){
			entityLocked = true;
		}
		else{
			entityLocked = false;
		}
		entityItemDTO.setLockFlag(selectedGridEntityItem.getLockFlag());
		
		//Set Jurisdiction filter
		resetFilter();
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		entityItemDTO.setJurisdictionId(selectedGridEntityItem.getJurisdictionId());
		entityItemDTO.setGeocode(selectedGridEntityItem.getGeocode());
		entityItemDTO.setCountry(selectedGridEntityItem.getCountry());
		entityItemDTO.setState(selectedGridEntityItem.getState());
		entityItemDTO.setCounty(selectedGridEntityItem.getCounty());
		entityItemDTO.setCity(selectedGridEntityItem.getCity());;
		entityItemDTO.setZip(selectedGridEntityItem.getZip());
		entityItemDTO.setUpdateUserId(selectedGridEntityItem.getUpdateUserId());
		entityItemDTO.setUpdateTimestamp(selectedGridEntityItem.getUpdateTimestamp());
		entityItemDTO.setExemptGraceDays(selectedGridEntityItem.getExemptGraceDays());
		
		filterHandler.setGeocode(selectedGridEntityItem.getGeocode());
		filterHandler.setCountry(selectedGridEntityItem.getCountry());
		filterHandler.setState(selectedGridEntityItem.getState());
		filterHandler.setCounty(selectedGridEntityItem.getCounty());
		filterHandler.setCity(selectedGridEntityItem.getCity());
		filterHandler.setZip(selectedGridEntityItem.getZip());
		
		//Reset copy from data
		entityCopyFromSelected = entityItemDTO.getEntityId();
		pickComponentListUpdateResult.clear();

		return "entity_update";
	}	
	
	public String updateAction() {
		String result = null;
		logger.info("start updateAction"); 
		try {
			String errorMessage = "";
			String errorTemp = "";
			
			//Validate full and partial Jurisdiction		
			boolean validJurisdiction = false;
			Jurisdiction aJurisdiction = null;
			if(filterHandler.getHasSomeInput()){
				aJurisdiction = filterHandler.getJurisdictionFromDB();
				if(aJurisdiction!=null){
					validJurisdiction = true;
				}
				else{
					validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
				}
				
				if(!validJurisdiction){
					errorMessage = errorMessage + " " + "An Invalid Jurisdiction was entered.";
				}
			}
			
			if(errorMessage!=null && errorMessage.length()>0){
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}

			EntityItem entityItem = new EntityItem();
			entityItem.setParentEntityId(entityItemDTO.getParentEntityId());
			entityItem.setEntityLevelId(entityItemDTO.getEntityLevelId());
			entityItem.setEntityId(entityItemDTO.getEntityId());
			entityItem.setEntityCode(entityItemDTO.getEntityCode());
			entityItem.setEntityName(entityItemDTO.getEntityName());
			entityItem.setDescription(entityItemDTO.getDescription());
			entityItem.setFein(entityItemDTO.getFein());
			entityItem.setAddressLine1(entityItemDTO.getAddressLine1());
			entityItem.setAddressLine2(entityItemDTO.getAddressLine2());
			entityItem.setZipPlus4(entityItemDTO.getZipPlus4());
			entityItem.setActiveFlag(entityItemDTO.getActiveFlag());
			entityItem.setExemptGraceDays(entityItemDTO.getExemptGraceDays());
			
			//entityItem.setLockFlag(entityItemDTO.getLockFlag());
			entityItem.setLockFlag(entityLocked ? "1" : "0");
		
			if(aJurisdiction!=null){
				entityItem.setJurisdictionId(aJurisdiction.getJurisdictionId());
			}
			else{
				entityItem.setJurisdictionId(null);
			}
			Jurisdiction localJurisdiction = filterHandler.getJurisdiction();
			entityItem.setGeocode(localJurisdiction.getGeocode());
			entityItem.setCountry(localJurisdiction.getCountry());
			entityItem.setState(localJurisdiction.getState());
			entityItem.setCounty(localJurisdiction.getCounty());
			entityItem.setCity(localJurisdiction.getCity());
			entityItem.setZip(localJurisdiction.getZip());

			entityItemService.saveOrUpdate(entityItem);

			//When an Entity is changed from Active to Inactive, all sub-Entities of the changed Entity will be changed to Inactive.
			if(selectedGridEntityItem.getActiveBooleanFlag()==true && entityItem.getActiveBooleanFlag()==false){
				entityItemService.setSubEntityInactive(entityItem.getEntityId());
			}
	
			Long selectedEntityItemId = selectedEntityItem.getEntityId();
			
			//Reload tree
			loadTree();
			
			TreeNode treeNode = this.findTreeNodeById(rootNode, selectedEntityItemId); 
			if(treeNode!=null){	
				EntityItem aEntityItem = (EntityItem)treeNode.getData();
				aEntityItem.setType(MAP_SELECTED);
	    		selectedEntityItem = aEntityItem;
	    		selectedRowKey = null;
			}
			
			//Reload combox
			retrieveComboBoxes(entityItemDTO.getEntityLevelId());
			
			//Reload datagrid
			//loadEntityItemsList(selectedEntityItemId);
			retrieveFilterSearchAction();
		
			currentAddChangeEntityId = entityItem.getEntityId();
			
			//Copy components from other entity
			Long fromEntityId = entityCopyFromSelected;
			Long toEntityId = currentAddChangeEntityId;
			
			//pickComponentListUpdateResult
			String selectedComponent = "00000000";
			if(entityLocked){
				entityItemService.copyEntityComponents(fromEntityId, 0L, selectedComponent);
			}
			else{
				if(pickComponentListUpdateResult!=null && pickComponentListUpdateResult.size()>0){
					for(int i=0; i<pickComponentListUpdateResult.size(); i++){
						String componentId = (String)pickComponentListUpdateResult.get(i);
						Long selectId = Long.valueOf(componentId);
						selectedComponent = selectedComponent.substring(0, selectId.intValue()-1) + "1" + selectedComponent.substring(selectId.intValue(), selectedComponent.length());
					}
					
					entityItemService.copyEntityComponents(fromEntityId, toEntityId, selectedComponent);
				}
			}
			
			selectedRowIndex = -1;
			selectedGridEntityItem = null;
			
		    result = "entity_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end updateAction");	    
        return result;
	}	
	
	public String displayCopyAddAction() {
		this.displayUpdateAction(EditAction.COPY_ADD);

		entityItemDTO.setEntityId(null);
		entityItemDTO.setEntityCode("");
		//0002132
		//entityItemDTO.setEntityName("");
		entityItemDTO.setUpdateUserId("");
		entityItemDTO.setUpdateTimestamp(null);
		
		return "entity_update";
	}	

	public String copyAddAction(){
		return this.addAction();
	}
		
	public String displayDeleteAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		if(entityCopyFromList!=null){
			entityCopyFromList.clear();
			entityCopyFromList = null;
		}
		
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = EditAction.DELETE;
		
		entityItemDTO = new EntityItemDTO();
		
		//Parent level
		entityItemDTO.setParentLevelDescription(entityLevelMap.get(selectedEntityItem.getEntityLevelId()));
		entityItemDTO.setParentCode(selectedEntityItem.getEntityCode());
		entityItemDTO.setParentName(selectedEntityItem.getEntityName());
		entityItemDTO.setParentDescription(selectedEntityItem.getDescription());
		entityItemDTO.setParentActiveFlag(selectedEntityItem.getActiveFlag());
	
		//This level
		entityItemDTO.setEntityLevelId(selectedGridEntityItem.getEntityLevelId());
		entityItemDTO.setParentEntityId(selectedGridEntityItem.getParentEntityId());
		
		//Rebuild entity level selection
		rebuildEntityLevelList(selectedEntityItem.getEntityLevelId(), (selectedGridEntityItem.getEntityId().equals(0L) ? true : false));
		
		//If there is a location child under this entity, no location selection at entity level selections.
		if(selectedEntityItem.getEntityLevelId().intValue()==1){
			List<EntityItem> list = entityItemService.findAllEntityItemsByParent(selectedGridEntityItem.getEntityId());
			if(list!=null && list.size()>0){
				entityLevelList = new ArrayList<SelectItem>();
				entityLevelList.add(new SelectItem(new Long(2L),"Division"));
			}
		}
		
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
		entityItemDTO.setFein(selectedGridEntityItem.getFein());
		entityItemDTO.setAddressLine1(selectedGridEntityItem.getAddressLine1());
		entityItemDTO.setAddressLine2(selectedGridEntityItem.getAddressLine2());
		entityItemDTO.setZipPlus4(selectedGridEntityItem.getZipPlus4());	
		entityItemDTO.setActiveFlag(selectedGridEntityItem.getActiveFlag());
		//entityItemDTO.setLockFlag(selectedGridEntityItem.getLockFlag());
		if(selectedGridEntityItem.getLockFlag()!=null && selectedGridEntityItem.getLockFlag().equals("1")){
			entityLocked = true;
		}
		else{
			entityLocked = false;
		}
		
		//Set Jurisdiction filter
		resetFilter();
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		entityItemDTO.setJurisdictionId(selectedGridEntityItem.getJurisdictionId());
		entityItemDTO.setGeocode(selectedGridEntityItem.getGeocode());
		entityItemDTO.setCountry(selectedGridEntityItem.getCountry());
		entityItemDTO.setState(selectedGridEntityItem.getState());
		entityItemDTO.setCounty(selectedGridEntityItem.getCounty());
		entityItemDTO.setCity(selectedGridEntityItem.getCity());;
		entityItemDTO.setZip(selectedGridEntityItem.getZip());
		entityItemDTO.setUpdateUserId(selectedGridEntityItem.getUpdateUserId());
		entityItemDTO.setUpdateTimestamp(selectedGridEntityItem.getUpdateTimestamp());
		entityItemDTO.setExemptGraceDays(selectedGridEntityItem.getExemptGraceDays());
		
		filterHandler.setGeocode(selectedGridEntityItem.getGeocode());
		filterHandler.setCountry(selectedGridEntityItem.getCountry());
		filterHandler.setState(selectedGridEntityItem.getState());
		filterHandler.setCounty(selectedGridEntityItem.getCounty());
		filterHandler.setCity(selectedGridEntityItem.getCity());
		filterHandler.setZip(selectedGridEntityItem.getZip());

		//Reset copy from data
		entityCopyFromSelected = entityItemDTO.getEntityId();
		pickComponentListUpdateResult.clear();
		
		return "entity_update";
	}	
	
	public String deleteAction() {
		String result = null;
		logger.info("start deleteAction"); 
		try {
			EntityItem entityItem = new EntityItem();
			entityItem.setEntityId(entityItemDTO.getEntityId());
			Long entityId = entityItem.getEntityId();
			
			//Validate
			String errorMessage = "";
			boolean isAllowToDeleteEntity = entityItemService.isAllowToDeleteEntity(entityId);
			if(!isAllowToDeleteEntity){
				errorMessage = errorMessage + " This Entity may not be deleted.";
			}
			
			if(errorMessage!=null && errorMessage.length()>0){
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}
			
			
			//Delete components
			String selectedComponent = "11111111";
			entityItemService.copyEntityComponents(entityId, -1L, selectedComponent);

			currentAddChangeEntityId = entityId;
			
			//Reload combox
			retrieveComboBoxes(entityItemDTO.getEntityLevelId());
			
			//Reload datagrid
			retrieveFilterSearchAction();
			
			selectedRowIndex = -1;
			selectedGridEntityItem = null;
			
		    result = "entity_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		
	    logger.info("end deleteAction");	    
        return result;
	}
	
	public String displayCopyToAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		if(pickEntityListOptions!=null){
			pickEntityListOptions.clear();
			pickEntityListOptions = null;
		}
		
		pickComponentListResult.clear();
		pickEntityListResult.clear();
		
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = EditAction.COPY_TO;
		
		entityItemDTO = new EntityItemDTO();
		
		//Parent level
		entityItemDTO.setParentLevelDescription(entityLevelMap.get(selectedEntityItem.getEntityLevelId()));
		entityItemDTO.setParentCode(selectedEntityItem.getEntityCode());
		entityItemDTO.setParentName(selectedEntityItem.getEntityName());
		entityItemDTO.setParentDescription(selectedEntityItem.getDescription());
		entityItemDTO.setParentActiveFlag(selectedEntityItem.getActiveFlag());
	
		//This level
		entityItemDTO.setEntityLevelId(selectedGridEntityItem.getEntityLevelId());
		entityItemDTO.setParentEntityId(selectedGridEntityItem.getParentEntityId());
		
		//Rebuild entity level selection
		rebuildEntityLevelList(selectedEntityItem.getEntityLevelId(),(selectedGridEntityItem.getEntityId().equals(0L) ? true : false));
		
		//If there is a location child under this entity, no location selection at entity level selections.
		if(selectedEntityItem.getEntityLevelId().intValue()==1){
			List<EntityItem> list = entityItemService.findAllEntityItemsByParent(selectedGridEntityItem.getEntityId());
			if(list!=null && list.size()>0){
				entityLevelList = new ArrayList<SelectItem>();
				entityLevelList.add(new SelectItem(new Long(2L),"Division"));
			}
		}
		
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
		entityItemDTO.setFein(selectedGridEntityItem.getFein());
		entityItemDTO.setAddressLine1(selectedGridEntityItem.getAddressLine1());
		entityItemDTO.setAddressLine2(selectedGridEntityItem.getAddressLine2());
		entityItemDTO.setZipPlus4(selectedGridEntityItem.getZipPlus4());	
		entityItemDTO.setActiveFlag(selectedGridEntityItem.getActiveFlag());
		entityItemDTO.setExemptGraceDays(selectedGridEntityItem.getExemptGraceDays());

		return "entity_copyto";
	}
	
	public String displayDefaultAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		logger.info("start displayDefaultAction"); 
		currentAction = EditAction.DEFAULT;
		
		entityItemDTO = new EntityItemDTO();
		
		//This level
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
		
		this.setEntityDefaultList(null);
		List<EntityDefault> list = entityDefaultService.findAllEntityDefaultsByEntityId(entityItemDTO.getEntityId());
		this.setEntityDefaultList(list);
			
		selectedEntityDefaultRowIndex = -1;
		selectedEntityDefault = null;
		
		//0003988: Entity Defaults Issues
		//If all defaults have been added, then don�t display the �Add� button on Set Entity Defaults screen.
		Map<String, String > mapExcluded = new LinkedHashMap<String, String>(); //Excluded map
    	for(EntityDefault entityDefault:getEntityDefaultList()){
    		mapExcluded.put(entityDefault.getDefaultCode(), entityDefault.getDefaultCode());
		}
    	List<SelectItem> anyList = createCodeList(mapExcluded);
    	disableDefaultAdd = true;
		if(anyList.size()>1){
			disableDefaultAdd = false;
		}

		return "entity_default";
	}
	
	private boolean disableDefaultAdd = true;
	
	public boolean getDisableDefaultAdd(){
		return disableDefaultAdd;
	}
	
	public String displayRegistrationAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		nexusDefinitionBackingBean.setSelectedEntityItemFromRegistration(selectedGridEntityItem);

		return "nexus_def_states";
	}
	
	public String displayDefinitionAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		nexusDefinitionBackingBean.setSelectedEntityItemFromDefinition(selectedGridEntityItem);

		return "nexus_def_states";
	}
	
	public String displayNexusDefinitionAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		nexusDefRegBackingBean.setSelectedEntityItemFromDefinition(selectedGridEntityItem);

		return "nexusdefreg_main";
	}
	public String displayGandSAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		 taxMatrix.setEntityCheck(true);
		 showhide="show";
		 if(selectedGridEntityItem!=null && selectedGridEntityItem.getEntityId()!=null){
			 taxMatrix.setEntityId(selectedGridEntityItem.getEntityId());
			 taxMatrix.setEntityCode(selectedGridEntityItem.getEntityCode() + " - " +selectedGridEntityItem.getEntityName());
			}
			else{
			  taxMatrix.setEntityId(999999999L);
			
			}
		
		 taxMatrix.updateMatrixList();
		return "tax_matrix_sales_main";
	}
	public String displayGandSgsbAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}

		 gsbMatrixBean.setEntityCheck(true);
		 showhide="show";
		 if(selectedGridEntityItem!=null && selectedGridEntityItem.getEntityId()!=null){
		 gsbMatrixBean.setEntityId(selectedGridEntityItem.getEntityId());
		 gsbMatrixBean.setEntityCode(selectedGridEntityItem.getEntityCode() + " - " +selectedGridEntityItem.getEntityName());
		 }else{
		 gsbMatrixBean.setEntityId(999999999L);
		 }
		 gsbMatrixBean.updateMatrixList();
		return "tax_matrix_sales_main";
	}
	public String displayCustomersAction() {
		selectedEntityItem = null;
			if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		  showhide="show";
		  custLoc.setEntityCheck(true);
		  custLoc.getFilterCust().setEntityId(selectedGridEntityItem.getEntityId());
          //nexusDefRegBackingBean.setSelectedEntityItemFromDefinition(selectedGridEntityItem);

		return "customerLocation_main";
	}
	public String displayLocationAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		logger.info("start displayLocationAction"); 
		currentAction = EditAction.DEFAULT;
		
		entityItemDTO = new EntityItemDTO();
		
		//This level
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
	
		this.setEntityLocnSetList(null);
		List<EntityLocnSet> list = entityLocnSetService.findAllEntityLocnSetsByEntityId(entityItemDTO.getEntityId());
		this.setEntityLocnSetList(list);
			
		selectedLocationListRowIndex = -1;
		selectedEntityLocnSet = null;

		return "entity_default";
	}
	
	public String displayLocationDtlAction() {
		selectedEntityItem = null;
		
		if(selectedGridEntityItem==null){
			return "";
		}
		else{
			selectedEntityItem =  entityItemService.findById(selectedGridEntityItem.getParentEntityId());
		}
		
		if(selectedEntityItem==null){
			return "";
		}
		
		if(selectedEntityLocnSet==null){
			this.setEntityLocnSetDtlList(null);
			
			selectedLocationListDetailRowIndex  = -1;
			selectedEntityLocnSetDtl = null;
			
			return "entity_default";
		}
		
		logger.info("start displayLocationAction"); 
		currentAction = EditAction.DEFAULT;
		
		entityItemDTO = new EntityItemDTO();
		
		//This level
		entityItemDTO.setEntityId(selectedGridEntityItem.getEntityId());
		entityItemDTO.setEntityCode(selectedGridEntityItem.getEntityCode());
		entityItemDTO.setEntityName(selectedGridEntityItem.getEntityName());
		entityItemDTO.setDescription(selectedGridEntityItem.getDescription());
	
		this.setEntityLocnSetDtlList(null);
		List<EntityLocnSetDtl> list = entityLocnSetDtlService.findAllEntityLocnSetDtlsByEntityLocnSetId(selectedEntityLocnSet.getEntityLocnSetId());
		this.setEntityLocnSetDtlList(list);
			
		selectedLocationListDetailRowIndex  = -1;
		selectedEntityLocnSetDtl = null;

		return "entity_default";
	}
	
	public String okAddChildAction() {
		//PP-193
		String result = null;		
		switch (currentAction) {			
			case UPDATE:
				result = updateAction();
				break;
				
			case ADD:
				result = addAction();
				break;
	
			default:
				break;
		}
		
		if(result!=null && result.length()>0){
			//update succeed and continue to add
			//0003704 item 3
			EntityItem aEntityItem = entityItemService.findById(currentAddChangeEntityId);
			result = displayAddAction(aEntityItem);
		}
		
		return result;
	}
	
	public String okAddPeerAction() {
		//PP-193
		String result = null;		
		switch (currentAction) {			
			case UPDATE:
				result = updateAction();
				break;
				
			case ADD:
				result = addAction();
				break;
	
			default:
				break;
		}
		
		if(result!=null && result.length()>0){
			//update succeed and continue to add
			//0003704 item 7
			EntityItem aEntityItem = entityItemService.findById(currentAddChangeEntityId);
			aEntityItem = entityItemService.findById(aEntityItem.getParentEntityId());

			result = displayAddAction(aEntityItem);
		}
		
		return result;
	}
	
	public String okAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteAction();
				break;
				
			case UPDATE:
				result = updateAction();
				break;
				
			case ADD:
				result = addAction();
				break;
				
			case COPY_ADD:
				result = copyAddAction();
				break;
			case VIEW:
				this.returnView = "entity_main";	
				result = returnBack();
				break;
	
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String okCopyToAction(){
		//1 = Defaults
		//2 = Nexus
		//3 = Registrations
		//4 = 
		//Possible future sub-components include:
		//5 = Direct Pay Permits
		//6 = Tax and Location Drivers
		//7 = Entity Mappings
		//8 = Affiliate Organizations and Locations

		String selectedComponent = "00000000";
		boolean componentChanged = false;
		SelectItem aSelectItem = null;
		if(pickComponentListResult!=null && pickComponentListResult.size()>0){
			for(int i=0; i<pickComponentListResult.size(); i++){
				String componentId = (String)pickComponentListResult.get(i);
				componentChanged = true;
				Long selectId = Long.valueOf(componentId);
				selectedComponent = selectedComponent.substring(0, selectId.intValue()-1) + "1" + selectedComponent.substring(selectId.intValue(), selectedComponent.length());
			}
		}
		
		if(!componentChanged){
			//No change
			return "entity_main";
		}
		
		if(pickEntityListResult!=null && pickEntityListResult.size()>0){
			for(int i=0; i<pickEntityListResult.size(); i++){
				String componentId = (String)pickEntityListResult.get(i);
				Long entityId = Long.valueOf(componentId);
				entityItemService.copyEntityComponents(entityItemDTO.getEntityId(), entityId, selectedComponent);
	    	}
		}

	    return "entity_main";
	}
	
	public String cancelAction() {
	    return "entity_main";
	}
	
	public void lockChangeAction(ValueChangeEvent event) {
    	String newValue = (String) event.getNewValue();
        String oldValue = (String) event.getOldValue();
    }
	
	public void defaultTypeSelectionChanged(ValueChangeEvent e) {
		selectedDefaultTypeList = (Long)e.getNewValue();
		
		/*if(selectedDefaultTypeList.equals(0L)) {	
		}
		else if(selectedDefaultTypeList.equals(1L)) {
			
		}
		else*/ if(selectedDefaultTypeList.equals(2L)) {
			displayLocationAction();
			displayLocationDtlAction();
			
			pickShipFromEntityListOptions = null;
			pickPOOriginEntityListOptions = null;
			pickPOAcceptanceEntityListOptions = null;
			entityItemMap = null;
		}else{
			displayDefaultAction();
		}
		
	}
	
	public void defaultCodeSelectionChanged(ValueChangeEvent e) {
		String code = (String)e.getNewValue();
		
		defaultValueList = createValueList(code);
	}
	
	public void defaultCodeSelectionChanged(ActionEvent e) {
		String code = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();

		defaultValueList = createValueList(code);
	}
	
	public List<SelectItem> createValueList(String code) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		
		if(code!=null && code.length()>0){
			
			if(currentAction.equals(EditAction.ADD)){
				selectItems.add(new SelectItem("","Select a Value"));
			}
			List<ListCodes> listCodes = cacheManager.getListCodesByType(code);
			if(listCodes != null) {
				for(ListCodes lc : listCodes) {
					selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				}
			}
		}
		
		return selectItems;
	}
	
	public List<SelectItem> createCodeList(Map<String, String > mapExcluded) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("","Select a Default Code"));
		List<ListCodes> listCodes = cacheManager.getListCodesByType("DEFCODES");
		if(listCodes != null) {
			for(ListCodes lc : listCodes) {
				String code = (String)mapExcluded.get(lc.getCodeCode());
				if(code==null || code.length()==0){
					selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				}
			}
		}
		return selectItems;
	}
	
	public String processEntityCommand() {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    EntityDefaultAction action = EntityDefaultAction.valueOf(command);
	    
	    if(action.equals(EntityDefaultAction.closeEntityDefaultMain)){
	    	return "entity_main";
	    }
	    else if(action.equals(EntityDefaultAction.displayAddSingle)){
	    	currentAction = EditAction.ADD;
	    	
	    	entityDefaultDTO = new EntityDefaultDTO();
	    	//entityDefaultDTO.setEntityDefaultId(null);
	    	entityDefaultDTO.setEntityId(selectedGridEntityItem.getEntityId());
	    	entityDefaultDTO.setDefaultCode("");
	    	entityDefaultDTO.setDefaultValue("");
	    	//entityDefaultDTO.setUpdateUserId(null);
	    	//entityDefaultDTO.setUpdateTimestamp(null);
	    	
	    	//It will display only Codes that do not already have a default defined in the selected Entity. 
	    	Map<String, String > mapExcluded = new LinkedHashMap<String, String>(); //Excluded map
	    	for(EntityDefault entityDefault:getEntityDefaultList()){
	    		mapExcluded.put(entityDefault.getDefaultCode(), entityDefault.getDefaultCode());
			}	
	    	defaultCodeList = createCodeList(mapExcluded);
	    	defaultValueList = createValueList(entityDefaultDTO.getDefaultCode());
	    
	    	return "entitydefault_update";
	    }
	    else if(action.equals(EntityDefaultAction.displayUpdateSingle)){
	    	currentAction = EditAction.UPDATE;
	    	entityDefaultDTO = selectedEntityDefault.getEntityDefaultDTO();
	    	
	    	//It will display only Codes that do not already have a default defined in the selected Entity. 
	    	//We will include current code in combo box.
	    	Map<String, String > mapExcluded = new LinkedHashMap<String, String>(); //Excluded map
	    	for(EntityDefault entityDefault:getEntityDefaultList()){
	    		mapExcluded.put(entityDefault.getDefaultCode(), entityDefault.getDefaultCode());
			}	
	    	mapExcluded.remove(entityDefaultDTO.getDefaultCode());
	    	
	    	defaultCodeList = createCodeList(mapExcluded);
	    	defaultValueList = createValueList(entityDefaultDTO.getDefaultCode());
	    	
	    	return "entitydefault_update";
	    }
	    else if(action.equals(EntityDefaultAction.displayDeleteSingle)){
	    	currentAction = EditAction.DELETE;
	    	entityDefaultDTO = selectedEntityDefault.getEntityDefaultDTO();
	    	
	    	//It will display only Codes that do not already have a default defined in the selected Entity. 
	    	//We will include current code in combo box.
	    	Map<String, String > mapExcluded = new LinkedHashMap<String, String>(); //Excluded map
	    	for(EntityDefault entityDefault:getEntityDefaultList()){
	    		mapExcluded.put(entityDefault.getDefaultCode(), entityDefault.getDefaultCode());
			}	
	    	mapExcluded.remove(entityDefaultDTO.getDefaultCode());
	    	
	    	defaultCodeList = createCodeList(mapExcluded);
	    	defaultValueList = createValueList(entityDefaultDTO.getDefaultCode());
	    	
	    	return "entitydefault_update";
	    }
	    else if(action.equals(EntityDefaultAction.okSingle)){
	    	if(currentAction==EditAction.ADD){
    			String errorMessage = "";
    			if(entityDefaultDTO.getDefaultCode()==null || entityDefaultDTO.getDefaultCode().length()==0){
    				errorMessage = errorMessage + " " + "Default Code cannot be empty.";
    			}
    			if(entityDefaultDTO.getDefaultValue()==null || entityDefaultDTO.getDefaultValue().length()==0){
    				errorMessage = errorMessage + " " + "Default Value cannot be empty.";
    			}
   
    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
    			
    			EntityDefault entityDefault = new EntityDefault();
    			//entityDefaultDTO.setEntityDefaultId(selectedEntityDefault.getEntityDefaultId());
    			entityDefault.setEntityId(entityDefaultDTO.getEntityId());
    			entityDefault.setDefaultCode(entityDefaultDTO.getDefaultCode());
    			entityDefault.setDefaultValue(entityDefaultDTO.getDefaultValue());
    	    	//entityDefaultDTO.setUpdateUserId(selectedEntityDefault.getUpdateUserId());
    	    	//entityDefaultDTO.setUpdateTimestamp(selectedEntityDefault.getUpdateTimestamp());

    			Long entityDefaultId = entityDefaultService.persist(entityDefault);
    			displayDefaultAction();
	    			   
	            return "entity_default";
	    	}
	    	else if(currentAction==EditAction.UPDATE){
	    		String errorMessage = "";
    			if(entityDefaultDTO.getDefaultCode()==null || entityDefaultDTO.getDefaultCode().length()==0){
    				errorMessage = errorMessage + " " + "Default Code cannot be empty.";
    			}
    			if(entityDefaultDTO.getDefaultValue()==null || entityDefaultDTO.getDefaultValue().length()==0){
    				errorMessage = errorMessage + " " + "Default Value cannot be empty.";
    			}
   
    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
    			
    			EntityDefault entityDefault = new EntityDefault();
    			entityDefault.setEntityDefaultId(entityDefaultDTO.getEntityDefaultId());
    			entityDefault.setEntityId(entityDefaultDTO.getEntityId());
    			entityDefault.setDefaultCode(entityDefaultDTO.getDefaultCode());
    			entityDefault.setDefaultValue(entityDefaultDTO.getDefaultValue());
    			entityDefault.setUpdateUserId(entityDefaultDTO.getUpdateUserId());
    			entityDefault.setUpdateTimestamp(entityDefaultDTO.getUpdateTimestamp());

    			entityDefaultService.saveOrUpdate(entityDefault);
    			displayDefaultAction();
	    		
	    		return "entity_default";
	    	}
	    	else if(currentAction==EditAction.DELETE){
	    		EntityDefault entityDefault = new EntityDefault();
    			entityDefault.setEntityDefaultId(entityDefaultDTO.getEntityDefaultId());
    			Long entityDefaultId = entityDefault.getEntityDefaultId();

    			entityDefaultService.remove(entityDefaultId);
    			displayDefaultAction();
    			
	    		return "entity_default";
	    	}
	    	
	    	return null;
	    }
	    else if(action.equals(EntityDefaultAction.cancelSingle)){
	    	return "entity_default";
	    }
	    else if(action.equals(EntityDefaultAction.displayAddLocationSet)){
	    	currentAction = EditAction.ADD;
	    	
	    	entityLocnSetDTO = new EntityLocnSetDTO();
	    	//entityDefaultDTO.setEntityDefaultId(null);
	    	entityLocnSetDTO.setEntityId(selectedGridEntityItem.getEntityId());
	    	entityLocnSetDTO.setLocnSetCode("");
	    	entityLocnSetDTO.setName("");
	    	entityLocnSetDTO.setActiveFlag("1");
	    	//entityDefaultDTO.setUpdateUserId(null);
	    	//entityDefaultDTO.setUpdateTimestamp(null);
	    
	    	return "entitylocation_update";
	    }
	    else if(action.equals(EntityDefaultAction.displayUpdateLocationSet)){
	    	currentAction = EditAction.UPDATE;
	    	entityLocnSetDTO = selectedEntityLocnSet.getEntityLocnSetDTO();
	    	
	    	return "entitylocation_update";
	    }
		else if(action.equals(EntityDefaultAction.displayDeleteLocationSet)){
			currentAction = EditAction.DELETE;
			entityLocnSetDTO = selectedEntityLocnSet.getEntityLocnSetDTO();
	    	
	    	return "entitylocation_update";
	    }
		else if(action.equals(EntityDefaultAction.okLocationSet)){
			if(currentAction==EditAction.ADD){
    			String errorMessage = "";
    			if(entityLocnSetDTO.getLocnSetCode()==null || entityLocnSetDTO.getLocnSetCode().length()==0){
    				errorMessage = errorMessage + " " + "Set Code cannot be empty.";
    			}
    			if(entityLocnSetDTO.getName()==null || entityLocnSetDTO.getName().length()==0){
    				errorMessage = errorMessage + " " + "Name cannot be empty.";
    			}
   
    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
    			
    			EntityLocnSet entityLocnSet = new EntityLocnSet();
    			entityLocnSet.setEntityId(entityLocnSetDTO.getEntityId());
    			entityLocnSet.setLocnSetCode(entityLocnSetDTO.getLocnSetCode());
    			entityLocnSet.setName(entityLocnSetDTO.getName());
    			entityLocnSet.setActiveFlag(entityLocnSetDTO.getActiveFlag());
    	    	//entityLocnSetDTO.setUpdateUserId(selectedEntityDefault.getUpdateUserId());
    	    	//entityLocnSetDTO.setUpdateTimestamp(selectedEntityDefault.getUpdateTimestamp());

    			Long entityDefaultId = entityLocnSetService.persist(entityLocnSet);
    			displayLocationAction();
	    			   
	            return "entity_default";
	    	}
	    	else if(currentAction==EditAction.UPDATE){
	    		String errorMessage = "";
	    		if(entityLocnSetDTO.getLocnSetCode()==null || entityLocnSetDTO.getLocnSetCode().length()==0){
    				errorMessage = errorMessage + " " + "Set Code cannot be empty.";
    			}
    			if(entityLocnSetDTO.getName()==null || entityLocnSetDTO.getName().length()==0){
    				errorMessage = errorMessage + " " + "Name cannot be empty.";
    			}
   
    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
	
    			EntityLocnSet entityLocnSet = new EntityLocnSet();
    			entityLocnSet.setEntityLocnSetId(entityLocnSetDTO.getEntityLocnSetId());
    			entityLocnSet.setEntityId(entityLocnSetDTO.getEntityId());
    			entityLocnSet.setLocnSetCode(entityLocnSetDTO.getLocnSetCode());
    			entityLocnSet.setName(entityLocnSetDTO.getName());
    			entityLocnSet.setActiveFlag(entityLocnSetDTO.getActiveFlag());

    			entityLocnSet.setUpdateUserId(entityLocnSetDTO.getUpdateUserId());
    			entityLocnSet.setUpdateTimestamp(entityLocnSetDTO.getUpdateTimestamp());

    			entityLocnSetService.saveOrUpdate(entityLocnSet);
    			displayLocationAction();
	    		
	    		return "entity_default";
	    	}
	    	else if(currentAction==EditAction.DELETE){
	    		EntityLocnSet entityLocnSet = new EntityLocnSet();
	    		entityLocnSet.setEntityLocnSetId(entityLocnSetDTO.getEntityLocnSetId());
	    		
    			Long entityLocnSetId = entityLocnSet.getEntityLocnSetId();

    			entityLocnSetService.remove(entityLocnSetId);
    			displayLocationAction();
    			
	    		return "entity_default";
	    	}
	    	
	    	return null;
	    }
		else if(action.equals(EntityDefaultAction.cancelLocationSet)){
			return "entity_default";
	    }
		else if(action.equals(EntityDefaultAction.displayAddLocationSetDetail)){
			currentAction = EditAction.ADD;
	    	
	    	entityLocnSetDTO = new EntityLocnSetDTO();
	    	//entityDefaultDTO.setEntityDefaultId(null);
	    	entityLocnSetDTO.setEntityId(selectedGridEntityItem.getEntityId());
	    	entityLocnSetDTO.setLocnSetCode(selectedEntityLocnSet.getLocnSetCode());
	    	entityLocnSetDTO.setName(selectedEntityLocnSet.getName());
	    	entityLocnSetDTO.setActiveFlag(selectedEntityLocnSet.getActiveFlag());
	    	
	    	entityLocnSetDtlDTO = new EntityLocnSetDtlDTO();
	    	entityLocnSetDtlDTO.setEntityLocnSetId(selectedEntityLocnSet.getEntityLocnSetId());
	    	entityLocnSetDtlDTO.setDescription("");
	    	entityLocnSetDtlDTO.setSfEntityId(null);
	    	entityLocnSetDtlDTO.setPooEntityId(null);
	    	entityLocnSetDtlDTO.setPoaEntityId(null);
	    	entityLocnSetDtlDTO.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
	    	//entityLocnSetDtlDTO.setUpdateUserId(null);
	    	//entityLocnSetDtlDTO.setUpdateTimestamp(null);
	    
	    	return "entitylocationdtl_update";
	    }
		else if(action.equals(EntityDefaultAction.displayUpdateLocationSetDetail)){
			currentAction = EditAction.UPDATE;
			
			entityLocnSetDTO = new EntityLocnSetDTO();
	    	//entityDefaultDTO.setEntityDefaultId(null);
	    	entityLocnSetDTO.setEntityId(selectedGridEntityItem.getEntityId());
	    	entityLocnSetDTO.setLocnSetCode(selectedEntityLocnSet.getLocnSetCode());
	    	entityLocnSetDTO.setName(selectedEntityLocnSet.getName());
	    	entityLocnSetDTO.setActiveFlag(selectedEntityLocnSet.getActiveFlag());
	    	
			entityLocnSetDtlDTO = selectedEntityLocnSetDtl.getEntityLocnSetDtlDTO();
	    	
	    	return "entitylocationdtl_update";
	    }
		else if(action.equals(EntityDefaultAction.displayDeleteLocationSetDetail)){
			currentAction = EditAction.DELETE;
			
			entityLocnSetDTO = new EntityLocnSetDTO();
	    	//entityDefaultDTO.setEntityDefaultId(null);
	    	entityLocnSetDTO.setEntityId(selectedGridEntityItem.getEntityId());
	    	entityLocnSetDTO.setLocnSetCode(selectedEntityLocnSet.getLocnSetCode());
	    	entityLocnSetDTO.setName(selectedEntityLocnSet.getName());
	    	entityLocnSetDTO.setActiveFlag(selectedEntityLocnSet.getActiveFlag());
	    	
			entityLocnSetDtlDTO = selectedEntityLocnSetDtl.getEntityLocnSetDtlDTO();
	    	
	    	return "entitylocationdtl_update";
	    }
		else if(action.equals(EntityDefaultAction.okLocationSetDetail)){		
			if(currentAction==EditAction.ADD){
				boolean gotOneEntity = false;
				if(entityLocnSetDtlDTO.getSfEntityId()!=null && entityLocnSetDtlDTO.getSfEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				if(entityLocnSetDtlDTO.getPooEntityId()!=null && entityLocnSetDtlDTO.getPooEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				if(entityLocnSetDtlDTO.getPoaEntityId()!=null && entityLocnSetDtlDTO.getPoaEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				
    			String errorMessage = "";
    			if(!gotOneEntity){
    				errorMessage = errorMessage + " " + "At least one Entity must be selected � either Ship From, P.O. Origin, or P.O. Acceptance.";
    			}
    			
    			Date effectTS = (Date)entityLocnSetDtlDTO.getEffectiveDate();
    			boolean hasDateIssue = false;
    			if(effectTS==null){
    				if (effectTS==null) {  
    					errorMessage = errorMessage + " " + "Effective Date may not be blank. ";
    					hasDateIssue = true;
    				}
    			}
    		
    			if(!hasDateIssue){	
    				Date todayDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();	
    				if (effectTS.before(todayDate)) {
    					errorMessage = errorMessage + " " + "Effective date must be on or after today. ";
    			    }
    			}

    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
    					
    			EntityLocnSetDtl entityLocnSetDtl = new EntityLocnSetDtl(); 		
	    		//entityLocnSetDtl.setEntityLocnSetDtlId(entityLocnSetDtlDTO.getEntityLocnSetDtlId());
    			entityLocnSetDtl.setEntityLocnSetId(entityLocnSetDtlDTO.getEntityLocnSetId());
    			entityLocnSetDtl.setDescription(entityLocnSetDtlDTO.getDescription());
    			//entityLocnSetDtl.setSfEntityId(entityLocnSetDtlDTO.getSfEntityId());
    			//entityLocnSetDtl.setPooEntityId(entityLocnSetDtlDTO.getPooEntityId());
    			//entityLocnSetDtl.setPoaEntityId(entityLocnSetDtlDTO.getPoaEntityId());
    			entityLocnSetDtl.setEffectiveDate(entityLocnSetDtlDTO.getEffectiveDate());
    			
    			if(entityLocnSetDtlDTO.getSfEntityId()!=null && entityLocnSetDtlDTO.getSfEntityId().longValue()>=0L){
    				entityLocnSetDtl.setSfEntityId(entityLocnSetDtlDTO.getSfEntityId());
				}
    			else{
    				entityLocnSetDtl.setSfEntityId(null);
    			}
				if(entityLocnSetDtlDTO.getPooEntityId()!=null && entityLocnSetDtlDTO.getPooEntityId().longValue()>=0L){
					entityLocnSetDtl.setPooEntityId(entityLocnSetDtlDTO.getPooEntityId());
				}
				else{
					entityLocnSetDtl.setPooEntityId(null);
				}
				if(entityLocnSetDtlDTO.getPoaEntityId()!=null && entityLocnSetDtlDTO.getPoaEntityId().longValue()>=0L){
					entityLocnSetDtl.setPoaEntityId(entityLocnSetDtlDTO.getPoaEntityId());
				}
				else{
					entityLocnSetDtl.setPoaEntityId(null);
				}
    				
    	    	//entityLocnSetDtlDTO.setUpdateUserId(selectedEntityDefault.getUpdateUserId());
    	    	//entityLocnSetDtlDTO.setUpdateTimestamp(selectedEntityDefault.getUpdateTimestamp());

    			Long entityDefaultDtlId = entityLocnSetDtlService.persist(entityLocnSetDtl);
    			displayLocationDtlAction();
	    			   
	            return "entity_default";
	    	}
	    	else if(currentAction==EditAction.UPDATE){
	    		boolean gotOneEntity = false;
				if(entityLocnSetDtlDTO.getSfEntityId()!=null && entityLocnSetDtlDTO.getSfEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				if(entityLocnSetDtlDTO.getPooEntityId()!=null && entityLocnSetDtlDTO.getPooEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				if(entityLocnSetDtlDTO.getPoaEntityId()!=null && entityLocnSetDtlDTO.getPoaEntityId().longValue()>=0L){
					gotOneEntity = true;
				}
				
    			String errorMessage = "";
    			if(!gotOneEntity){
    				errorMessage = errorMessage + " " + "At least one Entity must be selected � either Ship From, P.O. Origin, or P.O. Acceptance.";
    			}
    			
    			Date effectTS = (Date)entityLocnSetDtlDTO.getEffectiveDate();
    			boolean hasDateIssue = false;
    			if(effectTS==null){
    				if (effectTS==null) {  
    					errorMessage = errorMessage + " " + "Effective Date may not be blank. ";
    					hasDateIssue = true;
    				}
    			}
    		
    			if(!hasDateIssue){
	    			GregorianCalendar yesterday = new GregorianCalendar();
	    			yesterday.add(GregorianCalendar.DATE, -1);
	    			if (!effectTS.after(yesterday.getTime())) {
	    				errorMessage = errorMessage + " " + "Effective date must be on or after today. ";
	    			}	
    			}
    
    			if(errorMessage!=null && errorMessage.length()>0){
    				FacesMessage message = new FacesMessage(errorMessage);
    				message.setSeverity(FacesMessage.SEVERITY_ERROR);
    				FacesContext.getCurrentInstance().addMessage(null, message);
    				return "";
    			}
    			
	    		EntityLocnSetDtl entityLocnSetDtl = new EntityLocnSetDtl(); 		
	    		entityLocnSetDtl.setEntityLocnSetDtlId(entityLocnSetDtlDTO.getEntityLocnSetDtlId());
    			entityLocnSetDtl.setEntityLocnSetId(entityLocnSetDtlDTO.getEntityLocnSetId());
    			entityLocnSetDtl.setDescription(entityLocnSetDtlDTO.getDescription());
    			//entityLocnSetDtl.setSfEntityId(entityLocnSetDtlDTO.getSfEntityId());
    			//entityLocnSetDtl.setPooEntityId(entityLocnSetDtlDTO.getPooEntityId());
    			//entityLocnSetDtl.setPoaEntityId(entityLocnSetDtlDTO.getPoaEntityId());
    			entityLocnSetDtl.setEffectiveDate(entityLocnSetDtlDTO.getEffectiveDate());

    			entityLocnSetDtl.setUpdateUserId(entityLocnSetDtlDTO.getUpdateUserId());
    			entityLocnSetDtl.setUpdateTimestamp(entityLocnSetDtlDTO.getUpdateTimestamp());
    			
    			if(entityLocnSetDtlDTO.getSfEntityId()!=null && entityLocnSetDtlDTO.getSfEntityId().longValue()>=0L){
    				entityLocnSetDtl.setSfEntityId(entityLocnSetDtlDTO.getSfEntityId());
				}
    			else{
    				entityLocnSetDtl.setSfEntityId(null);
    			}
				if(entityLocnSetDtlDTO.getPooEntityId()!=null && entityLocnSetDtlDTO.getPooEntityId().longValue()>=0L){
					entityLocnSetDtl.setPooEntityId(entityLocnSetDtlDTO.getPooEntityId());
				}
				else{
					entityLocnSetDtl.setPooEntityId(null);
				}
				if(entityLocnSetDtlDTO.getPoaEntityId()!=null && entityLocnSetDtlDTO.getPoaEntityId().longValue()>=0L){
					entityLocnSetDtl.setPoaEntityId(entityLocnSetDtlDTO.getPoaEntityId());
				}
				else{
					entityLocnSetDtl.setPoaEntityId(null);
				}

    			entityLocnSetDtlService.saveOrUpdate(entityLocnSetDtl);
    			displayLocationDtlAction();
	    		
	    		return "entity_default";
	    	}
	    	else if(currentAction==EditAction.DELETE){
	    		EntityLocnSetDtl entityLocnSetDtl = new EntityLocnSetDtl();
	    		entityLocnSetDtl.setEntityLocnSetDtlId(entityLocnSetDtlDTO.getEntityLocnSetDtlId());
	    		
    			Long entityLocnSetDtlId = entityLocnSetDtl.getEntityLocnSetDtlId();

    			entityLocnSetDtlService.remove(entityLocnSetDtlId);
    			displayLocationDtlAction();
    			
	    		return "entity_default";
	    	}
	    	
	    	return null;
	    }
		else if(action.equals(EntityDefaultAction.closeLocationSetDetailMain)){
			//0002136, Reset Default Type selection
			selectedDefaultTypeList = 0L;
			return "entity_main";
	    }
		else if(action.equals(EntityDefaultAction.closeSingleMain)){
			//0002136, Reset Default Type selection
			selectedDefaultTypeList = 0L;
	    	return "entity_main";
	    }
		else if(action.equals(EntityDefaultAction.cancelLocationSetDetail)){
			return "entity_default";
	    }

	    return null;
	}
    
    public void setSelectedGridEntityItem(EntityItem selectedGridEntityItem){
		this.selectedGridEntityItem = selectedGridEntityItem;
	}
	
	public EntityItem getSelectedGridEntityItem(){
		return this.selectedGridEntityItem;
	}
	
	public void SetSelectedEntityDefault(EntityDefault selectedEntityDefault){
		this.selectedEntityDefault = selectedEntityDefault;
	}
	
	public EntityDefault getSelectedEntityDefault(){
		return this.selectedEntityDefault;
	}

	public void SetSelectedEntityLocnSet(EntityLocnSet selectedEntityLocnSet){
		this.selectedEntityLocnSet = selectedEntityLocnSet;
	}
	
	public EntityLocnSet getSelectedEntityLocnSet(){
		return this.selectedEntityLocnSet;
	}
	
	public void SetSelectedEntityLocnSetDtl(EntityLocnSetDtl selectedEntityLocnSetDtl){
		this.selectedEntityLocnSetDtl = selectedEntityLocnSetDtl;
	}
	
	public EntityLocnSetDtl getSelectedEntityLocnSetDtl(){
		return this.selectedEntityLocnSetDtl;
	}

	public UserSecurityBean getUserSecurityBean() {
		return userSecurityBean;
	}

	public void setUserSecurityBean(UserSecurityBean userSecurityBean) {
		this.userSecurityBean= userSecurityBean;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public int getSelectedEntityDefaultRowIndex() {
		return selectedEntityDefaultRowIndex;
	}

	public void setSelectedEntityDefaultRowIndex(int selectedEntityDefaultRowIndex) {
		this.selectedEntityDefaultRowIndex = selectedEntityDefaultRowIndex;
	}

	public int getSelectedLocationListRowIndex() {
		return selectedLocationListRowIndex ;
	}

	public void setSelectedLocationListRowIndex(int selectedLocationListRowIndex) {
		this.selectedLocationListRowIndex = selectedLocationListRowIndex;
	}
	
	public int getSelectedLocationListDetailRowIndex() {
		return selectedLocationListDetailRowIndex;
	}

	public void setSelectedLocationListDetailRowIndex(int selectedLocationListDetailRowIndex) {
		this.selectedLocationListDetailRowIndex = selectedLocationListDetailRowIndex;
	}

	public boolean isShowUpdateButton() {
		return showUpdateButton;
	}

	public void setShowUpdateButton(boolean showUpdateButton) {
		this.showUpdateButton = showUpdateButton;
	}

	public boolean isDisableDeleteButton() {
		return disableDeleteButton;
	}

	public void setDisableDeleteButton(boolean disableDeleteButton) {
		this.disableDeleteButton = disableDeleteButton;
	}

	public boolean isUpdateDisplay() {
		return updateDisplay;
	}

	public void setUpdateDisplay(boolean updateDisplay) {
		this.updateDisplay = updateDisplay;
	}

	public boolean isDisplayAddButton() {
		return displayAddButton;
	}

	public void setDisplayAddButton(boolean displayAddButton) {
		this.displayAddButton = displayAddButton;
	}

	public boolean isDisplayUpdateButton() {
		return displayUpdateButton;
	}

	public void setDisplayUpdateButton(boolean displayUpdateButton) {
		this.displayUpdateButton = displayUpdateButton;
	}

	public boolean isTransColDisplay() {
		return transColDisplay;
	}

	public void setTransColDisplay(boolean transColDisplay) {
		this.transColDisplay = transColDisplay;
	}

	public List<EntityLevel> getSelectedEntityLevelList() {
		return selectedEntityLevelList;
	}

	public void setSelectedEntityLevelList(List<EntityLevel> selectedEntityLevelList) {
		this.selectedEntityLevelList = selectedEntityLevelList;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}
    
    public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
		
		filterHandlerShipto.setCacheManager(getCacheManager());
	}
    
    public UserService getUserService() {
    	return userService;
    }
    
    public void setUserService(UserService userService) {
    	this.userService = userService;
    }	
    
    public RoleService getRoleService() {
    	return roleService;
    }
    
    public void setRoleService(RoleService roleService) {
    	this.roleService = roleService;
    }       
    
    public EntityItemService getEntityItemService() {
    	return entityItemService;
    }
    
    public void setEntityItemService(EntityItemService entityItemService) {
    	this.entityItemService = entityItemService;
    }  
    
    public EntityDefaultService getEntityDefaultService() {
    	return entityDefaultService;
    }
    
    public void setEntityDefaultService(EntityDefaultService entityDefaultService) {
    	this.entityDefaultService = entityDefaultService;
    }  
    
    public EntityLocnSetService getEntityLocnSetService() {
    	return entityLocnSetService;
    }
    
    public void setEntityLocnSetService(EntityLocnSetService entityLocnSetService) {
    	this.entityLocnSetService = entityLocnSetService;
    }  
    
    public EntityLocnSetDtlService getEntityLocnSetDtlService() {
    	return entityLocnSetDtlService;
    }
    
    public void setEntityLocnSetDtlService(EntityLocnSetDtlService entityLocnSetDtlService) {
    	this.entityLocnSetDtlService = entityLocnSetDtlService;
    } 
    
    public EntityLevelService getEntityLevelService() {
    	return entityLevelService;
    }
    
    public void setEntityLevelService(EntityLevelService entityLevelService) {
    	this.entityLevelService = entityLevelService;
    }    
    
    public UserEntityService getUserEntityService() {
    	return userEntityService;
    }
    
    public void setUserEntityService(UserEntityService userEntityService) {
    	this.userEntityService = userEntityService;
    }    
	
	Calendar calendar = Calendar.getInstance();

	private boolean noAdd = false;

	private int selectedFilterIndex;

	
	public boolean isNoAdd() {
		return noAdd;
	}

	public void setNoAdd(boolean noAdd) {
		this.noAdd = noAdd;
	}
	
	public int getSelectedFilterIndex() {
		return selectedFilterIndex;
	}

	public void setSelectedFilterIndex(int selectedFilterIndex) {
		this.selectedFilterIndex = selectedFilterIndex;
	}
	
	public void loadEntityItemsList(Long parentId){
		this.setEntityItemsList(null);
		List<EntityItem> list = entityItemService.findAllEntityItemsByParent(parentId);
		this.setEntityItemsList(list);
		
		selectedGridEntityItem = null;
		selectedRowIndex = -1;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}
	
	public List<Role> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}	
	
	public List<Menu> getMenusList() {
		return menusList;
	}

	public void setMenusList(List<Menu> menusList) {
		this.menusList = menusList;
	}	

	public List<EntityItem> getEntityItemsList() {
		return entityItemsList;
	}

	public void setEntityItemsList(List<EntityItem> entityItemsList) {
		this.entityItemsList = entityItemsList;
	}	

	public List<EntityLocnSet> getEntityLocnSetList() {
		return entityLocnSetList ;
	}

	public void setEntityLocnSetList(List<EntityLocnSet> entityLocnSetList) {
		this.entityLocnSetList = entityLocnSetList;
	}	
	
	public List<EntityLocnSetDtl> getEntityLocnSetDtlList() {
		return entityLocnSetDtlList ;
	}

	public void setEntityLocnSetDtlList(List<EntityLocnSetDtl> entityLocnSetDtlList) {
		this.entityLocnSetDtlList = entityLocnSetDtlList;
	}	
		
	public List<EntityDefault> getEntityDefaultList() {
		return entityDefaultList;
	}

	public void setEntityDefaultList(List<EntityDefault> entityDefaultList) {
		this.entityDefaultList = entityDefaultList;
	}
	
	public void setEntityLevelsList(List<EntityLevel> entityLevelsList) {
		this.entityLevelsList = entityLevelsList;
	}	
	
	public String getRerender() {
		return rerender;
	}
	
	public void setRerender(String rerender) {
		this.rerender = rerender;
	}
	
	public EntityItem getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(EntityItem selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public String getMode() {
		return this.mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public EntityLevel getSelectedEntityLevel() {
		return this.selectedEntityLevel;
	}
	
	public void setSelectedEntityLevel(EntityLevel entityLevel) {
		this.selectedEntityLevel = entityLevel;
	}	
	
	public UserEntity getSelectedUserEntity() {
		return this.selectedUserEntity;
	}
	
	public void setSelectedUserEntity(UserEntity userEntity) {
		this.selectedUserEntity = userEntity;
	}		

	public EntityItemDTO getEntityItemDTO() {
		return entityItemDTO;
	}
	
	public EntityDefaultDTO getEntityDefaultDTO() {
		return entityDefaultDTO;
	}
	
	public EntityLocnSetDtlDTO getEntityLocnSetDtlDTO() {
		return entityLocnSetDtlDTO;
	}
	
	public EntityLocnSetDTO getEntityLocnSetDTO() {
		return entityLocnSetDTO;
	}
	
	public void validateUserCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value;
	    if (currentAction.isAddAction() && (userService.findById(code) != null)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("User code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
			
	public boolean validateEntity(String code, String name) {
	    // Ensure code is unique and not empty
		logger.debug("code : " + code + "name : " + name );		
		if(name == null || code == null || name.length() <= 0 || code.length() <= 0){
			FacesMessage message  = new FacesMessage("Entity code and Entity name cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }else if (currentAction.isAddAction() && (entityItemService.findByCode(code) != null)) {
			FacesMessage message = new FacesMessage("Entity code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }
		return true;
	}
	
	public Long getBusinessUnitEntityLevelId(){
		Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionColumnDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		List <EntityLevel> entityLevelList = entityLevelService.findAllEntityLevels();
		Long tempId =0l;
		DriverNames driverNames = null;
		if(!entityLevelList.isEmpty()){
			for(EntityLevel entityLevel:entityLevelList){
				driverNames = transactionDriverNamesMap.get(entityLevel.getTransDetailColumnName());
				if(driverNames!=null && (driverNames.getBusinessUnitFlag() != null) && 
						driverNames.getBusinessUnitFlag().equals("1")){
					tempId = entityLevel.getEntityLevelId();
					break;
				}
			}
		}
		
		return tempId;
	}
	
	public List<DataDefinitionColumn> getDataDefinitionColList() {
		return dataDefinitionColList;
	}

	public void setDataDefinitionColList(
			List<DataDefinitionColumn> dataDefinitionColList) {
		this.dataDefinitionColList = dataDefinitionColList;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}

	public void setUserConfig(String file) {
		if (this.userConfig == null) { 
			try {
				this.userConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}	

	private List<SelectItem> entityLevelListMain = null;
	private List<SelectItem> entityLevelListMain1 = null;
	private List<SelectItem> entityLevelListMain2 = null;
	private List<SelectItem> entityLevelListMain3 = null;
	
	private EntityItem filterEntityItem = new EntityItem();
	
	private Map<String, String > entityLevelMapString = null;
	private Map<Long, String > entityLevelMapLong = null;
	
	private Long filterEntityLevelId = 0L;
	private Long filterEntityLevelId1 = 0L;
	private Long filterEntityLevelId2 = 0L;
	private Long filterEntityLevelId3 = 0L;
	
	private String addressLine1 = "";
	private String addressLine2 = "";
	
	private SearchJurisdictionHandler filterHandlerShipto = new SearchJurisdictionHandler(true);
	private TogglePanelController togglePanelController = null;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("EntityInformationPanel", true);
			togglePanelController.addTogglePanel("AddressInformationPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public SearchJurisdictionHandler getFilterHandlerShipto() {
		return filterHandlerShipto;
	}
	
	public String resetFilterSearchAction(){
		filterEntityLevelId = 0L;
		
		if(entityLevelListMain1!=null){
			entityLevelListMain1.clear();
			entityLevelListMain1=null;
		}
		filterEntityLevelId1 = 0L;

		if(entityLevelListMain2!=null){
			entityLevelListMain2.clear();
			entityLevelListMain2=null;
		}
		filterEntityLevelId2 = 0L;
		
		if(entityLevelListMain3!=null){
			entityLevelListMain3.clear();
			entityLevelListMain3=null;
		}
		filterEntityLevelId3 = 0L;

		addressLine1 = "";
		addressLine2 = "";
		
		filterHandlerShipto.reset();
		filterHandlerShipto.setCountry(matrixCommonBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipto.setState("");
			
		return null;
	}
	
	public boolean getDisablelevel1(){
		if(filterEntityLevelId==0L || filterEntityLevelId>=1L){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisablelevel2(){
		if(filterEntityLevelId==0L || filterEntityLevelId>=2L){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean getDisablelevel3(){
		if(filterEntityLevelId==0L || filterEntityLevelId>=3L){
			return false;
		}
		else{
			return true;
		}
	}
	
	private void retrieveComboBoxes(Long entityLevelId){
		
		if(entityLevelId!=null && entityLevelId==1L){
			if(entityLevelListMain1!=null){
				entityLevelListMain1.clear();
				entityLevelListMain1=null;
			}
			filterEntityLevelId1 = 0L;
		}

		if(entityLevelId!=null && (entityLevelId==1L || entityLevelId==2L)){
			if(entityLevelListMain2!=null){
				entityLevelListMain2.clear();
				entityLevelListMain2=null;
			}
			filterEntityLevelId2 = 0L;
		}
		
		if(entityLevelId!=null && (entityLevelId==1L || entityLevelId==2L || entityLevelId==3L)){
			if(entityLevelListMain3!=null){
				entityLevelListMain3.clear();
				entityLevelListMain3=null;
			}
			filterEntityLevelId3 = 0L;
		}
	}
	
	public String retrieveFilterSearchAction(){

		EntityItem filter = new EntityItem();
		
		filter.setAddressLine1(addressLine1);
		filter.setAddressLine2(addressLine2);
		filter.setGeocode(filterHandlerShipto.getGeocode());
		filter.setCity(filterHandlerShipto.getCity());
		filter.setCounty(filterHandlerShipto.getCounty());
		filter.setState(filterHandlerShipto.getState());
		filter.setCountry(filterHandlerShipto.getCountry());
		filter.setZip(filterHandlerShipto.getZip());
		filter.setZipPlus4(filterHandlerShipto.getZipPlus4());
		
		entityDataModel.setCriteria(filter, buildWhereClause());

		selectedGridEntityItem = null;
		selectedRowIndex = -1;
		
		String rowStr = matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = 100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = 4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		
		entityDataModel.setPageSize(rowpage);
		entityDataModel.setDbFetchMultiple(muitiplepage);
			
		return null;
	}
	
	public void sortAction(ActionEvent e) {
		entityDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public String resetSortAction() {
		entityDataModel.resetSortOrder();
		
		selectedGridEntityItem = null;
		selectedRowIndex = -1;
			
		return null;
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	public Long getFilterEntityLevelId() {
		return filterEntityLevelId;
	}

	public void setFilterEntityLevelId(Long filterEntityLevelId) {
		this.filterEntityLevelId = filterEntityLevelId;
	}
	
	public Long getFilterEntityLevelId1() {
		return filterEntityLevelId1;
	}

	public void setFilterEntityLevelId1(Long filterEntityLevelId1) {
		this.filterEntityLevelId1 = filterEntityLevelId1;
	}
	
	public Long getFilterEntityLevelId2() {
		return filterEntityLevelId2;
	}

	public void setFilterEntityLevelId2(Long filterEntityLevelId2) {
		this.filterEntityLevelId2 = filterEntityLevelId2;
	}
	
	public Long getFilterEntityLevelId3() {
		return filterEntityLevelId3;
	}

	public void setFilterEntityLevelId3(Long filterEntityLevelId3) {
		this.filterEntityLevelId3 = filterEntityLevelId3;
	}
	
	public Map<String, String > getEntityLevelMapString(){
    	if(entityLevelMapString==null){
    		entityLevelMapString = new LinkedHashMap<String, String >();
		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMapString.put(entityLevel.getEntityLevelId().toString(), entityLevel.getDescription());
			}
    	}
    	return entityLevelMapString;
    }
	
	public Map<Long, String > getEntityLevelMapLong(){
    	if(entityLevelMapLong==null){
    		entityLevelMapLong = new LinkedHashMap<Long, String >();
		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMapLong.put(entityLevel.getEntityLevelId(), entityLevel.getDescription());
			}
    	}
    	return entityLevelMapLong;
    }
	
	public EntityItem getFilterEntityItem() {
		return filterEntityItem;
	}

	public void setFilterEntityItem(EntityItem filterEntityItem) {
		this.filterEntityItem = filterEntityItem;
	}
	
	public List<SelectItem> getEntityLevelListMain(){
		if(entityLevelListMain==null){
			entityLevelListMain = new ArrayList<SelectItem>();
		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
		    entityLevelListMain.add(new SelectItem(new Long(0L), "Select a Level"));
			for (EntityLevel entityLevel : aList) {
				if(entityLevel.getEntityLevelId().intValue()>0){
					entityLevelListMain.add(new SelectItem(entityLevel.getEntityLevelId(), entityLevel.getDescription()));
				}
			}
	    }
		return entityLevelListMain;
	}
	
	public List<SelectItem> getEntityLevelListMain1(){
    	if(entityLevelListMain1==null){
    		entityLevelListMain1 = new ArrayList<SelectItem>();
    		entityLevelListMain1.add(new SelectItem(new Long(0L),"Select a " + getEntityLevelMapString().get("1")));
    		List<EntityItem> eList = entityItemService.findAllEntityItemsByLevelAndParent(1L, null);//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		entityLevelListMain1.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
    	    	}
    	    } 		
    	}
    	return entityLevelListMain1;
    }
	
	public List<SelectItem> getEntityLevelListMain2(){
    	if(entityLevelListMain2==null){
    		entityLevelListMain2 = new ArrayList<SelectItem>();
    		entityLevelListMain2.add(new SelectItem(new Long(0L),"Select a " + getEntityLevelMapString().get("2")));
    		
    		if(filterEntityLevelId1!=null && filterEntityLevelId1.intValue()>0){	
	    		List<EntityItem> eList = entityItemService.findAllEntityItemsByLevelAndParent(2L, filterEntityLevelId1);
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    		entityLevelListMain2.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
	    	    	}
	    	    } 
    		}
    	}
    	return entityLevelListMain2;
    }
	
	public List<SelectItem> getEntityLevelListMain3(){
    	if(entityLevelListMain3==null){
    		entityLevelListMain3 = new ArrayList<SelectItem>();
    		entityLevelListMain3.add(new SelectItem(new Long(0L),"Select a " + getEntityLevelMapString().get("3")));
    		
    		if(filterEntityLevelId2!=null && filterEntityLevelId2.intValue()>0){	
	    		List<EntityItem> eList = entityItemService.findAllEntityItemsByLevelAndParent(3L, filterEntityLevelId2); 
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    		entityLevelListMain3.add(new SelectItem(entityItem.getEntityId(),entityItem.getEntityCode() + " - " + entityItem.getEntityName()));
	    	    	}
	    	    } 		
    		}
    	}
    	return entityLevelListMain3;
    }
	
	public String filterEntityLevelChanged(){
		
		if(filterEntityLevelId==1L){	
			filterEntityLevelId2=0L;
			
			if(entityLevelListMain3!=null){
				entityLevelListMain3.clear();
				entityLevelListMain3=null;
			}
			filterEntityLevelId3=0L;
		}
		else if(filterEntityLevelId==2L){	
			filterEntityLevelId3=0L;
		}

		return null;
	}
	
	public String filterEntityLevel1Changed(){
		if(entityLevelListMain2!=null){
			entityLevelListMain2.clear();
			entityLevelListMain2=null;
		}
		filterEntityLevelId2=0L;
		
		if(entityLevelListMain3!=null){
			entityLevelListMain3.clear();
			entityLevelListMain3=null;
		}
		filterEntityLevelId3=0L;
		
		return null;
	}
	
	public String filterEntityLevel2Changed(){
		if(entityLevelListMain3!=null){
			entityLevelListMain3.clear();
			entityLevelListMain3=null;
		}
		filterEntityLevelId3=0L;
		 
		return null;
	}
	
	
	public String getShowhide() {
		return showhide;
	}

	public void setShowhide(String showhide) {
		this.showhide = showhide;
	}

	private String buildWhereClause(){
		String whereClause = " 1=1 ";

		if(filterEntityLevelId==0L){	
			String subWhere = "";
			if(filterEntityLevelId1 > 0L){
				if(subWhere.length()==0){
					subWhere += "(entity_id=" + filterEntityLevelId1.intValue();
				}
				else{
					subWhere += " or entity_id=" + filterEntityLevelId1.intValue();
				}
				
				if(filterEntityLevelId2 == 0L){
					subWhere += " or (entity_id IN (SELECT entity_id FROM tb_entity WHERE PARENT_ENTITY_ID = " + filterEntityLevelId1.intValue() +"))";
					
					subWhere += " or (entity_id IN (SELECT entity_id FROM tb_entity WHERE parent_entity_id IN (SELECT entity_id FROM tb_entity WHERE parent_entity_id = "+filterEntityLevelId1.intValue()+")))";
				}
			}
			if(filterEntityLevelId2 > 0L){
				if(subWhere.length()==0){
					subWhere += "(entity_id=" + filterEntityLevelId2.intValue();
				}
				else{
					subWhere += " or entity_id=" + filterEntityLevelId2.intValue();
				}
				
				if(filterEntityLevelId3 == 0L){
					subWhere += " or PARENT_ENTITY_ID = " + filterEntityLevelId2.intValue();
				}
			}
			if(filterEntityLevelId3 > 0L){
				if(subWhere.length()==0){
					subWhere += "(entity_id=" + filterEntityLevelId3.intValue();
				}
				else{
					subWhere += " or entity_id=" + filterEntityLevelId3.intValue();
				}
			}
			
			if(subWhere.length()>0){
				subWhere += ")";
				
				//Like (entity_id = 1 OR entity_id = 15 OR entity_id = 101)		
				whereClause += " and " + subWhere;
			}	
		}
		else if(filterEntityLevelId==1L){
			whereClause += " and entity_level_id=1";
			
			if(filterEntityLevelId1 > 0L){
				whereClause += " and entity_id=" + filterEntityLevelId1.intValue();
			}
		}
		else if(filterEntityLevelId==2L){
			whereClause += " and entity_level_id=2";
			
			if(filterEntityLevelId2 > 0L){
				whereClause += " and entity_id=" + filterEntityLevelId2.intValue();
			}
			else if(filterEntityLevelId1 > 0L){
				whereClause += " and parent_entity_id=" + filterEntityLevelId1.intValue();
			}
		}
		else if(filterEntityLevelId==3L){
			whereClause += " and entity_level_id=3";
			
			if(filterEntityLevelId3 > 0L){
				whereClause += " and entity_id=" + filterEntityLevelId3.intValue();
			}
			else if(filterEntityLevelId2 > 0L){
				whereClause += " and parent_entity_id=" + filterEntityLevelId2.intValue();
			}
			else if(filterEntityLevelId1 > 0L){
				whereClause += " and ((entity_level_id=3 AND parent_entity_id=" + filterEntityLevelId1.intValue() + ") or parent_entity_id in (SELECT entity_id FROM tb_entity WHERE entity_level_id=2 AND PARENT_ENTITY_ID=" + filterEntityLevelId1.intValue() +") )";
			}
		}
			
		return whereClause;
	}
	public boolean getIsEntitySelected() {
		return (showhide!=null && showhide.equals("show"));
	}
	
	public void selectEntityActionListener(ActionEvent event) {	
		showhide = " ";	
	    taxMatrix.setEntityCheck(false);
		taxMatrix.setCommonCallBack(null);
        gsbMatrixBean.setEntityCheck(false);
        gsbMatrixBean.setCommonCallBack(null);
        custLoc.setEntityCheck(false);
        custLoc.setCommonCallBack(null);	 
	}

	public String viewEntityAction(Long entityId,String view) {
		EntityItem entityItem = null;
		entityItem = entityItemService.findById(entityId);
		if(entityItem==null){
			return "";
		}
		this.returnView=view;
		this.currentAction=EditAction.VIEW;
		EntityItem parentEntityItem = null;
		parentEntityItem =  entityItemService.findById(entityItem.getParentEntityId());
		if(entityCopyFromList!=null){
			entityCopyFromList.clear();
			entityCopyFromList = null;
		}
		logger.info("start displayViewEntityAction"); 
		currentAction = EditAction.VIEW;
		entityItemDTO = new EntityItemDTO();
		//Parent level
		if(entityLevelMap==null)
			entityLevelMap=getEntityLevelMap();
		entityItemDTO.setParentLevelDescription(entityLevelMap.get(parentEntityItem.getEntityLevelId()));
		entityItemDTO.setParentCode(parentEntityItem.getEntityCode());
		entityItemDTO.setParentName(parentEntityItem.getEntityName());
		entityItemDTO.setParentDescription(parentEntityItem.getDescription());
		entityItemDTO.setParentActiveFlag(parentEntityItem.getActiveFlag());
		//This level
		entityItemDTO.setEntityLevelId(entityItem.getEntityLevelId());
		entityItemDTO.setParentEntityId(entityItem.getParentEntityId());
		//Rebuild entity level selection
		rebuildEntityLevelList(parentEntityItem.getEntityLevelId(), (entityItem.getEntityId().equals(0L) ? true : false));
		entityItemDTO.setEntityId(entityItem.getEntityId());
		entityItemDTO.setEntityCode(entityItem.getEntityCode());
		entityItemDTO.setEntityName(entityItem.getEntityName());
		entityItemDTO.setDescription(entityItem.getDescription());
		entityItemDTO.setFein(entityItem.getFein());
		entityItemDTO.setAddressLine1(entityItem.getAddressLine1());
		entityItemDTO.setAddressLine2(entityItem.getAddressLine2());
		entityItemDTO.setZipPlus4(entityItem.getZipPlus4());	
		entityItemDTO.setActiveFlag(entityItem.getActiveFlag());
		if(entityItem.getLockFlag()!=null && entityItem.getLockFlag().equals("1")){
			entityLocked = true;
		}
		else{
			entityLocked = false;
		}
		entityItemDTO.setLockFlag(entityItem.getLockFlag());
		//Set Jurisdiction filter
		resetFilter();
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		entityItemDTO.setJurisdictionId(entityItem.getJurisdictionId());
		entityItemDTO.setGeocode(entityItem.getGeocode());
		entityItemDTO.setCountry(entityItem.getCountry());
		entityItemDTO.setState(entityItem.getState());
		entityItemDTO.setCounty(entityItem.getCounty());
		entityItemDTO.setCity(entityItem.getCity());;
		entityItemDTO.setZip(entityItem.getZip());
		entityItemDTO.setUpdateUserId(entityItem.getUpdateUserId());
		entityItemDTO.setUpdateTimestamp(entityItem.getUpdateTimestamp());
		entityItemDTO.setExemptGraceDays(entityItem.getExemptGraceDays());
		filterHandler.setGeocode(entityItem.getGeocode());
		filterHandler.setCountry(entityItem.getCountry());
		filterHandler.setState(entityItem.getState());
		filterHandler.setCounty(entityItem.getCounty());
		filterHandler.setCity(entityItem.getCity());
		filterHandler.setZip(entityItem.getZip());
		//Reset copy from data
		entityCopyFromSelected = entityItemDTO.getEntityId();
		pickComponentListUpdateResult.clear();
		return "entity_view";
	}	

    private String returnBack() {
		if(this.returnView != null) {
			return this.returnView;
		}
		
		return null;
	}
  
    public EntityItem findEntityByEntityCode(String code) {
	  EntityItem entityItem  = entityItemService.findByCode(code);
	  return entityItem;
    }
    public User getCurrentUser() {
		return loginBean.getUser();
   }
}


