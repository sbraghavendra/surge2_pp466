package com.ncsts.view.bean;



import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LogMemory;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.BillTransaction;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLocnSet;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.ProruleAudit;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.User;
import com.ncsts.jsf.model.TransactionBillDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.CustService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.ProcruleService;
import com.ncsts.services.TransactionDetailBillService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.StateObservable;
import com.ncsts.view.util.TogglePanelController;


public abstract class TransactionDetailSaleBean implements BaseMatrixBackingBean.FindIdCallback, BaseMatrixBackingBean.SearchCallback, SearchJurisdictionHandler.SearchJurisdictionCallbackExt,
														TaxCodeCodeHandler.TaxCodeCodeHandlerCallback, SearchJurisdictionHandler.SearchJurisdictionCallbackLocation,
														SearchJurisdictionHandler.SearchJurisdictionIdCallback,
														ViewTaxCodeBean.TaxCodeInfoCallback, AdvancedSortBean.SortCallback, TaxCodeBackingBean.SearchCallback,
														Observer, HttpSessionBindingListener {

	static private Logger logger = LoggerFactory.getInstance().getLogger(TransactionDetailSaleBean.class);
	
	private StateObservable ov = null;

	private static final String DEFAULT_DISPLAY = "basicinformationFilter";
	private String selectedDisplay = DEFAULT_DISPLAY;
	
	private List<SelectItem> statusItems;
	HashMap<String, HashMap<String, String>> currentNexusInfo;
	HashMap<String, String> currentTaxRateInfo;
	
	private HtmlPanelGrid filterSelectionPanel;
	private HtmlPanelGrid taxDriverViewPanel;
	private HtmlPanelGrid locationDriverViewPanel;
	private  CommonCallBack commonCallBack;
	// Selection filter inputs
	private TaxCodeCodeHandler filterTaxCodeHandler;// = new TaxCodeSearchHandler("trans_main")
	private TaxCodeCodeHandler filterTaxCodeHandler1;
	private TaxCodeCodeHandler filterTaxCodeHandler2;
	private String selectedTaxcodeCode;
	private TaxMatrix filterSelectionTaxMatrix = new TaxMatrix();
	private LocationMatrix filterSelectionLocationMatrix = new LocationMatrix();
	private String transactionStatus = "";
	private String entityCode;
	private Date fromGLDate;
	private Date toGLDate;
	private Date certSignDate;
	private Date certExpirationDate;
	private Date invoiceDate;
	private Date enteredDate;
	private Date fromGlTranDate;
	private Date toGlTranDate;
	
	private Date userDate01;
	private Date userDate02;
	private Date userDate03;
	private Date userDate04;
	private Date userDate05;
	private Date userDate06;
	private Date userDate07;
	private Date userDate08;
	private Date userDate09;
	private Date userDate10;
	private Date userDate11;
	private Date userDate12;
	private Date userDate13;
	private Date userDate14;
	private Date userDate15;
	private Date userDate16;
	private Date userDate17;
	private Date userDate18;
	private Date userDate19;
	private Date userDate20;
	private Date userDate21;
	private Date userDate22;
	private Date userDate23;
	private Date userDate24;
	private Date userDate25;

	private Long transactionId;
	private Long jurisdictionId;
	private Long jurisdictionIdShipto;
	private Long jurisdictionIdShipfrom;
	private Long jurisdictionIdOrdrorgn;
	private Long jurisdictionIdOrdracpt;
	private Long jurisdictionIdFirstuse;
	private Long jurisdictionIdBillto;
	private Long jurisdictionIdTtlxfr;
	
	private Long taxMatrixId;
	private Long locationMatrixId;
	private Long glExtractId;
	private  BillTransaction viewsaleId;
	private EntityLocnSet entityLocSet;
	private String selectedCountry = "";
	private String selectedState = "";
	
	private String transactionTypeCodeTrans = "";
	private String ratetypeCodeTrans = "";
	private String methodDeliveryCodeTrans = "";
	private String invoiceNbrInvoice = "";
	
	private int selectedRowIndex = -1;
	private BillTransaction selectedTransaction;
	
	
	private BillTransaction saleTransactionFilter = new BillTransaction();
	
	private int currentTransactionIndex = -1;
//	private List<TransactionDetail> currentTransactionList;
	private List<BillTransaction> reprocessTransactionList=new ArrayList<BillTransaction>();
	private Jurisdiction currentJurisdiction;
	private Boolean applyToAll = false;
	private Boolean changesMade = false;
	
	private SearchBatchIdHandler batchIdHandler = new SearchBatchIdHandler();

	private SearchDriverHandler driverHandler = new SearchDriverHandler();
	
	private SearchJurisdictionHandler newJurisdictionHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler newJurisdictionHandlerModify = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerShipto = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerShipfrom = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerOrdrorgn = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerOrdracpt = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerFirstuse = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandlerBillto = new SearchJurisdictionHandler();
	
	private TaxCodeCodeHandler newTaxCodeHandler = new TaxCodeCodeHandler("taxCode");
	private TaxCodeCodeHandler newTaxCodeHandlerUpdate = new TaxCodeCodeHandler("taxCodeUpdate");
	
	private String comments; // this will hold the modified comments
	private boolean suspendTax; // this boolean value will decide the transaction will be suspended for Tax Matrix.
	private boolean suspendLocation; //this boolean value will decide the transaction will be susspended for Location Matrix
	
	private JurisdictionService jurisdictionService; // this service is declared to get the Jurisdiction details
	private TransactionDetailBillService transactionDetailBillService;
	private BatchMaintenanceService batchMaintenanceService;
	private MatrixCommonBean matrixCommonBean;
	private TransactionViewBean transactionViewBean;
	private LocationMatrixBackingBean locationMatrixBean;
	private TaxMatrixViewBean taxMatrixBean;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	private AdvancedSortBean advancedSortBean;
	private TransactionBlobBean transactionBlobBean;
	private TogglePanelController togglePanelController = null;
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	@Autowired
	private EntityItemService entityItemService;
	
	@Autowired
	private CacheManager cacheManager;
	
	@Autowired
	private CustService custService;
	
	@Autowired
	private CustLocnService custLocnService;
	
	@Autowired
	private CustomerLocationLookupBean customerLocationLookupBean;
	
	@Autowired
	private FilePreferenceBackingBean filePreferenceBean;
	
	//@Autowired
	//private SaleTransactionProcess saleTransactionProcess;
			
	@Autowired
	private TransactionDetailService transactionDetailService;
	
	@Autowired
	private BatchMaintenanceBackingBean batchMaintenanceBean;
	
	@Autowired
	private DriverHandlerBean driverHandlerBean;
	
	private TransactionBillDataModel transactionBillDataModel;
	private HtmlDataTable trDetailTable;
	private boolean displayAllFields = false;
	private boolean indicateAllSelected = false;
	
	private TaxCodeDetail existedTaxCodeDetail;
	
	private boolean displayWarning = false;
	private boolean displayIncomplete = false;
	
	private boolean ignoreTaxCode = false;
	
	private Map<String,Double> selectionTotals = new HashMap<String,Double>();
	
	private List<JurisdictionTaxrate> jurisdictionTaxrateList = new ArrayList<JurisdictionTaxrate>();

	private JurisdictionTaxrate selectedTaxRate;

	private int selectedTaxRateIndex;
	
	private ViewTaxCodeBean viewTaxCodeBean;
	
	private long totalSelected = 1;
	
	private PurchaseTransaction transactionDetail;
	
	private List<PurchaseTransaction> traList = new ArrayList<PurchaseTransaction>();
	
	private List<ProruleAudit> listviewResults=null;
	
	private boolean dispSaveAs = false;
	
	static final int MAX_SELECT_ALL = 10000;

	private OptionService optionService;
	private ProcruleService procruleService;
	private static final String optionCode = "SAVEGRIDCHANGES";
	protected static String STATUS_REPROCESSED = "Reprocessed";	
	public static String STATUS_PTRANS_NOTFOUND = "Purchase Transaction not found.";
	private static final String user = "USER";
	  
	private Boolean renderOptionPanel;
	  
	private boolean saveNow;
	  
	private boolean showOkButton = false;
	
	private long searchWait;
	private long searchSleep;
	
	private TransactionSplittingToolBean<PurchaseTransaction> transactionSplittingToolBean;
	private List<TransactionSplittingToolBean<?>> currentSplittingToolBean;
	
	private String multiTransCode;
	private Long subTransId;
	
	private HtmlDataTable trDetailSplitTable;
	
	private int selectedSplitRowIndex = -1;
	private PurchaseTransaction selectedSplitTransaction;
	private PurchaseTransaction selectedSplitTransactionTemp;
	
	private boolean displayAllViewSplitFields = false;
	
	private boolean showSplitGroupViewSplitButton = false;
	
	private boolean isExpandTransactionInformation = true;
	
	private String viewReturnPage = null;
	
	private Boolean isReadonly = false;

	private User currentUser;
	
	@Autowired
	private UserService userService;
	
	public Boolean getIsReadonly() {
		return isReadonly;
	}
	
	public void setIsReadonly(Boolean isReadonly) {
		this.isReadonly = isReadonly;
	}
	
	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			
			//limited information for View only:
			togglePanelController.addTogglePanel("transactionInformation3", true);
			togglePanelController.addTogglePanel("invoiceInformation3", false);
			togglePanelController.addTogglePanel("locationInformation3", false);
//			togglePanelController.addTogglePanel("taxAmountsInformation3", false);
//			togglePanelController.addTogglePanel("specialTaxingJurisdictions3", false);
			

			//Basic information:
			togglePanelController.addTogglePanel("transactionInformation1", true);
			togglePanelController.addTogglePanel("invoiceInformation1", false);
			togglePanelController.addTogglePanel("locationInformation", false);
			
			//Customer:
			togglePanelController.addTogglePanel("customerInformation", true);
			
			//Invlice:
			togglePanelController.addTogglePanel("invoiceInformation2", true);
			togglePanelController.addTogglePanel("invoiceLineInformation", false);
			togglePanelController.addTogglePanel("miscInformation", false);
			
			//Locations:
			togglePanelController.addTogglePanel("shipToInformation", true);
			togglePanelController.addTogglePanel("shipFromInformation", false);
			togglePanelController.addTogglePanel("orderOriginInformation", false);
			togglePanelController.addTogglePanel("orderAcceptanceInformation", false);
			togglePanelController.addTogglePanel("firstUseInformation", false);
			togglePanelController.addTogglePanel("billToInformation", false);
			togglePanelController.addTogglePanel("titleTransfer", false);
			
			//Tax mount:
			togglePanelController.addTogglePanel("taxAmountsInformation", true);
			togglePanelController.addTogglePanel("specialTaxingJurisdictions", false);	
			togglePanelController.addTogglePanel("stateTierInformation", false);
			togglePanelController.addTogglePanel("countycitySplitInformation", false);
			togglePanelController.addTogglePanel("stjInformation", false);
			
			//Transactions:
			togglePanelController.addTogglePanel("transactionInformation2", true);
			togglePanelController.addTogglePanel("generalLedgerInformation", false);
			togglePanelController.addTogglePanel("lastUpdateInformation", false);
			
			//User fields:
			togglePanelController.addTogglePanel("userTextFields", true);
			togglePanelController.addTogglePanel("userNumberFields", false);
			togglePanelController.addTogglePanel("userDateFields", false);
			
			//View Result:
			togglePanelController.addTogglePanel("resultFields", true);
			togglePanelController.addTogglePanel("ruleResults", false);

			
			
		}
		return togglePanelController;
	}

	public void setTogglePanelController(TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}
	
	public void expandTransactionInformationAction(ActionEvent e) {
		isExpandTransactionInformation = !isExpandTransactionInformation;
    }
	
	public boolean isIsExpandTransactionInformation() {
		return isExpandTransactionInformation;
	}
	
	public CustService getCustService() {
		return custService;
	}

	public void setCustService(CustService custService) {
		this.custService = custService;
	}
	
	public CustLocnService getCustLocnService() {
		return custLocnService;
	}

	public void setCustLocnService(CustLocnService custLocnService) {
		this.custLocnService = custLocnService;
	}
	
	public String resetSearchFilter(){
		//Reset everything
		saleTransactionFilter = new BillTransaction();
		
		custNbrBi = "";
		invoiceLineProductCodeBi = "";
		taxAmtTA = null;
		
		transactionTypeCodeTrans = "";
		ratetypeCodeTrans = "";
		methodDeliveryCodeTrans = "";
		
		invoiceNbrInvoice = "";
		
		//Basic Information//
		
		// 0005681
		filterHandler.reset();
//		filterHandler.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler.setCountry(null);
		filterHandler.setState("");
		
		filterHandler1.reset();
//		filterHandler1.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler1.setCountry(null);
		filterHandler1.setState("");
		
		filterHandler2.reset();
//		filterHandler2.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler2.setCountry(null);
		filterHandler2.setState("");
		
		filterHandler3.reset();
//		filterHandler3.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler3.setCountry(null);
		filterHandler3.setState("");
		
		filterHandler4.reset();
//		filterHandler4.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler4.setCountry(null);
		filterHandler4.setState("");
		
		
		filterHandlerShipto.reset();
//		filterHandlerShipto.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipto.setCountry(null);
		filterHandlerShipto.setState("");
		
		filterHandlerTtlxfr.reset();
//		filterHandlerTtlxfr.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerTtlxfr.setCountry(null);
		filterHandlerTtlxfr.setState("");
		
		filterHandlerShipfrom.reset();
//		filterHandlerShipfrom.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipfrom.setCountry(null);
		filterHandlerShipfrom.setState("");
		
		filterHandlerOrdrorgn.reset();
//		filterHandlerOrdrorgn.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerOrdrorgn.setCountry(null);
		filterHandlerOrdrorgn.setState("");
		
		filterHandlerOrdracpt.reset();
//		filterHandlerOrdracpt.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerOrdracpt.setCountry(null);
		filterHandlerOrdracpt.setState("");
		
		filterHandlerFirstuse.reset();
//		filterHandlerFirstuse.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerFirstuse.setCountry(null);
		filterHandlerFirstuse.setState("");
		
		filterHandlerBillto.reset();
//		filterHandlerBillto.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerBillto.setCountry(null);
		filterHandlerBillto.setState("");		
		
		
		if(filterEntityMenuItems!=null) {
			filterEntityMenuItems.clear();
			filterEntityMenuItems=null;
		}
		if(filterEntityMenuItems1!=null) {
			filterEntityMenuItems1.clear();
			filterEntityMenuItems1=null;
		}
		if(filterEntityMenuItems2!=null) {
			filterEntityMenuItems2.clear();
			filterEntityMenuItems2=null;
		} 
		
		if(filterDivisionEntityMenuItems!=null) {
			filterDivisionEntityMenuItems.clear();
			filterDivisionEntityMenuItems=null;
		}
		if(filterTransDivisionEntityMenuItems!=null) {
			filterTransDivisionEntityMenuItems.clear();
			filterTransDivisionEntityMenuItems=null;
		}

		selectedTaxcodeCode = "";
		filterTaxCodeHandler.reset();
		filterTaxCodeHandler1.reset();
		filterTaxCodeHandler2.reset();
		
		selectedCompanyEntityId = null;
		selectedDivisionEntityId = null;
		
		jurisdictionId = null;
		jurisdictionIdShipto = null;
		jurisdictionIdShipfrom = null;
		jurisdictionIdOrdrorgn = null;
		jurisdictionIdOrdracpt = null;
		jurisdictionIdFirstuse = null;
		jurisdictionIdBillto = null;
		jurisdictionIdTtlxfr = null;
		
		selectedTransCompanyEntityId = null;
		selectedTransDivisionEntityId = null;
			
		fromGLDate = null;
		toGLDate = null;
		fromGlTranDate = null; 		
		toGlTranDate = null; 
		certSignDate = null;
		certExpirationDate = null;
		invoiceDate = null;
		enteredDate = null;
		userDate01 = null;
		userDate02 = null;
		userDate03 = null;
		userDate04 = null;
		userDate05 = null;
		userDate06 = null;
		userDate07 = null;
		userDate08 = null;
		userDate09 = null;
		userDate10 = null;
		userDate11 = null;
		userDate12 = null;
		userDate13 = null;
		userDate14 = null;
		userDate15 = null;
		userDate16 = null;
		userDate17 = null;
		userDate18 = null;
		userDate19 = null;
		userDate20 = null;
		userDate21 = null;
		userDate22 = null;
		userDate23 = null;
		userDate24 = null;
		userDate25 = null; 
		entityCode = "";
		
		return null;
	}
	
	public void fillSearchFilter(){
		
		//Basic Information//
		if(entityCode!=null && entityCode.trim().length()>0){
			saleTransactionFilter.setEntityCode(entityCode.trim());
			
			EntityItem anItem1 = entityItemService.findByCode(entityCode.trim());
			if(anItem1!=null){
				saleTransactionFilter.setEntityId(anItem1.getEntityId());
			}
			else{
				saleTransactionFilter.setEntityId(null);
			}
		}
		else{
			saleTransactionFilter.setEntityCode("");
			saleTransactionFilter.setEntityId(null);
		}
		
		if(selectedCompanyEntityId!=null && selectedCompanyEntityId>0L){
			EntityItem anItem = entityItemService.findById(selectedCompanyEntityId);
			if(anItem!=null){
				saleTransactionFilter.setGlCompanyNbr(anItem.getEntityCode());
			}
			saleTransactionFilter.setGlDivisionNbr("");
		}
		if(selectedDivisionEntityId!=null && selectedDivisionEntityId>0L){
			EntityItem anItem = entityItemService.findById(selectedDivisionEntityId);
			if(anItem!=null){
				saleTransactionFilter.setGlDivisionNbr(anItem.getEntityCode());
			}
		}
		
		if(fromGLDate!=null){
			saleTransactionFilter.setGlDate(new Date(fromGLDate.getTime()));
		}
		if(toGLDate!=null){
			saleTransactionFilter.setGlToDate(new Date(toGLDate.getTime()));
		}
		
		//Same in Basic Information, Invoice, Limited Information (in view only)
		saleTransactionFilter.setTaxcodeCode(selectedTaxcodeCode);
		
		saleTransactionFilter.setShipfromGeocode(filterHandler.getGeocode());
		saleTransactionFilter.setShipfromCity(filterHandler.getCity());
		saleTransactionFilter.setShipfromCounty(filterHandler.getCounty());
		saleTransactionFilter.setShipfromStateCode(filterHandler.getState());
		saleTransactionFilter.setShipfromCountryCode(filterHandler.getCountry());
		saleTransactionFilter.setShipfromZip(filterHandler.getZip());
		
		saleTransactionFilter.setShiptoGeocode(filterHandler1.getGeocode());
		saleTransactionFilter.setShiptoCity(filterHandler1.getCity());
		saleTransactionFilter.setShiptoCounty(filterHandler1.getCounty());
		saleTransactionFilter.setShiptoStateCode(filterHandler1.getState());
		saleTransactionFilter.setShiptoCountryCode(filterHandler1.getCountry());
		saleTransactionFilter.setShiptoZip(filterHandler1.getZip());
		//end of Basic Information//
		
		//Customer//
		//Search cust_nbr & cust_locn_code only
		saleTransactionFilter.setCustName("");
		saleTransactionFilter.setLocationName("");
	
		if(jurisdictionId!=null){
			saleTransactionFilter.setCustJurisdictionId(jurisdictionId);
		}
		
		saleTransactionFilter.setCustGeocode(filterHandler2.getGeocode());
		saleTransactionFilter.setCustCity(filterHandler2.getCity());
		saleTransactionFilter.setCustCounty(filterHandler2.getCounty());
		saleTransactionFilter.setCustStateCode(filterHandler2.getState());
		saleTransactionFilter.setCustCountryCode(filterHandler2.getCountry());
		saleTransactionFilter.setCustZip(filterHandler2.getZip());
		saleTransactionFilter.setCustZipplus4(filterHandler2.getZipPlus4());
		
		if(certSignDate!=null){
			saleTransactionFilter.setCertSignDate(new Date(certSignDate.getTime()));
		}
		
		if(certExpirationDate!=null){
			saleTransactionFilter.setCertExpirationDate(new Date(certExpirationDate.getTime()));
		}
		//end of Customer//
		
		//Invoice//
		if(invoiceDate!=null){
			saleTransactionFilter.setInvoiceDate(new Date(invoiceDate.getTime()));
		}
		
		//This should have been set
		/*
		saleTransactionFilter.setTaxcodeCode(selectedTaxcodeCode);
		if(saleTransactionFilter.getDoNotCalcFlagBoolean()){
			saleTransactionFilter.setDoNotCalcFlag("1");
		}
		if(saleTransactionFilter.getCreditIndBoolean()){
			saleTransactionFilter.setCreditInd("1");
		}
		if(saleTransactionFilter.getTaxPaidToVendorFlagBoolean()){
			saleTransactionFilter.setTaxPaidToVendorFlag("1");
		}
		if(saleTransactionFilter.getExemptIndBoolean()){
			saleTransactionFilter.setExemptInd("1");
		}*/
		//end of Invoice//
		
		//Locations//
		saleTransactionFilter.setShiptoJurisdictionId(jurisdictionIdShipto);
		saleTransactionFilter.setShipfromJurisdictionId(jurisdictionIdShipfrom);
		saleTransactionFilter.setOrdrorgnJurisdictionId(jurisdictionIdOrdrorgn);
		saleTransactionFilter.setOrdracptJurisdictionId(jurisdictionIdOrdracpt);
		saleTransactionFilter.setFirstuseJurisdictionId(jurisdictionIdFirstuse);
		saleTransactionFilter.setBilltoJurisdictionId(jurisdictionIdBillto);
		saleTransactionFilter.setTtlxfrJurisdictionId(jurisdictionIdTtlxfr);
		
		saleTransactionFilter.setShiptoGeocode(filterHandlerShipto.getGeocode());
		saleTransactionFilter.setShiptoCountryCode(filterHandlerShipto.getCountry());
		saleTransactionFilter.setShiptoStateCode(filterHandlerShipto.getState());
		saleTransactionFilter.setShiptoCounty(filterHandlerShipto.getCounty());
		saleTransactionFilter.setShiptoCity(filterHandlerShipto.getCity());
		saleTransactionFilter.setShiptoZip(filterHandlerShipto.getZip());
		saleTransactionFilter.setShiptoZipplus4(filterHandlerShipto.getZipPlus4());
		///
		saleTransactionFilter.setTtlxfrGeocode(filterHandlerTtlxfr.getGeocode());
		saleTransactionFilter.setTtlxfrCountryCode(filterHandlerTtlxfr.getCountry());
		saleTransactionFilter.setTtlxfrStateCode(filterHandlerTtlxfr.getState());
		saleTransactionFilter.setTtlxfrCounty(filterHandlerTtlxfr.getCounty());
		saleTransactionFilter.setTtlxfrCity(filterHandlerTtlxfr.getCity());
		saleTransactionFilter.setTtlxfrZip(filterHandlerTtlxfr.getZip());
		saleTransactionFilter.setTtlxfrZipplus4(filterHandlerTtlxfr.getZipPlus4());
		
		saleTransactionFilter.setShipfromGeocode(filterHandlerShipfrom.getGeocode());
		saleTransactionFilter.setShipfromCountryCode(filterHandlerShipfrom.getCountry());
		saleTransactionFilter.setShipfromStateCode(filterHandlerShipfrom.getState());
		saleTransactionFilter.setShipfromCounty(filterHandlerShipfrom.getCounty());
		saleTransactionFilter.setShipfromCity(filterHandlerShipfrom.getCity());
		saleTransactionFilter.setShipfromZip(filterHandlerShipfrom.getZip());
		saleTransactionFilter.setShipfromZipplus4(filterHandlerShipfrom.getZipPlus4());
		
		saleTransactionFilter.setOrdrorgnGeocode(filterHandlerOrdrorgn.getGeocode());
		saleTransactionFilter.setOrdrorgnCountryCode(filterHandlerOrdrorgn.getCountry());
		saleTransactionFilter.setOrdrorgnStateCode(filterHandlerOrdrorgn.getState());
		saleTransactionFilter.setOrdrorgnCounty(filterHandlerOrdrorgn.getCounty());
		saleTransactionFilter.setOrdrorgnCity(filterHandlerOrdrorgn.getCity());
		saleTransactionFilter.setOrdrorgnZip(filterHandlerOrdrorgn.getZip());
		saleTransactionFilter.setOrdrorgnZipplus4(filterHandlerOrdrorgn.getZipPlus4());
		
		saleTransactionFilter.setOrdracptGeocode(filterHandlerOrdracpt.getGeocode());
		saleTransactionFilter.setOrdracptCountryCode(filterHandlerOrdracpt.getCountry());
		saleTransactionFilter.setOrdracptStateCode(filterHandlerOrdracpt.getState());
		saleTransactionFilter.setOrdracptCounty(filterHandlerOrdracpt.getCounty());
		saleTransactionFilter.setOrdracptCity(filterHandlerOrdracpt.getCity());
		saleTransactionFilter.setOrdracptZip(filterHandlerOrdracpt.getZip());
		saleTransactionFilter.setOrdracptZipplus4(filterHandlerOrdracpt.getZipPlus4());
		
		saleTransactionFilter.setFirstuseGeocode(filterHandlerFirstuse.getGeocode());
  	    saleTransactionFilter.setFirstuseCountryCode(filterHandlerFirstuse.getCountry());
		saleTransactionFilter.setFirstuseStateCode(filterHandlerFirstuse.getState());
		saleTransactionFilter.setFirstuseCounty(filterHandlerFirstuse.getCounty());
		saleTransactionFilter.setFirstuseCity(filterHandlerFirstuse.getCity());
		saleTransactionFilter.setFirstuseZip(filterHandlerFirstuse.getZip());
		saleTransactionFilter.setFirstuseZipplus4(filterHandlerFirstuse.getZipPlus4());
		
		saleTransactionFilter.setBilltoGeocode(filterHandlerBillto.getGeocode());
		saleTransactionFilter.setBilltoCountryCode(filterHandlerBillto.getCountry());
		saleTransactionFilter.setBilltoStateCode(filterHandlerBillto.getState());
		saleTransactionFilter.setBilltoCounty(filterHandlerBillto.getCounty());
		saleTransactionFilter.setBilltoCity(filterHandlerBillto.getCity());
		saleTransactionFilter.setBilltoZip(filterHandlerBillto.getZip());
		saleTransactionFilter.setBilltoZipplus4(filterHandlerBillto.getZipPlus4());
		//end of Locations//
		
		//Tax Amounts//
		//end of Tax Amounts//
		
		//Transaction//
		saleTransactionFilter.setGlCompanyNbr("");
		if(selectedTransCompanyEntityId!=null && selectedTransCompanyEntityId>0L){
			EntityItem anItem = entityItemService.findById(selectedTransCompanyEntityId);
			if(anItem!=null){
				saleTransactionFilter.setGlCompanyNbr(anItem.getEntityCode());
			}
		}
		saleTransactionFilter.setGlDivisionNbr("");
		if(selectedTransDivisionEntityId!=null && selectedTransDivisionEntityId>0L){
			EntityItem anItem = entityItemService.findById(selectedTransDivisionEntityId);
			if(anItem!=null){
				saleTransactionFilter.setGlDivisionNbr(anItem.getEntityCode());
			}
		}
		
		//fromGlTranDate for gl_date ?
		//toGlTranDate for gl_date ?
		if(enteredDate!=null){
			saleTransactionFilter.setEnteredDate(new Date(enteredDate.getTime()));
		}
		
		//end of Transaction//
		
		//User Fields//
		if(userDate01!=null){
			saleTransactionFilter.setUserDate01(new Date(userDate01.getTime()));
		}
		if(userDate02!=null){
			saleTransactionFilter.setUserDate02(new Date(userDate02.getTime()));
		}
		if(userDate03!=null){
			saleTransactionFilter.setUserDate03(new Date(userDate03.getTime()));
		}
		if(userDate04!=null){
			saleTransactionFilter.setUserDate04(new Date(userDate04.getTime()));
		}
		if(userDate05!=null){
			saleTransactionFilter.setUserDate05(new Date(userDate05.getTime()));
		}
		if(userDate06!=null){
			saleTransactionFilter.setUserDate06(new Date(userDate06.getTime()));
		}
		if(userDate07!=null){
			saleTransactionFilter.setUserDate07(new Date(userDate07.getTime()));
		}
		if(userDate08!=null){
			saleTransactionFilter.setUserDate08(new Date(userDate08.getTime()));
		}
		if(userDate09!=null){
			saleTransactionFilter.setUserDate09(new Date(userDate09.getTime()));
		}
		if(userDate10!=null){
			saleTransactionFilter.setUserDate10(new Date(userDate10.getTime()));
		}
		if(userDate11!=null){
			saleTransactionFilter.setUserDate11(new Date(userDate11.getTime()));
		}
		if(userDate12!=null){
			saleTransactionFilter.setUserDate12(new Date(userDate12.getTime()));
		}
		if(userDate13!=null){
			saleTransactionFilter.setUserDate13(new Date(userDate13.getTime()));
		}
		if(userDate14!=null){
			saleTransactionFilter.setUserDate14(new Date(userDate14.getTime()));
		}
		if(userDate15!=null){
			saleTransactionFilter.setUserDate15(new Date(userDate15.getTime()));
		}
		if(userDate16!=null){
			saleTransactionFilter.setUserDate16(new Date(userDate16.getTime()));
		}
		if(userDate17!=null){
			saleTransactionFilter.setUserDate17(new Date(userDate17.getTime()));
		}
		if(userDate18!=null){
			saleTransactionFilter.setUserDate18(new Date(userDate18.getTime()));
		}
		if(userDate19!=null){
			saleTransactionFilter.setUserDate19(new Date(userDate19.getTime()));
		}
		if(userDate20!=null){
			saleTransactionFilter.setUserDate20(new Date(userDate20.getTime()));
		}
		if(userDate21!=null){
			saleTransactionFilter.setUserDate21(new Date(userDate21.getTime()));
		}
		if(userDate22!=null){
			saleTransactionFilter.setUserDate22(new Date(userDate22.getTime()));
		}
		if(userDate23!=null){
			saleTransactionFilter.setUserDate23(new Date(userDate23.getTime()));
		}
		if(userDate24!=null){
			saleTransactionFilter.setUserDate24(new Date(userDate24.getTime()));
		}
		if(userDate25!=null){
			saleTransactionFilter.setUserDate25(new Date(userDate25.getTime()));
		}
		
		//to avoid join with TB_USER_TABLE
//		saleTransactionFilter.setTableName(null);
		
		//end of User Fields//
	}
	
	public void fillSelectedTransaction(){
		
		custNbrBi = saleTransactionFilter.getCustNbr();
		if(saleTransactionFilter.getTbCalcTaxAmt() != null) 
			taxAmtTA = saleTransactionFilter.getTbCalcTaxAmt().doubleValue();
		else 
			taxAmtTA = 0.0;
		
		invoiceLineProductCodeBi = saleTransactionFilter.getInvoiceLineProductCode();
		
		transactionTypeCodeTrans = saleTransactionFilter.getTransactionTypeCode();
		ratetypeCodeTrans = saleTransactionFilter.getRatetypeCode();
		methodDeliveryCodeTrans = saleTransactionFilter.getMethodDeliveryCode();
		
		invoiceNbrInvoice = saleTransactionFilter.getInvoiceNbr();
	
		//Basic Information//
		if(saleTransactionFilter.getEntityId() == null)
			saleTransactionFilter.setEntityId(saleTransactionFilter.getInstanceCreatedId());
		
		EntityItem aEntityItem = this.entityItemService.findById(saleTransactionFilter.getEntityId());
		if(aEntityItem!=null){
			entityCode = aEntityItem.getEntityCode();
		}
		else{
			entityCode = "";
		}
		
		//Reload division combo box
		selectedCompanyEntityId = null;
		selectedDivisionEntityId = null;
		if(filterDivisionEntityMenuItems!=null) {
			filterDivisionEntityMenuItems.clear();
			filterDivisionEntityMenuItems=null;
		}	
		
		if(saleTransactionFilter.getGlCompanyNbr()!=null && saleTransactionFilter.getGlCompanyNbr().length()>0){
			EntityItem anItem = entityItemService.findByCode(saleTransactionFilter.getGlCompanyNbr());
			if(anItem!=null){
				selectedCompanyEntityId = anItem.getEntityId();
			}
			
			if(selectedCompanyEntityId!=null){
				if(saleTransactionFilter.getGlDivisionNbr()!=null && saleTransactionFilter.getGlDivisionNbr().length()>0){
					EntityItem anItem1 = entityItemService.findByCode(saleTransactionFilter.getGlDivisionNbr());
					
					if(anItem1!=null){
						selectedDivisionEntityId = anItem1.getEntityId();
					}
				}
			}
		}
		
		if(saleTransactionFilter.getGlDate()!=null){
			fromGLDate = new Date(saleTransactionFilter.getGlDate().getTime());
			fromGlTranDate = new Date(saleTransactionFilter.getGlDate().getTime());
		}
		
		if(saleTransactionFilter.getGlToDate()!=null){
			toGLDate = new Date(saleTransactionFilter.getGlToDate().getTime());
			toGlTranDate = new Date(saleTransactionFilter.getGlToDate().getTime());
		}
		
		selectedTaxcodeCode = saleTransactionFilter.getTaxcodeCode();
		filterTaxCodeHandler.setTaxcodeCode(selectedTaxcodeCode);
		filterTaxCodeHandler1.setTaxcodeCode(selectedTaxcodeCode);
		filterTaxCodeHandler2.setTaxcodeCode(selectedTaxcodeCode);

		
		//Same in Basic Information, Invoice, Limited Information (in view only)
		saleTransactionFilter.setTaxcodeCode(selectedTaxcodeCode);
		
		filterHandler.setGeocode(saleTransactionFilter.getShipfromGeocode());
		filterHandler.setCity(saleTransactionFilter.getShipfromCity());
		filterHandler.setCounty(saleTransactionFilter.getShipfromCounty());
		filterHandler.setState(saleTransactionFilter.getShipfromStateCode());
		filterHandler.setCountry(saleTransactionFilter.getShipfromCountryCode());
		filterHandler.setZip(saleTransactionFilter.getShipfromZip());
		
		filterHandler1.setGeocode(saleTransactionFilter.getShiptoGeocode());
		filterHandler1.setCity(saleTransactionFilter.getShiptoCity());
		filterHandler1.setCounty(saleTransactionFilter.getShiptoCounty());
		filterHandler1.setState(saleTransactionFilter.getShiptoStateCode());
		filterHandler1.setCountry(saleTransactionFilter.getShiptoCountryCode());
		filterHandler1.setZip(saleTransactionFilter.getShiptoZip());
		
		//For limited view
		filterHandler3.setGeocode(saleTransactionFilter.getShipfromGeocode());
		filterHandler3.setCity(saleTransactionFilter.getShipfromCity());
		filterHandler3.setCounty(saleTransactionFilter.getShipfromCounty());
		filterHandler3.setState(saleTransactionFilter.getShipfromStateCode());
		filterHandler3.setCountry(saleTransactionFilter.getShipfromCountryCode());
		filterHandler3.setZip(saleTransactionFilter.getShipfromZip());
		
		filterHandler4.setGeocode(saleTransactionFilter.getShiptoGeocode());
		filterHandler4.setCity(saleTransactionFilter.getShiptoCity());
		filterHandler4.setCounty(saleTransactionFilter.getShiptoCounty());
		filterHandler4.setState(saleTransactionFilter.getShiptoStateCode());
		filterHandler4.setCountry(saleTransactionFilter.getShiptoCountryCode());
		filterHandler4.setZip(saleTransactionFilter.getShiptoZip());
		
		filterHandler5.setGeocode(saleTransactionFilter.getTtlxfrGeocode());
		filterHandler5.setCity(saleTransactionFilter.getTtlxfrCity());
		filterHandler5.setCounty(saleTransactionFilter.getTtlxfrCounty());
		filterHandler5.setState(saleTransactionFilter.getTtlxfrStateCode());
		filterHandler5.setCountry(saleTransactionFilter.getTtlxfrCountryCode());
		filterHandler5.setZip(saleTransactionFilter.getTtlxfrZip());
		
		//end of Basic Information//
		
		//Customer//
		//Search cust_nbr & cust_locn_code only
		//saleTransactionFilter.setCustName("");
		//saleTransactionFilter.setLocationName("");

		jurisdictionId =saleTransactionFilter.getCustJurisdictionId();

		filterHandler2.setGeocode(saleTransactionFilter.getCustGeocode());
		filterHandler2.setCity(saleTransactionFilter.getCustCity());
		filterHandler2.setCounty(saleTransactionFilter.getCustCounty());
		filterHandler2.setState(saleTransactionFilter.getCustStateCode());
		filterHandler2.setCountry(saleTransactionFilter.getCustCountryCode());
		filterHandler2.setZip(saleTransactionFilter.getCustZip());
		filterHandler2.setZipPlus4(saleTransactionFilter.getCustZipplus4());
		
		certSignDate = null;
		certExpirationDate = null;
		if(saleTransactionFilter.getCertSignDate()!=null){
			certSignDate = new Date(saleTransactionFilter.getCertSignDate().getTime());
		}
		if(saleTransactionFilter.getCertExpirationDate()!=null){
			certExpirationDate = new Date(saleTransactionFilter.getCertExpirationDate().getTime());
		}
		//end of Customer//
		
		//Invoice//
		invoiceDate = null;
		if(saleTransactionFilter.getInvoiceDate()!=null){
			invoiceDate = new Date(saleTransactionFilter.getInvoiceDate().getTime());
		}
		//end of Invoice//
		
		//Locations//
		jurisdictionIdShipto = saleTransactionFilter.getShiptoJurisdictionId();
		jurisdictionIdShipfrom = saleTransactionFilter.getShipfromJurisdictionId();
		jurisdictionIdOrdrorgn = saleTransactionFilter.getOrdrorgnJurisdictionId();
		jurisdictionIdOrdracpt = saleTransactionFilter.getOrdracptJurisdictionId();
		jurisdictionIdFirstuse = saleTransactionFilter.getFirstuseJurisdictionId();
		jurisdictionIdBillto = saleTransactionFilter.getBilltoJurisdictionId();
		jurisdictionIdTtlxfr = saleTransactionFilter.getTtlxfrJurisdictionId();
		
		filterHandlerShipto.setGeocode(saleTransactionFilter.getShiptoGeocode());
		filterHandlerShipto.setCountry(saleTransactionFilter.getShiptoCountryCode());
		filterHandlerShipto.setState(saleTransactionFilter.getShiptoStateCode());
		filterHandlerShipto.setCounty(saleTransactionFilter.getShiptoCounty());
		filterHandlerShipto.setCity(saleTransactionFilter.getShiptoCity());
		filterHandlerShipto.setZip(saleTransactionFilter.getShiptoZip());
		filterHandlerShipto.setZipPlus4(saleTransactionFilter.getShiptoZipplus4());
		
		filterHandlerTtlxfr.setGeocode(saleTransactionFilter.getTtlxfrGeocode());
		filterHandlerTtlxfr.setCountry(saleTransactionFilter.getTtlxfrCountryCode());
		filterHandlerTtlxfr.setState(saleTransactionFilter.getTtlxfrStateCode());
		filterHandlerTtlxfr.setCounty(saleTransactionFilter.getTtlxfrCounty());
		filterHandlerTtlxfr.setCity(saleTransactionFilter.getTtlxfrCity());
		filterHandlerTtlxfr.setZip(saleTransactionFilter.getTtlxfrZip());
		filterHandlerTtlxfr.setZipPlus4(saleTransactionFilter.getTtlxfrZipplus4());
		
		filterHandlerShipfrom.setGeocode(saleTransactionFilter.getShipfromGeocode());
		filterHandlerShipfrom.setCountry(saleTransactionFilter.getShipfromCountryCode());
		filterHandlerShipfrom.setState(saleTransactionFilter.getShipfromStateCode());
		filterHandlerShipfrom.setCounty(saleTransactionFilter.getShipfromCounty());
		filterHandlerShipfrom.setCity(saleTransactionFilter.getShipfromCity());
		filterHandlerShipfrom.setZip(saleTransactionFilter.getShipfromZip());
		filterHandlerShipfrom.setZipPlus4(saleTransactionFilter.getShipfromZipplus4());
		
		filterHandlerOrdrorgn.setGeocode(saleTransactionFilter.getOrdrorgnGeocode());
		filterHandlerOrdrorgn.setCountry(saleTransactionFilter.getOrdrorgnCountryCode());
		filterHandlerOrdrorgn.setState(saleTransactionFilter.getOrdrorgnStateCode());
		filterHandlerOrdrorgn.setCounty(saleTransactionFilter.getOrdrorgnCounty());
		filterHandlerOrdrorgn.setCity(saleTransactionFilter.getOrdrorgnCity());
		filterHandlerOrdrorgn.setZip(saleTransactionFilter.getOrdrorgnZip());
		filterHandlerOrdrorgn.setZipPlus4(saleTransactionFilter.getOrdrorgnZipplus4());
		
		filterHandlerOrdracpt.setGeocode(saleTransactionFilter.getOrdracptGeocode());
		filterHandlerOrdracpt.setCountry(saleTransactionFilter.getOrdracptCountryCode());
		filterHandlerOrdracpt.setState(saleTransactionFilter.getOrdracptStateCode());
		filterHandlerOrdracpt.setCounty(saleTransactionFilter.getOrdracptCounty());
		filterHandlerOrdracpt.setCity(saleTransactionFilter.getOrdracptCity());
		filterHandlerOrdracpt.setZip(saleTransactionFilter.getOrdracptZip());
		filterHandlerOrdracpt.setZipPlus4(saleTransactionFilter.getOrdracptZipplus4());
		
		filterHandlerFirstuse.setGeocode(saleTransactionFilter.getFirstuseGeocode());
		filterHandlerFirstuse.setCountry(saleTransactionFilter.getFirstuseCountryCode());
		filterHandlerFirstuse.setState(saleTransactionFilter.getFirstuseStateCode());
		filterHandlerFirstuse.setCounty(saleTransactionFilter.getFirstuseCounty());
		filterHandlerFirstuse.setCity(saleTransactionFilter.getFirstuseCity());
		filterHandlerFirstuse.setZip(saleTransactionFilter.getFirstuseZip());
		filterHandlerFirstuse.setZipPlus4(saleTransactionFilter.getFirstuseZipplus4());

		filterHandlerBillto.setGeocode(saleTransactionFilter.getBilltoGeocode());
		filterHandlerBillto.setCountry(saleTransactionFilter.getBilltoCountryCode());
		filterHandlerBillto.setState(saleTransactionFilter.getBilltoStateCode());
		filterHandlerBillto.setCounty(saleTransactionFilter.getBilltoCounty());
		filterHandlerBillto.setCity(saleTransactionFilter.getBilltoCity());
		filterHandlerBillto.setZip(saleTransactionFilter.getBilltoZip());
		filterHandlerBillto.setZipPlus4(saleTransactionFilter.getBilltoZipplus4());
		
		//end of Locations//
		
		//Tax Amounts//
		//end of Tax Amounts//
		
		//Transaction//
		//Reload division combo box
		selectedTransCompanyEntityId = null;
		selectedTransDivisionEntityId = null;
		if(filterTransDivisionEntityMenuItems!=null) {
			filterTransDivisionEntityMenuItems.clear();
			filterTransDivisionEntityMenuItems=null;
		}	
		
		if(saleTransactionFilter.getGlCompanyNbr()!=null && saleTransactionFilter.getGlCompanyNbr().length()>0){
			EntityItem anItem = entityItemService.findByCode(saleTransactionFilter.getGlCompanyNbr());
			if(anItem!=null){
				selectedTransCompanyEntityId = anItem.getEntityId();
			}
			
			if(selectedTransCompanyEntityId!=null){
				if(saleTransactionFilter.getGlDivisionNbr()!=null && saleTransactionFilter.getGlDivisionNbr().length()>0){
					EntityItem anItem1 = entityItemService.findByCode(saleTransactionFilter.getGlDivisionNbr());
					
					if(anItem1!=null){
						selectedTransDivisionEntityId = anItem1.getEntityId();
					}
				}
			}
		}
		
		enteredDate = null;
		if(saleTransactionFilter.getEnteredDate()!=null){
			enteredDate = new Date(saleTransactionFilter.getEnteredDate().getTime());
		}
		//end of Transaction//
		
		//User Fields//
		
		userDate01 = null;
		userDate02 = null;
		userDate03 = null;
		userDate04 = null;
		userDate05 = null;
		userDate06 = null;
		userDate07 = null;
		userDate08 = null;
		userDate09 = null;
		userDate10 = null;
		userDate11 = null;
		userDate12 = null;
		userDate13 = null;
		userDate14 = null;
		userDate15 = null;
		userDate16 = null;
		userDate17 = null;
		userDate18 = null;
		userDate19 = null;
		userDate20 = null;
		userDate21 = null;
		userDate22 = null;
		userDate23 = null;
		userDate24 = null;
		userDate25 = null;
	
	
		
		if(saleTransactionFilter.getUserDate01()!=null){
			userDate01 = new Date(saleTransactionFilter.getUserDate01().getTime());
		}
		if(saleTransactionFilter.getUserDate02()!=null){
			userDate02 = new Date(saleTransactionFilter.getUserDate02().getTime());
		}
		if(saleTransactionFilter.getUserDate03()!=null){
			userDate03 = new Date(saleTransactionFilter.getUserDate03().getTime());
		}
		if(saleTransactionFilter.getUserDate04()!=null){
			userDate04 = new Date(saleTransactionFilter.getUserDate04().getTime());
		}
		if(saleTransactionFilter.getUserDate05()!=null){
			userDate05 = new Date(saleTransactionFilter.getUserDate05().getTime());
		}
		if(saleTransactionFilter.getUserDate06()!=null){
			userDate06 = new Date(saleTransactionFilter.getUserDate06().getTime());
		}
		if(saleTransactionFilter.getUserDate07()!=null){
			userDate07 = new Date(saleTransactionFilter.getUserDate07().getTime());
		}
		if(saleTransactionFilter.getUserDate08()!=null){
			userDate08 = new Date(saleTransactionFilter.getUserDate08().getTime());
		}
		if(saleTransactionFilter.getUserDate09()!=null){
			userDate09 = new Date(saleTransactionFilter.getUserDate09().getTime());
		}
		if(saleTransactionFilter.getUserDate10()!=null){
			userDate10 = new Date(saleTransactionFilter.getUserDate10().getTime());
		}
		if(saleTransactionFilter.getUserDate11()!=null){
			userDate11 = new Date(saleTransactionFilter.getUserDate11().getTime());
		}
		if(saleTransactionFilter.getUserDate12()!=null){
			userDate12 = new Date(saleTransactionFilter.getUserDate12().getTime());
		}
		if(saleTransactionFilter.getUserDate13()!=null){
			userDate13 = new Date(saleTransactionFilter.getUserDate13().getTime());
		}
		if(saleTransactionFilter.getUserDate14()!=null){
			userDate14 = new Date(saleTransactionFilter.getUserDate14().getTime());
		}
		if(saleTransactionFilter.getUserDate15()!=null){
			userDate15 = new Date(saleTransactionFilter.getUserDate15().getTime());
		}
		if(saleTransactionFilter.getUserDate16()!=null){
			userDate16 = new Date(saleTransactionFilter.getUserDate16().getTime());
		}
		if(saleTransactionFilter.getUserDate17()!=null){
			userDate17 = new Date(saleTransactionFilter.getUserDate17().getTime());
		}
		if(saleTransactionFilter.getUserDate18()!=null){
			userDate18 = new Date(saleTransactionFilter.getUserDate18().getTime());
		}
		if(saleTransactionFilter.getUserDate19()!=null){
			userDate19 = new Date(saleTransactionFilter.getUserDate19().getTime());
		}
		if(saleTransactionFilter.getUserDate20()!=null){
			userDate20 = new Date(saleTransactionFilter.getUserDate20().getTime());
		}
		if(saleTransactionFilter.getUserDate21()!=null){
			userDate21 = new Date(saleTransactionFilter.getUserDate21().getTime());
		}
		if(saleTransactionFilter.getUserDate22()!=null){
			userDate22 = new Date(saleTransactionFilter.getUserDate22().getTime());
		}
		if(saleTransactionFilter.getUserDate23()!=null){
			userDate23 = new Date(saleTransactionFilter.getUserDate23().getTime());
		}
		if(saleTransactionFilter.getUserDate24()!=null){
			userDate24 = new Date(saleTransactionFilter.getUserDate24().getTime());
		}
		if(saleTransactionFilter.getUserDate25()!=null){
			userDate25 = new Date(saleTransactionFilter.getUserDate25().getTime());
		}
	
		//end of User Fields//

	}
	
	public void fromGlDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());
		Date newDate =((Date)newCalendar.getValue());
			
		fromGlTranDate = newDate;
	}
	
	public void fromGlTranDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());	
		Date newDate =((Date)newCalendar.getValue());

		fromGLDate = newDate;
	}
	
	public void toGlTranDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());
		Date newDate =((Date)newCalendar.getValue());
		
		toGLDate = newDate;
	}
	
	public void toGlDateChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlCalendar newCalendar = (HtmlCalendar) (((HtmlAjaxSupport) e.getComponent()).getParent());
		Date newDate =((Date)newCalendar.getValue());	
		
		toGlTranDate = newDate;
	}
	
	public String toGlDateChanged() {
		toGlTranDate = toGLDate;
		
		return null;
	}
	
	//----- Basic Information  -----//
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler1 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler2 = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandler3 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler4 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandler5 = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler filterHandlerShipto = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerTtlxfr = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerShipfrom = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerOrdrorgn = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerOrdracpt = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerFirstuse = new SearchJurisdictionHandler(true);
	private SearchJurisdictionHandler filterHandlerBillto = new SearchJurisdictionHandler(true);
	
	private List<SelectItem> filterCompanyEntityMenuItems;
	private List<SelectItem> filterTransCompanyEntityMenuItems;
	private List<SelectItem> filterEntityMenuItems;
	private List<SelectItem> filterEntityMenuItems1;
	private List<SelectItem> filterEntityMenuItems2;
	private List<SelectItem> filterTransactionTypeCodeItems;
	private List<SelectItem> filterProcessMethodCodeItems;
	private List<SelectItem> filterRateTypeCodeItems;
	private List<SelectItem> filterMethodOfDeliveryItems;
	private Long selectedCompanyEntityId = null;
	private Long selectedDivisionEntityId = null;
	private Long selectedTransCompanyEntityId = null;
	private Long selectedTransDivisionEntityId = null;

	private List<SelectItem> taxtypeMenuItems = null;
	private List<SelectItem> indicatorMenuItems = null;
	private Map<String,String> billTransactionUserPropertyMap;
	
	public List<SelectItem> getTaxtypeMenuItems() { 
	   if (taxtypeMenuItems == null) {
		   taxtypeMenuItems = new ArrayList<SelectItem>();
		   taxtypeMenuItems.add(new SelectItem("","Select a Tax Type"));
		    
		   List<ListCodes> listCodeList = cacheManager.getListCodesByType("TAXTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeCode()!=null && listcodes.getDescription()!=null){
					taxtypeMenuItems.add(new SelectItem(listcodes.getCodeCode().trim(), listcodes.getDescription().trim()));				
				}
			}
	   }	    
	   return taxtypeMenuItems;
	}
	
	public List<SelectItem> getIndicatorMenuItems() { 
	   if (indicatorMenuItems == null) {
		   indicatorMenuItems = new ArrayList<SelectItem>();
		   indicatorMenuItems.add(new SelectItem("","All"));
		   indicatorMenuItems.add(new SelectItem("1","True"));
		   indicatorMenuItems.add(new SelectItem("0","False"));  
	   }	    
	   return indicatorMenuItems;
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}
	public SearchJurisdictionHandler getFilterHandler1() {
		return filterHandler1;
	}
	public SearchJurisdictionHandler getFilterHandler2() {
		return filterHandler2;
	}
	public SearchJurisdictionHandler getFilterHandler3() {
		return filterHandler3;
	}
	public SearchJurisdictionHandler getFilterHandler4() {
		return filterHandler4;
	}
	public SearchJurisdictionHandler getFilterHandler5() {
		return filterHandler5;
	}
	public SearchJurisdictionHandler getFilterHandlerShipto() {
		return filterHandlerShipto;
	}
	public SearchJurisdictionHandler getFilterHandlerTtlxfr() {
		return filterHandlerTtlxfr;
	}
	public SearchJurisdictionHandler getFilterHandlerShipfrom() {
		return filterHandlerShipfrom;
	}
	public SearchJurisdictionHandler getFilterHandlerOrdrorgn() {
		return filterHandlerOrdrorgn;
	}
	public SearchJurisdictionHandler getFilterHandlerOrdracpt() {
		return filterHandlerOrdracpt;
	}
	public SearchJurisdictionHandler getFilterHandlerFirstuse() {
		return filterHandlerFirstuse;
	}
	public SearchJurisdictionHandler getFilterHandlerBillto() {
		return filterHandlerBillto;
	}
	
	public List<SelectItem> getFilterEntityMenuItems() {
		if(filterEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select an Entity"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItems();
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterEntityMenuItems = selectItems;
		}
 
		return filterEntityMenuItems;
	}
	
	public List<SelectItem> getFilterEntityMenuItems1() {
		if(filterEntityMenuItems1==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select an Entity"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItems();
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterEntityMenuItems1 = selectItems;
		}
 
		return filterEntityMenuItems1;
	}
	
	public List<SelectItem> getFilterEntityMenuItems2() {
		if(filterEntityMenuItems2==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select an Entity"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItems();
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterEntityMenuItems2 = selectItems;
		}
 
		return filterEntityMenuItems2;
	}
	
	public List<SelectItem> getfilterCompanyEntityMenuItems() {
		if(filterCompanyEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select a Company"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItemsByLevel(1L);
	    	
	    	//Level 1 & active
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0 && entityItem.getActiveBooleanFlag()){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterCompanyEntityMenuItems = selectItems;
		}
 
		return filterCompanyEntityMenuItems;
	}
	
	public List<SelectItem> getFilterTransCompanyEntityMenuItems() {
		if(filterTransCompanyEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select a Company"));
	    	
	    	List<EntityItem> eList = entityItemService.findAllEntityItemsByLevel(1L);
	    	
	    	//Level 1 & active
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0 && entityItem.getActiveBooleanFlag()){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterTransCompanyEntityMenuItems = selectItems;
		}
 
		return filterTransCompanyEntityMenuItems;
	}
	
	public void resetJurisdictionFilter() {	
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
		
		filterHandler1.reset();
		filterHandler1.setCountry("");
		filterHandler1.setState("");
		
		filterHandler2.reset();
		filterHandler2.setCountry("");
		filterHandler2.setState("");
		
		filterHandler3.reset();
		filterHandler3.setCountry("");
		filterHandler3.setState("");
		
		filterHandler4.reset();
		filterHandler4.setCountry("");
		filterHandler4.setState("");
		
		filterHandler5.reset();
		filterHandler5.setCountry("");
		filterHandler5.setState("");
		
		filterHandlerShipto.reset();
		filterHandlerShipto.setCountry("");
		filterHandlerShipto.setState("");
		
		filterHandlerTtlxfr.reset();
		filterHandlerTtlxfr.setCountry("");
		filterHandlerTtlxfr.setState("");
		
		filterHandlerShipfrom.reset();
		filterHandlerShipfrom.setCountry("");
		filterHandlerShipfrom.setState("");
		
		filterHandlerOrdrorgn.reset();
		filterHandlerOrdrorgn.setCountry("");
		filterHandlerOrdrorgn.setState("");
		
		filterHandlerOrdracpt.reset();
		filterHandlerOrdracpt.setCountry("");
		filterHandlerOrdracpt.setState("");
		
		filterHandlerFirstuse.reset();
		filterHandlerFirstuse.setCountry("");
		filterHandlerFirstuse.setState("");
		
		filterHandlerBillto.reset();
		filterHandlerBillto.setCountry("");
		filterHandlerBillto.setState("");
	}
	
	public void entityCodeChanged(ActionEvent e) throws AbortProcessingException { 
	}
	
	public void entityCodeChanged(ValueChangeEvent e) { 
		entityCode = (String)e.getNewValue();
	}
	
	public void selectedEntityId1Changed(ValueChangeEvent e) {
	    entityCode = (String)e.getNewValue();	
	}
	
	public String selectedEntityId1Changed() {
		return null;
	}
	
	public String transactionTypeCodeChanged() {
		transactionTypeCodeTrans = saleTransactionFilter.getTransactionTypeCode();
		return null;
	}
	
	public String ratetypeCodeChanged() {
		ratetypeCodeTrans = saleTransactionFilter.getRatetypeCode();
		return null;
	}
	
	public String methodDeliveryCodeChanged() {
		methodDeliveryCodeTrans = saleTransactionFilter.getMethodDeliveryCode();
		return null;
	}
	
	public String transactionTypeCodeTransChanged() {
		saleTransactionFilter.setTransactionTypeCode(transactionTypeCodeTrans);
		return null;
	}
	
	public String ratetypeCodeTransChanged() {
		saleTransactionFilter.setRatetypeCode(ratetypeCodeTrans);
		return null;
	}
	
	public String methodDeliveryCodeTransChanged() {
		saleTransactionFilter.setMethodDeliveryCode(methodDeliveryCodeTrans);
		return null;
	}
	
	public String invoiceNbrChanged() {
		invoiceNbrInvoice = saleTransactionFilter.getInvoiceNbr();
		return null;
	}
	
	public String invoiceNbrInvoiceChanged() {
		saleTransactionFilter.setInvoiceNbr(invoiceNbrInvoice);
		return null;
	}
	
	private String custNbrBi = "";
	private Double taxAmtTA = null;
	private String invoiceLineProductCodeBi = "";
	
	public void setCustNbrBi(String custNbrBi){
		this.custNbrBi = custNbrBi;	
	}
	
	public String getCustNbrBi(){
		return custNbrBi;	
	}
	
	public void setTaxAmtTA(Double taxAmtTA){
		this.taxAmtTA = taxAmtTA;	
	}
	
	public Double getTaxAmtTA(){
		return taxAmtTA;	
	}
	
	public void setInvoiceLineProductCodeBi(String invoiceLineProductCodeBi){
		this.invoiceLineProductCodeBi = invoiceLineProductCodeBi;	
	}
	
	public String getInvoiceLineProductCodeBi(){
		return invoiceLineProductCodeBi;	
	}
	
	public String customerIdChanged() {

		String newCustNbr = saleTransactionFilter.getCustNbr();
		
		if(newCustNbr==null || newCustNbr.length()==0){
			saleTransactionFilter.setCustNbr("");
			saleTransactionFilter.setCustName("");	
			custNbrBi = "";
		}
		else {
			saleTransactionFilter.setCustNbr(newCustNbr);
			custNbrBi = newCustNbr;
			Cust newCust = custService.getCustByNumber(newCustNbr.trim());
			if(newCust==null){
				//saleTransactionFilter.setCustNbr("");
				saleTransactionFilter.setCustName("");	
			}
			else{
				//saleTransactionFilter.setCustNbr(newCust.getCustNbr());
				saleTransactionFilter.setCustName(newCust.getCustName());	
			}
			
		}  
		
		return null;
	}
	
	public String invoiceLineProductCodeBiChanged() {
		String newProductCode = invoiceLineProductCodeBi;

		if(newProductCode==null || newProductCode.length()==0){
			saleTransactionFilter.setInvoiceLineProductCode("");
			invoiceLineProductCodeBi = "";
		}
		else {
			saleTransactionFilter.setInvoiceLineProductCode(newProductCode);
			invoiceLineProductCodeBi = newProductCode;
		}  
		
		return null;
	}
	
	public String invoiceLineProductCodeChanged() {
		String newProductCode = saleTransactionFilter.getInvoiceLineProductCode();

		if(newProductCode==null || newProductCode.length()==0){
			saleTransactionFilter.setInvoiceLineProductCode("");
			invoiceLineProductCodeBi = "";
		}
		else {
			saleTransactionFilter.setInvoiceLineProductCode(newProductCode);
			invoiceLineProductCodeBi = newProductCode;
		}  
		
		return null;
	}

	public String taxAmtChanged() {
		taxAmtTA = saleTransactionFilter.getTbCalcTaxAmt().doubleValue();
		
		return null;
	}
	
	public String taxAmtTAChanged() {
		saleTransactionFilter.setTbCalcTaxAmt(new BigDecimal(taxAmtTA));
		return null;
	}
	
	public String customerIdBiChanged() {

		String newCustNbr = custNbrBi;
		
		if(newCustNbr==null || newCustNbr.length()==0){
			saleTransactionFilter.setCustNbr("");
			saleTransactionFilter.setCustName("");	
			custNbrBi = "";
		}
		else {
			saleTransactionFilter.setCustNbr(newCustNbr);
			custNbrBi = newCustNbr;
			Cust newCust = custService.getCustByNumber(newCustNbr.trim());
			if(newCust==null){
				//saleTransactionFilter.setCustNbr("");
				saleTransactionFilter.setCustName("");	
			}
			else{
				//saleTransactionFilter.setCustNbr(newCust.getCustNbr());
				saleTransactionFilter.setCustName(newCust.getCustName());	
			}
			
		}  
		
		return null;
	}
	
	public void selectedEntityId2Changed(ValueChangeEvent e) {
		entityCode = (String)e.getNewValue();
	}
	
	public String selectedEntityId2Changed() {
		return null;
	}
	
	public void companyEntityItemSelectionChanged(ValueChangeEvent e) {
		selectedCompanyEntityId = (Long)e.getNewValue();
		
		selectedDivisionEntityId = null;
		
		if(filterDivisionEntityMenuItems!=null) {
			filterDivisionEntityMenuItems.clear();
			filterDivisionEntityMenuItems=null;
		}	
	}
	
	public String companyEntityItemSelectionChanged() {
		selectedDivisionEntityId = null;	
		if(filterDivisionEntityMenuItems!=null) {
			filterDivisionEntityMenuItems.clear();
			filterDivisionEntityMenuItems=null;
		}	
		
		selectedTransCompanyEntityId = selectedCompanyEntityId;
		selectedTransDivisionEntityId = null;
		if(filterTransDivisionEntityMenuItems!=null) {
			filterTransDivisionEntityMenuItems.clear();
			filterTransDivisionEntityMenuItems=null;
		}	
		 
		return null;
	}
	
	public void companyEntityItemTransSelectionChanged(ValueChangeEvent e) {
		selectedTransCompanyEntityId = (Long)e.getNewValue();
		
		selectedTransDivisionEntityId = null;
		
		if(filterTransDivisionEntityMenuItems!=null) {
			filterTransDivisionEntityMenuItems.clear();
			filterTransDivisionEntityMenuItems=null;
		}	
	}
	
	public String companyEntityItemTransSelectionChanged() {
		selectedTransDivisionEntityId = null;
		
		if(filterTransDivisionEntityMenuItems!=null) {
			filterTransDivisionEntityMenuItems.clear();
			filterTransDivisionEntityMenuItems=null;
		}	
		
		selectedCompanyEntityId = selectedTransCompanyEntityId;
		selectedDivisionEntityId = null;
		if(filterDivisionEntityMenuItems!=null) {
			filterDivisionEntityMenuItems.clear();
			filterDivisionEntityMenuItems=null;
		}	
		
		return null;
	}
	
	
	public String divisionEntityItemSelectionChanged(){
		selectedTransDivisionEntityId = selectedDivisionEntityId;
		 
		return null;
	}
	
	public String divisionEntityItemTransSelectionChanged(){
		selectedDivisionEntityId = selectedTransDivisionEntityId;
		 
		return null;
	}
	
	public Long getSelectedTransCompanyEntityId(){
		return selectedTransCompanyEntityId;
	}
	public Long getSelectedTransDivisionEntityId(){
		return selectedTransDivisionEntityId;
	}
	public void setSelectedTransCompanyEntityId(Long selectedTransCompanyEntityId){
		this.selectedTransCompanyEntityId = selectedTransCompanyEntityId;
	}
	public void setSelectedTransDivisionEntityId(Long selectedTransDivisionEntityId){
		this.selectedTransDivisionEntityId = selectedTransDivisionEntityId;
	}
	
	public Long getSelectedCompanyEntityId(){
		return selectedCompanyEntityId;
	}
	public Long getSelectedDivisionEntityId(){
		return selectedDivisionEntityId;
	}
	public void setSelectedCompanyEntityId(Long selectedCompanyEntityId){
		this.selectedCompanyEntityId = selectedCompanyEntityId;
	}
	public void setSelectedDivisionEntityId(Long selectedDivisionEntityId){
		this.selectedDivisionEntityId = selectedDivisionEntityId;
	}
	
	private List<SelectItem> filterDivisionEntityMenuItems;
	public List<SelectItem> getFilterDivisionEntityMenuItems() {
		if(filterDivisionEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select a Division"));
	    	
	    	List<EntityItem> eList = null;	
	    	if(selectedCompanyEntityId!=null && selectedCompanyEntityId>0L){
	    		eList =entityItemService.findAllEntityItemsByParent(selectedCompanyEntityId);
	    	}
	    	
	    	//Level 2 & parent Id & active
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0 && entityItem.getEntityLevelId()==2L && entityItem.getActiveBooleanFlag()){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterDivisionEntityMenuItems = selectItems;
		}
 
		return filterDivisionEntityMenuItems;
	}

	private List<SelectItem> filterTransDivisionEntityMenuItems;
	public List<SelectItem> getFilterTransDivisionEntityMenuItems() {
		if(filterTransDivisionEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select a Division"));
	    	
	    	List<EntityItem> eList = null;	
	    	if(selectedTransCompanyEntityId!=null && selectedTransCompanyEntityId>0L){
	    		eList =entityItemService.findAllEntityItemsByParent(selectedTransCompanyEntityId);
	    	}
	    	
	    	//Level 2 & parent Id & active
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0 && entityItem.getEntityLevelId()==2L && entityItem.getActiveBooleanFlag()){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
    	    		}
    	    	}
    	    } 	
	    	    
	    	filterTransDivisionEntityMenuItems = selectItems;
		}
 
		return filterTransDivisionEntityMenuItems;
	}
	
	public List<SelectItem> getFilterTransactionTypeCodeItems() { 
	   if (filterTransactionTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Transaction Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("TRANSTYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterTransactionTypeCodeItems = selectItems;
	   }	    
	   return filterTransactionTypeCodeItems;
	}
	
	public List<SelectItem> getFilterProcessMethodCodeItems() { 
		   if (filterProcessMethodCodeItems == null) {
			   List<SelectItem> selectItems = new ArrayList<SelectItem>();
				
			   selectItems.add(new SelectItem("","Select a Process Method"));
			   List<ListCodes> listCodes = cacheManager.getListCodesByType("PROCMETHOD");
			   if(listCodes != null) {
				   for(ListCodes lc : listCodes) {
					   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
				   }
			   }
				
			   filterProcessMethodCodeItems = selectItems;
		   }	    
		   return filterProcessMethodCodeItems;
		}
	
	public List<SelectItem> getFilterMethodOfDeliveryItems() { 
	   if (filterMethodOfDeliveryItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Method of Delivery"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("MOD");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterMethodOfDeliveryItems = selectItems;
	   }	    
	   return filterMethodOfDeliveryItems;
	}
	
	public List<SelectItem> getFilterRateTypeCodeItems() { 
	   if (filterRateTypeCodeItems == null) {
		   List<SelectItem> selectItems = new ArrayList<SelectItem>();
			
		   selectItems.add(new SelectItem("","Select a Rate Type"));
		   List<ListCodes> listCodes = cacheManager.getListCodesByType("RATETYPE");
		   if(listCodes != null) {
			   for(ListCodes lc : listCodes) {
				   selectItems.add(new SelectItem(lc.getCodeCode(), lc.getDescription()));
			   }
		   }
			
		   filterRateTypeCodeItems = selectItems;
	   }	    
	   return filterRateTypeCodeItems;
	}
	//----- End of Basic Information  -----//
	
	//----- Customer  -----//
	private List<SelectItem> filterCertTypeMenuItems;
	
	public List<SelectItem> getFilterCertTypeMenuItems() {
		if(filterCertTypeMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Certificate Type"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("CERTTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			filterCertTypeMenuItems = selectItems;
			saleTransactionFilter.setCertType("");
		}
		
		return filterCertTypeMenuItems;
	}
	
	public void recalCustomerNumber(ValueChangeEvent vce) {
		String newCustNbr= (String)vce.getNewValue();
		saleTransactionFilter.setCustNbr(newCustNbr);
		
		if(newCustNbr==null || newCustNbr.length()==0){
			saleTransactionFilter.setCustNbr("");
			saleTransactionFilter.setCustName("");	
		}
		else {
			saleTransactionFilter.setCustNbr(newCustNbr);
			Cust newCust = custService.getCustByNumber(newCustNbr.trim());
			if(newCust==null){
				//saleTransactionFilter.setCustNbr("");
				saleTransactionFilter.setCustName("");	
			}
			else{
				//saleTransactionFilter.setCustNbr(newCust.getCustNbr());
				saleTransactionFilter.setCustName(newCust.getCustName());	
			}
			
		}  
	}
	
	public void recalLocationCode(ValueChangeEvent vce) {
		String newCustLocnCode= (String)vce.getNewValue();
		saleTransactionFilter.setCustLocnCode(newCustLocnCode);
		
		if(newCustLocnCode==null || newCustLocnCode.length()==0){
			saleTransactionFilter.setCustLocnCode("");
			saleTransactionFilter.setLocationName("");
		}
		else{
			if(saleTransactionFilter.getCustNbr()!=null && saleTransactionFilter.getCustNbr().length()>0){
				Cust newCust = custService.getCustByNumber(saleTransactionFilter.getCustNbr().trim());
				if(newCust!=null){
					//Customer number is valid.
					
					CustLocn newCustLocn = custLocnService.getCustLocnByCustId(newCust.getCustId(), newCustLocnCode.trim());
					
					if(newCustLocn!=null){
						saleTransactionFilter.setLocationName(newCustLocn.getLocationName());
					}
					else{
						saleTransactionFilter.setLocationName("");
					}
				}
				else{
					saleTransactionFilter.setLocationName("");
				}
			}
			else{
				saleTransactionFilter.setLocationName("");
			}
		}  
	}
	
	public String searchCustomerLocationAction(){
		Cust aFilterCust = new Cust();
		aFilterCust.setCustNbr(saleTransactionFilter.getCustNbr());
		
		CustLocn aFilterCustLocn = new CustLocn();
		aFilterCustLocn.setCustLocnCode(saleTransactionFilter.getCustLocnCode());
		
		customerLocationLookupBean.setBean(this, "transactionDetailSaleBean");
		
		return customerLocationLookupBean.beginLookupAction(aFilterCust, aFilterCustLocn);
	}
	
	public void custLocnSelectionChanged(Cust aCust, CustLocn aCustLocn) {
		saleTransactionFilter.setCustNbr(aCust.getCustNbr());
		saleTransactionFilter.setCustName(aCust.getCustName());	
		saleTransactionFilter.setCustLocnCode(aCustLocn.getCustLocnCode());
		saleTransactionFilter.setLocationName(aCustLocn.getLocationName());
		custNbrBi = aCust.getCustNbr();
	}
	
	
	//----- end of Customer  -----//
	
	//----- Invoice  -----//
	private List<SelectItem> filterInvoiceDiscTypeCodeItems;
	
	private List<SelectItem> filterInvoiceLineDiscTypeCodeItems;
	
	public List<SelectItem> getFilterInvoiceDiscTypeCodeItems() {
		if(filterInvoiceDiscTypeCodeItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Discount Type"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("DISCTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			filterInvoiceDiscTypeCodeItems = selectItems;
		}
		
		return filterInvoiceDiscTypeCodeItems;
	}
	
	private List<SelectItem> filterInvoiceExemptReasonCodeItems;
	
	public List<SelectItem> getFilterInvoiceExemptReasonCodeItems() {
		if(filterInvoiceExemptReasonCodeItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Exempt Reason"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXREASON");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			filterInvoiceExemptReasonCodeItems = selectItems;
		}
		
		return filterInvoiceExemptReasonCodeItems;
	}
	
	public List<SelectItem> getFilterInvoiceLineDiscTypeCodeItems() {
		if(filterInvoiceLineDiscTypeCodeItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Discount Type"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("DISCTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			filterInvoiceLineDiscTypeCodeItems = selectItems;
		}
		
		return filterInvoiceLineDiscTypeCodeItems;
	}
	//----- end of Invoice  -----//

	
	public EntityItemService getEntityItemService() {
    	return entityItemService;
    }
    
    public void setEntityItemService(EntityItemService entityItemService) {
    	this.entityItemService = entityItemService;
    } 
    
    public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	public TransactionDetailSaleBean() {
		filterTaxCodeHandler = new TaxCodeCodeHandler("trans_main");
		filterTaxCodeHandler1 = new TaxCodeCodeHandler("trans_main");
		filterTaxCodeHandler2 = new TaxCodeCodeHandler("trans_main");
		newTaxCodeHandlerUpdate = new TaxCodeCodeHandler("trans_update");
	}
	
	public String getSelectedDisplay() {
		return selectedDisplay;
	}

	public void setSelectedDisplay(String selectedDisplay) {
		this.selectedDisplay = selectedDisplay;
	}
	
	public void displaySelectionChanged(ValueChangeEvent e) {
		this.selectedDisplay = (String)e.getNewValue();
	}

	public ViewTaxCodeBean getViewTaxCodeBean() {
		return viewTaxCodeBean;
	}

	public void setViewTaxCodeBean(ViewTaxCodeBean viewTaxCodeBean) {
		this.viewTaxCodeBean = viewTaxCodeBean;
	}

	public void searchTaxCodeInfoCallback(String taxcodeCountry, String taxcodeState, String taxcodeType, String taxcodeCode, String callingPage){
		viewTaxCodeBean.reset();
		viewTaxCodeBean.setTaxCode(taxcodeCountry, taxcodeState, taxcodeType, taxcodeCode);
		viewTaxCodeBean.setCallingPage(callingPage);
	}

	public TransactionDetailBillService getTransactionDetailBillService() {
		return transactionDetailBillService;
	}

	public void setTransactionDetailBillService(TransactionDetailBillService transactionDetailBillService) {
		this.transactionDetailBillService = transactionDetailBillService;
	}
	
	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
		batchIdHandler.setBatchMaintenanceService(batchMaintenanceService);
	}

	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}
	
	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}
	
	public TransactionBlobBean getTransactionBlobBean() {
		return transactionBlobBean;
	}

	public void setTransactionBlobBean(
			TransactionBlobBean transactionBlobBean) {
		this.transactionBlobBean = transactionBlobBean;
	}

	public List<JurisdictionTaxrate> getJurisdictionTaxrateList() {
		return jurisdictionTaxrateList;
	}

	public void setJurisdictionTaxrateList(
			List<JurisdictionTaxrate> jurisdictionTaxrateList) {
		this.jurisdictionTaxrateList = jurisdictionTaxrateList;
	}
	
	public JurisdictionTaxrate getSelectedTaxRate() {
		return selectedTaxRate;
	}

	public int getSelectedTaxRateIndex() {
		return selectedTaxRateIndex;
	}
	
	
	public List<ProruleAudit> getListviewResults() {
			
		if (getViewsaleId() != null) 
				
		//listviewResults = procruleService.getAllProcAuditRecords(getViewsaleId().getSaletransId());
			listviewResults = procruleService.getAllProcAuditRecords(getViewsaleId().getProcruleSessionId()+"");
			
		selectedRowIndex = -1;
		
	
		return listviewResults;
	}

	public void setListviewResults(List<ProruleAudit> listviewResults) {
		this.listviewResults = listviewResults;
	}

	public void setSelectedTaxRate(JurisdictionTaxrate selectedTaxRate) {
		this.selectedTaxRate = selectedTaxRate;
	}

	public void setSelectedTaxRateIndex(int selectedTaxRateIndex) {
		this.selectedTaxRateIndex = selectedTaxRateIndex;
	}
	
	public Map<String, String> getBillTransactionUserPropertyMap(){
		if(billTransactionUserPropertyMap==null){
			
			Map<String,String> userMap = new HashMap<String,String>();	
			Map<String,DataDefinitionColumn> columnsMap = this.getMatrixCommonBean().getBillTransactionPropertyMap();
			
			DataDefinitionColumn dataDefinitionColumn = null;
			NumberFormat formatter = new DecimalFormat("00");
			String columnName = "";
			
			///userText01
			
			for (int i = 1; i <= 100; i++) {
				columnName = "userText" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn)columnsMap.get(columnName);
				String description = "";
				if(dataDefinitionColumn!=null && dataDefinitionColumn.getDescription()!=null && dataDefinitionColumn.getDescription().length()>0){
					description = dataDefinitionColumn.getDescription();
					if(!description.startsWith("User Text")){
						userMap.put(columnName, description);
					}
					else{
						userMap.put(columnName, "User Text " + i);
					}
				}
				else{
					userMap.put(columnName, "User Text " + i);
				}
			}
			
			for (int i = 1; i <= 25; i++) {
				columnName = "userNumber" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn)columnsMap.get(columnName);
				String description = "";
				if(dataDefinitionColumn!=null && dataDefinitionColumn.getDescription()!=null && dataDefinitionColumn.getDescription().length()>0){
					description = dataDefinitionColumn.getDescription();
					if(!description.startsWith("User Number")){
						userMap.put(columnName, description);
					}
					else{
						userMap.put(columnName, "User Number " + i);
					}
				}
				else{
					userMap.put(columnName, "User Number " + i);
				}
			}
			
			for (int i = 1; i <= 25; i++) {
				columnName = "userDate" + formatter.format(i);
				dataDefinitionColumn = (DataDefinitionColumn)columnsMap.get(columnName);
				String description = "";
				if(dataDefinitionColumn!=null && dataDefinitionColumn.getDescription()!=null && dataDefinitionColumn.getDescription().length()>0){
					description = dataDefinitionColumn.getDescription();
					if(!description.startsWith("User Date")){
						userMap.put(columnName, description);
					}
					else{
						userMap.put(columnName, "User Date " + i);
					}
				}
				else{
					userMap.put(columnName, "User Date " + i);
				}
			}

			billTransactionUserPropertyMap = userMap;
		}
		
		return billTransactionUserPropertyMap;
	}
	
	//public Map<String,DataDefinitionColumn> getSaleTransactionPropertyMap() {
	
	// Method modified for 3987. New boolean variable added 
	public HtmlDataTable getTrDetailTable() {
		if (trDetailTable == null || filePreferenceBean.isMaxdecimalvalChanged()) {
			trDetailTable = new HtmlDataTable();
			trDetailTable.setId("aMDetailList");
			trDetailTable.setVar("trdetail");
			trDetailTable.setRowClasses("odd-row,even-row");
			matrixCommonBean.createBillTransactionDetailTable(trDetailTable,
					"transactionDetailSaleSearchBean", "transactionDetailSaleSearchBean.transactionBillDataModel", 
					matrixCommonBean.getBillTransactionPropertyMap(), true, displayAllFields, false, false, true);
		}
		return trDetailTable;
	}

	public void setTrDetailTable(HtmlDataTable trDetailTable) {
		this.trDetailTable = trDetailTable;
	}
	
	public void showOk(){
		setShowOkButton(true);
		
	}
	
	  public byte getGridOption() {
		  Option option = new Option();
		
			String userCode = null;
			try {
				userCode = matrixCommonBean.getLoginBean().getUserDTO().getUserCode();
			}
			catch(Exception e) {}
			
		    OptionCodePK optionCodePK = new OptionCodePK();
		    optionCodePK.setOptionCode(optionCode);
		    optionCodePK.setOptionTypeCode(user);
		    optionCodePK.setUserCode(userCode);
		    option = optionService.findByPK(optionCodePK);
		    if(option.getValue()==null || option.getValue()==""){
		    	return 2;
		    }
		    return Byte.parseByte(option.getValue());
		  }
	  
	  public void prepareReorderAction(){
		  setShowOkButton(false);
		   if(getGridOption() ==-1){
			 setRenderOptionPanel(true);
		  }
	  }
	  

	  
	  public void saveGridAction(){
		  setSaveNow(true);
		  reArrangeColumns();
	  }
	  
	  public void doNotSave(){
		  setSaveNow(false);
		  reArrangeColumns();
	  }
	  
	  
	  public boolean saveGridLayout(){
			boolean saveGrid = false;
			if(getGridOption() == 1){
				saveGrid = false;
			}
			if (getGridOption() == 0  || saveNow) {
				saveGrid = true;
			}
		  
		  
		  return saveGrid;
	  }
	
	
	// Method which calls the headed re order - 3987
	public void reArrangeColumns(){
		if(matrixCommonBean.getForHeaderRearrangeTemp() != null) {
			matrixCommonBean.setForHeaderRearrange(matrixCommonBean.getForHeaderRearrangeTemp());
		}
	
		matrixCommonBean.createTransactionDetailTable(trDetailTable,
				"transactionDetailSaleBean", "transactionDetailSaleBean.transactionBillDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields, true, saveGridLayout(), false);
			logger.info("Save Pref. "+saveGridLayout());	
	}
	
	public void refreshColumns() {
		matrixCommonBean.refreshColumns(displayAllFields);
		showOk();
	}
	
	public void resetForHeaderRearrangeTemp() {
		matrixCommonBean.setForHeaderRearrangeTemp(null);
	}

	public HtmlPanelGrid getFilterSelectionPanel() {
		// panel construction a function of current entity setting, so it cannot be created once... create each time...
//		if (true || (filterSelectionPanel == null)) {
			filterSelectionPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"filterPanel", 
					"transactionDetailSaleBean", "filterSelectionTaxMatrix", "matrixCommonBean", 
					false, false, false, true, true); 
			
			// Add in location matrix drivers
			MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, 
					matrixCommonBean.getCacheManager(),
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					filterSelectionPanel, 
					"transactionDetailSaleBean", "filterSelectionLocationMatrix", "matrixCommonBean", 
					false, false, false, true, true);
//		}
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
		return filterSelectionPanel;
	}
	
	public void setFilterSelectionPanel(HtmlPanelGrid filterSelectionPanel) {
		this.filterSelectionPanel = filterSelectionPanel;
	}
	
	public HtmlPanelGrid getTaxDriverViewPanel() {
		if (taxDriverViewPanel == null) {
			taxDriverViewPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"taxDriverViewPanel", 
					"transactionDetailSaleBean", "currentTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		matrixCommonBean.prepareTaxDriverPanel(taxDriverViewPanel);
		return taxDriverViewPanel;
	}

	public void setTaxDriverViewPanel(HtmlPanelGrid taxDriverViewPanel) {
		this.taxDriverViewPanel = taxDriverViewPanel;
	}

	public HtmlPanelGrid getLocationDriverViewPanel() {
		if (locationDriverViewPanel == null) {
			locationDriverViewPanel = MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(), 
					matrixCommonBean.getUserEntityService().getEntityItemDTO(),
					"locationDriverViewPanel", 
					"transactionDetailSaleBean", "currentTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		return locationDriverViewPanel;
	}

	public void setLocationDriverViewPanel(HtmlPanelGrid locationDriverViewPanel) {
		this.locationDriverViewPanel = locationDriverViewPanel;
	}

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		this.matrixCommonBean = matrixCommonBean;
		
		transactionStatus = matrixCommonBean.getUserPreferenceDTO().getUserTransInd();
		
		//4764 set default
		if(transactionStatus==null || transactionStatus.length()==0){
			transactionStatus = "*ALL";
		}
		
		filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		filterTaxCodeHandler1.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		filterTaxCodeHandler2.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterTaxCodeHandler1.setCacheManager(matrixCommonBean.getCacheManager());
		filterTaxCodeHandler2.setCacheManager(matrixCommonBean.getCacheManager());
		
		//0004440
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler1.setTaxCodeBackingBean(taxCodeBackingBean);
		
		filterTaxCodeHandler.setCallback(this);
		filterTaxCodeHandler1.setCallback(this);
		filterTaxCodeHandler2.setCallback(this);
		
		newTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		newTaxCodeHandlerUpdate.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());	
		
		
		
		newTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		newTaxCodeHandlerUpdate.setCacheManager(matrixCommonBean.getCacheManager());

		/*currentJurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		currentJurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());*/
		newJurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		newJurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		newJurisdictionHandlerModify.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		newJurisdictionHandlerModify.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		
		jurisdictionHandlerShipto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerShipto.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandlerShipfrom.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerShipfrom.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandlerOrdrorgn.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerOrdrorgn.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandlerOrdracpt.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerOrdracpt.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandlerFirstuse.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerFirstuse.setCacheManager(matrixCommonBean.getCacheManager());
		
		jurisdictionHandlerBillto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandlerBillto.setCacheManager(matrixCommonBean.getCacheManager());
		
		driverHandler.setDriverReferenceService(matrixCommonBean.getDriverReferenceService());
		driverHandler.setCacheManager(matrixCommonBean.getCacheManager());

		batchIdHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler.reset();
//		filterHandler.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler.setState("");

		
		filterHandler1.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler1.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler1.reset();
//		filterHandler1.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler1.setState("");

		
		filterHandler2.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler2.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler2.reset();
//		filterHandler2.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler2.setState("");

		
		filterHandler3.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler3.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler3.reset();
//		filterHandler3.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler3.setState("");

		
		filterHandler4.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler4.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler4.reset();
//		filterHandler4.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler4.setState("");

		filterHandler5.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler5.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandler5.reset();
//		filterHandler5.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandler5.setState("");
		
		filterHandlerShipto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerShipto.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerShipto.reset();
//		filterHandlerShipto.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipto.setState("");
		
		filterHandlerTtlxfr.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerTtlxfr.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerTtlxfr.reset();
//		filterHandlerTtlxfr.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerTtlxfr.setState("");
	
		filterHandlerShipfrom.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerShipfrom.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerShipfrom.reset();
//		filterHandlerShipfrom.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerShipfrom.setState("");

		
		filterHandlerOrdrorgn.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerOrdrorgn.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerOrdrorgn.reset();
//		filterHandlerOrdrorgn.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerOrdrorgn.setState("");

		
		filterHandlerOrdracpt.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerOrdracpt.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerOrdracpt.reset();
//		filterHandlerOrdracpt.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerOrdracpt.setState("");

		
		filterHandlerFirstuse.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerFirstuse.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerFirstuse.reset();
//		filterHandlerFirstuse.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerFirstuse.setState("");

		
		filterHandlerBillto.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandlerBillto.setCacheManager(matrixCommonBean.getCacheManager());
		filterHandlerBillto.reset();
//		filterHandlerBillto.setCountry(filePreferenceBean.getUserPreferenceDTO().getUserCountry());
		filterHandlerBillto.setState("");
				
		filterHandler.setCallbackLocation(this);
		filterHandler1.setCallbackLocation(this);
		filterHandler2.setCallbackLocation(this);
		filterHandler5.setCallbackLocation(this);
		filterHandlerShipto.setCallbackLocation(this);
		filterHandlerTtlxfr.setCallbackLocation(this);
		filterHandlerShipfrom.setCallbackLocation(this);
		filterHandlerOrdrorgn.setCallbackLocation(this);
		filterHandlerOrdracpt.setCallbackLocation(this);
		filterHandlerFirstuse.setCallbackLocation(this);
		filterHandlerBillto.setCallbackLocation(this);
	}
	
	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			newJurisdictionHandler.setChanged();
			jurisdictionHandler.setChanged();
			jurisdictionHandlerShipto.setChanged();
			jurisdictionHandlerShipfrom.setChanged();
			jurisdictionHandlerOrdrorgn.setChanged();
			jurisdictionHandlerOrdracpt.setChanged();
			jurisdictionHandlerFirstuse.setChanged();
			jurisdictionHandlerBillto.setChanged();
		}
	}
	
	public void valueBound(HttpSessionBindingEvent event){
		StateObservable ov = StateObservable.getInstance();
		this.ov = ov;
		this.ov.addObserver(this);
	}

	public void valueUnbound(HttpSessionBindingEvent event){	
		if(ov!=null){
			ov.deleteObserver(this);
		}
	}

	public TransactionViewBean getTransactionViewBean() {
		return transactionViewBean;
	}

	public void setTransactionViewBean(TransactionViewBean transactionViewBean) {
		this.transactionViewBean = transactionViewBean;
	}

	public LocationMatrixBackingBean getLocationMatrixBean() {
		return locationMatrixBean;
	}

	public void setLocationMatrixBean(LocationMatrixBackingBean locationMatrixBean) {
		this.locationMatrixBean = locationMatrixBean;
	}

	public TaxMatrixViewBean getTaxMatrixBean() {
		return taxMatrixBean;
	}

	public void setTaxMatrixBean(TaxMatrixViewBean taxMatrixBean) {
		this.taxMatrixBean = taxMatrixBean;
	}

	public TaxJurisdictionBackingBean getTaxJurisdictionBean() {
		return taxJurisdictionBean;
	}

	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}

	public TaxMatrix getFilterSelectionTaxMatrix() {
		return filterSelectionTaxMatrix;
	}

	public LocationMatrix getFilterSelectionLocationMatrix() {
		return filterSelectionLocationMatrix;
	}

	public Date getFromGLDate() {
		return fromGLDate;
	}

	public void setFromGLDate(Date fromGLDate) {
		this.fromGLDate = fromGLDate;
	}

	public Date getToGLDate() {
		return toGLDate;
	}

	public void setToGLDate(Date toGLDate) {
		this.toGLDate = toGLDate;
	}
	
	public Date getCertSignDate() {
		return certSignDate;
	}

	public void setCertSignDate(Date certSignDate) {
		this.certSignDate = certSignDate;
	}

	public Date getCertExpirationDate() {
		return certExpirationDate;
	}

	public void setCertExpirationDate(Date certExpirationDate) {
		this.certExpirationDate = certExpirationDate;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	public Date getEnteredDate() {
		return enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	public Date getFromGlTranDate() {
		return fromGlTranDate;
	}

	public void setFromGlTranDate(Date fromGlTranDate) {
		this.fromGlTranDate = fromGlTranDate;
	}
	
	public Date getToGlTranDate() {
		return toGlTranDate;
	}

	public void setToGlTranDate(Date toGlTranDate) {
		this.toGlTranDate = toGlTranDate;
	}
	
	public Date getUserDate01() {
		return userDate01;
	}

	public void setUserDate01(Date userDate01) {
		this.userDate01 = userDate01;
	}
	
	public Date getUserDate02() {
		return userDate02;
	}

	public void setUserDate02(Date userDate02) {
		this.userDate02 = userDate02;
	}
	
	public Date getUserDate03() {
		return userDate03;
	}

	public void setUserDate03(Date userDate03) {
		this.userDate03 = userDate03;
	}
	
	public Date getUserDate04() {
		return userDate04;
	}

	public void setUserDate04(Date userDate04) {
		this.userDate04 = userDate04;
	}
	
	public Date getUserDate05() {
		return userDate05;
	}

	public void setUserDate05(Date userDate05) {
		this.userDate05 = userDate05;
	}
	
	public Date getUserDate06() {
		return userDate06;
	}

	public void setUserDate06(Date userDate06) {
		this.userDate06 = userDate06;
	}
	
	public Date getUserDate07() {
		return userDate07;
	}

	public void setUserDate07(Date userDate07) {
		this.userDate07 = userDate07;
	}
	
	public Date getUserDate08() {
		return userDate08;
	}

	public void setUserDate08(Date userDate08) {
		this.userDate08 = userDate08;
	}
	
	public Date getUserDate09() {
		return userDate09;
	}

	public void setUserDate09(Date userDate09) {
		this.userDate09 = userDate09;
	}
	
	public Date getUserDate10() {
		return userDate10;
	}

	public void setUserDate10(Date userDate10) {
		this.userDate10 = userDate10;
	}
	
	public Date getUserDate11() {
		return userDate11;
	}

	public void setUserDate11(Date userDate11) {
		this.userDate11 = userDate11;
	}
	
	public Date getUserDate12() {
		return userDate12;
	}

	public void setUserDate12(Date userDate12) {
		this.userDate12 = userDate12;
	}
	
	public Date getUserDate13() {
		return userDate13;
	}

	public void setUserDate13(Date userDate13) {
		this.userDate13 = userDate13;
	}
	
	public Date getUserDate14() {
		return userDate14;
	}

	public void setUserDate14(Date userDate14) {
		this.userDate14 = userDate14;
	}
	
	public Date getUserDate15() {
		return userDate15;
	}

	public void setUserDate15(Date userDate15) {
		this.userDate15 = userDate15;
	}
	
	public Date getUserDate16() {
		return userDate16;
	}

	public void setUserDate16(Date userDate16) {
		this.userDate16 = userDate16;
	}
	
	public Date getUserDate17() {
		return userDate17;
	}

	public void setUserDate17(Date userDate17) {
		this.userDate17 = userDate17;
	}
	
	public Date getUserDate18() {
		return userDate18;
	}

	public void setUserDate18(Date userDate18) {
		this.userDate18 = userDate18;
	}
	
	public Date getUserDate19() {
		return userDate19;
	}

	public void setUserDate19(Date userDate19) {
		this.userDate19 = userDate19;
	}
	
	public Date getUserDate20() {
		return userDate20;
	}

	public void setUserDate20(Date userDate20) {
		this.userDate20 = userDate20;
	}
	
	public Date getUserDate21() {
		return userDate21;
	}

	public void setUserDate21(Date userDate21) {
		this.userDate21 = userDate21;
	}
	
	public Date getUserDate22() {
		return userDate22;
	}

	public void setUserDate22(Date userDate22) {
		this.userDate22 = userDate22;
	}
	
	public Date getUserDate23() {
		return userDate23;
	}

	public void setUserDate23(Date userDate23) {
		this.userDate23 = userDate23;
	}
	
	public Date getUserDate24() {
		return userDate24;
	}

	public void setUserDate24(Date userDate24) {
		this.userDate24 = userDate24;
	}
	
	public Date getUserDate25() {
		return userDate25;
	}

	public void setUserDate25(Date userDate25) {
		this.userDate25 = userDate25;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}
	
	public Long getJurisdictionIdShipto() {
		return jurisdictionIdShipto;
	}
	
	public void setJurisdictionIdShipto(Long jurisdictionIdShipto) {
		this.jurisdictionIdShipto = jurisdictionIdShipto;
	}
	
	public Long getJurisdictionIdTtlxfr() {
		return jurisdictionIdTtlxfr;
	}
	
	public void setJurisdictionIdTtlxfr(Long jurisdictionIdTtlxfr) {
		this.jurisdictionIdTtlxfr = jurisdictionIdTtlxfr;
	}

	public Long getJurisdictionIdShipfrom() {
		return jurisdictionIdShipfrom;
	}
	
	public void setJurisdictionIdShipfrom(Long jurisdictionIdShipfrom) {
		this.jurisdictionIdShipfrom = jurisdictionIdShipfrom;
	}
	
	public Long getJurisdictionIdOrdrorgn() {
		return jurisdictionIdOrdrorgn;
	}
	
	public void setJurisdictionIdOrdrorgn(Long jurisdictionIdOrdrorgn) {
		this.jurisdictionIdOrdrorgn = jurisdictionIdOrdrorgn;
	}

	public Long getJurisdictionIdOrdracpt() {
		return jurisdictionIdOrdracpt;
	}
	
	public void setJurisdictionIdOrdracpt(Long jurisdictionIdOrdracpt) {
		this.jurisdictionIdOrdracpt = jurisdictionIdOrdracpt;
	}

	public Long getJurisdictionIdFirstuse() {
		return jurisdictionIdFirstuse;
	}
	
	public void setJurisdictionIdFirstuse(Long jurisdictionIdFirstuse) {
		this.jurisdictionIdFirstuse = jurisdictionIdFirstuse;
	}
	
	public Long getJurisdictionIdBillto() {
		return jurisdictionIdBillto;
	}
	
	public void setJurisdictionIdBillto(Long jurisdictionIdBillto) {
		this.jurisdictionIdBillto = jurisdictionIdBillto;
	}

	public Long getTaxMatrixId() {
		return taxMatrixId;
	}

	public void setTaxMatrixId(Long taxMatrixId) {
		this.taxMatrixId = taxMatrixId;
	}

	public Long getLocationMatrixId() {
		return locationMatrixId;
	}

	public void setLocationMatrixId(Long locationMatrixId) {
		this.locationMatrixId = locationMatrixId;
	}

	public Long getGlExtractId() {
		return glExtractId;
	}

	public void setGlExtractId(Long glExtractId) {
		this.glExtractId = glExtractId;
	}

	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}
	
	public TaxCodeCodeHandler getFilterTaxCodeHandler1() {
		return filterTaxCodeHandler1;
	}
	
	public TaxCodeCodeHandler getFilterTaxCodeHandler2() {
		return filterTaxCodeHandler2;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public boolean isDisplayAllFields() {
		return displayAllFields;
	}

	public void setDisplayAllFields(boolean displayAllFields) {
		this.displayAllFields = displayAllFields;
	}

	public boolean getTaxMatrixIdDisabled() {
		return ((transactionStatus != null) && transactionStatus.equalsIgnoreCase("UST"));
	}

	public boolean getLocationMatrixIdDisabled() {
		return ((transactionStatus != null) && transactionStatus.equalsIgnoreCase("USL"));
	}
	
	public boolean getTaxCodeDisabled() {
		//return ((transactionStatus == null) || !transactionStatus.startsWith("P"));
		return ((transactionStatus == "") || transactionStatus.equalsIgnoreCase("U")|| transactionStatus.equalsIgnoreCase("US")|| transactionStatus.startsWith("USL")
				|| transactionStatus.startsWith("UST")|| transactionStatus.startsWith("UH") || transactionStatus.startsWith("FS"));
	}
	
	public Boolean getMultiSelect() {
		return ((transactionStatus != null) && 
				!transactionStatus.equals("") &&
				!transactionStatus.equals("*ALL") &&
				!transactionStatus.equals("U") &&
				!transactionStatus.equals("US"));
	}
	
	public int getSelectionCount() {
		return Math.max(getMultiSelect()? getSelectedIds().size():0,
				(selectedTransaction != null)? 1:0);
	}
	/*
	 * 3922 changed this to lists instead of sets in order to preserve an order... 
	 */
	private List<Long> getSelectedIds() {
		if (getMultiSelect() && (transactionBillDataModel.getSelectionMap().size() > 0)) {
			List<Long> result = new ArrayList<Long>();
			StringBuffer sb =  new StringBuffer();
			//go over everything that has been selected, and if it's true add it in
			for (Map.Entry<Long, Boolean> e : transactionBillDataModel.getSelectionMap().entrySet()) {
					if (logger.isDebugEnabled() && e.getValue()) {
						if (sb.length() > 0) sb.append(", ");
						sb.append(e.getKey().toString());
					}
					if (e.getValue()) {
						result.add(e.getKey());
					}
				}
			return result;
		} else {
			List<Long> result = new ArrayList<Long>();
			if (selectedTransaction != null) {
				result.add(selectedTransaction.getBilltransId());
			}
			return result;
		}

	}
	
	public BillTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public BillTransaction getSaleTransactionFilter() {
		return saleTransactionFilter;
	}
	
	public void setSaleTransactionFilter(BillTransaction saleTransactionFilter) {
		this.saleTransactionFilter = saleTransactionFilter;
	}
	
	public boolean getValidSelection() {
		return (selectedTransaction !=null);
	}
	
	public String getValidTaxMessage() {
		String message = "";
		if(selectedTransaction !=null){
			DecimalFormat formatter = new DecimalFormat("#0.00");
			RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
			formatter.setRoundingMode(ROUNDING_MODE);
			
			message = "Total Tax:  $";
			if(selectedTransaction.getTbCalcTaxAmt()!=null){
				message = message + formatter.format(selectedTransaction.getTbCalcTaxAmt());
			}
			else{
				message = message + "0.00";
			}
			
			/*
			message = message + "    Combined Rate:  ";
			if(selectedTransaction.getCombinedRate()!=null){
				message = message + (selectedTransaction.getCombinedRate().floatValue()*100f);
			}
			else{
				message = message + "0.00";
			}
			
			message = message + "%";
			*/
		}

		return message;
	}
	
	public boolean getDisplayStateTaxRateBox(){
		if(saleTransactionFilter !=null && 
				((saleTransactionFilter.getStateTier2TaxAmt()!=null && saleTransactionFilter.getStateTier2TaxAmt().doubleValue()!=0.0d) ||   
				 (saleTransactionFilter.getStateTier3TaxAmt()!=null && saleTransactionFilter.getStateTier3TaxAmt().doubleValue()!=0.0d) )){
			//When State Tier 2 or 3 amount <> 0, then the State Tax Rate box should not show.
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		BillTransaction selectedTransaction = new BillTransaction();
		//selectedTransaction.setTbCalcTaxAmt(109.0);
		//selectedTransaction.setCombinedRate(0.07);
		
		Double aDouble = 0.07;
		
		System.out.println("aDouble : " + aDouble.floatValue()*100);
		
		String message = "";
		if(selectedTransaction !=null){
			DecimalFormat formatter = new DecimalFormat("#0.00");
			RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
			formatter.setRoundingMode(ROUNDING_MODE);
			message = "Total Tax:   $" + formatter.format(selectedTransaction.getTbCalcTaxAmt()) + "         Combined Rate:   " + (selectedTransaction.getCombinedRate().floatValue()*100) + "%";
		}
		
		System.out.println("message : " + message);
		
		String value = "0.0001";
		Float aFloat =  Float.parseFloat(value);
		aFloat = aFloat * 100.0f;
		
		String ssss = aFloat.toString() + "%";
		System.out.println("aFloat : " + aFloat);
		System.out.println("ssss : " + ssss);
	}
	
	public boolean getValidSplitSelection() {
		return selectedSplitTransaction != null;
	}
	
	public boolean getDisplaySplitGroupViewSplit() {
		return showSplitGroupViewSplitButton;
	}
	
	public boolean getDisplaySplitGroupViewAlloc() {
		return selectedSplitTransaction != null && "OA".equalsIgnoreCase(selectedSplitTransaction.getMultiTransCode()) && !showSplitGroupViewSplitButton;
	}
	
	/**
	 * This is actually returning if the select all checkbox is checked -
	 * 
	 */
	public boolean isAllSelected() {
		return indicateAllSelected;
	}
	public void setAllSelected(boolean indicateAllSelected) {
		this.indicateAllSelected = indicateAllSelected;
	}
	public void selectAllChange(ActionEvent e){
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		transactionBillDataModel.tooggleSelectAll((Boolean)check.getValue(), MAX_SELECT_ALL);
	}
	public boolean isSelectAllEnabled(){
		return true;
		//we used to use this as a limiter...
		//return (transactionBillDataModel.getRowCount() < MAX_SELECT_ALL); 
	}
	
	public String getSelectAllCaption(){
		if(transactionBillDataModel.getRowCount() <= MAX_SELECT_ALL){
			return "Select All";
		} else {
			return "Select First "+MAX_SELECT_ALL;
		}
	}
	
	/**
	 * A warning is posted if they hit select all but not all were actually selected...
	 */
	public String getSelectAllWarning(){
		if( indicateAllSelected  && transactionBillDataModel.getRowCount() > MAX_SELECT_ALL){
			return " - Warning: "+MAX_SELECT_ALL + " record selection limit";
		} else {
			return "";			
		}
	}
	
	public void resetDetailTable(){
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		boolean isMaxdecValChanged = filePreferenceBean.isMaxdecimalvalChanged();
		
		
		if(rowpage==transactionBillDataModel.getPageSize() && muitiplepage==transactionBillDataModel.getDbFetchMultiple() && !isMaxdecValChanged){
			return;
		}
		
		// Rebuild the table
		logger.debug("Rebuilding table - resetDetailTable: " + displayAllFields);
		matrixCommonBean.createBillTransactionDetailTable(trDetailTable,
				"transactionDetailSaleBean", "transactionDetailSaleBean.transactionBillDataModel", 
				matrixCommonBean.getBillTransactionPropertyMap(), true, displayAllFields, false, false, true);
		
		transactionBillDataModel.setPageSize(rowpage);
		transactionBillDataModel.setDbFetchMultiple(muitiplepage);
		
		resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	

	public void displayChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		// Rebuild the table
		logger.debug("Rebuilding table - all fields: " + displayAllFields);
		matrixCommonBean.createBillTransactionDetailTable(trDetailTable,
				"transactionDetailSaleSearchBean", "transactionDetailSaleSearchBean.transactionBillDataModel", 
				matrixCommonBean.getBillTransactionPropertyMap(), false, displayAllFields, false, false, true);
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionBillDataModel.setPageSize(rowpage);
		transactionBillDataModel.setDbFetchMultiple(muitiplepage);
		
		resetForHeaderRearrangeTemp();
		
		LogMemory.executeGC();
	}
	
	public void displayViewSplitChange(ActionEvent e) {
		HtmlSelectBooleanCheckbox check = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
	    displayAllViewSplitFields = (check.getSubmittedValue() == null)? 
	    		((check.getValue() == null)? false:(Boolean) check.getValue()):Boolean.parseBoolean((String) check.getSubmittedValue());
		matrixCommonBean.createTransactionDetailTable(trDetailSplitTable,
				"transactionDetailSaleBean", "transactionDetailSaleBean.splitGroupDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), false, displayAllViewSplitFields, false, false, true, null);
	}
	
	public Map<String, String> getTransactionSortMap(){
		Map<String, String> sortMap = new HashMap<String, String>();
		matrixCommonBean.createTransactionSortMap(sortMap,
				"transactionDetailSaleBean", "transactionDetailSaleBean.transactionBillDataModel", 
				matrixCommonBean.getTransactionPropertyMap(), true, displayAllFields);
//		
//		for (String field : sortMap.keySet()) {
//			String description = sortMap.get(field);
//		}
		
		return sortMap;
	}
	
	public Map<String, String> getSortOrder() {
		return transactionBillDataModel.getSortOrder();
    }

	public String redisplayTransactionList() {
		return "transactions_process";
	}
	
	public TaxCodeDetail getExistedTaxCodeDetail(){
		return existedTaxCodeDetail;
	}
	
	private void initDisplayFields() {
		currentJurisdiction = new Jurisdiction();
		PurchaseTransaction trDetail = null;
		Long id = (trDetail == null)? null:trDetail.getShiptoJurisdictionId();
		if (id != null) {
			currentJurisdiction = jurisdictionService.findById(id);
		}

		//Set default country
		newJurisdictionHandler.reset();
		newJurisdictionHandler.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		newJurisdictionHandler.setState("");
		
		newJurisdictionHandlerModify.reset();
		
		//Set default country
		newJurisdictionHandlerModify.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		newJurisdictionHandlerModify.setState("");
		
		newTaxCodeHandler.reset();
		newTaxCodeHandlerUpdate.reset();
		//Set default country
		newTaxCodeHandlerUpdate.reset();
		filterTaxCodeHandler.rebuildMenus();
		filterTaxCodeHandler1.rebuildMenus();
		filterTaxCodeHandler2.rebuildMenus();

		existedTaxCodeDetail = new TaxCodeDetail();

		applyToAll = false;
		comments = "";
	}
	
	public Boolean getApplyToAll() {
		return applyToAll;
	}

	public void setApplyToAll(Boolean applyToAll) {
		this.applyToAll = applyToAll;
	}

	public int getCurrentTransactionIndex() {
		return currentTransactionIndex;
	}

	public BillTransaction getCurrentTransaction() {
		return selectedTransaction;
	}
	
	public Jurisdiction getCurrentJurisdiction() {
		return currentJurisdiction;
	}

	public SearchDriverHandler getDriverHandler() {
		return driverHandler;
	}

	public SearchJurisdictionHandler getNewJurisdictionHandler() {
		return newJurisdictionHandler;
	}
	
	public SearchJurisdictionHandler getNewJurisdictionHandlerModify() {
		return newJurisdictionHandlerModify;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerShipto() {
		return jurisdictionHandlerShipto;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerShipfrom() {
		return jurisdictionHandlerShipfrom;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerOrdrorgn() {
		return jurisdictionHandlerOrdrorgn;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerOrdracpt() {
		return jurisdictionHandlerOrdracpt;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerFirstuse() {
		return jurisdictionHandlerFirstuse;
	}
	
	public SearchJurisdictionHandler getJurisdictionHandlerBillto() {
		return jurisdictionHandlerBillto;
	}

	public TaxCodeCodeHandler getNewTaxCodeHandler() {
		return newTaxCodeHandler;
	}
	
	public TaxCodeCodeHandler getNewTaxCodeHandlerUpdate() {
		return newTaxCodeHandlerUpdate;
	}
	
	public SearchBatchIdHandler getBatchIdHandler() {
		return batchIdHandler;
	}

	public void searchDriver(ActionEvent e) {
		String driverCode = (String) e.getComponent().getAttributes().get("DRIVER_CODE");
		driverHandlerBean.initSearch(
				(driverCode.equals(TaxMatrix.DRIVER_CODE)? filterSelectionTaxMatrix:filterSelectionLocationMatrix), 
				(HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		driverHandlerBean.setCallBackScreen(null);
	}
	
/*	public boolean getMoreTransactions() {
		return (currentTransactionList == null)? 
				false:(currentTransactionIndex < currentTransactionList.size());
	}

	public boolean getProcessedAllTransactions() {
		return (currentTransactionList == null) ? true : 
			(currentTransactionIndex >= currentTransactionList.size());
	}
	
	public boolean getFinishedAllTransactions() {
		return (currentTransactionList == null || currentTransactionIndex==0);
	}	
	*/
	
/*	public void nextTransactionActionListener(ActionEvent e) {
		nextTransactionAction();
	}
*/
/*	// using this for non-popup pages Cancel button
	public String nextTransactionActionForPage() {
		boolean more = getMoreTransactions();
		nextTransactionAction();
		if (more) {
			return "trans_update";
		} else {
			return "trans_main_redirect";
		}
	}*/
	
	public String cancelAll(){
		logger.info("cancelAll() called -  changesMade: "+ changesMade);
		
		// 4181
		if (changesMade){
			transactionBillDataModel.refreshData(false);
			resetSelection();
			searchCallback("search");
		}
		totalSelected = 1;
		return "trans_main_redirect";	
	}
	
/*	public String nextTransactionAction() {
		if (getMoreTransactions()) {
			currentTransactionIndex += 1;
			initDisplayFields();
			return null;
		} else {
			currentTransactionIndex += 1;
		}
		
		if (changesMade) {
			logger.info("Data Changed ");

			transactionBillDataModel.refreshData(false);
			resetSelection();
			searchCallback("search");

			totalSelected = 1;
			return "trans_main_redirect";
		}
		totalSelected = 1;
		
		return null;
	}*/
	
	public void taxrateRowChanged(ActionEvent e){
		HtmlDataTable table = (HtmlDataTable) e.getComponent().getParent();
		selectedTaxRate = (JurisdictionTaxrate) table.getRowData();
		selectedTaxRateIndex = table.getRowIndex();
	}
	
	public void filterSaveAction(ActionEvent e) {
    }
	
	
	public String displayViewAction() {
		BillTransaction saleTransactionForView = new BillTransaction();
		this.viewReturnPage = null;
		
		return "transactions_view";
	}
	
	public Long getDisplayWarning() {
		return (displayWarning)? 1L:0L;
	}
	
	public Long getDisplayIncomplete() {
		return (displayIncomplete)? 1L:0L;
	}

	public void statusChangeListener(ActionEvent e) {
		if (getTaxMatrixIdDisabled()) {
			taxMatrixId = null;
		}
		if (getLocationMatrixIdDisabled()) {
			locationMatrixId = null;
		}
		if (getTaxCodeDisabled()) {
			filterTaxCodeHandler.reset();
			filterTaxCodeHandler1.reset();
			filterTaxCodeHandler2.reset();
		}
	}

	public String listTransactions() {         
		logger.info(" --------------------- enter getTransactionsList() --------------------------------- ");
		

		this.indicateAllSelected = false; //reset the all selected check, since they all won't be
		
/*        TransactionDetail trDetail = new TransactionDetail();
		
		String state = selectedState;
		if ((state != null) && !state.equals("")) {
			trDetail.setTransactionStateCode(state);
		}
		
		String country = selectedCountry;
		if ((country != null) && !country.equals("")) {
			trDetail.setTransactionCountryCode(country);
		}
		
		// Now, map the drivers to transaction detail properties
		matrixCommonBean.setTransactionProperties(filterSelectionTaxMatrix, trDetail);
		matrixCommonBean.setTransactionProperties(filterSelectionLocationMatrix, trDetail);	*/
    
		fillSearchFilter();
		
		transactionBillDataModel.setFilterCriteria(saleTransactionFilter);
		
		resetSelection();
		setDispSaveAs((transactionBillDataModel.getRowCount() > 0));
		
		//Reset if number of pages changes.
		resetDetailTable();
		
		return null;
    }	
	
	public void selectedTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedTransaction = (BillTransaction) table.getRowData();
	    selectedRowIndex = table.getRowIndex();
	    currentUser = userService.findById(matrixCommonBean.getLoginBean().getUserDTO().getUserCode().toUpperCase());
	}
	
	public void selectedSplitTransactionChanged (ActionEvent e) throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		selectedSplitTransaction = (PurchaseTransaction) table.getRowData();
	    selectedSplitRowIndex = table.getRowIndex();
	    
	    selectedSplitTransactionTemp = selectedSplitTransaction;
	}
	
	private void resetSelection() {
//		currentTransactionList = null;
		currentTransactionIndex = 0;
		selectedTransaction = null;
	//	selectedTransactionTemp = null;
		selectedRowIndex = -1;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public boolean isSuspendTax() {
		return suspendTax;
	}
	
	public void setSuspendTax(boolean suspendTax) {
		this.suspendTax = suspendTax;
	}
	
	public boolean isSuspendLocation() {
		return suspendLocation;
	}
	
	public void setSuspendLocation(boolean suspendLocation) {
		this.suspendLocation = suspendLocation;
	}

	public TransactionBillDataModel getTransactionBillDataModel() {
		return transactionBillDataModel;
	}

	public void setTransactionBillDataModel(TransactionBillDataModel transactionBillDataModel) {
		this.transactionBillDataModel = transactionBillDataModel;
		
		String rowStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThreshold() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThreshold();
		int rowpage = 0;
		if(rowStr==null || rowStr.length()==0){
			rowpage = (displayAllFields)? 25:100; 
		}
		else{
			rowpage = Integer.parseInt(rowStr);
		}
		
		String muitipleStr = (displayAllFields)? matrixCommonBean.getMatrixDTO().getSysFullThresholdPages() : 
			matrixCommonBean.getMatrixDTO().getSysLimitedThresholdPages();
		int muitiplepage = 0;
		if(muitipleStr==null || muitipleStr.length()==0){
			muitiplepage = (displayAllFields)? 4:4; 
		}
		else{
			muitiplepage = Integer.parseInt(muitipleStr);
		}
		
		transactionBillDataModel.setPageSize(rowpage);
		transactionBillDataModel.setDbFetchMultiple(muitiplepage);
	}
    
	 public void reprocess() {
		 /*
		 //PP-394
		 reprocessTransactionList=transactionSaleDataModel.getItems(getSelectedIds());
		 if (this.reprocessTransactionList != null && this.reprocessTransactionList.size() > 0) {
			 Set<Long> selectedIds = null;
			 if(this.transactionSaleDataModel != null) {
				 selectedIds = this.transactionSaleDataModel
						 .getSelectionMap().keySet();
			 }

			 //5258
			 boolean doPcoProcessing = false;
			 
			 Option pco  = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN") );
			 if (pco != null && pco.getValue().equalsIgnoreCase("1")) 
				 doPcoProcessing = true;
			  

			 List<SaleTransaction> list = new ArrayList<SaleTransaction>();
			 if (selectedIds != null && selectedIds.size() > 0) {
				 for (SaleTransaction t : this.reprocessTransactionList) {
					 if (selectedIds.contains(t.getId())) {
						 list.add(t);
					 }
				 }
			 } else if(this.selectedTransaction != null) {
				 list.add(this.selectedTransaction);
			 }

			 if (list != null && list.size() > 0) {
				 List<SaleTransactionDocument> docList = buildDocument(list);
				 if (docList != null && docList.size() > 0) {
					 for (SaleTransactionDocument doc : docList) {
						 if (doc != null) {
							 try {

								 String returnMessage = "";
								 if (!doPcoProcessing) {
									 returnMessage = saleTransactionProcess.reprocess(doc, false, true, Auditable.currentUserCode());
									 
									//5258
								 } else {									 
									 for(SaleTransaction saleTrans: doc.getSaleTransaction()) {
										 if (saleTrans.getSaletransId()!=null && saleTrans.getSaletransId() != 0) {

											 PurchaseTransaction purchaseTrans = transactionDetailService.findById(saleTrans.getSaletransId());
											 if (purchaseTrans != null) {
												 purchaseTrans.setIsReprocessing(true);
												 transactionDetailService.update(purchaseTrans);
												 saleTrans.setResult(STATUS_REPROCESSED);
											 } else {												 
												 saleTrans.setResult(STATUS_PTRANS_NOTFOUND);
											 }
										 }
									 }

								 }
								 								 
								 if(returnMessage!=null && returnMessage.length()>0){
									 if(returnMessage.endsWith("\n")){
										 returnMessage = returnMessage.substring(0, returnMessage.length()-1);
									 }
								 }

								 List<SaleTransaction> saleList = doc.getSaleTransaction();							
								 for (SaleTransaction saleTran : saleList){
									 if(saleTran.getTaxAmt()==null || (saleTran.getTaxAmt().doubleValue()==0.0d)){
										 saleTran.setCombinedRate(0.0d);
									 }			
								 }	
							 } catch (ProcessAbortedException e) {
								 e.printStackTrace();
							 }
						 }
					 }
					 if(this.selectedTransaction != null) {
						 this.selectedTransaction = null;
						 this.selectedRowIndex = -1;
					 }
				 }
			 }												
	 		}	
	 		*/
		}

	 	/*
		private List<SaleTransactionDocument> buildDocument(
				List<SaleTransaction> reprocessTransactionList) {
			List<SaleTransactionDocument> docList = new ArrayList<SaleTransactionDocument>();
			if (reprocessTransactionList != null) {
				Map<String, List<SaleTransaction>> sortedMap = new TreeMap<String, List<SaleTransaction>>();
				for (SaleTransaction t : reprocessTransactionList) {
					if (t != null) {
						String key = StringUtils.hasText(t.getInvoiceNbr()) ? t.getInvoiceNbr() : "*NULL";
						if (sortedMap.containsKey(key)) {
							sortedMap.get(key).add(t);
						} else {
							List<SaleTransaction> list = new ArrayList<SaleTransaction>();
							list.add(t);
							sortedMap.put(key, list);
						}
					}
				}

				for (Map.Entry<String, List<SaleTransaction>> e : sortedMap
						.entrySet()) {
					if ("*NULL".equals(e.getKey())) {
						for (SaleTransaction t : e.getValue()) {
							SaleTransactionDocument doc = new SaleTransactionDocument();
							List<SaleTransaction> l = new ArrayList<SaleTransaction>();
							l.add(t);
							doc.setSaleTransaction(l);
							docList.add(doc);
						}
					} else {
						SaleTransactionDocument doc = new SaleTransactionDocument();
						doc.setSaleTransaction(e.getValue());
						docList.add(doc);
					}
				}
			}

			return docList;
		}
		*/
	
	 public void sortAction(ActionEvent e) {
			String id = e.getComponent().getId();
			if(id.startsWith("s_trans_")){
				//PP-357
				transactionBillDataModel.toggleSortOrder(e.getComponent().getId().replace("s_trans_", ""));
			}
			else{		
				transactionBillDataModel.toggleSortOrder(e.getComponent().getId().replace("s_", ""));
			}
	}
	
	public String findTaxMatrixIdAction() {
		taxMatrixBean.findId(this, "taxMatrixId", "trans_main");
		return "tax_matrix_main";
	}
	
	public String findLocationMatrixIdAction() {
		locationMatrixBean.findId(this, "locationMatrixId", "trans_main");
		return "location_matrix_main";
	}
	
	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("taxMatrixId")) {
			taxMatrixId = id;
		} else if (context.equalsIgnoreCase("locationMatrixId")) {
			locationMatrixId = id;
		} else if(context.equalsIgnoreCase("processBatchNo")) {
			saleTransactionFilter.setProcessBatchNo(id);
		}
	}
	
	public void searchLocationMatrixAction() {
		locationMatrixBean.search(this, "search");
	}
	
	public void searchTaxMatrixAction() {
		taxMatrixBean.search(this, "search");
	}
	
	public void searchTaxCodeAction() {
		taxCodeBackingBean.search(this, "search");
	}
	
	public void searchCallback(String context) {
		if (context.equalsIgnoreCase("search")) {
			listTransactions();
		}
	}
	
	public void sortCallback(String context) {
		if (context.equalsIgnoreCase("sort")) {
			// Refresh data, but we can keep the count
			transactionBillDataModel.refreshData(true);
		}
    }
	
	public void glExtractIdSelectedListener(ActionEvent e) {
		saleTransactionFilter.setProcessBatchNo(batchIdHandler.getSelectedItemKey());
	}

	public void minimumTaxDriversActionListener(ActionEvent e) {
		matrixCommonBean.prepareTaxDriverPanel(filterSelectionPanel);
	}
	
	public void defaultSaleProcessListener(ActionEvent e){
		batchIdHandler.setDefaultSearch("IPS");
		batchIdHandler.reset();
	}
	
	public void defaultGLExtractListener(ActionEvent e){
		batchIdHandler.setDefaultSearch("GE");
		batchIdHandler.reset();
	}
	
	public void popupSearchAction(ActionEvent e) {
		// pass event to handler
		jurisdictionHandler.reset();
		jurisdictionHandler.setCallbackExt(this);
		jurisdictionHandler.popupSearchAction(e);
	}
	
	public void popupSearchActionShipto(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerShipto.reset();
		jurisdictionHandlerShipto.setCallbackExt(this);
		jurisdictionHandlerShipto.popupSearchAction(e);
	}
	
	public void popupSearchActionShipfrom(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerShipfrom.reset();
		jurisdictionHandlerShipfrom.setCallbackExt(this);
		jurisdictionHandlerShipfrom.popupSearchAction(e);
	}
	
	public void popupSearchActionOrdrorgn(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerOrdrorgn.reset();
		jurisdictionHandlerOrdrorgn.setCallbackExt(this);
		jurisdictionHandlerOrdrorgn.popupSearchAction(e);
	}
	
	public void popupSearchActionOrdracpt(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerOrdracpt.reset();
		jurisdictionHandlerOrdracpt.setCallbackExt(this);
		jurisdictionHandlerOrdracpt.popupSearchAction(e);
	}
	
	public void popupSearchActionFirstuse(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerFirstuse.reset();
		jurisdictionHandlerFirstuse.setCallbackExt(this);
		jurisdictionHandlerFirstuse.popupSearchAction(e);
	}
	
	public void popupSearchActionBillto(ActionEvent e) {
		// pass event to handler
		jurisdictionHandlerBillto.reset();
		jurisdictionHandlerBillto.setCallbackExt(this);
		jurisdictionHandlerBillto.popupSearchAction(e);
	}
	
	public void searchJurisdictionCallback() {
		Long id = jurisdictionHandler.getSearchJurisdictionResultId();
		jurisdictionId = id;
	}
	
	public Long searchJurisdictionIdCallback(SearchJurisdictionHandler searchJurisdictionHandler) {	
		if(searchJurisdictionHandler == filterHandler2){
			return jurisdictionId;
		}
		else if(searchJurisdictionHandler == filterHandlerOrdrorgn){
			return jurisdictionIdOrdrorgn;
		}
		else if(searchJurisdictionHandler == filterHandlerOrdracpt){
			return jurisdictionIdOrdracpt;
		}
		else if(searchJurisdictionHandler == filterHandlerFirstuse){
			return jurisdictionIdFirstuse;
		}
		else if(searchJurisdictionHandler == filterHandlerBillto){
			return jurisdictionIdBillto;
		}		
		else if(searchJurisdictionHandler == filterHandlerShipto){			
			return jurisdictionIdShipto;
		}
		else if(searchJurisdictionHandler == filterHandlerTtlxfr){		
			return jurisdictionIdTtlxfr;
		}
		else if(searchJurisdictionHandler == filterHandlerShipfrom){		
			return jurisdictionIdShipfrom;
		}
		
		return null;
	}
	
	public void searchJurisdictionCallbackExt(SearchJurisdictionHandler searchJurisdictionHandler) {

		if(searchJurisdictionHandler == jurisdictionHandlerShipto){
			jurisdictionIdShipto = jurisdictionHandlerShipto.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandlerShipfrom){
			jurisdictionIdShipfrom = jurisdictionHandlerShipfrom.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandlerOrdrorgn){
			jurisdictionIdOrdrorgn = jurisdictionHandlerOrdrorgn.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandlerOrdracpt){
			jurisdictionIdOrdracpt = jurisdictionHandlerOrdracpt.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandlerFirstuse){
			jurisdictionIdFirstuse = jurisdictionHandlerFirstuse.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandlerBillto){
			jurisdictionIdBillto = jurisdictionHandlerBillto.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == jurisdictionHandler){
			jurisdictionId = jurisdictionHandler.getSearchJurisdictionResultId();
		}
	}
	
	
	public void searchJurisdictionCallbackLocation(SearchJurisdictionHandler searchJurisdictionHandler, String componentName){
		if(searchJurisdictionHandler == filterHandler){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandlerShipfrom.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandlerShipfrom.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandlerShipfrom.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandlerShipfrom.setZip(searchJurisdictionHandler.getZip());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandlerShipfrom.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){
				
				filterHandlerShipfrom.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerShipfrom.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandlerShipfrom.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandlerShipfrom.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandlerShipfrom.setCity(searchJurisdictionHandler.getCity());
				filterHandlerShipfrom.setCounty(searchJurisdictionHandler.getCounty());
				filterHandlerShipfrom.setZip(searchJurisdictionHandler.getZip());
				filterHandlerShipfrom.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandlerShipfrom.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerShipfrom.setState(searchJurisdictionHandler.getState());
			}	
		}
		else if(searchJurisdictionHandler == filterHandler1){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandlerShipto.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandlerShipto.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandlerShipto.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandlerShipto.setZip(searchJurisdictionHandler.getZip());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandlerShipto.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){	
				filterHandlerShipto.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerShipto.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandlerShipto.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandlerShipto.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandlerShipto.setCity(searchJurisdictionHandler.getCity());
				filterHandlerShipto.setCounty(searchJurisdictionHandler.getCounty());
				filterHandlerShipto.setZip(searchJurisdictionHandler.getZip());
				filterHandlerShipto.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandlerShipto.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerShipto.setState(searchJurisdictionHandler.getState());
			}
		}
		else if(searchJurisdictionHandler == filterHandler2){
			//Update Jurisdiction Id
			jurisdictionId = searchJurisdictionHandler.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == filterHandlerOrdrorgn){
			//Update Jurisdiction Id
			jurisdictionIdOrdrorgn = searchJurisdictionHandler.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == filterHandlerOrdracpt){
			//Update Jurisdiction Id
			jurisdictionIdOrdracpt = searchJurisdictionHandler.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == filterHandlerFirstuse){
			//Update Jurisdiction Id
			jurisdictionIdFirstuse = searchJurisdictionHandler.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == filterHandlerBillto){
			//Update Jurisdiction Id
			jurisdictionIdBillto = searchJurisdictionHandler.getSearchJurisdictionResultId();
		}
		else if(searchJurisdictionHandler == filterHandler5){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandlerTtlxfr.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandlerTtlxfr.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandlerTtlxfr.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandlerTtlxfr.setZip(searchJurisdictionHandler.getZip());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandlerTtlxfr.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){	
				filterHandlerTtlxfr.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerTtlxfr.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandlerTtlxfr.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandlerTtlxfr.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandlerTtlxfr.setCity(searchJurisdictionHandler.getCity());
				filterHandlerTtlxfr.setCounty(searchJurisdictionHandler.getCounty());
				filterHandlerTtlxfr.setZip(searchJurisdictionHandler.getZip());
				filterHandlerTtlxfr.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandlerTtlxfr.setCountry(searchJurisdictionHandler.getCountry());
				filterHandlerTtlxfr.setState(searchJurisdictionHandler.getState());
			}
		}
		else if(searchJurisdictionHandler == filterHandlerShipto){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandler1.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandler1.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandler1.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandler1.setZip(searchJurisdictionHandler.getZip());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandler1.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){	
				filterHandler1.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler1.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandler1.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandler1.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandler1.setCity(searchJurisdictionHandler.getCity());
				filterHandler1.setCounty(searchJurisdictionHandler.getCounty());
				filterHandler1.setZip(searchJurisdictionHandler.getZip());
				filterHandler1.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandler1.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler1.setState(searchJurisdictionHandler.getState());
				
				jurisdictionIdShipto = searchJurisdictionHandler.getSearchJurisdictionResultId();
			}
		}
		else if(searchJurisdictionHandler == filterHandlerTtlxfr){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandler5.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandler5.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandler5.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandler5.setZip(searchJurisdictionHandler.getZip());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandler5.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){	
				filterHandler5.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler5.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandler5.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandler5.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandler5.setCity(searchJurisdictionHandler.getCity());
				filterHandler5.setCounty(searchJurisdictionHandler.getCounty());
				filterHandler5.setZip(searchJurisdictionHandler.getZip());
				filterHandler5.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandler5.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler5.setState(searchJurisdictionHandler.getState());
			
				jurisdictionIdTtlxfr = searchJurisdictionHandler.getSearchJurisdictionResultId();
			}
		}
		else if(searchJurisdictionHandler == filterHandlerShipfrom){
			if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.GEOCODE)){
				filterHandler.setGeocode(searchJurisdictionHandler.getGeocode());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.CITY)){
				filterHandler.setCity(searchJurisdictionHandler.getCity());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTY)){
				filterHandler.setCounty(searchJurisdictionHandler.getCounty());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ZIPCODE)){
				filterHandler.setZip(searchJurisdictionHandler.getZip());
			}		
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.PLUS4)){
				filterHandler.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.COUNTRY)){	
				filterHandler.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler.setState("");
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.STATE)){
				filterHandler.setState(searchJurisdictionHandler.getState());
			}
			else if(componentName.equalsIgnoreCase(SearchJurisdictionHandler.ALL)){
				filterHandler.setGeocode(searchJurisdictionHandler.getGeocode());
				filterHandler.setCity(searchJurisdictionHandler.getCity());
				filterHandler.setCounty(searchJurisdictionHandler.getCounty());
				filterHandler.setZip(searchJurisdictionHandler.getZip());
				filterHandler.setZipPlus4(searchJurisdictionHandler.getZipPlus4());
				
				filterHandler.setCountry(searchJurisdictionHandler.getCountry());
				filterHandler.setState(searchJurisdictionHandler.getState());
				
				jurisdictionIdShipfrom = searchJurisdictionHandler.getSearchJurisdictionResultId();
			}
		}

	}
	
	public void searchTaxCodeCodeCallback(TaxCodeCodeHandler taxCodeCodeHandler) {
		if(taxCodeCodeHandler == filterTaxCodeHandler){
			selectedTaxcodeCode = filterTaxCodeHandler.getTaxcodeCode();
			
			filterTaxCodeHandler1.setTaxcodeCode(selectedTaxcodeCode);
			filterTaxCodeHandler2.setTaxcodeCode(selectedTaxcodeCode);
		}
		else if(taxCodeCodeHandler == filterTaxCodeHandler1){
			selectedTaxcodeCode = filterTaxCodeHandler1.getTaxcodeCode();
	
			filterTaxCodeHandler.setTaxcodeCode(selectedTaxcodeCode);
			filterTaxCodeHandler2.setTaxcodeCode(selectedTaxcodeCode);
		}
		else if(taxCodeCodeHandler == filterTaxCodeHandler2){
			selectedTaxcodeCode = filterTaxCodeHandler2.getTaxcodeCode();
			
			filterTaxCodeHandler.setTaxcodeCode(selectedTaxcodeCode);
			filterTaxCodeHandler1.setTaxcodeCode(selectedTaxcodeCode);
		}
	}

	/// 0005972: Sales Transactions screen Batch Lookup displays the wrong screen.
	public String findProcessBatchIdAction() {

		String batchType = "IPS";
		
		Option isPCO = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));
		if (isPCO!=null && isPCO.getValue()!=null && "1".equals(isPCO.getValue())) {
			batchType = "PCO";
		}
		batchMaintenanceBean.findId(this, "processBatchNo", "transactions_sale", batchType);
		return "data_utility_batch";
	}
	
	
	
	public long getTotalSelected() {
		return totalSelected;
	}

	public void setTotalSelected(long totalSelected) {
		this.totalSelected = totalSelected;
	}

	public PurchaseTransaction getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(PurchaseTransaction transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public List<PurchaseTransaction> getTraList() {
		return traList;
	}

	public void setTraList(List<PurchaseTransaction> traList) {
		this.traList = traList;
	}

	public boolean isDispSaveAs() {
		return dispSaveAs;
	}

	public void setDispSaveAs(boolean dispSaveAs) {
		this.dispSaveAs = dispSaveAs;
	}

	public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}
	
	public ProcruleService getProcruleService() {
		return procruleService;
	}

	public void setProcruleService(ProcruleService procruleService) {
		this.procruleService = procruleService;
	}

	public Boolean getRenderOptionPanel() {
		return renderOptionPanel;
	}

	public void setRenderOptionPanel(Boolean renderOptionPanel) {
		this.renderOptionPanel = renderOptionPanel;
	}

	public boolean isSaveNow() {
		return saveNow;
	}

	public void setSaveNow(boolean saveNow) {
		this.saveNow = saveNow;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}
	
	public void searchCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry == null) {
			selectedCountry = "";
		}
		else {
			selectedCountry = newCountry;
		}
	
		if(stateMenuItems!=null) stateMenuItems.clear();

		stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		selectedState = "";
    }

	private List<SelectItem> countryMenuItems;
	public List<SelectItem> getCountryMenuItems() {
		if(countryMenuItems==null){
			selectedCountry = "";
		}
		countryMenuItems = getMatrixCommonBean().getCacheManager().createCountryItems();   
		return countryMenuItems;
	}

	private List<SelectItem> stateMenuItems;
	public List<SelectItem> getStateMenuItems() {
		if (stateMenuItems == null) {
		   stateMenuItems = getMatrixCommonBean().getCacheManager().createStateItems(selectedCountry);
		   selectedState = "";
		}
		return stateMenuItems;
	}
	
	public List<SelectItem> getStateMenuItemsModifyOriginal() { 
		return null;//ac getMatrixCommonBean().getCacheManager().createStateItems(getCurrentTransaction().getTransactionCountryCode());
	}
	
	public List<SelectItem> getStateJurisMenuItemsModifyOriginal() { 
		return getMatrixCommonBean().getCacheManager().createStateItems((getCurrentJurisdiction()==null) ? "" : getCurrentJurisdiction().getCountry());
	}

	public List<TransactionSplittingToolBean<?>> getCurrentSplittingToolBean() {
		return currentSplittingToolBean;
	}

	public void setCurrentSplittingToolBean(
			List<TransactionSplittingToolBean<?>> currentSplittingToolBean) {
		this.currentSplittingToolBean = currentSplittingToolBean;
	}

	public TransactionSplittingToolBean<PurchaseTransaction> getTransactionSplittingToolBean() {
		return transactionSplittingToolBean;
	}

	public void setTransactionSplittingToolBean(
			TransactionSplittingToolBean<PurchaseTransaction> transactionSplittingToolBean) {
		this.transactionSplittingToolBean = transactionSplittingToolBean;
	}

	public String getMultiTransCode() {
		return multiTransCode;
	}

	public void setMultiTransCode(String multiTransCode) {
		this.multiTransCode = multiTransCode;
	}

	public Long getSubTransId() {
		return subTransId;
	}

	public void setSubTransId(Long subTransId) {
		this.subTransId = subTransId;
	}

	public HtmlDataTable getTrDetailSplitTable() {
		if (trDetailSplitTable == null) {
			trDetailSplitTable = new HtmlDataTable();
			trDetailSplitTable.setId("aMDetailList");
			trDetailSplitTable.setVar("trdetail");
			matrixCommonBean.createTransactionDetailTable(trDetailSplitTable,
					"transactionDetailSaleBean", "transactionDetailSaleBean.splitGroupDataModel", 
					matrixCommonBean.getTransactionPropertyMap(), false, displayAllViewSplitFields, false, false, true, null);
		}
		return trDetailSplitTable;
	}

	public void setTrDetailSplitTable(HtmlDataTable trDetailSplitTable) {
		this.trDetailSplitTable = trDetailSplitTable;
	}

	public int getSelectedSplitRowIndex() {
		return selectedSplitRowIndex;
	}

	public void setSelectedSplitRowIndex(int selectedSplitRowIndex) {
		this.selectedSplitRowIndex = selectedSplitRowIndex;
	}

	public boolean isDisplayAllViewSplitFields() {
		return displayAllViewSplitFields;
	}

	public void setDisplayAllViewSplitFields(boolean displayAllViewSplitFields) {
		this.displayAllViewSplitFields = displayAllViewSplitFields;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
	
	public String getTransactionTypeCodeTrans() {
		return transactionTypeCodeTrans;
	}

	public void setTransactionTypeCodeTrans(String transactionTypeCodeTrans) {
		this.transactionTypeCodeTrans = transactionTypeCodeTrans;
	}
	
	public String getRatetypeCodeTrans() {
		return ratetypeCodeTrans;
	}

	public void setRatetypeCodeTrans(String ratetypeCodeTrans) {
		this.ratetypeCodeTrans = ratetypeCodeTrans;
	}
	
	public String getMethodDeliveryCodeTrans() {
		return methodDeliveryCodeTrans;
	}

	public void setMethodDeliveryCodeTrans(String methodDeliveryCodeTrans) {
		this.methodDeliveryCodeTrans = methodDeliveryCodeTrans;
	}
	
	public String getInvoiceNbrInvoice() {
		return invoiceNbrInvoice;
	}

	public void setInvoiceNbrInvoice(String invoiceNbrInvoice) {
		this.invoiceNbrInvoice = invoiceNbrInvoice;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}
	
	public String viewOkAction() {
		return this.viewReturnPage == null ? "transactions_sale" : this.viewReturnPage;
	}

	public String getViewReturnPage() {
		return viewReturnPage;
	}

	public void setViewReturnPage(String viewReturnPage) {
		this.viewReturnPage = viewReturnPage;
	}

	public BillTransaction getViewsaleId() {
		return viewsaleId;
	}

	public void setViewsaleId(BillTransaction viewsaleId) {
		this.viewsaleId = viewsaleId;
	}

	public EntityLocnSet getEntityLocSet() {
		return entityLocSet;
	}

	public void setEntityLocSet(EntityLocnSet entityLocSet) {
		this.entityLocSet = entityLocSet;
	}

	public String getEntityCode() {
		return entityCode;
	}
	public void setEntityCode(String entityCode) {
	   	this.entityCode = entityCode;
    }
	public CommonCallBack getCommonCallBack() {
		return commonCallBack;
	}
	public void setCommonCallBack(CommonCallBack commonCallBack) {
		this.commonCallBack = commonCallBack;
	}

	public HashMap<String, HashMap<String, String>> getCurrentNexusInfo() {
		return currentNexusInfo;
	}

	public void setCurrentNexusInfo(HashMap<String, HashMap<String, String>> currentNexusInfo) {
		this.currentNexusInfo = currentNexusInfo;
	}

	public HashMap<String, String> getCurrentTaxRateInfo() {
		return currentTaxRateInfo;
	}

	public void setCurrentTaxRateInfo(HashMap<String, String> currentTaxRateInfo) {
		this.currentTaxRateInfo = currentTaxRateInfo;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
}