package com.ncsts.view.bean;

import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.component.UIComponent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajax4jsf.component.UIDataAdaptor;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;

import com.ncsts.common.LogMemory;
import com.ncsts.dao.TransactionBlobDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.TransactionBlob;
import com.ncsts.domain.TransactionDetail;
import com.ncsts.jsf.model.TransactionBlobDataModel;
import com.ncsts.services.UserService;
import com.ncsts.view.util.BatchStatistics;
import com.ncsts.view.util.JsfHelper;

public class TransactionBlobBean {

	private Logger logger = Logger.getLogger(TransactionBlobBean.class);

	private List<TransactionBlob> transactionBlobList;

	private UserService userService;
	private TransactionBlobDAO transactionBlobDAO;
	private TransactionBlob exampleTransactionBlob;
	private TransactionBlob selectedBlob;
	private TransactionBlob workingBlob;
	private loginBean loginBean;
	private List<TransactionBlob> selectedBlobs;
	private int selectedRowIndex = -1;
	private FilePreferenceBackingBean filePreferenceBean;
	private TransactionBlobDataModel transactionBlobDataModel;
	
	private EditAction currentAction = EditAction.VIEW;
	private EditAction processAction = EditAction.START;
	
	private String sqlScripts = "";
	private String redirectPage = "";
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Create";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		
		return "";
	}
	
	public void setAction(EditAction action){
		currentAction = action;
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public String getSqlScripts() {
		return sqlScripts;
	}

	public void setSqlScripts(String sqlScripts) {
		this.sqlScripts = sqlScripts;
	}
	
	public String getRedirectPage() {
		return redirectPage;
	}

	public void setRedirectPage(String redirectPage) {
		this.redirectPage = redirectPage;
	}

	public List<TransactionBlob> getSelectedBlobs() {
		return this.selectedBlobs;
	}
	
	public String displayDeleteBlobAction() {
		currentAction = EditAction.DELETE;
	
		if(selectedRowIndex<0 || selectedBlob==null){			
			return "";
		}
	
		workingBlob = new TransactionBlob();
		BeanUtils.copyProperties(selectedBlob, workingBlob);
		workingBlob.setFileId(selectedBlob.getFileId());

		this.setRedirectPage("transactionblob_main");
	    return "transactionblob_update";
	}	
	
	boolean isProcessError = false;
	
	public boolean getIsProcessError(){
		return isProcessError;
	}
	
	public boolean getIsInProcessAction() {
		if(processAction.equals(EditAction.IN_PROCESS)){
			return true;
		}
    	
    	return false;
	}
	
	public boolean getIsStartAction() {
		if(processAction.equals(EditAction.START)){
			return true;
		}
    	
    	return false;
	}
	
	public void setProcessAction(EditAction aAction) {
		processAction = aAction;
	}
	
	public String goToDownloadAction(){
		refreshAction();
    	return "transactionblob_main";
	}
	
	public void processBlobAction(ActionEvent e) throws AbortProcessingException { 
		isProcessError = false;
		processAction = EditAction.IN_PROCESS;
	}

	public String addBlobAction() {	
		if(workingBlob.getFileName()==null || workingBlob.getFileName().trim().length()==0){
			isProcessError = true;
			processAction = EditAction.START;
			return "transactionblob_add";
		}
		
		workingBlob.setFileName(workingBlob.getFileName().trim());
		workingBlob.setUserName(loginBean.getUsername());
    	transactionBlobDAO.executeTransactions(workingBlob, getSqlScripts());
    	
    	try {
			Runtime r = Runtime.getRuntime();
			System.gc();
			Thread.sleep(5000);
			
		} catch (Exception ex) {
		}
    	
    	setSqlScripts("");
    	isProcessError = false;
    	processAction = EditAction.FINISHED;
    	return "transactionblob_add";
    }
	
	public String deleteBlobAction() {	
		if (currentAction.equals(EditAction.DELETE) && workingBlob!=null){
			transactionBlobDAO.remove(workingBlob.getFileId());
			workingBlob=null;
		}
    		
    	refreshAction();
    	return "transactionblob_main";
    }
	
	public String closeBlobAction(){ 
		refreshAction();

		processAction = EditAction.START;
		isProcessError = false;
		setSqlScripts("");
		return getRedirectPage();
	}
	 
	public String cancelBlobAction(){
		processAction = EditAction.START;
		isProcessError = false;
		setSqlScripts("");
		return getRedirectPage();
	}
	 
	 void copy(File src, File dst) throws IOException {
		    InputStream in = new FileInputStream(src);
		    OutputStream out = new FileOutputStream(dst);

		    // Transfer bytes from in to out
		    byte[] buf = new byte[1024];
		    int len;
		    while ((len = in.read(buf)) > 0) {
		        out.write(buf, 0, len);
		    }
		    in.close();
		    out.close();
		}

	 
	public String saveAsBlobAction() {	
		if(selectedRowIndex<0 || selectedBlob==null){
			return"";	
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String path = request.getSession().getServletContext().getRealPath("/");
		String pathSeparator = File.separator; // will return either "\" or "/", depends on OS
		
		String fileName = System.currentTimeMillis() + ".zip";
		String downloadPath = path + "download" + pathSeparator;
    	String fileNamePath = downloadPath + fileName;
		
    	//Create directory if not existing
    	// Create a directory; all non-existent ancestor directories are
    	// automatically created
    	boolean success = (new File(downloadPath)).mkdirs();
    	if (!success) {
    	    // Directory creation failed
    	}
    	
		String tempFileName = fileNamePath;
		transactionBlobDAO.saveTransactionBlob(selectedBlob.getFileId(), tempFileName);	
		  
		try {
			long begintime = System.currentTimeMillis();

			File file = new File(tempFileName);
			int contentLength = (int) file.length(); 
			FileInputStream istr = new FileInputStream(tempFileName);
			BufferedInputStream is = new BufferedInputStream(istr);
			
			response.setContentType("application/zip");
			response.setContentLength(contentLength);
			response.setHeader("Content-disposition", "attachment; filename=\"" + "download.zip" + "\"");

			ServletOutputStream out = response.getOutputStream(); 

			byte[] buff = new byte[4 * 1024]; // 4K buffer
			int bytesRead;
			while ((bytesRead = is.read(buff)) != -1) {
				out.write(buff, 0, bytesRead);
			}
			is.close();	
			out.flush();
			out.close();
			context.responseComplete();
				
			System.out.println("\t *** Executing saveAction, took " + Long.toString((System.currentTimeMillis() - begintime)/1000) + " seconds. \n");   
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Delete temp .zip file
		success = (new java.io.File(tempFileName)).delete();
		if (!success) {
		    // Deletion failed
		}
		
		LogMemory.executeGC();
	
	    return "";
	}
	
	public boolean getIsDeleteAction() {
		if(currentAction.equals(EditAction.DELETE)){
			return true;
		}
    	
    	return false;
	}
	
	public boolean getIsFinishedAction() {
		if(processAction.equals(EditAction.FINISHED)){
			return true;
		}
    	
    	return false;
	}
	
	public boolean getIsAddAction() {
		if(currentAction.equals(EditAction.ADD)){
			return true;
		}
    	
    	return false;
	}

    public boolean getDisableDeleteButton() {
    	if(selectedRowIndex<0){
    		return true;
    	}
    	
    	return false;
	}
    
    public boolean getDisableAddButton() {
    	if(getSqlScripts().length()==0){
    		return true;
    	}
    	
    	return false;
	}
    
    public boolean getDisableCancelButton() {
    	if(getSqlScripts().length()==0){
    		return true;
    	}
    	
    	return false;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		logger.info("TransactionBlobBean: enter selectedRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedBlob = (TransactionBlob) table.getRowData();
		this.selectedRowIndex = table.getRowIndex();
	}

	public String refreshAction() {
		init();
		selectedRowIndex = -1;
		selectedBlob = null;
		return null;
	}

	private void init() {  
	  transactionBlobDataModel.setDefaultSortOrder("fileId", TransactionBlobDataModel.SORT_DESCENDING);
	  transactionBlobDataModel.setPageSize(50);
	  
	  //Maintenance
	  exampleTransactionBlob = new TransactionBlob();
	  if(!loginBean.getRole().getRoleCode().equalsIgnoreCase("ADMIN")){
		exampleTransactionBlob.setUserName(loginBean.getUsername());
	  }
		
	  transactionBlobDataModel.setCriteria(exampleTransactionBlob);
  }
	
	public String selectAllAction() {
  	DetachedCriteria criteria = DetachedCriteria.forClass(TransactionBlob.class);
  	criteria.add(Expression.eq("batchTypeCode", "IP"));
  	criteria.add(Expression.eq("batchStatusCode", "I"));
  	criteria.add(Expression.or(Expression.eq("heldFlag", "0"), Expression.isNull("heldFlag")));

  	transactionBlobDataModel.setCriteria(criteria);
  	transactionBlobDataModel.refreshData(false);
  		selectedRowIndex = -1;
		return null;
	}
	

    private boolean getHasSelection() {
    	Iterator<TransactionBlob> iter = transactionBlobDataModel.iterator();
    	//while (iter.hasNext()) {
    	//	if (iter.next().getSelected()) {
    	//		return true;
    	//	}
    	//}
    	return false;
    }
  
	public TransactionBlobDataModel getTransactionBlobDataModel() {
	    if (exampleTransactionBlob == null) {
	      init();
	    }
			return this.transactionBlobDataModel;
	}
	
	public void sortAction(ActionEvent e) {
		transactionBlobDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public void setTransactionBlobDataModel(TransactionBlobDataModel transactionBlobDataModel) {
		this.transactionBlobDataModel = transactionBlobDataModel;
	}

	public int getSelectedRowIndex() {
		return this.selectedRowIndex;
	}

	/**
	 * @param selectedRowIndex the selectedRowIndex to set
	 */
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}


	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public TransactionBlobDAO getTransactionBlobDAO() {
		return this.transactionBlobDAO;
	}

	public void setTransactionBlobDAO(TransactionBlobDAO transactionBlobDAO) {
		this.transactionBlobDAO = transactionBlobDAO;
	}

	public TransactionBlob getSelectedBlob() {
		return this.selectedBlob;
	}
	
	public void setWorkingBlob(TransactionBlob transactionBlob) {
		this.workingBlob = transactionBlob;
	}
	
	public TransactionBlob getWorkingBlob() {
		return workingBlob;
	}
	
}