package com.ncsts.view.bean;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.AllocationMatrix;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionAllocationMatrixDetail;
import com.ncsts.domain.TaxAllocationMatrix;
import com.ncsts.domain.TaxAllocationMatrixDetail;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxHoliday;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.jsf.model.TaxAllocationMatrixDataModel;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.TaxAllocationMatrixService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.view.bean.ProcruleBean.WhenObject;
import com.ncsts.view.util.ArithmeticUtils;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.TogglePanelController;
import com.seconddecimal.billing.domain.SaleTransaction;

public class TaxAllocationMatrixViewBean extends BaseMatrixBackingBean<TaxAllocationMatrix, TaxAllocationMatrixService> implements BaseMatrixBackingBean.FindIdCallback {
	private Logger logger = LoggerFactory.getInstance().getLogger(TaxAllocationMatrixViewBean.class);
	
	@Autowired
	private TaxCodeDetailService taxCodeDetailService;
	
	@Autowired
	protected MatrixCommonBean matrixCommonBean;
	
	@Autowired
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	@Autowired
	private JurisdictionService jurisdictionService;
	
	private int selectedMasterRowIndex= -1;
	private TaxAllocationMatrix selectedMasterMatrixSource = null;
	private List<TaxAllocationMatrix> selectedMatrixDetails = null;
	
	private TaxAllocationMatrix selectedAllocDriverMatrix;
	private HtmlPanelGrid allocDriverPanel;
	
	private HtmlInputText allocationPercent;
	private HtmlInputText allocationComment;
	
	private boolean futureSplit;
	
	private Long instanceCreatedId;
	private Long jurisSplitInstanceId;
	
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	private SearchJurisdictionHandler jurisdictionHandler = new SearchJurisdictionHandler();
	
	private List<JurisdictionAllocationMatrixDetail> unsavedJurisdictionAllocationMatrixDetails = null;
	private boolean globalSplit = false;
	
	private String splitTaxCode = "";
	private String splitTaxCodeDescription = "";
	
	Map<Long, Long> instanceIdDetailIdMap = null;
	Map<Long, Boolean> jurisSplitInfoMap = null;
	
	private Long displayDeleteWarning = 0L;
	private Long displayGlobalJurisWarning = 0L;
	private Long displayIndividualJurisWarning = 0L;
	
	private Boolean allowDeleteTaxAllocationMatrix = false;
	
	private HtmlPanelGrid jurisdictionViewPanel;
	
	private String jurisOkAction = null;
	private boolean actionFromSplittingTool = false;

	private TogglePanelController togglePanelController = null;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TaxAllocationDriversPanel", true);
			togglePanelController.addTogglePanel("TaxAllocationMatrixPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public boolean isFutureSplit() {
		return futureSplit;
	}

	public void setFutureSplit(boolean futureSplit) {
		this.futureSplit = futureSplit;
	}

	public HtmlInputText getAllocationComment() {
		return allocationComment;
	}

	public void setAllocationComment(HtmlInputText allocationComment) {
		this.allocationComment = allocationComment;
	}

	public HtmlInputText getAllocationPercent() {
		return allocationPercent;
	}

	public void setAllocationPercent(HtmlInputText allocationPercent) {
		this.allocationPercent = allocationPercent;
	}

	public TaxAllocationMatrix getSelectedAllocDriverMatrix() {
		return selectedAllocDriverMatrix;
	}

	public void setSelectedAllocDriverMatrix(
			TaxAllocationMatrix selectedAllocDriverMatrix) {
		this.selectedAllocDriverMatrix = selectedAllocDriverMatrix;
	}

	public HtmlPanelGrid getAllocDriverPanel() {
		selectedAllocDriverMatrix = new TaxAllocationMatrix();
		selectedAllocDriverMatrix.initNullDriverFields();
		
		TaxAllocationMatrix matrix = new TaxAllocationMatrix();
		
		allocDriverPanel = MatrixCommonBean.createDriverPanel(
				matrix.getDriverCode(), matrixCommonBean.getCacheManager(), 
				matrixCommonBean.getUserEntityService().getEntityItemDTO(),
				"allocDriverPanel", 
				getBeanName(), "selectedAllocDriverMatrix", "matrixCommonBean", 
				false, true, false, true, false, true);
		
		matrixCommonBean.prepareTaxDriverPanel(allocDriverPanel);
		
		return allocDriverPanel;
	}

	public void setAllocDriverPanel(HtmlPanelGrid allocDriverPanel) {
		this.allocDriverPanel = allocDriverPanel;
	}

	public int getSelectedMasterRowIndex() {
		return selectedMasterRowIndex;
	}

	public void setSelectedMasterRowIndex(int selectedMasterRowIndex) {
		this.selectedMasterRowIndex = selectedMasterRowIndex;
		this.actionFromSplittingTool = false;
	}
	
	public List<TaxAllocationMatrix> getSelectedMatrixDetails() {	
		if(selectedMatrixDetails==null){
			selectedMatrixSource = null;
			setSelectedRowIndex(-1);
			selectedMatrixDetails = new ArrayList<TaxAllocationMatrix>();
		}
		return selectedMatrixDetails;
	}
	
	public List<TaxAllocationMatrixDetail> getSelectedMatrixTaxAllocDetails() {
		TaxAllocationMatrix matrix = (selectedMatrix != null)? selectedMatrix:selectedMatrixSource;
		List<TaxAllocationMatrixDetail> result = (matrix == null)?
				(new ArrayList<TaxAllocationMatrixDetail>()):matrix.getTaxAllocationMatrixDetails();
		
		if(result != null) {
			logger.debug("Detail items: " + result.size());
		}
		
		instanceIdDetailIdMap = new HashMap<Long, Long>();
		jurisSplitInfoMap = new HashMap<Long, Boolean>();
		
		Map<Long, Long> detailIdInstanceIdMap = new HashMap<Long, Long>();
		if(result != null) {
			for(TaxAllocationMatrixDetail d : result) {
				if(d.getTaxAllocationMatrixDetailId() != null) {
					detailIdInstanceIdMap.put(d.getTaxAllocationMatrixDetailId(), d.getInstanceCreatedId());
				}
				instanceIdDetailIdMap.put(d.getInstanceCreatedId(), d.getTaxAllocationMatrixDetailId());
				jurisSplitInfoMap.put(d.getInstanceCreatedId(), Boolean.FALSE);
			}
		}
		
		List<JurisdictionAllocationMatrixDetail> jurisDetails = (matrix == null)?
				(new ArrayList<JurisdictionAllocationMatrixDetail>()):matrix.getJurisdictionAllocationMatrixDetails();
		if(jurisDetails != null) {
			for(JurisdictionAllocationMatrixDetail d : jurisDetails) {
				if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() == 0L) {
					globalSplit = true;
				}
				
				if(d.getTaxAllocationMatrixDetailId() != null) {
					d.setTaxAllocationMatrixDetailInstanceId(detailIdInstanceIdMap.get(d.getTaxAllocationMatrixDetailId()));
				}
				
				if(d.getTaxAllocationMatrixDetailInstanceId() != null) {
					jurisSplitInfoMap.put(d.getTaxAllocationMatrixDetailInstanceId(), Boolean.TRUE);
				}
			}
		}
		
		return result;
	}
	
	public List<JurisdictionAllocationMatrixDetail> getSelectedMatrixJurisAllocDetails() {
		if(unsavedJurisdictionAllocationMatrixDetails != null) {
			return unsavedJurisdictionAllocationMatrixDetails;
		}
		
		unsavedJurisdictionAllocationMatrixDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
		TaxAllocationMatrix matrix = (selectedMatrix != null)? selectedMatrix:selectedMatrixSource;
		List<JurisdictionAllocationMatrixDetail> details = (matrix == null)?
				(new ArrayList<JurisdictionAllocationMatrixDetail>()):matrix.getJurisdictionAllocationMatrixDetails();
		
		if(details != null) {
			for(JurisdictionAllocationMatrixDetail d : details) {
				if(d.getTaxAllocationMatrixDetailInstanceId() == null) {
					if(jurisSplitInstanceId == 0L || d.getInstanceCreatedId().equals(jurisSplitInstanceId)) {
						JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
						BeanUtils.copyProperties(d, amd);
						amd.setTaxAllocationMatrixDetailInstanceId(jurisSplitInstanceId.longValue());
						unsavedJurisdictionAllocationMatrixDetails.add(amd);
						if(jurisSplitInstanceId == 0L) {
							globalSplit = true;
						}
					}
				}
				else if(d.getTaxAllocationMatrixDetailInstanceId().equals(jurisSplitInstanceId)) {
					JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
					BeanUtils.copyProperties(d, amd);
					unsavedJurisdictionAllocationMatrixDetails.add(amd);
				}
			}
		}
		
		return unsavedJurisdictionAllocationMatrixDetails;
	}
	
	public String updateMatrixList () {
		
		TaxAllocationMatrix matrix = newMatrix();
		BeanUtils.copyProperties(getFilterSelection(), matrix);
		
		// Fix the driver values
		matrix.adjustDriverFields();
		
		// NULLs get converted to 0, change back to null
		Long id = matrix.getId();
		if ((id != null) && (id <= 0L)) {
			matrix.setId(null);
		}
		
		updateMatrixFilter(matrix);

		((TaxAllocationMatrixDataModel) getMatrixDataModel()).setFilterCriteria(matrix);
		
	  	resetSelection();
	  	return "tax_allocation_matrix_main";
    }
	
	public String addMatrixDetail() {
		//Number num = (Number) allocationPercent.getValue();
	   // BigDecimal pct = (num == null)? ArithmeticUtils.ZERO : ArithmeticUtils.toBigDecimal(num.doubleValue());
	    
		//if (pct == null || pct.compareTo(ArithmeticUtils.ZERO) <= 0 || pct.compareTo(new BigDecimal("1")) > 0) {
		//	FacesMessage message = new FacesMessage("Allocation percent must be between 0 and 1.");
		//	message.setSeverity(FacesMessage.SEVERITY_ERROR);
		//	FacesContext.getCurrentInstance().addMessage(null, message);
		//	return null;
	    //}
		
		TaxAllocationMatrixDetail taxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
		//List<HtmlInputText> inputControls = this.matrixCommonBean.findInputControls(allocDriverPanel, new ArrayList<HtmlInputText>());
		//this.matrixCommonBean.setMatrixProperties(inputControls, taxAllocationMatrixDetail, selectedMatrix.getDriverCount());
		//taxAllocationMatrixDetail.setAllocationPercent(pct);
		//taxAllocationMatrixDetail.setComments((String) allocationComment.getValue());
		
		selectedMatrix.getTaxAllocationMatrixDetails().add(taxAllocationMatrixDetail);

		//allocationPercent.setValue(null);
		//allocationComment.setValue(null);
		
		return null;
	}
	
	private BigDecimal stripTrailingZeros(BigDecimal aBigDecimal){
		return aBigDecimal.round(new MathContext(10, RoundingMode.HALF_UP)).stripTrailingZeros();
	}
	
	public String addMatrixDetail_Jeesmon() {
		Number num = (Number) allocationPercent.getValue();
	    BigDecimal pct = (num == null)? ArithmeticUtils.ZERO : ArithmeticUtils.toBigDecimal(num.doubleValue());
	    
		if (pct == null || pct.compareTo(ArithmeticUtils.ZERO) <= 0 || pct.compareTo(new BigDecimal("1")) > 0) {
			FacesMessage message = new FacesMessage("Allocation percent must be between 0 and 1.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
	    }
		
		TaxAllocationMatrixDetail taxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
		List<HtmlInputText> inputControls = this.matrixCommonBean.findInputControls(allocDriverPanel, new ArrayList<HtmlInputText>());
		this.matrixCommonBean.setMatrixProperties(inputControls, taxAllocationMatrixDetail, selectedMatrix.getDriverCount());
		taxAllocationMatrixDetail.setAllocationPercent(pct);
		taxAllocationMatrixDetail.setComments((String) allocationComment.getValue());
		
		selectedMatrix.getTaxAllocationMatrixDetails().add(taxAllocationMatrixDetail);

		allocationPercent.setValue(null);
		allocationComment.setValue(null);
		
		return null;
	}
	
	public String addJurisMatrixDetail_Jeesmon() { 
		logger.info("enter addJurisMatrixDetail");
		// Re-verify jurisdiction, 
		Jurisdiction jurisdiction = jurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction == null) {
			FacesMessage message = new FacesMessage("Jurisdiction not found or not unique.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

        logger.info("jurisdiction.id = " + jurisdiction.getJurisdictionId());
	    logger.info("jurisdiction.state = " + jurisdiction.getState());
	    logger.info("jurisdiction.geocode = " + jurisdiction.getGeocode());
	    logger.info("alloc id = " + this.selectedMatrix.getTaxAllocationMatrixId());

	    // Make sure not already assigned
	    if(unsavedJurisdictionAllocationMatrixDetails != null) {
		    for (JurisdictionAllocationMatrixDetail amd : unsavedJurisdictionAllocationMatrixDetails) {
		    	if (amd.getJurisdiction().getJurisdictionId().equals(jurisdiction.getJurisdictionId())) {
					FacesMessage message = new FacesMessage("Jurisdiction allocation already exists.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
		    	}
		    }
	    }
	    
	    Number num = (Number) allocationPercent.getValue();
	    BigDecimal pct = (num == null)? ArithmeticUtils.ZERO : ArithmeticUtils.toBigDecimal(num.doubleValue());
	    
		if (pct == null || pct.compareTo(ArithmeticUtils.ZERO) <= 0 || pct.compareTo(new BigDecimal("1")) > 0) {
			FacesMessage message = new FacesMessage("Allocation percent must be between 0 and 1.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
	    }

	    JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
	    amd.setTaxAllocationMatrixDetailInstanceId(jurisSplitInstanceId.longValue());
	    amd.setJurisdiction(jurisdiction);
	    amd.setAllocationPercent(pct);
	    if(jurisSplitInstanceId == 0L) {
	    	amd.setTaxAllocationMatrixDetailId(0L);
	    }
	    else if(instanceIdDetailIdMap != null && instanceIdDetailIdMap.containsKey(jurisSplitInstanceId)) {
	    	amd.setTaxAllocationMatrixDetailId(instanceIdDetailIdMap.get(jurisSplitInstanceId));
	    }
	    
	    if(unsavedJurisdictionAllocationMatrixDetails == null) {
	    	unsavedJurisdictionAllocationMatrixDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
	    }
	    
	    unsavedJurisdictionAllocationMatrixDetails.add(amd);
    	jurisdictionHandler.reset();
    	allocationPercent.setValue(null);
	    return null;
	}
	
	public synchronized void selectedMasterRowChanged(ActionEvent e) throws AbortProcessingException {
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedMasterRowIndex = table.getRowIndex();
	    selectedMasterMatrixSource = (TaxAllocationMatrix) table.getRowData();
	    
	    this.futureSplit = "1".equalsIgnoreCase(selectedMasterMatrixSource.getFutureSplit());
	    
	    super.resetSelection();
	    
	    //Reset middle table
	    selectedMatrixDetails = null;

	    TaxAllocationMatrix matrix = selectedMasterMatrixSource;
	    if(selectedMasterMatrixSource != null){
			selectedMatrixDetails = getMatrixService().getDetailRecords(matrix,getMatrixDataModel().getDetailOrderBy());

			//Set first row as selected if exists.
			if(selectedMatrixDetails != null && selectedMatrixDetails.size()>0){	
				TaxAllocationMatrix matrixMiddle = ((TaxAllocationMatrix) selectedMatrixDetails.get(0));
				if (matrixMiddle != null) {
					selectedMatrixSource = getMatrixService().findByIdWithDetails(matrixMiddle.getTaxAllocationMatrixId());
					setSelectedRowIndex(0);
				}
			}
			else{
				selectedMatrixDetails = new ArrayList<TaxAllocationMatrix>();
			}
		}
		else {
			selectedMatrixDetails = new ArrayList<TaxAllocationMatrix>();
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public synchronized void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		// NOTE:  We preset selection to null to deal 
		// with multiple requests coming in while still
		// fetching matrix details
		selectedMatrixSource = null;
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		TaxAllocationMatrix matrixMiddle = ((TaxAllocationMatrix) table.getRowData());
		if (matrixMiddle != null) {
			selectedMatrixSource = getMatrixService().findByIdWithDetails(matrixMiddle.getTaxAllocationMatrixId());
			setSelectedRowIndex(table.getRowIndex());
		}
	}
	
	@Override
	public String saveAction() {
		return saveAction(false);
	}
	
	@Override
	public String deleteAction() {
		if(allowDeleteTaxAllocationMatrix){
			return super.deleteAction();
		}
		else{		
			selectedMatrix.setActiveBooleanFlag(false);			
			currentAction = EditAction.UPDATE;
			return saveAction(false);
		}
	}	
	
	public String saveAction(boolean forceValidate) {
		boolean valid = validateDrivers(FacesContext.getCurrentInstance(), null, forceValidate);
		
		String errMessage = verifyDates();
		if(errMessage!=null && errMessage.length()>0){
			FacesMessage message = new FacesMessage(errMessage);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		selectedMatrix.setBinaryWeight(super.CalcBinaryWeight());
		selectedMatrix.setSignificantDigits(super.CalcSignificantDigits());
		if(this.futureSplit) {
			selectedMatrix.setFutureSplit("1");
			selectedMatrix.setTaxAllocationMatrixDetails(new ArrayList<TaxAllocationMatrixDetail>());
		}
		else {
			selectedMatrix.setFutureSplit("0");
			
//			List<TaxAllocationMatrixDetail> selectedMatrixDetails = new ArrayList<TaxAllocationMatrixDetail>();
//			for (TaxObject taxObject : taxObjectItems) {
//				TaxAllocationMatrixDetail aTaxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
			//	aTaxAllocationMatrixDetail.setTaxAllocationMatrixId(selectedMatrix.getId());
//				aTaxAllocationMatrixDetail.setTaxcodeCode(taxObject.getTaxCode());
//				aTaxAllocationMatrixDetail.setAllocationPercent(taxObject.getAllocationPercent());	
//				selectedMatrixDetails.add(aTaxAllocationMatrixDetail);
//			}
//			selectedMatrix.setTaxAllocationMatrixDetails(selectedMatrixDetails);
			
			if (selectedMatrix != null && ArithmeticUtils.subtract(selectedMatrix.getTotalAllocationPercent(), 1.0).compareTo(ArithmeticUtils.ZERO) != 0) {
				FacesMessage message = new FacesMessage("Total Allocation % must equal 100%.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				valid = false;
		    }
		}
		
		List<TaxAllocationMatrixDetail> removeList = new ArrayList<TaxAllocationMatrixDetail>();
		for(TaxAllocationMatrixDetail t : selectedMatrix.getTaxAllocationMatrixDetails()) {
			t.setAllocationPercent(stripTrailingZeros(t.getAllocationPercent()));
			
			if((t.getTaxcodeCode()==null || t.getTaxcodeCode().length()==0) && 
					(t.getAllocationPercent()==null || t.getAllocationPercent().compareTo(ArithmeticUtils.ZERO) == 0)){
				
				//Ignore lines without both a taxcode and percent.
				removeList.add(t);	
				continue;
			}
			
			if(t.getTaxcodeCode()==null || t.getTaxcodeCode().length()==0){
				FacesMessage message = new FacesMessage("Please select a TaxCode.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				valid = false;
				break;
			}
			
			if(t.getAllocationPercent()==null || t.getAllocationPercent().compareTo(ArithmeticUtils.ZERO) == 0){
				FacesMessage message = new FacesMessage("Please select a Allocation Percent.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				valid = false;
				break;
			}
		}
		
		if(removeList.size() > 0) {
			selectedMatrix.getTaxAllocationMatrixDetails().removeAll(removeList);
		}
		
		/////////////////////////// Testing only
		
		for(TaxAllocationMatrixDetail t : selectedMatrix.getTaxAllocationMatrixDetails()) {
			System.out.println("t.getTaxcodeCode()" + t.getTaxcodeCode());
			System.out.println("t.getTaxAllocationMatrixDetailId()" + t.getTaxAllocationMatrixDetailId());
			System.out.println("t.getInstanceCreatedId()" + t.getInstanceCreatedId());
		}
		
		for(JurisdictionAllocationMatrixDetail juris : selectedMatrix.getJurisdictionAllocationMatrixDetails()) {
			System.out.println("juris.getInstanceCreatedId()" + juris.getInstanceCreatedId());
			System.out.println("juris.getTaxAllocationMatrixDetailId()" + juris.getTaxAllocationMatrixDetailId());
			System.out.println("juris.getTaxAllocationMatrixDetailInstanceId()" + juris.getTaxAllocationMatrixDetailInstanceId());
		}
		
		/////////////////////////////
		
	
		if (!valid) return null;
		
		return super.saveAction();
	}
	
	public String saveFromSplittingTool() {
		currentAction = EditAction.ADD_FROM_TRANSACTION;
		getCopyAddPanel();
		return saveAction(true);
	}
	
	@Override
	public void searchDriver(ActionEvent e) {
		String id = (String) e.getComponent().getAttributes().get("ID");
		if(id.equals("allocDriverPanel")) {
			getDriverHandlerBean().initSearch(selectedAllocDriverMatrix, (HtmlInputText) e.getComponent().getParent().getChildren().get(0), e.getComponent());
		}
		else {
			super.searchDriver(e);
		}
		
		if(id.equalsIgnoreCase("filterPanel")) {
			getDriverHandlerBean().setCallBackScreen("tax_allocation_matrix_main");
		}
		else{
			getDriverHandlerBean().setCallBackScreen("tax_alloc_matrix_add");
		}		
	}
	
	@Override
	public String resetFilter() {
		super.resetFilter();
		return "tax_allocation_matrix_main";
	}
	
	@Override
	public void resetSelection() {
		selectedMasterRowIndex= -1;
		selectedMasterMatrixSource = null;
		selectedMatrixDetails = null;
		super.resetSelection();
	}

	@Override
	public String getBeanName() {
		return "TaxAllocationMatrixViewBean";
	}

	@Override
	public boolean getIncludeDescFields() {
		return false;
	}

	@Override
	public String getMainPageAction() {
		return "tax_allocation_matrix_main";
	}

	@Override
	public String getDetailFormName() {
		return "matrixDetailForm";
	}

	public Long getInstanceCreatedId() {
		return instanceCreatedId;
	}

	public void setInstanceCreatedId(Long instanceCreatedId) {
		this.instanceCreatedId = instanceCreatedId;
	}

	public void deleteMatrixDetail(ActionEvent e) {
		logger.info("enter deleteMatrixDetail");
		
		Long deletedId = null;
		Iterator<TaxAllocationMatrixDetail> iter = selectedMatrix.getTaxAllocationMatrixDetails().iterator(); 
		while (iter.hasNext()) {
			TaxAllocationMatrixDetail detail = iter.next();
			
			if (detail.getInstanceCreatedId().equals(instanceCreatedId)) {
				deletedId = detail.getTaxAllocationMatrixDetailId();
				iter.remove();
				FacesUtils.refreshForm("matrixDetailForm");
				break;
			}
		}
		
		List<JurisdictionAllocationMatrixDetail> existing = selectedMatrix.getJurisdictionAllocationMatrixDetails();
		if(existing != null) {
			List<JurisdictionAllocationMatrixDetail> removeList = new ArrayList<JurisdictionAllocationMatrixDetail>();
			for(JurisdictionAllocationMatrixDetail d : existing) {
				if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId().equals(deletedId)) {
					removeList.add(d);
				}
			}
			
			if(removeList.size() > 0) {
				existing.removeAll(removeList);
			}
		}
	}
	
	@Override
	protected void prepareDetailDisplay(EditAction action, PurchaseTransaction detail) {
		if(action.equals(EditAction.COPY_ADD)) {
			if(selectedMatrix != null) {
				List<TaxAllocationMatrixDetail> details = new ArrayList<TaxAllocationMatrixDetail>();
				for(TaxAllocationMatrixDetail d : selectedMatrix.getTaxAllocationMatrixDetails()) {
					TaxAllocationMatrixDetail copy = new TaxAllocationMatrixDetail();
					BeanUtils.copyProperties(d, copy, new String[]{"taxAllocationMatrixDetailId"});
					details.add(copy);
				}
				selectedMatrix.setTaxAllocationMatrixDetails(details);
				
				List<JurisdictionAllocationMatrixDetail> jurisDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
				for(JurisdictionAllocationMatrixDetail d : selectedMatrix.getJurisdictionAllocationMatrixDetails()) {
					JurisdictionAllocationMatrixDetail copy = new JurisdictionAllocationMatrixDetail();
					if(d.getTaxAllocationMatrixDetailId() == 0L) {
						BeanUtils.copyProperties(d, copy);
					}
					else {
						BeanUtils.copyProperties(d, copy, new String[]{"taxAllocationMatrixDetailId"});
					}
					jurisDetails.add(copy);
				}
				selectedMatrix.setJurisdictionAllocationMatrixDetails(jurisDetails);
			}
		}
		
		super.prepareDetailDisplay(action, detail);
	}
	
	public BigDecimal getTotalJurisdictionAllocationPercent() {
		BigDecimal sum = ArithmeticUtils.ZERO;
		List<JurisdictionAllocationMatrixDetail> details = getSelectedMatrixJurisAllocDetails();
		if (details != null) {
			for (JurisdictionAllocationMatrixDetail detail : details) {
				if (detail.getAllocationPercent() != null) {
				    sum = ArithmeticUtils.add(sum, detail.getAllocationPercent());	
				}					
			}
		}
		
		return sum;
	}

	public SearchJurisdictionHandler getJurisdictionHandler() {
		return jurisdictionHandler;
	}

	public void setJurisdictionHandler(SearchJurisdictionHandler jurisdictionHandler) {
		this.jurisdictionHandler = jurisdictionHandler;
	}
	
	@Override
	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		super.setMatrixCommonBean(matrixCommonBean);
		filterHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		filterHandler.setCacheManager(matrixCommonBean.getCacheManager());
		jurisdictionHandler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		jurisdictionHandler.setCacheManager(matrixCommonBean.getCacheManager());
	}

	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}

	public void setFilterHandler(SearchJurisdictionHandler filterHandler) {
		this.filterHandler = filterHandler;
	}
	
	public String jurisCancelAction() {
		unsavedJurisdictionAllocationMatrixDetails = null;
		
		if(this.globalSplit) {
			this.globalSplit = false;
		}
		
		String view = null;
		if(this.actionFromSplittingTool) {
			view = this.jurisOkAction;
			this.actionFromSplittingTool = false;
		}
		else {
			view = "tax_alloc_matrix_add";
		}
		
		jurisdictionHandler.reset();
		return view;
	}
	
	public String verifyDates(){
		String errMessage = ""; 
		

		Date effectTS = (Date)selectedMatrix.getEffectiveDate();
		Date exprTS = (Date)selectedMatrix.getExpirationDate();
		
		boolean hasDateIssue = false;
		if(effectTS==null || exprTS==null){
			if (effectTS==null) {  
				errMessage = errMessage + "Effective Date may not be blank. ";
				hasDateIssue = true;
			}
			if (exprTS==null) {
				errMessage = errMessage + "Expiration Date may not be blank. ";
				hasDateIssue = true;
			}
		}
	
		if(hasDateIssue){
			return errMessage;
		}
	
		if(this.getAddAction()){		
			Date todayDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();	
			if (effectTS.before(todayDate)) {
				errMessage = errMessage + "Effective date must be on or after today. ";
		    }
		}
	
		if (effectTS!=null && exprTS!=null && exprTS.before(effectTS)) {
			errMessage = errMessage + "Expiration Date may not be earlier than Effective Date. ";
		} 
		
		return errMessage;
	}
	
	public String jurisSaveAction() {
		if (selectedMatrix != null && ArithmeticUtils.subtract(getTotalJurisdictionAllocationPercent(), 1.0).compareTo(ArithmeticUtils.ZERO) != 0) {
			FacesMessage message = new FacesMessage("Total allocation % must equal 100%.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
	    }
		
		if(jurisSplitInstanceId == 0L) {
			//Global should remove everything.
			selectedMatrix.getJurisdictionAllocationMatrixDetails().clear();
			globalSplit = true;
		}
		else {
			List<JurisdictionAllocationMatrixDetail> existing = selectedMatrix.getJurisdictionAllocationMatrixDetails();
			if(existing != null) {
				List<JurisdictionAllocationMatrixDetail> removeList = new ArrayList<JurisdictionAllocationMatrixDetail>();
				for(JurisdictionAllocationMatrixDetail d : existing) {
					if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(jurisSplitInstanceId)) {
						removeList.add(d);
					}
					else if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId().equals(0L)) {
						removeList.add(d);
					}
					else if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(0L)) {
						//Individual should remove any global.
						removeList.add(d);
					}
				}
				
				if(removeList.size() > 0) {
					existing.removeAll(removeList);
				}
			}
		}
		
		for(JurisdictionAllocationMatrixDetail j : unsavedJurisdictionAllocationMatrixDetails) {
			j.setAllocationPercent(stripTrailingZeros(j.getAllocationPercent()));
		}
		selectedMatrix.getJurisdictionAllocationMatrixDetails().addAll(unsavedJurisdictionAllocationMatrixDetails);
		unsavedJurisdictionAllocationMatrixDetails = null;
		
		String view = null;
		if(this.actionFromSplittingTool) {
			view = this.jurisOkAction;
			this.actionFromSplittingTool = false;
		}
		else {
			view = "tax_alloc_matrix_add";
		}
		
		return view;
	}
	
	public String deleteJurisMatrixDetail(ActionEvent e) {
		logger.info("enter deleteJurisMatrixDetail");
		
		Iterator<JurisdictionAllocationMatrixDetail> iter = unsavedJurisdictionAllocationMatrixDetails.iterator();
		List<JurisdictionAllocationMatrixDetail> removeList = new ArrayList<JurisdictionAllocationMatrixDetail>();
		while (iter.hasNext()) {
			JurisdictionAllocationMatrixDetail detail = iter.next();
			
			if (detail.getInstanceCreatedId().equals(instanceCreatedId)) {
				removeList.add(detail);
				FacesUtils.refreshForm("matrixDetailForm");
				break;
			}
		}
		
		if(removeList.size() > 0) {
			unsavedJurisdictionAllocationMatrixDetails.removeAll(removeList);
		}
		
		return null;
	}
	
	public String jurisDeleteAction() {
		if(displayDeleteWarning == 0L) {
			displayDeleteWarning = 1L;
			return null;
		}
		else {
			displayDeleteWarning = 0L;
		}
		
		List<JurisdictionAllocationMatrixDetail> existing = selectedMatrix.getJurisdictionAllocationMatrixDetails();
		if(existing != null) {
			List<JurisdictionAllocationMatrixDetail> removeList = new ArrayList<JurisdictionAllocationMatrixDetail>();
			for(JurisdictionAllocationMatrixDetail d : existing) {
				if(jurisSplitInstanceId == 0L && d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId().equals(jurisSplitInstanceId)) {
					removeList.add(d);
				}
				else if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId().equals(jurisSplitInstanceId)) {
					removeList.add(d);
				}
			}
			
			if(removeList.size() > 0) {
				existing.removeAll(removeList);
			}
		}
		
		unsavedJurisdictionAllocationMatrixDetails = null;
		
		String view = null;
		if(this.actionFromSplittingTool) {
			view = this.jurisOkAction;
			this.actionFromSplittingTool = false;
		}
		else {
			view = "tax_alloc_matrix_add";
		}
		
		return view;
	}

	public Long getJurisSplitInstanceId() {
		return jurisSplitInstanceId;
	}

	public void setJurisSplitInstanceId(Long jurisSplitInstanceId) {
		this.jurisSplitInstanceId = jurisSplitInstanceId;
	}
	
	private boolean hasIndividualSplit() {
		boolean result = false;
		if(this.jurisSplitInfoMap != null) {
			for(Map.Entry<Long, Boolean> e : this.jurisSplitInfoMap.entrySet()) {
				if(e.getValue()) {
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
	public String globalJurisSplitAction() {
		if(displayGlobalJurisWarning == 0L && hasIndividualSplit()) {
			displayGlobalJurisWarning = 1L;
			return null;
		}
		else {
			displayGlobalJurisWarning = 0L;
		}
		
		this.jurisSplitInstanceId = 0L;
		setViewPanel(null);
		unsavedJurisdictionAllocationMatrixDetails = null;
		this.globalSplit = true;
		
		this.getSelectedMatrixJurisAllocDetails();	
		if(unsavedJurisdictionAllocationMatrixDetails!=null && unsavedJurisdictionAllocationMatrixDetails.size()==0){
			//Create a empty row
			addJurisMatrixDetail();
		}
				
		splitTaxCode = "*ALL";
		splitTaxCodeDescription = "All Allocation TaxCodes";
		
		return "juris_alloc_matrix_add";
	}
	
	public String getSplitTaxCode() {
		return splitTaxCode;
	}

	public void setSplitTaxCode(String splitTaxCode) {
		this.splitTaxCode = splitTaxCode;
	}
	
	public String getSplitTaxCodeDescription() {
		return splitTaxCodeDescription;
	}

	public void setSplitTaxCodeDescription(String splitTaxCodeDescription) {
		this.splitTaxCodeDescription = splitTaxCodeDescription;
	}
	
	@Override
	public String displayCopyAddAction() {
		String returnScreen = displayUpdateAction();
		if(returnScreen==null || returnScreen.length()==0){
			return returnScreen;
		}
		
		currentAction = EditAction.COPY_ADD;
		selectedMatrix.setId(null);
		selectedMatrix.setEffectiveDate(null);
		selectedMatrix.setExpirationDate(null);
		selectedMatrix.initializeDefaults();
		
		createNewTaxDetailJuris(selectedMatrix);

		
	    return "matrix_copy_add";
	}	
	
	private void createNewTaxDetailJuris(TaxAllocationMatrix newMatrix){
		//////////////////////////////////////
		for(TaxAllocationMatrixDetail t : newMatrix.getTaxAllocationMatrixDetails()) {
			System.out.println("t.getTaxcodeCode()" + t.getTaxcodeCode());
			System.out.println("t.getTaxAllocationMatrixDetailId()" + t.getTaxAllocationMatrixDetailId());
			System.out.println("t.getInstanceCreatedId()" + t.getInstanceCreatedId());
		}
		
		for(JurisdictionAllocationMatrixDetail juris : newMatrix.getJurisdictionAllocationMatrixDetails()) {
			System.out.println("juris.getInstanceCreatedId()" + juris.getInstanceCreatedId());
			System.out.println("juris.getTaxAllocationMatrixDetailId()" + juris.getTaxAllocationMatrixDetailId());
			System.out.println("juris.getTaxAllocationMatrixDetailInstanceId()" + juris.getTaxAllocationMatrixDetailInstanceId());
		}
		//////////////////////////////////////////////////
		
		//Clone TaxAllocationMatrixDetail & JurisdictionAllocationMatrixDetail	
		Long instanceCreatedId = null;
		Long instanceCreatedIdOri = null;
		List<JurisdictionAllocationMatrixDetail> newJurisdictionAllocationMatrixDetailList = new ArrayList<JurisdictionAllocationMatrixDetail>();
		for(JurisdictionAllocationMatrixDetail juris : newMatrix.getJurisdictionAllocationMatrixDetails()) {
		
			JurisdictionAllocationMatrixDetail newJurisdictionAllocationMatrixDetail = new JurisdictionAllocationMatrixDetail();
			instanceCreatedId = newJurisdictionAllocationMatrixDetail.getInstanceCreatedId();
			BeanUtils.copyProperties(juris, newJurisdictionAllocationMatrixDetail);
			newJurisdictionAllocationMatrixDetail.setInstanceCreatedId(instanceCreatedId);//Need to use new instance Id
			newJurisdictionAllocationMatrixDetailList.add(newJurisdictionAllocationMatrixDetail);
			
			System.out.println("newJurisdictionAllocationMatrixDetail.getInstanceCreatedId()" + newJurisdictionAllocationMatrixDetail.getInstanceCreatedId());
			System.out.println("newJurisdictionAllocationMatrixDetail.getTaxAllocationMatrixDetailId()" + newJurisdictionAllocationMatrixDetail.getTaxAllocationMatrixDetailId());
			System.out.println("newJurisdictionAllocationMatrixDetail.getTaxAllocationMatrixDetailInstanceId()" + newJurisdictionAllocationMatrixDetail.getTaxAllocationMatrixDetailInstanceId());
		}
		
		Long taxAllocationMatrixDetailId = null;
		Long taxAllocationMatrixDetailInstanceCreatedId = null;
		List<TaxAllocationMatrixDetail> newTaxAllocationMatrixDetailList = new ArrayList<TaxAllocationMatrixDetail>();
		for(TaxAllocationMatrixDetail t : newMatrix.getTaxAllocationMatrixDetails()) {
			TaxAllocationMatrixDetail newTaxAllocationMatrixDetail = new TaxAllocationMatrixDetail();
			instanceCreatedId = newTaxAllocationMatrixDetail.getInstanceCreatedId();			
			BeanUtils.copyProperties(t, newTaxAllocationMatrixDetail);			
			newTaxAllocationMatrixDetail.setInstanceCreatedId(instanceCreatedId);//Need to use new instance Id		
			newTaxAllocationMatrixDetailList.add(newTaxAllocationMatrixDetail);
			
			instanceCreatedIdOri = t.getInstanceCreatedId();
			taxAllocationMatrixDetailId = newTaxAllocationMatrixDetail.getTaxAllocationMatrixDetailId();
			taxAllocationMatrixDetailInstanceCreatedId = newTaxAllocationMatrixDetail.getInstanceCreatedId();
			newTaxAllocationMatrixDetail.setTaxAllocationMatrixDetailId(null);
			
			for(JurisdictionAllocationMatrixDetail juris : newJurisdictionAllocationMatrixDetailList) {
				if((juris.getTaxAllocationMatrixDetailInstanceId()!=null) && (instanceCreatedIdOri.longValue() == juris.getTaxAllocationMatrixDetailInstanceId().longValue())){
					juris.setTaxAllocationMatrixDetailInstanceId(taxAllocationMatrixDetailInstanceCreatedId);
					juris.setTaxAllocationMatrixDetailId(null);
				}
			}
		}
		
		newMatrix.setTaxAllocationMatrixDetails(newTaxAllocationMatrixDetailList);
		newMatrix.setJurisdictionAllocationMatrixDetails(newJurisdictionAllocationMatrixDetailList);
	}
	
	public String displayCopyUpdateAction(){
		currentAction = EditAction.COPY_UPDATE;
		selectedMatrix.setId(null);
		selectedMatrix.setEffectiveDate(null);
		selectedMatrix.setExpirationDate(null);
		selectedMatrix.initializeDefaults();
		
		createNewTaxDetailJuris(selectedMatrix);
		
		return "matrix_update";
	}
	
	@Override
	public String displayUpdateAction() {
		this.globalSplit = false;
		this.actionFromSplittingTool = false;
		
		unsavedJurisdictionAllocationMatrixDetails = null;
				
		String returnScreen = super.displayUpdateAction();	
		if(returnScreen==null || returnScreen.length()==0){
			return returnScreen;
		}
		
		//Fill up TaxCode description
		List<TaxAllocationMatrixDetail> existing = selectedMatrix.getTaxAllocationMatrixDetails();
		if(existing!=null){
			for(TaxAllocationMatrixDetail d : existing) {
				TaxCode code = taxCodeDetailService.findTaxCode(new TaxCodePK(d.getTaxcodeCode()));
			    if(code!=null){
			    	d.setDescription(code.getDescription());
			    }
			}
		}
		
		//PP-453
		this.futureSplit = "1".equalsIgnoreCase(selectedMatrix.getFutureSplit());
		
		return returnScreen;
	}
	
	@Override
	public String displayDeleteAction() {
		String returnScreen = displayUpdateAction();
		if(returnScreen==null || returnScreen.length()==0){
			return returnScreen;
		}
		
		currentAction = EditAction.DELETE;
		
		if(getMatrixService().isTaxAllocationUsedinTransaction(selectedMatrix.getTaxAllocationMatrixId())){
			allowDeleteTaxAllocationMatrix = false;
		}
		else{
			allowDeleteTaxAllocationMatrix = true;
		}
		
	    return "matrix_delete";
	}	
	
	@Override
	public String displayViewAction() {
		String returnScreen = displayUpdateAction();
		currentAction = EditAction.VIEW;
		if(returnScreen==null || returnScreen.length()==0){
			return returnScreen;
		}
		 return "tax_allocation_matrix_view";
		 
		
	}	
	
	public String displayTaxAllocationMatrixView(Long taxAllocationId,String fromview) {
		
		selectedMatrixSource = getMatrixService().findByIdWithDetails(taxAllocationId);
		if(selectedMatrixSource !=null) {
			String nextAction = displayViewAction();
			if("viewFromTransaction".equals(fromview)) {
				currentAction = EditAction.VIEW_FROM_TRANSACTION;
			}
			else {
				currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
			}
			return nextAction;
		}
		else{
			return null;
		}
		
	}
	
	@Override
	public String displayAddAction() {
		this.futureSplit = false;
		this.globalSplit = false;
		this.actionFromSplittingTool = false;
		
		super.displayAddAction();
		selectedMatrix.setActiveFlag("1");
		
	//	selectedMatrix.setTaxAllocationMatrixDetails(new ArrayList<TaxAllocationMatrixDetail>());		
		//For TaxObject 
//		taxObjectItems= new ArrayList<TaxObject>();
//		TaxObject aTaxObject = new TaxObject();
//		taxObjectItems.add(aTaxObject);
		
		addMatrixDetail();
		
		return "matrix_add";
	}

	public boolean isGlobalSplit() {
		return globalSplit;
	}

	public void setGlobalSplit(boolean globalSplit) {
		this.globalSplit = globalSplit;
	}
	
	public Boolean getGlobalSplit() {
		return this.globalSplit;
	}
	
	public String individualJurisSplitAction_jeesmon() {
		if(displayIndividualJurisWarning == 0L && this.globalSplit) {
			displayIndividualJurisWarning = 1L;
			return "tax_alloc_matrix_add";
		}
		else {
			displayIndividualJurisWarning = 0L;
		}
		
		setViewPanel(null);
		unsavedJurisdictionAllocationMatrixDetails = null;
		this.globalSplit = false;
		
		this.getSelectedMatrixJurisAllocDetails();	
		
		for(TaxAllocationMatrixDetail t : selectedMatrix.getTaxAllocationMatrixDetails()) {
			
			if(t.getInstanceCreatedId() != null && t.getInstanceCreatedId().equals(jurisSplitInstanceId)) {
				if(t.getTaxcodeCode()==null || t.getTaxcodeCode().length()==0){
					/*
					FacesMessage message = new FacesMessage("Please select a TaxCode.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);*/
					return null;
				}
				else{
					splitTaxCode = t.getTaxcodeCode();
					splitTaxCodeDescription = t.getDescription();				
					break;
				}
			}
		}
		
		if(unsavedJurisdictionAllocationMatrixDetails!=null && unsavedJurisdictionAllocationMatrixDetails.size()==0){
			//Create a empty row
			addJurisMatrixDetail();
		}
		return "juris_alloc_matrix_add";
	}
	
	public boolean getDisplayJurisDelete() {
		return getSelectedMatrixJurisAllocDetails().size() > 0 && !getReadOnlyAction();
	}

	public Long getDisplayDeleteWarning() {
		return displayDeleteWarning;
	}

	public void setDisplayDeleteWarning(Long displayDeleteWarning) {
		this.displayDeleteWarning = displayDeleteWarning;
	}
	
	public void cancelDeleteWarning(ActionEvent e) {
		this.displayDeleteWarning = 0L;
	}
		
	public Boolean getAllowDeleteTaxAllocationMatrix() {
		return allowDeleteTaxAllocationMatrix;
	}

	public void setAllowDeleteTaxAllocationMatrix(Boolean allowDeleteTaxAllocationMatrix) {
		this.allowDeleteTaxAllocationMatrix = allowDeleteTaxAllocationMatrix;
	}
	
	public String prepareJurisSplit(TaxAllocationMatrix m, long jurisSplitInstanceId, EditAction action) {
		this.actionFromSplittingTool = true;
		this.selectedMatrix = m;
		this.currentAction = action;
		
		this.jurisSplitInstanceId = jurisSplitInstanceId;
		setViewPanel(null);
		
		unsavedJurisdictionAllocationMatrixDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
		if(selectedMatrix.getJurisdictionAllocationMatrixDetails() != null) {
			for(JurisdictionAllocationMatrixDetail d : selectedMatrix.getJurisdictionAllocationMatrixDetails()) {
				if(jurisSplitInstanceId == 0L) {
					if(d.getTaxAllocationMatrixDetailId() != null && d.getTaxAllocationMatrixDetailId() == 0L) {
						JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
						BeanUtils.copyProperties(d, amd);
						unsavedJurisdictionAllocationMatrixDetails.add(amd);
					}
				}
				else {
					if(d.getTaxAllocationMatrixDetailInstanceId() != null && d.getTaxAllocationMatrixDetailInstanceId() == jurisSplitInstanceId) {
						JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
						BeanUtils.copyProperties(d, amd);
						unsavedJurisdictionAllocationMatrixDetails.add(amd);
					}
				}				
			}
		}
		
		if(unsavedJurisdictionAllocationMatrixDetails!=null && unsavedJurisdictionAllocationMatrixDetails.size()==0){
			//Create a empty row
			addJurisMatrixDetail();
		}
		
		splitTaxCode = "";
		splitTaxCodeDescription = "";
		
		if(jurisSplitInstanceId == 0L) {
			globalSplit = true;
			splitTaxCode = "*ALL";
			splitTaxCodeDescription = "All Allocation TaxCodes";

		}
		else{
			globalSplit = false;
			
			for(TaxAllocationMatrixDetail t : m.getTaxAllocationMatrixDetails()) {
				
				if(t.getInstanceCreatedId() != null && t.getInstanceCreatedId().equals(jurisSplitInstanceId)) {
					if(t.getTaxcodeCode()==null || t.getTaxcodeCode().length()==0){
						return null;
					}
					else{
						splitTaxCode = t.getTaxcodeCode();
						splitTaxCodeDescription = t.getDescription();				
						break;
					}
				}
			}
		}
		
		return "juris_alloc_matrix_add";
	}
	
	public HtmlPanelGrid getJurisdictionViewPanel() {
		String objectProperty = null;
		String code = null;
	
			objectProperty = "selectedMatrix";
			code = selectedMatrix.getDriverCode();
		
		jurisdictionViewPanel = MatrixCommonBean.createDriverPanel(
				code, matrixCommonBean.getCacheManager(),
				matrixCommonBean.getEntityItemDTO(),
				"viewPanel", 
				getBeanName(), objectProperty, "matrixCommonBean",
				true, false, getIncludeDescFields(), true, false); 
		matrixCommonBean.prepareTaxDriverPanel(jurisdictionViewPanel);
		return jurisdictionViewPanel;
	}

	public HtmlPanelGrid getJurisdictionViewPanel_jeesmon() {
		String objectProperty = null;
		String code = null;
		if(this.globalSplit) {
			objectProperty = "selectedMatrix";
			code = selectedMatrix.getDriverCode();
		}
		else {
			objectProperty = "selectedAllocDriverMatrix";
			
			selectedAllocDriverMatrix = new TaxAllocationMatrix();
			selectedAllocDriverMatrix.initNullDriverFields();
			
			boolean found = false;
			if(this.jurisSplitInstanceId > 0L && selectedMatrix.getTaxAllocationMatrixDetails() != null) {
				for(TaxAllocationMatrixDetail d : selectedMatrix.getTaxAllocationMatrixDetails()) {
					if(d != null && d.getInstanceCreatedId().equals(this.jurisSplitInstanceId)) {
						matrixCommonBean.setMatrixProperties(d, selectedAllocDriverMatrix);
						found = true;
						break;
					}
				}
			}
			
			if(this.jurisSplitInstanceId < 0L && !found && this.actionFromSplittingTool) {
				matrixCommonBean.setMatrixProperties(selectedMatrix, selectedAllocDriverMatrix);
			}
			
			code = selectedAllocDriverMatrix.getDriverCode();
		}
		
		jurisdictionViewPanel = MatrixCommonBean.createDriverPanel(
				code, matrixCommonBean.getCacheManager(),
				matrixCommonBean.getEntityItemDTO(),
				"viewPanel", 
				getBeanName(), objectProperty, "matrixCommonBean",
				true, false, getIncludeDescFields(), true, false); 
		matrixCommonBean.prepareTaxDriverPanel(jurisdictionViewPanel);
		return jurisdictionViewPanel;
	}

	public void setJurisdictionViewPanel(HtmlPanelGrid jurisdictionViewPanel) {
		this.jurisdictionViewPanel = jurisdictionViewPanel;
	}

	public Map<Long, Boolean> getJurisSplitInfoMap() {
		return jurisSplitInfoMap;
	}

	public void setJurisSplitInfoMap(Map<Long, Boolean> jurisSplitInfoMap) {
		this.jurisSplitInfoMap = jurisSplitInfoMap;
	}
	
	public void globalJurisWarningCancel(ActionEvent e) {
		this.displayGlobalJurisWarning = 0L;
	}

	public Long getDisplayGlobalJurisWarning() {
		return displayGlobalJurisWarning;
	}

	public void setDisplayGlobalJurisWarning(Long displayGlobalJurisWarning) {
		this.displayGlobalJurisWarning = displayGlobalJurisWarning;
	}

	public Long getDisplayIndividualJurisWarning() {
		return displayIndividualJurisWarning;
	}

	public void setDisplayIndividualJurisWarning(Long displayIndividualJurisWarning) {
		this.displayIndividualJurisWarning = displayIndividualJurisWarning;
	}
	
	public void individualJurisWarningCancel(ActionEvent e) {
		this.displayIndividualJurisWarning = 0L;
	}

	public String getJurisOkAction() {
		return jurisOkAction;
	}

	public void setJurisOkAction(String jurisOkAction) {
		this.jurisOkAction = jurisOkAction;
	}
	
	public BigDecimal getRemainingJuriAllocationPercent() {
		List<JurisdictionAllocationMatrixDetail> selectedMatrixJurisAllocDetails = getSelectedMatrixJurisAllocDetails();
		if(selectedMatrixJurisAllocDetails != null) {	
			BigDecimal sum = ArithmeticUtils.ZERO;
			if (selectedMatrixJurisAllocDetails != null) {
				for (JurisdictionAllocationMatrixDetail juris : selectedMatrixJurisAllocDetails) {
					if (juris.getAllocationPercent() != null) {
					    sum = ArithmeticUtils.add(sum, juris.getAllocationPercent());	
					}					
				}
			}
			
			return ArithmeticUtils.subtract(new BigDecimal("1"), sum);
		}
		else {
			return new BigDecimal("1");
		}
	}
	
	public BigDecimal getRemainingAllocationPercent() {
		if(this.selectedMatrix != null) {
			return ArithmeticUtils.subtract(new BigDecimal("1"), this.selectedMatrix.getTotalAllocationPercent());
		}
		else {
			return ArithmeticUtils.ZERO;
		}
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentAction == EditAction.VIEW || currentAction == EditAction.VIEW_FROM_TRANSACTION || currentAction == EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY);
	}
	
	private Long selectedSearchCommand = null;
	private Long selectedTaxDetailCommand = null;
	
	//Up & Down 
	public String addJurisMatrixDetail() { 
		if(unsavedJurisdictionAllocationMatrixDetails == null) {
			unsavedJurisdictionAllocationMatrixDetails = new ArrayList<JurisdictionAllocationMatrixDetail>();
	    }
		
		JurisdictionAllocationMatrixDetail aJurisdiction = new JurisdictionAllocationMatrixDetail();
		aJurisdiction.setTaxAllocationMatrixDetailInstanceId(jurisSplitInstanceId);
		
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setJurisdictionId(null);
		//country should default.
		jurisdictionFilter.setCountry(getMatrixCommonBean().getUserPreferenceDTO().getUserCountry());
		aJurisdiction.setJurisdiction(jurisdictionFilter);
		
		if(globalSplit == true){
			aJurisdiction.setTaxAllocationMatrixDetailId(0L);
		}
		unsavedJurisdictionAllocationMatrixDetails.add(aJurisdiction);

	    return null;
	}
	
	public void findJurisdictionIdCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    selectedSearchCommand = null;
	    try{
	    	selectedSearchCommand = Long.parseLong(command);
	    }
	    catch(Exception e){	
	    }

	    return;
	}
	
	public void allocationPercentCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	   // selectedSearchCommand = null;
	   // try{
	   // 	selectedSearchCommand = Long.parseLong(command);
	   // }
	   // catch(Exception e){	
	   // }

	    return;
	}
	
	public String findJurisdictionIdAction() {
		//Used for screen action
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setJurisdictionId(null);
		
		if(selectedSearchCommand!=null){
			
			int commandInt = 0;
		    try{
		    	commandInt = Integer.parseInt(selectedSearchCommand.toString());
		    	
		    	JurisdictionAllocationMatrixDetail aJurisdiction = (JurisdictionAllocationMatrixDetail)unsavedJurisdictionAllocationMatrixDetails.get(commandInt);
		    	if(aJurisdiction!=null && aJurisdiction.getJurisdiction()!=null){
		    		jurisdictionFilter.setJurisdictionId(aJurisdiction.getJurisdiction().getId());		    		
		    		jurisdictionFilter.setCountry(aJurisdiction.getJurisdiction().getCountry());
		    	}
					
				
		    }
		    catch(Exception e){
		    }
		}
		
		taxJurisdictionBean.findId(this, "jurisdictionId", "juris_alloc_matrix_add", jurisdictionFilter);
		return "maintanence_tax_rates";
	}
	
	public void findIdCallback(Long id, String context) {
		//Used for seledted Id callback
		if (context.equalsIgnoreCase("jurisdictionId")) {
			//jurisdictionId = id;
			
			int commandInt = 0;
		    try{
		    	commandInt = Integer.parseInt(selectedSearchCommand.toString());
		    }
		    catch(Exception e){
		    	return;
		    }
		    
		    if(unsavedJurisdictionAllocationMatrixDetails==null || unsavedJurisdictionAllocationMatrixDetails.size()==0){
		    	return;//Impossible
		    }
		    
		    JurisdictionAllocationMatrixDetail aJurisdiction = (JurisdictionAllocationMatrixDetail)unsavedJurisdictionAllocationMatrixDetails.get(commandInt);
		    Jurisdiction selectedJurisdiction = jurisdictionService.findById(id);
		    if(aJurisdiction!=null && selectedJurisdiction!=null){
 	
		    
			 	    	
		    	aJurisdiction.setJurisdiction(selectedJurisdiction);
		    	//aJurisdiction.setJurisdictionId(id);
		    	//aJurisdiction.setGeocode(selectedJurisdiction.getGeocode());
		    	//aJurisdiction.setCountry(selectedJurisdiction.getCountry());
		    	//aJurisdiction.setState(selectedJurisdiction.getState());
		    	//aJurisdiction.setCity(selectedJurisdiction.getCity());
		    	//aJurisdiction.setCounty(selectedJurisdiction.getCounty());
		    	//aJurisdiction.setZip(selectedJurisdiction.getZip());
		    }
		}
	}

	private List<SelectItem> taxCodeItems = null; 
	
	public List<SelectItem> getTaxCodeItems() {
		if(taxCodeItems==null){
			//taxCodeItems = new ArrayList<SelectItem>();	
			//String countryCode = matrixCommonBean.getUserPreferenceDTO().getUserCountry();	
			//List<TaxCode> codes = ((countryCode == null || countryCode.length()==0))? null : taxCodeDetailService.findTaxCodeListByCountry(countryCode, null, null, null);			
			//if (codes != null) {
			//	for (TaxCode code : codes) {
			//		String value = code.getTaxCodePK().getTaxcodeCode() + " - " + code.getDescription();
			//		if (value.length() > 80) {
			//			value = value.substring(0, 80) + "...";
			//		}
			//		taxCodeItems.add(new SelectItem(code.getTaxCodePK().getTaxcodeCode(), value));	
			//	}
			//}
			
			
			taxCodeItems = new ArrayList<SelectItem>();
			taxCodeItems.add(new SelectItem("","Select a TaxCode"));
			List<TaxCode> codes = taxCodeDetailService.getAllTaxCode();
			

			if (codes != null) {
				for (TaxCode code : codes) {
					String value = code.getTaxCodePK().getTaxcodeCode();
					taxCodeItems.add(new SelectItem(value, value));
					// Check if previously selected code still exists
					//if ((taxCode != null) && code.getTaxCodePK().getTaxcodeCode().equals(taxCode)) {
					//	taxCodeExists = true;
					//}
				}
			}
		}
		
		return taxCodeItems;
	}
	
	public void taxcodeCodeSelected(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    TaxAllocationMatrixDetail selectedTaxObject = (TaxAllocationMatrixDetail)selectedMatrix.getTaxAllocationMatrixDetails().get(commandInt);    
	    String strTaxCode = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) event.getComponent()).getParent()).getSubmittedValue();
	    if(strTaxCode!=null && strTaxCode.length()>0){
		    TaxCode code = taxCodeDetailService.findTaxCode(new TaxCodePK(strTaxCode));
		    if(code!=null){
		    	selectedTaxObject.setDescription(code.getDescription());		    	
		    	selectedTaxObject.setTaxcodeCode(strTaxCode);
		    }
		    else{
		    	selectedTaxObject.setDescription("");
		    	selectedTaxObject.setTaxcodeCode("");
		    }
	    }
	    else{
	    	selectedTaxObject.setDescription("");
	    	selectedTaxObject.setTaxcodeCode("");
	    }
	
	    return;
	}
	
	/*
	 JurisdictionAllocationMatrixDetail amd = new JurisdictionAllocationMatrixDetail();
	    amd.setTaxAllocationMatrixDetailInstanceId(jurisSplitInstanceId.longValue());
	    amd.setJurisdiction(jurisdiction);
	    amd.setAllocationPercent(pct);
	 */
	
	//Jurisdiction Item
//	private List<TaxObject> taxObjectItems= new ArrayList<TaxObject>();
//	private List<JurisdictionAllocationMatrixDetail> jurisdictionObjectItems= new ArrayList<JurisdictionAllocationMatrixDetail>();
	
	public List<TaxAllocationMatrixDetail> getTaxObjectItems() {
	 	return selectedMatrix.getTaxAllocationMatrixDetails();
	}
	
	public List<JurisdictionAllocationMatrixDetail> getJurisdictionObjectItems() {
	 	return unsavedJurisdictionAllocationMatrixDetails;
	}
	
	public Integer getFirstJurisdictionObject() {
		if(unsavedJurisdictionAllocationMatrixDetails!=null && unsavedJurisdictionAllocationMatrixDetails.size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastJurisdictionObject() {
		if(unsavedJurisdictionAllocationMatrixDetails!=null && unsavedJurisdictionAllocationMatrixDetails.size()>=1){
			return new Integer(unsavedJurisdictionAllocationMatrixDetails.size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getFirstWhenObject() {
		if(selectedMatrix.getTaxAllocationMatrixDetails()!=null && selectedMatrix.getTaxAllocationMatrixDetails().size()>=1){
			return new Integer(0);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public Integer getLastWhenObject() {
		if(selectedMatrix.getTaxAllocationMatrixDetails()!=null && selectedMatrix.getTaxAllocationMatrixDetails().size()>=1){
			return new Integer(selectedMatrix.getTaxAllocationMatrixDetails().size() - 1);
		}
		else{
			return new Integer(-1);
		}
	}
	
	public void addWhenItem(ActionEvent e) throws AbortProcessingException {
		TaxObject aWhenObject = new TaxObject();
	}
	
	public void individualJurisSplitListener(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    selectedTaxDetailCommand = null;
	    try{
	    	selectedTaxDetailCommand = Long.parseLong(command);
	    }
	    catch(Exception e){	
	    }

	    return;
	}
	
	public String individualJurisSplitAction() {		
		if(selectedTaxDetailCommand!=null){			
			int commandInt = 0;
		    try{
		    	commandInt = Integer.parseInt(selectedTaxDetailCommand.toString());	    	
		    	TaxAllocationMatrixDetail selectedTaxObject = (TaxAllocationMatrixDetail)selectedMatrix.getTaxAllocationMatrixDetails().get(commandInt);    
			    String strTaxCode = selectedTaxObject.getTaxcodeCode();
			    if(strTaxCode!=null && strTaxCode.length()>0){
				    TaxCode code = taxCodeDetailService.findTaxCode(new TaxCodePK(strTaxCode));
				    if(code!=null){
				    	splitTaxCode = strTaxCode;
						splitTaxCodeDescription = code.getDescription();
						
						jurisSplitInstanceId = selectedTaxObject.getInstanceCreatedId();
				    }
				    else{
				    	return null;
				    }
			    }
			    else{
			    	return null;
			    }
		    }
		    catch(Exception e){
		    	return null;
		    }
		}
		
		//setViewPanel(null);
		unsavedJurisdictionAllocationMatrixDetails = null;
		this.globalSplit = false;
		return "juris_alloc_matrix_add";
	}
	
	public void processAddJurisdictionCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    addJurisMatrixDetail();

	    return;
	}
	
	public void processAddWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    //TaxObject aWhenObject = new TaxObject();
		//taxObjectItems.add(aWhenObject);
	    addMatrixDetail();

	    return;
	}
	
	public void processRemoveJurisdictionCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    unsavedJurisdictionAllocationMatrixDetails.remove(commandInt);

	    return;
	}
	
	public void processRemoveWhenCommand(ActionEvent event) {
	    String command = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
	    
	    int commandInt = 0;
	    try{
	    	commandInt = Integer.parseInt(command);
	    }
	    catch(Exception e){
	    	return;
	    }
	    
	    selectedMatrix.getTaxAllocationMatrixDetails().remove(commandInt);

	    return;
	}
	
	public class TaxObject{
		private String taxCode = "";
		private String description = "";
		private BigDecimal allocationPercent;
		
		public BigDecimal getAllocationPercent() {
			return allocationPercent;
		}

		public void setAllocationPercent(BigDecimal allocationPercent) {
			this.allocationPercent = allocationPercent;
		}

		public String getTaxCode() {
			return this.taxCode;
		}

		public void setTaxCode(String taxCode) {
			this.taxCode = taxCode;
		}
		
		public String getDescription() {
			return this.description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}
}
