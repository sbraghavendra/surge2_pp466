package com.ncsts.view.bean;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.JurisdictionTaxrate;
import com.ncsts.domain.LocationMatrix;
import com.ncsts.domain.ProruleAudit;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.PurchaseTransactionLog;
import com.ncsts.domain.SitusMatrix;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.VendorNexusDtl;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.JurisdictionTaxrateService;
import com.ncsts.services.ProcruleService;
import com.ncsts.services.SitusMatrixService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.services.VendorItemService;
import com.ncsts.services.VendorNexusDetailService;
import com.ncsts.view.util.TogglePanelController;
import com.ncsts.view.util.ValidTransactionDates;
import javax.faces.application.FacesMessage;

public class TransactionViewBean implements ApplicationContextAware {
	static private Logger logger = LoggerFactory.getInstance().getLogger(TransactionViewBean.class);
	
	private static final String DEFAULT_DISPLAY = "taxDriver";
	
	@Autowired
	private TaxCodeDetailService taxCodeDetailService;
	
	@Autowired
	private TaxCodeService taxCodeService;
	
	@Autowired
	private JurisdictionTaxrateService jurisdictionTaxrateService;
	
	@Autowired
	private TaxCodeBackingBean taxCodeBackingBean;
	
	@Autowired
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	
	@Autowired
	private JurisdictionService jurisdictionService;

	private ApplicationContext context = null;
	
	private TaxCodeCodeHandler filterTaxCodeHandler = new TaxCodeCodeHandler("trans_view");
	
	//cannot autowire due to circular dependency
	private LocationMatrixBackingBean locationMatrixBackingBean;
	private TaxMatrixViewBean taxMatrixViewBean;
	
	private TaxAllocationMatrixViewBean taxAllocationMatrixViewBean;
	
	private MatrixCommonBean matrixCommonBean;
	private PurchaseTransaction selectedTransaction;
	private PurchaseTransaction selectedTransactionTemp;
	private String okAction;
	private HtmlPanelGrid taxDriverViewPanel;
	private HtmlPanelGrid locationDriverViewPanel;
	
	private String selectedDisplay = DEFAULT_DISPLAY;
	private String filterFormState = "open";
	private TogglePanelController togglePanelController = null;
	private ProcruleService procruleService;
	private List<ProruleAudit> listviewResults=null;
	private Jurisdiction currentJurisdiction;
	private TaxMatrixViewBean taxMatrixBean;
	
	private AllocationMatrixBackingBean allocationMatrixBackingBean;
	
	private PurchaseTransactionLog selectedTransactionLog;
	
	private SitusMatrixService situsMatrixService;
	
	private VendorNexusDetailService vendorNexusDetailService;
	
	private SitusMatrix situsMatrix;
	
	private VendorNexusDtl vendorNexusDtl;
	
	private TaxCodeDetail taxCodeDetail;
	
	EntityItem aEntityItem;
	
	@Autowired
	private VendorBackingBean vendorBackingBean = null;
	
	private TransactionDetailBean transactionDetailBean = null;
	
	private String screenType = null;
	
	private SearchJurisdictionHandler txnLocnShiptoJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnShipfromJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnOrdrorgnJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnOrdracptJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnFirstuseJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnBilltoJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnLocnTtlxfrJurisdictionHandler = new SearchJurisdictionHandler();
	
	private SearchJurisdictionHandler txnVendorJurisdictionHandler = new SearchJurisdictionHandler();
	private VendorLookupHandler txnVendorLookupJurisdictionHandler = new VendorLookupHandler();

	private PurchaseTransaction updatedTransaction;
	
	private TransactionDetailService transactionDetailService;
	
	private ValidTransactionDates validTransactionDates = new ValidTransactionDates();
	
	JurisdictionTaxrate jurisdictionTaxrate;

	@Autowired
	private VendorItemService vendorItemService;
	
	public ValidTransactionDates getValidTransactionDates() {
		return validTransactionDates;
	}

	public void setValidTransactionDates(ValidTransactionDates validTransactionDates) {
		this.validTransactionDates = validTransactionDates;
	}

	public JurisdictionTaxrate getJurisdictionTaxrate() {
		return jurisdictionTaxrate;
	}

	public void setJurisdictionTaxrate(JurisdictionTaxrate jurisdictionTaxrate) {
		this.jurisdictionTaxrate = jurisdictionTaxrate;
	}

	public VendorItemService getVendorItemService() {
		return vendorItemService;
	}

	public void setVendorItemService(VendorItemService vendorItemService) {
		this.vendorItemService = vendorItemService;
	}

	public VendorLookupHandler getTxnVendorLookupJurisdictionHandler() {
		return txnVendorLookupJurisdictionHandler;
	}

	public void setTxnVendorLookupJurisdictionHandler(
			VendorLookupHandler txnVendorLookupJurisdictionHandler) {
		this.txnVendorLookupJurisdictionHandler = txnVendorLookupJurisdictionHandler;
	}

	public PurchaseTransaction getUpdatedTransaction() {
		return updatedTransaction;
	}

	public void setUpdatedTransaction(PurchaseTransaction updatedTransaction) {
		this.updatedTransaction = updatedTransaction;
	}

	public TransactionDetailService getTransactionDetailService() {
		return transactionDetailService;
	}

	public void setTransactionDetailService(
			TransactionDetailService transactionDetailService) {
		this.transactionDetailService = transactionDetailService;
	}

	public void setTransactionDetailBean(TransactionDetailBean transactionDetailBean){
		this.transactionDetailBean = transactionDetailBean;
	}

	public PurchaseTransaction getSelectedTransactionTemp() {
		return selectedTransactionTemp;
	}

	public void setSelectedTransactionTemp(
			PurchaseTransaction selectedTransactionTemp) {
		this.selectedTransactionTemp = selectedTransactionTemp;
	}
	
	public SearchJurisdictionHandler getTxnVendorJurisdictionHandler() {
		return txnVendorJurisdictionHandler;
	}

	public void setTxnVendorJurisdictionHandler(
			SearchJurisdictionHandler txnVendorJurisdictionHandler) {
		this.txnVendorJurisdictionHandler = txnVendorJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnShiptoJurisdictionHandler() {
		return txnLocnShiptoJurisdictionHandler;
	}

	public void setTxnLocnShiptoJurisdictionHandler(
			SearchJurisdictionHandler txnLocnShiptoJurisdictionHandler) {
		this.txnLocnShiptoJurisdictionHandler = txnLocnShiptoJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnShipfromJurisdictionHandler() {
		return txnLocnShipfromJurisdictionHandler;
	}

	public void setTxnLocnShipfromJurisdictionHandler(
			SearchJurisdictionHandler txnLocnShipfromJurisdictionHandler) {
		this.txnLocnShipfromJurisdictionHandler = txnLocnShipfromJurisdictionHandler;
	}
	

	public SearchJurisdictionHandler getTxnLocnOrdrorgnJurisdictionHandler() {
		return txnLocnOrdrorgnJurisdictionHandler;
	}

	public void setTxnLocnOrdrorgnJurisdictionHandler(
			SearchJurisdictionHandler txnLocnOrdrorgnJurisdictionHandler) {
		this.txnLocnOrdrorgnJurisdictionHandler = txnLocnOrdrorgnJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnOrdracptJurisdictionHandler() {
		return txnLocnOrdracptJurisdictionHandler;
	}

	public void setTxnLocnOrdracptJurisdictionHandler(
			SearchJurisdictionHandler txnLocnOrdracptJurisdictionHandler) {
		this.txnLocnOrdracptJurisdictionHandler = txnLocnOrdracptJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnFirstuseJurisdictionHandler() {
		return txnLocnFirstuseJurisdictionHandler;
	}

	public void setTxnLocnFirstuseJurisdictionHandler(
			SearchJurisdictionHandler txnLocnFirstuseJurisdictionHandler) {
		this.txnLocnFirstuseJurisdictionHandler = txnLocnFirstuseJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnBilltoJurisdictionHandler() {
		return txnLocnBilltoJurisdictionHandler;
	}

	public void setTxnLocnBilltoJurisdictionHandler(
			SearchJurisdictionHandler txnLocnBilltoJurisdictionHandler) {
		this.txnLocnBilltoJurisdictionHandler = txnLocnBilltoJurisdictionHandler;
	}

	public SearchJurisdictionHandler getTxnLocnTtlxfrJurisdictionHandler() {
		return txnLocnTtlxfrJurisdictionHandler;
	}

	public void setTxnLocnTtlxfrJurisdictionHandler(
			SearchJurisdictionHandler txnLocnTtlxfrJurisdictionHandler) {
		this.txnLocnTtlxfrJurisdictionHandler = txnLocnTtlxfrJurisdictionHandler;
	}
	
	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public EntityItem getaEntityItem() {
		return aEntityItem;
	}

	public void setaEntityItem(EntityItem aEntityItem) {
		this.aEntityItem = aEntityItem;
	}

	public TaxCodeDetail getTaxCodeDetail() {
		return taxCodeDetail;
	}

	public void setTaxCodeDetail(TaxCodeDetail taxCodeDetail) {
		this.taxCodeDetail = taxCodeDetail;
	}

	public VendorNexusDetailService getVendorNexusDetailService() {
		return vendorNexusDetailService;
	}

	public void setVendorNexusDetailService(
			VendorNexusDetailService vendorNexusDetailService) {
		this.vendorNexusDetailService = vendorNexusDetailService;
	}

	public VendorNexusDtl getVendorNexusDtl() {
		return vendorNexusDtl;
	}

	public void setVendorNexusDtl(VendorNexusDtl vendorNexusDtl) {
		this.vendorNexusDtl = vendorNexusDtl;
	}

	public SitusMatrix getSitusMatrix() {
		return situsMatrix;
	}

	public void setSitusMatrix(SitusMatrix situsMatrix) {
		this.situsMatrix = situsMatrix;
	}

	public SitusMatrixService getSitusMatrixService() {
		return situsMatrixService;
	}

	public void setSitusMatrixService(SitusMatrixService situsMatrixService) {
		this.situsMatrixService = situsMatrixService;
	}

	public PurchaseTransactionLog getSelectedTransactionLog() {
		return selectedTransactionLog;
	}

	public void setSelectedTransactionLog(
			PurchaseTransactionLog selectedTransactionLog) {
		this.selectedTransactionLog = selectedTransactionLog;
	}

	public AllocationMatrixBackingBean getAllocationMatrixBackingBean() {
		return allocationMatrixBackingBean;
	}

	public void setAllocationMatrixBackingBean(
			AllocationMatrixBackingBean allocationMatrixBackingBean) {
		this.allocationMatrixBackingBean = allocationMatrixBackingBean;
	}

	public TaxMatrixViewBean getTaxMatrixBean() {
		return taxMatrixBean;
	}

	public void setTaxMatrixBean(TaxMatrixViewBean taxMatrixBean) {
		this.taxMatrixBean = taxMatrixBean;
	}

	public TaxAllocationMatrixViewBean getTaxAllocationMatrixViewBean() {
		return taxAllocationMatrixViewBean;
	}

	public void setTaxAllocationMatrixViewBean(
			TaxAllocationMatrixViewBean taxAllocationMatrixViewBean) {
		this.taxAllocationMatrixViewBean = taxAllocationMatrixViewBean;
	}
	
	public VendorBackingBean getVendorBackingBean() {
		return vendorBackingBean;
	}

	public void setVendorBackingBean(VendorBackingBean vendorBackingBean) {
		this.vendorBackingBean = vendorBackingBean;
	}



	@Autowired
	private EntityItemService entityItemService;
	
	//cannot autowire due to circular dependency
	private EntityBackingBean entityBackingBean;
	public HtmlPanelGrid getTaxDriverViewPanel() {
		if (taxDriverViewPanel == null) {
			taxDriverViewPanel = MatrixCommonBean.createDriverPanel(
					TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"taxDriverViewPanel", 
					"transactionViewBean", "selectedTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		matrixCommonBean.prepareTaxDriverPanel(taxDriverViewPanel);
		return taxDriverViewPanel;
	}
	
	

	public void setTaxDriverViewPanelFromTransaction(HtmlPanelGrid taxDriverViewPanel) {
		this.taxDriverViewPanel = taxDriverViewPanel;
	}
	
	public void setTaxDriverViewPanel(HtmlPanelGrid taxDriverViewPanel) {
		this.taxDriverViewPanel = taxDriverViewPanel;
	}

	public HtmlPanelGrid getLocationDriverViewPanel() {
		if (locationDriverViewPanel == null) {
			locationDriverViewPanel = MatrixCommonBean.createDriverPanel(
					LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
					matrixCommonBean.getEntityItemDTO(),
					"locationDriverViewPanel", 
					"transactionViewBean", "selectedTransaction", "matrixCommonBean", 
					true, false, true, false, false);
		}
		
		return locationDriverViewPanel;
	}
	
	public HtmlPanelGrid getLocationDriverViewPanelFromTransaction() {
		String screenType = getScreenType();
		String screen = screenType + "locationDriverViewPanelFromTransaction";
	
		locationDriverViewPanel = MatrixCommonBean.createDriverPanel(
				LocationMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
				matrixCommonBean.getEntityItemDTO(),
				screen, "transactionDetailBean", "edit".equals(getScreenType()) ? "updatedTransaction":"selectedTransaction",
				"matrixCommonBean", true, false, true, false, false);
	
	return locationDriverViewPanel;
	}
	
	public HtmlPanelGrid getTaxDriverViewPanelFromTransaction() {
		String screenType = getScreenType();
		String screen = screenType + "taxDriverViewPanelFromTransaction";

		taxDriverViewPanel = MatrixCommonBean.createDriverPanel(
				TaxMatrix.DRIVER_CODE, matrixCommonBean.getCacheManager(),
				matrixCommonBean.getEntityItemDTO(),
				screen, "transactionDetailBean", "edit".equals(getScreenType()) ? "updatedTransaction":"selectedTransaction",
				"matrixCommonBean",true, false, true, false, false);
		
		matrixCommonBean.prepareTaxDriverPanel(taxDriverViewPanel);
		return taxDriverViewPanel;
	}
	
	public void setLocationDriverViewPanelFromTransaction(HtmlPanelGrid locationDriverViewPanel) {
		this.locationDriverViewPanel = locationDriverViewPanel;
	}

	public void setLocationDriverViewPanel(HtmlPanelGrid locationDriverViewPanel) {
		this.locationDriverViewPanel = locationDriverViewPanel;
	}
	
	

	public MatrixCommonBean getMatrixCommonBean() {
		return matrixCommonBean;
	}

	public void setMatrixCommonBean(MatrixCommonBean matrixCommonBean) {
		
		this.matrixCommonBean = matrixCommonBean;
		
		filterTaxCodeHandler.setTaxCodeDetailService(matrixCommonBean.getTaxCodeDetailService());
		filterTaxCodeHandler.setCacheManager(matrixCommonBean.getCacheManager());
		
		//0004440
		filterTaxCodeHandler.setTaxCodeBackingBean(taxCodeBackingBean);
		filterTaxCodeHandler.setReturnRuleUrl("trans_view");
		
		setTxnVendorhandler(txnVendorLookupJurisdictionHandler);
	}
	
	public void setTxnLocnHandler(SearchJurisdictionHandler handler) {
		
		handler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		handler.setCacheManager(matrixCommonBean.getCacheManager());
		handler.setTaxJurisdictionBean(taxJurisdictionBean);
		handler.setCallbackScreen("trans_view");
		handler.setTxnLocnEditScreen(true);
	}
	
	public void setTxnVendorhandler(VendorLookupHandler handler) {
			
		handler.setJurisdictionService(matrixCommonBean.getJurisdictionService());
		handler.setCacheManager(matrixCommonBean.getCacheManager());
		handler.setTaxJurisdictionBean(taxJurisdictionBean);
		handler.setVendorItemService(vendorItemService);
		handler.setVendorBackingBean(vendorBackingBean);
		handler.setCallbackScreen("trans_view");
	}
	
	public TaxCodeCodeHandler getFilterTaxCodeHandler() {
		return filterTaxCodeHandler;
	}

	public PurchaseTransaction getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public Jurisdiction getCurrentJurisdiction() {
		return currentJurisdiction;
	}
	
	public void setSelectedTransaction(PurchaseTransaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
		
		filterTaxCodeHandler.setTaxcodeCode(selectedTransaction.getTaxcodeCode());
	}
	
	public String getOkAction() {
		return okAction;
	}
	
	public void setOkAction(String okAction) {
		this.okAction = okAction;
	}
	
	public String okAction() {
		this.selectedDisplay = DEFAULT_DISPLAY;
		return okAction;
	}
	
	public TaxCodeDetail getStateTaxCodeDetail() {
		TaxCodeDetail taxCodeDetail = null;
		
		if(this.selectedTransaction != null && this.selectedTransaction.getStateTaxcodeDetailId() != null) {
			taxCodeDetail = taxCodeDetailService.findById(this.selectedTransaction.getStateTaxcodeDetailId());
		}
		
		if(taxCodeDetail == null) {
			taxCodeDetail = new TaxCodeDetail();
		}
		
		return taxCodeDetail;
	}
	
	public TaxCodeDetail getCountyTaxCodeDetail() {
		TaxCodeDetail taxCodeDetail = null;
		
		if(this.selectedTransaction != null && this.selectedTransaction.getCountyTaxcodeDetailId() != null) {
			taxCodeDetail = taxCodeDetailService.findById(this.selectedTransaction.getCountyTaxcodeDetailId());
		}
		
		if(taxCodeDetail == null) {
			taxCodeDetail = new TaxCodeDetail();
		}
		
		return taxCodeDetail;
	}
	
	public TaxCodeDetail getCityTaxCodeDetail() {
		TaxCodeDetail taxCodeDetail = null;
		
		if(this.selectedTransaction != null && this.selectedTransaction.getCityTaxcodeDetailId() != null) {
			taxCodeDetail = taxCodeDetailService.findById(this.selectedTransaction.getCityTaxcodeDetailId());
		}
		
		if(taxCodeDetail == null) {
			taxCodeDetail = new TaxCodeDetail();
		}
		
		return taxCodeDetail;
	}
	
	public String viewStateRuleAction() {
		return taxCodeBackingBean.viewRule(this.selectedTransaction, "state", "trans_view");
	}
	
	public String viewCountyRuleAction() {
		return taxCodeBackingBean.viewRule(this.selectedTransaction, "county", "trans_view");
	}
	
	public String viewCityRuleAction() {
		return taxCodeBackingBean.viewRule(this.selectedTransaction, "city", "trans_view");
	}
	
	public String viewTaxRateAction(){
		Jurisdiction jurisdiction = null;
		
		JurisdictionTaxrate jurisdictionTaxrate = new JurisdictionTaxrate();
		if(this.selectedTransaction.getStateTaxrateId() != null) {
			jurisdictionTaxrate = jurisdictionTaxrateService.findByIdInitialized(this.selectedTransaction.getStateTaxrateId());
			if(jurisdictionTaxrate != null) {
				jurisdiction = jurisdictionTaxrate.getJurisdiction();
			}
		}
		
		return taxJurisdictionBean.viewFromTransaction(jurisdiction, jurisdictionTaxrate);
	}
	
	public String viewLocationMatrixAction(){
		if(this.locationMatrixBackingBean == null) {
			initializeLocationMatrixBackingBean();
		}
		String locnMatrixId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("locnMatrixId");
		if(locnMatrixId == null){
			displayErrorMessage("Location Matrix ID not found");
			return null;
		}
		else {
			return locationMatrixBackingBean.viewFromTransaction(Long.valueOf(locnMatrixId), "loc_matrix_view");
		}
	}
	
	public String viewTaxMatrixAction(){
		if(this.taxMatrixViewBean == null) {
			initializeTaxMatrixViewBean();
		}
		
		return taxMatrixViewBean.viewFromTransaction(this.selectedTransaction.getTaxMatrixId(), "tax_matrix_view");
	}
	
	public String viewTaxCodeAction(){
		TaxCodePK id = new TaxCodePK();
		id.setTaxcodeCode(this.selectedTransaction.getTaxcodeCode());
		return taxCodeBackingBean.viewFromTransaction(id, "trans_view");
	}

	private void initializeTaxMatrixViewBean() {
		try {
			this.taxMatrixViewBean = context.getBean(TaxMatrixViewBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private void initializeLocationMatrixBackingBean() {
		try {
			this.locationMatrixBackingBean = context.getBean(LocationMatrixBackingBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;
	}

	public String getSelectedDisplay() {
		return selectedDisplay;
	}

	public void setSelectedDisplay(String selectedDisplay) {
		this.selectedDisplay = selectedDisplay;
	}
	
	public String displaySelectionChanged(ValueChangeEvent e) {
		this.selectedDisplay = (String)e.getNewValue();
		return "trans_view";
	}
	
	public void setTxnVendorDetails() {
		if(updatedTransaction !=null) {
			txnVendorLookupJurisdictionHandler.setId(String.valueOf(this.updatedTransaction.getVendorId()));
			txnVendorLookupJurisdictionHandler.setCode(this.updatedTransaction.getVendorCode());
			txnVendorLookupJurisdictionHandler.setName(this.updatedTransaction.getVendorName());
			txnVendorLookupJurisdictionHandler.setAddressline1(this.updatedTransaction.getVendorAddressLine1());
			txnVendorLookupJurisdictionHandler.setAddressline2(this.updatedTransaction.getVendorAddressLine2());
			txnVendorLookupJurisdictionHandler.setType(this.updatedTransaction.getVendorType());
			txnVendorLookupJurisdictionHandler.setTypename(this.updatedTransaction.getVendorTypeName());
			txnVendorLookupJurisdictionHandler.setCountry(this.updatedTransaction.getVendorAddressCountry());
			txnVendorLookupJurisdictionHandler.setState(this.updatedTransaction.getVendorAddressState());
			txnVendorLookupJurisdictionHandler.setCounty(this.updatedTransaction.getVendorAddressCounty());
			txnVendorLookupJurisdictionHandler.setCity(this.updatedTransaction.getVendorAddressCity());
			txnVendorLookupJurisdictionHandler.setZip(this.updatedTransaction.getVendorAddressZip());
		}
	}
	
	public void setTxnLocnJurisdiction() {
		setShiptoJurisdiction();
	    setShipfromJurisdiction(); 
	    setOrdOrgnJurisdiction();
	    setOrdracptJurisdiction();
	    setFirstuseJurisdiction();
	    setBilltoJurisdiction();
	    setTtlxfrJurisdiction();
		}
	
	public void setShiptoJurisdiction() {
		
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getShiptoJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnShiptoJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnShiptoJurisdictionHandler.setEntityCode(this.updatedTransaction.getShiptoEntityCode());
			txnLocnShiptoJurisdictionHandler.setGeocode(this.updatedTransaction.getShiptoGeocode());
			txnLocnShiptoJurisdictionHandler.setCountry(this.updatedTransaction.getShiptoCountryCode());
			txnLocnShiptoJurisdictionHandler.setState(this.updatedTransaction.getShiptoStateCode());
			txnLocnShiptoJurisdictionHandler.setCounty(this.updatedTransaction.getShiptoCounty());
			txnLocnShiptoJurisdictionHandler.setCity(this.updatedTransaction.getShiptoCity());
			txnLocnShiptoJurisdictionHandler.setZip(this.updatedTransaction.getShiptoZip());
			txnLocnShiptoJurisdictionHandler.setZipPlus4(this.updatedTransaction.getShiptoZipplus4());
			txnLocnShiptoJurisdictionHandler.setStj1Name(this.updatedTransaction.getShiptoStj1Name());
			txnLocnShiptoJurisdictionHandler.setStj2Name(this.updatedTransaction.getShiptoStj2Name());
			txnLocnShiptoJurisdictionHandler.setStj3Name(this.updatedTransaction.getShiptoStj3Name());
			txnLocnShiptoJurisdictionHandler.setStj4Name(this.updatedTransaction.getShiptoStj4Name());
			txnLocnShiptoJurisdictionHandler.setStj5Name(this.updatedTransaction.getShiptoStj5Name());
		}
		
	}
	
	public void setShipfromJurisdiction() {
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getShipfromJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnShipfromJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnShipfromJurisdictionHandler.setEntityCode(this.updatedTransaction.getShipfromEntityCode());
			txnLocnShipfromJurisdictionHandler.setGeocode(this.updatedTransaction.getShipfromGeocode());
			txnLocnShipfromJurisdictionHandler.setCountry(this.updatedTransaction.getShipfromCountryCode());
			txnLocnShipfromJurisdictionHandler.setState(this.updatedTransaction.getShipfromStateCode());
			txnLocnShipfromJurisdictionHandler.setCounty(this.updatedTransaction.getShipfromCounty());
			txnLocnShipfromJurisdictionHandler.setCity(this.updatedTransaction.getShipfromCity());
			txnLocnShipfromJurisdictionHandler.setZip(this.updatedTransaction.getShipfromZip());
			txnLocnShipfromJurisdictionHandler.setZipPlus4(this.updatedTransaction.getShipfromZipplus4());
			txnLocnShipfromJurisdictionHandler.setStj1Name(this.updatedTransaction.getShipfromStj1Name());
			txnLocnShipfromJurisdictionHandler.setStj2Name(this.updatedTransaction.getShipfromStj2Name());
			txnLocnShipfromJurisdictionHandler.setStj3Name(this.updatedTransaction.getShipfromStj3Name());
			txnLocnShipfromJurisdictionHandler.setStj4Name(this.updatedTransaction.getShipfromStj4Name());
			txnLocnShipfromJurisdictionHandler.setStj5Name(this.updatedTransaction.getShipfromStj5Name());
		}
		
	}
	
	public void setOrdOrgnJurisdiction() {
		
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getOrdrorgnJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnOrdrorgnJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnOrdrorgnJurisdictionHandler.setEntityCode(this.updatedTransaction.getOrdrorgnEntityCode());
			txnLocnOrdrorgnJurisdictionHandler.setGeocode(this.updatedTransaction.getOrdrorgnGeocode());
			txnLocnOrdrorgnJurisdictionHandler.setCountry(this.updatedTransaction.getOrdrorgnCountryCode());
			txnLocnOrdrorgnJurisdictionHandler.setState(this.updatedTransaction.getOrdrorgnStateCode());
			txnLocnOrdrorgnJurisdictionHandler.setCounty(this.updatedTransaction.getOrdrorgnCounty());
			txnLocnOrdrorgnJurisdictionHandler.setCity(this.updatedTransaction.getOrdrorgnCity());
			txnLocnOrdrorgnJurisdictionHandler.setZip(this.updatedTransaction.getOrdrorgnZip());
			txnLocnOrdrorgnJurisdictionHandler.setZipPlus4(this.updatedTransaction.getOrdrorgnZipplus4());
			txnLocnOrdrorgnJurisdictionHandler.setStj1Name(this.updatedTransaction.getOrdrorgnStj1Name());
			txnLocnOrdrorgnJurisdictionHandler.setStj2Name(this.updatedTransaction.getOrdrorgnStj2Name());
			txnLocnOrdrorgnJurisdictionHandler.setStj3Name(this.updatedTransaction.getOrdrorgnStj3Name());
			txnLocnOrdrorgnJurisdictionHandler.setStj4Name(this.updatedTransaction.getOrdrorgnStj4Name());
			txnLocnOrdrorgnJurisdictionHandler.setStj5Name(this.updatedTransaction.getOrdrorgnStj5Name());
		}
		
	}
	
	public void setOrdracptJurisdiction() {
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getOrdracptJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnOrdracptJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnOrdracptJurisdictionHandler.setEntityCode(this.updatedTransaction.getOrdracptEntityCode());
			txnLocnOrdracptJurisdictionHandler.setGeocode(this.updatedTransaction.getOrdracptGeocode());
			txnLocnOrdracptJurisdictionHandler.setCountry(this.updatedTransaction.getOrdracptCountryCode());
			txnLocnOrdracptJurisdictionHandler.setState(this.updatedTransaction.getOrdracptStateCode());
			txnLocnOrdracptJurisdictionHandler.setCounty(this.updatedTransaction.getOrdracptCounty());
			txnLocnOrdracptJurisdictionHandler.setCity(this.updatedTransaction.getOrdracptCity());
			txnLocnOrdracptJurisdictionHandler.setZip(this.updatedTransaction.getOrdracptZip());
			txnLocnOrdracptJurisdictionHandler.setZipPlus4(this.updatedTransaction.getOrdracptZipplus4());
			txnLocnOrdracptJurisdictionHandler.setStj1Name(this.updatedTransaction.getOrdracptStj1Name());
			txnLocnOrdracptJurisdictionHandler.setStj2Name(this.updatedTransaction.getOrdracptStj2Name());
			txnLocnOrdracptJurisdictionHandler.setStj3Name(this.updatedTransaction.getOrdracptStj3Name());
			txnLocnOrdracptJurisdictionHandler.setStj4Name(this.updatedTransaction.getOrdracptStj4Name());
			txnLocnOrdracptJurisdictionHandler.setStj5Name(this.updatedTransaction.getOrdracptStj5Name());
		}
	
	}
	
	public void setFirstuseJurisdiction() {
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getFirstuseJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnFirstuseJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnFirstuseJurisdictionHandler.setEntityCode(this.updatedTransaction.getFirstuseEntityCode());
			txnLocnFirstuseJurisdictionHandler.setGeocode(this.updatedTransaction.getFirstuseGeocode());
			txnLocnFirstuseJurisdictionHandler.setCountry(this.updatedTransaction.getFirstuseCountryCode());
			txnLocnFirstuseJurisdictionHandler.setState(this.updatedTransaction.getFirstuseStateCode());
			txnLocnFirstuseJurisdictionHandler.setCounty(this.updatedTransaction.getFirstuseCounty());
			txnLocnFirstuseJurisdictionHandler.setCity(this.updatedTransaction.getFirstuseCity());
			txnLocnFirstuseJurisdictionHandler.setZip(this.updatedTransaction.getFirstuseZip());
			txnLocnFirstuseJurisdictionHandler.setZipPlus4(this.updatedTransaction.getFirstuseZipplus4());
			txnLocnFirstuseJurisdictionHandler.setStj1Name(this.updatedTransaction.getFirstuseStj1Name());
			txnLocnFirstuseJurisdictionHandler.setStj2Name(this.updatedTransaction.getFirstuseStj2Name());
			txnLocnFirstuseJurisdictionHandler.setStj3Name(this.updatedTransaction.getFirstuseStj3Name());
			txnLocnFirstuseJurisdictionHandler.setStj4Name(this.updatedTransaction.getFirstuseStj4Name());
			txnLocnFirstuseJurisdictionHandler.setStj5Name(this.updatedTransaction.getFirstuseStj5Name());
		}
		
	}
	
	public void setBilltoJurisdiction() {
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getBilltoJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnBilltoJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnBilltoJurisdictionHandler.setEntityCode(this.updatedTransaction.getBilltoEntityCode());
			txnLocnBilltoJurisdictionHandler.setGeocode(this.updatedTransaction.getBilltoGeocode());
			txnLocnBilltoJurisdictionHandler.setCountry(this.updatedTransaction.getBilltoCountryCode());
			txnLocnBilltoJurisdictionHandler.setState(this.updatedTransaction.getBilltoStateCode());
			txnLocnBilltoJurisdictionHandler.setCounty(this.updatedTransaction.getBilltoCounty());
			txnLocnBilltoJurisdictionHandler.setCity(this.updatedTransaction.getBilltoCity());
			txnLocnBilltoJurisdictionHandler.setZip(this.updatedTransaction.getBilltoZip());
			txnLocnBilltoJurisdictionHandler.setZipPlus4(this.updatedTransaction.getBilltoZipplus4());
			txnLocnBilltoJurisdictionHandler.setStj1Name(this.updatedTransaction.getBilltoStj1Name());
			txnLocnBilltoJurisdictionHandler.setStj2Name(this.updatedTransaction.getBilltoStj2Name());
			txnLocnBilltoJurisdictionHandler.setStj3Name(this.updatedTransaction.getBilltoStj3Name());
			txnLocnBilltoJurisdictionHandler.setStj4Name(this.updatedTransaction.getBilltoStj4Name());
			txnLocnBilltoJurisdictionHandler.setStj5Name(this.updatedTransaction.getBilltoStj5Name());
		}
	}
	
	public void setTtlxfrJurisdiction() {
		if(updatedTransaction !=null){
			Long jurisdictionId = this.updatedTransaction.getTtlxfrJurisdictionId();
			String jurisdictionIdString = ((jurisdictionId == null) ? null : jurisdictionId.toString());
			txnLocnTtlxfrJurisdictionHandler.setJurisdictionId(jurisdictionIdString);
			txnLocnTtlxfrJurisdictionHandler.setEntityCode(this.updatedTransaction.getTtlxfrEntityCode());
			txnLocnTtlxfrJurisdictionHandler.setGeocode(this.updatedTransaction.getTtlxfrGeocode());
			txnLocnTtlxfrJurisdictionHandler.setCountry(this.updatedTransaction.getTtlxfrCountryCode());
			txnLocnTtlxfrJurisdictionHandler.setState(this.updatedTransaction.getTtlxfrStateCode());
			txnLocnTtlxfrJurisdictionHandler.setCounty(this.updatedTransaction.getTtlxfrCounty());
			txnLocnTtlxfrJurisdictionHandler.setCity(this.updatedTransaction.getTtlxfrCity());
			txnLocnTtlxfrJurisdictionHandler.setZip(this.updatedTransaction.getTtlxfrZip());
			txnLocnTtlxfrJurisdictionHandler.setZipPlus4(this.updatedTransaction.getTtlxfrZipplus4());
			txnLocnTtlxfrJurisdictionHandler.setStj1Name(this.updatedTransaction.getTtlxfrStj1Name());
			txnLocnTtlxfrJurisdictionHandler.setStj2Name(this.updatedTransaction.getTtlxfrStj2Name());
			txnLocnTtlxfrJurisdictionHandler.setStj3Name(this.updatedTransaction.getTtlxfrStj3Name());
			txnLocnTtlxfrJurisdictionHandler.setStj4Name(this.updatedTransaction.getTtlxfrStj4Name());
			txnLocnTtlxfrJurisdictionHandler.setStj5Name(this.updatedTransaction.getTtlxfrStj5Name());
		}
	}
	
	 //fixed for issue 0004627
		public String getFilterFormState() {
			return filterFormState;
		}
		
		public void setFilterFormState(String filterFormState) {
			this.filterFormState = filterFormState;
		}
		public void filterFormStateChanged(ValueChangeEvent e) {
			String v = (String) e.getNewValue();
			this.filterFormState = v;
		}

	public TogglePanelController getTogglePanelController() {
		if (togglePanelController == null) {
			togglePanelController = new TogglePanelController();
			
			togglePanelController.addTogglePanel("jurisdictionInformation", true);
			togglePanelController.addTogglePanel("taxCodeInformation", false);
			togglePanelController.addTogglePanel("taxRateInformation", false);
			togglePanelController.addTogglePanel("calculatedAmounts", false);
			
			togglePanelController.addTogglePanel("glInformation", true);
			togglePanelController.addTogglePanel("inventoryInformation", false);
			togglePanelController.addTogglePanel("paymentInformation", false);
			
			togglePanelController.addTogglePanel("shipToInformation", true);
			togglePanelController.addTogglePanel("shipFromInformation", false);
			togglePanelController.addTogglePanel("orderOriginInformation", false);
			togglePanelController.addTogglePanel("orderAcceptanceInformation", false);
			togglePanelController.addTogglePanel("firstUseInformation", false);
			togglePanelController.addTogglePanel("billToInformation", false);
			togglePanelController.addTogglePanel("titleTransferInformation", false);
			
			togglePanelController.addTogglePanel("transactionInformation", true);
			togglePanelController.addTogglePanel("lastUpdateInformation", false);
				togglePanelController.addTogglePanel("glextractInformation", false);
			togglePanelController.addTogglePanel("batchInformation", false);
			
			togglePanelController.addTogglePanel("invoiceInformation", true);
			togglePanelController.addTogglePanel("purchaseOrderInformation", false);
			togglePanelController.addTogglePanel("workOrderInformation", false);
			togglePanelController.addTogglePanel("projectInformation", false);
			
			togglePanelController.addTogglePanel("userTextInformation", true);
			togglePanelController.addTogglePanel("userNumberInformation", false);
			togglePanelController.addTogglePanel("userDateInformation", false);
			
			togglePanelController.addTogglePanel("processingResults", true);
			togglePanelController.addTogglePanel("processRulesResults", false);
			
			togglePanelController.addTogglePanel("vendorInformation", true);

			togglePanelController.addTogglePanel("requiredInfo", true);
			togglePanelController.addTogglePanel("locationDrivers", false);
			togglePanelController.addTogglePanel("taxDrivers", false);
			
			togglePanelController.addTogglePanel("allocationInformation", false);
			togglePanelController.addTogglePanel("locationInformation", false);
			togglePanelController.addTogglePanel("jurisdictionDetails", false);
			
			togglePanelController.addTogglePanel("taxCodeInformation", false);
			togglePanelController.addTogglePanel("taxRateInformation", false);
			togglePanelController.addTogglePanel("calculatedAmounts", false);
			togglePanelController.addTogglePanel("logValuesInformation", true);
			togglePanelController.addTogglePanel("generalInformation", true);
		}
		return togglePanelController;
	}

	public void setTogglePanelController(TogglePanelController togglePanelController) {
		this.togglePanelController = togglePanelController;
	}
	
	public String viewCpFilingEntityCodeAction(){
		if(this.selectedTransaction.getCpFilingEntityCode()!=null){
			if(this.entityBackingBean==null){
				initializeEntityBackingBean();
			}
			EntityItem aEntityItem = this.entityItemService.findByCode(this.selectedTransaction.getCpFilingEntityCode());
			if(aEntityItem==null){
				displayErrorMessage("ControlPoint Filing Entity code not found");
				return "";
			}
			return entityBackingBean.viewEntityAction(Long.valueOf(aEntityItem.getEntityId()),"trans_view");
		}
		return null;
	}
	
	public String viewEntityCodeAction(){
		if(this.entityBackingBean==null){
			initializeEntityBackingBean();
		}
		String entityId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("entityId");
		//If no parameter passed in xhtml file.
		if(entityId == null){
			displayErrorMessage("Entity ID not found");
			return null;
		}
		else {
			return entityBackingBean.viewEntityAction(Long.valueOf(entityId),"trans_view");
		}
	}
	
	public String viewInheritEntityCodeAction(){
		if(this.entityBackingBean==null){
			initializeEntityBackingBean();
		}
			String nextPage = entityBackingBean.viewEntityAction(this.selectedTransaction.getUnlockEntityId(),"trans_view");
			if(nextPage == null) {
				displayErrorMessage("Inherit.Entity Code not found"); 
			}
			return nextPage;
	}
	
	public String viewJurisdiction() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		String jurisdId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("jurisdictiondId");
		return taxJurisdictionBean.viewJurisdiction(Long.valueOf(jurisdId),fromview);
    }
	
	public String viewJurisdictionTaxRate() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("command");
		String jurisdictionTaxRateId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("jurisdictionTaxRateId");
		return taxJurisdictionBean.viewJurisdictionTaxRate(Long.valueOf(jurisdictionTaxRateId),fromview);
    }

	private void initializeEntityBackingBean() {
		try {
			this.entityBackingBean = context.getBean(EntityBackingBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public List<ProruleAudit> getListviewResults() {
		return listviewResults;
	}
	
	public List<ProruleAudit> getViewResults() {
			listviewResults = procruleService.getProcRuleRecords(this.selectedTransaction.getProcruleSessionId());
			return listviewResults;
	}

	public void setListviewResults(List<ProruleAudit> listviewResults) {
		this.listviewResults = listviewResults;
	}

	public ProcruleService getProcruleService() {
		return procruleService;
	}

	public void setProcruleService(ProcruleService procruleService) {
		this.procruleService = procruleService;
	}
	
	public String getCpFilingEntityCode() {
		String code = this.getSelectedTransaction().getCpFilingEntityCode();
		Map<String, String> entityCodes = matrixCommonBean.getCacheManager().getEntityCodeMap();
		String entityName = entityCodes.get(code); 
		if (entityName!=null) {
			return code + " - " + entityName;
		} else
			return code;		
	}
	
	
	public EntityItem getTransEntityCode(Long entityId) {
		if(this.selectedTransaction!=null){
			aEntityItem = this.entityItemService.findById(entityId);
			if(aEntityItem!=null){
				//return aEntityItem.getEntityCode() + " - "  +  aEntityItem.getEntityName();
				return aEntityItem;
			}
		}
		return null; 
	}
	
	public String getInheritEntityCode() {
		if(this.selectedTransaction!=null){
			EntityItem aEntityItem = this.entityItemService.findById(this.selectedTransaction.getUnlockEntityId());
			if(aEntityItem!=null){
				return String.valueOf(aEntityItem.getEntityCode()) + " - "  +  aEntityItem.getEntityName();
			}
		}
		return null;
			
	}
	
	public String processingPageJurisdiction(String geoCode,String City,String county,String stateCode, String zip,String zipplus4,String countryCode) { 
		StringBuffer jurStrBuffer = new StringBuffer();
		if(geoCode !=null && StringUtils.isNotEmpty(geoCode)){
			jurStrBuffer.append(geoCode).append(",");
		}
		if(City !=null && StringUtils.isNotEmpty(City)){
			jurStrBuffer.append(City).append(",");
		}
		if(county !=null && StringUtils.isNotEmpty(county)){
			jurStrBuffer.append(county).append(",");;
		}
		if(stateCode !=null && StringUtils.isNotEmpty(stateCode)){
			jurStrBuffer.append(stateCode).append(",");
		}
		if(zip !=null && StringUtils.isNotEmpty(zip)){
			jurStrBuffer.append(zip).append(",");
		}
		if(zipplus4 !=null && StringUtils.isNotEmpty(zipplus4)){
			jurStrBuffer.append(zipplus4).append(",");
		}
		if(countryCode !=null && StringUtils.isNotEmpty(countryCode)){
			jurStrBuffer.append(countryCode);
		}
		String jurisdiction = jurStrBuffer.toString();
		if(jurisdiction.endsWith(",")){
			return jurisdiction.substring(0, jurisdiction.length()-1);
		}
		return jurStrBuffer.toString();
		
		
	}
	public String getTaxcodeDescription(String taxcodeCode) {
		TaxCodePK id = new TaxCodePK();
		id.setTaxcodeCode(taxcodeCode);
		TaxCode taxCode = taxCodeService.findById(id);
		if(taxCode!=null){
			return taxCode.getDescription();
		}
		return null;
	}
	
	public String viewTaxAllocation() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String taxAllocMatrixId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("taxAllocMatrixId");
		String nextPage =  taxAllocationMatrixViewBean.displayTaxAllocationMatrixView(Long.valueOf(taxAllocMatrixId),fromview);
		if(nextPage == null){
    		displayErrorMessage("Tax allocation Matrix not found"); 
    		return null;
    	}
    	return nextPage;
	}
	
	public String viewLocnMatrix() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String locnMatrixId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("locnMatrixId");
		if(this.locationMatrixBackingBean == null) {
			initializeLocationMatrixBackingBean();
		}
		return locationMatrixBackingBean.viewLocnMatrix(Long.valueOf(locnMatrixId),fromview);
	}
	
	public String viewGSMatrix() {
		
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String gsTaxMatrixId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("gsTaxMatrixId");
		
		if(this.taxMatrixBean == null) {
			initializetaxMatrixBackingBean();
		}
		String nextPage = taxMatrixBean.viewGSMatrix(Long.valueOf(gsTaxMatrixId),fromview);
		if(nextPage == null){
    		displayErrorMessage("Goods & Services Matrix ID not found"); 
    		return null;
    	}
    	return nextPage;
		
		
	}
	
	private void initializetaxMatrixBackingBean() {
		try {
			this.taxMatrixBean = context.getBean(TaxMatrixViewBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private void initializeAllocationMatrixBackingBean() {
		try {
			this.allocationMatrixBackingBean = context.getBean(AllocationMatrixBackingBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public String viewTaxCode() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String taxcodeCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("taxcode");
    	String nextPage =  taxCodeBackingBean.viewTaxCode(taxcodeCode,fromview);
    	if(nextPage == null) {
			displayErrorMessage("TaxCode not found"); 
		}
		return nextPage;
    }
	
	
	public String viewTaxCodeDetails() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String taxcodeDetailId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("taxcodeDetailId");
		return taxCodeBackingBean.viewTaxcodeRule(taxcodeDetailId, fromview);
	}
	
	public String viewJurAllocn() {
		String fromview = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		Long allocationMatrixId;
		if(fromview.equals("viewFromTransaction")){
			allocationMatrixId = this.selectedTransaction.getAllocationMatrixId();
		}else {
			allocationMatrixId = this.selectedTransactionLog.getAllocationMatrixId();
		}
		
		if(this.allocationMatrixBackingBean == null) {
			initializeAllocationMatrixBackingBean();
		}
    	String nextPage = allocationMatrixBackingBean.ViewJurAllocn(allocationMatrixId,fromview);
    	if(nextPage == null){
    		displayErrorMessage("Allocation Matrix not found"); 
    		return null;
    	}
    	return nextPage;
		
    }
	
	public SitusMatrix getSitusMatrixDetails(Long situsMatrixId) {
		situsMatrix = situsMatrixService.findById(situsMatrixId);
		return situsMatrix;
	}
	
	public VendorNexusDtl getVendorNexusDtl(Long vendorNexusDetailId){
		vendorNexusDtl = vendorNexusDetailService.findById(vendorNexusDetailId);
		return vendorNexusDtl;
	}
	
	public TaxCodeDetail getTaxcodeDetails(Long taxcodeDetailId) {
		taxCodeDetail = taxCodeDetailService.findById(taxcodeDetailId);
		return taxCodeDetail;
	}
	
	public String viewVendorDetails() {
		String fromView = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromview");
		String vendorId = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vendorId");
		String vendorCode = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("vendorCode");
		return vendorBackingBean.displayVendorView(fromView,vendorId,vendorCode);
		
	}

	public String updateAction() { 
		
		logger.info("Transaction Edit Screen save action starts");
	    setVendorDetails();
	    System.out.println("Vendor juris details are set ");
	    
	    //Locations Page
	    setTxnLocnShiptoJurisdictionValues();
	    setTxnLocnShipfromJurisdictionValues();
	    setTxnLocnOrdrOrgnJurisdictionValues();
	    setTxnLocnOrdracptJurisdictionValues();
	    setTxnLocnFirstUseJurisdictionValues();
	    setTxnLocnBilltoJurisdictionValues();
	    setTxnLocnTtlxfrJurisdictionValues();
	    
	    logger.info("Location jurisdiction values are set..");
	    
	    transactionDetailService.updateTransaction(updatedTransaction);
	    if(transactionDetailBean!=null){
			transactionDetailBean.listTransactions();
		}
		System.out.println("Save Transaction Ends");
		System.out.println("Set to null?");
		//transactionDetailBean.setTrDetailTable(null);
		String dateValidation = validTransactionDates.validateDates();
		if(dateValidation == null){
			return null;
					
		}
		else{
			return "trans_main";
		}
	}
	
	private void setVendorDetails() {
		try {
		updatedTransaction.setVendorId(Long.valueOf(txnVendorLookupJurisdictionHandler.getId()));	
		updatedTransaction.setVendorCode(txnVendorLookupJurisdictionHandler.getCode());
		updatedTransaction.setVendorName(txnVendorLookupJurisdictionHandler.getName());
		updatedTransaction.setVendorType(txnVendorLookupJurisdictionHandler.getType());
		updatedTransaction.setVendorTypeName(txnVendorLookupJurisdictionHandler.getTypename()); 
		updatedTransaction.setVendorAddressLine1(txnVendorLookupJurisdictionHandler.getAddressline1());
		updatedTransaction.setVendorAddressLine2(txnVendorLookupJurisdictionHandler.getAddressline2()); 
		updatedTransaction.setVendorAddressCountry(txnVendorLookupJurisdictionHandler.getCountry());
	    updatedTransaction.setVendorAddressState(txnVendorLookupJurisdictionHandler.getState());
	    updatedTransaction.setVendorAddressCounty(txnVendorLookupJurisdictionHandler.getCounty());
		updatedTransaction.setVendorAddressCity(txnVendorLookupJurisdictionHandler.getCity());
		updatedTransaction.setVendorAddressZip(txnVendorLookupJurisdictionHandler.getZip());
		} catch(NumberFormatException e) {
			//Older Transactions. 
			e.printStackTrace();
		}
	}
	
	private void setTxnLocnShiptoJurisdictionValues() {

		updatedTransaction.setShiptoEntityCode(txnLocnShiptoJurisdictionHandler.getEntityCode()); 
		Jurisdiction jurisdiction = txnLocnShiptoJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setShiptoJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setShiptoJurisdictionId(null);
		}
		updatedTransaction.setShiptoGeocode(txnLocnShiptoJurisdictionHandler.getGeocode());
		updatedTransaction.setShiptoCountryCode(txnLocnShiptoJurisdictionHandler.getCountry());
		updatedTransaction.setShiptoStateCode(txnLocnShiptoJurisdictionHandler.getState());
		updatedTransaction.setShiptoCounty(txnLocnShiptoJurisdictionHandler.getCounty());
		updatedTransaction.setShiptoCity(txnLocnShiptoJurisdictionHandler.getCity());
		updatedTransaction.setShiptoZip(txnLocnShiptoJurisdictionHandler.getZip());
		updatedTransaction.setShiptoZipplus4(txnLocnShiptoJurisdictionHandler.getZipPlus4());
		updatedTransaction.setShiptoStj1Name(txnLocnShiptoJurisdictionHandler.getStj1Name());
		updatedTransaction.setShiptoStj2Name(txnLocnShiptoJurisdictionHandler.getStj2Name());
		updatedTransaction.setShiptoStj3Name(txnLocnShiptoJurisdictionHandler.getStj3Name());
		updatedTransaction.setShiptoStj4Name(txnLocnShiptoJurisdictionHandler.getStj4Name());
		updatedTransaction.setShiptoStj5Name(txnLocnShiptoJurisdictionHandler.getStj5Name());
		
	}
		
	private void setTxnLocnShipfromJurisdictionValues() {
		updatedTransaction.setShipfromEntityCode(txnLocnShipfromJurisdictionHandler.getEntityCode()); 
		Jurisdiction jurisdiction = txnLocnShipfromJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setShipfromJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setShipfromJurisdictionId(null);
		}
		
		updatedTransaction.setShipfromGeocode(txnLocnShipfromJurisdictionHandler.getGeocode());
		updatedTransaction.setShipfromCountryCode(txnLocnShipfromJurisdictionHandler.getCountry());
		updatedTransaction.setShipfromStateCode(txnLocnShipfromJurisdictionHandler.getState());
		updatedTransaction.setShipfromCity(txnLocnShipfromJurisdictionHandler.getCity());
		updatedTransaction.setShipfromCounty(txnLocnShipfromJurisdictionHandler.getCounty());
		updatedTransaction.setShipfromZip(txnLocnShipfromJurisdictionHandler.getZip());
		updatedTransaction.setShipfromZipplus4(txnLocnShipfromJurisdictionHandler.getZipPlus4());
		updatedTransaction.setShipfromStj1Name(txnLocnShipfromJurisdictionHandler.getStj1Name());
		updatedTransaction.setShipfromStj2Name(txnLocnShipfromJurisdictionHandler.getStj2Name());
		updatedTransaction.setShipfromStj3Name(txnLocnShipfromJurisdictionHandler.getStj3Name());
		updatedTransaction.setShipfromStj4Name(txnLocnShipfromJurisdictionHandler.getStj4Name());
		updatedTransaction.setShipfromStj5Name(txnLocnShipfromJurisdictionHandler.getStj5Name());
	}
	
	private void setTxnLocnOrdrOrgnJurisdictionValues() {
		updatedTransaction.setOrdrorgnEntityCode(txnLocnOrdrorgnJurisdictionHandler.getEntityCode()); 
		
		Jurisdiction jurisdiction = txnLocnOrdrorgnJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setOrdrorgnJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setOrdrorgnJurisdictionId(null);
		}
		updatedTransaction.setOrdrorgnGeocode(txnLocnOrdrorgnJurisdictionHandler.getGeocode());
		updatedTransaction.setOrdrorgnCountryCode(txnLocnOrdrorgnJurisdictionHandler.getCountry());
		updatedTransaction.setOrdrorgnStateCode(txnLocnOrdrorgnJurisdictionHandler.getState());
		updatedTransaction.setOrdrorgnCity(txnLocnOrdrorgnJurisdictionHandler.getCity());
		updatedTransaction.setOrdrorgnCounty(txnLocnOrdrorgnJurisdictionHandler.getCounty());
		updatedTransaction.setOrdrorgnZip(txnLocnOrdrorgnJurisdictionHandler.getZip());
		updatedTransaction.setOrdrorgnZipplus4(txnLocnOrdrorgnJurisdictionHandler.getZipPlus4());
		updatedTransaction.setOrdrorgnStj1Name(txnLocnOrdrorgnJurisdictionHandler.getStj1Name());
		updatedTransaction.setOrdrorgnStj2Name(txnLocnOrdrorgnJurisdictionHandler.getStj2Name());
		updatedTransaction.setOrdrorgnStj3Name(txnLocnOrdrorgnJurisdictionHandler.getStj3Name());
		updatedTransaction.setOrdrorgnStj4Name(txnLocnOrdrorgnJurisdictionHandler.getStj4Name());
		updatedTransaction.setOrdrorgnStj5Name(txnLocnOrdrorgnJurisdictionHandler.getStj5Name());

	}
	
	private void setTxnLocnOrdracptJurisdictionValues() {
		updatedTransaction.setOrdracptEntityCode(txnLocnOrdracptJurisdictionHandler.getEntityCode()); 
		
		Jurisdiction jurisdiction = txnLocnOrdracptJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setOrdracptJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setOrdracptJurisdictionId(null);
		}
		updatedTransaction.setOrdracptGeocode(txnLocnOrdracptJurisdictionHandler.getGeocode());
		updatedTransaction.setOrdracptCountryCode(txnLocnOrdracptJurisdictionHandler.getCountry());
		updatedTransaction.setOrdracptStateCode(txnLocnOrdracptJurisdictionHandler.getState());
		updatedTransaction.setOrdracptCity(txnLocnOrdracptJurisdictionHandler.getCity());
		updatedTransaction.setOrdracptCounty(txnLocnOrdracptJurisdictionHandler.getCounty());
		updatedTransaction.setOrdracptZip(txnLocnOrdracptJurisdictionHandler.getZip());
		updatedTransaction.setOrdracptZipplus4(txnLocnOrdracptJurisdictionHandler.getZipPlus4());
		updatedTransaction.setOrdracptStj1Name(txnLocnOrdracptJurisdictionHandler.getStj1Name());
		updatedTransaction.setOrdracptStj2Name(txnLocnOrdracptJurisdictionHandler.getStj2Name());
		updatedTransaction.setOrdracptStj3Name(txnLocnOrdracptJurisdictionHandler.getStj3Name());
		updatedTransaction.setOrdracptStj4Name(txnLocnOrdracptJurisdictionHandler.getStj4Name());
		updatedTransaction.setOrdracptStj5Name(txnLocnOrdracptJurisdictionHandler.getStj5Name());

	}
	
	private void setTxnLocnFirstUseJurisdictionValues() {
		updatedTransaction.setFirstuseEntityCode(txnLocnFirstuseJurisdictionHandler.getEntityCode()); 
		
		Jurisdiction jurisdiction = txnLocnFirstuseJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setFirstuseJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setFirstuseJurisdictionId(null);
		}
		updatedTransaction.setFirstuseGeocode(txnLocnFirstuseJurisdictionHandler.getGeocode());
		updatedTransaction.setFirstuseCountryCode(txnLocnFirstuseJurisdictionHandler.getCountry());
		updatedTransaction.setFirstuseStateCode(txnLocnFirstuseJurisdictionHandler.getState());
		updatedTransaction.setFirstuseCity(txnLocnFirstuseJurisdictionHandler.getCity());
		updatedTransaction.setFirstuseCounty(txnLocnFirstuseJurisdictionHandler.getCounty());
		updatedTransaction.setFirstuseZip(txnLocnFirstuseJurisdictionHandler.getZip());
		updatedTransaction.setFirstuseZipplus4(txnLocnFirstuseJurisdictionHandler.getZipPlus4());
		updatedTransaction.setFirstuseStj1Name(txnLocnFirstuseJurisdictionHandler.getStj1Name());
		updatedTransaction.setFirstuseStj2Name(txnLocnFirstuseJurisdictionHandler.getStj2Name());
		updatedTransaction.setFirstuseStj3Name(txnLocnFirstuseJurisdictionHandler.getStj3Name());
		updatedTransaction.setFirstuseStj4Name(txnLocnFirstuseJurisdictionHandler.getStj4Name());
		updatedTransaction.setFirstuseStj5Name(txnLocnFirstuseJurisdictionHandler.getStj5Name());

	}
	
	private void setTxnLocnBilltoJurisdictionValues() {
		updatedTransaction.setBilltoEntityCode(txnLocnBilltoJurisdictionHandler.getEntityCode()); 
		Jurisdiction jurisdiction = txnLocnBilltoJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setBilltoJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setBilltoJurisdictionId(null);
		}
		updatedTransaction.setBilltoGeocode(txnLocnBilltoJurisdictionHandler.getGeocode());
		updatedTransaction.setBilltoCountryCode(txnLocnBilltoJurisdictionHandler.getCountry());
		updatedTransaction.setBilltoStateCode(txnLocnBilltoJurisdictionHandler.getState());
		updatedTransaction.setBilltoCity(txnLocnBilltoJurisdictionHandler.getCity());
		updatedTransaction.setBilltoCounty(txnLocnBilltoJurisdictionHandler.getCounty());
		updatedTransaction.setBilltoZip(txnLocnBilltoJurisdictionHandler.getZip());
		updatedTransaction.setBilltoZipplus4(txnLocnBilltoJurisdictionHandler.getZipPlus4());
		updatedTransaction.setBilltoStj1Name(txnLocnBilltoJurisdictionHandler.getStj1Name());
		updatedTransaction.setBilltoStj2Name(txnLocnBilltoJurisdictionHandler.getStj2Name());
		updatedTransaction.setBilltoStj3Name(txnLocnBilltoJurisdictionHandler.getStj3Name());
		updatedTransaction.setBilltoStj4Name(txnLocnBilltoJurisdictionHandler.getStj4Name());
		updatedTransaction.setBilltoStj5Name(txnLocnBilltoJurisdictionHandler.getStj5Name());

	}
	
	private void setTxnLocnTtlxfrJurisdictionValues() {
		updatedTransaction.setTtlxfrEntityCode(txnLocnTtlxfrJurisdictionHandler.getEntityCode()); 
		
		Jurisdiction jurisdiction = txnLocnTtlxfrJurisdictionHandler.getJurisdictionFromDB();
		if (jurisdiction != null) {
			updatedTransaction.setTtlxfrJurisdictionId(jurisdiction.getJurisdictionId());
		}
		else {
			updatedTransaction.setTtlxfrJurisdictionId(null);
		}
		updatedTransaction.setTtlxfrGeocode(txnLocnTtlxfrJurisdictionHandler.getGeocode());
		updatedTransaction.setTtlxfrCountryCode(txnLocnTtlxfrJurisdictionHandler.getCountry());
		updatedTransaction.setTtlxfrStateCode(txnLocnTtlxfrJurisdictionHandler.getState());
		updatedTransaction.setTtlxfrCity(txnLocnTtlxfrJurisdictionHandler.getCity());
		updatedTransaction.setTtlxfrCounty(txnLocnTtlxfrJurisdictionHandler.getCounty());
		updatedTransaction.setTtlxfrZip(txnLocnTtlxfrJurisdictionHandler.getZip());
		updatedTransaction.setTtlxfrZipplus4(txnLocnTtlxfrJurisdictionHandler.getZipPlus4());
		updatedTransaction.setTtlxfrStj1Name(txnLocnTtlxfrJurisdictionHandler.getStj1Name());
		updatedTransaction.setTtlxfrStj2Name(txnLocnTtlxfrJurisdictionHandler.getStj2Name());
		updatedTransaction.setTtlxfrStj3Name(txnLocnTtlxfrJurisdictionHandler.getStj3Name());
		updatedTransaction.setTtlxfrStj4Name(txnLocnTtlxfrJurisdictionHandler.getStj4Name());
		updatedTransaction.setTtlxfrStj5Name(txnLocnTtlxfrJurisdictionHandler.getStj5Name());

	}
	
	public JurisdictionTaxrate getTaxRateAction(Long jurTaxRateId){
		jurisdictionTaxrate = new JurisdictionTaxrate();
		if(jurTaxRateId != null) {
			jurisdictionTaxrate = jurisdictionTaxrateService.findByIdInitialized(jurTaxRateId);
			return jurisdictionTaxrate;
		}
		return null;
	}
	
	public String getStjRateandSetAmt(String stjno,String stjTaxtypeUsedCode,Long jurTaxRateId,String stjname){
		if(stjname != null && stjTaxtypeUsedCode !=null) {
			if(jurTaxRateId != null) {
				JurisdictionTaxrate jurisdictionTaxrate = jurisdictionTaxrateService.findByIdInitialized(jurTaxRateId);
				if(jurisdictionTaxrate != null) { 
					Jurisdiction jurisdiction = jurisdictionTaxrate.getJurisdiction();
					if(jurisdiction != null) {
						for(int i=1; i <= 5; i++){
							try{
								Class<?> noparams[] = {};
								Object paramsObj[] = {};
								Method jurstjMethod = jurisdiction.getClass().getDeclaredMethod("getStj"+i+"Name", noparams);
								String value = (String)jurstjMethod.invoke(jurisdiction, paramsObj);
								if(stjname.equals(value)){
									BigDecimal rateValue;
									BigDecimal setAmtValue;
									Method methodrate;
									Method methodsetamt;
									if("S".equals(stjTaxtypeUsedCode)) {
										methodrate = jurisdictionTaxrate.getClass().getDeclaredMethod("getStj"+i+"SalesRate", noparams);
										methodsetamt = jurisdictionTaxrate.getClass().getDeclaredMethod("getStj"+i+"SalesSetamt", noparams);
									}else {
										methodrate = jurisdictionTaxrate.getClass().getDeclaredMethod("getStj"+i+"UseRate", noparams);
										methodsetamt = jurisdictionTaxrate.getClass().getDeclaredMethod("getStj"+i+"UseSetamt", noparams);
									}
									rateValue = (BigDecimal)methodrate.invoke(jurisdictionTaxrate, paramsObj);
									setAmtValue = (BigDecimal)methodsetamt.invoke(jurisdictionTaxrate, paramsObj);
									String rateMethod = "setStj" + stjno + "JurisdTaxRate";
									String setAmtMethod = "setStj" + stjno + "JurisdTaxSetAmount";
									Class<BigDecimal>[] paramBigDecimal = new Class[1];
									paramBigDecimal[0] = BigDecimal.class;
									Method ratemethod = selectedTransaction.getClass().getDeclaredMethod(rateMethod,paramBigDecimal);
									ratemethod.invoke(selectedTransaction, rateValue);
									Method setAmtmethod = selectedTransaction.getClass().getDeclaredMethod(setAmtMethod,paramBigDecimal);
									setAmtmethod.invoke(selectedTransaction, setAmtValue);
									break;
								}
							}catch(Exception e) {
								System.out.println("Exception occured in class TransactionViewBean in method getStjRateandSetAmt()."+e.getMessage());
							}
						}
					}
					else {
						return null;
					}
				}
				else {
					return null;
				}
		    }
			return stjname;
		}
		else {
			return null;
		}
	}
	
	public SitusMatrix getSitusMatrix(Long situsMatrixId) {
		situsMatrix = new SitusMatrix();
		if(situsMatrixId !=null){
			situsMatrix = situsMatrixService.findById(situsMatrixId);
			return situsMatrix;
		}
		return null;
	}
	
	public VendorNexusDtl getVendorNexusDetail(Long vendorNexusDtlId) {
		vendorNexusDtl = new VendorNexusDtl();
		if(vendorNexusDtlId !=null){
			vendorNexusDtl = vendorNexusDetailService.findById(vendorNexusDtlId);
			return vendorNexusDtl;
		}
		return null;
	}
	
	public void displayErrorMessage(String errormessage) {
		FacesMessage message = new FacesMessage(errormessage);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		
	}
	
	public void setCurrentAction(EditAction currAction) {
		transactionDetailBean.setCurrentAction(currAction);
	}
}
