package com.ncsts.view.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;


public class DataUtilityGLMarkExtractBackingBean extends DataUtilityGLExtractBackingBean {
    @SuppressWarnings("unused")
    private Logger logger = Logger.getLogger(DataUtilityGLMarkExtractBackingBean.class);
    private BatchMaintenanceDataModel batchMaintenanceDataModel;
    public static final String GM_BATCH_TYPE_CODE = "GM";

    private void init() {
        
    	super.getBatchMaintenanceDataModel().setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
        super.setBatchMaintenance(new BatchMaintenance());
        super.getBatchMaintenanceBean().resetSearchAction();
        super.getBatchMaintenance().setEntryTimestamp(null);
        super.getBatchMaintenance().setBatchStatusCode(null);
        super.getBatchMaintenance().setBatchTypeCode(GM_BATCH_TYPE_CODE);
        super.getBatchMaintenanceBean().setBatchTypeCode(GM_BATCH_TYPE_CODE);
        super.getBatchMaintenanceBean().getBatchMaintenanceDTO().setBatchTypeCode(GM_BATCH_TYPE_CODE);
        super.getBatchMaintenanceBean().setCalledFromConfigImportExpMenu(false);
        super.getBatchMaintenanceBean().prepareBatchTypeSpecificsFields();
        super.getBatchMaintenanceDataModel().setPageSize(50);
        super.getBatchMaintenanceDataModel().setCriteria(super.getBatchMaintenance());
        super.setSelectedRowTrIndex(-1);
        super.setSelectedTransaction(null);
        super.setTransactionCount(null);
        super.setRecordCount(null);
        super.setTaxAmount(null);
        resetDetailTable();
    }
    
    public BatchMetadata getBatchMetadata() {
        if (super.getBatchMetadataAsIs() == null) {
        	super.setBatchMetadata(super.getBatchMaintenanceService().findBatchMetadataById("GM"));
        }
        return super.getBatchMetadataAsIs();
    }
    
    public String refreshAction() {
        init();
        super.getBatchMaintenanceDataModel().refreshData(false);
        super.setSelectedBatchIndex(-1);
        return null;
    }
    
    public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
        return super.getBatchMaintenanceDataModel();
    }
    
    public String markAction() {
        String glDate = null;
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        try{
        	if(super.getGlmarkCalendarDate() != null) {
        		glDate =  super.getGlmarkCalendarDate().getValue().toString();
        	}
			Date date = (Date)formatter.parse(glDate);
			System.out.println(date);        
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			glDate =  (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "/" +  cal.get(Calendar.YEAR);
			System.out.println("formatedDate : " + glDate);    
		}catch(Exception e){
			e.printStackTrace();
		}
        
        BatchMaintenance batchMaintenance = new BatchMaintenance();
        batchMaintenance.setYearPeriod(getFilterYear());
        super.getBatchMaintenanceService().createNewBatchGL(batchMaintenance, glDate);

        refreshAction();
        return "glmarktrans_main";
    }
    
  	public void retrieveBatchMaintenance() { 
  		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(GM_BATCH_TYPE_CODE);
  		batchMaintenanceDataModel =  super.getBatchMaintenanceBean().prepareBatchMaintenance(batchMaintenance);
  		super.setBatchMaintenanceDataModel(batchMaintenanceDataModel); 
  	}
  	
  	public String resetBatchFields(){
  		super.resetBatchFields();
  		return null;
  	}
  	public String navigateAction() {
		init();
		return "data_utility_gl_marktrans";
	}
  	
} 