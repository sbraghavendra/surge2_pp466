package com.ncsts.view.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.BatchMetadata;
import com.ncsts.domain.ImportMapBatch;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.User;
import com.ncsts.dto.BatchMetadataList;
import com.ncsts.jsf.model.BatchMaintenanceDataModel;
import com.ncsts.services.BatchMaintenanceService;
import com.ncsts.services.OptionService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.BatchStatistics;

public class ImportMapBean {

	private Logger logger = Logger.getLogger(ImportMapBean.class);
	
	@Autowired
	private loginBean loginBean;
	
	private BatchMaintenanceBackingBean batchMaintenanceBean;
	
	@Autowired
	private OptionService optionService;
	
	@Autowired
	private loginBean loginservice;

	private List<BatchMaintenance> batchList;

	private UserService userService;

	private BatchMaintenanceDAO batchMaintenanceDAO;
	
	private BatchMaintenance exampleBatchMaintenance;

	private BatchMaintenance selectedBatch;
	private List<ImportMapBatch> selectedBatches;
	
    private HtmlCalendar batchStartTime = new HtmlCalendar();

	private int selectedRowIndex = -1;

	private BatchErrorsBackingBean batchErrorsBean;
	private BatchStatistics batchStatistics;
	
	private FilePreferenceBackingBean filePreferenceBean;
	private BatchMetadata batchMetadata;
	private BatchMaintenanceDataModel batchMaintenanceDataModel;
	private ImportMapAddBean importMapAddBean;

	private BatchMaintenanceService batchMaintenanceService;


	private BatchMetadataList batchMetadataList;
  
	private boolean initialAOLOnFlag = true;
	
	private BCPTransactionDetailBean bcpTransactionDetailBean;

	private BCPSaleTransactionBean bcpSaleTransactionBean;

	private Boolean isNavigated = false;
	

	public BatchMaintenanceBackingBean getBatchMaintenanceBean() {
		return batchMaintenanceBean;
	}

	public void setBatchMaintenanceBean(
			BatchMaintenanceBackingBean batchMaintenanceBean) {
		this.batchMaintenanceBean = batchMaintenanceBean;
	}

	public BCPTransactionDetailBean getBcpTransactionDetailBean() {
		return bcpTransactionDetailBean;
	}

	public void setBcpTransactionDetailBean(BCPTransactionDetailBean bcpTransactionDetailBean) {
		this.bcpTransactionDetailBean = bcpTransactionDetailBean;
	}
	
	public BCPSaleTransactionBean getBcpSaleTransactionBean() {
		return bcpSaleTransactionBean;
	}

	public void setBcpSaleTransactionBean(BCPSaleTransactionBean bcpSaleTransactionBean) {
		this.bcpSaleTransactionBean = bcpSaleTransactionBean;
	}

	public String errorsAction() {
	  batchErrorsBean.setBatchMaintenance(selectedBatch);
	  batchErrorsBean.setReturnView("importmap_main");
	  
	  //0001701: Import errors make the system crash
	  batchErrorsBean.getBatchErrorDataModel().setBatchMaintenance(selectedBatch);

	  return "batch_maintenance_errors";
    }
	
	private boolean currentPurchasingSelected = false;

	public String navigateAction() {
		batchMaintenanceBean.resetSearchAction();
		initialAOLOnFlag = false;
		init();
		batchMaintenanceDataModel.refreshData(false);
		selectedRowIndex = -1;
		isNavigated = true;
		batchMaintenanceBean.setBatchTypeCode(batchMaintenanceBean.getBatchMaintenanceDTO().getBatchTypeCode()) ; 
		batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
		batchMaintenanceBean.prepareBatchTypeSpecificsFields();
		
		return "importmap_main";
	}
	
	public List<ImportMapBatch> getSelectedBatches() {
		return this.selectedBatches;
	}
	
	public String processAction() {
  		List<ImportMapBatch> batches = new ArrayList<ImportMapBatch>();
		Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
		while (iter.hasNext()) {
			BatchMaintenance bm = iter.next();
			if (bm.getSelected()) {
				batches.add(new ImportMapBatch(bm));
			}
		}
		if (batches.size() == 0 && selectedBatch!=null) batches.add(new ImportMapBatch(selectedBatch));
		
		// sort in order (oldest to newest)
		Collections.sort(batches);
		for (ImportMapBatch trub : batches) {
			if (! trub.isImported()) {
				trub.setError("Not an imported batch");
			} 
			else if (trub.isHeld()) {
				trub.setError("On Hold");
			}
		}
		
		selectedBatches = batches;	
  		
  	    return "importmap_process";
	}
	
	public boolean getIsPcoBatch() {
		if(selectedBatch.getBatchTypeCode().equalsIgnoreCase("PCO") ){
			return true;
		}
		else{
			return false;
		}
	}

	public String viewAction() {
		if(getIsPurchasingMenu()){
			if(selectedBatch.getBatchTypeCode().equalsIgnoreCase("IP")){
				this.bcpTransactionDetailBean.setBatchId(this.selectedBatch.getbatchId());
				this.bcpSaleTransactionBean.setBatchId(new Long(0));
			}
			else if(selectedBatch.getBatchTypeCode().equalsIgnoreCase("PCO")){
				this.bcpTransactionDetailBean.setBatchId(this.selectedBatch.getbatchId());
				this.bcpSaleTransactionBean.setBatchId(this.selectedBatch.getbatchId());
			}
			else{
				this.bcpTransactionDetailBean.setBatchId(new Long(0));
				this.bcpSaleTransactionBean.setBatchId(new Long(0));
			}
			
			return "importmap_transactions";
		}
		else{
			if(selectedBatch.getBatchTypeCode().equalsIgnoreCase("IPS")){
				this.bcpTransactionDetailBean.setBatchId(new Long(0));
				this.bcpSaleTransactionBean.setBatchId(this.selectedBatch.getbatchId());
			}
			else if(selectedBatch.getBatchTypeCode().equalsIgnoreCase("PCO")){
				this.bcpTransactionDetailBean.setBatchId(this.selectedBatch.getbatchId());
				this.bcpSaleTransactionBean.setBatchId(this.selectedBatch.getbatchId());
			}
			else{
				this.bcpTransactionDetailBean.setBatchId(new Long(0));
				this.bcpSaleTransactionBean.setBatchId(new Long(0));
			}
			return "importmap_saletransactions";
		}
	}
	
    public String submitProcessAction() {	
    	//Removed by MF, AC
    	//if (((Date)batchStartTime.getValue()).before(new Date())) {
      	//	JsfHelper.addError("Cannot use submit date in the past");
      	//	return null;
      	//}
      	  
        //Including checked rows
      	boolean validCheck = false;
    	Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
    	BatchMaintenance batchMaintenance = null;	
    	while (iter.hasNext()) {
    		batchMaintenance = (BatchMaintenance)iter.next();	
    		if (batchMaintenance.getSelected() && ("I".equalsIgnoreCase(batchMaintenance.getBatchStatusCode()) || "IA".equalsIgnoreCase(batchMaintenance.getBatchStatusCode())) 
    		  		&& !batchMaintenance.isHeld()) {
    			
				batchMaintenance.setBatchStatusCode("FP");
				
				//Control already done with the timezone conversion
				batchMaintenance.setSchedStartTimestamp((Date)batchStartTime.getValue());
	
				batchMaintenance.setUpdateTimestamp(new Date());
				batchMaintenance.setStatusUpdateTimestamp(new Date());
				batchMaintenance.setStatusUpdateUserId(Auditable.currentUserCode());
				batchMaintenanceDataModel.getBatchMaintenanceDAO().update(batchMaintenance);
				batchMaintenanceDataModel.refreshData(false);
				validCheck = true;
    		}
    	}
    	
    	//Including highlighted row if needed
      	if(!validCheck && (selectedBatch != null) && ("I".equalsIgnoreCase(selectedBatch.getBatchStatusCode()) || "IA".equalsIgnoreCase(selectedBatch.getBatchStatusCode())) 
      	  		&& !selectedBatch.isHeld()){
      		selectedBatch.setBatchStatusCode("FP");
      		
      		//Control already done with the timezone conversion
    		selectedBatch.setSchedStartTimestamp((Date)batchStartTime.getValue());
	
    		selectedBatch.setUpdateTimestamp(new Date());
    		selectedBatch.setStatusUpdateTimestamp(new Date());
    		selectedBatch.setStatusUpdateUserId(Auditable.currentUserCode());
    		batchMaintenanceDataModel.getBatchMaintenanceDAO().update(selectedBatch);
    		batchMaintenanceDataModel.refreshData(false);
      	}
    		
    	selectedRowIndex = -1;
    	return "importmap_main";
    }
  
	public String statisticsAction() {
		batchStatistics = new BatchStatistics(selectedBatch, batchMaintenanceDAO, filePreferenceBean.getBatchPreferenceDTO());
		batchStatistics.calcStatistics();
		return "importmap_statistics";
	}
	public void refreshStatistics() {
		batchStatistics.calcStatistics();
	}

	private boolean isAllBatchWhen() {
		String aolWhen = filePreferenceBean.getUserPreferenceDTO().getAolWhen();
		
		if (aolWhen != null && aolWhen.trim().equalsIgnoreCase("1")) {
			return false;
		} else {
			return true;
		}
	}

	private boolean isAllBatchWhich() {
		String aolWhich = filePreferenceBean.getUserPreferenceDTO().getAolWhich();
		
		if (aolWhich != null && aolWhich.trim().equalsIgnoreCase("1")) {
			return false;
		} else {
			return true;
		}
	}

	public String getMessageAOL() {
		String messageAOL = "";
		if (initialAOLOnFlag && filePreferenceBean.getUserPreferenceDTO().isAolEnabled()) {
			if (isAllBatchWhen() && isAllBatchWhich()) {
				messageAOL = "Alert On Logon - All Batches";
			} else if (isAllBatchWhen() && !isAllBatchWhich()) {
				messageAOL = "Alert On Logon - All Unprocessed Batches";
			} else if (!isAllBatchWhen() && isAllBatchWhich()) {
				messageAOL = "Alert On Logon - All Batches Since Last Logoff";
			} else if (!isAllBatchWhen() && !isAllBatchWhich()) {
				messageAOL = "Alert On Logon - Unprocessed Batches Since Last Logoff";
			}
		}

		return messageAOL;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException {
		logger.info("ImportMapBean: enter selectedRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedBatch = (BatchMaintenance) table.getRowData();
		this.selectedRowIndex = table.getRowIndex();
	}

	//Bug 3210
	public String refreshAction() {
		init();
		batchMaintenanceDataModel.refreshData(false);
		selectedRowIndex = -1;
		return null;
	}

	// Bug 2250
	public String selectAllAction() {
  	DetachedCriteria criteria = DetachedCriteria.forClass(BatchMaintenance.class);
  	criteria.add(Restrictions.in("batchTypeCode", Arrays.asList("IP", "PCO")));
  	//criteria.add(Expression.eq("batchStatusCode", "I"));
  	criteria.add(Restrictions.or(Restrictions.eq("batchStatusCode", "I"), Restrictions.eq("batchStatusCode", "IA")));
  	criteria.add(Restrictions.or(Restrictions.eq("heldFlag", "0"), Restrictions.isNull("heldFlag")));

  	batchMaintenanceDataModel.setCriteria(criteria);
  	batchMaintenanceDataModel.refreshData(false);
  		selectedRowIndex = -1;
		return null;
	}
	
  public Boolean getDisplayError() {
		return (selectedRowIndex != -1) && (selectedBatch.getErrorSevCode() != null) &&
		(selectedBatch.getErrorSevCode().trim().length() > 0);
	}
  
  public Boolean getDisplayProcess() {
  	if ((selectedRowIndex != -1) && (selectedBatch != null)
  			&& ("I".equalsIgnoreCase(selectedBatch.getBatchStatusCode()) || "IA".equalsIgnoreCase(selectedBatch.getBatchStatusCode()))  && !selectedBatch.isHeld()) return true;
  	
  	return getHasSelection();
  }
  
  private boolean getHasSelection() {
	Iterator<BatchMaintenance> iter = batchMaintenanceDataModel.iterator();
	while (iter.hasNext()) {
		if (iter.next().getSelected()) {
			return true;
		}
	}
	return false;
  }
  
  public Boolean getDisplayView() {
  	return selectedRowIndex != -1 
  			&& selectedBatch != null
  			&& ("I".equalsIgnoreCase(selectedBatch.getBatchStatusCode()) || "IA".equalsIgnoreCase(selectedBatch.getBatchStatusCode()))
  			&& !selectedBatch.isHeld();
  }
  
  public boolean getIsPurchasingMenu(){
	return loginBean.getIsPurchasingSelected();
  }

  public String getCurrentModuleMenu(){
	if(getIsPurchasingMenu()){
		return "PURCHUSE";
	}
	else{
		return "BILLSALE";
	}
  }
  
  private void init() {  
	batchMaintenanceDataModel.setDefaultSortOrder("batchId", BatchMaintenanceDataModel.SORT_DESCENDING);
	batchMaintenanceDataModel.setPageSize(50);
	
	if (initialAOLOnFlag && filePreferenceBean.getUserPreferenceDTO().isAolEnabled()) {
		//For AOL
		DetachedCriteria criteria = DetachedCriteria.forClass(BatchMaintenance.class);
		
		Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));  
		String option_pco = (option==null || option.getValue()==null)? "0" : option.getValue();
		if(option_pco.equals("1")){
			if(loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense()){
				criteria.add(Restrictions.or(Restrictions.or(Restrictions.eq("batchTypeCode", "IP"), Restrictions.eq("batchTypeCode", "IPS")), Restrictions.eq("batchTypeCode", "PCO")));
			}
			else if(loginBean.getHasPurchaseLicense() && !loginBean.getHasBillingLicense()){
				criteria.add(Restrictions.or(Restrictions.eq("batchTypeCode", "IP"), Restrictions.eq("batchTypeCode", "PCO")));
			}
			else if(!loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense()){
				criteria.add(Restrictions.or(Restrictions.eq("batchTypeCode", "IPS"), Restrictions.eq("batchTypeCode", "PCO")));
			}
		}
		else{
			if(loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense()){
				criteria.add(Restrictions.or(Restrictions.eq("batchTypeCode", "IP"), Restrictions.eq("batchTypeCode", "IPS")));
			}
			else if(loginBean.getHasPurchaseLicense() && !loginBean.getHasBillingLicense()){
				criteria.add(Expression.eq("batchTypeCode", "IP"));
			}
			else if(!loginBean.getHasPurchaseLicense() && loginBean.getHasBillingLicense()){
				criteria.add(Expression.eq("batchTypeCode", "IPS"));
			}
		}
			
		String aolWhen = filePreferenceBean.getUserPreferenceDTO().getAolWhen();
		String aolWhich = filePreferenceBean.getUserPreferenceDTO().getAolWhich();

		if (isAllBatchWhen() && isAllBatchWhich()) {
		} 
		else if (isAllBatchWhen() && !isAllBatchWhich()) {
			criteria.add(Expression.ne("batchStatusCode", "P"));
		} 
		else if (!isAllBatchWhen() && isAllBatchWhich()) {
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH/mm/ss");
			Date aDate = null;
			try{
				aDate = dateFormat.parse(userService.getUserLogoff(Auditable.currentUserCode()));
			}
			catch(Exception e){
			}
			
			if(aDate!=null){
				criteria.add(Expression.ge("entryTimestamp", aDate));
			}
		} 
		else if (!isAllBatchWhen() && !isAllBatchWhich()) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH/mm/ss");
			Date aDate = null;
			try{
				aDate = dateFormat.parse(userService.getUserLogoff(Auditable.currentUserCode()));
			}
			catch(Exception e){
			}

			if(aDate!=null){
				criteria.add(Expression.ge("entryTimestamp", aDate));
			}
			criteria.add(Expression.ne("batchStatusCode", "P"));
		}
		
		batchMaintenanceDataModel.setCriteria(criteria);
	}
	else{	
		setExampleBatchMaintenance();
	}
  }
  
  public void setExampleBatchMaintenance () {
	  
	  //Maintenance
	  exampleBatchMaintenance = new BatchMaintenance();
	  exampleBatchMaintenance.setEntryTimestamp(null);
	  exampleBatchMaintenance.setBatchStatusCode(null);
	
	
	  boolean pcoEnabled = false;
	  Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));  
	  String option_pco = (option==null || option.getValue()==null)? "0" : option.getValue();
	  if(option_pco.equals("1"))
		  pcoEnabled = true;
	
	
	  if(getIsPurchasingMenu()){
		  exampleBatchMaintenance.setBatchTypeCode("IP");//PCO will be included in JAO
		  if(pcoEnabled){
			  this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("PCO");
		  }
		  else {
			  this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("IP");
		  }
	  }
	  else{
		  exampleBatchMaintenance.setBatchTypeCode("IPS");//PCO will be included in JAO
		  if(pcoEnabled){
			  this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("PCO");
		  }
		  else {
			  this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("IPS");
		  }
	  }

	  if (pcoEnabled){
		  exampleBatchMaintenance.setAttachPcoToCriteria(true);
		  batchMaintenanceBean.getBatchMaintenanceDTO().setBatchTypeCode("PCO");
	  }
	  else {
		  if(getIsPurchasingMenu()){
			  batchMaintenanceBean.getBatchMaintenanceDTO().setBatchTypeCode("IP");
		  }
		  else {
			  batchMaintenanceBean.getBatchMaintenanceDTO().setBatchTypeCode("IPS");
		  }
	  }
	  batchMaintenanceDataModel.setCriteria(exampleBatchMaintenance);
  }
  
	public BatchMaintenanceDataModel getBatchMaintenanceDataModel() {
	    if (exampleBatchMaintenance == null) {
	      init();
	    }
	    return this.batchMaintenanceDataModel;
	}
	
	public void sortAction(ActionEvent e) {
		batchMaintenanceDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}

	public void setBatchMaintenanceDataModel(BatchMaintenanceDataModel batchMaintenanceDataModel) {
		this.batchMaintenanceDataModel = batchMaintenanceDataModel;
	}

	public BatchMetadata getBatchMetadata() {
		if (this.batchMetadata == null) {
			if (getIsPurchasingMenu()) {
				this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("IP");
			} else {
				this.batchMetadata = batchMaintenanceDAO.findBatchMetadataById("IPS");
			}
		}
		return this.batchMetadata;
	}

	public int getSelectedRowIndex() {
		return this.selectedRowIndex;
	}

	/**
	 * @param selectedRowIndex the selectedRowIndex to set
	 */
	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}

	/**
	 * @return the batchErrorsBean
	 */
	public BatchErrorsBackingBean getBatchErrorsBean() {
		return this.batchErrorsBean;
	}

	/**
	 * @param batchErrorsBean the batchErrorsBean to set
	 */
	public void setBatchErrorsBean(BatchErrorsBackingBean batchErrorsBean) {
		this.batchErrorsBean = batchErrorsBean;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public boolean getInitialAOLOnFlag() {
		return initialAOLOnFlag;
	}

	public void setInitialAOLOnFlag(boolean initialAOLOnFlag) {
		this.initialAOLOnFlag = initialAOLOnFlag;
	}

	public void viewMapImportAction() {
	}

	public String addImportMapAction() {
		importMapAddBean.clear(this);
		return "Add";
	}

	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public ImportMapAddBean getImportMapAddBean() {
		return this.importMapAddBean;
	}

	public void setImportMapAddBean(ImportMapAddBean importMapAddBean) {
		this.importMapAddBean = importMapAddBean;
	}

	private void addError(String s) {
		addMessage(s, FacesMessage.SEVERITY_ERROR);
	}
	private void addInfo(String s) {
		addMessage(s, FacesMessage.SEVERITY_INFO);
	}

	private void addMessage(String s, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(severity, s, null));
	}

	public HtmlCalendar getBatchStartTime() {
		batchStartTime.resetValue();
		batchStartTime.setValue(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime());
//		batchStartTime.setValue(new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(filePreferenceBean.getBatchPreferenceDTO().getDefaultBatchStartTime()));
		return this.batchStartTime;
	}

	public void setBatchStartTime(HtmlCalendar batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public BatchMaintenance getSelectedBatch() {
		return this.selectedBatch;
	}

	public BatchStatistics getBatchStatistics() {
		return this.batchStatistics;
	}

	public BatchMetadataList getBatchMetadataList() {
		return batchMetadataList;
	}

	public void setBatchMetadataList(BatchMetadataList batchMetadataList) {
		this.batchMetadataList = batchMetadataList;
	}

	public BatchMaintenanceService getBatchMaintenanceService() {
		return batchMaintenanceService;
	}

	public void setBatchMaintenanceService(BatchMaintenanceService batchMaintenanceService) {
		this.batchMaintenanceService = batchMaintenanceService;
	}

	public String updateUserFieldsAction() {
		if(selectedBatch != null){
			BatchMaintenance updatedbatch = batchMaintenanceService.findById(selectedBatch.getBatchId());
			if(updatedbatch!=null){
				String selectedType = "";
				if(updatedbatch.getBatchTypeCode().equalsIgnoreCase("PCO")){
					boolean ipSelected =  loginservice.getIsPurchasingSelected();
					if(ipSelected){
						selectedType = "IP";
			        }
			        else{
			        	boolean ipsSelected =  loginservice.getIsBillingSelected();
			        	if(ipsSelected){
			        		selectedType = "IPS";
			        	}
			        	else{
			        		logger.error("Module not selected");
							return null;
			        	}
			        }			
				}
				else{
					selectedType = updatedbatch.getBatchTypeCode();
				}
				BatchMetadata batchMetadata = batchMaintenanceService.findBatchMetadataById(selectedType);
				batchMetadataList = new BatchMetadataList(batchMetadata, updatedbatch);
			}
			else{
				logger.error("Batch ID: " + selectedBatch.getBatchId() + " not found");
				return null;
			}
		}
		return "importMapMain_defuserfields_update";
	}

	public String updateUserFields() {
		batchMaintenanceService.update(batchMetadataList.update());
		batchMaintenanceDataModel.refreshData(false);
		selectedRowIndex = -1;
		return "importMapMain_defuserfields";
	}

	public String cancelUpdateUserFields() {
		return "importMapMain_defuserfields";
	}

	public boolean isNavigatedFlag() {
		return isNavigated;
	}

	public void setNavigatedFlag(boolean navigatedFlag) {
		this.isNavigated = isNavigated;
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }

	public void retrieveBatchMaintenance() { 
		selectedRowIndex = -1;
		
		// after login if AOL screen was not showed. 
		if(exampleBatchMaintenance == null) {
			setExampleBatchMaintenance();
		}
		BatchMaintenance batchMaintenance = new BatchMaintenance();
		batchMaintenance.setBatchTypeCode(exampleBatchMaintenance.getBatchTypeCode());
        batchMaintenance.setAttachPcoToCriteria(exampleBatchMaintenance.isAttachPcoToCriteria());
        batchMaintenanceBean.setCalledFromConfigImportExpMenu(false);
		this.batchMaintenanceDataModel =  batchMaintenanceBean.prepareBatchMaintenance(batchMaintenance);
		
	}
	
	public String resetBatchFields(){
		batchMaintenanceBean.setResetBatchAccordion(false);
		batchMaintenanceBean.resetOtherBatchFields();
		return null;
	}
}