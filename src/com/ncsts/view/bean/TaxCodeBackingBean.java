package com.ncsts.view.bean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.ncsts.common.CacheManager;
import com.ncsts.common.constants.PurchaseTransactionServiceConstants;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.domain.PurchaseTransaction;
import com.ncsts.domain.ReferenceDetail;
import com.ncsts.domain.ReferenceDetailPK;
import com.ncsts.domain.ReferenceDocument;
import com.ncsts.domain.TaxCode;
import com.ncsts.domain.TaxCodeDetail;
import com.ncsts.domain.TaxCodePK;
import com.ncsts.domain.TaxCodeStatePK;
import com.ncsts.domain.User;
import com.ncsts.dto.TaxCodeDetailDTO;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.jsf.model.TaxCodeDataModel;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.OptionService;
import com.ncsts.services.PurchaseTransactionService;
import com.ncsts.services.ReferenceDocumentService;
import com.ncsts.services.TaxCodeDetailService;
import com.ncsts.services.TaxCodeService;
import com.ncsts.services.TaxCodeStateService;
import com.ncsts.services.TransactionDetailService;
import com.ncsts.view.util.ArithmeticUtils;
import com.ncsts.view.util.TogglePanelController;

@SuppressWarnings("ALL")
public class TaxCodeBackingBean implements InitializingBean {
	private Logger logger = Logger.getLogger(TaxCodeBackingBean.class);
	
	@Autowired
	private TaxCodeService taxCodeService;
	
	@Autowired
	private loginBean loginBean;
	
	@Autowired
	private TaxCodeDetailService taxCodeDetailService;
	
	@Autowired
	private CacheManager cacheManager;
	
	@Autowired
	private JurisdictionService jurisdictionService;
	
	@Autowired
	private TransactionDetailService transactionDetailService;
	
	@Autowired
	private ReferenceDocumentService referenceDocumentService;
	
	@Autowired
	private OptionService optionService;
	
	@Autowired
	private TaxCodeStateService taxCodeStateService;

	@Autowired
	private DataDefinitionService dataDefinitionService;

	@Autowired
	private PurchaseTransactionService purchaseTransactionService;
	
	public interface FindCodeCallback {
		void findCodeCallback(String code, String context);
	}
	
	private String enteredTaxCode = "";
	private String enteredDesc = "";
	private String selectedActiveInactive = "-1";
	
	private List<TaxCode> taxCodeList;
	private List<TaxCodeDetailDTO> jurisdictionList;
	private List<TaxCodeDetailDTO> taxCodeRuleList;
	private List<ReferenceDetail> refDocList;
	
	private List<TaxCodeDetail> taxCodeDetailsForCopy;
	
	private EditAction currentAction = EditAction.VIEW;
	
	private TaxCode selectedTaxCode;
	
	private JurisdictionFilterHandler filterHandler = new JurisdictionFilterHandler();
	private JurisdictionUpdateHandler updateHandler = new JurisdictionUpdateHandler();
	
	private Map<String, TaxCodeDetailDTO> selectionMap = new LinkedHashMap<String, TaxCodeDetailDTO>();
	
	private TaxCode selectedTaxCodeSource;
	private TaxCodeDetailDTO selectedJurisdiction;
	private TaxCodeDetailDTO selectedTaxCodeRule;
	private ReferenceDetail selectedRefDocDetail;
	private ReferenceDetail updateRefDocDetail;
	private TaxCodeDetailDTO updateTaxCodeRule;
	private ReferenceDocument selectedReferenceDocument;
	private String selectedJurisLocalTaxability;
	private String selectedJurisCountryCode;
	private String selectedJurisStateCode;
	private TaxCodeDetailDTO selectedTaxCodeCountyRule;
	private TaxCodeDetailDTO selectedTaxCodeCityRule;
	
	private int selectedTaxCodeIndex = -1;
	private int selectedJurisdictionIndex = -1;
	private int selectedTaxCodeRuleIndex = -1;
	private int selectedRefDocDetailIndex = -1;
	
	private List<SelectItem> taxTypeList;
	private List<SelectItem> tcTypeList;
	private List<SelectItem> rateTypeList;
	private List<SelectItem> serviceTypeList;
	private List<SelectItem> exemptReasonList;
	private List<SelectItem> processTypeList;
	private List<SelectItem> groupByDriverList;
	private List<SelectItem> allocationBucketList;
	private List<SelectItem> allocateByList;
	private List<SelectItem> allocateConditionList;
	
	private Long displayBackdateWarning = 0L;
	
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	private boolean addRuleToSelected = false;
	
	Map<String, String> refTypeCodeDescMap;
	
	private SearchDocRefHandler docRefHandler = new SearchDocRefHandler();
	
	private int jurisTablePageNumber;
	private int jurisTablePageSize = 100;
	
	private HtmlInputText refDocCodeInput = new HtmlInputText();
	
	private PurchaseTransaction sourceTransaction;
	private boolean hasExistingStateRule = false;
	private boolean addCountyRule = false;
	private boolean addCityRule = false;
	
	private String returnView;
	private HtmlInputFileUpload uploadFile;
	private UploadedFile uploadedFile;
	private List<SelectItem> refTypeList;
	private String url = null;
	
	private FindCodeCallback findCodeCallback = null;
	private String findIdAction;
	private String findIdContext;
	
	private TaxCodeDataModel taxCodeDataModel;
	
    private TogglePanelController togglePanelController = null;
    
    private boolean copyRuleTaxableThresholdAmt = false; 
    
    private boolean copyRuleMinTaxablAmt = false; 
    
    private boolean copyRuleMaxTaxablAmt = false; 
    
    private boolean copyRuleMaxTaxAmt = false; 
    
    private boolean copyAction = false;  
    
	public void setDocRefHandler(SearchDocRefHandler docRefHandler) {
		this.docRefHandler = docRefHandler;
	}
	
	public SearchDocRefHandler getDocRefHandler() {
		return docRefHandler;
	}
	

	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TaxCodePanel", true);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	

	public TaxCodeDataModel getTaxCodeDataModel() {
		return taxCodeDataModel;
	}

	public void setTaxCodeDataModel(TaxCodeDataModel taxCodeDataModel) {
		this.taxCodeDataModel = taxCodeDataModel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<SelectItem> getRefTypeList() {
		if (refTypeList == null) {
			refTypeList = new ArrayList<SelectItem>();
			refTypeList.add(new SelectItem("", "Select Type Code"));
			for (ListCodes code : cacheManager.getListCodesByType("REFTYPE")) {
				refTypeList.add(new SelectItem(code.getCodeCode(), code.getCodeCode() + " - " + code.getDescription()));
			}
		}
		return refTypeList;
	}
	
	public Long getDisplayBackdateWarning() {
		return displayBackdateWarning;
	}

	public void setDisplayBackdateWarning(Long displayBackdateWarning) {
		this.displayBackdateWarning = displayBackdateWarning;
	}
	
	public void cancelWarning(ActionEvent e) {
		displayBackdateWarning = 0L;
	}

	public void setRefTypeList(List<SelectItem> refTypeList) {
		this.refTypeList = refTypeList;
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
    
	public ReferenceDocument getSelectedReferenceDocument() {
		return selectedReferenceDocument;
	}

	public void setSelectedReferenceDocument(
			ReferenceDocument selectedReferenceDocument) {
		this.selectedReferenceDocument = selectedReferenceDocument;
	}

	public interface SearchCallback {
		void searchCallback(String context);
	}
	
	private SearchCallback  searchCallback  = null;
	private String searchContext;
	
	public void search(SearchCallback callback, String context) {
		searchCallback = callback;
		searchContext = context;
	}
	
	public void exitSearchMode() {
		if (searchCallback != null) {
			searchCallback = null;
			searchContext = "";
		}
	}
	
	public void searchAction() {
		if (searchCallback != null){
			searchCallback.searchCallback(searchContext);
		}
	}
	
	public TaxCode getSelectedTaxCode() {
		return selectedTaxCode;
	}

	public void setSelectedTaxCode(TaxCode selectedTaxCode) {
		this.selectedTaxCode = selectedTaxCode;
	}

	public int getSelectedTaxCodeIndex() {
		return selectedTaxCodeIndex;
	}

	public void setSelectedTaxCodeIndex(int selectedTaxCodeIndex) {
		this.selectedTaxCodeIndex = selectedTaxCodeIndex;
	}

	public List<TaxCode> getTaxCodeList() {
		if(taxCodeList == null) {
			if(getFindMode()){
				//Append "%"
				String taxCode = (enteredTaxCode!=null && enteredTaxCode.trim().length()>0) ? ("%"+enteredTaxCode.trim()+"%") : "";
				String desc = (enteredDesc!=null && enteredDesc.trim().length()>0) ? ("%"+enteredDesc.trim()+"%") : "";
				taxCodeList = taxCodeService.findTaxCodeList(taxCode, desc, selectedActiveInactive);
			}
			else{
				taxCodeList = taxCodeService.findTaxCodeList(enteredTaxCode, enteredDesc, selectedActiveInactive);
			}
		}
		
		return taxCodeList;
	}

	public void setTaxCodeList(List<TaxCode> taxCodeList) {
		this.taxCodeList = taxCodeList;
	}

	public String getEnteredTaxCode() {
		return enteredTaxCode;
	}

	public void setEnteredTaxCode(String enteredTaxCode) {
		this.enteredTaxCode = enteredTaxCode;
	}

	public String getEnteredDesc() {
		return enteredDesc;
	}

	public void setEnteredDesc(String enteredDesc) {
		this.enteredDesc = enteredDesc;
	}

	public String taxCodeSearchFilter() {
		taxCodeList = null;
		resetSelection();
		getTaxCodeList();
		return null;
	}
	
	public void sortAction(ActionEvent e) {
		taxCodeDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public String taxCodeResetFilter() {
		enteredTaxCode = "";
		enteredDesc = "";
		selectedActiveInactive = "-1";
		return null;
	}
	
	public void findCode(FindCodeCallback callback, String context, String action, String code) {
		//Called by calling bean to set return page
		findCodeCallback = callback;
		findIdContext = context;	//"TaxcodeCode"
		findIdAction = action;		//calling page

		taxCodeResetFilter();
		taxCodeSearchFilter();
		
		if (code != null) {
			enteredTaxCode = code;				
		}
	}
	
	public String findAction() {
		//Called by TaxCode Lookup screen and return back to calling screen
		if(selectedTaxCodeSource!=null && selectedTaxCodeSource.getId()!=null){
			findCodeCallback.findCodeCallback(selectedTaxCodeSource.getTaxCodePK().getTaxcodeCode(), findIdContext);
		}
		else{
			findCodeCallback.findCodeCallback("", findIdContext);
		}
	
		exitFindMode();
		return findIdAction;
	}

	public String cancelFindAction() {
		exitFindMode();
		return findIdAction;
	}

	public boolean getFindMode() {
		return (findCodeCallback != null);
	}

	public void exitFindMode() {
		if(findCodeCallback != null) {
			findCodeCallback = null;
			taxCodeResetFilter();
			taxCodeSearchFilter();
		}
		else{
			if(taxCodeList == null) {
				getTaxCodeList();
			}
		}
	}

	public String navigateAction() {
		exitFindMode();	
		return "taxability_codes";
	}
	
	public String getActionText() {
		return currentAction.getActionText();
	}
	
	public void selectedTaxCodeChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedTaxCodeSource = (TaxCode) table.getRowData();
		this.selectedTaxCodeIndex = table.getRowIndex();
	}
	
	public void selectedTaxcodeRuleChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedTaxCodeRule = (TaxCodeDetailDTO) table.getRowData();
		this.selectedTaxCodeRuleIndex = table.getRowIndex();
	}
	
	public void selectedRefDocChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedRefDocDetail = (ReferenceDetail) table.getRowData();
		this.selectedReferenceDocument = this.selectedRefDocDetail.getReferenceDetailPK().getRefDoc();
		this.selectedRefDocDetailIndex = table.getRowIndex();
	}
		
	public void validateTaxCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure name is unique
		if (currentAction.isAddAction()) {
			TaxCodePK id = new TaxCodePK();
			id.setTaxcodeCode((String) value);
			if (taxCodeService.findById(id) != null) {
				((UIInput)toValidate).setValid(false);
				FacesMessage message = new FacesMessage("Tax Code already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(toValidate.getClientId(context), message);
			}
	    }
	}
	
	public boolean getAddAction() {
		return currentAction.isAddAction();
	}

	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getReadOnlyAction() {
		return currentAction.isReadOnlyAction();
	}
	
	public boolean getViewOnlyAction() {
		return currentAction.equals(EditAction.VIEW) || currentAction.equals(EditAction.VIEW_FROM_TRANSACTION) || currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY)
				|| currentAction.equals(EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public boolean getCopyUpdateAction() {
		return currentAction.equals(EditAction.COPY_UPDATE);
	}
	
	public String displayAddAction() {
		currentAction = EditAction.ADD;
		
		TaxCodePK taxCodePK = new TaxCodePK();
		taxCodePK.setTaxcodeCode(null);
		selectedTaxCode = new TaxCode();
		selectedTaxCode.setTaxCodePK(taxCodePK); 
		selectedTaxCode.setDescription(null);
		selectedTaxCode.setActiveFlag("1");

		return "taxability_codes_add";
	}
	
	public static void main(String[] ssss){
		String code = "13344123333";
		boolean foundLetter = false;
		try{
			byte[] bytes = code.getBytes("US-ASCII");
			for(int i=0; i< bytes.length; i++){
				System.out.println(bytes[i]);
				if((bytes[i]>=65 && bytes[i]<=90) || (bytes[i]>=97 && bytes[i]<=122)){
					foundLetter = true;
					break;
				}	
			}
		}
		catch(Exception e){	
		}
		
		System.out.println(foundLetter);
	}
	
	public boolean verifyTaxCodeData(){
		boolean errMessage = false; 

		//4071
		if(selectedTaxCode.getTaxCodePK().getTaxcodeCode().contains("%")){
			FacesMessage message = new FacesMessage("TaxCode cannot contain % sign. Please re-enter.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errMessage = true;
		}
		
		if(selectedTaxCode.getDescription()!=null && selectedTaxCode.getDescription().contains("%")){
			FacesMessage message = new FacesMessage("Description cannot contain % sign. Please re-enter.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errMessage = true;
		}
		
		boolean foundLetter = false;
		try{
			byte[] bytes = selectedTaxCode.getTaxCodePK().getTaxcodeCode().getBytes("US-ASCII");
			for(int i=0; i< bytes.length; i++){
				if((bytes[i]>=65 && bytes[i]<=90) || (bytes[i]>=97 && bytes[i]<=122)){
					foundLetter = true;
					break;
				}	
			}
		}
		catch(Exception e){	
		}
		
		if(!foundLetter){
			FacesMessage message = new FacesMessage("TaxCode must contain an alphabet letter. Please re-enter.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errMessage = true;
		}
		
		return errMessage;
	}
	
	public String okTaxCodeAction() {
		switch (currentAction) {
			case ADD:		
				if (verifyTaxCodeData()) {
					return null;
				}

				selectedTaxCode.setCustomFlag("1");
				taxCodeService.save(selectedTaxCode);
				break;
			case COPY_ADD:
				if (verifyTaxCodeData()) {
					return null;
				}
				
				selectedTaxCode.setCustomFlag("1");
				taxCodeService.save(selectedTaxCode);
				//save rules
				if(taxCodeDetailsForCopy != null) {
					if(selectedTaxCode.getCopyRulesBooleanFlag()) {
						for(TaxCodeDetail tcd : taxCodeDetailsForCopy) {
							TaxCodeDetail ntcd = new TaxCodeDetail();
							ntcd.setTaxcodeCode(selectedTaxCode.getTaxcodeCode());
							ntcd.setTaxcodeCountryCode(tcd.getTaxcodeCountryCode());
							ntcd.setTaxcodeStateCode(tcd.getTaxcodeStateCode());
							ntcd.setTaxCodeCounty(tcd.getTaxCodeCounty());
							ntcd.setTaxCodeCity(tcd.getTaxCodeCity());		
							ntcd.setEffectiveDate(tcd.getEffectiveDate());
							ntcd.setExpirationDate(tcd.getExpirationDate());
							ntcd.setTaxcodeTypeCode(tcd.getTaxcodeTypeCode());
							ntcd.setTaxtypeCode(tcd.getTaxtypeCode());
							ntcd.setRateTypeCode(tcd.getRateTypeCode());
							ntcd.setTaxableThresholdAmt(tcd.getTaxableThresholdAmt());
							ntcd.setBaseChangePct(tcd.getBaseChangePct());
							ntcd.setSpecialRate(tcd.getSpecialRate());
							
							ntcd.setTaxCodeStj(tcd.getTaxCodeStj());//0009055
							ntcd.setJurLevel(tcd.getJurLevel());
							ntcd.setCustomFlag("1");
							ntcd.setSortNo(tcd.getSortNo());
							
							ntcd.setModifiedFlag("0");
							ntcd.setActiveFlag(tcd.getActiveFlag());
							taxCodeDetailService.save(ntcd);
							
							List<ReferenceDetail> rdList = taxCodeDetailService.getReferenceDetails(tcd.getTaxcodeDetailId());
							if(rdList != null) {
								for (ReferenceDetail rd : rdList) {
									ReferenceDetail nrd = new ReferenceDetail();
									ReferenceDetailPK id = new ReferenceDetailPK();
									id.setTaxcodeDetail(taxCodeDetailService.findById(ntcd.getTaxcodeDetailId()));
									id.setRefDoc(rd.getReferenceDetailPK().getRefDoc());
									nrd.setReferenceDetailPK(id);
									taxCodeDetailService.addDocumentReference(nrd);
								}
							}
						}
					}
				
				}
				
				break;
			case DELETE:
				if(taxCodeDetailService.isTaxCodeUsed(selectedTaxCode)){
					//Update active_flag = �0�
					selectedTaxCode.setModifiedFlag("1");
					selectedTaxCode.setActiveFlag("0");
					taxCodeService.update(selectedTaxCode);
				}
				else{
					//Delete TaxCode
					try {
						taxCodeDetailService.removeTaxCode(selectedTaxCode);
					} catch (Exception ex) {
						FacesMessage message = new FacesMessage("Cannot remove TaxCode detail record that is in use.");
						message.setSeverity(FacesMessage.SEVERITY_ERROR);
						FacesContext.getCurrentInstance().addMessage(null, message);
						return null;
					}
				}
				break;
				
			case UPDATE:
				selectedTaxCode.setModifiedFlag("1");
				taxCodeService.update(selectedTaxCode);
				selectedTaxCode = taxCodeService.findById(selectedTaxCode.getId());
				break;
			case VIEW_FROM_TRANSACTION:
			case VIEW_FROM_TRANSACTION_LOG_HISTORY:
			case VIEW_FROM_TRANSACTION_LOG_ENTRY:
				return getReturnView();
		}
		
		taxCodeList = null;
		resetSelection();
		getTaxCodeList();
		return "taxability_codes";
	}
	
	public boolean isValidSelection() {
		return selectedTaxCodeSource != null;
	}
	
	public void resetSelection() {
		selectedTaxCode = null;
		selectedTaxCodeIndex = -1;
		selectedTaxCodeSource = null;
	}
	
	public String displayUpdateAction() {
		currentAction = EditAction.UPDATE;
		selectedTaxCode = taxCodeService.findById(selectedTaxCodeSource.getId());
		return "taxability_codes_add";
	}
	
	public String displayDeleteAction() {
		currentAction = EditAction.DELETE;
		selectedTaxCode = taxCodeService.findById(selectedTaxCodeSource.getId());
		if(taxCodeDetailService.isTaxCodeUsed(selectedTaxCode)){
			//Update active_flag = ��
			FacesMessage message = new FacesMessage("TaxCode used on Goods & Services Matrix(s) and cannot be deleted. Click Ok to set to Inactive.");

			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		return "taxability_codes_add";
	}
	
	public String displayViewTaxCodeAction() {
		currentAction = EditAction.VIEW;
		selectedTaxCode = taxCodeService.findById(selectedTaxCodeSource.getId());
		return "taxability_codes_add";
	}
	
	public String viewFromTransaction(TaxCodePK id, String view) {
		currentAction = EditAction.VIEW_FROM_TRANSACTION;
		this.returnView = view;
		selectedTaxCode = taxCodeService.findById(id);
		return "taxability_codes_add";
	}
	
	public String displayCopyAction() {
		currentAction = EditAction.COPY_ADD;
		selectedTaxCode = taxCodeService.findById(selectedTaxCodeSource.getId());
		selectedTaxCode.setTaxCodePK(new TaxCodePK());
		
		taxCodeDetailsForCopy = taxCodeDetailService.findTaxCodeDetailListByTaxCode(selectedTaxCodeSource.getTaxcodeCode());
		
		return "taxability_codes_add";
	}
	
	private String returnRuleUrl = null;
	private Boolean viewRuleOnly = false;
	private String viewTaxcodeCode = null;
	
	public void setReturnRuleUrl(String returnRuleUrl){
		this.returnRuleUrl = returnRuleUrl;
	}
	
	public String getReturnRuleUrl(){
		return returnRuleUrl;
	}
	
	public void setViewRuleOnly(Boolean viewRuleOnly){
		this.viewRuleOnly = viewRuleOnly;
	}
	
	public Boolean getViewRuleOnly(){
		return viewRuleOnly;
	}
	
	public void setViewTaxcodeCode(String viewTaxcodeCode){
		this.viewTaxcodeCode = viewTaxcodeCode;
	}
	
	public String getViewTaxcodeCode(){
		return viewTaxcodeCode;
	}
	
	
	public String viewAction() {
		return returnRuleUrl;
	}
	
	public String cancelTaxCodeAction() {
		return "taxability_codes";
	}
	
	public String displayRulesView() {
		//Before call this, viewTaxcodeCode & returnRuleUrl need to be set
		
		//0004440
		viewRuleOnly = true;
		
		return displayRulesScreen();
	}
	
	public String displayRulesAction() {

		//0004440
		viewRuleOnly = false;	
		returnRuleUrl = "taxability_codes";
		viewTaxcodeCode = selectedTaxCodeSource.getTaxcodeCode();
		
		return displayRulesScreen();
	}
	
	public String displayRulesScreen() {
		if(viewTaxcodeCode==null || viewTaxcodeCode.trim().length()==0){
			return null;
		}
		
		selectedTaxCode = taxCodeService.findById(new TaxCodePK(viewTaxcodeCode));
		
		selectedJurisdiction = null;
		selectedJurisdictionIndex = -1;
		selectedJurisCountryCode = null;
		selectedJurisStateCode = null;
		
		selectedTaxCodeRule = null;
		selectedTaxCodeRuleIndex = -1;
		
		jurisdictionList = null;
		taxCodeRuleList = null;
		
		selectionMap.clear();
		filterHandler.reset();
		filterHandler.setCountry(getDefUserCountry());
		
		return "taxability_codes_rules";
	}

	public JurisdictionFilterHandler getFilterHandler() {
		return filterHandler;
	}

	public void setFilterHandler(JurisdictionFilterHandler filterHandler) {
		this.filterHandler = filterHandler;
	}
	
	public JurisdictionUpdateHandler getUpdateHandler() {
		return updateHandler;
	}

	public void setUpdateHandler(JurisdictionUpdateHandler updateHandler) {
		this.updateHandler = updateHandler;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		filterHandler.setCacheManager(cacheManager);
		filterHandler.setJurisdictionService(jurisdictionService);
		filterHandler.setTaxCodeStateService(taxCodeStateService);
		filterHandler.setCountry(getDefUserCountry());
		
		docRefHandler.setCacheManager(cacheManager);
		docRefHandler.setReferenceDocumentService(referenceDocumentService);
		updateHandler.setCacheManager(cacheManager);
	}
	
	public String getDefUserCountry() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFUSERCOUNTRY", "USER", userCode));
		return option.getValue();
	}
	
	public String getDefUserTaxability() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFTAXCODETYPE", "USER", userCode));
		return option.getValue();
	}
	
	public String getDefUserTaxType() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFTAXTYPECODE", "USER", userCode));
		return option.getValue();
	}
	
	public String getDefUserRateType() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		String userCode = userDTO.getUserCode(); 
		Option option = optionService.findByPK(new OptionCodePK ("DEFRATETYPECODE", "USER", userCode));
		return option == null ? "" : option.getValue();
	}
	
	public void clearSelections() {
		if(selectionMap != null && selectionMap.size() > 0) {
			if(jurisdictionList != null) {
				for(TaxCodeDetailDTO tcd : jurisdictionList) {
					String key = tcd.getTaxcodeCountryCode() + "-" + tcd.getTaxcodeStateCode() + "-" + tcd.getTaxCodeCounty() + "-" + tcd.getTaxCodeCity();
					if(selectionMap.containsKey(key)) {
						tcd.setSelected(false);
					}
				}
			}
			selectionMap.clear();
		}
	}
	
	public String rulesSearchFilterAction() {
		String taxcodeCode = selectedTaxCode.getTaxcodeCode();
		String country = filterHandler.getCountry();
		String state = (filterHandler.getState()==null || filterHandler.getState().length()==0) ? null : filterHandler.getState();
		String county = (filterHandler.getCounty()==null || filterHandler.getCounty().length()==0) ? null : filterHandler.getCounty();
		String city = (filterHandler.getCity()==null || filterHandler.getCity().length()==0) ? null : filterHandler.getCity();
		String stj = (filterHandler.getStj()==null || filterHandler.getStj().length()==0) ? null : filterHandler.getStj();
		
		jurisdictionList = taxCodeDetailService.getTaxcodeDetailsJurisList(taxcodeCode, country, state, county, city, stj);
		selectedJurisdiction = null;
		selectedJurisdictionIndex = -1;
		selectedJurisCountryCode = null;
		selectedJurisStateCode = null;
		taxCodeRuleList = null;
		selectedTaxCodeRule = null;
		selectedTaxCodeRuleIndex = -1;
		jurisTablePageNumber = 1;
		return null;
	}
	
	public String rulesClearFilterAction() {
		filterHandler.reset();
		return null;
	}

	public List<TaxCodeDetailDTO> getJurisdictionList() {
		if(jurisdictionList == null) {
			jurisdictionList = taxCodeDetailService.getTaxcodeDetailsJurisList(selectedTaxCode.getTaxcodeCode(), getDefUserCountry(), null, null, null, null);
			jurisTablePageNumber = 1;
		}
		
		return jurisdictionList;
	}
	
	public String showLocalTaxabilityAction() {
		if(this.selectedJurisCountryCode != null && this.selectedJurisStateCode != null) {
			filterHandler.reset();
			jurisdictionList = taxCodeDetailService.getTaxcodeDetails(selectedTaxCode.getTaxcodeCode(), this.selectedJurisCountryCode, 
					this.selectedJurisStateCode, 
					"*ALL".equals(filterHandler.getCounty()) ? null : filterHandler.getCounty(), 
					"*ALL".equals(filterHandler.getCity()) ? null : filterHandler.getCity(), true, false, false);
			selectedJurisdiction = null;
			selectedJurisdictionIndex = -1;
			selectedJurisCountryCode = null;
			selectedJurisStateCode = null;
			taxCodeRuleList = null;
			jurisTablePageNumber = 1;
		}
		return null;
	}
	
	public void selectedJurisdictionChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedJurisdiction = (TaxCodeDetailDTO) table.getRowData();
		this.selectedJurisdictionIndex = table.getRowIndex();
		this.taxCodeRuleList = null;
		this.selectedTaxCodeRule = null;
		this.selectedTaxCodeRuleIndex = -1;
	}
	
	public void checkboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		HtmlDataTable table = (HtmlDataTable) chk.getParent().getParent();
		TaxCodeDetailDTO tcd = (TaxCodeDetailDTO) table.getRowData();
		String key = tcd.getTaxcodeCountryCode() + "-" + tcd.getTaxcodeStateCode() + "-" + tcd.getTaxCodeCounty() + "-" + tcd.getTaxCodeCity();
		if((Boolean) chk.getValue()) {
			selectionMap.put(key, tcd);
		}
		else {
			selectionMap.remove(key);
		}
	}

	public List<TaxCodeDetailDTO> getTaxCodeRuleList() {
		if(taxCodeRuleList == null && selectedJurisdiction != null && selectedTaxCode != null) {
			
			taxCodeRuleList = taxCodeDetailService.getTaxcodeRules(selectedTaxCode.getTaxcodeCode(), 
					selectedJurisdiction.getTaxcodeCountryCode(), 
					selectedJurisdiction.getTaxcodeStateCode(), 
					selectedJurisdiction.getTaxCodeCounty(), 
					selectedJurisdiction.getTaxCodeCity(),
					selectedJurisdiction.getTaxCodeStj(),
					selectedJurisdiction.getJurLevel());
		}
		return taxCodeRuleList;
		
	}

	public void setTaxCodeRuleList(List<TaxCodeDetailDTO> taxCodeRuleList) {
		this.taxCodeRuleList = taxCodeRuleList;
	}
	
	public boolean getJurisChecked() {
		return selectionMap != null && selectionMap.size() > 0;
	}
	
	public boolean getJurisSelected() {
		return selectedJurisdiction != null;
	}
	
	public boolean isRuleSelected() {
		return selectedTaxCodeRuleIndex > -1;
	}
	
	public Boolean getEffectivedateSelection() {
		if(selectedTaxCodeRuleIndex > -1){
			if(loginBean.getUser().getAdminFlag()!=null && 
					(loginBean.getUser().getAdminFlag().equals("1") || 
							loginBean.getUser().getAdminFlag().equals("2"))){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	public boolean getIsOverride(){
		if(updateHandler.getLevel()!=null && updateHandler.getLevel().equalsIgnoreCase("*STJ")){			
			return false;
		}
		
		return true;
	}
	
	public boolean updateFields(TaxCodeDetail td, TaxCodeDetailDTO updateTaxCodeRule){	
		
		if(maxTaxAmountChecked==true){
			if(maxTaxAmountOption.equals("1")){
				td.setTaxableThresholdAmt(updateTaxCodeRule.getTaxableThresholdAmt());
				updateTaxCodeRule.setMinimumTaxableAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMaximumTaxableAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMaximumTaxAmt(ArithmeticUtils.ZERO);
			}
			else{
				td.setTaxableThresholdAmt(ArithmeticUtils.ZERO);
			}
			
			if(maxTaxAmountOption.equals("2")){
				td.setMinimumTaxableAmt(updateTaxCodeRule.getMinimumTaxableAmt());
				updateTaxCodeRule.setTaxableThresholdAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMaximumTaxableAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMaximumTaxAmt(ArithmeticUtils.ZERO);
			}
			else{
				td.setMinimumTaxableAmt(ArithmeticUtils.ZERO);
			}
			
			if(maxTaxAmountOption.equals("3")){
				td.setMaximumTaxableAmt(updateTaxCodeRule.getMaximumTaxableAmt());
				updateTaxCodeRule.setTaxableThresholdAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMinimumTaxableAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMaximumTaxAmt(ArithmeticUtils.ZERO);
			}
			else{
				td.setMaximumTaxableAmt(ArithmeticUtils.ZERO);
			}
			
			if(maxTaxAmountOption.equals("4")){
				td.setMaximumTaxAmt(updateTaxCodeRule.getMaximumTaxAmt());	
				updateTaxCodeRule.setMaximumTaxableAmt(ArithmeticUtils.ZERO);;
				updateTaxCodeRule.setTaxableThresholdAmt(ArithmeticUtils.ZERO);
				updateTaxCodeRule.setMinimumTaxableAmt(ArithmeticUtils.ZERO);
				
				//td.setTaxProcessTypeCode(updateTaxCodeRule.getTaxProcessTypeCode());
				//td.setGroupByDriver(updateTaxCodeRule.getGroupByDriver());
			}
			else{
				td.setMaximumTaxAmt(ArithmeticUtils.ZERO);
				//td.setTaxProcessTypeCode("");
				//td.setGroupByDriver("");
			}
			
			td.setTaxProcessTypeCode(updateTaxCodeRule.getTaxProcessTypeCode());
			td.setGroupByDriver(updateTaxCodeRule.getGroupByDriver());
		}
		else{
			td.setTaxableThresholdAmt(ArithmeticUtils.ZERO);
			td.setMinimumTaxableAmt(ArithmeticUtils.ZERO);
			td.setMaximumTaxableAmt(ArithmeticUtils.ZERO);
			td.setMaximumTaxAmt(ArithmeticUtils.ZERO);
			td.setTaxProcessTypeCode("");
			td.setGroupByDriver("");
		}
		if(specialRateChecked==true){
			
			boolean specialRateZeroNull = false;
			boolean specialSetAmtZeroNull = false;
			if(updateTaxCodeRule.getSpecialRate()==null || updateTaxCodeRule.getSpecialRate().compareTo(ArithmeticUtils.ZERO)==0){
				specialRateZeroNull = true;
			}
			if(updateTaxCodeRule.getSpecialSetAmt()==null || updateTaxCodeRule.getSpecialSetAmt().compareTo(ArithmeticUtils.ZERO)==0){
				specialSetAmtZeroNull = true;
			}
				
			if(specialRateZeroNull && specialSetAmtZeroNull){
				FacesMessage message = new FacesMessage("When Special Rate or Set Amount is checked, a Special Rate or Set Amount must be entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
			else if(specialSetAmtZeroNull && updateTaxCodeRule.getSpecialRate().compareTo(ArithmeticUtils.ZERO)==-1) {
				FacesMessage message = new FacesMessage("A Special Rate > 0 must be entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
			else if(specialRateZeroNull && updateTaxCodeRule.getSpecialSetAmt().compareTo(ArithmeticUtils.ZERO)==-1) {
				FacesMessage message = new FacesMessage("A Set Amount > 0 must be entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}	
			else if(!specialRateZeroNull && !specialSetAmtZeroNull) {
				FacesMessage message = new FacesMessage("Cannot enter both Special Rate and Set Amount.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
		}
		
		if(maxTaxAmountChecked==true){
			if(maxTaxAmountOption.equals("1")){	
				if(updateTaxCodeRule.getTaxableThresholdAmt()==null || updateTaxCodeRule.getTaxableThresholdAmt().compareTo(ArithmeticUtils.ZERO)<1){
					FacesMessage message = new FacesMessage("When Taxable Threadhold Amount is selected, an amount > 0 must be entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}
			else if(maxTaxAmountOption.equals("2")){
				if(updateTaxCodeRule.getMinimumTaxableAmt()==null || updateTaxCodeRule.getMinimumTaxableAmt().compareTo(ArithmeticUtils.ZERO)<1){
					FacesMessage message = new FacesMessage("When Minimum Taxable Amount is selected, an amount > 0 must be entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}	
			else if(maxTaxAmountOption.equals("3")){
				if(updateTaxCodeRule.getMaximumTaxableAmt()==null || updateTaxCodeRule.getMaximumTaxableAmt().compareTo(ArithmeticUtils.ZERO)<1){
					FacesMessage message = new FacesMessage("When Maximum Taxable Amount is selected, an amount > 0 must be entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}	
			else if(maxTaxAmountOption.equals("4")){
				if(updateTaxCodeRule.getMaximumTaxAmt()==null || updateTaxCodeRule.getMaximumTaxAmt().compareTo(ArithmeticUtils.ZERO)<1){
					FacesMessage message = new FacesMessage("When Maximum Tax Amount is selected, an amount > 0 must be entered.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return false;
				}
			}
			else{
				FacesMessage message = new FacesMessage("When MaxTax Amount is checked, a MaxTax option must be selected. and an amount > 0 entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
			
			if(updateTaxCodeRule.getTaxProcessTypeCode()==null || updateTaxCodeRule.getTaxProcessTypeCode().length()==0){
				FacesMessage message = new FacesMessage("Tax Process Type is mandatory.");//GroupByDriver is optional
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
		}
		
		if(percentBaseChecked==true){
			if(updateTaxCodeRule.getBaseChangePct()==null || updateTaxCodeRule.getBaseChangePct().compareTo(ArithmeticUtils.ZERO)<1){
				FacesMessage message = new FacesMessage("When Percent of Base is selected, an amount > 0 must be entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
		}
		
		//Distribution Rules required when Taxability = Distribute.
		if(updateTaxCodeRule.getTaxcodeTypeCode().equalsIgnoreCase("D")){
			if((updateTaxCodeRule.getAllocBucket()==null || updateTaxCodeRule.getAllocBucket().length()==0) ||
					(updateTaxCodeRule.getAllocProrateByCode()==null || updateTaxCodeRule.getAllocProrateByCode().length()==0) ||
					(updateTaxCodeRule.getAllocConditionCode()==null || updateTaxCodeRule.getAllocConditionCode().length()==0)){
				//all need to be selected
				FacesMessage message = new FacesMessage("Distribution Rules required when Taxability = Distribute.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
			}
			
		}
		else{
			updateTaxCodeRule.setAllocBucket("");
			updateTaxCodeRule.setAllocProrateByCode("");
			updateTaxCodeRule.setAllocConditionCode("");
		}
		
		if(!updateTaxCodeRule.getTaxcodeTypeCode().equalsIgnoreCase("E")){
			updateTaxCodeRule.setExemptReason("");
		}
/*		
		if((updateTaxCodeRule.getAllocBucket()==null || updateTaxCodeRule.getAllocBucket().length()==0) &&
				(updateTaxCodeRule.getAllocProrateByCode()==null || updateTaxCodeRule.getAllocProrateByCode().length()==0) &&
				(updateTaxCodeRule.getAllocConditionCode()==null || updateTaxCodeRule.getAllocConditionCode().length()==0)){
			//no selection is fine
		}
		else if((updateTaxCodeRule.getAllocBucket()!=null && updateTaxCodeRule.getAllocBucket().length()>0) &&
				(updateTaxCodeRule.getAllocProrateByCode()!=null && updateTaxCodeRule.getAllocProrateByCode().length()>0) &&
				(updateTaxCodeRule.getAllocConditionCode()!=null && updateTaxCodeRule.getAllocConditionCode().length()>0)){
			//all selection is fine
		}
		else{
			FacesMessage message = new FacesMessage("All Distribution Rules values must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
		}
	*/	
		//PP-18
		if(specialRateChecked==true){
			td.setSpecialRate(updateTaxCodeRule.getSpecialRate());
			td.setSpecialSetAmt(updateTaxCodeRule.getSpecialSetAmt());
		}
		else{
			td.setSpecialRate(ArithmeticUtils.ZERO);
			td.setSpecialSetAmt(ArithmeticUtils.ZERO);
		}
		
		
		
		if(percentBaseChecked==true){
			td.setBaseChangePct(updateTaxCodeRule.getBaseChangePct());
		}
		else{
			td.setBaseChangePct(ArithmeticUtils.ZERO);
		}
		
		td.setAllocBucket(updateTaxCodeRule.getAllocBucket());
		td.setAllocProrateByCode(updateTaxCodeRule.getAllocProrateByCode());
		td.setAllocConditionCode(updateTaxCodeRule.getAllocConditionCode());
		
		return true;
	}
	
	public void populateFields(){
		specialRateChecked=false;
		maxTaxAmountChecked=false;
		percentBaseChecked=false;	
		maxTaxAmountOption="0";
		if((updateTaxCodeRule.getSpecialRate()!=null && updateTaxCodeRule.getSpecialRate().compareTo(ArithmeticUtils.ZERO)==1) || 
				updateTaxCodeRule.getSpecialSetAmt()!=null && updateTaxCodeRule.getSpecialSetAmt().compareTo(ArithmeticUtils.ZERO)==1){
			specialRateChecked=true;
		}
		
		if(updateTaxCodeRule.getTaxableThresholdAmt()!=null && updateTaxCodeRule.getTaxableThresholdAmt().compareTo(ArithmeticUtils.ZERO)==1){
			maxTaxAmountOption="1";
			maxTaxAmountChecked=true;
			copyRuleTaxableThresholdAmt = true;
		}
		else if(updateTaxCodeRule.getMinimumTaxableAmt()!=null && updateTaxCodeRule.getMinimumTaxableAmt().compareTo(ArithmeticUtils.ZERO)==1){
			maxTaxAmountOption="2";
			maxTaxAmountChecked=true;
			copyRuleMinTaxablAmt = true;
		}
		else if(updateTaxCodeRule.getMaximumTaxableAmt()!=null && updateTaxCodeRule.getMaximumTaxableAmt().compareTo(ArithmeticUtils.ZERO)==1){
			maxTaxAmountOption="3";
			maxTaxAmountChecked=true;
			copyRuleMaxTaxablAmt = true;
		}
		else if(updateTaxCodeRule.getMaximumTaxAmt()!=null && updateTaxCodeRule.getMaximumTaxAmt().compareTo(ArithmeticUtils.ZERO)==1){
			maxTaxAmountOption="4";
			maxTaxAmountChecked=true;
			copyRuleMaxTaxAmt = true;
		}
		
		if((updateTaxCodeRule.getTaxProcessTypeCode()!=null && updateTaxCodeRule.getTaxProcessTypeCode().length()>0) ||
			(updateTaxCodeRule.getGroupByDriver()!=null && updateTaxCodeRule.getGroupByDriver().length()>0)){
			maxTaxAmountChecked=true;
		}
		
		if(updateTaxCodeRule.getBaseChangePct()!=null && updateTaxCodeRule.getBaseChangePct().compareTo(ArithmeticUtils.ZERO)==1){
			percentBaseChecked=true;
		}
	}
	
	public String taxCodeRuleAddAction() {
		copyAction = false;
		currentAction = EditAction.ADD;
		resetFields();
		
		updateTaxCodeRule = new TaxCodeDetailDTO();
		updateTaxCodeRule.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		updateTaxCodeRule.setTaxcodeTypeCode(getDefUserTaxability());
		updateTaxCodeRule.setTaxtypeCode(getDefUserTaxType());
		updateTaxCodeRule.setRateTypeCode(getDefUserRateType());
		updateTaxCodeRule.setActiveFlag("1");
		
		updateHandler.setCacheManager(cacheManager);
		updateHandler.setJurisdictionService(jurisdictionService);
		updateHandler.setTaxCodeStateService(taxCodeStateService);
		updateHandler.setOptionService(optionService);
		updateHandler.reset();
		
		return "taxability_codes_rule_add";
	}
	
	public boolean getRuleUsed(){
		return ruleUsed;
	}
	
	public void resetFields(){
		specialRateChecked = false;
		maxTaxAmountChecked = false;
		percentBaseChecked = false;
		ruleUsed = false;
		maxTaxAmountOption = "0";	
	}
	
	public String taxCodeRuleCopyUpdateAction(){	
		//String returnUrl = taxCodeRuleUpdateAction();
		updateTaxCodeRule.setTaxcodeDetailId(null);
		updateTaxCodeRule.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		updateTaxCodeRule.setUpdateUserId("");
		updateTaxCodeRule.setUpdateTimestamp(null);
		ruleUsed = false;
		currentAction = EditAction.COPY_UPDATE;

		return "taxability_codes_rule_add";	
	}
	
	public String taxCodeRuleCopyAddAction(){	
		String returnUrl = taxCodeRuleUpdateAction();
		System.out.println(":)");
		updateTaxCodeRule.setTaxcodeDetailId(null);
		updateTaxCodeRule.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		updateTaxCodeRule.setUpdateUserId("");
		updateTaxCodeRule.setUpdateTimestamp(null);
		updateHandler.createStateItems();
		updateHandler.createCountyItemsIgnoreCity();
		updateHandler.createCityItems();
			
		ruleUsed = false;
		currentAction = EditAction.COPY_ADD;

		return returnUrl;	
	}
	
	public String taxCodeRuleUpdateAction() {
		copyAction = true;
		currentAction = EditAction.UPDATE;
		resetFields();
		
		updateTaxCodeRule = new TaxCodeDetailDTO();
		BeanUtils.copyProperties(selectedTaxCodeRule, updateTaxCodeRule);
		
		updateHandler.setCacheManager(cacheManager);
		updateHandler.setJurisdictionService(jurisdictionService);
		updateHandler.setTaxCodeStateService(taxCodeStateService);
		updateHandler.setOptionService(optionService);
		//updateHandler.reset();
		updateHandler.setLevel(updateTaxCodeRule.getJurLevel());
		updateHandler.setCountry(updateTaxCodeRule.getTaxcodeCountryCode());
		updateHandler.setState(updateTaxCodeRule.getTaxcodeStateCode());
		updateHandler.setCounty(updateTaxCodeRule.getTaxCodeCounty());
		updateHandler.setCity(updateTaxCodeRule.getTaxCodeCity());
		updateHandler.setStj(updateTaxCodeRule.getTaxCodeStj());
		updateHandler.setDisplayViewOnly();
		
		populateFields();
		
		long ruleUsedCount = taxCodeDetailService.getRulesusedCount(updateTaxCodeRule.getTaxcodeDetailId());
		if(ruleUsedCount > 0) {
			ruleUsed=true;
		}
		
		return "taxability_codes_rule_add";
	}
	
	public String taxCodeRuleDeleteAction() {
		taxCodeRuleUpdateAction();
		
		currentAction = EditAction.DELETE;
		
		if(ruleUsed) {
			FacesMessage message = new FacesMessage("Rule used on Transaction(s) and cannot be deleted. Click Ok to set to Inactive.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return "taxability_codes_rule_add";
	}
	
	public String taxCodeRuleViewAction() {
		taxCodeRuleUpdateAction();
		this.returnView = null;
		currentAction = EditAction.VIEW;
		
		return "taxability_codes_rule_add";
	}
	
	public boolean getBackdateAction() {
		return currentAction.equals(EditAction.BACKDATE);
	}
	
	public String displayBackDateAction(){
		taxCodeRuleUpdateAction();
		currentAction = EditAction.BACKDATE;
		
		return "taxability_codes_rule_add";
	}

	public TaxCodeDetailDTO getSelectedJurisdiction() {
		return selectedJurisdiction;
	}

	public boolean validateEffectiveDate() {
		//Effective Date � the default is today date and it may not be earlier than today and must not already exist for the selected Jurisdiction
		boolean valid = true;
		if(currentAction.isAddAction()) {
			Date effDate = updateTaxCodeRule.getEffectiveDate();
			Date todayDate = new com.ncsts.view.util.DateConverter().getUserLocalDate();	
			if (effDate.before(todayDate)) {
				FacesMessage message = new FacesMessage("Effective date must be on or after today.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return false;
		    }
		}
		
		return valid;
	}

	public TaxCodeDetailDTO getSelectedTaxCodeRule() {
		return selectedTaxCodeRule;
	}
	
	public TaxCodeDetailDTO getUpdateTaxCodeRule() {
		return updateTaxCodeRule;
	}
	
	public List<SelectItem> getTaxabilityItems() {
		if (tcTypeList == null) {
			tcTypeList = new ArrayList<SelectItem>();
			tcTypeList.add(new SelectItem("", "Select Taxability"));
			for (ListCodes code : cacheManager.getListCodesByType("TCTYPE")) {
				// When Jurisdiction Level "*STJ" is selected: Do not display the D
				if((updateHandler.getLevel()!=null && updateHandler.getLevel().equalsIgnoreCase("*STJ")) 
						&& code.getCodeCode().equalsIgnoreCase("D")){	
				}
				else{
					tcTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
				}
			}
		}
		return tcTypeList;
	}
	
	public List<SelectItem> getTaxTypeItems() {
		if (taxTypeList == null) {
			taxTypeList = new ArrayList<SelectItem>();
			taxTypeList.add(new SelectItem("", "Select Tax Type"));
			for (ListCodes code : cacheManager.getListCodesByType("TAXTYPE")) {
				taxTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		return taxTypeList;
	}
	
	public List<SelectItem> getRateTypeItems() {
		if (rateTypeList == null) {
			rateTypeList = new ArrayList<SelectItem>();
			rateTypeList.add(new SelectItem("", "Select Rate Type"));
			for (ListCodes code : cacheManager.getListCodesByType("RATETYPE")) {
				rateTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		
		return rateTypeList;
	}
	
	private boolean specialRateChecked = false;
	private boolean maxTaxAmountChecked = false;
	private boolean percentBaseChecked = false;
	private boolean ruleUsed = false;
	private String maxTaxAmountOption = "0";
	
	public String getMaxTaxAmountOption() {
		return maxTaxAmountOption;
	}
	public void setMaxTaxAmountOption(String maxTaxAmountOption) {
		this.maxTaxAmountOption = maxTaxAmountOption;
	}
	
	public boolean getSpecialRateChecked() {
		return this.specialRateChecked;
	}
	
	public void setSpecialRateChecked(boolean specialRateChecked) {
		this.specialRateChecked = specialRateChecked;
	}
	
	public boolean getMaxTaxAmountChecked() {
		return this.maxTaxAmountChecked;
	}
	
	public void setMaxTaxAmountChecked(boolean maxTaxAmountChecked) {
		this.maxTaxAmountChecked = maxTaxAmountChecked;
	}
	
	public boolean getPercentBaseChecked() {
		return this.percentBaseChecked;
	}
	
	public void setPercentBaseChecked(boolean percentBaseChecked) {
		this.percentBaseChecked = percentBaseChecked;
	}
	
	public List<SelectItem> getServiceTypeItems() {
		if (serviceTypeList == null) {
			serviceTypeList = new ArrayList<SelectItem>();
			serviceTypeList.add(new SelectItem("", "Select Goods/Svc Type"));
			for (ListCodes code : cacheManager.getListCodesByType("GSTYPE")) {
				serviceTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		
		return serviceTypeList;
	}
	
	public List<SelectItem> getProcessTypeItems() {
		if (processTypeList == null) {
			processTypeList = new ArrayList<SelectItem>();
			List<ListCodes> processTypeCodes = cacheManager.getListCodesByType("TAXPROCTYP");
			Collections.sort(processTypeCodes, new Comparator<ListCodes>() {
				
				public int compare(ListCodes o1, ListCodes o2) {
					return Integer.valueOf(o1.getCodeCode()).compareTo(Integer.valueOf(o2.getCodeCode()));
				}
			});
			
			//processTypeList.add(new SelectItem("", "Select Processing Type"));
			for (ListCodes code : processTypeCodes) {
				processTypeList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}

		}
		
		
		return processTypeList;
	}
	
	public List<SelectItem> getGroupByDriverItems() {
		if (groupByDriverList == null) {
			groupByDriverList = new ArrayList<SelectItem>();
			groupByDriverList.add(new SelectItem("", "Select Group By Driver"));
			
			Map<String, String> fixedColumns = PurchaseTransaction.getTransactionColumnName();
/*			List<String> list = new ArrayList(fixedColumns.keySet());
			java.util.Collections..sort(fixedColumns);*/
			Map<String, DataDefinitionColumn> mappedColumns = new HashMap();
			List<DataDefinitionColumn> allColumns = dataDefinitionService.getAllDataDefinitionColumnByTable("TB_PURCHTRANS");
			for (DataDefinitionColumn column : allColumns) {
				mappedColumns.put(column.getColumnName(), column);
			}
			List<String> list = new ArrayList(fixedColumns.keySet());
			for(String name : list){
				String label = name;
				if (mappedColumns.containsKey(name) && mappedColumns.get(name).getDescription() != null) {
					label = mappedColumns.get(name).getDescription() + " (" +name+")";
				}
				groupByDriverList.add(new SelectItem(fixedColumns.get(name), label));
			}
		}
		
		return groupByDriverList;
	}
	
	public List<SelectItem> getAllocationBucketItems() {
		if (allocationBucketList == null) {
			allocationBucketList = new ArrayList<SelectItem>();
			allocationBucketList.add(new SelectItem("", "Select Distribute Bucket"));

			NumberFormat formatter = new DecimalFormat("00");
			for (int i = 1; i <= 10; i++) {
				//like "alloc_01", "Allocation Bucket 1"
				allocationBucketList.add(new SelectItem("DISTR_" + formatter.format(i) + "_AMT", "Distribution Bucket " + i));
			}
		}
		
		return allocationBucketList;
	}

	public List<SelectItem> getAllocateConditionItems() {
		if (allocateConditionList == null) {
			allocateConditionList = new ArrayList<SelectItem>();
			allocateConditionList.add(new SelectItem("", "Select Distribute Condition"));
			for (ListCodes code : cacheManager.getListCodesByType("DISTRCONCD")) {
				allocateConditionList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		
		return allocateConditionList;
	}
	
	public List<SelectItem> getAllocateByItems() {
		if (allocateByList == null) {
			allocateByList = new ArrayList<SelectItem>();
			allocateByList.add(new SelectItem("", "Select Distribute By"));
			for (ListCodes code : cacheManager.getListCodesByType("DISTRBYCD")) {
				allocateByList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		
		return allocateByList;
	}

	public List<SelectItem> getExemptReasonItems() {
		if (exemptReasonList == null) {
			exemptReasonList = new ArrayList<SelectItem>();
			exemptReasonList.add(new SelectItem("", "Select Exempt Reason"));
			for (ListCodes code : cacheManager.getListCodesByType("EXTCREASON")) {
				exemptReasonList.add(new SelectItem(code.getCodeCode(), code.getDescription()));
			}
		}
		
		return exemptReasonList;
	}
	
	public String okAddTaxCodeRuleAction() {
		String returnUrl = addTaxCodeRule();
		if(returnUrl==null){
			return null;
		}
		
		return taxCodeRuleAddAction();
	}
	
	public String doAddTaxCodeRule() {
		 //copyAction = false;
		 return addTaxCodeRule();
	}
	
	public String okTaxCodeRuleAction() {
		System.out.println(currentAction);
		switch (currentAction) {
			case ADD:
			case COPY_ADD:
				return doAddTaxCodeRule();
			case UPDATE:
				return updateTaxCodeRule();
			case COPY_UPDATE:
				return addTaxCodeRule();
			case DELETE:
				return deleteTaxCodeRule();
			case ADD_FROM_TRANSACTION:
				return addTaxCodeRuleFromTransaction();
			case VIEW:
				return returnBack();
			case VIEW_FROM_TRANSACTION:
			case VIEW_FROM_TRANSACTION_LOG_ENTRY:
				return returnBack();
		}
		
		return "taxability_codes_rules";
	}
	
	public boolean verifyData(){
		boolean errMessage = false; 
		
		Date effectTS = updateTaxCodeRule.getEffectiveDate();
		Date exprTS = updateTaxCodeRule.getExpirationDate();

		boolean hasDateIssue = false;
		if(effectTS==null || exprTS==null){
			if (effectTS==null) {  
				errMessage = true;
				FacesMessage message = new FacesMessage("Effective Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
			if (exprTS==null) {
				errMessage = true;
				FacesMessage message = new FacesMessage("Expiration Date may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				hasDateIssue = true;
			}
		}
	
		if(hasDateIssue){
			return errMessage;
		}
	
		if (effectTS!=null && exprTS!=null && exprTS.before(effectTS)) {
			errMessage = true;
			FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		} 

		return errMessage;
	}
	
	public boolean goEffectiveDate(){
		Date effectTS = updateTaxCodeRule.getEffectiveDate();
		
		GregorianCalendar yesterday = new GregorianCalendar();
		yesterday.add(GregorianCalendar.DATE, -1);
		if (!effectTS.after(yesterday.getTime())) {
			return false;
		}

		return true;
	}
	
	public String backdateAction() {
		if(getDisplayBackdateWarning() == 0L) {
			if (verifyData()) {
				return null;
			}

			long sameEffectiveDateCount = taxCodeDetailService.getTaxCodeRulesCount(updateTaxCodeRule.getTaxcodeCode(), updateTaxCodeRule.getTaxcodeCountryCode(), 
					updateTaxCodeRule.getTaxcodeStateCode(), updateTaxCodeRule.getTaxCodeCounty(), updateTaxCodeRule.getTaxCodeCity(), updateTaxCodeRule.getTaxCodeStj(), updateTaxCodeRule.getEffectiveDate(), updateTaxCodeRule.getJurLevel());
			if(sameEffectiveDateCount > 0) {
				FacesMessage message = new FacesMessage("New Effective date must not already exist for the selected Jurisdiction(s).");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			//Get min effective date
			Date minDate = taxCodeDetailService.minEffectiveDate(updateTaxCodeRule.getTaxcodeCode(), updateTaxCodeRule.getTaxcodeCountryCode(), 
					updateTaxCodeRule.getTaxcodeStateCode(), updateTaxCodeRule.getTaxCodeCounty(), updateTaxCodeRule.getTaxCodeCity(), updateTaxCodeRule.getTaxCodeStj(), updateTaxCodeRule.getJurLevel());
			if(minDate==null){
				FacesMessage message = new FacesMessage("Matrix not exist.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);	
				return null;
			}
			
			Long effectiveDateCount = taxCodeDetailService.matrixEffectiveDateCount(updateTaxCodeRule.getTaxcodeCode(), updateTaxCodeRule.getTaxcodeCountryCode(), 
					updateTaxCodeRule.getTaxcodeStateCode(), updateTaxCodeRule.getTaxCodeCounty(), updateTaxCodeRule.getTaxCodeCity(), updateTaxCodeRule.getTaxCodeStj(), updateTaxCodeRule.getJurLevel(), updateTaxCodeRule.getEffectiveDate());
			
			//Compare min effective date and new effective date
			if (effectiveDateCount > 1L && updateTaxCodeRule.getEffectiveDate().compareTo(minDate)<0){
				setDisplayBackdateWarning(1L);
				return null;
			}
			
			if(!goEffectiveDate()){
				setDisplayBackdateWarning(2L);
				return null;
			}	
		}
		else {
			setDisplayBackdateWarning(0L);
		}
		
		//Update Active field only
		TaxCodeDetail td = new TaxCodeDetail();
		td.setTaxcodeDetailId(updateTaxCodeRule.getTaxcodeDetailId());
		td.setEffectiveDate(updateTaxCodeRule.getEffectiveDate());	
		try {
			taxCodeDetailService.updateBackdate(td);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		rulesSearchFilterAction();
		this.updateTaxCodeRule = null;
		
		return "taxability_codes_rules";
	}

	private String returnBack() {
		if(this.returnView != null) {
			return this.returnView;
		}
		
		return "taxability_codes_rules";
	}

	//PWD
	private String addTaxCodeRuleFromTransaction() {
		if("".equals(selectedTaxCodeRule.getTaxcodeTypeCode())) {
			FacesMessage message = new FacesMessage("Invalid Taxability for State.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if("T".equals(selectedTaxCodeRule.getTaxcodeTypeCode())) {
			if("".equals(selectedTaxCodeRule.getTaxtypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Override Tax Type for State.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else if("".equals(selectedTaxCodeRule.getRateTypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Rate Type for State.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
				
		if(addCountyRule) {
			if("".equals(selectedTaxCodeCountyRule.getTaxcodeTypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Taxability for County.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			if("T".equals(selectedTaxCodeCountyRule.getTaxcodeTypeCode())) {
				if("".equals(selectedTaxCodeCountyRule.getTaxtypeCode())) {
					FacesMessage message = new FacesMessage("Invalid Override Tax Type for County.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				else if("".equals(selectedTaxCodeCountyRule.getRateTypeCode())) {
					FacesMessage message = new FacesMessage("Invalid Rate Type for County.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		
		if(addCityRule) {
			if("".equals(selectedTaxCodeCityRule.getTaxcodeTypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Taxability for City.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			if("T".equals(selectedTaxCodeCityRule.getTaxcodeTypeCode())) {
				if("".equals(selectedTaxCodeCityRule.getTaxtypeCode())) {
					FacesMessage message = new FacesMessage("Invalid Override Tax Type for City.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
				else if("".equals(selectedTaxCodeCityRule.getRateTypeCode())) {
					FacesMessage message = new FacesMessage("Invalid Rate Type for City.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
				
		TaxCodeDetailDTO stateRule = selectedTaxCodeRule;
		TaxCodeDetailDTO countyRule = addCountyRule ? selectedTaxCodeCountyRule : null;
		TaxCodeDetailDTO cityRule = addCityRule ? selectedTaxCodeCityRule : null;
		
		
		//StuffAddedByTaxCodeRuleAdd
		//TaxcodeCode
		//JurLevel
		//TaxCodeCountryCode
		//TaxcodeStateCode
		//TaxCodeCounty
		//TaxCodeCity
		//TaxCodeSTJ
		//EffectiveDate
		//TaxcodeTypeCode
		//TaxTypeCode
		//TaxableThresholdAmount
		//BaseChangePct
		//SpecialRate
		//CustomFlag
		//ModifiedFlag
		//ActiveFlag
		//SortNo
		//CitationRef
		//GoodSVCTypeCode
		//ExemptReason
		//UpdateUserID
		//UpdateTimestamp
		
		
		//0005054
		if(stateRule!=null){			
			stateRule.setBaseChangePct(stripTrailingZeros(stateRule.getBaseChangePct()));
			stateRule.setTaxCodeStj("*ALL");
			stateRule.setJurLevel("*STATE");	
			stateRule.setSortNo(CalculateSortNo(stateRule.getTaxcodeCountryCode(), stateRule.getTaxcodeStateCode(), stateRule.getTaxCodeCounty(), stateRule.getTaxCodeCity(), stateRule.getTaxCodeStj()));
		}
		
		if(countyRule!=null){
			countyRule.setBaseChangePct(stripTrailingZeros(countyRule.getBaseChangePct()));
			countyRule.setTaxCodeStj("*ALL");
			countyRule.setJurLevel("*COUNTY");	
			countyRule.setSortNo(CalculateSortNo(countyRule.getTaxcodeCountryCode(), countyRule.getTaxcodeStateCode(), countyRule.getTaxCodeCounty(), countyRule.getTaxCodeCity(), countyRule.getTaxCodeStj()));
		}
		
		if(cityRule!=null){
			cityRule.setBaseChangePct(stripTrailingZeros(cityRule.getBaseChangePct()));
			cityRule.setTaxCodeStj("*ALL");
			cityRule.setJurLevel("*CITY");	
			cityRule.setSortNo(CalculateSortNo(cityRule.getTaxcodeCountryCode(), cityRule.getTaxcodeStateCode(), cityRule.getTaxCodeCounty(), cityRule.getTaxCodeCity(), cityRule.getTaxCodeStj()));
		}
		
		try {
			//ProcessSuspendedTransactions Goes Here.
			Long[] ids = taxCodeDetailService.addRulesFromTransaction(stateRule, countyRule, cityRule);
			processTransactions(ids);
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
			FacesMessage message = new FacesMessage(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		searchAction();
		exitSearchMode();
		return "trans_main";
	}
	
	private  void processTransactions(Long[] ruleIds) throws Exception {
		if(ruleIds != null && ruleIds.length == 3) {
			try {			
				
				//city rule
				if(ruleIds[2] != null) 
					purchaseTransactionService.processSuspendedTransactions(ruleIds[2], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, true);
			
				//county rule
				if(ruleIds[1] != null)
					purchaseTransactionService.processSuspendedTransactions(ruleIds[1], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, true);
				

				
				if(ruleIds[1] == null && ruleIds[2] == null) {
					if(ruleIds[0] == null) {
						throw new Exception("Invalid State Rule");
					}
					
					//state rule
					purchaseTransactionService.processSuspendedTransactions(ruleIds[0], PurchaseTransactionServiceConstants.SuspendReason.SUSPEND_TAXCODE_DTL, true);
					
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
			finally{
			}
		}
		else {
			throw new Exception("Invalid rules");
		}
	}

	private String deleteTaxCodeRule() {		
		TaxCodeDetail deleteTd = taxCodeDetailService.findById(updateTaxCodeRule.getTaxcodeDetailId());
		if(deleteTd != null) {
			try {
				if(ruleUsed){
					deleteTd.setActiveFlag("0");
					taxCodeDetailService.updateDetail(deleteTd);
				}
				else{
					taxCodeDetailService.remove(deleteTd);
				}
			}
			catch(Exception e){
				FacesMessage message = new FacesMessage(e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}

		rulesSearchFilterAction();
		this.updateTaxCodeRule = null;
		
		return "taxability_codes_rules";
	}

	private String addTaxCodeRule() {

		setTaxAmounts();
		
		if(!validateEffectiveDate()) {
			return null;
		}
		
		if("".equals(updateTaxCodeRule.getTaxcodeTypeCode())) {
			FacesMessage message = new FacesMessage("Invalid Taxability.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		if("T".equals(updateTaxCodeRule.getTaxcodeTypeCode())) {
			if("".equals(updateTaxCodeRule.getTaxtypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Tax Type.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			else if("".equals(updateTaxCodeRule.getRateTypeCode())) {
				FacesMessage message = new FacesMessage("Invalid Rate Type.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}

		if(updateHandler.getLevel().equalsIgnoreCase("*COUNTRY")){
			if(updateHandler.getCountry()==null || updateHandler.getCountry().length()==0){
				FacesMessage message = new FacesMessage("Country must be selected.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else if(updateHandler.getCountry()==null || updateHandler.getCountry().length()==0 || 
			updateHandler.getState()==null || updateHandler.getState().length()==0 ||
			updateHandler.getCounty()==null || updateHandler.getCounty().length()==0 ||
			updateHandler.getCity()==null || updateHandler.getCity().length()==0 ||
			updateHandler.getStj()==null || updateHandler.getStj().length()==0){
			FacesMessage message = new FacesMessage("Country, State, County, City, and STJ must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		
		TaxCodeDetail td = new TaxCodeDetail();
		td.setTaxcodeCode(selectedTaxCode.getTaxcodeCode());
		td.setJurLevel(updateHandler.getLevel());
		td.setTaxcodeCountryCode(updateHandler.getCountry());
		td.setTaxcodeStateCode(updateHandler.getState());
		td.setTaxCodeCounty(updateHandler.getCounty());
		td.setTaxCodeCity(updateHandler.getCity());
		td.setTaxCodeStj(updateHandler.getStj());
		
		td.setEffectiveDate(updateTaxCodeRule.getEffectiveDate());
		try {
			td.setExpirationDate(sdf.parse("12/31/9999"));
		}
		catch(ParseException e){}
		
		td.setTaxcodeTypeCode(updateTaxCodeRule.getTaxcodeTypeCode());
		td.setTaxtypeCode(updateTaxCodeRule.getTaxtypeCode());
		if(!"".equals(updateTaxCodeRule.getRateTypeCode())) {
			td.setRateTypeCode(updateTaxCodeRule.getRateTypeCode());
		}
		td.setTaxableThresholdAmt(updateTaxCodeRule.getTaxableThresholdAmt());
		td.setBaseChangePct(stripTrailingZeros(updateTaxCodeRule.getBaseChangePct()));
		td.setSpecialRate(updateTaxCodeRule.getSpecialRate());
		td.setCustomFlag("1");
		td.setModifiedFlag("0");
		td.setActiveFlag(updateTaxCodeRule.getActiveFlag());
		
		//Calculate sort no
		td.setSortNo(CalculateSortNo(td.getTaxcodeCountryCode(), td.getTaxcodeStateCode(), td.getTaxCodeCounty(), td.getTaxCodeCity(), td.getTaxCodeStj()));
		
		long taxcodeJurisdInfoCount = taxCodeDetailService.getTaxCodeJurisdInfoCount(td.getTaxcodeCode(), td.getTaxcodeCountryCode(), 
				td.getTaxcodeStateCode(), td.getTaxCodeCounty(), td.getTaxCodeCity(), td.getTaxCodeStj(), td.getJurLevel());
		
		long sameEffectiveDateCount = taxCodeDetailService.getTaxCodeRulesCount(td.getTaxcodeCode(), td.getTaxcodeCountryCode(), 
				td.getTaxcodeStateCode(), td.getTaxCodeCounty(), td.getTaxCodeCity(), td.getTaxCodeStj(), td.getEffectiveDate(), td.getJurLevel());
		
		if(currentAction.equals(EditAction.COPY_UPDATE)) {
			if(taxcodeJurisdInfoCount > 0 && sameEffectiveDateCount > 0) {
				FacesMessage message = new FacesMessage("Effective date cannot be the same as the original TaxCode/Jurisdiction.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}
		else {
			if(taxcodeJurisdInfoCount > 0) {
				FacesMessage message = new FacesMessage("Same Taxcode and Jurisdiction Information must not already exist.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			//Check duplicate effective date
			if(sameEffectiveDateCount > 0) {
				FacesMessage message = new FacesMessage("Same Effective date must not already exist for the selected Jurisdiction(s).");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
		}

		if(!updateFields(td, updateTaxCodeRule)){
			return null;
		}
		
		//Distribution Bucket validation
		long bucketCount = taxCodeDetailService.getAllocBucketCount(td.getTaxcodeDetailId(), td.getAllocBucket());
		if(bucketCount > 0) {
			FacesMessage message = new FacesMessage("A Distribution Bucket can only be used once.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}

		td.setCitationRef(updateTaxCodeRule.getCitationRef());
		td.setGoodSvcTypeCode(updateTaxCodeRule.getGoodSvcTypeCode());
		td.setExemptReason(updateTaxCodeRule.getExemptReason());
		td.setUpdateUserId(Auditable.currentUserCode());
		td.setUpdateTimestamp(new Date());
		
		try {
			taxCodeDetailService.save(td);
			
			//j.setHaveRulesFlag(true);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		rulesSearchFilterAction();
		this.updateTaxCodeRule = null;
		
		return "taxability_codes_rules";
	}
	
	private BigDecimal stripTrailingZeros(BigDecimal aBigDecimal){
		if(aBigDecimal!=null){
			return aBigDecimal.round(new MathContext(10, RoundingMode.HALF_UP)).stripTrailingZeros();
		}
		else{
			return null;
		}
	}
	
	public static String allString = "*ALL";
	
	//Calculate sort no
	public static Long CalculateSortNo(String taxcodeCountry, String taxcodeState, String taxcodeCounty, String taxcodeCity, String taxcodeStj){
	
		String country = taxcodeCountry;
		if(country==null || country.trim().length()==0){
			country = allString;
		}
		else{
			country = country.trim();
		}
		
		String state = taxcodeState;
		if(state==null || state.trim().length()==0){
			state = allString;
		}
		else{
			state = state.trim();
		}
			 
		String county = taxcodeCounty; 
		if(county==null || county.trim().length()==0){
			county = allString;
		}
		else{
			county = county.trim();
		}
		
		String city = taxcodeCity; 
		if(city==null || city.trim().length()==0){
			city = allString;
		}
		else{
			city = city.trim();
		}
		
		String stj = taxcodeStj;
		if(stj==null || stj.trim().length()==0){
			stj = allString;
		}
		else{
			stj = stj.trim();
		}
		
		Long weight = 0L;
		if(!country.equalsIgnoreCase(allString)){
			weight = weight + 16L;
		}
		
		if(!state.equalsIgnoreCase(allString)){
			weight = weight + 8L;
		}
		
		if(!county.equalsIgnoreCase(allString)){
			weight = weight + 4L;
		}
		
		if(!city.equalsIgnoreCase(allString)){
			weight = weight + 2L;
		}
		
		if(!stj.equalsIgnoreCase(allString)){
			weight = weight + 1L;
		}
			
		return weight;
	}
	
	private String updateTaxCodeRule() {
		//Update Active field only
		TaxCodeDetail td = new TaxCodeDetail();
		//td.setTaxcodeDetailId(updateTaxCodeRule.getTaxcodeDetailId());
		//td.setActiveFlag(updateTaxCodeRule.getActiveFlag());

		BeanUtils.copyProperties(updateTaxCodeRule, td);
		td.setUpdateUserId(Auditable.currentUserCode());
		td.setUpdateTimestamp(new Date());
		if(!updateFields(td, updateTaxCodeRule)){
			return null;
		}
		
		//Distribution Bucket validation
		long bucketCount = taxCodeDetailService.getAllocBucketCount(td.getTaxcodeDetailId(), td.getAllocBucket());
		if(bucketCount > 0) {
			FacesMessage message = new FacesMessage("A Distribution Bucket can only be used once.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		try {
			taxCodeDetailService.updateDetail(td);
		}
		catch(Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
		
		rulesSearchFilterAction();
		this.updateTaxCodeRule = null;
		
		return "taxability_codes_rules";
	}
	
	public String taxCodeRulesViewAction() {
		if(currentAction != null && currentAction.equals(EditAction.ADD_FROM_TRANSACTION)) {
			exitSearchMode();
			return "trans_main";
		}
		else {
			return "taxability_codes_rules";
		}
	}

	public int getSelectedJurisdictionIndex() {
		return selectedJurisdictionIndex;
	}

	public int getSelectedTaxCodeRuleIndex() {
		return selectedTaxCodeRuleIndex;
	}
	
	public String taxCodeRuleAddToSelectedAction() {
		currentAction = EditAction.ADD;
		addRuleToSelected = true;
		
		selectedTaxCodeRuleIndex = -1;
		
		selectedTaxCodeRule = new TaxCodeDetailDTO();
		selectedTaxCodeRule.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		selectedTaxCodeRule.setTaxcodeTypeCode(getDefUserTaxability());
		selectedTaxCodeRule.setTaxtypeCode(getDefUserTaxType());
		selectedTaxCodeRule.setRateTypeCode(getDefUserRateType());
		selectedTaxCodeRule.setActiveFlag("1");
		
		if(selectionMap != null && selectionMap.size() == 1) {
			selectedJurisLocalTaxability = selectionMap.values().iterator().next().getLocalTaxabilityCode();
		}
		else {
			selectedJurisLocalTaxability = null;
		}
		
		return "taxability_codes_rule_add";
	}
	
	public boolean getShowJurisInAdd() {
		boolean flag = false;
		if(addRuleToSelected) {
			if(selectionMap != null && selectionMap.size() == 1) {
				selectedJurisdiction = selectionMap.values().toArray(new TaxCodeDetailDTO[]{})[0];
				flag = true;
			}
		}
		else {
			flag = true;
		}
		return flag;
	}
	
	public String refDocsAction() {
		refDocList = null;
		selectedRefDocDetail = null;
		selectedRefDocDetailIndex = -1;
		selectedReferenceDocument = null;
		return "taxability_codes_ref_docs";
	}
	
	public String refDocsCancelAction() {
		return "taxability_codes_ref_docs";
	}

	public List<ReferenceDetail> getRefDocList() {
		if(refDocList == null) {
			refDocList = taxCodeDetailService.getReferenceDetails(selectedTaxCodeRule.getTaxcodeDetailId()); 
		}
		return refDocList;
	}
	
	public Map<String, String> getRefTypeCodeDescMap() {
		if(refTypeCodeDescMap == null) {
			refTypeCodeDescMap = cacheManager.getRefTypeCodeDescMap();
		}
		
		return refTypeCodeDescMap;
	}
	
	public boolean isRefDocSelected() {
		return selectedRefDocDetailIndex > -1;
	}
	
	public String refDocAddAction() {
		currentAction = EditAction.ADD;
		
		updateRefDocDetail = new ReferenceDetail();
		ReferenceDetailPK id = new ReferenceDetailPK();
		id.setTaxcodeDetail(taxCodeDetailService.findById(selectedTaxCodeRule.getTaxcodeDetailId()));
		id.setRefDoc(new ReferenceDocument());
		updateRefDocDetail.setReferenceDetailPK(id);
		
		return "taxability_codes_ref_doc_add";
	}
	public String refDocAddNewAction() {
		currentAction = EditAction.ADD_NEW;
		
		updateRefDocDetail = new ReferenceDetail();
		ReferenceDetailPK id = new ReferenceDetailPK();
		id.setTaxcodeDetail(taxCodeDetailService.findById(selectedTaxCodeRule.getTaxcodeDetailId()));
		id.setRefDoc(new ReferenceDocument());
		updateRefDocDetail.setReferenceDetailPK(id);
		selectedReferenceDocument=new ReferenceDocument();
		return "taxability_codes_ref_doc_add_new";
	}
	
	public String refDocDeleteAction() {
		currentAction = EditAction.DELETE;
		
		updateRefDocDetail = new ReferenceDetail();
		updateRefDocDetail.setUpdateUserId(selectedRefDocDetail.getUpdateUserId());
		updateRefDocDetail.setUpdateTimestamp(selectedRefDocDetail.getUpdateTimestamp());
		
		ReferenceDocument referenceDocument = new ReferenceDocument();
		referenceDocument.setRefDocCode(selectedReferenceDocument.getRefDocCode());
		referenceDocument.setRefTypeCode(selectedReferenceDocument.getRefTypeCode());
		referenceDocument.setDescription(selectedReferenceDocument.getDescription());
		referenceDocument.setKeyWords(selectedReferenceDocument.getKeyWords());
		referenceDocument.setDocName(selectedReferenceDocument.getDocName());
		referenceDocument.setUrl(selectedReferenceDocument.getUrl());
		
		ReferenceDetailPK id = new ReferenceDetailPK();
		id.setTaxcodeDetail(taxCodeDetailService.findById(selectedTaxCodeRule.getTaxcodeDetailId()));
		id.setRefDoc(referenceDocument);
		updateRefDocDetail.setReferenceDetailPK(id);

		return "taxability_codes_ref_doc_add";
	}
	
	
	public void displayViewAction() {
		String fromView = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fromView");
		String url = null;
		if("refDocAction".equals(fromView)) {
			url = selectedReferenceDocument.getUrl();
		}
		else {
			url = docRefHandler.getSelectedItem().getUrl();
		}
		logger.debug("File url : " + url);
		processUrl(url,fromView);
		
	}
	
	private void processUrl(String url,String fromView) {
		try {
			if(url!=null &&(url.startsWith("http:") || url.startsWith("https:"))) {
				setUrlValue(url,fromView);
			}
			else if (url!=null &&(url.startsWith("www") || url.startsWith("WWW"))) {
				setUrlValue("http://"+url,fromView);
				
			} else {
				String path = null;
				String docName = null;
				setUrl(null);
				Option option = optionService.findByPK(new OptionCodePK(
						"REFDOCFILEPATH", "SYSTEM", "SYSTEM"));
				if (option != null) {
					url = option.getValue();
				}
				if("refDocAction".equals(fromView)) {
					docName = selectedReferenceDocument.getDocName();
					path = url + "\\" + selectedReferenceDocument.getDocName();
				}
				else {
					docName = docRefHandler.getSelectedItemKey().getDocName();
					path = url + "\\" + docRefHandler.getSelectedItemKey().getDocName();
				}
				logger.debug("File path : " + path);
				logger.debug("File name : " +  docName);
				try {

					File file = new File(path);
					FacesContext context = FacesContext.getCurrentInstance();
					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					FileInputStream istr = new FileInputStream(path);
					BufferedInputStream bstr = new BufferedInputStream(istr);
					int contentLength = (int) file.length(); // get the file
																// size (in
																// bytes)
					byte[] data = new byte[contentLength]; // allocate byte
															// array of right
															// size
					bstr.read(data, 0, contentLength); // read into byte array
					bstr.close();
					istr.close();
					response.setContentType("application/octet-stream");
					response.setContentLength(contentLength);
					response.setHeader("Content-disposition",
							"attachment; filename=\""
									+ docName + "\"");
					ServletOutputStream out = response.getOutputStream();
					out.write(data);
					out.flush();
					out.close();
					context.responseComplete();
				} catch (FileNotFoundException fe) {
					logger.debug("File not found");
					FacesMessage msg = new FacesMessage("Path not found");
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
			}
		} catch (IOException e) {
			logger.debug("Error reading file");
			FacesMessage msg = new FacesMessage("Path not found");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		
	}
	
	private void setUrlValue(String url,String fromView) {
		if("refDocAction".equals(fromView)) {
			setUrl(url);
		}
		else {
			docRefHandler.setUrlLookup(url);
		}

	}

	public void popupDocRefSearchAction(ActionEvent e) {
		String inputCode = (String) refDocCodeInput.getSubmittedValue();
		updateRefDocDetail.getReferenceDetailPK().getRefDoc().setRefDocCode(inputCode);
		ReferenceDocument refDoc = new ReferenceDocument();
		refDoc.setRefDocCode(inputCode);
		docRefHandler.setRefDoc(refDoc);
		docRefHandler.refCodeLookup();
	}

	
	public void docRefSelectedListener(ActionEvent e) {
		updateRefDocDetail.getReferenceDetailPK().setRefDoc(referenceDocumentService.findById(docRefHandler.getSelectedItemKey().getId()));
		docRefHandler.setInputValue(refDocCodeInput, docRefHandler.getSelectedItemKey().getId());
	}
	
	public String refDocsOkAction() {
		switch (currentAction) {
		case ADD:
			ReferenceDocument refDoc = referenceDocumentService.findById(updateRefDocDetail.getReferenceDetailPK().getRefDoc().getRefDocCode());
			if (refDoc == null) {
				FacesMessage message = new FacesMessage("Document not found.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			updateRefDocDetail.getReferenceDetailPK().setRefDoc(refDoc);
			try {
				taxCodeDetailService.addDocumentReference(updateRefDocDetail);
			} catch (DataIntegrityViolationException ex) {
				FacesMessage message = new FacesMessage("Reference already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			refDocList = null;
			selectedRefDocDetail = null;
			selectedRefDocDetailIndex = -1;
			selectedReferenceDocument = null;
			
			List<ReferenceDetail> docList = getRefDocList();
			if(docList==null || docList.size()==0) {
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(0));
			}
			else{
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(docList.size()));
			}

			break;
		case ADD_NEW:
			
			String name = selectedReferenceDocument.getDocName();
			String url= selectedReferenceDocument.getUrl();
			String key= selectedReferenceDocument.getKeyWords();
			
			if (((name == null) || name.trim().equals("")) && ((url == null) || url.trim().equals(""))) {
				FacesMessage message = new FacesMessage("Document Name or Path/URL is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			/*if (((name != null) && !name.trim().equals("")) && ((url == null) || url.trim().equals(""))) {
				FacesMessage message = new FacesMessage("Path/URL is required.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
*/			
			if ((key != null) && (key.trim().length()>100)) {
				FacesMessage message = new FacesMessage("Keywords cannot be longer than 100 characters.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			if ((name != null) && (name.trim().length()>50)) {
				FacesMessage message = new FacesMessage("Document Name cannot be longer than 50 characters.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			if ((url != null) && (url.trim().length()>255)) {
				FacesMessage message = new FacesMessage("Path/URL cannot be longer than 255 characters.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			ReferenceDocument selectedDocument = new ReferenceDocument();
			selectedDocument.setRefDocCode(selectedReferenceDocument.getRefDocCode());		
			List<ReferenceDocument> referenceDocumentList = referenceDocumentService.searchForRefDoc(selectedDocument);
			if(referenceDocumentList!=null && referenceDocumentList.size()>0){
				FacesMessage message = new FacesMessage("Reference Code already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			
			referenceDocumentService.save(selectedReferenceDocument);
			if(this.uploadedFile!=null){
				try{
					saveRefDocFile(this.uploadedFile);
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
			updateRefDocDetail.getReferenceDetailPK().setRefDoc(selectedReferenceDocument);
			try {
				taxCodeDetailService.addDocumentReference(updateRefDocDetail);
			} catch (DataIntegrityViolationException ex) {
				FacesMessage message = new FacesMessage("Reference already exists.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			refDocList = null;
			selectedRefDocDetail = null;
			selectedRefDocDetailIndex = -1;
			selectedReferenceDocument = null;
			
			List<ReferenceDetail> doc1List = getRefDocList();
			if(doc1List==null || doc1List.size()==0) {
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(0));
			}
			else{
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(doc1List.size()));
			}

			break;
		case DELETE:
			try {
				taxCodeDetailService.removeDocumentReference(updateRefDocDetail);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage(e.getMessage());
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return null;
			}
			refDocList = null;
			selectedRefDocDetail = null;
			selectedRefDocDetailIndex = -1;
			selectedReferenceDocument = null;
			
			List<ReferenceDetail> doc2List = getRefDocList();
			if(doc2List==null || doc2List.size()==0) {
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(0));
			}
			else{
				selectedTaxCodeRule.setRefdoccount(new BigDecimal(doc2List.size()));
			}

			break;
		}
		
		return "taxability_codes_ref_docs";
	}

	public ReferenceDetail getSelectedRefDocDetail() {
		return selectedRefDocDetail;
	}

	public ReferenceDetail getUpdateRefDocDetail() {
		return updateRefDocDetail;
	}

	public int getSelectedRefDocDetailIndex() {
		return selectedRefDocDetailIndex;
	}
	
	public boolean getDisableRuleItemsBasedOnTaxability() {
		boolean disable = false;
		if("E".equalsIgnoreCase(selectedTaxCodeRule.getTaxcodeTypeCode())) {
			if("L".equalsIgnoreCase(selectedJurisLocalTaxability)) {
				disable = false;
			}
			else if(selectionMap != null && selectionMap.size() > 1) {
				disable = false;
			}
			else {
				disable = true;
			}
		}
		return disable;
	}
	
	public boolean getDisableCountyRuleItemsBasedOnTaxability() {
		boolean disable = false;
		if(selectedTaxCodeCountyRule != null && "E".equalsIgnoreCase(selectedTaxCodeCountyRule.getTaxcodeTypeCode())) {
			if("L".equalsIgnoreCase(selectedJurisLocalTaxability)) {
				disable = false;
			}
			else {
				disable = true;
			}
		}
		return disable;
	}
	
	public boolean getDisableCityRuleItemsBasedOnTaxability() {
		boolean disable = false;
		if(selectedTaxCodeCityRule != null && "E".equalsIgnoreCase(selectedTaxCodeCityRule.getTaxcodeTypeCode())) {
			if("L".equalsIgnoreCase(selectedJurisLocalTaxability)) {
				disable = false;
			}
			else {
				disable = true;
			}
		}
		return disable;
	}

	public String getSelectedActiveInactive() {
		return selectedActiveInactive;
	}

	public void setSelectedActiveInactive(String selectedActiveInactive) {
		this.selectedActiveInactive = selectedActiveInactive;
	}

	public String getSelectedJurisCountryCode() {
		return selectedJurisCountryCode;
	}

	public void setSelectedJurisCountryCode(String selectedJurisCountryCode) {
		this.selectedJurisCountryCode = selectedJurisCountryCode;
	}

	public String getSelectedJurisStateCode() {
		return selectedJurisStateCode;
	}

	public void setSelectedJurisStateCode(String selectedJurisStateCode) {
		this.selectedJurisStateCode = selectedJurisStateCode;
	}

	public int getJurisTablePageNumber() {
		return jurisTablePageNumber;
	}

	public void setJurisTablePageNumber(int jurisTablePageNumber) {
		this.jurisTablePageNumber = jurisTablePageNumber;
	}
	
	public int getRowCount() {
		if(taxCodeList==null){
			return 0;
		}
		else{
			return taxCodeList.size();
		}
	}
	
	private int getFirstRow() {
		if(taxCodeList==null || taxCodeList.size()==0){
			return 0;
		}
		else{
			return 1;
		}
	}

	private int getLastRow() {
		if(taxCodeList==null || taxCodeList.size()==0){
			return 0;
		}
		else{
			return taxCodeList.size();
		}
	}
	
	public String getPageDescription() {		
		return String.format("Displaying rows %d through %d (%d matching rows)",
				getFirstRow(), getLastRow(), getRowCount());
	}

	public int getJurisTablePageSize() {
		return jurisTablePageSize;
	}

	public void setJurisTablePageSize(int jurisTablePageSize) {
		this.jurisTablePageSize = jurisTablePageSize;
	}

	public HtmlInputText getRefDocCodeInput() {
		return refDocCodeInput;
	}

	public void setRefDocCodeInput(HtmlInputText refDocCodeInput) {
		this.refDocCodeInput = refDocCodeInput;
	}
	
	private Jurisdiction jurisdictionFromTransaction;
	
	public Jurisdiction getJurisdictionFromTransaction(){
		return jurisdictionFromTransaction;
	}
	
	public void addFromTransaction(PurchaseTransaction detail) {
		sourceTransaction = detail;
		currentAction = EditAction.ADD_FROM_TRANSACTION;
		if(updateTaxCodeRule == null)
			updateTaxCodeRule = new TaxCodeDetailDTO();
		selectedTaxCode = new TaxCode();
		selectedJurisdiction = new TaxCodeDetailDTO();
		selectedTaxCodeRule = new TaxCodeDetailDTO();
		selectedTaxCodeCountyRule = new TaxCodeDetailDTO();
		selectedTaxCodeCityRule = new TaxCodeDetailDTO();
		
		hasExistingStateRule = false;
		addCountyRule = false;
		addCityRule = false;
		
		if(sourceTransaction.getShiptoJurisdictionId() == null) {
			FacesMessage message = new FacesMessage("Null or Invalid Jurisdiction");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		else if(sourceTransaction.getTaxcodeCode() == null) {
			FacesMessage message = new FacesMessage("Null or Invalid TaxCode");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return;
		}
		
		jurisdictionFromTransaction = jurisdictionService.findById(sourceTransaction.getShiptoJurisdictionId());
		if(jurisdictionFromTransaction == null) {
			jurisdictionFromTransaction = new Jurisdiction();
		}
		
		TaxCodeStatePK taxCodeStatePK = new TaxCodeStatePK();
		taxCodeStatePK.setCountry(jurisdictionFromTransaction.getCountry());
		taxCodeStatePK.setTaxcodeStateCode(jurisdictionFromTransaction.getState());
		TaxCodeStateDTO state = taxCodeStateService.findByPK(taxCodeStatePK);
		if(state == null) {
			state = new TaxCodeStateDTO();
		}
		
		TaxCodePK taxCodePk = new TaxCodePK();
		taxCodePk.setTaxcodeCode(sourceTransaction.getTaxcodeCode());
		selectedTaxCode = taxCodeService.findById(taxCodePk);
		
		selectedJurisdiction = new TaxCodeDetailDTO();
		selectedJurisdiction.setTaxcodeCountryCode(jurisdictionFromTransaction.getCountry());
		selectedJurisdiction.setTaxCodeCountryName(cacheManager.getCountryMap().get(jurisdictionFromTransaction.getCountry()));
		selectedJurisdiction.setTaxcodeStateCode(jurisdictionFromTransaction.getState());
		selectedJurisdiction.setName(state.getName());
		selectedJurisdiction.setTaxCodeCounty(jurisdictionFromTransaction.getCounty());
		selectedJurisdiction.setTaxCodeCity(jurisdictionFromTransaction.getCity());
		selectedJurisdiction.setLocalTaxabilityCode(state.getLocalTaxabilityCode());
		
		selectedJurisLocalTaxability = selectedJurisdiction.getLocalTaxabilityCode();
		
		List<TaxCodeDetailDTO> rules = taxCodeDetailService.getTaxcodeRules(sourceTransaction.getTaxcodeCode(), jurisdictionFromTransaction.getCountry(), jurisdictionFromTransaction.getState(), null, null, null, null);
		if(rules != null && rules.size() > 0) {
			for (TaxCodeDetailDTO rule : rules) {
				if("1".equals(rule.getActiveFlag()) && rule.getEffectiveDate() != null && (selectedTaxCodeRule.getEffectiveDate() == null || rule.getEffectiveDate().getTime() > selectedTaxCodeRule.getEffectiveDate().getTime())) {
					selectedTaxCodeRule = rules.get(0);
					hasExistingStateRule = true;
				}
			}
		}
		
		Date effectiveDate = null;
		if(!hasExistingStateRule) {
			effectiveDate = taxCodeDetailService.getRuleEffectiveDate(sourceTransaction.getTaxcodeCode(), sourceTransaction.getTransactionCountryCode(), sourceTransaction.getTransactionStateCode(), null, null, "S", "D");
			selectedTaxCodeRule.setTaxcodeCode(sourceTransaction.getTaxcodeCode());
			selectedTaxCodeRule.setTaxcodeCountryCode(jurisdictionFromTransaction.getCountry());
			selectedTaxCodeRule.setTaxcodeStateCode(jurisdictionFromTransaction.getState());
			selectedTaxCodeRule.setTaxCodeCounty("*ALL");
			selectedTaxCodeRule.setTaxCodeCity("*ALL");
			selectedTaxCodeRule.setEffectiveDate(effectiveDate == null ? new com.ncsts.view.util.DateConverter().getUserLocalDate() : effectiveDate);
			selectedTaxCodeRule.setTaxcodeTypeCode(getDefUserTaxability());
			selectedTaxCodeRule.setTaxtypeCode(getDefUserTaxType());
			selectedTaxCodeRule.setRateTypeCode(getDefUserRateType());
			selectedTaxCodeRule.setActiveFlag("1");
			
			//0005054
			selectedTaxCodeRule.setTaxableThresholdAmt(new BigDecimal(0));
			selectedTaxCodeRule.setTaxLimitationAmt(new BigDecimal(0));
			selectedTaxCodeRule.setBaseChangePct(new BigDecimal(0));
			selectedTaxCodeRule.setSpecialRate(new BigDecimal(0));
		}

		effectiveDate = taxCodeDetailService.getRuleEffectiveDate(sourceTransaction.getTaxcodeCode(), sourceTransaction.getTransactionCountryCode(), sourceTransaction.getTransactionStateCode(), jurisdictionFromTransaction.getCounty(), null, "S", "D");
		selectedTaxCodeCountyRule.setTaxcodeCode(sourceTransaction.getTaxcodeCode());
		selectedTaxCodeCountyRule.setTaxcodeCountryCode(jurisdictionFromTransaction.getCountry());
		selectedTaxCodeCountyRule.setTaxcodeStateCode(jurisdictionFromTransaction.getState());
		selectedTaxCodeCountyRule.setTaxCodeCounty(jurisdictionFromTransaction.getCounty());
		selectedTaxCodeCountyRule.setTaxCodeCity("*ALL");
		selectedTaxCodeCountyRule.setEffectiveDate(effectiveDate == null ? new com.ncsts.view.util.DateConverter().getUserLocalDate() : effectiveDate);
		selectedTaxCodeCountyRule.setTaxcodeTypeCode(getDefUserTaxability());
		selectedTaxCodeCountyRule.setTaxtypeCode(getDefUserTaxType());
		selectedTaxCodeCountyRule.setRateTypeCode(getDefUserRateType());
		selectedTaxCodeCountyRule.setActiveFlag("1");
		//0005054
		selectedTaxCodeCountyRule.setTaxableThresholdAmt(new BigDecimal(0));
		selectedTaxCodeCountyRule.setTaxLimitationAmt(new BigDecimal(0));
		selectedTaxCodeCountyRule.setBaseChangePct(new BigDecimal(0));
		selectedTaxCodeCountyRule.setSpecialRate(new BigDecimal(0));
		
		effectiveDate = taxCodeDetailService.getRuleEffectiveDate(sourceTransaction.getTaxcodeCode(), sourceTransaction.getTransactionCountryCode(), sourceTransaction.getTransactionStateCode(), null, jurisdictionFromTransaction.getCity(), "S", "D");
		selectedTaxCodeCityRule.setTaxcodeCode(sourceTransaction.getTaxcodeCode());
		selectedTaxCodeCityRule.setTaxcodeCountryCode(jurisdictionFromTransaction.getCountry());
		selectedTaxCodeCityRule.setTaxcodeStateCode(jurisdictionFromTransaction.getState());
		selectedTaxCodeCityRule.setTaxCodeCounty("*ALL");
		selectedTaxCodeCityRule.setTaxCodeCity(jurisdictionFromTransaction.getCity());
		selectedTaxCodeCityRule.setEffectiveDate(effectiveDate == null ? new com.ncsts.view.util.DateConverter().getUserLocalDate() : effectiveDate);
		selectedTaxCodeCityRule.setTaxcodeTypeCode(getDefUserTaxability());
		selectedTaxCodeCityRule.setTaxtypeCode(getDefUserTaxType());
		selectedTaxCodeCityRule.setRateTypeCode(getDefUserRateType());
		selectedTaxCodeCityRule.setActiveFlag("1");
		//0005054
		selectedTaxCodeCityRule.setTaxableThresholdAmt(new BigDecimal(0));
		selectedTaxCodeCityRule.setTaxLimitationAmt(new BigDecimal(0));
		selectedTaxCodeCityRule.setBaseChangePct(new BigDecimal(0));
		selectedTaxCodeCityRule.setSpecialRate(new BigDecimal(0));
	}
	
	public String getActionTextSuffix() {
		return currentAction.getActionTextSuffix();
	}

	public boolean isHasExistingStateRule() {
		return hasExistingStateRule;
	}

	public TaxCodeDetailDTO getSelectedTaxCodeCountyRule() {
		return selectedTaxCodeCountyRule;
	}

	public TaxCodeDetailDTO getSelectedTaxCodeCityRule() {
		return selectedTaxCodeCityRule;
	}

	public boolean isAddCountyRule() {
		return addCountyRule;
	}

	public void setAddCountyRule(boolean addCountyRule) {
		this.addCountyRule = addCountyRule;
	}

	public boolean isAddCityRule() {
		return addCityRule;
	}

	public void setAddCityRule(boolean addCityRule) {
		this.addCityRule = addCityRule;
	}

	public String getReturnView() {
		return returnView;
	}

	public void setReturnView(String returnView) {
		this.returnView = returnView;
	}
	
	public String viewRule(PurchaseTransaction detail, String type, String returnView) {
		sourceTransaction = detail;
		this.returnView = returnView;
		currentAction = EditAction.VIEW;
		
		TaxCodeDetail taxCodeDetail = null;
		
		if("state".equalsIgnoreCase(type)) {
			//taxCodeDetail = taxCodeDetailService.findById(detail.getStateTaxcodeDetailId());
			taxCodeDetail = taxCodeDetailService.findById(detail.getStateTaxcodeDetailId()); 
		}
		else if("county".equalsIgnoreCase(type)) {
			taxCodeDetail = taxCodeDetailService.findById(detail.getCountyTaxcodeDetailId());
		}
		else if("city".equalsIgnoreCase(type)) {
			taxCodeDetail = taxCodeDetailService.findById(detail.getCityTaxcodeDetailId());
		}
		
		if(taxCodeDetail == null) {
			FacesMessage message = new FacesMessage("Cannot find TaxCode Rule");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
				
		TaxCodePK taxCodePk = new TaxCodePK();
		taxCodePk.setTaxcodeCode(taxCodeDetail.getTaxcodeCode());
		selectedTaxCode = taxCodeService.findById(taxCodePk);
		
		selectedJurisdiction = new TaxCodeDetailDTO();
		selectedTaxCodeRule = new TaxCodeDetailDTO();
		
		TaxCodeStatePK taxCodeStatePK = new TaxCodeStatePK();
		taxCodeStatePK.setCountry(taxCodeDetail.getTaxcodeCountryCode());
		taxCodeStatePK.setTaxcodeStateCode(taxCodeDetail.getTaxcodeStateCode());
		TaxCodeStateDTO state = taxCodeStateService.findByPK(taxCodeStatePK);
		if(state == null) {
			state = new TaxCodeStateDTO();
		}
				
		selectedJurisdiction.setTaxcodeCountryCode(taxCodeDetail.getTaxcodeCountryCode());
		selectedJurisdiction.setTaxCodeCountryName(cacheManager.getCountryMap().get(taxCodeDetail.getTaxcodeCountryCode()));
		selectedJurisdiction.setTaxcodeStateCode(taxCodeDetail.getTaxcodeStateCode());
		selectedJurisdiction.setName(state.getName());
		selectedJurisdiction.setTaxCodeCounty(taxCodeDetail.getTaxCodeCounty());
		selectedJurisdiction.setTaxCodeCity(taxCodeDetail.getTaxCodeCity());
		selectedJurisdiction.setLocalTaxabilityCode(state.getLocalTaxabilityCode());
		
		selectedJurisLocalTaxability = selectedJurisdiction.getLocalTaxabilityCode();
		
		try {
			BeanUtils.copyProperties(taxCodeDetail, selectedTaxCodeRule);
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return "taxability_codes_rule_add";
	}
	private void saveRefDocFile(UploadedFile uploadFile) throws IOException{
		String filePath=null;
		Option option = optionService.findByPK(new OptionCodePK(
				"REFDOCFILEPATH", "SYSTEM", "SYSTEM"));
		if (option != null) {
			filePath = option.getValue();
		}
		String fileName=uploadedFile.getName();
		filePath=filePath+"/"+fileName;
		File destination=new File(filePath);
		InputStream input = null;
		    OutputStream output = null;
		    try {
		        input =uploadFile.getInputStream();
		        output = new FileOutputStream(destination);
		        byte[] buf = new byte[1024];
		        int bytesRead;
		        while ((bytesRead = input.read(buf)) > 0) {
		            output.write(buf, 0, bytesRead);
		        }
		    } finally {
		        input.close();
		        output.close();
		    }

	}
	public void urlResetAction(ActionEvent e) {
		url = null;
    }
	public boolean viewRuleCancelButton() {
		switch (currentAction) {
			case ADD:
				return false;
			case UPDATE:
				return false;
			case DELETE:
				return false;
			case ADD_FROM_TRANSACTION:
				return false;
			case VIEW:
				return true;
		}
		return false;
	}
	
	public void taxabilityChangeAction(ActionEvent e) {
		if(!updateTaxCodeRule.getTaxcodeTypeCode().equalsIgnoreCase("E")){
			updateTaxCodeRule.setExemptReason("");
		}
    }
	
	public void selectedTaxabilityChangeAction(ActionEvent e) {
		if(!selectedTaxCodeRule.getTaxcodeTypeCode().equalsIgnoreCase("E")){
			selectedTaxCodeRule.setExemptReason("");
		}
    }

	public String levelSelectedAction(){	
		//PP-13
		System.out.println(updateHandler.getLevel());
		System.out.println(updateTaxCodeRule.getJurLevel());
		String returnValue = null;
		if((updateHandler.getLevel().equals("*STJ") || updateHandler.getPreviousLevel().equals("*STJ")) || (updateTaxCodeRule.getTaxcodeTypeCode()!=null && updateTaxCodeRule.getTaxcodeTypeCode().equalsIgnoreCase("D"))){
			updateTaxCodeRule.setTaxcodeTypeCode("");
			
			//Reset Override Tax Rates Rules
			resetFields();		
			updateTaxCodeRule.setSpecialRate(null);
			updateTaxCodeRule.setSpecialSetAmt(null);
			updateTaxCodeRule.setTaxableThresholdAmt(null);
			updateTaxCodeRule.setMinimumTaxableAmt(null);
			updateTaxCodeRule.setMaximumTaxableAmt(null);	
			updateTaxCodeRule.setMaximumTaxAmt(null);
			updateTaxCodeRule.setTaxProcessTypeCode(null);
			updateTaxCodeRule.setGroupByDriver(null);
			updateTaxCodeRule.setBaseChangePct(null);
					
			//Reset Distribution Rules
			updateTaxCodeRule.setAllocBucket("");
			updateTaxCodeRule.setAllocProrateByCode("");
			updateTaxCodeRule.setAllocConditionCode("");
			
			
			returnValue = "taxability_codes_rule_add";
		}
		
		if(tcTypeList != null){
			tcTypeList.clear();
			tcTypeList=null;
		}
		updateHandler.setPreviousLevel(updateHandler.getLevel());
//		
		return returnValue;
	}
	
	public String viewTaxCode(String taxcodeCode,String fromview) {
		if("viewFromTransaction".equals(fromview)) {
			currentAction = EditAction.VIEW_FROM_TRANSACTION;
			returnView = "trans_view";
		}
		else if("viewFromTransactionLog".equals(fromview)) {
			currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
			returnView = "trans_log_entry_view";
		}
		else{
			currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_HISTORY;
			returnView = "trans_history";
		}

		TaxCodePK taxcodePK = new TaxCodePK(taxcodeCode);
		selectedTaxCode = taxCodeService.findById(taxcodePK);
		return "taxability_codes_add";
	}
	
	
	public String viewTaxcodeRule(String taxcodeDetailId, String fromview) {
		if("viewTransactionFromLog".equals(fromview)) {
			this.returnView = "trans_log_entry_view";
			currentAction = EditAction.VIEW_FROM_TRANSACTION_LOG_ENTRY;
		}
		else{
			this.returnView = "trans_view";
			currentAction = EditAction.VIEW_FROM_TRANSACTION;;
		}
		
		TaxCodeDetail taxCodeDetail = null;
		taxCodeDetail = taxCodeDetailService.findById(Long.valueOf(taxcodeDetailId));
		
		if(taxCodeDetail !=null){
			String view = prepareselectedJurisdictionRule(taxCodeDetail);
			return view; 
		}
		return null;
		
	}
	
private String prepareselectedJurisdictionRule(TaxCodeDetail taxCodeDetail) {
		
		TaxCodePK taxCodePk = new TaxCodePK();
		taxCodePk.setTaxcodeCode(taxCodeDetail.getTaxcodeCode());
		selectedTaxCode = taxCodeService.findById(taxCodePk);
		selectedTaxCodeRule = new TaxCodeDetailDTO();
		updateTaxCodeRule = new TaxCodeDetailDTO();
		
		try {
			//BeanUtils.copyProperties(taxCodeDetail, selectedTaxCodeRule);
			BeanUtils.copyProperties(taxCodeDetail, updateTaxCodeRule);
		}
		catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		
		updateHandler.setCacheManager(cacheManager);
		updateHandler.setJurisdictionService(jurisdictionService);
		updateHandler.setTaxCodeStateService(taxCodeStateService);
		updateHandler.setOptionService(optionService);
		updateHandler.setLevel(taxCodeDetail.getJurLevel());
		updateHandler.setCountry(taxCodeDetail.getTaxcodeCountryCode());
		updateHandler.setState(taxCodeDetail.getTaxcodeStateCode());
		updateHandler.setCounty(taxCodeDetail.getTaxCodeCounty());
		updateHandler.setCity(taxCodeDetail.getTaxCodeCity());
		updateHandler.setStj(taxCodeDetail.getTaxCodeStj());
		updateHandler.setDisplayViewOnly();
		
		return "taxability_codes_rule_add";
	}

private void setTaxAmounts() {
	if(copyAction){
		if(copyRuleTaxableThresholdAmt && (updateTaxCodeRule.getTaxableThresholdAmt() !=null && updateTaxCodeRule.getTaxableThresholdAmt().compareTo(ArithmeticUtils.ZERO) == 0)){
			updateTaxCodeRule.setTaxableThresholdAmt(null);
		}
		
		if(copyRuleMaxTaxablAmt && (updateTaxCodeRule.getMaximumTaxableAmt() !=null && updateTaxCodeRule.getMaximumTaxableAmt().compareTo(ArithmeticUtils.ZERO) == 0)){
			updateTaxCodeRule.setMaximumTaxableAmt(null);
		}
		
		if(copyRuleMaxTaxAmt && (updateTaxCodeRule.getMaximumTaxAmt() !=null && updateTaxCodeRule.getMaximumTaxAmt().compareTo(ArithmeticUtils.ZERO) == 0)){
			updateTaxCodeRule.setMaximumTaxAmt(null);
		}
		
		if(copyRuleMinTaxablAmt && (updateTaxCodeRule.getMinimumTaxableAmt()!=null && updateTaxCodeRule.getMinimumTaxableAmt().compareTo(ArithmeticUtils.ZERO) == 0)){
			updateTaxCodeRule.setMinimumTaxableAmt(null);
		}
	}
}

public void setTaxCode(ValueChangeEvent e) {
	String taxCode = (String)e.getNewValue();
	String stripTaxCode = null;
	selectedTaxCode = getSelectedTaxCode();
	stripTaxCode = stripField(taxCode);

	if(selectedTaxCode != null) {
		TaxCodePK stripTaxcodePK = new TaxCodePK();
		stripTaxcodePK.setTaxcodeCode(stripTaxCode);
		selectedTaxCode.setTaxCodePK(stripTaxcodePK);
		setSelectedTaxCode(selectedTaxCode);
	}
	else {
		TaxCode addTaxCode = new TaxCode();
		TaxCodePK stripTaxcodePK = new TaxCodePK();
		stripTaxcodePK.setTaxcodeCode(stripTaxCode);
		addTaxCode.setTaxCodePK(stripTaxcodePK); 
		setSelectedTaxCode(addTaxCode);
	}
}
public void setTaxCodedescription(ValueChangeEvent e) {
	String taxcodeDesc = (String)e.getNewValue();
	String stripDesc = null;
	selectedTaxCode = getSelectedTaxCode();
	stripDesc = stripField(taxcodeDesc);
	selectedTaxCode.setDescription(stripDesc);
}
public void setTaxCodeActiveInactive(ValueChangeEvent e) {
	String activeInactive = (String)e.getNewValue();
	selectedTaxCode = getSelectedTaxCode(); 
	if(selectedTaxCode != null) {
		if("-1".equals(activeInactive)){
			selectedTaxCode.setActiveFlag("1");
		}
		else {
			selectedTaxCode.setActiveFlag(activeInactive);
		}
	}
}

private String stripField(String fieldToStrip){
	String stripField = null;
	if(fieldToStrip.contains("%")) {
		stripField = fieldToStrip.replaceAll("%", "");
	}
	else {
		stripField = fieldToStrip;
	}
	return stripField;
}

public String findRefDocAction(){
	return "taxcodes_lookup_ref_docs";
}

public User getCurrentUser() {
	return loginBean.getUser();
}

}
