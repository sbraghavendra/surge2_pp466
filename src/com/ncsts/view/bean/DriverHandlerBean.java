package com.ncsts.view.bean;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxCommandButton;
import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.common.Util;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.domain.DriverReferencePK;
import com.ncsts.domain.Matrix;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.view.util.FacesUtils;
import com.ncsts.view.util.TogglePanelController;

public class DriverHandlerBean {

   	static private Logger logger = LoggerFactory.getInstance().getLogger(DriverHandlerBean.class);
   	
   	private HtmlInputText driverValueInput = new HtmlInputText();
   	private HtmlInputText driverDescInput = new HtmlInputText();
   	private HtmlInputText userValueInput = new HtmlInputText();
   	private HtmlAjaxCommandButton okButton;
   	
   	private String driver;
   	private Matrix matrix; 
   	private DriverNames driverName;
   	private String columnDescription;
   	private boolean setDescription;
   	private HtmlInputText input;
   	private String origDriverValue;
   	
   	private String driverValue;
   	private String driverDesc;
   	private String userValue;
   	private String callBackScreen;
   	
   	@Autowired
	private DriverReferenceService driverReferenceService;
   	
   	@Autowired
	private CacheManager cacheManager;
	
	private String selectedItemKey;
	private List<DriverReference> searchList;
	
	private TogglePanelController togglePanelController = null;
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("TransactionPanel", true);
			togglePanelController.addTogglePanel("DriverPanel", true);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public void init() {
	}
	
	public String getPageDescription() {
		if(searchList!=null && searchList.size()>0){		
			return String.format("Displaying rows %d through %d (%d matching rows)",
				1, searchList.size(), searchList.size());
		}
		else{
			return String.format("Displaying rows %d through %d (%d matching rows)",
					0, 0, 0);
		}
	}
	
	public String callBackAction() {
		return callBackScreen;
	}
	
	public void setCallBackScreen(String callBackScreen){
		this.callBackScreen = callBackScreen;
	}
	
	public String displayDriverSearch() {
		return "driver_handler_main";
	}
	
	public void initSearch(Matrix matrix, HtmlInputText input, UIComponent btn) {
		String driver = (String) btn.getAttributes().get("DRIVER");
		Boolean setDescription = (Boolean) btn.getAttributes().get("DESC");
		String reRender = (String) btn.getAttributes().get("RERENDER");
		System.out.println("reRender:"+reRender);
		reset();
		this.matrix = matrix;
		this.driver = driver;
		this.setDescription = setDescription;
		this.input = input;
		this.origDriverValue = (String) input.getSubmittedValue();
		FacesUtils.refreshComponent(input);

		// Get column information
		Map<String,DriverNames> map = cacheManager.getMatrixColDriverNamesMap(matrix.getDriverCode());
		driverName = map.get(driver);
		String transProperty = Util.columnToProperty(driverName.getTransDtlColName());
		//boolean isSaleTrans = "GSB".equals(matrix.getDriverCode());
		DataDefinitionColumn column = null;//cacheManager.getDataDefinitionPropertyMap("").get(transProperty); 
		if(matrix.getDriverCode().equals("GSB"))
			column=	cacheManager.getDataDefinitionPropertyMap("TB_BILLTRANS").get(transProperty);
		else{
			column=	cacheManager.getDataDefinitionPropertyMap("TB_PURCHTRANS").get(transProperty);
		}
			
		columnDescription = (column == null)? null:column.getDescription();
		
		
		
		logger.debug("Driver search - " + driver + " - " + transProperty);
		
		// Initialize values
		setDriverValue(origDriverValue);
		if (matrix.getHasDescriptions()) {
			String driverProperty = Util.columnToProperty(driver);
			setDriverDesc(Util.getPropertyAsString(matrix, driverProperty+"Desc"));
		}
		
		/*//AC
		try{
		okButton.setReRender(reRender);
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
		
		// Get initial data
		initializeSearch();
	}
	
	private void addAllNullSelection(){
		if(searchList!=null){
			//Add *ALL
			DriverReferencePK driverReferencePK = new DriverReferencePK(driverName.getTransDtlColName(), "*ALL");
			DriverReference driverReference = new DriverReference();
			driverReference.setDriverReferencePK(driverReferencePK);
			driverReference.setDriverDesc("*ALL");
			searchList.add(0, driverReference);
			
			//Add *NULL
			if(driverName.getNullDriverFlag()!=null && driverName.getNullDriverFlag().equalsIgnoreCase("1")){
				driverReferencePK = new DriverReferencePK(driverName.getTransDtlColName(), "*NULL");
				driverReference = new DriverReference();
				driverReference.setDriverReferencePK(driverReferencePK);
				driverReference.setDriverDesc("*NULL");
				searchList.add(1, driverReference);
			}
		}
	}
	
	public void initUpdate(Matrix matrix){
		this.matrix = matrix;
	}
	
	public HtmlAjaxCommandButton getOkButton() {
		return okButton;
	}

	public void setOkButton(HtmlAjaxCommandButton okButton) {
		this.okButton = okButton;
	}

	public HtmlInputText getDriverValueInput() {
		return driverValueInput;
	}

	public void setDriverValueInput(HtmlInputText driverValueInput) {
		this.driverValueInput = driverValueInput;
	}

	public HtmlInputText getDriverDescInput() {
		return driverDescInput;
	}

	public void setDriverDescInput(HtmlInputText driverDescInput) {
		this.driverDescInput = driverDescInput;
	}

	public HtmlInputText getUserValueInput() {
		return userValueInput;
	}

	public void setUserValueInput(HtmlInputText userValueInput) {
		this.userValueInput = userValueInput;
	}

	public DriverNames getDriverName() {
		return driverName;
	}

	public String getDriverValue() {
		return driverValue;
	}

	public void setDriverValue(String driverValue) {
		this.driverValue = driverValue;
		setInputValue(driverValueInput, driverValue);
	}

	public String getDriverDesc() {
		return driverDesc;
	}

	public void setDriverDesc(String driverDesc) {
		this.driverDesc = driverDesc;
		setInputValue(driverDescInput, driverDesc);
	}

	public String getUserValue() {
		return userValue;
	}

	public void setUserValue(String userValue) {
		this.userValue = userValue;
		setInputValue(userValueInput, userValue);
	}

	public String getColumnDescription() {
		return columnDescription;
	}

	public void setDriverReferenceService(DriverReferenceService driverReferenceService) {
		this.driverReferenceService = driverReferenceService;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void reset() {
		setDriverValue(null);
		setDriverDesc(null);
		setUserValue(null);
		searchList = null;
		selectedItemKey = null;
	}
	
	public String getSelectedItemKey() {
		return selectedItemKey;
	}

	public void setSelectedItemKey(String selectedItemKey) {
		this.selectedItemKey = selectedItemKey;
	}

	public List<DriverReference> getSearchList() {
		return searchList;
	}

	protected void initializeSearch() {
		searchList = performSearch();
		addAllNullSelection();
		selectedItemKey = null;
	}
	
	public void clearAction(ActionEvent e) {
		reset();
	}
	
	public void searchAction(ActionEvent e) {
		initializeSearch();
	}
	
	public void updateListener(ActionEvent e) {
		DriverReference ref = getSelectedItem();
		if (ref != null) {
			String driverProperty = Util.columnToProperty(driver);
			Util.setProperty(matrix, driverProperty, ref.getDriverValue(), String.class);
			if (matrix.getHasDescriptions()) {
				Util.setProperty(matrix, driverProperty+"Desc", ref.getDriverDesc(), String.class);
			}
			FacesUtils.refreshComponent(input);
		}
	}
	
	public void updateBU(String columnName) {
			String driverProperty = Util.columnToProperty(columnName);
			Util.setProperty(matrix, driverProperty, "*ALL", String.class);
			FacesUtils.refreshComponent(input);
	}
	
	public boolean getHasSomeInput() {
		return (((driverValue != null) && !driverValue.equals("")) || 
				((driverDesc != null) && !driverDesc.equals("")) ||
				((userValue != null) && !userValue.equals("")));
	}
	
	public boolean getHasAllInput() {
		return (((driverValue != null) && !driverValue.equals("")));
	}
	

	public boolean getValidSearchResult() {
		return (selectedItemKey != null);
	}

	@SuppressWarnings("unchecked")
	public void selectItem(ActionEvent e) { 
	    HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
	    selectedItemKey = ((DriverReference) table.getRowData()).getDriverValue();
		logger.debug("Selected ID: " + ((selectedItemKey == null)? "NULL":selectedItemKey));		
	}

	public DriverReference getSelectedItem() {
		for (DriverReference ref : searchList) {
			if (ref.getDriverValue().equals(selectedItemKey)) {
				return ref;
			}
		}
		return null;
	}
	
	private List<DriverReference> performSearch() {
		return driverReferenceService.findDriverReference(driverName.getTransDtlColName(), 
				driverValue, driverDesc, userValue, 1000);
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
}

