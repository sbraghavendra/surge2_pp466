package com.ncsts.view.bean;
/*
 * @author Muneer
 */
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.User;
import com.ncsts.dto.DataStatisticsDTO;
import com.ncsts.services.DataStatisticsService;

public class DataStatisticsLinesBackingBean {
	
    private Logger logger = LoggerFactory.getInstance().getLogger(DataStatisticsLinesBackingBean.class);	

    private boolean viewDataStatistics;
	private DataStatisticsDTO dataStatisticsDTO = new DataStatisticsDTO(); //this is declared for search
	private DataStatisticsDTO dataStatisticsDTONew = new DataStatisticsDTO(); //this is declared for updating DB Statistics line
	private List<DataStatisticsDTO> dataStatisticsList; // this list is declared for displaying the table rows
	private DataStatisticsDTO selectedDataStatistics = new DataStatisticsDTO(); //this is declared for capturing one selected row from the table i.e by default 0th index of selected rows
	
	private DataStatisticsService dataStatisticsService;
	
	@Autowired
	private loginBean loginBean;
	
	private int selectedRowIndex = -1;
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
		
	public DataStatisticsDTO getDataStatisticsDTO() {
		return dataStatisticsDTO;
	}


	public void setDataStatisticsDTO(DataStatisticsDTO dataStatisticsDTO) {
		this.dataStatisticsDTO = dataStatisticsDTO;
	}
	
	public boolean isShowButtons() {
		return (selectedRowIndex != -1);
	}
	
	/**
	 * @return the selectedDBStatistics
	 */
	public DataStatisticsDTO getSelectedDataStatistics() {
		return selectedDataStatistics;
	}

	/**
	 * @param selectedDBStatistics the selectedDBStatistics to set
	 */
	public void setSelectedDataStatistics(DataStatisticsDTO selectedDataStatistics) {
		this.selectedDataStatistics = selectedDataStatistics;
	}
	
	/**
	 * @return the dBStatisticsDTONew
	 */
	public DataStatisticsDTO getDataStatisticsDTONew() {
		return dataStatisticsDTONew;
	}

	/**
	 * @param dBStatisticsDTONew the dBStatisticsDTONew to set
	 */
	public void setDataStatisticsDTONew(DataStatisticsDTO dataStatisticsDTONew) {
		this.dataStatisticsDTONew = dataStatisticsDTONew;
	}

	public void selectedRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		selectedRowIndex = table.getRowIndex();

		List<DataStatisticsDTO> selected = new ArrayList<DataStatisticsDTO>(); 
		selectedDataStatistics = (DataStatisticsDTO)table.getRowData();
	}
	
	public void filterSaveAction(ActionEvent e) {
    }

	public String takeSelection() {
		if(selectedDataStatistics==null){
			this.selectedDataStatistics = new DataStatisticsDTO();
		}
		return "db_statistics_view";
	}
	
	public String viewAction() {
		logger.info("start Cancel View Action");
		logger.info("end Cancel View Action");	  
	    return "db_statistics_main";
	}	
	
	public String updateAction() {
		
		logger.info("start updateAction for DB Statistics lines"); 
		dataStatisticsService.update(selectedDataStatistics);
		logger.info("end updateAction");	
		
		dataStatisticsList = null;
		selectedDataStatistics = null;
		selectedRowIndex = -1;
	    return "db_statistics_main";
	}
	
	public String displayUpdateAction() {
		return "db_statistics_go_update";
	}
	
	public String removeAction() {
		logger.info("start removeAction"); 
		dataStatisticsService.remove(selectedDataStatistics.getDataStatisticsId());
		dataStatisticsList = null; // Cause list to refresh
		selectedDataStatistics = null;
		selectedRowIndex = -1;
	    return "db_statistics_main";
	}
	
	public String displayDeleteAction() {
	    return "db_statistics_delete"; 
	}	
	
	public String displayAddAction() {
		dataStatisticsDTONew = new DataStatisticsDTO();
	    return "data_statistics_add";
	}
	
	public String addAction() {
		logger.info("start addAction for DataStatistics Lines"); 

		logger.debug("**********dataStatisticsDTONew values for Add are ***********");
		logger.debug(dataStatisticsDTONew.toString());
		
		dataStatisticsService.addNewStatisticsLine(dataStatisticsDTONew);
		logger.info("end addAction for Data Statistics Lines");
		dataStatisticsDTONew =  new DataStatisticsDTO();
		dataStatisticsList = null; // Cause list to refresh
		selectedRowIndex = -1;
		selectedDataStatistics = null;

	    return "db_statistics_main"; 
	}

	public List<DataStatisticsDTO> getDataStatisticsList() {
		if (dataStatisticsList == null) {
			logger.info("  its in getDataStatisticsList");
			dataStatisticsList = dataStatisticsService.findAllDataStatisticsLines();
			this.setViewDataStatistics(true);
			selectedRowIndex = -1;
		}
		return dataStatisticsList;
	}

	public void setDataStatisticsList(List<DataStatisticsDTO> dataStatisticsList) {
		this.dataStatisticsList = dataStatisticsList;
	}

	public DataStatisticsService getDataStatisticsService() {
		return dataStatisticsService;
	}

	public void setDataStatisticsService(DataStatisticsService dataStatisticsService) {
		this.dataStatisticsService = dataStatisticsService;
	}

	public boolean isViewDataStatistics() {
		return viewDataStatistics;
	}

	public void setViewDataStatistics(boolean viewDataStatistics) {
		this.viewDataStatistics = viewDataStatistics;
	}

	public void validateDBStatisticsID(FacesContext context, UIComponent toValidate, Object value) {
		Long id = (Long) value; 
	    if (dataStatisticsService.findById(id) != null) {
			((UIInput)toValidate).setValid(false);
			
			FacesMessage message = new FacesMessage("The DB Statistics ID is already used.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateRowTitle(FacesContext context, UIComponent toValidate, Object value) {
		String code = (String) value;
		if (code==null || code.trim().length()==0) {
			((UIInput)toValidate).setValid(false);

			FacesMessage message = new FacesMessage("The Row Title may not be blank.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateWhereClause(FacesContext context, UIComponent toValidate, Object value) {
		String whereClause = (String) value;	
		if (whereClause!=null && whereClause.trim().length()>0 && 
				!dataStatisticsService.isValidWhereClause(whereClause.trim())) {
			((UIInput)toValidate).setValid(false);

			FacesMessage message = new FacesMessage("The Where Clause statement is not valid.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateGroupByOnDrilldown(FacesContext context, UIComponent toValidate, Object value) {
		String groupByOnDrilldown = (String) value;
		if (groupByOnDrilldown!=null && groupByOnDrilldown.trim().length()>0 && 
				!dataStatisticsService.isValidGroupByOnDrilldown(groupByOnDrilldown.trim())) {
			((UIInput)toValidate).setValid(false);

			FacesMessage message = new FacesMessage("The Group by on Drilldown is not valid.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }

}
