package com.ncsts.view.bean;

import java.beans.PropertyDescriptor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Column;

import org.ajax4jsf.component.UIDataAdaptor;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.ncsts.common.CacheManager;
import com.ncsts.common.DatabaseUtil;
import com.ncsts.common.Util;
import com.ncsts.dao.BatchMaintenanceDAO;
import com.ncsts.domain.Auditable;
import com.ncsts.domain.BCPBillTransaction;
import com.ncsts.domain.BatchErrorLog;
import com.ncsts.domain.BatchMaintenance;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverNamesPK;
import com.ncsts.domain.ImportDefinition;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.ImportSpec;
import com.ncsts.domain.ImportSpecPK;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.ListCodesPK;
import com.ncsts.domain.Option;
import com.ncsts.domain.OptionCodePK;
import com.ncsts.dto.DbPropertiesDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO;
import com.ncsts.dto.ImportMapProcessPreferenceDTO.DuplicationRestriction;
import com.ncsts.exception.DateFormatException;
import com.ncsts.management.DbPropertiesBean;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.services.ImportDefinitionService;
import com.ncsts.services.ImportMapService;
import com.ncsts.services.OptionService;
import com.ncsts.view.util.ImportMapSaleUploadParser;
import com.ncsts.view.util.JsfHelper;

public class ImportMapSaleAddBean {
	private Logger logger = Logger.getLogger(ImportMapSaleAddBean.class);

	private enum AddState {
		SELECT_SPEC, NEED_FILE, HAVE_FILE, PROCESSING, COMPLETE
	}

	private AddState state = AddState.SELECT_SPEC;

	private String backgroundError;
	private Thread background;

	private int selectedRowIndex = -1;
	private ImportSpec selectedImportSpec;
	private ImportDefinition selectedDefinition;
	private List<ImportSpec> importSpecList;
	private HtmlSelectOneMenu selectMenu = new HtmlSelectOneMenu();
	private Collection<SelectItem> fileItems;
	private File selectedFile;
	private ImportMapSaleUploadParser parser;
	private List<BatchMaintenance> currentBatches;
	private List<ImportDefinitionDetail> mappedColumns;

	private long bytesInFile;
	private long estimatedRows;
	private BatchMaintenance newBatch;

	private BatchMaintenanceDAO batchMaintenanceDAO;
	private FilePreferenceBackingBean filePreferenceBean;
	private ImportMapProcessPreferenceDTO preferences;

	private CacheManager cacheManager;
	private DbPropertiesBean dbPropertiesBean;

	private ImportMapService importMapService;
	private ImportDefAndSpecsService importDefAndSpecsService;
	private ImportDefinitionService importDefinitionService;
	private ImportMapSaleBean importMapSaleBean;
	
	@Autowired
	private OptionService optionService;
	@Autowired
	private loginBean loginservice;

	private DbPropertiesDTO dbPropertiesDTO = null;

	public void clear(ImportMapSaleBean bean) {
		this.importMapSaleBean = bean;
		this.batchMaintenanceDAO = bean.getBatchMaintenanceDAO();
		clear();
	}

	public void clear() {
		this.selectedRowIndex = -1;
		this.selectedDefinition = null;
		this.importSpecList = null;
		this.selectedFile = null;
		preferences = filePreferenceBean.getImportMapProcessPreferenceDTO();
		currentBatches = null;
		mappedColumns = null;
		fileItems = null;
		background = null;
		state = AddState.SELECT_SPEC;
	}

	public void addAction() {
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(selectedImportSpec.getDefaultDirectory()
					+ File.separator + selectedFile.getName());
			parser = new ImportMapSaleUploadParser(def, preferences);
			parser.setCacheManager(cacheManager);
		} catch (DateFormatException dfe) {
			JsfHelper.addError(dfe.getMessage());
		}

		parser.setImportDefinitionDetail(mappedColumns);

		newBatch = new BatchMaintenance();
		// since this runs in a background thread, it has no access to a
		// FacesContext, so we need to get it now in foreground

		newBatch.setUpdateUserId(Auditable.currentUserCode());
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc02(selectedFile.getName());
		newBatch.setBatchTypeCode("IPS");
		newBatch.setBatchStatusCode("IX");
		newBatch.setTotalBytes(parser.getFileSize());
		batchMaintenanceDAO.save(newBatch);

		// now do the heavy lifting
		parser.setColumnProperties(batchMaintenanceDAO
				.getColumnProperties(BCPBillTransaction.class));
		processUpdateInBackground(newBatch);
	}

	public String closeAction() {
		background = null;

		// a new batch has been created, so clear selection
		if (importMapSaleBean != null) {
			importMapSaleBean.getBatchMaintenanceDataModel().refreshData(false);
			importMapSaleBean.setSelectedRowIndex(-1);
		}
		newBatch = null;
		return "importmapsale_main";
	}

	public String cancelAction() {
		if (parser != null) {
			parser.setAbort(true);
			try {
				int secCount = 0;
				while (!parser.wasCompleted() && secCount < 50) {
					Thread.sleep(1000);
					secCount++;
				}

			} catch (Exception ex) {
			}
		}

		return closeAction();
	}

	private void processUpdateInBackground(final BatchMaintenance batch) {
		String selectedDb = ChangeContextHolder.getDataBaseType();
		dbPropertiesDTO = dbPropertiesBean.getDbPropertiesDTO(selectedDb);

		parser.setTransactionColumnDriverNamesMap(cacheManager
				.getTransactionColumnDriverNamesMap(null));
		parser.setBCPTransactionsColumnMap(cacheManager
				.getBCPSaleTransactionsColumnMap());
		parser.setErrorListCodes(cacheManager.getErrorListCodes());

		// 5427 Convert to upper for import headers
		// Just want to get these data before running Thread
		getMappedColumns();

		state = AddState.PROCESSING;
		backgroundError = null;
		background = new Thread(new Runnable() {
			public void run() {
				System.out
						.println("############ processUpdateInBackground for Import File using URL: "
								+ dbPropertiesDTO.getUrl());

				try {
					Thread.sleep(2000);
				} catch (Exception ex) {
				}

				try {
					Class.forName(dbPropertiesDTO.getDriverClassName());
				} catch (ClassNotFoundException e) {
					System.out.println("xxxxxxxxxxxxxx Can't load class: "
							+ dbPropertiesDTO.getDriverClassName());
					return;
				}
				Connection conn = null;

				try {
					parser.setCompleted(false);

					// Standalone connection
					conn = DriverManager.getConnection(
							dbPropertiesDTO.getUrl(),
							dbPropertiesDTO.getUserName(),
							dbPropertiesDTO.getPassword());
					conn.setAutoCommit(false);

					batch.setVc01(selectedImportSpec.getImportSpecCode());
					batch.setVc02(selectedFile.getName());
					batch.setEntryTimestamp(new Date());

					// Import file
					importMapUpdatesUsingJDBC(batch, parser, conn);

					// force completion
					state = AddState.COMPLETE;
					if (parser.getErrorCount() > 0) {
						batch.setHeldFlag("1");
						// batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
					}

					if (parser.getMaxErrorSeen() < 10) {
						// Perfect, no error.
					} else if (parser.getMaxErrorSeen() == 10) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
					} else if (parser.getMaxErrorSeen() == 20) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
					} else if (parser.getMaxErrorSeen() >= 30) {
						batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
					}

					batch.setNu01(parser.getComputedTotalAmount());
					batch.setVc01(selectedImportSpec.getImportSpecCode());

					if (parser.wasAborted()) {
						batch.setBatchStatusCode("ABI");
					} else {
						batch.setBatchStatusCode("I");
					}
					batch.setTotalRows(new Long(parser.getTotalRows()));
					batch.setStatusUpdateTimestamp(new Date());
					batch.setStatusUpdateUserId(batch.getUpdateUserId());
					batch.setUpdateUserId(batch.getUpdateUserId());
					batch.setUpdateTimestamp(new Date());

					updateBatchUsingJDBC(batch, conn);

					parser.setCompleted(true);
				} catch (Throwable e) {
					e.printStackTrace();

					if (e.getCause() != null) {
						e.getCause().printStackTrace();
					}

					parser.setCompleted(true);
					logger.debug(e, e);
					backgroundError = "Unable to save batch: " + e.getMessage();
				} finally {
					if (conn != null)
						try {
							conn.close();
						} catch (SQLException ignore) {
						}
				}
			}
		});
		background.start();
		logger.debug("Background started");
	}

	protected void updateBatchUsingJDBC(BatchMaintenance batchMaintenance,
			Connection conn) throws Exception {
		String sql = batchMaintenance.getRawUpdateStatement();
		PreparedStatement prep = conn.prepareStatement(sql);
		batchMaintenance.populateUpdatePreparedStatement(conn, prep);
		prep.addBatch();

		prep.executeBatch();
		conn.commit();
		prep.clearBatch();

		return;
	}

	protected BatchMaintenance importMapUpdatesUsingJDBC(
			BatchMaintenance batchMaintenance, ImportMapSaleUploadParser parser,
			Connection conn) throws Exception {

		Iterator<BCPBillTransaction> truIter = parser.iterator();

		// 5427 Convert to upper for import headers
		if (!isFileAllColumnsMapped()) {
			parser.addError(new BatchErrorLog("IP9", 1, "", "", null));
		}

		String sql = null;
		PreparedStatement prep = null;
		Statement stat = null;

		int i = 1;
		while (truIter.hasNext()) {
			BCPBillTransaction td = truIter.next();
			if (td != null) {

				if (prep == null) {
					sql = td.getRawInsertStatement();
					prep = conn.prepareStatement(sql);
					stat = conn.createStatement();
				}

				td.setProcessBatchNo(batchMaintenance.getbatchId());
				//td.setUpdateUserId(batchMaintenance.getUpdateUserId());
				td.setBcpBilltransId(getNextBcpSaletransId(stat));

				// populate data to Prepared Statement
				td.populatePreparedStatement(conn, prep);
				prep.addBatch();

				if ((i++ % 50) == 0) {
					logger.debug("Begin Flush: " + (i - 1));

					if (outAID != null) {
						writeLogAID("   ... Begin Flush: record " + (i - 1));
					}

					prep.executeBatch();
					conn.commit();
					prep.clearBatch();
					logger.debug("End Flush");

					if (outAID != null) {
						writeLogAID("   ... End Flush");
					}

					parser.setLinesWritten(i - 1);
				}
				td = null;
			}

			// 0001701: Import errors make the system crash
			if (parser.getBatchErrorLogs().size() >= 50) {
				// flush every 50+ errors
				flushErrorLog(conn, batchMaintenance);
			}
		}

		if (prep != null) {
			if (outAID != null) {
				writeLogAID("   ... Begin Final Flush: record " + (i - 1));
			}

			prep.executeBatch();
			conn.commit();
			prep.clearBatch();

			if (outAID != null) {
				writeLogAID("   ... End Final Flush");
			}

			try {
				prep.close();
			} catch (Exception ex) {
			}
		}
		if (outAID != null) {
			writeLogAID("   ### Total Rows Imported: " + (i - 1));
		}

		// perform the checksum and generate any more ErrorLogs
		parser.checksum();

		parser.setLinesWritten(i - 1);

		// 0001701: Import errors make the system crash
		flushErrorLog(conn, batchMaintenance);

		if (outAID != null) {
			writeLogAID("   ### Total Non-Warning Errors: "
					+ parser.getErrorCount());
		}
		if (outAID != null) {
			writeLogAID("   ### Total Error Count: "
					+ parser.getErrorCountTotal());
		}

		if (stat != null) {
			try {
				stat.close();
			} catch (Exception ex) {
			}
		}

		return batchMaintenance;
	}
	
	void flushErrorLog(Connection conn, BatchMaintenance batchMaintenance)
			throws Exception {
		// 0001701: Import errors make the system crash
		// Save error if needed
		String sqlError = null;
		PreparedStatement prepError = null;

		Statement statErrorNext = null;
		int ie = 1;
		for (BatchErrorLog error : parser.getBatchErrorLogs()) {
			if (prepError == null) {
				sqlError = error.getRawInsertStatement();
				prepError = conn.prepareStatement(sqlError);
				statErrorNext = conn.createStatement();
			}

			error.setBatchErrorLogId(getNextBatchErrorLogId(statErrorNext));
			error.setProcessType("IPS");
			error.setProcessTimestamp(batchMaintenance.getEntryTimestamp());
			error.setBatchId(batchMaintenance.getBatchId());
			error.populatePreparedStatement(conn, prepError);
			prepError.addBatch();

			if ((ie++ % 50) == 0) {
				prepError.executeBatch();
				conn.commit();
				prepError.clearBatch();
			}

			if (outAID != null) {
				String errorStr = "   ... Error: RowNo: " + error.getRowNo()
						+ ", Column No: " + error.getColumnNo()
						+ ", Error Def Code: " + error.getErrorDefCode()
						+ ", Import Header Column: "
						+ error.getImportHeaderColumn()
						+ ", Import Header Value: "
						+ error.getImportColumnValue()
						+ ", Trans Dtl Column Name: "
						+ error.getTransDtlColumnName();
				writeLogAID(errorStr);
			}
		}
		if (prepError != null) {
			prepError.executeBatch();
			conn.commit();
			prepError.clearBatch();

			try {
				prepError.close();
			} catch (Exception ex) {
			}
		}

		if (statErrorNext != null) {
			try {
				statErrorNext.close();
			} catch (Exception ex) {
			}
		}

		parser.resetBatchErrorLogs();

	}

	private long getNextBcpSaletransId(Statement stat) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"SQ_TB_BCP_BILLTRANS_ID", "NEXTID");
			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBcpSaletransId() failed: "
					+ e.toString());
		}

		return nextId;
	}

	private long getNextBatchErrorLogId(Statement statErrorNext) {
		long nextId = -1;
		try {
			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(statErrorNext),
					"sq_tb_batch_error_log_id", "NEXTID");

			// Statement stat = conn.createStatement();
			ResultSet rs = statErrorNext.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ getNextBatchErrorLogId() failed: "
					+ e.toString());
		} finally {
		}

		return nextId;
	}

	/*
	 * private void processUpdateInBackground(final BatchMaintenance batch) {
	 * state=AddState.PROCESSING; backgroundError = null; background = new
	 * Thread(new Runnable() { public void run() { try {
	 * parser.setCompleted(false);
	 * 
	 * batch.setVc01(selectedImportSpec.getImportSpecCode());
	 * batch.setVc02(selectedFile.getName()); batch.setEntryTimestamp(new
	 * Date()); Iterator<BcpSaleTransaction> iter = parser.iterator();
	 * batchMaintenanceDAO.importMapUpdates(batch, parser);
	 * 
	 * // force completion state=AddState.COMPLETE; if (parser.getErrorCount() >
	 * 0) { batch.setHeldFlag("1");
	 * batch.setErrorSevCode(BatchMaintenance.ErrorState.Error); }
	 * batch.setNu01(parser.getComputedTotalAmount());
	 * batch.setVc01(selectedImportSpec.getImportSpecCode());
	 * 
	 * if (parser.wasAborted()) { batch.setBatchStatusCode("ABI"); } else {
	 * batch.setBatchStatusCode("I"); } batch.setTotalRows(new
	 * Long(parser.getTotalRows())); batch.setStatusUpdateTimestamp(new Date());
	 * batch.setStatusUpdateUserId(batch.getUpdateUserId());
	 * batch.setUpdateUserId(batch.getUpdateUserId());
	 * batch.setUpdateTimestamp(new Date()); batchMaintenanceDAO.update(batch);
	 * 
	 * parser.setCompleted(true); } catch (Throwable e) {
	 * parser.setCompleted(true); logger.debug(e, e); backgroundError =
	 * "Unable to save batch: " + e.getMessage(); } } }); background.start();
	 * logger.debug("Background started"); }
	 */

	public Map<String, PropertyDescriptor> getBcpSaleTransactionColumnProperties() {
		HashMap<String, PropertyDescriptor> map = new HashMap<String, PropertyDescriptor>();
		Class<?> clazz = BCPBillTransaction.class;
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			Column col = f.getAnnotation(Column.class);
			if (col != null) {
				try {
					String s = f.getName();
					PropertyDescriptor propertyDescriptor = BeanUtils
							.getPropertyDescriptor(clazz, s);

					map.put(col.name(), propertyDescriptor);

				} catch (Exception e) {
				}
			}
		}

		// Need to include AuditListener.class
		try {
			String s = "updateUserId";
			PropertyDescriptor propertyDescriptor = BeanUtils
					.getPropertyDescriptor(clazz, s); // Use
																	// BcpSaleTransaction.class
																	// here.
			map.put("UPDATE_USER_ID", propertyDescriptor);

			s = "updateTimestamp";
			propertyDescriptor = BeanUtils.getPropertyDescriptor(clazz,
					s); // Use BcpSaleTransaction.class here.

			map.put("UPDATE_TIMESTAMP", propertyDescriptor);

		} catch (Exception e) {
		}

		return map;
	}

	protected BatchMaintenance addBatchUsingJDBC(
			BatchMaintenance batchMaintenance, Connection conn) {
		// Get next batch Id
		long nextId = -1;
		Statement stat = null;
		try {
			stat = conn.createStatement();

			String sql = DatabaseUtil.getSequenceQuery(
					DatabaseUtil.getDatabaseProductName(stat),
					"sq_tb_batch_id", "NEXTID");

			ResultSet rs = stat.executeQuery(sql);
			if (rs.next()) {
				nextId = rs.getLong("NEXTID");
			}
			rs.close();
		} catch (Exception e) {
			logger.error("############ addBatchUsingJDBC() failed: "
					+ e.toString());
			return null;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		// Set new batch id
		batchMaintenance.setBatchId(nextId);
		PreparedStatement prep = null;
		try {
			String sql = batchMaintenance.getRawAddStatement();
			prep = conn.prepareStatement(sql);
			batchMaintenance.populateAddPreparedStatement(conn, prep);
			prep.addBatch();
			prep.executeBatch();
			conn.commit();
		} catch (Exception e) {
			return null;
		} finally {
			if (prep != null) {
				try {
					prep.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	public void selectedRowChanged(ActionEvent e)
			throws AbortProcessingException {
		UIComponent uiComponent = e.getComponent().getParent();
		UIDataAdaptor table = (UIDataAdaptor) uiComponent;
		this.selectedImportSpec = (ImportSpec) table.getRowData();
		this.selectedRowIndex = table.getRowIndex();
		this.selectedFile = null;
		selectedDefinition = importDefAndSpecsService.find(selectedImportSpec
				.getImportDefinitionCode());
		if (selectedDefinition == null) {
			JsfHelper.addError("Could not find definition for "
					+ selectedImportSpec.getImportDefinitionCode());
			return;
		}
		try {
			selectMenu.setValue("");
			fileItems = new ArrayList<SelectItem>();
			fileItems.add(new SelectItem("", "Select File"));
			File file = new File(selectedImportSpec.getDefaultDirectory());
			List<String> files = new ArrayList<String>();
			for (File f : file.listFiles()) {
				files.add(f.getName());
			}
			Collections.sort(files);
			for (String f : files) {
				fileItems.add(new SelectItem(f, f));
			}
			final UISelectItems items = new UISelectItems();
			items.setValue(fileItems);
			selectMenu.getChildren().clear();
			selectMenu.getChildren().add(items);

		} catch (Exception ex) {
			fileItems = null;
			JsfHelper.addError("Default directory not available: "
					+ selectedImportSpec.getDefaultDirectory());
		}
		state = AddState.NEED_FILE;
	}

	public void getFileDetails(ActionEvent e) {
		HtmlSelectOneMenu selectItems = (HtmlSelectOneMenu) e.getComponent()
				.getParent();
		String fileName = (String) selectItems.getValue();
		this.selectedFile = null;
		this.mappedColumns = null;
		// in case this process fails and we no longer have a valid selected
		// file.
		state = AddState.NEED_FILE;
		if ((fileName == null) || (fileName.trim().length() == 0))
			return;
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(selectedImportSpec.getDefaultDirectory()
					+ File.separator + fileName);
			parser = new ImportMapSaleUploadParser(def, preferences);
			Set<String> headerNames = new HashSet<String>();
			for (String s : parser.getHeaders()) {
				headerNames.add(s == null ? "" : s.toUpperCase());
			}

			for (ImportDefinitionDetail detail : getMappedColumns()) {
				// 5427 Convert to upper for import headers. They should be
				// upper in db, but just make sure.
				if (!headerNames.contains(detail.getImportColumnName()
						.toUpperCase())) {
					JsfHelper
							.addError(selectItems,
									"The Definition has mapped columns that are not in the import file.");
					return;
				}
			}

			if (preferences == null) {
				preferences = filePreferenceBean
						.getImportMapProcessPreferenceDTO();
			}
			BatchMaintenance nameBatch = null;
			;
			if ((preferences.getFileNameDuplicateRestrictionType() != DuplicationRestriction.None)) {
				nameBatch = validateFileNames(fileName, getCurrentBatches());
			}
			BatchMaintenance sizeBatch = null;
			if ((preferences.getFileSizeDuplicateRestrictionType() != DuplicationRestriction.None)) {
				sizeBatch = validateFileLengths(fileName, getCurrentBatches());
			}
			String msg = null;
			if ((sizeBatch != null) && (nameBatch != null)) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId() + ", size: "
						+ sizeBatch.getBatchId();
			} else if (nameBatch != null) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId();
			} else if (sizeBatch != null) {
				msg = "Potential duplicate file size with Batch Id: "
						+ sizeBatch.getBatchId();
			}

			// 5427 Convert to upper for import headers.
			if (!isFileAllColumnsMapped()) {
				if (msg == null) {
					msg = "";
				} else {
					msg = msg + "\n";
				}
				msg = msg
						+ " Import file columns not mapped in the Definition.";
			}

			if ((nameBatch != null)
					&& (preferences.getFileNameDuplicateRestrictionType() == DuplicationRestriction.Full)) {
				JsfHelper.addError(selectItems, msg);
				return;
			} else if ((sizeBatch != null)
					&& (preferences.getFileSizeDuplicateRestrictionType() == DuplicationRestriction.Full)) {
				JsfHelper.addError(selectItems, msg);
				return;
			}
			if (msg != null) {
				JsfHelper.addWarning(selectItems, msg);
			}
			parser.parseFile();
			this.bytesInFile = parser.getFileSize();
			this.estimatedRows = parser.getEstimatedRows();
			// enable the OK button
			this.selectedFile = new File(fileName);
			state = AddState.HAVE_FILE;
		} catch (Exception ex) {
			logger.error(ex);
			JsfHelper.addError(selectItems,
					"Could not read file: " + ex.getMessage());
		}
	}

	private boolean isFileAllColumnsMapped() {
		boolean returnCode = true;

		List<ImportDefinitionDetail> mappedColumns = getMappedColumns();
		Set<String> localMappedColumns = new HashSet<String>();
		for (ImportDefinitionDetail detail : mappedColumns) {
			localMappedColumns.add(detail.getImportColumnName().toUpperCase());
		}

		List<String> headerNames = parser.getHeaders();
		for (String headerName : headerNames) {
			if (!localMappedColumns.contains(headerName.toUpperCase())) {
				returnCode = false;
				break;
			}
		}

		return returnCode;
	}

	private BatchMaintenance validateFileNames(String fileName,
			List<BatchMaintenance> list) {
		for (BatchMaintenance bm : list) {
			if ((!bm.getBatchStatusCode().startsWith("AB"))
					&& (bm.getVc02() != null)
					&& (bm.getVc02().trim().length() > 0)) {
				if (fileName.equalsIgnoreCase(bm.getVc02())) {
					return bm;
				}
			}
		}
		return null;
	}

	private BatchMaintenance validateFileLengths(String fileName,
			List<BatchMaintenance> list) {
		for (BatchMaintenance bm : list) {
			try {
				if ((!bm.getBatchStatusCode().equalsIgnoreCase("ABI"))
						&& (bm.getVc02() != null)
						&& (bm.getVc02().trim().length() > 0)
						&& (bm.getTotalBytes() == parser.getFileSize())) {
					logger.debug("Size match: " + fileName + ", "
							+ bm.getVc02());
					return bm;
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return null;
	}

	// expensive, so do as little as often
	private List<BatchMaintenance> getCurrentBatches() {
		if (currentBatches == null) {
			Criteria criteria = batchMaintenanceDAO.createCriteria();
			criteria.add(Expression.eq("batchTypeCode", "IPS"));
			// get all but deleted batches
			criteria.add(Expression.ne("batchStatusCode", "D"));
			currentBatches = batchMaintenanceDAO.find(criteria);
		}
		return currentBatches;
	}

	private List<ImportDefinitionDetail> getMappedColumns() {
		if ((this.mappedColumns == null) && (selectedDefinition != null)) {
			this.mappedColumns = importDefinitionService
					.getAllHeaders(selectedDefinition.getImportDefinitionCode());
		}
		return mappedColumns;
	}

	public boolean getDisplayFileSelector() {
		return (fileItems != null) && (state != AddState.SELECT_SPEC);
	}

	public boolean getEnableFileSelector() {
		return (state == AddState.NEED_FILE) && (fileItems != null);
	}

	// public boolean getDisplayFileSelectorButton() {
	// return (this.selectedFile != null);
	// }

	public boolean getDisplayOk() {
		return (state == AddState.HAVE_FILE);
	}

	public boolean getDisplayComplete() {
		return (state == AddState.COMPLETE);
	}

	public boolean getDisplayProgress() {
		return state == AddState.PROCESSING;
	}

	public boolean getDisplayBatch() {
		if (state == AddState.PROCESSING) {
			return true;
		} 
		else if(state == AddState.COMPLETE){
			return true;
		}
		else{
			return false;
		}
	}

	public String getProgress() {
		if (state == AddState.COMPLETE) {
			try {
				TimeUnit.MICROSECONDS.sleep(2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "100.0";
		} else if ((parser == null) || (state != AddState.PROCESSING)) {
			return "0.0";
		} else if (backgroundError != null) {
			JsfHelper.addError(backgroundError);
			background = null;
			state = AddState.COMPLETE;
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(1);
		String s = "0.0";
		Double scale = parser.getProgress() * 99.0;
		if (scale >= 0.01) {
			s = nf.format(scale);
		}
		logger.debug("Progress at: " + s);
		return s;
	}

	public long getBytesInFile() {
		return bytesInFile;
	}

	public long getEstimatedRows() {
		return estimatedRows;
	}

	public BatchMaintenance getNewBatch() {
		if (this.newBatch == null) {
			return new BatchMaintenance();
		}
		return this.newBatch;
	}

	public ImportMapService getImportMapService() {
		return importMapService;
	}

	public void setImportMapService(ImportMapService importMapService) {
		this.importMapService = importMapService;
	}

	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}

	public void setImportDefAndSpecsService(
			ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}

	public ImportDefinitionService getImportDefinitionService() {
		return importDefinitionService;
	}

	public void setImportDefinitionService(
			ImportDefinitionService importDefinitionService) {
		this.importDefinitionService = importDefinitionService;
	}

	public List<ImportSpec> getImportSpecList() {
		if (this.importSpecList == null) {
			this.importSpecList = importDefAndSpecsService.getAllForUserByType("IPS");
			
			Option option = optionService.findByPK(new OptionCodePK("PCO", "ADMIN", "ADMIN"));  
			String option_pco = (option==null || option.getValue()==null)? "0" : option.getValue();
			boolean	option_ips = loginservice.getHasBillingLicense();
			if(option_ips && option_pco.equals("1")){
				List<ImportSpec> importSpecListPco = importDefAndSpecsService.getAllForUserByType("PCO");
				if(importSpecListPco!=null && importSpecListPco.size()>0){
					importSpecList.addAll(importSpecListPco);
				}
			}	
		}
		return this.importSpecList;
	}

	public ImportSpec getSelectedImportSpec() {
		return selectedImportSpec;
	}

	public int getSelectedRowIndex() {
		return this.selectedRowIndex;
	}

	public ImportDefinition getSelectedDefinition() {
		return this.selectedDefinition;
	}

	public BatchMaintenanceDAO getBatchMaintenanceDAO() {
		return this.batchMaintenanceDAO;
	}

	public void setBatchMaintenanceDAO(BatchMaintenanceDAO batchMaintenanceDAO) {
		this.batchMaintenanceDAO = batchMaintenanceDAO;
	}

	public File getSelectedFile() {
		return this.selectedFile;
	}

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(
			FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public HtmlSelectOneMenu getSelectMenu() {
		return this.selectMenu;
	}

	public void setSelectMenu(HtmlSelectOneMenu selectMenu) {
		this.selectMenu = selectMenu;
	}

	public Collection<SelectItem> getFileItems() {
		return this.fileItems;
	}

	public ImportMapSaleBean getImportMapSaleBean() {
		return this.importMapSaleBean;
	}

	public void setImportMapSaleBean(ImportMapSaleBean importMapSaleBean) {
		this.importMapSaleBean = importMapSaleBean;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void setDbPropertiesBean(DbPropertiesBean dbPropertiesBean) {
		this.dbPropertiesBean = dbPropertiesBean;
	}

	public ImportSpec getImportSpecAID(String specCode)
			throws DataAccessException {
		ImportSpec importSpec = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_IMPORT_SPEC where IMPORT_SPEC_TYPE = 'IP' and UPPER(IMPORT_SPEC_CODE)='"
							+ specCode.toUpperCase() + "'");
			if (rs.next()) {
				importSpec = new ImportSpec();
				ImportSpecPK importSpecPK = new ImportSpecPK();
				importSpecPK
						.setImportSpecType(rs.getString("IMPORT_SPEC_TYPE"));
				importSpecPK
						.setImportSpecCode(rs.getString("IMPORT_SPEC_CODE"));
				importSpec.setImportSpecPK(importSpecPK);

				importSpec.setDescription(rs.getString("DESCRIPTION"));
				importSpec.setImportDefinitionCode(rs
						.getString("IMPORT_DEFINITION_CODE"));
				importSpec.setDefaultDirectory(rs
						.getString("DEFAULT_DIRECTORY"));
				importSpec.setLastImportFileName(rs
						.getString("LAST_IMPORT_FILE_NAME"));
				importSpec.setWhereClause(rs.getString("WHERE_CLAUSE"));
				importSpec.setComments(rs.getString("COMMENTS"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getImportSpecAID() failed: " +
			// e.toString());
			System.out.println("e.toString() : " + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return importSpec;
	}

	public ImportDefinition getImportDefinitionAID(String definitionCode)
			throws DataAccessException {
		ImportDefinition importDefinition = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_IMPORT_DEFINITION where UPPER(IMPORT_DEFINITION_CODE)='"
							+ definitionCode.toUpperCase() + "'");
			if (rs.next()) {
				importDefinition = new ImportDefinition();
				importDefinition.setImportDefinitionCode(rs
						.getString("IMPORT_DEFINITION_CODE"));
				importDefinition.setDescription(rs.getString("DESCRIPTION"));
				importDefinition.setImportFileTypeCode(rs
						.getString("IMPORT_FILE_TYPE_CODE"));
				importDefinition.setFlatDelChar(rs.getString("FLAT_DEL_CHAR"));
				importDefinition.setFlatDelTextQual(rs
						.getString("FLAT_DEL_TEXT_QUAL"));
				importDefinition.setFlatDelLine1HeadFlag(rs
						.getString("FLAT_DEL_LINE1_HEAD_FLAG"));
				importDefinition.setComments(rs.getString("COMMENTS"));
				importDefinition.setSampleFileName(rs
						.getString("SAMPLE_FILE_NAME"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getImportDefinitionAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return importDefinition;
	}

	public static DateFormat logDateformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");
	private Connection conn = null;
	private BufferedWriter outAID = null;

	public boolean initVerification(Connection connection, BufferedWriter out,
			String specStr) {
		this.conn = connection;
		this.outAID = out;

		writeLogAID(" ");
		selectedImportSpec = getImportSpecAID(specStr);
		if (selectedImportSpec == null) {
			// No ImportSpec found
			writeLogAID("Error: Cannot find spec for " + specStr);
			return false;
		}

		selectedDefinition = getImportDefinitionAID(selectedImportSpec
				.getImportDefinitionCode());
		if (selectedDefinition == null) {
			// No ImportDefinition found
			writeLogAID("Error: Cannot find definition for "
					+ selectedImportSpec.getImportDefinitionCode());
			return false;
		}

		writeLogAID("Import Spec Code:  "
				+ selectedImportSpec.getImportSpecCode());
		writeLogAID("Import Definition: "
				+ selectedImportSpec.getImportDefinitionCode());
		try {
			writeLogAID("Database URL:      " + conn.getMetaData().getURL());
		} catch (Exception e) {
		}

		return true;
	}

	public void writeLogAID(String message) {
		Date d = new Date(System.currentTimeMillis());
		String currentDateTime = logDateformat.format(d);
		try {
			outAID.write("[" + currentDateTime + "] " + message);
			outAID.newLine();
			outAID.flush();
		} catch (Exception e) {
		}
	}

	public void addActionAID(String fileName, String filePath) {
		writeLogAID("File Imported:    " + filePath); // We actually use this
														// file to import.
		writeLogAID(" ");
		writeLogAID("** Started import procedure");

		try {
			if (!getFileDetailsAID(filePath, fileName)) {
				// Verification error
				return;
			}

			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(filePath);

			parser = new ImportMapSaleUploadParser(def, preferences);
		} catch (DateFormatException dfe) {
			writeLogAID("Error: Importing stopped with error, "
					+ dfe.getMessage());
			return;
		}

		parser.setImportDefinitionDetail(mappedColumns);

		newBatch = new BatchMaintenance();
		newBatch.setUpdateUserId("STSCORP");
		newBatch.setEntryTimestamp(new Date());
		newBatch.setVc02(fileName);
		newBatch.setVc03("API");
		newBatch.setBatchTypeCode("IPS");
		newBatch.setBatchStatusCode("IX");
		newBatch.setTotalBytes(parser.getFileSize());
		BatchMaintenance batchMaintenance = addBatchUsingJDBC(newBatch, conn);
		if (batchMaintenance == null) {
			// Cannot create a new batch
			writeLogAID("Error: Cannot create new Batch Id. Importing stopped.");

			return;
		}
		writeLogAID("Batch Id: " + batchMaintenance.getBatchId() + " created");

		// now do the heavy lifting
		parser.setColumnProperties(getBcpSaleTransactionColumnProperties());
		processUpdateInBackgroundAID(batchMaintenance, conn);
	}

	private void processUpdateInBackgroundAID(final BatchMaintenance batch,
			Connection conn) {
		try {
			writeLogAID("Retrieving mapping from database....");

			parser.setTransactionColumnDriverNamesMap(getTransactionColumnDriverNamesMapAID());
			parser.setBCPTransactionsColumnMap(getBCPTransactionsColumnMapAID());
			parser.setErrorListCodes(getErrorListCodesAID());

			// 5427 Convert to upper for import headers
			// Just want to get these data before running Thread
			getMappedColumnsAID();

			writeLogAID("Retrieving mapping from database.... done");

			state = AddState.PROCESSING;
			backgroundError = null;
			parser.setCompleted(false);
			conn.setAutoCommit(false);

			batch.setVc01(selectedImportSpec.getImportSpecCode());
			batch.setEntryTimestamp(new Date());

			// Import file
			writeLogAID("Importing data file....");
			importMapUpdatesUsingJDBC(batch, parser, conn);
			writeLogAID("Importing data file.... done");

			// force completion
			state = AddState.COMPLETE;
			if (parser.getErrorCount() > 0) {
				batch.setHeldFlag("1");
				// batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
			}

			if (parser.getMaxErrorSeen() < 10) {
				// Perfect, no error.
			} else if (parser.getMaxErrorSeen() == 10) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Warning);
			} else if (parser.getMaxErrorSeen() == 20) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Error);
			} else if (parser.getMaxErrorSeen() >= 30) {
				batch.setErrorSevCode(BatchMaintenance.ErrorState.Terminal);
			}

			batch.setNu01(parser.getComputedTotalAmount());
			batch.setVc01(selectedImportSpec.getImportSpecCode());

			if (parser.wasAborted()) {
				batch.setBatchStatusCode("ABI");
			} else {
				batch.setBatchStatusCode("I");
			}

			batch.setTotalRows(new Long(parser.getTotalRows()));
			batch.setStatusUpdateTimestamp(new Date());
			batch.setStatusUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateUserId(batch.getUpdateUserId());
			batch.setUpdateTimestamp(new Date());

			updateBatchUsingJDBC(batch, conn);

			parser.setCompleted(true);
			parser.closeReader();

			writeLogAID("Held Flag: " + batch.getHeldFlag()
					+ ", Batch Status Code: " + batch.getBatchStatusCode());
			writeLogAID("** Finished import procedure");
			writeLogAID(" ");

			// Continue to process the batch when HeldFlag is not 1 and
			// BatchStatusCode is 'I'
			if ((batch.getHeldFlag() == null || !batch.getHeldFlag()
					.equalsIgnoreCase("1"))
					&& batch.getBatchStatusCode().equalsIgnoreCase("I")) {
				writeLogAID("** Started 'Flagged for Process' procedure for Batch Id, "
						+ batch.getbatchId());

				batch.setBatchStatusCode("FP");
				// Control already done with the timezone conversion, UTC
				batch.setSchedStartTimestamp(new Date());
				batch.setUpdateTimestamp(new Date());
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				updateBatchUsingJDBC(batch, conn);

				writeLogAID("Set Batch Status Code to FP");
				writeLogAID("** Finished 'Flagged for Process' procedure");
				writeLogAID(" ");
			} else {
				writeLogAID("** No 'Flagged for Process' procedure needed");
				writeLogAID(" ");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			parser.setCompleted(true);

			writeLogAID("Error: Importing aborted with error, "
					+ e.getMessage());

			try {
				batch.setBatchStatusCode("ABI");
				batch.setStatusUpdateTimestamp(new Date());
				batch.setStatusUpdateUserId("STSCORP");
				batch.setUpdateTimestamp(new Date());
				updateBatchUsingJDBC(batch, conn);
			} catch (Exception ee) {
			}

		} finally {
		}
	}

	public boolean getFileDetailsAID(String filePath, String fileName) {
		this.selectedFile = null;
		this.mappedColumns = null;
		// in case this process fails and we no longer have a valid selected
		// file.
		state = AddState.NEED_FILE;
		try {
			ImportDefinition def = new ImportDefinition(selectedDefinition);
			def.setSampleFileName(filePath);

			parser = new ImportMapSaleUploadParser(def, preferences);
			Set<String> headerNames = new HashSet<String>();
			for (String s : parser.getHeaders()) {
				headerNames.add(s == null ? "" : s.toUpperCase());
			}

			for (ImportDefinitionDetail detail : getMappedColumnsAID()) {
				// 5427 Convert to upper for import headers. They should be
				// upper in db, but just make sure.
				if (!headerNames.contains(detail.getImportColumnName()
						.toUpperCase())) {
					writeLogAID("Error: The Definition has mapped columns that are not in the import file");
					return false;
				}
			}

			if (preferences == null) {
				preferences = getImportMapProcessPreferenceDTOAID();
			}
			BatchMaintenance nameBatch = null;
			;
			if ((preferences.getFileNameDuplicateRestrictionType() != DuplicationRestriction.None)) {
				nameBatch = validateFileNamesAID(fileName);
			}
			BatchMaintenance sizeBatch = null;
			if ((preferences.getFileSizeDuplicateRestrictionType() != DuplicationRestriction.None)) {
				sizeBatch = validateFileLengthsAID(fileName);
			}
			String msg = null;
			if ((sizeBatch != null) && (nameBatch != null)) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId() + ", size: "
						+ sizeBatch.getBatchId();
			} else if (nameBatch != null) {
				msg = "Potential duplicate file name with Batch Id: "
						+ nameBatch.getBatchId();
			} else if (sizeBatch != null) {
				msg = "Potential duplicate file size with Batch Id: "
						+ sizeBatch.getBatchId();
			}

			// 5427 Convert to upper for import headers.
			if (!isFileAllColumnsMapped()) {
				if (msg == null) {
					msg = "";
				} else {
					msg = msg + "\n";
				}
				msg = msg
						+ " Import file columns not mapped in the Definition.";
			}

			if ((nameBatch != null)
					&& (preferences.getFileNameDuplicateRestrictionType() == DuplicationRestriction.Full)) {
				writeLogAID(msg);
				return false;
			} else if ((sizeBatch != null)
					&& (preferences.getFileSizeDuplicateRestrictionType() == DuplicationRestriction.Full)) {
				writeLogAID(msg);
				return false;
			}
			if (msg != null) {
				writeLogAID(msg); // Just warning. Continue.
			}
			parser.parseFile();
			this.bytesInFile = parser.getFileSize();
			this.estimatedRows = parser.getEstimatedRows();
			// enable the OK button
			this.selectedFile = new File(fileName);
			state = AddState.HAVE_FILE;
		} catch (Exception ex) {
			writeLogAID("Could not read file: " + ex.getMessage());
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		System.out.println("############ main for Automatic Import of Data");

		String fileName = "formatted_text3.txt";
		String filePath = "C:\\AP_IMPORT\\STSTEST\\formatted_text3.txt";
		String specStr = "COLLECTIVE_BRANDS_TEST_SPEC";

		String logFile = "C:\\AP_IMPORT\\STSTEST\\formatted_text3.log";

		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(logFile));
			// out.write("aString");
		} catch (Exception e) {
		}

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out
					.println("xxxxxxxxxxxxxx Can't load class: oracle.jdbc.driver.OracleDriver");
			return;
		}

		Connection conn = null;
		try {
			// Standalone connection
			conn = DriverManager.getConnection(
					"jdbc:oracle:thin:@10.1.106.9:1521:ststest", "stscorp",
					"stscorp");
			conn.setAutoCommit(false);

			ImportMapSaleAddBean importMapAddBean = new ImportMapSaleAddBean();
			if (importMapAddBean.initVerification(conn, out, specStr)) {
				// Continue the process
				importMapAddBean.addActionAID(fileName, filePath);
			} else {
				System.out.println("Error: Stop importing file, " + filePath);
			}

		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		try {
			out.close();
		} catch (Exception e) {
		}
	}

	public Map<String, String> getSystemPreferenceMapAID() {
		logger.debug(" getSystemPreferenceMapAID ");
		Map<String, String> systemPreferenceMap = new HashMap<String, String>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_OPTION where OPTION_TYPE_CODE='SYSTEM' and USER_CODE='SYSTEM'");
			while (rs.next()) {
				systemPreferenceMap.put(rs.getString("OPTION_CODE"),
						rs.getString("VALUE"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getSystemPreferenceMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		logger.debug("exit getSystemPreferenceMap");
		return systemPreferenceMap;
	}

	public Map<String, String> getAdminPreferenceMapAID() {
		Map<String, String> adminPreferenceMap = new HashMap<String, String>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_OPTION where OPTION_TYPE_CODE='ADMIN' and USER_CODE='ADMIN'");
			while (rs.next()) {
				adminPreferenceMap.put(rs.getString("OPTION_CODE"),
						rs.getString("VALUE"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getSystemPreferenceMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return adminPreferenceMap;
	}

	public ImportMapProcessPreferenceDTO getImportMapProcessPreferenceDTOAID() {
		ImportMapProcessPreferenceDTO importMapProcessPreferenceDTO = new ImportMapProcessPreferenceDTO();
		Map<String, String> map = new HashMap<String, String>();
		map.putAll(getSystemPreferenceMapAID());
		map.putAll(getAdminPreferenceMapAID());

		// System - Import/Map/Process
		String fileNameDuplicateRestriction = (String) map
				.get("FILENAMEDUPRES");
		importMapProcessPreferenceDTO
				.setFileNameDuplicateRestriction(fileNameDuplicateRestriction);
		String fileSizeDuplicateRestriction = (String) map
				.get("FILESIZEDUPRES");
		importMapProcessPreferenceDTO
				.setFileSizeDuplicateRestriction(fileSizeDuplicateRestriction);
		String terminalErrorsBeforeAbortImport = (String) map.get("ERRORROWS");
		importMapProcessPreferenceDTO
				.setTerminalErrorsBeforeAbortImport(terminalErrorsBeforeAbortImport);
		String defaultImportDateMap = (String) map.get("IMPORTDATEMAP");
		importMapProcessPreferenceDTO
				.setDefaultImportDateMap(defaultImportDateMap);
		String rowsToImportMapBeforeSave = (String) map.get("MAPROWS");
		importMapProcessPreferenceDTO
				.setRowsToImportMapBeforeSave(rowsToImportMapBeforeSave);
		String endOfFileMarker = (String) map.get("BATCHEOFMARKER");
		importMapProcessPreferenceDTO.setEndOfFileMarker(endOfFileMarker);
		String batchCountMarker = (String) map.get("BATCHCOUNTMARKER");
		importMapProcessPreferenceDTO.setBatchCountMarker(batchCountMarker);
		String batchTotalMarker = (String) map.get("BATCHTOTALMARKER");
		importMapProcessPreferenceDTO.setBatchTotalMarker(batchTotalMarker);
		String deleteTransactionWithZeroAmountFlag = (String) map
				.get("DELETE0AMTFLAG");
		if ("1".equalsIgnoreCase(deleteTransactionWithZeroAmountFlag)) {
			importMapProcessPreferenceDTO
					.setDeleteTransactionWithZeroAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setDeleteTransactionWithZeroAmountFlag(false);
		}
		String holdTransactionAmountFlag = (String) map.get("HOLDTRANSFLAG");
		if ("1".equalsIgnoreCase(holdTransactionAmountFlag)) {
			importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO.setHoldTransactionAmountFlag(false);
		}
		String processTransactionAmountFlag = (String) map.get("AUTOPROCFLAG");
		if ("1".equalsIgnoreCase(processTransactionAmountFlag)) {
			importMapProcessPreferenceDTO.setProcessTransactionAmountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setProcessTransactionAmountFlag(false);
		}
		String killProcessingProcedureCountFlag = (String) map
				.get("KILLPROCFLAG");
		if ("1".equalsIgnoreCase(killProcessingProcedureCountFlag)) {
			importMapProcessPreferenceDTO
					.setKillProcessingProcedureCountFlag(true);
		} else {
			importMapProcessPreferenceDTO
					.setKillProcessingProcedureCountFlag(false);
		}
		String holdTransactionAmount = (String) map.get("HOLDTRANSAMT");
		importMapProcessPreferenceDTO
				.setHoldTransactionAmount(holdTransactionAmount);
		String processTransactionAmount = (String) map.get("AUTOPROCAMT");
		importMapProcessPreferenceDTO
				.setProcessTransactionAmount(processTransactionAmount);
		String applyTaxCodeAmount = (String) map.get("AUTOPROCID");
		importMapProcessPreferenceDTO.setApplyTaxCodeAmount(applyTaxCodeAmount);
		String killProcessingProcedureCount = (String) map.get("KILLPROCCOUNT");
		importMapProcessPreferenceDTO
				.setKillProcessingProcedureCount(killProcessingProcedureCount);
		String doNotImportMarkers = (String) map.get("DONOTIMPORT");
		importMapProcessPreferenceDTO.setDoNotImportMarkers(doNotImportMarkers);
		String replaceWithMarkers = (String) map.get("REPLACEWITH");
		importMapProcessPreferenceDTO.setReplaceWithMarkers(replaceWithMarkers);

		// Admin - Import/Map/Process
		String allocationsEnabled = (String) map.get("ALLOCATIONSENABLED");
		if ("1".equalsIgnoreCase(allocationsEnabled)) {
			importMapProcessPreferenceDTO.setAllocationsEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setAllocationsEnabled(false);
		}

		// System - GL Extract
		String glExtractFilePath = (String) map.get("GLEXTRACTFILEPATH");
		importMapProcessPreferenceDTO.setGlExtractFilePath(glExtractFilePath);
		String glExtractFilePrefix = (String) map.get("GLEXTRACTPREFIX");
		importMapProcessPreferenceDTO
				.setGlExtractFilePrefix(glExtractFilePrefix);
		String glExtractFileExtension = (String) map.get("GLEXTRACTEXT");
		importMapProcessPreferenceDTO
				.setGlExtractFileExtension(glExtractFileExtension);

		// Admin - GL Extract
		String glExtractEnabled = (String) map.get("GLEXTRACTENABLED");
		if ("1".equalsIgnoreCase(glExtractEnabled)) {
			importMapProcessPreferenceDTO.setGlExtractEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setGlExtractEnabled(false);
		}
		String glExtractWindowName = (String) map.get("GLEXTRACTWIN");
		importMapProcessPreferenceDTO
				.setGlExtractWindowName(glExtractWindowName);
		String glExtractSetMark = (String) map.get("GLEXTRACTMARK");
		if ("1".equalsIgnoreCase(glExtractSetMark)) {
			importMapProcessPreferenceDTO.setGlExtractSetMark(true);
		} else {
			importMapProcessPreferenceDTO.setGlExtractSetMark(false);
		}

		// System - Security
		String pwdExpirationWarningDays = (String) map.get("PWEXPDAYS");
		importMapProcessPreferenceDTO
				.setPwdExpirationWarningDays(pwdExpirationWarningDays);
		String chgPassword = (String) map.get("PWUSERCHG");
		if ("1".equalsIgnoreCase(chgPassword)) {
			importMapProcessPreferenceDTO.setChgPassword(true);
		} else {
			importMapProcessPreferenceDTO.setChgPassword(false);
		}
		String denyPasswordChangeMessage = (String) map.get("PWDENYMSG");
		importMapProcessPreferenceDTO
				.setDenyPasswordChangeMessage(denyPasswordChangeMessage);

		// Admin - Security
		String activateExtendedSecurity = (String) map.get("EXTENDSEC");
		if ("1".equalsIgnoreCase(activateExtendedSecurity)) {
			importMapProcessPreferenceDTO.setActivateExtendedSecurity(true);
		} else {
			importMapProcessPreferenceDTO.setActivateExtendedSecurity(false);
		}
		String activateDebugExtendedSecurity = (String) map.get("DEBUGSEC");
		if ("1".equalsIgnoreCase(activateDebugExtendedSecurity)) {
			importMapProcessPreferenceDTO
					.setActivateDebugExtendedSecurity(true);
		} else {
			importMapProcessPreferenceDTO
					.setActivateDebugExtendedSecurity(false);
		}

		// System - Directories
		String sqlFilePath = (String) map.get("SQLFILEPATH");
		importMapProcessPreferenceDTO.setSqlFilePath(sqlFilePath);
		String sqlFileExt = (String) map.get("SQLFILEEXT");
		importMapProcessPreferenceDTO.setSqlFileExt(sqlFileExt);
		String impExpFilePath = (String) map.get("IMPEXPFILEPATH");
		importMapProcessPreferenceDTO.setImpExpFilePath(impExpFilePath);
		String impExpFileExt = (String) map.get("IMPEXPFILEEXT");
		importMapProcessPreferenceDTO.setImpExpFileExt(impExpFileExt);
		String refDocFilePath = (String) map.get("REFDOCFILEPATH");
		importMapProcessPreferenceDTO.setRefDocFilePath(refDocFilePath);
		String refDocFileExt = (String) map.get("REFDOCFILEEXT");
		importMapProcessPreferenceDTO.setRefDocFileExt(refDocFileExt);
		String defaultDirCrystal = (String) map.get("DEFAULTDIRCRYSTAL");
		importMapProcessPreferenceDTO.setDefaultDirCrystal(defaultDirCrystal);

		// User - Directories
		String importFilePath = (String) map.get("IMPORTFILEPATH");
		importMapProcessPreferenceDTO.setImportFilePath(importFilePath);
		String importFileExt = (String) map.get("IMPORTFILEEXT");
		importMapProcessPreferenceDTO.setImportFileExt(importFileExt);
		String userDirCrystal = (String) map.get("USERDIRCRYSTAL");
		importMapProcessPreferenceDTO.setUserDirCrystal(userDirCrystal);

		// System - Tax Updates
		String lastTaxRateDateFull = (String) map.get("LASTTAXRATEDATEFULL");
		importMapProcessPreferenceDTO
				.setLastTaxRateDateFull(lastTaxRateDateFull);
		String lastTaxRateRelFull = (String) map.get("LASTTAXRATERELFULL");
		importMapProcessPreferenceDTO.setLastTaxRateRelFull(lastTaxRateRelFull);
		String lastTaxRateDateUpdate = (String) map
				.get("LASTTAXRATEDATEUPDATE");
		importMapProcessPreferenceDTO
				.setLastTaxRateDateUpdate(lastTaxRateDateUpdate);
		String lastTaxRateRelUpdate = (String) map.get("LASTTAXRATERELUPDATE");
		importMapProcessPreferenceDTO
				.setLastTaxRateRelUpdate(lastTaxRateRelUpdate);
		String taxWarnRate = (String) map.get("TAXWARNRATE");
		importMapProcessPreferenceDTO.setTaxWarnRate(taxWarnRate);

		// System - Compliance
		String defCCHTaxCatCode = (String) map.get("DEFCCHTAXCATCODE");
		importMapProcessPreferenceDTO.setDefCCHTaxCatCode(defCCHTaxCatCode);
		String defCCHGroupCode = (String) map.get("DEFCCHGROUPCODE");
		importMapProcessPreferenceDTO.setDefCCHGroupCode(defCCHGroupCode);
		String tpExtractFilePath = (String) map.get("TPEXTRACTEFILEPATH");
		importMapProcessPreferenceDTO.setTpExtractFilePath(tpExtractFilePath);
		String tpExtractFilePrefix = (String) map.get("TPEXTRACTPREFIX");
		importMapProcessPreferenceDTO
				.setTpExtractFilePrefix(tpExtractFilePrefix);
		String tpExtractExt = (String) map.get("TPEXTRACTEXT");
		importMapProcessPreferenceDTO.setTpExtractExt(tpExtractExt);

		// Admin - Compliance
		String tpExtractEnabled = (String) map.get("TPEXTRACTENABLED");
		if ("1".equalsIgnoreCase(tpExtractEnabled)) {
			importMapProcessPreferenceDTO.setTpExtractEnabled(true);
		} else {
			importMapProcessPreferenceDTO.setTpExtractEnabled(false);
		}

		return importMapProcessPreferenceDTO;
	}

	private BatchMaintenance validateFileNamesAID(String fileName) {
		BatchMaintenance batchMaintenance = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("SELECT * FROM TB_BATCH WHERE BATCH_STATUS_CODE not like 'AB%' and upper(VC02)='"
							+ fileName.toUpperCase() + "'");
			if (rs.next()) {
				batchMaintenance = new BatchMaintenance();
				batchMaintenance.setBatchId(rs.getLong("BATCH_ID"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ validateFileNamesAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	private BatchMaintenance validateFileLengthsAID(String fileName) {
		BatchMaintenance batchMaintenance = null;
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("SELECT * FROM TB_BATCH WHERE BATCH_STATUS_CODE not like 'ABI' and TOTAL_BYTES="
							+ parser.getFileSize());
			if (rs.next()) {
				batchMaintenance = new BatchMaintenance();
				batchMaintenance.setBatchId(rs.getLong("BATCH_ID"));
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ validateFileLengthsAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return batchMaintenance;
	}

	private List<ImportDefinitionDetail> getMappedColumnsAID() {
		if ((this.mappedColumns == null) && (selectedDefinition != null)) {
			mappedColumns = new ArrayList<ImportDefinitionDetail>();
			Statement stat = null;
			try {
				stat = conn.createStatement();
				ResultSet rs = stat
						.executeQuery("select * from TB_IMPORT_DEFINITION_DETAIL where IMPORT_DEFINITION_CODE='"
								+ selectedDefinition.getImportDefinitionCode()
								+ "'");
				while (rs.next()) {
					ImportDefinitionDetail importDefinitionDetail = new ImportDefinitionDetail();

					importDefinitionDetail.setImportDefinitionCode(rs
							.getString("IMPORT_DEFINITION_CODE"));
					importDefinitionDetail.setTransDtlColumnName(rs
							.getString("TRANS_DTL_COLUMN_NAME"));
					importDefinitionDetail.setImportColumnName(rs
							.getString("IMPORT_COLUMN_NAME"));
					importDefinitionDetail.setSelectClause(rs
							.getString("SELECT_CLAUSE"));
					mappedColumns.add(importDefinitionDetail);
				}
				rs.close();
			} catch (Exception e) {
				// logger.error("############ getMappedColumnsAID() failed: " +
				// e.toString());
			} finally {
				if (stat != null) {
					try {
						stat.close();
					} catch (Exception ex) {
					}
				}
			}
		}
		return mappedColumns;
	}

	private Map<String, DriverNames> getTransactionColumnDriverNamesMapAID() {
		Map<String, DriverNames> result = new LinkedHashMap<String, DriverNames>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_DRIVER_NAMES order by DRIVER_ID");
			while (rs.next()) {
				DriverNames driverNames = new DriverNames();

				DriverNamesPK driverNamesPK = new DriverNamesPK(
						rs.getString("DRIVER_NAMES_CODE"),
						rs.getLong("DRIVER_ID"));
				driverNames.setDriverNamesPK(driverNamesPK);

				driverNames.setTransDtlColName(rs
						.getString("TRANS_DTL_COLUMN_NAME"));
				driverNames
						.setMatrixColName(rs.getString("MATRIX_COLUMN_NAME"));
				driverNames.setBusinessUnitFlag(rs
						.getString("BUSINESS_UNIT_FLAG"));
				driverNames.setMandatoryFlag(rs.getString("MANDATORY_FLAG"));
				driverNames.setNullDriverFlag(rs.getString("NULL_DRIVER_FLAG"));
				driverNames.setWildcardFlag(rs.getString("WILDCARD_FLAG"));
				driverNames.setRangeFlag(rs.getString("RANGE_FLAG"));
				driverNames.setToNumberFlag(rs.getString("TO_NUMBER_FLAG"));
				driverNames.setActiveFlag(rs.getString("ACTIVE_FLAG"));

				result.put(driverNames.getTransDtlColName(), driverNames);
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getTransactionColumnDriverNamesMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	private Map<String, DataDefinitionColumn> getBCPTransactionsColumnMapAID() {
		Map<String, DataDefinitionColumn> result = new LinkedHashMap<String, DataDefinitionColumn>();

		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_DATA_DEF_COLUMN where TABLE_NAME='TB_BCP_TRANSACTIONS' order by COLUMN_NAME");
			while (rs.next()) {
				DataDefinitionColumn dataDefinitionColumn = new DataDefinitionColumn();

				DataDefinitionColumnPK ataDefinitionColumnPK = new DataDefinitionColumnPK(
						rs.getString("TABLE_NAME"), rs.getString("COLUMN_NAME"));
				dataDefinitionColumn
						.setDataDefinitionColumnPK(ataDefinitionColumnPK);

				dataDefinitionColumn.setDataType(rs.getString("DATA_TYPE"));
				dataDefinitionColumn.setDatalength(rs.getLong("DATA_LENGTH"));
				dataDefinitionColumn
						.setDescription(rs.getString("DESCRIPTION"));
				dataDefinitionColumn.setAbbrDesc(rs.getString("ABBR_DESC"));
				dataDefinitionColumn.setDescColumnName(rs
						.getString("DESC_COLUMN_NAME"));
				dataDefinitionColumn.setDefinition(rs.getString("DEFINITION"));
				dataDefinitionColumn.setMapFlag(rs.getString("MAP_FLAG"));
				dataDefinitionColumn.setMinimumViewFlag(rs
						.getString("MINIMUM_VIEW_FLAG"));

				// Do some fixups to make sure AbbrDesc and Desc always exist!
				String property = Util.columnToProperty(dataDefinitionColumn
						.getColumnName());
				String abbr = dataDefinitionColumn.getAbbrDesc();
				if ((abbr == null) || abbr.equals("")) {
					// jmw - This is trying to set value to something > the max
					// field size
					// rather than try to get the size for tb_data_def_column
					// from tb_data_def_column
					// I am hardcoding for now
					String s = Util.propertyToLabel(property);
					dataDefinitionColumn.setAbbrDesc(s.substring(0,
							Math.min(20, s.length()) - 1));
				}
				String desc = dataDefinitionColumn.getDescription();
				if ((desc == null) || desc.equals("")) {
					String s = Util.propertyToLabel(property);
					dataDefinitionColumn.setDescription(s.substring(0,
							Math.min(40, s.length()) - 1));
				}

				result.put(dataDefinitionColumn.getColumnName(),
						dataDefinitionColumn);
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getBCPTransactionsColumnMapAID() failed: "
			// + e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}

	private Map<String, ListCodes> getErrorListCodesAID() {
		Map<String, ListCodes> result = new LinkedHashMap<String, ListCodes>();
		Statement stat = null;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat
					.executeQuery("select * from TB_LIST_CODE where CODE_TYPE_CODE='ERRORCODE' order by DESCRIPTION");
			while (rs.next()) {
				ListCodes listCodes = new ListCodes();

				ListCodesPK listCodesPK = new ListCodesPK(
						rs.getString("CODE_TYPE_CODE"),
						rs.getString("CODE_CODE"));
				listCodes.setListCodesPK(listCodesPK);

				listCodes.setDescription(rs.getString("DESCRIPTION"));
				listCodes.setMessage(rs.getString("MESSAGE"));
				listCodes.setExplanation(rs.getString("EXPLANATION"));
				listCodes.setSeverityLevel(rs.getString("SEVERITY_LEVEL"));
				listCodes.setWriteImportLineFlag(rs
						.getString("WRITE_IMPORT_LINE_FLAG"));
				listCodes.setAbortImportFlag(rs.getString("ABORT_IMPORT_FLAG"));

				result.put(listCodes.getCodeCode(), listCodes);
			}
			rs.close();
		} catch (Exception e) {
			// logger.error("############ getErrorListCodesAID() failed: " +
			// e.toString());
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (Exception ex) {
				}
			}
		}

		return result;
	}
}
