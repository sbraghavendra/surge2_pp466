package com.ncsts.view.bean;

import java.util.List;

import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import org.springframework.stereotype.Component;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.VendorItem;
import com.ncsts.dto.TaxCodeStateDTO;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.VendorItemService;

public class VendorLookupHandler implements BaseMatrixBackingBean.FindIdCallback,VendorBackingBean.FindVendorIdCallback {
   	
	private HtmlInputText idInput = new HtmlInputText();
	private HtmlInputText codeInput = new HtmlInputText();
	private HtmlInputText nameInput = new HtmlInputText();
	private HtmlInputText typeInput = new HtmlInputText();
	private HtmlInputText typenameInput = new HtmlInputText();
	private HtmlInputText addressline1Input = new HtmlInputText();
	private HtmlInputText addressline2Input = new HtmlInputText();
	private HtmlInputText geocodeInput = new HtmlInputText();
   	private HtmlInputText cityInput = new HtmlInputText();
   	private HtmlInputText countyInput = new HtmlInputText();
   	private HtmlSelectOneMenu stateInput = new HtmlSelectOneMenu();
   	private HtmlSelectOneMenu countryInput = new HtmlSelectOneMenu();
   	private HtmlInputText zipInput = new HtmlInputText();
	
   	private SearchJurisdictionHandler jurisdictionHandler;
	private String id;
	private String code;
	private String name;
	private String type;
	private String typename;
	private String addressline1;
	private String addressline2;
   	private String geocode;
   	private String city;
   	private String county;
   	private String state;
   	private String country;
   	private String zip;
	private JurisdictionService jurisdictionService;
	private VendorItemService vendorItemService;
	private CacheManager cacheManager;
	private TaxJurisdictionBackingBean taxJurisdictionBean;
	private VendorBackingBean vendorBackingBean;
	private String callbackScreen = "";
	private List<SelectItem> stateMenuItems;
	
	public SearchJurisdictionHandler getJurisdictionHandler() {
		if(jurisdictionHandler == null)
			jurisdictionHandler = new SearchJurisdictionHandler();
		return jurisdictionHandler;
	}

	public void setJurisdictionHandler(SearchJurisdictionHandler jurisdictionHandler) {
		this.jurisdictionHandler = jurisdictionHandler;
	}

	public VendorItemService getVendorItemService() {
		return vendorItemService;
	}
	
	public void setVendorItemService(VendorItemService vendorItemService ) {
		this.vendorItemService = vendorItemService;
	}

	public VendorBackingBean getVendorBackingBean() {
		return vendorBackingBean;
	}

	public void setVendorBackingBean(VendorBackingBean vendorBackingBean) {
		this.vendorBackingBean = vendorBackingBean;
	}

	public void setCacheManager(CacheManager cacheManager) {
		if (this.cacheManager == null) {
			this.cacheManager = cacheManager;
		}
	}
	
	public void setTaxJurisdictionBean(TaxJurisdictionBackingBean taxJurisdictionBean) {
		this.taxJurisdictionBean = taxJurisdictionBean;
	}
	
	public void setCallbackScreen(String callbackScreen) {
		this.callbackScreen = callbackScreen;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}
	
	public HtmlInputText getIdInput() {
		return idInput;
	}

	public HtmlInputText getCodeInput() {
		return codeInput;
	}

	public HtmlInputText getNameInput() {
		return nameInput;
	}

	public HtmlInputText getTypeInput() {
		return typeInput;
	}
	
	public String getType() {
		return type;
	}

	public String getTypename() {
		return typename;
	}

	public void setType(String type) {
		this.type = type;
		setInputValue(typeInput, type);
	}

	public void setTypename(String typename) {
		this.typename = typename;
		setInputValue(typenameInput, typename);
	}

	public HtmlInputText getTypenameInput() {
		return typenameInput;
	}

	public HtmlInputText getAddressline1Input() {
		return addressline1Input;
	}

	public HtmlInputText getAddressline2Input() {
		return addressline2Input;
	}

	public String getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getAddressline1() {
		return addressline1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public HtmlSelectOneMenu getCountryInput() {
		if (this.cacheManager != null){
			List<SelectItem> activeCountryItems = cacheManager.createCountryItems();
			UISelectItems items = new UISelectItems();
			items.setValue(activeCountryItems);
			countryInput.getChildren().clear();
			countryInput.getChildren().add(items);
		}	    
		
		return countryInput;
	}

	public void setIdInput(HtmlInputText idInput) {
		this.idInput = idInput;
	}

	public void setCodeInput(HtmlInputText codeInput) {
		this.codeInput = codeInput;
	}

	public void setNameInput(HtmlInputText nameInput) {
		this.nameInput = nameInput;
	}

	public void setTypeInput(HtmlInputText typeInput) {
		this.typeInput = typeInput;
	}

	public void setTypenameInput(HtmlInputText typenameInput) {
		this.typenameInput = typenameInput;
	}

	public void setAddressline1Input(HtmlInputText addressline1Input) {
		this.addressline1Input = addressline1Input;
	}

	public void setAddressline2Input(HtmlInputText addressline2Input) {
		this.addressline2Input = addressline2Input;
	}

	public void setId(String id) {
		this.id = id;
		setInputValue(idInput, id);
	}

	public void setCode(String code) {
		this.code = code;
		setInputValue(codeInput, code);
	}

	public void setName(String name) {
		this.name = name;
		setInputValue(nameInput, name);
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
		setInputValue(addressline1Input, addressline1);
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
		setInputValue(addressline2Input, addressline2);
	}

	public HtmlInputText getGeocodeInput() {
		return geocodeInput;
	}

	public void setGeocodeInput(HtmlInputText geocodeInput) {
		this.geocodeInput = geocodeInput;
	}

	public HtmlInputText getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlInputText cityInput) {
		this.cityInput = cityInput;
	}

	public HtmlInputText getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlInputText countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getStateInput() {
		return stateInput;
	}
	
	public void setCountryInput(HtmlSelectOneMenu countryInput) {
		this.countryInput = countryInput;
	}

	public void setStateInput(HtmlSelectOneMenu stateInput) {
		this.stateInput = stateInput;
	}

	public HtmlInputText getZipInput() {
		return zipInput;
	}

	public void setZipInput(HtmlInputText zipInput) {
		this.zipInput = zipInput;
	}
	
	
	public String getGeocode() {
		return trimAndUpper(geocode);
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
		setInputValue(geocodeInput, geocode);
	}

	public String getCity() {
		return trimAndUpper(city);
	}

	public void setCity(String city) {
		this.city = city;
		setInputValue(cityInput, city);
	}

	public String getCounty() {
		return trimAndUpper(county);
	}

	public void setCounty(String county) {
		this.county = county;
		setInputValue(countyInput, county);
	}
	
	public void setZip(String zip) {
		this.zip = zip;
		setInputValue(zipInput, zip);
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setState(String state) {
		this.state = state;
		setInputValue(stateInput, state);
	}
	
	public void setCountry(String country) {
		this.country = country;
		setInputValue(countryInput, country);
		
		stateMenuItems = null;
	}
	
	public String getCountry() {
		return trimAndUpper(country);
	}


	public String getState() {
		return trimAndUpper(state);
	}
	
	public List<SelectItem> getStateItems() {
		if(stateMenuItems==null){
			stateMenuItems = cacheManager.createStateItems(country);
		}
		
		return stateMenuItems;
    }
	
	public String getCountryDescription() {
		if(country!=null && country.length()>0){
			String countryDesc = cacheManager.getCountryMap().get(country);
			if (countryDesc == null) {
				return country;
			}
			else {
				return countryDesc + " (" + country + ")";
			
			}
		}
		else{
			return "";
		}
	}
	
	public String getStateDescription() {
		if(state!=null && state.length()>0){
			
			TaxCodeStateDTO aTaxCodeStateDTO = cacheManager.getTaxCodeStateMap().get(state);
			if(aTaxCodeStateDTO!=null){
				return aTaxCodeStateDTO.getName() + " (" + state + ")";
			}
			else{
				return " (" + state + ")";	
			}
		}
		else{
			return "";
		}
	}
	
	public void reset() {
		setGeocode("");
		setCity("");
		setCounty("");
		setState("");
		setCountry("");
		setZip("");
		jurisdictionHandler=null;
	}
	
	public String findTxnVendorJurisAction() {
		Jurisdiction jurisdictionFilter = new Jurisdiction();
		jurisdictionFilter.setCountry(getInputValue(countryInput));
		jurisdictionFilter.setCity(getInputValue(cityInput));
		jurisdictionFilter.setCounty(getInputValue(countyInput));
		jurisdictionFilter.setState(getInputValue(stateInput));
		jurisdictionFilter.setZip(getInputValue(zipInput));
		taxJurisdictionBean.findId(this, "jurisdictionId", callbackScreen, jurisdictionFilter);
		return "maintanence_tax_rates";
	}
	
	public void findIdCallback(Long id, String context) {
		if (context.equalsIgnoreCase("jurisdictionId")) {
			Jurisdiction selectedJurisdiction = jurisdictionService.findById(id);
			setJurisdiction(selectedJurisdiction);
		}
			
	}
	public String findVendorIdAction() {
		vendorBackingBean.findId(this, "vendorId", callbackScreen);
		SearchJurisdictionHandler testHandler = vendorBackingBean.getFilterHandlerShipto();
		testHandler.setCity(getCity());
		testHandler.setGeocode(getGeocode());
		testHandler.setCountry(getCountry());
		testHandler.setState(getState());
		testHandler.setZip(getZip());
		testHandler.setCounty(getCounty());
		
		vendorBackingBean.setVendorCode(getCode());
		vendorBackingBean.setVendorName(getName());
        return "vendor_main";
	}

	public void findVendorIdCallback(Long id, String code, String context) {
		if (context.equalsIgnoreCase("vendorId")) {
			VendorItem vendorItem = vendorItemService.findById(id);
			setVendorDetails(vendorItem);
			
	    }
	} 
	
	public void setVendorDetails(VendorItem vendorItem) {
		setId(String.valueOf(vendorItem.getVendorId()));
		setCode(vendorItem.getVendorCode());
		setName(vendorItem.getVendorName());
		setType(null); 
		setTypename(null); 
		setAddressline1(vendorItem.getAddressLine1());
		setAddressline2(vendorItem.getAddressLine2());
		setGeocode(vendorItem.getGeocode());
		setCity(vendorItem.getCity());
		setCounty(vendorItem.getCounty());
		setCountry(vendorItem.getCountry());
		setState(vendorItem.getState());
		setZip(vendorItem.getZip());
		
	}
	
	public void setJurisdiction(Jurisdiction jurisdiction) {
		setGeocode((jurisdiction == null)? null:jurisdiction.getGeocode());
		setCity((jurisdiction == null)? null:jurisdiction.getCity());
		setCounty((jurisdiction == null)? null:jurisdiction.getCounty());
		setState((jurisdiction == null)? null:jurisdiction.getState());
		setCountry((jurisdiction == null)? null:jurisdiction.getCountry());
		setZip((jurisdiction == null)? null:jurisdiction.getZip());
	}
	
	public String getInputValueOfGeocode(){
		return getInputValue(geocodeInput);
	}
	
	public String getInputValueOfCity(){
		return getInputValue(cityInput);
	}
	
	public String getInputValueOfCounty(){
		return getInputValue(countyInput);
	}
	
	public String getInputValueOfState(){
		return getInputValue(stateInput);
	}

	public String getInputValueOfCountry(){
		return getInputValue(countryInput);
	}
	
	public String getInputValueOfZip(){
		return getInputValue(zipInput);
	}
	

	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	private String getInputValue(UIInput input) {
		String value = (String) input.getSubmittedValue();
		value = (value != null)? value:(String) input.getValue();
		return trimAndUpper(value);
	}
	
	public String trimAndUpper(String value){
		if(value==null){
			return "";
		}
		else{
			return value.trim().toUpperCase();
		}
	}
}

