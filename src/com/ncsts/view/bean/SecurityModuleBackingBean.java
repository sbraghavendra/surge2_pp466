package com.ncsts.view.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.component.html.HtmlListShuttle;
import org.richfaces.component.html.HtmlScrollableDataTable;
import org.richfaces.component.html.HtmlTree;
import org.richfaces.event.NodeSelectedEvent;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.richfaces.model.selection.Selection;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DriverNames;
import com.ncsts.domain.DriverReference;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.Menu;
import com.ncsts.domain.Role;
import com.ncsts.domain.RoleMenu;
import com.ncsts.domain.TaxMatrix;
import com.ncsts.domain.User;
import com.ncsts.domain.UserEntity;
import com.ncsts.domain.UserEntityPK;
import com.ncsts.domain.UserMenuEx;
import com.ncsts.domain.UserMenuExPK;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.dto.EntityLevelDTO;
import com.ncsts.dto.MenuDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.dto.UserEntityDTO;
import com.ncsts.dto.UserEntityRoleDTO;
import com.ncsts.dto.UserSecurityDTO;
import com.ncsts.management.UserSecurityBean;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.DriverReferenceService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.MenuService;
import com.ncsts.services.RoleService;
import com.ncsts.services.RoleUserMenuService;
import com.ncsts.services.UserEntityService;
import com.ncsts.services.UserMenuExService;
import com.ncsts.services.UserService;
import com.ncsts.view.util.ConfigSetting;
import com.ncsts.view.util.DesEncrypter;
import com.ncsts.view.util.FacesUtils;

public class SecurityModuleBackingBean {
    
    private Logger logger = LoggerFactory.getInstance().getLogger(SecurityModuleBackingBean.class);	
    
	private EditAction currentAction = EditAction.VIEW;
	
	private static final String ITEM_ROLES = "Roles";
    private static final String ITEM_USERS = "Users";
    private static final String ITEM_MENU_OPTIONS = "Menu Options";
    private static final String	ITEM_ENTITIES = "Entities";
    private String tableName = "TB_PURCHTRANS";
    
    public static String MAP_MAPPED = "mapped";
    public static String MAP_SELECTED = "selected";
    public static String MAP_NONE = "";
    

    public static final String ROLE_EXCEPTION_TYPE = "";
    public static final String GRANTED_EXCEPTION_TYPE = "G";
    public static final String DENIED_EXCEPTION_TYPE = "D";
    
	private boolean viewUsers; 
	private boolean viewRoles;
	private boolean viewMenus;	
	private boolean viewEntityItems;	
	private boolean viewEntityLevels = true;
	private boolean viewUserEntitys = true;	
	private boolean viewColNameTable = true;
	private boolean viewSublist;
	private List<User> usersList = new ArrayList<User>();
	private List<Role> rolesList = new ArrayList<Role>();
	private List<Menu> menusList = new ArrayList<Menu>();	
	private List<EntityItem> entityItemsList = new ArrayList<EntityItem>();	
	private List<EntityLevel> entityLevelsList = new ArrayList<EntityLevel>();	
	private List <EntityLevel> selectedEntityLevelList = new ArrayList<EntityLevel>();
	private List<UserEntity> userEntityList = new ArrayList<UserEntity>();	
	private List<DataDefinitionColumn> dataDefinitionColList = new ArrayList<DataDefinitionColumn>();
	private CacheManager cacheManager;
    private UserService userService;       
    private RoleService roleService;
    private MenuService menuService;
    private EntityItemService entityItemService; 
    private EntityLevelService entityLevelService;
    private UserEntityService userEntityService;
    //private ListCodesService listCodesService;
    private DataDefinitionService dataDefinitionService;
    private String selectedView;
    private String selectedUserRole;
    private String selectedLevelOneCombo;
    private String selectedLevelTwoCombo;
    private String  selectedLevelThreeCombo;
    private String selectedLevelFourCombo;
    private String rerender;
    private Role selectedRole;
    private User selectedUser;
    private Menu selectedMenu;  
    private EntityItem selectedEntity;
    private EntityLevel selectedEntityLevel;
    private UserEntity selectedUserEntity;    

    private Role selectedRoleOri;
    private User selectedUserOri;
    private Menu selectedMenuOri;
    private EntityItem selectedEntityOri;
    
    private String mode;
    private long nextEntity;
    private List<SelectItem> mainMenuItems;
    private List<SelectItem> commandTypeItems;
    private DataDefinitionColumn entityLevelToSet;
    private boolean transColDisplay=false;
    private boolean displayUpdateButton = false;
    private boolean displayAddButton = false;
    private boolean updateDisplay=false;
    private boolean disableDeleteButton=false;
    private boolean showUpdateButton=false;
    private boolean showOkButton = false;
        
    private HtmlInputSecret pwdControl = new HtmlInputSecret(); 
    private HtmlInputSecret newPwdControl = new HtmlInputSecret();  
    private HtmlInputSecret confirmPwdControl = new HtmlInputSecret(); 
    private String pwdChangeMessage;
    private HtmlInputSecret pwdEmailControl = new HtmlInputSecret(); 
    private HtmlInputSecret newEmailControl = new HtmlInputSecret();  
    private String oldEmail = "";  
    
    private String emailChangeMessage;
    
    private HtmlInputText entityLevelId = new HtmlInputText();
    private HtmlInputText transDetailColumnName = new HtmlInputText();
    private HtmlInputText description = new HtmlInputText();
    private HtmlInputText updateUserId = new HtmlInputText();
    private HtmlInputText updateTimestamp = new HtmlInputText();
    private HtmlInputText columnSearch = new HtmlInputText(); 
    private HtmlInputText descriptionSearch = new HtmlInputText();
    private HtmlInputText dataTypeSearch = new HtmlInputText();
    private Date currentTime;
        
    private List<Menu> grantedList = new ArrayList<Menu>();	
    private List<Menu> deniedList = new ArrayList<Menu>();

	private RoleUserMenuService roleUserMenuService;

	private DriverReferenceService driverReferenceService;
	private EntityItemDTO entityItemDTO;
	private List<DriverReference> transctionDetailColumn = new ArrayList<DriverReference>();

	private ArrayList<UserEntityRoleDTO> userEntityRoleList;
	private ArrayList<UserEntityRoleDTO> saveMapping = new ArrayList<UserEntityRoleDTO>();
	private ArrayList<UserEntityRoleDTO> deleteMapping = new ArrayList<UserEntityRoleDTO>();;
	private ArrayList<UserEntityRoleDTO> changeMapping = new ArrayList<UserEntityRoleDTO>();;
	private Collection<Role> selectedRoleList;
	private Collection<EntityItem> selectedEntityList;
	private Collection<UserEntityRoleDTO> selectedMappingList;

	private UserEntityRoleDTO selectedMapping;
	private ArrayList<MenuDTO> defaultMenu;
	private ArrayList<MenuDTO> deniedMenu;

	private UserMenuExService userMenuExService;
	private List<UserEntityDTO> userEntityMapList;  
	private List<SelectItem> userRoleComboItems ;
	/*private List<SelectItem> levelOneCombo;
	private List<SelectItem> levelTwoCombo = new ArrayList<SelectItem>();;
	private List<SelectItem> levelThreeCombo = new ArrayList<SelectItem>();;
	private List<SelectItem> levelFourCombo = new ArrayList<SelectItem>();;*/
	
	private HtmlSelectOneMenu levelOneCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu levelTwoCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu levelThreeCombo = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu levelFourCombo = new HtmlSelectOneMenu();
	

	private EntityItemDTO parentEntityAndLevelInfo;
	
	private ConfigSetting userConfig;
	
	private int selectedRowIndex = -1;
	private int selectedEntityLevelRowIndex = -1;
	
	private loginBean loginBean;
	
	private UserSecurityBean userSecurityBean;
	
    private TreeNode rootNode = null;
    private EntityItem selectedEntityItem = null;
    private Map<Long, EntityItem > globalEntityArray = new LinkedHashMap<Long, EntityItem >();
    private static final String USER_STSCORP = "STSCORP";
    
    private static final String TWO = "2";
    private static final String ONE = "1";
    private static final String ZERO = "0";
    
    public void init() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    UserDTO user = (UserDTO) session.getAttribute("user");
        if(USER_STSCORP.equals(user.getUserCode()) || TWO.equals(user.getAdminFlag())){
        	viewUsers = false;
            viewMenus = false;
            viewEntityItems = false;  
            this.setRerender("rolesTable");
            this.setMode(ITEM_ROLES);   
        	viewRoles = true;
	    	List<Role> rolesList = roleService.findAllRoles();
	        this.setRolesList(rolesList);
	        userRoleComboItems = new ArrayList<SelectItem>();
	        userRoleComboItems.add(new SelectItem("0","Select a Role"));
	        for(Role role : rolesList)
	            userRoleComboItems.add(new SelectItem(role.getRoleCode(),role.getRoleCode()+"-"+role.getRoleName()));
	        this.setUsersList(null); 
	        this.setMenusList(null);
	        this.setEntityItemsList(null);   
	        viewUsers = false;
	        viewMenus = false;
	         viewEntityItems = false;  
	        this.setRerender("rolesTable");
	        this.setMode(ITEM_ROLES);  
        }
        else {
        	viewUserDetails();
        }
	}
  
    private void addNodes(String path, TreeNode node, Long parentId) {
        //Add to local array
        Map<Long, EntityItem > localEntityArray = new LinkedHashMap<Long, EntityItem >();
    	for (Long entityKey : globalEntityArray.keySet()) {
    		EntityItem entityItem = (EntityItem)globalEntityArray.get(entityKey);
    		if(parentId.equals(entityItem.getParentEntityId())){
    			localEntityArray.put(entityKey, entityItem);
			}
    	}
    	
    	//Remove 
    	for (Long entityKey : localEntityArray.keySet()) {
    		globalEntityArray.remove(entityKey);
    	}
    	
        //Build
        for(EntityItem tcd : localEntityArray.values()) {
        	TreeNodeImpl nodeImpl = new TreeNodeImpl();
            nodeImpl.setData(tcd);
            node.addChild(tcd.getEntityId(), nodeImpl);

            addNodes(null, nodeImpl, tcd.getEntityId());		
		}
    }

    private void loadTree() {
        try {
        	EntityItem rootEntityItem = (EntityItem)globalEntityArray.get(new Long(0));
            
            rootNode = new TreeNodeImpl();
            rootNode.setData(rootEntityItem);
              
            TreeNodeImpl nodeImpl = new TreeNodeImpl();
            nodeImpl.setData(rootEntityItem);
            rootNode.addChild(rootEntityItem.getEntityId(), nodeImpl);
            
            globalEntityArray.remove(new Long(0));
            addNodes(null, nodeImpl, new Long(0));
            
        } catch (Exception e) {
            throw new FacesException(e.getMessage(), e);
        } finally {

        }
    }
    
    public void processSelection(NodeSelectedEvent event) {
        HtmlTree tree = (HtmlTree) event.getComponent();
        EntityItem entityItem = (EntityItem) tree.getRowData();
        if(rootNode!=null){
        	TreeNode treeNode = rootNode.getChild(entityItem.getEntityId());
        	if(treeNode!=null){
        		EntityItem aEntityItem = (EntityItem)treeNode.getData();
        	}	
        }
        
        if(entityItem.getType().equalsIgnoreCase(MAP_MAPPED) || entityItem.getType().equalsIgnoreCase(MAP_SELECTED)){
        }
        else{
        	if(selectedEntityItem!=null){
            	selectedEntityItem.setType(MAP_NONE);
            }
        	entityItem.setType(MAP_SELECTED);
    		selectedEntityItem = entityItem;
        }
    }
    
    public EntityItem findEntityItemById(TreeNode parent, Long ID){
    	EntityItem entityItem = null;
    	Iterator<Map.Entry<Object, TreeNode>> it = parent.getChildren();
        while (it!=null &&it.hasNext()) {
            Map.Entry<Object, TreeNode> entry = it.next();
            
            if(ID.equals(entry.getKey())){
            	entityItem = (EntityItem)entry.getValue().getData();
            	break;
            }
            
            entityItem = findEntityItemById(entry.getValue(), ID);
            if(entityItem!=null){
            	break;
            }
        }
        
        return entityItem;
    }
    
    public TreeNode getTreeNode() {
        if (rootNode == null) {
            loadTree();
        }
        
        return rootNode;
    }

	public UserSecurityBean getUserSecurityBean() {
		return userSecurityBean;
	}

	public void setUserSecurityBean(UserSecurityBean userSecurityBean) {
		this.userSecurityBean= userSecurityBean;
	}
	
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}
	
	public int getSelectedEntityLevelRowIndex() {
		return selectedEntityLevelRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
	
	public void setSelectedEntityLevelRowIndex(int selectedEntityLevelRowIndex) {
		this.selectedEntityLevelRowIndex = selectedEntityLevelRowIndex;
	}

	public boolean isShowUpdateButton() {
		return showUpdateButton;
	}

	public void setShowUpdateButton(boolean showUpdateButton) {
		this.showUpdateButton = showUpdateButton;
	}

	public boolean isDisableDeleteButton() {
		return disableDeleteButton;
	}

	public void setDisableDeleteButton(boolean disableDeleteButton) {
		this.disableDeleteButton = disableDeleteButton;
	}

	public boolean isUpdateDisplay() {
		return updateDisplay;
	}

	public void setUpdateDisplay(boolean updateDisplay) {
		this.updateDisplay = updateDisplay;
	}

	public boolean isDisplayAddButton() {
		return displayAddButton;
	}

	public void setDisplayAddButton(boolean displayAddButton) {
		this.displayAddButton = displayAddButton;
	}

	public boolean isDisplayUpdateButton() {
		return displayUpdateButton;
	}

	public void setDisplayUpdateButton(boolean displayUpdateButton) {
		this.displayUpdateButton = displayUpdateButton;
	}


	public HtmlInputText getDescriptionSearch() {
		return descriptionSearch;
	}

	public void setDescriptionSearch(HtmlInputText descriptionSearch) {
		this.descriptionSearch = descriptionSearch;
	}

	public HtmlInputText getDataTypeSearch() {
		return dataTypeSearch;
	}

	public void setDataTypeSearch(HtmlInputText dataTypeSearch) {
		this.dataTypeSearch = dataTypeSearch;
	}

	public boolean isTransColDisplay() {
		return transColDisplay;
	}

	public void setTransColDisplay(boolean transColDisplay) {
		this.transColDisplay = transColDisplay;
	}

	public DataDefinitionColumn getEntityLevelToSet() {
		return entityLevelToSet;
	}

	public void setEntityLevelToSet(DataDefinitionColumn entityLevelToSet) {
		this.entityLevelToSet = entityLevelToSet;
	}

	public HtmlInputText getColumnSearch() {
		return columnSearch;
	}

	public void setColumnSearch(HtmlInputText columnSearch) {
		this.columnSearch = columnSearch;
	}

	public DataDefinitionColumn getEntilyLevelToSet() {
		return entityLevelToSet;
	}

	public void setEntilyLevelToSet(DataDefinitionColumn entilyLevelToSet) {
		this.entityLevelToSet = entilyLevelToSet;
	}

	public List<EntityLevel> getSelectedEntityLevelList() {
		return selectedEntityLevelList;
	}

	public void setSelectedEntityLevelList(List<EntityLevel> selectedEntityLevelList) {
		this.selectedEntityLevelList = selectedEntityLevelList;
	}

	public Date getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}

	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}

	public boolean isViewColNameTable() {
		return viewColNameTable;
	}

	public void setViewColNameTable(boolean viewColNameTable) {
		this.viewColNameTable = viewColNameTable;
	}



	public long getNextEntity() {
		return nextEntity;
	}

	public void setNextEntity(long nextEntity) {
		this.nextEntity = nextEntity;
	}

	public HtmlInputText getEntityLevelId() {
		return entityLevelId;
	}

	public void setEntityLevelId(HtmlInputText entityLevelId) {
		this.entityLevelId = entityLevelId;
	}

	public HtmlInputText getTransDetailColumnName() {
		return transDetailColumnName;
	}

	public void setTransDetailColumnName(HtmlInputText transDetailColumnName) {
		this.transDetailColumnName = transDetailColumnName;
	}

	public HtmlInputText getDescription() {
		return description;
	}

	public void setDescription(HtmlInputText description) {
		this.description = description;
	}

	public HtmlInputText getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(HtmlInputText updateUserId) {
		this.updateUserId = updateUserId;
	}

	public HtmlInputText getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(HtmlInputText updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public SecurityModuleBackingBean() {

	}
    public HtmlInputSecret getPwdControl() {
		return pwdControl;
	}

	public void setPwdControl(HtmlInputSecret pw) {
		this.pwdControl = pw;
	}

	public HtmlInputSecret getNewPwdControl() {
		return newPwdControl;
	}

	public void setNewPwdControl(HtmlInputSecret PW) {
		this.newPwdControl = PW;
	}

	public HtmlInputSecret getConfirmPwdControl() {
		return confirmPwdControl;
	}

	public void setConfirmPwdControl(HtmlInputSecret PW) {
		this.confirmPwdControl = PW;
	}
	 
    public String getPwdChangeMessage() {
    	return this.pwdChangeMessage;
    }
    
    public void setPwdChangeMessage(String msg) {
    	this.pwdChangeMessage = msg;
    }
    
    public HtmlInputSecret getPwdEmailControl () {
		return pwdEmailControl ;
	}

	public void setPwdEmailControl (HtmlInputSecret pw) {
		this.pwdEmailControl  = pw;
	}

	public HtmlInputSecret getNewEmailControl() {
		return newEmailControl;
	}

	public void setNewEmailControl(HtmlInputSecret PW) {
		this.newEmailControl = PW;
	}
	 
    public String getEmailChangeMessage() {
    	return this.emailChangeMessage;
    }
    
    public void setEmailChangeMessage(String msg) {
    	this.emailChangeMessage = msg;
    }
    
    public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
    
    public UserService getUserService() {
    	return userService;
    }
    
    public void setUserService(UserService userService) {
    	this.userService = userService;
    }	
    
    public RoleService getRoleService() {
    	return roleService;
    }
    
    public void setRoleService(RoleService roleService) {
    	this.roleService = roleService;
    }    
    
    public MenuService getMenuService() {
    	return menuService;
    }
    
    public void setMenuService(MenuService menuService) {
    	this.menuService = menuService;
    }    
    
    public EntityItemService getEntityItemService() {
    	return entityItemService;
    }
    
    public void setEntityItemService(EntityItemService entityItemService) {
    	this.entityItemService = entityItemService;
    }     
    
    public EntityLevelService getEntityLevelService() {
    	return entityLevelService;
    }
    
    public void setEntityLevelService(EntityLevelService entityLevelService) {
    	this.entityLevelService = entityLevelService;
    }    
    
    public UserEntityService getUserEntityService() {
    	return userEntityService;
    }
    
    public void setUserEntityService(UserEntityService userEntityService) {
    	this.userEntityService = userEntityService;
    }    


	public RoleUserMenuService getRoleUserMenuService() {
		return roleUserMenuService;
	}

	public void setRoleUserMenuService(RoleUserMenuService roleUserMenuService) {
		this.roleUserMenuService = roleUserMenuService;
	}
	

	public boolean getAddAction() {
		return currentAction.equals(EditAction.ADD);
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public String getActionText() {
		return currentAction.getActionText();
	}
	Calendar calendar = Calendar.getInstance();

	private boolean noAdd = false;

	private int selectedFilterIndex;

	
	public boolean isNoAdd() {
		return noAdd;
	}

	public void setNoAdd(boolean noAdd) {
		this.noAdd = noAdd;
	}
	
	public int getSelectedFilterIndex() {
		return selectedFilterIndex;
	}

	public void setSelectedFilterIndex(int selectedFilterIndex) {
		this.selectedFilterIndex = selectedFilterIndex;
	}

	private SelectItem[] VIEWITEMS = null;
		
	public SelectItem[] getViewItems() {	
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    UserDTO user = (UserDTO) session.getAttribute("user");
	        
	    String userCode = user.getUserCode();
		if(userCode.equalsIgnoreCase("STSCORP") || TWO.equals(user.getAdminFlag())){
			VIEWITEMS = new SelectItem[]{
		        new SelectItem(ITEM_ROLES,ITEM_ROLES),
		        new SelectItem(ITEM_USERS,ITEM_USERS),
		        new SelectItem(ITEM_MENU_OPTIONS,ITEM_MENU_OPTIONS)
			};
		}
		else{
			VIEWITEMS = new SelectItem[]{
			    new SelectItem(ITEM_USERS,ITEM_USERS)
			};
		}
		return VIEWITEMS;
	}
		
	public SelectItem[] getAdministrativeItems() {
		SelectItem[] administrativeItems ;
		cacheManager.flushAllUsersMap();
		User user = getCurrentUser();
		if(TWO.equals(user.getAdminFlag())) {
			administrativeItems = new SelectItem[]{new SelectItem("0","User"),new SelectItem("1","System"),new SelectItem("2","Admin")};
		}
		else if(ONE.equals(user.getAdminFlag())) {
			administrativeItems = new SelectItem[]{new SelectItem("0","User"),new SelectItem("1","System") };
		}
		else {
			administrativeItems = new SelectItem[]{new SelectItem("0","User")};
		}
		return administrativeItems;
	}
	
	private Map<String, String> administrativeMap = null;

	public Map<String, String>  getAdministrativeMap(){
		if(administrativeMap==null){
			administrativeMap = new LinkedHashMap<String, String>();
			administrativeMap.put("0","User");
			administrativeMap.put("1","System");
			administrativeMap.put("2","Admin");
	    }
		return administrativeMap;
	}
	
	public List<SelectItem> getMainMenuItems() {
		if (mainMenuItems == null) {
			mainMenuItems = new ArrayList<SelectItem>();
			mainMenuItems.add(new SelectItem("", "Select Main Menu Code"));
			for (String code : menuService.findAllMainMenuCodes()) {
				mainMenuItems.add(new SelectItem(code, code));
			}
		}
		return mainMenuItems;
	}

	public List<SelectItem> getCommandTypeItems() {
	
			commandTypeItems = new ArrayList<SelectItem>();
			commandTypeItems.add(new SelectItem("", "Select Command Type"));
			for (ListCodes code : cacheManager.getListCodesByType("CMDTYPE")) {
				if(code != null && code.getCodeCode()!= null && code.getDescription()!= null){				
					commandTypeItems.add(new SelectItem(
							code.getCodeCode(), 
							code.getDescription()));
				}
			}
		
		return commandTypeItems;
	}
	
	//disableAdd
	public boolean getDisableAdd(){
		logger.info("getDisableAdd() getValidSelection: " + getValidSelection());
		logger.info("getDisableAdd() viewSublist: " + viewSublist);
		logger.info("getDisableAdd() noAdd: " + noAdd);
		
		boolean valid = false;
		String mode = getMode();
		if (mode != null) {
			if (mode.equalsIgnoreCase(ITEM_ROLES)) {
				valid = true;
			} else if (mode.equalsIgnoreCase(ITEM_USERS)){
				valid = true;
			} else if (mode.equalsIgnoreCase(ITEM_MENU_OPTIONS)) {
				valid = true;
			} else if (mode.equalsIgnoreCase(ITEM_ENTITIES)) {
				valid = true;
			}
		}
		
		return (!valid || viewSublist || noAdd);
	}

	public boolean getValidSelection() {
		logger.info("Inside valid selection");
		boolean valid = false;
		String mode = getMode();
		if (mode != null) {
			if (mode.equalsIgnoreCase(ITEM_ROLES)) {
				Role role = this.getSelectedRole();
				String code = (role != null)? role.getRoleCode():null;
				valid = ((code != null) && !code.equals(""));
			} else if (mode.equalsIgnoreCase(ITEM_USERS)){
				User user = this.getSelectedUser();
				String code = (user != null)? user.getUserCode():null;
				valid = ((code != null) && !code.equals(""));
			} else if (mode.equalsIgnoreCase(ITEM_MENU_OPTIONS)) {
				Menu menu = this.getSelectedMenu();
				String code = (menu != null)? menu.getMenuCode():null;
				valid = ((code != null) && !code.equals(""));
			} else if (mode.equalsIgnoreCase(ITEM_ENTITIES)) {
				/*EntityLevel entityLevel = this.getSelectedEntityLevel();
				Long code = (entityLevel != null)? entityLevel.getEntityLevelId():null;*/
				EntityItem entityItem = this.getSelectedEntity();
				Long code = (entityItem != null)? entityItem.getEntityId():null;
				//valid = ((code != null) && (code > 0L));
				valid = (code != null); 
			}
		}
		
		return valid;
	}

	/**
     * Value change listener for the view change event. Sets up the 
     * firstComboItems listbox according to the view.
     *
     * @param event value change event
     */
public void viewChanged(ValueChangeEvent event) {
    	
    	String newView = (String) event.getNewValue();
    	viewChangedAction(newView);
    }
    
	public void refreshAction() {
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	    UserDTO user = (UserDTO) session.getAttribute("user");
		if(USER_STSCORP.equals(user.getUserCode()) || TWO.equals(user.getAdminFlag())){
			if(getSelectedView() == null){
				this.selectedView = ITEM_ROLES;//Default View
			}
			else {
				this.selectedView = getSelectedView();
			}
		}
		else {
			this.selectedView = ITEM_USERS;
		}
		viewChangedAction(this.selectedView);
   }
    
    private void viewChangedAction(String newView) {
    	logger.info("start viewChanged()");
        this.setSelectedView(newView);
        logger.info("event selected ==" + newView +"" +this.getSelectedView());
        viewSublist = false;
        selectedRowIndex = -1;
        
        if (newView.equals(ITEM_ROLES)) {
          List<Role> rolesList = roleService.findAllRoles();
          this.setRolesList(rolesList);
          userRoleComboItems = new ArrayList<SelectItem>();
          userRoleComboItems.add(new SelectItem("0","Select a Role"));
          for(Role role : rolesList)
        	  userRoleComboItems.add(new SelectItem(role.getRoleCode(),role.getRoleCode()+"-"+role.getRoleName()));
          this.setUsersList(null); 
          this.setMenusList(null);
          this.setEntityItemsList(null);          
          viewRoles = true;
          viewUsers = false;
          viewMenus = false;
          viewEntityItems = false;  
          this.setRerender("rolesTable");
          this.setMode(ITEM_ROLES);
          logger.info("in Roles, size = " + rolesList.size());
        } else if (newView.equals(ITEM_USERS)) {
        	viewUserDetails();
          logger.info("in Users, size = " + usersList.size());        
        } else if (newView.equals(ITEM_MENU_OPTIONS)) {
          List<Menu> list = menuService.findAllMenus();
          this.setMenusList(list);
          this.setRolesList(null); 
          this.setUsersList(null);  
          this.setEntityItemsList(null);
          viewMenus = true;
          viewUsers = false;  
          viewRoles = false;
          viewEntityItems = false;
          this.setRerender("menusTable"); 
          this.setMode(ITEM_MENU_OPTIONS);           
          logger.info("in Menus, size = " + menusList.size());  
        } else if (newView.equals(ITEM_ENTITIES)) {
			// List<EntityItem> list = entityItemService.findAllEntityItems();
			List<EntityItem> list = entityItemService
					.findAllEntityItemsByParent(0l);
			this.setEntityItemsList(list);
			
			List<SelectItem> entityItems = new ArrayList<SelectItem>();
			entityItems.add(new SelectItem("-1","Select a Level 1 Entity"));
			if(list != null && list.size() > 0){
				EntityItem currentEntity = list.get(0);
				this.getparentEntityData(currentEntity);
				for (EntityItem entity : list) {
					if (!"*ALL".equalsIgnoreCase(entity.getEntityCode()))
						entityItems.add(new SelectItem(entity.getEntityId(), entity.getEntityCode()+ "-" + entity.getEntityName()));
				}
			}else{
				logger.debug("Inside fgdfgdr56546456");
				EntityItem currentEntity = new EntityItem();
				currentEntity.setEntityLevelId(1l);
				this.getparentEntityData(currentEntity);
				this.parentEntityAndLevelInfo.setParentCode("*ALL");
			}
			UISelectItems items = new UISelectItems();
			items.setValue(entityItems);
			levelOneCombo.getChildren().clear();
			levelOneCombo.getChildren().add(items);

			
			levelTwoCombo.getChildren().clear();
			levelThreeCombo.getChildren().clear();
			levelFourCombo.getChildren().clear(); 
			this.setMenusList(null);
			this.setRolesList(null);
			this.setUsersList(null);
			viewEntityItems = true;
			viewMenus = false;
			viewUsers = false;
			viewRoles = false;
			this.setRerender("entityItemsTable,entityCombo");
			this.setMode(ITEM_ENTITIES);
			logger.info("in Menus, size = " + entityItemsList.size());
		}else {
          this.setEntityItemsList(null);
          this.setMenusList(null);
          this.setRolesList(null); 
          this.setUsersList(null);  
          this.setUserEntityMapList(null);
          viewEntityItems = false;
          viewMenus = false;
          viewUsers = false;  
          viewRoles = false;
          viewSublist = false;
          this.setSelectedEntity(null);
          this.setSelectedMenu(null);
          this.setSelectedRole(null);
          this.setSelectedUser(null);
          this.setSelectedEntityOri(null);
          this.setSelectedMenuOri(null);
          this.setSelectedRoleOri(null);
          this.setSelectedUserOri(null);
          this.setMode("");
        }
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    public void viewUserDetails() {
    	List<User> list = userService.findAllUsers();
        if(list.size() > 0) {
      	  for(User user : list) {
          	  List<UserEntity> userEntityList = userEntityService.findByUserCode(user.getUserCode());
  	        	if (userEntityList == null || userEntityList.size() == 0 ) {
  	        		user.setUserMappedBooleanFag(false);
  	        	}
  	        	else {
  	        		user.setUserMappedBooleanFag(true);
  	        	}
            }
        }
       
        this.setUsersList(list);
        userRoleComboItems = new ArrayList<SelectItem>();
        userRoleComboItems.add(new SelectItem("0","Select a User"));
					for(User user : list) {
						  if (user.getLastUsedEntityId() != null) {
						    EntityItem ei = entityItemService.findById(user.getLastUsedEntityId());
						    if (ei != null) {
						    	user.setLastUsedEntityCode(ei.getEntityCode());
						    	user.setLastUsedEntityName(ei.getEntityName());
						    }
						  }
						  userRoleComboItems.add(new SelectItem(user.getUserCode(),user.getUserCode()+"-"+user.getUserName()));
         }
        this.setRolesList(null); 
        this.setMenusList(null);
        this.setEntityItemsList(null);          
        viewUsers = true;  
        viewRoles = false;
        viewMenus = false;
        viewEntityItems = false; 
        this.setRerender("usersTable");
        this.setMode(ITEM_USERS);  
    }
    
    public void getparentEntityData(EntityItem currentEntity){
	    if(currentEntity != null){
	        parentEntityAndLevelInfo = new EntityItemDTO();	        
	        EntityLevel levelInfo = entityLevelService.findById(currentEntity.getEntityLevelId());
	        
	        if(levelInfo != null){
	        	this.noAdd  = false;
	        	
		        parentEntityAndLevelInfo.setEntityLevelId(currentEntity.getEntityLevelId());
		        logger.debug("levelInfo.y () +++++" + levelInfo.getTransDetailColumnName());
		        parentEntityAndLevelInfo.setEntityColumn(levelInfo.getTransDetailColumnName());
		        parentEntityAndLevelInfo.setEntityLevelDescription(levelInfo.getDescription());
		        
		        
		        EntityItem parent = entityItemService.findById(currentEntity.getParentEntityId());
		        parentEntityAndLevelInfo.setEntityId(currentEntity.getParentEntityId());
		        if(parent != null && !"*ALL".equalsIgnoreCase(parent.getEntityCode())){
			        logger.debug("parent.getEntityCode() +++++" + parent.getEntityCode());
			        parentEntityAndLevelInfo.setParentCode(parent.getEntityCode());
			        parentEntityAndLevelInfo.setParentName(parent.getEntityName());
			        parentEntityAndLevelInfo.setParentEntityLevel(parent.getEntityLevelId());
			        
			        EntityLevel parentlevelInfo = entityLevelService.findById(parent.getEntityLevelId());
			        logger.debug("parentlevelInfo.getDescription() +++++" + parentlevelInfo.getDescription());
			        parentEntityAndLevelInfo.setParentLevelDescription(parentlevelInfo.getDescription());
		        }else{
		        	parentEntityAndLevelInfo.setParentCode("-");
			        parentEntityAndLevelInfo.setParentName("-");
			        //parentEntityAndLevelInfo.setParentEntityLevel(parent.getEntityLevelId());
			        parentEntityAndLevelInfo.setParentLevelDescription("-");
		        }
	        }else{
	        	this.noAdd  = true;
	        }
	        
	    }
    }  
    
    public void entityItemChanged(ActionEvent e) throws AbortProcessingException {
    	logger.info("enter selectedUserRowChanged");
    	HtmlSelectOneMenu sel = (HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()) 
	    .getParent(); 
        String compId = sel.getId();
        logger.info("event selected ==" + compId );
        String id = "-1";
        if("id1".equalsIgnoreCase(compId))
        	id = selectedLevelOneCombo;
        else if("id2".equalsIgnoreCase(compId))
        	id = selectedLevelTwoCombo;
        else if("id3".equalsIgnoreCase(compId))
        	id = selectedLevelThreeCombo;
        else if("id4".equalsIgnoreCase(compId))
        	id = selectedLevelFourCombo;
        logger.info("event selected d==" + id );
        getChangedEntityItem(id,compId); 
    }
    
    public void getChangedEntityItem(String id , String compId) {
    	 //the id is not null
        if(id != null){
        	this.setSelectedEntity(null);
        	this.setSelectedEntityOri(null);
        	
        	if("-1".equalsIgnoreCase(id))
        		id = "0";
        	else if("-2".equalsIgnoreCase(id))
        		id = selectedLevelOneCombo;
        	else if("-3".equalsIgnoreCase(id))
        		id = selectedLevelTwoCombo;
        	else if("-4".equalsIgnoreCase(id))
        		id = selectedLevelThreeCombo;
        		
        	List<EntityItem> list= entityItemService.findAllEntityItemsByParent(Long.valueOf(id));
        	
	        this.setEntityItemsList(list);
	        
	        if(list != null && !list.isEmpty()){
	        	logger.info("list selected ==" + list.size());
	        	this.setEntityItemsList(list);
	        	EntityItem currentEntity  = list.get(0);
		        int level = currentEntity.getEntityLevelId().intValue();
		        
		        List<SelectItem>entityItems = new ArrayList<SelectItem>();
		        entityItems.add(new SelectItem("-"+level,"Select a Level "+level+" Entity"));
		        for(EntityItem entity : list)
		        	entityItems.add(new SelectItem(entity.getEntityId(),entity.getEntityCode()+"-"+entity.getEntityName()));
		        UISelectItems items = new UISelectItems();
		        items.setId(compId + "_items");
				items.setValue(entityItems);
				
				this.getparentEntityData(currentEntity);
			        
		        logger.info("level selected ==" + level);
		        switch(level){ 
		        	case 1:
		        			levelTwoCombo.getChildren().clear();
		        			levelThreeCombo.getChildren().clear();
		        			levelFourCombo.getChildren().clear();
		        		break;
			        case 2:
			        		levelTwoCombo.getChildren().clear();
			        		levelTwoCombo.getChildren().add(items);
			        		levelThreeCombo.getChildren().clear();
			        		levelFourCombo.getChildren().clear();
			        	break;
			        case 3:
			        		levelThreeCombo.getChildren().clear();
			        		levelThreeCombo.getChildren().add(items);
			        		levelFourCombo.getChildren().clear();
			        	break;
			        case 4:
			        		levelFourCombo.getChildren().clear();
			        		levelFourCombo.getChildren().add(items);
			        	break;
		        	
		        }
	        }else{
	        	logger.info("event selected in else ==" + id);
	        	EntityItem currentEntity  = new EntityItem();
	        	Long parentLevel = entityItemService.findById(Long.valueOf(id)).getEntityLevelId();
	        	logger.debug("new parent level " + parentLevel);
	        	currentEntity.setEntityLevelId(parentLevel+1l);
	        	currentEntity.setParentEntityId(Long.valueOf(id));
	        	this.getparentEntityData(currentEntity);
	        	if(parentLevel == 1){
	        		levelTwoCombo.getChildren().clear();
		            levelThreeCombo.getChildren().clear();
		            levelFourCombo.getChildren().clear();
	        	}else if(parentLevel == 2){
	        		levelThreeCombo.getChildren().clear();
		            levelFourCombo.getChildren().clear();
	        	}else if(parentLevel == 3){
	        		levelFourCombo.getChildren().clear();
	        	}
	        }
        }
        this.setMenusList(null);
        this.setRolesList(null); 
        this.setUsersList(null);  
        viewEntityItems = true;
        viewMenus = false;
        viewUsers = false;  
        viewRoles = false;
        this.setMode(ITEM_ENTITIES);  
    }
    
   
	/* to get the selected view */
	public String getSelectedView() {
		return selectedView;
	}

	public void setSelectedView(String selectedView) {
		this.selectedView = selectedView;
	}
	
	public String getSelectedUserRole() {
		return selectedUserRole;
	}

	public void setSelectedUserRole(String selectedUserRole) {
		this.selectedUserRole = selectedUserRole;
	}

	public String getSelectedLevelOneCombo() {
		return selectedLevelOneCombo;
	}

	public void setSelectedLevelOneCombo(String selectedLevelOneCombo) {
		this.selectedLevelOneCombo = selectedLevelOneCombo;
	}

	public String getSelectedLevelTwoCombo() {
		return selectedLevelTwoCombo;
	}

	public void setSelectedLevelTwoCombo(String selectedLevelTwoCombo) {
		this.selectedLevelTwoCombo = selectedLevelTwoCombo;
	}

	public String getSelectedLevelThreeCombo() {
		return selectedLevelThreeCombo;
	}

	public void setSelectedLevelThreeCombo(String selectedLevelThreeCombo) {
		this.selectedLevelThreeCombo = selectedLevelThreeCombo;
	}

	public String getSelectedLevelFourCombo() {
		return selectedLevelFourCombo;
	}

	public void setSelectedLevelFourCombo(String selectedLevelFourCombo) {
		this.selectedLevelFourCombo = selectedLevelFourCombo;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}
	
	public List<Role> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}	
	
	public List<Menu> getMenusList() {
		return menusList;
	}

	public void setMenusList(List<Menu> menusList) {
		this.menusList = menusList;
	}	
	
	public List<Menu> getGrantedList() {
		return grantedList;
	}

	public List<Menu> getDeniedList() {
		return deniedList;
	}

	public void setGrantedList(List<Menu> grantedList) {
		this.grantedList = grantedList;
	}

	public void setDeniedList(List<Menu> deniedList) {
		this.deniedList = deniedList;
	}
	
	public ArrayList<MenuDTO> getDefaultMenu() {
		return defaultMenu;
	}

	public ArrayList<MenuDTO> getDeniedMenu() {
		return deniedMenu;
	}

	public void setDefaultMenu(ArrayList<MenuDTO> defaultMenu) {
		this.defaultMenu = defaultMenu;
	}

	public void setDeniedMenu(ArrayList<MenuDTO> deniedMenu) {
		this.deniedMenu = deniedMenu;
	}

	public List<EntityItem> getEntityItemsList() {
		return entityItemsList;
	}

	public void setEntityItemsList(List<EntityItem> entityItemsList) {
		this.entityItemsList = entityItemsList;
	}	
	
	public List<EntityLevel> getEntityLevelsList() {
		entityLevelsList = entityLevelService.findAllEntityLevels();
        this.setEntityLevelsList(entityLevelsList);	
        this.setViewEntityLevels(true);
		return entityLevelsList;
	}
	
	public void setEntityLevelsList(List<EntityLevel> entityLevelsList) {
		this.entityLevelsList = entityLevelsList;
	}	
	
	public boolean getViewUsers() {
		return viewUsers;
	}

	public void setViewUsers(boolean viewUsers) {
		this.viewUsers = viewUsers;
	}
	
	public boolean getViewRoles() {
		return viewRoles;
	}

	public void setViewRoles(boolean viewRoles) {
		this.viewRoles = viewRoles;
	}	
	
	public boolean getViewMenus() {
		return viewMenus;
	}

	public void setViewMenus(boolean viewMenus) {
		this.viewMenus = viewMenus;
	}
	
	public boolean getViewEntityItems() {
		return viewEntityItems;
	}

	public void setViewEntityItems(boolean viewEntityItems) {
		this.viewEntityItems = viewEntityItems;
	}	
	
	public boolean getViewEntityLevels() {
		return viewEntityLevels;
	}

	public void setViewEntityLevels(boolean viewEntityLevels) {
		this.viewEntityLevels = viewEntityLevels;
	}	
	
	public boolean getViewUserEntitys() {
		return viewUserEntitys;
	}

	public void setViewUserEntitys (boolean viewUserEntitys) {
		this.viewUserEntitys = viewUserEntitys;
	}	
	
	public String getRerender() {
		return rerender;
	}
	
	public void setRerender(String rerender) {
		this.rerender = rerender;
	}
	
    public Role getSelectedRoleOri() {
		return this.selectedRoleOri;
	}
	
	public void setSelectedRoleOri(Role role) {
		this.selectedRoleOri = role;
	}
	
	public User getSelectedUserOri() {
		return this.selectedUserOri;
	}
	
	public void setSelectedUserOri(User user) {
		this.selectedUserOri = user;
	}	
	
	public Menu getSelectedMenuOri() {
		return this.selectedMenuOri;
	}
	
	public void setSelectedMenuOri(Menu menu) {
		this.selectedMenuOri = menu;
	}
	
	public EntityItem getSelectedEntityOri() {
		return selectedEntityOri;
	}

	public void setSelectedEntityOri(EntityItem selectedEntity) {
		this.selectedEntityOri = selectedEntity;
	}
	
	public Role getSelectedRole() {
		return this.selectedRole;
	}
	
	public void setSelectedRole(Role role) {
		this.selectedRole = role;
	}
	
	public User getSelectedUser() {
		return this.selectedUser;
	}
	
	public void setSelectedUser(User user) {
		this.selectedUser = user;
	}	
	
	public EntityItem getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(EntityItem selectedEntity) {
		this.selectedEntity = selectedEntity;
	}

	public String getMode() {
		return this.mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public Menu getSelectedMenu() {
		return this.selectedMenu;
	}
	
	public void setSelectedMenu(Menu menu) {
		this.selectedMenu = menu;
	}	
	
	public EntityLevel getSelectedEntityLevel() {
		return this.selectedEntityLevel;
	}
	
	public void setSelectedEntityLevel(EntityLevel entityLevel) {
		this.selectedEntityLevel = entityLevel;
	}	
	
	public UserEntity getSelectedUserEntity() {
		return this.selectedUserEntity;
	}
	
	public void setSelectedUserEntity(UserEntity userEntity) {
		this.selectedUserEntity = userEntity;
	}		
	
	public DriverReferenceService getDriverReferenceService() {
		return driverReferenceService;
	}

	public UserMenuExService getUserMenuExService() {
		return userMenuExService;
	}

	public void setUserMenuExService(UserMenuExService userMenuExService) {
		this.userMenuExService = userMenuExService;
	}

	public EntityItemDTO getEntityItemDTO() {
		return entityItemDTO;
	}

	public List<DriverReference> getTransctionDetailColumn() {
		return transctionDetailColumn;
	}

	public void setDriverReferenceService(
			DriverReferenceService driverReferenceService) {
		this.driverReferenceService = driverReferenceService;
	}

	public void setEntityItemDTO(EntityItemDTO entityItemDTO) {
		this.entityItemDTO = entityItemDTO;
	}

	public void setTransctionDetailColumn(
			List<DriverReference> transctionDetailColumn) {
		this.transctionDetailColumn = transctionDetailColumn;
	}

	public ArrayList<UserEntityRoleDTO> getUserEntityRoleList() {
		return userEntityRoleList;
	}

	public void setUserEntityRoleList(
			ArrayList<UserEntityRoleDTO> userEntityRoleList) {
		this.userEntityRoleList = userEntityRoleList;
	}

	public Collection<Role> getSelectedRoleList() {
		return selectedRoleList;
	}

	public Collection<EntityItem> getSelectedEntityList() {
		return selectedEntityList;
	}

	public void setSelectedRoleList(Collection<Role> selectedRoleList) {
		this.selectedRoleList = selectedRoleList;
	}

	public void setSelectedEntityList(Collection<EntityItem> selectedEntityList) {
		this.selectedEntityList = selectedEntityList;
	}

	public Collection<UserEntityRoleDTO> getSelectedMappingList() {
		return selectedMappingList;
	}

	public void setSelectedMappingList(
			Collection<UserEntityRoleDTO> selectedMappingList) {
		this.selectedMappingList = selectedMappingList;
	}

	public ArrayList<UserEntityRoleDTO> getSaveMapping() {
		return saveMapping;
	}

	public ArrayList<UserEntityRoleDTO> getDeleteMapping() {
		return deleteMapping;
	}
	
	public ArrayList<UserEntityRoleDTO> getChangeMapping() {
		return changeMapping;
	}

	public void setSaveMapping(ArrayList<UserEntityRoleDTO> saveMapping) {
		this.saveMapping = saveMapping;
	}

	public void setDeleteMapping(ArrayList<UserEntityRoleDTO> deleteMapping) {
		this.deleteMapping = deleteMapping;
	}
	
	public void setChangeMapping(ArrayList<UserEntityRoleDTO> changeMapping) {
		this.changeMapping = changeMapping;
	}

	public UserEntityRoleDTO getSelectedMapping() {
		return selectedMapping;
	}

	public void setSelectedMapping(UserEntityRoleDTO selectedMapping) {
		this.selectedMapping = selectedMapping;
	}
	
	public void selectedRoleorUserChanged(ValueChangeEvent event){
		String code = (String) event.getNewValue();
		if("0".equalsIgnoreCase(code)){
			this.setViewSublist(false);	
		}else{
			this.setViewSublist(true);	
			if(ITEM_ROLES.equalsIgnoreCase(this.getSelectedView())){
					userEntityMapList = new ArrayList<UserEntityDTO>();
					List<Object[]> objList = userEntityService.findByRoleCode(code);
					for(Iterator<?> pairs  = objList.iterator(); pairs.hasNext();){
						Object[] pair = (Object[]) pairs.next();
						UserEntityDTO userEntity = new UserEntityDTO();
						UserEntityPK userEntityPK = new UserEntityPK();
						userEntityPK.setUserCode(pair[0].toString());
						userEntity.setUserEntityPK(userEntityPK);
						userEntity.setUserName(pair[1].toString());
						userEntity.setEntity((EntityItem)pair[2]);
						userEntityMapList.add(userEntity);
					}
			}else if(ITEM_USERS.equalsIgnoreCase(this.getSelectedView())){
					userEntityMapList = new ArrayList<UserEntityDTO>();
					List<Object[]> objList = userEntityService.findForUserCode(code);
					for(Iterator<?> pairs  = objList.iterator(); pairs.hasNext();){
						Object[] pair = (Object[]) pairs.next();
						UserEntityDTO userEntity = new UserEntityDTO();
						UserEntityPK userEntityPK = new UserEntityPK();
						userEntityPK.setUserCode(pair[0].toString());
						userEntity.setUserEntityPK(userEntityPK);
						userEntity.setUserName(pair[1].toString());
						userEntity.setEntity((EntityItem)pair[2]);
						userEntity.setEntityLevel((EntityLevel)pair[3]);
						userEntity.setRoleCode(pair[4].toString());
						userEntity.setRoleName(pair[5].toString());
						userEntityMapList.add(userEntity);
					}
			}
		}
	}

	public List<UserEntityDTO> getUserEntityMapList() {
		return userEntityMapList;
	}

	public void setUserEntityMapList(List<UserEntityDTO> userEntityMapList) {
		this.userEntityMapList = userEntityMapList;
	}

	public boolean getViewSublist() {
		return viewSublist;
	}

	public void setViewSublist(boolean viewSublist) {
		this.viewSublist = viewSublist;
	}

	public List<SelectItem> getUserRoleComboItems() {
		return userRoleComboItems;
	}

	public void setUserRoleComboItems(List<SelectItem> userRoleComboItems) {
		this.userRoleComboItems = userRoleComboItems;
	}

	public HtmlSelectOneMenu getLevelOneCombo() {
		return levelOneCombo;
	}

	public void setLevelOneCombo(HtmlSelectOneMenu levelOneCombo) {
		this.levelOneCombo = levelOneCombo;
	}

	public HtmlSelectOneMenu getLevelTwoCombo() {
		return levelTwoCombo;
	}

	public void setLevelTwoCombo(HtmlSelectOneMenu levelTwoCombo) {
		this.levelTwoCombo = levelTwoCombo;
	}

	public HtmlSelectOneMenu getLevelThreeCombo() {
		return levelThreeCombo;
	}

	public void setLevelThreeCombo(HtmlSelectOneMenu levelThreeCombo) {
		this.levelThreeCombo = levelThreeCombo;
	}

	public HtmlSelectOneMenu getLevelFourCombo() {
		return levelFourCombo;
	}

	public void setLevelFourCombo(HtmlSelectOneMenu levelFourCombo) {
		this.levelFourCombo = levelFourCombo;
	}

	public EntityItemDTO getParentEntityAndLevelInfo() {
		return parentEntityAndLevelInfo;
	}

	public void setParentEntityAndLevelInfo(EntityItemDTO parentEntityAndLevelInfo) {
		this.parentEntityAndLevelInfo = parentEntityAndLevelInfo;
	}

	public void selectedUserRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedUserRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) 
		    .getParent(); 
		//Selection sel = table.getSelection(); 
		List<User> selected = new ArrayList<User>(); 
		selected.add((User)table.getRowData()); 
		selectedRowIndex = table.getRowIndex();
		if (selected.size() > 0) {	
			this.setSelectedUser(selected.get(0));
			this.setSelectedUserOri(selected.get(0));
			logger.info(" user selected = " + this.selectedUser.getUserName());
			logger.info("exit selectedUserRowChanged");				
		} else {
			logger.info(" selected.size() < 0 ");			
		}	
	} 	
	
	public void selectedMenuRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedMenuRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) 
		    .getParent(); 
		//Selection sel = table.getSelection(); 
		List<Menu> selected = new ArrayList<Menu>(); 
		selected.add((Menu)table.getRowData()); 
		selectedRowIndex = table.getRowIndex();
		if (selected.size() > 0) {	
			this.setSelectedMenu(selected.get(0));
			this.setSelectedMenuOri(selected.get(0));
			logger.info(" menu code selected = " + this.selectedMenu.getMenuCode());
			logger.info(" main menu code selected = " + this.selectedMenu.getMainMenuCode());	
			logger.info(" menu sequence selected = " + this.selectedMenu.getMenuSequence());
			logger.info(" option name selected = " + this.selectedMenu.getOptionName());	
			logger.info(" command type selected = " + this.selectedMenu.getCommandType());	
			logger.info(" command line selected = " + this.selectedMenu.getCommandLine());	
			logger.info(" keyvalues selected = " + this.selectedMenu.getKeyValues());
			logger.info(" user id selected = " + this.selectedMenu.getUpdateUserId());
			logger.info("exit selectedMenuRowChanged");				
		} else {
			logger.info(" selected.size() < 0 ");			
		}	
	}	
	
	public void selectedRoleRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedRoleRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) 
		    .getParent(); 
		//Selection sel = table.getSelection(); 
		List<Role> selected = new ArrayList<Role>(); 
		selected.add((Role)table.getRowData()); 
		selectedRowIndex = table.getRowIndex();
		
		if (selected.size() > 0) {	
			this.setSelectedRole(selected.get(0));
			this.setSelectedRoleOri(selected.get(0));
			logger.info(" role selected = " + this.selectedRole.getRoleName());
			logger.info("exit selectedRoleRowChanged");				
		} else {
			logger.info(" selected.size() < 0 ");			
		}	
	} 
	
	public void selectedEntityRowChanged (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityRowChanged");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()) 
		    .getParent(); 
		//Selection sel = table.getSelection(); 
		List<EntityItem> selected = new ArrayList<EntityItem>(); 
		selected.add((EntityItem)table.getRowData()); 
		selectedRowIndex = table.getRowIndex();
		if (selected.size() > 0) {	
			this.setSelectedEntity(selected.get(0));
			this.setSelectedEntityOri(selected.get(0));
			this.setEntityItemDTO(getSelectedEntity().getEntityItemDTO());
			logger.info(" entity selected = " + this.selectedEntity.getEntityId());
			logger.info("exit selectedEntityRowChanged");				
		} else {
			logger.info(" selected.size() < 0 ");			
		}	
	}	
	
	
	public void selectedEntityLevelRowChanged (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityLevelRowChanged");
		getMaxEntity();
		HtmlDataTable  table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		//Selection sel = table.getSelection(); 
		List<EntityLevel> selected = new ArrayList<EntityLevel>(); 	
		selected.add((EntityLevel)table.getRowData()); 
		selectedEntityLevelRowIndex = table.getRowIndex();
		//for (Iterator<Object> iter = sel.getKeys(); iter.hasNext();) { 
		//	Object next = iter.next(); 
		//	table.setRowKey(next); 
		//	selected.add((EntityLevel)table.getRowData()); 
		//} 
		setShowUpdateButton(false);
		setDisableDeleteButton(false);
		if (selected.size() > 0) {	
			this.setSelectedEntityLevel(selected.get(0));
			setShowUpdateButton(true);
			Long tempMax = nextEntity-1;
			if(selectedEntityLevel.getEntityLevelId().equals(tempMax)){
				setDisableDeleteButton(true);
			}
			//else {setDisableDeleteButton(false);
			//}
			}
		else {
			//setShowUpdateButton(false);
			logger.info(" selected.size() < 0 ");			
		}	
	}	
	
	public void selectedEntityLevelRowChangedToAdd (ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedEntityLevelRowChangedToAdd");
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();  
		entityLevelToSet = (DataDefinitionColumn)table.getRowData();
		if(entityLevelToSet!=null){
			showOkButton = true;
		}

		logger.info(" entity level selected For Add= " + this.entityLevelToSet.getDataDefinitionColumnPK().getColumnName());
		logger.info("exit selectedEntityLevelRowChangedToAdd");		
		
	}	
	
	public String setSelectedData(){
		if(entityLevelToSet !=null){
			transDetailColumnName.setValue(entityLevelToSet.getDataDefinitionColumnPK().getColumnName().toString());
			description.setValue(entityLevelToSet.getDescription().toString());
		}
		return "security_entitylevel_add";
		
	}
	

	
	public void selectionFilterChanged(ActionEvent e) {
		HtmlDataTable table = (HtmlDataTable) (((HtmlAjaxSupport) e.getComponent()).getParent());
		DriverReference drvRef = (DriverReference) table.getRowData();
		selectedFilterIndex = table.getRowIndex();
		
		this.entityItemDTO.setEntityCode(drvRef.getDriverValue());
		this.entityItemDTO.setEntityName(drvRef.getDriverDesc());
	}
	
	public String searchDriverReference(){
		transctionDetailColumn.clear();
		transctionDetailColumn = driverReferenceService.findByDriverName(entityItemDTO.getEntityCode(),entityItemDTO.getEntityColumn(),0);			
		return "security_entity_add";
	}
	
	public boolean displayUpdateEntityLevelAction() {	
		logger.info("start displayUpdateEntityLevelAction " + entityItemDTO.getEntityId() + parentEntityAndLevelInfo.getParentCode());
		Long count = entityItemService.count(entityItemDTO.getEntityId());
		//Set the values that doesn't come from selected row
		entityItemDTO.setParentLevelDescription("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentLevelDescription());
		entityItemDTO.setParentCode("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentCode());
		entityItemDTO.setParentName("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentName());
		entityItemDTO.setEntityLevel(parentEntityAndLevelInfo.getEntityLevelDescription());
		entityItemDTO.setEntityColumn(parentEntityAndLevelInfo.getEntityColumn());	
		if(count <= 0L || entityItemDTO.getEntityId() == 0){
			return true;
		}else{
			return false;
		}
	}
	
	
	public String displayViewAction() {
		String returnScreen = displayUpdateAction();
		currentAction = EditAction.VIEW;
		return returnScreen;
	}
	
	public String displayUpdateAction() {
		String result = null;
		logger.info("start displayUpdateAction"); 
		currentAction = EditAction.UPDATE;
		
		if (getValidSelection()) {
			if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {
				this.setSelectedRole(getSelectedRoleOri());
				result = "security_role_update";
			} else if(this.getMode().equalsIgnoreCase(ITEM_USERS)) {
				this.setSelectedUser(getSelectedUserOri());
				result = "security_user_update";
			} else if(this.getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)) {
				logger.debug(" menu code selected = " + this.selectedMenu.getMenuCode());
				logger.debug(" main menu code selected = " + this.selectedMenu.getMainMenuCode());	
				logger.debug(" menu sequence selected = " + this.selectedMenu.getMenuSequence());
				logger.debug(" option name selected = " + this.selectedMenu.getOptionName());	
				logger.debug(" command type selected = " + this.selectedMenu.getCommandType());	
				logger.debug(" command line selected = " + this.selectedMenu.getCommandLine());	
				logger.debug(" keyvalues selected = " + this.selectedMenu.getKeyValues());
				logger.debug(" user id selected = " + this.selectedMenu.getUpdateUserId());	
				this.setSelectedMenu(getSelectedMenuOri());
				result = "security_menu_update";
			} else if(this.getMode().equalsIgnoreCase(ITEM_ENTITIES)) {
				this.setSelectedEntity(getSelectedEntityOri());
				this.setEntityItemDTO(getSelectedEntity().getEntityItemDTO());
				
				displayUpdateEntityLevelAction();
				result = "security_entity_update";				
			}
		}
		
	    logger.info("end displayUpdateAction");
	    return result;
	}	
	
	public String updateAction() {
		String result = null;
		logger.info("start updateAction"); 
		
		try {
			if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {
			    Role role = this.getSelectedRole();	
			    logger.info("roleName control value = " + (String)role.getRoleName());
	            roleService.saveOrUpdate(role);        
	            List<Role> rolesList = roleService.findAllRoles();
	            this.setRolesList(rolesList);
	            this.setSelectedRole(null);
	            this.setSelectedRoleOri(null);
	            selectedRowIndex = -1;
	            result = "security_main";
			} else if(this.getMode().equalsIgnoreCase(ITEM_USERS)) {
			    User user = this.getSelectedUser();	
			    logger.info("userName control value = " + (String)user.getUserName());
	            userService.saveOrUpdate(user);  
	            cacheManager.flushAllUsersMap();
	            List<User> list = userService.findAllUsers();
	            this.setUsersList(list);
	            this.setSelectedUser(null);
	            this.setSelectedUserOri(null);
	            selectedRowIndex = -1;
	            List<UserEntity> userEntityList = userEntityService.findByUserCode(user.getUserCode());
	        	if (userEntityList == null || userEntityList.size() == 0 ) {
	        		this.setMode(ITEM_USERS); 
	        		this.setSelectedUser(user);
	        		result = displayMapAction();
	        	}	
	        	else {
	        		 result = "security_main";
	        	}
			} else if(this.getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)) {	
			    Menu menu = this.getSelectedMenu();	
			    logger.info("mainMenuCode control value = " + (String)menu.getMainMenuCode());
	            menuService.saveOrUpdate(menu);        
	            List<Menu> list = menuService.findAllMenus();
	            this.setMenusList(list);
	            this.setSelectedMenu(null);
	            this.setSelectedMenuOri(null);
	            selectedRowIndex = -1;
			    result = "security_main";
			} else if(this.getMode().equalsIgnoreCase(ITEM_ENTITIES)) {	
				EntityItemDTO entityItemDTO = this.getEntityItemDTO();
				if(entityItemDTO.getEntityName() != null && entityItemDTO.getEntityName().length() > 0){
					EntityItem entityItem = new EntityItem();
					entityItem.setEntityId(entityItemDTO.getEntityId());
					entityItem.setEntityCode(entityItemDTO.getEntityCode());
					entityItem.setEntityName(entityItemDTO.getEntityName());
					entityItem.setEntityLevelId(entityItemDTO.getEntityLevelId());
					entityItem.setParentEntityId(entityItemDTO.getParentEntityId());
					entityItemService.saveOrUpdate(entityItem);
					List<EntityItem> entityItemsList = entityItemService.findAllEntityItemsByParent(entityItemDTO.getParentEntityId());
					this.setEntityItemsList(entityItemsList);
					this.setSelectedEntity(null);
					this.setSelectedEntityOri(null);
					selectedRowIndex = -1;
				    result = "security_main";
				}else{
					String message = "Entity name cannot be empty.";
					FacesMessage msg  = new FacesMessage(message);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, msg);
					return "";
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "failure";
		}
	    logger.info("end updateAction");	    
        return result;
	}		
	
	public String displayAddAction() {
		String result = null;
		logger.info("start displayAddAction"); 
		currentAction = EditAction.ADD;
		
		if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {
			this.setSelectedRole(new Role());
			result = "security_role_add";
		} else if (this.getMode().equalsIgnoreCase(ITEM_USERS)) {
			this.setSelectedUser(new User());
			result = "security_user_add";
		} else if (this.getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)) {
			this.setSelectedMenu(new Menu());
			result = "security_menu_add";
		} else if (this.getMode().equalsIgnoreCase(ITEM_ENTITIES)) {
			//this.setSelectedEntityLevel(new EntityLevel());
			entityItemDTO = new EntityItemDTO();
			entityItemDTO.setParentLevelDescription("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentLevelDescription());
			entityItemDTO.setParentCode("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentCode());
			entityItemDTO.setParentName("-".equalsIgnoreCase(parentEntityAndLevelInfo.getParentCode()) ? "*None" : parentEntityAndLevelInfo.getParentName());
			entityItemDTO.setEntityLevel(parentEntityAndLevelInfo.getEntityLevelDescription());
			entityItemDTO.setEntityColumn(parentEntityAndLevelInfo.getEntityColumn());	
			logger.debug("to add +++++++++++" + (parentEntityAndLevelInfo.getEntityLevelId()) + " " +parentEntityAndLevelInfo.getEntityId());
			entityItemDTO.setEntityLevelId(parentEntityAndLevelInfo.getEntityLevelId());
			entityItemDTO.setParentEntityId(parentEntityAndLevelInfo.getEntityId());

			transctionDetailColumn = driverReferenceService.findByTransactionDetailColumnName(entityItemDTO.getEntityColumn(),0);

			result = "security_entity_add";
			
		}
		
	    logger.info("end displayAddAction");
	    return result;
	}	
	

	public String addAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {	
			    Role role = getSelectedRole();	
			    logger.info("roleCode control value = " + (String)role.getRoleCode());		    
			    logger.info("roleName control value = " + (String)role.getRoleName());
	            roleService.persist(role);        
	            List<Role> rolesList = roleService.findAllRoles();
	            this.setRolesList(rolesList);
	            selectedRowIndex = -1;
			    result = "security_main";
			} else if(this.getMode().equalsIgnoreCase(ITEM_USERS)) {	
			    User user = getSelectedUser();	
			    logger.info("userCode control value = " + (String)user.getUserCode());		    
			    logger.info("userName control value = " + (String)user.getUserName());
	            userService.persist(user);  
	            cacheManager.flushAllUsersMap();
	            List<User> list = userService.findAllUsers();
	            this.setUsersList(list);
	            List<UserEntity> userEntityList = userEntityService.findByUserCode(getSelectedUser().getUserCode());
	        	if (userEntityList == null || userEntityList.size() == 0 ) {
	        		this.setMode(ITEM_USERS); 
	        		this.setSelectedUser(user);
	        		String maptoEntitiesPage = displayMapAction();
	        		return maptoEntitiesPage;
	        	}		        	
			    } else if(this.getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)) {	
			    Menu menu = getSelectedMenu();	
			    logger.info("menuCode control value = " + (String)menu.getMenuCode());		    
			    logger.info("mainMenuCode control value = " + (String)menu.getMainMenuCode());
	            menuService.persist(menu);        
	            List<Menu> list = menuService.findAllMenus();
	            this.setMenusList(list);
	            selectedRowIndex = -1;
			    result = "security_main";
			} else if(this.getMode().equalsIgnoreCase(ITEM_ENTITIES)) {
				EntityItemDTO entityItemDTO = this.getEntityItemDTO();
				if(validateEntity(entityItemDTO.getEntityCode(),entityItemDTO.getEntityName())){
						EntityItem entityItem = new EntityItem();
						entityItem.setEntityCode(entityItemDTO.getEntityCode());
						entityItem.setEntityName(entityItemDTO.getEntityName());
						entityItem.setEntityLevelId(entityItemDTO.getEntityLevelId());
						entityItem.setParentEntityId(entityItemDTO.getParentEntityId());
						entityItemService.persist(entityItem);
						
						List<EntityItem> entityItemsList = entityItemService.findAllEntityItemsByParent(entityItemDTO.getParentEntityId());
						this.setEntityItemsList(entityItemsList);
						
						int level = entityItemDTO.getEntityLevelId().intValue();
						List<SelectItem> selentityItems = new ArrayList<SelectItem>();
						selentityItems.add(new SelectItem("-"+level,"Select a Level "+level+" Entity"));
				        for(EntityItem entity : entityItemsList){
				        	 if(!"*ALL".equalsIgnoreCase(entity.getEntityCode()))
				        		 selentityItems.add(new SelectItem(entity.getEntityId(),entity.getEntityCode()+"-"+entity.getEntityName()));
				        }
				        UISelectItems items = new UISelectItems();
				        items.setId("item_" + level);
						items.setValue(selentityItems);
						
						if(level == 1){
							levelOneCombo.getChildren().clear();
							levelOneCombo.getChildren().add(items);
						}else if(level == 2){
							levelTwoCombo.getChildren().clear();
							levelTwoCombo.getChildren().add(items);
						}else if(level == 3){
							levelThreeCombo.getChildren().clear();
							levelThreeCombo.getChildren().add(items);
						}else if(level == 4){
							levelFourCombo.getChildren().clear();
							levelFourCombo.getChildren().add(items);
						}	
						selectedRowIndex = -1;
					    result = "security_main";	
				}else{
					return "";
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "failure";
		}
		
	    logger.info("end addAction");	    
        return result;
	}	
	
	public boolean isShowUserSecurityOkMapButton() {
		if(getSelectedUser() !=null) {
			List<UserEntity> userEntityList = userEntityService.findByUserCode(getSelectedUser().getUserCode());
			if (userEntityList == null || userEntityList.size() == 0 ) {
				return true;
			}
		}
		return false;
	}
		
	public String displayDeleteAction() {
		String result = null;
		logger.info("start displayDeleteAction"); 
		currentAction = EditAction.DELETE;

		if (getValidSelection()) {
			if(this.getMode().equalsIgnoreCase(ITEM_ROLES)){
				this.setSelectedRole(getSelectedRoleOri());
   			    List<UserEntity> list = new ArrayList<UserEntity>();
		    	list = userEntityService.findByRole(getSelectedRole().getRoleCode());
		   if(list !=null && list.size()>0){
			   logger.info("NOT NULL "+getSelectedRole().getRoleCode()); 
				FacesMessage message = new FacesMessage("Role mapped to User");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				result="";
		   }else {
			   result = "security_role_delete";
		}
				
			} else if(this.getMode().equalsIgnoreCase(ITEM_USERS)){
				this.setSelectedUser(getSelectedUserOri());
				result = "security_user_delete";
			} else if(this.getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)){
				this.setSelectedMenu(getSelectedMenuOri());
				result = "security_menu_delete";
			} else if(this.getMode().equalsIgnoreCase(ITEM_ENTITIES)){
				this.setSelectedEntity(getSelectedEntityOri());
				this.setEntityItemDTO(getSelectedEntity().getEntityItemDTO());
				if(displayUpdateEntityLevelAction()){
					result = "security_entity_delete";
				}else{
					result = "";
					FacesMessage message = new FacesMessage("Cannot delete Entity with sub-Entities!");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
		}
			
	    logger.info("end displayDeleteAction");
		return result;
	}	
	
	public String deleteAction() {
		String result = null;
		logger.info("start deleteAction"); 
		try {
			if(getMode().equalsIgnoreCase(ITEM_ROLES)){	
			    Role role = getSelectedRole();	
				   roleService.remove(role.getRoleCode());        
		            setSelectedRole(null);
		            setSelectedRoleOri(null);
		            List<Role> rolesList = roleService.findAllRoles();
		            setRolesList(rolesList);
		            selectedRowIndex = -1;
			        result = "security_main";
		
	            
			} else if(getMode().equalsIgnoreCase(ITEM_USERS)){	
			    User user = getSelectedUser();	
	            userService.remove(user.getUserCode()); 

	            //PP-263
	            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	            String userCode = user.getUserCode();
	            String dbName = (String)session.getAttribute("selectedDb");
	            userSecurityBean.removeUserDbMapping(userCode, dbName);
	            
	            cacheManager.flushAllUsersMap();
	            setSelectedUser(null);
	            setSelectedUserOri(null);
	            List<User> list = userService.findAllUsers();
	            setUsersList(list);
	            cacheManager.flushAllUsersMap();
	            selectedRowIndex = -1;
		        result = "security_main";
			} else if(getMode().equalsIgnoreCase(ITEM_MENU_OPTIONS)){	
			    Menu menu = getSelectedMenu();	
	            menuService.remove(menu.getMenuCode());        
	            setSelectedMenu(null);
	            setSelectedMenuOri(null);
	            List<Menu> list = menuService.findAllMenus();
	            setMenusList(list);
	            selectedRowIndex = -1;
		        result = "security_main";
			} else if(getMode().equalsIgnoreCase(ITEM_ENTITIES)) {
				EntityItem entityItems = getSelectedEntity(); 
				entityItemService.remove(entityItems.getEntityId());
				setSelectedEntity(null);
				setSelectedEntityOri(null);
	            List<EntityItem> entityItemsList = entityItemService.findAllEntityItemsByParent(entityItems.getParentEntityId());
				this.setEntityItemsList(entityItemsList);
				
				int level = entityItems.getEntityLevelId().intValue();
				List<SelectItem> selentityItems = new ArrayList<SelectItem>();
				selentityItems.add(new SelectItem("-"+level,"Select a Level "+level+" Entity"));
		        for(EntityItem entity : entityItemsList){
		        	 if(!"*ALL".equalsIgnoreCase(entity.getEntityCode()))
		        		 selentityItems.add(new SelectItem(entity.getEntityId(),entity.getEntityCode()+"-"+entity.getEntityName()));
		        }
		        UISelectItems items = new UISelectItems();
		        items.setId("item_" + level);
				items.setValue(selentityItems);
				
				if(level == 1){
					levelOneCombo.getChildren().clear();
					levelOneCombo.getChildren().add(items);
				}else if(level == 2){
					levelTwoCombo.getChildren().clear();
					levelTwoCombo.getChildren().add(items);
				}else if(level == 3){
					levelThreeCombo.getChildren().clear();
					levelThreeCombo.getChildren().add(items);
				}else if(level == 4){
					levelFourCombo.getChildren().clear();
					levelFourCombo.getChildren().add(items);
				}
				selectedRowIndex = -1;
			    result = "security_main";
			}
		} 		
		catch (Exception ex) {
			ex.printStackTrace();
			result="";
		}
		
	    logger.info("end deleteAction");	    
		return result;
	}

	public String displayMapAction() {
		String result = null;
		try {
			if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {
				List<Menu> allList = menuService.findAllMenus();
				Role role = this.getSelectedRole();	
				List<Menu> grantedList = menuService.findMenusByRole(role.getRoleCode());
				logger.info("grantedList changed before= " + grantedList.size());
				allList.removeAll(grantedList);
			    this.setDeniedList(allList);
			    this.setGrantedList(grantedList);
			    result = "security_role_map";
			} else if(getMode().equalsIgnoreCase(ITEM_USERS)){	
				List<Role> rolesList = roleService.findAllRoles();//source list
				this.setRolesList(rolesList);			    
			    List<EntityItem> eList = entityItemService.findAllEntityItems();//source list
			    

			    //Company configuration
			    //display all Entities for selection rather than just �Business Unit� Entities.
			    /*
			    //keep business unit entity level only
			    Long businessUnitEntityLevelId = getBusinessUnitEntityLevelId();
			    List<EntityItem> removeList = new ArrayList<EntityItem>();
			    if(eList != null && eList.size() > 0){
			    	for(EntityItem entityItem:eList){
			    		if(entityItem.getEntityLevelId().longValue()!=businessUnitEntityLevelId.longValue() && (entityItem.getEntityId().longValue()!=0)){
			    			//Include *ALL in the display list
			    			removeList.add(entityItem);
			    		}
			    	}
			    }
			    */
			    
			    //TaxCode Security
			    if(globalEntityArray!=null){
			    	globalEntityArray.clear();
			    }
			    globalEntityArray = new LinkedHashMap<Long, EntityItem >();
			    
			    if(eList != null && eList.size() > 0){
			    	for(EntityItem entityItem:eList){
			    		entityItem.setType(MAP_MAPPED);//Map first, later unmap.
			    		globalEntityArray.put(entityItem.getEntityId(), entityItem);
			    	}
			    } 			   
		        
		        userEntityRoleList = new ArrayList<UserEntityRoleDTO>();//target list
		        User user = getSelectedUser();
		        List<UserEntity> ueList = userEntityService.findByUserCode(user.getUserCode());
		        for(UserEntity userEnt : ueList){			        
			        String roleCode = userEnt.getRoleCode();
			        Role role = roleService.findById(roleCode);
			        Long entityId = userEnt.getEntityId();
			        EntityItem entityItem = entityItemService.findById(entityId);
			        eList.remove(entityItem);//remove from source list
			        
			        UserEntityRoleDTO uerObj = new UserEntityRoleDTO();
			        uerObj.setUser(user);
			        uerObj.setEntityItem(entityItem);
			        uerObj.setRole(role);
			        
			        userEntityRoleList.add(uerObj);
		        }
		        this.setEntityItemsList(eList);
		        
		        //TaxCode Security
			    //The eList is ummapped
			    if(eList != null && eList.size() > 0){
			    	for(EntityItem entityItem:eList){
			    		EntityItem aEntityItem = globalEntityArray.get(entityItem.getEntityId());
			    		aEntityItem.setType(MAP_NONE);
			    	}
			    }   
			    selectedEntityItem = null;
			    if (rootNode != null) {
			    	rootNode = null;
			    }
		        
		        
		        this.saveMapping.clear();
		        this.deleteMapping.clear();
		        this.changeMapping.clear();
		        result = "security_user_map";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "failure";
		}
		return result;
	}
	
	public String assignEntity(){
		if(selectedEntityItem!= null){
			List<Role> role;
			if(getSelectedRoleList() != null && getSelectedRoleList().size() > 0){
				role = new ArrayList<Role>(getSelectedRoleList());
			}else{
				role = new ArrayList<Role>(getRolesList());
			}
			Role selectedRole = role.get(0);
			logger.debug("selected role          " + selectedRole.toString());
			
			//Company config
			EntityItem selectedEntity = selectedEntityItem;
			selectedEntityItem.setType(MAP_MAPPED);
			selectedEntityItem = null;
			
			this.entityItemsList.remove(selectedEntity);		
			logger.debug("selected Entity         " + selectedEntity.toString());
			
			UserEntityRoleDTO uerObj = new UserEntityRoleDTO();
			uerObj.setUser(getSelectedUser());
	        uerObj.setEntityItem(selectedEntity);
	        uerObj.setRole(selectedRole);        
	        this.userEntityRoleList.add(uerObj);	
	        this.saveMapping.add(uerObj); 
	        if(this.deleteMapping != null && this.deleteMapping.contains(uerObj))
	        	this.deleteMapping.remove(uerObj);
	        
	        //Update changed items
	        if(this.changeMapping != null && this.changeMapping.contains(uerObj)){
	        	this.changeMapping.remove(uerObj);
	        }
	        else if(this.changeMapping != null){
	        	this.changeMapping.add(uerObj);
	        }
	        	
		}
		return "";
	}
	
	public String removeEntity(){
		if(getSelectedMappingList() != null && getSelectedMappingList().size() > 0) {
			List<UserEntityRoleDTO> mapList = new ArrayList<UserEntityRoleDTO>(getSelectedMappingList()); 
			UserEntityRoleDTO uerObj = mapList.get(0);
			logger.debug("selected uerObj         " + uerObj.toString());
	        this.userEntityRoleList.remove(uerObj);	        
	        this.entityItemsList.add(uerObj.getEntityItem());      
	        
	        //Company config
	        EntityItem aEntityItem = findEntityItemById(rootNode, uerObj.getEntityItem().getEntityId());
	        if(aEntityItem!=null){
	        	aEntityItem.setType(MAP_NONE);
	        }
	        
	        this.deleteMapping.add(uerObj);
	        if(this.saveMapping != null && this.saveMapping.contains(uerObj))
	        	this.saveMapping.remove(uerObj);
	        
	        //Update changed items
	        if(this.changeMapping != null && this.changeMapping.contains(uerObj)){
	        	this.changeMapping.remove(uerObj);
	        }
	        else if(this.changeMapping != null){
	        	this.changeMapping.add(uerObj);
	        }
		}
		return "";
	}
	
	public String removeAllEntity(){
		List<UserEntityRoleDTO> mapList = getUserEntityRoleList();
		for(UserEntityRoleDTO uerObj : mapList){
			this.entityItemsList.add(uerObj.getEntityItem());
			
			//Company config
	        EntityItem aEntityItem = findEntityItemById(rootNode, uerObj.getEntityItem().getEntityId());
	        if(aEntityItem!=null){
	        	aEntityItem.setType(MAP_NONE);
	        }
			
			this.deleteMapping.add(uerObj);
			
			//Update changed items
	        if(this.changeMapping != null && this.changeMapping.contains(uerObj)){
	        	this.changeMapping.remove(uerObj);
	        }
	        else if(this.changeMapping != null){
	        	this.changeMapping.add(uerObj);
	        }
		}
		this.userEntityRoleList.clear();
		this.saveMapping.clear();
		return "";
	}
	
	public String mapAction() {
		logger.info("start mapAction"); 
		if(this.getMode().equalsIgnoreCase(ITEM_ROLES)) {
			Role role = this.getSelectedRole();	
			String roleCode = role.getRoleCode();
			
			List<Menu> newGrantedList = this.getGrantedList();
			List<RoleMenu> existRMList = roleUserMenuService.findRoleMenuByRoleCode(roleCode);
			for(Iterator<Menu> itr = newGrantedList.iterator();itr.hasNext();){
				Menu roleMenu = itr.next();		
				RoleMenu newMenu = new RoleMenu(roleCode,roleMenu.getMenuCode());
				if(!existRMList.contains(newMenu)){
					logger.info("menu to be added= " + newMenu.getRoleMenuPK().getMenuCode());
					roleUserMenuService.saveOrUpdate(newMenu);
				}
			}
			List<Menu> oldGrantedList = menuService.findMenusByRole(roleCode);
			for(Iterator<Menu> itr = oldGrantedList.iterator();itr.hasNext();){
				Menu oldMenu = itr.next();
				if(!newGrantedList.contains(oldMenu)){
					logger.info("menu to be deleted= " +oldMenu.getMenuCode());
					RoleMenu oldRoleMenu = new RoleMenu(roleCode,oldMenu.getMenuCode());
					roleUserMenuService.deleteRoleMenu(oldRoleMenu);
				}
			}
			this.setSelectedRole(null);
			this.setSelectedRoleOri(null);
		} else if(getMode().equalsIgnoreCase(ITEM_USERS)){
			
			List<UserEntityRoleDTO> oldAssignedList = this.getDeleteMapping();
			for(UserEntityRoleDTO uer : oldAssignedList){
				//4177
				if(this.changeMapping != null && this.changeMapping.contains(uer)){
					UserEntity userEntity = new UserEntity();
					userEntity.setRoleCode(uer.getRole().getRoleCode());
					UserEntityPK id = new UserEntityPK(uer.getUser().getUserCode(),uer.getEntityItem().getEntityId());
					logger.debug("Removing default mapping " + id);
					userEntity.setId(id);
					userEntityService.delete(userEntity);
				}
			}
			
			List<UserEntityRoleDTO> newAssignedList = this.getSaveMapping();
			for(UserEntityRoleDTO uer : newAssignedList){
				UserEntity userEntity = new UserEntity();
				userEntity.setRoleCode(uer.getRole().getRoleCode());
				UserEntityPK id = new UserEntityPK(uer.getUser().getUserCode(),uer.getEntityItem().getEntityId());
				logger.debug("Adding new mapping " + id);
				userEntity.setId(id);
				userEntityService.saveOrUpdate(userEntity);
			}
			this.saveMapping.clear();

			this.deleteMapping.clear();
			this.changeMapping.clear();
			
			this.setSelectedUser(null);
			this.setSelectedUserOri(null);
		}
		selectedRowIndex = -1;
		refreshUserMappings();
	    return "security_main";	        
	}
	
	private void refreshUserMappings() {
		List<User> list = userService.findAllUsers();
		for(User user : list) {
	      	  List<UserEntity> userEntityList = userEntityService.findByUserCode(user.getUserCode());
		        	if (userEntityList == null || userEntityList.size() == 0 ) {
		        		user.setUserMappedBooleanFag(false);
		        	}
		        	else {
		        		user.setUserMappedBooleanFag(true);
		        	}
	        }
		
			this.setUsersList(list);
	}
	
	public String displayUserEntityMapAction(){
		if(getSelectedMappingList() != null && getSelectedMappingList().size() > 0) {
			List<UserEntityRoleDTO> mapList = new ArrayList<UserEntityRoleDTO>(getSelectedMappingList()); 
			selectedMapping = mapList.get(0);
			List<Menu> allMenu = menuService.findAllMenus();
			List<Menu> roleMenu = menuService.findMenusByRole(selectedMapping.getRole().getRoleCode());
			List<UserMenuEx> exMenu = userMenuExService.findByUserEntity(selectedMapping.getUser().getUserCode(),selectedMapping.getEntityItem().getEntityId());
			defaultMenu = new ArrayList<MenuDTO>();
			deniedMenu = new ArrayList<MenuDTO>(); 
			for(Menu menu : allMenu){
				boolean added = false;
				for(UserMenuEx emeMenu: exMenu){
					if(emeMenu.getId().getMenuCode().equalsIgnoreCase(menu.getMenuCode())){
						if(GRANTED_EXCEPTION_TYPE.equalsIgnoreCase(emeMenu.getExceptionType())){
							MenuDTO granted = new MenuDTO();
							granted.setMenuCode(menu.getMenuCode());
							granted.setOptionName(menu.getOptionName());
							granted.setExceptionType(GRANTED_EXCEPTION_TYPE);
							defaultMenu.add(granted);
							added = true;
						}else if(DENIED_EXCEPTION_TYPE.equalsIgnoreCase(emeMenu.getExceptionType())){
							MenuDTO denied = new MenuDTO();
							denied.setMenuCode(menu.getMenuCode());
							denied.setOptionName(menu.getOptionName());
							denied.setExceptionType(DENIED_EXCEPTION_TYPE);
							deniedMenu.add(denied);
							added = true;
						}
					}
				}
				for(Menu rlMenu: roleMenu){
					if(rlMenu.getMenuCode().equalsIgnoreCase(menu.getMenuCode())){
						MenuDTO grantedMenu = new MenuDTO();
						grantedMenu.setMenuCode(menu.getMenuCode());
						grantedMenu.setOptionName(menu.getOptionName());
						grantedMenu.setExceptionType(ROLE_EXCEPTION_TYPE);
						if(!deniedMenu.contains(grantedMenu)){
							defaultMenu.add(grantedMenu);
							added = true;
						}
					}
				}
				if(!added){
					MenuDTO denied = new MenuDTO();
					denied.setMenuCode(menu.getMenuCode());
					denied.setOptionName(menu.getOptionName());
					denied.setExceptionType(ROLE_EXCEPTION_TYPE);
					deniedMenu.add(denied);
				}
			}
			return "security_user_entity_map";
		}else{
			FacesMessage message = new FacesMessage("Please select a row and retry.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return "";
		}
	}
	
	public String mapUserEntityAction(){
		List<UserMenuEx> exMenu = userMenuExService.findByUserEntity(selectedMapping.getUser().getUserCode(),selectedMapping.getEntityItem().getEntityId());
		List<Menu> menuList = menuService.findMenusByRole(selectedMapping.getRole().getRoleCode());
		
		//4468 Add user/entity mapping if not exist.
		UserEntityPK userentityid = new UserEntityPK(selectedMapping.getUser().getUserCode(),getSelectedMapping().getEntityItem().getEntityId());
		UserEntity userEntity = userEntityService.findById(userentityid);
		if(userEntity==null){
			userEntity = new UserEntity();
			userEntity.setRoleCode(selectedMapping.getRole().getRoleCode());
			logger.debug("Adding new mapping " + userentityid);
			userEntity.setId(userentityid);
			userEntityService.saveOrUpdate(userEntity);
		}
		
		for(Menu roleMenu : menuList){
			defaultMenu.remove(roleMenu.getMenuDTO());
		}		
		for(Menu roleMenu : menuList){
			MenuDTO deny = roleMenu.getMenuDTO();
			if(deniedMenu.contains(deny)){
				UserMenuEx newExc = new UserMenuEx();
				UserMenuExPK id = new UserMenuExPK(getSelectedMapping().getUser().getUserCode(),getSelectedMapping().getEntityItem().getEntityId(),deny.getMenuCode());
				newExc.setId(id);
				newExc.setExceptionType(DENIED_EXCEPTION_TYPE);
				userMenuExService.saveOrUpdate(newExc);
			}
		}
		for(MenuDTO save : defaultMenu){
			UserMenuEx newExc = new UserMenuEx();
			UserMenuExPK id = new UserMenuExPK(getSelectedMapping().getUser().getUserCode(),getSelectedMapping().getEntityItem().getEntityId(),save.getMenuCode());
			newExc.setId(id);
			newExc.setExceptionType(GRANTED_EXCEPTION_TYPE);
			userMenuExService.saveOrUpdate(newExc);
		}
		for(UserMenuEx deleteExMenu : exMenu){
			if(GRANTED_EXCEPTION_TYPE.equalsIgnoreCase(deleteExMenu.getExceptionType())){
				MenuDTO menu  = new MenuDTO();
				menu.setMenuCode(deleteExMenu.getId().getMenuCode());
				if(!defaultMenu.contains(menu))
					userMenuExService.delete(deleteExMenu);
			}else if(DENIED_EXCEPTION_TYPE.equalsIgnoreCase(deleteExMenu.getExceptionType())){
				MenuDTO menu  = new MenuDTO();
				menu.setMenuCode(deleteExMenu.getId().getMenuCode());
				if(!deniedMenu.contains(menu))
					userMenuExService.delete(deleteExMenu);
			}
		}
		return "security_user_map";
	}
	
	//not used
	@SuppressWarnings("unchecked")
	public void denyDefaultmenu(ActionEvent e){
		HtmlListShuttle myListShuttle = (HtmlListShuttle)((HtmlAjaxSupport)e.getComponent()).getParent();
		Collection<MenuDTO> selection = myListShuttle.getTargetSelection();
		List<MenuDTO> trget = new ArrayList<MenuDTO>(selection);
		logger.debug("traget moved " + trget.size());
		for(MenuDTO menu : trget){
			if("D".equalsIgnoreCase(menu.getExceptionType())){
					menu.setExceptionType("");
			}else{
				menu.setExceptionType("G");
			}
			defaultMenu.add(menu);
			deniedMenu.remove(menu);
		}
	}
	
	public void onlistchangedAction(ActionEvent e){
		List<Menu> roleMenuList = menuService.findMenusByRole(selectedMapping.getRole().getRoleCode());

		for(MenuDTO deniedDTO: deniedMenu){
			boolean found = false;
			for(Menu roleMenu : roleMenuList ){
				MenuDTO role= roleMenu.getMenuDTO();
				if(deniedDTO.getMenuCode().equalsIgnoreCase(role.getMenuCode())){
					found = true;
					break;
				}
			}

			if(found){
				deniedDTO.setExceptionType(DENIED_EXCEPTION_TYPE);
			}
			else{
				deniedDTO.setExceptionType(ROLE_EXCEPTION_TYPE);
			}
		}
		
		for(MenuDTO defaultDTO: defaultMenu){
			boolean found = false;
			for(Menu roleMenu : roleMenuList ){
				MenuDTO role= roleMenu.getMenuDTO();
				if(defaultDTO.getMenuCode().equalsIgnoreCase(role.getMenuCode())){
					found = true;
					break;
				}
			}

			if(found){
				defaultDTO.setExceptionType(ROLE_EXCEPTION_TYPE);
			}
			else{
				defaultDTO.setExceptionType(GRANTED_EXCEPTION_TYPE);
			}
		}
	}
	
	public String okAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteAction();
				refreshUserMappings();
				break;
				
			case UPDATE:
				result = updateAction();
				refreshUserMappings();
				break;
				
			case ADD:
				result = addAction();
				refreshUserMappings();
				break;
				
			case VIEW:
				result = cancelAction();
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String cancelAction() {
		logger.info("in cancelAction");
		// Force prior values to be forgotton
		this.setSelectedRole(getSelectedRoleOri());
		this.setSelectedMenu(getSelectedMenuOri());
		this.setSelectedUser(getSelectedUserOri());
		this.setSelectedEntity(getSelectedEntityOri());
		FacesUtils.refreshView();
	    return "security_main_cancel";
	}		
	
	public String changePasswordAction() {
		this.pwdChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        String userCode = user.getUserCode();
        String oldPwd = "";
        UserSecurityDTO userSecurityDTO = userSecurityBean.getUserSecurity(userCode.toUpperCase());
		if(userSecurityDTO!=null){
			oldPwd = DesEncrypter.getInstance().decrypt(userSecurityDTO.getPassword());
		}
		else{
			this.pwdChangeMessage = "User not found.";
        	return null;
		}
        String pwd = (String) this.pwdControl.getValue();
        String newPwd = (String) this.newPwdControl.getValue();
        String confirmPwd = (String) this.confirmPwdControl.getValue();
             
        // Check for blank passwords
        if (pwd == null || "".equalsIgnoreCase(pwd.trim())) {
        	this.pwdChangeMessage = "Please enter current password.";
        	return null;        	
        }
        
        // Check entered current password against saved current password 
        if (!pwd.equals(oldPwd)){
        	this.pwdChangeMessage = "Password does not match user password.  Please correct.";
        	return null;
        }
        
        //Change password
        boolean passwordChange = false;
        if(!((newPwd == null || "".equalsIgnoreCase(newPwd.trim())) && 
        		(confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())))) {
        	//Not all empty
        	if (newPwd == null || "".equalsIgnoreCase(newPwd.trim()) || 
        		confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())) {
        		this.pwdChangeMessage = "Please enter all fields.";
        		return null;        	
        	}
        	
        	//check Password Complexity
            String returnMessage = userSecurityBean.isValidNewPassword(userSecurityDTO.getUserId(), pwd, newPwd, confirmPwd);
            if(returnMessage!=null){
            	this.pwdChangeMessage = returnMessage;
                return null;
            }

            passwordChange = true;
        }
        
        //Change email
        boolean emailChange = false;  
        String newEmail = (String) this.newEmailControl.getValue();
 		if (!newEmail.equals(oldEmail)) { 
 			emailChange = true;
 		}
 		
        if(passwordChange){
        	if(!userSecurityBean.updateUserPassword(userCode.toUpperCase(), DesEncrypter.getInstance().encrypt(newPwd))){
        		this.pwdChangeMessage = "Cannot update password.";
                return null;
        	}
        }
        
 		if(emailChange){
 			userSecurityDTO.setEmail(newEmail);
 			userSecurityBean.updateUserSecurity(userSecurityDTO);
 		}
 		
        pwdControl.setValue("");	
        newPwdControl.setValue("");
        confirmPwdControl.setValue("");
        newEmailControl.setValue("");
        oldEmail = "";
        
        return "success";
	}
	
	/*
	 public String changePasswordAction() {
		this.pwdChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the password from the ConfigSetting
        String userCode = user.getUserCode();
        String oldPwd = userConfig.getProperty("user." + userCode + ".password");
        String pwd = (String) this.pwdControl.getValue();
        String newPwd = (String) this.newPwdControl.getValue();
        String confirmPwd = (String) this.confirmPwdControl.getValue();
        
        // Check for blank passwords
        if (pwd == null || "".equalsIgnoreCase(pwd.trim())) {
        	this.pwdChangeMessage = "Please enter current password.";
        	return null;        	
        }
        
        // Check entered current password against saved current password 
        if (!pwd.equals(oldPwd)){
        	this.pwdChangeMessage = "Password does not match user password.  Please correct.";
        	return null;
        }
        
        //Change password
        boolean passwordChange = false;
        if(!((newPwd == null || "".equalsIgnoreCase(newPwd.trim())) && 
        		(confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())))) {
        	//Not all empty
        	if (newPwd == null || "".equalsIgnoreCase(newPwd.trim()) || 
        		confirmPwd == null || "".equalsIgnoreCase(confirmPwd.trim())) {
        		this.pwdChangeMessage = "Please enter all fields.";
        		return null;        	
        	}
        	
        	// Check new and confirmed passwords are the same
            if (!newPwd.equals(confirmPwd)) {
            	this.pwdChangeMessage = "New password and re-entered new password should be identical.  Please correct.";
            	return null;
            }
            
            passwordChange = true;
        }
        
        //Change email
        boolean emailChange = false;  
        String newEmail = (String) this.newEmailControl.getValue();
 		if (!newEmail.equals(oldEmail)) { 
 			emailChange = true;
 		}
 		
        if(passwordChange){
        	userConfig.setProperty("user." + userCode + ".password", newPwd);
        }
 		if(emailChange){
 	        userConfig.setProperty("user." + userCode + ".email", newEmail);
 		}
 		if(passwordChange || emailChange){
 			userConfig.saveProperties();
 		}
 		
        pwdControl.setValue("");	
        newPwdControl.setValue("");
        confirmPwdControl.setValue("");
        newEmailControl.setValue("");
        oldEmail = "";
        
        return "success";
	}
	 * */
	
	public String cancelchangePasswordAction(){
		   pwdControl.setValue("");	
	       newPwdControl.setValue("");
	       confirmPwdControl.setValue("");
	       newEmailControl.setValue("");
	       oldEmail = ""; 
	        
		return "cancel";
	}

	//not used
	public void selectedUserEntityRowChanged(ActionEvent e) throws AbortProcessingException { 
		logger.info("enter selectedUserEntityRowChanged");
	    HtmlScrollableDataTable table = (HtmlScrollableDataTable) ((HtmlAjaxSupport) e.getComponent()) 
		    .getParent(); 
		Selection sel = table.getSelection(); 
		List<UserEntity> selected = new ArrayList<UserEntity>(); 
		for (Iterator<Object> iter = sel.getKeys(); iter.hasNext();) { 
			Object next = iter.next(); 
			table.setRowKey(next); 
			selected.add((UserEntity)table.getRowData()); 
		} 
		if (selected.size() > 0) {	
			this.setSelectedUserEntity(selected.get(0));
			logger.info(" role code = " + this.selectedUserEntity.getRoleCode());
			logger.info("exit selectedUserEntityRowChanged");				
		} else {
			logger.info(" selected.size() < 0 ");			
		}	
	}	
	
	
	public void validateUserCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value;
	    if (currentAction.isAddAction() && (userService.findById(code) != null)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("User code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}

	public void validateRoleCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value;
		if (currentAction.isAddAction() && (roleService.findById(code) != null)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Role code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public void validateMenuCode(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure code is unique
		String code = (String) value;
		if (currentAction.isAddAction() && (menuService.findById(code) != null)) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Menu code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
			
	public boolean validateEntity(String code, String name) {
	    // Ensure code is unique and not empty
		logger.debug("code : " + code + "name : " + name );		
		if(name == null || code == null || name.length() <= 0 || code.length() <= 0){
			FacesMessage message  = new FacesMessage("Entity code and Entity name cannot be empty.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }else if (currentAction.isAddAction() && (entityItemService.findByCode(code) != null)) {
			FacesMessage message = new FacesMessage("Entity code already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return false;
	    }
		return true;
	}
	
	
	
	/*--------------------------------------------Krishna's Changes for Entity Structure level-------------------------------------------*/
	public void setAddAction(){
		if(entityLevelToSet!=null){
			transDetailColumnName.setValue(entityLevelToSet.getColumnName());
			description.setValue(entityLevelToSet.getDescription());
			showOkButton=false;
			}
		}
	
	public String setUpdateAction(){
		if(entityLevelToSet !=null){
			transDetailColumnName.setValue(entityLevelToSet.getColumnName());
			description.setValue(entityLevelToSet.getDescription());
			return "security_entitylevel_update";
		}
		else {
			FacesMessage message = new FacesMessage("Please select a row to add new Entity");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
	}
	
	public Long getMaxEntity(){
		List <EntityLevel>getMaxList = new ArrayList<EntityLevel>();
		List<Long> tempList = new ArrayList<Long>();
		getMaxList = entityLevelService.findAllEntityLevels();
		Long tempId =0l;
		if(!getMaxList.isEmpty()){
		for(EntityLevel entityLevel:getMaxList){
		Long temp =	entityLevel.getEntityLevelId();
		tempList.add(temp);
		}
		tempId = (((Long)(tempList.get(0))).longValue());
		//TODO have to change for loop
		for(int i =0; i<tempList.size(); i++){
		     if(((Long)(tempList.get(i))).longValue()>tempId){
		        tempId =  ((Long)(tempList.get(i))).longValue();  
		     }
		}
		nextEntity = tempId +1;
		}
		else {
			nextEntity=1;
		}
		return nextEntity;
	}
	
	public Long getBusinessUnitEntityLevelId(){
		Map<String,DriverNames> transactionDriverNamesMap = cacheManager.getTransactionColumnDriverNamesMap(TaxMatrix.DRIVER_CODE);
		
		List <EntityLevel> entityLevelList = entityLevelService.findAllEntityLevels();
		Long tempId =0l;
		DriverNames driverNames = null;
		if(!entityLevelList.isEmpty()){
			for(EntityLevel entityLevel:entityLevelList){
				driverNames = transactionDriverNamesMap.get(entityLevel.getTransDetailColumnName());
				if(driverNames!=null && (driverNames.getBusinessUnitFlag() != null) && 
						driverNames.getBusinessUnitFlag().equals("1")){
					tempId = entityLevel.getEntityLevelId();
					break;
				}
			}
		}
		
		return tempId;
	}
	
	public String setUpForAddEntity(){
		transDetailColumnName.resetValue();
        description.resetValue();
		getMaxEntity();
		updateTimestamp.resetValue();
		updateDisplay=false;

		return "security_entitylevel_add";
		
	}

	public String addEntityStructureAction(){
		//String transDetailColumnNameLocal = transDetailColumnName.getValue().toString().trim().toUpperCase();
		String transDetailColumnNameLocal = "";
		if(transDetailColumnName.getValue()!=null){
			transDetailColumnNameLocal = transDetailColumnName.getValue().toString().trim().toUpperCase();
		}
		String descriptionLocal = description.getValue().toString().trim();
		boolean errorNow = false;
		
		if(nextEntity>0){
			//No blank Trans. Dtl. Column Name
			if (transDetailColumnNameLocal.length()==0) {
				FacesMessage message = new FacesMessage("Trans. Dtl. Column Name field may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			} 
		}
		
		EntityLevelDTO entityLevelDTO = new EntityLevelDTO();
		entityLevelDTO.setEntityLevelId(nextEntity);
		entityLevelDTO.setTransDetailColumnName(transDetailColumnNameLocal);
		entityLevelDTO.setDescription(descriptionLocal);
		 
		//No duplicate Trans. Dtl. Column Name
		if(!errorNow && (Long)entityLevelId.getValue()>0L){
			boolean preventAdd = false;
			List <EntityLevel>validateList = new  ArrayList<EntityLevel>();
			validateList = entityLevelService.findAllEntityLevels();
			for(EntityLevel entityLevel:validateList){
				entityLevel.getTransDetailColumnName();	
				if(entityLevel.getTransDetailColumnName()!=null){
					if(entityLevel.getTransDetailColumnName().equalsIgnoreCase(entityLevelDTO.getTransDetailColumnName())){
						preventAdd=true;
					}
				}
			}
			
			if(preventAdd){
				entityLevelToSet=null;

				FacesMessage message = new FacesMessage("Duplicate Trans. Dtl. Column Name is not Allowed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			}
		}
		
		//Valid Trans. Dtl. Column Name only
		if(!errorNow && (Long)entityLevelId.getValue()>0L){
			Map<String,DataDefinitionColumn> transMap = cacheManager.getDataDefinitionColumnMap("TB_PURCHTRANS");
			if(!transMap.containsKey(transDetailColumnNameLocal)){
				FacesMessage message = new FacesMessage("Invalid Trans. Dtl. Column Name is not Allowed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			}
		}
		
		//No blank description
		if (descriptionLocal.length()==0 ) {
			FacesMessage message = new FacesMessage("Description field may not be blank.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorNow = true;
		}
		
		//Display error if needed
		if (errorNow) {
			return null;
		}
		
		entityLevelService.save(entityLevelDTO);
		entityLevelToSet=null;
		transDetailColumnName.setValue("");
		description.setValue("");
		disableDeleteButton=false;
		showUpdateButton=false;
		selectedEntityLevelRowIndex = -1;
		
		return "security_entitylevel_cancel";
	}
	
	public String displayUpdateEntityStructureLevelAction() {
		logger.info("start displayUpdateEntityLevelAction");
        transDetailColumnName.resetValue();
        description.resetValue();
		if(selectedEntityLevel!=null){
		EntityLevel el = entityLevelService.findById(selectedEntityLevel.getEntityLevelId());
		this.setSelectedEntityLevel(el);
        logger.info(" el.id = " + el.getEntityLevelId()) ; 		
        logger.info(" el.desc = " + this.getSelectedEntityLevel().getDescription()) ;  
        logger.info(" el.column = " + this.getSelectedEntityLevel().getTransDetailColumnName()) ;         
        logger.info(" el.user = " + this.getSelectedEntityLevel().getUpdateUserId()) ;  
        logger.info(" el.ts = " + this.getSelectedEntityLevel().getUpdateTimestamp()) ; 
        entityLevelId.setValue(selectedEntityLevel.getEntityLevelId());
        transDetailColumnName.setValue(selectedEntityLevel.getTransDetailColumnName());
        description.setValue(selectedEntityLevel.getDescription());
        updateUserId.setValue(selectedEntityLevel.getUpdateUserId());
        updateTimestamp.setValue(selectedEntityLevel.getUpdateTimestamp());
        updateDisplay=true;
		return "security_entitylevel_update";	
		}
		FacesMessage message = new FacesMessage("Please select a row to update");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}	

	public String updateEntityStructureAction(){
		logger.info("Enter Update Entity Action");
		
		String transDetailColumnNameLocal = "";
		if(transDetailColumnName.getValue()!=null){
			transDetailColumnNameLocal = transDetailColumnName.getValue().toString().trim().toUpperCase();
		}
		String descriptionLocal = description.getValue().toString().trim();
		boolean errorNow = false;

		if((Long)entityLevelId.getValue()>0L){
			//No blank Trans. Dtl. Column Name
			if (transDetailColumnNameLocal.length()==0) {
				FacesMessage message = new FacesMessage("Trans. Dtl. Column Name field may not be blank.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			} 
		}
		
		EntityLevelDTO entityLevelDTO = new EntityLevelDTO();
		entityLevelDTO.setEntityLevelId((Long)entityLevelId.getValue());
		entityLevelDTO.setTransDetailColumnName(transDetailColumnNameLocal);
		entityLevelDTO.setDescription(descriptionLocal);
		
		//No duplicate Trans. Dtl. Column Name
		if(!errorNow && (Long)entityLevelId.getValue()>0L ){
			boolean preventAdd = false;
			List <EntityLevel>validateList = new  ArrayList<EntityLevel>();
			validateList = entityLevelService.findAllEntityLevels();
			
			for(EntityLevel entityLevel:validateList){
				entityLevel.getTransDetailColumnName();	
				if(entityLevel.getTransDetailColumnName()!=null){
					if(entityLevel.getTransDetailColumnName().toString().equalsIgnoreCase(entityLevelDTO.getTransDetailColumnName().toString()) 
						&& (!entityLevel.getEntityLevelId().equals(entityLevelDTO.getEntityLevelId()))){
						preventAdd=true;
					}
				}
			}
			
			if(preventAdd){
				entityLevelToSet=null;

				FacesMessage message = new FacesMessage("Duplicate Trans. Dtl. Column Name is not Allowed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			}
		}	
		
		//Valid Trans. Dtl. Column Name only
		if(!errorNow && (Long)entityLevelId.getValue()>0L){
			Map<String,DataDefinitionColumn> transMap = cacheManager.getDataDefinitionColumnMap("TB_PURCHTRANS");
			if(!transMap.containsKey(transDetailColumnNameLocal)){
				FacesMessage message = new FacesMessage("Invalid Trans. Dtl. Column Name is not Allowed.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				errorNow = true;
			}
		}
		
		//No blank description
		if (descriptionLocal.length()==0 ) {
			FacesMessage message = new FacesMessage("Description field may not be blank.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
			errorNow = true;
		}
		
		//Display error if needed
		if (errorNow) {
			return null;
		}

		entityLevelService.update(entityLevelDTO);
		entityLevelToSet=null; 
		selectedEntityLevel = null;
		disableDeleteButton=false;
		showUpdateButton=false;
		selectedEntityLevelRowIndex = -1;
		
		return "security_entitylevel_cancel";
	}
	
	public String setUpForDelete(){
		getMaxEntity();
		Long tempMax = nextEntity-1;
		if(selectedEntityLevel.getEntityLevelId()!=null && selectedEntityLevel.getEntityLevelId().equals(tempMax)){
			EntityLevel el = entityLevelService.findById(selectedEntityLevel.getEntityLevelId());
			this.setSelectedEntityLevel(el);

			return "security_entitylevel_delete";
		}
		FacesMessage message = new FacesMessage("Please select last row to delete");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
		
	}
	public String removeEntityLevelAction(){
			entityLevelService.remove(selectedEntityLevel.getEntityLevelId());
			selectedEntityLevel=null;
			disableDeleteButton=false;
			showUpdateButton=false;
			selectedEntityLevelRowIndex = -1;
			return "security_entitylevel_cancel";
	}
	
	public void cancelEntityLevelAction(){
		if(!dataDefinitionColList.isEmpty()){
		dataDefinitionColList.clear();}
		if(entityLevelToSet!=null){
		entityLevelToSet = null;}
		if(selectedEntityLevel !=null){
			selectedEntityLevel = null;
		}
		transDetailColumnName.resetValue();
        description.resetValue();
        showOkButton=false;
		showUpdateButton=false;
		disableDeleteButton=false;
		
	}
	public String cancelAddEntityLevelAction(){
		if(!dataDefinitionColList.isEmpty()){
		dataDefinitionColList.clear();}
		transDetailColumnName.resetValue();
        description.resetValue();
		return "security_entitylevel_cancel";
		
	}
	
	public void getByColumnName(ActionEvent e){
		String columnName ="";
		String description ="";
		String dataType ="";
		if(columnSearch!=null || columnSearch.getValue() !=""){
		columnName = columnSearch.getValue().toString().toUpperCase().trim();
		}
		if(descriptionSearch !=null || descriptionSearch.getValue() !=""){
		description = descriptionSearch.getValue().toString().trim();
		}
		if(dataTypeSearch !=null || dataTypeSearch.getValue()!=""){
		dataType = dataTypeSearch.getValue().toString().toUpperCase().trim();
		}
		if(!dataDefinitionColList.isEmpty()){
			dataDefinitionColList.clear();
		}
		
		dataDefinitionColList = dataDefinitionService.getAllDataDefinitionByColumnNameAndDesc(columnName, description, dataType);
		showOkButton=false;
	}
	
	public void getAllByTableName(ActionEvent e){
		String colName="";
		String desc = "";
		String dataSearch = "";
			
		if(transDetailColumnName.getSubmittedValue().toString().trim()!=null 
				&& transDetailColumnName.getSubmittedValue().toString().trim()!=""){
			logger.info("getAllByTableName trans col value "+transDetailColumnName.getSubmittedValue().toString());
			columnSearch.setValue(transDetailColumnName.getSubmittedValue().toString().trim());
			colName = transDetailColumnName.getSubmittedValue().toString().trim();
			dataDefinitionColList = dataDefinitionService.getAllDataDefinitionByColumnNameAndDesc(colName, desc, dataSearch);
		}else{
		dataDefinitionColList = dataDefinitionService.getAllDataDefinitionColumnByTable(tableName);
		}
		updateDisplay=false;
		showOkButton=false;
		//return "security_entitylevel_set_column"; 
	}
	
	public List<DataDefinitionColumn> getDataDefinitionColList() {
		//dataDefinitionColList = dataDefinitionService.getAllDataDefinitionColumnByTable(tableName);
		return dataDefinitionColList;
	}

	public void setDataDefinitionColList(
			List<DataDefinitionColumn> dataDefinitionColList) {
		this.dataDefinitionColList = dataDefinitionColList;
	}

	public void setUserEntityList(List<UserEntity> userEntityList) {
		this.userEntityList = userEntityList;
	}	
	
	public List<UserEntity> getUserEntityList() {
		logger.info("getUserEntityList()");
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO user = (UserDTO) session.getAttribute("user");
		
		userEntityList = userEntityService.findByUserCode(user.getUserCode());
		if (userEntityList != null) {
			logger.info("size = " + userEntityList.size());
		} else {
			logger.info("size = 0 " );			
		}
        this.setUserEntityList(userEntityList);	
		return userEntityList;
	}
	
	public void resetInputs(){
		columnSearch.setValue("");
		descriptionSearch.setValue("");
		dataTypeSearch.setValue("");
		showOkButton=false;
	}

	public boolean isShowOkButton() {
		return showOkButton;
	}

	public void setShowOkButton(boolean showOkButton) {
		this.showOkButton = showOkButton;
	}

	public void setUserConfig(String file) {
		if (this.userConfig == null) { 
			try {
				this.userConfig = new ConfigSetting(file);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
	
	public void changeActionListener(ActionEvent event) {
    	logger.info("start changeActionListener");

  		this.pwdChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the email from the ConfigSetting
        String userCode = user.getUserCode();
        String email = "";
        
        UserSecurityDTO userSecurityDTO = userSecurityBean.getUserSecurity(userCode.toUpperCase());
		if(userSecurityDTO!=null){
			email = userSecurityDTO.getEmail();
		}
        
        
        //if (userConfig.propertyExists("user." + userCode + ".email")){
        //	email = userConfig.getProperty("user." + userCode + ".email");
    	//}
        
        newEmailControl.setValue(email); 
        oldEmail = email;
        pwdControl.setValue("");	
	    newPwdControl.setValue("");
	    confirmPwdControl.setValue("");
	}
	
	public void changeActionListenerBK(ActionEvent event) {
    	logger.info("start changeActionListener");

  		this.pwdChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the email from the ConfigSetting
        String userCode = user.getUserCode();
        String email = "";
        if (userConfig.propertyExists("user." + userCode + ".email")){
        	email = userConfig.getProperty("user." + userCode + ".email");
    	}
        
        newEmailControl.setValue(email); 
        oldEmail = email;
        pwdControl.setValue("");	
	    newPwdControl.setValue("");
	    confirmPwdControl.setValue("");
	}
	
	public void changeEmailActionListener(ActionEvent event) {
    	logger.info("start changeEmailActionListener");
    	
    	this.emailChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the email from the ConfigSetting
        String userCode = user.getUserCode();
        String email = "";
        UserSecurityDTO userSecurityDTO = userSecurityBean.getUserSecurity(userCode.toUpperCase());
		if(userSecurityDTO!=null){
			email = userSecurityDTO.getEmail();
		}
        //if (userConfig.propertyExists("user." + userCode + ".email")){
       // 	email = userConfig.getProperty("user." + userCode + ".email");
    	//}
        
        newEmailControl.setValue(email);   
	}
	
	public void changeEmailActionListenerBK(ActionEvent event) {
    	logger.info("start changeEmailActionListener");
    	
    	this.emailChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the email from the ConfigSetting
        String userCode = user.getUserCode();
        String email = "";
        if (userConfig.propertyExists("user." + userCode + ".email")){
        	email = userConfig.getProperty("user." + userCode + ".email");
    	}
        
        newEmailControl.setValue(email);   
	}
	
	public String changeEmailAction() {
		this.emailChangeMessage = null;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        UserDTO user = (UserDTO) session.getAttribute("user");
        
        // Get the UserCode to get the password from the ConfigSetting
        String userCode = user.getUserCode();
        String oldPwd = userConfig.getProperty("user." + userCode + ".password");
        String pwd = (String) this.pwdEmailControl.getValue();
        String newEmail = (String) this.newEmailControl.getValue();
        if (pwd == null || "".equalsIgnoreCase(pwd.trim())) {
        	this.emailChangeMessage = "Please enter user password.";
        	return null;        	
        }
        
        // Check entered current password against saved current password 
        if (!pwd.equals(oldPwd)){
        	this.emailChangeMessage = "Password does not match user password.  Please correct.";
        	return null;
        }
        
        // Passed all checks ... change value ... then save
        userConfig.setProperty("user." + userCode + ".email", newEmail);
        userConfig.saveProperties();
        
        pwdEmailControl.setValue("");	
        newEmailControl.setValue("");
        
        return "security_password";
	}
	
	public String cancelChangeEmailAction(){
		pwdEmailControl.setValue("");	
		newEmailControl.setValue("");
	        
		return "security_password";
	}

	public loginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}	
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
	
	public boolean getDisableUpdateUserSecurityBtn() {
		if (mode.equalsIgnoreCase(ITEM_USERS)) {
			int seluserAdminFlag = Integer.parseInt(getSelectedUser().getAdminFlag()); 
			cacheManager.flushAllUsersMap();
			User user = getCurrentUser();
			int currUserAdminFlag =  Integer.parseInt(getCurrentUser().getAdminFlag());
			if(USER_STSCORP.equals(user.getUserCode())) { 
				return false;
			}
			else if(seluserAdminFlag >= currUserAdminFlag ) {
					return true;
			    }
			else {
				return false;
			}
		}
		return false; 
   }
	
	
	public boolean getDisableDeleteUserSecurityBtn() {
		if (mode.equalsIgnoreCase(ITEM_USERS)) {
			int seluserAdminFlag = Integer.parseInt(getSelectedUser().getAdminFlag()); 
			cacheManager.flushAllUsersMap();
			User user = getCurrentUser();
			int currUserAdminFlag =  Integer.parseInt(getCurrentUser().getAdminFlag());
			
			if(USER_STSCORP.equals(user.getUserCode())) { 
				return false;
			}
			else if(currUserAdminFlag == 0 || currUserAdminFlag == 1) {
				return true;
			}
			else if(seluserAdminFlag >= currUserAdminFlag ) {
				return true;
		    }
			else {
				return false;
			}
		}
		return false; 
   }
}
