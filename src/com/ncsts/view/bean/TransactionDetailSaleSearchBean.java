package com.ncsts.view.bean;

import com.ncsts.services.TransactionDetailBillService;
import org.springframework.beans.factory.annotation.Autowired;

import com.ncsts.domain.EntityLocnSet;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.EntityLocnSetService;
import com.ncsts.domain.BillTransaction;

import java.util.HashMap;

public class TransactionDetailSaleSearchBean extends TransactionDetailSaleBean {
	
	@Autowired
	private TransactionDetailSaleViewBean transactionDetailSaleViewBean;
	@Autowired
	private EntityLocnSetService entityLocnSetService;



	@Autowired
	private TransactionDetailBillService transactionDetailBillService;
	private TransactionDetailSaleBean transactionDetailSaleBean;
	private BillingTestingToolBean billingTestingToolBean;
	private String beanName = "";
	
	public TransactionDetailSaleSearchBean(){
		super();
		
		//0004440
		this.getFilterTaxCodeHandler().setReturnRuleUrl("transactions_sale"); //transactions_view
		this.getFilterTaxCodeHandler1().setReturnRuleUrl("transactions_sale");
	}
	
	public String displayViewAction() {

		BillTransaction billTransactionForView = new BillTransaction();
		EntityLocnSet entityLocSet=entityLocnSetService.getEntityLocationforView(this.getSelectedTransaction().getBilltransId());
	   	org.springframework.beans.BeanUtils.copyProperties(this.getSelectedTransaction(), billTransactionForView);
    	transactionDetailSaleViewBean.setViewsaleId(this.getSelectedTransaction());
    	transactionDetailSaleViewBean.setEntityLocSet(entityLocSet);
    	transactionDetailSaleViewBean.setSaleTransactionForView(billTransactionForView);
    	transactionDetailSaleViewBean.setViewReturnPage(null);

		transactionDetailSaleViewBean.setCurrentNexusInfo(transactionDetailBillService.getNexusInfo(billTransactionForView.getBilltransId()));
		transactionDetailSaleViewBean.setCurrentTaxRateInfo(transactionDetailBillService.getTaxRateInfo(billTransactionForView.getBilltransId()));
		
		return "transactions_view";
	}

	public EntityLocnSetService getEntityLocnSetService() {
		return entityLocnSetService;
	}
	
	public void setBean(TransactionDetailSaleBean transactionDetailSaleBean, String beanName){
		this.transactionDetailSaleBean = transactionDetailSaleBean;
		this.beanName = beanName;
	}

	public void setEntityLocnSetService(EntityLocnSetService entityLocnSetService) {
		this.entityLocnSetService = entityLocnSetService;
	}
	
	@Override
	public TransactionDetailBillService getTransactionDetailBillService() {
		return transactionDetailBillService;
	}

	@Override
	public void setTransactionDetailBillService(TransactionDetailBillService transactionDetailBillService) {
		this.transactionDetailBillService = transactionDetailBillService;
	}

}
