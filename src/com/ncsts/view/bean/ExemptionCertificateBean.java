package com.ncsts.view.bean;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.richfaces.component.html.HtmlDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.ncsts.common.CacheManager;
import com.ncsts.domain.Cust;
import com.ncsts.domain.CustCert;
import com.ncsts.domain.CustEntity;
import com.ncsts.domain.CustEntityPK;
import com.ncsts.domain.CustLocn;
import com.ncsts.domain.CustLocnEx;
import com.ncsts.domain.EntityItem;
import com.ncsts.domain.EntityLevel;
import com.ncsts.domain.Jurisdiction;
import com.ncsts.domain.ListCodes;
import com.ncsts.domain.User;
import com.ncsts.dto.EntityItemDTO;
import com.ncsts.jsf.model.CustCertDataModel;
import com.ncsts.services.CustCertService;
import com.ncsts.services.CustLocnService;
import com.ncsts.services.CustService;
import com.ncsts.services.EntityItemService;
import com.ncsts.services.EntityLevelService;
import com.ncsts.services.JurisdictionService;
import com.ncsts.services.ListCodesService;
import com.ncsts.services.OptionService;
import com.ncsts.view.util.TogglePanelController;

public class ExemptionCertificateBean implements SearchJurisdictionHandler.SearchJurisdictionCallback {	
	private Logger logger = Logger.getLogger(ExemptionCertificateBean.class);
	
	@Autowired
	private MatrixCommonBean matrixCommonBean;
	
	@Autowired
	private CustomerLocationLookupBean customerLocationLookupBean;

	private Cust filterCust = new Cust();
	private CustCert filterCustCert = new CustCert();
	private CustCert selCustCert = new CustCert();
	private CustCert updateCustCert = new CustCert();
	private CustLocn updateCustLocn = new CustLocn();
	
	private List<SelectItem> filterStateMenuItems;
	private List<SelectItem> filterCountryMenuItems;
	private List<SelectItem> filterActiveCustomerMenuItems;
	private List<SelectItem> filterActiveLocationMenuItems;
	private List<SelectItem> filterLocnStateMenuItems;
	private List<SelectItem> filterLocnCountryMenuItems;
	private List<SelectItem> filterEntityMenuItems;
	private List<SelectItem> filterExemptTypeMenuItems;
	
	private String filterJurisdictionHolidayCode;
	private String filterJurisdictionCountry;
	private String filterJurisdictionState;
	private String filterJurisdictionTaxCode;
	private String filterJurisdictionCounty;
	private String filterJurisdictionCity;
	private boolean isEntityCheck=false;
	private List<SelectItem> entityLevelList = null;
	private Map<Long, String > entityLevelMap = null;
	boolean selectAll = false;
	private loginBean loginBean;
	
	private List<EntityItem> retrieveEntityItemList= new ArrayList<EntityItem>();
	private List<SelectItem> filterJurisdictionCountyMenuItems;
	private List<SelectItem> filterJurisdictionCityMenuItems;
	
	private CacheManager cacheManager;
	private CustCertDataModel custCertDataModel;
	
	private SimpleSelection selection = new SimpleSelection();
	private int selectedCustRowIndex = -1;
	private EditAction currentAction = EditAction.VIEW;
   
	private FilePreferenceBackingBean filePreferenceBean;
	private CustService custService;
	private CustLocnService custLocnService;
	private CustCertService custCertService;
	private EntityItemService entityItemService; 
	private ListCodesService listCodesService;
	private OptionService optionService;
	private JurisdictionService jurisdictionService;
	protected EntityLevelService entityLevelService;
	public String entityCode;
	
	private List<SelectItem> selEntityMenuItems;
	private List<SelectItem> selExemptTypeMenuItems;
	
	private List<SelectItem> updateExemptReasonMenuItems;
	private List<SelectItem> updateCertTypeMenuItems;
	
	private List<SelectItem> updateCertStateMenuItems;
	
	private Map<String, String > exemptionTypeMap = null;
	private Map<String, String > exemptReasonMap = null;
	
	private EntityItemDTO filterEntityItem = null;
	
	private SearchJurisdictionHandler filterHandler = new SearchJurisdictionHandler();
	
	private HtmlSelectOneMenu countyInput = new HtmlSelectOneMenu();	
	private HtmlSelectOneMenu cityInput = new HtmlSelectOneMenu();
	private EditExempAction currentExempAction = EditExempAction.VIEW;
	private Long exemptionCustomerSelected = 0L;
	private HtmlInputFileUpload uploadFile;
	private UploadedFile upFile;
	private String browserFilename = "";
	private TogglePanelController togglePanelController = null;
	private String showhide="";
	  
	enum EditExempAction {
		ADD,
		ADD_EXEMP,
		UPDATE_EXEMP,
		DELETE_EXEMP,
		VIEW_EXEMP,
		ADD_EXEMP_FROM_TRANSACTION,
		ADD_CERT,
		COPY_ADD_CERT,
		UPDATE_CERT,
		DELETE_CERT,
		VIEW_CERT,
		ADD_CERT_FROM_VALIDATE,
		COPY_ADD_FROM_TRANSACTION,
		UPDATE,
		VIEW,
		DELETE,
		START,
		IN_PROCESS,
		EXECUTE,
		FINISHED,
		VIEW_FROM_TRANSACTION,
		COPY_TO,
		DEFAULT,
		DELETE_ALL;
	}
	
	public  TogglePanelController getTogglePanelController(){
		if(togglePanelController==null){
			togglePanelController = new TogglePanelController();
			togglePanelController.addTogglePanel("CustomerInformationpanel", true);
			togglePanelController.addTogglePanel("LocationInformationPanel", false);
			togglePanelController.addTogglePanel("ExemptInformationPanel", false);
		}
		return togglePanelController;
	}
	
	public void setTogglePanelController(TogglePanelController togglePanelController){
		this.togglePanelController = togglePanelController;;
	}
	
	public void searchJurisdictionCallback() {
		//Reload cert states
		updateCertStateMenuItems = getCacheManager().createStateItems(filterHandler.getCountry());	
		updateCustCert.setCertState("");
	}
	
	public UploadedFile getUpFile(){
	  return upFile;
	}

	public void setUpFile(UploadedFile upFile){
		this.upFile = upFile;
	}

	public HtmlInputFileUpload getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(HtmlInputFileUpload uploadFile) {
		this.uploadFile = uploadFile;
	}
	
	public String getBrowserFilename() {
		return this.browserFilename;
	}

	public void setBrowserFilename(String browserFilename) {
		this.browserFilename = browserFilename;
	}
	
	public String viewFileAction(){
		boolean foundError = false; 
		
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

			
			if(updateCustCert.getCertImage()==null || updateCustCert.getCertImageUrl()==null || updateCustCert.getCertImageUrl().length()==0){
				FacesMessage msg = new FacesMessage("Certificate Image is not uploaded.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return null;
			}
			
			byte[] dataImage = updateCustCert.getCertImage();

	        response.setContentType("application/octet-stream");
	        response.setContentLength(dataImage.length);
	        response.setHeader("Content-disposition", "attachment; filename=\"" + updateCustCert.getCertImageUrl() + "\"");
	        ServletOutputStream out = response.getOutputStream();
	        out.write(dataImage);
	        out.flush();
	        out.close();
	        context.responseComplete();
		}catch(FileNotFoundException fe){
			FacesMessage msg = new FacesMessage("Path not found");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}catch(IOException e){
			logger.debug("Error reading file");
		}
		
		return null;

	}
	
	public void fileUploadAction() throws IOException {
		if (this.uploadFile.getUploadedFile() != null) {
			boolean foundError = false; // 4323 - see below
			
			try{
				InputStream istr = this.uploadFile.getUploadedFile().getInputStream();	
				BufferedInputStream bstr = new BufferedInputStream( istr );

				int contentLength = (int) this.uploadFile.getUploadedFile().getSize(); // get the file size (in bytes)
				byte[] data = new byte[contentLength]; // allocate byte array of right size
				bstr.read( data, 0, contentLength ); // read into byte array
				bstr.close();
				istr.close();
				
				updateCustCert.setCertImage(data);
				updateCustCert.setCertImageUrl(this.uploadFile.getUploadedFile().getName());
			}catch(FileNotFoundException fe){
				FacesMessage msg = new FacesMessage("Path not found");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}catch(IOException e){
				logger.debug("Error reading file");
			}
		}
		else{
			//"Certificate
			FacesMessage msg = new FacesMessage("Certificate Image is required.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}
	
	
	public String getActionExempText() {
		if(currentExempAction.equals(EditExempAction.ADD)){
			return "Add an Exemption";
		}
		else if(currentExempAction.equals(EditExempAction.ADD_EXEMP)){
			return "Add a Temporarily Exemption";
		}
		else if(currentExempAction.equals(EditExempAction.UPDATE_EXEMP)){
			return "Update a Temporarily Exemption";
		}
		else if(currentExempAction.equals(EditExempAction.DELETE_EXEMP)){
			return "Delete a Temporarily Exemption";
		}
		else if(currentExempAction.equals(EditExempAction.VIEW_EXEMP)){
			return "View a Temporarily Exemption";
		}
		else if(currentExempAction.equals(EditExempAction.ADD_EXEMP_FROM_TRANSACTION)){
			return "Add an Exemption From a Customer Location";
		}
		else if(currentExempAction.equals(EditExempAction.ADD_CERT)){
			return "Add a Certificate";
		}
		else if(currentExempAction.equals(EditExempAction.COPY_ADD_CERT)){
			return "Copy/Add a Certificate";
		}
		else if(currentExempAction.equals(EditExempAction.ADD_CERT_FROM_VALIDATE)){
			return "Add a Certificate";
		}
		else if(currentExempAction.equals(EditExempAction.UPDATE_CERT)){
			return "Update a Certificate";
		}
		else if(currentExempAction.equals(EditExempAction.DELETE_CERT)){
			return "Delete a Certificate";
		}
		else if(currentExempAction.equals(EditExempAction.VIEW_CERT)){
			return "View a Certificate";
		}
		else if(currentExempAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentExempAction.equals(EditAction.VIEW)){
			return "View";
		}

		return "";
	}
	
	public Boolean getIsViewOnly() {
		return (currentExempAction.equals(EditExempAction.DELETE_CERT)) || (currentExempAction.equals(EditExempAction.VIEW_CERT)) ||
				(currentExempAction.equals(EditExempAction.DELETE_EXEMP)) || (currentExempAction.equals(EditExempAction.VIEW_EXEMP));
	}
	
	public void customerSelectionChanged(ValueChangeEvent e) {
		exemptionCustomerSelected = (Long)e.getNewValue();
		
		if(selEntityMenuItems!=null){
			selEntityMenuItems.clear();
			selEntityMenuItems = null;
		}	
	}
	
	public Boolean getDisplayAddFromTransactionAction() {
		return (currentExempAction == EditExempAction.ADD_EXEMP_FROM_TRANSACTION);
	}
	
	public String addExempFromLocation(Long custId, Long custLocnId){
		this.currentExempAction = EditExempAction.ADD_EXEMP_FROM_TRANSACTION;
		
		updateCustCert= new CustCert();		
		exemptionCustomerSelected = custId;
		updateCustCert.setCustLocnId(custLocnId);
		
		updateCustCert.setEntityId(0L);
		updateCustCert.setExemptionTypeCode("");
		
		if(custId!=null){
			Cust aCust = custService.findById(custId);
			if(aCust!=null){
				updateCustCert.setCustName(aCust.getCustName());
				updateCustCert.setCustNbr(aCust.getCustNbr());
			}
		}
		
		if(custLocnId!=null){
			CustLocn aCustLocn = custLocnService.findById(custLocnId);
			if(aCustLocn!=null){
				updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
				updateCustCert.setCustLocnName(aCustLocn.getLocationName());
			}
		}
		
		if(selEntityMenuItems!=null){
			selEntityMenuItems.clear();
			selEntityMenuItems = null;
		}	
		getSelEntityMenuItems();
		
		return "exemption_update";
	}
	
	public String launchExemptCertFromLocation(Long custId, Long custLocnId){
		
		resetFilterSearchAction();
		
		if(custId!=null){
			Cust aCust = custService.findById(custId);
			if(aCust!=null){
				filterCustCert.setCustName(aCust.getCustName());
				filterCustCert.setCustNbr(aCust.getCustNbr());
			}
		}
		
		
		if(custLocnId!=null){
			CustLocn aCustLocn = custLocnService.findById(custLocnId);
			if(aCustLocn!=null){
				filterCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
				filterCustCert.setCustLocnName(aCustLocn.getLocationName());
			}
		}
		showhide="show";
		retrieveFilterCustCert();
		
		return "exemptionCertificate_main";
	}
	
	public void prepareAddValues(ActionEvent e) throws AbortProcessingException {
		String custNbr = filterCustCert.getCustNbr();
		String custLocnCode = filterCustCert.getCustLocnCode();

		if(entityCode!=null && entityCode.length()>0){
		EntityItem entityItem = entityItemService.findByCode(entityCode);
		Long entityId = entityItem.getEntityId();
		}
	}
	
	public String displayAddExempAction() {
		this.currentExempAction = EditExempAction.ADD;
		
		updateCustCert= new CustCert();		
		exemptionCustomerSelected = 0L;
		updateCustCert.setCustLocnId(0L);
		updateCustCert.setEntityId(0L);
		updateCustCert.setExemptionTypeCode("");
		
		if(selEntityMenuItems!=null){
			selEntityMenuItems.clear();
			selEntityMenuItems = null;
		}
		
		//Set default values if 
		String custNbr = filterCustCert.getCustNbr();
		String custLocnCode = filterCustCert.getCustLocnCode();
		//Long entityId = filterCustCert.getEntityId();
		
		updateCustCert.setCustNbr(custNbr); //No matter valid or not

		Cust newCust = null;
		if(!(custNbr==null || custNbr.length()==0)){
			newCust = custService.getCustByNumber(custNbr);
			
			if(newCust!=null){
				//Set customer values
				exemptionCustomerSelected = newCust.getCustId();
				updateCustCert.setCustNbr(newCust.getCustNbr());
				updateCustCert.setCustName(newCust.getCustName());
				
				//Set location values
				CustLocn newCustLocn = null;
				updateCustCert.setCustLocnCode(custLocnCode); //No matter valid or not
				if(!(custLocnCode==null || custLocnCode.length()==0) && newCust!=null){
					newCustLocn = custLocnService.getCustLocnByCustId(newCust.getCustId(), custLocnCode);
					
					if(newCustLocn!=null){
						updateCustCert.setCustLocnId(newCustLocn.getCustLocnId());
						updateCustCert.setCustLocnCode(newCustLocn.getCustLocnCode());
						updateCustCert.setCustLocnName(newCustLocn.getLocationName());
					}
				}
				if(entityCode!=null && entityCode.length()>0){
					EntityItem entityItemBycode = entityItemService.findByCode(entityCode);
					Long entityId = entityItemBycode.getEntityId();
					
				//Set entity value
				if(entityId!=null && entityId > 0L){			
					boolean foundOne = false;
					List<EntityItem> eList = entityItemService.findAllEntityItemsWithCustomer(exemptionCustomerSelected);//source list   
		    	    if(eList != null && eList.size() > 0){
		    	    	for(EntityItem entityItem:eList){
		    	    		
		    	    		if(entityItem.getEntityId().longValue() == entityId.longValue()){
		    	    			foundOne = true;
		    	    			break;
		    	    		}		
		    	    	}
		    	    }
		    	    
		    	    if(foundOne){
		    	    	updateCustCert.setEntityId(entityId);
						getSelEntityMenuItems();
		    	    }
				}
			}
		}
		}

		return "exemption_update";
	}
	
	public Boolean getIsOkEnabled() {
		return (currentExempAction == EditExempAction.ADD || currentExempAction == EditExempAction.ADD_EXEMP_FROM_TRANSACTION) && 
				(exemptionCustomerSelected!=null && exemptionCustomerSelected!=0L) &&
				(updateCustCert.getCustLocnId()!=null && updateCustCert.getCustLocnId()!=0L) && 
				(updateCustCert.getEntityId()!=null && updateCustCert.getEntityId()!=0L) &&
				(updateCustCert.getExemptionTypeCode()!=null && updateCustCert.getExemptionTypeCode().length()>0) ;
	}
	
	public Boolean getIsExemptionSelectionEnabled() {
		return (currentExempAction == EditExempAction.ADD || currentExempAction == EditExempAction.ADD_EXEMP_FROM_TRANSACTION) && 
				(exemptionCustomerSelected!=null && exemptionCustomerSelected!=0L) &&
				(updateCustCert.getCustLocnId()!=null && updateCustCert.getCustLocnId()!=0L) && 
				(updateCustCert.getEntityId()!=null && updateCustCert.getEntityId()!=0L);
	}
	
	public Long getExemptionCustomerSelected(){
		return exemptionCustomerSelected;
	}
	
	public void setExemptionCustomerSelected(Long exemptionCustomerSelected){
		this.exemptionCustomerSelected = exemptionCustomerSelected;
	}

	public Map<String, String >  getExemptionTypeMap(){
		if(exemptionTypeMap==null){
			exemptionTypeMap = new LinkedHashMap<String, String >();

			List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					exemptionTypeMap.put(listcodes.getCodeCode(), listcodes.getDescription());
				}
			}
	    }
		return exemptionTypeMap;
	}
	
	public Map<String, String >  getExemptReasonMap(){
		if(exemptReasonMap==null){
			exemptReasonMap = new LinkedHashMap<String, String >();

			List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXREASON");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					exemptReasonMap.put(listcodes.getCodeCode(), listcodes.getDescription());
				}
			}
	    }
		return exemptReasonMap;
	}
	
	public List<SelectItem> getUpdateCertStateMenuItems() {     
	   return updateCertStateMenuItems;
	}
	
	public List<SelectItem> getUpdateCertTypeMenuItems() {
		if(updateCertTypeMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select a Certificate Type"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("CERTTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			updateCertTypeMenuItems = selectItems;
			updateCustCert.setExemptReasonCode("");
		}
		
		return updateCertTypeMenuItems;
	}
	
	public List<SelectItem> getUpdateExemptReasonMenuItems() {
		if(updateExemptReasonMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select an Exemption Reason"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXREASON");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			updateExemptReasonMenuItems = selectItems;
			updateCustCert.setExemptReasonCode("");
		}
		
		return updateExemptReasonMenuItems;
	}
	
	public List<SelectItem> getSelExemptTypeMenuItems() {
		if(selExemptTypeMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","Select an Exemption Type"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
			selExemptTypeMenuItems = selectItems;
			updateCustCert.setExemptionTypeCode("");
		}
		
		return selExemptTypeMenuItems;
	}
	
	public List<SelectItem> getSelEntityMenuItems() {
		if(selEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(new Long(0L),"Select an Entity"));
	    	
	    	if(exemptionCustomerSelected!=null && exemptionCustomerSelected!=0L){
	    	
		    	List<EntityItem> eList = entityItemService.findAllEntityItemsWithCustomer(exemptionCustomerSelected);//source list   
	    	    if(eList != null && eList.size() > 0){
	    	    	for(EntityItem entityItem:eList){
	    	    		selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() + " - " + entityItem.getEntityName()));		
	    	    	}
	    	    } 	  
	    	}   	
	    	selEntityMenuItems = selectItems;
		}
 
		return selEntityMenuItems;
	}
	
	public String displayAddExempCertAction(){
		String result = null;
		
		if(!isValidSelection()){
			return null;
		}
		
		if(updateCustCert.getExemptionTypeCode()!=null && updateCustCert.getExemptionTypeCode().equals("1")){
			result = displayAddExemptionAction();
		}
		else if(updateCustCert.getExemptionTypeCode()!=null && updateCustCert.getExemptionTypeCode().equals("2")){
			result = displayAddCertificateAction();
		}
		else{
			result=null;
		}
		
		return result;
	}
	
	public String displayValidateExempCertAction() {

		currentExempAction = EditExempAction.ADD_CERT_FROM_VALIDATE;
			
		Cust aCust = custService.findById(selCustCert.getCustId());
		CustLocn aCustLocn = custLocnService.findById(selCustCert.getCustLocnId());
		EntityItem aEntityItem = entityItemService.findById(selCustCert.getEntityId());
		
		if(aCust!=null){
			updateCustCert.setCustNbr(aCust.getCustNbr());
			updateCustCert.setCustName(aCust.getCustName());
		}
		
		if(aCustLocn!=null){
			updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
			updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		}
		
		if(aEntityItem!=null){
			updateCustCert.setEntityCode(aEntityItem.getEntityCode());
			updateCustCert.setEntityName(aEntityItem.getEntityName());
		}
		
		if (updateCertStateMenuItems != null) {
			updateCertStateMenuItems.clear();
			updateCertStateMenuItems = null;
		}
		
		updateCustCert.setCustLocnExId(selCustCert.getCustLocnExId());
		updateCustCert.setExemptionTypeCode("2");
		
		updateCertStateMenuItems = getCacheManager().createStateItems(aCustLocn.getCountry());	
		updateCustCert.setCountry(aCustLocn.getCountry());
		updateCustCert.setCertState("");
		
		updateCustCert.setEffectiveDate(selCustCert.getEffectiveDate());
		updateCustCert.setExpirationDate(selCustCert.getExpirationDate());
		updateCustCert.setExemptReasonCode(selCustCert.getExemptReasonCode());
		updateCustCert.setCertTypeCode("");
		updateCustCert.setCountryFlag(selCustCert.getCountryFlag());
		updateCustCert.setStateFlag(selCustCert.getStateFlag());
		updateCustCert.setCountyFlag(selCustCert.getCountyFlag());
		updateCustCert.setCityFlag(selCustCert.getCityFlag());
		updateCustCert.setStj1Flag(selCustCert.getStj1Flag());
		updateCustCert.setStj2Flag(selCustCert.getStj2Flag());
		updateCustCert.setStj3Flag(selCustCert.getStj3Flag());
		updateCustCert.setStj4Flag(selCustCert.getStj4Flag());
		updateCustCert.setStj5Flag(selCustCert.getStj5Flag());
		updateCustCert.setActiveFlag("1");
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		filterHandler.setCallback(this);
		resetFilter();
		
		filterHandler.setCountry(updateCustCert.getCountry());
		
		return "certificate_update";
	}
	
	public String displayCopyAddExempCertAction(){
		String result = null;
		
		if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("2")){
			result = displayCopyAddCertificateAction(); //Certificate only
		}
		else{
			result=null;
		}
		
		return result;
	}
	
	
	public String displayUpdateExempCertAction(){
		String result = null;
		
		if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("1")){ //Temporary Exemption
			result = displayUpdateExemptionAction();
		}
		else if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("2")){
			result = displayUpdateCertificateAction();
		}
		else{
			result=null;
		}
		
		return result;
	}
	
	public String displayDeleteExempCertAction(){
		String result = null;
		
		if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("1")){ //Temporary Exemption
			result = displayDeleteExemptionAction();
		}
		else if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("2")){
			result = displayDeleteCertificateAction();
		}
		else{
			result=null;
		}
		
		return result;
	}
	
	public String displayViewExempCertAction(){
		String result = null;
		
		if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("1")){ //Temporary Exemption
			result = displayViewExemptionAction();
		}
		else if(selCustCert!=null && selCustCert.getExemptionTypeCode().equals("2")){
			result = displayViewCertificateAction();
		}
		else{
			result=null;
		}
		
		return result;
	}
	
	public String okCustCertAction() {
		String result = null;
		
		switch (currentExempAction) {
			case DELETE:
				result = deleteCustAction();
				break;
				
			case UPDATE:
				result = updateCustAction();
				break;
				
			case ADD:
				
				break;
				
			case VIEW:
				result = "customerLocation_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	//Displaying Exemption
	public String displayAddExemptionAction() {
		currentExempAction = EditExempAction.ADD_EXEMP;
			
		Cust aCust = custService.findById(exemptionCustomerSelected);
		CustLocn aCustLocn = custLocnService.findById(updateCustCert.getCustLocnId());
		EntityItem aEntityItem = entityItemService.findById(updateCustCert.getEntityId());
		
		if(aCust!=null){
			updateCustCert.setCustNbr(aCust.getCustNbr());
			updateCustCert.setCustName(aCust.getCustName());
		}
		
		if(aCustLocn!=null){
			updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
			updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		}
		
		if(aEntityItem!=null){
			updateCustCert.setEntityCode(aEntityItem.getEntityCode());
			updateCustCert.setEntityName(aEntityItem.getEntityName());
		}
		
		
		Long graceDays = aEntityItem.getExemptGraceDays();
		int addDays = 30;//default days
		if(graceDays!=null){
			addDays = graceDays.intValue();
		}
		
		updateCustCert.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
	  
		Calendar cal = Calendar.getInstance();   
		cal.setTime(updateCustCert.getEffectiveDate());   
		cal.add(Calendar.DATE, addDays);   
		
		updateCustCert.setExpirationDate(cal.getTime());
		
		updateCustCert.setExemptReasonCode("");
		
		updateCustCert.setCountryBooleanFlag(true);
		updateCustCert.setStateBooleanFlag(true);
		updateCustCert.setCountyBooleanFlag(true);
		updateCustCert.setCityBooleanFlag(true);
		updateCustCert.setStj1BooleanFlag(true);
		updateCustCert.setStj2BooleanFlag(true);
		updateCustCert.setStj3BooleanFlag(true);
		updateCustCert.setStj4BooleanFlag(true);
		updateCustCert.setStj5BooleanFlag(true);
		updateCustCert.setActiveFlag("1");
		
		return "temp_exemption_update";
	}
	
	public String displayUpdateExemptionAction() {
		currentExempAction = EditExempAction.UPDATE_EXEMP;
		
		Cust aCust = custService.findById(selCustCert.getCustId());
		CustLocn aCustLocn = custLocnService.findById(selCustCert.getCustLocnId());
		EntityItem aEntityItem = entityItemService.findById(selCustCert.getEntityId());
		
		updateCustCert = new CustCert();
		updateCustCert.setCustId(selCustCert.getCustId());
		updateCustCert.setCustLocnId(selCustCert.getCustLocnId());
		updateCustCert.setEntityId(selCustCert.getEntityId());
		
		if(aCust!=null){
			updateCustCert.setCustNbr(aCust.getCustNbr());
			updateCustCert.setCustName(aCust.getCustName());
		}
		
		if(aCustLocn!=null){
			updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
			updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		}
		
		if(aEntityItem!=null){
			updateCustCert.setEntityCode(aEntityItem.getEntityCode());
			updateCustCert.setEntityName(aEntityItem.getEntityName());
		}
		
		CustLocnEx custLocnEx = custCertService.findCustLocnEx(selCustCert.getCustLocnExId());
		updateCustCert.setCustLocnExId(custLocnEx.getCustLocnExId());
		updateCustCert.setCustLocnId(custLocnEx.getCustLocnId());
		updateCustCert.setEntityId(custLocnEx.getEntityId());
		updateCustCert.setExemptionTypeCode(custLocnEx.getExemptionTypeCode());
		updateCustCert.setEffectiveDate(custLocnEx.getEffectiveDate());
		updateCustCert.setExpirationDate(custLocnEx.getExpirationDate());
		updateCustCert.setExemptReasonCode(custLocnEx.getExemptReasonCode());
		updateCustCert.setCountryFlag(custLocnEx.getCountryFlag());
		updateCustCert.setStateFlag(custLocnEx.getStateFlag());
		updateCustCert.setCountyFlag(custLocnEx.getCountyFlag());
		updateCustCert.setCityFlag(custLocnEx.getCityFlag());
		
		updateCustCert.setStj1Flag(custLocnEx.getStj1Flag());
		updateCustCert.setStj2Flag(custLocnEx.getStj2Flag());
		updateCustCert.setStj3Flag(custLocnEx.getStj3Flag());
		updateCustCert.setStj4Flag(custLocnEx.getStj4Flag());
		updateCustCert.setStj5Flag(custLocnEx.getStj5Flag());
		updateCustCert.setActiveFlag(custLocnEx.getActiveFlag());
		updateCustCert.setUpdateUserId(custLocnEx.getUpdateUserId());
		updateCustCert.setUpdateTimestamp(custLocnEx.getUpdateTimestamp());
		
		return "temp_exemption_update";
	}
	
	public String displayDeleteExemptionAction() {
		displayUpdateExemptionAction();
		
		currentExempAction = EditExempAction.DELETE_EXEMP;
		
		return "temp_exemption_update";
	}
	
	public String displayViewExemptionAction() {
		displayUpdateExemptionAction();
		
		currentExempAction = EditExempAction.VIEW_EXEMP;
		
		return "temp_exemption_update";
	}
	
	//Displaying Certificate
	public String displayAddCertificateAction() {

		currentExempAction = EditExempAction.ADD_CERT;
		
		uploadFile = new HtmlInputFileUpload();
			
		Cust aCust = custService.findById(exemptionCustomerSelected);
		CustLocn aCustLocn = custLocnService.findById(updateCustCert.getCustLocnId());
		EntityItem aEntityItem = entityItemService.findById(updateCustCert.getEntityId());
		
		if(aCust!=null){
			updateCustCert.setCustNbr(aCust.getCustNbr());
			updateCustCert.setCustName(aCust.getCustName());
		}
		
		if(aCustLocn!=null){
			updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
			updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		}
		
		if(aEntityItem!=null){
			updateCustCert.setEntityCode(aEntityItem.getEntityCode());
			updateCustCert.setEntityName(aEntityItem.getEntityName());
		}
		
		if (updateCertStateMenuItems != null) {
			updateCertStateMenuItems.clear();
			updateCertStateMenuItems = null;
		}
		
		updateCertStateMenuItems = getCacheManager().createStateItems(aCustLocn.getCountry());	
		updateCustCert.setCountry(aCustLocn.getCountry());
		updateCustCert.setCertState("");
		
		
		/* leave it blank
		Long graceDays = aEntityItem.getExemptGraceDays();
		int addDays = 30; //default days
		if(graceDays!=null){
			addDays = graceDays.intValue();
		}
		*/
		
		updateCustCert.setEffectiveDate(new com.ncsts.view.util.DateConverter().getUserLocalDate());
		
		/*
		Calendar cal = Calendar.getInstance();   
		cal.setTime(updateCustCert.getEffectiveDate());   
		cal.add(Calendar.DATE, addDays);   
		
		updateCustCert.setExpirationDate(cal.getTime());
		*/
		
		updateCustCert.setExemptReasonCode("");
		updateCustCert.setCertTypeCode("");
		
		updateCustCert.setCountryBooleanFlag(true);
		updateCustCert.setStateBooleanFlag(true);
		updateCustCert.setCountyBooleanFlag(true);
		updateCustCert.setCityBooleanFlag(true);
		updateCustCert.setStj1BooleanFlag(true);
		updateCustCert.setStj2BooleanFlag(true);
		updateCustCert.setStj3BooleanFlag(true);
		updateCustCert.setStj4BooleanFlag(true);
		updateCustCert.setStj5BooleanFlag(true);
		updateCustCert.setActiveFlag("1");
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		filterHandler.setCallback(this);
		resetFilter();
		
		filterHandler.setCountry(updateCustCert.getCountry());
		
		return "certificate_update";
	}
	
	public String displayCopyAddCertificateAction(){
		displayUpdateCertificateAction();

		currentExempAction = EditExempAction.COPY_ADD_CERT;
		
		updateCustCert.setCustCertId(null);
		updateCustCert.setCertNbr("");
			
		return "certificate_update";
	}
	
	public String displayUpdateCertificateAction() {

		currentExempAction = EditExempAction.UPDATE_CERT;
		uploadFile = new HtmlInputFileUpload();
		
		//selCustCert
		Cust aCust = custService.findById(selCustCert.getCustId());
		CustLocn aCustLocn = custLocnService.findById(selCustCert.getCustLocnId());
		EntityItem aEntityItem = entityItemService.findById(selCustCert.getEntityId());
		
		updateCustCert = new CustCert();
		CustCert aCustCert = custCertService.findById(selCustCert.getTrueCustCertId());//True CustCertId
		BeanUtils.copyProperties(aCustCert, updateCustCert);
		
		if(aCust!=null){
			updateCustCert.setCustNbr(aCust.getCustNbr());
			updateCustCert.setCustName(aCust.getCustName());
		}
		
		if(aCustLocn!=null){
			updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
			updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		}
		
		if(aEntityItem!=null){
			updateCustCert.setEntityCode(aEntityItem.getEntityCode());
			updateCustCert.setEntityName(aEntityItem.getEntityName());
		}
		
		if (updateCertStateMenuItems != null) {
			updateCertStateMenuItems.clear();
			updateCertStateMenuItems = null;
		}
		
		//Card code ExemptionTypeCode
		updateCustCert.setExemptionTypeCode("2");
		
		updateCertStateMenuItems = getCacheManager().createStateItems(updateCustCert.getCountry());	
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		filterHandler.setCallback(this);
		resetFilter();
		filterHandler.setGeocode(updateCustCert.getGeocode());
		filterHandler.setCountry(updateCustCert.getCountry());
		filterHandler.setState(updateCustCert.getState());
		filterHandler.setCounty(updateCustCert.getCounty());
		filterHandler.setCity(updateCustCert.getCity());
		filterHandler.setZip(updateCustCert.getZip());
		
		return "certificate_update";
	}
	
	public String displayDeleteCertificateAction() {
		displayUpdateCertificateAction();

		currentExempAction = EditExempAction.DELETE_CERT;
			
		return "certificate_update";
	}
	
	public String displayViewCertificateAction() {
		displayUpdateCertificateAction();

		currentExempAction = EditExempAction.VIEW_CERT;
		
		return "certificate_update";
	}
	
	public String okExemptionAction() {
		String result = null;
		
		switch (currentExempAction) {

			case ADD_EXEMP:
				result = processAddExemptionAction();
				break;
				
			case ADD_EXEMP_FROM_TRANSACTION:
				result = processAddExemptionAction();
				break;
				
			case UPDATE_EXEMP:
				result = processUpdateExemptionAction();
				break;
				
			case DELETE_EXEMP:
				result = processDeleteExemptionAction();
				break;
				
			case VIEW_EXEMP:
				result = "exemptionCertificate_main";
				break;
				
			default:
				result = "exemptionCertificate_main";
				break;
		}
		return result;
	}

	public String processAddExemptionAction() {
		String result = null;
		try {
			String errorMessage = "";
			
			Date effectTS = (Date)updateCustCert.getEffectiveDate();
			if (effectTS==null) {  
				errorMessage = errorMessage + " " + "The Effective Date is mandatory.";
				
				FacesMessage message = new FacesMessage("The Effective Date is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			if(this.updateCustCert.getExpirationDate() == null) {
				errorMessage = errorMessage + " " + "Expiration Date is mandatory.";
				
				FacesMessage message = new FacesMessage("Expiration Date is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			// Expiration Date may not be earlier than Effective Date.
			if(this.updateCustCert.getEffectiveDate()!=null && this.updateCustCert.getExpirationDate()!=null){
				
				if(this.updateCustCert.getExpirationDate().getTime() < this.updateCustCert.getEffectiveDate().getTime()) {
					errorMessage = errorMessage + " " + "Expiration Date may not be earlier than Effective Date";
					
					FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				
				
				//8.1 Expiration Date not > Effective Date + Grace Days
				EntityItem aEntityItem = entityItemService.findById(updateCustCert.getEntityId());
				Long graceDays = aEntityItem.getExemptGraceDays();
				int addDays = 30;//default days
				if(graceDays!=null){
					addDays = graceDays.intValue();
				}
				
				Calendar cal = Calendar.getInstance();   
				cal.setTime(updateCustCert.getEffectiveDate());   
				cal.add(Calendar.DATE, addDays);   

				if(updateCustCert.getExpirationDate().getTime() > cal.getTime().getTime()){
					errorMessage = errorMessage + " " + "The Expiration Date may not be after the Effective Date + the number of Grace Days.";
				
					FacesMessage message = new FacesMessage("The Expiration Date may not be after the Effective Date + the number of Grace Days.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				
			}
			
			if(updateCustCert.getExemptReasonCode()==null || updateCustCert.getExemptReasonCode().trim().length()==0){
				errorMessage = errorMessage + " " + "An Exempt Reason must be selected.";
				
				FacesMessage message = new FacesMessage("An Exempt Reason must be selected.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			if(!updateCustCert.getCountryBooleanFlag() && !updateCustCert.getStateBooleanFlag() && !updateCustCert.getCountyBooleanFlag() &&
					!updateCustCert.getCityBooleanFlag() && !updateCustCert.getStj1BooleanFlag() && !updateCustCert.getStj2BooleanFlag() && 
					!updateCustCert.getStj3BooleanFlag() && !updateCustCert.getStj4BooleanFlag() && !updateCustCert.getStj5BooleanFlag()){
				errorMessage = errorMessage + " " + "At least one Exempt In jurisdiction level must be checked.";
				
				FacesMessage message = new FacesMessage("At least one Exempt In Jurisdiction level must be checked.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}

			if(errorMessage!=null && errorMessage.length()>0){
				return "";
			}
			
			CustLocnEx aCustLocnEx = new CustLocnEx();
			aCustLocnEx.setCustLocnId(updateCustCert.getCustLocnId());
			aCustLocnEx.setEntityId(updateCustCert.getEntityId());
			aCustLocnEx.setExemptionTypeCode(updateCustCert.getExemptionTypeCode());
			aCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			aCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			aCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			aCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			aCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			aCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			aCustLocnEx.setCityFlag(updateCustCert.getCityFlag());
			
			aCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			aCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			aCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			aCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			aCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());
			
			aCustLocnEx.setActiveFlag(updateCustCert.getActiveFlag());	
			
			//Update Cust and CustEntity
			custCertService.addCustLocnEx(aCustLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String processUpdateExemptionAction() {

		String result = null;
		try {
			String errorMessage = "";
			
			Date effectTS = (Date)updateCustCert.getEffectiveDate();
			if (effectTS==null) {  
				errorMessage = errorMessage + " " + "The Effective Date is mandatory.";
				
				FacesMessage message = new FacesMessage("The Effective Date is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			if(this.updateCustCert.getExpirationDate() == null) {
				errorMessage = errorMessage + " " + "Expiration Date is mandatory.";
				
				FacesMessage message = new FacesMessage("Expiration Date is mandatory.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			// Expiration Date may not be earlier than Effective Date.
			if(this.updateCustCert.getEffectiveDate()!=null && this.updateCustCert.getExpirationDate()!=null){
				
				if(this.updateCustCert.getExpirationDate().getTime() < this.updateCustCert.getEffectiveDate().getTime()) {
					errorMessage = errorMessage + " " + "Expiration Date may not be earlier than Effective Date";
					
					FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				
				
				//8.1 Expiration Date not > Effective Date + Grace Days
				EntityItem aEntityItem = entityItemService.findById(updateCustCert.getEntityId());
				Long graceDays = aEntityItem.getExemptGraceDays();
				int addDays = 30;//default days
				if(graceDays!=null){
					addDays = graceDays.intValue();
				}
				
				Calendar cal = Calendar.getInstance();   
				cal.setTime(updateCustCert.getEffectiveDate());   
				cal.add(Calendar.DATE, addDays);   

				if(updateCustCert.getExpirationDate().getTime() > cal.getTime().getTime()){
					errorMessage = errorMessage + " " + "The Expiration Date may not be after the Effective Date + the number of Grace Days.";
				
					FacesMessage message = new FacesMessage("The Expiration Date may not be after the Effective Date + the number of Grace Days.");
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
				
			}
			
			if(updateCustCert.getExemptReasonCode()==null || updateCustCert.getExemptReasonCode().trim().length()==0){
				errorMessage = errorMessage + " " + "An Exempt Reason must be selected.";
				
				FacesMessage message = new FacesMessage("An Exempt Reason must be selected.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
			
			if(!updateCustCert.getCountryBooleanFlag() && !updateCustCert.getStateBooleanFlag() && !updateCustCert.getCountyBooleanFlag() &&
					!updateCustCert.getCityBooleanFlag() && !updateCustCert.getStj1BooleanFlag() && !updateCustCert.getStj2BooleanFlag() && 
					!updateCustCert.getStj3BooleanFlag() && !updateCustCert.getStj4BooleanFlag() && !updateCustCert.getStj5BooleanFlag()){
				errorMessage = errorMessage + " " + "At least one Exempt In jurisdiction level must be checked.";
				
				FacesMessage message = new FacesMessage("At least one Exempt In Jurisdiction level must be checked.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}

			if(errorMessage!=null && errorMessage.length()>0){
				return "";
			}
			
			CustLocnEx aCustLocnEx = new CustLocnEx();
			
			aCustLocnEx.setCustLocnExId(updateCustCert.getCustLocnExId());
			
			aCustLocnEx.setCustLocnId(updateCustCert.getCustLocnId());
			aCustLocnEx.setEntityId(updateCustCert.getEntityId());
			aCustLocnEx.setExemptionTypeCode(updateCustCert.getExemptionTypeCode());
			aCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			aCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			aCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			aCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			aCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			aCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			aCustLocnEx.setCityFlag(updateCustCert.getCityFlag());
			
			aCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			aCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			aCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			aCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			aCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());
			
			aCustLocnEx.setActiveFlag(updateCustCert.getActiveFlag());	
			
			//Update Cust and CustEntity
			custCertService.updateCustLocnEx(aCustLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String processDeleteExemptionAction() {

		String result = null;
		try {
			CustLocnEx aCustLocnEx = new CustLocnEx();
			
			aCustLocnEx.setCustLocnExId(updateCustCert.getCustLocnExId());
			
			aCustLocnEx.setCustLocnId(updateCustCert.getCustLocnId());
			aCustLocnEx.setEntityId(updateCustCert.getEntityId());
			aCustLocnEx.setExemptionTypeCode(updateCustCert.getExemptionTypeCode());
			aCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			aCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			aCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			aCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			aCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			aCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			aCustLocnEx.setCityFlag(updateCustCert.getCityFlag());
			
			aCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			aCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			aCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			aCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			aCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());
			
			aCustLocnEx.setActiveFlag(updateCustCert.getActiveFlag());	
			
			//Update Cust and CustEntity
			custCertService.deleteCustLocnEx(aCustLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public void countryCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setCountryBooleanFlag(true);
			
			updateCustCert.setCountryBaseChgPct(null);
			updateCustCert.setCountrySpecialRate(null);
		}
		else {
			updateCustCert.setCountryBooleanFlag(false);
		}		
	}
	
	public Boolean getIsCountryCheckboxSelected() {
		return updateCustCert.getCountryBooleanFlag();
	}
	
	public void stateCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStateBooleanFlag(true);
			
			updateCustCert.setStateBaseChgPct(null);
			updateCustCert.setStateSpecialRate(null);
		}
		else {
			updateCustCert.setStateBooleanFlag(false);
		}		
	}
	
	public Boolean getIsStateCheckboxSelected() {
		return updateCustCert.getStateBooleanFlag();
	}
	
	public void countyCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setCountyBooleanFlag(true);
			
			updateCustCert.setCountyBaseChgPct(null);
			updateCustCert.setCountySpecialRate(null);
		}
		else {
			updateCustCert.setCountyBooleanFlag(false);
		}		
	}
	
	public Boolean getIsCountyCheckboxSelected() {
		return updateCustCert.getCountyBooleanFlag();
	}
	
	public void cityCheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setCityBooleanFlag(true);
			
			updateCustCert.setCityBaseChgPct(null);
			updateCustCert.setCitySpecialRate(null);
		}
		else {
			updateCustCert.setCityBooleanFlag(false);
		}		
	}
	
	public Boolean getIsCityCheckboxSelected() {
		return updateCustCert.getCityBooleanFlag();
	}
	
	public void stj1CheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStj1BooleanFlag(true);
			
			updateCustCert.setStj1BaseChgPct(null);
			updateCustCert.setStj1SpecialRate(null);
		}
		else {
			updateCustCert.setStj1BooleanFlag(false);
		}		
	}
	
	public Boolean getIsStj1CheckboxSelected() {
		return updateCustCert.getStj1BooleanFlag();
	}
	
	public void stj2CheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStj2BooleanFlag(true);
			
			updateCustCert.setStj2BaseChgPct(null);
			updateCustCert.setStj2SpecialRate(null);
		}
		else {
			updateCustCert.setStj2BooleanFlag(false);
		}		
	}
	
	public Boolean getIsStj2CheckboxSelected() {
		return updateCustCert.getStj2BooleanFlag();
	}
	
	public void stj3CheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStj3BooleanFlag(true);
			
			updateCustCert.setStj3BaseChgPct(null);
			updateCustCert.setStj3SpecialRate(null);
		}
		else {
			updateCustCert.setStj3BooleanFlag(false);
		}		
	}
	
	public Boolean getIsStj3CheckboxSelected() {
		return updateCustCert.getStj3BooleanFlag();
	}
	
	public void stj4CheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStj4BooleanFlag(true);
			
			updateCustCert.setStj4BaseChgPct(null);
			updateCustCert.setStj4SpecialRate(null);
		}
		else {
			updateCustCert.setStj4BooleanFlag(false);
		}		
	}
	
	public Boolean getIsStj4CheckboxSelected() {
		return updateCustCert.getStj4BooleanFlag();
	}
	
	public void stj5CheckboxSelected(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			updateCustCert.setStj5BooleanFlag(true);
			
			updateCustCert.setStj5BaseChgPct(null);
			updateCustCert.setStj5SpecialRate(null);
		}
		else {
			updateCustCert.setStj5BooleanFlag(false);
		}		
	}
	
	public Boolean getIsStj5CheckboxSelected() {
		return updateCustCert.getStj5BooleanFlag();
	}
	
	private boolean validateCertData(){
		boolean errorMessage = false;
		
		//1.	Certificate Number is mandatory.
		if(updateCustCert.getCertNbr()==null || updateCustCert.getCertNbr().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Certificate Number is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//2.	An Exempt Reason must be selected.
		if(updateCustCert.getExemptReasonCode()==null || updateCustCert.getExemptReasonCode().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("An Exempt Reason must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//3.	Invoice Number may not be entered when Blanket Cert is checked.
		if(updateCustCert.getInvoiceNbr().trim().length()>0 && updateCustCert.getBlanketBooleanFlag()){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Invoice Number may not be entered when Blanket Cert is checked.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		
		//4.	Blanket Cert checkbox may not be checked when an Invoice Number is entered.
		
		
		//6.	Effective Date is mandatory.
		if(this.updateCustCert.getEffectiveDate() == null) {
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Effective Date is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		
		//7.	Expiration Date is mandatory.
		if(this.updateCustCert.getExpirationDate() == null) {
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Expiration Date is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//8.	Expiration Date may not be earlier than Effective Date.
		if(this.updateCustCert.getEffectiveDate()!=null && this.updateCustCert.getExpirationDate()!=null){
			
			if(this.updateCustCert.getExpirationDate().getTime() < this.updateCustCert.getEffectiveDate().getTime()) {
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("Expiration Date may not be earlier than Effective Date");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}	
		}
			
		//9.	Name on Certificate is mandatory.
		if(updateCustCert.getCertCustName() ==null || updateCustCert.getCertCustName().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Name on Certificate is mandatory.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//10.	A Certificate State must be selected.
		if(updateCustCert.getCertState()==null || updateCustCert.getCertState().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("A Certificate State must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		
		//5.	The Certificate Number, Exempt Reason, Invoice Number, Effective Date, and Certificate State  must be unique.
		if((updateCustCert.getCertNbr()!=null && updateCustCert.getCertNbr().trim().length()>0) &&
		   (updateCustCert.getExemptReasonCode()!=null && updateCustCert.getExemptReasonCode().trim().length()>0) &&
		   (updateCustCert.getEffectiveDate()!= null) && 
		   (updateCustCert.getCertState()!=null && updateCustCert.getCertState().trim().length()>0)){
			
			//Invoice InvoiceNbr could be blank.			
			if(!custCertService.validateUniqueCertificate(updateCustCert)){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("The Certificate Number, Exempt Reason, Invoice Number, Effective Date, and Certificate State  must be unique.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		
		//11.	A Certificate Type must be selected.
		if(updateCustCert.getCertTypeCode()==null || updateCustCert.getCertTypeCode().trim().length()==0){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("A Certificate Type must be selected.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//12.	At least one Exempt In jurisdiction level must be checked.
		if(!updateCustCert.getCountryBooleanFlag() && !updateCustCert.getStateBooleanFlag() && !updateCustCert.getCountyBooleanFlag() &&
				!updateCustCert.getCityBooleanFlag() && !updateCustCert.getStj1BooleanFlag() && !updateCustCert.getStj2BooleanFlag() && 
				!updateCustCert.getStj3BooleanFlag() && !updateCustCert.getStj4BooleanFlag() && !updateCustCert.getStj5BooleanFlag()){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("At least one Exempt In jurisdiction level must be checked.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		//13.	Percent of Base and Special Rate may only be entered for jurisdictions that the Exempt In is unchecked.
		
		//14.	Percent of Base and Special Rate are mutually exclusive and only one may be entered per jurisdictional level.
		boolean mutuallyExclusiveError = false;
		if(!updateCustCert.getCountryBooleanFlag()){
			if(updateCustCert.getCountryBaseChgPct()!=null && updateCustCert.getCountryBaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getCountrySpecialRate()!=null &&   updateCustCert.getCountrySpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStateBooleanFlag()){
			if(updateCustCert.getStateBaseChgPct()!=null && updateCustCert.getStateBaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStateSpecialRate()!=null &&   updateCustCert.getStateSpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getCountyBooleanFlag()){
			if(updateCustCert.getCountyBaseChgPct()!=null && updateCustCert.getCountyBaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getCountySpecialRate()!=null &&   updateCustCert.getCountySpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getCityBooleanFlag()){
			if(updateCustCert.getCityBaseChgPct()!=null && updateCustCert.getCityBaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getCitySpecialRate()!=null &&   updateCustCert.getCitySpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStj1BooleanFlag()){
			if(updateCustCert.getStj1BaseChgPct()!=null && updateCustCert.getStj1BaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStj1SpecialRate()!=null &&   updateCustCert.getStj1SpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStj2BooleanFlag()){
			if(updateCustCert.getStj2BaseChgPct()!=null && updateCustCert.getStj2BaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStj2SpecialRate()!=null &&   updateCustCert.getStj2SpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStj3BooleanFlag()){
			if(updateCustCert.getStj3BaseChgPct()!=null && updateCustCert.getStj3BaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStj3SpecialRate()!=null &&   updateCustCert.getStj3SpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStj4BooleanFlag()){
			if(updateCustCert.getStj4BaseChgPct()!=null && updateCustCert.getStj4BaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStj4SpecialRate()!=null &&   updateCustCert.getStj4SpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(!updateCustCert.getStj5BooleanFlag()){
			if(updateCustCert.getStj5BaseChgPct()!=null && updateCustCert.getStj5BaseChgPct().doubleValue()!=0.0d &&        
					updateCustCert.getStj5SpecialRate()!=null &&   updateCustCert.getStj5SpecialRate().doubleValue()!=0.0d){
				mutuallyExclusiveError = true;
			}
		}
		
		if(mutuallyExclusiveError){
			errorMessage = true;
			
			FacesMessage message = new FacesMessage("Percent of Base and Special Rate are mutually exclusive and only one may be entered per Jurisdictional level.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		//Validate full and partial Jurisdiction		
		boolean validJurisdiction = false;
		Jurisdiction aJurisdiction = null;
		if(filterHandler.getHasSomeInput()){
			aJurisdiction = filterHandler.getJurisdictionFromDB();
			if(aJurisdiction!=null){
				validJurisdiction = true;
			}
			else{
				validJurisdiction = filterHandler.getIsValidPartialJurisdiction();
			}
			
			if(!validJurisdiction){
				errorMessage = true;
				
				FacesMessage message = new FacesMessage("An Invalid Jurisdiction was entered.");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		
		return errorMessage;
	}
	
	public String okCertificateAddAction() {
		String result = null;
		
		switch (currentExempAction) {

			case ADD_CERT:
			case COPY_ADD_CERT:
				result = processOkAddCertificateAction();
				break;
				
			default:
				result = "exemptionCertificate_main";
				break;
		}
		return result;
	}
	
	public String okCertificateAction() {
		String result = null;
		
		switch (currentExempAction) {

			case ADD_CERT:
			case COPY_ADD_CERT:
				result = processAddCertificateAction();
				break;
				
			case ADD_CERT_FROM_VALIDATE:
				result = processValidateCertificateAction();
				break;
				
			case UPDATE_CERT:
				result = processUpdateCertificateAction();
				break;
				
			case DELETE_CERT:
				result = processDeleteCertificateAction();
				break;
				
			case VIEW_CERT:
				result = "exemptionCertificate_main";
				break;
				
			default:
				result = "exemptionCertificate_main";
				break;
		}
		return result;
	}
	
	public String processValidateCertificateAction(){
		String result = null;
		try {
			if(validateCertData()){
				return "";
			}
			
			//Add CustLocn
			updateCustCert.setGeocode(filterHandler.getGeocode());		
			updateCustCert.setCountry(filterHandler.getCountry());
			updateCustCert.setState(filterHandler.getState());
			updateCustCert.setCity(filterHandler.getCity());
			updateCustCert.setCounty(filterHandler.getCounty());
			updateCustCert.setZip(filterHandler.getZip());
			
			CustLocnEx custLocnEx = custCertService.findCustLocnEx(selCustCert.getCustLocnExId());
			//custLocnEx.setExemptionTypeCode("2");//Certificate
			custLocnEx.setActiveFlag("V");
			
			//Update Cust and CustEntity
			custCertService.validateCustCertExemp(updateCustCert, custLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String processOkAddCertificateAction(){
		String result = processAddCertificateAction();
		if(result==null || result.length()==0){
			return "";
		}
		
		currentExempAction = EditExempAction.COPY_ADD_CERT;
		updateCustCert.setCustCertId(null);
		updateCustCert.setCertNbr("");
			
		return "certificate_update";
	}

	public String processAddCertificateAction() {

		String result = null;
		try {
			if(validateCertData()){
				return "";
			}
			
			//Add CustLocn
			updateCustCert.setGeocode(filterHandler.getGeocode());		
			updateCustCert.setCountry(filterHandler.getCountry());
			updateCustCert.setState(filterHandler.getState());
			updateCustCert.setCity(filterHandler.getCity());
			updateCustCert.setCounty(filterHandler.getCounty());
			updateCustCert.setZip(filterHandler.getZip());
			
			//Add CustLocnEx
			CustLocnEx aCustLocnEx = new CustLocnEx();
			aCustLocnEx.setCustLocnId(updateCustCert.getCustLocnId());
			aCustLocnEx.setEntityId(updateCustCert.getEntityId());
			aCustLocnEx.setExemptionTypeCode("2");//Certificate
			aCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			aCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			aCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			aCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			aCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			aCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			aCustLocnEx.setCityFlag(updateCustCert.getCityFlag());
			
			aCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			aCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			aCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			aCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			aCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());
			
			aCustLocnEx.setActiveFlag(updateCustCert.getActiveFlag());
				
			//Update Cust and CustEntity
			custCertService.addCustCertExemp(updateCustCert, aCustLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String processUpdateCertificateAction() {

		String result = null;
		try {
			if(validateCertData()){
				return "";
			}
			
			//Update CustLocn
			updateCustCert.setGeocode(filterHandler.getGeocode());		
			updateCustCert.setCountry(filterHandler.getCountry());
			updateCustCert.setState(filterHandler.getState());
			updateCustCert.setCity(filterHandler.getCity());
			updateCustCert.setCounty(filterHandler.getCounty());
			updateCustCert.setZip(filterHandler.getZip());
	
			CustLocnEx updateCustLocnEx = new CustLocnEx();
			updateCustLocnEx.setCustCertId(updateCustCert.getCustCertId());
			updateCustLocnEx.setEffectiveDate(updateCustCert.getEffectiveDate());
			updateCustLocnEx.setExpirationDate(updateCustCert.getExpirationDate());
			updateCustLocnEx.setExemptReasonCode(updateCustCert.getExemptReasonCode());
			updateCustLocnEx.setCountryFlag(updateCustCert.getCountryFlag());
			updateCustLocnEx.setStateFlag(updateCustCert.getStateFlag());
			updateCustLocnEx.setCountyFlag(updateCustCert.getCountyFlag());
			updateCustLocnEx.setCityFlag(updateCustCert.getCityFlag());	
			updateCustLocnEx.setStj1Flag(updateCustCert.getStj1Flag());
			updateCustLocnEx.setStj2Flag(updateCustCert.getStj2Flag());
			updateCustLocnEx.setStj3Flag(updateCustCert.getStj3Flag());
			updateCustLocnEx.setStj4Flag(updateCustCert.getStj4Flag());
			updateCustLocnEx.setStj5Flag(updateCustCert.getStj5Flag());

			custCertService.updateCustCertExemp(updateCustCert, updateCustLocnEx);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String processDeleteCertificateAction() {

		String result = null;
		try {
			
			custCertService.deleteCustCert(updateCustCert);

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "exemptionCertificate_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}

	public List<SelectItem> getFilterCountryMenuItems() {
		if(filterCountryMenuItems==null){
			filterCustCert.setCountry("");
		}
		filterCountryMenuItems = getCacheManager().createCountryItems();   
		return filterCountryMenuItems;
	}
	
	public List<SelectItem> getFilterLocnCountryMenuItems() {
		if(filterLocnCountryMenuItems==null){
			filterCustCert.setCustLocnCountry("");
		}
		filterLocnCountryMenuItems = getCacheManager().createCountryItems();   
		return filterLocnCountryMenuItems;
	}
	
	public List<SelectItem> getFilterStateMenuItems() { 
	   if (filterStateMenuItems == null) {
		   filterStateMenuItems = getCacheManager().createStateItems(filterCustCert.getCountry());
		   filterCustCert.setState("");
	   }	    
	   return filterStateMenuItems;
	}
	
	public List<SelectItem> getFilterLocnStateMenuItems() { 
	   if (filterLocnStateMenuItems == null) {
		   filterLocnStateMenuItems = getCacheManager().createStateItems(filterCustCert.getCustLocnCountry());
		   filterCustCert.setCustLocnState("");
	   }	    
	   return filterLocnStateMenuItems;
	}
	
	public void filterCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry==null) filterCustCert.setCountry("");
		else filterCustCert.setCountry(newCountry);
	
		if(filterStateMenuItems!=null) filterStateMenuItems.clear();

		filterStateMenuItems = getCacheManager().createStateItems(filterCustCert.getCountry());
		filterCustCert.setState("");
    }
	
	public void filterLocnCountryChanged(ValueChangeEvent e){
		String newCountry = (String)e.getNewValue();
		if(newCountry==null) filterCustCert.setCustLocnCountry("");
		else filterCustCert.setCustLocnCountry(newCountry);
	
		if(filterLocnStateMenuItems!=null) filterLocnStateMenuItems.clear();

		filterLocnStateMenuItems = getCacheManager().createStateItems(filterCustCert.getCustLocnCountry());
		filterCustCert.setCustLocnState("");
    }
	
	public List<SelectItem> getFilterActiveCustomerMenuItems() {
		if(filterActiveCustomerMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Customers"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));
	   
	    	filterActiveCustomerMenuItems = selectItems;
	    	
			filterCustCert.setCustActiveFlag("");
		}
 
		return filterActiveCustomerMenuItems;
	}
	
	public List<SelectItem> getFilterEntityMenuItems() {
		if(filterEntityMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem(0L,"All Entities"));

	    	List<EntityItem> eList = entityItemService.findAllEntityItems();//source list   
    	    if(eList != null && eList.size() > 0){
    	    	for(EntityItem entityItem:eList){
    	    		if(entityItem.getEntityId().intValue()!=0){
    	    			selectItems.add(new SelectItem(entityItem.getEntityId(), entityItem.getEntityCode() +" - "+ entityItem.getEntityName()));
    	    		}
    	    	}
    	    } 	
	    	
	    	filterEntityMenuItems = selectItems;
	    	
			filterCustCert.setEntityCode("");
		}
 
		return filterEntityMenuItems;
	}
	
	public List<SelectItem> getFilterExemptTypeMenuItems() {
		if(filterExemptTypeMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Exemptions"));

	    	List<ListCodes> listCodeList = cacheManager.getListCodesByType("EXTYPE");
			for (ListCodes listcodes : listCodeList) {
				if(listcodes != null && listcodes.getCodeTypeCode()!=null && listcodes.getDescription()!=null){
					selectItems.add(new SelectItem(listcodes.getCodeCode(), listcodes.getDescription()));			
				}
			}
	    	
	    	filterExemptTypeMenuItems = selectItems;
			filterCustCert.setExemptionTypeCode("");
		}
		
		return filterExemptTypeMenuItems;
	}
	
	public List<SelectItem> getFilterActiveLocationMenuItems() {
		if(filterActiveLocationMenuItems==null){
			List<SelectItem> selectItems = new ArrayList<SelectItem>();
	    	selectItems.add(new SelectItem("","All Locations"));
	    	selectItems.add(new SelectItem("1","Active Only"));
	    	selectItems.add(new SelectItem("0","Inactive Only"));
	   
	    	filterActiveLocationMenuItems = selectItems;
	    	
			filterCustCert.setCustLocnActiveFlag("");
		}
 
		return filterActiveLocationMenuItems;
	}
	
	
	public JurisdictionService getJurisdictionService() {
		return jurisdictionService;
	}

	public void setJurisdictionService(JurisdictionService jurisdictionService) {
		this.jurisdictionService = jurisdictionService;
	}	
	
	public void setFilterJurisdictionHolidayCode(String filterJurisdictionHolidayCode) {
		this.filterJurisdictionHolidayCode = filterJurisdictionHolidayCode;
	}
	
	public String getFilterJurisdictionHolidayCode() {
		return filterJurisdictionHolidayCode;
	}
	
	public void setFilterJurisdictionCountry(String filterJurisdictionCountry) {
		this.filterJurisdictionCountry = filterJurisdictionCountry;
	}
	
	public String getFilterJurisdictionCountry() {
		return filterJurisdictionCountry;
	}
	
	public void setFilterJurisdictionState(String filterJurisdictionState) {
		this.filterJurisdictionState = filterJurisdictionState;
	}
	
	public String getFilterJurisdictionState() {
		return filterJurisdictionState;
	}
	
	public void setFilterJurisdictionCounty(String filterJurisdictionCounty) {
		this.filterJurisdictionCounty = filterJurisdictionCounty;
	}
	
	public String getFilterJurisdictionCounty() {
		return filterJurisdictionCounty;
	}
	
	public void setFilterJurisdictionCity(String filterJurisdictionCity) {
		this.filterJurisdictionCity = filterJurisdictionCity;
	}
	
	public String getFilterJurisdictionCity() {
		return filterJurisdictionCity;
	}

	public void setFilterJurisdictionTaxCode(String filterJurisdictionTaxCode) {
		this.filterJurisdictionTaxCode = filterJurisdictionTaxCode;
	}
	
	public String getFilterJurisdictionTaxCode() {
		return filterJurisdictionTaxCode;
	}
	
	public EntityItemService getEntityItemService() {
    	return entityItemService;
    }
    
    public void setEntityItemService(EntityItemService entityItemService) {
    	this.entityItemService = entityItemService;
    }  
    
    public OptionService getOptionService() {
		return optionService;
	}

	public void setOptionService(OptionService optionService) {
		this.optionService = optionService;
	}

	public CustService getCustService() {
		return custService;
	}

	public void setCustService(CustService custService) {
		this.custService = custService;
	}
	
	public CustLocnService getCustLocnService() {
		return custLocnService;
	}

	public void setCustLocnService(CustLocnService custLocnService) {
		this.custLocnService = custLocnService;
	}
	
	public CustCertService getCustCertService() {
		return custCertService;
	}

	public void setCustCertService(CustCertService custCertService) {
		this.custCertService = custCertService;
	}

	public ListCodesService getListCodesService() {
		return listCodesService;
	}

	public void setListCodesService(ListCodesService listCodesService) {
		this.listCodesService = listCodesService;
	}
	
	public SimpleSelection getSelection() {
		return selection;
	}
	
	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}
	
	public EntityLevelService getEntityLevelService() {
		return entityLevelService;
	}

	public void setFilterEntityItem(EntityItemDTO filterEntityItem){
		this.filterEntityItem = filterEntityItem;
	}
	
	public EntityItemDTO getFilterEntityItem(){
		return this.filterEntityItem;
	}

	public void setEntityLevelService(EntityLevelService entityLevelService) {
		this.entityLevelService = entityLevelService;
	}
	
	public void setCustCertDataModel(CustCertDataModel custCertDataModel) {
		this.custCertDataModel = custCertDataModel;
	}
	
	public CustCertDataModel getCustCertDataModel() {
		return custCertDataModel;
	}
	
	public loginBean getLoginBean() {
		return loginBean;
	}
	
	public void setLoginBean(loginBean loginBean) {
		this.loginBean = loginBean;
	}

	public int getSelectedCustRowIndex() {
		return selectedCustRowIndex;
	}
	
	public List<SelectItem> getFilterJurisdictionCountyMenuItems() { 
	   if (filterJurisdictionCountyMenuItems == null) {
		   filterJurisdictionCountyMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCountyMenuItems.add(new SelectItem("",""));
		   List<String> listCounty = jurisdictionService.findTaxCodeCounty(filterJurisdictionCountry, filterJurisdictionState);
		   for (String countyname : listCounty) {
			   if(countyname!=null && countyname.length()>0){
				   filterJurisdictionCountyMenuItems.add(new SelectItem(countyname,countyname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCountyMenuItems;
	}
		
	public List<SelectItem> getFilterJurisdictionCityMenuItems() { 
	   if (filterJurisdictionCityMenuItems == null) {		   
		   filterJurisdictionCityMenuItems = new ArrayList<SelectItem>();
		   filterJurisdictionCityMenuItems.add(new SelectItem("",""));
		   List<String> listCity = jurisdictionService.findTaxCodeCity(filterJurisdictionCountry, filterJurisdictionState);
		   for (String cityname : listCity) {
			   if(cityname!=null && cityname.length()>0){
				   filterJurisdictionCityMenuItems.add(new SelectItem(cityname,cityname));
			   }
		   }  
	   }	    
	   return filterJurisdictionCityMenuItems;
	}
    
    public void selectAllCheckboxChecked(ActionEvent e) throws AbortProcessingException {
		HtmlSelectBooleanCheckbox chk = (HtmlSelectBooleanCheckbox) ((HtmlAjaxSupport) e.getComponent()).getParent();
		if((Boolean) chk.getValue()) {
			selectAll = true;
		}
		else {
			selectAll = false;
		}
		
		for (EntityItem entityItem : retrieveEntityItemList) {
			entityItem.setSelectBooleanFlag(selectAll);

		}
	}
	
	public void retrieveFilterEntity() {
		//Retrieve entities
		List<EntityItem> list = entityItemService.findAllEntityItemsWithFilter(filterEntityItem.getEntityLevelId(), 
				filterEntityItem.getEntityCode().trim(), filterEntityItem.getEntityName().trim());
		
		selectAll = false;	
		retrieveEntityItemList = list;
	}
	
	public void retrieveFilterCustCert() {
		selectedCustRowIndex = -1;
		selCustCert = new CustCert();
		
		//CustCert aCustCert = new CustCert();
		//Get entity id
		if(entityCode!=null && entityCode.length()>0){
			EntityItem entityItem = entityItemService.findByCode(entityCode);
			if(entityItem!=null){
				filterCustCert.setEntityId(entityItem.getEntityId());
			}
			else{
				filterCustCert.setEntityId(-1L);
			}
		}
		
		/*
		if(filterCust.getCustNbr()!=null && filterCust.getCustNbr().trim().length()>0){
			aCustCert.setCustNbr(filterCust.getCustNbr().trim());
		}
		if(filterCust.getCustName()!=null && filterCust.getCustName().trim().length()>0){
			aCustCert.setCustName(filterCust.getCustName().trim());
		}
		if(filterCust.getActiveFlag()!=null && filterCust.getActiveFlag().length()>0){
			if(filterCust.getActiveFlag().equals("1")){
				aCustCert.setActiveFlag("1");
			}
			else{
				aCustCert.setActiveFlag("0");
			}
		}
		*/
		
		custCertDataModel.setCriteria(filterCustCert);
	}

	public void selectedCustRowChanged(ActionEvent e) throws AbortProcessingException {
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent(); 
		setSelectedCustRowIndex(table.getRowIndex());

		selCustCert = new CustCert();
		selCustCert = (CustCert)table.getRowData();
	}
	
	public String getActionText() {
		if(currentAction.equals(EditAction.ADD)){
			return "Add ";
		}
		else if(currentAction.equals(EditAction.COPY_ADD)){
			return "Copy/Add";
		}
		else if(currentAction.equals(EditAction.UPDATE)){
			return "Update";
		}
		else if(currentAction.equals(EditAction.DELETE)){
			return "Delete";
		}
		else if(currentAction.equals(EditAction.VIEW)){
			return "View";
		}

		return "";
	}
	
	public Boolean getDisplayCopyAddAction() {
		return (currentAction == EditAction.COPY_ADD);
	}
	
	public Boolean getDisplayAddAction() {
		return (currentAction == EditAction.ADD);
	}
	
	public Boolean getDisplayDeleteAction() {
		return (currentAction == EditAction.DELETE);
	}
	
	public Boolean getDisplayUpdateAction() {
		return (currentAction == EditAction.UPDATE);
	}
	
	public Boolean getDisplayViewAction() {
		return (currentExempAction == EditExempAction.VIEW_CERT) || (currentExempAction == EditExempAction.VIEW_EXEMP);
	}
	
	public Map<Long, String >  getEntityLevelMap(){
		if(entityLevelMap==null){
			entityLevelMap = new LinkedHashMap<Long, String >();

		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelMap.put(entityLevel.getEntityLevelId(), entityLevel.getDescription());
			}
	    }
		return entityLevelMap;
	}

	public List<EntityItem> getRetrieveEntityItemList() {
	 	return retrieveEntityItemList;
	 }
	
	public List<SelectItem> getEntityLevelList(){
		if(entityLevelList==null){
			entityLevelList = new ArrayList<SelectItem>();
		    List<EntityLevel> aList = entityLevelService.findAllEntityLevels();
			for (EntityLevel entityLevel : aList) {
				entityLevelList.add(new SelectItem(entityLevel.getEntityLevelId(), entityLevel.getDescription()));
			}
	    }
		return entityLevelList;
	}
	
	public SearchJurisdictionHandler getFilterHandler() {
		return filterHandler;
	}
	
	public void resetFilter() {
		filterHandler.reset();
		filterHandler.setCountry("");
		filterHandler.setState("");
	}
	
	public String okCustAction() {
		String result = null;
		switch (currentAction) {
			case DELETE:
				result = deleteCustAction();
				break;
				
			case UPDATE:
				result = updateCustAction();
				break;
				
			case ADD:
				result = addCustAction();
				break;
				
			case VIEW:
				result = "customerLocation_main";
				break;
				
			default:
				result = cancelAction();
				break;
		}
		return result;
	}
	
	public String okCustAndAddAction(){
		String result =  addCustAction();
		if(result==null || result.length()==0){
			return result;
		}
		
		this.currentAction = EditAction.ADD;
		
		updateCustLocn = new CustLocn();
		updateCustLocn.setActiveBooleanFlag(true);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		
		return "custlocn_update";
	}
	
	public String addCustAction() {
		String result = null;
		try {
			String errorMessage = "";
			
			//CustEntity
			List<CustEntity> updateCustEntityList = new ArrayList<CustEntity>();
			
			//At least one entity must be checked
			boolean foundOne = false;
			for (EntityItem entityItem : retrieveEntityItemList) {
				if(entityItem.getSelectBooleanFlag()){
					foundOne = true;
					
					CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
					CustEntity aCustEntity = new CustEntity();
					aCustEntity.setId(aCustEntityPK);
					updateCustEntityList.add(aCustEntity);
				}
			}
			if(!foundOne){
				errorMessage = errorMessage + " " + "At least one Entity must be checked.";
			}

			if(errorMessage!=null && errorMessage.length()>0){
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}
			
			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String updateCustAction() {
		String result = null;
		try {
			String errorMessage = "";
			String errorTemp = "";
			
			//CustEntity
			List<CustEntity> updateCustEntityList = new ArrayList<CustEntity>();
			
			//At least one entity must be checked
			boolean foundOne = false;
			for (EntityItem entityItem : retrieveEntityItemList) {
				if(entityItem.getSelectBooleanFlag()){
					foundOne = true;
					
					CustEntityPK aCustEntityPK = new CustEntityPK(0L, entityItem.getEntityId());
					CustEntity aCustEntity = new CustEntity();
					aCustEntity.setId(aCustEntityPK);
					updateCustEntityList.add(aCustEntity);
				}
			}
			if(!foundOne){
				errorMessage = errorMessage + " " + "At least one Entity must be checked.";
			}

			if(errorMessage!=null && errorMessage.length()>0){
				FacesMessage message = new FacesMessage(errorMessage);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}

			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	public String deleteCustAction() {
		String result = null;
		logger.info("start addAction"); 
		try {
			String errorMessage = "";
			
			//Refresh & Retrieve
			retrieveFilterCustCert();
			
		    result = "customerLocation_main";		
		} catch (Exception ex) {
			ex.printStackTrace();
			result = "";
		}
		   
        return result;
	}
	
	boolean isAdminRole(){
		return (loginBean.getRole().getRoleCode().equalsIgnoreCase("ADMIN"));
	}
	
	public boolean getSelectAll(){
		return selectAll;
	}
	
	public void setSelectAll(boolean selectAll){
		this.selectAll = selectAll;
	}
	
	public String displayAddCustAction() {
		this.currentAction = EditAction.ADD;

		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevelId(0l);
		filterEntityItem.setEntityCode("");
		filterEntityItem.setEntityName("");
		
		retrieveFilterEntity();
		
		selectAll = false;

		return "cust_update";
	}
	
	public String displayViewCustAction(){
		displayUpdateCustAction();
		this.currentAction = EditAction.VIEW;
		
		return "cust_update";
	}
	
	public String displayUpdateCustAction() {
		this.currentAction = EditAction.UPDATE;

		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevelId(0l);
		filterEntityItem.setEntityCode("");
		filterEntityItem.setEntityName("");
		
		updateCustLocn = new CustLocn();

		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		filterHandler.setGeocode(updateCustLocn.getGeocode());
		filterHandler.setCountry(updateCustLocn.getCountry());
		filterHandler.setState(updateCustLocn.getState());
		filterHandler.setCounty(updateCustLocn.getCounty());
		filterHandler.setCity(updateCustLocn.getCity());
		filterHandler.setZip(updateCustLocn.getZip());
		updateCustLocn.setZipplus4("");
		
		//Set selected entities
		retrieveFilterEntity();

		Hashtable<Long, CustEntity> custEntityHashtable = new Hashtable<Long, CustEntity>();
		
		for (EntityItem aEntityItem : retrieveEntityItemList){
			if(custEntityHashtable.get(aEntityItem.getEntityId())!=null){
				aEntityItem.setSelectBooleanFlag(true);
			}
		}
		
		return "cust_update";
	}
	
	public String displayDeleteCustAction() {
		displayUpdateCustAction();
		this.currentAction = EditAction.DELETE;
		
		return "cust_update";
	}
	
	public String displayAddCustLocnAction() {
		this.currentAction = EditAction.ADD;

		updateCustLocn = new CustLocn();
		updateCustLocn.setActiveBooleanFlag(true);
		
		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		
		return "custlocn_update";
	}
	
	public String displayViewCustLocnAction(){
		displayUpdateCustLocnAction();
		this.currentAction = EditAction.VIEW;
		
		return "custlocn_update";
	}
	
	public String displayUpdateCustLocnAction() {
		this.currentAction = EditAction.UPDATE;
		
		updateCustLocn = new CustLocn();

		//Set Jurisdiction filter
		filterHandler.setJurisdictionService(getJurisdictionService());
		filterHandler.setCacheManager(getCacheManager());
		resetFilter();
		filterHandler.setGeocode(updateCustLocn.getGeocode());
		filterHandler.setCountry(updateCustLocn.getCountry());
		filterHandler.setState(updateCustLocn.getState());
		filterHandler.setCounty(updateCustLocn.getCounty());
		filterHandler.setCity(updateCustLocn.getCity());
		filterHandler.setZip(updateCustLocn.getZip());
		updateCustLocn.setZipplus4("");
		
		return "custlocn_update";
	}
	
	public String displayDeleteCustLocnAction() {
		displayUpdateCustLocnAction();
		this.currentAction = EditAction.DELETE;
		
		return "custlocn_update";
	}
	
	public String resetJurisdictionSearchAction(){
		filterJurisdictionCounty = "";
		filterJurisdictionCity = "";

		return null;
	}
	
	public Boolean getIsOkAddEnabled() {
		return currentExempAction.equals(EditExempAction.COPY_ADD_CERT) || currentExempAction.equals(EditExempAction.ADD_CERT) ;
	}
	
	public Boolean getDisplayCopyAddButtons() {
		return (selCustCert!=null && selCustCert.getExemptionTypeCode()!=null && selCustCert.getExemptionTypeCode().equals("2")); //Certificate
	}
	
	public String cancelAction() {
		return "exemptionCertificate_main";
	}
	
	public Boolean getIsViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	public Boolean getIsUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}

	public Boolean getDisplayCustButtons() {
		return (selectedCustRowIndex!=-1);
	}
	
	public Boolean getDisplayValidateButtons() {
		return (selectedCustRowIndex!=-1 && selCustCert!=null && selCustCert.getExemptionTypeCode().equals("1")); 
	}
	
	public Boolean getIsAddFromValidate() {
		return (currentExempAction.equals(EditExempAction.ADD_CERT_FROM_VALIDATE)); 
	}
	
	public Boolean getDisplayCustDeleteButtons() {
		return (selectedCustRowIndex!=-1 && isAdminRole());
	}
	
	public String resetFilterEntitySearchAction(){
		filterEntityItem = new EntityItemDTO();
		filterEntityItem.setEntityLevel("0");
		return null;
	}
	
	public String resetFilterSearchAction(){
		//clear filter on maintenance
		filterCust = new Cust();
		filterCustCert = new CustCert();
		entityCode=null;
		return null;
	}

	public CacheManager getCacheManager() {
		return this.cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}   

	public FilePreferenceBackingBean getFilePreferenceBean() {
		return this.filePreferenceBean;
	}

	public void setFilePreferenceBean(FilePreferenceBackingBean filePreferenceBean) {
		this.filePreferenceBean = filePreferenceBean;
	}

	public Cust getFilterCust() {
		return filterCust;
	}

	public void setFilterCust(Cust filterCust) {
		this.filterCust = filterCust;
	}
	
	public CustCert getFilterCustCert() {
		return filterCustCert;
	}

	public void setFilterCustLocn(CustCert filterCustCert) {
		this.filterCustCert = filterCustCert;
	}
	
	public CustCert getUpdateCustCert() {
		return updateCustCert;
	}

	public void setUpdateCustCert(CustCert updateCustCert) {
		this.updateCustCert = updateCustCert;
	}
	
	public CustLocn getUpdateCustLocn() {
		return updateCustLocn;
	}

	public void setUpdateCustLocn(CustLocn updateCustLocn) {
		this.updateCustLocn = updateCustLocn;
	}

	public void setSelectedCustRowIndex(int selectedCustRowIndex) {
		this.selectedCustRowIndex = selectedCustRowIndex;
	}
	
	public void sortAction(ActionEvent e) {
		custCertDataModel.toggleSortOrder(e.getComponent().getParent().getId());
	}
	
	public void countySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCity = "";
			setInputValue(cityInput, "");
		}
	}
	
	public void citySelected(ActionEvent e) {
		String value = (String) ((HtmlSelectOneMenu) ((HtmlAjaxSupport) e.getComponent()).getParent()).getSubmittedValue();
		if(StringUtils.hasText(value)) {
			filterJurisdictionCounty = "";
			setInputValue(countyInput, "");
		}
	}

	public HtmlSelectOneMenu getCountyInput() {
		return countyInput;
	}

	public void setCountyInput(HtmlSelectOneMenu countyInput) {
		this.countyInput = countyInput;
	}

	public HtmlSelectOneMenu getCityInput() {
		return cityInput;
	}

	public void setCityInput(HtmlSelectOneMenu cityInput) {
		this.cityInput = cityInput;
	}
	
	private void setInputValue(UIInput input, String value) {
		input.setLocalValueSet(false);
		input.setSubmittedValue(null);
		input.setValue(value);
	}
	
	public String searchCustomerLocationAction(){
		Cust aFilterCust = new Cust();
		aFilterCust.setCustNbr(updateCustCert.getCustNbr());
		
		CustLocn aFilterCustLocn = new CustLocn();
		aFilterCustLocn.setCustLocnCode(updateCustCert.getCustLocnCode());
		
		customerLocationLookupBean.setBean(this, "exemptionCertificateBean");
		
		return customerLocationLookupBean.beginLookupAction(aFilterCust, aFilterCustLocn);
	}
	
	public void searchAction(ActionEvent e) {

	}
	
	public void custLocnSelectionChanged(Cust aCust, CustLocn aCustLocn) {
		exemptionCustomerSelected = aCust.getCustId();
		
		updateCustCert.setCustNbr(aCust.getCustNbr());
		updateCustCert.setCustName(aCust.getCustName());	
		updateCustCert.setCustLocnCode(aCustLocn.getCustLocnCode());
		updateCustCert.setCustLocnName(aCustLocn.getLocationName());
		
		if(selEntityMenuItems!=null){
			selEntityMenuItems.clear();
			selEntityMenuItems = null;
		}
		
	}
	
	public void recalCustomerNumber(ValueChangeEvent vce) {
		String newCustNbr= (String)vce.getNewValue();
		updateCustCert.setCustNbr(newCustNbr);
		
		if(newCustNbr==null || newCustNbr.length()==0){
			exemptionCustomerSelected = 0L;
			updateCustCert.setCustNbr("");
			updateCustCert.setCustName("");	
			
			if(selEntityMenuItems!=null){
				selEntityMenuItems.clear();
				selEntityMenuItems = null;
			}
			getSelEntityMenuItems();
		}
		else {
			Cust newCust = custService.getCustByNumber(newCustNbr.trim());
			if(newCust==null){
				exemptionCustomerSelected = 0L;
				
				updateCustCert.setCustNbr("");
				updateCustCert.setCustName("");	
				
				if(selEntityMenuItems!=null){
					selEntityMenuItems.clear();
					selEntityMenuItems = null;
				}	
				getSelEntityMenuItems();
			}
			else{
				exemptionCustomerSelected = newCust.getCustId();
				
				updateCustCert.setCustNbr(newCust.getCustNbr());
				updateCustCert.setCustName(newCust.getCustName());	

				if(selEntityMenuItems!=null){
					selEntityMenuItems.clear();
					selEntityMenuItems = null;
				}	
				getSelEntityMenuItems();
			}
			
		}  
	}
	
	public void recalLocationCode(ValueChangeEvent vce) {
		String newCustLocnCode= (String)vce.getNewValue();
		updateCustCert.setCustLocnCode(newCustLocnCode);
		
		if(newCustLocnCode==null || newCustLocnCode.length()==0){
			updateCustCert.setCustLocnCode("");
			updateCustCert.setCustLocnName("");
		}
		else{
			if(updateCustCert.getCustNbr()!=null && updateCustCert.getCustNbr().length()>0){
				Cust newCust = custService.getCustByNumber(updateCustCert.getCustNbr().trim());
				if(newCust!=null){
					//Customer number is valid.
					
					CustLocn newCustLocn = custLocnService.getCustLocnByCustId(newCust.getCustId(), newCustLocnCode.trim());
					
					if(newCustLocn!=null){
						updateCustCert.setCustLocnName(newCustLocn.getLocationName());
					}
					else{
						updateCustCert.setCustLocnName("");
					}
				}
				else{
					updateCustCert.setCustLocnName("");
				}
			}
			else{
				updateCustCert.setCustLocnName("");
			}
		}  
	}
	
	private boolean isValidSelection(){
		boolean rtnBoolean = true;
		
		String custNbr = updateCustCert.getCustNbr();
		String custLocnCode = updateCustCert.getCustLocnCode();
		Long entityId = updateCustCert.getEntityId();
		String exemptionTypeCode = updateCustCert.getExemptionTypeCode();
		
		Cust newCust = null;
		if(custNbr==null || custNbr.length()==0){
			FacesMessage msg = new FacesMessage("A Customer must be selected.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			rtnBoolean = false;
		}
		else{
			newCust = custService.getCustByNumber(custNbr);
			if(newCust==null){
				FacesMessage msg = new FacesMessage("A Customer must be valid.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				rtnBoolean = false;
			}
		}
		
		CustLocn newCustLocn = null;
		if(custLocnCode==null || custLocnCode.length()==0){
			FacesMessage msg = new FacesMessage("A Location must be selected.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			rtnBoolean = false;
		}
		else{
			if(newCust!=null){
				newCustLocn = custLocnService.getCustLocnByCustId(newCust.getCustId(), custLocnCode);
				
				if(newCustLocn==null){
					FacesMessage msg = new FacesMessage("A Location must be valid in the selected Customer.");
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, msg);
					rtnBoolean = false;
				}
			}
		}
		
		if(entityId==null || entityId == 0L){
			FacesMessage msg = new FacesMessage("An Entity must be selected.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			rtnBoolean = false;
		}
		
		if(exemptionTypeCode==null || exemptionTypeCode.length()==0){
			FacesMessage msg = new FacesMessage("An Exemption Type must be selected.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			rtnBoolean = false;
		}
		
		/* By MF
		if(rtnBoolean){
			CustCert aCustCert = new CustCert();
			aCustCert.setCustLocnId(newCustLocn.getCustLocnId());
			aCustCert.setEntityId(updateCustCert.getEntityId());
			aCustCert.setExemptionTypeCode(updateCustCert.getExemptionTypeCode());
			
			if(!custCertService.validateUniqueExemption(aCustCert)){
				FacesMessage msg = new FacesMessage("The Customer, Location, Entity, and Exemption Type must be unique.");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);

				rtnBoolean = false;
			}
		}
		*/
		
		//To make sure upper/lower cases are matched
		if(rtnBoolean){
			exemptionCustomerSelected = newCust.getCustId();
			updateCustCert.setCustLocnId(newCustLocn.getCustLocnId());
			
			updateCustCert.setCustNbr(newCust.getCustNbr());
			updateCustCert.setCustLocnCode(newCustLocn.getCustLocnCode());
		}
		
		return rtnBoolean;
	}
	  
	public boolean getIsCertificateSelected() {
		return (showhide!=null && showhide.equals("show"));
	}
	
	public void selectexemptCertActionListener(ActionEvent event) {	
		showhide = " ";	 
	}
	
	public String closeAction() {
		return "customerLocation_main";
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	  
	public boolean getIsEntityCheck() {
		return isEntityCheck;
	}

	public void setEntityCheck(boolean isEntityCheck) {
		this.isEntityCheck = isEntityCheck;
	}  
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }
}
