package com.ncsts.view.bean;
/**
 * @author Muneer Basha
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.ajax4jsf.component.html.HtmlAjaxSupport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.component.html.HtmlDataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.ncsts.common.CacheManager;
import com.ncsts.common.LoggerFactory;
import com.ncsts.domain.DataDefinitionColumn;
import com.ncsts.domain.DataDefinitionColumnPK;
import com.ncsts.domain.ImportDefinitionDetail;
import com.ncsts.domain.User;
import com.ncsts.dto.DataDefinitionColumnDTO;
import com.ncsts.dto.DataDefinitionTableDTO;
import com.ncsts.dto.UserDTO;
import com.ncsts.services.DataDefinitionService;
import com.ncsts.services.ImportDefAndSpecsService;
import com.ncsts.services.ImportDefinitionService;

public class DataDefinitionBean {
	
	private Logger logger = LoggerFactory.getInstance().getLogger(DataDefinitionBean.class);
	
	protected DataDefinitionService dataDefinitionService;	
	private CacheManager cacheManager;

	private List<DataDefinitionTableDTO> dataDefinitionTableList = new ArrayList<DataDefinitionTableDTO>();
	private List<DataDefinitionColumnDTO> dataDefinitionColumnList = null;
  
	private DataDefinitionTableDTO selectedDataDefinitionTable;
	private DataDefinitionColumnDTO selectedDataDefinitionColumn;
	private int selectedTableIndex = -1;
	private int selectedColumnIndex = -1;
  
	private DataDefinitionTableDTO dataDefinitionTableDTONew = new DataDefinitionTableDTO(); //this is declared for updating/adding DB Statistics line
	private DataDefinitionColumnDTO dataDefinitionColumnDTONew = new DataDefinitionColumnDTO();
	
	@Autowired
	private ImportDefAndSpecsService importDefAndSpecsService; 
	
	private ImportDefinitionService importDefinitionService;
  
	private int rowCount = 0;
  
  	private HtmlSelectOneMenu tableNameMenu;
  	private HtmlSelectOneMenu columnNameMenu;
  	private List<SelectItem> dataTypeMenu;
  	private HtmlSelectOneMenu descriptionColumnMenu;
  	
  	private EditAction currentAction = EditAction.VIEW;
  	
	private List<SelectItem> importdefComboItems;
  	
  	private String importdefcode = "";
  	
  	private boolean isImpdefcodeitemsSelect = true;
  	
  	private String resetType = null;
  	
  	List<String> impdefcodeList = null;
  	
  	private int datadefcolumnPageSize = 50;
	private int datadefcolumnPageNumber;
	
	@Autowired
	private loginBean loginBean;
	
  	
	public int getDatadefcolumnPageSize() {
		return datadefcolumnPageSize;
	}

	public void setDatadefcolumnPageSize(int datadefcolumnPageSize) {
		this.datadefcolumnPageSize = datadefcolumnPageSize;
	}

	public int getDatadefcolumnPageNumber() {
		return datadefcolumnPageNumber;
	}

	public void setDatadefcolumnPageNumber(int datadefcolumnPageNumber) {
		this.datadefcolumnPageNumber = datadefcolumnPageNumber;
	}

	public enum RadioResetType {
		RESETTODEFAULT("resettoDefault"),
		RESETTOIMPORTDEFINITIONCOLUMN("resettoimportDefinitionColumnHeadings");
		private String resettype;
		private RadioResetType(String resettype) {
			this.resettype= resettype;
		}
		public String getText() {
			return resettype;
		}
	}
  	
	public List<String> getImpdefcodeList() {
		return impdefcodeList;
	}

	public void setImpdefcodeList(List<String> impdefcodeList) {
		this.impdefcodeList = impdefcodeList;
	}

	public String getResetType() {
		return resetType;
	}

	public void setResetType(String resetType) {
		this.resetType = resetType;
	}

	public boolean getIsImpdefcodeitemsSelect() {
		return isImpdefcodeitemsSelect;
	}

	public void setImpdefcodeitemsSelect(boolean isImpdefcodeitemsSelect) {
		this.isImpdefcodeitemsSelect = isImpdefcodeitemsSelect;
	}

	public String getImportdefcode() {
		return importdefcode;
	}

	public void setImportdefcode(String importdefcode) {
		this.importdefcode = importdefcode;
	}

	public List<SelectItem> getImportdefComboItems() {
		return importdefComboItems;
	}

	public void setImportdefComboItems(List<SelectItem> importdefComboItems) {
		this.importdefComboItems = importdefComboItems;
	}

	public ImportDefAndSpecsService getImportDefAndSpecsService() {
		return importDefAndSpecsService;
	}

	public void setImportDefAndSpecsService(
			ImportDefAndSpecsService importDefAndSpecsService) {
		this.importDefAndSpecsService = importDefAndSpecsService;
	}
	
	public ImportDefinitionService getImportDefinitionService() {
		return importDefinitionService;
	}

	public void setImportDefinitionService(
			ImportDefinitionService importDefinitionService) {
		this.importDefinitionService = importDefinitionService;
	}

  	public CacheManager getCacheManager() {
		return this.cacheManager;
	}
  	
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
  
  	public HtmlSelectOneMenu getTableNameMenu() {				
  		if (tableNameMenu == null){			
			tableNameMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> tableNameList = new ArrayList<SelectItem>();		
			tableNameList.add(new SelectItem("","Select Table Name"));
		
			List<String> definitionList = dataDefinitionService.getTableNameList();
	
			for (String dataDefinition : definitionList) {			
				if(dataDefinition != null && dataDefinition!=null){	
					
					//CCH 
					if(dataDefinition.equalsIgnoreCase("TB_CCH_CODE") || 
					   dataDefinition.equalsIgnoreCase("TB_CCH_TXMATRIX") ||
					   dataDefinition.equalsIgnoreCase("TB_BCP_CCH_CODE") || 
					   dataDefinition.equalsIgnoreCase("TB_BCP_CCH_TXMATRIX")) continue;
					
					tableNameList.add(new SelectItem(dataDefinition,dataDefinition));
				}
			}
		
			final UISelectItems items = new UISelectItems();
			items.setValue(tableNameList);
			tableNameMenu.getChildren().add(items);
		}	
		
		return tableNameMenu;
  	}

  	public HtmlSelectOneMenu getColumnNameMenu() {				
  		if (columnNameMenu == null){			
  			columnNameMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> columnNameList = new ArrayList<SelectItem>();		
			columnNameList.add(new SelectItem("","Select Column Name"));
				
			String tableName = selectedDataDefinitionTable.getTableName();
			List<String> definitionList = dataDefinitionService.getColumnNameList(tableName);
	
			for (String dataDefinition : definitionList) {			
				if(dataDefinition != null && dataDefinition!=null){		
					
					//CCH
					if(tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_TAXCAT_CODE") ||
						
						tableName.equalsIgnoreCase("TB_BCP_TRANSACTIONS") && dataDefinition.equalsIgnoreCase("CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_BCP_TRANSACTIONS") && dataDefinition.equalsIgnoreCase("CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_BCP_TRANSACTIONS") && dataDefinition.equalsIgnoreCase("CCH_TAXCAT_CODE") || 
						
						tableName.equalsIgnoreCase("TB_TMP_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TMP_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_TMP_TRANSACTION_DETAIL") && dataDefinition.equalsIgnoreCase("CCH_TAXCAT_CODE") || 
						
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("NEW_CCH_TAXCAT_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("NEW_CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("NEW_CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("OLD_CCH_TAXCAT_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("OLD_CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TRANSACTION_DETAIL_EXCP") && dataDefinition.equalsIgnoreCase("OLD_CCH_ITEM_CODE") || 
							
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("ELSE_CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("ELSE_CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("ELSE_CCH_TAXCAT_CODE") || 
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("THEN_CCH_GROUP_CODE") || 
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("THEN_CCH_ITEM_CODE") || 
						tableName.equalsIgnoreCase("TB_TAX_MATRIX") && dataDefinition.equalsIgnoreCase("THEN_CCH_TAXCAT_CODE")) continue;
					
					columnNameList.add(new SelectItem(dataDefinition,dataDefinition));
				}
			}
		
			final UISelectItems items = new UISelectItems();
			items.setValue(columnNameList);
			columnNameMenu.getChildren().add(items);
		}	
		
		return columnNameMenu;
  	}
  	
  	public List<SelectItem> getDataTypeMenu() {				
  		if (dataTypeMenu == null){						
			final List<SelectItem> dataTypeList = new ArrayList<SelectItem>();	
			
			dataTypeList.add(new SelectItem("","Select Data Type"));
			List<String> definitionList = dataDefinitionService.getDataTypeList();
			for (String dataDefinition : definitionList) {			
				if(dataDefinition != null && dataDefinition!=null){				
					dataTypeList.add(new SelectItem(dataDefinition,dataDefinition));
				}
			}
			dataTypeMenu = dataTypeList;
		}	
		
		return dataTypeMenu;
  	}
  	
  	public HtmlSelectOneMenu getDescriptionColumnMenu() {	
  		logger.info("start getDescriptionColumnMenu: here1" ); 
  		if (descriptionColumnMenu == null){
  			logger.info("start getDescriptionColumnMenu: here2" ); 
  			descriptionColumnMenu = new HtmlSelectOneMenu();			
			final Collection<SelectItem> descriptionColumnList = new ArrayList<SelectItem>();		
			descriptionColumnList.add(new SelectItem("","Select Desc. Column"));	
			
			String tableName = selectedDataDefinitionTable.getTableName();
			List<String> definitionList = dataDefinitionService.getDescriptionColumnList(tableName);
			logger.info("start getDescriptionColumnMenu: " + definitionList.size()); 
			
			for (String dataDefinition : definitionList) {			
				if(dataDefinition != null && dataDefinition!=null){			
					logger.info(" value: " + dataDefinition); 
					descriptionColumnList.add(new SelectItem(dataDefinition,dataDefinition));
				}
			}
		
			final UISelectItems items = new UISelectItems();
			items.setValue(descriptionColumnList);
			descriptionColumnMenu.getChildren().add(items);
		}	
		
		return descriptionColumnMenu;
  	}

  	public void setDescriptionColumnMenu(HtmlSelectOneMenu descriptionColumnMenu) {
  		this.descriptionColumnMenu = descriptionColumnMenu;
  	}
  	
  	public void setTableNameMenu(HtmlSelectOneMenu tableNameMenu) {
  		this.tableNameMenu = tableNameMenu;
  	}
  	
  	public void setColumnNameMenu(HtmlSelectOneMenu columnNameMenu) {
  		this.columnNameMenu = columnNameMenu;
  	}
  	
  	public void setDataTypeMenu(List<SelectItem> dataTypeMenu) {
  		this.dataTypeMenu = dataTypeMenu;
  	}
  
  	public String getActionText() {
		return currentAction.getActionText();
	}
  	
  	public void fetchDataDefinitionTableList(){
	  dataDefinitionTableList = new ArrayList<DataDefinitionTableDTO>();
	  dataDefinitionTableList=this.dataDefinitionService.getAllDataDefinitionTable();
	  selectedTableIndex = -1;
	  selectedDataDefinitionTable = null;
	  
	  if(dataDefinitionTableList!=null)
		  rowCount = dataDefinitionTableList.size();	  
  	}
  	
  	public void fetchDataDefinitionColumnList(){
  		if(selectedDataDefinitionTable!=null){
  			String dataDefinitionTableName = selectedDataDefinitionTable.getTableName();
  			DataDefinitionColumnDTO dataDefinitionColumnDTO = new DataDefinitionColumnDTO();
  			DataDefinitionColumnPK dataDefinitionColumnPK = new DataDefinitionColumnPK();
  			dataDefinitionColumnPK.setTableName(dataDefinitionTableName);
  			dataDefinitionColumnDTO.setDataDefinitionColumnPK(dataDefinitionColumnPK);
  			dataDefinitionColumnList = new ArrayList<DataDefinitionColumnDTO>();
  			dataDefinitionColumnList=this.dataDefinitionService.getAllDataDefinitionColumn(dataDefinitionColumnDTO);
  			logger.debug("dataDefinitionColumnList.size:"+dataDefinitionColumnList.size());
  		}
  		else{
  			dataDefinitionColumnList=null;
  		}
		
		selectedColumnIndex = -1;
		selectedDataDefinitionColumn = null;
    }
  
  	public void selectedTableRowChanged(ActionEvent e) throws AbortProcessingException { 
  		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDataDefinitionTable = (DataDefinitionTableDTO) table.getRowData();
		this.selectedTableIndex = table.getRowIndex();
		
		if (selectedDataDefinitionTable != null) {
			String dataDefinitionTableName = selectedDataDefinitionTable.getTableName();
			logger.debug("Selected Table Name ::"+dataDefinitionTableName);
			dataDefinitionColumnList = null;
		}
	}

  	public void selectedColumnRowChanged(ActionEvent e) throws AbortProcessingException { 
		HtmlDataTable table = (HtmlDataTable) ((HtmlAjaxSupport) e.getComponent()).getParent();
		this.selectedDataDefinitionColumn = (DataDefinitionColumnDTO) table.getRowData();
		this.selectedColumnIndex = table.getRowIndex();
		
		logger.debug("selectedColumnRowChanged: "+selectedDataDefinitionColumn.getColumnName());
		logger.debug("selectedColumnRowChanged: "+selectedDataDefinitionColumn.getTableName());
		logger.debug("selectedColumnIndex: "+selectedColumnIndex);
	}
  	
  	public String getPageDescription() {
		int rows = dataDefinitionColumnList == null ? 0 : dataDefinitionColumnList.size();
		int start = rows == 0 ? 0 : ((datadefcolumnPageNumber - 1) * datadefcolumnPageSize) + 1;
		int end = datadefcolumnPageNumber * datadefcolumnPageSize;
		if(end > rows) {
			end = rows;
		}
		
		String desc = String.format("Displaying rows %d through %d (%d matching rows)", start, end, rows);
		return desc;
	}
 
	public DataDefinitionService getDataDefinitionService() {
		return dataDefinitionService;
	}
	
	public void setDataDefinitionService(DataDefinitionService dataDefinitionService) {
		this.dataDefinitionService = dataDefinitionService;
	}
	
	
	public List<DataDefinitionTableDTO> getDataDefinitionTableList() {
		
		dataDefinitionTableList = dataDefinitionService.getAllDataDefinitionTable();
		this.setDataDefinitionTableList(dataDefinitionTableList);
	
		if(dataDefinitionTableList != null)
		     logger.debug(" dataDefinitionTableList size "+dataDefinitionTableList.size());
		else
			logger.debug(" dataDefinitionTableList is Null");
		
		return dataDefinitionTableList;
	}
	
	public void setDataDefinitionTableList(List<DataDefinitionTableDTO> dataDefinitionTableList) {
		this.dataDefinitionTableList = dataDefinitionTableList;
	}
	
	public List<DataDefinitionColumnDTO> getDataDefinitionColumnList() {
		if(dataDefinitionColumnList == null){
			fetchDataDefinitionColumnList();
			datadefcolumnPageNumber = 1;
		}
		return dataDefinitionColumnList;
	}
	
	public void setDataDefinitionColumnList(
			List<DataDefinitionColumnDTO> dataDefinitionColumnList) {
		this.dataDefinitionColumnList = dataDefinitionColumnList;
	}
	
	public DataDefinitionTableDTO getSelectedDataDefinitionTable() {
		return selectedDataDefinitionTable;
	}
	
	public void setSelectedDataDefinitionTable(DataDefinitionTableDTO selectedDataDefinitionTable) {
		this.selectedDataDefinitionTable = selectedDataDefinitionTable;
	}
	
	public int getRowCount() {
		return rowCount;
	}
	
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	public DataDefinitionColumnDTO getSelectedDataDefinitionColumn() {
		return selectedDataDefinitionColumn;
	}
	
	public void setSelectedDataDefinitionColumn(
			DataDefinitionColumnDTO selectedDataDefinitionColumn) {
		this.selectedDataDefinitionColumn = selectedDataDefinitionColumn;
	}
	 
	public int getSelectedTableIndex() {
		return selectedTableIndex;
	}

	public void setSelectedTableIndex(int selectedTableIndex) {
		this.selectedTableIndex = selectedTableIndex;
	}

	public int getSelectedColumnIndex() {
		return selectedColumnIndex;
	}

	public void setSelectedColumnIndex(int selectedColumnIndex) {
		this.selectedColumnIndex = selectedColumnIndex;
	}
	
	public String viewAction(){
		logger.info("start Cancel View Action for Data Definition");
		logger.info("end Cancel View Action for Data Definition Add");	    
	    return "datadefinition_main";
	  		
	}

	public DataDefinitionTableDTO getDataDefinitionTableDTONew() {
		return dataDefinitionTableDTONew;
	}
	
	public void setDataDefinitionTableDTONew(
			DataDefinitionTableDTO dataDefinitionTableDTONew) {
		this.dataDefinitionTableDTONew = dataDefinitionTableDTONew;
	}
	
	public DataDefinitionColumnDTO getDataDefinitionColumnDTONew() {
		return dataDefinitionColumnDTONew;
	}
	
	public void setDataDefinitionColumnDTONew(
			DataDefinitionColumnDTO dataDefinitionColumnDTONew) {
		this.dataDefinitionColumnDTONew = dataDefinitionColumnDTONew;
	}
	
	public void validateTableName(FacesContext context, UIComponent toValidate, Object value) {
	    // Ensure table name is unique
		String tableName = (String) value; 
		// TODO:  Are there other restrictions on table name?  All uppercase, no spaces, etc?
	    if (dataDefinitionService.findById(tableName) != null) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Table Name already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public boolean getDeleteAction() {
		return currentAction.equals(EditAction.DELETE);
	}

	public boolean getUpdateAction() {
		return currentAction.equals(EditAction.UPDATE);
	}
	
	public boolean getViewAction() {
		return currentAction.equals(EditAction.VIEW);
	}
	
	public void activateDataType(){
		
	}
	
	public String displayUpdateTableAction() {
		currentAction = EditAction.UPDATE;
		tableNameMenu = null;
		dataDefinitionTableDTONew = selectedDataDefinitionTable;
		return "go_to_datadef_update";
	}
	
	public String displayViewTableAction() {
		String returnScreen = displayUpdateTableAction();
		currentAction = EditAction.VIEW;
		return returnScreen;
	}
	
	public String displayDeleteTableAction() {
		currentAction = EditAction.DELETE;
		tableNameMenu = null;
		dataDefinitionTableDTONew = selectedDataDefinitionTable;
		return "go_to_datadef_delete";
	}	

	public String displayAddTableAction() {
		currentAction = EditAction.ADD;
		tableNameMenu = null;
		dataDefinitionTableDTONew =  new DataDefinitionTableDTO();
		return "go_to_datadef_add";
	}
	public void displayResetDescsTableAction(ActionEvent e) {
		resetType = RadioResetType.RESETTODEFAULT.getText();
		importdefcode = "";
		isImpdefcodeitemsSelect = true;
	}
	
	public String displayUpdateColumnAction() {
		currentAction = EditAction.UPDATE;
		columnNameMenu = null;
		descriptionColumnMenu = null;
		dataDefinitionColumnDTONew = selectedDataDefinitionColumn;
		return "go_to_datacolumn_update";
	}
	
	public String displayViewColumnAction() {
		String returnScreen = displayUpdateColumnAction();
		currentAction = EditAction.VIEW;
		return returnScreen;
	}
	
	public String displayDeleteColumnAction() {
		currentAction = EditAction.DELETE;
		columnNameMenu = null;
		descriptionColumnMenu = null;
		dataDefinitionColumnDTONew = selectedDataDefinitionColumn;
		return "go_to_datacolumn_delete";
	}	

	public String displayAddColumnAction() {
		currentAction = EditAction.ADD;
		columnNameMenu = null;
		descriptionColumnMenu = null;
		dataDefinitionColumnDTONew =  new DataDefinitionColumnDTO();
		DataDefinitionColumnPK dataDefinitionColumnPK = new DataDefinitionColumnPK();
		dataDefinitionColumnPK.setTableName(selectedDataDefinitionTable.getTableName());
		dataDefinitionColumnDTONew.setDataDefinitionColumnPK(dataDefinitionColumnPK);
		dataDefinitionColumnDTONew.setMapFlagDisplay(true);
		
		return "go_to_datacolumn_add";
	}
	
	public String okTableAction() {
		String result = null;

		switch (currentAction) {
			case ADD:
				result = addTableAction();
				break;
				
			case DELETE:
				result = removeTableAction();
				break;
				
			case UPDATE:
				result = updateTableAction();
				break;
				
			case VIEW:
				return "datadefinition_main";
		}
		
		return result;
	}
	
	public String okColumnAction() {
		String result = null;

		switch (currentAction) {
			case ADD:
				result = addColumnAction();
				break;
				
			case DELETE:
				result = removeColumnAction();
				break;
				
			case UPDATE:
				result = updateColumnAction();
				break;
				
			case VIEW:
				 return "datadefinition_main";
				
		}
		
		return result;
	}
	
	public String removeTableAction() {
		logger.info("start removeTableAction for DataDefination"); 
		dataDefinitionService.delete(dataDefinitionTableDTONew.getTableName());
		tableNameMenu = null;
		fetchDataDefinitionTableList();	
		fetchDataDefinitionColumnList();
	    return "datadefinition_main";
	  }

	public String updateTableAction() {
		logger.info("start updateTableAction for DataDefination"); 
		//5244
		String definitionTextArea = dataDefinitionTableDTONew.getDefinition();
		if(definitionTextArea!=null && definitionTextArea.length()>255){
			dataDefinitionTableDTONew.setDefinition(definitionTextArea.substring(0, 255));
		}
		dataDefinitionService.update(dataDefinitionTableDTONew);
		tableNameMenu = null;
		fetchDataDefinitionTableList();
		fetchDataDefinitionColumnList();
	    return "datadefinition_main";
	}
	
	public String addTableAction() {
		logger.info("start addTableAction for DataDefination "); 
		try {
			//5244
			String definitionTextArea = dataDefinitionTableDTONew.getDefinition();
			if(definitionTextArea!=null && definitionTextArea.length()>255){
				dataDefinitionTableDTONew.setDefinition(definitionTextArea.substring(0, 255));
			}
			this.dataDefinitionService.addNewDataDefinitionTable(dataDefinitionTableDTONew);
			dataDefinitionTableDTONew =  new DataDefinitionTableDTO();
			
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			logger.error("Error adding table");
			return "go_to_datadef_add";
		}
		
		tableNameMenu = null;
		fetchDataDefinitionTableList();
		fetchDataDefinitionColumnList();
		return "datadefinition_main";
	}

	public String removeColumnAction() {
		logger.info("start removeColumnAction for DataDefination"); 
		dataDefinitionService.delete(dataDefinitionColumnDTONew.getTableName() , dataDefinitionColumnDTONew.getColumnName());
		
		//Flush cached DataDefinitionMap
		cacheManager.flushDataDefinitionMap(dataDefinitionColumnDTONew.getDataDefinitionColumnPK().getTableName());
		
		fetchDataDefinitionColumnList();
		columnNameMenu = null;
		descriptionColumnMenu = null;
	    return "datadefinition_main";
	  }

	public String updateColumnAction() {
		logger.info("start updateColumnAction for DataDefination"); 
		dataDefinitionColumnDTONew.getDataDefinitionColumnPK().setColumnName(dataDefinitionColumnDTONew.getColumnName()); 
		
		//5244
		String definitionTextArea = dataDefinitionColumnDTONew.getDefinition();
		if(definitionTextArea!=null && definitionTextArea.length()>255){
			dataDefinitionColumnDTONew.setDefinition(definitionTextArea.substring(0, 255));
		}
		dataDefinitionService.update(dataDefinitionColumnDTONew);
		
		//Flush cached DataDefinitionMap
		cacheManager.flushDataDefinitionMap(dataDefinitionColumnDTONew.getDataDefinitionColumnPK().getTableName());

		fetchDataDefinitionColumnList();
		columnNameMenu = null;
		descriptionColumnMenu = null;
	    return "datadefinition_main";
	}
	
	public String addColumnAction() {
		try {
			dataDefinitionColumnDTONew.getDataDefinitionColumnPK().setColumnName(dataDefinitionColumnDTONew.getColumnName());
			
			//5244
			String definitionTextArea = dataDefinitionColumnDTONew.getDefinition();
			if(definitionTextArea!=null && definitionTextArea.length()>255){
				dataDefinitionColumnDTONew.setDefinition(definitionTextArea.substring(0, 255));
			}
			dataDefinitionService.addNewDataDefinitionColumn(dataDefinitionColumnDTONew);
			//Flush cached DataDefinitionMap
			cacheManager.flushDataDefinitionMap(dataDefinitionColumnDTONew.getDataDefinitionColumnPK().getTableName());
			
			dataDefinitionColumnDTONew =  new DataDefinitionColumnDTO();
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			logger.error("Error adding column");
			return "go_to_datacolumn_add";
		}

		fetchDataDefinitionColumnList();
		columnNameMenu = null;
		descriptionColumnMenu = null;
		return "datadefinition_main";
	}
	
	public void validateColumnName(FacesContext context, UIComponent toValidate, Object value) {
		String columnName = (String) value; 
	    if (dataDefinitionService.getDataDefinitionColumnByTable(dataDefinitionColumnDTONew.getTableName(), columnName) != null) {
			((UIInput)toValidate).setValid(false);
			FacesMessage message = new FacesMessage("Column Name already exists.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(toValidate.getClientId(context), message);
	    }
	}
	
	public List<SelectItem> getImpdefComboItems() {
		importdefComboItems = new ArrayList<SelectItem>();
		if(selectedDataDefinitionTable != null) {
			importdefComboItems.add(new SelectItem("","Select Import Definition"));
			impdefcodeList = importDefAndSpecsService.getImportDefinitionCodes(selectedDataDefinitionTable.getTableName());
			for (String importDefcode : impdefcodeList) {
				 importdefComboItems.add(new SelectItem(importDefcode));
				}
			return importdefComboItems;
		}
		return null;
	}
	
	public void updateImpdefcodeitems(ValueChangeEvent event) {
		resetType = (String)event.getNewValue();
		if(RadioResetType.RESETTOIMPORTDEFINITIONCOLUMN.getText().equals(resetType)) {
			isImpdefcodeitemsSelect = false; 
		}
		else {
			isImpdefcodeitemsSelect = true;
		}
	}
	public void resetDescsAction() {

		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		UserDTO userDTO = (UserDTO) session.getAttribute("user");
		if(RadioResetType.RESETTODEFAULT.getText().equals(resetType)) {
			
			List<DataDefinitionColumn> dataDefinitionColumnList =  dataDefinitionService.getAllDataDefinitionColumnByTable(this.selectedDataDefinitionTable.getTableName());
			for (DataDefinitionColumn dataDefinitionColumn : dataDefinitionColumnList) {
				String defaultvalue = processResetDefaultColumn(dataDefinitionColumn.getColumnName());
				dataDefinitionColumn.setDescription(StringUtils.substring(defaultvalue, 0, 50));
				dataDefinitionColumn.setAbbrDesc(StringUtils.substring(defaultvalue, 0, 20)); 
				dataDefinitionColumn.setDefinition(null);
				dataDefinitionColumn.setMinimumViewFlag("0");
				dataDefinitionColumn.setUpdateUserId(userDTO.getUserCode());
				dataDefinitionColumn.setUpdateTimestamp(new Date());
				if(dataDefinitionColumn.getColumnName().startsWith("USER")){
					dataDefinitionColumn.setDescColumnName(null);
				}
				dataDefinitionService.update(dataDefinitionColumn);
			}
		}
		else {
			String selectedimpdefcode = getImportdefcode();
			List<ImportDefinitionDetail> importDefinitionDetailList =   importDefinitionService.getResetImpDefColHeadings(selectedimpdefcode,this.selectedDataDefinitionTable.getTableName());
			for(ImportDefinitionDetail importDefinitionDetail : importDefinitionDetailList) {
				DataDefinitionColumn dataDefinitionColumn =  dataDefinitionService.getDataDefinitionColumnByTable(this.selectedDataDefinitionTable.getTableName(), importDefinitionDetail.getTransDtlColumnName());
				if(dataDefinitionColumn !=null) {
					String defaultvalue = processResetDefaultColumn(importDefinitionDetail.getImportColumnName());
					dataDefinitionColumn.setDescription(StringUtils.substring(defaultvalue, 0, 50));
					dataDefinitionColumn.setAbbrDesc(StringUtils.substring(defaultvalue, 0, 20)); 
					dataDefinitionColumn.setUpdateUserId(userDTO.getUserCode());
					dataDefinitionColumn.setUpdateTimestamp(new Date());
					dataDefinitionService.update(dataDefinitionColumn);
				}
			}
		}
		fetchDataDefinitionColumnList();
	}
	
	public String processResetDefaultColumn(String columnName) {
		columnName = columnName.replaceAll("_", " ").toLowerCase();
		char[] chars = columnName.toLowerCase().toCharArray();
	    boolean found = false;
	    for (int i = 0; i < chars.length; i++) {
	        if (!found && Character.isLetter(chars[i])) {
	           chars[i] = Character.toUpperCase(chars[i]);
	           found = true;
    	    } else if (Character.isWhitespace(chars[i])) {
	          found = false;
	       }
	    }
	    return String.valueOf(chars);
	}	
	
	public void cancelResetDescsAction() {
		resetType = null;
	}
	
	public Boolean getIsEnabledButton() {
		if(RadioResetType.RESETTOIMPORTDEFINITIONCOLUMN.getText().equals(getResetType()) &&  getImportdefcode().trim().length() == 0) {
			return true;
		}
		return false;
	}
	
	public User getCurrentUser() {
		return loginBean.getUser();
   }

}
